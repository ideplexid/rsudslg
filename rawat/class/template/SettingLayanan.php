<?php
/**
 * this class used to provide setting of each room
 * that based on rawat prototype
 * 
 * @author      : Nurul Huda
 * @since       : 17 agustus 2014
 * @database    : smis_adm_settings
 * @copyright   : goblooge@gmail.com
 * @license     : Apache 3 
 * @version     : 3.7.0
 * */
 
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'rawat/class/resource/KelasResource.php';
require_once 'rawat/class/resource/EmployeeResource.php';
class SettingsLayanan extends ModulTemplate {    
	/*@var string*/
	private $poliname;
	/*@var string*/
	private $polislug;
	/*@var Database*/
	private $db;
    /*@var SettingsBuilder*/
	private $settings;
	/*@var string*/
	private $kelas;
    /*@var string*/
	private $tab;

	public function __construct($db,$polislug,$poliname) {
		parent::__construct();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db       = $db;
		$slug           = $this->polislug;
		$this->settings = new SettingsBuilder($db,"layanan",$this->polislug,"setting_layanan","Settings ".$poliname);
		$this->settings ->setShowDescription(true);
		$this->settings ->setPrototype($this->poliname,$this->polislug,"rawat");
		$this->settings ->setTabulatorMode(Tabulator::$LANDSCAPE_RIGHT);
		$this->kelas    = getSettings($db,"smis-rs-kelas","kelas_i");
    }

    public function isGroup($code,$name,$icon){
        $this->tab = $code;
        $this->settings->addTabs($code,$name,$icon);
        return $this->settings->isGroup($code);
    }
    
    public function addItem($setname,$name,$value="",$type="text",$desc=""){
        $this->settings	->addItem($this->tab,new SettingsItem($this->db,$setname."-".$this->polislug,$name,$value,$type,$desc));
        return $this;    
    }

    public function addSection($section){
        $this->settings	->addSection($this->tab,$section);
        return $this;    
    }

    public function getJenisPasien(){
        $serv = new ServiceConsumer($this->db,"get_jenis_patient",NULL,"registration");
        $serv ->execute();
        $res  = $serv->getContent();
        return $res;
    }

    public function getExtention($service,$section=null){
        $serv   = new ServiceConsumer($this->db,$service,NULL);
        $serv   ->execute();
        $result = $serv->getContent();
        if(count($result)>0 && $section!=null){
            $this ->addSection($section); 
        }
        foreach($result as $x){
            $this	->addItem($x[0],$x[1],$x[2],$x[3],$x[4]);
        }
        return $this; 
    }

    public function addItemAccounting($setname,$autonotif=true){
        $this->addSection("AKUNTING");
        $res      = $this->getJenisPasien();
        if($autonotif){
            $this->settings ->addItem($this->tab,new SettingsItem($this->db,"smis-rs-acc-e-".$setname."","Aktifkan Acruel Base ","0","checkbox","Aktifkan Auto Notif Acruel Base Ke Akunting"));
        }
        foreach($res as $x){
            $slug = $x['value'];
            $nama = $x['name'];
            $this->settings ->addItem($this->tab,new SettingsItem($this->db,"smis-rs-acc-d-".$setname.""."-".$slug,"Debit  - <strong>".$nama."</strong>","","text","kode debit pasien ".$nama))
                            ->addItem($this->tab,new SettingsItem($this->db,"smis-rs-acc-k-".$setname.""."-".$slug,"Kredit - <strong>".$nama."</strong>","","text","kode kredit pasien ".$nama));
        }
        return $this;    
    }

    public function addItemAccountingOperasi(){
        $this->addSection("AKUNTING");
        $res                    = $this->getJenisPasien();
        $this->settings ->addItem($this->tab,new SettingsItem($this->db,"smis-rs-acc-e-operasi","Aktifkan Acruel Base ","0","checkbox","Aktifkan Auto Notif Acruel Base Ke Akunting"));
        $operasi                = array();
        $operasi['dp']          = "Dokter Pendamping";
        $operasi['op1']         = "Operator I";
        $operasi['op2']         = "Operator II";
        $operasi['aop1']        = "Asisten Operator I";
        $operasi['aop2']        = "Asisten Operator II";
        $operasi['ana']         = "Anastesi";
        $operasi['as-ana1']     = "Asisten Anastesi I";
        $operasi['as-ana2']     = "Asisten Anastesi II";
        $operasi['team']        = "Team OK";
        $operasi['bidan']       = "Bidan I";
        $operasi['bidan2']      = "Bidan II";
        $operasi['perawat']     = "Perawat";
        $operasi['sewa']        = "Sewa Kamar";
        $operasi['alat']        = "Sewa Alat";
        $operasi['oomloop1']    = "Oomloop I";
        $operasi['oomloop2']    = "Oomloop II";
        $operasi['instrument1'] = "Instrument I";
        $operasi['instrument2'] = "Instrument II";
        $operasi['rr']          = "Recovery Room";
        foreach($res as $x){
            $slug               = $x['value'];
            $nm                 = $x['name'];
            foreach($operasi as $cdx=>$nm){
                $this->settings ->addItem($this->tab,new SettingsItem($this->db,"smis-rs-acc-d-ok-".$cdx.""."-".$slug,"Kode Debet Operasi - ".$nm,"","text","Kode Debit Operasi - ".$nm))
                                ->addItem($this->tab,new SettingsItem($this->db,"smis-rs-acc-k-ok-".$cdx.""."-".$slug,"Kode Kredit Operasi - ".$nm,"","text","Kode Kredit Operasi - ".$nm));
            }
        }
        return $this;    
    }
    
	public function initialize() {	
		loadLibrary("smis-libs-function-medical");
		if($this->isGroup("general","General Ruangan","fa fa-university")){
            $opsi_kelas     = KelasResource::getKelasOption();
            $update_uri     = new OptionBuilder();
            $update_uri     ->add("","-1","1")
                            ->add("URJ","0","0")
                            ->add("URI","1","0");          
            $mode           = new OptionBuilder();
            $mode           ->addSingle("Stand Alone")
                            ->addSingle("Integrated");            
            $valset         = getSettings($this->db,"smis-rs-urjip","URJ");		
            $option         = new OptionBuilder ();
            $option	        ->add("Unit Rawat Jalan","URJ",$valset=="URJ"?"1":"0")
                            ->add("Unit Rawat Inap","URI",$valset=="URI"?"1":"0")
                            ->add("Unit Rawat Jalan / Inap","URJI",$valset=="URJI"?"1":"0")
                            ->add("Unit Penunjang","UP",$valset=="UP"?"1":"0");
            $option_kelas   = KelasResource::getKelasOptionDefault($this->kelas);
            $this	        ->addItem("smis-rs-kelas","Kelas ",$option_kelas->getContent (),"select","Kelas Ruangan Ini apakah Rawat Jalan,Kelas I,dll ")
                            //->addItem("smis-rs-kelas-aplicare","Kelas Aplicare ",$option_kelas->getContent (),"select","Kelas Aplicare Ruangan Ini apakah Rawat Jalan,Kelas I,dll ")
                            ->addItem("smis-rs-show-kelas-asal","Tampilkan Kelas Asal","0","checkbox","Tampilkan Kelas Asal")
                            ->addItem("smis-rs-urjip","Jenis Ruangan",$option->getContent (),"select","Jenis Ruangan,apakah Rawat Jalan (URJ),Rawat Inap (URI),ataukah Unit Penunjang (UP)")
                            ->addItem("smis-rs-update-urji","Mengubah Status URI/URJ ketika masuk kesini","0","checkbox","Mengubah Status URI/URJ ketika masuk ke ruang tertentu")
                            ->addItem("smis-rs-update-urji-set","Ketika Mengubah Status diubah ke URI atau URJ",$update_uri->getContent(),"select","Mengubah Status URI/URJ ketika masuk ke ruang tertentu")
                            ->addItem("smis-rs-skb","Tampilkan SKB","0","checkbox","Mencetak Surat Keterangan Berobat")
                            ->addItem("smis-rs-antrian-mode","Mode Antrian",$mode->getContent (),"select","Mode Integrasi ataukah Stand Alone")
                            ->addItem("smis-rs-slug-grup-pv","Nama Grup Ruangan untuk Provit Share","","text","nama grup ruangan ini pada pembagian hasil atau provit share (irna_a,irna_b,irna_b1,irna_c,irna_e,irna_f ,irna_g,paviliun)")
                            ->addItem("smis-rs-slug-grup-kelas-pv","Nama Grup Kelas untuk Provit Share","","text","nama kelas ruangan ini pada pembagian hasil atau provit share (non_kelas,kelas_hcu,kelas_nicu,kelas_i,kelas_ii,kelas_iii,kelas_vip,kelas_vvip,kelas_deluxe)")
                            ->addItem("smis-rs-titipan","Mengaktifkan Pasien Titipan","0","checkbox","Mengaktifkan Pasien Titipan")
                            ->addItem("smis-rs-profile","Mengaktifkan No. Profile","0","checkbox","Mengaktifkan NO. Profile Pasien")
                            ->addItem("smis-rs-provit-share-service","Mengaktifkan Setup Bagi Provit Share Lewat Service","0","checkbox","Jika di pilih provit share akan diaktifkan lewat service get_pv di manajemen")
                            ->addItem("smis-rs-show-exit-button","Mengaktifkan Tombol Keluar Pada Antrian","1","checkbox","Jika dicentang maka akan muncul tombol keluar di menu antrian")
                            ->addItem("smis-rs-layanan-load-once","Mengaktifkan Load Sekali untuk Tiap Page di Layanan","1","checkbox","Jika diaktifkan akan di Load sekali saja menu-menu yang ada di layanan")
                            ->addItem("smis-rs-dpjp-mode","Mode Dokter DPJP","0","checkbox","Model Data Dokter DPJP jika dicentang akan menjadi multiple,jika tidak berarti single")
                            ->addItem("smis-rs-exit-mode","Keluar dari Menu ini Keluar dari Rumah Sakit","0","checkbox","Jika menu ini diaktifkan maka keluar dari ruang ini berarti keluar juga dari ruamh sakit")
                            ->addItem("smis-rs-operation-room-status","Tampilkan Posisi Ruangan Operasi","0","checkbox","jika diaktifkan maka akan menampilkan posisi dan ruangan operasi dari pasien yang bersangkutan - khusus VK atau OK")
                            ->addItem("smis-rs-tutup-tagihan","Aktifkan Tutup Tagihan","0","checkbox","Aktifkan Tutup Tagihan")
                            ->addItem("smis-rs-tampil-tarif-operasi","Menampilkan Tarif Operasi","0","checkbox","Menampilkan Tarif Operasi");
        }
		
		/**GIZI */
        if($this->isGroup("gizi","Gizi","fa fa-spoon")){
            $this	->addItem("smis-rs-gizi-makanan","Menu Makanan Gizi","0","checkbox","Aktifkan Menu Makanan Gizi")
                    ->addItem("smis-rs-use-gizi","Daftarkan Setiap Pasienya ke Gizi","0","checkbox","Setiap Pasien yang ada di Poli ini,akan didaftarkan ke Gizi untuk dapat jatah makan")
                    ->addItem("smis-rs-use-gizi-simple","Tampilan Edit Gizi Sederhana","0","checkbox","Tampilan Gizi Cukup Satu Saja")
                    ->addItem("smis-rs-use-gizi-non-chooser","Tampilan Pilihan Menu Tanpa Chooser","0","checkbox","Tampilan Pilihan Menu Tanpa Chooser");
            $this   ->getExtention("get_settings_rawat_gizi","Extension Gizi");
        }
        
        /**TINDAKAN PERAWAT */
        if($this->isGroup("tindakan_perawat","Tindakan Perawat","fa fa-lightbulb-o")){   
            $optsirajal = new OptionBuilder();
            $optsirajal ->add("Rawat Jalan",0,1);
            $optsirajal ->add("Rawat Inap",1);
                     
            $opsi_kelas = KelasResource::getKelasOption();
            $perawat    = EmployeeResource::getJumlahPerawat();
            $this	    ->addItem("smis-rs-tperawat","Aktifkan","0","checkbox","Aktifkan Menu Tindakan Perawat")
                        ->addItem("smis-rs-name-tindakan_perawat","Nama Tab")
                        ->addItem("smis-rs-kelas-tperawat","Tindakan Perawat",$opsi_kelas->getContent(),"select","Kelas  Tindakan Perawat Ruangan ini")                             
                        ->addItem("smis-rs-tarif-tperawat","Tarif Tindakan Perawat",$optsirajal ->getContent(),"select","tarif tindakan perawat ranap atay rajal (default rajal)")
                        ->addItem("smis-rs-tindakan-perawat-jumlah","Tampilkan Jumlah Tindakan","0","checkbox","Tampilkan Jumlah Tidnakan atau Tidak")
                        ->addItem("smis-rs-triple-tindakan-perawat","Jumlah Perawat di Tindakan Perawat",$perawat->getContent(),"select","Jumlah Tampilan Perawat pada Tindakan Perawat")
                        ->addItem("smis-rs-employee-tindakan-perawat","Filter Perawat Ruangan","0","checkbox","jika di centang hanya perawat ruangan ini yang tampil saat memilih perawat ")
                        ->addItem("smis-rs-employee-tindakan-perawat-jabatan","Filter Jabatan Untuk Pegawai","","text","Pencarian Pegawai akan difilter yang jabatanya Berdasarkan String ini ")
                        ->addItemAccounting("tindakan-perawat");
            
        }
        /**TINDAKAN IGD */
        if($this->isGroup("tindakan_igd","Tindakan Perawat IGD"," fa fa-warning")){            
            $opsi_kelas     = KelasResource::getKelasOption();
            $perawat        = EmployeeResource::getJumlahPerawat();
            $this       	->addItem("smis-rs-tperawat-igd","Aktifkan","0","checkbox","Aktifkan Menu Tindakan IGD")
                            ->addItem("smis-rs-name-tindakan_perawat_igd","Nama Tab")
                            ->addItem("smis-rs-kelas-tperawat-igd","Kelas",$opsi_kelas->getContent(),"select","Kelas  Tindakan Perawat IGD Pasien")          
                            ->addItem("smis-rs-total-tindakan-igd","Jumlah Perawat",$perawat->getContent(),"select","Jumlah Perawat di Tindakan IGD")
                            ->addItem("smis-rs-icd-tindakan-igd","Tampilkan ICD","0","checkbox","Tampilkan ICD pada Tindakan IGD")
                            ->addItem("smis-rs-employee-tindakan-igd","Filter Perawat","0","checkbox","jika di centang hanya perawat ruangan ini yang tampil saat memilih perawat ")
                            ->addItem("smis-rs-dokter-tindakan-igd","Filter Dokter","0","checkbox","jika di centang hanya dokter ruangan ini yang tampil saat memilih dokter")
                            ->addItemAccounting("tindakan-igd");
        }
        /**TINDAKAN DOKTER */
        if($this->isGroup("tindakan_dokter","Tindakan Dokter"," fa fa-user-md")){       
            $optsirajal = new OptionBuilder();
            $optsirajal ->add("Rawat Jalan",0,1);
            $optsirajal ->add("Rawat Inap",1);
            
            $opsi_kelas = KelasResource::getKelasOption();
            $perawat    = EmployeeResource::getJumlahPerawat();
            $this       ->addItem("smis-rs-tdokter","Aktifkan",0,"checkbox","Mengaktifkan menu Tindakan Dokter")
                        ->addItem("smis-rs-name-tindakan_dokter","Nama Tab")
                        ->addItem("smis-rs-kelas-tdokter","Kelas",$opsi_kelas->getContent(),"select","Filter Kelas Tindakan Dokter Ruangan ini saat memilih daftar tindakan dokter yang sudah di setup di modul manajer")
                        ->addItem("smis-rs-tdokter-jp","Filter Jenis Pasien","0","checkbox","jika ini dicentang,maka pada saat melakukan pemilihan tindakan dokter akan difilter berdasarkan carabayar pasien")
                        ->addItem("smis-rs-total-perawat-tindakan-dokter","Jumlah Perawat",$perawat->getContent(),"select","Jumlah Perawat di Tindakan Dokter")
                        ->addItem("smis-rs-ui-harga-perawat-tindakan-dokter","Menampilkan Jasa Perawat","0","checkbox","Jika dicentang maka Harga Jasa Perawat akan ditampilkan")
                        ->addItem("smis-rs-employee-tindakan-dokter","Filter Perawat","0","checkbox","jika di centang hanya perawat ruangan ini yang tampil saat memilih perawat ")
                        ->addItem("smis-rs-dokter-tindakan-dokter","Filter Dokter","0","checkbox","jika di centang hanya dokter ruangan ini yang tampil saat memilih dokter")
                        ->addItem("smis-rs-tarif-tindakan-dokter","Tarif Tindakan Dokter",$optsirajal ->getContent(),"select","tarif tindakan dokter ranap atay rajal (default rajal)")
                        ->addItemAccounting("tindakan-dokter")
                        ->addItemAccounting("perawat-tindakan-dokter",false);
        }
        /**KONSUL DOKTER */
        if($this->isGroup("konsul","Konsul Dokter","fa fa-recycle")){            
            $opsi_kelas     = KelasResource::getKelasOption();
            $this       	->addItem("smis-rs-konsuldokter","Aktifkan","0","checkbox","Apakah Ruangan ini butuh Konsul Dokter")
                            ->addItem("smis-rs-name-konsul_dokter","Nama Tab")
                            ->addItem("smis-rs-kelas-konsuldokter","Kelas",$opsi_kelas->getContent(),"select","Kelas  Konsul Dokter Ruangan ini")
                            ->addItem("smis-rs-konsul-mode-tarif","Memisahkan Tarif dengan Dokternya","0","checkbox","jika di centang maka tarif dokter tidak terikat dengan dokternya")
                            ->addItem("smis-rs-dokter-konsul","Filter Dokter","0","checkbox","jika di centang hanya dokter ruangan ini yang tampil saat memilih dokter")
                            ->addItemAccounting("konsul-dokter");
        }
        /**VISITE DOKTER */
        if($this->isGroup("visite","Visite Dokter","fa fa-tint")){            
            $opsi_kelas     = KelasResource::getKelasOption();
            $dokter         = EmployeeResource::getJumlahDokter();
            $this       	->addItem("smis-rs-visite","Aktifkan","0","checkbox","Mengaktifkan Menu Visite Dokter")
                            ->addItem("smis-rs-name-visite_dokter","Nama Tab")
                            ->addItem("smis-rs-kelas-visite","Visite Dokter",$opsi_kelas->getContent(),"select","Kelas  Visite DOkter Ruangan ini Sesuai")
                            ->addItem("smis-rs-dokter-visite","Filter Dokter ","0","checkbox","jika di centang hanya dokter ruangan ini yang tampil saat memilih dokter")
                            ->addItem("smis-rs-total-visite-dokter","Jumlah Dokter",$dokter->getContent(),"select","Jumlah Dokter di Visite Dokter")
                            ->addItem("smis-rs-visite-mode-tarif","Memisahkan Tarif dengan Dokternya","0","checkbox","jika dicentang maka tarif dokter tidak terikat dengan dokternya")
                            ->addItemAccounting("visite-dokter");   
        }
        /**PERIKSA DOKTER */
        if($this->isGroup("periksa","Periksa Dokter","fa fa-medkit")){            
            $opsi_kelas     = KelasResource::getKelasOption();
            $perawat        = EmployeeResource::getJumlahPerawat();
            $this       	->addItem("smis-rs-konsultasidokter","Aktifkan","0","checkbox","Mengaktifkan Menu Periksa Dokter (Khusus Poli) ")
                            ->addItem("smis-rs-name-konsultasi_dokter","Nama Tab")
                            ->addItem("smis-rs-dokter-periksa","Filter Dokter","0","checkbox","jika dicentang maka hanya dokter ruangan ini yang tampil saat memilih dokter")
                            ->addItem("smis-rs-perawat-periksa","Filter Perawat","0","checkbox","jika dicentang maka hanya perawat ruangan ini yang tampil saat memilih perawat")
                            ->addItem("smis-rs-total-periksa-dokter","Jumlah Perawat",$perawat->getContent(),"select","Jumlah Perawat di Periksa Dokter")
                            ->addItem("smis-rs-ui-harga-perawat-periksa-dokter","Menampilkan Jasa Perawat","0","checkbox","Jika dicentang maka Harga Jasa Perawat akan ditampilkan")
                            ->addItemAccounting("periksa-dokter");
        }
        /**OPERASI */
        if($this->isGroup("operasi","Operasi"," fa fa-cut")){
            require_once 'rawat/class/resource/KelasResource.php';            
            require_once 'rawat/class/resource/OperasiResource.php';            
            $opsi_kelas     = KelasResource::getKelasOption();
            $okadapter      = OperasiResource::getAdapterTagihan();
            $this	        ->addItem("smis-rs-ok","Aktifkan","0","checkbox","Mengaktifkan Menu Operasi")
                            ->addItem("smis-rs-name-ok","Nama Tab")
                            ->addItem("smis-rs-name-ok-cyto","Nilai Kenaikan Cyto")
                            ->addItem("smis-rs-name-ok-local","Nilai Persen Tindakan 2 pada Pembiusan Local")
                            ->addItem("smis-rs-name-ok-regional","Nilai Persen Tindakan 2 pada Pembiusan Regional / General")
                            
                            ->addItem("smis-rs-kelas-operatie","Kelas",$opsi_kelas->getContent(),"select","Kelas Operasi Ruangan ini")                            
                            ->addItem("smis-rs-acc-ok-detail","Model Service Tagihan Operasi",$okadapter->getContent(),"select","Model Service Tagihan OK yang ingin ditampilkan")
                            ->addItem("smis-rs-operatie-icdtindakan","Tampilkan Input ICD Tindakan","0","checkbox","tampilkan input icd tindakan operasi")        
                            ->addItem("smis-rs-operatie-jp","Filter Tarif mengikuti Carabayar","0","checkbox","jika diaktifkan maka tarif operasi akan difilter sesuai dengan carabayar pasienya")                            
                            ->addSection("FILTER KARYAWAN")
                            ->addItem("smis-rs-operatie-filter-ok_dokter_pendamping","Filter Untuk Dokter Pendamping","","text","Pencarian Pegawai Dokter Pendamping akan difilter yang jabatanya Berdasarkan String ini ")
                            ->addItem("smis-rs-operatie-filter-ok_operator_satu","Filter Untuk Operator I","","text","Pencarian Pegawai Operator I akan difilter yang jabatanya Berdasarkan String ini ")
                            ->addItem("smis-rs-operatie-filter-ok_operator_dua","Filter Untuk Operator II","","text","Pencarian Pegawai Operator II akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_asisten_operator_satu","Filter Untuk Asisten Operator I","","text","Pencarian Pegawai Asisten Operator I akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_asisten_operator_dua","Filter Untuk Asisten Operator II","","text","Pencarian Pegawai Asisten Operator II akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_anastesi","Filter Untuk Dokter Anastesi","","text","Pencarian Pegawai Dokter Anastesi akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_asisten_anastesi","Filter Untuk Asisten Anastesi I","","text","Pencarian Pegawai Asisten Anastesi I akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_asisten_anastesi_dua","Filter Untuk Asisten Anastesi II","","text","Pencarian Pegawai Asisten Anastesi II akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_instrument","Filter Untuk Instrument I","","text","Pencarian Pegawai Instrument I akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_instrument_dua","Filter Untuk Instrument II","","text","Pencarian Pegawai Instrument II akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_oomloop_satu","Filter Untuk Oomloop I","","text","Pencarian Pegawai Oomloop I akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_oomloop_dua","Filter Untuk Oomloop II","","text","Pencarian Pegawai Oomloop II akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_team_ok","Filter Untuk Team OK","","text","Pencarian Pegawai Team OK akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_bidan","Filter Untuk Bidan I","","text","Pencarian Pegawai Bidan I akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_bidan_dua","Filter Untuk Bidan II","","text","Pencarian Pegawai Bidan II akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addItem("smis-rs-operatie-filter-ok_perawat","Filter Untuk Perawat","","text","Pencarian Pegawai Perawat akan difilter yang jabatanya  Berdasarkan String ini  ")
                            ->addSection("USER INTERFACE")
                            ->addItem("smis-rs-operatie-ui-diagnosa","UI Diagnosa",OperasiResource::getUIOperasiDiagnosa(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_tindakan","UI Tindakan 1",OperasiResource::getUIOperasiTindakan(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_tindakan_dua","UI Tindakan 2",OperasiResource::getUIOperasiTindakanDua(),"select","")
                            
                            ->addItem("smis-rs-operatie-ui-harga_tindakan_satu","UI Harga Tindakan 1",OperasiResource::getUIHargaTindakan1(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_tindakan_dua","UI Harga Tindakan 2",OperasiResource::getUIHargaTindakan2(),"select","")
                            

                            ->addItem("smis-rs-operatie-ui-nama_sewa_kamar","UI Sewa Kamar ",OperasiResource::getUIOperasiSewaKamar(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_sewa_kamar","UI Harga Sewa Kamar",OperasiResource::getUIOperasiHargaSewaKamar(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_sewa_alat","UI Harga Sewa Alat",OperasiResource::getUIOperasiHargaSewaAlat(),"select","")
                            ->addItem("smis-rs-operatie-ui-kelas","UI Kelas",OperasiResource::getUIOperasiKelas(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perujuk","UI Nama Perujuk",OperasiResource::getUIOperasiPerujuk(),"select","")
                            ->addItem("smis-rs-operatie-ui-kode_perujuk","UI Kode Perujuk",OperasiResource::getUIOperasiKodePerujuk(),"select","")                            
                            ->addItem("smis-rs-operatie-ui-nama_dokter_pendamping","UI Dokter Pendamping",OperasiResource::getUIOperasiDokterPendamping(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_dokter_pendamping","UI Harga Dokter Pendamping",OperasiResource::getUIOperasiBiayaDokterPendamping(),"select","")                            
                            ->addItem("smis-rs-operatie-ui-nama_operator_satu","UI Nama Operator I",OperasiResource::getUIOperasiOperator1(),"select","")
                            ->addItem("smis-rs-operatie-ui-jenis_operator_satu","UI Jenis Operator I",OperasiResource::getUIOperasiJenisOperator1(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_operator_satu","UI Harga Operator I",OperasiResource::getUIOperasiBiayaOperator1(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_operator_dua","UI Nama Operator II",OperasiResource::getUIOperasiOperator2(),"select","")
                            ->addItem("smis-rs-operatie-ui-jenis_operator_dua","UI Jenis Operator II",OperasiResource::getUIOperasiJenisOperator2(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_operator_dua","UI Harga Operator II",OperasiResource::getUIOperasiBiayaOperator2(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_asisten_operator_satu","UI Nama Asisten Operator I",OperasiResource::getUIOperasiAsistenOperator1(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_asisten_operator_satu","UI Harga Asisten Operator I",OperasiResource::getUIOperasiBiayaAsistenOperator1(),"select","")						
                            ->addItem("smis-rs-operatie-ui-nama_asisten_operator_dua","UI Nama Asisten Operator II",OperasiResource::getUIOperasiAsistenOperator2(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_asisten_operator_dua","UI Harga Asisten Operator II",OperasiResource::getUIOperasiBiayaAsistenOperator2(),"select","")
                            ->addItem("smis-rs-operatie-ui-anastesi_hadir","UI Asisten Hadir",OperasiResource::getUIOperasiKehadiranAnastesi(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_anastesi","UI Nama Anastesi",OperasiResource::getUIOperasiAnastesi(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_anastesi","UI Harga Anastesi",OperasiResource::getUIOperasiBiayaAnastesi(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_asisten_anastesi","UI Nama Asisten Anastesi I",OperasiResource::getUIOperasiAsistenAnastesi1(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_asisten_anastesi","UI Harga Asisten Anastesi I",OperasiResource::getUIOperasiBiayaAsistenAnastesi1(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_asisten_anastesi_dua","UI Nama Asisten Anastesi II",OperasiResource::getUIOperasiAsistenAnastesi2(),"select","")						
                            ->addItem("smis-rs-operatie-ui-harga_asisten_anastesi_dua","UI Harga Asisten Anastesi II",OperasiResource::getUIOperasiBiayaAsistenAnastesi2(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_instrument","UI Nama Instrument I",OperasiResource::getUIOperasiInstrument1() ,"select","")
                            ->addItem("smis-rs-operatie-ui-harga_instrument","UI Harga Instrument I",OperasiResource::getUIOperasiBiayaInstrument1(),"select","")					
                            ->addItem("smis-rs-operatie-ui-nama_instrument_dua","UI Nama Instrument II",OperasiResource::getUIOperasiInstrument2(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_instrument_dua","UI Harga Instrument I",OperasiResource::getUIOperasiBiayaInstrument2(),"select","")				
                            ->addItem("smis-rs-operatie-ui-nama_oomloop_satu","Nama Oomloop I",OperasiResource::getUIOperasiOomloop1(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_oomloop_satu","UI Harga Oomloop I",OperasiResource::getUIOperasiBiayaOomloop1(),"select","")						
                            ->addItem("smis-rs-operatie-ui-nama_oomloop_dua","UI Nama Oomloop II",OperasiResource::getUIOperasiOomloop2(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_oomloop_dua","UI Harga Oomloop II",OperasiResource::getUIOperasiBiayaOomloop2(),"select","")					
                            ->addItem("smis-rs-operatie-ui-nama_bidan","UI Nama Bidan I",OperasiResource::getUIOperasiBidan1(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_bidan","UI Harga Bidan I",OperasiResource::getUIOperasiBiayaBidan1(),"select","")						
                            ->addItem("smis-rs-operatie-ui-nama_bidan_dua","UI Nama Bidan II",OperasiResource::getUIOperasiBidan2(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_bidan_dua","UI Harga Bidan II",OperasiResource::getUIOperasiBiayaBidan2(),"select","")					
                            ->addItem("smis-rs-operatie-ui-nama_team_ok","UI Team OK",OperasiResource::getUIOperasiTeamOK(),"select","")
                            ->addItem("smis-rs-operatie-ui-harga_team_ok","UI Harga Team OK",OperasiResource::getUIOperasiBiayaTeamOK(),"select","")
                            ->addItem("smis-rs-operatie-ui-recovery_room","UI Harga Recovery Room",OperasiResource::getUIOperasiBiayaRR(),"select","")                            
                            ->addItem("smis-rs-operatie-ui-harga_perawat","UI Harga Perawat",OperasiResource::getUIOperasiBiayaPerawat(),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat","UI Nama Perawat I",OperasiResource::getUIOperasiPerawat(),"select","")                            
                            ->addItem("smis-rs-operatie-ui-nama_perawat_dua","UI Nama Perawat II",OperasiResource::getUIOperasiPerawat("_dua","II"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_tiga","UI Nama Perawat III",OperasiResource::getUIOperasiPerawat("_tiga","III"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_empat","UI Nama Perawat IV",OperasiResource::getUIOperasiPerawat("_empat","IV"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_lima","UI Nama Perawat V",OperasiResource::getUIOperasiPerawat("_lima","V"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_enam","UI Nama Perawat VI",OperasiResource::getUIOperasiPerawat("_enam","VI"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_tujuh","UI Nama Perawat VII",OperasiResource::getUIOperasiPerawat("_tujuh","VII"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_delapan","UI Nama Perawat VIII",OperasiResource::getUIOperasiPerawat("_delapan","VIII"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_sembilan","UI Nama Perawat IX",OperasiResource::getUIOperasiPerawat("_sembilan","IX"),"select","")
                            ->addItem("smis-rs-operatie-ui-nama_perawat_sepuluh","UI Nama Perawat X",OperasiResource::getUIOperasiPerawat("_sembilan","X"),"select","")
                            ->addItem("smis-rs-operatie-ui-keterangan","UI Keterangan",OperasiResource::getUIOperasiKeterangan(),"select","")
                            ->addItemAccountingOperasi();    
        }
        /**BED */
        if($this->isGroup("bed","Bed"," fa fa-bed")){
            $bed    = new OptionBuilder();
            $bed    ->add("Perhitungan Per Hari","hari")
                    ->add("Perhitungan per Jam","jam")
                    ->add("Perhitungan per Jam Tertentu","per jam")
                    ->add("Perhitungan per Jam Tertentu + Lebih Jam","per jam+");            
            $this	->addItem("smis-rs-bed","Aktifkan","0","checkbox","Mengaktifkan Penggunaan Bed (Khusus Rawat Inap)")
                    ->addItem("smis-rs-name-bed","Nama Tab untuk Bed")
                    ->addItem("smis-rs-free-bed","Bebaskan Biaya Bed","0","checkbox","Membebaskan Biaya Bed")
                    ->addItem("smis-rs-bed-counter","Mode Perhitungan Kamar",$bed->getContent(),"select","Mengubah Mode Perhitungan Bed di tiap Ruang")
                    ->addItem("smis-rs-bed-counter-jam","Satuan Perhitungan Untuk Tiap (x) Jam","6","text","Satu Hari dihitung minimal berapa jam")
                    ->addItem("smis-rs-bed-counter-cut-off","Pemotongan Setiap Jam Berapa","","text","Waktu Pemotongan jam dihitung berdasarkan jam berapa")
                    ->addItem("smis-rs-kelas-bed","Tarif Bed","0","checkbox","jika di centang berarti memilih sendiri,jika tidak berarti mengikuti kelas ruangan dengan setting di menu manajer ")
                    ->addItem("smis-rs-bed-price-choice","Tarif Bed Multiple Choice","","textarea",'jika di isi maka akan ada pilihan harga bagi pasien </br>Diamond : {"Khusus":"375000"}</br>Emerald : {"Khusus":"350000"}</br>Ruby 	: {"Khusus":"225000"} ')
                    ->addItemAccounting("bed");
        }
		/**ALAT DAN OBAT MEDIS */
        if($this->isGroup("alat_obat","Alat Obat","fa fa-wrench")){
            $this	->addItem("smis-rs-alok","Aktifkan","0","checkbox","Mengaktifkan Penggunaan Stok dan Alat Obat")
                    ->addItem("smis-rs-name-bed","Nama Tab untuk Bed")
                    ->addItem("smis-rs-connect-stok-obat","Koneksikan Stok Obat dengan BOI","0","checkbox","Koneksikan Stok Obat dengan BOI")
                    ->addItem("smis-rs-connect-barcode-scan","Aktifkan Barcode Scan Pada Penggunaan Obat","0","checkbox","Koneksikan Barcode Scan pada Penggunaan Obat di Layanan Pasien")
                    ->addItem("smis-rs-stok-obat-boi","Gunakan BOI untuk ruangan ini ",$this->polislug,"text","Menggunakan BOI pada Ruangan")
                    ->addItem("smis-rs-pengali-aok","Faktor pengali Harga Obat Rungan ini","","text","Pengali Harga Obat")
                    ->addItem("smis-rs-aok-gudang-non-edit","Disable Editing ALat dan Obat di Gudang","","checkbox","Alat Obat yang berasal dari gudang tidak dapat diubah")
                    ->addItem("smis-rs-hidden-harga-alok","Tampilkan Harga Alok di Ruangan","1","checkbox","Tampilkan Harga Alat dan Obat di Ruangan")
                    ->addItem("smis-is-inventory-barang","Inventory Barang","0","checkbox","Penggunaan Barang")
                    ->addItem("smis-is-inventory-obat","Inventory Obat","0","checkbox","Penggunaan Obat")
                    ->addItemAccounting("alok")
                    ->addItemAccounting("alok-hpp",false);
        }

        /**OKSIGEN CENTRAL */
        if($this->isGroup("oksigen","Oksigen","fa fa-fire")){
            $this	->addSection("OKSIGEN CENTRAL")
                    ->addItem("smis-rs-oksigen-central","Oksigen Central","0","checkbox","Apakah Ruangan ini butuh Oksigen Central")
                    ->addItem("smis-rs-name-oksigen_central","Nama Tab Oksigen Central")
                    ->addItem("smis-rs-oksigen-manual-skala","Maksimum Skala Oksigen Central","10","text","Nilai Maksimum Skala Oksigen Central - default 10")
                    ->addItemAccounting("oksigen-central")
                    ->addSection("OKSIGEN MANUAL")
                    ->addItem("smis-rs-oksigen-manual","Oksigen Manual","0","checkbox","Apakah Ruangan ini butuh Oksigen Manual")
                    ->addItem("smis-rs-name-oksigen_manual","Nama Tab Oksigen Manual")
                    ->addItem("smis-rs-oksigen-report","Grup Laporan Oksigen",ArrayAdapter::slugFormat("unslug",$this->polislug),"text","Nama Kelompok Grup Laporan Oksigen")
                    ->addItem("smis-rs-tarif-oksigen_manual","Aktifkan Pilihan tarif Untuk Oksigen Manual","0","checkbox","Tarif dari Oksigen Manual")                    
                    ->addItemAccounting("oksigen-manual");
        }

        /** REGISTRASI KE RUANG LAIN */
        if($this->isGroup("register","Registrasi"," fa fa-map-o")){
            require_once 'rawat/class/resource/KelasResource.php';            
            $opsi_kelas = KelasResource::getKelasOption();
            $this	    ->addItem("smis-rs-rujukan","Aktifkan","0","checkbox","Apakah Ruangan bisa Mendaftarkan ke Ruang Lain ")
                        ->addItem("smis-rs-kelas-rujukan","Kelas Rujukan",$opsi_kelas->getContent(),"select","Kelas Rujukan Ruangan Ini")
                        ->addItem("smis-rs-register-date","Non Aktifkan Pilihan Tanggal","0","checkbox","Non Aktifkan Pilihan Tanggal di pada Register Pasien")
                        //->addItem("smis-rs-register-df-cara-keluar","Cara Keluar Default Pasien",medical_carapulang(),"select","Jika Keluar Otomatis dari ruang melalui Rujukan maka Default Cara Keluarnya adalah")
                        ->addItem("smis-rs-register-show-df-save","Menampilkan Default Tombol Register pada Modal",1,"checkbox","Jika di aktifkan maka Modal pada Rujukan pasien akan menampilkan tombol <a href='#' class='btn btn-primary'> <i class='fa fa-save'></i></a>");
            $serv       = new ServiceProviderList($this->db,"push_antrian");
            $serv       ->execute();
            $ruangan    = $serv->getContent();
            foreach($ruangan as $option){
                $this->settings	->addItem("register",new SettingsItem($this->db,"smis-rs-register-".$this->polislug."-".$option['value'],$option['name'],"1","checkbox","Registrasi ruang ini akan muncul di Menu Register Pasien"));
            }
        }
        
        /**BPJS */
        if($this->isGroup("bpjs","BPJS","fa fa-money")){
            $this	->addItem("smis-rs-bpjs-plan","Rencana Asuhan BPJS","0","checkbox","Menampilkan Rencana Asuhan BPJS")
                    ->addItem("smis-rs-bpjs-check","Aktifkan BPJS Checker","0","checkbox","Mengaktifkan Fitur BPJS Checker")
                    ->addItem("smis-rs-bpjs-warning","Persentase Warning Plafon","100","text","Warning akan Muncul jika total tagihan sudah mencapai (x) Persen dari plafon")
                    ->addItem("smis-rs-activate-bpjs-stop","Aktifkan BPJS Stop Plafon","0","checkbox","Mengaktifkan Stop Plafon jika sudah mencapai persentase tertentu")
                    ->addItem("smis-rs-bpjs-stop","Persentase Stop Plafon","100","text","Stop menambah tagihan baru jika total tagihan sudah mencapai (x) Persen dari plafon")
                    ->addItem("smis-rs-bpjs-autoloa-","Autoload Update Checker Setiap Perubahan Tagihan","1","checkbox","jika diaktifkan maka akan melakukan autoload setiap kali ada ada transaksi tetapi akan membuat system lebih berat dari biasanya");
        }
        
        /**PRINT OUT */
		if($this->isGroup("printout","Print Out Ruangan"," fa fa-print")){
            $this   ->addItem("smis-rs-printout-table","Gunakan Biaya Table","0","checkbox","Biaya Table untuk Memformat Kesalahan Nama pada Tindakan Perawat IGD")
                    ->addItem("smis-rs-printout-waktu","Tampilkan Waktu","0","checkbox","Waktu Ditampilkan")
                    ->addItem("smis-rs-printout-layanan","Tampilkan Layanan","0","checkbox","Menampilkan Layanan")
                    ->addItem("smis-rs-printout-biaya","Tampilkan Biaya","0","checkbox","Menampilkan Biaya")
                    ->addItem("smis-rs-printout-satuan","Tampilkan Satuan","0","checkbox","Menampilkan Satuan")
                    ->addItem("smis-rs-printout-jumlah","Tampilkan Jumlah","0","checkbox","Menampilkan Jumlah")
                    ->addItem("smis-rs-printout-jumlah-alok","Tampilkan Jumlah Alat Obat","0","checkbox","Menampilkan Jumlah Alat Obat Sendiri")
                    ->addItem("smis-rs-printout-keterangan","Tampilkan Keterangan","0","checkbox","Menampilkan Keterangan")
                    ->addItem("smis-rs-printout-jenis","Tampilkan Jenis","0","checkbox","Menampilkan Jenis Layanan")
                    ->addItem("smis-rs-printout-susun","Tampilkan Jenis Bersusun","0","checkbox","Menampilkan Jenis Layanan Secara Bersusun")
                    ->addItem("smis-rs-printout-susun-jumlah","Tampilkan Penjumlahan Secara Bersusun","0","checkbox","Menampilkan Jumlah Jenis Layanan Secara Bersusun")
                    ->addItem("smis-rs-printout-alok-bed-oksigen","Jumlahkan Alat Obat Tanpa Bed dan Oksigen","0","checkbox","Bed dan Oksigen akan Ditampilkan Sendiri")
                    ->addItem("smis-rs-printout-space","Tampilkan Space Diantara Total","0","checkbox","Menampilkan Spasi Diantara Total dan Selanjutnya")
                    ->addItem("smis-rs-title-kepala","Tampilan Title Kepala Ruang","","text","Title Kepala")
                    ->addItem("smis-rs-nama-kepala","Tampilan Nama Kepala Ruang","","text","Nama Kepala")
                    ->addItem("smis-rs-printout-visite","Ganti Nama Visite Menjadi","Visite","text","Mengganti Nama Tulisan Visite")
                    ->addItem("smis-rs-printout-total","Tampilan Total","1","checkbox","Menampilkan Total")
                    ->addItem("smis-rs-font-title","Ukuran Font Title","","text","Ukuran Font Title")
                    ->addItem("smis-rs-font-header","Ukuran Font Header","","text","Ukuran Font Header")
                    ->addItem("smis-rs-font-body","Ukuran Font Body","","text","Ukuran Font Body")
                    ->addItem("smis-rs-font-footer","Ukuran Font Footer","","text","Ukuran Font Footer")
                    ->addItem("smis-rs-font-subtitle","Ukuran Font Sub Title","","text","Ukuran Font Sub Title")
                    ->addItem("smis-rs-numbertell","Tampilan Pembilang","0","checkbox","Tampilkan Pembilang");
        }
        /**Menu Rekam Medis */
        if($this->isGroup("rekam_medis","Rekam Medis"," fa fa-book")){
            $option_kelas_grup = KelasResource::getKelasOptionGroup($this->kelas);   
            $this	->addItem("smis-rs-dokumen","Aktifkan Traking Dokumen","0","checkbox","Mengaktifkan Tracking Dokumen Rekam Medis")
                    ->addItem("smis-rs-tampil-rujukan-antrian","Tampilkan Rujukan","0","checkbox","Jika Fitur ini diaktifkan saat memulangkan pasien akan muncul data rujukan kemana jika pulangnya di rujuk")
                    ->addItem("smis-rs-rl52","Menggunakan RL-5.2","0","checkbox","Apakah Termasuk Laporan RL - 5.2")
                    ->addItem("smis-rs-rl52-default","Default Isian RL 5.2",get_rl52_option(),"select","Nilai Default RL 5.2")
                    ->addItem("smis-rs-rl12","Masuk Laporan RL-1.2","0","checkbox","Masuk Laporan RL-1.2")
                    ->addItem("smis-rs-grup-ruangan","Grup Laporan MR Ruangan ini",$this->polislug,"text","Grup Laporan MR")
                    ->addItem("smis-rs-rl13-kelas","Kelompok Laporan RL-1.3","0","text","Kelompok Kelas RL-1.3")
                    ->addItem("smis-rs-rl13","Masuk Laporan RL-1.3","0","checkbox","Masuk Laporan RL-1.3")
                    ->addItem("smis-rs-kelas-grup-ruangan","Grup Kelas Laporan MR Ruangan ini",$option_kelas_grup->getContent(),"select","Grup Kelas Laporan MR")
                    ->addItem("smis-rs-rl13-layanan","Kelompok Layanan",get_rl13_option(),"select","Kelompok Layanan RL-1.3")
                    ->addItem("smis-rs-icd-mr","Laporan ICD Tindakan untuk Rekam Medis","8","text","Laporan ICD Tindakan RL : </br> 1. Operasi </br> 2. Tindakan Dokter </br> 4. Tindakan Perawat </br> 8.Tindakan Perawat IGD ");
            $this   ->addSection("Checking Keluar Pasien"); 
            $this   ->addItem("smis-rs-check-pendaftaran"," Crawler Pendaftaran","0","checkbox","System Akan Mengambil Data Pendaftaran dibutuhkan untuk Plebitis,Cauti dan Menampilkan Hak Kelas BPJS")
                    ->addItem("smis-rs-check-diagnosa"," Cek Diagnosa Sebelum Keluar","0","checkbox","Jika Data Dignosa Tidak Lengkap Maka Tidak Boleh Keluar")
                    ->addItem("smis-rs-show-cauti"," Tampilkan Ca-UTI","0","checkbox","Tampilkan Opsi Ca-UTI")
                    ->addItem("smis-rs-check-cauti"," Cek Ca-UTI Sebelum Keluar","0","checkbox","Jika Data Ca-Uti Tidak Lengkap Maka Tidak Boleh Keluar")
                    ->addItem("smis-rs-edit-cauti"," User Bisa Melakukan Edit Ca-UTI","0","checkbox","Jika di Edit maka ruang ini bisa mengubah data Ca-UTI")
                    ->addItem("smis-rs-show-plebitis"," Tampilkan Plebitis","0","checkbox","Tampilkan Opsi Plebitis")
                    ->addItem("smis-rs-check-plebitis"," Cek Plebitis Sebelum Keluar","0","checkbox","Jika Data PLebitis Tidak Lengkap Maka Tidak Boleh Keluar")
                    ->addItem("smis-rs-edit-plebitis"," User Bisa Melakukan Edit Plebitis","0","checkbox","Jika di Edit maka ruang ini bisa mengubah data Plebitis");
            $this   ->getExtention("get_settings_rawat_rm","Extension Medical Record");    
            $this   ->addItem("smis-rs-diagnosa-gizi"," Diagnosa Gizi","0","checkbox","Menampilkan Diagnosa Gizi");
            $this   ->addItem("smis-rs-laporan-diagnosa-gizi"," LaporanDiagnosa Gizi","0","checkbox","Laporan Diagnosa Gizi");      
        }

        if($this->isGroup("kasir","Kasir"," fa fa-money")){
            $this	->addItem("smis-rs-kasir-diskon","Pengajuan Diskon","0","checkbox","Aktifkan Menu Untuk Mengajukan Diskon")
                    ->addItem("smis-rs-kasir-surat-sakit","Surat Sakit","0","checkbox","Aktifkan Menu Untuk Mengajukan Surat Sakit")
                    ->addItem("smis-rs-delay-js-crawler","Delay Crawler Total Tagihan","100","text","Nilai Delay Crawler Total Tagihan (default 100 milisecond),semakin tinggi semakin aman tetapi semakin lambat ")
                    ->addItem("smis-rs-socket-timeout-crawler","Maksimum Time Out Socket Total Tagihan","60","text","Maksimum Waktu Tunggu Koneksi Socket Total Tagihan (default 60 second),semakin tinggi semakin aman tetapi semakin lambat ")
                    ->addItem("smis-rs-skipper-crawler","Maksimum Percobaan Ulang Crawler ketika Gagal","100","text","Berapa Kali percobaan Crawler sebelum akhirnya di Automatic Skip")
                    ->addItem("smis-rs-tagihan-rumus","Mengaktifkan Tampilan Tagihan Rumus","0","checkbox","Mengaktifkan Tagihan Rumus di Ruangan ini");
        }
		
        if($this->isGroup("extension","External Modul"," fa fa-book")){
            $opsi_kelas     = KelasResource::getKelasOption();

            $posisi = new OptionBuilder();
            $posisi ->add("di Transfer Pasien","0","1");
            $posisi ->add("di Pelayanan Pasien","1","0");

            $this	->addSection("Laboratory")
                    ->addItem("smis-rs-laboratory","Laboratory","0","checkbox","Ruangan Untuk Mendaftarkan ke Laboratory")
                    ->addItem("smis-rs-kelas-laboratory","Kelas Laboratory",$opsi_kelas->getContent(),"select","Kelas Ruangan Untuk Mendaftarkan ke Laboratory")
                    ->addSection("Radiology")
                    ->addItem("smis-rs-radiology","Radiology","0","checkbox","Ruangan Untuk Mendaftarkan ke Radiology")
                    ->addItem("smis-rs-kelas-radiology","Kelas Radiology",$opsi_kelas->getContent(),"select","Kelas Ruangan Untuk Mendaftarkan ke Radiology")
                    ->addSection("Fisiotherapy")
                    ->addItem("smis-rs-fisiotherapy","Fisiotherapy","0","checkbox","Apakah Ruangan bisa Mendaftarkan ke Fisiotherapy")
                    ->addItem("smis-rs-kelas-fisiotherapy","Kelas Fisiotherapy",$opsi_kelas->getContent(),"select","Kelas Ruangan Untuk Mendaftarkan ke Fisiotherapy")
                    ->addItem("smis-rs-posisi-fisiotherapy","Posisi",$posisi ->getContent(),"select","Posisi tab fisiotherapy")
                    
                    ->addSection("Elektromedis")
                    ->addItem("smis-rs-elektromedis","Elektromedis","0","checkbox","Apakah Ruangan bisa Mendaftarkan ke Elektromedis")
                    ->addItem("smis-rs-kelas-elektromedis","Kelas Elektromedis",$opsi_kelas->getContent(),"select","Kelas Ruangan Untuk Mendaftarkan ke Elektromedis")

                    ->addSection("Medical Checkup")
                    ->addItem("smis-rs-mcu","MCU","0","checkbox","Apakah Ruangan bisa Mendaftarkan ke MCU")
                    ->addItem("smis-rs-kelas-mcu","Kelas Elektromedis",$opsi_kelas->getContent(),"select","Kelas Ruangan Untuk Mendaftarkan ke MCU")
                    ->addItem("smis-rs-posisi-mcu","Posisi MCU",$posisi->getContent(),"select","Posisi tab MCU")

                    ->addSection("Bank Darah")
                    ->addItem("smis-rs-bank_darah","Bank Darah","0","checkbox","Apakah Ruangan bisa Mendaftarkan ke Bank Darah")
                    ->addItem("smis-rs-kelas-bank_darah","Kelas Bank Darah",$opsi_kelas->getContent(),"select","Kelas Ruangan Untuk Mendaftarkan ke Bank Darah")
                    ->addItem("smis-rs-e_resep","E-Resep","0","checkbox","Layanan entri E-Resep di Keperawatan/Penunjang");
        }

        /**EXTENSTION */
        if($this->isGroup("rumus_pv","Rumus PV - Khusus Syamrabu"," fa fa-user-md")){
            $this	->addItem("smis-rs-rumus-pv-anastesi"," PV Dr. Anastesi","0","checkbox"," PV Dr. Anastesi")
                    ->addItem("smis-rs-rumus-pv-anak"," PV Dr. Anak","0","checkbox"," PV Dr. Anak")
                    ->addItem("smis-rs-rumus-pv-igd"," PV Dr. IGD","0","checkbox"," PV Dr. IGD")
                    ->addItem("smis-rs-rumus-pv-umum"," PV Dr. Umum","0","checkbox"," PV Dr. Umum")
                    ->addItem("smis-rs-rumus-pv-spesialis"," PV Dr. Spesialis","0","checkbox"," PV Dr. Spesialis")
                    ->addItem("smis-rs-rumus-pv-dpjp-satu"," PV Dr. DPJP I","0","checkbox"," PV Dr. DPJP I")
                    ->addItem("smis-rs-rumus-pv-dpjp-dua"," PV Dr. DPJP II","0","checkbox"," PV Dr. DPJP II")
                    ->addItem("smis-rs-rumus-pv-dpjp-tiga"," PV Dr. DPJP III","0","checkbox"," PV Dr. DPJP III")
                    ->addItem("smis-rs-rumus-pv-konsul-satu"," PV DR. Konsul I","0","checkbox"," PV Dr. Konsul I")
                    ->addItem("smis-rs-rumus-pv-konsul-dua"," PV DR. Konsul II","0","checkbox"," PV Dr. Konsul II")
                    ->addItem("smis-rs-rumus-pv-biaya-dpjp-satu"," PV Biaya DPJP I","0","checkbox"," PV Dr. Biaya DPJP I")
                    ->addItem("smis-rs-rumus-pv-biaya-dpjp-dua"," PV Biaya DPJP II","0","checkbox"," PV Dr. Biaya DPJP II")
                    ->addItem("smis-rs-rumus-pv-biaya-dpjp-tiga"," PV Biaya DPJP III","0","checkbox"," PV Dr. Biaya DPJP III")
                    ->addItem("smis-rs-rumus-pv-keterangan"," PV Formula","0","checkbox"," PV Formula")
                    ->addItem("smis-rs-rumus-pv-sync-konsul"," Sinkronisasi Dokter Konsul dengan Tagihan Rumus","0","checkbox"," Konsul Dokter Langsung Sync dengan Tagihan Rumus")
                    ->addItem("smis-rs-rumus-pv-sync-dpjp"," Sinkronisasi Dokter DPJP dengan Tagihan Rumus","0","checkbox"," DPJP Dokter Langsung Sync dengan Tagihan Rumus");
        }

        if($this->isGroup("settings_laporan","Setting Laporan Pendapatan","fa fa-book")){
            $this	->addItem("smis-rs-lapend-tperawat","Laporan Tindakan Perawat ","1","checkbox","")
                    ->addItem("smis-rs-lapend-tigd","Laporan Tindakan Ruangan","1","checkbox","")
                    ->addItem("smis-rs-lapend-tdokter","Laporan Tindakan Dokter","1","checkbox","")
                    ->addItem("smis-rs-lapend-konsul","Laporan Konsul ","1","checkbox","")
                    ->addItem("smis-rs-lapend-visite","Laporan Visite","1","checkbox","")
                    ->addItem("smis-rs-lapend-periksa","Laporan Periksa","1","checkbox","")
                    ->addItem("smis-rs-lapend-alok","Laporan Alat & Obat","1","checkbox","")
                    ->addItem("smis-rs-lapend-oksigen-central","Laporan Oksigen Central","1","checkbox","")
                    ->addItem("smis-rs-lapend-oksigen-manual","Laporan Oksigen Manual","1","checkbox","")
                    ->addItem("smis-rs-lapend-bed","Laporan Bed","1","checkbox","")
                    ->addItem("smis-rs-lapend-operasi","Laporan Operasi","1","checkbox","");
        }

        $this->settings->setPartialLoad(true);
		$response = $this->settings->init();
    }
}
?>
