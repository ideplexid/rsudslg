<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
class LaporanDiagnosaRehabMedisTemplate extends LayananTemplate {
    protected $action;

	public function __construct($db, $slug, $name, $action) {
        parent::__construct ($db, $slug, $name);
        $this->action = $action;
	}
	public function initialize() {
		if (isset($_POST['command']) && $_POST['command'] == "excel") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");

			$params = array();
			$params['tanggal_dari'] = $_POST['tanggal_dari'];
			$params['tanggal_sampai'] = $_POST['tanggal_sampai'];
			$consumer_service = new ServiceConsumer(
				$db,
				"get_data_laporan_diagnosa_rehab_medis",
				$params,
				"medical_record"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$data = $content[0];

			$file = new PHPExcel();
			$sheet = $file->getActiveSheet();
			$sheet->setTitle("Laporan Diagnosa Rehab Medis");
			$i = 1;
			$last_column = "D";

			$sheet->mergeCells("A" . $i . ":" . $last_column . $i)->setCellValue("A" . $i, "Laporan Diagnosa Rehab Medik");
			$sheet->getStyle("A" . $i . ":" . $last_column . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("A" . $i)->getFont()->setBold(true);
			$i = $i + 1;
			$sheet->mergeCells("A" . $i . ":" . $last_column . $i)->setCellValue("A" . $i, ArrayAdapter::format("unslug", $this->polislug));
			$sheet->getStyle("A" . $i . ":" . $last_column . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("A" . $i)->getFont()->setBold(true);
			$i = $i + 1;
			$sheet->mergeCells("A" . $i . ":" . $last_column . $i)->setCellValue("A" . $i, "Periode : " . ArrayAdapter::format("date d-m-Y", $_POST['tanggal_dari']) . " s.d. " . ArrayAdapter::format("date d-m-Y", $_POST['tanggal_sampai']));
			$sheet->getStyle("A" . $i . ":" . $last_column . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("A" . $i)->getFont()->setBold(true);
			$i = $i + 2;

			$border_start = $i;
			$col = 65;
			$sheet->setCellValue(chr($col) . $i, "No.");
			$sheet->getColumnDimension(chr($col))->setWidth(mb_strwidth("No.") + 15);
			$col++;
			$sheet->setCellValue(chr($col) . $i, "Diagnosa");
			$sheet->getColumnDimension(chr($col))->setWidth(mb_strwidth("Diagnosa") + 100);
			$col++;
			$sheet->setCellValue(chr($col) . $i, "Kelompok");
			$sheet->getColumnDimension(chr($col))->setWidth(mb_strwidth("Kelompok") + 15);
			$col++;
			$sheet->setCellValue(chr($col) . $i, "Jumlah");
			$sheet->getColumnDimension(chr($col))->setWidth(mb_strwidth("Jumlah") + 15);
			$sheet->getStyle("A" . $i . ":" . $last_column . $i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$sheet->getStyle("A" . $i . ":" . $last_column . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("A" . $i . ":" . $last_column . $i)->getFont()->setBold(true);
			$i += 1;

			if ($data != null) {
				$nomor = 1;
				foreach ($data as $d) {
					$sheet->setCellValue("A" . $i, $nomor++);
					$sheet->setCellValue("B" . $i, $d['diagnosa_rehab_medis']);
					$sheet->setCellValue("C" . $i, $d['grup_diagnosa']);
					$sheet->setCellValue("D" . $i, $d['jumlah']);
					$sheet->getStyle("D" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$i = $i + 1;
				}
			}

			$thin = array();
			$thin['borders'] = array();
			$thin['borders']['allborders'] = array();
			$thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
			$sheet->getStyle("A" . $border_start . ":" . $last_column . ($i - 1))->applyFromArray($thin);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Laporan Diagnosa Rehab Medis - ' . ArrayAdapter::format("unslug", $this->polislug) . ' - ' . ArrayAdapter::format("date Ymd", $_POST['tanggal_dari']) . ' - ' . ArrayAdapter::format("date Ymd", $_POST['tanggal_sampai']) . '.xls"');
			header('Cache-Control: max-age=0');
			$writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
			$writer->save('php://output');
			return;
		}

        $data                   = $_POST;
        $data ['polislug']      = $this->polislug;
		$data ['action']        = "laporan_diagnosa_rehab_medis";
		$data ['page']          = "rawat";
		
		$service = new ServiceConsumer ( $this->db, "get_lap_diagnosa_rehab_medis", $data, "medical_record" );
		$service->execute ();
		echo $service->getContent ();
	}
}
?>