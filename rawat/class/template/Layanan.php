<?php 
require_once "smis-base/smis-include-service-consumer.php";
require_once "rawat/class/responder/LayananResponder.php";
class Layanan extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $db;
	private $layanan_settings;
	private $tabs;
	private $diagnosa_only;
    private $is_titipan_enabled;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug             = $polislug;
		$this->poliname             = $poliname;
		$this->db                   = $db;
		$this->diagnosa_only        = false;
        $this->is_titipan_enabled   = getSettings($db,"smis-rs-titipan-".$this->polislug,"0");
	}
	
	public function setDiagnosaOnly($only){
		$this->diagnosa_only = $only;
	}
	
	private function getLoadTab($action,$load=false) {
		if(!$load) return "";
		$nrm        = isset($_POST ['nrm_pasien']) ? $_POST ['nrm_pasien'] : "";
		$nama       = isset($_POST ['nama']) ? $_POST ['nama'] : "";
		$noreg      = isset($_POST ['no_reg']) ? $_POST ['no_reg'] : "";
		if($nama=="") {
            $nama   = isset($_POST ['nama_pasien']) ? $_POST ['nama_pasien'] : "";
		}
		
		if($noreg==""){
            $noreg  = isset($_POST ['noreg_pasien']) ? $_POST ['noreg_pasien'] : "";
		}

		
		      
		$poliname   = isset($_POST ['prototype_name']) ? $_POST ['prototype_name'] : "";
		$polislug   = isset($_POST ['prototype_slug']) ? $_POST ['prototype_slug'] : "";
		$id_antrian = isset($_POST ['id_antrian']) ? $_POST ['id_antrian'] : "";
		$carabayar  = isset($_POST ['carabayar']) ? $_POST ['carabayar'] : "";
        $titipan    = isset($_POST ['titipan']) ? $_POST ['titipan'] : "";
		$db         = $this->db;
		

		if($noreg=="" || $noreg==null){
			$antr = new DBTable($db,"smis_rwt_antrian_".$this->polislug);
			$a = $antr->select($_POST['id_antrian']);
			$nrm = $a->nrm_pasien;
			$nama = $a->nama_pasien;
			$noreg = $a->no_register;
			$carabayar = $a->carabayar;
		}

		ob_start();
		$obj		= null;
		switch ($action){
			case "tindakan_perawat" 	: require_once 'rawat/class/template/layanan/tindakan/TindakanPerawat.php'; 	$obj=new TindakanPerawat($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	 	break;
			case "tindakan_perawat_igd"	: require_once 'rawat/class/template/layanan/tindakan/TindakanPerawatIGD.php';	$obj=new TindakanPerawatIGD($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	break;
			case "tindakan_dokter"		: require_once 'rawat/class/template/layanan/tindakan/TindakanDokter.php';		$obj=new TindakanDokter($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	    break;												
			case "konsul_dokter"		: require_once 'rawat/class/template/layanan/tindakan/KonsulDokter.php';		$obj=new KonsulDokter($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);			break;												
			case "konsultasi_dokter"	: require_once 'rawat/class/template/layanan/tindakan/KonsultasiDokter.php';	$obj=new KonsultasiDokter($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);		break;												
			case "visite_dokter"		: require_once 'rawat/class/template/layanan/tindakan/Visite.php';				$obj= new VisiteDokter($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);			break;												
			case "vk"					: require_once 'rawat/class/template/layanan/tindakan/VK.php';					$obj=new Vk($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);					break;												
			case "ok"					: require_once 'rawat/class/template/layanan/tindakan/OK.php';					$obj=new Ok($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);					break;												
			case "rr"					: require_once 'rawat/class/template/layanan/tindakan/RecoveryRoom.php';		$obj=new RecoveryRoom($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);			break;												
			case "faal_paru"			: require_once 'rawat/class/template/layanan/tindakan/FaalParu.php';			$obj=new FaalParu($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);				break;												
			case "endoscopy"			: require_once 'rawat/class/template/layanan/tindakan/Endoscopy.php';			$obj=new Endoscopy($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);				break;												
			case "bronchoscopy"			: require_once 'rawat/class/template/layanan/tindakan/Bronchoscopy.php';		$obj=new Bronchoscopy($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);			break;												
			case "audiometry"			: require_once 'rawat/class/template/layanan/tindakan/Audiometry.php';			$obj=new Audiometry($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);			break;												
			case "spirometry"			: require_once 'rawat/class/template/layanan/tindakan/Spirometry.php';			$obj=new Spirometry($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);			break;												
			case "ekg"					: require_once 'rawat/class/template/layanan/tindakan/Ekg.php';					$obj=new Ekg($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);					break;
			case "oksigen_central"		: require_once 'rawat/class/template/layanan/tindakan/OksigenCentral.php';		$obj=new OksigenCentralTemplate($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);break;
			case "oksigen_manual"		: require_once 'rawat/class/template/layanan/tindakan/OksigenManual.php';		$obj=new OksigenManualTemplate($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	break;
			case "bed"					: require_once 'rawat/class/template/layanan/tindakan/Bed.php';					$obj=new Bed($db, $polislug, $poliname, $nama, $nrm, $noreg,$id_antrian,$carabayar,$titipan);		break;
			case "alok"					: require_once 'rawat/class/template/layanan/tindakan/Alok.php';				$obj=new Alok($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);		            break;
			
            case "laboratory"			: require_once 'rawat/class/template/layanan/penunjang/DaftarLaboratory.php';	$obj=new DaftarLaboratory($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);		break;
			case "radiology"			: require_once 'rawat/class/template/layanan/penunjang/DaftarRadiology.php';	$obj=new DaftarRadiology($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);		break;
			case "fisiotherapy"			: require_once 'rawat/class/template/layanan/penunjang/DaftarFisiotherapy.php';	$obj=new DaftarFisiotherapy($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	break;
			case "elektromedis"			: require_once 'rawat/class/template/layanan/penunjang/DaftarElektromedis.php';	$obj=new DaftarElektromedis($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	break;
			case "mcu"					: require_once 'rawat/class/template/layanan/penunjang/DaftarMCU.php';			$obj=new DaftarMCU($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	break;
			case "bank_darah"			: require_once 'rawat/class/template/layanan/penunjang/DaftarBankDarah.php';	$obj=new DaftarBankDarah($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);		break;
			
			case "diagnosa_gz" 			: require_once 'rawat/class/template/layanan/laporan/DiagnosaGizi.php'; 		break;
			case "diagnosa" 			: require_once 'rawat/class/template/layanan/laporan/Diagnosa.php'; 			$obj=new Diagnosa ($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan); 			break;
			case "diagnosa_rehab_medis" : require_once 'rawat/class/template/layanan/laporan/DiagnosaRehabMedis.php';	$obj=new DiagnosaRehabMedis ($db, $polislug, $poliname, $nama, $nrm, $noreg, $carabayar, $titipan); break;
			case "asuhan_obat"			: require_once 'rawat/class/template/layanan/laporan/AsuhanObat.php';			$obj=new AsuhanObat($db,$titipan);																    break;
			case "alergi_obat"			: require_once 'rawat/class/template/layanan/laporan/AlergiObat.php';			$obj=new AlergiObat($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);			break;
			case "rujukan"				: require_once 'rawat/class/template/layanan/laporan/Rujukan.php';				$obj=new Rujukan($db, $polislug, $poliname, $nama, $nrm, $noreg, $id_antrian,$carabayar,$titipan);	break;
			case "e_resep"				: require_once "rawat/class/template/layanan/laporan/EResep.php"; 				$obj=new EResep($db,$titipan); 																		break;
			case "indikator"			: require_once 'rawat/class/template/layanan/laporan/Indikator.php';			$obj=new Indikator($db,$titipan);																	break;
			case "lap_operasi"			: require_once 'rawat/class/template/layanan/laporan/LapOperasi.php';			$obj=new LapOperasi($db,$titipan);																	break;
			case "lap_gigimulut"		: require_once 'rawat/class/template/layanan/laporan/LapGigiMulut.php';			$obj=new LapGigiMulut($db,$titipan);																break;
			case "lap_prolanis" 		: require_once 'rawat/class/template/layanan/laporan/LapProlanis.php';			$obj=new LapProlanis($db,$titipan);																	break;
			case "lap_perinatologi"		: require_once 'rawat/class/template/layanan/laporan/LaporanPerinatologi.php';	$obj=new LaporanPerinatologi( $db );  break;
			case "lap_rehabilitasi_medik" : require_once 'rawat/class/template/layanan/laporan/LapRehabilitasiMedik.php';			$obj=new LapRehabilitasiMedik( $db,$titipan);											break;
			case "lap_pkhusus"			: require_once 'rawat/class/template/layanan/laporan/LapPKhusus.php';			$obj=new LapPKhusus( $db,$titipan);																	break;
			case "lap_jiwa"				: require_once 'rawat/class/template/layanan/laporan/LapJiwa.php';				$obj=new LapJiwa( $db,$titipan);																	break;
			case "lap_igd"				: require_once 'rawat/class/template/layanan/laporan/LapIGD.php';				$obj=new LapIGD($db,$titipan);																		break;
			case "lap_kb"				: require_once 'rawat/class/template/layanan/laporan/LapKB.php';				$obj=new LapKB($db,$titipan);																		break;
			case "lap_persalinan"		: require_once 'rawat/class/template/layanan/laporan/LapPersalinan.php';		$obj=new LapPersalinan($db,$titipan);																break;
			case "lap_keluarga_berencana" : require_once 'rawat/class/template/layanan/laporan/LapKeluargaBerencana.php';		$obj=new LapKeluargaBerencana($db,$titipan);												break;
			case "total_tagihan_kasir"	: require_once 'rawat/class/template/layanan/laporan/TotalTagihan.php';			$obj=new TotalTagihan($db,$titipan);															    break;
			case "tagihan_rumus"	    : require_once 'rawat/class/template/layanan/laporan/TagihanRumus.php';			$obj=new TagihanRumus($db,$polislug, $poliname, $nama, $nrm, $noreg,$carabayar);				    break;
			case "odontogram"			: require_once 'rawat/class/template/layanan/laporan/Odontogram.php';			$obj=new Odontogram($db);																		    break;
			case "biaya"				: require_once 'rawat/class/template/layanan/laporan/Biaya.php';				$obj=new Biaya($db, $polislug, $poliname, $nama, $nrm, $noreg, $carabayar,$titipan);				break;
			case "rencana_bpjs"			: require_once 'rawat/class/template/layanan/laporan/RencanaBPJS.php';			$obj=new RencanaBPJS($db);																		break;
			case "dokter_dpjp"			: require_once 'rawat/class/template/layanan/laporan/DokterDPJP.php';			$obj=new DokterDPJP( $db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar);				break;
			case "resumemedis"			: require_once 'rawat/class/template/layanan/laporan/ResumeMedis.php';			$obj=new ResumeMedis( $db, $polislug, $poliname, $nama, $nrm, $noreg, $id_antrian,$carabayar);	break;
            case "plebitis"			    : require_once 'rawat/class/template/layanan/laporan/Plebitis.php';			    $obj=new Plebitis( $db, $polislug, $poliname, $nama, $nrm, $noreg, $id_antrian,$carabayar);	    break;
            case "cauti"			    : require_once 'rawat/class/template/layanan/laporan/Cauti.php';			    $obj=new Cauti($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	    break;
            case "imunisasi"			: require_once 'rawat/class/template/layanan/laporan/Imunisasi.php';			$obj=new Imunisasi( $db, $polislug, $poliname, $nama, $nrm, $noreg, $id_antrian,$carabayar);	break;
            case "asesmen_syaraf"		: require_once 'rawat/class/template/layanan/laporan/AsesmenSyaraf.php';		$obj=new AsesmenSyaraf( $db,$titipan);	                                                        break;
            case "asesmen_anak"	    	: require_once 'rawat/class/template/layanan/laporan/AsesmenAnak.php';  		$obj=new AsesmenAnak( $db);	                                                                    break;
            case "asesmen_perawat"	    : require_once 'rawat/class/template/layanan/laporan/AsesmenPerawat.php';  		$obj=new AsesmenPerawat( $db);	                                                                break;
            case "asesmen_jantung"	    : require_once 'rawat/class/template/layanan/laporan/AsesmenJantung.php';  		$obj=new AsesmenJantung( $db);	                                                                break;
            case "asesmen_psikologi"	: require_once 'rawat/class/template/layanan/laporan/AsesmenPsikologi.php';  	$obj=new AsesmenPsikologi( $db);	                                                            break;
            case "asesmen_psikiatri"	: require_once 'rawat/class/template/layanan/laporan/AsesmenPsikiatri.php';  	$obj=new AsesmenPsikiatri( $db);	                                                            break;
            //case "makanan_gizi"	        : require_once 'rawat/class/template/layanan/gizi/MakananGizi.php';  	    	$obj=new MakananGizi( $db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	    break;
			case "makanan_gizi"			: require_once 'rawat/class/template/layanan/gizi/PemesananMakanGiziLayanan.php'; $obj = new PemesananMakanGiziLayanan( $db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan ); break;

			case "makanan_gizi_layanan" : require_once 'rawat/class/template/layanan/gizi/MakananGiziLayanan.php';  	$obj=new MakananGiziLayanan( $db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	    break;
            case "diskon_kasir"         : require_once 'rawat/class/template/layanan/laporan/DiskonKasir.php';  	    $obj=new DiskonKasir( $db, $polislug, $poliname, $nama, $nrm, $noreg,$titipan);	                break;
            case "surat_sakit"          : require_once 'rawat/class/template/layanan/penunjang/SuratSakit.php';  	    $obj=new SuratSakit( $db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	    break; 
			case "jenis_perawatan"      : require_once 'rawat/class/template/layanan/laporan/JenisPerawatan.php';  	    $obj=new JenisPerawatan( $db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	    break;
			case "infeksi"          	: require_once 'rawat/class/template/layanan/laporan/Infeksi.php';  	    $obj=new Infeksi( $db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);	    break;
		}
		if($obj!=null) $obj->initialize ();
		$res	= ob_get_clean();
		return $res;
	}
	
	public function initialize() {		
		if($_POST['action']!='layanan'){
			echo $this->getLoadTab($_POST['action'],true);
		}else{
			if (isset($_POST ['super_command']) && $_POST ['super_command'] != "") {
				$this->superCommand($_POST ['super_command']);
			} else if (isset($_POST ['command'])) {
				$this->command($_POST ['command']);
			} else if (isset($_POST ['load_tab']) && $_POST ['load_tab'] == "load_tab") {
				if (! isset($_POST ['id']) || $_POST ['id'] == '' || $_POST ['id'] == '0' || $_POST ['id'] == null)
					return;
				$this->initTabs ();
				echo $this->tabs->getHtml ();
				return;
			} else {
				$this->phpPreLoad ();
				$this->jsLoader ();
				$this->cssLoader ();
				$this->jsPreLoad ();
				$this->cssPreLoad ();
				$this->htmlPreLoad ();
			}
		}		
	}

	public function superCommand($super_command) {
		if (isset($_POST ['super_command']) && $_POST ['super_command'] == 'antrian') {
			$header	 = array("Nomor",'Nama','NRM',"No Register");
			$uitable = new Table ($header);
			$uitable->setName("layanan");
			$uitable->setModel(Table::$SELECT);
			if (isset($_POST ['command'])) {
				$adapter = new SimpleAdapter ();
				$adapter ->add("nrm_pasien", "nrm_pasien")
						 ->add("NRM", "nrm_pasien", "only-digit6")
						 ->add("Nama", "nama_pasien")
						 ->add("No Register", "no_register")
						 ->add("Nomor", "nomor")
						 ->add("Asal", "asal")
						 ->add("Umur", "umur")
						 ->add("Kunjungan", "kunjungan")
						 ->add("carabayar", "carabayar")
						 ->add("golongan_umur", "golongan_umur")
						 ->add("jk", "jk")
						 ->add("kunjungan", "kunjungan")
						 ->add("kelas", "kelas","unslug")
						 ->add("kelas_asal", "kelas")
						 ->add("rl52", "rl52")
						 ->add("room_name", "room_name")
						 ->add("state_name", "state_name")
						 ->add("status_pasien", "status_pasien")
						 ->add("dokumen", "dokumen")
						 ->add("alamat", "alamat")
						 ->add("detail", "detail")
						 ->add("spesialisasi", "spesialisasi");
				$column  = array ('id','no_register','nrm_pasien','nama_pasien');
				$dbtable = new DBTable($this->db, "smis_rwt_antrian_" . $this->polislug, $column);
				$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
				if($this->diagnosa_only){
					$dbtable->addCustomKriteria("selesai", " ='1' ");
				}else{
					$dbtable->addCustomKriteria("selesai", " ='0' ");
				}
				$dbres = new LayananResponder($dbtable, $uitable, $adapter);
				$dbres->setPolislug($this->polislug);
				$dbres->setUseAdapterForSelect(true);

				$data = $dbres->command($_POST ['command']);
                echo json_encode($data);
				return;
			}
			echo $uitable->getHtml ();
			return;
		}else if (isset($_POST ['super_command']) && $_POST ['super_command'] == 'save_kelas_asal') {
			$dbtable			= new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
			$up['kelas']		= $_POST['kelas_asal'];
			$id['no_register']	= $_POST['noreg_pasien'];
			$dbtable->update($up, $id);
			
			$res = new ResponsePackage();
			$res ->setContent("")
				 ->setStatus(ResponsePackage::$STATUS_OK)
				 ->setAlertVisible(true)
				 ->setAlertContent("Berhasil", "Penyimpanan Kelas Asal Berhasil");
			echo json_encode($res->getPackage());
			return;
		}else if (isset($_POST ['super_command']) && $_POST ['super_command'] == 'save_rl52') {
			$dbtable			= new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
			$up['rl52']			= $_POST['rl52'];
			$id['no_register']	= $_POST['noreg_pasien'];
			$dbtable->update($up, $id);

			$params = array();
			$params['noreg_pasien'] = $_POST['noreg_pasien'];
			$params['ruangan_entri'] = $this->polislug;
			$params['jenis_kegiatan'] = $_POST['rl52'];
			$consumer_service = new ServiceConsumer(
				$this->db,
				"update_jenis_kegiatan",
				$params,
				"registration"
			);
			$consumer_service->execute();
			
			$res = new ResponsePackage();
			$res ->setContent("")
				 ->setStatus(ResponsePackage::$STATUS_OK)
				 ->setAlertVisible(true)
				 ->setAlertContent("Berhasil", "Penyimpanan RL - 5.2 Asal Berhasil");
			echo json_encode($res->getPackage());
			return;
		}else if (isset($_POST ['super_command']) && $_POST ['super_command'] == 'save_dokumen') {
			$dbtable			= new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
			$up['dokumen']		= $_POST['dokumen'];
			$id['no_register']	= $_POST['noreg_pasien'];
			$dbtable->update($up, $id);
			
			$res = new ResponsePackage();
			$res ->setContent("")
				 ->setStatus(ResponsePackage::$STATUS_OK)
				 ->setAlertVisible(true)
				 ->setAlertContent("Berhasil", "Status Dokumen diperbarui");
			echo json_encode($res->getPackage());
			return;
		}else if (isset($_POST['super_command']) && $_POST['super_command'] == 'get_info_titipan') {
			$dbtable	= new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
			$row 		= $dbtable->select(array("no_register"=>$_POST['noreg_pasien']));
			$keterangan = "-";
			if($row != null && $row->titipan=="1") {
				$keterangan = "Titipan";
			}
			echo json_encode($keterangan);
			return;
		}else if (isset($_POST ['super_command']) && $_POST ['super_command'] == 'save_state_room') {
			$dbtable				= new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
			$up['room_name']		= $_POST['room_name'];
			$up['state_name']		= $_POST['state_name'];
			$up['status_pasien']	= $_POST['status_pasien'];
			$id['no_register']		= $_POST['noreg_pasien'];
			$dbtable->update($up, $id);
			
			$res = new ResponsePackage();
			$res ->setContent("")
				 ->setStatus(ResponsePackage::$STATUS_OK)
				 ->setAlertVisible(true)
				 ->setAlertContent("Berhasil", "Penyimpanan Status Posisi Pasien Operasi Bisa disimpan");
			echo json_encode($res->getPackage());
			return;
		} else if (isset($_POST ['super_command']) && $_POST ['super_command'] == "save_spesialisasi") {
			$dbtable						= new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
			$update_data['spesialisasi']	= $_POST['spesialisasi'];
			$id['id']						= $_POST['id_antrian'];
			$dbtable->update($update_data, $id);

			$params = array();
			$params['noreg_pasien'] = $_POST['noreg_pasien'];
			$params['ruangan_entri'] = $this->polislug;
			$params['spesialisasi'] = $_POST['spesialisasi'];
			$consumer_service = new ServiceConsumer(
				$db,
				"update_last_spesialisasi",
				$params,
				"registration"
			);
			$consumer_service->execute();
			
			$res = new ResponsePackage();
			$res ->setContent("")
				 ->setStatus(ResponsePackage::$STATUS_OK)
				 ->setAlertVisible(true)
				 ->setAlertContent("Berhasil", "Jenis Spesialisasi Berhasil Disimpan");
			echo json_encode($res->getPackage());
			return;
		} else if (isset($_POST ['super_command']) && $_POST ['super_command'] == "save_spesialisasi2") {
			$dbtable						= new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
			$update_data['spesialisasi2']	= $_POST['spesialisasi2'];
			$id['id']						= $_POST['id_antrian'];
			$dbtable->update($update_data, $id);

			$params = array();
			$params['noreg_pasien'] = $_POST['noreg_pasien'];
			$params['ruangan_entri'] = $this->polislug;
			$params['spesialisasi2'] = $_POST['spesialisasi2'];
			$consumer_service = new ServiceConsumer(
				$db,
				"update_last_spesialisasi2",
				$params,
				"registration"
			);
			$consumer_service->execute();
			
			$res = new ResponsePackage();
			$res ->setContent("")
				 ->setStatus(ResponsePackage::$STATUS_OK)
				 ->setAlertVisible(true)
				 ->setAlertContent("Berhasil", "Ext. Spesialisasi Berhasil Disimpan");
			echo json_encode($res->getPackage());
			return;
		}
	}
	public function getSettings($slug) {
		return getSettings($this->db, $slug . "-" . $this->polislug, "0") == "1";
	}
	
	private function isBPJS(){
		if(isset($_POST['carabayar']) && strpos($_POST['carabayar'], "bpjs")!==false){
			return true;
		}
		return false;
	}
	
	public function initTabs() {
		$this->tabs = new Tabulator('layanan_tabs', "tabs", Tabulator::$POTRAIT);		
		/* Pelayanan */
		if($this->isBPJS()){
			$tabs = new Tabulator("", "", Tabulator::$LANDSCAPE);
			if ($this->getSettings("smis-rs-bpjs-plan")) 		$tabs->add("rencana_bpjs", "Rencana BPJS", $this->getLoadTab("rencana_bpjs"),"html","fa fa-money","layanan.loadData('rencana_bpjs')");			
			if ($tabs->getTotalElement () > 0)						$this->tabs->add("bpjs", "BPJS", $tabs, Tabulator::$TYPE_COMPONENT,"fa fa-file","trigger_first(this)");			
		}
		
		$tabs = new Tabulator("", "", Tabulator::$LANDSCAPE);
		if ($this->getSettings("smis-rs-diagnosa")) 			$tabs->add("diagnosa", "Diagnosa", $this->getLoadTab("diagnosa"),"html","fa fa-stethoscope","layanan.loadData('diagnosa')");
		if ($this->getSettings("smis-rs-input_data_diagnosa_rehab_medis")) 	$tabs->add("diagnosa_rehab_medis", "Diagnosa Medis", $this->getLoadTab("diagnosa_rehab_medis"), "html", "fa fa-heart", "layanan.loadData('diagnosa_rehab_medis')");
		if ($this->getSettings("smis-rs-diagnosa-gizi")) 		$tabs->add("diagnosa_gz", "Diagnosa Gizi", $this->getLoadTab("diagnosa_gz"),"html","fa fa-stethoscope","layanan.loadData('diagnosa_gz')");
		
		if ($this->getSettings("smis-rs-fisiotherapy") && $this->getSettings("smis-rs-posisi-fisiotherapy")=="1" )		$tabs->add("fisiotherapy", " Fisiotherapy", $this->getLoadTab("fisiotherapy"),"html","fa fa-bed","layanan.loadData('fisiotherapy')");
		if ($this->getSettings("smis-rs-mcu") && $this->getSettings("smis-rs-posisi-mcu")=="1" )				$tabs->add("mcu", " Medical Check Up ", $this->getLoadTab("mcu"),"html","fa fa-users","layanan.loadData('mcu')");
			
		if ($this->getSettings("smis-rs-resumemedis")) 			$tabs->add("resumemedis", "Resume Medis", $this->getLoadTab("resumemedis"),"html","fa fa-instagram","layanan.loadData('resumemedis')");
		
		if(!$this->diagnosa_only){
			if ($this->getSettings("smis-rs-odontogram")) 		$tabs->add("odontogram", getSettings($this->db,"smis-rs-name-odontogram-".$this->polislug, "Odontogram"), $this->getLoadTab("odontogram"),"html","fa fa-legal","layanan.loadData('odontogram')");
			if ($this->getSettings("smis-rs-ok"))				$tabs->add("ok", getSettings($this->db,"smis-rs-name-ok-".$this->polislug, "Kamar Operasi"), $this->getLoadTab("ok"),"html","fa fa-cut","layanan.loadData('ok')");
			if ($this->getSettings("smis-rs-tperawat"))			$tabs->add("tindakan_perawat", getSettings($this->db,"smis-rs-name-tindakan_perawat-".$this->polislug, "Tindakan Perawat"), $this->getLoadTab("tindakan_perawat"),"html","fa fa-lightbulb-o","layanan.loadData('tindakan_perawat')");
			if ($this->getSettings("smis-rs-tperawat-igd"))		$tabs->add("tindakan_perawat_igd", getSettings($this->db,"smis-rs-name-tindakan_perawat_igd-".$this->polislug, "Tindakan Perawat IGD"), $this->getLoadTab("tindakan_perawat_igd"),"html","fa fa-warning","layanan.loadData('tindakan_perawat_igd')");
			if ($this->getSettings("smis-rs-tdokter"))			$tabs->add("tindakan_dokter", getSettings($this->db,"smis-rs-name-tindakan_dokter-".$this->polislug, "Tindakan Dokter"), $this->getLoadTab("tindakan_dokter"),"html","fa fa-user-md","layanan.loadData('tindakan_dokter')");
			if ($this->getSettings("smis-rs-konsuldokter")) 	$tabs->add("konsul_dokter", getSettings($this->db,"smis-rs-name-konsul_dokter-".$this->polislug, "Konsul Dokter"), $this->getLoadTab("konsul_dokter"),"html","fa fa-recycle","layanan.loadData('konsul_dokter')");
			if ($this->getSettings("smis-rs-konsultasidokter"))	$tabs->add("konsultasi_dokter", getSettings($this->db,"smis-rs-name-konsultasi_dokter-".$this->polislug, "Periksa Dokter"), $this->getLoadTab("konsultasi_dokter"),"html","fa fa-medkit","layanan.loadData('konsultasi_dokter')");
			if ($this->getSettings("smis-rs-visite"))			$tabs->add("visite_dokter", getSettings($this->db,"smis-rs-name-visite_dokter-".$this->polislug, "Visite Dokter"), $this->getLoadTab("visite_dokter"),"html","fa fa-tint","layanan.loadData('visite_dokter')");
			//if ($this->getSettings("smis-rs-vk"))				$tabs->add("vk", getSettings($this->db,"smis-rs-name-vk-".$this->polislug, "Kamar Bersalin"), $this->getLoadTab("vk"),"html","fa fa-venus","layanan.loadData('vk')");
			//if ($this->getSettings("smis-rs-rr"))				$tabs->add("rr", getSettings($this->db,"smis-rs-name-rr-".$this->polislug, "Recovery Room"), $this->getLoadTab("rr"),"html","fa fa-paperclip","layanan.loadData('rr')");
		}
		if ($tabs->getTotalElement () > 0)						$this->tabs->add("urj", "Pelayanan", $tabs, Tabulator::$TYPE_COMPONENT,"fa fa-heartbeat","trigger_first(this)");
		
		if(!$this->diagnosa_only){
			/* Alat dan Obat */
			$tabs = new Tabulator("oal", "Obat dan Alat", Tabulator::$LANDSCAPE);
			if ($this->getSettings("smis-rs-oksigen-central"))	$tabs->add("oksigen_central", getSettings($this->db,"smis-rs-name-oksigen_central-".$this->polislug, "Oksigen Central"), $this->getLoadTab("oksigen_central"),"html","fa fa-fire","layanan.loadData('oksigen_central')");
			if ($this->getSettings("smis-rs-oksigen-manual"))	$tabs->add("oksigen_manual", getSettings($this->db,"smis-rs-name-oksigen_manual-".$this->polislug, "Oksigen Manual"), $this->getLoadTab("oksigen_manual"),"html","fa fa-fire-extinguisher","layanan.loadData('oksigen_manual')");
			if ($this->getSettings("smis-rs-bed"))				$tabs->add("bed", getSettings($this->db,"smis-rs-name-bed-".$this->polislug, "Bed Kamar"), $this->getLoadTab("bed"),"html","fa fa-bed","layanan.loadData('bed')");
			if ($this->getSettings("smis-rs-alok"))				$tabs->add("alok", getSettings($this->db,"smis-rs-name-alok-".$this->polislug, "Alat dan Obat"), $this->getLoadTab("alok"),"html","fa fa-wrench","layanan.loadData('alok')");
			if ($this->getSettings("smis-rs-asuhan-obat"))		$tabs->add("asuhan_obat", getSettings($this->db,"smis-rs-name-asuhan-obat-".$this->polislug, "Asuhan Obat"), $this->getLoadTab("asuhan_obat"),"html","fa fa-paperclip","layanan.loadData('asuhan_obat')");
			if ($this->getSettings("smis-rs-alergi-obat"))		$tabs->add("alergi_obat", getSettings($this->db,"smis-rs-name-alergi-obat-".$this->polislug, "Alergi Obat"), $this->getLoadTab("alergi_obat"),"html","fa fa-eyedropper","layanan.loadData('alergi_obat')");
			if ($tabs->getTotalElement () > 0)						$this->tabs->add("oal", "Obat dan Alat", $tabs, Tabulator::$TYPE_COMPONENT ,"fa fa-wrench","trigger_first(this)");
			/* Register */
			$tabs = new Tabulator("", "", Tabulator::$LANDSCAPE);
			if ($this->getSettings("smis-rs-rujukan"))			$tabs->add("rujukan", "Registrasi", $this->getLoadTab("rujukan"),"html","fa fa-send","layanan.loadData('rujukan')");
			if ($this->getSettings("smis-rs-fisiotherapy") && $this->getSettings("smis-rs-posisi-fisiotherapy")=="0" )		$tabs->add("fisiotherapy", " Fisiotherapy", $this->getLoadTab("fisiotherapy"),"html","fa fa-bed","layanan.loadData('fisiotherapy')");
		
			if ($this->getSettings("smis-rs-laboratory"))		$tabs->add("laboratory", " Laboratory ", $this->getLoadTab("laboratory"),"html","fa fa-eyedropper","layanan.loadData('laboratory')");
			if ($this->getSettings("smis-rs-radiology"))		$tabs->add("radiology", " Radiology ", $this->getLoadTab("radiology"),"html","fa fa-warning","layanan.loadData('radiology')");
			if ($this->getSettings("smis-rs-elektromedis"))		$tabs->add("elektromedis", " Elektromedis ", $this->getLoadTab("elektromedis"),"html","fa fa-bolt","layanan.loadData('elektromedis')");
			if ($this->getSettings("smis-rs-mcu") && $this->getSettings("smis-rs-posisi-mcu")=="0" )				$tabs->add("mcu", " Medical Check Up ", $this->getLoadTab("mcu"),"html","fa fa-users","layanan.loadData('mcu')");
			if ($this->getSettings("smis-rs-bank_darah"))		$tabs->add("bank_darah", " Bank Darah", $this->getLoadTab("bank_darah"),"html","fa fa-tint","layanan.loadData('bank_darah')");
			if ($this->getSettings("smis-rs-kasir-surat-sakit"))$tabs->add("surat_sakit", "Surat Sakit", $this->getLoadTab("surat_sakit"), Tabulator::$TYPE_HTML,"fa fa-file-o","layanan.loadData('surat_sakit')");
            if ($tabs->getTotalElement () > 0)					$this->tabs->add("d_penunjang", "Transfer Pasien", $tabs, Tabulator::$TYPE_COMPONENT ,"fa fa-wheelchair","trigger_first(this)");
		}
		
		/* E-Resep */
		if ($this->getSettings("smis-rs-e_resep")) {
			$tabs = new Tabulator("", "", Tabulator::$LANDSCAPE);
			$tabs->add ("e_resep", "Pembuatan E-Resep", $this->getLoadTab("e_resep"), "html", "fa fa-file", "layanan.loadData('e_resep')");
			if ($tabs->getTotalElement () > 0)
				$this->tabs->add("e_resep", "E-Resep", $tabs, Tabulator::$TYPE_COMPONENT, "fa fa-file", "trigger_first(this)");
		}
		
		/* Register */
		$tabs = new Tabulator("", "", Tabulator::$LANDSCAPE);
		if ($this->getSettings("smis-rs-dokter-dpjp"))		 $tabs->add("dokter_dpjp", "Dokter DPJP ", $this->getLoadTab("dokter_dpjp"),"html","fa fa-user-md","layanan.loadData('dokter_dpjp')");
		if ($this->getSettings("smis-rs-plebitis"))		     $tabs->add("plebitis", "Plebitis RM-35 ", $this->getLoadTab("plebitis"),"html","fa fa-list","layanan.loadData('plebitis')");
		if ($this->getSettings("smis-rs-cauti"))		     $tabs->add("cauti", "Cauti RM-36 ", $this->getLoadTab("cauti"),"html","fa fa-snowflake-o","layanan.loadData('cauti')");
		if ($this->getSettings("smis-rs-imunisasi"))		 $tabs->add("imunisasi", "Imunisasi ", $this->getLoadTab("imunisasi"),"html","fa fa-eyedropper","layanan.loadData('imunisasi')");
		if ($this->getSettings("smis-rs-laporan-iklin"))	 $tabs->add("indikator", "Indikator Klinis ", $this->getLoadTab("indikator"),"html","fa fa-bar-chart","layanan.loadData('indikator')");
		if ($this->getSettings("smis-rs-laporan-operasi"))	 $tabs->add("lap_operasi", "Lap. Operasi", $this->getLoadTab("lap_operasi"),"html","fa fa-line-chart","layanan.loadData('lap_operasi')");
		if ($this->getSettings("smis-rs-laporan-gigimulut")) $tabs->add("lap_gigimulut", "Lap. Gigi & Mulut", $this->getLoadTab("lap_gigimulut"),"html","fa fa-file-o","layanan.loadData('lap_gigimulut')");
		if ($this->getSettings("smis-rs-laporan-prolanis"))	 $tabs->add("lap_prolanis", "Lap. Prolanis & Rujuk Balik", $this->getLoadTab("lap_prolanis"),"html","fa fa-recycle","layanan.loadData('lap_prolanis')");
		if ($this->getSettings("smis-rs-laporan-rl35"))	 	 $tabs->add("lap_perinatologi", "Lap. Perinatologi", $this->getLoadTab("lap_perinatologi"),"html","fa fa-file","layanan.loadData('lap_perinatologi')");
		if ($this->getSettings("smis-rs-laporan-rl39"))	 	 $tabs->add("lap_rehabilitasi_medik", "Lap. Rehabilitasi Medik", $this->getLoadTab("lap_rehabilitasi_medik"),"html","fa fa-file","layanan.loadData('lap_rehabilitasi_medik')");
		if ($this->getSettings("smis-rs-laporan-rl310"))	 $tabs->add("lap_pkhusus", "Lap. Pelayanan Khusus", $this->getLoadTab("lap_pkhusus"),"html","fa fa-file","layanan.loadData('lap_pkhusus')");
		if ($this->getSettings("smis-rs-laporan-rl311"))	 $tabs->add("lap_jiwa", "Lap. Kesehatan Jiwa", $this->getLoadTab("lap_jiwa"),"html","fa fa-archive","layanan.loadData('lap_jiwa')");
		if ($this->getSettings("smis-rs-laporan-rl312"))	 $tabs->add("lap_keluarga_berencana", "Lap. Keluarga Berencana", $this->getLoadTab("lap_keluarga_berencana"),"html","fa fa-archive","layanan.loadData('lap_keluarga_berencana')");
		if ($this->getSettings("smis-rs-laporan-igd"))		 $tabs->add("lap_igd", "Lap. IGD", $this->getLoadTab("lap_igd"),"html","fa fa-pie-chart","layanan.loadData('lap_igd')");
		if ($this->getSettings("smis-rs-laporan-kb"))		 $tabs->add("lap_kb", "Lap. KB", $this->getLoadTab("lap_kb"),"html","fa fa-medkit","layanan.loadData('lap_kb')");
		if ($this->getSettings("smis-rs-laporan-persalinan"))$tabs->add("lap_persalinan", " Lap. Persalinan", $this->getLoadTab("lap_persalinan"),"html","fa fa-child","layanan.loadData('lap_persalinan')");
		if ($this->getSettings("smis-rs-asesmen-syaraf"))    $tabs->add("asesmen_syaraf", " Assesmen Syaraf", $this->getLoadTab("asesmen_syaraf"),"html","fa fa-child","layanan.loadData('asesmen_syaraf')");
		if ($this->getSettings("smis-rs-asesmen-anak"))      $tabs->add("asesmen_anak", " Assesmen Anak", $this->getLoadTab("asesmen_anak"),"html","fa fa-child","layanan.loadData('asesmen_anak')");
		if ($this->getSettings("smis-rs-asesmen-perawat"))   $tabs->add("asesmen_perawat", " Assesmen Keperawatan", $this->getLoadTab("asesmen_perawat"),"html","fa fa-child","layanan.loadData('asesmen_perawat')");
		if ($this->getSettings("smis-rs-asesmen-jantung"))   $tabs->add("asesmen_jantung", " Assesmen Jantung", $this->getLoadTab("asesmen_jantung"),"html","fa fa-child","layanan.loadData('asesmen_jantung')");
		if ($this->getSettings("smis-rs-asesmen-psikologi")) $tabs->add("asesmen_psikologi", " Assesmen Psikologi", $this->getLoadTab("asesmen_psikologi"),"html","fa fa-child","layanan.loadData('asesmen_psikologi')");
		if ($this->getSettings("smis-rs-asesmen-jenis-perawatan"))   $tabs->add("jenis_perawatan", " Assesmen Jenis Perawatan", $this->getLoadTab("jenis_perawatan"),"html","fa fa-user-md","layanan.loadData('jenis_perawatan')");
		if ($this->getSettings("smis-rs-asesmen-infeksi")) $tabs->add("infeksi", " Assesmen Infeksi", $this->getLoadTab("infeksi"),"html","fa fa-scissors","layanan.loadData('infeksi')");
		if ($tabs->getTotalElement () > 0)					 $this->tabs->add("report", "Laporan", $tabs, Tabulator::$TYPE_COMPONENT ,"fa fa-area-chart","trigger_first(this)");
		
		$tabs = new Tabulator("", "", Tabulator::$LANDSCAPE);
		if ($this->getSettings("smis-rs-gizi-makanan"))      	  $tabs->add("makanan_gizi", "Menu Makanan Gizi", $this->getLoadTab("makanan_gizi"),"html","fa fa-cutlery","layanan.loadData('makanan_gizi')");
		if ($this->getSettings("smis_gz_menu_gizi_layanan"))      $tabs->add("makanan_gizi_layanan", "Menu Makanan Gizi", $this->getLoadTab("makanan_gizi_layanan"),"html","fa fa-spoon","layanan.loadData('makanan_gizi_layanan')");
		if ($tabs->getTotalElement () > 0)					 	  $this->tabs->add("makan_gizi", "Gizi", $tabs, Tabulator::$TYPE_COMPONENT ,"fa fa-cutlery","trigger_first(this)");
		

		$tabs = new Tabulator("", "", Tabulator::$LANDSCAPE);
        if ($this->getSettings("smis-rs-kasir-diskon"))     $tabs->add("diskon_kasir", "Pengajuan Diskon Kasir", $this->getLoadTab("diskon_kasir"), Tabulator::$TYPE_HTML,"fa fa-money","layanan.loadData('diskon_kasir')");
                                                            $tabs->add("biaya", "Tagihan Ruangan", $this->getLoadTab("biaya"), Tabulator::$TYPE_HTML,"fa fa-file-o","layanan.loadData('biaya')");
                                                            $tabs->add("total_tagihan_kasir", "Total Tagihan", $this->getLoadTab("total_tagihan"), Tabulator::$TYPE_HTML,"fa fa-money","layanan.loadData('total_tagihan_kasir')");
		if ($this->getSettings("smis-rs-tagihan-rumus"))    $tabs->add("tagihan_rumus", "Tagihan Rumus", $this->getLoadTab("tagihan_rumus"), Tabulator::$TYPE_HTML,"fa fa-list","layanan.loadData('tagihan_rumus')");
        $this->tabs->add("tagihan", "Tagihan", $tabs, Tabulator::$TYPE_COMPONENT ,"fa fa-money","trigger_first(this)");
	}


	public function phpPreLoad() {
		$modal_pasien = new Modal("layanan_modal", '', "Pilih Pasien");
		$id_antrian = "";
		if (isset($_POST ['id_antrian'])) {
			$id_antrian = $_POST ['id_antrian'];
		}
		$id     = new Hidden("id_antrian", "id_antrian", $id_antrian);
		$action = new Button('select_student', 'Select', 'Select');
        $action ->setAction("layanan.list_antrian()")
                ->setClass("btn-primary")
                ->setIsButton(Button::$ICONIC)
                ->setIcon("icon-list-alt icon-white");
		
		$b_action = new Button('select_student', 'Kembali', 'Kembali');
		$b_action ->setClass("btn-primary")
                  ->setIsButton(Button::$ICONIC)
                  ->setAction("layanan.come_back()")
                  ->setIcon("fa fa-sign-out");
		
		$noreg    = new Text('noreg_pasien', 'noreg_pasien', "");
		$nama     = new Text('nama_pasien', 'nama_pasien', "");
		$nama     ->setClass("smis-two-option-input");
		
		$plafon = new Text('plafon_bpjs', 'plafon_bpjs', "");
		$plafon ->setModel(Text::$MONEY);
		$plafon ->addClass("info_bpjs");

		$total  = new Text('total_tagihan', 'total_tagihan', "");
		$total  ->setModel(Text::$MONEY);
		$total  ->addClass("info_bpjs");

		$sisa   = new Text('sisa_plafon', 'sisa_plafon', "");
		$sisa   ->setModel(Text::$MONEY);
		$sisa   ->addClass("info_bpjs");

		$naik   = new Text('naik_kelas_bpjs', 'naik_kelas_bpjs', "");
		$naik   ->setModel(Text::$MONEY);
		$naik   ->addClass("info_bpjs");
		
		$kbpjs  = new Text('kelas_bpjs', 'kelas_bpjs', "");
		$kbpjs  ->setModel(Text::$MONEY);
		$kbpjs  ->addClass("info_bpjs");
		

        $update = new Button("","","");
        $update ->setClass("btn-primary")
                ->setIcon("fa fa-download")
                ->setAction("layanan.load_plafon_update()")
                ->setIsButton(Button::$ICONIC);
        
        
        
		$warning_persen  = getSettings($this->db,"smis-rs-bpjs-warning-" . $this->polislug,"100");
		$stop_persen     = getSettings($this->db,"smis-rs-bpjs-stop-" . $this->polislug,"100");
		$ac_stop_plafon  = getSettings($this->db,"smis-rs-activate-bpjs-stop-" . $this->polislug,"0");
		$autoload_change = getSettings($this->db,"smis-rs-bpjs-autoload-" . $this->polislug,"1");
        
        $warning_plafon         = new Hidden('warning_plafon', 'warning_plafon', $warning_persen);
		$activate_stop_plafon   = new Hidden('activate_stop_plafon', 'activate_stop_plafon', $ac_stop_plafon);
		$stop_plafon            = new Hidden('stop_plafon', 'stop_plafon', $stop_persen);
		$autoload_plafon        = new Hidden('autoload_plafon', 'autoload_plafon', $autoload_change);
		$tanggal_lahir          = new Hidden('tanggal_lahir', 'tanggal_lahir', "");
		
		$nm = new InputGroup("");
		$nm ->addComponent($nama)
            ->addComponent($action)
            ->addComponent($b_action);
		
        $plebi  = new OptionBuilder();
        $plebi  ->add("","","1")
                ->add("Bukan Pasien Plebitis","0","0")
                ->add("Pasien Plebitis","1","0");
        
        $caut   = new OptionBuilder();
        $caut   ->add("","","1")
                ->add("Bukan Pasien Ca-Uti","0","0")
                ->add("Pasien Ca-Uti","1","0");
        
		
		$nrm            = new Text('nrm_pasien','nrm_pasien',"");
		$diagnosa_only  = new Hidden('mode_layanan','mode_layanan', $this->diagnosa_only?"diagnosa_only":"");
		$asal           = new Text('asal_pasien','asal_pasien',"");
		$umur           = new Text('umur_pasien','umur_pasien',"");
		$kunjungan      = new Text('kunjungan_pasien','kunjungan_pasien',"");
		$bayar          = new Text('carabayar','carabayar',"");
		$gol            = new Text('gol_umur','gol_umur',"");
		$jk             = new Text('jk','jk',"");
		$alamat         = new Text('alamat','alamat',"");
		$kelas          = NULL;
        $titipan        = new Text('titipan','titipan',"");
        
        /*cek plebitis*/
        $plebitis           = NULL;
        $plebitis_name      = "";
        if(getSettings($this->db, "smis-rs-show-plebitis-".$this->polislug, "0")=="1"){
            $plebitis       = new Select("locker_plebitis","locker_plebitis",$plebi->getContent());
            $plebitis_name  = "Plebitis";
        }else{
             $plebitis      = new Hidden("locker_plebitis","locker_plebitis","");
        }
        /*end plebitis*/
		
        /*end cauti*/
        $cauti          = NULL;
        $cauti_name     = "";
		if(getSettings($this->db, "smis-rs-show-plebitis-".$this->polislug, "0")=="1"){
            $cauti      = new Select("locker_cauti","locker_cauti",$caut->getContent());
            $cauti_name = "Ca-UTI";
        }else{
             $cauti     = new Hidden("locker_cauti","locker_cauti","");
        }
		/*end cauti*/
		
		/*end cauti*/
        $op_room          = NULL;
        $op_room_name     = "";
		$op_state         = NULL;
        $op_state_name    = "";
		if(getSettings($this->db, "smis-rs-operation-room-status-".$this->polislug, "0")=="1"){
			loadLibrary("smis-libs-function-medical");
            $op_room      	= new Select("operation_room_name","operation_room_name",get_operation_room());
			$op_room_name 	= "Posisi";
			$op_state       = new Select("operation_state_name","operation_state_name",get_operation_state());
			$op_state_name  = "Status";
		}else{
			$op_room      	= new Hidden("operation_room_name","operation_room_name","");
			$op_state       = new Hidden("operation_state_name","operation_state_name","");
		}
		require_once "rawat/function/get_status_pasien.php";
		$status_pasien    	= new Select("operation_status_pasien","operation_status_pasien",get_status_pasien());
        /*end cauti*/
        
		$nkelas         = "Kelas Asal";
		if(getSettings($this->db, "smis-rs-show-kelas-asal-".$this->polislug, "0")=="1"){
			$service        = new ServiceConsumer($this->db, "get_kelas");
            $service->setCached(true,"get_kelas");
			$service->execute ();
			$kls            = $service->getContent ();
			$option_kelas   = new OptionBuilder ();
			foreach($kls as $k) {
				$option_kelas->add($k ['nama'], $k ['slug']);
			}
			$kelas_asal = new Select( 'kelas_asal', 'kelas_asal', $option_kelas->getContent());
		}else{
			$kelas_asal = new Hidden('kelas_asal', 'kelas_asal', '');
			$nkelas="";
		}
		
		
		$rl52           = NULL;
		$name_rl52      = "RL - 52";
		if(getSettings($this->db, "smis-rs-rl52-".$this->polislug, "0")=="1"){
			loadLibrary("smis-libs-function-medical");
			$rl52       = new Select("rl52", "rl52", get_rl52_option());
		}else{
			$rl52       = new Hidden("rl52", "rl52", "");
			$name_rl52  = "";
		}
		
		$dokumen_rm          = NULL;
		$nama_dokumen_rm     = "";
		if(getSettings($this->db, "smis-rs-dokumen-".$this->polislug, "0")=="1"){
			loadLibrary("smis-libs-function-medical");
			$dokumen_rm      = new Select("dokumen", "dokumen",get_dokumen_status());
			$nama_dokumen_rm = "Dokumen";
		}else{
			$dokumen_rm      = new Hidden("dokumen", "dokumen", "");
		}
		
		$noreg       ->setDisabled(true);
		$nama        ->setDisabled(true);
		$nrm         ->setDisabled(true);
		$asal        ->setDisabled(true);
		$plafon      ->setDisabled(true);
		$total       ->setDisabled(true);
		$sisa        ->setDisabled(true);
		$naik        ->setDisabled(true);
		$kbpjs       ->setDisabled(true);
		$umur        ->setDisabled(true);
		$kunjungan   ->setDisabled(true);
		$bayar       ->setDisabled(true);
		$gol         ->setDisabled(true);
		$jk          ->setDisabled(true);
        $titipan     ->setDisabled(true);
		$alamat      ->setDisabled(true);
        
        $total_grup = new InputGroup("","total_tagihan","");
        $total_grup ->addComponent($total)
                    ->addComponent($update);
        
		if(getSettings($this->db,"smis-rs-edit-plebitis-".$this->polislug,"0")=="0"){
            $plebitis->setDisabled(true);
        }
        
        if(getSettings($this->db,"smis-rs-edit-cauti-".$this->polislug,"0")=="0"){
            $cauti->setDisabled(true);
        }
        
		$form = new Form("form_pasien", "", "Pasien");
		$form	->addElement("", $id)
				->addElement("", $diagnosa_only)
				->addElement("Nama", $nm)
				->addElement("NRM", $nrm)
				->addElement("No Registrasi", $noreg)
				->addElement("Jenis Kelamin", $jk)
				->addElement("Umur", $umur)
				->addElement("Golongan", $gol)
				->addElement("Ruang Asal", $asal)
				->addElement("Alamat", $alamat)
				->addElement($nkelas, $kelas_asal)
				->addElement($name_rl52, $rl52)
				->addElement($nama_dokumen_rm, $dokumen_rm)
				->addElement("Kunjungan", $kunjungan)
				->addElement("Cara Bayar", $bayar)
                ->addElement("Keterangan", $titipan)
				->addElement("Total Tagihan", $total_grup)
				->addElement("Plafon", $plafon)
				->addElement("Sisa Dana", $sisa)
				->addElement("Hak Kelas", $kbpjs)
				->addElement("Naik Kelas", $naik)
				->addElement("", $warning_plafon)
				->addElement("", $stop_plafon)
                ->addElement("", $activate_stop_plafon)
                ->addElement("", $autoload_plafon)
                ->addElement($plebitis_name,$plebitis)
				->addElement($cauti_name,$cauti)
				->addElement($op_state_name, $op_state)
				->addElement($op_room_name, $op_room)
				->addElement("Tindak Lanjut",$status_pasien)
				->addElement("", $tanggal_lahir);

		if (getSettings($this->db, "smis-rs-laporan-rl31-enable_field_spesialisasi-" . $this->polislug, "0") == "1") {
			$option = new OptionBuilder();
			$option->addSingle("Penyakit Dalam");
			$option->addSingle("Kesehatan Anak");
			$option->addSingle("Obstetri");
			$option->addSingle("Ginekologi");
			$option->addSingle("Bedah");
			$option->addSingle("Bedah Orthopedi");
			$option->addSingle("Bedah Saraf");
			$option->addSingle("Luka Bakar");
			$option->addSingle("S a r a f");
			$option->addSingle("J i w a");
			$option->addSingle("Psikologi");
			$option->addSingle("Penatalaksana Pnyguna. NAPZA");
			$option->addSingle("T H T");
			$option->addSingle("M a t a");
			$option->addSingle("Kulit & Kelamin");
			$option->addSingle("Kardiologi");
			$option->addSingle("Paru-paru");
			$option->addSingle("Geriatri");
			$option->addSingle("Radioterapi");
			$option->addSingle("Kedokteran Nuklir");
			$option->addSingle("K u s t a");
			$option->addSingle("Rehabilitasi Medik");
			$option->addSingle("Isolasi");
			$option->addSingle("I C U");
			$option->addSingle("I C C U");
			$option->addSingle("NICU / PICU");
			$option->addSingle("Umum");
			$option->addSingle("Gigi & Mulut");
			$option->addSingle("Pelayanan Rawat Darurat");
			$option->addSingle("Perinatologi");
			$option->addSingle("Urologi");
			$option->addSingle("Bedah Plastik");

			$spesialisasi_select = new Select("spesialisasi", "spesialisasi", $option->getContent());
			$form->addElement("Jenis Spesialisasi", $spesialisasi_select);

			if (getSettings($this->db, "smis-rs-laporan-rl31-enable_field_spesialisasi2-" . $this->polislug, "0") == "1") {
				$option = new OptionBuilder();
				$option->add("", "", "1");
				$option->addSingle("ICU (Ext.)");
				$option->addSingle("Isolasi (Ext.)");
				$option->addSingle("NICU (Ext.)");
				$option->addSingle("PICU (Ext.)");
				
				$spesialisasi2_select = new Select("spesialisasi2", "spesialisasi2", $option->getContent());
				$form->addElement("Ext. Spesialisasi", $spesialisasi2_select);
			}
		} else {
			$spesialisasi_hidden = new Hidden("spesialisasi", "spesialisasi", "");
			$form->addElement("", $spesialisasi_hidden);
			$spesialisasi2_hidden = new Hidden("spesialisasi2", "spesialisasi2", "");
			$form->addElement("", $spesialisasi2_hidden);
		}
		
		$request = new Button("request_plafon_change","request_plafon_change","Permintaan Peninjauan Plafon");
		$request ->setClass("btn-danger");
		$request ->setIcon("fa fa-money");
		$request ->setIsButton(Button::$ICONIC_TEXT);
		$request ->setAction("layanan.request_tinjau_plafon()");
		
		echo "<div style='display:none;' id='peringatan_bpjs' class='alert alert-block alert-danger'>
			<h4>Peringatan Plafon Pasien</h4>
			<ul>
				<li>Warning ini muncul dikarenakan Total Tagihan hampir / sudah mencapai 100% dari total 
                    Plafon Pasien atau Plafon Pasien belum ter - isi</li>
				<li>Isi Plafon Pasien Terlebih dahulu pada Menu Plafon, 
                    lalu cari RM Pasien, Masukkan Kode INA-CBG's beserta Deskripsinya, 
                    lalu masukkan Plafon Pasien dan Kunci Rencana. Selesai</li>
				<li>Cek kembali perawatan pasien, pastikan sesuai rencana dan diagnosa pasien</li>
                <li>Konsulkan mengenai diagnosa pasien kepada Dokter DPJP</li>
				<li>Jika diketahui diagnosa pasien berubah, segera lakukan update diagnosa 
                    dan kode INA-CBG's beserta plafon di SIMRS</li>
                <li>Apabila tidak dapat menambahkan tindakan, pemeriksaan penunjang, 
					ataupun keperluan farmasi harap menghubungi Tim SIMRS</li>
				<li>Apabila dibutuhkan peninjauan ulang plafon oleh team SIMRS, bisa tekan tombol 'Permintaan Peninjauan Plafon' </li>
			<ul>
			".$request->getHtml()."
		</div>";
        
        echo "<div style='display:none;' id='peringatan_titipan' class='alert alert-block alert-warning'>
			<h3>Pasien Titipan</h3>
			<ul>
				<li>Pasien Titipan kelasnya haruslah di set supaya mengikuti 
                    kelas ruangan titipan atau sesuai kebijakan </li>
			<ul>
		</div>";
		
		$hiddenslug = new Hidden("layanan_hidden_polislug","",$this->polislug);
		$hiddenname = new Hidden("layanan_hidden_poliname","",$this->poliname);
		$hidentitip = new Hidden("layanan_hidden_titipan","",$this->is_titipan_enabled);
		$hidenlonce = new Hidden("layanan_hidden_load_once","",getSettings($this->db,"smis-rs-layanan-load-once-".$this->polislug,"1"));
		echo $hiddenname    ->getHtml();
		echo $hiddenslug    ->getHtml();
        echo $hidentitip    ->getHtml();		
		echo $hidenlonce    ->getHtml();		
		echo $form          ->getHtml();
		echo $modal_pasien  ->getHtml();
		echo "</br><div id='table_content'></div>";
		echo "<div class='hide' id='css_bpjs_lock'></div>";
		echo addJS ("framework/smis/js/table_action.js");
        echo addJS ("rawat/resource/js/reload_bpjs.js",false);
		echo addJS ("rawat/resource/js/rawat.js",false);
		echo addJS ("rawat/resource/js/layanan.js",false);
		echo addCSS("rawat/resource/css/layanan.css",false);
		echo addJS ("rawat/resource/js/layananready.js",false);
	}	

	public function cssPreLoad() {
		echo addCSS("rawat/resource/css/antrian.css", false);
	}
}
?>