<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'rawat/class/responder/GiziResponder.php';

class GiziTemplate extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $simple;
	private $non_chooser;
	public function __construct($db,$polislug,$poliname) {
		parent::__construct ();
		$this->polislug    = $polislug;
		$this->poliname    = $poliname;
		$this->simple      = getSettings($db,"smis-rs-use-gizi-simple-".$polislug,"0")=="1";
		$this->non_chooser = getSettings($db,"smis-rs-use-gizi-non-chooser-".$polislug,"0")=="1";
		$this->db          = $db;
		$header            = null;
		if($this->simple){
			$header        = array ('Pasien',"NRM","No Reg","Menu","Diet",'PK',"Bed");
		}else{
			$header        = array ('Pasien',"NRM","No Reg","Pagi","Siang",'Malam',"Bed");
		}
		$this->uitable = new Table($header,"Gizi " . $poliname,NULL,true );
		$this->uitable ->setName("gizi" )
                       ->setDelButtonEnable(false )
                       ->setPrintButtonEnable(false )
                       ->setAddButtonEnable(false );
	}
    
	public function command($command) {
		$this->dbtable = new DBTable($this->db,'smis_rwt_gizi_'.$this->polislug);
		$column        = array ("nama_pasien","noreg_pasien","nrm_pasien","menu_pagi","diet_pagi","pk_pagi","menu_siang","diet_siang","pk_siang","menu_malam","diet_malam","pk_malam");
		$this->dbtable ->setPreferredView(true,"smis_rwt_vgizi_".$this->polislug,$column)
                       ->setUseWhereforView(true )
                       ->setViewForSelect(true,false );		
		$adapter       = new SimpleAdapter ();
		$adapter       ->add("Pasien","nama_pasien" )
                       ->add("NRM","nrm_pasien","digit6" )
                       ->add("No Reg","noreg_pasien","digit6" )
                       ->add("noreg","noreg_pasien" )
                       ->add("Bed","bed" )
                       ->add("Pagi","menu_pagi" )
                       ->add("Siang","menu_siang" )
                       ->add("Malam","menu_malam" )
                       ->add("Menu","menu_pagi" )
                       ->add("Diet","diet_pagi" )
                       ->add("PK","pk_pagi" );
		$this->dbres   = new GiziResponder($this->dbtable,$this->uitable,$adapter,$this->polislug);
		$this->dbres   ->setSimple($this->simple);
		$data          = $this->dbres->command($_POST ['command'] );
		echo json_encode($data );
	}
    
	public function superCommand($scommand){
        $resp     = null;	
		if(startsWith($scommand,"diet")){
            require_once "rawat/function/diet_responder.php";
            $resp = diet_responder($this->db,$scommand);
        }else if(startsWith($scommand,"menu")){
            require_once "rawat/function/menu_responder.php";
            $resp = menu_responder($this->db,$scommand);
        }
		$super = new SuperCommand ();
		$super ->addResponder($scommand,$resp);
		$init  = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		$this->uitable ->addModal("id","hidden","","")
                       ->addModal("nama_pasien","text","Nama Pasien","",'n',null,true)
                       ->addModal("nrm_pasien","hidden","","")
                       ->addModal("noreg_pasien","hidden","","");
		if($this->simple){
			$this->uitable  ->addModal("menu_pagi",($this->non_chooser?"text":"chooser-gizi-menu_pagi-Menu"),"Menu","")
                            ->addModal("diet_pagi",($this->non_chooser?"text":"chooser-gizi-diet_pagi-Diet"),"Diet","")
                            ->addModal("pk_pagi","textarea","Pesanan Khusus","");
		}else{
			$this->uitable  ->addModal("menu_pagi",($this->non_chooser?"text":"chooser-gizi-menu_pagi-Menu Pagi"),"Menu Pagi","")
                            ->addModal("diet_pagi",($this->non_chooser?"text":"chooser-gizi-diet_pagi-Diet Pagi"),"Diet Pagi","")
                            ->addModal("pk_pagi","textarea","Pesanan Khusus Pagi","")
                            ->addModal("menu_siang",($this->non_chooser?"text":"chooser-gizi-menu_siang-Menu Siang"),"Menu Siang","")
                            ->addModal("diet_siang",($this->non_chooser?"text":"chooser-gizi-diet_siang-Diet Siang"),"Diet Siang","")
                            ->addModal("pk_siang","textarea","Pesanan Khusus Siang","")
                            ->addModal("menu_malam",($this->non_chooser?"text":"chooser-gizi-menu_malam-Menu Malam"),"Menu Malam","")
                            ->addModal("diet_malam",($this->non_chooser?"text":"chooser-gizi-diet_malam-Diet Malam"),"Diet Malam","")
                            ->addModal("pk_malam","textarea","Pesanan Khusus Malam","");
		}
		
		$modal    = $this->uitable->getModal ();
		$modal    ->setTitle($this->poliname );
		$poliname = new Hidden("gz_poliname","",$this->poliname);
		$polislug = new Hidden("gz_polislug","",$this->polislug);
		echo $poliname ->getHtml();
		echo $polislug ->getHtml();
        
		echo $this  ->uitable->getHtml ();
		echo $modal ->getHtml();
		echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS  ("framework/smis/js/table_action.js");
		echo addJS  ("rawat/resource/js/gizi.js",false);
		echo addCSS ("framework/bootstrap/css/datepicker.css");
	}
}
?>