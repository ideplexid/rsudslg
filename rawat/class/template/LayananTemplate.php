<?php 

require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("rawat/class/responder/RawatResponder.php");
class LayananTemplate extends ModulTemplate{
    /*@var Table*/
	protected $uitable;
	/*@var DBResponder*/
	protected $dbres;
    /*@var MySQLTable*/
	protected $dbtable;
    /*@var string*/
	protected $titipan;
    /*@var string*/
	protected $poliname;
	/*@var string*/
	protected $polislug;
    /*@var Database*/
	protected $db;
    /*@var string*/
	protected $nama_pasien;
	/*@var string*/
	protected $nrm;
	/*@var string*/
	protected $noreg;
    /*@var string*/
	protected $carabayar;
    
    public function __construct(Database $db,$polislug="",$poliname="",$nama_pasien="", $nrm_pasien="", $noreg_pasien="",$carabayar="",$titipan=""){
        $this->db             = $db;
        $this->polislug       = $polislug;
		$this->poliname       = $poliname;
		$this->carabayar      = $carabayar;
		$this->nama_pasien    = $nama_pasien;
		$this->nrm            = $nrm_pasien;
		$this->noreg          = $noreg_pasien;
        $this->setTitipan($titipan);
    }
    
    public function setTitipan($titipan){
        $this->titipan=strtolower($titipan);
        return $this;
    }
    
    public function getUiTableTitle($name){
        return $name.($this->titipan!=""?" - ".ArrayAdapter::format("unslug",$this->titipan)." [ <i class='fa fa-link'></i> ] ":" ".$this->poliname);
    }
    
    public function getDBTable($table,$jenis,$adapter){
        require_once "rawat/class/dbtable/TindakanDBTable.php";
        $dbtable = new TindakanDBTable($this->db, $table.$this->polislug);
        //$dbtable ->setAutoSynch(true);
        $dbtable ->setAdapter($adapter);
        $dbtable ->setEntity($this->polislug);
        $dbtable ->setJenis($jenis);

        //$dbtable = new DBTable($this->db, $table.$this->polislug);
		if($this->titipan!=""){
            $tname = $table.$this->titipan;
            require_once "rawat/class/dbtable/ServiceDBTable.php";
            $dbtable = new ServiceDBTable($tname,$dbtable->getColumn(),"operate_table",$this->titipan);
        }
        $dbtable->addCustomKriteria("noreg_pasien", " ='".$this->noreg."'");
        $this->dbtable = $dbtable;
        return $dbtable;
    }
    
	public static function getKelasRuanganPasien($db, $setup, $polislug, $noreg_pasien ){
		$set=getSettings($db, $setup.$polislug,"0");
		if($set=="1"){
			$dbtable=new DBTable($db, "smis_rwt_antrian_".$polislug);
			$one=$dbtable->select(array("no_register"=>$noreg_pasien));
			return $one->kelas;
		}else if($set=="0"){
			return getSettings($db, "smis-rs-kelas-" . $polislug, "");
		}else if($set=="-1"){
			return "";
		}else{
			return $set;
		}
	}
    
    public static function getProvitShareByService(Database $db, DBResponder $dbres, $polislug ,$carabayar, $codepv){
        if(getSettings($db,"smis-rs-provit-share-service-".$polislug,"0")=="1"){
            require_once "rawat/function/get_pv_service.php";
            $ps = get_pv_service($db, $polislug ,$carabayar, $codepv);
            $dbres->addColumnFixValue ( "pembagian", $ps );    
        }
    }
}

?>