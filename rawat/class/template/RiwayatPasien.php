<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("rawat/class/responder/RiwayatResponder.php");
require_once ("smis-base/smis-include-service-consumer.php");


 class RiwayatPasien extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$header=array ("No.",'Masuk',"Keluar","Status","Asal","Kunjungan","No. Reg",'NRM','Kelamin','Pasien',"Umur","Cara Bayar","Cara Keluar" );
		require_once "rawat/class/table/RiwayatTable.php";
		$uitable = new RiwayatTable( $header, "", NULL, true );
		$uitable->setPolislug($polislug);
		$uitable->setDelButtonEnable(false);
		$uitable->setEditButtonEnable(false);
		$uitable->setReloadButtonEnable(false);
		$uitable->setAddButtonEnable(false);
		
		$uitable->setName ( "riwayat" );
		
		$preview=new Button("","","Preview");
		$preview->setIcon("fa fa-eye");
		$preview->setClass("btn btn-info");
		$preview->setIsButton(Button::$ICONIC_TEXT);
		$uitable->addContentButton("print_preview",$preview);

		$reintegrated=new Button("","","");
		$reintegrated->setIcon("fa fa-recycle");
		$reintegrated->setClass("btn btn-danger");
		$reintegrated->setIsButton(Button::$ICONIC_TEXT);
		$uitable->addContentButton("reintegrated",$reintegrated);
		$this->uitable=$uitable;
		
	}
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_antrian_' . $this->polislug );
		$this->dbtable->setShowAll(true);
		$this->dbtable->addCustomKriteria(NULL,"waktu>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"waktu<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("cara_keluar"," LIKE '".$_POST['cara_keluar']."'");
		if(isset($_POST['state']) && $_POST['state']!=""){
			if($_POST['state']=="Stand Alone"){
				$this->dbtable->addCustomKriteria("no_register"," < 0");
			}else if($_POST['state']=="Integrated"){
				$this->dbtable->addCustomKriteria("no_register"," > 0");
			}
		}
		if(isset($_POST['nrm_pasien']) &&  $_POST['nrm_pasien']!=""){
			$this->dbtable->addCustomKriteria("nrm_pasien"," ='".$_POST['nrm_pasien']."' ");
		}
		if(isset($_POST['no_register']) &&  $_POST['no_register']!=""){
			$this->dbtable->addCustomKriteria("no_register"," ='".$_POST['no_register']."' ");
		}
		
		$this->dbtable->setOrder ( " waktu ASC " );
		$adapter=new SimpleAdapter();
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Masuk", "waktu","date d M Y H:i");
		$adapter->add("Asal", "asal","unslug");
		$adapter->add("Status", "selesai","trivial_selesai_0_Aktif_Non Aktif");
		$adapter->add("Kunjungan", "kunjungan");
		$adapter->add("No. Reg", "no_register");
		$adapter->add("NRM", "nrm_pasien","only-digit8");
		$adapter->add("Pasien", "nama_pasien");
		$adapter->add("Kelamin", "jk","trivial_0_L_P");
		$adapter->add("Cara Bayar", "carabayar");
		$adapter->add("Cara Keluar", "cara_keluar");
		$adapter->add("Keluar", "waktu_keluar","date d M Y H:i");
		$adapter->add("Umur", "umur");
		$this->dbres = new RiwayatResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}
	
	/* when it's star build */
	public function phpPreload() {
		
		loadLibrary("smis-libs-function-medical");
		$keluar=medical_carapulang();
		$keluar[]=array("name"=>"","value"=>"%","default"=>"1");
		$state=new OptionBuilder();
		$state->add("","","1");
		$state->add("Stand Alone","Stand Alone");
		$state->add("Integrated","Integrated");
		
		
		$this->uitable->addModal("dari", "datetime", "Dari", "");
		$this->uitable->addModal("sampai", "datetime", "Sampai", "");
		$this->uitable->setFooterVisible(false);
		$this->uitable->addModal("state", "select", "State", $state->getContent());
		$this->uitable->addModal("cara_keluar", "select", "Keluar", $keluar);
		$this->uitable->addModal("nrm_pasien", "text", "NRM Pasien", "");
		$this->uitable->addModal("no_register", "text", "No. Register", "");
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btn=new Button("", "", "");
		$btn->setAction("riwayat.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "");
		$btn->setAction("riwayat.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC);
		$btg->addButton($btn);
		$form->addElement("", $btg);
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var riwayat;		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydatetime').datetimepicker({minuteStep:1});
			var column=new Array('id',"no_register","selesai","nama_pasien","nrm_pasien",'waktu_keluar','cara_keluar','keterangan_keluar',"asal","kelas","jk","carabayar");
			riwayat=new TableAction("riwayat","<?php echo $this->polislug ?>","riwayat",column);
			riwayat.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			riwayat.getViewData=function(){
				var save_data=this.getRegulerData();
				save_data['command']="list";
				save_data['dari']=$("#riwayat_dari").val();
				save_data['sampai']=$("#riwayat_sampai").val();
				save_data['cara_keluar']=$("#riwayat_cara_keluar").val();
				save_data['state']=$("#riwayat_state").val();
				save_data['no_register']=$("#riwayat_no_register").val();
				save_data['nrm_pasien']=$("#riwayat_nrm_pasien").val();
				return save_data;
			};	

			riwayat.reintegrated=function(id){
				var a=this.getRegulerData();
				a['command']="reintegrated";
				a['id']=id;
				$.post("",a,function(res){
					var json=getContent(res);
					riwayat.view();
				});
			};
			
		});
		</script>
<?php
	}
}?>
