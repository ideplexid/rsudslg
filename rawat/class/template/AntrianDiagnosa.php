<?php
require_once 'rawat/class/adapter/AntrianAdapter.php';
require_once 'rawat/class/responder/AntrianResponder.php';
require_once ("smis-framework/smis/template/ModulTemplate.php");

 class AntrianDiagnosaTemplate extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;		
		$layani = new Button ( "", "", "" );
		$layani->setClass ( "btn-success" );
		$layani->setIcon ( "fa fa-sign-in" );
		$layani->setIsButton ( Button::$ICONIC );
		
		$header=array ('Waktu',"Nomor","Asal","Kunjungan","No. Register",'NRM','Jenis Kelamin','Pasien',"Umur","Cara Bayar" );
		$this->mode=getSettings($this->db, "smis-rs-antrian-mode-".$this->polislug, "Integrated");
		$this->uitable = new Table ( $header, "Diagnosa " . $poliname, NULL, true );
		$this->uitable->setName ( "antriandiagnosa" );
		$this->uitable->addContentButton ( "layani", $layani );
		$this->uitable->setDelButtonEnable ( false );
		$this->uitable->setEditButtonEnable ( false );
		$this->uitable->setReloadButtonEnable( false );
		$this->uitable->setPrintButtonEnable(false);
		$this->uitable->setHelpButtonEnabled(true, $this->polislug,"antrian",$this->polislug,$this->poliname,"rawat");
		if($this->mode=="Integrated") $this->uitable->setAddButtonEnable ( false );
		
		if(getSettings($db, "smis-rs-skb-".$this->polislug, "0")=="1"){
			$btn=new Button("", "", "SKB");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setIcon("fa fa-heartbeat");
			$btn->setClass(" btn-primary");
			$this->uitable->addContentButton("skb", $btn);
		}
	}
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_antrian_' . $this->polislug );
		$this->dbtable->setOrder ( " waktu ASC, nomor ASC " );
		$this->dbtable->addCustomKriteria ( "selesai", "='1'" );
		$this->dbres = new AntrianResponder ( $this->dbtable, $this->uitable, new AntrianAdapter ("") , $this->poliname,$this->polislug,$this->mode);
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}
	
	/* when it's star build */
	public function phpPreload() {
		/* This is Modal Form and used for add and edit the table */
		$keluar=new OptionBuilder();
		$keluar->add("Tidak Datang");
		$keluar->add("Dipulangkan Hidup");
		$keluar->add("Dipulangkan Mati <=48 Jam");
		$keluar->add("Dipulangkan Mati >48 Jam");
		$keluar->add("Pulang Paksa");
		$keluar->add("Kabur");
		$keluar->add("Rawat Inap");
		$keluar->add("Pindah Kamar");
		$keluar->add("Dirujuk Ke Poli Lain","Rujuk Poli Lain");
		$keluar->add("Dirujuk Ke RS Lain","Rujuk RS Lain");
		$keluar->add("Dikembalikan ke Perujuk","Dikembalikan ke Perujuk");
		
		
		$this->uitable->addModal("id", "hidden", "", "");
		if($this->mode=="Stand Alone"){
			/*untuk memanipulasi data ruang*/	
			require_once 'smis-base/smis-include-service-consumer.php';
			$urjip=new ServiceConsumer($this->db, "get_urjip",array());
			$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
            $urjip->setCached(true,"get_urjip");
			$urjip->execute();
			$content=$urjip->getContent();
			$ruangan=array();
			foreach ($content as $autonomous=>$ruang){
				foreach($ruang as $nama_ruang=>$jip){
					$option=array();
					$option['value']=$nama_ruang;
					$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
					if($nama_ruang==$this->polislug) $option['default']="1";
					$ruangan[]=$option;
				}
			}
			$option=array();
			$option['value']="Pendaftaran";
			$option['name']="PENDAFTARAN";
			$ruangan[]=$option;
			
			/*untuk memanipulasi data kelas*/
			require_once 'smis-base/smis-include-service-consumer.php';
			$service = new ServiceConsumer ( $this->db, "get_kelas" );
            $service->setCached(true,"get_kelas");
			$service->execute ();
			$kelas = $service->getContent ();
			$option_kelas = new OptionBuilder ();
			$cur_kelas= getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
			foreach ( $kelas as $k ) {
				$nama = $k ['nama'];
				$slug = $k ['slug'];
				$option_kelas->add ( $nama, $slug, $slug == $cur_kelas ? "1" : "0" );
			}
			
			/*untuk memanipulasi data bahwa selesai*/
			$selesai=new OptionBuilder();
			$selesai->add("Ya","1","0");
			$selesai->add("Belum","0","1");
			
			$jk=new OptionBuilder();
			$jk->add("Laki-Laki","0","1");
			$jk->add("Perempuan","1","0");
			
			/*untuk memanipulasi data cara bayar*/
			$service = new ServiceConsumer ( $this->db, "get_carabayar" );
			$service->execute ();
			$dataasuransi= $service->getContent ();			
			$adapter=new SelectAdapter("nama", "slug");
			$jenis_pembayaran=$adapter->getContent($dataasuransi);
			
			$this->uitable->addModal("no_register", "hidden", "", "","");
			$this->uitable->addModal("nrm_pasien", "text", "NRM Pasien", "","n");
			$this->uitable->addModal("nama_pasien", "text", "Nama Pasien", "","n");
			$this->uitable->addModal("jk", "select", "Jenis Kelamin", $jk->getContent(),"n");
			$this->uitable->addModal("asal", "select", "Asal", $ruangan,$this->mode=="Integrated"?"y":"n");
			$this->uitable->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran,"n");
			$this->uitable->addModal("kelas", "select", "Kelas", $option_kelas->getContent());
			$this->uitable->addModal("selesai", "select", "Selesai", $selesai->getContent());
			
		}else{
			$this->uitable->addModal("no_register", "hidden", "", "");
			$this->uitable->addModal("nrm_pasien", "hidden", "", "");
			$this->uitable->addModal("nama_pasien", "hidden", "", "");
			$this->uitable->addModal("selesai", "hidden", "", "1");
		}
		$this->uitable->addModal("waktu_keluar", "date", "Tanggal", date("Y-m-d"),"n");
		$this->uitable->addModal("cara_keluar", "select", "Keluar", $keluar->getContent());
		$this->uitable->addModal("keterangan_keluar", "textarea", "Keterangan", "");
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( $this->poliname );
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var antriandiagnosa;		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array('id',"no_register","selesai","nama_pasien","nrm_pasien",'waktu_keluar','cara_keluar','keterangan_keluar',"asal","kelas","jk","carabayar");
			antriandiagnosa=new TableAction("antriandiagnosa","<?php echo $this->polislug ?>","antriandiagnosa",column);
			antriandiagnosa.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			antriandiagnosa.getSaveData=function(){
				var save_data=this.getRegulerData();
				save_data['command']="save";
				for(var i=0;i<this.column.length;i++){
					var name=this.column[i];
					save_data[name]=$("#"+this.prefix+"_"+name).val();
				}
				if("Integrated"=="<?php echo $this->mode; ?>"){
					save_data['selesai']=1;
				}
				return save_data;
			};
			antriandiagnosa.view();
			
			antriandiagnosa.layani=function(id){
				var data={
							page:"<?php echo $this->polislug ?>",
							action:"layanan",
							id_antrian:id,
							mode_layanan:"diagnosa_only",
							prototype_name:"<?php echo $_POST['prototype_name']; ?>",
							prototype_slug:"<?php echo $_POST['prototype_slug']; ?>",
							prototype_implement:"<?php echo $_POST['prototype_implement']; ?>"
						};
				LoadSmisPage(data);
			};

			antriandiagnosa.skb=function(id){
				var data=this.getRegulerData();
				data['command']="skb";
				data['id']=id;
				$.post("",data,function(res){
					var json=getContent(res);
					smis_print(json);
				});
			};
			
		});
		</script>
<?php
	}
}?>
