<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once "smis-base/smis-include-service-consumer.php";
require_once "rawat/function/get_data_pasien.php";
require_once "rawat/function/get_bed_pasien.php";
class MakananGiziLayanan extends LayananTemplate {
	public function __construct($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);
	}
	public function initialize() {
		$pasien                                 = get_data_pasien($this->db,$this->polislug,$this->noreg);
        $data                                   = $_POST;
        $data['polislug']                       = $this->polislug;
        $data['page']                           = "rawat";
        $data['action']                         = "makanan_gizi_layanan";
        $data['protoslug']                      = $_POST['protoslug'];
        $data['protoname']                      = $_POST['protoname'];
        $data['protoimplement']                 = $_POST['protoimplement'];
        $data['noreg_pasien']                   = $this->noreg;
        $data['nama_pasien']                    = $this->nama_pasien;
        $data['nrm_pasien']                     = $this->nrm;                
        $data['bed']                            = get_bed_pasien($this->db,$this->polislug,$this->noreg);
        $data['umur']                           = $pasien!=null?$pasien['umur']:"";
        $data['ibu_kandung']                    = $pasien!=null?$pasien['ibu_kandung']:"";
        $data['setting_pesan_penunggu']         = getSettings($this->db, "smis_gz_pesanan_penunggu_pasien-".$this->polislug,"0");
        $data['setting_auto_pesan_penunggu']    = getSettings($this->db, "smis_gz_auto_pesanan_penunggu_pasien-".$this->polislug,"0");
        $data['carabayar']                      = $this->carabayar;
		        
        $service = new ServiceConsumer ( $this->db, "gizi_register", $data, "gizi" );
		$service ->execute ();
		echo $service->getContent ();
	}
}
?>