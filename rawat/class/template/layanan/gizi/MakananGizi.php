<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once 'rawat/class/responder/GiziResponder.php';

class MakananGizi extends LayananTemplate {
    private $simple;
	private $non_chooser;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar);
		$this->simple       = getSettings($db, "smis-rs-use-makanan_gizi-simple-".$polislug, "0")=="1";
		$this->non_chooser  = getSettings($db, "smis-rs-use-makanan_gizi-non-chooser-".$polislug, "0")=="1";
	}
    
	public function superCommand($scommand){
        $resp     = null;	
		if(startsWith($scommand,"diet")){
            require_once "rawat/function/diet_responder.php";
            $resp = diet_responder($this->db,$scommand);
        }else if(startsWith($scommand,"menu")){
            require_once "rawat/function/menu_responder.php";
            $resp = menu_responder($this->db,$scommand);
        }
		$super = new SuperCommand ();
		$super ->addResponder($scommand,$resp);
		$init  = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	public function command($command) {
		$this->dbtable = new DBTable($this->db, 'smis_rwt_gizi_' . $this->polislug);
		$column = array ("nama_pasien","noreg_pasien","nrm_pasien","menu_pagi","diet_pagi","pk_pagi","menu_siang","diet_siang","pk_siang","menu_malam","diet_malam","pk_malam");
		$this->dbtable->setPreferredView(true, "smis_rwt_vgizi_" . $this->polislug, $column);
		$this->dbtable->setUseWhereforView(true);
		$this->dbtable->setViewForSelect(true, false);
		
		$adapter = new SimpleAdapter ();
		$this->dbres = new GiziResponder($this->dbtable, new Table(array()), $adapter, $this->polislug);
		$this->dbres->setSimple($this->simple);
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}

	public function phpPreload() {	
        $gz_id               = "";
        $idx['noreg_pasien'] = $this->noreg;
        $this->dbtable       = new DBTable($this->db,'smis_rwt_gizi_' . $this->polislug);
        $gz                  = $this->dbtable->select($idx);
        if($gz != null){
            $gz_id           = $gz->id;
        }
        $this->uitable = new Table("","",NULL,false);
        $this->uitable ->setName("makanan_gizi")
                       ->setFooterVisible(false)
                       ->addModal("id","hidden","",$gz_id)
                       ->addModal("nama_pasien","hidden","",$this->nama_pasien)
                       ->addModal("noreg_pasien","hidden","",$this->noreg)
                       ->addModal("nrm_pasien","hidden","",$this->nrm);        
        if($this->simple){
			$this->uitable  ->addModal("menu_pagi", ($this->non_chooser?"text":"chooser-makanan_gizi-menu_pagi-Menu"), "Menu", "")
                            ->addModal("diet_pagi", ($this->non_chooser?"text":"chooser-makanan_gizi-diet_pagi-Diet"), "Diet", "")
                            ->addModal("pk_pagi", "textarea", "Pesanan Khusus", "");
		}else{
			$this->uitable  ->addModal("menu_pagi", ($this->non_chooser?"text":"chooser-makanan_gizi-menu_pagi-Menu Pagi"), "Menu Pagi", "")
                            ->addModal("diet_pagi", ($this->non_chooser?"text":"chooser-makanan_gizi-diet_pagi-Diet Pagi"), "Diet Pagi", "")
                            ->addModal("pk_pagi", "textarea", "Pesanan Khusus Pagi", "")
                            ->addModal("menu_siang", ($this->non_chooser?"text":"chooser-makanan_gizi-menu_siang-Menu Siang"), "Menu Siang", "")
                            ->addModal("diet_siang", ($this->non_chooser?"text":"chooser-makanan_gizi-diet_siang-Diet Siang"), "Diet Siang", "")
                            ->addModal("pk_siang", "textarea", "Pesanan Khusus Siang", "")
                            ->addModal("menu_malam", ($this->non_chooser?"text":"chooser-makanan_gizi-menu_malam-Menu Malam"), "Menu Malam", "")
                            ->addModal("diet_malam", ($this->non_chooser?"text":"chooser-makanan_gizi-diet_malam-Diet Malam"), "Diet Malam", "")
                            ->addModal("pk_malam", "textarea", "Pesanan Khusus Malam", "");
		}
        
		$modal = $this->uitable->getModal ();
		$form  = $modal->getForm()->setTitle("Menu Makanan Gizi");	
		$btn   = new Button("","","Simpan");
		$btn    ->setAction("makanan_gizi.save()")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setClass("btn-primary")
                ->setIcon("fa fa-save");
                
		$form   ->addElement("",$btn);
		
        
		$carabayar = new Hidden("makanan_gizi_carabayar","",$this->carabayar);
		$poliname  = new Hidden("makanan_gizi_poliname","",$this->poliname);
		$polislug  = new Hidden("makanan_gizi_polislug","",$this->polislug);
		
		echo $carabayar ->getHtml();
		echo $poliname  ->getHtml();
		echo $polislug  ->getHtml();
		echo $form      ->getHtml ();
		echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addJS  ("framework/smis/js/table_action.js");
		echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
		echo addJS  ("rawat/resource/js/makanan_gizi.js",false);
	}
}

?>