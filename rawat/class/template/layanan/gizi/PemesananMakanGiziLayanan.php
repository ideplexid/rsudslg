<?php
require_once ("rawat/class/template/LayananTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("rawat/class/template/LayananTemplate.php");

class PemesananMakanGiziLayanan extends LayananTemplate {
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien, $carabayar, $titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien, $carabayar, $titipan);
	}
    
	public function initialize() {
        $data                    = $_POST;
        $data ['polislug']       = $this->polislug;
        $data ['bed']			 = "";
        $bed_row 				 = $this->db->get_row("
        	SELECT *
			FROM smis_rwt_bed_kamar_" . $this->polislug . "
			WHERE prop = '' AND noreg_pasien = '" . $this->noreg . "'
			ORDER BY id DESC
			LIMIT 0, 1
        ");
        if ($bed_row != null)
        	$data ['bed']		 = $bed_row->nama;
        $data ['jenis_pasien']   = "";
        $carabayar_row 			 = $this->db->get_row("
        	SELECT *
			FROM smis_rwt_antrian_" . $this->polislug . "
			WHERE prop = '' AND no_register = '" . $this->noreg . "'
			ORDER BY id DESC
			LIMIT 0, 1
        ");
        if ($carabayar_row != null)
        	$data ['jenis_pasien']   = $carabayar_row->carabayar;
        $data ['kelas'] 		 = getSettings( $this->db, "smis-rs-kelas-" . $this->polislug );
		$data ['noreg_pasien']   = $this->noreg;
		$data ['nama_pasien']    = $this->nama_pasien;
		$data ['nrm_pasien']     = $this->nrm;
		$data ['action']         = "makanan_gizi";
		$data ['page']           = "rawat";
		$data ['protoslug']      = $_POST['protoslug'];
        $data ['protoname']      = $_POST['protoname'];
        $data ['protoimplement'] = $_POST['protoimplement'];
		
		$service = new ServiceConsumer ( $this->db, "reg_pemesanan_makanan", $data, "gizi" );
		$service->execute ();
		echo $service->getContent ();
	}
}
?>
