<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

class TindakanDokter extends LayananTemplate {
	private $muitable;
	private $number;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$header			= array('Tanggal','Tindakan','Dokter',"Biaya Dokter","Perawat","Biaya perawat");
		$this->uitable 	= new Table($header, $this->getUiTableTitle("Tindakan Dokter"), NULL, true);
		$this->uitable	->setName("tindakan_dokter");
		$array_mui		= array("Nama",'DTD','Kode');
		$this->muitable = new Table($array_mui);
		$this->muitable ->setModel(Table::$BOTH)
                        ->setName("mr_icd_tindakan")
                        ->setDelButtonEnable(false)
                        ->setPrintButtonEnable(false)
                        ->setReloadButtonEnable(false);
		$this->number	= array("I"=>"","II"=>"_dua","III"=>"_tiga","IV"=>"_empat","V"=>"_lima","VI"=>"_enam","VII"=>"_tujuh","VIII"=>"_delapan","IX"=>"_sembilan","X"=>"_sepuluh");
	}
	
	
	private function initDokterSuperCommand(SuperCommand $supercommand){
		$strict_dr	  = getSettings($this->db, "smis-rs-dokter-tindakan-dokter-".$this->polislug, "0");		
		$dkadapter 	  = new SimpleAdapter ();
		$dkadapter	  ->add("Jabatan","nama_jabatan")
				  	  ->add("Nama","nama")
				  	  ->add("NIP","nip");
		$header		  = array ('Nama','Jabatan',"NIP");
		$dktable 	  = new Table($header);
		$dktable	  ->setName("dokter_tindakan_dokter")
					  ->setModel(Table::$SELECT);
		$dokter		  = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter");
		$dokter		  ->setStrict($strict_dr,$this->polislug);
		$supercommand ->addResponder("dokter_tindakan_dokter", $dokter);
	}
	
	private function initTarifSuperCommand(SuperCommand $supercommand){		
		$dktable    = new Table(array ("Nama","Kelas","Pasien","Tarif"), "", NULL, true);
		$dktable    ->setName("tarif_tindakan_dokter")
				    ->setModel(Table::$SELECT);
		$dkadapter  = new SimpleAdapter ();
		$dkadapter  ->add("Nama","nama")
				    ->add("Kelas","kelas","unslug")
				    ->add("Pasien","jenis_pasien","unslug")
				    ->add("Tarif","tarif","money Rp.");
        $data_kelas = getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
        $serv_tarif = "get_tindakan_dokter_rajal";
        if(getSettings($this->db,"smis-rs-tarif-tindakan-dokter-".$this->polislug,"0")=="1"){
            $serv_tarif = "get_tindakan_dokter_ranap";    
        }
		$tarif 		= new ServiceResponder($this->db, $dktable, $dkadapter, $serv_tarif);
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-tdokter-", $this->polislug, $_POST['noreg_pasien']);
			$tarif		->addData("kelas", $data_kelas);
			if(getSettings($this->db, "smis-rs-tdokter-jp-".$this->polislug , "0")=="1"){
				$tarif	->addData("jenis_pasien", $_POST['carabayar']);
			}
		}else{
			$data_kelas = getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
			$tarif		->addData("kelas", $data_kelas);			
		}
		$supercommand	->addResponder("tarif_tindakan_dokter", $tarif);
	}
	
	private function initPerawatSuperCommand(SuperCommand $supercommand, $command){
		$strict	   		= getSettings($this->db, "smis-rs-employee-tindakan-dokter-".$this->polislug, "0");
		$pkadapter 		= new SimpleAdapter ();
		$pkadapter		->add("Jabatan","nama_jabatan")
						->add("Nama","nama")
						->add("NIP","nip");
		$header	 		= array ('Nama','Jabatan',"NIP");
		$pktable 		= new Table($header);
		$pktable 		->setName($command)
				 		->setModel(Table::$SELECT);
		$perawat 		= new EmployeeResponder($this->db, $pktable, $pkadapter, "perawat");
		$perawat		->setStrict($strict,$this->polislug);
		$supercommand	->addResponder($command, $perawat);
	}
	
	private function initICD(SuperCommand $supercommand){
		$madapter 		= new SimpleAdapter ();
		$madapter 		->add("Nama","nama")
				  		->add("Kode","icd")
				  		->add("DTD","dtd")
				  		->add("Grup","grup");
		$mresponder 	= new ServiceResponder($this->db, $this->muitable, $madapter , "get_icdtindakan");
		$supercommand	->addResponder("mr_icd_tindakan", $mresponder);
	}
	
	public function superCommand($super_command) {
		$super = new SuperCommand ();
		switch($super_command){
			case "dokter_tindakan_dokter" 	: $this->initDokterSuperCommand($super); break;
			case "tarif_tindakan_dokter" 	: $this->initTarifSuperCommand($super); break;
			case "mr_icd_tindakan"			: $this->initICD($super); break;
			default							: $this->initPerawatSuperCommand($super,$super_command); break; 
		}
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}		
	}
	
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter ->add("Tindakan","nama_tindakan")
				 ->add("Dokter","nama_dokter")
				 ->add("Biaya Dokter","harga","money Rp.")
				 ->add("Perawat","nama_perawat")
				 ->add("Biaya perawat","harga_perawat","money Rp.");
		if(getSettings($this->db, "smis-rs-tdokter-waktu-".$this->polislug, "0") =="1"){
			$adapter->add("Tanggal","waktu","date d M Y H:i");
		}else{
			$adapter->add("Tanggal","waktu","date d M Y");
        }				
        
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter2 = new TagihanAdapter ($this->db,$this->polislug);
        $adapter2 ->setAdapterName("tindakan_dokter");
        $this->getDBTable("smis_rwt_tindakan_dokter_","tindakan_dokter",$adapter2);
        
		//$this->getDBTable("smis_rwt_tindakan_dokter_");
        require_once "rawat/class/responder/TindakanDokterResponder.php";
        $this->dbres = new TindakanDokterResponder($this->dbtable, $this->uitable, $adapter, $this->polislug);
		if ($this->dbres->is("save")) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-tindakan-dokter");
		}        
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_tindakan_dokter","TDK","tindakan_dokter","waktu","nama_tindakan","harga");
        }        
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}
	
	public function phpPreload() {
		$this->uitable	->addModal("id","hidden","","")
						->addModal("nama_pasien","hidden","", $this->nama_pasien)
						->addModal("noreg_pasien","hidden","", $this->noreg)
						->addModal("nrm_pasien","hidden","", $this->nrm)
						->addModal("waktu","datetime","Waktu", date("Y-m-d H:i"))
						->addModal("nama_tindakan","chooser-tindakan_dokter-tarif_tindakan_dokter-Tarif Tindakan Dokter","Tindakan","" ,"n",null,false,null,true)
						->addModal("nama_dokter","chooser-tindakan_dokter-dokter_tindakan_dokter-Dokter","Dokter","","n",null,false,null,false )
						->addModal("id_dokter","hidden","","","n")
						->addModal("jenis_dokter","hidden","","")
						->addModal("harga","money","Biaya","","n",null,false,null,false,"nama_perawat");
		
		$counter	= getSettings($this->db, "smis-rs-total-perawat-tindakan-dokter-".$this->polislug, "0")*1;
		$numero		= 1;
		foreach($this->number as $rum=>$num){
			if($numero<=$counter){
				$this->uitable->addModal("nama_perawat".$num, "chooser-tindakan_dokter-perawat_tindakan_dokter".$num."-Perawat".$num."-Pilih Perawat ".$rum, "Perawat ".$rum,"");	
			}else{
				$this->uitable->addModal("nama_perawat".$num, "hidden","","","n",null,false,null,false);
			}
			$this->uitable->addModal("id_perawat".$num, "hidden","","");
			$numero++;
		}
		if(getSettings($this->db, "smis-rs-ui-harga-perawat-tindakan-dokter-".$this->polislug, "0")=="1"){
			$this->uitable  ->addModal("harga_perawat","money","Biaya Perawat","","y",null,false,null,false,"jenis_dokter");
		}
		$this->uitable	->addModal("nama_icd","chooser-tindakan_dokter-mr_icd_tindakan-Kode ICD","Nama ICD IX","","y",null,true)
					  	->addModal("kode_icd","text","Kode ICD IX","","y",null,true)
					  	->addModal("kode_dtd","text","Kode DTD","","y",null,true)
                        ->addModal("kode_grup","text","Nama Grup","","y",null,true)
                        ->addModal("jaspel_nilai","hidden","","")
						->addModal("jaspel_persen","hidden","","")
						->addModal("jaspel_nilai_perawat","hidden","","")
                        ->addModal("jaspel_persen_perawat","hidden","","");
		
		$this->muitable	->addModal("id","hidden","","")
						->addModal("nama","text","Nama","")
						->addModal("icd","text","ICD IX","")
						->addModal("grup","text","Grup","")
						->addModal("dtd","text","DTD","")
                        ->addModal("bagi_dokter","hidden","","")
                        ->addModal("id_tindakan","hidden","","");

		$modal 			= $this->uitable->getModal ();
		$modal			->setTitle($this->poliname);
		$modal_icd		= $this->muitable->getModal();
		$modal_icd		->setTitle("Kode ICD IX Tindakan");
		echo $this		->uitable->getHtml ();
		echo $modal		->getHtml ();
		echo $modal_icd ->getHtml();
		echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS  ("framework/smis/js/table_action.js");
		echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addCSS ("framework/bootstrap/css/datepicker.css");
		echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
		echo addJS  ("rawat/resource/js/tindakan_dokter.js" ,false);
		
		/*Parsing ke Javascript*/
		$carabayar 	= new Hidden("tdk_carabayar","",$this->carabayar);
		$poliname	= new Hidden("tdk_poliname","",$this->poliname);
		$polislug	= new Hidden("tdk_polislug","",$this->polislug);
		echo $carabayar ->getHtml();
		echo $poliname  ->getHtml();
		echo $polislug  ->getHtml();
	}
}