<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once 'rawat/class/service_consumer/OksigenService.php';
require_once 'rawat/class/adapter/OksigenManualAdapter.php';
require_once 'rawat/class/responder/OksigenManualResponder.php';

class OksigenManualTemplate extends LayananTemplate {
	private $activate_selection = false;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		if(getSettings($db,"smis-rs-tarif-oksigen_manual-".$this->polislug,"0")=="1"){
			$this->activate_selection = true;
		}
		if($this->activate_selection){
			$this->uitable = new Table ( array ('Waktu',"Range","Pasang","Lepas","Harga Per Jam",'Jumlah Jam','Total'), $this->getUiTableTitle("Oksigen Manual"), NULL, true );
		}else {
			$this->uitable = new Table ( array ('Waktu',"Harga (H)","Liter (L)",'Biaya Lain (B)','Total (H*L+B)'), $this->getUiTableTitle("Oksigen Manual"), NULL, true );			
		}
		$this->uitable->setName ( "oksigen_manual" );
	}

	public function superCommand($super_command) {		
		$super = new SuperCommand ();
		$dktable    = new Table(array ("Durasi","Liter","Tarif"), "", NULL, true);
		$dktable    ->setName("oksigen_manual_list_harga")
                    ->setModel(Table::$SELECT);
		$dkadapter  = new SimpleAdapter ();
		$dkadapter  ->add("Durasi", "durasi")
                    ->add("Liter", "liter")
                    ->add("Tarif", "tarif", "money Rp.");		
		$tarif = new ServiceResponder($this->db, $dktable, $dkadapter, "get_tarif_oksigen");
		$super->addResponder("oksigen_manual_list_harga", $tarif);
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}	
	}
    
	public function command($command) {
		$column = array ('id','nama_pasien','nrm_pasien','noreg_pasien','jumlah_liter',	'biaya_lain');
		require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter2 = new TagihanAdapter ($this->db,$this->polislug);
        $adapter2 ->setAdapterName("oksigen_manual");
        $this->getDBTable("smis_rwt_oksigen_manual_","oksigen_manual",$adapter2);

		//$this->getDBTable("smis_rwt_oksigen_manual_");
        //$this->dbtable->setColumn($column);
		$this->dbres = new OksigenManualResponder ( $this->dbtable, $this->uitable, new OksigenManualAdapter (), $this->polislug );
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_oksigen_manual","O2M","oksigen_manual","waktu","nrm_pasien","harga");
		}
		$data = $this->dbres->command ( $_POST ['command'] );		
        echo json_encode ( $data );
	}

	/* when it's star build */
	public function phpPreload() {
		//$this->uitable->setModal ( $mcolumn, $mtype, $mname, $mvalue, null, $disabled );
		$this->uitable->addModal("id", "hidden", "", "");
		$this->uitable->addModal("nama_pasien", "hidden", "", $this->nama_pasien);
		$this->uitable->addModal("nrm_pasien", "hidden", "", $this->nrm);
		$this->uitable->addModal("noreg_pasien", "hidden", "", $this->noreg);
		if($this->activate_selection){
			$this->uitable->addModal("waktu_pasang", "datetime", "Pasang", "");
			$this->uitable->addModal("waktu_lepas", "datetime", "Lepas", "");
			$this->uitable->addModal("range_harga", "chooser-oksigen_manual-oksigen_manual_list_harga-Pilih Range Harga", "Pilih Range", "","n",null,true);
			//$this->uitable->addModal("jumlah_jam", "text", "Durasi Jam", "","n",null,true);
			$this->uitable->addModal("harga_jam", "money", "Harga/Jam", "","n",null,true);
			$this->uitable->addModal("harga", "hidden", "", "","n",null,true);			
		}else{
			$this->uitable->addModal("jumlah_liter", "text", "Jumlah (Liter)", "","n",null,false,null,true,"biaya_lain");
			$this->uitable->addModal("biaya_lain", "money", "Biaya Manometer", "","y",null,false,null,false,"save");	
		}
		
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Oksigen Manual" );
        $carabayar=new Hidden("omn_carabayar","",$this->carabayar);
		$poliname=new Hidden("omn_poliname","",$this->poliname);
		$polislug=new Hidden("omn_polislug","",$this->polislug);
        echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
        echo addJS ( "rawat/resource/js/oksigen_manual.js",false );
	}
}