<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once "smis-libs-hrd/EmployeeResponder.php";
class Alok extends LayananTemplate {
    private $is_harga_show=true;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$header=array ('Tanggal','Nama',"Harga",'Jumlah' );
		$this->uitable = new Table ( $header, $this->getUiTableTitle("Alat dan Obat"), NULL, true );
		$this->uitable->setName ( "alok" );
        $this->is_harga_show=getSettings($db,"smis-rs-hidden-harga-alok-".$polislug,"1")=="1";
	}
    
    public function setShowHarga($enable){
        $this->is_harga_show=$enable;
        return $this;
    }
    
	public function superCommand($super_command) {
		$dbtable = new DBTable ( $this->db, "smis_rwt_aok_" . $this->polislug );
		$head=array ("Nama","Satuan","Harga","Kategori" );
		$dktable = new Table ( $head, "", NULL, true );
		$dktable->setName ( "tarif_alok" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Satuan", "satuan" );
		$dkadapter->add ( "Harga", "harga", "money Rp." );
		$dkadapter->add ( "Kategori", "kategori" );
		
		$tarif = new RawatResponder ( $dbtable, $dktable, $dkadapter ,$this->polislug);
		
		$super = new SuperCommand ();
		$super->addResponder ( "tarif_alok", $tarif );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	public function command($command) {
        require_once "rawat/class/responder/AlokStandAloneResponder.php";
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter2 = new TagihanAdapter ($this->db,$this->polislug);
        $adapter2 ->setAdapterName("alok");
        $this->getDBTable("smis_rwt_alok_","alok",$adapter2);

        //$this->getDBTable("smis_rwt_alok_");
        $adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
		$adapter->add ( "Nama", "nama" );
		$adapter->add ( "Satuan", "satuan" );
		$adapter->add ( "Jumlah", "jumlah", "number" );
		$adapter->add ( "Harga", "harga", "money Rp." );
		
		$this->dbres = new AlokStandAloneResponder ( $this->dbtable, $this->uitable, $adapter, $this->polislug );
		if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_alok","ALK","alok","tanggal","nama","harga");
        }
        $data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}
	public function phpPreload() {
		//$free=getSettings($db, "smis-rs-free-alok-".$this->polislug,"0")=="1";
		
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm );
		$this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) ,"n");
		$this->uitable->addModal ( "nama", "chooser-alok-tarif_alok-Daftar Alat Obat", "Obat", "","n",null,false,null,true );
		$this->uitable->addModal ( "satuan", "hidden", "", "" );
		$this->uitable->addModal ( "kode", "hidden", "", "" );
		$this->uitable->addModal ( "harga", "money", "Harga", "", 'n', null, false,null,false,"jumlah" );
		$this->uitable->addModal ( "jumlah", "text", "Jumlah", "1", 'n', "numeric",false,null,false,"save"  );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( $this->poliname );
		
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS("rawat/resource/js/alok_stand_alone.js",false);
        
		if(!$this->is_harga_show)
            echo addCSS ("rawat/resource/css/alok_hide_harga.css",false );
		
		$carabayar=new Hidden("alok_carabayar","",$this->carabayar);
		$poliname=new Hidden("alok_poliname","",$this->poliname);
		$polislug=new Hidden("alok_polislug","",$this->polislug);
		echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		
	}
}
?>