<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once("smis-framework/smis/template/ModulTemplate.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
class KonsulDokter extends LayananTemplate {
	private $is_separated;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->is_separated=getSettings($this->db, "smis-rs-konsul-mode-tarif-".$this->polislug, "0");
		$array=array('Tanggal','Dokter',"Kelas","Harga");
		if($this->is_separated=="1"){
            array_unshift($array,"Konsul");
		}
		$this->uitable = new Table($array,$this->getUiTableTitle("Konsul Dokter"), NULL, true);
		$this->uitable->setName("konsul_dokter");
		
	}
	public function command($command) {
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter = new TagihanAdapter ($this->db,$this->polislug);
        $adapter ->setAdapterName("konsul_dokter");
        $this->getDBTable("smis_rwt_konsul_dokter_","konsul_dokter",$adapter);
		//$this->getDBTable("smis_rwt_konsul_dokter_");
        $adapter = new SimpleAdapter();
		$adapter ->add("Tanggal", "waktu", "date d M Y")
                 ->add("Dokter", "nama_dokter")
                 ->add("Kelas", "kelas", "unslug")
                 ->add("Harga", "harga", "money Rp.")
                 ->add("Konsul", "nama_konsul");
		$this->dbres = new RawatResponder($this->dbtable, $this->uitable, $adapter, $this->polislug ,$this->carabayar);
		if($this->dbres->is("save")) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-konsul");
			if(getSettings($this->db,"smis-rs-rumus-pv-sync-konsul-".$this->polislug,"0")=="1"){
                $data                 = array();
                $data['id_dokter']    = $_POST['id_dokter'];
                $data['nama_dokter']  = $_POST['nama_dokter'];
                $data['noreg_pasien'] = $_POST['noreg_pasien'];
                $serv                 = new ServiceConsumer($this->db,"push_dokter_konsul",$data,"kasir");
                $serv->execute();
            }
		}
        
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_konsul","KSL","konsul","waktu","nama_dokter","harga");
        }
        
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}
	public function superCommand($super_command) {		
		$strict_dr	= getSettings($this->db, "smis-rs-dokter-konsul-".$this->polislug, "0");
		$dkadapter 	= new SimpleAdapter();
		$dkadapter	->add("Jabatan", "nama_jabatan")
					->add("Nama", "nama")
					->add("NIP", "nip");
		$header		= array('Nama','Jabatan',"NIP");
		$dktable 	= new Table($header);
		$dktable	->setName("dokter_konsul")
					->setModel(Table::$SELECT);
		$dokter 	= new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter");
		$dokter		->setStrict($strict_dr,$this->polislug);
		
		$head		= array('Konsul',"Dokter",'Jabatan',"Kelas","Tarif");
		$dkadapter 	= new SimpleAdapter();
		$dkadapter 	->add("Konsul", "nama_konsul")
					->add("Dokter", "nama_dokter")
					->add("Jabatan", "jabatan", "unslug")
					->add("Kelas", "kelas", "unslug")
					->add("Tarif", "tarif", "money Rp.");		
		$dktable 	= new Table($head);
		$dktable	->setName("tarif_konsul")
					->setModel(Table::$SELECT);		
		$tarif 		= new ServiceResponder($this->db, $dktable, $dkadapter, "get_konsul");
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-konsuldokter-", $this->polislug, $_POST['noreg_pasien']);
			$tarif		->addData("kelas", $data_kelas);
		}else{
			$data_kelas = getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
			$tarif		->addData("kelas", $data_kelas);
		}
		
		$super = new SuperCommand();
		$super ->addResponder("tarif_konsul", $tarif);
		$super ->addResponder("dokter_konsul", $dokter);
		$init  = $super->initialize();
		if($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		require_once "rawat/class/resource/KelasResource.php";
		$this->uitable	->addModal("id", "hidden", "", "")
						->addModal("nama_pasien", "hidden", "", $this->nama_pasien)
						->addModal("noreg_pasien", "hidden", "", $this->noreg)
						->addModal("nrm_pasien", "hidden", "", $this->nrm)
						->addModal("waktu", "date", "Tanggal",  date("Y-m-d") );
		if($this->is_separated=="1"){
			$this->uitable	->addModal("nama_dokter", "chooser-konsul_dokter-dokter_konsul-Pilih Dokter", "Dokter", "","n",null,false,null,true)
							->addModal("nama_konsul", "chooser-konsul_dokter-tarif_konsul-Pilih Konsul", "Konsul", "" ,"n",null,false,null,false);
		}else{
			$this->uitable	->addModal("nama_dokter", "chooser-konsul_dokter-tarif_konsul-Pilih Konsul", "Dokter", "","n",null,false,null,false)
							->addModal("nama_konsul", "text", "Konsul", "" ,"y",null,false,null,true);
		}
		$this->uitable	->addModal("id_dokter", "hidden", "", "","n")
						->addModal("id_konsul", "hidden", "", "")
						->addModal("kelas", "select", "Kelas", KelasResource::getKelasOptionDefault()->getContent(), 'n', null, true)
                        ->addModal("harga", "money", "Harga", "", 'n', null, true)
                        ->addModal("jaspel_nilai","hidden","","")
                        ->addModal("jaspel_persen","hidden","","");
		$modal 		= $this->uitable->getModal();
		$modal		->setTitle($this->poliname);
		$separated	= new Hidden("is_konsul_separated", "", $this->is_separated);
		
        $carabayar	= new Hidden("konsul_dokter_carabayar","",$this->carabayar);
		$poliname	= new Hidden("konsul_dokter_poliname","",$this->poliname);
		$polislug	= new Hidden("konsul_dokter_polislug","",$this->polislug);
        
        echo $separated	->getHtml();
		echo $this		->uitable->getHtml();
		echo $modal		->getHtml();		
        echo $carabayar	->getHtml();
		echo $poliname	->getHtml();
		echo $polislug	->getHtml();
        echo addJS ("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS ("framework/smis/js/table_action.js");
		echo addCSS("framework/bootstrap/css/datepicker.css");
        echo addJS ("rawat/resource/js/konsul_dokter.js",false);
	}
}

?>