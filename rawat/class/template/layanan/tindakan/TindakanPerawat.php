<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class TindakanPerawat extends LayananTemplate {
    private $number;
	private $is_show_jumlah;
	private $last_setup;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->is_show_jumlah = getSettings($db, "smis-rs-tindakan-perawat-jumlah-".$this->polislug, "0")=="1";
		$array = array ('Tanggal','Nama','Perawat',"Biaya");
		if($this->is_show_jumlah){
			$array = array ('Tanggal','Nama','Perawat',"Satuan","Jumlah","Biaya");
		}        
        $this->number       = array("I"=>"","II"=>"_dua","III"=>"_tiga","IV"=>"_empat","V"=>"_lima","VI"=>"_enam","VII"=>"_tujuh","VIII"=>"_delapan","IX"=>"_sembilan","X"=>"_sepuluh");
		$this->uitable      = new Table($array,$this->getUiTableTitle("Tindakan Perawat"), NULL, true);
		$this->uitable->setName("tindakan_perawat");
		$this->last_setup	= array();
	}
    
    private function loadLastSaveSetting(){
		$settings   = getSettings($this->db, "smis-settings-tindakan-perawat-".$this->polislug, "[]");
		$set        = json_decode($settings,true);
		$this->last_setup['dokter_id']   = isset($set['id_dokter'])?$set['id_dokter']:"0";
		$this->last_setup['dokter_nama'] = isset($set['nama_dokter'])?$set['nama_dokter']:"";
		foreach($this->number as $rum=>$num){
			$this->last_setup['id_perawat'.$num]    = isset($set['id_perawat'.$num])?$set['id_perawat'.$num]:"0";
			$this->last_setup['nama_perawat'.$num]  = isset($set['nama_perawat'.$num])?$set['nama_perawat'.$num]:""; 
		}		
	}
	
	private function saveLastSetting(){
		$set    = array();
		foreach($this->number as $rum=>$num){
			$set['id_perawat'.$num]     = $_POST['id_perawat'.$num];
			$set['nama_perawat'.$num]   = $_POST['nama_perawat'.$num];
		}
		$json=json_encode($set);
		setSettings($this->db, "smis-settings-tindakan-perawat-".$this->polislug, $json);
	}
    
	public function command($command) {
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter = new TagihanAdapter ($this->db,$this->polislug);
        $adapter ->setAdapterName("tindakan_perawat");
        $this->getDBTable("smis_rwt_tindakan_perawat_","tindakan_perawat",$adapter);
        //$this->getDBTable("smis_rwt_tindakan_perawat_");        
		$adapter = new SimpleAdapter ();
		$adapter ->add("Tanggal", "waktu", "date d-M-Y")
                 ->add("Nama", "nama_tindakan")
                 ->add("Perawat", "nama_perawat")
                 ->add("Jumlah", "jumlah")
                 ->add("Satuan", "satuan","money Rp.")
                 ->add("Kelas", "kelas", "unslug")
                 ->add("Biaya", "harga_tindakan", "money Rp.");
		$this->dbres = new RawatResponder($this->dbtable, $this->uitable, $adapter, $this->polislug);
		if ($this->dbres->is("save")) {
            $this->saveLastSetting();
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-tindakan-perawat");
            /*hitung jumlah perawat*/
            $total_perawat=0;
            foreach($this->number as $rum=>$num){
                if(isset($_POST['id_perawat'.$num]) &&  $_POST['id_perawat'.$num]!="" && $_POST['id_perawat'.$num]!="0" && $_POST['id_perawat'.$num]!=0){
                    $total_perawat++;
                }
            }
            $this->dbres->addColumnFixValue("total_perawat", $total_perawat);            
		}
        
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_tindakan_perawat","PRW","tindakan_perawat","waktu","nama_tindakan","harga_tindakan");
        }
        
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}
    
    private function initTarifTindakanPerawat(SuperCommand $super){
        $dktable    = new Table(array ("Nama","Kelas","Tarif"), "", NULL, true);
		$dktable    ->setName("tarif_keperawatan")
                    ->setModel(Table::$SELECT);
		$dkadapter  = new SimpleAdapter ();
		$dkadapter  ->add("Nama", "nama")
                    ->add("Kelas", "kelas", "unslug")
                    ->add("Tarif", "tarif", "money Rp.");
		$data_kelas = getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
		$serv_tarif = "get_keperawatan";
		if(getSettings($this->db,"smis-rs-tarif-tperawat-".$this->polislug,"0")=="1"){
            $serv_tarif = "get_keperawatan_ranap";    
        }
		$tarif = new ServiceResponder($this->db, $dktable, $dkadapter, $serv_tarif);
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-tperawat-", $this->polislug, $_POST['noreg_pasien']);
			$tarif->addData("kelas", $data_kelas);
		}else{
			$data_kelas = getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
			$tarif->addData("kelas", $data_kelas);
		}
        $super->addResponder("tarif_keperawatan", $tarif);
    }
    
    private function initPerawatSuperCommand(SuperCommand $supercommand, $command){
		$strict         = getSettings($this->db, "smis-rs-employee-tindakan-perawat-".$this->polislug, "0");		
		$jabtan			= getSettings($this->db,"smis-rs-employee-tindakan-perawat-jabatan-".$this->polislug,"");
		$dkadapter      = new SimpleAdapter ();
		$dkadapter	    ->add("Jabatan", "nama_jabatan")
                        ->add("Nama", "nama")
                        ->add("NIP", "nip");
		$head           = array ('Nama','Jabatan',"NIP");
		$dktable        = new Table($head, "", NULL, true);
		$dktable        ->setName($command)
                        ->setModel(Table::$SELECT);
		$perawat        = new EmployeeResponder($this->db, $dktable, $dkadapter, $jabtan);
		$perawat        ->setStrict($strict,$this->polislug);		
		$supercommand   ->addResponder($command, $perawat);
	}
    
	public function superCommand($super_command) {		
		$super = new SuperCommand ();
		switch($super_command){
			case "tarif_keperawatan" 	   		      : $this->initTarifTindakanPerawat($super); break;
			default							   		  : $this->initPerawatSuperCommand($super,$super_command); break; 
		}
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	public function phpPreload() {
		require_once "rawat/class/resource/KelasResource.php";
        $this->loadLastSaveSetting();
		$this->uitable  ->addModal("id", "hidden", "", "")
                        ->addModal("nama_pasien", "hidden", "", $this->nama_pasien)
                        ->addModal("noreg_pasien", "hidden", "", $this->noreg)
                        ->addModal("nrm_pasien", "hidden", "", $this->nrm)
                        ->addModal("waktu", "date", "Tanggal", date("Y-m-d"));
		
        /**
		 * Menghitung jumlah counter untuk tindakan Perawat
		 * bisa menggunakan Setting -> tampilan -> Jumlah Perawat di Tindakan Perawat
		 */		
		$counter=getSettings($this->db, "smis-rs-triple-tindakan-perawat-".$this->polislug, "1")*1;
		$numero=1;
        
		foreach($this->number as $rum=>$num){
			if($numero<=$counter){
				$this->uitable->addModal("nama_perawat".$num, "chooser-tindakan_perawat-perawat_tindakan_perawat".$num."-Pilih Perawat ".$rum, "Perawat ".$rum, $this->last_setup['nama_perawat'.$num]);	
			}else{
				$this->uitable->addModal("nama_perawat".$num, "hidden", "", "","n",null,false,null,false);
			}
			$this->uitable->addModal("id_perawat".$num, "hidden", "", $this->last_setup['id_perawat'.$num]);
			$numero++;
		}
        
		$this->uitable  ->addModal("nama_tindakan", "chooser-tindakan_perawat-tarif_keperawatan-Pilih Tindakan", "Tindakan", "", 'n', null, false,null,false)
                        ->addModal("id_tindakan", "hidden", "", "")
                        ->addModal("kelas", "select", "Kelas", KelasResource::getKelasOptionDefault()->getContent(), 'n', null, true)
                        ->addModal("satuan", "money", "Harga", "", 'n', null, true);
		if($this->is_show_jumlah){
			$this->uitable  ->addModal("jumlah", "text", "Jumlah", "1", 'n', "numeric", false,null,false,"save")
                            ->addModal("harga_tindakan", "money", "Total", "", 'n', null, true);
		}else{
			$this->uitable ->addModal("jumlah", "hidden", "", "1", 'n', null, true)
                           ->addModal("harga_tindakan", "hidden", "", "", 'n', null, true);
		}	
        $this->uitable 	->addModal("jaspel", "hidden", "","")
						->addModal("jaspel_persen", "hidden", "","")
						->addModal("jaspel_lain_lain", "hidden", "","")
                       	->addModal("jaspel_penunjang", "hidden", "","");
				
		$modal = $this->uitable->getModal ();
		$modal->setTitle($this->poliname);
		
        $carabayar  = new Hidden("tp_carabayar","",$this->carabayar);
		$poliname   = new Hidden("tp_poliname","",$this->poliname);
		$polislug   = new Hidden("tp_polislug","",$this->polislug);
        
		echo $this  ->uitable->getHtml ();
		echo $modal ->getHtml ();
		echo addJS	("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS	("framework/smis/js/table_action.js");		
        echo addJS	("rawat/resource/js/tindakan_perawat.js",false);
        echo addCSS	("framework/bootstrap/css/datepicker.css");
        echo $carabayar ->getHtml();
		echo $poliname  ->getHtml();
		echo $polislug  ->getHtml();
	}
}

?>