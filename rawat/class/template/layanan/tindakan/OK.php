<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'rawat/class/adapter/OkAdapterTindakan.php';
require_once 'rawat/class/adapter/AnestesiAdapterTindakan.php';
require_once 'rawat/class/responder/OKResponder.php';

class Ok extends LayananTemplate {
	private $list_harga_kamar;
	private $team_name;
	private $team_id;
	private $team_abrev;
	private $complete;
	private $show_all_biaya;
	
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->complete         = getSettings($db, "smis-operatie-item-" . $polislug, "0")=="1";
		$header                 = array ("Tanggal",'Tindakan 1',"Tindakan 2",'Dokter',"Total");
		$this->uitable          = new Table($header, $this->getUiTableTitle("Kamar Operasi"), NULL, true);
		$this->uitable			->setName("ok");
		$this->uitable			->setDetailButtonEnable(true);
		$this->list_harga_kamar = null;
		$this->team_abrev       = getSettings($db, "smis-rs-abrev-operatie-" . $this->polislug, "OK");
		$this->team_id          = getSettings($db, "smis-rs-operatie-team-id-" . $this->polislug, "38");
		$this->team_name        = getSettings($db, "smis-rs-operatie-team-name-" . $this->polislug, "Team OK");
		$this->show_all_biaya   = getSettings($db, "smis-rs-operatie-hidden-" . $this->polislug, "0")=="0";
	}
	
	private function getTarifCommand(){
		$header     = array ("Nama","Kelas","Tarif");
		$dktable    = new Table ($header);
		$dktable    ->setName("ok_tarif_ok")
                    ->setModel(Table::$SELECT);
		$dkadapter  = new SimpleAdapter ();
		$dkadapter  ->add("Nama", "nama")
                    ->add("Kelas", "kelas", "unslug")
                    ->add("Tarif", "tarif", "money Rp.");
		$tarif      = new ServiceResponder($this->db, $dktable, $dkadapter, "get_ok","manajemen");
		return $tarif;
	}
	
	private function getTarifTindakanCommand(){
		$header    = array ("Dokter","Kelas","Pasien","Tarif");
		$dktable   = new Table ($header);
		$dktable   ->setName("ok_nama_tindakan")
                   ->setModel(Table::$SELECT);
		$dkadapter = new SimpleAdapter ();
		$dkadapter ->add("Dokter", "nama_dokter")
				   ->add("Kelas", "kelas", "unslug")
				   ->add("Pasien", "jenis_pasien", "unslug")
				   ->add("Tarif", "tarif", "money Rp.");
		$tarif     = new ServiceResponder($this->db, $dktable, $dkadapter, "get_tindakan_dokter","manajemen");
		return $tarif;
	}

	private function getNamaOperasiCommand($command="ok_nama_operasi"){
		$mode   = getSettings($this->db,"smis-rs-operatie-model-" . $this->polislug, "operator");
		$header = array ("Operasi","Jenis","Kelas","Dokter","Tarif","Ass. Anatesi","Ass. Operator II","Instrumen","Oomloop");
		if ($mode == "anastesi"){
            $header = array ("Nama","Kelas","Tarif");
        }
		$dktable   = new Table ($header);
		$dktable   ->setName($command)
				   ->setModel(Table::$SELECT);
		$dkadapter = new SimpleAdapter ();
		$dkadapter ->add("Dokter", "nama_dokter")
				   ->add("Operasi", "nama")
				   ->add("Jenis", "jenis_operasi")
				   ->add("Kelas", "kelas","unslug")
				   ->add("Tarif", "tarif","money Rp.")
				   ->add("Ass. Anatesi", "tarif_asisten_anastesi","money Rp.")
				   ->add("Ass. Operator II", "tarif_asisten_operator_dua","money Rp.")
				   ->add("Instrumen", "tarif_instrument_satu","money Rp.")
				   ->add("Oomloop", "tarif_oomloop_satu","money Rp.");
		if ($mode == "anastesi") {
			$dkadapter = new SimpleAdapter();
			$dkadapter ->add("Nama", "nama")
					   ->add("Kelas", "kelas", "unslug")
					   ->add("Tarif", "tarif", "money Rp.");
		}
		$service = "get_tindakan_operasi";
		if($command=="ok_nama_vk"){
			$service = "get_tindakan_vk";	
		}

		$tarif_ok = new ServiceResponder($this->db, $dktable, $dkadapter, $service,"manajemen");
		if ($mode == "anastesi")
			$tarif_ok = new ServiceResponder($this->db, $dktable, $dkadapter, "get_anastesi", "manajemen");
		
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-operatie-", $this->polislug, $_POST['noreg_pasien']);
			$tarif_ok->addData("kelas", $data_kelas);
			if(getSettings($this->db, "smis-rs-operatie-jp-".$this->polislug , "0")=="1"){
				$tarif->addData("jenis_pasien", $_POST['carabayar']);
			}
		}else{			
			$data_kelas = getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
			$tarif_ok->addData("kelas", $data_kelas);				
		}
		return $tarif_ok;
	}
    
    private function getICDCommand(){
		$muitable   = new Table(array ("Nama",'Keterangan','Kode'));
        $muitable   ->setName("ok_icd_tindakan")
                    ->setModel(Table::$SELECT);        
		$madapter   = new SimpleAdapter ();
		$madapter   ->add("Nama", "nama")
                    ->add("Kode", "icd")
				    ->add("Keterangan", "terjemah");
		$mresponder = new ServiceResponder($this->db, $muitable, $madapter , "get_icdtindakan");
		return $mresponder;
	}
	
	public function getKaryawanCommand($super_command){
		$eadapt   = new SimpleAdapter ();
		$eadapt   ->add("Jabatan", "nama_jabatan")
                  ->add("Nama", "nama")
                  ->add("NIP", "nip");
		$head_karyawan = array ('Nama','Jabatan',"NIP");	
		$dktable  = new Table($head_karyawan, "", NULL, true);
		$dktable  ->setName($super_command)
                  ->setModel(Table::$SELECT);
		$filter   = getSettings($this->db,"smis-rs-operatie-filter-".$super_command."-" . $this->polislug,"");
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt,  $filter);
		return $employee;
	}
	
	public function superCommand($super_command) {
		$super = new SuperCommand ();	
		switch($super_command){
				case "ok_icd_tindakan" 				: $super->addResponder("ok_icd_tindakan", $this->getICDCommand()); break;
				case "ok_nama_tindakan" 			: $super->addResponder("ok_nama_tindakan", $this->getTarifTindakanCommand()); break;
                case "ok_nama_operasi" 				: $super->addResponder("ok_nama_operasi", $this->getNamaOperasiCommand("ok_nama_operasi")); break;
                case "ok_nama_operasi_dua" 		    : $super->addResponder("ok_nama_operasi_dua", $this->getNamaOperasiCommand("ok_nama_operasi_dua")); break;			
				case "ok_nama_vk" 					: $super->addResponder("ok_nama_vk", $this->getNamaOperasiCommand("ok_nama_vk")); break;			
				case "ok_nama_tindakan_anastesi" 	: $super->addResponder("ok_nama_tindakan_anastesi", $this->getNamaOperasiCommand("ok_nama_tindakan_anastesi")); break;			
				case "ok_tarif_ok" 					: $super->addResponder("ok_tarif_ok", $this->getTarifCommand()); break;
				default 							: $super->addResponder($super_command, $this->getKaryawanCommand($super_command)); break;
		}
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	public function command($command) {
        global $db;
        require_once "rawat/class/adapter/TagihanOKAdapter.php";
        $adapter = new TagihanOKAdapter ($this->db,$this->polislug);
        $adapter ->setAlwaysSHow(true);
        $this->getDBTable("smis_rwt_ok_","ok",$adapter);

		//$this->getDBTable("smis_rwt_ok_");
        $adapter     = new OkAdapter ($this->complete);
		$mode        = getSettings($this->db,"smis-rs-operatie-model-" . $this->polislug, "operator");
		if ($mode == "anastesi")
			$adapter = new OKAdapter ($this->complete);
		$this->dbres = new OKResponder($this->dbtable, $this->uitable, $adapter, $this->polislug);
		$this->dbres ->setAccounting("get_accounting_ok","OK","operasi","waktu","nama_tindakan","harga_operator_satu");
		if ($this->dbres->is("save")) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-ok");

            $cyto       = 0;
            $local      = getSettings($db,"smis-rs-name-ok-local-".$this->polislug,"65");
            $regional   = getSettings($db,"smis-rs-name-ok-regional-".$this->polislug,"85");

            $harga_tindakan_satu = $_POST['harga_tindakan_satu'];
            $harga_tindakan_dua  = $_POST['harga_tindakan_dua'];

            if($_POST['jenis_operasi']=="CITO"){
                $cyto       = getSettings($db,"smis-rs-name-ok-cyto","25");
                $harga_tindakan_satu = $harga_tindakan_satu *(100+$cyto)/100;
                $harga_tindakan_dua = $harga_tindakan_dua *(100+$cyto)/100;
            }

            $persen_bius = 0;
            if($_POST['jenis_bius']=="Lokal"){
                $persen_bius = $local;               
            }else if($_POST['jenis_bius']=="Regional / General"){
                $persen_bius = $regional;
            }

            $harga_tindakan_dua = $harga_tindakan_dua *$persen_bius/100;
            $harga_tindakan     = $harga_tindakan_satu+$harga_tindakan_dua;

            $this->dbres->addColumnFixValue("percent_cito",$cyto);
            $this->dbres->addColumnFixValue("percent_bius",$persen_bius);
            $this->dbres->addColumnFixValue("harga_tindakan",$harga_tindakan);
		}        
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}

	public function phpPreload() {
		$this->uitable  ->addModal("id", "hidden", "", "")
                        ->addModal("nama_pasien", "hidden", "", $this->nama_pasien)
                        ->addModal("noreg_pasien", "hidden", "", $this->noreg)
                        ->addModal("nrm_pasien", "hidden", "", $this->nrm)
                        ->addModal("waktu", "datetime", "Tanggal",  date("Y-m-d H:i:s") )
                        ->addModal("bagi_dokter", "hidden", "",  "0" )
                        ->addModal("bagi_asisten_operator", "hidden", "",  "0" )
                        ->addModal("bagi_oomloop", "hidden", "",  "0" );
        $this->customize_ui();
        if(getSettings($this->db,"smis-rs-operatie-icdtindakan-" . $this->polislug,"0")=="1"){
            $this->uitable  ->addModal("nama_icd","chooser-ok-ok_icd_tindakan-Kode ICD IX", "ICD 9", "", "y",null,true)
                            ->addModal("kode_icd","text", "Kode ICD IX", "", "y",null,true)
                            ->addModal("ket_icd","text", "Keterangan", "", "y",null,true);
        }
        
		$modal = $this->uitable->getModal ();
		$modal ->setComponentSize(Modal::$MEDIUM);
		if(getSettings($this->db, "smis-rs-operatie-two-column-".$this->polislug, "0")=="1"){
			$modal ->setModalSize(Modal::$FULL_MODEL);
		}
		$modal ->setTitle($this->poliname);
				
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addJS("framework/smis/js/table_action.js");
		echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
		$auto = getSettings($this->db, "smis-rs-auto-operasi-" . $this->polislug, "0")=="1";
		if($auto){
            echo addJS("rawat/resource/js/okauto.js",false);
        }
	}
    
	private function customize_ui(){
		$ser    = new ServiceConsumer($this->db,"get_diagnosa_ok",array("noreg_pasien"=>$this->noreg),"medical_record");
		$ser    ->setMode(ServiceConsumer::$SINGLE_MODE)
                ->execute();
		$diag   = $ser->getContent();
		
		$list 	= array(
                    "nama_tindakan","harga_tindakan_satu","nama_tindakan_dua","harga_tindakan_dua","diagnosa","nama_sewa_kamar","harga_sewa_kamar","harga_sewa_alat","kelas",
					"harga_dokter_pendamping","nama_dokter_pendamping","id_dokter_pendamping",					
					"harga_operator_satu","nama_operator_satu","id_operator_satu","jenis_operator_satu",
					"nama_operator_dua","id_operator_dua","jenis_operator_dua",
					"harga_operator_dua","id_perujuk","nama_perujuk","kode_perujuk",
                    "nama_asisten_operator_satu","id_asisten_operator_satu","harga_asisten_operator_satu",
					"nama_asisten_operator_dua","id_asisten_operator_dua","harga_asisten_operator_dua",
					"anastesi_hadir","harga_anastesi","nama_anastesi","id_anastesi",
					"nama_asisten_anastesi","id_asisten_anastesi","harga_asisten_anastesi",
					"nama_asisten_anastesi_dua","id_asisten_anastesi_dua","harga_asisten_anastesi_dua",
					"nama_instrument","id_instrument","harga_instrument","nama_instrument_dua","id_instrument_dua",
					"harga_instrument_dua","nama_oomloop_satu","id_oomloop_satu","harga_oomloop_satu",
					"nama_oomloop_dua","id_oomloop_dua","harga_oomloop_dua","nama_bidan","id_bidan",
					"harga_bidan","nama_bidan_dua","id_bidan_dua","harga_bidan_dua",
					"nama_team_ok","id_team_ok","harga_team_ok",
					"harga_perawat",
					"nama_perawat","id_perawat",
					"nama_perawat_dua","id_perawat_dua",
					"nama_perawat_tiga","id_perawat_tiga",
					"nama_perawat_empat","id_perawat_empat",
					"nama_perawat_lima","id_perawat_lima",
					"nama_perawat_enam","id_perawat_enam",
					"nama_perawat_tujuh","id_perawat_tujuh",
					"nama_perawat_delapan","id_perawat_delapan",
					"nama_perawat_sembilan","id_perawat_sembilan",
					"nama_perawat_sepuluh","id_perawat_sepuluh",
					"recovery_room",
                    "keterangan",
                    
                    
                );
        $jenis_operasi = new OptionBuilder();
        $jenis_operasi ->add("Elektif","Elektif","1");
        $jenis_operasi ->add("CITO","CITO");
        $jenis_bius = new OptionBuilder();
        $jenis_bius ->add("Lokal","Lokal","1");
        $jenis_bius ->add("Regional / General","Regional / General");
        $this->uitable->addModal("jenis_operasi","select","Jenis Operasi",$jenis_operasi->getContent());
        $this->uitable->addModal("jenis_bius","select","Jenis Bius",$jenis_bius->getContent());
        
		foreach($list as $x){
            //$this->uitable->addModal("nama_tindakan","select","Nama Tindakan 1");
            //$this->uitable->addModal("nama_tindakan_dua","select","Nama Tindakan 2");
            $settings   = getSettings($db,"smis-rs-operatie-ui-".$x."-".$this->polislug,"text;$x;;y;null;true;null;true;;");
        
            if(  strrpos($x, "id_", -strlen($x)) !== false){
				$this->uitable->addModal($x, "hidden", "", "");				
			}else {
				$explode    = explode(";",$settings);
				$model      = $explode[0];
				$name       = $explode[1];
				$df         = $x=="diagnosa"?$diag:$explode[2];
				$is_empty   = $explode[3];
				$typical    = $explode[4]=="null"?null:$explode[4];
				$disabled   = $explode[5]=="true";
				$option     = $explode[6]=="null"?null:$explode[6];
				$autofocus  = $explode[7]=="true";
				$next       = $explode[8];				
				$this->uitable->addModal($x, $model, $name, $df,$is_empty,$typical,$disabled,$option,$autofocus,$next);
			}
		}	
	}

	public function jsPreLoad() {
		$hiddenslug      = new Hidden("ok_hidden_polislug","",$this->polislug);
		$hiddenname      = new Hidden("ok_hidden_poliname","",$this->poliname);
		$hiddencarabayar = new Hidden("ok_hidden_carabayar","",$this->carabayar);
		loadLibrary("smis-libs-function-javascript");
		echo $hiddenname        ->getHtml();
		echo $hiddenslug        ->getHtml();
		echo $hiddencarabayar   ->getHtml();
		echo addJS("rawat/resource/js/ok.js",false);		
	}

	public function cssPreLoad() {
		echo addCSS("rawat/resource/css/ok.css",false);
	}
}