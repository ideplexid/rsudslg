<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
class VisiteDokter extends LayananTemplate {
	private $is_separated;
	private $jumlah_dokter;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->is_separated  = getSettings($this->db, "smis-rs-visite-mode-tarif-".$this->polislug, "0");
		$this->jumlah_dokter = getSettings($this->db,"smis-rs-total-visite-dokter-" . $this->polislug,"1")*1;
		
		$array=array ('Tanggal',"Kelas","Harga",'Dokter');
		if($this->is_separated=="1"){
			$array=array ("Visite",'Tanggal',"Kelas","Harga",'Dokter');
		}
		$this->uitable = new Table ($array, $this->getUiTableTitle("Visite Dokter"), NULL, true);
		if($this->jumlah_dokter>=2) $this->uitable->addHeaderElement("Dokter II");
		if($this->jumlah_dokter>=3) $this->uitable->addHeaderElement("Dokter III");
		$this->uitable->setName("visite_dokter");
	}
	public function command($command) {
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter = new TagihanAdapter ($this->db,$this->polislug);
        $adapter ->setAdapterName("visite_dokter");
        $this->getDBTable("smis_rwt_visite_dokter_","visite_dokter",$adapter);

        //$this->getDBTable("smis_rwt_visite_dokter_");
		$adapter = new SimpleAdapter ();
		$adapter ->add("Tanggal", "waktu", "date d M Y H:i")
                 ->add("Dokter", "nama_dokter")
                 ->add("Dokter II", "nama_dokter_dua")
                 ->add("Dokter III", "nama_dokter_tiga")
                 ->add("Kelas", "kelas", "unslug")
                 ->add("Harga", "harga", "money Rp.")
                 ->add("Visite", "nama_visite");
                 
		$this->dbres = new RawatResponder($this->dbtable, $this->uitable, $adapter, $this->polislug);
		if ($this->dbres->is("save")) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-visite");
		}
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_visite","VST","visite","waktu","nama_dokter","harga");
        }
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}
	
	
	private function initDokterSuperCommand(SuperCommand $supercommand,$command){
		$strict_dr=getSettings($this->db, "smis-rs-dokter-visite-".$this->polislug, "0");
		$head=array ('Nama','Jabatan',"NIP");
		$strict_dr=getSettings($this->db, "smis-rs-dokter-tindakan-igd-".$this->polislug, "0");
		$dktable = new Table($head, "", NULL, true);
		$dktable->setName($command)
				->setModel(Table::$SELECT);
		$dkadapter = new SimpleAdapter ();
		$dkadapter	->add("Jabatan", "nama_jabatan")
					->add("Nama", "nama")
					->add("NIP", "nip");
		$dokter = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter");		
		$dokter->setStrict($strict_dr,$this->polislug);
		$supercommand->addResponder($command, $dokter);
	}
	
	private function initTarifVisite(SuperCommand $supercommand,$command){
		$head=array ('Nama','Jabatan',"Kelas","Tarif");
		$dkadapter = new SimpleAdapter ();
		if($this->is_separated=="1"){
            $dkadapter->add("Nama", "nama_visite");
        }else{
            $dkadapter->add("Nama", "nama_dokter");
        }	
		$dkadapter  ->add("Jabatan", "jabatan", "unslug")
                    ->add("Kelas", "kelas", "unslug")
                    ->add("Tarif", "tarif", "money Rp.");
		
		$dktable = new Table($head);
		$dktable ->setName($command)
                 ->setModel(Table::$SELECT);
		
		$tarif = new ServiceResponder($this->db, $dktable, $dkadapter, "get_visite");
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-visite-", $this->polislug, $_POST['noreg_pasien']);
			$tarif->addData("kelas", $data_kelas);
		}else{
			$data_kelas =getSettings($this->db, "smis-rs-kelas-" . $this->polislug, "");
			$tarif->addData("kelas", $data_kelas);
		}
		$supercommand->addResponder($command, $tarif);
	}
	
	public function superCommand($super_command) {
		$super = new SuperCommand ();
		if($super_command!="tarif_visite"){
			$this->initDokterSuperCommand($super,$super_command);			
		}else{
			$this->initTarifVisite($super,$super_command);
		}
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		/* This is Modal Form and used for add and edit the table */
		$this->uitable  ->addModal("id", "hidden", "", "")
                        ->addModal("nama_pasien", "hidden", "", $this->nama_pasien)
                        ->addModal("noreg_pasien", "hidden", "", $this->noreg)
                        ->addModal("nrm_pasien", "hidden", "", $this->nrm)
                        ->addModal("waktu", "datetime", "Tanggal",  date("Y-m-d") );		
		
		if($this->is_separated=="1"){
			$this->uitable  ->addModal("nama_dokter", "chooser-visite_dokter-dokter_visite-Pilih Dokter", "Dokter", "","n",null,false,null,true)
                            ->addModal("nama_visite", "chooser-visite_dokter-tarif_visite-Pilih Visite", "Visite", "" ,"n",null,false,null,false);
		}else{
			$this->uitable  ->addModal("nama_visite", "hidden", "", "" ,"y",null,false,null,true)
                            ->addModal("nama_dokter", "chooser-visite_dokter-tarif_visite-Pilih Visite", "Dokter", "","n",null,false,null,false);
		}
		
		if($this->jumlah_dokter>=2){
			$this->uitable  ->addModal("nama_dokter_dua", "chooser-visite_dokter-dokter_visite_dua-Pilih Dokter II", "Dokter II", "","n",null,false,null,false)
                            ->addModal("id_dokter_dua", "hidden", "", "" ,"n",null,false,null,false);
		}
		if($this->jumlah_dokter>=3){
			$this->uitable ->addModal("nama_dokter_tiga", "chooser-visite_dokter-dokter_visite_tiga-Pilih Dokter III", "Dokter III", "","n",null,false,null,false)
                           ->addModal("id_dokter_tiga", "hidden", "", "" ,"n",null,false,null,false);
		}
		require_once "rawat/class/resource/KelasResource.php";
        
		$this->uitable  ->addModal("id_dokter", "hidden", "", "","n")
                        ->addModal("id_visite", "hidden", "", "")
                        ->addModal("kelas", "select", "Kelas", KelasResource::getKelasOptionDefault()->getContent(), 'n', null, true)
                        ->addModal("harga", "money", "Harga", "", 'y', null, true)
                        ->addModal("jaspel_nilai","hidden","","")
                        ->addModal("jaspel_persen","hidden","","");;
		$modal = $this->uitable->getModal ();
		$modal->setTitle($this->poliname);
		$separated  = new Hidden("is_visite_separated", "", $this->is_separated);
		$carabayar  = new Hidden("vst_carabayar","",$this->carabayar);
		$poliname   = new Hidden("vst_poliname","",$this->poliname);
		$polislug   = new Hidden("vst_polislug","",$this->polislug);		
		echo $carabayar     ->getHtml();
		echo $poliname      ->getHtml();
		echo $polislug      ->getHtml();		
		echo $separated     ->getHtml();
		echo $this->uitable ->getHtml();
		echo $modal         ->getHtml();
		echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addJS("framework/smis/js/table_action.js");
		echo addJS("rawat/resource/js/visite_dokter.js",false);
		echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	}
}

?>