<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once "rawat/class/adapter/BedKamarAdapter.php";
require_once "rawat/class/responder/BedResponder.php";

class Bed extends LayananTemplate {
	private $id_antrian;
	private $kelas_bed;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien, $id_antrian,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->kelas_bed=getSettings($db,"smis-rs-kelas-bed-" . $this->polislug,"0");
		$header=array ('Nama','Harga',"Masuk","Keluar","Hari","Total");		
		if($this->kelas_bed=="1"){
			$header=array ('Nama',"Tarif",'Harga',"Masuk","Keluar","Hari","Total");		
		}		
		$this->uitable = new Table ( $header, $this->getUiTableTitle("Bed Kamar Inap"), NULL, true );
		$this->uitable->setName ( "bed" );
		$this->id_antrian = $id_antrian;		
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Nama", "nama_bed" );
		$adapter->add ( "Tarif", "nama_tarif" );
		$adapter->add ( "Hari", "hari" );
		$adapter->add ( "Masuk", "waktu_masuk", "date d M Y H:i" );
		$adapter->add ( "Keluar", "waktu_keluar", "date d M Y H:i" );
		$adapter->add ( "Total", "biaya", "money Rp." );
		$adapter->add ( "Harga", "harga", "money Rp." );
        
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter2 = new TagihanAdapter ($this->db,$this->polislug);
        $adapter2 ->setAdapterName("bed");
        $this->getDBTable("smis_rwt_bed_","bed",$adapter2);

        $this->dbres = new BedResponder ( $this->dbtable, $this->uitable, $adapter, $this->polislug );
		if($this->dbres->isSave() && ($_POST['waktu_keluar']!="0000-00-00 00:00" || $_POST['waktu_keluar']!="0000-00-00 00:00:00" ) || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_bed","BED","bed","waktu_keluar","nama_tarif","biaya");
        }
        $response = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $response );
	}
    
	public function superCommand($super_command) {
		$hader=array ("No.","Nama","Biaya" );
		$dktable = new Table ( $hader, "", NULL, true );
		$dktable->setName ( "nama_tarif" )
				->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter	->setUseNumber(true,"No.","back.")
					->add ( "Nama", "name","unslug" )
					->add ( "Biaya", "value", "money Rp." );
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_list_bed" );
		
		$header=array ("Nama","Status","Pasien","NRM");
		$bedtable = new Table ( $header, "Bed", NULL, true );
		$bedtable->setName ( "nomor_bed" );
		$bedtable->setModel ( Table::$SELECT );
		$btable = new DBTable ( $this->db, "smis_rwt_bed_kamar_" . $this->polislug );
		$btable->setOrder ( " nama ASC " );
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Nama", "nama" );
		$adapter->add ( "Status", "terpakai", "trivial_0_Kosong_Terpakai" );
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit6" );
        
		$super = new SuperCommand ();
		$super->addResponder ( "nomor_bed", new DBResponder ( $btable, $bedtable, $adapter ) );
		$super->addResponder ( "nama_tarif", $tarif );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	public function phpPreload() {
		$bed_harga_service = new ServiceConsumer ( $this->db, "get_bed", array ("ruangan" => $this->polislug), "manajemen" );
		$bed_harga_service->execute ();
		$harga = $bed_harga_service->getContent ();
		$atable = new DBTable ( $this->db, "smis_rwt_antrian_" . $this->polislug );
		$row = $atable->select ( $this->id_antrian );
		$tmasuk = $row->waktu;
		$freebed=getSettings($this->db, "smis-rs-free-bed-" . $this->polislug, "0")=="0";
		$this->uitable->addModal("id", "hidden", "", "","n");
		$this->uitable->addModal("nama_pasien", "hidden", "", $this->nama_pasien,"n");
		$this->uitable->addModal("noreg_pasien", "hidden", "", $this->noreg,"n");
		$this->uitable->addModal("nrm_pasien", "hidden", "", $this->nrm,"n");
		$this->uitable->addModal("id_bed", "hidden", "", "","n");
		$this->uitable->addModal("nama_bed", "chooser-bed-nomor_bed-Nomor Bed", "Bed", "","n",null,true);
		$this->uitable->addModal("waktu_masuk", "datetime", "Masuk", $tmasuk,"n");
		$this->uitable->addModal("waktu_keluar", "datetime", "Keluar", "","n");
		/*pilihan tarif bed kamar*/
		$df=getSettings($this->db,"smis-rs-bed-price-choice-".$this->polislug,null);
		$list_tarif=json_decode($df,true);
		if($list_tarif==null){
			$this->uitable->addModal("pilihan_tarif", "hidden", "", "","y");
		}else{
			$option=new OptionBuilder();
			$option->add("","","1");
			foreach($list_tarif as $x=>$v){
				$name=$x." - ".ArrayAdapter::format("only-money Rp.",$v);
				$option->add($name,$v);
			}
			$this->uitable->addModal("pilihan_tarif", "select", "Tarif Khusus", $option->getContent(),"y");			
		}
		
		if($this->kelas_bed=="1"){
			$this->uitable->addModal("nama_tarif","chooser-bed-nama_tarif-Tarif","Tarif Ruangan","","n",null,false);
		}else{
			$this->uitable->addModal("nama_tarif","hidden","","","y");
		}
		
		$this->uitable->addModal("harga", "money", "Biaya", $harga,"n",null,$freebed);
		
        $modal = $this->uitable->getModal ();
		$button = new Button ( "", "", "Save dan Kosongkan" );
		$button->setClass ( "btn-warning" );
		$button->setAction ( "bed.save_and_empty()" );
		$modal->addFooter ( $button );
		$modal->setTitle ( $this->poliname );

		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addJS ( "rawat/resource/js/bed.js",false );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		
		$poliname=new Hidden("bed_poliname","",$this->poliname);
		$polislug=new Hidden("bed_polislug","",$this->polislug);
		echo $poliname->getHtml();
		echo $polislug->getHtml();
	}
}
?>