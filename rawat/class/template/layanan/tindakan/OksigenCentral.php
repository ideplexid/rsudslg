<?php 

require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'rawat/class/adapter/OksigenCentralAdapter.php';
require_once 'rawat/class/responder/OksigenCentralResponder.php';
class OksigenCentralTemplate extends LayananTemplate {
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$head=array ('Waktu',"Durasi","Harga",'Skala','Total');
		$this->uitable = new Table ( $head, $this->getUiTableTitle("Oksigen Central"), NULL, true );
		$this->uitable->setName ( "oksigen_central" );
	}

	public function command($command) {
		$column = array ('id','nama_pasien','nrm_pasien','noreg_pasien','mulai','selesai','skala');
		require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter2 = new TagihanAdapter ($this->db,$this->polislug);
        $adapter2 ->setAdapterName("oksigen_central");
        $this->getDBTable("smis_rwt_oksigen_central_","oksigen_central",$adapter2);

		//$this->getDBTable("smis_rwt_oksigen_central_");
        $this->dbtable->setColumn($column);
		$this->dbres = new OksigenCentralResponder ( $this->dbtable, $this->uitable, new OksigenAdapter (), $this->polislug );
		if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_oksigen_central","O2C","oksigen_central","selesai","skala","harga");
        }
        $data = $this->dbres->command ( $_POST ['command'] );
        
		echo json_encode ( $data );
	}
    
	public function phpPreload() {
		/* This is Modal Form and used for add and edit the table */
		$scale=getSettings($this->db,"smis-rs-oksigen-manual-skala-".$this->polislug,"10")*1;
		$skala = new OptionBuilder();
		for($i = 1; $i <= $scale; $i ++) {
            $skala->addSingle($i);
		}
        
        $this->uitable->addModal("id","hidden","","",null,false);
		$this->uitable->addModal("nama_pasien","hidden","",$this->nama_pasien,null,true);
		$this->uitable->addModal("nrm_pasien","hidden","",$this->nrm,null,true);
		$this->uitable->addModal("noreg_pasien","hidden","",$this->noreg,null,true);
		$this->uitable->addModal("mulai","datetime","Mulai","",null,false);
		$this->uitable->addModal("selesai","datetime","Selesai","",null,false);
		$this->uitable->addModal("skala","select","Skala",$skala->getContent(),null,false);
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( "Oksigen Central" );
        $carabayar=new Hidden("oct_carabayar","",$this->carabayar);
		$poliname=new Hidden("oct_poliname","",$this->poliname);
		$polislug=new Hidden("oct_polislug","",$this->polislug);
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
        echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
        echo addJS ( "rawat/resource/js/oksigen_central.js",false );
	}
}
?>