<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'rawat/class/adapter/VKAdapterTindakan.php';
require_once 'rawat/class/responder/OKResponder.php';
class Vk extends LayananTemplate {
	private $list_harga_kamar;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->uitable = new Table(array ('Biaya','Team',"Tanggal","Total"), $this->getUiTableTitle("Kamar Bersalin"), NULL, true);
		$this->uitable->setName("vk");
		$this->list_harga_kamar = null;
	}
	public function superCommand($super_command) {
		$super = new SuperCommand ();
		$eadapt = new SimpleAdapter ();
		$eadapt->add("Jabatan","nama_jabatan");
		$eadapt->add("Nama","nama");
		$eadapt->add("NIP","nip");
        $header= array ('Nama','Jabatan',"NIP");
		$dktable = new Table($header, "", NULL, true);
		$dktable->setName("vk_operator_satu");
		$dktable->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "dokter");
		$super->addResponder("vk_operator_satu", $employee);

		$dktable = new Table ($header, "", NULL, true);
		$dktable->setName("vk_operator_dua");
		$dktable->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "dokter");
		$super->addResponder("vk_operator_dua", $employee);

		$dktable = new Table($header, "", NULL, true);
		$dktable->setName("vk_anastesi");
		$dktable->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "");
		$super->addResponder("vk_anastesi", $employee);

		$dktable = new Table ($header, "", NULL, true);
		$dktable->setName("vk_asisten_anastesi");
		$dktable->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "");
		$super->addResponder("vk_asisten_anastesi", $employee);

		$dktable = new Table ($header, "", NULL, true);
		$dktable->setName("vk_team_vk");
		$dktable->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "team");
		$super->addResponder("vk_team_vk", $employee);

		$dktable = new Table ($header, "", NULL, true);
		$dktable->setName("vk_team_bidan");
		$dktable->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "team");
		$super->addResponder("vk_team_bidan", $employee);

		$dktable = new Table ($header, "", NULL, true);
		$dktable->setName("vk_team_perawat");
		$dktable->setModel(Table::$SELECT);
		$employee = new EmployeeResponder($this->db, $dktable, $eadapt, "team");
		$super->addResponder("vk_team_perawat", $employee);

		$dktable = new Table ($header, "", NULL, true);
		$dktable->setName("vk_tarif_vk");
		$dktable->setModel(Table::$SELECT);
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add("Nama","nama");
		$dkadapter->add("Kelas","kelas","unslug");
		$dkadapter->add("Tarif","tarif","money Rp.");
		$tarif = new ServiceResponder($this->db, $dktable, $dkadapter, "get_vk");
		$super->addResponder("vk_tarif_vk", $tarif);

		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	public function command($command) {
        //$this->getDBTable("smis_rwt_vk_");
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter = new TagihanAdapter ($this->db,$this->polislug);
        $adapter ->setAdapterName("vk");
        $this->getDBTable("smis_rwt_vk_","vk",$adapter);

        $this->dbres = new OKResponder($this->dbtable, $this->uitable, new VkAdapter (), $this->polislug);
		if ($this->dbres->is("save")) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-vk");
		}
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_vk","VK","vk","waktu","nama_tindakan","harga_operator_satu");
        }
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}

	/* when it's star build */
	public function phpPreload() {
        
		$this->uitable  ->addModal("id","hidden","","")
                        ->addModal("nama_pasien","hidden","", $this->nama_pasien)
                        ->addModal("noreg_pasien","hidden","", $this->noreg)
                        ->addModal("nrm_pasien","hidden","", $this->nrm)
                        ->addModal("waktu","date","Tanggal",  date("Y-m-d"))
                        ->addModal("nama_operator_satu","chooser-vk-vk_operator_satu","Operator I","")
                        ->addModal("id_operator_satu","hidden","","")
                        ->addModal("jenis_operator_satu","hidden","","")
                        ->addModal("harga_operator_satu","money","Biaya I","")
                        ->addModal("nama_operator_dua","chooser-vk-vk_operator_dua","Operator II","")
                        ->addModal("id_operator_dua","hidden","","")
                        ->addModal("jenis_operator_dua","hidden","","")
                        ->addModal("harga_operator_dua","money","Biaya II","")
                        ->addModal("nama_anastesi","chooser-vk-vk_anastesi","Anastesi","")
                        ->addModal("id_anastesi","hidden","","")
                        ->addModal("harga_anastesi","money","Biaya Anastesi","")
                        ->addModal("anastesi_hadir","checkbox","Anastesi Hadir ? ","")
                        ->addModal("nama_asisten_anastesi","chooser-vk-vk_asisten_anastesi","Asisten Anastesi","")
                        ->addModal("id_asisten_anastesi","hidden","","")
                        ->addModal("harga_asisten_anastesi","money","Biaya Asisten","")
                        ->addModal("nama_team_vk","chooser-vk-vk_team_vk","Team VK","")
                        ->addModal("id_team_vk","hidden","","")
                        ->addModal("harga_team_vk","money","Biaya Team VK","")
                        ->addModal("nama_bidan","chooser-vk-vk_team_bidan","Team Bidan","")
                        ->addModal("id_bidan","hidden","","")
                        ->addModal("harga_bidan","money","Biaya Team Bidan","")
                        ->addModal("nama_perawat","chooser-vk-vk_team_perawat","Team Perawat","")
                        ->addModal("id_perawat","hidden","","")
                        ->addModal("harga_perawat","money","Biaya Team Perawat","")
                        ->addModal("nama_sewa_kamar","chooser-vk-vk_tarif_vk","Kamar Operasi","")
                        ->addModal("harga_sewa_kamar","money","Biaya Kamar","")
                        ->addModal("kelas","hidden","","");

		$modal = $this->uitable->getModal ();
		$modal->setTitle($this->poliname);
        $carabayar=new Hidden("vk_carabayar","",$this->carabayar);
		$poliname=new Hidden("vk_poliname","",$this->poliname);
		$polislug=new Hidden("vk_polislug","",$this->polislug);
		
		echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();	
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS("framework/smis/js/table_action.js");
		echo addCSS("framework/bootstrap/css/datepicker.css");
        echo addJS("rawat/resource/js/vk.js",false);
	}
	
}


?>