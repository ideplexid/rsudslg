<?php
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("rawat/class/template/LayananTemplate.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
show_error();
class KonsultasiDokter extends LayananTemplate {
	/**@var array */
	protected $number;
	/**@var array */
	private $last_setup;
	public function __construct($db,$polislug,$poliname,$nama_pasien,$nrm_pasien,$noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db,$polislug,$poliname,$nama_pasien,$nrm_pasien,$noreg_pasien,$carabayar,$titipan);
		$head				= array ('Tanggal','Dokter',"Harga","Perawat");
		$this->uitable 		= new Table($head,$this->getUiTableTitle("Periksa"));
		$this->uitable->setName("konsultasi_dokter");
		$this->number   	= array("I"=>"_satu","II"=>"_dua","III"=>"_tiga","IV"=>"_empat","V"=>"_lima","VI"=>"_enam","VII"=>"_tujuh","VIII"=>"_delapan","IX"=>"_sembilan","X"=>"_sepuluh");
		$this->last_setup	= array();
	}
	public function command($command) {
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter = new TagihanAdapter ($this->db,$this->polislug);
        $adapter ->setAdapterName("konsultasi_dokter");
		$this->getDBTable("smis_rwt_konsultasi_dokter_","konsultasi_dokter",$adapter);
		//$this->getDBTable("smis_rwt_konsultasi_dokter_");
        $adapter = new SimpleAdapter ();
		$adapter ->add("Tanggal","waktu","date d-M-Y")
				 ->add("Dokter","nama_dokter")
				 ->add("Harga","harga","money Rp.")
				 ->add("Perawat","harga_perawat","money Rp.");
		$this->dbres = new RawatResponder( $this->dbtable,$this->uitable,$adapter,$this->polislug);
		if($this->dbres->is("save")){
            self::getProvitShareByService($this->db,$this->dbres,$this->polislug ,$this->carabayar,"smis-pv-konsultasi");
		}
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_periksa","PRS","periksa","waktu","nama_dokter","harga");
        }
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}

	private function initPerawatSuperCommand(SuperCommand $supercommand, $command){
		$strict         = getSettings($this->db, "smis-rs-perawat-periksa-".$this->polislug, "0");		
		$dkadapter      = new SimpleAdapter ();
		$dkadapter	    ->add("Jabatan", "nama_jabatan")
                        ->add("Nama", "nama")
                        ->add("NIP", "nip");
		$head           = array ('Nama','Jabatan',"NIP");
		$dktable        = new Table($head, "", NULL, true);
		$dktable        ->setName($command)
                        ->setModel(Table::$SELECT);
		$perawat        = new EmployeeResponder($this->db, $dktable, $dkadapter, "perawat");
		$perawat        ->setStrict($strict,$this->polislug);		
		$supercommand   ->addResponder($command, $perawat);
	}

	private function initDokterSuperCommand(SuperCommand $super){
		$strict_dr	= getSettings($this->db,"smis-rs-dokter-periksa-".$this->polislug,"0");
		$header		= array ('Nama','Jabatan',"NIP");
		$dktable 	= new Table($header);
		$dktable	->setName("dokter_konsultasi_dokter")
					->setModel(Table::$SELECT);
		$dkadapter  = new SimpleAdapter();
		$dkadapter  ->add("Jabatan","nama_jabatan")
					->add("Nama","nama")
					->add("NIP","nip");
		$dokter 	= new EmployeeResponder($this->db,$dktable,$dkadapter,"dokter");
		$dokter		->setStrict($strict_dr,$this->polislug);
		$super		->addResponder("dokter_konsultasi_dokter",$dokter);
	}

	public function superCommand($super_command) {
		$super = new SuperCommand ();
		switch($super_command){
			case "dokter_konsultasi_dokter" : $this->initDokterSuperCommand($super); 				 break;
			default							: $this->initPerawatSuperCommand($super,$super_command); break; 
		}
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	private function loadLastSaveSetting(){
		$settings   = getSettings($this->db,"smis-settings-periksa-perawat-".$this->polislug,"[]");
		$set        = json_decode($settings,true);
		foreach($this->number as $rum=>$num){
			$this->last_setup['id_perawat'.$num]    = isset($set['id_perawat'.$num])?$set['id_perawat'.$num]:"0";
			$this->last_setup['nama_perawat'.$num]  = isset($set['nama_perawat'.$num])?$set['nama_perawat'.$num]:""; 
		}		
	}
	public function phpPreload() {
		$konsultasi_harga_service = new ServiceConsumer($this->db,"get_konsultasi",array ("ruangan" => $this->polislug),"manajemen");
		$konsultasi_harga_service->execute ();
		$harga = $konsultasi_harga_service->getContent ();
		$this->uitable  ->addModal("id","hidden","","")
                        ->addModal("nama_pasien","hidden","",$this->nama_pasien)
                        ->addModal("noreg_pasien","hidden","",$this->noreg)
                        ->addModal("nrm_pasien","hidden","",$this->nrm)
                        ->addModal("waktu","date","Tanggal", date("Y-m-d") )
                        ->addModal("nama_dokter","chooser-konsultasi_dokter-dokter_konsultasi_dokter-Pilih Dokter","Dokter","","n",null,false,null,true)
                        ->addModal("id_dokter","hidden","","","n")
                        ->addModal("harga","money","Harga",$harga['harga'],'y',null,false,null,false,"save")
                        ->addModal("jaspel_nilai","hidden","",$harga['jaspel_nominal'],'y',null,false,null,false,"save")
                        ->addModal("jaspel_persen","hidden","",$harga['jaspel_persen'],'y',null,false,null,false,"save")
                        ->addModal("jaspel_nilai_perawat","hidden","",$harga['jaspel_nominal_perawat'],'y',null,false,null,false,"save")
                        ->addModal("jaspel_persen_perawat","hidden","",$harga['jaspel_persen_perawat'],'y',null,false,null,false,"save")
                        ;
		$counter = getSettings($this->db, "smis-rs-total-periksa-dokter-".$this->polislug, "1")*1;
		$numero	 = 1;
		foreach($this->number as $rum=>$num){
			if($numero<=$counter){
				$this->uitable ->addModal("nama_perawat".$num, "chooser-konsultasi_dokter-perawat_konsultasi_dokter".$num."-Pilih Perawat ".$rum, "Perawat ".$rum, $this->last_setup['nama_perawat'.$num]);	
				$this->uitable ->addModal("id_perawat".$num, "hidden", "", $this->last_setup['id_perawat'.$num]);
			}
			$numero++;
		}
		if(getSettings($this->db, "smis-rs-ui-harga-perawat-periksa-dokter-".$this->polislug, "0")=="1"){
			$this->uitable  ->addModal("harga_perawat","money","Biaya Perawat","0","y",null,false,null,false,"save");
		}
		
		$modal 			= $this->uitable->getModal ();
		$modal			->setTitle($this->poliname);
        $carabayar		= new Hidden("ksd_carabayar","",$this->carabayar);
		$poliname		= new Hidden("ksd_poliname","",$this->poliname);
		$polislug		= new Hidden("ksd_polislug","",$this->polislug);
        echo $carabayar	->getHtml();
		echo $poliname	->getHtml();
		echo $polislug	->getHtml();
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS	("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS	("framework/smis/js/table_action.js");
		echo addCSS	("framework/bootstrap/css/datepicker.css");
        echo addJS	("rawat/resource/js/konsultasi_dokter.js",false);
	}
}

?>