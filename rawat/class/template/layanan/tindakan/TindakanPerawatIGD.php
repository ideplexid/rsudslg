<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class TindakanPerawatIGD extends LayananTemplate {
	private $last_setup;
	private $number;
	private $is_show_jumlah;
	public function __construct($db,$polislug,$poliname,$nama_pasien,$nrm_pasien,$noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db,$polislug,$poliname,$nama_pasien,$nrm_pasien,$noreg_pasien,$carabayar,$titipan);
		$header2=array ('Tanggal','Nama','Perawat',"Dokter","Biaya");
		$this->is_show_jumlah=getSettings($db,"smis-rs-tindakan-perawat-jumlah-" . $this->polislug,"0")=="1";
		if($this->is_show_jumlah){
			$header2=array ('Tanggal','Nama','Perawat',"Dokter","Satuan","Jumlah","Biaya");
		}
		$this->number=array("I"=>"","II"=>"_dua","III"=>"_tiga","IV"=>"_empat","V"=>"_lima","VI"=>"_enam","VII"=>"_tujuh","VIII"=>"_delapan","IX"=>"_sembilan","X"=>"_sepuluh");
		$judul=getSettings($this->db,"smis-rs-name-tindakan_perawat_igd-".$this->polislug,$this->getUiTableTitle("Tindakan Perawat IGD"));
		$this->uitable = new Table($header2,$judul ,NULL,true);
		$this->uitable->setName("tindakan_perawat_igd");
		$this->last_setup=array();
	}
	
	private function loadLastSaveSetting(){
		$settings=getSettings($this->db,"smis-settings-tindakan-perawat-igd-".$this->polislug,"[]");
		$set=json_decode($settings,true);
		$this->last_setup['dokter_id']=isset($set['id_dokter'])?$set['id_dokter']:"0";
		$this->last_setup['dokter_nama']=isset($set['nama_dokter'])?$set['nama_dokter']:"";
		foreach($this->number as $rum=>$num){
			$this->last_setup['id_perawat'.$num]=isset($set['id_perawat'.$num])?$set['id_perawat'.$num]:"0";
			$this->last_setup['nama_perawat'.$num]=isset($set['nama_perawat'.$num])?$set['nama_perawat'.$num]:""; 
		}		
	}
	
	private function saveLastSetting(){
		$set=array();
		$set['id_dokter']=$_POST['id_dokter'];
		$set['nama_dokter']=$_POST['nama_dokter'];
		foreach($this->number as $rum=>$num){
			$set['id_perawat'.$num]=$_POST['id_perawat'.$num];
			$set['nama_perawat'.$num]=$_POST['nama_perawat'.$num];
		}
		$json=json_encode($set);
		setSettings($this->db,"smis-settings-tindakan-perawat-igd-".$this->polislug,$json);
	}
	
	public function command($command) {
        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter = new TagihanAdapter ($this->db,$this->polislug);
        $adapter ->setAdapterName("tindakan_igd");
		$this->getDBTable("smis_rwt_tindakan_igd_","tindakan_igd",$adapter);
		//$this->getDBTable("smis_rwt_tindakan_igd_");
        $adapter = new SimpleAdapter ();
		$adapter->add("Nama","nama_tindakan")
				->add("Perawat","nama_perawat")
				->add("Kelas","kelas","unslug")
				->add("Satuan","satuan","money Rp.")
				->add("Jumlah","jumlah")
				->add("Dokter","nama_dokter")
				->add("Biaya","harga_tindakan","money Rp.");
		
		if(getSettings($this->db,"smis-rs-tdokter-waktu-".$this->polislug,"0") =="1"){
			$adapter->add("Tanggal","waktu","date d M Y H:i");
		}else{
			$adapter->add("Tanggal","waktu","date d M Y");
		}
				
		$this->dbres = new RawatResponder($this->dbtable,$this->uitable,$adapter,$this->polislug);
		if ($this->dbres->is("save")) {
			$this->saveLastSetting();
            self::getProvitShareByService($this->db,$this->dbres,$this->polislug ,$this->carabayar,"smis-pv-tindakan-perawat-igd");
            /*hitung jumlah perawat*/
            $total_perawat=0;
            foreach($this->number as $rum=>$num){
                if(isset($_POST['id_perawat'.$num]) &&  $_POST['id_perawat'.$num]!="" && $_POST['id_perawat'.$num]!="0" && $_POST['id_perawat'.$num]!=0){
                    $total_perawat++;
                }
            }
            $this->dbres->addColumnFixValue("total_perawat",$total_perawat);
		}
        
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_tindakan_igd","PIGD","tindakan_igd","waktu","nama_tindakan","harga_tindakan");
        }
        
		$data = $this->dbres->command($_POST ['command']);
		echo json_encode($data);
	}
	
	private function initDokterSuperCommand(SuperCommand $supercommand){
		$head=array ('Nama','Jabatan',"NIP");
		$strict_dr=getSettings($this->db,"smis-rs-dokter-tindakan-igd-".$this->polislug,"0");
		$dktable = new Table($head,"",NULL,true);
		$dktable->setName("dokter_tindakan_perawat_igd")
				->setModel(Table::$SELECT);
		$dkadapter = new SimpleAdapter ();
		$dkadapter	->add("Jabatan","nama_jabatan")
					->add("Nama","nama")
					->add("NIP","nip");
		$dokter= new EmployeeResponder($this->db,$dktable,$dkadapter,"dokter");		
		$dokter->setStrict($strict_dr,$this->polislug);
		$supercommand->addResponder("dokter_tindakan_perawat_igd",$dokter);
	}
	
	private function initICDTindakan(SuperCommand $supercommand){
		$hader		= array ("Nama","ICD IX","Keterangan");
		$dktable 	= new Table($hader,"",NULL,true);
		$dktable	->setName("icdtindakan_tindakan_perawat_igd")
					->setModel(Table::$SELECT);
		$dkadapter 	= new SimpleAdapter ();
		$dkadapter 	->add("Nama","nama")
				   	->add("ICD IX","icd")
				   	->add("Keterangan","terjemah");
		$tarif 		= new ServiceResponder($this->db,$dktable,$dkadapter,"get_icdtindakan");
		$supercommand->addResponder("icdtindakan_tindakan_perawat_igd",$tarif);
	}
	
	private function initTarifSuperCommand(SuperCommand $supercommand){		
		$hader		= array ("Nama","Kelas","Tarif");
		$dktable 	= new Table($hader,"",NULL,true);
		$dktable	->setName("tarif_keperawatan_igd")
					->setModel(Table::$SELECT);
		$dkadapter 	= new SimpleAdapter ();
		$dkadapter	->add("Nama","nama")
					->add("Kelas","kelas","unslug")
					->add("Tarif","tarif","money Rp.");
		$tarif 		= new ServiceResponder($this->db,$dktable,$dkadapter,"get_keperawatan");
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db,"smis-rs-kelas-tperawat-igd-",$this->polislug,$_POST['noreg_pasien']);
			$tarif->addData("kelas",$data_kelas);
		}else{
			$data_kelas = getSettings($this->db,"smis-rs-kelas-" . $this->polislug,"");
			$tarif->addData("kelas",$data_kelas);
		}
		$supercommand->addResponder("tarif_keperawatan_igd",$tarif);
	}
	
	private function initPerawatSuperCommand(SuperCommand $supercommand,$command){
		$strict		= getSettings($this->db,"smis-rs-employee-tindakan-igd-".$this->polislug,"0");		
		$dkadapter  = new SimpleAdapter ();
		$dkadapter	->add("Jabatan","nama_jabatan")
					->add("Nama","nama")
					->add("NIP","nip");
		$head		= array('Nama','Jabatan',"NIP");
		$dktable 	= new Table($head,"",NULL,true);
		$dktable	->setName($command)
					->setModel(Table::$SELECT);
		$perawat 	= new EmployeeResponder($this->db,$dktable,$dkadapter,"perawat");
		$perawat	->setStrict($strict,$this->polislug);		
		$supercommand->addResponder($command,$perawat);
	}
	
	public function superCommand($super_command) {
		$super = new SuperCommand ();
		switch($super_command){
			case "dokter_tindakan_perawat_igd" 		  : $this->initDokterSuperCommand($super); break;
			case "tarif_keperawatan_igd" 	   		  : $this->initTarifSuperCommand($super); break;
			case "icdtindakan_tindakan_perawat_igd"   : $this->initICDTindakan($super); break;
			default							   		  : $this->initPerawatSuperCommand($super,$super_command); break; 
		}
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
    
	public function phpPreload() {
		require_once "rawat/class/resource/KelasResource.php";
		$this->loadLastSaveSetting();
		$this->uitable	->addModal("id","hidden","","")
						->addModal("nama_pasien","hidden","",$this->nama_pasien)
						->addModal("noreg_pasien","hidden","",$this->noreg)
						->addModal("nrm_pasien","hidden","",$this->nrm);
		
		if(getSettings($this->db,"smis-rs-tdokter-waktu-".$this->polislug,"0") =="1"){
			$this->uitable->addModal("waktu","datetime","Waktu",date("Y-m-d H:i"));
		}else{
			$this->uitable->addModal("waktu","date","Tanggal",date("Y-m-d"));
		}				
						
		$this->uitable->addModal("nama_dokter","chooser-tindakan_perawat_igd-dokter_tindakan_perawat_igd-Pilih Dokter","Dokter",$this->last_setup['dokter_nama'],"n",null,false,null,true  )
						->addModal("id_dokter","hidden","",$this->last_setup['dokter_id']);
		/**
		 * Menghitung jumlah counter untuk tindakan Perawat
		 * bisa menggunakan Setting -> tampilan -> JUmlah Perawat IGD
		 */		
		$counter	= getSettings($this->db,"smis-rs-total-tindakan-igd-".$this->polislug,"1")*1;
		$numero		= 1;
		foreach($this->number as $rum=>$num){
			if($numero<=$counter){
				$this->uitable	->addModal("nama_perawat".$num,"chooser-tindakan_perawat_igd-perawat_tindakan_perawat_igd".$num."-Pilih Perawat ".$rum,"Perawat ".$rum,$this->last_setup['nama_perawat'.$num]);	
			}else{
				$this->uitable->addModal("nama_perawat".$num,"hidden","","","n",null,false,null,false);
			}
			$this->uitable->addModal("id_perawat".$num,"hidden","",$this->last_setup['id_perawat'.$num]);
			$numero++;
		}
		
		$this->uitable	->addModal("nama_tindakan","chooser-tindakan_perawat_igd-tarif_keperawatan_igd-Pilih Tindakan","Tindakan","",'n',null,false)
						->addModal("id_tindakan","hidden","","0")
						->addModal("kelas","select","Kelas",KelasResource::getKelasOptionDefault()->getContent(),'n',null,true);
		$this->uitable->addModal("satuan","money","Harga","",'n',null,true);
		if($this->is_show_jumlah){
			$this->uitable	->addModal("jumlah","text","Jumlah","1",'n',"numeric",false,null,false,"save")
							->addModal("harga_tindakan","money","Total","",'n',null,true);
		}else{
			$this->uitable	->addModal("jumlah","hidden","","1",'n',null,true)
							->addModal("harga_tindakan","hidden","","",'n',null,true);
		}
		
		$icdtindakan	= getSettings($this->db,"smis-rs-icd-tindakan-igd-" . $this->polislug,"0")=="1";
		if($icdtindakan){
			$this->uitable	->addModal("nama_icd","chooser-tindakan_perawat_igd-icdtindakan_tindakan_perawat_igd-Pilih ICD","Nama ICD","","n",null,true,null,false  )
						  	->addModal("kode_icd","text","Kode ICD","","y",null,true)
						  	->addModal("ket_icd","text","Ket ICD","","y",null,true);
		}else{
			$this->uitable	->addModal("nama_icd","hidden","","","y",null,false,null,true  )
						  	->addModal("kode_icd","hidden","","y",null,true)
						  	->addModal("ket_icd","hidden","","y",null,true);
		}
		$this->uitable		->addModal("jaspel","hidden","","")
							->addModal("jaspel_lain_lain","hidden","","")
							->addModal("jaspel_penunjang","hidden","","");
        
		$modal = $this->uitable->getModal ();
		$modal ->setTitle($this->poliname);
		
		echo $this->uitable->getHtml ();
		echo $modal	->getHtml ();
		echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS  ("framework/smis/js/table_action.js");
		echo addCSS ("framework/bootstrap/css/datepicker.css");
		echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
		echo addJS  ("rawat/resource/js/tindakan_perawat_igd.js",false);
		
		/*Parsing ke Javascript*/
		$carabayar	= new Hidden("tpi_carabayar","",$this->carabayar);
		$poliname	= new Hidden("tpi_poliname","",$this->poliname);
		$polislug	= new Hidden("tpi_polislug","",$this->polislug);
		echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
	}
}
?>