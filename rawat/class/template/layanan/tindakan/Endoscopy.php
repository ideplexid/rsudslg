<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class Endoscopy extends LayananTemplate {
    private $harga;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->uitable = new Table ( array ('Tanggal','Dokter','Asisten','Kelas',"Biaya"), $this->getUiTableTitle("Endoscopy"), NULL, true );
		$this->uitable->setName ( "endoscopy" );
		$this->harga = array ();
	}
	public function superCommand($super_command) {
		$strict=getSettings($this->db, "smis-rs-employee-endoscopy-".$this->polislug, "0");
		$strict_dr=getSettings($this->db, "smis-rs-dokter-endoscopy-".$this->polislug, "0");
		$dktable = new Table ( array ('Nama','Jabatan',"NIP"), "", NULL, true );
		$dktable->setName ( "dokter_endoscopy" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$dokter = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		$dokter->setStrict($strict_dr,$this->polislug);
		
		$dktable = new Table ( array ('Nama','Jabatan',"NIP" ), "", NULL, true );
		$dktable->setName ( "asisten_endoscopy" );
		$dktable->setModel ( Table::$SELECT );
		$asisten = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "perawat" );
		$asisten->setStrict($strict,$this->polislug);
		
		$dktable = new Table ( array ("Kelas","Tarif","Keterangan" ), "", NULL, true );
		$dktable->setName ( "tarif_endoscopy" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Keterangan", "keterangan" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		
		
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_endoscopy" );
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-endoscopy-", $this->polislug, $_POST['noreg_pasien']);
			$tarif->addData ( "kelas", $data_kelas );
		}else{
			$data_kelas =getSettings ( $this->db, "smis-rs-kelas-" . $this->polislug, "" );
			$tarif->addData ( "kelas", $data_kelas );
		}
		
		$super = new SuperCommand ();
		$super->addResponder ( "dokter_endoscopy", $dokter );
		$super->addResponder ( "asisten_endoscopy", $asisten );
		$super->addResponder ( "tarif_endoscopy", $tarif );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	public function command($command) {
		$this->getDBTable("smis_rwt_endoscopy_");
        $adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "waktu", "date d-M-Y" );
		$adapter->add ( "Dokter", "nama_dokter" );
		$adapter->add ( "Asisten", "nama_asisten" );
		$adapter->add ( "Kelas", "kelas", "unslug" );
		$adapter->add ( "Biaya", "biaya", "money Rp." );
		
		$this->dbres = new RawatResponder ( $this->dbtable, $this->uitable, $adapter, $this->polislug );
		if ($this->dbres->is ( "save" )) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-endoscopy");
		}
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_endoscopy","EDC","endoscopy","waktu","nama_tindakan","biaya");
        }
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}
	public function phpPreload() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm );
		$this->uitable->addModal ( "waktu", "date", "Tanggal",  date("Y-m-d")  );
		$this->uitable->addModal ( "nama_dokter", "chooser-endoscopy-dokter_endoscopy-Pilih Dokter", "Dokter", "","y",null,false,null,true );
		$this->uitable->addModal ( "nama_asisten", "chooser-endoscopy-asisten_endoscopy-Pilih Asisten", "Asisten", "" );
		$this->uitable->addModal ( "id_dokter", "hidden", "", "" );
		$this->uitable->addModal ( "id_asisten", "hidden", "", "" );
		$this->uitable->addModal ( "kelas", "chooser-endoscopy-tarif_endoscopy-Pilih Tarif", "Kelas", "", 'n', null, false );
		$this->uitable->addModal ( "biaya", "money", "Harga", "", 'n', null, true );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( $this->poliname );
		$carabayar=new Hidden("edc_carabayar","",$this->carabayar);
		$poliname=new Hidden("edc_poliname","",$this->poliname);
		$polislug=new Hidden("edc_polislug","",$this->polislug);
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
        echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "rawat/resource/js/endoscopy.js",false );
	}
}

?>