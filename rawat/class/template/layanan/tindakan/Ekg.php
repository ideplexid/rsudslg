<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class Ekg extends LayananTemplate {
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->uitable = new Table ( array ('Tanggal','Pengirim','Pembaca','Petugas',"Biaya"), $this->getUiTableTitle("Electro Cardio Graph"), NULL, true );
		$this->uitable->setName ( "ekg" );
	}
	public function command($command) {
		$this->getDBTable("smis_rwt_ekg_");
        $adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "waktu", "date d-M-Y" );
		$adapter->add ( "Pengirim", "nama_dokter_pengirim" );
		$adapter->add ( "Pembaca", "nama_dokter_pembaca" );
		$adapter->add ( "Petugas", "nama_petugas" );
		$adapter->add ( "Kelas", "kelas", "unslug" );
		$adapter->add ( "Biaya", "biaya", "money Rp." );
		
		$this->dbres = new RawatResponder( $this->dbtable, $this->uitable, $adapter, $this->polislug );
		if ($this->dbres->is ( "save" )) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-ekg");
		}
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_ekg","EKG","ekg","waktu","nama_dokter_pembaca","biaya");
        }
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}
	public function superCommand($super_command) {
		$strict=getSettings($this->db, "smis-rs-employee-ekg-".$this->polislug, "0");
		$strict_dr=getSettings($this->db, "smis-rs-dokter-ekg-".$this->polislug, "0");
		$strict_drp=getSettings($this->db, "smis-rs-dokter-pembaca-ekg-".$this->polislug, "0");
		
		$dktable = new Table ( array ('Nama','Jabatan',"NIP"), "", NULL, true );
		$dktable->setName ( "dokter_pengirim_ekg" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$dokter_pengirim = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		$dokter_pengirim->setStrict($strict_dr,$this->polislug);
		
		$dktable = new Table ( array ('Nama','Jabatan',"NIP" ), "", NULL, true );
		$dktable->setName ( "dokter_pembaca_ekg" );
		$dktable->setModel ( Table::$SELECT );
		$dokter_pembaca = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		$dokter_pembaca->setStrict($strict_drp,$this->polislug);
		
		$dktable = new Table ( array ('Nama','Jabatan',"NIP"), "", NULL, true );
		$dktable->setName ( "petugas_ekg" );
		$dktable->setModel ( Table::$SELECT );
		$ekg = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "perawat" );
		$ekg->setStrict($strict,$this->polislug);
		
		$dktable = new Table ( array ("Kelas","Tarif","Keterangan"), "", NULL, true );
		$dktable->setName ( "tarif_ekg" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Keterangan", "keterangan" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_ekg" );
		if(isset($_POST['noreg_pasien'])){
			$data_kelas = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-ekg-", $this->polislug, $_POST['noreg_pasien']);
			$tarif->addData ( "kelas", $data_kelas );
		}else{
			$data_kelas =getSettings ( $this->db, "smis-rs-kelas-" . $this->polislug, "" );
			$tarif->addData ( "kelas", $data_kelas );
		}
		
		
		$super = new SuperCommand ();
		$super->addResponder ( "dokter_pengirim_ekg", $dokter_pengirim );
		$super->addResponder ( "dokter_pembaca_ekg", $dokter_pembaca );
		$super->addResponder ( "petugas_ekg", $ekg );
		$super->addResponder ( "tarif_ekg", $tarif );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm );
		$this->uitable->addModal ( "waktu", "date", "Tanggal",  date("Y-m-d")  );
		$this->uitable->addModal ( "nama_dokter_pengirim", "chooser-ekg-dokter_pengirim_ekg-Pengirim", "Pengirim", "" );
		$this->uitable->addModal ( "nama_dokter_pembaca", "chooser-ekg-dokter_pembaca_ekg-Pembaca", "Pembaca", "" );
		$this->uitable->addModal ( "nama_petugas", "chooser-ekg-petugas_ekg-Petugas", "Petugas", "" );
		$this->uitable->addModal ( "id_dokter_pengirim", "hidden", "", "" );
		$this->uitable->addModal ( "id_dokter_pembaca", "hidden", "", "" );
		$this->uitable->addModal ( "id_petugas", "hidden", "", "" );
		$this->uitable->addModal ( "kelas", "chooser-ekg-tarif_ekg-Tarif EKG", "Kelas", "", 'n', null, true );
		$this->uitable->addModal ( "biaya", "money", "Harga", "", 'n', null, true );
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( $this->poliname );
		$carabayar=new Hidden("ekg_carabayar","",$this->carabayar);
		$poliname=new Hidden("ekg_poliname","",$this->poliname);
		$polislug=new Hidden("ekg_polislug","",$this->polislug);
        echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "rawat/resource/js/ekg.js",false );
	}
	
}

?>