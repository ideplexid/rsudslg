<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class RecoveryRoom extends LayananTemplate {
	private $list_harga_kamar;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar,$titipan);
		$this->uitable = new Table ( array (
				'Tanggal',
				'Petugas',
				'Asisten',
				"Harga"
		), "Recovery Room " . $poliname, NULL, true );
		$this->uitable->setName ( "rr" );
		$this->list_harga_kamar = null;
	}
	public function superCommand($super_command) {
		$super = new SuperCommand ();
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );

		$dktable = new Table ( array (
				'Nama',
				'Jabatan',
				"NIP"
		), "", NULL, true );
		$dktable->setName ( "asisten_rr" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "dokter" );
		$super->addResponder ( "asisten_rr", $employee );

		$dktable = new Table ( array (
				'Nama',
				'Jabatan',
				"NIP"
		), "", NULL, true );
		$dktable->setName ( "petugas_rr" );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt, "team" );
		$super->addResponder ( "petugas_rr", $employee );

		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	public function command($command) {
		$rradapter = new SimpleAdapter ();
		$rradapter->add ( "Tanggal", "waktu", "date d M Y" );
		$rradapter->add ( "Asisten", "nama_asisten" );
		$rradapter->add ( "Petugas", "nama_petugas" );
		$rradapter->add ( "Harga", "harga", "money Rp." );

        require_once "rawat/class/adapter/TagihanAdapter.php";
        $adapter2 = new TagihanAdapter ($this->db,$this->polislug);
        $adapter2 ->setAdapterName("rr");
        $this->getDBTable("smis_rwt_rr_","rr",$adapter2);

		//$this->getDBTable("smis_rwt_rr_");
        $this->dbres = new RawatResponder( $this->dbtable, $this->uitable, $rradapter, $this->polislug );
		if ($this->dbres->is ( "save" )) {
            self::getProvitShareByService($this->db,$this->dbres, $this->polislug ,$this->carabayar, "smis-pv-rr");
		}
        if($this->dbres->isSave() || $this->dbres->isDel()){
            $this->dbres->setAccounting("get_accounting_rr","RR","recovery_room","waktu","nama_asisten","harga");
        }
		$data = $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}
	public function phpPreload() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm );
		$this->uitable->addModal ( "waktu", "date", "Tanggal", date ( 'Y-m-d' ),"n",null,false,null,false ,"nama_asisten" );
		$this->uitable->addModal ( "nama_asisten", "chooser-rr-asisten_rr", "Asisten", "","y",null,false,null,true);
		$this->uitable->addModal ( "id_asisten", "hidden", "", "" );
		$this->uitable->addModal ( "nama_petugas", "chooser-rr-petugas_rr", "Petugas", "" );
		$this->uitable->addModal ( "id_petugas", "hidden", "", "" );
		$this->uitable->addModal ( "harga", "money", "Biaya RR", "","n",null,false,null,false ,"save");
		$modal = $this->uitable->getModal ();
		$modal->setTitle ( $this->poliname );
        $carabayar=new Hidden("rr_carabayar","",$this->carabayar);
		$poliname=new Hidden("rr_poliname","",$this->poliname);
		$polislug=new Hidden("rr_polislug","",$this->polislug);
        echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $this->uitable->getHtml ();
		echo $modal->getHtml();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "rawat/resource/js/recovery_room.js",false );
	}
	
}

?>