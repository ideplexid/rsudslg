<?php 

require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");

class RencanaBPJS extends ModulTemplate {
	private $service;
	private $db;
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
	}
	public function initialize() {
		$data = $_POST;

		$nrm = isset ( $_POST ['nrm_pasien'] ) ? $_POST ['nrm_pasien'] : "";
		$nama = isset ( $_POST ['nama'] ) ? $_POST ['nama'] : (isset ( $_POST ['nama_pasien'] ) ? $_POST ['nama_pasien'] : "");
		$noreg = isset ( $_POST ['no_reg'] ) ? $_POST ['no_reg'] : (isset ( $_POST ['noreg_pasien'] ) ? $_POST ['noreg_pasien'] : "");
		$implement = isset ( $_POST ['prototype_implement'] ) ? $_POST ['prototype_implement'] : "";
		$polislug = isset ( $_POST ['prototype_slug'] ) ? $_POST ['prototype_slug'] : "";
		$gol_umur = isset ( $_POST ['gol_umur'] ) ? $_POST ['gol_umur'] : "TGL LAHIR TDK VALID";
		$jk = isset ( $_POST ['jk'] ) ? $_POST ['jk'] : "0";
		$kunjungan = isset ( $_POST ['kunjungan'] ) ? $_POST ['kunjungan'] : "Baru";
		$carabayar = isset ( $_POST ['carabayar'] ) ? $_POST ['carabayar'] : "Umum";

		$data ['polislug'] = $polislug;
		$data ['noreg_pasien'] = $noreg;
		$data ['nama_pasien'] = $nama;
		$data ['nrm_pasien'] = $nrm;
		$data ['action'] = "rencana_bpjs";
		$data ['page'] = "rawat";
		$data ['gol_umur'] = $gol_umur;
		$data ['jk'] = $jk;
		$data ['kunjungan'] = $kunjungan;
		$data ['carabayar'] = $carabayar;

		$service = new ServiceConsumer ( $this->db, "get_rencana_bpjs", $data, "medical_record" );
		$service->execute ();
		echo $service->getContent ();
	}
}



?>