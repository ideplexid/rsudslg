<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

class TagihanRumus extends LayananTemplate {    
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ($db,$polislug,$poliname,$nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar);		
	}
    
    public function getKaryawanCommand($super_command){
		$eadapt = new SimpleAdapter ();
		$eadapt->add ( "Jabatan", "nama_jabatan" );
		$eadapt->add ( "Nama", "nama" );
		$eadapt->add ( "NIP", "nip" );
		$head_karyawan=array ('Nama','Jabatan',"NIP");	
	
		/*Spesial Case untuk data Karyawan*/
		$dktable = new Table ( $head_karyawan, "", NULL, true );
		$dktable->setName ( $super_command );
		$dktable->setModel ( Table::$SELECT );
		$employee = new EmployeeResponder ( $this->db, $dktable, $eadapt,  "dokter");
		return $employee;
	}
    
    public function getRumusCommand($super_command){
        $uitable=new Table(array("No.","Nama","Biaya","Potongan","Tagihan","Keterangan"));
        $uitable->setName("rumus_tagihan_rumus");
        $uitable->setModel(Table::$SELECT);
        $adapter=new SimpleAdapter();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add("Nama","nama");
        $adapter->add("Biaya","rumus_biaya");
        $adapter->add("Potongan","rumus_potongan");
        $adapter->add("Tagihan","rumus_tagihan");
        $adapter->add("Keterangan","keterangan");
        $dbresponder=new ServiceResponder($this->db,$uitable,$adapter,"get_rumus_tagihan","kasir");
        return $dbresponder;
    }
    
    public function getRumusPVCommand($super_command){
        $grup_slug=getSettings($db,"smis-rs-slug-grup-pv-".$this->polislug,"");
        $uitable=new Table(array("No.","Kode","Unit","Ruang Awal","Ruang Akhir","Keterangan"));
        $uitable->setName("keterangan_pv_tagihan_rumus");
        $uitable->setModel(Table::$SELECT);        
        $adapter=new SimpleAdapter();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add("Unit","unit");
        $adapter->add("Kode","kode");
        $adapter->add("Ruang Awal","ruang_awal");
        $adapter->add("Ruang Akhir","ruang_akhir");
        $adapter->add("Keterangan","keterangan");
        $dbresponder=new ServiceResponder($this->db,$uitable,$adapter,"get_formula_pv","manajemen");
        if($grup_slug!=""){
            $dbresponder->addData("grup_slug",$grup_slug);
        }
        $dbresponder->addData("ruangan",$this->polislug);
        return $dbresponder;
    }
	
	public function superCommand($super_command) {	
        $super=new SuperCommand();            
        switch ($super_command){
           case "rumus_tagihan_rumus"               : $super->addResponder("rumus_tagihan_rumus",$this->getRumusCommand($super));          break;
           case "keterangan_pv_tagihan_rumus"       : $super->addResponder("keterangan_pv_tagihan_rumus",$this->getRumusPVCommand($super));          break;
           default                                  : $super->addResponder ( $super_command, $this->getKaryawanCommand($super_command) );  break;
        }
        $supr=$super->initialize();
        if($supr!=null){
            echo $supr;
        }
        return;
	}
	
    /**
     * @brief get detail of formula by given id
     * @param String $id_rumus 
     * @return Array group and kelas name   
     */
    private function getDetailRumus($id_rumus){
        $serv=new ServiceConsumer($this->db,"get_formula_pv",NULL,"manajemen");
        $serv->addData("id",$id_rumus);
        $serv->addData("command","edit");
        $serv->execute();
        $rumus=$serv->getContent();
        
        $ruangan="";
        if($rumus['perawat_irna_a']!="0"){
            $ruangan.="[irna_a]";
        }
        if($rumus['perawat_irna_b']!="0"){
            $ruangan.="[irna_b]";
        }
        if($rumus['perawat_irna_1']!="0"){
            $ruangan.="[irna_b1]";
        }
        if($rumus['perawat_irna_c']!="0"){
            $ruangan.="[irna_c]";
        }
        if($rumus['perawat_irna_e']!="0"){
            $ruangan.="[irna_e]";
        }
        if($rumus['perawat_irna_f']!="0"){
            $ruangan.="[irna_f]";
        }
        if($rumus['perawat_irna_g']!="0"){
            $ruangan.="[irna_g]";
        }
        if($rumus['perawat_paviliun']!="0"){
            $ruangan.="[paviliun]";
        }
        
        $kelas="";
        if($rumus['kelas_awal']!=""){
           $kelas.="[".$rumus['kelas_awal']."]"; 
        }
        
        if($rumus['kelas_akhir']!=""){
           $kelas.="[".$rumus['kelas_akhir']."]"; 
        }
        
        
        
        return array("ruangan"=>$ruangan,"kelas"=>$kelas);
    }
    
    /**
     * @brief mengambil berapa lama rawat dari masing-masing ruangan 
     *          berdasarkan group ruangan
     * @param Array $hasil kode ruangan yang termasuk 
     * @param String $noreg adalah nomor registrasi pasien 
     * @return  array $hasil dari lama rawat di masing-masing ruangan
     */
    public function getLamaRawat($hasil,$noreg){
        $serv=new ServiceConsumer($this->db,"get_lama_rawat_pv");
        $serv->addData("group_name",$hasil['ruangan']);
        $serv->addData("class_name",$hasil['kelas']);
        $serv->addData("noreg_pasien",$noreg);
        $serv->execute();
        $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
        $content=$serv->getContent();
        echo json_encode($content);
        
        $data_ruangan=array();
        $data_kelas=array();
        $data_history=array();
        foreach($content as $key=>$value){
            /* data untuk durasi kelas */
            if($value['durasi']>0){
                if(!isset($data_kelas[$value['kelas']])){
                    $data_kelas[$value['kelas']]=0;
                }
                $data_kelas[$value['kelas']]+=$value['durasi'];
            }
            /* data untuk durasi ruangan */
            if($value['durasi']>0){
                if(!isset($data_ruangan[$value['ruangan']])){
                    $data_ruangan[$value['ruangan']]=0;
                }
                $data_ruangan[$value['ruangan']]+=$value['durasi'];
            }
            /*history_ruang*/
            //if($value['durasi']>0){
                $data_history[]=$value;
            //}            
        }
        $hasil=array();
        $hasil['ruangan']=$data_ruangan;
        $hasil['kelas']=$data_kelas;
        $hasil['history_ruang']=$data_history;
        return $hasil;
    }
       
	public function command($command) {
		if($_POST['command']=="save"){
			$ser=new ServiceConsumer($this->db,"change_tagihan_rumus",NULL,"kasir");
            $ser->addData("id_dpjp_satu",$_POST['id_dpjp_satu']);
            $ser->addData("nama_dpjp_satu",$_POST['nama_dpjp_satu']);
            $ser->addData("id_dpjp_dua",$_POST['id_dpjp_dua']);
            $ser->addData("nama_dpjp_dua",$_POST['nama_dpjp_dua']);
            $ser->addData("id_dpjp_tiga",$_POST['id_dpjp_tiga']);
            $ser->addData("nama_dpjp_tiga",$_POST['nama_dpjp_tiga']);
            $ser->addData("id_konsul_satu",$_POST['id_konsul_satu']);
            $ser->addData("nama_konsul_satu",$_POST['nama_konsul_satu']);
            $ser->addData("id_konsul_dua",$_POST['id_konsul_dua']);
            $ser->addData("nama_konsul_dua",$_POST['nama_konsul_dua']);
            $ser->addData("id_anak",$_POST['id_anak']);
            $ser->addData("nama_anak",$_POST['nama_anak']);
            $ser->addData("id_igd",$_POST['id_igd']);
            $ser->addData("nama_igd",$_POST['nama_igd']);
            $ser->addData("id_anastesi",$_POST['id_anastesi']);
            $ser->addData("nama_anastesi",$_POST['nama_anastesi']);
            $ser->addData("id_umum",$_POST['id_umum']);
            $ser->addData("nama_umum",$_POST['nama_umum']);
            $ser->addData("id_spesialis",$_POST['id_spesialis']);
            $ser->addData("nama_spesialis",$_POST['nama_spesialis']);
            $ser->addData("biaya_dpjp_satu",$_POST['biaya_dpjp_satu']);
            $ser->addData("biaya_dpjp_dua",$_POST['biaya_dpjp_dua']);
            $ser->addData("biaya_dpjp_tiga",$_POST['biaya_dpjp_tiga']);
            $ser->addData("id_rumus_pv",$_POST['id_rumus_pv']);
            $ser->addData("keterangan_pv",$_POST['keterangan_pv']);
			$ser->addData("noreg_pasien",$_POST['noreg_pasien']);
			$ser->addData("nama_pasien",$_POST['nama_pasien']);
			$ser->addData("nrm_pasien",$_POST['nrm_pasien']);
            $ser->addData("kelas_plafon",$_POST['kelas_plafon']);
            $ser->addData("kode_inacbg",$_POST['kode_inacbg']);
            $ser->addData("ruang_pelayanan",ArrayAdapter::format("unslug",$this->polislug));
            $ser->addData("id_rumus",$_POST['id_rumus']);
            $ser->addData("nama_rumus",$_POST['nama_rumus']);
            $ser->addData("deskripsi_inacbg",$_POST['deskripsi_inacbg']);
            $ser->addData("rumus_tagihan",$_POST['rumus_tagihan']);
            $ser->addData("rumus_biaya",$_POST['rumus_biaya']);
            $ser->addData("rumus_potongan",$_POST['rumus_potongan']);
            $ser->addData("tarif_plafon_inacbg",$_POST['tarif_plafon_inacbg']);
            $ser->addData("tarif_plafon_kelas1",$_POST['tarif_plafon_kelas1']);
            
            $ser->addData("mulai_dpjp_satu",$_POST['mulai_dpjp_satu']);
            $ser->addData("mulai_dpjp_dua",$_POST['mulai_dpjp_dua']);
            $ser->addData("mulai_dpjp_tiga",$_POST['mulai_dpjp_tiga']);
            
            $ser->addData("selesai_dpjp_satu",$_POST['selesai_dpjp_satu']);
            $ser->addData("selesai_dpjp_dua",$_POST['selesai_dpjp_dua']);
            $ser->addData("selesai_dpjp_tiga",$_POST['selesai_dpjp_tiga']);
            
            
            $hasil_grup=$this->getDetailRumus($_POST['id_rumus_pv']);
            $hasil_array=$this->getLamaRawat($hasil_grup,$_POST['noreg_pasien']);
            foreach($hasil_array['ruangan'] as $key=>$value){
                $ser->addData($key,$value);
            }
            foreach($hasil_array['kelas'] as $key=>$value){
                $ser->addData("durasi_".$key,$value);
            }
            $ser->addData("history_ruang",$hasil_array['history_ruang']);
			$ser->execute();
		}
		return NULL;
	}

	public function phpPreload() {
        
        $ser=new ServiceConsumer($this->db,"get_tagihan_rumus",NULL,"kasir");
        $ser->addData("noreg_pasien",$this->noreg);
        $ser->execute();
        $data=$ser->getContent();
        
        $kelas_pasien=new OptionBuilder();
        $kelas_pasien->addSingle("",($data['kelas_plafon']==""?"1":"0"));
        $kelas_pasien->addSingle("Kelas I",($data['kelas_plafon']=="Kelas I"?"1":"0"));
        $kelas_pasien->addSingle("Kelas II",($data['kelas_plafon']=="Kelas II"?"1":"0"));
        $kelas_pasien->addSingle("Kelas III",($data['kelas_plafon']=="Kelas III"?"1":"0"));
        	
		$this->uitable=new Table(array("No.","Waktu","Dokter"),"",null,false);
		$this->uitable->setName("tagihan_rumus");
		$this->uitable->setFooterVisible(false);
        
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "kelas_plafon", "select", "Kelas Pasien", $kelas_pasien->getContent(),"n" );
        $this->uitable->addModal ( "kode_inacbg", "text", "Kode INACBG", $data["kode_inacbg"],"n" );
        $this->uitable->addModal ( "ruang_pelayanan", "text", "Ruangan", ArrayAdapter::format("unslug",$this->polislug),"n",null,true );        
		$this->uitable->addModal ( "id_rumus", "hidden", "", $data["id_rumus"],"n",null,true );		
		$this->uitable->addModal ( "nama_rumus", "chooser-tagihan_rumus-rumus_tagihan_rumus-Pilih Rumus", "Rumus", $data["nama_rumus"],"n" );
		$this->uitable->addModal ( "deskripsi_inacbg", "text", "Deskripsi", $data["deskripsi_inacbg"],"n" );
        $this->uitable->addModal ( "rumus_tagihan", "hidden", "", $data["rumus_tagihan"],"n" );
		$this->uitable->addModal ( "rumus_biaya", "hidden", "", $data["rumus_biaya"],"n" );
		$this->uitable->addModal ( "rumus_potongan", "hidden", "", $data["rumus_potongan"],"n" );
		$this->uitable->addModal ( "tarif_plafon_inacbg", "money", "Plafon INACBG", $data["tarif_plafon_inacbg"],"n",null,false );
		$this->uitable->addModal ( "tarif_plafon_kelas1", "money", "Plafon Kelas I", $data["tarif_plafon_kelas1"],"n",null,false );
        $this->uitable->addModal ( "total_biaya", "money", "Total Biaya", $data["total_biaya"],"y",null,true );
		$this->uitable->addModal ( "total_potongan", "money", "Total Potongan", $data["total_potongan"],"y",null,true );
		$this->uitable->addModal ( "total_tagihan", "money", "Total Tagihan", $data["total_tagihan"],"y",null,true );
		
        if(getSettings($this->db,"smis-rs-rumus-pv-anastesi-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "nama_anastesi", "chooser-tagihan_rumus-anastesi_tagihan_rumus-Dokter Anastesi", "Dr. Anastesi", $data["nama_anastesi"],"y",null );
        else $this->uitable->addModal ( "nama_anastesi", "text", "Dr. Anastesi", $data["nama_anastesi"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-anak-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "nama_anak", "chooser-tagihan_rumus-anak_tagihan_rumus-Dokter Anak", "Dr. Anak", $data["nama_anak"],"y",null );
		else $this->uitable->addModal ( "nama_anak", "text", "Dr. Anak", $data["nama_anak"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-igd-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "nama_igd", "chooser-tagihan_rumus-igd_tagihan_rumus-Dokter IGD", "Dr. IGD", $data["nama_igd"],"y",null );
		else $this->uitable->addModal ( "nama_igd", "text", "Dr. IGD", $data["nama_igd"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-umum-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "nama_umum", "chooser-tagihan_rumus-umum_tagihan_rumus-Dokter Umum", "Dr. Umum", $data["nama_umum"],"y",null );
        else $this->uitable->addModal ( "nama_umum", "text", "Dr. Umum", $data["nama_umum"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-spesialis-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "nama_spesialis", "chooser-tagihan_rumus-spesialis_tagihan_rumus-Dokter Spesialis", "Dr. Spesialis", $data["nama_spesialis"],"y",null );        
		else $this->uitable->addModal ( "nama_spesialis", "text", "Dr. Spesialis", $data["nama_spesialis"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-dpjp-satu-".$this->polislug,"0")=="1") {
            $this->uitable->addModal ( "nama_dpjp_satu", "chooser-tagihan_rumus-dpjp_satu_tagihan_rumus-Dr. DPJP I", "DPJP I", $data["nama_dpjp_satu"],"y",null);
            $this->uitable->addModal ( "mulai_dpjp_satu", "datetime", "Mulai DPJP I", $data["mulai_dpjp_satu"],"y",null);
            $this->uitable->addModal ( "selesai_dpjp_satu", "datetime", "Selesai DPJP I", $data["selesai_dpjp_satu"],"y",null);
            $this->uitable->addModal ( "durasi_dpjp_satu", "text", "(Jam) DPJP I", $data["durasi_dpjp_satu"],"y",null,true);
        }else {
            $this->uitable->addModal ( "nama_dpjp_satu", "text", "Dr. DPJP I", $data["nama_dpjp_satu"],"y",null,true );
            $this->uitable->addModal ( "mulai_dpjp_satu", "hidden", "", $data["mulai_dpjp_satu"],"y",null);
            $this->uitable->addModal ( "selesai_dpjp_satu", "hidden", "", $data["selesai_dpjp_satu"],"y",null);
        }
        
        if(getSettings($this->db,"smis-rs-rumus-pv-dpjp-dua-".$this->polislug,"0")=="1"){
            $this->uitable->addModal ( "nama_dpjp_dua", "chooser-tagihan_rumus-dpjp_dua_tagihan_rumus-Dr. DPJP II", "DPJP II", $data["nama_dpjp_dua"],"y",null );
            $this->uitable->addModal ( "mulai_dpjp_dua", "datetime", "Mulai DPJP II", $data["mulai_dpjp_dua"],"y",null);
            $this->uitable->addModal ( "selesai_dpjp_dua", "datetime", "Selesai DPJP II", $data["selesai_dpjp_dua"],"y",null);
            $this->uitable->addModal ( "durasi_dpjp_dua", "text", "(Jam) DPJP II", $data["durasi_dpjp_dua"],"y",null,true);
        } else {
            $this->uitable->addModal ( "nama_dpjp_dua", "text", "Dr. DPJP II", $data["nama_dpjp_dua"],"y",null,true );
            $this->uitable->addModal ( "mulai_dpjp_dua", "hidden", "", $data["mulai_dpjp_dua"],"y",null);
            $this->uitable->addModal ( "selesai_dpjp_dua", "hidden", "", $data["selesai_dpjp_dua"],"y",null);
        }
        
        if(getSettings($this->db,"smis-rs-rumus-pv-dpjp-tiga-".$this->polislug,"0")=="1"){
            $this->uitable->addModal ( "nama_dpjp_tiga", "chooser-tagihan_rumus-dpjp_tiga_tagihan_rumus-Dr. DPJP III", "DPJP III", $data["nama_dpjp_tiga"],"y",null );
            $this->uitable->addModal ( "mulai_dpjp_tiga", "datetime", "Mulai DPJP III", $data["mulai_dpjp_tiga"],"y",null);
            $this->uitable->addModal ( "selesai_dpjp_tiga", "datetime", "Selesai DPJP III", $data["selesai_dpjp_tiga"],"y",null);
            $this->uitable->addModal ( "durasi_dpjp_tiga", "text", "(Jam) DPJP III", $data["durasi_dpjp_tiga"],"y",null,true);
        }else {
            $this->uitable->addModal ( "nama_dpjp_tiga", "text", "Dr. DPJP III", $data["nama_dpjp_tiga"],"y",null,true );
            $this->uitable->addModal ( "mulai_dpjp_tiga", "hidden", "", $data["mulai_dpjp_tiga"],"y",null);
            $this->uitable->addModal ( "selesai_dpjp_tiga", "hidden", "", $data["selesai_dpjp_tiga"],"y",null);
        }
        
        if(getSettings($this->db,"smis-rs-rumus-pv-biaya-dpjp-satu-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "biaya_dpjp_satu", "money", "Uang DPJP I", $data["biaya_dpjp_satu"],"y",null );
		else $this->uitable->addModal ( "biaya_dpjp_satu", "text", "Uang DPJP I", $data["biaya_dpjp_satu"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-biaya-dpjp-dua-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "biaya_dpjp_dua", "money", "Uang DPJP II", $data["biaya_dpjp_dua"],"y",null );
		else $this->uitable->addModal ( "biaya_dpjp_dua", "text", "Uang DPJP II", $data["biaya_dpjp_dua"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-biaya-dpjp-tiga-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "biaya_dpjp_tiga", "money", "Uang DPJP III", $data["biaya_dpjp_tiga"],"y",null );		
        else $this->uitable->addModal ( "biaya_dpjp_tiga", "text", "Uang DPJP III", $data["biaya_dpjp_tiga"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-konsul-satu-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "nama_konsul_satu", "chooser-tagihan_rumus-konsul_satu_tagihan_rumus-Dokter Konsul I", "Konsul I", $data["nama_konsul_satu"],"y",null);
		else $this->uitable->addModal ( "nama_konsul_satu", "text", "Dr. Konsul I", $data["nama_konsul_satu"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-konsul-satu-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "nama_konsul_dua", "chooser-tagihan_rumus-konsul_dua_tagihan_rumus-Dokter Konsul II", "Konsul II", $data["nama_konsul_dua"],"y",null );
        else $this->uitable->addModal ( "nama_konsul_dua", "text", "Dr. Konsul II", $data["nama_konsul_dua"],"y",null,true );
        
        if(getSettings($this->db,"smis-rs-rumus-pv-keterangan-".$this->polislug,"0")=="1") 
            $this->uitable->addModal ( "keterangan_pv", "chooser-tagihan_rumus-keterangan_pv_tagihan_rumus-Rumus Provit Share", "Rumus Provit Share", $data["keterangan_pv"],"y",null );
		else $this->uitable->addModal ( "keterangan_pv", "text", "Provit Share", $data["keterangan_pv"],"y",null,true );
        
        $this->uitable->addModal ( "id_anastesi", "hidden", "", $data["id_anastesi"],"y",null,true );
		$this->uitable->addModal ( "id_anak", "hidden", "", $data["id_anak"],"y",null,true );
		$this->uitable->addModal ( "id_spesialis", "hidden", "", $data["id_spesialis"],"y",null,true );
		$this->uitable->addModal ( "id_dpjp_dua", "hidden", "", $data["id_dpjp_dua"],"y",null,true );
		$this->uitable->addModal ( "id_dpjp_tiga", "hidden", "", $data["id_dpjp_tiga"],"y",null,true );
		$this->uitable->addModal ( "id_konsul_satu", "hidden", "", $data["id_konsul_satu"],"y",null,true );
		$this->uitable->addModal ( "id_konsul_dua", "hidden", "", $data["id_konsul_dua"],"y",null,true );
		$this->uitable->addModal ( "id_rumus_pv", "hidden", "", $data["id_rumus_pv"],"y",null,true );
		$this->uitable->addModal ( "id_igd", "hidden", "", $data["id_igd"],"y",null,true );
        $this->uitable->addModal ( "id_umum", "hidden", "", $data["id_umum"],"y",null,true );
        $this->uitable->addModal ( "id_dpjp_satu", "hidden", "", $data["id_dpjp_satu"],"y",null,true );
        
		$modal = $this->uitable->getModal ();
		$form=$modal->getForm()->setTitle("Tagihan Rumus");	
                
		$btn=new Button("","","Simpan");
		$btn->setAction("tagihan_rumus.save()");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btn->setClass("btn-primary");
		$btn->setIcon("fa fa-save");
		$form->addElement("",$btn);
		
		$carabayar=new Hidden("tagihan_rumus_carabayar","",$this->carabayar);
		$poliname=new Hidden("tagihan_rumus_poliname","",$this->poliname);
		$polislug=new Hidden("tagihan_rumus_polislug","",$this->polislug);
		
		echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $form->getHtml ();
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addJS ( "rawat/resource/js/tagihan_rumus.js",false );        
        echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
}

?>