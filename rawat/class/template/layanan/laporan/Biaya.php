<?php
require_once "smis-framework/smis/template/ModulTemplate.php";
require_once "rawat/class/template/TagihanService.php";
require_once "rawat/class/table/BiayaTable.php";

class Biaya extends ModulTemplate {
	private $poliname;
	private $polislug;
	private $dbres;
	private $dbtable;
	private $nama_pasien;
	private $nrm;
	private $noreg;
	private $db;
	private $carabayar;
	private $uitable;
	private $header;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien, $carabayar) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$this->carabayar= $carabayar;
		$this->nama_pasien = $nama_pasien;
		$this->nrm = $nrm_pasien;
		$this->noreg = $noreg_pasien;
		$this->header=array();
		if(getSettings($db, "smis-rs-printout-waktu-" . $this->polislug, "0")=="1") $this->header[]="Waktu";
		if(getSettings($db, "smis-rs-printout-jenis-" . $this->polislug, "0")=="1") $this->header[]="Jenis";
		if(getSettings($db, "smis-rs-printout-layanan-" . $this->polislug, "0")=="1") $this->header[]="Jenis BOA";
		if(getSettings($db, "smis-rs-printout-jumlah-" . $this->polislug, "0")=="1") $this->header[]="Jumlah";
		if(getSettings($db, "smis-rs-printout-satuan-" . $this->polislug, "0")=="1") $this->header[]="Satuan";
		if(getSettings($db, "smis-rs-printout-biaya-" . $this->polislug, "0")=="1") $this->header[]="Biaya";
		if(getSettings($db, "smis-rs-printout-keterangan-" . $this->polislug, "0")=="1") $this->header[]="Keterangan";
		if(getSettings($db,"smis-rs-printout-table-" . $this->polislug, "1")=="1"){
			$this->uitable=new BiayaTable($this->header);
		}else{
			$this->uitable=new Table($this->header);
		} 
		$this->uitable=new BiayaTable($this->header);
		$this->uitable->setFooterControlVisible(false);
		$this->uitable->setEditButtonEnable(false);
		$this->uitable->setDelButtonEnable(false);
		$this->uitable->setAddButtonEnable(false);
		$this->uitable->setName("biaya");
	}	
	
	public function phpPreLoad(){	
		$datapasien=new DBTable($this->db, "smis_rwt_antrian_".$this->polislug);
		$pasien=$datapasien->select(array("no_register"=>$this->noreg));
		$total_0=count($this->header);
		$total_1=floor($total_0/2);
		$total_11=floor($total_1/2);
		$total_12=$total_1-$total_11;
		$total_2=$total_0-$total_1;
		$total_21=floor($total_2/2);
		$total_22=$total_2-$total_21;
		
		$nama=getSettings($this->db, "smis_autonomous_title", "");		
		$head1="<tr> 
					<td colspan='$total_0' class='center'><h4>".strtoupper($this->poliname."  ".$nama)."</h4><h5>Daftar Tagihan Obat, Alat, Jasa, Oksigen, dan Kamar</h5></td> 
				</tr>";
		$head3="<tr> 
					<td colspan='$total_11'>Nama</td> 
					<td colspan='$total_12'>".$pasien->nama_pasien."</td> 
					<td colspan='$total_21'>NRM</td> 
					<td colspan='$total_22'>".ArrayAdapter::format("only-digit8", $pasien->nrm_pasien)."</td> 
				</tr>";
		$head4="<tr> 
					<td colspan='$total_11'>Kelas / Jenis</td> 
					<td colspan='$total_12'>".ArrayAdapter::format("unslug", $pasien->kelas ." - " .$pasien->carabayar)."</td> 
					<td colspan='$total_21'>No. Reg</td> 
					<td colspan='$total_22'>".ArrayAdapter::format("only-digit8", $pasien->no_register)."</td> 
				</tr>";
		$head5="<tr> 
					<td colspan='$total_11'>Asal</td colspan='$total_12'> 
					<td colspan='$total_12'>".ArrayAdapter::format("unslug", $pasien->asal)."</td> 
					<td colspan='$total_21'>Tanggal</td> 
					<td colspan='$total_22'>".ArrayAdapter::format("date d M Y", $pasien->waktu)."</td> 
				</tr>";
		
		$head=$this->uitable->getHeaderButton();
		$form=$this->uitable->getModal()->getForm();
		$this->uitable->addHeader("before", $head1);
		$this->uitable->addHeader("before", $head3);
		$this->uitable->addHeader("before", $head4);
		$this->uitable->addHeader("before", $head5);
		$this->uitable->setAction(false);
		
		global $user;
		$title=getSettings($this->db,"smis-rs-title-kepala-" . $this->polislug, "Pencetak");
		$nama=getSettings($this->db,"smis-rs-nama-kepala-" . $this->polislug, $user->getNameOnly());
		
		$footer="<tr>
					<td colspan='$total_1' > ".($pasien->keterangan_keluar!=""?"Keterangan : \n".$pasien->keterangan_keluar:"")." </td>
					<td colspan='$total_2' class='center'> 
						$title</br></br>
						$nama
					</td>
				</tr>";
		$this->uitable->addFooter("before", $footer);
		
		loadLibrary("smis-libs-function-medical");		
		$by_select=new Select("by_carapulang","",medical_carapulang_simple());
		
		$btn=new Button("","Validasi","Validasi");
		$btn->setClass("btn-danger");
		$btn->setIcon("fa fa-sign-out");
		$btn->setAction("biaya.validasi()");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		
		$text=new Text("by_tgl_keluar","",date("Y-m-d H:i:s"));
		$text->setModel(Text::$DATETIME);
		
		
		$form->addElement("Cara Keluar",$by_select);
		$form->addElement("Waktu Keluar",$text);
		$form->addElement("",$btn);
		$form->addElement("",$head);
		
		echo $form->getHTML();
		echo $this->uitable->getHtml();
		
		/*Parsing ke Javascript*/
		$carabayar=new Hidden("by_carabayar","",$this->carabayar);
		$poliname=new Hidden("by_poliname","",$this->poliname);
		$polislug=new Hidden("by_polislug","",$this->polislug);		
		$by_namapx=new Hidden("by_namapx","",$this->nama_pasien);
		$by_nrmpx=new Hidden("by_nrmpx","",$this->nrm);
		$by_noregpx=new Hidden("by_noregpx","",$this->noreg);
		
		echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $by_namapx->getHtml();
		echo $by_nrmpx->getHtml();
		echo $by_noregpx->getHtml();
		
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "rawat/resource/js/biaya.js",false );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
	
	public function command($command){
		$this->uitable->setAction(false);
		loadLibrary("smis-libs-function-math");
		
		$tagihan=new TagihanService($this->db, $this->noreg, $this->polislug);
		$tagihan->setMode(TagihanService::$MODE_RIGID);
		$hasil=$tagihan->getTagihan();
		$ldata=$hasil["data"];
		
		$simpleadapter=new SimpleAdapter();
		$simpleadapter->add("Jenis", "layanan");
		$simpleadapter->add("Jenis BOA", "nama");
		$simpleadapter->add("Waktu", "waktu");
		$simpleadapter->add("Satuan", "satuan","money Rp.");
		$simpleadapter->add("Jumlah", "jumlah");
		$simpleadapter->add("Biaya", "biaya","money Rp.");
		$simpleadapter->add("Keterangan", "keterangan");
		$content=$simpleadapter->getContent($ldata);
		$tgh=$tagihan->getRigidPosition();
		
		if($tgh=="nama"){
			$position="Jenis BOA";
		}else{
			$position=ucfirst($tgh);
		}
		
		if(getSettings($this->db, "smis-rs-printout-total-" . $this->polislug, "1")=="1"){
			$ttl=array();
			$ttl[$position]="<strong>Total</strong>";
			$ttl['Biaya']="<strong>".ArrayAdapter::format("money Rp.", $tagihan->getRigidTotal())."</strong>";
			$ttl['Keterangan']=getSettings($this->db, "smis-rs-numbertell-" . $this->polislug, "0")=="1"?"<strong>".numbertell($tagihan->getRigidTotal())." Rupiah </strong>":"";
			$content[]=$ttl;
		}
		
		$this->uitable->setContent($content);
		$list=$this->uitable->getBodyContent();
		$response=new ResponsePackage();
		$response->setStatus(ResponsePackage::$STATUS_OK);
		$response->setContent($list);
		echo json_encode($response->getPackage());
	}

	public function superCommand($super_command) {
		if ($_POST ['super_command'] == "validasi_spesialisasi") {
			$noreg_pasien = $_POST['noreg_pasien'];
			$carapulang = $_POST['carapulang'];
			$enable_field_spesialisasi = getSettings($this->db, "smis-rs-laporan-rl31-enable_field_spesialisasi-" . $this->polislug, "0");
			$valid = "1";
			if ($enable_field_spesialisasi == "1" && $carapulang != "Tidak Datang" && $carapulang != "Kabur") {
				$spesialisasi_row = $this->db->get_row("
					SELECT spesialisasi
					FROM smis_rwt_antrian_" . $this->polislug . "
					WHERE no_register = '" . $noreg_pasien . "'
				");
				$spesialisasi = "";
				if ($spesialisasi_row != null)
					$spesialisasi = $spesialisasi_row->spesialisasi;
				if ($spesialisasi == "")
					$valid = "0";
			}

			$data['valid'] = $valid;
			echo json_encode($data);
			return;
		}
	}
	
	public function cssPreLoad(){
		$fhead=getSettings($this->db, "smis-rs-font-title-" . $this->polislug, "inherit");
		$fshead=getSettings($this->db, "smis-rs-font-subtitle-" . $this->polislug, "inherit");
		$head=getSettings($this->db, "smis-rs-font-header-" . $this->polislug, "inherit");
		$fbody=getSettings($this->db, "smis-rs-font-body-" . $this->polislug, "inherit");
		$ffoot=getSettings(  $this->db, "smis-rs-font-footer-" . $this->polislug, "inherit");
		echo "<style type='text/css'>";
			echo "table#table_biaya > thead > tr > td > h4 {font-size:$fhead;} ";
			echo "table#table_biaya > thead > tr > td > h5 {font-size:$fshead;} ";
			echo "table#table_biaya > thead > tr > td {font-size:$head !important;} ";
			echo "table#table_biaya > tbody > tr > td {font-size:$fbody !important;} ";
			echo "table#table_biaya > tfoot > tr > td {font-size:$ffoot !important;} ";
		echo "</style>";		
	}
}
?>