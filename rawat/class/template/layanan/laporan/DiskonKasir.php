<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once "rawat/class/table/DiskonTable.php";
require_once "smis-base/smis-include-service-consumer.php";
require_once "smis-framework/smis/template/ModulTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';
class DiskonKasir extends LayananTemplate {    
    private $simple;
	private $non_chooser;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien);
		$header             = array("No.","Waktu Pengajuan","Waktu Cek","Petugas","Kepada","Pengecek","Status","Nilai","Persen");
        $this->uitable      = new DiskonTable($header,"",NULL,true);
		$this->uitable
             ->setName("diskon_kasir");	
	}
	
	public function superCommand($super_command) {	
        $dkadapt = new SimpleAdapter ();
		$dkadapt ->add ( "Jabatan", "nama_jabatan" )
				 ->add ( "Nama", "nama" )
				 ->add ( "NIP", "nip" );
		$head    = array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $head, "", NULL, true );
		$dktable ->setName ( 'diskon_kasir_pegawai' )
				 ->setModel ( Table::$SELECT );
		$perawat = new EmployeeResponder ( $this->db, $dktable, $dkadapt, "" );
        $super   = new SuperCommand ();
		$super   ->addResponder ( "diskon_kasir_pegawai", $perawat );
		$init    = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	public function command($command) {
		require_once "smis-base/smis-include-service-consumer.php";
        $adapter = new SimpleAdapter();
        $adapter ->setUseNumber(true,"No.","back.")
                 ->add("Waktu Pengajuan","waktu_ajukan","date d M Y H:i")
                 ->add("Waktu Cek","waktu_cek","date d M Y H:i")
                 ->add("Petugas","operator")
                 ->add("Kepada","pegawai")
                 ->add("Nilai","nilai","money Rp.")
                 ->add("Persen","persen","back%")
                 ->add("Pengecek","pengecek")
                 ->add("setuju","setuju")
                 ->add("Status","setuju","trivial_0_Belum_1_Setuju_Tolak");
        $serv = new ServiceResponder($db,$this->uitable,$adapter,"ajukan_diskon","kasir");
        $data = $serv ->command($_POST['command']);
        echo json_encode($data);
	}

	public function phpPreload() {	
        global $user;
        $this ->uitable ->addModal("id", "hidden", "", "" )
                        ->addModal("nama_pasien", "hidden", "", $this->nama_pasien )
                        ->addModal("noreg_pasien", "hidden", "", $this->noreg )
                        ->addModal("nrm_pasien", "hidden", "", $this->nrm )
                        ->addModal("waktu_ajukan", "datetime", "Waktu Pengajuan",date("Y-m-d H:i:s"))
                        ->addModal("nilai", "money", "Nilai","")
                        ->addModal("persen", "text", "Persen (%)","")
                        ->addModal("keterangan", "textarea", "Keterangan","")
                        ->addModal("operator", "hidden", "",$user->getNameOnly())
                        ->addModal("pegawai", "chooser-diskon_kasir-diskon_kasir_pegawai-Kepada", "Kepada","")
                        ->addModal("id_pegawai", "hidden", "","");
		$modal     = $this->uitable->getModal ();
        $modal     ->setTitle("Pengajuan Diskon ke Kasir");
		$poliname  = new Hidden("diskon_kasir_poliname","",$this->poliname);
		$polislug  = new Hidden("diskon_kasir_polislug","",$this->polislug);
		
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $this->uitable->getHtml();
        echo $modal->getHtml();
        
        echo addJS ("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addJS ("framework/smis/js/table_action.js");
		echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
		echo addJS ("rawat/resource/js/diskon_kasir.js",false);
	}
}

?>