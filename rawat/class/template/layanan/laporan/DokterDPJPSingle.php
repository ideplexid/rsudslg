<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once "smis-base/smis-include-service-consumer.php";
require_once "smis-libs-hrd/EmployeeResponder.php";
class DokterDPJP extends LayananTemplate {
	private $is_separated;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar);		
	}
	
	public function superCommand($super_command) {		
		$dkadapter = new SimpleAdapter ();
		$dkadapter ->add ( "Jabatan", "nama_jabatan" )
                   ->add ( "Nama", "nama" )
                   ->add ( "NIP", "nip" );
		$header    = array ('Nama','Jabatan',"NIP" );
		$dktable   = new Table ( $header);
		$dktable   ->setName ( "dokter_pj" )
                   ->setModel ( Table::$SELECT );
		$dokter    = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		$dokter    ->setStrict($strict_dr,$this->polislug);
		$super     = new SuperCommand ();
		$super     ->addResponder ( "dokter_pj", $dokter);
		$init      = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	
	public function command($command) {
		if($_POST['command']=="save"){
			$ser    = new ServiceConsumer($this->db,"change_dpjp",NULL,"registration");
			$ser    ->addData("noreg_pasien",$_POST['noreg_pasien'])
                    ->addData("id_dokter",$_POST['id_dokter'])
                    ->addData("nama_dokter",$_POST['nama_dokter'])
                    ->execute();
		}
		return NULL;
	}

	public function phpPreload() {
		$_id_dokter=0;
		$_nama_dokter="";
		$_dokter_history=array();
		if ($this->noreg != "") {
			$data_post = array ("command" => "edit","id" => $this->noreg );
			$service = new ServiceConsumer($this->db, "get_registered", $data_post);
			$service->execute ();
			$data = $service->getContent ();
			if($data!=NULL){
				$_id_dokter=$data['id_dokter'];
				$_nama_dokter=$data['nama_dokter'];
				$_dokter_history=json_decode($data['history_dokter'],true);
				if($_dokter_history==NULL){
					$_dokter_history=array(array("waktu"=>$data['tanggal_inap'],"nama_dokter"=>$_nama_dokter));
				}
			}
		}
		/* This is Modal Form and used for add and edit the table */
		
		$adapter=new SimpleAdapter();
		$adapter->setUseNumber(true,"No.","back.");
		$adapter->add("Waktu","waktu","date d M Y H:i:s");
		$adapter->add("Dokter","nama_dokter");
		$content=$adapter->getContent($_dokter_history);		
		$this->uitable=new Table(array("No.","Waktu","Dokter"),"",$content,false);
		$this->uitable->setName("dokter_dpjp");
		$this->uitable->setFooterVisible(false);
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien );
		$this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg );
		$this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm );
		$this->uitable->addModal ( "nama_dokter", "chooser-dokter_dpjp-dokter_pj-Pilih DPJP", "Nama DPJP", $_nama_dokter,"n",null,true );		
		$this->uitable->addModal ( "id_dokter", "hidden", "", $_id_dokter );
		$modal = $this->uitable->getModal ();
		$form=$modal->getForm()->setTitle("Dokter DPJP");	
		$btn=new Button("","","Simpan");
		$btn->setAction("dokter_dpjp.save()");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btn->setClass("btn-primary");
		$btn->setIcon("fa fa-save");
		$form->addElement("",$btn);
		
		$carabayar=new Hidden("dpjp_carabayar","",$this->carabayar);
		$poliname=new Hidden("dpjp_poliname","",$this->poliname);
		$polislug=new Hidden("dpjp_polislug","",$this->polislug);
		
		echo $carabayar->getHtml();
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $form->getHtml ();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		echo addCSS ( "rawat/resource/css/dokter_dpjp.css",false );
		echo addJS ( "rawat/resource/js/dokter_dpjp_single.js",false );
	}
}

?>