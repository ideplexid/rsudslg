<?php 

require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");

class LapIGD extends ModulTemplate {
	private $service;
	private $db;
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
	}
	public function initialize() {
		$data = $_POST;

		$nrm = isset ( $_POST ['nrm_pasien'] ) ? $_POST ['nrm_pasien'] : "";
		$nama = isset ( $_POST ['nama'] ) ? $_POST ['nama'] : (isset ( $_POST ['nama_pasien'] ) ? $_POST ['nama_pasien'] : "");
		$noreg = isset ( $_POST ['no_reg'] ) ? $_POST ['no_reg'] : (isset ( $_POST ['noreg_pasien'] ) ? $_POST ['noreg_pasien'] : "");
		$implement = isset ( $_POST ['prototype_implement'] ) ? $_POST ['prototype_implement'] : "";
		$polislug = isset ( $_POST ['prototype_slug'] ) ? $_POST ['prototype_slug'] : "";

		$dbtavle = new DBTable ( $this->db, "smis_rwt_antrian_" . $polislug );
		$row = $dbtavle->select ( $_POST ['id'] ); // id adalah id antrian

		$data ['id'] = $_POST ['id'];
		$data ['polislug'] = $polislug;
		$data ['noreg_pasien'] = $noreg;
		$data ['nama_pasien'] = $nama;
		$data ['nrm_pasien'] = $nrm;
		$data ['action'] = "lap_igd";
		$data ['waktu'] = $row->waktu;
		$data ['page'] = "rawat";

		/*
		 * $data['jk']=$row->jk; $data['umur']=$row->umur; $data['page']="rawat"; $data['asal_ruang']=$row->asal;
		*/

		$service = new ServiceConsumer ( $this->db, "get_lap_igd", $data, "medical_record" );
		$service->execute ();
		echo $service->getContent ();
	}
}


?>