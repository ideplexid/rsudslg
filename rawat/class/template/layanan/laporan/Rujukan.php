<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'rawat/class/responder/RujukanResponder.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

class Rujukan extends LayananTemplate {
	private $harga;
	private $id_antrian;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien, $id_antrian) {
		parent::__construct ($db,$polislug,$poliname,$nama_pasien, $nrm_pasien, $noreg_pasien);
		$header				= array ('Tanggal','Dokter','Ruangan','No Urut','Keterangan');
		$this->uitable 		= new Table ( $header, "Rujukan " . $poliname);
		$this->uitable->setName ( "rujukan" );
		$this->id_antrian 	= $id_antrian;
		$this->harga 		= array ();
	}
	
	public function superCommand($super_command){
		if($super_command=="chooser_kamar"){
			require_once("rawat/snippet/chooser_kamar.php");
			return;
		}
		$dkadapter 	= new SimpleAdapter ();
		$dkadapter	->add ( "Jabatan", "nama_jabatan" )
					->add ( "Nama", "nama" )
					->add ( "NIP", "nip" );
		$header		= array ('Nama','Jabatan',"NIP" );
		$dktable 	= new Table ( $header);
		$dktable->setName ( "rujukan_dokter" );
		$dktable->setModel ( Table::$SELECT );
		$dokter 	= new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		
		$super 		= new SuperCommand ();
		$super->addResponder ( "rujukan_dokter", $dokter );
		$init 		= $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	public function command($command) {
		$adapter 		= new SimpleAdapter ();
		$adapter		->add ( "Tanggal", "waktu", "date d M Y H:i" )
				 		->add ( "Dokter", "nama_dokter" )
				 		->add ( "No Urut", "no_urut" )
				 		->add ( "Ruangan", "ruangan", "strtolower" )
				 		->add ( "Keterangan", "keterangan" );
		$this->dbtable 	= new DBTable ( $this->db, "smis_rwt_rujukan_" . $this->polislug );
		$this->dbtable->addCustomKriteria ( "noreg_pasien", " = '" . $_POST ['noreg_pasien'] . "'" );
		$this->dbtable->addCustomKriteria ( "nrm_pasien", " = '" . $_POST ['nrm_pasien'] . "'" );
		$kelas 			= self::getKelasRuanganPasien($this->db, "smis-rs-kelas-rujukan-", $this->polislug, $_POST['noreg_pasien']);
		$this->dbres 	= new RujukanResponder ( $this->dbtable, $this->uitable, $adapter, $this->polislug );
		$this->dbres->setKelas($kelas);
		$data 			= $this->dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
	}

	/* when it's star build */
	public function phpPreload() {

        $urjip=new ServiceConsumer($db, "get_urjip",array());
        $urjip->setCached(true,"get_urjip");
        $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
        $urjip->execute();
        $content=$urjip->getContent();
        $ruangan			= array();
        foreach ($content as $autonomous=>$ruang){
			foreach($ruang as $nama_ruang=>$jip){
                if(getSettings($this->db, "smis-rs-register-".$this->polislug."-" . $nama_ruang, "1") =="1" ){
                    $option			 	 = array();
                    $option['value'] 	 = $nama_ruang;
                    if(isset($jip['name'])){
                        $option['name']	 = $jip['name'];
                    }else{
                        $option['name']	 = ArrayAdapter::format("unslug", $nama_ruang);
                    }
                    $ruangan[]			 = $option;
                }                
			}
		}
		
		$disable = getSettings($this->db, "smis-rs-register-date-".$this->polislug, "0")=="1";
		$this->uitable	->addModal ("id", "hidden", "", "")
						->addModal ("id_antrian", "hidden", "", $this->id_antrian)
						->addModal ("nama_pasien", "hidden", "", $this->nama_pasien)
						->addModal ("noreg_pasien", "hidden", "", $this->noreg)
						->addModal ("nrm_pasien", "hidden", "", $this->nrm)
						->addModal ("waktu", "datetime", "Tanggal", date("Y-m-d H:i"),"n",null,$disable)
						->addModal ("nama_dokter", "chooser-rujukan-rujukan_dokter", "Dokter", "")
						->addModal ("id_dokter", "hidden", "", "")
						->addModal ("ruangan", "select", "Ruangan", $ruangan)
						->addModal ("bed_kamar", "chooser-rujukan-chooser_kamar-Pilih Bed", "Bed", $ruangan)
						->addModal ("id_bed_kamar", "hidden", "", "")
						->addModal ("keterangan", "textarea", "Keterangan", "")
						->addModal ("keterangan_salah_kamar", "textarea", "Ket. Salah Kamar", "");
		$modal	= $this->uitable->getModal ();
		$modal->setTitle ( $this->poliname );
		
        if(getSettings($this->db,"smis-rs-register-show-df-save-".$this->polislug,"1")=="0"){
            $modal->clearFooter();
        }
        
        $button = new Button("","","Kirim dan Keluarkan");
		$button ->setIsButton(Button::$ICONIC_TEXT)
				->setClass(" btn-danger ")
				->setIcon(" fa fa-sign-out")
				->setAction("rujukan.save_out()");
        //$modal	->addFooter($button);
        
        $button	= new Button("","","Pindah Salah Kamar");
		$button ->setIsButton(Button::$ICONIC_TEXT)
				->setClass(" btn-warning ")
				->setIcon(" fa fa-sign-out")
				->setAction("rujukan.salah_kamar()");
        $modal	->addFooter($button);
		
		/** fetch to see another place is active this patient */
		$serv 	 = new ServiceConsumer($this->db,"is_patient_active",array("noreg_pasien"=>$this->noreg));
		$serv->setMode(ServiceConsumer::$KEY_ENTITY);
		$serv->execute();
		$content = $serv->getContent();
		$ruangan = "";
		foreach($content as $slug=>$state){
			if($state=="1" && $slug!=$this->polislug){
				$ruangan .= "<li>".ArrayAdapter::format("unslug",$slug)."</li>";
			}
		}
		if($ruangan!=""){
			$ruangan	  = "<ul>".$ruangan."</ul>";
			$alert 		  = new Alert("rujukan_alert","Pasien ini Masih Active di ruangan berikut : ",$ruangan);
			$alert->setAlertMode(Alert::$WARNING);
			$alert->setRemovable(true);	
			echo $alert->getHtml();
		}

		echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
		echo addJS  ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS  ( "rawat/resource/js/rujukan.js",false );
		echo addJS  ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		
		/*Parsing ke Javascript*/
		$poliname = new Hidden("rjk_poliname","",$this->poliname);
		$polislug = new Hidden("rjk_polislug","",$this->polislug);
		echo $poliname->getHtml();
		echo $polislug->getHtml();		
	}
}
?>