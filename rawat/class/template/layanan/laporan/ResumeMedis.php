<?php 

/**
 * used for handling the data of the current patient
 * all data about what treatment, what he using, what drugs, what radiology
 * and laboratory including the doctor treatment will be show in here.
 * each data spare each time patient come in (each noreg_pasien)
 * 
 * @author 	: Nurul Huda
 * @version : 1.0.0
 * @since	: 17 Desember 2016
 * @license	: LGPLv3
 * @service : 	- get_tindakan
 * 				- get_rad
 * 				- get_lab
 * 				- get_diagnosa
 * 				- get_tindakan
 * 				- get_riwayat_obat
 * */

require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once("rawat/class/adapter/LabAdapter.php");
require_once("rawat/class/adapter/RadAdapter.php");

class ResumeMedis extends LayananTemplate {
	private $id_antrian;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien, $id_antrian) {
		parent::__construct ($db,$polislug,$poliname,$nama_pasien, $nrm_pasien, $noreg_pasien);
		$this->uitable = $this->initUiTable();
		$this->uitable->setName ( "resumemedis" );
		$this->id_antrian = $id_antrian;
	}
	
	/**
	 * @brief init the UiTable special case for 
	 * 		  this Class
	 * @return  Table $uitable;
	 */
	private function initUiTable(){
		$diagnosa=new Button("rsm_diagnosa", "rsm_diagnosa", "Diagnosa");
		$diagnosa->setIsButton(Button::$ICONIC)
				->setClass("btn-primary")
				->setIcon("fa fa-book");
		$tindakan=new Button("rsm_tindakan", "rsm_tindakan", "Tindakan");
		$tindakan->setIsButton(Button::$ICONIC)
				 ->setClass("btn-primary")
				 ->setIcon("fa fa-stethoscope");
		$lab=new Button("rsm_lab", "rsm_lab", "Laboratory");
		$lab->setIsButton(Button::$ICONIC)
			->setClass("btn-primary")
			->setIcon("fa fa-eyedropper");
		$rad=new Button("rsm_rad", "rsm_rad", "Radiology");
		$rad->setIsButton(Button::$ICONIC)
			->setClass("btn-primary")
			->setIcon("fa fa-child");
		$obat=new Button("rsm_obat", "rsm_obat", "Obat");
		$obat->setIsButton(Button::$ICONIC)
			->setClass("btn-primary")
			->setIcon("fa fa-square");

		$file_data=new Button("file_data", "file_data", "Hasil Scan RM");
		$file_data->setIsButton(Button::$ICONIC)
			->setClass("btn-primary")
			->setIcon("fa fa-square");
			
		$header=array("Kunjungan Ke","Tanggal","Nomor Register","Umur");
		$uitable=new Table($header);
		$uitable->setName("resumemedis")
				->setMaxContentButton(1, "Rekam Medis")
				->setEditButtonEnable(false)
				->setDelButtonEnable(false)
				->setAddButtonEnable(false)
				->addContentButton("file_data", $file_data)
				->addContentButton("rsm_diagnosa", $diagnosa)
				->addContentButton("rsm_tindakan", $tindakan)
				->addContentButton("rsm_rad", $rad)
				->addContentButton("rsm_lab", $lab)
				->addContentButton("rsm_obat", $obat);
		return $uitable;
	}
	
	/**
	 * @brief used for create Responder of Diagnosa
	 * @param SuperCommand $supercommand 
	 * @return  null
	 */
	protected function initDiagnosa(SuperCommand $supercommand){
		$head=array("Tanggal","Dokter","Diagnosa Primer","Diagnosa Sekunder","Tensi","Nadi","Suhu","Respiration Rate","Gula Darah","Keadaan Umum","Keadaan Luka","Nyeri");
		$diagnosatable=new Table($head,"",NULL,false);
		$diagnosatable->setName("rsm_diagnosa")
					  ->setFooterVisible(false);
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "tanggal","date d M Y" )
				->add ( "Dokter", "nama_dokter" )
				->add ( "Diagnosa Primer", "diagnosa" )
				->add ( "Diagnosa Sekunder", "keterangan" )
				->add ( "Tensi", "tensi" )
				->add ( "Nadi", "nadi" )
				->add ( "Suhu", "suhu" )
				->add ( "Respiration Rate", "rr" )
				->add ( "Nyeri", "nyeri" )
				->add ( "Keadaan Umum", "keadaan_umum" )
				->add ( "Keadaan Luka", "keadaan_luka" )
				->add ( "Gula Darah", "gula_darah" );
		
		$responder_diagnosa = new ServiceResponder($this->db, $diagnosatable, $adapter,"get_diagnosa","medical_record");
		$supercommand->addResponder("rsm_diagnosa", $responder_diagnosa);
	}
	
	/**
	 * @brief used for create Responder of Tindakan Dokter
	 * @param SuperCommand $supercommand 
	 * @return  null
	 */
	protected function initTindakan(SuperCommand $supercommand){
		$head=array("Ruang","Tanggal","Dokter","Tindakan","ICD","Kode ICD");
		$tindakantble=new Table($head,"",NULL,false);
		$tindakantble->setName("rsm_tindakan")
					 ->setModel(Table::$NONE);
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Ruang", "smis_entity" )
				->add ( "Dokter", "nama_dokter" )
				->add ( "Tindakan", "nama_tindakan" )
				->add ( "Tanggal", "tanggal","date d M Y H:i" )
				->add ( "ICD", "nama_icd" )
				->add ( "Kode ICD", "kode_icd" )
				->add ( "Tanggal", "waktu", "date d M Y" );
		$responder_tindakan= new ServiceResponder ( $this->db, $tindakantble, $adapter, "get_tindakan" );
		$responder_tindakan->setMode(ServiceConsumer::$JOIN_ENTITY);
		$supercommand->addResponder("rsm_tindakan", $responder_tindakan);
	}
	
	/**
	 * @brief used for create Responder of Laboratory
	 * @param SuperCommand $supercommand 
	 * @return  null
	 */
	protected function initLaboratory(SuperCommand $supercommand){

		if(isset($_POST['command']) && $_POST['command']=="hasil"){
			$r = new ResponsePackage();
			$r->setContent("");
			$r->setStatus(ResponsePackage::$STATUS_OK);
			if(file_exists("smis-temp/Hasil_Lab_".$_POST['id'].".pdf")){
				$r->setContent("smis-temp/Hasil_Lab_".$_POST['id'].".pdf");
			}else{
				$r->setAlertContent("Belum Ada Hasil","Hasil Lab belum tersedia");
				$r->setAlertVisible(true);
			}
			echo json_encode($r->getPackage());
			return;
		}

		$head=array("Tanggal","No","Dokter","Petugas","Konsultan","Hasil");
		$labtble=new Table($head,"",NULL,false);
		$labtble->setName("rsm_lab")
				->setModel(Table::$NONE);
		$adapter = new LabAdapter ();
		$adapter->add ( "Tanggal", "tanggal","date d M Y " )
				->add ( "No", "no_lab" )
				->add ( "Dokter", "nama_dokter" )
				->add ( "Petugas", "nama_petugas" )
				->add ( "konsultan", "nama_konsultan" );
		$responder_lab= new ServiceResponder ( $this->db, $labtble, $adapter, "get_lab" );
		$supercommand->addResponder("rsm_lab", $responder_lab);
	}
	
	/**
	 * @brief used for create Responder of Radiology
	 * @param SuperCommand $supercommand 
	 * @return  null
	 */
	protected function initRadiology(SuperCommand $supercommand){
		$head=array("Tanggal","No","Dokter","Petugas","Konsultan","Hasil");
		$radtble=new Table($head,"",NULL,false);
		$radtble->setName("rsm_rad")->setModel(Table::$NONE);
		$adapter = new RadAdapter ();
		$adapter->add ( "Tanggal", "tanggal","date d M Y " )
				->add ( "No", "no_lab" )
				->add ( "Dokter", "nama_dokter" )
				->add ( "Petugas", "nama_petugas" )
				->add ( "konsultan", "nama_konsultan" );
		$responder_rad= new ServiceResponder ( $this->db, $radtble, $adapter, "get_rad" );
		$supercommand->addResponder("rsm_rad", $responder_rad);
	}
	
	/**
	 * @brief used for create Responder of Obat
	 * @param SuperCommand $supercommand 
	 * @return  null
	 */
	protected function initObat(SuperCommand $supercommand){
		$head=array("Racikan","Obat","Jumlah");
		$obatable=new Table($head,"",NULL,false);
		$obatable->setName("rsm_obat")
					  ->setFooterVisible(false);
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Racikan", "Racikan" )
				->add ( "Obat", "Obat" )
				->add ( "Jumlah", "Jumlah" );
		$responder_obat= new ServiceResponder ( $this->db, $obatable, $adapter, "get_riwayat_obat" );
		$responder_obat->setMode(ServiceConsumer::$JOIN_ENTITY);
		$supercommand->addResponder("rsm_obat", $responder_obat);
	}
	
	/**
	 * @brief handling the super command for each 
	 * 		  request;
	 * @param String $super_command 
	 * @return  
	 */
	public function superCommand($super_command){
		$supercommand=new SuperCommand();
		switch($super_command){
			case "rsm_diagnosa" : $this->initDiagnosa($supercommand); 	break;
			case "rsm_tindakan" : $this->initTindakan($supercommand); 	break;
			case "rsm_rad" 		: $this->initRadiology($supercommand); 	break;
			case "rsm_obat" 	: $this->initObat($supercommand); 		break;
			case "rsm_lab" 		: $this->initLaboratory($supercommand); break;
		}
		$superdata=$supercommand->initialize();
		if($superdata!=null){
			echo $superdata;
			return;
		}
	}
	
	public function command($command) {
		$radapter=new SimpleAdapter();
		$radapter->add("Kunjungan Ke", "no_kunjungan")
			 ->add("Tanggal", "tanggal","date d M Y")
			 ->add("Nomor Register", "id","digit8")
			 ->add("Kunjungan Ke", "no_kunjungan")
			 ->add("Umur", "umur");	
		$serv=new ServiceResponder ( $this->db, $this->uitable, $radapter, "get_registered_all" );
		unset($_POST['noreg_pasien']);
		$serv->addData("nrm",$this->nrm);
		$data=$serv->command($_POST['command']);
		echo json_encode ( $data );
	}

	/* when it's start build */
	public function phpPreload() {
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "rawat/resource/js/resumemedis.js",false );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
		
        /*modal lab dan radiology*/
        $modal=new Modal("rsm_lab_modal", "Hasil Lab", "Hasil Lab");
        $modal->setModalSize(Modal::$HALF_MODEL);
        echo $modal->getHtml();

        $modal=new Modal("rsm_rad_modal", "Hasil Rad", "Hasil Rad");
        $modal->setModalSize(Modal::$HALF_MODEL);
        echo $modal->getHtml();
        
		/*Parsing ke Javascript*/
		$poliname=new Hidden("rsm_poliname","",$this->poliname);
		$polislug=new Hidden("rsm_polislug","",$this->polislug);
		$nrm=new Hidden("rsm_nrm_pasien","",$this->nrm*1);
		$nama=new Hidden("rsm_nama_pasien","",$this->nama_pasien);
		$noreg=new Hidden("rsm_noreg_pasien","",$this->noreg);
		echo $poliname->getHtml();
		echo $polislug->getHtml();
		echo $nrm->getHtml();
		echo $nama->getHtml();
		echo $noreg->getHtml();
	}
}


?>