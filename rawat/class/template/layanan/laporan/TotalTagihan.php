<?php 
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");

class TotalTagihan extends ModulTemplate {
	private $service;
	private $db;
	private $timeout;
	private $delay;
	private $skipper;
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
		$this->skipper=100;
	}
	
	public function initialize() {
		$data = $_POST;
		$data ['polislug'] = isset ( $_POST ['prototype_slug'] ) ? $_POST ['prototype_slug'] : "";
		$data ['noreg_pasien'] = isset ( $_POST ['no_reg'] ) ? $_POST ['no_reg'] : (isset ( $_POST ['noreg_pasien'] ) ? $_POST ['noreg_pasien'] : "");
		$data ['nama_pasien'] = isset ( $_POST ['nama'] ) ? $_POST ['nama'] : (isset ( $_POST ['nama_pasien'] ) ? $_POST ['nama_pasien'] : "");
		$data ['nrm_pasien'] = isset ( $_POST ['nrm_pasien'] ) ? $_POST ['nrm_pasien'] : "";
		$data ['action'] = "total_tagihan_kasir";
		$data ['page'] = "rawat";
		$data ['delay'] = $this->delay;
		$data ['fail']=isset($_POST['fail'])?$_POST['fail']*1:1;
		
		$this->delay=getSettings($this->db,"smis-rs-delay-js-crawler-" . $data ['polislug'],"100");
		$this->timeout=getSettings($this->db,"smis-rs-socket-timeout-crawler-" . $data ['polislug'],60);
		$this->skipper=getSettings($this->db,"smis-rs-skipper-crawler-" . $data ['polislug'],100);
		$data ['skipper']=$this->skipper;
		
		$service = new ServiceConsumer ( $this->db, "get_total_tagihan_kasir", $data, "kasir" );
		ini_set('default_socket_timeout', $this->timeout * (1 + $data ['fail'])); /* more failed more time out */
		
		$service->execute ();
		$ctx=$service->getContent ();
		
		if(is_array($ctx)){
			$code=$service->getCode();
			$msg=" <p text-align='justify'>Jaringan ke Server sedang Sibuk, Mohon Tunggu Beberapa Saat Lagi !! 
					Code Service [ <strong>".$code."</strong> ] , ".
					"Ruangan <strong>".ArrayAdapter::format("unslug",$_POST ['slug'])."</strong>, ".
					"Nama Pasien <strong>".$data ['nama_pasien']."</strong>, ".
					"No Reg Pasien <strong>".ArrayAdapter::format("only-digit8",$data ['noreg_pasien'])."</strong>, ".
					"Nama Pasien <strong>".ArrayAdapter::format("only-digit8",$data ['nrm_pasien'])."</strong>".
					"dengan durasi timeout ".$this->timeout."s </p>";
			$content=array();
			$content['msg']=$msg;
			$content['code']=$code;
			$content['fail']=$data ['fail']+1;
			$response=new ResponsePackage();
			$response->setContent($content);
			$response->setStatus(ResponsePackage::$STATUS_OK);
			echo json_encode($response->getPackage());
		}else{
			echo $ctx;
		}
	}
	
}
?>