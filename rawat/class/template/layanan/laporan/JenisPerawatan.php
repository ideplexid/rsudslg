<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
class JenisPerawatan extends LayananTemplate {
	public function __construct($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan) {
        parent::__construct ($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);
	}
    
	public function initialize() {
		$data                  = $_POST;
		$data['polislug']      = $this->polislug;
		$data['noreg_pasien']  = $this->noreg;
		$data['nama_pasien']   = $this->nama_pasien;
		$data['nrm_pasien']    = $this->nrm;
		$data['action']        = "jenis_perawatan";
		$data['page']          = "rawat";
		$data['jk']            = isset($_POST['jk']) && ($_POST['jk']=="Perempuan" || $_POST['jk']) =="1"?"1":"0";
		$data['kelas']         = getSettings ( $this->db, "smis-rs-kelas-".$this->polislug,"");
		
		$service = new ServiceConsumer ( $this->db, "reg_jenis_perawatan", $data, "medical_record" );
		$service ->execute();
		echo $service->getContent();
	}
}
?>