<?php

require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");

class AsesmenPerawat extends ModulTemplate {
    private $service;
	private $db;
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
	}
    
    public function initialize() {
        $data = $_POST;

		$nrm = isset ( $_POST ['nrm_pasien'] ) ? $_POST ['nrm_pasien'] : "";
		$nama = isset ( $_POST ['nama'] ) ? $_POST ['nama'] : (isset ( $_POST ['nama_pasien'] ) ? $_POST ['nama_pasien'] : "");
		$noreg = isset ( $_POST ['no_reg'] ) ? $_POST ['no_reg'] : (isset ( $_POST ['noreg_pasien'] ) ? $_POST ['noreg_pasien'] : "");
		$implement = isset ( $_POST ['prototype_implement'] ) ? $_POST ['prototype_implement'] : "";
		$polislug = isset ( $_POST ['prototype_slug'] ) ? $_POST ['prototype_slug'] : "";

		$dbtavle = new DBTable ( $this->db, "smis_rwt_antrian_" . $polislug );
		$row = $dbtavle->select ( $_POST ['id_antrian'] ); // id adalah id antrian

		$data ['id_antrian'] = $_POST ['id_antrian'];
		$data ['polislug'] = $polislug;
		$data ['noreg_pasien'] = $noreg;
		$data ['nama_pasien'] = $nama;
		$data ['nrm_pasien'] = $nrm;
		$data ['action'] = "asesmen_perawat";
		$data ['page'] = "rawat";

		$data ['jk'] = $row->jk;
		$data ['umur'] = $row->umur;
		$data ['asal_ruang'] = $row->asal;
        $data ['carabayar'] = $row->carabayar;
        $data ['waktu_masuk'] = $row->waktu;
		$data ['page'] = "rawat";
		$detail = json_decode ( $row->detail, true );
		if ($detail == null) {
			$data ['alamat'] = "";
            $data ['tgl_lahir'] = "";
		} else {
			$data ['alamat'] = $detail ['alamat'];
            $data ['tgl_lahir'] = $detail ['tgl_lahir'];
		}

		$service = new ServiceConsumer ( $this->db, "get_asesmen_perawat", $data, "medical_record" );
		$service->execute ();
		echo $service->getContent ();
    }
}

?>