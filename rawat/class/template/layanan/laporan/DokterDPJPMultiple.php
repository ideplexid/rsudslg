<?php
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
class DokterDPJP extends LayananTemplate {
	private $is_separated;
	public function __construct($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar) {
		parent::__construct ($db, $polislug, $poliname, $nama_pasien, $nrm_pasien, $noreg_pasien,$carabayar);		
	}
	
	public function superCommand($super_command) {		
		$dkadapter = new SimpleAdapter ();
		$dkadapter ->add("Jabatan", "nama_jabatan")
                   ->add("Nama", "nama")
                   ->add("NIP", "nip");
		$header    = array ('Nama','Jabatan',"NIP");
		$dktable   = new Table($header);
		$dktable   ->setName("dokter_pj")
                   ->setModel(Table::$SELECT);
		$dokter    = new EmployeeResponder($this->db, $dktable, $dkadapter, "dokter");
		$dokter    ->setStrict($strict_dr,$this->polislug);
		$super     = new SuperCommand ();
		$super     ->addResponder("dokter_pj", $dokter);
		$init      = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	
	public function command($command) {
		if($_POST['command']=="save"){
			$ser = new ServiceConsumer($this->db,"change_dpjp_multiple",NULL,"registration");
			$ser ->addData("noreg_pasien",$_POST['noreg_pasien'])
                 ->addData("id_dokter",$_POST['id_dokter'])
                 ->addData("nama_dokter",$_POST['nama_dokter'])
                 ->addData("waktu",$_POST['waktu'])
                 ->addData("waktu_selesai",$_POST['waktu_selesai'])
                 ->addData("id",$_POST['id']*1-1)
                 ->execute();
		}
		
		if($_POST['command']=="del"){
			$ser = new ServiceConsumer($this->db,"remove_dpjp_multiple",NULL,"registration");
			$ser ->addData("noreg_pasien",$_POST['noreg_pasien'])
                 ->addData("id",$_POST['id']*1-1)
                 ->execute();
		}
		
		if($_POST['command']=="edit"){
			$history = $this->getHistoryDPJP($_POST['noreg_pasien']);
			$pack    = new ResponsePackage();
			$pack    ->setStatus(ResponsePackage::$STATUS_OK)
                     ->setContent($history[$_POST['id']*1-1]);
			echo json_encode($pack->getPackage());
		}
	}
	
	public function getHistoryDPJP($noreg){
		$_dokter_history = array();
		$data_post       = array ("command" => "edit","id" => $noreg);
		$service         = new ServiceConsumer($this->db, "get_registered", $data_post);
		$service->execute ();
		$data            = $service->getContent ();
		if($data!=NULL){
			$_id_dokter      = $data['id_dokter'];
			$_nama_dokter    = $data['nama_dokter'];
			$_dokter_history = json_decode($data['history_dokter'],true);
			if($_dokter_history==NULL){
				$one_dokter                 = array();
				$one_dokter['waktu']        = $data['tanggal_inap'];
				$one_dokter['nama_dokter']  = $_nama_dokter;
				$_dokter_history            = array();
				$_dokter_history[]          = $one_dokter;		
			}
		}		
		return $_dokter_history;
	}

	public function phpPreload() {
		$_id_dokter          = 0;
		$_nama_dokter        = "";
		$_dokter_history     = array();
		if ($this->noreg != "") {
			$_dokter_history = $this->getHistoryDPJP($this->noreg);
		}
		/* This is Modal Form and used for add and edit the table */
		require_once "rawat/class/adapter/DokterDPJPMultipleAdapter.php";
		$adapter       = new DokterDPJPMultipleAdapter();		
		$content       = $adapter->getContent($_dokter_history);		
		$this->uitable = new Table(array("No.","Dokter","Dari","Sampai"),"",$content,true);
		$this->uitable ->setAddButtonEnable(false)
                       ->setReloadButtonEnable(false)
                       ->setPrintButtonEnable(false)
                       ->setName("dokter_dpjp")
                       ->setFooterVisible(false)
                       ->addModal("id", "hidden", "", "")
                       ->addModal("nama_pasien", "hidden", "", $this->nama_pasien)
                       ->addModal("noreg_pasien", "hidden", "", $this->noreg)
                       ->addModal("nrm_pasien", "hidden", "", $this->nrm)
                       ->addModal("nama_dokter", "chooser-dokter_dpjp-dokter_pj-Pilih DPJP", "Nama DPJP", "","n",null,true)
                       ->addModal("waktu", "datetime", "Dari", "")
                       ->addModal("waktu_selesai", "datetime", "Sampai", "")
                       ->addModal("id_dokter", "hidden", "", $_id_dokter);
		$modal = $this ->uitable->getModal ();
		$form  = $modal->getForm()->setTitle("Dokter DPJP");	
		$btn   = new Button("","","Simpan");
		$btn   ->setAction("dokter_dpjp.save()")
               ->setIsButton(Button::$ICONIC_TEXT)
               ->setClass("btn-primary")
               ->setIcon("fa fa-save");
		$form  ->addElement("",$btn);
		
		$carabayar = new Hidden("dpjp_carabayar","",$this->carabayar);
		$poliname  = new Hidden("dpjp_poliname","",$this->poliname);
		$polislug  = new Hidden("dpjp_polislug","",$this->polislug);
		
		echo $carabayar     ->getHtml();
		echo $poliname      ->getHtml();
		echo $polislug      ->getHtml();
		echo $form          ->getHtml ();
		echo $this->uitable ->getHtml ();
		echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js");
		echo addJS  ("framework/smis/js/table_action.js");
		echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css");
		echo addCSS ("rawat/resource/css/dokter_dpjp.css",false);
		echo addJS  ("rawat/resource/js/dokter_dpjp_multiple.js",false);		
	}
}

?>