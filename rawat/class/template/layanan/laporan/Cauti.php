<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
class Cauti extends LayananTemplate {
	public function __construct($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan) {
		parent::__construct ($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);
	}
	public function initialize() {
        $data                   = $_POST;
        $data ['polislug']      = $this->polislug;
		$data ['noreg_pasien']  = $this->noreg;
		$data ['nama_pasien']   = $this->nama_pasien;
		$data ['nrm_pasien']    = $this->nrm;
		$data ['action']        = "cauti";
		$data ['page']          = "rawat";
		$data ['gol_umur']      = isset ( $_POST ['gol_umur'] ) ? $_POST ['gol_umur'] : "TGL LAHIR TDK VALID";;
		$data ['jk']            = isset($_POST ['jk']) && ($_POST ['jk']=="Perempuan" || $_POST ['jk']=="1")?"1":"0";
		$data ['kunjungan']     = isset ( $_POST ['kunjungan'] ) ? $_POST ['kunjungan'] : "Baru";
		$data ['carabayar']     = $this->carabayar;
		$data ['urji']          = getSettings($this->db, "smis-rs-urjip-".$this->polislug, "URJ")=="URI"?"1":"0";
		
		$service = new ServiceConsumer ( $this->db, "reg_cauti", $data, "medical_record" );
		$service->execute ();
		echo $service->getContent ();
	}
}



?>