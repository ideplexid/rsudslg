<?php 

global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterServiceTemplate.php";
require_once 'smis-libs-hrd/EmployeeResponder.php';

$ruangan = array();
if(!isset($_POST['command']) && $_POST['super_command']==""){
    $urjip=new ServiceConsumer($db, "get_urjip",array());
    $urjip->setCached(true,"get_urjip");
    $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
    $urjip->execute();
    $content=$urjip->getContent();
    $ruangan			= array();
    foreach ($content as $autonomous=>$ruang){
        foreach($ruang as $nama_ruang=>$jip){
            $option			 	 = array();
            $option['value'] 	 = $nama_ruang;
            if(isset($jip['name'])){
                $option['name']	 = $jip['name'];
            }else{
                $option['name']	 = ArrayAdapter::format("unslug", $nama_ruang);
            }
            $ruangan[]			 = $option;             
        }
    }
}

$gizi = new MasterServiceTemplate($db,"reg_diagnosa_gizi",$_POST['prototype_slug'],'diagnosa_gz');
$gizi ->setPrototipe(true,$_POST['prototype_name'],$_POST['prototype_slug'],'rawat');

$gizi ->getUItable()->setTitle("Diagnosa Gizi ".$_POST['prototype_name']);
$gizi ->getUItable()->addHeaderElement("No.");
$gizi ->getUItable()->addHeaderElement("Tanggal");
$gizi ->getUItable()->addHeaderElement("No. RM");
$gizi ->getUItable()->addHeaderElement("No. Reg");
$gizi ->getUItable()->addHeaderElement("Pasien");
$gizi ->getUItable()->addHeaderElement("Jenis Kelamin");
$gizi ->getUItable()->addHeaderElement("Dokter");
$gizi ->getUItable()->addHeaderElement("Diagnosa Gizi");
$gizi ->getUItable()->addHeaderElement("BB/TB");
$gizi ->getUItable()->addHeaderElement("Status Gizi");
$gizi ->getUItable()->addHeaderElement("Poli Rujukan");

$gizi ->getUItable()->addModal("id","hidden","","");
$gizi ->getUItable()->addModal("tanggal","date","Tanggal","");
$gizi ->getUItable()->addModal("","label","<strong>DATA DOKTER / PSIKOLOG</strong>","");
$gizi ->getUItable()->addModal("id_dokter","hidden","","");
$gizi ->getUItable()->addModal("nama_dokter","chooser-diagnosa_gz-dokter_diagnosa_gz-Dokter","Dokter / Psikolog","");
$gizi ->getUItable()->addModal("nip_dokter","text","NIP Dokter","","y",null,true);
$gizi ->getUItable()->addModal("","label","<strong>DIAGNOSA</strong>","");
$gizi ->getUItable()->addModal("nama_icd","chooser-diagnosa_gz-kode_icd_gz-Diagnosa","Diagnosa Gizi","");
$gizi ->getUItable()->addModal("kode_icd","hidden","","");
$gizi ->getUItable()->addModal("ruangan","hidden","",$_POST['prototype_slug']);
$gizi ->getUItable()->addModal("bbtb","text","BB / TB","");
$gizi ->getUItable()->addModal("status_gizi","text","Status GIzi","");
$gizi ->getUItable()->addModal("ruangan_rujukan","select","Ruangan Rujukan",$ruangan);

$gizi ->getAdapter()->setUseNumber(true,"No.","back.");
$gizi ->getAdapter()->add("Tanggal","tanggal","date d M Y");
$gizi ->getAdapter()->add("No. RM","nrm_pasien");
$gizi ->getAdapter()->add("No. Reg","noreg_pasien");
$gizi ->getAdapter()->add("Pasien","nama_pasien");
$gizi ->getAdapter()->add("Jenis Kelamin","jk","trivial_0_Laki-Laki_Perempuan");
$gizi ->getAdapter()->add("Dokter","nama_dokter");
$gizi ->getAdapter()->add("Diagnosa Gizi","nama_icd");
$gizi ->getAdapter()->add("BB/TB","bbtb");
$gizi ->getAdapter()->add("Status Gizi","status_gizi");
$gizi ->getAdapter()->add("Poli Rujukan","ruangan_rujukan","unslug");

$gizi ->addRegulerData("nama_pasien","nama_pasien","free-id-value");
$gizi ->addRegulerData("noreg_pasien","noreg_pasien","free-id-value");
$gizi ->addRegulerData("nrm_pasien","nrm_pasien","free-id-value");
$gizi ->addRegulerData("jk","jk","free-id-value");
$gizi ->addRegulerData("carabayar","carabayar","free-id-value");
$gizi ->addRegulerData("tanggal_lahir","tanggal_lahir","free-id-value");


$gizi ->setDateEnable(true);
$gizi ->setModalTitle("Diagnosa Gizi");
$gizi ->setModalComponentSize(Modal::$MEDIUM);

$gizi ->addJSColumn("id");
$gizi ->addJSColumn("tanggal");
$gizi ->addJSColumn("id_dokter");
$gizi ->addJSColumn("nama_dokter");
$gizi ->addJSColumn("nip_dokter");
$gizi ->addJSColumn("nama_icd");
$gizi ->addJSColumn("kode_icd");
$gizi ->addJSColumn("ruangan");
$gizi ->addJSColumn("bbtb");
$gizi ->addJSColumn("status_gizi");
$gizi ->addJSColumn("ruangan_rujukan");

/**Dokter GIzi */
$dkadapt = new SimpleAdapter ();
$dkadapt ->add ( "Jabatan", "nama_jabatan" )
        ->add ( "Nama", "nama" )
        ->add ( "NIP", "nip" );
$head    = array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $head, "", NULL, true );
$dktable ->setName ( 'dokter_diagnosa_gz' )
        ->setModel ( Table::$SELECT );
$perawat = new EmployeeResponder ( $db, $dktable, $dkadapt, "dokter" );
$gizi ->addSuperCommandResponder("dokter_diagnosa_gz",$perawat);
$gizi ->addSuperCommandArray("dokter_diagnosa_gz","id_dokter","id");
$gizi ->addSuperCommandArray("dokter_diagnosa_gz","nama_dokter","nama");
$gizi ->addSuperCommandArray("dokter_diagnosa_gz","nip_dokter","nip");


/**Diagnosa GIzi */
$dkadapt = new SimpleAdapter ();
$dkadapt ->add ( "Jabatan", "nama_jabatan" )
        ->add ( "Nama", "nama" )
        ->add ( "NIP", "nip" );
$head    = array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $head, "", NULL, true );
$dktable ->setName ( 'kode_icd_gz' )
        ->setModel ( Table::$SELECT );
$perawat = new ServiceResponder ( $db, $dktable, $dkadapt,"get_icd_gizi","gizi" );
$gizi ->addSuperCommandResponder("kode_icd_gz",$perawat);
$gizi ->addSuperCommandArray("kode_icd_gz","nama_icd","nama");
$gizi ->addSuperCommandArray("kode_icd_gz","kode_icd","icd");


$gizi ->initialize();