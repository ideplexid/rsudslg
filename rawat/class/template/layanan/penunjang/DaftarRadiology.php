<?php 
require_once "rawat/class/template/LayananTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
class DaftarRadiology extends LayananTemplate {
	public function __construct($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan) {
        parent::__construct ($db, $polislug, $poliname, $nama, $nrm, $noreg,$carabayar,$titipan);
	}
    
	public function initialize() {
		$data                  = $_POST;
		$data['polislug']      = $this->polislug;
		$data['noreg_pasien']  = $this->noreg;
		$data['nama_pasien']   = $this->nama_pasien;
		$data['nrm_pasien']    = $this->nrm;
		$data['action']        = "radiology";
		$data['page']          = "rawat";
		$data['jk']            = isset($_POST['jk']) && ($_POST['jk']=="Perempuan" || $_POST['jk'] =="1")?"1":"0";
		$data['kelas']         = getSettings ( $this->db, "smis-rs-kelas-".$this->polislug,"");
		if($this->noreg!=""){
			$data['kelas']     = self::getKelasRuanganPasien($this->db, "smis-rs-kelas-radiology-", $this->polislug, $this->noreg);
			if($data['kelas']==""){
				$data['kelas'] = $_POST['kelas'];
			}
		}
		$service = new ServiceConsumer ( $this->db, "rad_register", $data, "radiology" );
		$service ->execute();
		echo $service->getContent();
	}
}
?>