<?php
require_once 'rawat/class/adapter/SalaryAdapter.php';
require_once 'rawat/class/adapter/TindakanPerawatAdapter.php';
require_once 'rawat/class/adapter/TindakanPerawatIGDAdapter.php';
require_once 'rawat/class/adapter/TindakanDokterAdapter.php';
require_once 'rawat/class/adapter/KonsulDokterAdapter.php';
require_once 'rawat/class/adapter/KonsultasiDokterAdapter.php';
require_once 'rawat/class/adapter/VisiteDokterAdapter.php';
require_once 'rawat/class/adapter/EndoscopyAdapter.php';
require_once 'rawat/class/adapter/BronchoscopyAdapter.php';
require_once 'rawat/class/adapter/EKGAdapter.php';
require_once 'rawat/class/adapter/AudiometryAdapter.php';
require_once 'rawat/class/adapter/SpirometryAdapter.php';
require_once 'rawat/class/adapter/FaalParuAdapter.php';
require_once 'rawat/class/adapter/RRAdapter.php';
require_once 'rawat/class/adapter/OKAdapter.php';
require_once 'rawat/class/adapter/VKAdapter.php';

class SalaryCollector {
	private $id;
	private $from;
	private $to;
	private $polislug;
	private $db;
	private $adapter;
	private $mode;
	
	public static $MODE_EMPLOYEE="individual";
	public static $MODE_ROOM="communal";
	
	private static $TABNAME = array (
			"tindakan_perawat" => array ("id_perawat" ),
			"tindakan_igd" => array ("id_perawat","id_dokter" ),
			"tindakan_dokter" => array ("id_dokter" ),
			"visite_dokter" => array ("id_dokter"),
			"konsul_dokter" => array ("id_dokter"),
			"konsultasi_dokter" => array ("id_dokter"),
			"endoscopy" => array ("id_dokter","id_asisten" ),
			"faal_paru" => array ( "id_dokter","id_asisten" ),
			"ekg" => array ("id_dokter_pembaca","id_dokter_pengirim","id_petugas" ),
			"bronchoscopy" => array ( "id_dokter","id_perawat" ),
			"audiometry" => array ( "id_dokter","id_perawat" ),
			"spirometry" => array ( "id_dokter","id_perawat"),
			"rr" => array ("id_asisten","id_petugas" ),
			"ok" => array ("id_operator_satu","id_operator_dua","id_anastesi","id_asisten_anastesi","id_team_ok","id_bidan","id_perawat" ),
			"vk" => array ("id_operator_satu","id_operator_dua","id_anastesi","id_asisten_anastesi","id_team_vk","id_bidan","id_perawat" ) 
	);
	
	public function __construct($db, $id, $from, $to, $polislug, $mode="individual") {
		$this->id = $id;
		$this->from = $from;
		$this->to = $to;
		$this->polislug = $polislug;
		$this->db = $db;
		$this->mode=$mode;
		$this->initAdapter ();
	}
	public function initAdapter() {
		$mode=$this->mode;
		$this->adapter = array (
				"tindakan_perawat" => new TindakanPerawatAdapter ( $this->id, $this->polislug ,$mode),
				"tindakan_igd" => new TindakanPerawatIGDAdapter ( $this->id, $this->polislug ,$mode),
				"tindakan_dokter" => new TindakanDokterAdapter ( $this->id, $this->polislug ,$mode),
				"visite_dokter" => new VisiteDokterAdapter ( $this->id, $this->polislug ,$mode),
				"konsul_dokter" => new KonsulDokterAdapter ( $this->id, $this->polislug ,$mode),
				"konsultasi_dokter" => new KonsultasiDokterAdapter ( $this->id, $this->polislug ,$mode),
				"endoscopy" => new EndoscopyAdapter ( $this->id, $this->polislug ,$mode),
				"faal_paru" => new FaalParuAdapter ( $this->id, $this->polislug ,$mode),
				"ekg" => new EKGAdapter ( $this->id, $this->polislug ,$mode),
				"bronchoscopy" => new BronchoscopyAdapter ( $this->id, $this->polislug ,$mode),
				"audiometry" => new AudiometryAdapter ( $this->id, $this->polislug ,$mode),
				"spirometry" => new SpirometryAdapter ( $this->id, $this->polislug ,$mode),
				"rr" => new RRAdapter ( $this->id, $this->polislug ,$mode),
				"ok" => new OKAdapter ( $this->id, $this->polislug ,$mode),
				"vk" => new VKAdapter ( $this->id, $this->polislug ,$mode) 
		);
	}
	public function getSalary() {
		$hasil = array ();
		foreach ( self::$TABNAME as $name => $check ) {
			$slug = "smis_rwt_" . $name . "_" . $this->polislug;
			$dbtable = new DBTable ( $this->db, $slug );
			$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
			$ckc = self::createWhere ( $check, $this->id );
			$dbtable->addCustomKriteria ( null, $ckc );
			$dbtable->addCustomKriteria ( null, " waktu >= '" . $this->from . "'" );
			$dbtable->addCustomKriteria ( null, " waktu < '" . $this->to . "'" );
			$dbtable->setShowAll(true);
			$data = $dbtable->view ( "", "0" );
			$adapter = $this->adapter [$name];
			$list = $adapter->getContent ( $data ['data'] );
			$hasil = array_merge ( $hasil, $list );
		}
		return $hasil;
	}
	public static function createWhere($check, $id) {
		$d = " ( ";
		$d .= implode ( " ='" . $id . "' OR ", $TABNAME );
		$d .= " ='" . $id . "' ) ";
	}
}

?>