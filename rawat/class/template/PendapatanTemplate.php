<?php 

loadLibrary("smis-libs-function-math");
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");

class PendapatanTemplate extends ModulTemplate{    
    protected $poliname;
	protected $polislug;
	protected $uitable;
	protected $dbres;
	protected $dbtable;
	protected $db;
	
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db = $db;
		$uitable = new Table ( array(), "", NULL, false );
		$uitable->setDelButtonEnable(false);
		$uitable->setEditButtonEnable(false);
		$uitable->setReloadButtonEnable(false);
		$uitable->setAddButtonEnable(false);
		$this->uitable=$uitable;
		
	}
    
    public function getCaraBayar(){
		$ser=new ServiceConsumer($this->db,"get_carabayar",NULL,"registration");
		$ser->execute();
		$content=$ser->getContent();
		$result=array();
		$result[]=array("name"=>"","value"=>"%","default"=>"1");
		foreach($content as $x){
			$result[]=array("name"=>$x['nama'],"value"=>$x['slug'],"default"=>"0");
		}
		return $result;
	}
    
    protected function group_by($map){
        $option=new OptionBuilder();
        $option->add("","","1");
        
        $keys=array_keys($map);
        $array=combine($keys,false,false);
        foreach($array as $x){
            $index=count($x);
            $name="Group By ";
            $value="";
            foreach($x as $i){
                $index--;
                $name.=$i;
                $value.=$map[$i];
                if($index>0){
                    $name.="/";
                    $value.=",";
                }
            }
            $option->add($name,$value,"0",count($x)." Groups Combination");
        }
        return $option;
    }
    
    protected function order_by($map){
        $option=new OptionBuilder();
        $option->add("","","1");
        $keys=array_keys($map);
        $array=combine($keys,false,true);
        foreach($array as $x){
            $index=count($x);
            $name="Order By ";
            $value="";
            foreach($x as $i){
                $index--;
                $name.=$i;
                $value.=$map[$i];
                if($index>0){
                    $name.=" - ";
                    $value.=",";
                }
            }
            $option->add($name,$value,"0",count($x)." Orders Combination");
        }
        return $option;
    }
    
}


?>