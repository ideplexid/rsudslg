<?php
require_once 'rawat/class/template/PendapatanTemplate.php';

 class TindakanIGDDetail extends PendapatanTemplate {
	
    public function __construct($db, $polislug, $poliname) {
		parent::__construct ($db, $polislug, $poliname);
		$header=array ("No.",'Waktu',"Nama Dokter","Nama Pasien","No. Reg",'NRM',"Tindakan",'Harga Tindakan','Jumlah',"Total","Cara Bayar");
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "tindakan_igd_detail" );
	}
	
	
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("Total","harga_tindakan","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("Tindakan", "nama_tindakan");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Nama Dokter", "nama_dokter");
		$adapter->add("Harga Tindakan", "satuan","money Rp.");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "harga_tindakan","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga Tindakan", "satuan");
			$adapter->add("Total", "harga_tindakan");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga_tindakan");
			$adapter->addFixValue("Nama Pasien", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Tindakan", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Harga Tindakan", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
		}
	}
    
    private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']=strpos($group,"date(waktu)")!==false?" date(waktu) ":"'-'";
        $syntax['satuan']=strpos($group,"satuan")!==false?" satuan ":"'-'";
        $syntax['jumlah']=" sum(jumlah) ";
        $syntax['harga_tindakan']=" sum(harga_tindakan) ";
        $syntax['noreg_pasien']=" '-' ";
        $syntax['nrm_pasien']=" '-' ";
        $syntax['nama_pasien']=" '-' ";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        $syntax['nama_tindakan']=strpos($group,"nama_tindakan")!==false?" nama_tindakan ":"'-'";
        $syntax['nama_dokter']=strpos($group,"nama_dokter")!==false?" nama_dokter ":"'-'";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_rwt_tindakan_igd_".$this->polislug;
        return $result;
    }
	
    
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_tindakan_igd_' . $this->polislug );
		$this->dbtable->addCustomKriteria(NULL,"waktu>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"waktu<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		if($_POST['nama_tindakan']!=""){
			$this->dbtable->addCustomKriteria("nama_tindakan"," = '".$_POST['nama_tindakan']."'");
		}
		$this->dbtable->setOrder ( " waktu ASC " );
		$adapter=new SummaryAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']!="" ){
            $qv=$this->makeSyntax($_POST['grup']);            
            $qc="SELECT COUNT(*) as total FROM smis_rwt_tindakan_igd_".$this->polislug;
            $this->dbtable->setPreferredQuery(true,$qv,$qc);
            $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
            $this->dbtable->setGroupBy(true,$_POST['grup']);
            $this->dbtable->setUseWhereforView(true);
            $this->detail($this->uitable,$adapter,$this->dbtable);
            if(strpos($_POST['grup'],"date(waktu)")===false){
                $adapter->add("Waktu","waktu");
            }
        }else{
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}
		
		if(isset($_POST['fix_carabayar'])){
			$this->fixCaraBayar();
		}
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	protected function fixCarabayar(){
		
		$query="ALTER TABLE `smis_rwt_tindakan_igd_".$this->polislug."` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`,
				ADD `satuan` int(11) NOT NULL AFTER `carabayar`,
				ADD `jumlah` int(11) NOT NULL AFTER `satuan`;";
		$this->db->query($query);
		$query="UPDATE smis_rwt_tindakan_igd_".$this->polislug." a
				LEFT JOIN smis_rwt_antrian_".$this->polislug." b
				ON a.noreg_pasien=b.no_register
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
		$query="UPDATE smis_rwt_tindakan_igd_".$this->polislug." a
				SET a.jumlah=1 WHERE a.jumlah=0 ";
		$this->db->query($query);
		$query="UPDATE smis_rwt_tindakan_igd_".$this->polislug." a
				SET a.satuan=harga_tindakan WHERE a.satuan=0 ";
		$this->db->query($query);
	}
	
	
	public function superCommand($super_command) {
		$dktable = new Table ( array ("Nama","Kelas","Tarif"), "", NULL, true );
		$dktable->setName ( "tarif_keperawatan_igd" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		$data_kelas =getSettings ( $this->db, "smis-rs-kelas-" . $this->polislug, "" );
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_keperawatan" );
		$tarif->addData ( "kelas", $data_kelas );
		
		$super = new SuperCommand ();
		$super->addResponder ( "tarif_keperawatan_igd", $tarif );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
	
        $map['Carabayar']="carabayar";
        $map['Dokter']="nama_dokter";
        $map['Harga']="satuan";
        $map['Tanggal']="date(waktu)";
        $map['Tindakan']="nama_tindakan";
        $group=$this->group_by($map);
		
        $omap['Carabayar']=" carabayar ASC ";
        $omap['Dokter']=" nama_dokter ASC ";
        $omap['Jumlah']=" jumlah DESC ";
        $omap['Tanggal']=" waktu ASC ";
        $omap['Tindakan']=" nama_tindakan ASC ";
        $omap['Harga']=" harga_tindakan DESC ";
        $ordered=$this->order_by($omap);
        
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_tindakan", "chooser-tindakan_igd_detail-tarif_keperawatan_igd-Tindakan", "Tindakan","");
		$this->uitable->addModal("grup", "select", "Grup",$group->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("tindakan_igd_detail.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("tindakan_igd_detail.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("tindakan_igd_detail.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("tindakan_igd_detail.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var tindakan_igd_detail;		
		var tarif_keperawatan_igd;		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array('id',"no_register","selesai","nama_pasien","nrm_pasien",'waktu_keluar','cara_keluar','keterangan_keluar',"asal","kelas","jk","carabayar");
			tindakan_igd_detail=new TableAction("tindakan_igd_detail","<?php echo $this->polislug ?>","tindakan_igd_detail",column);
			tindakan_igd_detail.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			tindakan_igd_detail.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#tindakan_igd_detail_dari").val();
				save_data['sampai']=$("#tindakan_igd_detail_sampai").val();
				save_data['carabayar']=$("#tindakan_igd_detail_carabayar").val();
				save_data['nama_tindakan']=$("#tindakan_igd_detail_nama_tindakan").val();
				save_data['grup']=$("#tindakan_igd_detail_grup").val();
				save_data['orderby']=$("#tindakan_igd_detail_orderby").val();
				return save_data;
			};	

			tindakan_igd_detail.fix_carabayar=function(id){
				var a=this.getRegulerData();
				a['command']="list";
				a['fix_carabayar']="1";
				a['id']=id;
				var self=this;
				showLoading();
				$.post("",a,function(res){
					var json=getContent(res);
					$("#"+self.prefix+"_list").html(json.list);
					$("#"+self.prefix+"_pagination").html(json.pagination);	
					dismissLoading();
				});
			};
			
			tarif_keperawatan_igd=new TableAction("tarif_keperawatan_igd","<?php echo $this->polislug ?>","tindakan_igd_detail",new Array());
			tarif_keperawatan_igd.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			tarif_keperawatan_igd.setSuperCommand("tarif_keperawatan_igd");
			tarif_keperawatan_igd.selected=function(json){
				var nama=json.nama;
				$("#tindakan_igd_detail_nama_tindakan").val(nama);
			};
			
		});
		</script>
<?php
	}
}?>
