<?php
require_once 'rawat/class/template/PendapatanTemplate.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class TindakanDokterDetail extends PendapatanTemplate {
		
    public function __construct($db, $polislug, $poliname) {
		parent::__construct ($db, $polislug, $poliname);
		$header=array ("No.",'Waktu',"Nama Dokter","Nama Pasien","No. Reg",'NRM',"Tindakan",'Harga Dokter',"Harga Perawat",'Jumlah',"Total","Cara Bayar");
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "tindakan_dokter_detail" );
	}
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("Total","harga_total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("Tindakan", "nama_tindakan");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Nama Dokter", "nama_dokter");
		$adapter->add("Harga Dokter", "harga","money Rp.");
		$adapter->add("Harga Perawat", "harga_perawat","money Rp.");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "harga_total","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga Dokter", "harga");
			$adapter->add("Harga Perawat", "harga_perawat");
			$adapter->add("Total", "harga_total");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga");
			$adapter->addFixValue("Nama Pasien", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Tindakan", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Harga Tindakan", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
		}
	}
	
	
	private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']=strpos($group,"date(waktu)")!==false?" date(waktu) ":"'-'";
        $syntax['harga']=strpos($group,"harga")!==false?" harga ":"'-'";
        $syntax['harga_perawat']=strpos($group,"harga_perawat")!==false?" harga_perawat ":"'-'";
        $syntax['jumlah']=" sum(jumlah) ";
        $syntax['harga_total']=" sum((harga_perawat+harga)*jumlah) ";
        $syntax['noreg_pasien']=" '-' ";
        $syntax['nrm_pasien']=" '-' ";
        $syntax['nama_pasien']=" '-' ";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        $syntax['nama_tindakan']=strpos($group,"nama_tindakan")!==false?" nama_tindakan ":"'-'";
        $syntax['nama_dokter']=strpos($group,"nama_dokter")!==false?" nama_dokter ":"'-'";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_rwt_tindakan_dokter_".$this->polislug;
        return $result;
    }
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_tindakan_dokter_' . $this->polislug );
		$this->dbtable->addCustomKriteria(NULL,"waktu>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"waktu<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		if($_POST['nama_tindakan']!=""){
			$this->dbtable->addCustomKriteria("nama_tindakan"," = '".$_POST['nama_tindakan']."'");
		}
		if($_POST['nama_dokter']!=""){
			$this->dbtable->addCustomKriteria("nama_dokter"," = \"".$_POST['nama_dokter']."\"");
		}
		$this->dbtable->setOrder ( " waktu ASC " );
		$adapter=new SummaryAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']!="" ){
            $qv=$this->makeSyntax($_POST['grup']);            
            $qc="SELECT COUNT(*) as total FROM smis_rwt_tindakan_dokter_".$this->polislug;
            $this->dbtable->setPreferredQuery(true,$qv,$qc);
            $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
            $this->dbtable->setGroupBy(true,$_POST['grup']);
            $this->dbtable->setUseWhereforView(true);
            $this->detail($this->uitable,$adapter,$this->dbtable);
            if(strpos($_POST['grup'],"date(waktu)")===false){
                $adapter->add("Waktu","waktu");
            }
        }else{
			
			$qv="SELECT date(waktu) as waktu,
					harga as harga,
					harga_perawat as harga_perawat,
					jumlah as jumlah,
					(harga_perawat+harga)*jumlah as harga_total,					
					
					noreg_pasien as noreg_pasien,
					nrm_pasien as nrm_pasien,
					nama_dokter as nama_dokter,
					nama_pasien as nama_pasien,
					nama_tindakan as nama_tindakan,
					carabayar as carabayar
					FROM smis_rwt_tindakan_dokter_".$this->polislug."";
			
			$qc="SELECT COUNT(*) as total FROM smis_rwt_tindakan_dokter_".$this->polislug;
			$this->dbtable->setPreferredQuery(true,$qv,$qc);
			$this->dbtable->setUseWhereforView(true);			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}
		
		if(isset($_POST['fix_carabayar'])){
			$this->fixCaraBayar();
		}
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	protected function fixCarabayar(){
		
		$query="ALTER TABLE `smis_rwt_tindakan_dokter_".$this->polislug."` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`;";
        $this->db->query($query);
        $query="ALTER TABLE `smis_rwt_tindakan_dokter_".$this->polislug."` 
				ADD `jumlah` int(11) NOT NULL AFTER `carabayar`;";
        $this->db->query($query);
		$query="UPDATE smis_rwt_tindakan_dokter_".$this->polislug." a
				LEFT JOIN smis_rwt_antrian_".$this->polislug." b
				ON a.noreg_pasien=b.no_register
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
		$query="UPDATE smis_rwt_tindakan_dokter_".$this->polislug." a
				SET a.jumlah=1 WHERE a.jumlah=0 ";
		$this->db->query($query);
	}
    
    
    private function getTarifTindakanDokter(SuperCommand $super){
        $dktable = new Table ( array ("Nama","Kelas","Tarif"), "", NULL, true );
		$dktable->setName ( "tarif_tindakan_dokter_detail" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		$data_kelas =getSettings ( $this->db, "smis-rs-kelas-" . $this->polislug, "" );
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_tindakan_dokter" );
		$tarif->addData ( "kelas", $data_kelas );
		$super->addResponder ( "tarif_tindakan_dokter_detail", $tarif );
    }
    
    private function getDokterTindakanDokter(SuperCommand $super){
        $dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( "dokter_tindakan_dokter_detail" );
		$dktable->setModel ( Table::$SELECT );
		$dokter = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		$super->addResponder ( "dokter_tindakan_dokter_detail", $dokter );
    }
    
    private function initTotalSuperCommand(){
        $dbtable=new DBTable($this->db, "smis_rwt_tindakan_dokter_".$this->polislug);
        $dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'");
        $query=$dbtable->getQueryCount("");
        $content=$this->db->get_var($query);
        $pack=new ResponsePackage();
        $pack->setContent($content);
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($pack->getPackage());
    }
    
    private function initLimitSuperCommand(){
        $dbtable=new DBTable($this->db, "smis_rwt_tindakan_dokter_".$this->polislug);
        $dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'");
        $dbtable->setMaximum(1);
        $dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
        $limit_start=$_POST['limit_start'];
        $data=$dbtable->view("", $limit_start);
        $tindakan_dokter=$data['data'][0];
        
        if($tindakan_dokter->id_tindakan!="0" && $tindakan_dokter->bagi_dokter=="0"){
            $serv=new ServiceConsumer($this->db,"get_tindakan_dokter");
            $serv->addData("id",$tindakan_dokter->id_tindakan);
            $serv->addData("command","edit");
            $serv->setEntity("manajemen");
            $serv->execute();
            $cons=$serv->getContent();
            if(isset($cons['jaspel']) && $cons['jaspel']>0){
                $update['bagi_dokter']=$cons['jaspel'];
                $id['id']=$tindakan_dokter->id;
                $dbtable->update($update,$id);
            }
        }
        $pack=new ResponsePackage();
        $pack->setContent($tindakan_dokter);
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($pack->getPackage());
        return;
    }
    
	
	public function superCommand($super_command) {
        if($super_command=="limit"){
            $this->initLimitSuperCommand();
            return;
        }else if($super_command=="total"){
            $this->initTotalSuperCommand();
            return;
        }
        
		$super = new SuperCommand ();
        if($super_command=="tarif_tindakan_dokter_detail"){
            $this->getTarifTindakanDokter($super);
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }else if($super_command=="dokter_tindakan_dokter_detail"){
            $this->getDokterTindakanDokter($super);
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }
	}
    
    
    
	
	/* when it's star build */
	public function phpPreload() {
		
		
        $map['Carabayar']="carabayar";
        $map['Dokter']="nama_dokter";
        $map['Harga']=" harga, harga_perawat ";
        $map['Tanggal']="date(waktu)";
        $map['Tindakan']="nama_tindakan";
        $group=$this->group_by($map);
		
        $omap['Carabayar']=" carabayar ASC ";
        $omap['Dokter']=" nama_dokter ASC ";
        $omap['Jumlah']=" jumlah DESC ";
        $omap['Tanggal']=" waktu ASC ";
        $omap['Tindakan']=" nama_tindakan ASC ";
        $omap['Harga']=" harga_total DESC ";
        $ordered=$this->order_by($omap);
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_tindakan", "chooser-tindakan_dokter_detail-tarif_tindakan_dokter_detail-Tindakan", "Tindakan","");
		$this->uitable->addModal("nama_dokter", "chooser-tindakan_dokter_detail-dokter_tindakan_dokter_detail-Dokter", "Dokter","");
		$this->uitable->addModal("grup", "select", "Grup",$group->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("tindakan_dokter_detail.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("tindakan_dokter_detail.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("tindakan_dokter_detail.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("tindakan_dokter_detail.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
        
        $btn=new Button("", "", "Fiksasi Dokter");
		$btn->setAction("tindakan_dokter_detail.rekaptotal()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-circle-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$form->addElement("", $btg);
		
        $hidden_poliname=new Hidden("TD_DT_POLINAME","TD_DT_POLINAME",$this->poliname);
		$hidden_polislug=new Hidden("TD_DT_POLISLUG","TD_DT_POLISLUG",$this->polislug);
		
        $close  =new Button("", "", "Batal");
        $close	->addClass("btn-primary")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon("fa fa-close")
                ->setAction("tindakan_dokter_detail.batal()");
        
        $load   =new LoadingBar("tindakan_dokter_detail_bar", "");
        $modal  =new Modal("tindakan_dokter_detail_modal", "", "Processing...");
        $modal	->addHTML($load->getHtml(),"after")
                ->addFooter($close);
		
        echo $modal->getHtml();
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
        echo $hidden_poliname->getHtml();
        echo $hidden_polislug->getHtml();
        echo addJS ( "base-js/smis-base-loading.js");
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "rawat/resource/js/tindakan_dokter_detail.js",false );
	}
	
}?>
