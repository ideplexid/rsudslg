<?php
require_once 'rawat/class/template/PendapatanTemplate.php';


 class OksigenManualDetail extends PendapatanTemplate {
	
    public function __construct($db, $polislug, $poliname) {
		parent::__construct ($db, $polislug, $poliname);
		$header=array ("No.",'Waktu',"Nama Pasien","No. Reg",'NRM',
					'H.Liter(HJ)','Jumlah(J)','Biaya Lain(BL)',"H.Total (HJ*J)+BL","Cara Bayar");
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "oksigen_manual_detail" );
	}
    
    private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']=strpos($group,"date(waktu)")!==false?" date(waktu) ":"'-'";
        $syntax['harga']=" sum(harga) ";
        $syntax['jumlah_liter']=" sum(jumlah_liter) ";
        $syntax['biaya_lain']=" sum(biaya_lain)  ";
        $syntax['harga_liter']=strpos($group,"harga_liter")!==false?" harga_liter ":"'-'";
        $syntax['noreg_pasien']=" '-' ";
        $syntax['nrm_pasien']=" '-' ";
        $syntax['nama_pasien']=" '-' ";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_rwt_oksigen_manual_".$this->polislug;
        return $result;
    }
    
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){		
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("H.Total (HJ*J)+BL","harga","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("H.Liter(HJ)", "harga_liter","money Rp.");
		$adapter->add("Jumlah(J)", "jumlah_liter");
		$adapter->add("Biaya Lain(BL)", "biaya_lain","money Rp.");
		$adapter->add("H.Total (HJ*J)+BL", "harga","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu Mulai", "mulai","date d/m/Y");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("H.Total (HJ*J)+BL","harga");
			$adapter->add("Nama Pasien", "nama_pasien");
			$adapter->add("No. Reg", "noreg_pasien");
			$adapter->add("NRM", "nrm_pasien");
			$adapter->add("H.Liter(HJ)", "harga_liter");
			$adapter->add("Jumlah(J)", "jumlah_liter");
			$adapter->add("Biaya Lain(BL)", "biaya_lain");
			$adapter->add("H.Total (HJ*J)+BL", "harga");
			$adapter->add("Cara Bayar", "carabayar");
		}
	}
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_oksigen_manual_' . $this->polislug );
		$this->dbtable->addCustomKriteria(NULL,"waktu>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"waktu<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		$this->dbtable->setOrder ( " waktu ASC " );
		$adapter=new SummaryAdapter();
		
		
		if(isset($_POST['grup']) && $_POST['grup']!="" ){
            $qv=$this->makeSyntax($_POST['grup']);            
            $qc="SELECT COUNT(*) as total FROM smis_rwt_oksigen_manual_".$this->polislug;
            $this->dbtable->setPreferredQuery(true,$qv,$qc);
            $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
            $this->dbtable->setGroupBy(true,$_POST['grup']);
            $this->dbtable->setUseWhereforView(true);
            $this->detail($this->uitable,$adapter,$this->dbtable);
            if(strpos($_POST['grup'],"date(waktu)")===false){
                $adapter->add("Waktu","waktu");
            }
        }else{			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}
		
		if(isset($_POST['fix_carabayar'])){
			$this->fixCaraBayar();
		}
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	protected function fixCarabayar(){
		$query="ALTER TABLE `smis_rwt_oksigen_manual_".$this->polislug."` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`;";
		$this->db->query($query);
		$query="UPDATE smis_rwt_oksigen_manual_".$this->polislug." a
				LEFT JOIN smis_rwt_antrian_".$this->polislug." b
				ON a.noreg_pasien=b.no_register
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
	}
	
	
	
	/* when it's star build */
	public function phpPreload() {
		
        $map['Carabayar']="carabayar";
        $map['Harga']=" harga_liter ";
        $map['Tanggal']=" date(waktu) ";
        $group=$this->group_by($map);
		
        $omap['Carabayar']=" carabayar ASC ";
        $omap['Tanggal']=" waktu ASC ";
        $omap['Biaya']=" harga DESC ";
        $ordered=$this->order_by($omap);
        
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("grup", "select", "Grup",$group->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("oksigen_manual_detail.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("oksigen_manual_detail.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("oksigen_manual_detail.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("oksigen_manual_detail.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var oksigen_manual_detail;		
		var alok;		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array('id',"no_register","selesai","nama_pasien","nrm_pasien",'waktu_keluar','cara_keluar','keterangan_keluar',"asal","kelas","jk","carabayar");
			oksigen_manual_detail=new TableAction("oksigen_manual_detail","<?php echo $this->polislug ?>","oksigen_manual_detail",column);
			oksigen_manual_detail.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			oksigen_manual_detail.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#oksigen_manual_detail_dari").val();
				save_data['sampai']=$("#oksigen_manual_detail_sampai").val();
				save_data['carabayar']=$("#oksigen_manual_detail_carabayar").val();
				save_data['grup']=$("#oksigen_manual_detail_grup").val();
				save_data['orderby']=$("#oksigen_manual_detail_orderby").val();
				return save_data;
			};	

			oksigen_manual_detail.fix_carabayar=function(id){
				var a=this.getRegulerData();
				a['command']="list";
				a['fix_carabayar']="1";
				a['id']=id;
				showLoading();
				$.post("",a,function(res){
					var json=getContent(res);
					oksigen_manual_detail.view();
					dismissLoading();
				});
			};
			
			
			
		});
		</script>
<?php
	}
}?>
