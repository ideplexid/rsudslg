<?php

require_once 'rawat/class/template/PendapatanTemplate.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class OperasiDetail extends PendapatanTemplate {
	
     public function __construct($db, $polislug, $poliname) {
		parent::__construct ($db, $polislug, $poliname);
		$header=array ("No.",'Waktu',"Jenis Pembiusan","Jenis Operasi","Dr. Operator I","Dr. Operator II","Dr. Perujuk","Dr. Anastesi","Oomloop","Asisten","Cara Bayar","Tindakan I","Tindakan II","Pasien","No. Reg",'NRM','Harga','Jumlah',"Total","Bagi Dokter");
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "operasi_detail" );
        $this->uitable->setAction(true);
        $this->uitable->setAddButtonEnable(false);
        $this->uitable->setDelButtonEnable(false);
        $this->uitable->setPrintButtonEnable(false);
        $this->uitable->setReloadButtonEnable(true);
        $this->uitable->setEditButtonEnable(true);
        
        $button=new Button("","","");
        $button->setIcon(" fa fa-list-alt");
        $button->setClass(" btn-danger");
        $button->setIsButton(Button::$ICONIC);
        $this->uitable->addContentButton("limit_one",$button);
        
        $button=new Button("","","");
        $button->setIcon(" fa fa-upload");
        $button->setClass(" btn-info");
        $button->setIsButton(Button::$ICONIC);
        $this->uitable->addContentButton("detail_history",$button);
	}
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("Total","harga_total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
        $adapter->add("id", "id");
        $adapter->add("Jenis Pembiusan", "jenis_bius");
        $adapter->add("Jenis Operasi", "jenis_operasi");

		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Pasien", "nama_pasien");
        $adapter->add("Dr. Perujuk", "nama_perujuk");
        $adapter->add("Tindakan I", "nama_tindakan");
        $adapter->add("Tindakan II", "nama_tindakan_dua");
        $adapter->add("Oomloop", "nama_oomloop_satu");
        $adapter->add("Asisten", "nama_asisten_operator_satu");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
        $adapter->add("Dr. Operator I", "nama_dokter");
        $adapter->add("Dr. Operator II", "nama_dokter_dua");
        $adapter->add("Dr. Anastesi", "nama_anastesi");
		$adapter->add("Harga", "harga","money Rp.");
       
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "harga_total","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
        $adapter->add("Bagi Dokter", "bagi_dokter","money Rp.");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga", "harga");
            $adapter->add("Total", "harga_total");
            $adapter->add("Bagi Dokter", "bagi_dokter");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga");
			$adapter->addFixValue("Pasien", "");
            $adapter->addFixValue("Dr. Perujuk", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Tindakan", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
            $adapter->addFixValue("Bagi Dokter", "");

		}
    }
    

    
	
    private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']=strpos($group,"date(waktu)")!==false?" date(waktu) ":"'-'";
        $syntax['harga']=strpos($group,"harga_operator_satu")!==false?" harga_operator_satu ":"'-'";        
        $syntax['jenis_bius']=" '-' ";
        $syntax['jenis_operasi']=" '-' ";        
        $syntax['jumlah']=" sum(1) ";
        $syntax['harga_total']=" sum(harga_tindakan) ";
        $syntax['noreg_pasien']=" '-' ";
        $syntax['nrm_pasien']=" '-' ";
        $syntax['nama_pasien']=" '-' ";
        $syntax['keterangan']=" '-' ";
        $syntax['kode_perujuk']=" '-' ";
        $syntax['bagi_dokter']=" '-' ";
        $syntax['nama_oomloop_satu']=" '-' ";
        $syntax['nama_asisten_operator_satu']=" '-' ";
        $syntax['id']=" '' ";
        $syntax['nama_dokter']=strpos($group,"nama_operator_satu")!==false?" nama_operator_satu ":"'-'";
        $syntax['nama_dokter_dua']="'-'";
        $syntax['nama_anastesi']=strpos($group,"nama_anastesi")!==false?" nama_anastesi ":"'-'";
        $syntax['nama_perujuk']=strpos($group,"nama_perujuk")!==false?" nama_perujuk ":"'-'";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        $syntax['nama_tindakan']=strpos($group,"nama_tindakan")!==false?" nama_tindakan ":"'-'";
        $syntax['nama_tindakan)dua']="'-'";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_rwt_ok_".$this->polislug;
        return $result;
    }
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_ok_' . $this->polislug );
        $adapter=new SummaryAdapter();
        if(isset($_POST['command']) && ( $_POST['command']=="list" || $_POST['command']=="excel") ){
            $this->dbtable->addCustomKriteria(NULL,"waktu>='".$_POST['dari']."'");
            $this->dbtable->addCustomKriteria(NULL,"waktu<'".$_POST['sampai']."'");
            $this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['filter_carabayar']."'");
            if($_POST['nama_tindakan']!=""){
                $this->dbtable->addCustomKriteria("nama_tindakan"," = '".$_POST['nama_tindakan']."'");
            }
            if($_POST['nama_dokter']!=""){
                $this->dbtable->addCustomKriteria("nama_operator_satu"," = '".$_POST['nama_dokter']."'");
            }
            if($_POST['nama_anastesi']!=""){
                $this->dbtable->addCustomKriteria("nama_anastesi"," = '".$_POST['nama_anastesi']."'");
            }
            if($_POST['jenis_bius']!=""){
                $this->dbtable->addCustomKriteria("jenis_bius"," = '".$_POST['jenis_bius']."'");
            }
            if($_POST['jenis_operasi']!=""){
                $this->dbtable->addCustomKriteria("jenis_operasi"," = '".$_POST['jenis_operasi']."'");
            }
            $this->dbtable->setOrder ( " waktu ASC " );
            
            
            if(isset($_POST['grup']) && $_POST['grup']!="" ){
                $qv=$this->makeSyntax($_POST['grup']);            
                $qc="SELECT COUNT(*) as total FROM smis_rwt_ok_".$this->polislug;
                $this->dbtable->setPreferredQuery(true,$qv,$qc);
                $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
                $this->dbtable->setGroupBy(true,$_POST['grup']);
                $this->dbtable->setUseWhereforView(true);
                $this->detail($this->uitable,$adapter,$this->dbtable);
                if(strpos($_POST['grup'],"date(waktu)")===false){
                    $adapter->add("Waktu","waktu");
                }
            }else{
                $qv="SELECT date(waktu) as waktu,
                        harga_tindakan as harga,
                        1 as jumlah,
                        jenis_bius,
                        jenis_operasi,
                        harga_tindakan as harga_total,
                        noreg_pasien as noreg_pasien,
                        nrm_pasien as nrm_pasien,
                        nama_operator_satu as nama_dokter,
                        nama_operator_dua as nama_dokter_dua,
                        nama_anastesi as nama_anastesi,
                        nama_perujuk as nama_perujuk,
                        kode_perujuk as kode_perujuk,
                        nama_pasien as nama_pasien,
                        nama_tindakan as nama_tindakan,
                        nama_tindakan_dua as nama_tindakan_dua,
                        nama_oomloop_satu as nama_oomloop_satu,
                        nama_asisten_operator_satu as nama_asisten_operator_satu,
                        carabayar as carabayar,
                        bagi_dokter as bagi_dokter,
                        keterangan  as keterangan,
                        id as id
                        FROM smis_rwt_ok_".$this->polislug."";			
                $qc="SELECT COUNT(*) as total FROM smis_rwt_ok_".$this->polislug;
                $this->dbtable->setPreferredQuery(true,$qv,$qc);
                $this->dbtable->setUseWhereforView(true);			
                
                $this->detail($this->uitable,$adapter,$this->dbtable);
            }
            
            if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
                $this->dbtable->setOrder($_POST['orderby'],true);
            }		
            if(isset($_POST['fix_carabayar'])){
                $this->fixCaraBayar();
            }
        }
		require_once "rawat/class/responder/OperasiDetailResponder.php";
		$this->dbres = new OperasiDetailResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
    /**
     * @brief some table have no carabayar field
     *          using this method will automatcally added it 
     *          and fill by compare with smis_rwt_antrian_
     *          table based on no_register
     * @return  
     */
	protected function fixCarabayar(){		
		$query="ALTER TABLE `smis_rwt_ok_".$this->polislug."` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`;";
		$this->db->query($query);
		$query="UPDATE smis_rwt_ok_".$this->polislug." a
				LEFT JOIN smis_rwt_antrian_".$this->polislug." b
				ON a.noreg_pasien=b.no_register
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
	}
	
    /**
     * @brief get tindakan operasi from manajer 
     * @param SuperCommand $supercommand 
     * @return null 
     */
    private function getTindakanOperasi(SuperCommand &$supercommand){
        $dktable = new Table ( array ("Nama","Kelas","Tarif"), "", NULL, true );
		$dktable->setName ( "tarif_operasi_detail" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		$data_kelas =getSettings ( $this->db, "smis-rs-kelas-" . $this->polislug, "" );
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_tindakan_operasi" );
		$supercommand->addResponder ( "tarif_operasi_detail", $tarif );
    }
    
    /**
     * @brief get the doctor name from hrd
     * @param SuperCommand $supercommand 
     * @return null;
     */
    private function getKaryawanOperasiDetail(SuperCommand &$supercommand,$command,$kriteria){
        $dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( $command );
		$dktable->setModel ( Table::$SELECT );
		$dokter = new EmployeeResponder ( $this->db, $dktable, $dkadapter, $kriteria );
		$supercommand->addResponder ( $command, $dokter );
    }
    
    /**
     * @brief only compare the data to fill 'keterangan'
     *        not update the operator and perujuk
     * @param DBTable $dbtable 
     * @param Array $operasi
     * @return string keterangan
     */
    private function compareData(DBTable &$dbtable,$operasi){
        $d=$this->getOperatorPerujukOperasi($dbtable,$operasi);
        $dt=array();
        $up['id']=$operasi->id;
        if($operasi->id_operator_satu==$d['id_operator'] && $d['success_operator']=="1" && $operasi->id_perujuk==$d['id_perujuk'] && $d['success_perujuk']=="1" ){
            $dt['keterangan']="FIX";
        }else{
            $up['id']=$operasi->id;
            $dt['keterangan']=str_replace(PHP_EOL, '', $d['keterangan']);
        }
        
        $tarif_ok=$this->getTarifOperasi($dbtable,$operasi);
        if( $tarif_ok['bagi_dokter']!=$operator->bagi_dokter){
            $dt['keterangan'].=". Koreksi Bagi Dokter ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_dokter']);
        }
        if( $tarif_ok['bagi_asisten_operasi']!=$operator->bagi_asisten_operator){
            $dt['keterangan'].=". Koreksi Bagi Asisten Operasi ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_asisten_operasi']);
        }
        $dbtable->update($dt, $up);
        return $dt['keterangan'];
    }
    
    /**
     * @brief replace what ever in database using autocorrect
     *          no matter autocorrect empty or not
     * @param DBTable $dbtable 
     * @param Array $operasi
     * @return string keterangan
     */
    private function FullAutoCorret(DBTable &$dbtable,$operasi){
        $d=$this->getOperatorPerujukOperasi($dbtable,$operasi);  
        $dt=array();
        $up['id']=$operasi->id;
        if($operasi->id_operator_satu==$d['id_operator'] && $d['success_operator']=="1" && $operasi->id_perujuk==$d['id_perujuk'] && $d['success_perujuk']=="1" ){
            $dt['keterangan']="FIX";
        }else{
            $CODE="";
            if( $operasi->id_operator_satu!=$d['id_operator']){
                $CODE.="<strong>[OP_REPLACE]</strong>";  
                $dt['id_operator_satu']=$d['id_operator'];
                $dt['nama_operator_satu']=$d['nama_operator'];
            }
            if($operasi->id_perujuk!=$d['id_perujuk']){
                $CODE.="<strong>[OP_REPLACE]</strong>";  
                $dt['id_perujuk']=$d['id_perujuk'];
                $dt['nama_perujuk']=$d['nama_perujuk']; 
            }
            if($CODE!=""){
                $dt['keterangan']=$CODE."\n".str_replace(PHP_EOL, '', $d['keterangan']);
            }
           
        }
        
        $tarif_ok=$this->getTarifOperasi($dbtable,$operasi);
        if( $tarif_ok['bagi_dokter']!=$operator->bagi_dokter){
            $dt['bagi_dokter']=$tarif_ok['bagi_dokter'];
            $dt['keterangan'].=". Koreksi Bagi Dokter ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_dokter']);
        }
        if( $tarif_ok['bagi_asisten_operasi']!=$operator->bagi_asisten_operator){
            $dt['bagi_asisten_operator']=$tarif_ok['bagi_asisten_operasi'];
            $dt['keterangan'].=". Koreksi Bagi Asisten Operasi ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_asisten_operasi']);
        }
        
        
         $dbtable->update($dt, $up);
         return $dt['keterangan'];
    }
    
    /**
     * @brief it will update the data when the correction have value
     *          if the correction empty, then leave it as is.
     * @param DBTable $dbtable 
     * @param Array $operasi
     * @return string keterangan
     */
    private function AutoCorretIfCorrectionNotEmpty(DBTable &$dbtable,$operasi){
        $d=$this->getOperatorPerujukOperasi($dbtable,$operasi);  
        $dt=array();
        $up['id']=$operasi->id;
        if($operasi->id_operator_satu==$d['id_operator'] && $d['success_operator']=="1" && $operasi->id_perujuk==$d['id_perujuk'] && $d['success_perujuk']=="1" ){
            $dt['keterangan']="FIX";
        }else{
            $CODE="";
            if( $d['success_operator']=="1"){
                $CODE.="<strong>[OP_UPDATE]</strong>";  
                $dt['id_operator_satu']=$d['id_operator'];
                $dt['nama_operator_satu']=$d['nama_operator'];
            }
            if($d['success_perujuk']=="1"){
                $CODE.="<strong>[OP_UPDATE]</strong>";  
                $dt['id_perujuk']=$d['id_perujuk'];
                $dt['nama_perujuk']=$d['nama_perujuk']; 
            }
            if($CODE!=""){
                $dt['keterangan']=$CODE."\n".str_replace(PHP_EOL, '', $d['keterangan']);
            }
        }
        
        
        $tarif_ok=$this->getTarifOperasi($dbtable,$operasi);
        if( $tarif_ok['bagi_dokter']>0 && $tarif_ok['bagi_dokter']!=$operator->bagi_dokter){
            $dt['bagi_dokter']=$tarif_ok['bagi_dokter'];
            $dt['keterangan'].=". Koreksi Bagi Dokter ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_dokter']);
        }
        if( $tarif_ok['bagi_asisten_operasi']>0 && $tarif_ok['bagi_asisten_operasi']!=$operator->bagi_asisten_operator){
            $dt['bagi_asisten_operator']=$tarif_ok['bagi_asisten_operasi'];
            $dt['keterangan'].=". Koreksi Bagi Asisten Operasi ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_asisten_operasi']);
        }
        
        
        $dbtable->update($dt, $up);
        return $dt['keterangan'];
    }
    
    /**
     * @brief it will update the data when the source data in database have no value
     *          if the source in database have value, then leave it as is.
     * @param DBTable $dbtable 
     * @param Array $operasi
     * @return string keterangan 
     */
    private function AutoCorretIfEmpty(DBTable &$dbtable,$operasi){
        $d=$this->getOperatorPerujukOperasi($dbtable,$operasi);  
        $dt=array();
        $up['id']=$operasi->id;
        if($operasi->id_operator_satu==$d['id_operator'] && $d['success_operator']=="1" && $operasi->id_perujuk==$d['id_perujuk'] && $d['success_perujuk']=="1" ){
            $dt['keterangan']="FIX";
            $dbtable->update($dt, $up);
        }else{
            $CODE="";
            if($operasi->id_operator_satu=="0" && $d['success_operator']=="1"){
                $CODE.="<strong>[OP_FILLED]</strong>";  
                $dt['id_operator_satu']=$d['id_operator'];
                $dt['nama_operator_satu']=$d['nama_operator'];
            }
            if($operasi->id_perujuk=="0" && $d['success_perujuk']=="1"){
                $CODE.="<strong>[OP_FILLED]</strong>";  
                $dt['id_perujuk']=$d['id_perujuk'];
                $dt['nama_perujuk']=$d['nama_perujuk']; 
            }
            if($CODE!=""){
                $dt['keterangan']=$CODE."\n".str_replace(PHP_EOL, '', $d['keterangan']);
            }
            $up['id']=$operasi->id;
        }
        
        $tarif_ok=$this->getTarifOperasi($dbtable,$operasi);
        if($operator->bagi_dokter==0 && $tarif_ok['bagi_dokter']!=$operator->bagi_dokter){
            $dt['bagi_dokter']=$tarif_ok['bagi_dokter'];
            $dt['keterangan'].=". Koreksi Bagi Dokter ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_dokter']);
        }
        if( $operator->bagi_asisten_operator==0 && $tarif_ok['bagi_asisten_operasi']!=$operator->bagi_asisten_operator){
            $dt['bagi_asisten_operator']=$tarif_ok['bagi_asisten_operasi'];
            $dt['keterangan'].=". Koreksi Bagi Asisten Operasi ".ArrayAdapter::format("only-money Rp.",$tarif_ok['bagi_asisten_operasi']);
        }
        
        $dbtable->update($dt, $up);
        return $dt['keterangan'];
    }
    
    /**
     * @brief get operator and perujuk Operasi
     *        for correcting purpose
     *          it will call service in registration
     * @service - registration : get_operator_perujuk_operasi
     * @param DBTable $dbtable 
     * @param Object $operasi 
     * @return  
     */
    private function getOperatorPerujukOperasi(DBTable &$dbtable,$operasi){
        $data=array();
        $data['tanggal']=$operasi->waktu;
        $data['nrm_pasien']=$operasi->nrm_pasien;
        $data['id_dokter_operator']=$operasi->id_operator_satu;
        $data['nama_dokter_operator']=$operasi->nama_operator_satu;
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->db,"get_operator_perujuk_operasi",$data,"registration");
        $serv->execute();
        $d=$serv->getContent();
        return $d;
    }
    
    /**
     * @brief finding the provit share price of the tarif operasi
     * @param \DBTable $dbtable 
     * @param Object $operasi 
     * @return  
     */
    private function getTarifOperasi(DBTable &$dbtable,$operasi){
        $data['nama_tindakan']=$operasi->nama_tindakan;
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->db,"get_tarif_operasi",$data,"manajemen");
        $serv->execute();
        $d=$serv->getContent();
        return $d;
    }
    
    /**
     * @brief this command for oen limit fixation so the data will 
     *          autocorrect only this one data only
     * @return  String JSON
     */
    private function initLimitOneSuperCommand(){
        $dbtable=new DBTable($this->db, "smis_rwt_ok_".$this->polislug);
        $operasi=$dbtable->select($_POST['id']);
        $keterangan="";
        switch($_POST['autokoreksi']){
            case "compare"  : $keterangan = $this->compareData($dbtable,$operasi);                      break;
            case "full"     : $keterangan = $this->FullAutoCorret($dbtable,$operasi);                   break;
            case "s_emp"    : $keterangan = $this->AutoCorretIfEmpty($dbtable,$operasi);                break;
            case "c_full"   : $keterangan = $this->AutoCorretIfCorrectionNotEmpty($dbtable,$operasi);   break;
        }
        $pack=new ResponsePackage();
        $pack->setContent($keterangan);
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($pack->getPackage());
        return;
    }
    
    
    /**
     * @brief command for handling looping in javascript
     *        used javascript /rawat/resource/js/operasi_detail.js
     *          on function rekaploop()
     * @return  String JSON
     */
    private function initLimitSuperCommand(){
        $dbtable=new DBTable($this->db, "smis_rwt_ok_".$this->polislug);
        $dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'");
        $dbtable->setMaximum(1);
        $limit_start=$_POST['limit_start'];
        $data=$dbtable->view("", $limit_start);
        $operasi=$data['data'][0];
        switch($_POST['autokoreksi']){
            case "compare"  : $this->compareData($dbtable,$operasi);break;
            case "full"     : $this->FullAutoCorret($dbtable,$operasi);break;
            case "s_emp"    : $this->AutoCorretIfEmpty($dbtable,$operasi);break;
            case "c_full"   : $this->AutoCorretIfCorrectionNotEmpty($dbtable,$operasi);break;
        }
        $pack=new ResponsePackage();
        $pack->setContent($operasi);
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($pack->getPackage());
        return;
    }
    
    /**
     * @brief command for handling looping in javascript
     *        used javascript /rawat/resource/js/operasi_detail.js
     *          on function rekaptotal()
     * @return  
     */
    private function initTotalSuperCommand(){
        $dbtable=new DBTable($this->db, "smis_rwt_ok_".$this->polislug);
        $dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'");
        $query=$dbtable->getQueryCount("");
        $content=$this->db->get_var($query);
        $pack=new ResponsePackage();
        $pack->setContent($content);
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($pack->getPackage());
    }

	public function superCommand($super_command) {		
        if($super_command=="limit_one"){
            $this->initLimitOneSuperCommand();
            return;
        }else if($super_command=="limit"){
            $this->initLimitSuperCommand();
            return;
        }else if($super_command=="total"){
            $this->initTotalSuperCommand();
            return;
        }else if($super_command=="detail_history"){
            echo $this->getDetailHistory();
            return;
        }
        
        $super = new SuperCommand ();
        if($super_command=="dokter_operasi_detail"){
            $this->getKaryawanOperasiDetail($super,$super_command,"dokter");
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }else if($super_command=="operator_operasi_detail"){
            $this->getKaryawanOperasiDetail($super,$super_command,"dokter");
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }else if($super_command=="anastesi_operasi_detail"){
            $this->getKaryawanOperasiDetail($super,$super_command,"dokter");
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }else if($super_command=="perujuk_operasi_detail"){
            $this->getKaryawanOperasiDetail($super,$super_command,"dokter");
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }else if($super_command=="asisten_operasi_detail" || $super_command=="oomloop_operasi_detail"){
            $this->getKaryawanOperasiDetail($super,$super_command,"perawat");
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }else if($super_command=="tarif_operasi_detail"){
            $this->getTindakanOperasi($super);
            $init = $super->initialize ();
            if ($init != null) {
                echo $init;
                return;
            }
        }
		
	}
    
    public function getDetailHistory(){
        $id=$_POST['id'];
        $dbtable=new DBTable($this->db,"smis_rwt_ok_".$this->polislug);
        
        $x=$dbtable->select($id);
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->db,"get_history_dokter_utama",null,"registration");
        $serv->addData("nrm",$x->nrm_pasien);
        $serv->addData("tanggal",$x->waktu);
        $serv->execute();
        $content=$serv->getContent();  
        
        $response=new ResponsePackage();
        $response->setStatus(ResponsePackage::$STATUS_OK);
        $response->setWarning(true,"History Data Of ".$id,$content);
        return json_encode($response->getPackage());
    }
  
    
	/* when it's start build */
	public function phpPreload() {
		$map['Carabayar']="carabayar";
        $map['Harga']="harga_operator_satu";
        $map['Operator']="nama_operator_satu";
        $map['Anastesi']="nama_anastesi";
        $map['Perujuk']="nama_perujuk";
        $map['Tanggal']="date(waktu)";
        $map['Tindakan']="nama_tindakan";
        $group=$this->group_by($map);
		
        $omap['Carabayar']=" carabayar ASC ";
        $omap['Operator']=" nama_dokter ASC ";
        $omap['Perujuk']=" nama_perujuk ASC ";
        $omap['Tanggal']="waktu ASC";
        $omap['Tindakan']=" nama_tindakan ASC ";
        $omap['Harga']=" harga_operator_satu DESC ";
        $ordered=$this->order_by($omap);
        
        $autocorrect=new OptionBuilder();
        $autocorrect->add("Compare Data","compare","1");
		$autocorrect->add("Full Auto Correct","full");                           
        $autocorrect->add("Auto Correct if Source Empty","s_emp");          
        $autocorrect->add("Auto Correct if Correction not Empty","c_full");
        
        $jenis_operasi = new OptionBuilder();
        $jenis_operasi ->add("","","1");
        $jenis_operasi ->add("Elektif","Elektif");
        $jenis_operasi ->add("CITO","CITO");
        $jenis_bius = new OptionBuilder();
        $jenis_bius ->add("","","1");
        $jenis_bius ->add("Lokal","Lokal");
        $jenis_bius ->add("Regional / General","Regional / General");
       
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("filter_carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_tindakan", "chooser-operasi_detail-tarif_operasi_detail-Tindakan I", "Tindakan I","");
        $this->uitable->addModal("nama_dokter", "chooser-operasi_detail-dokter_operasi_detail-Operator I", "Dr. Operator I","");
        $this->uitable->addModal("nama_anastesi", "chooser-operasi_detail-anastesi_operasi_detail-Anastesi", "Dr. Anastesi","");
        $this->uitable->addModal("jenis_operasi","select","Jenis Operasi",$jenis_operasi->getContent());
        $this->uitable->addModal("jenis_bius","select","Jenis Bius",$jenis_bius->getContent());
		$this->uitable->addModal("grup", "select", "Grup",$group->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());
        $this->uitable->addModal("autokoreksi", "select", "Koreksi",$autocorrect->getContent());
		$form=$this->uitable->getModal()->getForm();
        
        $this->uitable->clearContent();
        $this->uitable->addModal("id", "hidden", "","");		
        $this->uitable->addModal("nama_operator_satu", "chooser-operasi_detail-operator_operasi_detail-Dr. Operator", "Dr. Operator","");
		$this->uitable->addModal("id_operator_satu", "hidden", "","");		
        $this->uitable->addModal("nama_perujuk", "chooser-operasi_detail-perujuk_operasi_detail-Dr. Perujuk", "Dr. Perujuk","");
		$this->uitable->addModal("id_perujuk", "hidden", "","");        
        $this->uitable->addModal("nama_asisten_operator_satu", "chooser-operasi_detail-asisten_operasi_detail-Asisten Operator", "Asisten","");
		$this->uitable->addModal("id_asisten_operator_satu", "hidden", "","");
		$this->uitable->addModal("nama_oomloop_satu", "chooser-operasi_detail-oomloop_operasi_detail-Oomloop", "Oomloop","");
		$this->uitable->addModal("id_oomloop_satu", "hidden", "","");
        $this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
        $modal=$this->uitable->getModal();
		$modal->setTitle("Operasi");
        
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("operasi_detail.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("operasi_detail.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("operasi_detail.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("operasi_detail.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
        
        $btn=new Button("", "", "Fiksasi Dokter");
		$btn->setAction("operasi_detail.rekaptotal()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-circle-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
        
        $form->addElement("", $btg);
		
        $hidden_poliname=new Hidden("OP_DT_POLINAME","OP_DT_POLINAME",$this->poliname);
		$hidden_polislug=new Hidden("OP_DT_POLISLUG","OP_DT_POLISLUG",$this->polislug);
		
        $close  =new Button("", "", "Batal");
        $close	->addClass("btn-primary")
                ->setIsButton(Button::$ICONIC_TEXT)
                ->setIcon("fa fa-close")
                ->setAction("operasi_detail.batal()");
        
        $load       =new LoadingBar("operasi_detail_bar", "");
        $modalload  =new Modal("operasi_detail_modal", "", "Processing...");
        $modalload	->addHTML($load->getHtml(),"after")
                    ->addFooter($close);
        
        echo $modal->getHtml();
		echo $modalload->getHtml();
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
        echo $hidden_poliname->getHtml();
        echo $hidden_polislug->getHtml();
        echo addJS ( "base-js/smis-base-loading.js");
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
        echo addJS ( "rawat/resource/js/operasi_detail.js",false );
		
	}
}