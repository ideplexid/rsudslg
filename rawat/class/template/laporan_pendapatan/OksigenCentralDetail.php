<?php
require_once 'rawat/class/template/PendapatanTemplate.php';

 class OksigenCentralDetail extends PendapatanTemplate {
	
    public function __construct($db, $polislug, $poliname) {
		parent::__construct ($db, $polislug, $poliname);
		$header=array ("No.",'Waktu Mulai',"Nama Pasien","No. Reg",'NRM',
					'H.Jam(HJ)','H.Menit(HM)','Jam(J)','Menit(M)','Skala(S)',"H.Total (HJ*J+HM*M)*S","Cara Bayar");
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "oksigen_central_detail" );
	}
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("H.Total (HJ*J+HM*M)*S","harga","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu Mulai", "mulai","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("H.Jam(HJ)", "harga_jam","money Rp.");
		$adapter->add("H.Menit(HM)", "harga_menit","money Rp.");
		$adapter->add("Jam(J)", "jam");
		$adapter->add("Menit(M)", "menit");
		$adapter->add("Skala(S)", "skala");
		$adapter->add("H.Total (HJ*J+HM*M)*S", "harga","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu Mulai", "mulai","date d/m/Y");
			$adapter->add("Jumlah", "skala");
			$adapter->addSummary("H.Total (HJ*J+HM*M)*S","harga");
			$adapter->addFixValue("Waktu","Total");
			$adapter->add("Nama Pasien", "nama_pasien");
			$adapter->add("No. Reg", "noreg_pasien");
			$adapter->add("NRM", "nrm_pasien");
			$adapter->add("H.Jam(HJ)", "harga_jam");
			$adapter->add("H.Menit(HM)", "harga_menit");
			$adapter->add("Jam(J)", "jam");
			$adapter->add("Menit(M)", "menit");
			$adapter->add("Skala(S)", "skala");
			$adapter->add("H.Total (HJ*J+HM*M)*S", "harga");
			$adapter->add("Cara Bayar", "carabayar");
		}
	}
    
    private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']=strpos($group,"date(mulai)")!==false?" date(mulai) ":"'-'";
        $syntax['harga']=" sum(harga) ";
        $syntax['harga_jam']=" sum(harga_jam) ";
        $syntax['harga_menit']=" sum(harga_menit) ";
        $syntax['noreg_pasien']=" '-' ";
        $syntax['nrm_pasien']=" '-' ";
        $syntax['nama_pasien']=" '-' ";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_rwt_oksigen_central_".$this->polislug;
        return $result;
    }
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_oksigen_central_' . $this->polislug );
		$this->dbtable->addCustomKriteria(NULL,"mulai>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"mulai<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		$this->dbtable->setOrder ( " mulai ASC " );
		$adapter=new SummaryAdapter();
		
		
		if(isset($_POST['grup']) && $_POST['grup']!="" ){
            $qv=$this->makeSyntax($_POST['grup']);            
            $qc="SELECT COUNT(*) as total FROM smis_rwt_oksigen_central_".$this->polislug;
            $this->dbtable->setPreferredQuery(true,$qv,$qc);
            $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
            $this->dbtable->setGroupBy(true,$_POST['grup']);
            $this->dbtable->setUseWhereforView(true);
            $this->detail($this->uitable,$adapter,$this->dbtable);
            if(strpos($_POST['grup'],"date(mulai)")===false){
                $adapter->add("Waktu","waktu");
            }
        }else{			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}
		
		if(isset($_POST['fix_carabayar'])){
			$this->fixCaraBayar();
		}
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	protected function fixCarabayar(){
		$query="ALTER TABLE `smis_rwt_oksigen_central_".$this->polislug."` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`;";
		$this->db->query($query);
		$query="UPDATE smis_rwt_oksigen_central_".$this->polislug." a
				LEFT JOIN smis_rwt_antrian_".$this->polislug." b
				ON a.noreg_pasien=b.no_register
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
	}
	
	
	
	/* when it's star build */
	public function phpPreload() {
		$map['Carabayar']="carabayar";
        $map['Harga']=" harga ";
        $map['Tanggal']=" date(mulai) ";
        $group=$this->group_by($map);
		
        $omap['Carabayar']=" carabayar ASC ";
        $omap['Tanggal']=" mulai ASC ";
        $omap['Biaya']=" harga DESC ";
        $ordered=$this->order_by($omap);
        
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("grup", "select", "Grup",$group->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("oksigen_central_detail.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("oksigen_central_detail.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("oksigen_central_detail.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("oksigen_central_detail.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var oksigen_central_detail;		
		var alok;		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array('id',"no_register","selesai","nama_pasien","nrm_pasien",'waktu_keluar','cara_keluar','keterangan_keluar',"asal","kelas","jk","carabayar");
			oksigen_central_detail=new TableAction("oksigen_central_detail","<?php echo $this->polislug ?>","oksigen_central_detail",column);
			oksigen_central_detail.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			oksigen_central_detail.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#oksigen_central_detail_dari").val();
				save_data['sampai']=$("#oksigen_central_detail_sampai").val();
				save_data['carabayar']=$("#oksigen_central_detail_carabayar").val();
				save_data['grup']=$("#oksigen_central_detail_grup").val();
				save_data['orderby']=$("#oksigen_central_detail_orderby").val();
				return save_data;
			};	

			oksigen_central_detail.fix_carabayar=function(id){
				var a=this.getRegulerData();
				a['command']="list";
				a['fix_carabayar']="1";
				a['id']=id;
				showLoading();
				$.post("",a,function(res){
					var json=getContent(res);
					oksigen_central_detail.view();
					dismissLoading();
				});
			};
			
			
			
		});
		</script>
<?php
	}
}?>
