<?php
require_once 'rawat/class/template/PendapatanTemplate.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class VisiteDetail extends PendapatanTemplate {
	
    public function __construct($db, $polislug, $poliname) {
		parent::__construct ($db, $polislug, $poliname);
		$header=array ("No.",'Waktu',"Nama Dokter","Nama Pasien","No. Reg",'NRM','Harga','Jumlah',"Total","Cara Bayar");
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "visite_detail" );
	}
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("Total","harga_total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Nama Dokter", "nama_dokter");
		$adapter->add("Harga", "harga","money Rp.");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "harga_total","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
		
		if($_POST['command']=="excel"){
			$dbtable->setShowAll(true);
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga", "harga");
			$adapter->add("Total", "harga_total");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga");
			$adapter->addFixValue("Nama Pasien", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Tindakan", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
		}
	}
    
     private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']=strpos($group,"date(waktu)")!==false?" date(waktu) ":"'-'";
        $syntax['harga']=strpos($group,"harga")!==false?" harga ":"'-'";
        $syntax['jumlah']=" sum(1) ";
        $syntax['harga_total']=" sum(harga) ";
        $syntax['noreg_pasien']=" '-' ";
        $syntax['nrm_pasien']=" '-' ";
        $syntax['nama_pasien']=" '-' ";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        $syntax['nama_dokter']=strpos($group,"nama_dokter")!==false?" nama_dokter ":"'-'";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_rwt_visite_dokter_".$this->polislug;
        return $result;
    }
	
	private function carabayar(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$qv="SELECT '-' as waktu,
					harga as harga,
					sum(1) as jumlah,
					sum(harga) as harga_total,					
					'-' as noreg_pasien,
					'-' as nrm_pasien,
					'-' as nama_pasien,
					'-' as nama_dokter,
					carabayar as carabayar
					FROM smis_rwt_visite_dokter_".$this->polislug."
			";
			
		$qc="SELECT COUNT(*) as total FROM smis_rwt_visite_dokter_".$this->polislug;
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
	}
	
	
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_visite_dokter_' . $this->polislug );
		$this->dbtable->addCustomKriteria(NULL,"waktu>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"waktu<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		
		if($_POST['nama_dokter']!=""){
			$this->dbtable->addCustomKriteria("nama_dokter"," = \"".$_POST['nama_dokter']."\"");
		}
		$this->dbtable->setOrder ( " waktu ASC " );
		$adapter=new SummaryAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']!="" ){
            $qv=$this->makeSyntax($_POST['grup']);            
            $qc="SELECT COUNT(*) as total FROM smis_rwt_visite_dokter_".$this->polislug;
            $this->dbtable->setPreferredQuery(true,$qv,$qc);
            $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
            $this->dbtable->setGroupBy(true,$_POST['grup']);
            $this->dbtable->setUseWhereforView(true);
            $this->detail($this->uitable,$adapter,$this->dbtable);
            if(strpos($_POST['grup'],"date(waktu)")===false){
                $adapter->add("Waktu","waktu");
            }
        }else{
			
			$qv="SELECT date(waktu) as waktu,
					harga as harga,
					1 as jumlah,
					harga as harga_total,
					noreg_pasien as noreg_pasien,
					nrm_pasien as nrm_pasien,
					nama_dokter as nama_dokter,
					nama_pasien as nama_pasien,
					carabayar as carabayar
					FROM smis_rwt_visite_dokter_".$this->polislug."";			
			$qc="SELECT COUNT(*) as total FROM smis_rwt_visite_dokter_".$this->polislug;
			$this->dbtable->setPreferredQuery(true,$qv,$qc);
			$this->dbtable->setUseWhereforView(true);			
			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}
		
		if(isset($_POST['fix_carabayar'])){
			$this->fixCaraBayar();
		}
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	protected function fixCarabayar(){		
		$query="ALTER TABLE `smis_rwt_visite_dokter_".$this->polislug."` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`;";
		$this->db->query($query);
		$query="UPDATE smis_rwt_visite_dokter_".$this->polislug." a
				LEFT JOIN smis_rwt_antrian_".$this->polislug." b
				ON a.noreg_pasien=b.no_register
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
	}
	
	
	public function superCommand($super_command) {
		$dktable = new Table ( array ("Nama","Kelas","Tarif"), "", NULL, true );
		$dktable->setName ( "tarif_visite_detail" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Kelas", "kelas", "unslug" );
		$dkadapter->add ( "Tarif", "tarif", "money Rp." );
		$data_kelas =getSettings ( $this->db, "smis-rs-kelas-" . $this->polislug, "" );
		$tarif = new ServiceResponder ( $this->db, $dktable, $dkadapter, "get_visite" );
		$tarif->addData ( "kelas", $data_kelas );
		
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( "dokter_visite_detail" );
		$dktable->setModel ( Table::$SELECT );
		$dokter = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
		
		$super = new SuperCommand ();
		$super->addResponder ( "tarif_visite_detail", $tarif );
		$super->addResponder ( "dokter_visite_detail", $dokter );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		
        $map['Carabayar']="carabayar";
        $map['Dokter']="nama_dokter";
        $map['Harga']=" harga ";
        $map['Tanggal']="date(waktu)";
        $group=$this->group_by($map);
		
        $omap['Carabayar']=" carabayar ASC ";
        $omap['Dokter']=" nama_dokter ASC ";
        $omap['Jumlah']=" jumlah DESC ";
        $omap['Tanggal']=" waktu ASC ";
        $omap['Harga']=" harga_total DESC ";
        $ordered=$this->order_by($omap);
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_dokter", "chooser-visite_detail-dokter_visite_detail-Dokter", "Dokter","");
		$this->uitable->addModal("grup", "select", "Grup",$group->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("visite_detail.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("visite_detail.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("visite_detail.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("visite_detail.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var visite_detail;		
		var dokter_visite_detail; 		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array("");
			visite_detail=new TableAction("visite_detail","<?php echo $this->polislug ?>","visite_detail",column);
			visite_detail.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			visite_detail.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#visite_detail_dari").val();
				save_data['sampai']=$("#visite_detail_sampai").val();
				save_data['carabayar']=$("#visite_detail_carabayar").val();
				save_data['nama_dokter']=$("#visite_detail_nama_dokter").val();
				save_data['grup']=$("#visite_detail_grup").val();
				save_data['orderby']=$("#visite_detail_orderby").val();
				return save_data;
			};	
			
			
			dokter_visite_detail=new TableAction("dokter_visite_detail","<?php echo $this->polislug ?>","visite_detail",new Array());
			dokter_visite_detail.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			dokter_visite_detail.setSuperCommand("dokter_visite_detail");
			dokter_visite_detail.selected=function(json){
				var nama=json.nama;
				$("#visite_detail_nama_dokter").val(nama);
			};

			visite_detail.excel=function(){
				var data=this.getViewData();
				data['command']="excel";
				download_visite(data);
			};

			visite_detail.fix_carabayar=function(id){
				var a=this.getRegulerData();
				a['command']="list";
				a['fix_carabayar']="1";
				a['id']=id;
				var self=this;
				showLoading();
				$.post("",a,function(res){
					var json=getContent(res);
					$("#"+self.prefix+"_list").html(json.list);
					$("#"+self.prefix+"_pagination").html(json.pagination);	
					dismissLoading();
				});
			};
			
			function download_visite(data){
				var text=	"<form target='downloadFrame' method='POST' action='' id='form_download' >";
						for(var key in data){
							text+="<input type='hidden' value=\""+data[key]+"\" name='"+key+"'>";
						}
						text+="<input type='hidden' value='download_token' name='download_token'>";
					text+="</form>";
					text+='<iframe name="downloadFrame" style="display: none;" src="" />';
					$("#printing_area").html(text);
					var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
					var android = navigator.userAgent.toLowerCase().match(/android/g) ? true : false;
					if (iOS || android)
						$("#form_download").attr("target", "_blank");
					$("#form_download").submit();
			}
			
		});
		</script>
<?php
	}
}?>
