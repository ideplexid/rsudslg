<?php
require_once 'rawat/class/template/PendapatanTemplate.php';

 class AlatObatDetail extends PendapatanTemplate {
	
    public function __construct($db, $polislug, $poliname) {
		parent::__construct ($db, $polislug, $poliname);
		$header=array ("No.",'Waktu',"Nama Pasien","No. Reg",'NRM',"Alat Obat",'Harga','Jumlah',"Total","Cara Bayar");
		$this->uitable->setHeader ( $header );
		$this->uitable->setName ( "alat_obat_detail" );
	}
	
	
	
	private function detail(Table &$uitable,ArrayAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<total>Total</total>");
		$adapter->addSummary("Total","harga","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("Alat Obat", "nama");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Harga", "satuan","money Rp.");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "harga","money Rp.");
		$adapter->add("Cara Bayar", "carabayar","unslug");
		
		if($_POST['command']=="excel"){
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga", "satuan");
			$adapter->add("Total", "harga");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga");
			$adapter->addFixValue("Nama Pasien", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Tindakan", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Harga Tindakan", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
		}
	}
    
     private function makeSyntax($group){
        $syntax=array();
        $syntax['waktu']=strpos($group,"date(tanggal)")!==false?" date(tanggal) ":"'-'";
        $syntax['satuan']=strpos($group,"harga")!==false?" harga ":"'-'";
        $syntax['jumlah']=" sum(jumlah) ";
        $syntax['harga']=" sum(harga*jumlah) ";
        $syntax['noreg_pasien']=" '-' ";
        $syntax['nrm_pasien']=" '-' ";
        $syntax['nama_pasien']=" '-' ";
        $syntax['carabayar']=strpos($group,"carabayar")!==false?" carabayar ":"'-'";
        $syntax['nama']=strpos($group,"nama")!==false?" nama ":"'-'";
        
        $total=count($syntax);
        $result="SELECT ";
        foreach($syntax as $x=>$v){
            $total--;
            $result.=" ".$v." as ".$x;
            if($total>0){
                $result.=",";
            }
        }
        $result.=" FROM smis_rwt_alok_".$this->polislug;
        return $result;
    }
	
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_rwt_alok_' . $this->polislug );
		$this->dbtable->addCustomKriteria(NULL,"tanggal>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"tanggal<'".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("carabayar"," LIKE '".$_POST['carabayar']."'");
		if($_POST['nama']!=""){
			$this->dbtable->addCustomKriteria(" nama "," LIKE '%".$_POST['nama']."%'");
		}
		$this->dbtable->setOrder ( " tanggal ASC " );
		$adapter=new SummaryAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']!="" ){
            $qv=$this->makeSyntax($_POST['grup']);            
            $qc="SELECT COUNT(*) as total FROM smis_rwt_alok_".$this->polislug;
            $this->dbtable->setPreferredQuery(true,$qv,$qc);
            $this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
            $this->dbtable->setGroupBy(true,$_POST['grup']);
            $this->dbtable->setUseWhereforView(true);
            $this->detail($this->uitable,$adapter,$this->dbtable);
            if(strpos($_POST['grup'],"date(tanggal)")===false){
                $adapter->add("Waktu","waktu");
            }
        }else{
			
			$qv="SELECT date(tanggal) as waktu,
					(harga*jumlah) as harga,
					(jumlah) as jumlah,
					harga as satuan,
					noreg_pasien as noreg_pasien,
					nrm_pasien as nrm_pasien,
					nama_pasien as nama_pasien,
					nama as nama,
					carabayar as carabayar
					FROM smis_rwt_alok_".$this->polislug."
			";
			
			$qc="SELECT COUNT(*) as total FROM smis_rwt_alok_".$this->polislug;
			$this->dbtable->setPreferredQuery(true,$qv,$qc);
			$this->dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
			$this->dbtable->setGroupBy(true,"  nama, date(tanggal), satuan, carabayar ");
			$this->dbtable->setUseWhereforView(true);			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}
		
		if(isset($_POST['fix_carabayar'])){
			$this->fixCaraBayar();
		}
		$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter,$this->polislug );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	protected function fixCarabayar(){
		$query="ALTER TABLE `smis_rwt_alok_".$this->polislug."` 
				ADD `carabayar` VARCHAR(32) NOT NULL AFTER `id`;";
		$this->db->query($query);
		$query="UPDATE smis_rwt_alok_".$this->polislug." a
				LEFT JOIN smis_rwt_antrian_".$this->polislug." b
				ON a.noreg_pasien=b.no_register
				SET a.carabayar=b.carabayar WHERE a.carabayar='' ";
		$this->db->query($query);
	}
	
	
	public function superCommand($super_command) {
		$dbtable = new DBTable ( $this->db, "smis_rwt_aok_" . $this->polislug );
		$head=array ("Nama","Satuan","Harga","Kategori" );
		$dktable = new Table ( $head, "", NULL, true );
		$dktable->setName ( "alok" );
		$dktable->setModel ( Table::$SELECT );
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "Satuan", "satuan" );
		$dkadapter->add ( "Harga", "harga", "money Rp." );
		$dkadapter->add ( "Kategori", "kategori" );
		
		$tarif = new DBResponder ( $dbtable, $dktable, $dkadapter);
		$super = new SuperCommand ();
		$super->addResponder ( "alok", $tarif );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		
		$map['Carabayar']="carabayar";
        $map['Harga']=" harga ";
        $map['Alat Obat']=" nama ";
        $map['Tanggal']="date(tanggal)";
        $group=$this->group_by($map);
		
        $omap['Carabayar']=" carabayar ASC ";
        $omap['Alat Obat']=" nama ASC ";
        $omap['Jumlah']=" jumlah DESC ";
        $omap['Tanggal']=" waktu ASC ";
        $omap['Harga']=" harga ASC ";
        $ordered=$this->order_by($omap);
        
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama", "chooser-alat_obat_detail-alok-Alat dan Obat", "Alat Obat","");
		$this->uitable->addModal("grup", "select", "Grup",$group->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$ordered->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("alat_obat_detail.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("alat_obat_detail.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("alat_obat_detail.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Fiksasi Carabayar");
		$btn->setAction("alat_obat_detail.fix_carabayar()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-money");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var alat_obat_detail;		
		var alok;		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array('id',"no_register","selesai","nama_pasien","nrm_pasien",'waktu_keluar','cara_keluar','keterangan_keluar',"asal","kelas","jk","carabayar");
			alat_obat_detail=new TableAction("alat_obat_detail","<?php echo $this->polislug ?>","alat_obat_detail",column);
			alat_obat_detail.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			alat_obat_detail.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#alat_obat_detail_dari").val();
				save_data['sampai']=$("#alat_obat_detail_sampai").val();
				save_data['carabayar']=$("#alat_obat_detail_carabayar").val();
				save_data['nama']=$("#alat_obat_detail_nama").val();
				save_data['grup']=$("#alat_obat_detail_grup").val();
				save_data['orderby']=$("#alat_obat_detail_orderby").val();
				return save_data;
			};	

			alat_obat_detail.fix_carabayar=function(id){
				var a=this.getRegulerData();
				a['command']="list";
				a['fix_carabayar']="1";
				a['id']=id;
				showLoading();
				$.post("",a,function(res){
					var json=getContent(res);
					alat_obat_detail.view();
					dismissLoading();
				});
			};
			
			alok=new TableAction("alok","<?php echo $this->polislug ?>","alat_obat_detail",new Array());
			alok.setPrototipe("<?php echo $this->poliname ?>","<?php echo $this->polislug ?>","rawat");
			alok.setSuperCommand("alok");
			alok.selected=function(json){
				var nama=json.nama;
				$("#alat_obat_detail_nama").val(nama);
			};
			
		});
		</script>
<?php
	}
}?>
