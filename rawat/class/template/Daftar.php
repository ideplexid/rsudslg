<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
class Daftar extends ModulTemplate {
	private $polislug;
	private $db;
	private $tabs;
	private $poliname;
	public function __construct($db, $polislug, $poliname) {
		parent::__construct ();
		$this->polislug = $polislug;
		$this->poliname = $poliname;
		$this->db 		= $db;
	}
	
	public function initialize() {
		if($_POST['action']!='daftar'){
			echo $this->loadTabs($_POST['action']);
		}else{
			parent::initialize();
		}
	}
	
	public function loadTabs($tb) {		
		$slug 	= $_POST ['prototype_slug'];
		$name 	= $_POST ['prototype_name'];
		$db		= $this->db;		
		ob_start();
		$obj	= null;
		
		switch ($tb){
			case "bed_kamar" 			: require_once 'rawat/class/template/daftar/BedKamar.php';          $obj = new BedKamar($db, $slug, $name );          break;	
			case "aok" 					: require_once 'rawat/class/template/daftar/AOK.php'; 		        $obj = new AOK($db, $slug, $name );               break;
			case "aokgudang" 			: require_once 'rawat/class/template/daftar/AOKGudang.php'; 		$obj = new AOKGudang($db, $slug, $name );         break;
			case "aokgudangheroff" 		: require_once 'rawat/class/template/daftar/AOKGudangHerOff.php'; 	$obj = new AOKGudangHerOff($db, $slug, $name );   break;
			case "aokruang" 			: require_once 'rawat/class/template/daftar/AOKRuang.php'; 		    $obj = new AOKRuang($db, $slug, $name );          break;
            case "pulangmassal" 		: require_once 'rawat/class/template/daftar/PulangMassal.php'; 	    $obj = new PulangMassal($db, $slug, $name );      break;
		}
		if($obj!=null) $obj->initialize ();
		$res=ob_get_clean();
		return $res;
	}
	
	/* when it's star build */
	public function phpPreLoad() {
		$this->tabs = new Tabulator('mytabs', "tabs", Tabulator::$POTRAIT );
		$this->tabs->add("bed_kamar",         "Bed Kamar",                $this->loadTabs("bed_kamar" ),        Tabulator::$TYPE_HTML," fa fa-bed");
		$this->tabs->add("aok_gudang",        "Alat Obat Gudang",         $this->loadTabs("aokgudang" ),        Tabulator::$TYPE_HTML," fa fa-medkit");
		$this->tabs->add("aok_gudangheroff",  "Alat Obat Gudang Her Off", $this->loadTabs ("aokgudangheroff"),    Tabulator::$TYPE_HTML," fa fa-trash");
		$this->tabs->add("aok_ruang",         "Alat Obat Ruang",          $this->loadTabs("aokruang" ),         Tabulator::$TYPE_HTML," fa fa-medkit");
		$this->tabs->add("pulang_masal",      "Pulang Massal",            $this->loadTabs("pulangmassal" ),     Tabulator::$TYPE_HTML," fa fa-wheelchair");
		echo $this->tabs->getHtml ();
	}
}

?>





