<?php 

class TagihanOKAdapter extends ArrayAdapter {
	private $content;
    protected $polislug;
    protected $db;
	public function __construct($db,$polislug="") {
		parent::__construct ();
        $this->polislug	= $polislug;
        $this->db		= $db;
        $cur_urjip = getSettings($db,"smis-rs-urjip-".$polislug,"URJ");
        if($cur_urjip!="URJ"){
            $cur_urjip = "URI";
        }
        $this->urjip = $this->polislug=="igd"?"IGD":$cur_urjip;
        require_once "rawat/function/summary_biaya_ok.php";
	}
    
    public function adapt($d) {
		$a                  = array();
		$a ['id']           = $d->id."_o1";
		$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
		$a ['nama']         = "Operator : ".$d->nama_operator_satu.($d->nama_operator_satu!=""?", ".$d->nama_operator_dua:"");
		$a ['biaya']        = $d->harga_tindakan;
		$a ['keterangan']   = $d->nama_tindakan.", ".$d->nama_tindakan_dua ;
		$a ['jumlah']       = 1;
		$a ['satuan']       = $d->harga_tindakan;
		$a ['start']        = $d->waktu;
		$a ['end']          = $d->waktu;
        $a ['debet']        = getSettings($this->db,"smis-rs-acc-d-ok-op1-".$this->polislug."-".$d->carabayar,"");
		$a ['kredit']       = getSettings($this->db,"smis-rs-acc-k-ok-op1-".$this->polislug."-".$d->carabayar,"");
        $this->content[]    = $a;
        
    }
    /*
	public function adapt($d) {
        $a                    	 = array();
		$a ['id']             	 = $d->id."_o1";
		$a ['waktu']          	 = self::format ( "date d M Y", $d->waktu );
		$a ['nama']           	 = "Dokter Operator : ".$d->nama_operator_satu ;
		$a ['biaya']          	 = summary_biaya_ok($d);
		$a ['keterangan']     	 = $d->nama_tindakan ;
		$a ['jumlah']         	 = 1;
		$a ['satuan']         	 = summary_biaya_ok($d);
		$a ['start']          	 = $d->waktu;
		$a ['end']            	 = $d->waktu;
        $a ['debet']          	 = getSettings($this->db,"smis-rs-acc-d-ok-op1-".$this->polislug."-".$d->carabayar,"");
		$a ['kredit']         	 = getSettings($this->db,"smis-rs-acc-k-ok-op1-".$this->polislug."-".$d->carabayar,"");
        $a ['akunting_nama']  	 = "Operator 1 : ".$d->nama_operator_satu ;;
        $a ['akunting_only']  	 = 0;
        $a ['akunting_nilai'] 	 = $d->harga_operator_satu;
        $this->content[]      	 = $a;
		
			
		if($d->harga_operator_dua>0 || $d->nama_operator_dua!=""){
			$a                   = array();
			$a ['id']            = $d->id."_o2";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_operator_dua ;
			$a ['nama']          = "Operator 2 ".$d->nama_operator_dua ;
			$a ['keterangan']    = $d->nama_operator_dua;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_operator_dua;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-op2-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-op2-".$this->polislug."-".$d->carabayar,"");
            $a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}

		if($d->harga_dokter_pendamping>0 || $d->nama_dokter_pendamping!=""){
			$a                   = array();
			$a ['id']            = $d->id."_dp";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_dokter_pendamping ;
			$a ['nama']          = "Dokter Pendamping ".$d->nama_dokter_pendamping ;
			$a ['keterangan']    = $d->nama_dokter_pendamping;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_dokter_pendamping;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-dp-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-dp-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
			$this->content[]     = $a;
		}
		
		if($d->harga_asisten_operator_dua>0 || $d->nama_asisten_operator_dua!=""){
			$a                   = array();
			$a ['id']            = $d->id."_ao2";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_asisten_operator_dua ;
			$a ['nama']          = "Asisten Operator 2 ".$d->nama_asisten_operator_dua ;
			$a ['keterangan']    = $d->nama_asisten_operator_dua;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_asisten_operator_dua;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-aop1-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-aop1-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_asisten_operator_satu>0 || $d->nama_asisten_operator_satu!=""){
			$a                   = array();
			$a ['id']            = $d->id."_ao1";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_asisten_operator_satu ;
			$a ['nama']          = "Asisten Operator 1 ".$d->nama_asisten_operator_satu ;
			$a ['keterangan']    = $d->nama_asisten_operator_satu;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_asisten_operator_satu;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-aop2-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-aop2-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_anastesi>0 || $d->nama_anastesi!=""){
			$hadir				 = $d->anastesi_hadir=="0"?"( - )":"( + )";
			$a                   = array();
			$a ['id']            = $d->id."_a";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_anastesi ;
			$a ['nama']          = "Anastesi ".$hadir. " : ".$d->nama_anastesi ;
			$a ['keterangan']    =  $d->nama_anastesi ;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_anastesi;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
			$a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-ana-".$this->polislug."-".$d->carabayar,"");
			$a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-ana-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
			$this->content[]     = $a;
		}
		
		if($d->harga_asisten_anastesi>0 || $d->nama_asisten_anastesi!=""){
			$a                   = array();
			$a ['id']            = $d->id."_aa";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_asisten_anastesi ;
			$a ['nama']          = "Pranata Anastesi : ".$d->nama_asisten_anastesi ;
			$a ['keterangan']    = $d->nama_asisten_anastesi ;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_asisten_anastesi;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
			$a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-as-ana1-".$this->polislug."-".$d->carabayar,"");
			$a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-as-ana1-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
			$this->content[]     = $a;
		}
		
		
		if($d->harga_asisten_anastesi_dua>0 || $d->nama_asisten_anastesi_dua!=""){
			$a                   = array();
			$a ['id']            = $d->id."_aa2";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_asisten_anastesi_dua ;
			$a ['nama']          = "Pranata Anastesi II : ".$d->nama_asisten_anastesi_dua;
			$a ['keterangan']    = $d->nama_asisten_anastesi_dua ;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_asisten_anastesi_dua;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-as-ana2-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-as-ana2-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_instrument>0 || $d->nama_instrument!=""){
			$a                   = array();
			$a ['id']            = $d->id."_itm1";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_instrument ;
			$a ['nama']          = "Instrument I : ".$d->nama_instrument;
			$a ['keterangan']    = $d->nama_instrument ;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_instrument;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-instrument1-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-instrument1-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
        if($d->harga_instrument_dua>0 || $d->nama_instrument_dua!=""){
			$a                   = array();
			$a ['id']            = $d->id."_itm2";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_instrument_dua;
			$a ['nama']          = "Instrument I : ".$d->nama_instrument_dua;
			$a ['keterangan']    = $d->nama_instrument_dua ;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_instrument_dua;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-instrument2-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-instrument2-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_oomloop_satu>0 || $d->nama_oomloop_satu!=""){
			$a                   = array();
			$a ['id']            = $d->id."_oml1";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_oomloop_satu ;
			$a ['nama']          = "Oomloop I : ".$d->nama_oomloop_satu;
			$a ['keterangan']    = $d->nama_oomloop_satu;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_oomloop_satu;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-oomloop1-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-oomloop1-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_oomloop_dua>0 || $d->nama_oomloop_dua!=""){
			$a                   = array();
			$a ['id']            = $d->id."_oml2";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_oomloop_dua;
			$a ['nama']          = "Oomloop II : ".$d->nama_oomloop_dua;
			$a ['keterangan']    = $d->nama_oomloop_dua;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_oomloop_dua;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-oomloop2-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-oomloop2-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_team_ok>0){
			$a					 = array();
			$a ['id']            = $d->id."_to";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_team_ok ;
			$a ['nama']          = trim($d->nama_team_ok)==""?"Team OK":$d->nama_team_ok ;
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_team_ok;
			$a ['keterangan']    = "";
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
			$a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-team-".$this->polislug."-".$d->carabayar,"");
			$a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-team-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
			$this->content[]     = $a;
		}
		
		if($d->harga_bidan>0){
			$a					 = array();
			$a ['id']            = $d->id."_hb";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_bidan ;
			$a ['nama']          = "Bidan";
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_bidan;
			$a ['keterangan']    = $d->nama_bidan ;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-bidan-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-bidan-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_bidan_dua>0){
			$a                   = array();
			$a ['id']            = $d->id."_hb2";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_bidan_dua ;
			$a ['nama']          = "Bidan II";
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_bidan_dua;
			$a ['keterangan']    = $d->nama_bidan_dua ;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-bidan2-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-bidan2-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_perawat>0){
			$a                   = array();
			$a ['id']            = $d->id."_hp";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_perawat;
			$a ['nama']          = "Perawat";
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_perawat;
			$a ['keterangan']    = $d->nama_perawat ;
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-perawat-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-perawat-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->recovery_room>0){
			$a                   = array();
			$a ['id']            = $d->id."_rr";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->recovery_room;
			$a ['nama']          = "Recovery Room";
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->recovery_room;
			$a ['keterangan']    = "";
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
            $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-rr-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-rr-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
            $this->content[]     = $a;
		}
		
		if($d->harga_sewa_alat){
			$a                   = array();
			$a ['id']            = $d->id."_salt";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_sewa_alat;
			$a ['nama']          = "Sewa Alat";
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_sewa_alat;
			$a ['keterangan']    = "";
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
			$a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-alat-".$this->polislug."-".$d->carabayar,"");
			$a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-alat-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
			$this->content[]     = $a;
		}
        
        if($d->harga_sewa_kamar){
			$a                   = array();
			$a ['id']            = $d->id."_skmr";
			$a ['waktu']         = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']         = $d->harga_sewa_kamar;
			$a ['nama']          = "Sewa Kamar";
			$a ['jumlah']        = 1;
			$a ['satuan']        = $d->harga_sewa_kamar;
			$a ['keterangan']    = "";
			$a ['start']         = $d->waktu;
			$a ['end']           = $d->waktu;
			$a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-sewa-".$this->polislug."-".$d->carabayar,"");
			$a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-sewa-".$this->polislug."-".$d->carabayar,"");
			$a ['akunting_only'] = 1;
			$this->content[]     = $a;
		}		
	}*/
	
	public function getContent($data){
		foreach($data as $d){
			$this->adapt($d);
		}
		return $this->content;
	}
	
}


?>