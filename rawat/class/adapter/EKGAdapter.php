<?php
class EKGAdapter extends SalaryAdapter {
	public function adapt($d) {
		$ruangan = $this->ruangan;
		$waktu = $d ['waktu'];
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$nilai_asli = $d ['biaya'];
		$pembagian = json_decode ( $d ['pembagian'], true );
		$ket = self::format ( "unslug", $d ['kelas'] );
		
		if ( $this->is($d['id_dokter_pengirim'],$pembagian['s-dokter-pengirim'])  ) {
			$honor_persen = $pembagian ['dokter-pengirim'];
			$honor_sebagai = "Dokter Pengirim EKG ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_dokter_pengirim'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
		
		if ($this->is($d['id_dokter_pembaca'],$pembagian['s-dokter-pembaca'])) {
			$honor_persen = $pembagian ['dokter-pembaca'];
			$honor_sebagai = "Dokter Pembaca EKG";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_dokter_pembaca'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
		
		if ($this->is($d ['id_petugas'],$pembagian['s-petugas'])) {
			$honor_persen = $pembagian ['petugas'];
			$honor_sebagai = "Petugas EKG ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$ket = self::format ( "unslug", $d ['kelas'] );
			$karyawan=$d ['nama_petugas'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan,$ket );
		}
		
	}
}

?>