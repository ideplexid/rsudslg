<?php 

class TagihanRigidAdapter extends TagihanAdapter{
	private $content;
	private $total;
	private $total_oksigen;
	private $total_bed;
	
	private $visitename;
	private $total_visite;
	private $total_konsul;
	private $total_penunjang;
	private $total_periksa;
	private $total_tindakan_perawat;
	private $total_tindakan_dokter;
	
	
	public function __construct($db,$polislug="") {
		parent::__construct ($db,$polislug);
		$this->visitename="Visite";
	}
	
	public function setVisiteName($vname){
		$this->visitename=$vname;
	}
	
	public function getTotal(){
		return $this->total;
	}
	
	public function getTotalOksigen(){
		return $this->total_oksigen;
	}
	
	public function getTotalBed(){
		return $this->total_bed;
	}
	
	public function getTotalAlatObatTanpaOksigen(){
		return $this->total_alat_obat-$this->total_oksigen;
	}
	
	public function getTotalAlatObatTanpaBed(){
		return $this->total_alat_obat-$this->total_bed;
	}
	
	public function getTotalAlatObatTanpaOksigenBed(){
		return $this->total_alat_obat-$this->total_bed-$this->total_oksigen;
	}
	
	public function getTotalTindakanPerawat(){
		return $this->total_tindakan_perawat;
	}
	
	public function getTotalPenunjangPerawat(){
		return $this->total_penunjang;
	}
	
	public function getTotalPeriksa(){
		return $this->total_periksa;
	}
	
	public function getTotalKonsul(){
		return $this->total_konsul;
	}
	
	public function getTotalVisite(){
		return $this->total_visite;
	}
	public function getTotalTindakanDokter(){
		return $this->total_tindakan_dokter;
	}
	public function adapt($d) {
		if ($this->adapter_name == "ok") {
			$this->rigidOK($d);
		}else if ($this->adapter_name == "tindakan_dokter") {
			$this->tindakan_dokter($d);
		}else{
			$c=parent::adapt($d);
			$this->total+=$c['biaya'];
			$this->content[]=$c;
		}
	}
	public function rigidOK($d){
        if($d->harga_tindakan>0 ){
			$a                      = array();
			$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
			$a ['nama']             = "Operasi ".$d->nama_tindakan." dan ".$d->nama_tindakan_dua;
			$a ['biaya']            = $d->harga_tindakan;
			$a ['keterangan']       = $d->nama_operator_satu." ".($d->nama_operator_dua!=""?$d->nama_operator_dua:"") ;
			$a ['jumlah']           = 1;
			$a ['satuan']           = $d->harga_tindakan;
			$a ['urutan']           = 100;
			$this->content[]        = $a;
			$this->total           += $a['biaya'];
		}
        /*
		if($d->harga_operator_satu>0 || $d->nama_operator_satu!=""){
			$a                      = array();
			$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
			$a ['nama']             = "Operasi Operator 1";
			$a ['biaya']            = $d->harga_operator_satu;
			$a ['keterangan']       =  $d->nama_operator_satu ;
			$a ['jumlah']           = 1;
			$a ['satuan']           = $d->harga_operator_satu;
			$a ['urutan']           = 100;
			$this->content[]        = $a;
			$this->total           += $a['biaya'];
		}
			
		if($d->harga_operator_dua>0 || $d->nama_operator_dua!=""){
			$a                      = array();
			$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']            = $d->harga_operator_dua ;
			$a ['nama']             = "Operasi Operator 2";
			$a ['keterangan']       = $d->nama_operator_dua;
			$a ['jumlah']           = 1;
			$a ['satuan']           = $d->harga_operator_dua;
			$this->content[]        = $a;
			$a ['urutan']           = 101;
			$this->total           += $a['biaya'];
		}
		
		if($d->harga_asisten_operator_satu>0 || $d->nama_asisten_operator_satu!=""){
			$a                      = array();
			$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
			$a ['nama']             = "Asisten Operator 1";
			$a ['biaya']            = $d->harga_asisten_operator_satu;
			$a ['keterangan']       =  $d->nama_asisten_operator_satu ;
			$a ['jumlah']           = 1;
			$a ['satuan']           = $d->harga_asisten_operator_satu;
			$a ['urutan']           = 100;
			$this->content[]        = $a;
			$this->total           += $a['biaya'];
		}
			
		if($d->harga_asisten_operator_dua>0 || $d->nama_asisten_operator_dua!=""){
			$a                      = array();
			$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']            = $d->harga_asisten_operator_dua ;
			$a ['nama']             = "Asissten Operator 2";
			$a ['keterangan']       = $d->nama_asisten_operator_dua;
			$a ['jumlah']           = 1;
			$a ['satuan']           = $d->harga_asisten_operator_dua;
			$this->content[]        = $a;
			$a ['urutan']           = 101;
			$this->total           += $a['biaya'];
		}


		if($d->harga_anastesi>0 || $d->nama_anastesi !="" ){
			$hadir              = $d->anastesi_hadir=="0"?"( - )":"";
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_anastesi ;
			$a ['nama']         = "Anastesi ".$hadir;
			$a ['keterangan']   =  $d->nama_anastesi ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_anastesi;
			$a ['urutan']       = 101;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}

		if($d->harga_asisten_anastesi>0 || $d->harga_asisten_anastesi!=""){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_asisten_anastesi ;
			$a ['nama']         = "Asisten Anastesi";
			$a ['keterangan']   = $d->nama_asisten_anastesi ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_asisten_anastesi;
			$a ['urutan']       = 102;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
		if($d->harga_asisten_anastesi_dua>0 || $d->harga_asisten_anastesi_dua!=""){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_asisten_anastesi_dua ;
			$a ['nama']         = "Asisten Anastesi 2";
			$a ['keterangan']   = $d->nama_asisten_anastesi_dua ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_asisten_anastesi_dua;
			$a ['urutan']       = 102;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}

		if($d->harga_team_ok>0){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_team_ok ;
			$a ['nama']         = $d->nama_team_ok ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_team_ok;
			$a ['keterangan']   = "";
			$a ['urutan']       = 101;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}

		if($d->harga_bidan>0){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_bidan ;
			$a ['nama']         = "Bidan";
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_bidan;
			$a ['keterangan']   = $d->nama_bidan ;
			$a ['urutan']       = 103;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
		if($d->harga_bidan_dua>0){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_bidan_dua ;
			$a ['nama']         = "Bidan 2";
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_bidan_dua;
			$a ['keterangan']   = $d->nama_bidan_dua ;
			$a ['urutan']       = 103;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}


		if($d->harga_perawat>0){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_perawat;
			$a ['nama']         = "Perawat";
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_perawat;
			$a ['keterangan']   = $d->nama_perawat ;
			$a ['urutan']       = 104;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}

		if($d->harga_sewa_kamar>0){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_sewa_kamar;
			$a ['nama']         = "Sewa Kamar Operasi";
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_sewa_kamar;
			$a ['urutan']       = 105;
			$a ['keterangan']   = "";
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
        
        if($d->harga_sewa_alat>0){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_sewa_alat;
			$a ['nama']         = "Sewa Alat Operasi";
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_sewa_alat;
			$a ['urutan']       = 105;
			$a ['keterangan']   = "";
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
		if($d->nama_oomloop_satu >0 || $d->harga_oomloop_satu!=""){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_oomloop_satu ;
			$a ['nama']         = "Oomloop I";
			$a ['keterangan']   = $d->nama_oomloop_satu ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_oomloop_satu;
			$a ['urutan']       = 102;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
		if($d->nama_oomloop_dua >0 || $d->harga_oomloop_dua!=""){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_oomloop_dua ;
			$a ['nama']         = "Oomloop II";
			$a ['keterangan']   = $d->nama_oomloop_dua ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_oomloop_dua;
			$a ['urutan']       = 102;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
		if($d->nama_instrument >0 || $d->harga_instrument!=""){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_instrument ;
			$a ['nama']         = "Instrument	 I";
			$a ['keterangan']   = $d->nama_instrument ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_instrument;
			$a ['urutan']       = 102;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
		if($d->nama_instrument_dua >0 || $d->harga_instrument_dua!=""){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->harga_instrument_dua ;
			$a ['nama']         = "Instrument II";
			$a ['keterangan']   = $d->nama_instrument_dua ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_instrument_dua;
			$a ['urutan']       = 102;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
		if($d->recovery_room >0 ){
			$a                  = array();
			$a ['waktu']        = self::format ( "date d M Y", $d->waktu );
			$a ['biaya']        = $d->recovery_room ;
			$a ['nama']         = "Recovery Room";
			$a ['keterangan']   = "" ;
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->recovery_room;
			$a ['urutan']       = 102;
			$this->content[]    = $a;
			$this->total       += $a['biaya'];
		}
		
        */
	}


	public function bronchoscopy($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$a ['urutan']           = 90;
		$this->total_penunjang += $d->biaya;
		return $a;
	}
	public function ekg($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = "EKG";
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = $d->nama_dokter_pengirim;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$a ['urutan']           = 80;
		$this->total_penunjang += $d->biaya;
		return $a;
	}
	public function endoscopy($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$a ['urutan']           = 89;
		$this->total_penunjang += $d->biaya;
		return $a;
	}
	public function faal_paru($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$a ['urutan']           = 91;
		$this->total_penunjang += $d->biaya;
		return $a;
	}
	public function fisiotherapy($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$a ['urutan']           = 92;
		return $a;
	}
	public function spirometry($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = "Spirometry";
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$this->total_penunjang += $d->biaya;
		return $a;
	}
	public function konsul_dokter($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = "Konsul " . $d->nama_dokter;
		$a ['biaya']            = $d->harga;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga;
		$a ['urutan']           = 94;
		$this->total_konsul    += $d->harga;
		return $a;
	}
	public function konsultasi_dokter($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = "Periksa";
		$a ['biaya']            = $d->harga;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga;
		$a ['urutan']           = 95;	
		$this->total_periksa   += $d->harga;
		return $a;
	}
	public function visite_dokter($d) {
		$a ['waktu']            = self::format ( "date d M Y", $d->waktu );
		$a ['nama']             = $this->visitename;
		$a ['biaya']            = $d->harga;
		$a ['keterangan']       = $d->nama_dokter;
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga;
		$a ['urutan']           = 80;
		$this->total_visite    += $d->harga;
		return $a;
	}
	public function tindakan_dokter($d) {
		$a                           = array();
		$a ['waktu']                 = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                  = $d->nama_tindakan;
		$a ['biaya']                 = $d->harga;
		$a ['keterangan']            = $d->nama_dokter ;
		$a ['jumlah']                = 1;
		$a ['satuan']                = $d->harga;
		$a ['urutan']                = 81;
		$this->content[]             = $a;
		$this->total                += $a['biaya'];
		$this->total_tindakan_dokter+=$a['biaya'];
		
		$a                            = array();
		$a ['waktu']                  = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                   = "Asisten ".$d->nama_tindakan;
		$a ['biaya']                  = $d->harga_perawat;
		$a ['keterangan']             =  $d->nama_perawat;
		$a ['jumlah']                 = 1;
		$a ['satuan']                 = $d->harga_perawat;
		$a ['urutan']                 = 82;
		$this->content[]              = $a;
		$this->total                 += $a['biaya'];		
		$this->total_tindakan_dokter +=$a['biaya'];
		
		
	}
	public function tindakan_perawat($d) {
		$a ['waktu']                  = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                   = $d->nama_tindakan;
		$a ['biaya']                  = $d->harga_tindakan;
		$a ['keterangan']             = $d->nama_perawat;
		$a ['jumlah']                 = $d->jumlah;
		$a ['satuan']                 = $d->satuan;
		$a ['urutan']                 = 83;
		$this->total_tindakan_perawat+= $d->harga_tindakan;
		return $a;
	}
	public function tindakan_perawat_igd($d) {
		$a ['waktu']                    = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                     = $d->nama_tindakan;
		$a ['biaya']                    = $d->harga_tindakan;
		$a ['keterangan']               = $d->nama_dokter;
		$a ['jumlah']                   = $d->jumlah;
		$a ['satuan']                   = $d->satuan;
		$a ['urutan']                   = 83;
		$this->total_tindakan_perawat  += $d->harga_tindakan;
		return $a;
	}
	public function tindakan_asisten($d) {
		$a ['waktu']                    = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                     = $d->nama_tindakan;
		$a ['biaya']                    = $d->harga_tindakan;
		$a ['keterangan']               = "Asisten (" . $d->nama_asisten . ")";
		$a ['jumlah']                   = 1;
		$a ['satuan']                   = $d->harga_tindakan;
		$a ['urutan']                   = 85;
		return $a;
	}

	public function rr($d) {
		$a ['waktu']                    = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                     = "Recovery Room";
		$a ['jumlah']                   = "1";
		$a ['satuan']                   = $d->harga;
		$a ['biaya']                    = $d->harga;
		$a ['keterangan']               = "";
		$this->total_alat_obat          = $this->total_alat_obat+$d->harga;
		$a ['urutan']=56;
		return $a;
	}
	public function alok($d) {
		$a ['waktu']                    = self::format ( "date d M Y", $d->tanggal );
		$a ['nama']                     = $d->nama;
		$a ['biaya']                    = $d->harga * $d->jumlah;
		$a ['jumlah']                   = $d->jumlah;
		$a ['satuan']                   = $d->harga;
		$a ['keterangan']               = "";
		$a ['urutan']                   = 50;
		$this->total_alat_obat          = $this->total_alat_obat+($d->harga * $d->jumlah);
		return $a;
	}
	public function audiometry($d) {
		$a ['waktu']                    = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                     = $d->nama_tindakan;
		$a ['biaya']                    = $d->biaya;
		$a ['keterangan']               = $d->nama_dokter;
		$a ['jumlah']                   = "1";
		$a ['satuan']                   = $d->biaya;
		$a ['urutan']                   = 89;
		$this->total_penunjang         += $d->biaya;
		return $a;
	}
	public function bed($d) {
		$a ['waktu']                    = self::format ( "date d M Y H:i", $d->waktu_masuk ) . " - " . self::format ( "date d M Y H:i", $d->waktu_keluar );
		$a ['nama']                     = $d->nama_bed;
		$a ['biaya']                    = $d->biaya;
		$a ['keterangan']               = ($d->waktu_masuk=="0000-00-00 00:00:00" ||  $d->waktu_keluar=="0000-00-00 00:00:00")?"<font class='label label-important'>Cek Kembali!!! </font>":$d->jam . " Jam " . $d->menit . " Menit ";
		$a ['jumlah']                   = $d->hari;
		$a ['satuan']                   = $d->harga;
		$a ['urutan']                   = 51;
		$this->total_alat_obat          = $this->total_alat_obat+$d->harga;
		$this->total_bed                = $this->total_bed+$d->hari*$d->harga;
		return $a;
	}
	public function oksigen_central($d) {
		$a ['waktu']                    = self::format ( "date d M Y H:i", $d->mulai ) . " - " . self::format ( "date d M Y H:i", $d->selesai );
		$a ['nama']                     = "Oksigen Central";
		$a ['biaya']                    = $d->harga;
		$a ['keterangan']               = ($d->mulai=="0000-00-00 00:00:00" || $d->selesai=="0000-00-00 00:00:00")?"<font class='label label-important'>Cek Kembali!!! </font>":$d->jam . " Jam " . $d->menit . " Menit ";
		$a ['jumlah']                   = 1;
		$a ['satuan']                   = $d->harga;
		$a ['urutan']                   = 52;
		$this->total_alat_obat          = $this->total_alat_obat+$d->harga;
		$this->total_oksigen            = $this->total_oksigen+$d->harga;
		return $a;
	}
	public function oksigen_manual($d) {
		$a ['waktu']                    = self::format ( "date d M Y", $d->waktu );
		$a ['nama']                     = "Oksigen Manual";
		$a ['biaya']                    = $d->harga;
		$a ['keterangan']               = "";
		$a ['jumlah']                   = $d->jumlah_liter;
		$a ['satuan']                   = $d->harga_liter;
		$a ['urutan']                   = 53;
		$this->total_alat_obat          = $this->total_alat_obat+$d->harga;
		$this->total_oksigen            = $this->total_oksigen+$d->harga;
		return $a;
	}

	public function getContent($data){
		$this->content=array();
		foreach($data as $d){
			$this->adapt($d);
		}
		return $this->content;
	}
	
}


?>