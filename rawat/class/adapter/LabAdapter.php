<?php

class LabAdapter extends SimpleAdapter{
	
	public function adapt($d){
		$array=parent::adapt($d);
		
		$flag="<div class='pagebreak'> </div>";
		$strpos1=strpos($d['cetak_gabung'], $flag);
		$strpos2=strpos($d['cetak_gabung'], $flag,$strpos1+strlen($flag));
		$hasilnya=substr($d['cetak_gabung'],$strpos1+strlen($flag), $strpos2-$strpos1-strlen($flag));
		
		$hasil="<div class='hide' id='hasil_lab_".$d['id']."'>".$hasilnya."</div>";
		
		$btn=new Button("", "", "Hasil");
		$btn->setClass("btn-primary");
		$btn->setAction("lab.hasil('".$d['id']."')");
		$btn->setIsButton(Button::$ICONIC);
		$btn->setIcon("fa fa-expand");
		$array['Hasil']=$btn->getHtml().$hasil;
		return $array;
	}
	
}

?>