<?php 

class OkAdapter extends ArrayAdapter {
	private $complete;
	public function __construct($complete){
		parent::__construct();
		$this->complete=$complete;
        require_once "rawat/function/summary_biaya_ok.php";
	}
	
	public function adaptSimple($d){
		$a = array ();
		$a ['id'] = $d->id;
		$team = array (
				"Operator I" => $d->nama_operator_satu,
				"Operator II" => $d->nama_operator_dua,
				"Anastesi" => $d->nama_anastesi,
				"Asisten " => $d->nama_asisten_anastesi,
				"Bidan" => $d->nama_bidan,
				"Perawat " => $d->nama_perawat,
				"Team OK" => $d->nama_team_ok,
				"Kamar" => $d->nama_sewa_kamar,
				"Perujuk" => $d->nama_perujuk
		);
		
		$biaya = array (
				"Operator I" => $d->harga_operator_satu,
				"Operator II" => $d->harga_operator_dua,
				"Anastesi" => $d->harga_anastesi,
				"Asisten " => $d->harga_asisten_anastesi,
				"Bidan" => $d->harga_bidan,
				"Perawat " => $d->harga_perawat,
				"Team OK" => $d->harga_team_ok,
				"Kamar" => $d->harga_sewa_kamar,
                "Alat" => $d->harga_sewa_alat
		);
		$a ['Tanggal'] = self::format ( "date d-M-Y H:i", $d->waktu );
		$a ['Team'] = self::format ( "array", $team );
		$a ['Biaya'] = self::format ( "array-money Rp.", $biaya );
		//$sum = 0;
		//foreach ( $biaya as $b => $v ) {
		//	$sum += ($v * 1);
		//}
		$a ['Total'] = self::format ( "money Rp.",  $d->harga_tindakan);
		return $a;
	}
	
    public function adapt($d){
        if(is_array($d)){
            $d          = (object) $d;
        }
        $a              = array ();
		$a['id']        = $d->id;
        $a['Total']     = self::moneyFormat("money Rp.",$d->harga_tindakan);//summary_biaya_ok($d)
        $a['Tanggal']   = self::dateFormat("date d M Y H:i", $d->waktu );
        $a['Dokter']    = $d->nama_operator_satu;
        $a['Tindakan 1']  = $d->nama_tindakan;
        $a['Tindakan 2']  = $d->nama_tindakan_dua;
		return $a;
	}
    
	/*public function adapt($d){
        if(is_array($d)){
            $d=(object) $d;
        }
		if($this->complete)
			return $this->adaptComplete($d);
		return $this->adaptSimple($d);
	}*/
	
	public function adaptComplete($d) {
		$a = array ();
		$a ['id'] = $d->id;

		$team = array (
				"Operator I" => $d->nama_operator_satu,
				"Operator II" => $d->nama_operator_dua,
				"Asisten Operator I" => $d->nama_asisten_operator_satu,
				"Asisten Operator II" => $d->nama_asisten_operator_dua,
				"Anastesi" => $d->nama_anastesi,
				"Asisten Anastesi I" => $d->nama_asisten_anastesi,
				"Asisten Anastesi II" => $d->nama_asisten_anastesi_dua,
				"Bidan I" => $d->nama_bidan,
				"Bidan II" => $d->nama_bidan_dua,
				"Perawat " => $d->nama_perawat,
				"Team OK" => $d->nama_team_ok,
				"Oomloop I" => $d->nama_oomloop_satu,
				"Oomloop II" => $d->nama_oomloop_dua,
				"Instrument I" => $d->nama_instrument,
				"Instrument II" => $d->nama_instrument_dua,
				"Kamar" => $d->nama_sewa_kamar,
				"Perujuk" => $d->nama_perujuk
		);

		$biaya = array (
				"Operator I" => $d->harga_operator_satu,
				"Operator II" => $d->harga_operator_dua,
				"Asisten Operator I" => $d->harga_asisten_operator_satu,
				"Asisten Operator II" => $d->harga_asisten_operator_dua,
				"Anastesi" => $d->harga_anastesi,
				"Asisten Anastesi I" => $d->harga_asisten_anastesi,
				"Asisten Anastesi II" => $d->harga_asisten_anastesi_dua,
				"Bidan I" => $d->harga_bidan,
				"Bidan II" => $d->harga_bidan_dua,
				"Perawat " => $d->harga_perawat,
				"Team OK" => $d->harga_team_ok,
				"Oomloop I" => $d->harga_oomloop_satu,
				"Oomloop II" => $d->harga_oomloop_dua,
				"Instrument I" => $d->harga_instrument,
				"Instrument II" => $d->harga_instrument_dua,
				"Kamar" => $d->harga_sewa_kamar,
                "Alat" => $d->harga_sewa_alat,
				"Recovery Room" => $d->recovery_room
		);
		
		

		$a ['Tanggal'] = self::format ( "date d-M-Y H:i", $d->waktu );
		$a ['Team'] = self::format ( "array", $team );
		$a ['Biaya'] = self::format ( "array-money Rp.", $biaya );
		//$sum = 0;
		//foreach ( $biaya as $b => $v ) {
		//	$sum += ($v * 1);
		//}
		$a ['Total'] = self::format ( "money Rp.", $d->harga_tindakan );
		return $a;
	}
}

?>