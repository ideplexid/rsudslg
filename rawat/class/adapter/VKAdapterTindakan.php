<?php 

class VkAdapter extends ArrayAdapter {
	public function adapt($d) {
        if(is_array($d)){
            $d = (object) $d;
        }
		$a = array ();
		$a ['id'] = $d->id;

		$team = array (
				"Operator I" => $d->nama_operator_satu,
				"Operator II" => $d->nama_operator_dua,
				"Anastesi" => $d->nama_anastesi,
				"Asisten " => $d->nama_asisten_anastesi,
				"Bidan" => $d->nama_bidan,
				"Perawat " => $d->nama_perawat,
				"Team VK" => $d->nama_team_vk,
				"Kamar" => $d->nama_sewa_kamar
		);

		$biaya = array (
				"Operator I" => $d->harga_operator_satu,
				"Operator II" => $d->harga_operator_dua,
				"Anastesi" => $d->harga_anastesi,
				"Asisten " => $d->harga_asisten_anastesi,
				"Bidan" => $d->harga_bidan,
				"Perawat " => $d->harga_perawat,
				"team VK" => $d->harga_team_vk,
				"Kamar" => $d->harga_sewa_kamar
		);

		$a ['Tanggal'] = self::format ( "date d-M-Y", $d->waktu );
		$a ['Team'] = self::format ( "array", $team );
		$a ['Biaya'] = self::format ( "array-money Rp.", $biaya );
		$sum = 0;
		foreach ( $biaya as $b => $v ) {
			$sum += ($v * 1);
		}
		$a ['Total'] = self::format ( "money Rp.", $sum );

		return $a;
	}
}

?>