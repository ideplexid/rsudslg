<?php
class TindakanIGDAdapter extends SalaryAdapter {
	public function adapt($d) {
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$nilai_asli = $d ['harga_tindakan'];
		$pembagian = json_decode ( $d ['pembagian'], true );
		$waktu = $d ['waktu'];
		$ruangan = $this->ruangan;
		$ket = $d ['nama_tindakan'];
		$karyawan=$d['nama_perawat'];
			
		if ($this->is($d ['id_perawat'],$pembagian['s-perawat']) ) {
			$honor_persen = $pembagian ['perawat'];
			$honor_sebagai = $d['nama_tindakan'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
        
        $karyawan=$d['nama_dokter'];
        if ($this->is($d ['id_dokter'],$pembagian['s-dokter']) ) {
			$honor_persen = $pembagian ['dokter'];
			$honor_sebagai = $d['nama_tindakan'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
	}
}

?>