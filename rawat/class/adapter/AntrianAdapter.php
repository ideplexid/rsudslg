<?php 
loadLibrary("smis-libs-function-medical");
require_once "rawat/class/MapRuangan.php";
class AntrianAdapter extends ArrayAdapter {
	private $jumlah_pasien=0;
	private $jumlah_bed=0;
	public function __construct($jumlah_pasien){
		$this->jumlah_pasien=$jumlah_pasien;
        loadLibrary("smis-libs-function-time");
		parent::__construct();
	}
	
	public function getDokumenStatus($id,$dokumen){
		$content=get_dokumen_status($dokumen);
		$selection=new Select("","",$content);
		$selection->setDisabled(true);
		$selection->setAction("antrian.save_document('".$id."',this.value)");
		return $selection->getHtml();
	}
	
	public function getJumlahBed(){
		return $this->jumlah_bed;
	}
	
	public function setJumlahBed($jml){
		$this->jumlah_bed=$jml;
	}
	
	public function getContent($set){
		$result=parent::getContent($set);
		$total=array();
		$total['Asal']="<strong>Jumlah Pasien Aktif</strong>";
		$total['Kunjungan']=$this->jumlah_pasien." Orang";
		$total['Umur']="<strong>Jumlah Bed</strong>";
		$total['Cara Bayar']=$this->jumlah_bed." Bed";
		if($this->jumlah_bed<$this->jumlah_pasien){
			$total['Alamat']="<i class='fa fa-warning fa-3x' ></i> ";
			$total['Dokumen']="Jumlah Pasien Sudah Melebihi Kapasitas !!";
		}		
		$result[]=$total;
		return $result;
	}
	
	public function adapt($d) {
		$detail 			= json_decode($d->detail,true);
		$l                  = array ();
		$l['id']            = $d->id;
		$l['Nomor']         = $d->nomor;
		$l['Pasien']        = $d->nama_pasien;
		$l['Kunjungan']     = $d->kunjungan;
		$l['Cara Bayar']    = $d->carabayar;
		$l['Umur']          = $d->umur;
		$l['Alamat']        = $d->alamat;
		$l['Asal']          = MapRuangan::getRealName(trim(strtolower($d->asal)));//self::format("unslug", $d->asal);
		$l['Golongan']      = $d->golongan_umur;
		$l['Status Pasien'] = $d->status_pasien;
		$l['Dokumen']       = $this->getDokumenStatus($d->id,$d->dokumen);
		$l['Jenis Kelamin'] = $d->jk == "0" ? "Laki-laki" : "Perempuan";
		$l['NRM']           = self::format('digit6', $d->nrm_pasien);
        $l['Titipan']       = self::format('trivial_1_<i class="fa fa-check">_', $d->titipan);
		$compare            = date_compare($d->waktu, date("Y-m-d"));
        $l["Waktu"]         = self::format("date d-M-Y H:i", $d->waktu);
        $l["Waktu Register"]= self::format("date d-M-Y H:i", $d->waktu_register);
		$l["No. Profile"]   = $detail['no_profile'];
		$l["Keterangan"]    = $d->keterangan_pindah;
		$l["Status Pasien"]    = "<a class='btn btn-primary' onclick='antrian.resume_medis(".$d->id.")'> View</a>";

		if ($compare == - 1) {
			$l["Waktu"]     = "<font class='label label-important'>".$l["Waktu"]."</font>";
		} else if ($compare == 1) {
			$l["Waktu"]     = "<font class='label label-success'>".$l["Waktu"]."</font>";
		} else {
			$l["Waktu"]     = "<font class='label label-info'>".$l["Waktu"]."</font>";
		}
		$l["No. Register"]  = self::format('digit6',$d->no_register);
		$l["No. Bed"]       = $d->bed_kamar;
		return $l;
	}
}
?>