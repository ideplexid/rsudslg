<?php 

class DokterDPJPMultipleAdapter extends SimpleAdapter{
	
	public function adapt($ar){
		$this->number++;
		$array=array();
		$array["No."]=$this->number.".";
		$array["id"]=$this->number;
		$array['Dokter']=$ar['nama_dokter'];
		$array['Dari']=self::format("date d M Y H:i",$ar['waktu']);
		$array['Sampai']=isset($ar['waktu_selesai']) ? self::format("date d M Y H:i",$ar['waktu_selesai']):"";
		return $array;
	}
	
}

?>