<?php
class KonsultasiDokterAdapter extends SalaryAdapter {
	public function adapt($d) {
		$pembagian = json_decode ( $d ['pembagian'], true );
		if ($this->is($d['id_dokter'], $pembagian['s-dokter'])) {
			$nama = $d ['nama_pasien'];
			$nrm = $d ['nrm_pasien'];
			$nilai_asli = $d ['harga'];
			$jenis = $d ['jenis_dokter'];
			$waktu = $d ['waktu'];
			$ruangan = $this->ruangan;
			$honor_persen = $pembagian ['dokter'];			
			$honor_sebagai = "Konsultasi Dokter ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$ket = self::format ( "unslug", $d ['kelas'] );
			$karyawan=$d['nama_dokter'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
	}
}

?>