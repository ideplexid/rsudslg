<?php
class VKAdapter extends SalaryAdapter {
	public function adapt($d) {
		$ruangan = $this->ruangan;
		$waktu = $d ['waktu'];
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$pembagian = json_decode ( $d ['pembagian'], true );
		$ket = self::format ( "unslug", $d ['kelas'] );
		
		/*BIDAN*/
		if ($this->is($d ['id_bidan'],$pembagian['s-team-bidan']) ) {
			$ket = self::format ( "unslug", $d ['kelas'] );
			$nilai_asli = $d ['harga_bidan'];
			$honor_persen = $pembagian ['team-bidan'];
			$honor_sebagai = "Bidan ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d['nama_bidan'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
		
		/*PERAWAT*/
		if ($this->is($d ['id_perawat'],$pembagian['s-team-perawat']) ) {
			$ket = self::format ( "unslug", $d ['kelas'] );
			$nilai_asli = $d ['harga_perawat'];
			$honor_persen = $pembagian ['team-perawat'];
			$honor_sebagai = "Perawat VK ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d['nama_perawat'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
		
		/*TEAM VK*/
		if ($this->is($d ['id_team_vk'],$pembagian['s-team-vk']) ) {
			$ket = self::format ( "unslug", $d ['kelas'] );
			$nilai_asli = $d ['harga_team_vk'];
			$honor_persen = $pembagian ['team-vk'];
			$honor_sebagai = "Team VK ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d['nama_team_vk'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
		
		/*OPERATOR I*/
		$nilai_asli = $d ['harga_operator_satu'];
		$honor_sebagai = "Operator VK I ";
		$karyawan=$d['nama_operator_satu'];
		if ($d ['jenis_operator_satu'] == "dokter-organik" && $this->is($d ['id_operator_satu'],$pembagian['s-dorganik']) ){
			$honor_persen = $pembagian ['dorganik'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}else if ( $this->is($d ['id_operator_satu'],$pembagian['s-dtamu']) ){
			$honor_persen = $pembagian ['dtamu'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
		
		/*OPERATOR II*/
		$nilai_asli = $d ['harga_operator_dua'];
		$honor_sebagai = "Operator VK II ";
		$karyawan=$d['nama_operator_dua'];
		if ($d ['jenis_operator_dua'] == "dokter-organik" && $this->is($d ['id_operator_dua'],$pembagian['s-dorganik']) ){
			$honor_persen = $pembagian ['dorganik'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}else if ( $this->is($d ['id_operator_dua'],$pembagian['s-dtamu']) ){
			$honor_persen = $pembagian ['dtamu'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
		
		/*ANASTESI*/
		if ($d ['anastesi_hadir'] == "0" && $this->is($d ['id_anastesi'],$pembagian['s-anastesi-hadir']) ) {
			$nilai_asli = $d ['harga_anastesi'];
			$honor_persen = $pembagian ['anastesi-hadir'];
			$honor_sebagai = "Anastesi VK ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$ket .=" (HADIR) ";
			$karyawan=$d['nama_anastesi'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}else if($this->is($d ['id_anastesi'],$pembagian['s-anastesi-thadir']) ){
			$nilai_asli = $d ['harga_anastesi'];
			$honor_persen = $pembagian ['anastesi-thadir'];
			$ket .= " (TIDAK HADIR) ";
			$honor_sebagai = "Anastesi VK ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d['nama_anastesi'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
		
		/*ASISTEN ANASTESI*/
		$ket = self::format ( "unslug", $d ['kelas'] );
		$nilai_asli = $d ['harga_asisten_anastesi'];
		$karyawan=$d['nama_asisten_anastesi'];
		$honor_sebagai = "Asisten Anastesi VK ";
		if ($d ['anastesi_hadir'] == "0" && $this->is($d ['id_asisten_anastesi'],$pembagian['s-as-anastesi-hadir']) ) {
			$honor_persen = $pembagian ['as-anastesi-hadir'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}else if($this->is($d ['id_asisten_anastesi'],$pembagian['s-as-anastesi-thadir'])){
			$honor_persen = $pembagian ['as-anastesi-thadir'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan ,$ket);
		}
	}
}

?>