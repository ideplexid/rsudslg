<?php
class TindakanKeperawatanAdapter extends SalaryAdapter {
	public function adapt($d) {
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$nilai_asli = $d ['harga_tindakan'];
		$pembagian = json_decode ( $d ['pembagian'], true );
		$jenis = "perawat";
		$waktu = $d ['waktu'];
		$ruangan = $this->ruangan;
		$ket = $d ['nama_tindakan'];
		$karyawan=$d['nama_perawat'];
			
		if ($jenis == "perawat" && $this->is($d ['id_perawat'],$pembagian['s-perawat']) ) {
			$honor_persen = $pembagian ['perawat'];
			$honor_sebagai = $d['nama_tindakan'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}		
	}
}

?>
