<?php 
class OksigenAdapter extends ArrayAdapter {
	public function adapt($d) {
        if(is_array($d)){
            $d = (object) $d;
        }
		$l = array ();
		$l ['id'] = $d->id;

		$harga [""] = self::format ( 'money Rp.', $d->harga_jam );
		$harga [" "] = self::format ( 'money Rp.', $d->harga_menit );
		$l ['Harga'] = self::format ( "array", $harga );

		$durasi ['Jam'] = $d->jam . " Jam";
		$durasi ['Menit'] = $d->menit . " Menit";
		$l ['Durasi'] = self::format ( "array", $durasi );

		$waktu ['Mulai'] = self::format ( "date d M Y H:i", $d->mulai );
		$waktu ['Selesai'] = self::format ( "date d M Y H:i", $d->selesai );
		$l ['Waktu'] = self::format ( "array", $waktu );
		$l ['Skala'] = $d->skala;

		$l ['Total'] = self::format ( 'money Rp.', $d->harga );
		return $l;
	}
}

?>