<?php

class DaftarBedKamarAdapter extends SimpleAdapter{

    private $palicare;
    public function __construct($use_number=false,$number_name=""){
        parent::__construct($use_number,$number_name);
        $this->palicare["NN"]  =  "-";
        $this->palicare["VVP"] = "VVIP";
        $this->palicare["VIP"] = "VIP";
        $this->palicare["UTM"] = "UTAMA";
        $this->palicare["KL1"] = "KELAS I";
        $this->palicare["KL2"] = "KELAS II";
        $this->palicare["KL3"] = "KELAS III";
        $this->palicare["ICU"] = "ICU";
        $this->palicare["ICC"] = "ICCU";
        $this->palicare["NIC"] = "NICU";
        $this->palicare["PIC"] = "PICU";
        $this->palicare["IGD"] = "IGD";
        $this->palicare["UGD"] = "UGD";
        $this->palicare["SAL"] = "RUANG BERSALN";
        $this->palicare["HCU"] = "HCU";
        $this->palicare["ISO"] = "RUANG ISOLASI";
    }

    public function adapt($d){
        $result = parent::adapt($d);
        $result['Kelas Aplicare'] = $this->palicare[$d->kelas_aplicare];
        return $result;
    }

}