<?php
class TindakanPerawatAdapter extends SalaryAdapter {
	public function adapt($d) {
		$ruangan = $this->ruangan;
		$waktu = $d ['waktu'];
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$nilai_asli = $d ['harga_tindakan'];
		$pembagian = json_decode ( $d ['pembagian'], true );
		if ($this->is($d ['id_perawat'],$pembagian['s-perawat'])) {
			$honor_persen = $pembagian ['perawat'];
			$honor_sebagai = "Tindakan Perawat ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$ket = $d['nama_tindakan']." ".self::format ( "unslug", $d ['kelas'] )." @ ".ArrayAdapter::format("only-money", $d['satuan'])." x ".$d['jumlah'];
			$karyawan=$d ['nama_perawat'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan,$ket );
		}
	}
}

?>                                                                  