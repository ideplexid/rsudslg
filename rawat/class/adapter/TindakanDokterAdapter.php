<?php
class TindakanDokterAdapter extends SalaryAdapter {
	public function adapt($d) {
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$nilai_asli = $d ['harga'];
		$pembagian = json_decode ( $d ['pembagian'], true );
		$jenis = $d ['jenis_dokter'];
		$waktu = $d ['waktu'];
		$ruangan = $this->ruangan;
		$ket = $d ['nama_tindakan'];
		$karyawan=$d['nama_dokter'];
			
		if ($jenis == "dokter-organik" && $this->is($d ['id_dokter'],$pembagian['s-dorganik']) ) {
			$honor_persen = $pembagian ['dorganik'];
			$honor_sebagai = "Tindakan Dokter Organik";
			$nilai = $honor_persen * $nilai_asli / 100;
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}else if($this->is($d ['id_dokter'],$pembagian['s-dtamu'])){
			$honor_persen = $pembagian ['dtamu'];
			$nilai = $honor_persen * $nilai_asli / 100;
			$honor_sebagai = "Tindakan Dokter Tamu";
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
		
		if($this->is($d ['id_asisten'],$pembagian['s-asisten'])){
			$nilai_asli = $d ['harga_asisten'];
			$karyawan=$d['nama_asisten'];
			$honor_persen = $pembagian ['asisten'];
			$nilai = $honor_persen * $nilai_asli  / 100;
			$honor_sebagai = "Asisten Tindakan Dokter ";
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
		
	}
}

?>