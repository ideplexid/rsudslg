<?php
class RRAdapter extends SalaryAdapter {
	public function adapt($d) {
		$ruangan = $this->ruangan;
		$waktu = $d ['waktu'];
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$nilai_asli = $d ['harga'];
		$pembagian = json_decode ( $d ['pembagian'], true );
		$ket = self::format ( "unslug", $d ['kelas'] );
		
		if ($this->id_karyawan == $d ['id_asisten']) {
			$honor_persen = $pembagian ['asisten-rr'];
			$honor_sebagai = "Asisten Recovery Room ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_asisten'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan,$ket );
		}
		
		if ($this->is($d ['id_petugas'],$pembagian['s-petugas'])) {
			$honor_persen = $pembagian ['petugas'];
			$honor_sebagai = "Petugas Recovery Room ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_petugas'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan,$ket );
		}
		
	}
}

?>