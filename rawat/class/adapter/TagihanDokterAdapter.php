<?php 

class TagihanDokterAdapter extends ArrayAdapter {
	private $content;
    protected $polislug;
    protected $db;
	public function __construct($db,$polislug="") {
		parent::__construct ();
        $this->polislug=$polislug;
        $this->db=$db;
        $cur_urjip = getSettings($db,"smis-rs-urjip-".$polislug,"URJ");
        if($cur_urjip!="URJ"){
            $cur_urjip = "URI";
        }
        $this->urjip = $this->polislug=="igd"?"IGD":$cur_urjip;
	}
	
	public function adapt($d) {		
		$a                  = array();
		$a ['id']           = $d->id;
		$a ['waktu']        = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']         = $d->nama_tindakan." (" . $d->nama_dokter . ") ";
		$a ['biaya']        = $d->harga;
		$a ['keterangan']   = "Dokter (" . $d->nama_dokter . ") : ".ArrayAdapter::format("only-money Rp.", $d->harga);
		$a ['jumlah']       = 1;
		$a ['satuan']       = $d->harga;
		$a ['start']        = $d->waktu;
		$a ['end']          = $d->waktu;
        $a ['debet']        = getSettings($this->db,"smis-rs-acc-d-tindakan-dokter-".$this->polislug."-".$d->carabayar,"");
		$a ['kredit']       = getSettings($this->db,"smis-rs-acc-k-tindakan-dokter-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']  = $d->nama_dokter;
        $a ['id_dokter']    = $d->id_dokter;
		
		$a ['jaspel_persen']    = $d->jaspel_persen;
		$a ['jaspel_dokter']    = $d->jaspel_nilai;     
		
		$a ['jaspel_persen_perawat']    = $d->jaspel_persen_perawat;
        $a ['jaspel_perawat']    		= $d->jaspel_nilai_perawat;    

        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        
        $this->content[]    = $a;

		if($d->harga_perawat>0){
			$a                  = array();
			$a ['id']           = $d->id."_atd";
			$a ['waktu']        = self::format ( "date d-M-Y", $d->waktu );
			$a ['nama']         = "Assisten ".$d->nama_tindakan;
			$a ['biaya']        = $d->harga_perawat;
			$a ['keterangan']   = "Assisten (".$d->nama_asisten." ) : ".ArrayAdapter::format("only-money Rp.", $d->harga_perawat);
			$a ['jumlah']       = 1;
			$a ['satuan']       = $d->harga_perawat;
			$a ['start']        = $d->waktu;
			$a ['end']          = $d->waktu;
            $a ['debet']        = getSettings($this->db,"smis-rs-acc-d-perawat-tindakan-dokter-".$this->polislug."-".$d->carabayar,"");
            $a ['kredit']       = getSettings($this->db,"smis-rs-acc-k-perawat-tindakan-dokter-".$this->polislug."-".$d->carabayar,"");
            
            $a ['urjigd']          = $this->urjip;
            $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
            
            $this->content[]    = $a;
		}
	}
	
	public function getContent($data){
		foreach($data as $d){
			$this->adapt($d);
		}
		return $this->content;
	}
	
}


?>