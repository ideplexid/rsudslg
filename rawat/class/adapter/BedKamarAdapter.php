<?php 

class BedKamarAdapter extends ArrayAdapter {
	public function adapt($d) {
		$a = array ();
		$a ['name'] = $d->nama . " " . ($d->terpakai == 1 ? " (Terpakai " . $d->nama_pasien . " ) " : "");
		$a ['value'] = $d->id;
		return $a;
	}
}

?>