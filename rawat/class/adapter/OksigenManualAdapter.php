<?php
class OksigenManualAdapter extends ArrayAdapter {
	public function adapt($d) {
        if(is_array($d)){
            $d = (object) $d;
        }
		$l = array ();
		$l ['id'] = $d->id;
		$l ['Waktu'] = self::format ( "date d M Y", $d->waktu );
		$l ['Harga (H)'] = self::format ( "money Rp.", $d->harga_liter );
		$l ['Biaya Lain (B)'] = self::format ( "money Rp.", $d->biaya_lain );
		$l ['Liter (L)'] = $d->jumlah_liter;
		$l ['Total (H*L+B)'] = self::format ( "money Rp.", $d->harga );
		$l ['Range'] = $d->range_harga;
		$l ['Harga Per Jam'] = self::format ( "money Rp.", $d->harga_jam );
		$l ['Jumlah Jam'] = $d->jumlah_jam;
		$l ['Pasang'] = self::format ( "date d M Y H:i", $d->waktu_pasang );
		$l ['Lepas'] = self::format ( "date d M Y H:i", $d->waktu_lepas);
		$l ['Total'] = self::format ( "money Rp.", $d->harga );
		return $l;
	}
}
?>