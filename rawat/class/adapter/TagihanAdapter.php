<?php 
/**
 * this class used to adapt the data from database into
 * cashier format so could be easily store to the cashier 
 * database
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @version     : 18.0.9
 * @since       : 16 Mei 2014
 * */

class TagihanAdapter extends ArrayAdapter {	
	protected $adapter_name;
	protected $total_alat_obat;
    protected $polislug;
    protected $db;
	public function __construct($db,$polislug="") {
		global $db;
		parent::__construct ();
		$this->total_alat_obat=0;
        $this->polislug=$polislug;
        $this->db;
        $cur_urjip = getSettings($db,"smis-rs-urjip-".$polislug,"URJ");
        if($cur_urjip!="URJ"){
            $cur_urjip = "URI";
        }
        $this->urjip = $this->polislug=="igd"?"IGD":$cur_urjip;
	}
	
	public function getTotalAlatObat(){
		return $this->total_alat_obat;
	}
    
	public function setAdapterName($name) {
		$this->adapter_name = $name;
	}
    
	public function adapt($d) {
        switch($this->adapter_name){
            case "audiometry"           : return $this->audiometry($d);
            case "bronchoscopy"         : return $this->bronchoscopy($d);
            case "bed"                  : return $this->bed($d);
            case "oksigen_central"      : return $this->oksigen_central($d);
            case "oksigen_manual"       : return $this->oksigen_manual($d);
            case "endoscopy"            : return $this->endoscopy($d);
            case "faal_paru"            : return $this->faal_paru($d);
            case "ekg"                  : return $this->ekg($d);
            case "spirometry"           : return $this->spirometry($d);
            case "konsul_dokter"        : return $this->konsul_dokter($d);
            case "konsultasi_dokter"    : return $this->konsultasi_dokter($d);
            case "visite_dokter"        : return $this->visite_dokter($d);
            case "tindakan_dokter"      : return $this->tindakan_dokter($d);
            case "tindakan_perawat"     : return $this->tindakan_perawat($d);
            case "tindakan_igd"         : return $this->tindakan_perawat_igd($d);
            case "tindakan_asisten"     : return $this->tindakan_asisten($d);
            case "ok"                   : return $this->ok($d);
            case "vk"                   : return $this->vk($d);
            case "alok"                 : return $this->alok($d);
            case "rr"                   : return $this->rr($d);
        }		
		return $d;
	}
    
	public function rr($d) {
		$a ['waktu']        = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']         = "Recovery Room #$d->id";
		$a ['jumlah']       = "1";
		$a ['id']           = $d->id;
		$a ['satuan']       = $d->harga;
		$a ['biaya']        = $d->harga;
		$a ['start']        = $d->waktu;
		$a ['end']          = $d->waktu;
		$a ['keterangan']   = "Petugas : " . $d->nama_petugas . "  Asisten : " . $d->nama_asisten;
        $a ['debet']        = getSettings($this->db,"smis-rs-acc-d-ok-rr-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']       = getSettings($this->db,"smis-rs-acc-k-ok-rr-".$this->polislug."-".$d->carabayar,"");

        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
		return $a;
	}
    
	public function alok($d) {
		$a ['waktu']            = self::format ( "date d-M-Y", $d->tanggal );
		$a ['nama']             = $d->nama;
		$a ['id']               = $d->id;
		$a ['biaya']            = $d->harga * $d->jumlah;
		$a ['jumlah']           = $d->jumlah;
		$a ['satuan']           = $d->harga;	
		$a ['start']            = $d->tanggal;
		$a ['end']              = $d->tanggal;
		$a ['keterangan']       = "Harga Satuan " . self::format ( "only-money Rp.", $d->harga ) . ", Sejumlah  " . $d->jumlah . "";
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-alok-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-alok-".$this->polislug."-".$d->carabayar,"");
        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->tanggal,0,10);
        $this->total_alat_obat += ($d->harga * $d->jumlah);
		return $a;
	}
	public function audiometry($d) {
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['id']               = $d->id;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . "), Perawat (" . $d->nama_perawat . ")";
		$a ['jumlah']           = "1";
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
		$a ['satuan']           = $d->biaya;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-audiometry-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-audiometry-".$this->polislug."-".$d->carabayar,"");
        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        return $a;
	}
	public function bed($d) {		
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "mini-date d M Y H:i", $d->waktu_masuk ) . " - " . self::format ( "mini-date d M Y H:i", $d->waktu_keluar );
		$a ['nama']             = "Sewa Kamar ".$d->nama_bed." Selama ".$d->hari." Hari ";
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Durasi " . $d->hari . " Hari ";
		$a ['jumlah']           = $d->hari;
		$a ['start']            = $d->waktu_masuk;
		$a ['end']              = $d->waktu_keluar;
		$a ['satuan']           = $d->harga;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-bed-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-bed-".$this->polislug."-".$d->carabayar,"");
        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu_keluar,0,10);
        return $a;
	}
	public function oksigen_central($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d M Y H:i", $d->mulai ) . " - " . self::format ( "date d M Y H:i", $d->selesai );
		$a ['nama']             = "Oksigen Central";
		$a ['biaya']            = $d->harga;
		$a ['keterangan']       = "Durasi ( " . $d->jam . " Jam " . $d->menit . " Menit ), Skala " . $d->skala . ", Tarif " . self::format ( "only-money Rp.", $d->harga_jam ) . " / Jam ,  " . self::format ( "only-money Rp.", $d->harga_menit ) . " / Menit";
		$a ['jumlah']           = 1;
		$a ['start']            = $d->mulai;
		$a ['end']              = $d->selesai;
		$a ['satuan']           = $d->harga;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-oksigen-central-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-oksigen-central-".$this->polislug."-".$d->carabayar,"");
        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->selesai,0,10);
        return $a;
	}
	public function oksigen_manual($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y H:i", $d->waktu_pasang );
		$a ['nama']             = "Oksigen Manual";
		$a ['biaya']            = $d->harga;
		$a ['keterangan']       = $d->jumlah_liter . " Liter, Tarif " . self::format ( "only-money Rp.", $d->harga_liter ) . " / Liter , Biaya Lain : " . self::format ( "only-money Rp.", $d->biaya_lain ) . "";
		$a ['jumlah']           = $d->jumlah_liter;
		$a ['satuan']           = $d->harga_liter;
		$a ['start']            = $d->waktu_pasang;
		$a ['end']              = $d->waktu_lepas;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-oksigen-manual-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-oksigen-manual-".$this->polislug."-".$d->carabayar,"");
        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu_lepas,0,10);
        return $a;
	}
	public function ok($d) {
        require_once "rawat/function/summary_biaya_ok.php";
		$a ['id']     = $d->id;
		$a ['waktu']  = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']   = "Operasi #$d->id";
		$a ['biaya']  = summary_biaya_ok($d);
		
		$a ['keterangan']    = "Operator 1 : " . $d->nama_operator_satu . " (" . self::format ( "only-money Rp.", $d->harga_operator_satu ) . "), ";
		$a ['keterangan']   .= "Operator 2 : " . $d->nama_operator_dua . " (" . self::format ( "only-money Rp.", $d->harga_operator_dua ) . "), ";
		$a ['keterangan']   .= "Asisten Operator 1 : " . $d->nama_asisten_operator_satu . " (" . self::format ( "only-money Rp.", $d->harga_asisten_operator_dua ) . "), ";
		$a ['keterangan']   .= "Asisten Operator 1 : " . $d->nama_asisten_operator_dua . " (" . self::format ( "only-money Rp.", $d->harga_asisten_operator_dua ) . "), ";
		$a ['keterangan']   .= "Anastesi : " . $d->nama_anastesi . " (" . self::format ( "only-money Rp.", $d->harga_anastesi ) . "), ";
		$a ['keterangan']   .= "Asisten Anastesi : " . $d->nama_asisten_anastesi . " (" . self::format ( "only-money Rp.", $d->harga_asisten_anastesi ) . "), ";
		$a ['keterangan']   .= "Asisten Anastesi 2 : " . $d->nama_asisten_anastesi_dua . " (" . self::format ( "only-money Rp.", $d->harga_asisten_anastesi_dua ) . "), ";
		$a ['keterangan']   .= "Instrument I: " . $d->nama_instrument . " (" . self::format ( "only-money Rp.", $d->harga_instrument ) . "), ";
		$a ['keterangan']   .= "Instrument II: " . $d->nama_instrument_dua . " (" . self::format ( "only-money Rp.", $d->harga_instrument_dua ) . "), ";
		$a ['keterangan']   .= "Team OK : " . $d->nama_team_ok . " (" . self::format ( "only-money Rp.", $d->harga_team_ok ) . "), ";
		$a ['keterangan']   .= "Bidan I : " . $d->nama_bidan . " (" . self::format ( "only-money Rp.", $d->harga_bidan ) . "), ";
		$a ['keterangan']   .= "Bidan II : " . $d->nama_bidan_dua . " (" . self::format ( "only-money Rp.", $d->harga_bidan_dua ) . "), ";
		$a ['keterangan']   .= "Perawat : " . $d->nama_perawat . " (" . self::format ( "only-money Rp.", $d->harga_perawat ) . "), ";
		$a ['keterangan']   .= "Sewa Kamar " . self::format ( "unslug", $d->kelas ) . " : " . $d->nama_sewa_kamar . " (" . self::format ( "only-money Rp.", $d->harga_sewa_kamar ) . ")";
		$a ['keterangan']   .= "Sewa Alat " . self::format ( "unslug", $d->kelas ) . " : (" . self::format ( "only-money Rp.", $d->harga_sewa_alat ) . ")";
		$a ['keterangan']   .= "Asisten Omloop 1 : " . $d->nama_oomloop_satu . " (" . self::format ( "only-money Rp.", $d->harga_oomloop_satu ) . "), ";
		$a ['keterangan']   .= "Asisten Omloop 2 : " . $d->nama_oomloop_dua . " (" . self::format ( "only-money Rp.", $d->harga_oomloop_dua ) . "), ";
		$a ['start']         = $d->waktu;
		$a ['end']           = $d->waktu;
        $a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-op1-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-op1-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
		return $a;
	}
	public function vk($d) {
		$a ['id']            = $d->id;
		$a ['waktu']         = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']          = "Bersalin #$d->id";
		$a ['biaya']         = $d->harga_operator_satu + $d->harga_operator_dua + $d->harga_anastesi + $d->harga_asisten_anastesi + $d->harga_team_ok + $d->harga_bidan + $d->harga_perawat + $d->harga_sewa_kamar+$d->harga_sewa_alat;
		$a ['keterangan']    = "Operator 1 : " . $d->nama_operator_satu . " (" . self::format ( "only-money Rp.", $d->harga_operator_satu ) . "), ";
		$a ['keterangan']   .= "Operator 2 : " . $d->nama_operator_dua . " (" . self::format ( "only-money Rp.", $d->harga_operator_dua ) . "), ";
		$a ['keterangan']   .= "Anastesi : " . $d->nama_anastesi . " (" . self::format ( "only-money Rp.", $d->harga_anastesi ) . "), ";
		$a ['keterangan']   .= "Asisten Anastesi : " . $d->nama_asisten_anastesi . " (" . self::format ( "only-money Rp.", $d->harga_asisten_anastesi ) . "), ";
		$a ['keterangan']   .= "Team VK : " . $d->nama_team_vk . " (" . self::format ( "only-money Rp.", $d->harga_team_vk ) . "), ";
		$a ['keterangan']   .= "Bidan : " . $d->nama_bidan . " (" . self::format ( "only-money Rp.", $d->harga_bidan ) . "), ";
		$a ['keterangan']   .= "Perawat : " . $d->nama_perawat . " (" . self::format ( "only-money Rp.", $d->harga_perawat ) . "), ";
		$a ['keterangan']   .= "Sewa Kamar " . self::format ( "unslug", $d->kelas ) . " : " . $d->nama_sewa_kamar . " (" . self::format ( "only-money Rp.", $d->harga_sewa_kamar ) . ")";
		$a ['keterangan']   .= "Sewa Alat " . self::format ( "unslug", $d->kelas ) . " : (" . self::format ( "only-money Rp.", $d->harga_sewa_alat ) . ")";
		$a ['debet']         = getSettings($this->db,"smis-rs-acc-d-ok-op1-".$this->polislug."-".$d->carabayar,"");
		$a ['kredit']        = getSettings($this->db,"smis-rs-acc-k-ok-op1-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']  = "";
        $a ['id_dokter']    = "";
        $a ['jaspel_dokter']= "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        return $a;
	}
	public function bronchoscopy($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . "), Perawat (" . $d->nama_perawat . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-bronchoscopy-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-bronchoscopy-".$this->polislug."-".$d->carabayar,"");
		return $a;
	}
	public function ekg($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = "EKG";
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Pengirim (" . $d->nama_dokter_pengirim . ") , Pembaca (" . $d->nama_dokter_pembaca . "), Petugas (" . $d->nama_petugas . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-ekg-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-ekg-".$this->polislug."-".$d->carabayar,"");
		return $a;
	}
	public function endoscopy($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ") , Asisten (" . $d->nama_asisten . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;		
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-endoscopy-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-endoscopy-".$this->polislug."-".$d->carabayar,"");
		return $a;
	}
	public function faal_paru($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ") , Asisten (" . $d->nama_asisten . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;	
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-faal-paru-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-faal-paru-".$this->polislug."-".$d->carabayar,"");
		return $a;
	}
	public function fisiotherapy($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ") , Petugas (" . $d->nama_petugas . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;		
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"".$this->polislug."-".$d->carabayar,"");
		return $a;
	}
	public function spirometry($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = "Spirometry";
		$a ['biaya']            = $d->biaya;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ") , Perawat (" . $d->nama_perawat . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->biaya;		
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-spirometry-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-spirometry-".$this->polislug."-".$d->carabayar,"");
		return $a;
	}
	public function konsul_dokter($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = "Konsul " . $d->nama_dokter;
		$a ['biaya']            = $d->harga;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ") , Kelas : " . ArrayAdapter::format ( "unslug", $d->kelas );
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga;
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-konsul-dokter-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-konsul-dokter-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']      = $d->nama_dokter;
        $a ['id_dokter']        = $d->id_dokter;
		
		$a ['jaspel_persen']    = $d->jaspel_persen;
		$a ['jaspel_dokter']    = $d->jaspel_nilai;
		

        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        
        return $a;
	}
	public function konsultasi_dokter($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = "Periksa " . $d->nama_dokter;
		$a ['biaya']            = $d->harga+$d->harga_perawat;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ") ";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga+$d->harga_perawat;		
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-periksa-dokter-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-periksa-dokter-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']      = $d->nama_dokter;
        $a ['id_dokter']        = $d->id_dokter;
        $a ['jaspel_persen']    = $d->jaspel_persen;
		$a ['jaspel_dokter']    = $d->jaspel_nilai;
		$a ['jaspel_persen_perawat']    = $d->jaspel_persen_perawat;
        $a ['jaspel_perawat']    		= $d->jaspel_nilai_perawat;    
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        return $a;
	}
	public function visite_dokter($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = "Visite " . $d->nama_dokter;
		$a ['biaya']            = $d->harga;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga;		
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-visite-dokter-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-visite-dokter-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']      = $d->nama_dokter;
        $a ['id_dokter']        = $d->id_dokter;
        $a ['jaspel_persen']    = $d->jaspel_persen;
        $a ['jaspel_dokter']    = $d->jaspel_nilai;
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        return $a;
	}
	public function tindakan_dokter($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan." Oleh (" . $d->nama_dokter . ") ";
		$a ['biaya']            = $d->harga+$d->harga_perawat;
		$a ['keterangan']       = "Dokter (" . $d->nama_dokter . ") : ".ArrayAdapter::format("only-money Rp.", $d->harga).", Assisten (".$d->nama_asisten." ) : ".ArrayAdapter::format("only-money Rp.", $d->harga_perawat);
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga+$d->harga_perawat;
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-tindakan-dokter-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-tindakan-dokter-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']      = $d->nama_dokter;
        $a ['id_dokter']        = $d->id_dokter;
        $a ['jaspel_persen']    = $d->jaspel_persen;
		$a ['jaspel_dokter']    = $d->jaspel_nilai;     
		
		$a ['jaspel_persen_perawat']    = $d->jaspel_persen_perawat;
        $a ['jaspel_perawat']    		= $d->jaspel_nilai_perawat;     
		
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        
        return $a;
	}
	public function tindakan_perawat($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->harga_tindakan;
		$a ['keterangan']       = "Perawat (" . $d->nama_perawat . " ) @ ".ArrayAdapter::format("only-money", $d->satuan)." x ".$d->jumlah;
		$a ['jumlah']           = $d->jumlah;
		$a ['satuan']           = $d->satuan;
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-tindakan-perawat-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-tindakan-perawat-".$this->polislug."-".$d->carabayar,"");
		
		$a ['jaspel_persen_perawat']    = $d->jaspel_persen;
        $a ['jaspel_perawat']    		= $d->jaspel;     

        $a ['nama_dokter']      = "";
        $a ['id_dokter']        = "";
        $a ['jaspel_dokter']    = "";
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        
        return $a;
	}
	public function tindakan_perawat_igd($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->harga_tindakan;
		$a ['keterangan']       = "Perawat (" . $d->nama_perawat . ") , Dokter Jaga (".$d->nama_dokter.")";
		$a ['jumlah']           = $d->jumlah;
		$a ['satuan']           = $d->satuan;
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-tindakan-igd-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-tindakan-igd-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']      = $d->nama_dokter;
        $a ['id_dokter']        = $d->id_dokter;
        $a ['jaspel_dokter']    = 0;
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        return $a;
	}
	public function tindakan_asisten($d) {
		$a ['id']               = $d->id;
		$a ['waktu']            = self::format ( "date d-M-Y", $d->waktu );
		$a ['nama']             = $d->nama_tindakan;
		$a ['biaya']            = $d->harga_tindakan;
		$a ['keterangan']       = "Asisten (" . $d->id_asisten . ")";
		$a ['jumlah']           = 1;
		$a ['satuan']           = $d->harga_tindakan;
		$a ['start']            = $d->waktu;
		$a ['end']              = $d->waktu;
        $a ['debet']            = getSettings($this->db,"smis-rs-acc-d-tindakan-perawat-".$this->polislug."-".$d->carabayar,"");
        $a ['kredit']           = getSettings($this->db,"smis-rs-acc-k-tindakan-perawat-".$this->polislug."-".$d->carabayar,"");
        
        $a ['nama_dokter']      = "";
        $a ['id_dokter']        = "";
        $a ['jaspel_dokter']    = 0;
        $a ['urjigd']          = $this->urjip;
        $a ['tanggal_tagihan'] = substr($d->waktu,0,10);
        
        return $a;
	}
	
}


?>