<?php
abstract class SalaryAdapter extends ArrayAdapter {
	private $content;
	protected $id_karyawan;
	protected $ruangan;
	protected $tipe;
	public static $TYPE_INDIVIDU="individual";
	public static $TYPE_COMMUNAL="communal";
    public static $TYPE_BOTH="both";
	
	
	public function __construct($id_karyawan, $ruangan, $tipe="individual") {
		parent::__construct ();
		$this->content = array ();
		$this->ruangan = $ruangan;
		$this->id_karyawan = $id_karyawan;
		$this->tipe=$tipe;
	}
	public function is($id_karyawan,$pembagian){    
        if($this->tipe==self::$TYPE_BOTH && ($this->id_karyawan==$id_karyawan || $this->id_karyawan =="-1")  ){
            return true;
        }else if($this->tipe==self::$TYPE_INDIVIDU && $pembagian==self::$TYPE_INDIVIDU  && ($this->id_karyawan==$id_karyawan || $this->id_karyawan =="-1") )
			return true;
		else if($this->tipe==self::$TYPE_COMMUNAL && $pembagian==self::$TYPE_COMMUNAL && ($this->id_karyawan==$id_karyawan || $this->id_karyawan =="-1") )
			return true;
		return false;
	}
	

	protected function pushContent($waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $nama_karyawan="", $ket = "") {
		$array = array ();
		$array ['waktu'] = self::format ( "date d M Y", $waktu );
		$array ['ruangan'] = $ruangan;
		$array ['sebagai'] = $honor_sebagai;
		$array ['percentage'] = $honor_persen . "%";
		$array ['nilai'] = $nilai;
		$array ['asli'] = $nilai_asli;
		$array ['pasien'] = self::format ( "only-digit6", $nrm ) . " : " . $nama;
		$array ['keterangan'] = $ket;
		$array ['karyawan'] = $nama_karyawan;		
		$this->content [] = $array;
	}
	public function getContent($data) {
		foreach ( $data as $d ) {
			$this->adapt ( $d );
		}
		return $this->content;
	}
}

?>