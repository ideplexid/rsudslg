<?php
class VisiteDokterAdapter extends SalaryAdapter {
	public function adapt($d) {
		$pbg = json_decode ( $d ['pembagian'], true );
		if ($this->is($d ['id_dokter'],$pbg['s-dokter'])) {
			$nama = $d ['nama_pasien'];
			$nrm = $d ['nrm_pasien'];
			$nilai_asli = $d ['harga'];
			$pembagian = json_decode ( $d ['pembagian'], true );
			$jenis = $d ['jenis_dokter'];
			$waktu = $d ['waktu'];
			$ruangan = $this->ruangan;
			$honor_persen = $pembagian ['dokter'];
			$karyawan=$d['nama_dokter'];
			$honor_sebagai = "Visite Dokter ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$ket = self::format ( "unslug", $d ['kelas'] );
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama,$karyawan, $ket );
		}
	}
}

?>