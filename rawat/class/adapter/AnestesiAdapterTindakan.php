<?php 
class AnestesiAdapter extends ArrayAdapter {
	private $complete;
	public function __construct($complete){
		parent::__construct();
		$this->complete=$complete;
	}
	
	public function adaptSimple($d){
		$a = array ();
		$a['id'] = $d->id;
		$team = array (
			"Anestesi" 			=> $d->nama_anastesi,
			"Penata Anestesi" 	=> $d->nama_asisten_anastesi,
			"Perawat RR I" 		=> $d->nama_oomloop_satu,
			"Perawat RR II" 	=> $d->nama_instrument
		);
		
		$biaya = array (
			$d->nama_tindakan	=> $d->harga_anastesi
		);
		$a ['Tanggal'] = self::format ( "date d-M-Y", $d->waktu );
		$a ['Team'] = self::format ( "array", $team );
		$a ['Biaya'] = self::format ( "array-money Rp.", $biaya );
		$sum = 0;
		foreach ( $biaya as $b => $v ) {
			$sum += ($v * 1);
		}
		$a ['Total'] = self::format ( "money Rp.", $sum );
		
		return $a;
	}
	
	public function adapt($d){
        if(is_array($d)){
            $d=(object) $d;
        }
        if($this->complete)
			return $this->adaptComplete($d);
		return $this->adaptSimple($d);
	}
	
	public function adaptComplete($d) {
		$a = array ();
		$a ['id'] = $d->id;

		$team = array (
			"Anestesi" 			=> $d->nama_anastesi,
			"Penata Anestesi" 	=> $d->nama_asisten_anastesi,
			"Perawat RR I" 		=> $d->nama_oomloop_satu,
			"Perawat RR II" 	=> $d->nama_instrument
		);
		
		
		$biaya = array (
			$d->nama_tindakan	=> $d->harga_anastesi
		);

		$a ['Tanggal'] = self::format ( "date d-M-Y", $d->waktu );
		$a ['Team'] = self::format ( "array", $team );
		$a ['Biaya'] = self::format ( "array-money Rp.", $biaya );
		$sum = 0;
		foreach ( $biaya as $b => $v ) {
			$sum += ($v * 1);
		}
		$a ['Total'] = self::format ( "money Rp.", $sum );

		return $a;
	}
}
?>