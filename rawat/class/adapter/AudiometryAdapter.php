<?php
class AudiometryAdapter extends SalaryAdapter {
	public function adapt($d) {
		$ruangan = $this->ruangan;
		$waktu = $d ['waktu'];
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$nilai_asli = $d ['biaya'];
		$pembagian = json_decode ( $d ['pembagian'], true );
			
		if ($this->is($d ['id_dokter'],$pembagian['s-dokter']) ) {
			$honor_persen = $pembagian ['dokter'];
			$honor_sebagai = "Dokter Audiometry ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$ket = self::format ( "unslug", $d ['kelas'] );
			$karyawan=$d ['nama_dokter'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
		
		if ($this->is($d ['id_perawat'],$pembagian['s-perawat'])) {
			$honor_persen = $pembagian ['perawat'];
			$honor_sebagai = "Perawat Audiometry ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$ket = self::format ( "unslug", $d ['kelas'] );
			$karyawan=$d ['nama_perawat'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan,$ket );
		}
	}
}

?>