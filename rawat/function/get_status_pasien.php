<?php 
function get_status_pasien(){
    $option = new OptionBuilder();
    $option ->addSingle("Sudah Ditindak Lanjuti")
            ->addSingle("Belum Ditindak Lanjuti")
            ->addSingle("");
    return $option->getContent();
}