<?php 
    function menu_responder(Database $db, $scommand){
        $hket    = array ("Nama","Keterangan","Waktu");		
        $adapter = new SimpleAdapter ();
        $adapter ->add("Nama", "nama")
                 ->add("Penyakit", "penyakit")
                 ->add("Waktu", "waktu");
        $dktable = new Table($hket, "", NULL, true);
        $dktable ->setName($scommand)
                 ->setModel(Table::$SELECT);
        $respond = new ServiceResponder($db, $dktable, $adapter, "get_menu");
        return $respond;		
    }
?>