<?php 
function tutup_tagihan($noreg,$polislug){
    global $db;
    if(getSettings($db,"smis-rs-tutup-tagihan-".$polislug,"0")=="0"){
        return 0;
    }
    require_once "smis-base/smis-include-service-consumer.php";
    $serv = new ServiceConsumer($db,"cek_tutup_tagihan",NULL,"registration");
    $serv ->addData("noreg_pasien",$noreg);
    $serv ->execute();
    $result = $serv ->getContent();
    return $result;
}