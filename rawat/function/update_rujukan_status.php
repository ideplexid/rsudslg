<?php 

function update_rujukan_status(DBTable $dbtable,$polislug,$id,$noreg_pasien){
/**melakukan update data rujukan pasien ketika pasien tersebut dikeluarkan dari ruangan */
require_once "smis-base/smis-include-service-consumer.php";
    $dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
    $db  = $dbtable->get_db();
    $row = null;
    if($id!=""){
        $row = $dbtable->select(array("id",$id));
    }else{
        $row = $dbtable->select(array("no_register",$noreg_pasien));
    }
    if(strpos($row->cara_keluar,"Rujuk")!==false){
        $save['tanggal']        				= $row->waktu_keluar;
        $save['nama_pasien']    				= $row->nama_pasien;
        $save['nrm_pasien']     				= $row->nrm_pasien;
        $save['noreg_pasien']   				= $row->no_register;
        $save['ruangan']        				= $polislug;
        $save['dirujuk']       					= $row->cara_keluar;
        $save['nama_rujukan']   				= $row->nama_rs;
        $save['poli_rujukan']   				= $row->nama_unit;
        $save['is_spesialis']   				= $row->is_spesialistik;
        $save['alasan_rujukan_nonspesialistik'] = $row->keterangan_keluar;
        
        $serv = new ServiceConsumer($db,"push_rujukan",$save,"medical_record");
        $serv ->execute();
    }
}

?>