<?php 
function in_log($db,$polislug,$nama,$noreg,$nrm,$petugas,$asal,$keterangan){
    $data['nama_pasien']    = $nama;
    $data['noreg_pasien']   = $noreg;
    $data['nrm_pasien']     = $nrm;
    $data['petugas']        = $petugas;
    $data['asal']           = $asal;
    $data['keterangan']     = $keterangan;
    $data['tanggal']        = date("Y-m-d H:i:s");
    $dbtable = new DBTable($db,"smis_rwt_in_log_".$polislug);
    $dbtable ->insert($data);
}