<?php 

function diet_responder(Database $db, $scommand){
    $hmenu   = array ("Nama","Penyakit","Keterangan");
    $adapter = new SimpleAdapter ();
    $adapter ->add("Nama", "nama")
             ->add("Penyakit", "penyakit")
             ->add("Keterangan", "keterangan");
    $dktable = new Table($hmenu, "", NULL, true);
    $dktable ->setName($scommand)
             ->setModel(Table::$SELECT);
    $respon  = new ServiceResponder($db, $dktable, $adapter, "get_diet");
    return $respon;
}

?>