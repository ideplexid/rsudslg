<?php

function notif_pasien_out(Database $db, $polislug, $poliname, $id,$nama,$cara_keluar){
    global $notification;
    $key2        = md5($poliname."-gizi ".$id." - keluar");
    $msg         = "Pasien <strong>".$nama."</strong> Keluar dari <strong>".$poliname."</strong> ( ".$cara_keluar." )";
    $push_gizi   = getSettings($db, "smis-rs-use-gizi-".$polislug, "0")=="1";
    $push_keluar = !($cara_keluar=="Tidak Datang" || $cara_keluar=="Pindah Kamar" );
    if($push_gizi && $push_keluar){
        $notification->addNotification("Pasien Keluar", $key2, $msg,"gizi","pesanan");
    }
}

?>