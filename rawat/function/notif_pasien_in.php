<?php 

function notif_pasien_in(Database $db,$id,$poliname,$polislug,$nama,$asal=null){
    global $notification;
    $key    = md5($poliname." ".$id);
    $key2   = md5($poliname."-gizi ".$id." - masuk");
    $msg="Pasien <strong>".$nama."</strong> Masuk di <strong>".$poliname."</strong> ";
    if(isset($asal)){
        $msg.="dari <strong>".$asal."</strong>";
    }
    $msg_gizi = $msg;
    if(isset($asal) && $asal!="" && strcmp(strtolower($asal), "pendaftaran")){
        $msg_gizi = "Pasien <strong>".$nama."</strong> Masuk di <strong>".$poliname."</strong> Pindahan dari <strong>".$asal."</strong>";
    }
    $notification->addNotification("Pasien Masuk", $key, $msg,$polislug,"antrian");
    if(getSettings($db, "smis-rs-use-gizi-".$polislug, "0")=="1"){
        $notification->addNotification("Pasien Masuk", $key2, $msg_gizi,"gizi","pesanan");
    }
}

?>