<?php 

function get_bed_pasien($db,$polislug,$noreg){
    $dbtable = new DBTable($db,"smis_rwt_bed_kamar_".$polislug);
    $kamar   = $dbtable ->select(array("noreg_pasien"=>$noreg));
    if($kamar!=null){
        return $kamar->nama;
    }
    return "";
}

?>