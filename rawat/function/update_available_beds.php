<?php 

function update_available_beds($slug){
    global $db;

    //kode aplicare

    $palicare["NN"]  =  "-";
    $palicare["VVP"] = "VVIP";
    $palicare["VIP"] = "VIP";
    $palicare["UTM"] = "UTAMA";
    $palicare["KL1"] = "KELAS I";
    $palicare["KL2"] = "KELAS II";
    $palicare["KL3"] = "KELAS III";
    $palicare["ICU"] = "ICU";
    $palicare["ICC"] = "ICCU";
    $palicare["NIC"] = "NICU";
    $palicare["PIC"] = "PICU";
    $palicare["IGD"] = "IGD";
    $palicare["UGD"] = "UGD";
    $palicare["SAL"] = "RUANG BERSALN";
    $palicare["HCU"] = "HCU";
    $palicare["ISO"] = "RUANG ISOLASI";

    $dbtable = new DBTable($db,"smis_adm_prototype");
    $proto = $dbtable ->select(array("slug"=>$slug));
    $koderuang = $proto->id;

    /*mengosongi yang lama */
    $update = "UPDATE available_beds SET kapasitas = 0, tersedia = 0 WHERE koderuang = '".$koderuang ."' ;";
    $db->query($update);

    $query = "SELECT kelas_aplicare, count(*) as total, SUM(terpakai) as terpakai FROM smis_rwt_bed_kamar_".$slug." WHERE prop!='del' GROUP BY kelas_aplicare ";
    $result = $db ->get_result($query);

    $total_kapasitas = 0;
    $total_terpakai = 0;
    $total_tersedia = 0;
    
    
    foreach($result as $row){
        $kapasitas = $row->total;
        $terpakai = $row->terpakai;
        $tersedia = $kapasitas-$terpakai;
        $kodekelas = $row->kelas_aplicare;
        $namakelas = $palicare[$kodekelas];
        
        $total_kapasitas += $kapasitas;
        $total_terpakai += $terpakai;
        $total_tersedia += $tersedia;

        $query = "SELECT count(*) as total FROM available_beds WHERE koderuang='".$koderuang ."' AND  kodekelas = '".$kodekelas."' ";
        $ada_ruang = $db->get_var($query);
        if($ada_ruang*1==0){
            $query = "INSERT INTO available_beds (`kodekelas`,`namakelas`,`koderuang`,`namaruang`,`kapasitas`,`tersedia`,`tersediapria`,`tersediawanita`,`tersediapriawanita`) VALUES 
            ('".$kodekelas."','".$namakelas."','".$koderuang ."','".$proto->nama."','".$kapasitas."','".$tersedia ."','0','0','0') ";
            $db->query($query);
        }else{
            $query = "UPDATE available_beds set `kodekelas`='".$kodekelas."',
                                                `namakelas` = '".$namakelas."',
                                                `namaruang` = '".$proto->nama."',                                                
                                                `kapasitas` = '".$kapasitas."',
                                                `tersedia` = '".$tersedia."',
                                                `tersediapria` = '0',
                                                `tersediawanita` = '0',
                                                `tersediapriawanita` = '0'
                        WHERE  `koderuang` = '".$koderuang."' AND  kodekelas = '".$kodekelas."' ";
            $db->query($query);
        }

        //kode siranap
        $query = "SELECT id FROM smis_adm_prototype WHERE slug = '$slug' ; ";
        $kode_ruangan = $db->get_var($query);
        $tipe_pasien = getSettings($db,"smis-rs-rl52-default-".$slug,"");
        $total_TT = $total_kapasitas;

        $query  = "SELECT count(*) as total from smis_rwt_bed_kamar_".$slug." a LEFT JOIN smis_rwt_antrian_".$slug." b ON a.noreg_pasien = b.no_register 
                    WHERE b.no_register IS NOT NULL AND b.jk=0 ";
        $terpakai_male = $db->get_var($query);

        $query  = "SELECT count(*) as total from smis_rwt_bed_kamar_".$slug." a LEFT JOIN smis_rwt_antrian_".$slug." b ON a.noreg_pasien = b.no_register 
                    WHERE b.no_register IS NOT NULL AND b.jk=1 ";
        $terpakai_female = $db->get_var($query);

        $query = "SELECT count(*) as total FROM siranap_available_beds WHERE kode_ruang='$kode_ruangan'  ";
        $ada_ruang = $db->get_var($query);
        if($ada_ruang*1==0){
            $query = "INSERT INTO siranap_available_beds 
                    (`kode_ruang`,`tipe_pasien`,`total_TT`,`terpakai_male`,`terpakai_female`,`kosong_male`,`kosong_female`,`waiting`) 
                        VALUES 
                    ('".$kode_ruangan."','".$tipe_pasien."','".$total_TT ."','".$terpakai_male."','".$terpakai_female."','0','0','0') ";
            $db->query($query);
        }else{
            $query = "UPDATE siranap_available_beds set 
                                    `tipe_pasien` = '".$tipe_pasien."',
                                    `total_TT` = '".$total_TT."',                                                
                                    `terpakai_male` = '".$terpakai_male."',
                                    `terpakai_female` = '".$terpakai_female."',
                                    `kosong_male` = '0',
                                    `kosong_female` = '0',
                                    `waiting` = '0'
                        WHERE  `kode_ruang` = '".$kode_ruangan."' ";
            $db->query($query);
        }

    }
    
}