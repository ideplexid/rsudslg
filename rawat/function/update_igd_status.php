<?php
 /* jika ruangan adalah IGD update tanggal selesai */
function update_igd_status($db,$polislug,$id, $noreg_pasien,$carakeluar,$waktu){
    if(getSettings($db,"smis-rs-laporan-igd-" . $polislug,"0")=="1"){
        if($noreg_pasien==""){
            $dbtable        = new DBTable($db,"smis_rwt_antrian_".$polislug);
            $row            = $dbtable->select($id);
            $noreg_pasien   = $row->no_register;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($db,"update_igd_pulang",NULL,"medical_record");
        $serv ->addData("ruangan",$polislug)
              ->addData("noreg_pasien",$noreg_pasien)
              ->addData("cara_keluar",$carakeluar)
              ->addData("waktu_keluar",$waktu)
              ->execute();
    }
}
?>