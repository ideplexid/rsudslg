<?php 

function summary_biaya_ok($d){
    if(is_array($d)){
        $d = (object) $d;
    }
    $total  = 0;
    $total += $d->harga_dokter_pendamping;
    $total += $d->harga_operator_satu;
    $total += $d->harga_operator_dua;
    $total += $d->harga_asisten_operator_satu;
    $total += $d->harga_asisten_operator_dua;
    $total += $d->harga_anastesi;
    $total += $d->harga_asisten_anastesi;
    $total += $d->harga_asisten_anastesi_dua;
    $total += $d->harga_instrument;
    $total += $d->harga_instrument_dua;
    $total += $d->harga_team_ok;
    $total += $d->harga_bidan;
    $total += $d->harga_bidan_dua;
    $total += $d->harga_perawat;
    $total += $d->harga_sewa_kamar;
    $total += $d->harga_sewa_alat;
    $total += $d->harga_oomloop_satu;
    $total += $d->harga_oomloop_dua;
    $total += $d->recovery_room;
    return $total;
}

?>