<?php 

function get_data_pasien($db,$polislug,$noreg){
    $dbtable = new DBTable($db,"smis_rwt_antrian_".$polislug);
    $dbtable ->setFetchMethode(DBTable::$ARRAY_FETCH);
    $pasien  = $dbtable ->select(array("no_register"=>$noreg));
    if($pasien!=null){
        return $pasien;
    }
    return null;
}

?>