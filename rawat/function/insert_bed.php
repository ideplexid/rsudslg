<?php
function insert_bed($polislug, $nrm_pasien,$nama_pasien,$noreg_pasien, $id_bed,$nama_bed,$waktu,$carabayar){
    global $db;
    
    $bed_kamar          = new DBTable($db,"smis_rwt_bed_kamar_".$polislug);
    $bed                = new DBTable($db,"smis_rwt_bed_".$polislug);
    
    /** cek apakah bednya yang dipilih sudah dipakai oleh orang lain */
    $bedpakai['id']         = $id_bed;
    $bedpakai['terpakai']   = "0";
    if(!$bed_kamar->is_exist($bedpakai)){
        return;
    }

    /** cek apakah ada bed yang masih ditempati oleh pasien ini */
    $exist['noreg_pasien'] = $noreg_pasien;
    if($bed_kamar->is_exist($exist)){
        return;
    }

    /** cek apakah masih ada tempat tidur yang ditempati pasien ini */    
    $exist['waktu_keluar'] = "0000-00-00 00:00:00";
    if($bed->is_exist($exist)){
        return;
    }

    /**lolos semua pengecekan mengisi penggunaan bed*/
    $bed_kamar_update['terpakai']       = "1";
    $bed_kamar_update['nama_pasien']    = $nama_pasien;
    $bed_kamar_update['nrm_pasien']     = $nrm_pasien;
    $bed_kamar_update['noreg_pasien']   = $noreg_pasien;
    $bed_kamar->update($bed_kamar_update,$bedpakai);

    /**mengambil harga default dari bed */
    require_once "smis-base/smis-include-service-consumer.php";
    $bed_harga_service  = new ServiceConsumer ($db,"get_bed",NULL,"manajemen");
    $bed_harga_service  ->addData("ruangan",$polislug);
    $bed_harga_service  ->execute ();
    $harga              = $bed_harga_service->getContent ();

    /**mengisi biaya dan pewaktu dari bed */
    $bed_insert['nama_pasien']  = $nama_pasien;
    $bed_insert['noreg_pasien'] = $noreg_pasien;
    $bed_insert['nrm_pasien']   = $nrm_pasien;
    $bed_insert['id_bed']       = $id_bed; 
    $bed_insert['nama_bed']     = $nama_bed; 
    $bed_insert['waktu_masuk']  = $waktu; 
    $bed_insert['harga']        = $harga;
    $bed_insert['carabayar']    = $carabayar;
    $bed->insert($bed_insert);
}
?>