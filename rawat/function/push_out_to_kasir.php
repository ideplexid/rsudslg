<?php
function push_out_to_kasir($db,$polislug,$id, $noreg_pasien,$carakeluar,$waktu){
    global $user;
    /**excluded because it's not out from medical treatment */
    $exclude = array("Rawat Inap","Masuk IGD","Pindah Kamar","Dirujuk Ke Poli Lain");
    if(in_array($carakeluar,$exclude)){
        return;
    }
    if(getSettings($db,"smis-rs-exit-mode-" . $polislug,"0")=="1"){
        if($noreg_pasien==""){
            $dbtable        = new DBTable($db,"smis_rwt_antrian_".$polislug);
            $row            = $dbtable->select($id);
            $noreg_pasien   = $row->no_register;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $service = new ServiceConsumer ( $db, "set_unregistered", NULL, "registration" );
        $service ->addData("total_tagihan",0)
                 ->addData("asuransi",0)
                 ->addData("bank",0)
                 ->addData("cash",0)
                 ->addData("diskon",0)
                 ->addData("noreg_pasien",$noreg_pasien)
                 ->addData('id',$noreg_pasien)
                 ->addData('carapulang',$carakeluar)
                 ->addData('waktu_pulang',$waktu)
                 ->addData('gratis',0)
                 ->addData('selesai',1)
                 ->addData('tanggal_pulang',$waktu)
                 ->addData('username',$user->getUsername())
                 ->execute();
    }
}
?>