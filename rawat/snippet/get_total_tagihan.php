<?php 
require_once "smis-base/smis-include-service-consumer.php";
require_once "smis-libs-bpjs/TotalTagihanServiceConsumer.php";
global $db;
$serv                   = new TotalTagihanServiceConsumer($db, $_POST['noreg_pasien']);
$serv->execute();
$hasil                  = $serv->getContent();
$data['total_tagihan']  = $hasil;

$pack  = new ResponsePackage();
$pack  ->setStatus(ResponsePackage::$STATUS_OK);
$pack  ->setContent($data);
echo json_encode($pack->getPackage());

?>