<?php
global $user;
global $db;
require_once "smis-base/smis-include-service-consumer.php";

$serv = new ServiceConsumer($db,"request_plafon",NULL,"bpjs");
$serv ->addData("noreg_pasien",$_POST["noreg_pasien"]);
$serv ->addData("nama_pasien",$_POST["nama_pasien"]);
$serv ->addData("nrm_pasien",$_POST["nrm_pasien"]);
$serv ->addData("ruangan",$_POST["ruangan"]);
$serv ->addData("total_tagihan",$_POST["total_tagihan"]);
$serv ->addData("plafon",$_POST["plafon"]);
$serv ->addData("keterangan",$_POST["keterangan"]);
$serv ->addData("petugas",$user->getNameOnly());
$serv ->execute();

$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$response ->setAlertVisible(true);
$response ->setAlertContent("Success","Permintaan Peninjauan Berhasil di Simpan");

echo json_encode($response->getPackage());
?>