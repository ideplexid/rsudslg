<?php 
global $db;
$ruangan     = $_POST['ruangan'];
$header      = array("Nama","Status","Pasien","NRM");
$uitable     = new Table($header);
$uitable     ->setName("chooser_kamar")
             ->setModel(Table::$SELECT);

if(isset($_POST['command'])){
    require_once "smis-base/smis-include-service-consumer.php";
    $adapter = new SimpleAdapter ();
    $adapter ->add ( "Nama", "nama" )
             ->add ( "Status", "terpakai", "trivial_0_Kosong_Terpakai" )
             ->add ( "Pasien", "nama_pasien" )
             ->add ( "NRM", "nrm_pasien", "digit6" );
	$dbres   = new ServiceResponder($db, $uitable, $adapter,"get_bed_kamar_chooser",$ruangan);
	$data    = $dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}
echo $uitable->getHtml();
?>