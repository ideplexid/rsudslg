<?php
global $db;
$dbtable = new DBTable($db,"smis_hrd_employee");
$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$response ->setContent(array("resume"=>1));
$msg = "";
$warn = array();
$employee = $_POST['employee'];
foreach($employee as $x){
    if($x['id']=="" && $x['nama']==""){
        continue;
    }
    $title =$x['title'];
    $exist = array(
        "id" => $x['id'],
        "nama" => $x['nama']
    );
    if(!$dbtable ->is_exist($exist)){
        $msg  .= "<li>Silakan Pilih ".$x['title'].", Pastikan Jangan Diketik Manual</li>";
        $warn[] = $x['warn'];
    }
}

if($msg!=""){
    //$response->setAlertVisible(true);
    //$response->setAlertContent("Data yang diinputkan Salah","<ul>".$msg."</ul>",Alert::$DANGER);
    $array = array();
    $array['resume'] = "0";
    $array['warn'] = $warn;
    $array['msg'] = "<ul>".$msg."</ul>";
    $response ->setContent($array);
}

echo json_encode($response->getPackage());