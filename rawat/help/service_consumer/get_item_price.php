<?php 

/**
 * 
 * this provided manual for get_tagihan service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response="159000";
$provided=array();
$provided['id_obat']="12";
$provided['satuan']="Biji";

$descrption="
This service must provide by Gudang Farmasi, This Service Consumer only One Goal:
1. Provide The Last Price of Item (Obat) in Gudang Farmasi
";

$author="
* @version 1.0
* @since 8 Oct 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_item_price");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>