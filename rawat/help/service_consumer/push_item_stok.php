<?php 

/**
 * 
 * fungsi ini untuk meminta penggunaan stok obat tertentu
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';


$provided=array();
$provided['id_obat']="14 (Drug ID)";
$provided['jumlah']="10 (Total Item That Used)";
$provided['satuan']="Biji";
$provided['keterangan']="Digunakan Oleh Pasien X, NoRM : 10000, NoReg:20892010 ";
$provided['user']="Admin Name (admin) --> Username that used this thing, used for Log of the stok";

$response=array();
$response['success']="0 or 1 (0 if fail and 1 if succeed)";
$response['reason']="( if success=1, let it blank, otherwise give a reason ) Stok Sisa 12 yang diminta 14";
$response['stok']="12 (stok terakhir saat ini)";
$response['id_transaksi']="100 ( ID from the process of usability )";

$descrption="
This service must provide by Modul that Used BOI, to used the item that his already used by User
";

$author="
* @version 1.0
* @since 8 Oct 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("push_item_stok");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>