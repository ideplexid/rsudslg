<?php 

/**
 * 
 * this provided manual for get_tagihan service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';


$provided=array();
$provided['id_obat']="14 (Drug ID)";
$provided['jumlah']="10 (Total Item That Used before)";
$provided['satuan']="Biji";
$provided['keterangan']="Batal Digunakan Oleh Pasien X, NoRM : 10000, NoReg:20892010 ";
$provided['user']="Admin Name (admin) --> Username that used this thing, used for Log of the stok";
$provided['id_transaksi']="100 (transaction id that sent by boi, now have to return)";

$response=array();
$response['success']="0 or 1 (0 if fail and 1 if succeed)";
$response['reason']="( if success=1, let it blank, otherwise give a reason ) No Drug Like That";
$descrption="This service must provide by Modul that Used BOI, used to cancel Stok that Already Used";

$author="
* @version 1.0
* @since 8 Oct 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("push_item_stok");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>