<?php 

/**
 * 
 * this provided manual for browse_resep service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$needed=array();
$needed[]="Bodrex";
$needed[]="Biji";
$needed[]="15";


$descrption="
This Service using ServiceResponder, with using Default data and
have one 'Needed Data' need to be overriden, the 'Edit' action.
must privide by 'Gudang Farmasi'
the 'Edit' Action should provide :
1. Nama Obat
2. Satuan Obat
3. ID Obat
";

$author="
* @version 1.0
* @since 8 Oct 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_list_obat",ManualService::$RESPOND_SERVICE);
$man->setAuthor($author);
$man->setDescription($descrption);
$man->addDataNeeded("edit", $needed);
echo $man->getHtml();
?>