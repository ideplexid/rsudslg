<?php 

/**
 * 
 * this provided manual for get_tagihan service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response['jumlah']="15";
$response['satuan']="Biji";

$provided=array();
$provided['id_obat']="12";
$provided['ruangan']="anggrek_ii_a";

$descrption="
This service must provide by Modul that Used BOI, This Service Have 2 Goal:
1. Provide The Last Stok Item Total
2. Provide The Item Count
";

$author="
* @version 1.0
* @since 8 Oct 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_item_stok");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>