<strong>Menambahkan Pasien</strong>
<ul>
	<li>Tombol <a href="#"  class="input btn btn-primary " > <i class="fa fa-plus"></i>  </a> Dipakai untuk Menambah Pasien</li>
	<li> Muncul Dialog</li>
	<li> Masukan NRM Pasien</li>
	<li> Masukan Nama Pasien</li>
	<li> Pilih Jenis Kelamin, Asal, Cara Bayar, dan Kelas Biarkan Sama dengan Kelas Ruangan</li>
	<li> Selesai Pilih yang 'Belum'</li>
	<li> Tanggal Otomatis Hari ini, tapi Boleh diubah jika ternyata tidak sama</li>
	<li> Keluar Dikosongkan Dahulu</li>
	<li> Kemudian Tekan Tombol <a href="#" class="btn btn-primary "> <i class="fa fa-save"></i>  </a></li>
	<li> Data Pasien Akan Tersimpan</li>
</ul>

<div class='line'></div>

<strong>Masukan Tindakan Pasien</strong>
<ul>
	<li>Tekan Tombol <a href="#"  class="input btn btn-success" > <i class="fa fa-sign-in"></i>  </a> Untuk Lanjut ke Detail Tindakan, Visite dll</li>
	<li> Setelah Ditekan Akan Pindah Halaman lain (ke Halaman Layanan)</li>
</ul>

<div class='line'></div>

<strong>Mengeluarkan Pasien</strong>
<ul>
	<li>Tombol <a href="#"  class="input btn btn-danger " > <i class="fa fa-sign-out"></i>  </a> Dipakai untuk Mengeluarkan Pasien</li>
	<li> Selesai Pilih yang 'Ya'</li>
	<li> Tanggal Otomatis Hari ini, tapi Boleh diubah jika ternyata tidak sama</li>
	<li> Keluar pilih sesuai dengan Kondisi Keluar Pasien dari Ruagan ini Seperti Keluar Hidup, Pulang Paksa, Pindah Kamar dll</li>
	<li> Kemudian Tekan Tombol <a href="#" class="btn btn-primary "> <i class="fa fa-save"></i>  </a></li>
	<li> Data Pasien Akan Hilang dari Daftar tetapi Sebenarnya Tidak Hilang, Malainkan Di simpan tetapi tidak dapat diubah atau diutak-atik</li>
</ul>
