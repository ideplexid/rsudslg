<?php
	require_once("smis-libs-inventory/mutasi_masuk_unit.php");
    
    $slug=$_POST['prototype_slug'];
	$name=$_POST['prototype_name'];
	$page=$_POST['page'];
	$action=$_POST['action'];
	$implement=$_POST['prototype_implement'];
    
	global $db;
	$mutasi_masuk_unit = new MutasiMasukUnit($db, $name, "smis_rwt_obat_masuk_".$page, "smis_rwt_stok_obat_".$page, "smis_rwt_kartu_stok_obat_".$page, "smis_rwt_riwayat_stok_obat_".$page, $page);
	$mutasi_masuk_unit->setPrototype($name,$slug,$implement);
    $mutasi_masuk_unit->initialize();
?>