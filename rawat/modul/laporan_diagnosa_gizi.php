<?php 

global $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterServiceTemplate.php";
require_once "smis-libs-class/MasterSlaveServiceTemplate.php";
require_once "rawat/class/responder/LaporanDiagnosaResponder.php";

$gizi = new MasterSlaveServiceTemplate($db,"lap_diagnosa_gizi",$_POST['prototype_slug'],'laporan_diagnosa_gizi');
$gizi ->setPrototipe(true,$_POST['prototype_name'],$_POST['prototype_slug'],'rawat');

$responder = new LaporanDiagnosaResponder($db,$gizi->getUItable(),$gizi->getAdapter(),"lap_diagnosa_gizi","gizi");
$gizi ->setDBresponder($responder);
$gizi ->getUItable()->setAction(false);
$gizi ->getUItable()->addModal("dari","date","Dari","");
$gizi ->getUItable()->addModal("sampai","date","Sampai","");
$form = $gizi ->getForm();
$form ->setTitle("Laporan Diagnosa Gizi ".$_POST['prototype_name']);

$btn = new Button("","","Search");
$btn ->setIcon("fa fa-search");
$btn ->setIsButton(Button::$ICONIC);
$btn ->setClass("btn btn-primary");
$btn ->setAction("laporan_diagnosa_gizi.view()");

$excel = new Button("","","Excel");
$excel ->setIcon("fa fa-file-excel-o");
$excel ->setIsButton(Button::$ICONIC);
$excel ->setClass("btn btn-primary");
$excel ->setAction("laporan_diagnosa_gizi.excel()");
$form ->addElement("",$btn);
$form ->addElement("",$excel);

$gizi ->getUItable()->addHeaderElement("No.");
$gizi ->getUItable()->addHeaderElement("Tanggal Kunjungan");
$gizi ->getUItable()->addHeaderElement("Tanggal Lahir");
$gizi ->getUItable()->addHeaderElement("No. RM");
$gizi ->getUItable()->addHeaderElement("No. Registrasi");
$gizi ->getUItable()->addHeaderElement("Nama");
$gizi ->getUItable()->addHeaderElement("Poli Rujukan");
$gizi ->getUItable()->addHeaderElement("Dokter");
$gizi ->getUItable()->addHeaderElement("Jenis Kelamin");
$gizi ->getUItable()->addHeaderElement("Diagnosa Gizi");
$gizi ->getUItable()->addHeaderElement("BB/TB");
$gizi ->getUItable()->addHeaderElement("Status Gizi");
$gizi ->getUItable()->addHeaderElement("Cara Bayar");

$gizi ->getAdapter()->setUseNumber(true,"No.","back.");
$gizi ->getAdapter()->add("Tanggal Kunjungan","tanggal","date d M Y");
$gizi ->getAdapter()->add("Tanggal Lahir","tanggal_lahir","date d M Y");
$gizi ->getAdapter()->add("No. RM","nrm_pasien");
$gizi ->getAdapter()->add("No. Registrasi","noreg_pasien");
$gizi ->getAdapter()->add("Nama","nama_pasien");
$gizi ->getAdapter()->add("Jenis Kelamin","jk","trivial_0_Laki-Laki_Perempuan");
$gizi ->getAdapter()->add("Dokter","nama_dokter");
$gizi ->getAdapter()->add("Diagnosa Gizi","nama_icd");
$gizi ->getAdapter()->add("BB/TB","bbtb");
$gizi ->getAdapter()->add("Status Gizi","status_gizi");
$gizi ->getAdapter()->add("Poli Rujukan","ruangan_rujukan","unslug");
$gizi ->getAdapter()->add("Cara Bayar","carabayar","unslug");

$gizi ->addRegulerData("dari","dari","id-value");
$gizi ->addRegulerData("sampai","sampai","id-value");
$gizi ->setDateEnable(true);
$gizi ->setModalTitle("Laporan Diagnosa Gizi");
$gizi ->setModalComponentSize(Modal::$MEDIUM);

$gizi ->initialize();