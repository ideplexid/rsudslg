<?php
	require_once("smis-libs-inventory/mutasi_keluar_unit.php");
    $slug=$_POST['prototype_slug'];
	$name=$_POST['prototype_name'];
	$page=$_POST['page'];
	$action=$_POST['action'];
	$implement=$_POST['prototype_implement'];
    
	global $db;
	$mutasi_keluar_unit = new MutasiKeluarUnit($db, $name, "smis_rwt_obat_masuk_".$page, "smis_rwt_stok_obat_".$slug, "smis_rwt_mutasi_keluar_".$slug, "smis_rwt_stok_mutasi_keluar_".$slug, "smis_rwt_kartu_stok_obat_".$slug, "smis_rwt_riwayat_stok_obat_".$slug, $page);
	$mutasi_keluar_unit->setPrototype($name,$slug,$implement);
    $mutasi_keluar_unit->initialize();
?>