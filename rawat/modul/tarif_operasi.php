<?php
/**
 * this class used to handle 
 * the queue that registered in this patient
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv3
 * @since 		: 17 Aug 2014
 * @database 	: smis_rwt_antrian_
 * @version 	: 4.0.5 
 * 
 */
global $db;

require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterServiceTemplate.php";
require_once "smis-base/smis-include-service-consumer.php";

$div    = "<div id='header_status_operasi'>
            <h2 id='title_status_operasi'>Tarif Operasi</h2>
          </div>";

$status = new MasterServiceTemplate($db,"get_tindakan_operasi",$_POST['prototype_slug'],"tarif_operasi");
$status->setPrototipe(true,$_POST['prototype_name'],$_POST['prototype_slug'],$_POST['prototype_implement']);
$status->getUItable()->setHeader(array ('No.','Nama','Dokter','Kelas','Pasien',"Jenis Operasi",'Tarif Dokter','Team OK',"Sewa Kamar" ));
$status->getUItable()->addHeader("before",$div);
$status->getUItable()->setAction(false);
$adapter = $status->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add ( "Dokter", "nama_dokter" );
$adapter->add ( "Nama", "nama" );
$adapter->add ( "Jenis Operasi", "jenis_operasi" );
$adapter->add ( "Kelas", "kelas", "unslug" );
$adapter->add ( "Pasien", "jenis_pasien", "unslug" );
$adapter->add ( "Tarif Dokter", "tarif", "money Rp." );
$adapter->add ( "Team OK", "ok", "money Rp." );
$adapter->add ( "Sewa Kamar", "tarif_sewa_ok", "money Rp." );
$status ->getUItable()->setControlPosition(Table::$CONTROL_TOP);
$status->initialize();