<?php
/**
 * this class used to handle 
 * the queue that registered in this patient
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv3
 * @since 		: 17 Aug 2014
 * @database 	: smis_rwt_antrian_
 * @version 	: 4.0.5 
 * 
 */
global $db;

$img    = "<img src='".getLogo()."' >";
$title  = getSettings($db,"smis_autonomous_title");
$div    = "<div id='header_status_operasi'>
            <div id='logo_status_operasi'>".$img."</div> <h2 id='title_status_operasi'>".$title."</h2>
          </div>";

require_once "smis-libs-class/MasterTemplate.php";
$status = new MasterTemplate($db,"smis_rwt_antrian_".$_POST['prototype_slug'],$_POST['prototype_slug'],"status_operasi");
$status->setPrototipe(true,$_POST['prototype_name'],$_POST['prototype_slug'],$_POST['prototype_implement']);
$status->getUItable()->setHeader(array("No.","Masuk","NRM","Nama","Asal","Ruangan","Status"));
$status->getUItable()->addHeader("before",$div);
$status->getDBTable()->addCustomKriteria(" selesai","='0' ");
$status->getDBTable()->setShowAll(true);
$status->getUItable()->setAction(false);
$status->getUItable()->setFooterVisible(false);
$status->getAdapter()->setUseNumber(true,"No.","back.");
$status->getAdapter()->add("NRM","nrm_pasien");
$status->getAdapter()->add("Asal","asal","unslug");
$status->getAdapter()->add("Nama","nama_pasien");
$status->getAdapter()->add("Ruangan","room_name");
$status->getAdapter()->add("Status","state_name");
$status->getAdapter()->add("Masuk","waktu","date d M Y H:i");
$status->addResouce("js","rawat/resource/js/status_operasi.js","after",false);
$status->addResouce("css","rawat/resource/css/status_operasi.css","after",false);
$status->initialize();
?>
