<?php 		

global $db;

if (isset($_POST['id_dokter'])) {
	$id = $_POST['id_dokter'];
	$from = $_POST['from_date'];
	$to = $_POST['to_date'] . ' 23:59:59';
	$slug = $_POST['slug'];
	
	$table = "smis_rwt_konsultasi_dokter_$slug";
	$dbtable = new DBTable($db, $table);
	$pdr_rows = $dbtable->get_result("
		SELECT noreg_pasien, nama_pasien, sum(harga) as pdr
		FROM $table
		WHERE id_dokter = '$id' AND waktu BETWEEN '$from' AND '$to'
		AND prop NOT LIKE 'del'
		GROUP BY noreg_pasien
	");
	
	$table = "smis_rwt_antrian_$slug";
	$dbtable = new DBTable($db, $table);
	$antrian_rows = $dbtable->get_result("
		SELECT no_register
		FROM $table
		WHERE waktu BETWEEN '$from' AND '$to'		
	");
		
	$noRegister = array();
	foreach ($antrian_rows as $row) {
		$noRegister[] = $row->no_register;
	}	
		
	echo json_encode(array($pdr_rows, $noRegister));

}
?>
