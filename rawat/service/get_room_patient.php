<?php 
/**
 * provising data to show patient in room
 * 
 * @database : smis_rwt_antrian_
 * @author 	 : Nurul Huda
 * @copyright: goblooge@gmail.com
 * @license  : LGPLv3
 * @since	 : 5 Aug 2017
 * @version	 : 1.0.1
 * */
 
global $db;
$entitiy = $_GET['entity'];
if(isset($_POST['command']) && $_POST['command']!=""){
    $dbtable = new DBTable($db,"smis_rwt_antrian_".$entitiy);
    if(isset($_POST['selesai']) && $_POST['selesai']!="")   $dbtable->addCustomKriteria("selesai","'".$_POST['selesai']."'");
    if(isset($_POST['dari']) && $_POST['dari']!="")         $dbtable->addCustomKriteria("tanggal >=","'".$_POST['tanggal']."'");
    if(isset($_POST['sampai']) && $_POST['sampai']!="")     $dbtable->addCustomKriteria("tanggal <","'".$_POST['sampai']."'");
    if(isset($_POST['tanggal']) && $_POST['tanggal']!="")   $dbtable->addCustomKriteria("tanggal =","'".$_POST['tanggal']."'");
    $dbres   = new ServiceProvider($dbtable);
    $data    = $dbres->command($_POST['command']);
    echo json_encode($data);
}

?>