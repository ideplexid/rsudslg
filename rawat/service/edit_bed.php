<?php 
show_error();
global $db;
$entity=$_GET['entity'];

require_once "rawat/class/adapter/TagihanAdapter.php";
$adapter = new TagihanAdapter ($db,$entity);
$adapter ->setAdapterName("bed");

require_once "rawat/class/dbtable/TindakanDBTable.php";
$dbtable = new TindakanDBTable($db, "smis_rwt_bed_".$entity);
$dbtable ->setAutoSynch(true);
$dbtable ->setAdapter($adapter);
$dbtable ->setEntity($entity);
$dbtable ->setJenis("bed");

require_once "smis-framework/smis/service/ServiceProvider.php";
require_once "rawat/class/responder/BedServiceResponder.php";

$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$service=new BedServiceResponder($dbtable,$entity);
$data=$service->command($_POST['command']);
echo json_encode($data);
?>