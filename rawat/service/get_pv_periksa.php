<?php 
global $db;

require_once 'rawat/class/adapter/SalaryAdapter.php';
require_once 'rawat/class/adapter/KonsultasiDokterAdapter.php';
$slug=$_GET["entity"];
$dbtable=new DBTable($db, "smis_rwt_konsultasi_dokter_".$slug);
$dbtable->setShowAll(true);
$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);

if($_POST['id_karyawan']!="-1"){
	$dbtable->addCustomKriteria("id_dokter", "='".$_POST['id_karyawan']."'");
}


$res= $dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'")
			  ->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'")
			  ->view("", 0);

$dlist=$res['data'];
$adapter=new KonsultasiDokterAdapter($_POST['id_karyawan'], $slug,KonsultasiDokterAdapter::$TYPE_BOTH);
$result=$adapter->getContent($dlist);
echo json_encode($result);


?>