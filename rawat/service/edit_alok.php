<?php 
show_error();
global $db;
$entity=$_GET['entity'];

require_once "rawat/class/adapter/TagihanAdapter.php";
$adapter = new TagihanAdapter ($db,$entity);
$adapter ->setAdapterName("alok");

require_once "rawat/class/dbtable/TindakanDBTable.php";
$dbtable = new TindakanDBTable($db, "smis_rwt_alok_".$entity);
$dbtable ->setAutoSynch(true);
$dbtable ->setAdapter($adapter);
$dbtable ->setEntity($entity);
$dbtable ->setJenis("alok");


require_once "smis-framework/smis/service/ServiceProvider.php";
require_once "rawat/class/responder/AlokServiceResponder.php";
$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$service=new AlokServiceResponder($dbtable,$entity);
$data=$service->command($_POST['command']);
echo json_encode($data);
?>