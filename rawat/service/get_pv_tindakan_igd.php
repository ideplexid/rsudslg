<?php 
	global $db;
    
	require_once 'rawat/class/adapter/SalaryAdapter.php';
	require_once 'rawat/class/adapter/TindakanIGDAdapter.php';
	$slug=$_GET["entity"];
	$dbtable=new DBTable($db, "smis_rwt_tindakan_igd_".$slug);
	$dbtable->setShowAll(true);
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);

	if($_POST['id_karyawan']!="-1" && $_POST['id_karyawan']!=""){
		$dbtable->addCustomKriteria(NULL, " (id_perawat ='".$_POST['id_karyawan']."' OR id_dokter ='".$_POST['id_karyawan']."' ) ");
	}


	$res= $dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'")
				  ->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'")
				  ->view("", 0);
	$dlist=$res['data'];
	$tipical=$_POST['type'];
    $adapter=null;
    if($tipical=="individu"){
        $adapter=new TindakanIGDAdapter($_POST['id_karyawan'], $slug,TindakanIGDAdapter::$TYPE_INDIVIDU);
    }else if($tipical=="communal"){
        $adapter=new TindakanIGDAdapter($_POST['id_karyawan'], $slug,TindakanIGDAdapter::$TYPE_COMMUNAL);
    }else{
        $adapter=new TindakanIGDAdapter($_POST['id_karyawan'], $slug,TindakanIGDAdapter::$TYPE_BOTH);
    }
	$result=$adapter->getContent($dlist);
	echo json_encode($result);
?>
