<?php 
global $db;
$id                     = array();
$id['no_register']      = $_POST['id'];
$data                   = array();
$detail                 = array();
$detail['alamat']       = $_POST['alamat_pasien'];
$detail['caradatang']   = $_POST['caradatang'];
$data['alamat']         = $_POST['alamat_pasien']." - ".$_POST['nama_kelurahan']." - ".$_POST['nama_kecamatan']." - ".$_POST['nama_kabupaten']." - ".$_POST['nama_provinsi'];
$data['waktu']          = $_POST['tanggal'];
$data['nama_pasien']    = $_POST['nama_pasien'];
$data['jk']             = $_POST['kelamin'];
$data['carabayar']      = $_POST['carabayar'];
$data['umur']           = $_POST['umur'];
$data['golongan_umur']  = $_POST['gol_umur'];
$data['ibu_kandung']    = $_POST['ibu'];
$data['detail']         = json_encode($detail);

$entity                 = $_GET['entity'];
$dbtable                = new DBTable($db,"smis_rwt_antrian_".$entity);
$dbtable->update($data,$id);

$idx = array("noreg_pasien"=> $_POST['id']);
$uptd = array(
    "nama_pasien"       =>$_POST['nama_pasien'],
    "carabayar"         =>$_POST['carabayar']
);
$dbtable                = new DBTable($db,"smis_rwt_alok_".$entity);
$dbtable->update($uptd,$idx);

$uptd = array(
    "nama_pasien"       =>$_POST['nama_pasien']
);
$dbtable                = new DBTable($db,"smis_rwt_alok_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_bed_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_bed_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_bed_kamar_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_bronchoscopy_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_audiometry_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_ekg_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_endoscopy_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_spirometry_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_faal_paru_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_konsul_sokter_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_visite_dokter_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_konsultasi_dokter_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_ok_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_oksigen_central_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_oksigen_manual_".$entity);
$dbtable->update($uptd,$idx);

$dbtable                = new DBTable($db,"smis_rwt_rr_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_tindakan_dokter_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_tindakan_perawat_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_tindakan_igd_".$entity);
$dbtable->update($uptd,$idx);
$dbtable                = new DBTable($db,"smis_rwt_vk_".$entity);
$dbtable->update($uptd,$idx);

