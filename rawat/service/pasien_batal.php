<?php 
    global $db;
    $poli                         = $_GET['entity'];
    $data['no_register']          = $_POST['noreg_pasien'];
    $update['selesai']            = 1;
    $update['waktu_keluar']       = date("Y-m-d H:i:s");
    $update['cara_keluar']        = "Tidak Datang";
    $update['keterangan_keluar']  = "Dibatalkan oleh pendaftaran";
    $dbtable                      = new DBTable($db,"smis_rwt_antrian_".$poli);
    $dbtable                      ->update($update,$data);
?>