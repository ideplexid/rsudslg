<?php 
global $db;
$table=array();
$table['periksa']=array("query"=>"id_dokter, sum(harga) as total","tbl"=>"smis_rwt_konsultasi_dokter");
$table['konsul']=array("query"=>"id_dokter, sum(harga) as total","tbl"=>"smis_rwt_konsul_dokter");
$table['visite']=array("query"=>"id_dokter, sum(harga) as total","tbl"=>"smis_rwt_visite_dokter");
$table['ok']=array("query"=>"id_operator_satu as id_dokter, sum(harga_operator_satu) as total","tbl"=>"smis_rwt_ok");

$carabayar=array();
$carabayar['umum']=array("dari"=>$_POST['dari'],"sampai"=>$_POST['sampai'],"kriteria"=>"carabayar!='bpjs'");
$carabayar['bpjs']=array("dari"=>$_POST['dari_bpjs'],"sampai"=>$_POST['sampai_bpjs'],"kriteria"=>"carabayar='bpjs'");

$result=array();
foreach($carabayar as $c=>$cv){
    foreach($table as $k=>$t){
        $query="SELECT ".$t['query']." FROM ".$t['tbl']."_".$_GET['entity']."
                WHERE prop!='del' 
                AND waktu >= '".$cv['dari']."' 
                AND waktu < '".$cv['sampai']."' 
                AND ".$cv['kriteria']."
                GROUP BY id_dokter";
        $list=$db->get_result($query);
        foreach($list as $x){
            if(!isset($result[$x->id_dokter])){
                $result[$x->id_dokter]=array();
            }
            $result[$x->id_dokter][$c.'_'.$k]=$x->total;
        }
    }
}
echo json_encode($result);
?>