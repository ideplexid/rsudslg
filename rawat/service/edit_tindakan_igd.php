<?php 
global $db;
$entity=$_GET['entity'];
require_once "smis-framework/smis/service/ServiceProvider.php";
require_once "rawat/function/get_pv_service.php";

require_once "rawat/class/adapter/TagihanAdapter.php";
$adapter = new TagihanAdapter ($db,$entity);
$adapter ->setAdapterName("tindakan_igd");

require_once "rawat/class/dbtable/TindakanDBTable.php";
$dbtable = new TindakanDBTable($db, "smis_rwt_tindakan_igd_".$entity);
$dbtable ->setAutoSynch(true);
$dbtable ->setAdapter($adapter);
$dbtable ->setEntity($entity);
$dbtable ->setJenis("tindakan_igd");

$ps = "";
if(getSettings($db,"smis-rs-provit-share-service-".$entity,"0")=="1"){
    $ps = get_pv_service($db, $entity ,$_POST['carabayar'], "smis-pv-tindakan-perawat-igd");    
}
$noreg=$_POST['noreg_pasien'];

/*provit share per perawat*/
$number=array("I"=>"",
            "II"=>"_dua",
            "III"=>"_tiga",
            "IV"=>"_empat",
            "V"=>"_lima",
            "VI"=>"_enam",
            "VII"=>"_tujuh",
            "VIII"=>"_delapan",
            "IX"=>"_sembilan",
            "X"=>"_sepuluh"
            );
$total_perawat=0;
foreach($number as $rum=>$num){
    if(isset($_POST['id_perawat'.$num]) &&  $_POST['id_perawat'.$num]!="" && $_POST['id_perawat'.$num]=="0" && $_POST['id_perawat'.$num]!=0 ){
        $total_perawat++;
    }
}

//$dbtable=new DBTable($db, "smis_rwt_tindakan_igd_".$entity);
$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$service=new ServiceProvider($dbtable);
$service->addColumnFixValue("kelas", getSettings($db, "smis-rs-kelas-".$entity, "rawat_jalan"));
$service->addColumnFixValue("pembagian", $ps);
$service->addColumnFixValue ( "total_perawat", $total_perawat );
$data=$service->command($_POST['command']);
echo json_encode($data);


?>