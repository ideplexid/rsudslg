<?php
	global $db;
	
	/// param			: noreg_pasien
	/// json response	: [0].ruangan, [0].sewa_kamar
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$slug_rawat = $_GET['entity'];
		
		$dbtable = new DBTable($db, "smis_rwt_bed_" . $slug_rawat);
		$row = $dbtable->get_row("
			SELECT SUM(biaya) AS 'sewa_kamar'
			FROM smis_rwt_bed_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$sewa_kamar = 0;
		if ($row != null) {
			$sewa_kamar += $row->sewa_kamar;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"		=> $slug_rawat,
			"sewa_kamar"	=> $sewa_kamar
		);
		echo json_encode($data);
	}
?>