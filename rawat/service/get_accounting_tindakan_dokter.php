<?php 
    global $db;
    $id      = $_POST['data'];
    $entity  = $_GET['entity'];
    $ename   = ArrayAdapter::format("unslug",$_GET['entity']);
    
    $dbtable = new DBTable($db,"smis_rwt_tindakan_dokter_".$entity);
    $x = $dbtable ->selectEventDel($id);
    $acc_ename = $entity."-".$x->carabayar;
     $list  = array();
     $final = array();
    //content untuk debit
    if($x->harga>0){
        $debet=array();
        $debet['akun']    = getSettings($db,"smis-rs-acc-k-tindakan-dokter-".$acc_ename,"");
        $debet['debet']   = 0;
        $debet['kredit']  = $x->harga;
        $debet['ket']     = "Pendapatan Tindakan Dokter ".$ename." - ".$x->nama_tindakan." pasien  ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $debet['code']    = "kredit-tindakan-dokter-".$acc_ename."-".$x->id;

        //content untuk kredit
        $kredit=array();
        $kredit['akun']    = getSettings($db,"smis-rs-acc-d-tindakan-dokter-".$acc_ename,"");
        $kredit['debet']   = $x->harga;
        $kredit['kredit']  = 0;
        $kredit['ket']     = "Piutang Tindakan Dokter ".$ename." - ".$x->nama_tindakan." pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $kredit['code']    = "debet-tindakan-dokter-".$acc_ename."-".$x->id;
        
        $list[] = $debet;
        $list[] = $kredit;
    }
    
    if($x->harga_perawat){
        //unutk perwaat debet
        $debet=array();
        $debet['akun']    = getSettings($db,"smis-rs-acc-k-perawat-tindakan-dokter-".$acc_ename,"");
        $debet['debet']   = 0;
        $debet['kredit']  = $x->harga_perawat;
        $debet['ket']     = "Pendapatan  Perawat Tindakan Dokter ".$ename." - ".$x->nama_tindakan." pasien  ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $debet['code']    = "kredit-tindakan-dokter-".$acc_ename."-".$x->id;

        //unutk perwaat kredit
        $kredit=array();
        $kredit['akun']    = getSettings($db,"smis-rs-acc-d-perawat-tindakan-dokter-".$acc_ename,"");
        $kredit['debet']   = $x->harga_perawat;
        $kredit['kredit']  = 0;
        $kredit['ket']     = "Piutang Perawat Tindakan Dokter ".$ename." - ".$x->nama_tindakan." pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $kredit['code']    = "debet-tindakan-dokter-".$acc_ename."-".$x->id;
        
        $list[] = $debet;
        $list[] = $kredit;
        
    }
    
    if($x->harga+$x->harga_perawat>0){
        //content untuk header
        $header=array();
        $header['tanggal']      = $x->waktu;
        $header['keterangan']   = "Tindakan Dokter Pasien ".$ename." - ".$x->nama_tindakan." - Pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $header['code']         = "tindakan-dokter-".$acc_ename."-".$x->id;
        $header['nomor']        = "TDK-".$ename."-".$x->id;
        $header['debet']        = $x->harga+$x->harga_perawat;
        $header['kredit']       = $x->harga+$x->harga_perawat;
        $header['io']           = "1";
        
        $transaction_karcis=array();
        $transaction_karcis['header']=$header;
        $transaction_karcis['content']=$list;
        
        
        $final[]=$transaction_karcis;
    }
    
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
    
?>