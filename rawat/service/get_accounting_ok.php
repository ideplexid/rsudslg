<?php 
  global $db;
  $id   = $_POST['data'];
  $entity = $_GET['entity'];
  $ename  = ArrayAdapter::format("unslug",$_GET['entity']);
  $list  = array();
  $final  = array();
  $dbtable = new DBTable($db,"smis_rwt_ok_".$entity);
  $x = $dbtable ->selectEventDel($id);
  $acc_ename = $entity."-".$x->carabayar;
  
  /*operator satu*/
  if($x->harga_operator_satu>0){
        $debet=array();
        $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-op1-".$acc_ename,"");
        $debet['debet']  = 0;
        $debet['kredit'] = $x->harga_operator_satu;
        $debet['ket']   = "Pendapatan OK - Operator I ".$ename." - ".$x->nama_operator_satu." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $debet['code']  = "kredit-ok-op1-".$acc_ename."-".$x->id;
        $list[] = $debet;
        $kredit=array();
        $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-op1-".$acc_ename,"");
        $kredit['debet']  = $x->harga_operator_satu;
        $kredit['kredit'] = 0;
        $kredit['ket']   = "Piutang OK - Operator I ".$ename." - ".$x->nama_operator_satu." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $kredit['code']  = "debet-ok-op1-".$acc_ename."-".$x->id;
        $list[] = $kredit;
  }
    
  
  /*operator dua*/
   if($x->harga_operator_dua>0){
        $debet=array();
        $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-op2-".$acc_ename,"");
        $debet['debet']  = 0;
        $debet['kredit'] = $x->harga_operator_dua;
        $debet['ket']   = "Pendapatan OK - Operator II ".$ename." - ".$x->nama_operator_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $debet['code']  = "kredit-ok-op2-".$acc_ename."-".$x->id;
        $list[] = $debet;
        $kredit=array();
        $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-op2-".$acc_ename,"");
        $kredit['debet']  = $x->harga_operator_dua;
        $kredit['kredit'] = 0;
        $kredit['ket']   = "Piutang OK - Operator II ".$ename." - ".$x->nama_operator_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $kredit['code']  = "debet-ok-op2-".$acc_ename."-".$x->id;
        $list[] = $kredit;
   }
  
  /*asisten operator satu*/
  if($x->harga_asisten_operator_satu>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-aop1-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_asisten_operator_satu;
    $debet['ket']   = "Pendapatan OK - Asisten Operator I ".$ename." - ".$x->nama_asisten_operator_satu." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-aop1-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-aop1-".$acc_ename,"");
    $kredit['debet']  = $x->harga_asisten_operator_satu;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Asisten Operator I ".$ename." - ".$x->nama_asisten_operator_satu." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-aop1-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
  
  /*asisten operator dua*/
  if($x->harga_asisten_operator_dua>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-aop2-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_asisten_operator_dua;
    $debet['ket']   = "Pendapatan OK - Asisten Operator II ".$ename." - ".$x->nama_asisten_operator_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-aop1-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-aop2-".$acc_ename,"");
    $kredit['debet']  = $x->harga_asisten_operator_dua;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Asisten Operator II ".$ename." - ".$x->nama_asisten_operator_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-aop1-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
  
  /*anastesi*/
  if($x->harga_anastesi>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-ana-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_anastesi;
    $debet['ket']   = "Pendapatan OK - Anastesi ".$ename." - ".$x->nama_anastesi." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-ana-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-ana-".$acc_ename,"");
    $kredit['debet']  = $x->harga_anastesi;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Anastesi ".$ename." - ".$x->nama_anastesi." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-ana-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
  /*asisten anastesi satu*/
  if($x->harga_asisten_anastesi>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-as-ana1-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_asisten_anastesi;
    $debet['ket']   = "Pendapatan OK - Asisten Anastesi I ".$ename." - ".$x->nama_asisten_anastesi." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-as-ana1-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-as-ana1-".$acc_ename,"");
    $kredit['debet']  = $x->harga_asisten_anastesi;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Asisten Anastesi I ".$ename." - ".$x->nama_asisten_anastesi." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-as-ana1-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
  /*asisten anastesi satu*/
  if($x->harga_asisten_anastesi_dua>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-as-ana2-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_asisten_anastesi_dua;
    $debet['ket']   = "Pendapatan OK - Asisten Anastesi II ".$ename." - ".$x->nama_asisten_anastesi_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-as-ana2-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-as-ana2-".$acc_ename,"");
    $kredit['debet']  = $x->harga_asisten_anastesi_dua;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Asisten Anastesi II ".$ename." - ".$x->nama_asisten_anastesi_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-as-ana2-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
  /*team ok*/
  if($x->harga_team_ok>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-team-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_team_ok;
    $debet['ket']   = "Pendapatan OK - Team OK ".$ename." - ".$x->nama_team_ok." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-team-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-team-".$acc_ename,"");
    $kredit['debet']  = $x->harga_team_ok;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Team OK ".$ename." - ".$x->nama_team_ok." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-team-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
  
  /*bidan I*/
  if($x->harga_bidan>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-bidan-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_bidan;
    $debet['ket']   = "Pendapatan OK - Bidan I ".$ename." - ".$x->nama_bidan." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-bidan-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-bidan-".$acc_ename,"");
    $kredit['debet']  = $x->harga_bidan;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Bidan I ".$ename." - ".$x->nama_bidan." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-bidan-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
  /*bidan II*/
  if($x->harga_bidan_dua>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-bidan2-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_bidan_dua;
    $debet['ket']   = "Pendapatan OK - Bidan II".$ename." - ".$x->nama_bidan_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-bidan2-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-bidan2-".$acc_ename,"");
    $kredit['debet']  = $x->harga_bidan_dua;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Bidan II ".$ename." - ".$x->nama_bidan_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-bidan2-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
   /*perawat*/
   if($x->harga_perawat>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-perawat-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_perawat;
    $debet['ket']   = "Pendapatan OK - Perawat ".$ename." - ".$x->nama_perawat." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-bidan-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-perawat-".$acc_ename,"");
    $kredit['debet']  = $x->harga_perawat;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Perawat ".$ename." - ".$x->nama_perawat." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-perawat-".$acc_ename."-".$x->id;
    $list[] = $kredit;
   }
    
    
  /*sewa_kamar*/
  if($x->harga_sewa_kamar>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-sewa-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_sewa_kamar;
    $debet['ket']   = "Pendapatan OK - Sewa Kamar ".$ename." - ".$x->nama_sewa_kamar." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-sewa-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-sewa-".$acc_ename,"");
    $kredit['debet']  = $x->harga_sewa_kamar;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Sewa Kamar ".$ename." - ".$x->nama_sewa_kamar." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-sewa-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
  
  if($x->harga_sewa_alat>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-alat-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_sewa_alat;
    $debet['ket']   = "Pendapatan OK - Sewa Alat ".$ename." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-sewa-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-alat-".$acc_ename,"");
    $kredit['debet']  = $x->harga_sewa_alat;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Sewa Alat ".$ename." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-alat-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
  
  /*oomloop 1*/
  if($x->harga_oomloop_satu>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-oomloop1-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_oomloop_satu;
    $debet['ket']   = "Pendapatan OK - Oomloop I ".$ename." - ".$x->nama_oomloop_satu." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-oomloop1-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-oomloop1-".$acc_ename,"");
    $kredit['debet']  = $x->harga_oomloop_satu;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Oomloop I ".$ename." - ".$x->nama_oomloop_satu." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-oomloop1-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
  /*oomloop 2*/
  if($x->harga_oomloop_dua>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-oomloop2-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_oomloop_dua;
    $debet['ket']   = "Pendapatan OK - Oomloop II ".$ename." - ".$x->nama_oomloop_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-oomloop2-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-oomloop2-".$acc_ename,"");
    $kredit['debet']  = $x->harga_oomloop_dua;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Oomloop II ".$ename." - ".$x->nama_oomloop_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-oomloop2-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
  /*instrument 1*/
  if($x->harga_instrument>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-instrument1-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_instrument;
    $debet['ket']   = "Pendapatan OK - Instrument I ".$ename." - ".$x->nama_instrument." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-instrument1-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-instrument1-".$acc_ename,"");
    $kredit['debet']  = $x->harga_instrument;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Instrument I ".$ename." - ".$x->nama_instrument." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-instrument1-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
  /*instrument 2*/
  if($x->harga_instrument_dua>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-instrument2-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->harga_instrument_dua;
    $debet['ket']   = "Pendapatan OK - Instrument II ".$ename." - ".$x->nama_instrument_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-instrument1-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-instrument2-".$acc_ename,"");
    $kredit['debet']  = $x->harga_instrument_dua;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Instrument II ".$ename." - ".$x->nama_instrument_dua." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-instrument2-".$acc_ename."-".$x->id;
    $list[] = $kredit;
  }
    
/*recovery room */
if($x->recovery_room>0){
    $debet=array();
    $debet['akun']  = getSettings($db,"smis-rs-acc-k-ok-rr-".$acc_ename,"");
    $debet['debet']  = 0;
    $debet['kredit'] = $x->recovery_room;
    $debet['ket']   = "Pendapatan OK - Recovery Room ".$ename." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']  = "kredit-ok-rr-".$acc_ename."-".$x->id;
    $list[] = $debet;
    $kredit=array();
    $kredit['akun']  = getSettings($db,"smis-rs-acc-d-ok-rr-".$acc_ename,"");
    $kredit['debet']  = $x->recovery_room;
    $kredit['kredit'] = 0;
    $kredit['ket']   = "Piutang OK - Recovery Room ".$ename." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']  = "debet-ok-rr-".$acc_ename."-".$x->id;
    $list[] = $kredit;
}
  
  //content untuk header
  $header=array();
  $header['tanggal']   = $x->waktu;
  $header['keterangan']  = "Operasi Pasien ".$ename." - ".$x->nama_dokter." - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
  $header['code']     = "ok-".$acc_ename."-".$x->id;
  $header['nomor']    = "OK-".$ename."-".$x->id;
  $header['debet']    = $x->harga_operator_satu
                        +$x->harga_operator_dua
                        +$x->harga_asisten_operator_satu
                        +$x->harga_asisten_operator_dua
                        +$x->harga_anastesi
                        +$x->harga_asisten_anastesi
                        +$x->harga_asisten_anastesi_dua
                        +$x->harga_team_ok
                        +$x->harga_bidan
                        +$x->harga_bidan_dua
                        +$x->harga_perawat
                        +$x->harga_sewa_kamar
                        +$x->harga_oomloop_satu
                        +$x->harga_oomloop_dua
                        +$x->harga_instrument
                        +$x->harga_instrument_dua
                        +$x->recovery_room;
  $header['kredit']    = $header['debet'];
  $header['io']      = "1";
  
  $transaction_karcis=array();
  $transaction_karcis['header']=$header;
  $transaction_karcis['content']=$list;
  
  
  $final[]=$transaction_karcis;
  
  echo json_encode($final);
  
  /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
  
?>