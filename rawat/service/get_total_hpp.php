<?php
	global $db;
	if (isset($_POST['id_obat']) && isset($_POST['ruangan'])) { 
		$dbtable = new DBTable($db, "smis_rwt_stok_obat_" . $_GET['entity']);
		$row = $dbtable->get_row("
			SELECT id_obat, SUM(sisa * hna) AS 'total_hpp', satuan
			FROM smis_rwt_stok_obat_" . $_GET['entity'] . " a
			LEFT JOIN smis_rwt_obat_masuk_". $_GET['entity']." b
			ON a.id_obat_masuk=b.id
			WHERE 
			a.prop NOT LIKE 'del' 
			AND b.status='sudah'
			AND id_obat = '" . $_POST['id_obat'] . "' 
			AND satuan = satuan_konversi AND konversi = '1'
			GROUP BY id_obat, satuan, satuan_konversi
			LIMIT 0, 1
		");
		$data = array(
			"total_hpp"	=> $row != null ? $row->total_hpp : 0,
			"satuan"	=> $row != null ? $row->satuan : "-"
		);		
		echo json_encode($data);
	}
?>