<?php 
global $db;
$entity=$_GET['entity'];
show_error();
require_once "smis-framework/smis/service/ServiceProvider.php";
require_once "rawat/class/service_provider/OksigenManualServiceProvider.php";

$adapter = new TagihanAdapter ($db,$entity);
$adapter ->setAdapterName("oksigen_manual");

require_once "rawat/class/dbtable/TindakanDBTable.php";
$dbtable = new TindakanDBTable($db, "smis_rwt_oksigen_manual_".$entity);
$dbtable ->setAutoSynch(true);
$dbtable ->setAdapter($adapter);
$dbtable ->setEntity($entity);
$dbtable ->setJenis("oksigen_manual");

//$dbtable=new DBTable($db, "smis_rwt_oksigen_manual_".$entity);
$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$service=new OksigenManualServiceProvider($dbtable,$entity);
$data=$service->command($_POST['command']);
echo json_encode($data);
?>