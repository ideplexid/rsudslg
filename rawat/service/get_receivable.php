<?php 
global $db;
$awal=$_POST['awal'];
$akhir=$_POST['akhir'];
$entity=$_GET['entity'];

$response=array();
$response['page']="0"; 													// always 0
$response['max_page']="0"; 												// always 0.
$response['data']=array(); 		
$urjip=strtolower(getSettings($db, "smis-rs-urjip-".$entity, "urj"));	// array of string


$query["Tindakan Perawat"]="SELECT SUM(harga_tindakan) FROM smis_rwt_tindakan_perawat_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Tindakan Dokter"]="SELECT SUM(harga) FROM smis_rwt_tindakan_dokter_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Recovery Room"]="SELECT SUM(harga) FROM smis_rwt_rr_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Periksa Dokter"]="SELECT SUM(harga+harga_perawat) FROM smis_rwt_konsultasi_dokter_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Alat Obat"]="SELECT SUM(harga*jumlah) FROM smis_rwt_alok_".$entity." WHERE tanggal>='$awal' AND tanggal<='$akhir'";
$query["Operasi"]="SELECT SUM(harga_operator_satu+harga_operator_dua+harga_anastesi+harga_asisten_anastesi+harga_team_ok+harga_bidan+harga_perawat+harga_sewa_kamar+harga_sewa_alat) FROM smis_rwt_ok_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Bersalin"]="SELECT SUM(harga_operator_satu+harga_operator_dua+harga_anastesi+harga_asisten_anastesi+harga_team_vk+harga_bidan+harga_perawat+harga_sewa_kamar) FROM smis_rwt_vk_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Bronchoscopy"]="SELECT SUM(biaya) FROM smis_rwt_bronchoscopy_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["EKG"]="SELECT SUM(biaya) FROM smis_rwt_ekg_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Spirometry"]="SELECT SUM(biaya) FROM smis_rwt_spirometry_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Bed"]="SELECT SUM(biaya) FROM smis_rwt_bed_".$entity." WHERE waktu_keluar>='$awal' AND waktu_keluar<='$akhir'";
$query["Audiometry"]="SELECT SUM(biaya) FROM smis_rwt_audiometry_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Faal Paru"]="SELECT SUM(biaya) FROM smis_rwt_faal_paru_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Konsul Dokter"]="SELECT SUM(harga) FROM smis_rwt_konsul_dokter_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Visite"]="SELECT SUM(biaya) FROM smis_rwt_visite_dokter_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Oksigen Manual"]="SELECT SUM(harga) FROM smis_rwt_oksigen_manual_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";
$query["Oksigen Central"]="SELECT SUM(harga) FROM smis_rwt_oksigen_central_".$entity." WHERE waktu>='$awal' AND waktu<='$akhir'";

foreach ($query as $name=>$q){
	$nilai=$db->get_var($q);
	$dt=array();
	$dt["layanan"]=$name;
	$dt["nilai"]=$nilai;
	$dt["urjip"]=$urjip;
	$response["data"][]=$dt;
}
echo json_encode($response);

?>