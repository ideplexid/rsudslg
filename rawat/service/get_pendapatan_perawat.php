<?php 
global $db;
require_once "rawat/function/array_summary.php";
$table=array();
$table['tindakan_perawat']=array("query"=>" * ","tbl"=>"smis_rwt_tindakan_perawat");
$table['tindakan_igd']=array("query"=>" * ","tbl"=>"smis_rwt_tindakan_igd");

$carabayar=array();
$carabayar['umum']=array("dari"=>$_POST['dari'],"sampai"=>$_POST['sampai'],"kriteria"=>"carabayar!='bpjs'");
$carabayar['bpjs']=array("dari"=>$_POST['dari_bpjs'],"sampai"=>$_POST['sampai_bpjs'],"kriteria"=>"carabayar='bpjs'");

$result=array();
foreach($carabayar as $c=>$cv){
    foreach($table as $k=>$t){
        $query="SELECT ".$t['query']." FROM ".$t['tbl']."_".$_GET['entity']."
                WHERE prop!='del' 
                AND waktu >= '".$cv['dari']."' 
                AND waktu < '".$cv['sampai']."' 
                AND ".$cv['kriteria']." ";
        $list=$db->get_result($query);
        foreach($list as $x){
            $satuan=$x->jaspel/$x->total_perawat;
            array_summary($result,$x->id_perawat,$c,$satuan);
            array_summary($result,$x->id_perawat_dua,$c,$satuan);
            array_summary($result,$x->id_perawat_tiga,$c,$satuan);
            array_summary($result,$x->id_perawat_empat,$c,$satuan);
            array_summary($result,$x->id_perawat_lima,$c,$satuan);
            array_summary($result,$x->id_perawat_enam,$c,$satuan);
            array_summary($result,$x->id_perawat_tujuh,$c,$satuan);
            array_summary($result,$x->id_perawat_delapan,$c,$satuan);
            array_summary($result,$x->id_perawat_sembilan,$c,$satuan);
            array_summary($result,$x->id_perawat_sepuluh,$c,$satuan);
        }
    }
}
echo json_encode($result);
?>