<?php 
	global $db;
	if(isset($_POST['command'])){
		
		$polislug=$_GET['entity'];
		$dari=$_POST['dari'];
		$sampai=$_POST['sampai'];		
		/*$kriteria="(
						( smis_rwt_antrian_".$polislug.".waktu<='".$dari."' AND smis_rwt_antrian_".$polislug.".waktu_keluar>='".$dari."' AND waktu_keluar!='0000-00-00 00:00:00') 
					OR  ( smis_rwt_antrian_".$polislug.".waktu<='".$dari."' AND smis_rwt_antrian_".$polislug.".waktu_keluar>='".$sampai."' AND waktu_keluar!='0000-00-00 00:00:00') 
					OR  ( smis_rwt_antrian_".$polislug.".waktu>='".$dari."' AND smis_rwt_antrian_".$polislug.".waktu_keluar<='".$sampai."' AND waktu_keluar!='0000-00-00 00:00:00') 
					OR  ( smis_rwt_antrian_".$polislug.".waktu<='".$sampai."' AND smis_rwt_antrian_".$polislug.".waktu_keluar>='".$sampai."' AND waktu_keluar!='0000-00-00 00:00:00')
					OR  ( smis_rwt_antrian_".$polislug.".selesai=0)
				  )";*/
        $kriteria="( 
                    (smis_rwt_antrian_".$polislug.".waktu>='".$dari."' AND smis_rwt_antrian_".$polislug.".waktu<'".$sampai."') 
                    OR (smis_rwt_antrian_".$polislug.".waktu_keluar>='".$dari."' AND smis_rwt_antrian_".$polislug.".waktu_keluar<'".$sampai."'  AND waktu_keluar!='0000-00-00 00:00:00')
                    OR ( smis_rwt_antrian_".$polislug.".waktu<'".$dari."' AND smis_rwt_antrian_".$polislug.".waktu_keluar>='".$sampai."' AND waktu_keluar!='0000-00-00 00:00:00')
                    OR (( smis_rwt_antrian_".$polislug.".waktu<'".$dari."' AND smis_rwt_antrian_".$polislug.".waktu_keluar='0000-00-00 00:00:00') AND selesai != '1')
				  )";
		$dbtable=new DBTable($db,"smis_rwt_antrian_".$polislug);
		$qv="SELECT smis_rwt_antrian_".$polislug.".* , 
                    smis_rwt_rujukan_".$polislug.".ruangan as pindahan
			 FROM 
                smis_rwt_antrian_".$polislug." LEFT JOIN                 
                (SELECT * FROM smis_rwt_rujukan_".$polislug." WHERE prop!='del' )
                smis_rwt_rujukan_".$polislug."
                
			 ON smis_rwt_antrian_".$polislug.".no_register = smis_rwt_rujukan_".$polislug.".noreg_pasien ";
		
		$qc="SELECT count(*) as total FROM smis_rwt_antrian_".$polislug." LEFT JOIN 
            
            (SELECT * FROM smis_rwt_rujukan_".$polislug." WHERE prop!='del' )
            smis_rwt_rujukan_".$polislug."
			ON smis_rwt_antrian_".$polislug.".no_register = smis_rwt_rujukan_".$polislug.".noreg_pasien ";
		
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setUseWhereforView(true);
		$dbtable->addCustomKriteria(" smis_rwt_antrian_".$polislug.".cara_keluar "," !='Tidak Datang' ");
		//$dbtable->addCustomKriteria(NULL," ( smis_rwt_rujukan_".$polislug.".prop !='del') ");// OR smis_rwt_rujukan_".$polislug.".prop is NULL
		$dbtable->addCustomKriteria(NULL,$kriteria);
		$dbtable->setMaximum(1);	
		if($_POST['command']=="count"){
			$query=$dbtable->getQueryCount("");
			$total=$db->get_var($query);
			echo json_encode($total);
			return;
		}
		$data=$dbtable->view("", $_POST['halaman']);
		echo json_encode($data['data']);
	}
?>

