<?php 
    global $db;
    $id      = $_POST['data'];
    $entity  = $_GET['entity'];
    $ename   = ArrayAdapter::format("unslug",$_GET['entity']);
    $list   = array();
    $final=array();
    $dbtable = new DBTable($db,"smis_rwt_alok_".$entity);
    $x = $dbtable ->selectEventDel($id);
    $acc_ename = $entity."-".$x->carabayar;
    
    /*pendapatan*/
        $debet=array();
        $debet['akun']    = getSettings($db,"smis-rs-acc-k-alok-".$acc_ename,"");
        $debet['debet']   = 0;
        $debet['kredit']  = $x->harga*$x->jumlah;
        $debet['ket']     = "Pendapatan Alat & Obat Kesehatan ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $debet['code']    = "kredit-alok-".$acc_ename."-".$x->id;
        $list[] = $debet;
        $kredit=array();
        $kredit['akun']    = getSettings($db,"smis-rs-acc-d-alok-".$acc_ename,"");
        $kredit['debet']   = $x->harga*$x->jumlah;
        $kredit['kredit']  = 0;
        $kredit['ket']     = "Piutang Alat & Obat Kesehatan ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $kredit['code']    = "debet-alok-".$acc_ename."-".$x->id;
        $list[] = $kredit;
        
    /*hpp*/
    if($x->hpp>0){
        $debet=array();
        $debet['akun']    = getSettings($db,"smis-rs-acc-k-alok-hpp-".$acc_ename,"");
        $debet['debet']   = 0;
        $debet['kredit']  = $x->hpp*$x->jumlah;
        $debet['ket']     = "Persediaan HPP -  Alat & Obat Kesehatan ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $debet['code']    = "kredit-alok-hpp-".$acc_ename."-".$x->id;
        $list[] = $debet;
        $kredit=array();
        $kredit['akun']    = getSettings($db,"smis-rs-acc-d-alok-hpp-".$acc_ename,"");
        $kredit['debet']   = $x->hpp*$x->jumlah;
        $kredit['kredit']  = 0;
        $kredit['ket']     = "Biaya HPP - Alat & Obat Kesehatan ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
        $kredit['code']    = "debet-alok-hpp-".$acc_ename."-".$x->id;
        $list[] = $kredit;
    }

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->tanggal;
    $header['keterangan']   = "Alat & Obat Kesehatan Pasien ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $header['code']         = "alok-".$acc_ename."-".$x->id;
    $header['nomor']        = "AOK-".$ename."-".$x->id;
    $header['debet']        = ($x->harga+$x->hpp)*$x->jumlah;
    $header['kredit']       = ($x->harga+$x->hpp)*$x->jumlah;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;
    
    $final[]=$transaction_karcis;    
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>