<?php 
	global $db;
	$entity=$_GET['entity'];
	$id=NULL;
	$id_antrian=NULL;
	if(isset($_POST["noreg_pasien"]) && $_POST["noreg_pasien"]!="" ){
		$id=array("noreg_pasien"=>$_POST["noreg_pasien"]);
		$id_antrian=array("no_register"=>$_POST["noreg_pasien"]);
	}else if(isset($_POST["nrm_pasien"]) && $_POST["nrm_pasien"]!=""){
		$id=array("nrm_pasien"=>$_POST["nrm_pasien"]);
		$id_antrian=$id;
	}else{
		return;
	}
	$up=array("nrm_pasien"=>$_POST["nrm_pasien_baru"],"nama_pasien"=>$_POST["nama_pasien_baru"]);
	$dbtable=new DBTable($db,"smis_rwt_antrian_".$entity);
	$dbtable->update($up,$id_antrian);
	
	$query=array();
	$query[]="smis_rwt_rujukan";
	$query[]="smis_rwt_tindakan_perawat";
	$query[]="smis_rwt_tindakan_igd";
	$query[]="smis_rwt_tindakan_dokter";
	$query[]="smis_rwt_alok";
	$query[]="smis_rwt_oksigen_central";
	$query[]="smis_rwt_oksigen_manual";
	$query[]="smis_rwt_visite_dokter";
	$query[]="smis_rwt_konsul_dokter";
	$query[]="smis_rwt_konsultasi_dokter";
	$query[]="smis_rwt_faal_paru";
	$query[]="smis_rwt_bronchoscopy";
	$query[]="smis_rwt_endoscopy";
	$query[]="smis_rwt_spirometry";
	$query[]="smis_rwt_audiometry";
	$query[]="smis_rwt_bed";
	$query[]="smis_rwt_ekg";
	$query[]="smis_rwt_vk";
	$query[]="smis_rwt_ok";
	foreach($query as $index=>$table){
		$dbtable->setName($table."_".$entity);
		$dbtable->update($up,$id);
	}
?>