<?php 

/**
 * fungsi ini dipakai untuk menghitung berapa jumlah operator operasi dari seorang dokter
 * dan menghitung jumlah berapa kali perujuk dari seorang dokter
 * dalam rentang waktu tertentu.
 * 
 * dalam update terbaru dibuat langsung nilai rupiah 
 * dari berapa nilai bagi hasil dokter
 * sehingga akan lebih memudahkan proses perhitungan
 * 
 * @author : Nurul Huda
 * @since : 01-Jan-2017
 * @version : 2.0.1
 * @database : smis_rwt_ok_[entity]
 * @copyright  : goblooge@gmail.com
 * @license : LGPLv3
 * */
 
global $db;
$entity=$_GET['entity'];
$hasil=array();
$dbname="smis_rwt_ok_".$entity;
$query="SELECT sum(bagi_dokter)/2 as uang, sum(1) as jumlah 
                FROM ".$dbname."
                WHERE 
                waktu>='".$_POST['dari']."' 
                AND waktu<'".$_POST['sampai']."' 
                AND prop!='del' 
                AND id_operator_satu='".$_POST['id_dokter']."'";
$x=$db->get_row($query);
$hasil['uang_operator']=$x->uang;
$hasil['jumlah_operator']=$x->jumlah;

$query="SELECT sum(bagi_dokter)/2 as uang, sum(1) as jumlah 
                FROM ".$dbname."
                WHERE 
                waktu>='".$_POST['dari']."' 
                AND waktu<'".$_POST['sampai']."' 
                AND prop!='del' 
                AND id_perujuk='".$_POST['id_dokter']."'";
$y=$db->get_row($query);
$hasil['uang_rujukan']=$y->uang;
$hasil['jumlah_rujukan']=$y->jumlah;

echo json_encode($hasil);

?>