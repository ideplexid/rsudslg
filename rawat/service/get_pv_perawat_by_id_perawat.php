<?php 

global $db;
$entity=$_GET['entity'];
$id_perawat=$_POST['id_perawat'];
$dari=$_POST['dari'];
$sampai=$_POST['sampai'];
$carabayar=$_POST['carabayar'];

$dbtable=new DBTable($db,"smis_rwt_tindakan_perawat_".$entity);
$dbtable->addCustomKriteria(NULL," waktu >='".$dari."'");
$dbtable->addCustomKriteria(NULL," waktu <'".$sampai."'");
$dbtable->setView(array("id","jaspel","waktu","jumlah","kelas","nama_tindakan","nama_pasien","nrm_pasien","noreg_pasien","total_perawat","carabayar"));
if($carabayar!=""){
    $dbtable->addCustomKriteria(" carabayar ","='".$carabayar."'");
}
$dbtable->addCustomKriteria(NULL," 
        (
            id_perawat          ='".$id_perawat."' OR 
            id_perawat_dua      ='".$id_perawat."' OR 
            id_perawat_tiga     ='".$id_perawat."' OR 
            id_perawat_empat    ='".$id_perawat."' OR 
            id_perawat_lima     ='".$id_perawat."' OR 
            id_perawat_enam     ='".$id_perawat."' OR 
            id_perawat_tujuh    ='".$id_perawat."' OR 
            id_perawat_delapan  ='".$id_perawat."' OR 
            id_perawat_sembilan ='".$id_perawat."' OR 
            id_perawat_sepuluh  ='".$id_perawat."'
        )
");

$view=$dbtable->view("",0);
$data_tindakan_perawat=$view['data'];

$dbtable->setName("smis_rwt_tindakan_igd_".$entity);
$view=$dbtable->view("",0);
$data_tindakan_igd=$view['data'];

$result=array_merge($data_tindakan_igd,$data_tindakan_perawat);
$pack=array();
$pack['data']=$result;
$pack['page']=1;
$pack['max_page']=1;
echo json_encode($pack);
?>