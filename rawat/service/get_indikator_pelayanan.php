<?php 
/**
 JHP 	= Total Jumlah Hari Perawatan dalam Periode Tertentu
 JTT	= total jumlah tempat tidur
 JH 	= total jumlah hari dalam periode tertentu
 JP 	= jumlah pasien
 JPH 	= jumlah pasien hidup
 JPM48 	= jumlah pasien meninggal > 48 jam
 JPM 	= jumlah pasien mati

 bor = JHP * 100% / (JTT*JH)
 los = JHP / JP
 toi = (JTT * JH - JHP)/JP
 bto = JP/JTT
 ndr = JPM48 * 100% / JP
 gdr = JPM * 100% / JP
 */

global $db;
$ruangan=$_GET['entity'];
loadLibrary("smis-libs-function-time");
$rl12=getSettings($db,  "smis-rs-rl12-" . $_GET['entity'], "0")=="1";
if(!$rl12){
	return NULL;
}
$dari=$_POST['dari'];
$sampai=$_POST['sampai'];

$JH=day_diff_only($dari,$sampai)+1; // karena tanggal awalnya juga dihitung.

$query="SELECT count(*) FROM smis_rwt_bed_kamar_".$ruangan." WHERE prop!='del'";
$JTT=$db->get_var($query);

$JPH_INTERN=0; 		// total jumlah penggunaan yang berada dalam rentang tanggal
$JPH_EXTERN=0; 		// total jumlah penggunaan mana tanggal berada dalam rentang penggunaan
$JPH_START_INSIDE=0;	// total jumlah hari yang tanggal mulai nya berada dalam tengah-tengah rentang
$JPH_END_INSIDE=0;	// total jumlah hari yang tanggal selesainya berada dalam tengah-tengah tanggal

//mencari hari yang berada dalam peride $dari dan $sampai.
$query="SELECT SUM(hari) FROM smis_rwt_bed_".$ruangan." WHERE DATE(waktu_masuk)>'".$dari."' AND DATE(waktu_keluar)<'".$sampai."' AND prop!='del'";
$JPH_INTERN=$db->get_var($query);

//mencari hari yang mana rentan periode ada dalam hari itu.
$query="SELECT count(*) FROM smis_rwt_bed_".$ruangan." WHERE DATE(waktu_masuk)<='".$dari."' AND DATE(waktu_keluar)>='".$sampai."' AND prop!='del'";
$JPH_EXTERN=$db->get_var($query)*$JH;

//mencari hari tanggal mulainya berada dalam rentang.
$query="SELECT SUM(DATEDIFF('".$sampai."',DATE(waktu_masuk))+1) FROM smis_rwt_bed_".$ruangan." WHERE DATE(waktu_masuk)>'".$dari."' AND DATE(waktu_keluar)>='".$sampai."' AND DATE(waktu_masuk)<='".$sampai."' AND prop!='del'";
$JPH_START_INSIDE=$db->get_var($query);

//mencari hari tanggal akhirnya berada dalam rentang.
$query="SELECT SUM(DATEDIFF(DATE(waktu_keluar),'".$dari."')+1) FROM smis_rwt_bed_".$ruangan." WHERE DATE(waktu_masuk)<='".$dari."' AND DATE(waktu_keluar)<'".$sampai."' AND DATE(waktu_keluar)>='".$dari."' AND prop!='del'";
$JPH_END_INSIDE=$db->get_var($query);
$JHP=$JPH_INTERN+$JPH_EXTERN+$JPH_START_INSIDE+$JPH_END_INSIDE;

$query="SELECT count(*) FROM smis_rwt_antrian_".$ruangan." WHERE DATE(waktu)>='".$dari."' AND DATE(waktu)<='".$sampai."' AND prop!='del' AND cara_keluar!='Tidak Datang'";
$JP=$db->get_var($query);

$query="SELECT count(*) FROM smis_rwt_antrian_".$ruangan." WHERE DATE(waktu)>='".$dari."' AND DATE(waktu)<='".$sampai."' AND prop!='del' AND cara_keluar!='Tidak Datang' AND cara_keluar!='Dipulangkan Mati <=48 Jam' AND cara_keluar!='Dipulangkan Mati >48 Jam'";
$JPH=$db->get_var($query);

$query="SELECT count(*) FROM smis_rwt_antrian_".$ruangan." WHERE DATE(waktu)>='".$dari."' AND DATE(waktu)<='".$sampai."' AND prop!='del' AND cara_keluar='Dipulangkan Mati >48 Jam'";
$JPM48=$db->get_var($query);

$query="SELECT count(*) FROM smis_rwt_antrian_".$ruangan." WHERE DATE(waktu)>='".$dari."' AND DATE(waktu)<='".$sampai."' AND prop!='del' AND ( cara_keluar='Dipulangkan Mati >48 Jam' OR cara_keluar!='Dipulangkan Mati <=48 Jam' )";
$JPM=$db->get_var($query);

$result=array();
$result['JHP']=$JHP;
$result['JTT']=$JTT;
$result['JH']=$JH;
$result['JP']=$JP;
$result['JPH']=$JPH;
$result['JPM48']=$JPM48;
$result['JPM']=$JPM;
$result['JPH_INTERN']=$JPH_INTERN;
$result['JPH_EXTERN']=$JPH_EXTERN;
$result['JPH_START_INSIDE']=$JPH_START_INSIDE;
$result['JPH_END_INSIDE']=$JPH_END_INSIDE;
$result['RUANGAN']=$_GET['entity'];
$result['GRUP_RUANGAN']=getSettings($db, "smis-rs-grup-ruangan-".$_GET['entity'], $_GET['entity']);
$result['KELAS']=getSettings($db, "smis-rs-kelas-grup-ruangan-".$_GET['entity'], "");

echo json_encode($result);

?>