<?php
    $entity                           = $_GET["entity"];
    $kriteria ['no_register']         = $_POST['noreg_pasien'];
    $tbname                           = "smis_rwt_antrian_".$entity;
    $dbtable                          = new DBTable($db,$tbname);
    $pasien                           = $dbtable->select($kriteria);
    $result                           = array();
    if($pasien != null){
        $result ['ruangan']           = get_entity_name($db,$entity);
        $result ['entity']            = $entity;
        $result ['keluar']            = $pasien->cara_keluar;
        $result ['waktu_keluar']      = $pasien->waktu_keluar;
        $result ['status']            = $pasien->selesai;
    }
    echo json_encode($result);
?>