<?php 
global $db;
$noreg=$_POST['noreg_pasien'];
$entity=$_GET['entity'];
$query["tindakan_perawat"]="SELECT SUM(harga_tindakan) FROM smis_rwt_tindakan_perawat_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["tindakan_perawat_igd"]="SELECT SUM(harga_tindakan) FROM smis_rwt_tindakan_igd_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["tindakan_dokter"]="SELECT SUM(harga+harga_perawat) FROM smis_rwt_tindakan_dokter_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["recovery_room"]="SELECT SUM(harga) FROM smis_rwt_rr_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["periksa"]="SELECT SUM(harga+harga_perawat) FROM smis_rwt_konsultasi_dokter_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["alat_obat"]="SELECT SUM(harga*jumlah) FROM smis_rwt_alok_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["operasi"]="SELECT SUM(harga_tindakan) FROM smis_rwt_ok_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["recovery_room_operasi"]="SELECT SUM(recovery_room) FROM smis_rwt_ok_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["vk"]="SELECT SUM(harga_operator_satu+harga_operator_dua+harga_anastesi+harga_asisten_anastesi+harga_team_vk+harga_bidan+harga_perawat+harga_sewa_kamar) FROM smis_rwt_vk_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND prop!='del'";
$query["bronchoscopy"]="SELECT SUM(biaya) FROM smis_rwt_bronchoscopy_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["endoscopy"]="SELECT SUM(biaya) FROM smis_rwt_endoscopy_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["ekg"]="SELECT SUM(biaya) FROM smis_rwt_ekg_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["spirometry"]="SELECT SUM(biaya) FROM smis_rwt_spirometry_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["bed"]="SELECT SUM(biaya) FROM smis_rwt_bed_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["audiometry"]="SELECT SUM(biaya) FROM smis_rwt_audiometry_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["konsul"]="SELECT SUM(harga) FROM smis_rwt_konsul_dokter_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["visite"]="SELECT SUM(harga) FROM smis_rwt_visite_dokter_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["oksigen_manual"]="SELECT SUM(harga) FROM smis_rwt_oksigen_manual_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$query["oksigen_central"]="SELECT SUM(harga) FROM smis_rwt_oksigen_central_".$entity." WHERE noreg_pasien='".$noreg."'  AND prop!='del'";
$query["faal_paru"]="SELECT SUM(biaya) FROM smis_rwt_faal_paru_".$entity." WHERE noreg_pasien='".$noreg."' AND prop!='del'";
$response=array();
$total=0;
foreach ($query as $name=>$q){
	$nilai=$db->get_var($q)*1;
	$total=+$nilai;
}
$response['ruangan']=$entity;
$response['total']=$total;
echo json_encode($response);
?>