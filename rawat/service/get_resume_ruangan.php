<?php 
	global $db;
	$entity=$_GET['entity'];
	$dari=$_POST['dari'];
	$sampai=$_POST['sampai'];
	
	$query = "SELECT COUNT(*) as jumlah, cara_keluar FROM smis_rwt_antrian_".$entity. " WHERE prop!='del' AND waktu>='".$dari."' AND waktu<'".$sampai."' GROUP BY cara_keluar";
	$datalist = $db->get_result($query,false);
	
	$result=array();
	$result['ruangan']=$entity;
	$result['berlangsung']=0;
	$result['hidup']=0;
	$result['mati_k48']=0;
	$result['mati_l48']=0;
	$result['pulang_paksa']=0;
	$result['kabur']=0;
	$result['rawat_inap']=0;
	$result['pindah_kamar']=0;
	$result['poli_lain']=0;
	$result['rs_lain']=0;
	$result['perujuk']=0;
	$result['tdk_datang']=0;
	$result['total']=0;
	$result['total_datang']=0;
	
	foreach($datalist as $one){
		if($one['cara_keluar']=="") 								$result['berlangsung']	=$one['jumlah'];
		else if($one['cara_keluar']=="Tidak Datang") 				$result['tdk_datang']	=$one['jumlah'];
		else if($one['cara_keluar']=="Dipulangkan Hidup") 			$result['hidup']		=$one['jumlah'];
		else if($one['cara_keluar']=="Dipulangkan Mati <=48 Jam")	$result['mati_k48']		=$one['jumlah'];
		else if($one['cara_keluar']=="Dipulangkan Mati >48 Jam") 	$result['mati_l48']		=$one['jumlah'];
		else if($one['cara_keluar']=="Pulang Paksa") 				$result['pulang_paksa']	=$one['jumlah'];
		else if($one['cara_keluar']=="Kabur") 						$result['kabur']		=$one['jumlah'];
		else if($one['cara_keluar']=="Rawat Inap") 					$result['rawat_inap']	=$one['jumlah'];
		else if($one['cara_keluar']=="Pindah Kamar") 				$result['pindah_kamar']	=$one['jumlah'];
		else if($one['cara_keluar']=="Rujuk Poli Lain") 			$result['poli_lain']	=$one['jumlah'];
		else if($one['cara_keluar']=="Rujuk RS Lain") 				$result['rs_lain']		=$one['jumlah'];
		else if($one['cara_keluar']=="Dikembalikan ke Perujuk") 	$result['perujuk']		=$one['jumlah'];
		$result['total']+=$one['jumlah'];
	}
	$result['total_datang']=$result['total']-$result['tdk_datang'];
	echo json_encode($result);
?>