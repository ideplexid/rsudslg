<?php 
global $db;
$awal=$_POST['awal'];
$akhir=$_POST['akhir'];
$entity=$_GET['entity'];
$origin=isset($_POST['origin'])?$_POST['origin']:"%";

$response=array();
$urjip=strtolower(getSettings($db, "smis-rs-urjip-".$entity, "urj"));	// array of string
$query["tindakan_perawat"]="SELECT SUM(harga_tindakan) FROM smis_rwt_tindakan_perawat_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["tindakan_perawat_igd"]="SELECT SUM(harga_tindakan) FROM smis_rwt_tindakan_igd_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["tindakan_dokter"]="SELECT SUM(harga+harga_perawat) FROM smis_rwt_tindakan_dokter_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["recovery_room"]="SELECT SUM(harga) FROM smis_rwt_rr_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["periksa"]="SELECT SUM(harga+harga_perawat) FROM smis_rwt_konsultasi_dokter_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["alat_obat"]="SELECT SUM(harga*jumlah) FROM smis_rwt_alok_".$entity." WHERE tanggal>='$awal' AND tanggal<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["operasi"]="SELECT SUM(harga_operator_satu+harga_operator_dua+harga_asisten_operator_satu+harga_asisten_operator_dua+harga_anastesi+harga_asisten_anastesi+harga_asisten_anastesi_dua+harga_team_ok+harga_bidan+harga_bidan_dua+harga_perawat+harga_sewa_kamar+harga_sewa_alat+harga_oomloop_satu+harga_oomloop_dua+harga_instrument+harga_instrument_dua) FROM smis_rwt_ok_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["recovery_room_operasi"]="SELECT SUM(recovery_room) FROM smis_rwt_ok_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["vk"]="SELECT SUM(harga_operator_satu+harga_operator_dua+harga_anastesi+harga_asisten_anastesi+harga_team_vk+harga_bidan+harga_perawat+harga_sewa_kamar) FROM smis_rwt_vk_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["bronchoscopy"]="SELECT SUM(biaya) FROM smis_rwt_bronchoscopy_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["endoscopy"]="SELECT SUM(biaya) FROM smis_rwt_endoscopy_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["ekg"]="SELECT SUM(biaya) FROM smis_rwt_ekg_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["spirometry"]="SELECT SUM(biaya) FROM smis_rwt_spirometry_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["bed"]="SELECT SUM(biaya) FROM smis_rwt_bed_".$entity." WHERE waktu_keluar>='$awal' AND waktu_keluar<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["audiometry"]="SELECT SUM(biaya) FROM smis_rwt_audiometry_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["faal_paru"]="SELECT SUM(biaya) FROM smis_rwt_faal_paru_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["konsul"]="SELECT SUM(harga) FROM smis_rwt_konsul_dokter_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir'AND origin LIKE '".$origin."'  AND prop!='del'";
$query["visite"]="SELECT SUM(harga) FROM smis_rwt_visite_dokter_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir'AND origin LIKE '".$origin."'  AND prop!='del'";
$query["oksigen_manual"]="SELECT SUM(harga) FROM smis_rwt_oksigen_manual_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'  AND prop!='del'";
$query["oksigen_central"]="SELECT SUM(harga) FROM smis_rwt_oksigen_central_".$entity." WHERE waktu>='$awal' AND waktu<'$akhir' AND origin LIKE '".$origin."'   AND prop!='del'";
foreach ($query as $name=>$q){
	$nilai=$db->get_var($q);
	if($name=="faal_paru")
		$name="spirometry";
	else if($name=="tindakan_perawat_igd")
		$name="tindakan_perawat";
	else if($name=="recovery_room_operasi")
		$name="recovery_room";
	$dt=array();
	$dt["layanan"]=$name;
	$dt["nilai"]=$nilai;
	$dt["urjip"]=$urjip;
	$dt["ruangan"]=$entity;
	$response[]=$dt;
}
echo json_encode($response);
?>