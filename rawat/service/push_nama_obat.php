<?php 
	global $db;
	$ruangan=$_GET['entity'];
	$kelipatan=getSettings($db, "smis-rs-pengali-aok-" . $ruangan, "1");
	$data=array();
	$data['id_obat']=$_POST['id_obat'];
	$data['nama']=$_POST['nama'];
	$data['satuan']=$_POST['satuan'];
	$data['kategori']=$_POST['kategori'];
	$data['harga']=$_POST['harga']*$kelipatan;
	$data['harga_asli']=$_POST['harga'];
	$data['kelipatan']=$kelipatan;
	$data['connect']=1;
	$up['id_obat']=$_POST['id_obat'];
	$up['satuan']=$_POST['satuan'];
	$dbtable=new DBTable($db, "smis_rwt_aok_".$ruangan);
	$dbtable->setRealDelete(true);
	$result=$dbtable->insertOrUpdate($data, $up);
	echo json_encode($ruangan);
?>