<?php 

global $db;
$ruangan=$_GET['entity'];
$dari=$_POST['dari'];
$sampai=$_POST['sampai'];
if (isset ( $_POST ['command'] )) {
	$dbtable=new DBTable($db, "smis_rwt_tindakan_perawat_".$ruangan);
    $query_view="SELECT nama_tindakan, sum(harga_tindakan*jumlah) as harga, sum(jumlah) as jumlah FROM smis_rwt_tindakan_perawat_".$ruangan;
	$query_count="SELECT count(*) FROM smis_rwt_tindakan_perawat_".$ruangan;
    $dbtable->setPreferredQuery(true,$query_view,$query_count);
    $dbtable->setShowAll(true);
	$dbtable->setUseWhereforView ( true );
	$dbtable->addCustomKriteria(NULL,"waktu>='".$dari."'");
	$dbtable->addCustomKriteria(NULL,"waktu<'".$sampai."'");
	$dbtable->setOrder(" nama_tindakan ASC  ");
    $dbtable->setGroupBy(true," nama_tindakan ");
	if(isset($_POST['carabayar']) && $_POST['carabayar']!=""){
        $dbtable->addCustomKriteria("carabayar"," ='".$_POST['carabayar']."' ");
    }
	$service = new ServiceProvider ( $dbtable );
	$pack = $service->command ( $_POST ['command'] );
	echo json_encode ( $pack );
}

?>