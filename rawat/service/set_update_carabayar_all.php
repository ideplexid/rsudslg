<?php 
global $db;
$id=array("noreg_pasien"=>$_POST["noreg_pasien"]);
$up=array("carabayar"=>$_POST["carabayar"]);

$exist=array("no_register"=>$_POST["noreg_pasien"]);
$dbtable=new DBTable($db,"smis_rwt_antrian_".$_GET['entity']);

if(!$dbtable->is_exist($exist)){
	return;
}

$dbtable->update($up,$exist);

$query=array();
$query[]="smis_rwt_tindakan_perawat";
$query[]="smis_rwt_tindakan_igd";
$query[]="smis_rwt_tindakan_dokter";
$query[]="smis_rwt_alok";
$query[]="smis_rwt_oksigen_central";
$query[]="smis_rwt_oksigen_manual";
$query[]="smis_rwt_visite_dokter";
$query[]="smis_rwt_konsul_dokter";
$query[]="smis_rwt_konsultasi_dokter";
$query[]="smis_rwt_faal_paru";
$query[]="smis_rwt_bronchoscopy";
$query[]="smis_rwt_endoscopy";
$query[]="smis_rwt_spirometry";
$query[]="smis_rwt_audiometry";
$query[]="smis_rwt_bed";
$query[]="smis_rwt_ekg";
$query[]="smis_rwt_vk";
$query[]="smis_rwt_ok";

foreach($query as $q){
	$dbtable->setName($q."_".$_GET['entity']);
	$dbtable->update($up,$id);
}


?>