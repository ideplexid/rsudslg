<?php 
global $db;
show_error();
$noreg = $_POST['noreg_pasien'];
$tutup = $_POST['tutup_tagihan'];
$antrian = new DBTable($db,"smis_rwt_antrian_".$_GET['entity']);
if($tutup=="1"){
    
    $current = $antrian->select(array("no_register"=>$noreg));
    if($current->selesai=="0"){
        require_once "rawat/class/responder/RawatResponder.php";
        require_once "rawat/class/responder/BedResponder.php";
        
        //menutup antrian
        $tutup                       = array();
        $tutup['selesai']            = 1;
        $tutup['waktu_keluar']       = date("Y-m-d H:i:s");
        $tutup['cara_keluar']        = "Dipulangkan Hidup";
        $tutup['keterangan_keluar']  = "DIkeluarkan Oleh Kasir";
        $tutup['dipulangkan_kasir']  = "1";
        
        $idx['no_register']         = $noreg;
        $idx['selesai']             = "0";
        $antrian ->update($tutup,$idx);    
        
        //mengosongkan semua bed yang memakai namanya
        $bed_kamar = new DBTable($db,"smis_rwt_bed_kamar_".$_GET['entity']);
        $bed_kamar->update(array(
            "nama_pasien"=>"",
            "keterangan"=>"",
            "nrm_pasien"=>"",
            "noreg_pasien"=>""
        ),array("noreg_pasien"=>$noreg));

        //mengosongkan Bed
        //TODO 
        
        $bed = new DBTable($db,"smis_rwt_bed_".$_GET['entity']);
        $bed ->addCustomKriteria(" noreg_pasien ","='".$noreg."'");
        $bed ->addCustomKriteria(" waktu_keluar ","='0000-00-00 00:00:00'");
        $bed ->setShowAll(true);
        
        $beds   = $bed ->view("","0");
        $data   = $beds['data'];
        foreach($data as $onebed){
            $uptd["waktu_keluar"] = date("Y-m-d H:i:s");
            $uptd ['hari']      = BedResponder::getDayLength($onebed->waktu_masuk, $uptd["waktu_keluar"],$_GET['entity']);
            $uptd ['biaya']     = $uptd ['hari'] * $onebed->harga;
            $bed->update($uptd,array("id"=>$onebed->id));
        }

        //mengosongkan Oksigen
        $oksigen = new DBTable($db,"smis_rwt_oksigen_manual_".$_GET['entity']);
        $oksigen ->addCustomKriteria(" noreg_pasien ","='".$noreg."'");
        $oksigen ->addCustomKriteria(" waktu_lepas ","='0000-00-00 00:00:00'");
        $oksigen ->setShowAll(true);
        $oksgn   = $oksigen ->view("","0");
     
        $data    = $oksgn['data'];
        
        loadLIbrary("smis-libs-function-time");
        foreach($data as $oks){
            $uptd["waktu_lepas"]=date("Y-m-d H:i:s");
            $minute 			= minute_different($uptd["waktu_lepas"],$oks->waktu_pasang);
            $hour 				= ceil($minute/60);
            $uptd["jumlah_jam"]	= $hour;
            $uptd["harga"]		= $hour*$oks->harga_jam;

            $oksigen->update($uptd,array("id"=>$oks->id));
        }
    }

    

}else{
    //membuka antrian
    $buka                       = array();
    $buka['selesai']            = 0;
    $buka['waktu_keluar']       = "0000-00-00 0000:00";
    $buka['cara_keluar']        = "";
    $buka['keterangan_keluar']  = "";
    $buka['dipulangkan_kasir']  = "0";

    $idx['no_register']         = $noreg;
    $idx['dipulangkan_kasir']   = "1";
    
    $antrian ->update($buka,$idx);
}