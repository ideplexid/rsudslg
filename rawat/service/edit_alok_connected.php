<?php 
show_error();
global $db;
$entity=$_GET['entity'];
require_once "smis-base/smis-include-service-consumer.php";
require_once "rawat/class/responder/RawatResponder.php";
require_once "rawat/class/responder/AlokConnectedResponder.php";
require_once "rawat/class/responder/AlokConnectedServiceResponder.php";

require_once "rawat/class/adapter/TagihanAdapter.php";
$adapter = new TagihanAdapter ($db,$entity);
$adapter ->setAdapterName("alok");

require_once "rawat/class/dbtable/TindakanDBTable.php";
$dbtable = new TindakanDBTable($db, "smis_rwt_alok_".$entity);
$dbtable ->setAutoSynch(true);
$dbtable ->setAdapter($adapter);
$dbtable ->setEntity($entity);
$dbtable ->setJenis("alok");

$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$service=new AlokConnectedServiceResponder($dbtable,$entity);
$data=$service->command($_POST['command']);
echo json_encode($data);
?>