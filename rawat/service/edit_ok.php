<?php 
global $db;
$entity=$_GET['entity'];
require_once "smis-framework/smis/service/ServiceProvider.php";
require_once 'smis-libs-manajemen/ProvitSharingService.php';

require_once "rawat/class/adapter/TagihanOKAdapter.php";
$adapter = new TagihanOKAdapter ($db,$entity);
$adapter ->setAlwaysSHow(true);
$adapter ->setAdapterName("ok");

require_once "rawat/class/dbtable/TindakanDBTable.php";
$dbtable = new TindakanDBTable($db, "smis_rwt_ok_".$entity);
$dbtable ->setAutoSynch(true);
$dbtable ->setAdapter($adapter);
$dbtable ->setEntity($entity);
$dbtable ->setJenis("ok");

$noreg=$_POST['noreg_pasien'];
//$dbtable=new DBTable($db, "smis_rwt_ok_".$entity);
$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$service=new ServiceProvider($dbtable);
$service->setAddEnable(false);
$service->setDelEnable(false);
$data=$service->command($_POST['command']);
echo json_encode($data);

?>