<?php 
    global $db;
    $ruangan=$_GET['entity'];
    $dari=$_POST['dari'];
    $sampai=$_POST['sampai'];
    if (isset ( $_POST ['command'] )) {
        $dbtable=new DBTable($db, "smis_rwt_alok_".$ruangan);
        $query_view="SELECT nama, sum(harga*jumlah) as harga, sum(jumlah) as jumlah FROM smis_rwt_alok_".$ruangan;
        $query_count="SELECT count(*) FROM smis_rwt_alok_".$ruangan;
        $dbtable->setPreferredQuery(true,$query_view,$query_count);
        $dbtable->setShowAll(true);
        $dbtable->setUseWhereforView ( true );
        $dbtable->addCustomKriteria(NULL,"tanggal>='".$dari."'");
        $dbtable->addCustomKriteria(NULL,"tanggal<'".$sampai."'");
        $dbtable->setOrder(" nama ASC  ");
        $dbtable->setGroupBy(true," nama ");
        if(isset($_POST['carabayar']) && $_POST['carabayar']!=""){
            $dbtable->addCustomKriteria("carabayar"," ='".$_POST['carabayar']."' ");
        }
        $service = new ServiceProvider ( $dbtable );
        $pack = $service->command ( $_POST ['command'] );
        echo json_encode ( $pack );
    }
?>