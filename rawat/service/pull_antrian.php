<?php
/**
 * jika pasien salah kamar maka bisa di keluarkan dengan mudah
 * sehingga pasien-pasien yang memang belum di apa-apakan bisa segera dikelularkan 
 * tanpa mendapatkan masalah */

global $db; 
require_once "rawat/class/resource/OutPatientValidator.php";
$entity		  = $_GET['entity'];
$noreg_pasien = $_POST['noreg_pasien'];
$nrm_pasien   = $_POST['nrm_pasien'];
$carakeluar   = isset($_POST['alasan'])?$_POST['alasan']:"Tidak Datang";
$can_out 	  = OutPatientValidator::is_patient_can_out($db,$entity,$noreg_pasien,$nrm_pasien,$carakeluar);
$hasil	 	  = OutPatientValidator::pulang_error_reason($can_out,$nrm,$noreg_pasien);
if($can_out==1){
	$id["no_register"]	= $noreg_pasien;
	$id["selesai"]		= 0;
	$up["selesai"]		= 1;
	$up["cara_keluar"]	= $carakeluar;
	$up["waktu_keluar"]	= date("Y-m-d H:i:s");
	$dbtable->update($up,$id);
}
echo json_encode($hasil);
?>