<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$slug_prototype = $_GET['entity'];
		
		$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_" . $slug_prototype);
		$rows = $dbtable->get_result("
			SELECT nama_tindakan
			FROM smis_rwt_tindakan_perawat_" . $slug_prototype . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$jaspel = 0;
		foreach ($rows as $row) {
			$jaspel_row = $dbtable->get_row("
				SELECT CASE WHEN jaspel IS NULL THEN 0 ELSE jaspel END AS 'jaspel'
				FROM jaspel_keperawatan_2016
				WHERE layanan LIKE '" . $row->nama_tindakan . "'
			");
			$jaspel += $jaspel_row->jaspel;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"	=> $slug_prototype,
			"jaspel" 	=> $jaspel
		);
		echo json_encode($data);
	}
?>