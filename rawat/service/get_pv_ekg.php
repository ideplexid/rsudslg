<?php 
	global $db;
	require_once 'rawat/class/adapter/SalaryAdapter.php';
	require_once 'rawat/class/adapter/EKGAdapter.php';
	$slug=$_GET["entity"];
	$dbtable=new DBTable($db, "smis_rwt_ekg_".$slug);
	$dbtable->setShowAll(true);
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);

	if($_POST['id_karyawan']!="-1"){
		$dbtable->addCustomKriteria(NULL ,  " ( id_dokter_pembaca='".$_POST['id_karyawan']."' OR id_dokter_pengirim='".$_POST['id_karyawan']."') ");
	}
	$res= $dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'")
				  ->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'")
				  ->view("", 0);
	$dlist=$res['data'];
	$adapter=new EKGAdapter($_POST['id_karyawan'], $slug,EKGAdapter::$TYPE_INDIVIDU);
	$result=$adapter->getContent($dlist);
	echo json_encode($result);
?>