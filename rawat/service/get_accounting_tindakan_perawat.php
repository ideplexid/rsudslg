<?php 
    global $db;
    $id      = $_POST['data'];
    $entity  = $_GET['entity'];
    $ename   = ArrayAdapter::format("unslug",$_GET['entity']);
    
    $dbtable = new DBTable($db,"smis_rwt_tindakan_perawat_".$entity);
    $x = $dbtable ->selectEventDel($id);
    $acc_ename = $entity."-".$x->carabayar;
    
    //content untuk debit
    $debet=array();
    $debet['akun']    = getSettings($db,"smis-rs-acc-k-tindakan-perawat-".$acc_ename,"");
    $debet['debet']   = 0;
    $debet['kredit']  = $x->harga_tindakan;
    $debet['ket']     = "Pendapatan Tindakan Perawat ".$ename." - ".$x->nama_tindakan." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']    = "kredit-tindakan-perawat-".$acc_ename."-".$x->id;

    //content untuk kredit
    $kredit=array();
    $kredit['akun']    = getSettings($db,"smis-rs-acc-d-tindakan-perawat-".$acc_ename,"");
    $kredit['debet']   = $x->harga_tindakan;
    $kredit['kredit']  = 0;
    $kredit['ket']     = "Piutang Tindakan Perawat ".$ename." - ".$x->nama_tindakan." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']    = "debet-tindakan-perawat-".$acc_ename."-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Tindakan Perawat Pasien ".$ename." - ".$x->nama_tindakan." - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $header['code']         = "tindakan-perawat-".$acc_ename."-".$x->id;
    $header['nomor']        = "TPRW-".$ename."-".$x->id;
    $header['debet']        = $x->harga_tindakan;
    $header['kredit']       = $x->harga_tindakan;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;
    
    $final=array();
    $final[]=$transaction_karcis;
    
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>