<?php 
    global $db;
    $ruangan=$_GET['entity'];
    $dari=$_POST['dari'];
    $sampai=$_POST['sampai'];
    if (isset ( $_POST ['command'] )) {
        $dbtable=new DBTable($db, "smis_rwt_ok_".$ruangan);
        $query_view="SELECT nama_tindakan, sum(harga_operator_satu
                                                +harga_operator_dua
                                                +harga_asisten_operator_satu
                                                +harga_asisten_operator_dua
                                                +harga_anastesi
                                                +harga_asisten_anastesi
                                                +harga_asisten_anastesi_dua
                                                +harga_team_ok
                                                +harga_bidan
                                                +harga_bidan_dua
                                                +harga_perawat
                                                +harga_sewa_kamar
                                                +harga_sewa_alat
                                                +harga_oomloop_satu
                                                +harga_oomloop_dua
                                                +harga_instrument
                                                +harga_instrument_dua) as harga, count(*) as jumlah FROM smis_rwt_ok_".$ruangan;
        $query_count="SELECT count(*) FROM smis_rwt_ok_".$ruangan;
        $dbtable->setPreferredQuery(true,$query_view,$query_count);
        $dbtable->setShowAll(true);
        $dbtable->setUseWhereforView ( true );
        $dbtable->addCustomKriteria(NULL,"waktu>='".$dari."'");
        $dbtable->addCustomKriteria(NULL,"waktu<'".$sampai."'");
        $dbtable->setOrder(" nama_tindakan ASC  ");
        $dbtable->setGroupBy(true," nama_tindakan ");
        if(isset($_POST['carabayar']) && $_POST['carabayar']!=""){
            $dbtable->addCustomKriteria("carabayar"," ='".$_POST['carabayar']."' ");
        }
        $service = new ServiceProvider ( $dbtable );
        $pack = $service->command ( $_POST ['command'] );
        echo json_encode ( $pack );
    }
?>