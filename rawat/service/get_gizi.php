<?php
global $db;
$polislug = $_GET ['entity'];
$use_gizi = getSettings ( $db, "smis-rs-use-gizi-" . $polislug, "0" )=="1";
if ($use_gizi) {
	$dbtable = new DBTable ( $db, "smis_rwt_vgizi_" . $polislug );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$list = $data ['data'];
	echo json_encode ( $list );
} else {
	echo json_encode ( array () );
}
?>
