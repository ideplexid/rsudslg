<?php
	global $db;
	
	/// param			: noreg_pasien
	/// json response	: [0].ruangan, [0].jasa_dokter
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$slug_rawat = $_GET['entity'];
		
		$id_dokter_filter = "";
		if (isset($_POST['id_dokter_csv'])) {
			$id_dokter_csv = $_POST['id_dokter_csv'];
			$id_dokter_filter = " AND id_dokter IN (" . $id_dokter_csv . ")";
		}
		
		// konsultasi dokter :
		$dbtable = new DBTable($db, "smis_rwt_konsultasi_dokter_" . $slug_rawat);
		$konsultasi_row = $dbtable->get_row("
			SELECT SUM(harga) AS 'jasa_dokter'
			FROM smis_rwt_konsultasi_dokter_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' " . $id_dokter_filter . "
		");
		// konsul dokter :
		$dbtable = new DBTable($db, "smis_rwt_konsul_dokter_" . $slug_rawat);
		$konsul_row = $dbtable->get_row("
			SELECT SUM(harga) AS 'jasa_dokter'
			FROM smis_rwt_konsul_dokter_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' " . $id_dokter_filter . "
		");
		// visite dokter :
		$dbtable = new DBTable($db, "smis_rwt_visite_dokter_" . $slug_rawat);
		$visite_row = $dbtable->get_row("
			SELECT SUM(harga) AS 'jasa_dokter'
			FROM smis_rwt_visite_dokter_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' " . $id_dokter_filter . "
		");
		// tindakan dokter :
		$dbtable = new DBTable($db, "smis_rwt_tindakan_dokter_" . $slug_rawat);
		$tindakan_row = $dbtable->get_row("
			SELECT SUM(harga) AS 'jasa_dokter'
			FROM smis_rwt_tindakan_dokter_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' " . $id_dokter_filter . "
		");
		$jasa_dokter = 0;
		if ($konsultasi_row != null) {
			$jasa_dokter += $konsultasi_row->jasa_dokter;
		}
		if ($konsul_row != null) {
			$jasa_dokter += $konsul_row->jasa_dokter;
		}
		if ($visite_row != null) {
			$jasa_dokter += $visite_row->jasa_dokter;
		}
		if ($tindakan_row != null) {
			$jasa_dokter += $tindakan_row->jasa_dokter;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"		=> $slug_rawat,
			"jasa_dokter"	=> $jasa_dokter
		);
		echo json_encode($data);
	}
?>