<?php
    global $db;
    $polislug                   = $_GET['entity'];
    $noreg_pasien               = $_POST['noreg_pasien'];
    $dbtable                    = new DBTable($db,"smis_rwt_antrian_".$polislug);
    $row                        = $dbtable ->select(array("no_register"=>$noreg_pasien,"selesai"=>"1"));
    $result                     = array();
    $result['cara_keluar']      = "";
    $result['waktu_keluar']     = "";
    if($row!=null){
        $result['cara_keluar']  = $row->cara_keluar;
        $result['waktu_keluar'] = $row->waktu_keluar;    
    }
    echo json_encode($result);
?>