<?php 
    global $db;
    $id      = $_POST['data'];
    $entity  = $_GET['entity'];
    $ename   = ArrayAdapter::format("unslug",$_GET['entity']);
    
    $dbtable = new DBTable($db,"smis_rwt_konsultasi_dokter_".$entity);
    $x = $dbtable ->selectEventDel($id);
    $acc_ename = $entity."-".$x->carabayar;
    
    $total_harga = $x->harga+$x->harga_perawat;
    //content untuk debit
    $debet=array();
    $debet['akun']    = getSettings($db,"smis-rs-acc-k-periksa-dokter-".$acc_ename,"");
    $debet['debet']   = 0;
    $debet['kredit']  = $total_harga;;
    $debet['ket']     = "Pendapatan Periksa Dokter ".$ename." - ".$x->nama_dokter." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']    = "kredit-periksa-dokter-".$acc_ename."-".$x->id;

    //content untuk kredit
    $kredit=array();
    $kredit['akun']    = getSettings($db,"smis-rs-acc-d-periksa-dokter-".$acc_ename,"");
    $kredit['debet']   = $x->harga;
    $kredit['kredit']  = 0;
    $kredit['ket']     = "Piutang Periksa Dokter ".$ename." - ".$x->nama_dokter." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']    = "debet-periksa-dokter-".$acc_ename."-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Periksa Dokter Pasien ".$ename." - ".$x->nama_dokter." - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $header['code']         = "periksa-dokter-".$acc_ename."-".$x->id;
    $header['nomor']        = "PRK-".$ename."-".$x->id;
    $header['debet']        = $x->harga;
    $header['kredit']       = $x->harga;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;
    
    $final=array();
    $final[]=$transaction_karcis;
    
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
    
?>