<?php
	global $db;
	
	if (isset($_POST['id_obat'])) {
		$id_obat = $_POST['id_obat'];
		$prototype_slug = $_GET['entity'];
		$dbtable = new DBTable($db, "smis_rwt_stok_obat_" . $prototype_slug);
		$jumlah = 0;
		$row = $dbtable->get_row("
			SELECT SUM(sisa) AS 'jumlah'
			FROM smis_rwt_stok_obat_" . $prototype_slug . "
			WHERE prop NOT LIKE 'del' AND konversi = 1 AND id_obat = '" . $id_obat . "'
		");
		if ($row != null)
			$jumlah += $row->jumlah;
		$row = $dbtable->get_row("
			SELECT CASE WHEN SUM(sisa * konversi) IS NULL THEN 0 ELSE SUM(sisa * konversi) END AS 'jumlah'
			FROM smis_rwt_stok_obat_" . $prototype_slug . "
			WHERE prop NOT LIKE 'del' AND konversi > 1 AND id_obat = '" . $id_obat . "'
		");
		if ($row != null)
			$jumlah += $row->jumlah;
		$data = array();
		$data['data'] = array(
			"ruangan"	=> $prototype_slug,
			"jumlah"	=> $jumlah
		);
		echo json_encode($data);
	}
?>