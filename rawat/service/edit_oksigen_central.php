<?php 
global $db;
$entity=$_GET['entity'];
require_once "smis-framework/smis/service/ServiceProvider.php";
require_once "rawat/class/service_provider/OksigenCentralServiceProvider.php";

require_once "rawat/class/adapter/TagihanAdapter.php";
$adapter = new TagihanAdapter ($db,$entity);
$adapter ->setAdapterName("oksigen_central");

require_once "rawat/class/dbtable/TindakanDBTable.php";
$dbtable = new TindakanDBTable($db, "smis_rwt_oksigen_central_".$entity);
$dbtable ->setAutoSynch(true);
$dbtable ->setAdapter($adapter);
$dbtable ->setEntity($entity);
$dbtable ->setJenis("oksigen_central");

//$dbtable=new DBTable($db, "smis_rwt_oksigen_central_".$entity);
$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
$service=new OksigenCentralServiceProvider($dbtable,$entity);
$data=$service->command($_POST['command']);
echo json_encode($data);
?>