<?php
	global $db;
	
	/// param			: noreg_pasien
	/// json response	: [0].ruangan, [0].alok
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$slug_rawat = $_GET['entity'];
		
		$dbtable = new DBTable($db, "smis_rwt_alok_" . $slug_rawat);
		$row = $dbtable->get_row("
			SELECT SUM(jumlah * harga) AS 'alok'
			FROM smis_rwt_alok_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$alok = 0;
		if ($row != null) {
			$alok += $row->alok;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"	=> $slug_rawat,
			"alok"		=> $alok
		);
		echo json_encode($data);
	}
?>