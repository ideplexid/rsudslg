<?php
global $db;
global $user;
$asal 		= isset($_POST['asal'])?$_POST['asal']:"Pendaftaran";
if($asal=="Pendaftaran"){
	$dbtable = new DBTable($db,"smis_rwt_antrian_".$_GET['entity']);
	$ex = array("no_register"=>$_POST['no_register']);
	if($dbtable->is_exist($ex)){
		/** pasien ini sudah ada di dalam sistem rawat , tidak bisa di daftarkan lagi dari pendaftaran 
		 * kalau mengembalikan harus dari ruangan.
		 */
		$one = $dbtable->select($ex);
		$result = array();
		$result['content']=array();
		$result['content']['nomor']=$one->nomor;
		echo json_encode($result);
		return;
	}
}

require_once ("rawat/class/template/Antrian.php");
require_once ("smis-base/smis-include-service-consumer.php");
$dbtable=new DBTable($db, "smis_adm_prototype");
$id=array("slug"=>$_GET ['entity']);
$pro=$dbtable->select($id);
$nama=$pro->nama;
$is_change = getSettings($db, "smis-rs-update-urji-".$_GET ['entity'], "0");
$urji      = getSettings($db, "smis-rs-update-urji-set-".$_GET ['entity'], "-1");
if($is_change=="1" && $urji!="-1" && $urji!=-1){
	$ser=new ServiceConsumer($db, "set_uri",NULL,"registration");
	$ser->addData("noreg_pasien", $_POST['no_register']);
	$ser->addData("urji", $urji);
	$ser->addData("tanggal", $_POST['waktu']);
	$ser->addData("nama_dokter", isset($_POST['nama_dokter'])?$_POST['nama_dokter']:"");
	$ser->addData("id_dokter", isset($_POST['id_dokter'])?$_POST['id_dokter']:"");
	$ser->addData("tujuan", $_GET ['entity']);
	$ser->execute();
}
$antri = new AntrianTemplate ( $db, $_GET ['entity'], $nama );
$antri->initialize ();

/**update di rekam medis */
$polislug 	= $_GET ['entity'];
$nama 		= $_POST['nama_pasien'];
$noreg 		= $_POST['no_register'];
$nrm 		= $_POST['nrm_pasien'];
$petugas 	= $user->getNameOnly();
$asal 		= isset($_POST['asal'])?$_POST['asal']:"Pendaftaran";
$keterangan = "Pasien Masuk Melalui Push Antrian";
require_once "rawat/function/in_log.php";
in_log($db,$polislug,$nama,$noreg,$nrm,$petugas,$asal,$keterangan);

/**update di pendaftaran */
$datax = array();
$datax['noreg_pasien'] 		= $_POST['no_register'];
$datax['last_ruangan'] 		= $_GET ['entity'];
$datax['last_nama_ruangan'] = get_entity_name($db,$_GET ['entity']);;
$datax['last_bed'] 	   		= isset($_POST['bed_kamar']) && $_POST['bed_kamar']!=""?$_POST['bed_kamar']:"";
$datax['last_kelas']   		= getSettings($db, "smis-rs-kelas-" . $_GET['entity'], "");
$serv  				   		= new ServiceConsumer($db,"update_last_ruangan",$datax,"registration");
$serv->execute();

$datax = array();
$datax['noreg_pasien'] 		= $_POST['no_register'];
$datax['jenis_kegiatan']	= getSettings($db, "smis-rs-rl52-default-" . $_GET['entity'], "");
$serv  				   		= new ServiceConsumer($db,"update_jenis_kegiatan",$datax,"registration");
$serv->execute();