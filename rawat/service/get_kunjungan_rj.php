<?php 
global $db;
$tahun  = $_POST['tahun'];
$entity = $_GET['entity'];
$query  = "select '' AS id,
        year(waktu) AS tahun,
        sum(if((month(waktu) = 1),1,0)) AS jan,
        sum(if((month(waktu) = 2),1,0)) AS feb,
        sum(if((month(waktu) = 3),1,0)) AS mar,
        sum(if((month(waktu) = 4),1,0)) AS apr,
        sum(if((month(waktu) = 5),1,0)) AS mei,
        sum(if((month(waktu) = 6),1,0)) AS jun,
        sum(if((month(waktu) = 7),1,0)) AS jul,
        sum(if((month(waktu) = 8),1,0)) AS ags,
        sum(if((month(waktu) = 9),1,0)) AS sep,
        sum(if((month(waktu) = 10),1,0)) AS okt,
        sum(if((month(waktu) = 11),1,0)) AS nov,
        sum(if((month(waktu) = 12),1,0)) AS des,
        sum(if((year(waktu) = '".$tahun."'),1,0)) AS total,
        '' AS prop from smis_rwt_antrian_".$entity." 
        where (prop != 'del') 
        AND (year(waktu) = '".$tahun."')
        group by year(waktu)
        ";
$result = $db->get_result($query);

$array=array();
foreach($result as $key => $value) {
    foreach($value as $x => $y) {
        $array[$x] = $y;
    }
}

$array['ruangan']=$entity;
$array['urji']=getSettings($db,"smis-rs-urjip-" . $entity,"");
echo json_encode($array);

?>