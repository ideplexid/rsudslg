<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$slug_rawat = $_GET['entity'];
		
		$dbtable = new DBTable($db, "smis_rwt_ok_" . $slug_rawat);
		$row = $dbtable->get_row("
			SELECT SUM(harga_operator_satu) AS 'jasa_operator_1', 
				   SUM(harga_operator_dua) AS 'jasa_operator_2', 
				   SUM(harga_asisten_operator_satu) AS 'jasa_ast_operator_1',
				   SUM(harga_asisten_operator_dua) AS 'jasa_ast_operator_2',
				   SUM(harga_anastesi) AS 'jasa_anastesi',
				   SUM(harga_asisten_anastesi) AS 'jasa_ast_anastesi_1',
				   SUM(harga_asisten_anastesi_dua) AS 'jasa_ast_anastesi_2',
				   SUM(harga_team_ok) AS 'jasa_team_ok',
				   SUM(harga_bidan) AS 'jasa_bidan_1',
				   SUM(harga_bidan_dua) AS 'jasa_bidan_2',
				   SUM(harga_perawat) AS 'jasa_perawat',
				   SUM(harga_sewa_kamar) AS 'harga_operasi',
                   SUM(harga_sewa_alat) AS 'harga_alat',
				   SUM(harga_oomloop_satu) AS 'jasa_oomploop_1',
				   SUM(harga_oomloop_dua) AS 'jasa_oomploop_2',
				   SUM(harga_instrument) AS 'jasa_instrument_1',
				   SUM(harga_instrument_dua) AS 'jasa_instrument_2',
				   SUM(recovery_room) AS 'rr'
			FROM smis_rwt_ok_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$jasa_operator_1 = 0;
		$jasa_operator_2 = 0;
		$jasa_ast_operator_1 = 0;
		$jasa_ast_operator_2 = 0;
		$jasa_anastesi = 0;
		$jasa_ast_anastesi_1 = 0;
		$jasa_ast_anastesi_2 = 0;
		$jasa_team_ok = 0;
		$jasa_bidan_1 = 0;
		$jasa_bidan_2 = 0;
		$jasa_perawat = 0;
		$harga_operasi = 0;
        $harga_alat = 0;
		$jasa_oomploop_1 = 0;
		$jasa_oomploop_2 = 0;
		$jasa_instrument_1 = 0;
		$jasa_instrument_2 = 0;
		$rr = 0;
		if ($row != null) {
			$jasa_operator_1 += $row->jasa_operator_1;
			$jasa_operator_2 += $row->jasa_operator_2;
			$jasa_ast_operator_1 += $row->jasa_ast_operator_1;
			$jasa_ast_operator_2 += $row->jasa_ast_operator_2;
			$jasa_anastesi += $row->jasa_anastesi;
			$jasa_ast_anastesi_1 += $row->jasa_ast_anastesi_1;
			$jasa_ast_anastesi_2 += $row->jasa_ast_anastesi_2;
			$jasa_team_ok += $row->jasa_team_ok;
			$jasa_bidan_1 += $row->jasa_bidan_1;
			$jasa_bidan_2 += $row->jasa_bidan_2;
			$jasa_perawat += $row->jasa_perawat;
			$harga_operasi += $row->harga_operasi;
            $harga_alat += $row->harga_alat;
			$jasa_oomploop_1 += $row->jasa_oomploop_1;
			$jasa_oomploop_2 += $row->jasa_oomploop_2;
			$jasa_instrument_1 += $row->jasa_instrument_1;
			$jasa_instrument_2 += $row->jasa_instrument_2;
			$rr += $row->rr;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"				=> $slug_rawat,
			"jasa_operator_1"		=> $jasa_operator_1,
			"jasa_operator_2"		=> $jasa_operator_2,
			"jasa_ast_operator_1"	=> $jasa_ast_operator_1,
			"jasa_ast_operator_2"	=> $jasa_ast_operator_2,
			"jasa_anastesi"			=> $jasa_anastesi,
			"jasa_ast_anastesi_1"	=> $jasa_ast_anastesi_1,
			"jasa_ast_anastesi_2"	=> $jasa_ast_anastesi_2,
			"jasa_team_ok"			=> $jasa_team_ok,
			"jasa_bidan_1"			=> $jasa_bidan_1,
			"jasa_bidan_2"			=> $jasa_bidan_2,
			"jasa_perawat"			=> $jasa_perawat,
			"harga_operasi"			=> $harga_operasi,
            "harga_alat"			=> $harga_alat,
			"jasa_oomploop_1"		=> $jasa_oomploop_1,
			"jasa_oomploop_2"		=> $jasa_oomploop_2,
			"jasa_instrument_1"		=> $jasa_instrument_1,
			"jasa_instrument_2"		=> $jasa_instrument_2,
			"rr"					=> $rr
		);
		echo json_encode($data);
	}
?>