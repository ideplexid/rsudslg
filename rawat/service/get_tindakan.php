<?php 

/**
 * using for get the list of Doctor Treatment
 * used for Resume Medic of each patient on each Number of Registration
 * 
 * @database : smis_rwt_tindakan_dokter_%
 * @used	 : - medical_record/modul/resume_medis.php
 * 			   - rawat/class/template/ResumeMedis.php
 * @author 	 : Nurul Huda
 * @copyright: goblooge@gmail.com
 * @license  : LGPLv3
 * @since	 : 14 may 2015
 * @version	 : 1.0.1
 * */

	$slug = $_GET["entity"];
	$dbtable = new DBTable($db, "smis_rwt_tindakan_dokter_" . $slug);
	if(isset($_POST['noreg_pasien']) && $_POST['noreg_pasien'] != '') {
		$dbtable->setOrder("kode_icd , nama_dokter, nama_pasien ");
		$dbtable->setUseWhereforView(true);
		$dbtable->addCustomKriteria("noreg_pasien", "='".$_POST['noreg_pasien']."'");
		$dbtable->setShowAll(true);
	} else {
		$query_value = "
			SELECT a.*, b.waktu_register
			FROM smis_rwt_tindakan_dokter_" . $slug . " a LEFT JOIN smis_rwt_antrian_" . $slug. " b ON a.noreg_pasien = b.no_register
			WHERE a.prop = '' AND b.prop = ''
			ORDER BY b.waktu_register DESC, a.noreg_pasien ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
	}
	$service = new ServiceProvider($dbtable);
	$pack = $service->command($_POST['command']);
	echo json_encode($pack);
?>