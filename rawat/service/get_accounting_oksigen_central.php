<?php 
    global $db;
    $id      = $_POST['data'];
    $entity  = $_GET['entity'];
    $ename   = ArrayAdapter::format("unslug",$_GET['entity']);
    
    $dbtable = new DBTable($db,"smis_rwt_oksigen_central_".$entity);
    $x = $dbtable ->selectEventDel($id);
    $acc_ename = $entity."-".$x->carabayar;
    //content untuk debit
    $debet=array();
    $debet['akun']    = getSettings($db,"smis-rs-acc-k-oksigen-central-".$acc_ename,"");
    $debet['debet']   = 0;
    $debet['kredit']  = $x->harga;
    $debet['ket']     = "Pendapatan Oksigen Central ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']    = "kredit-oksigen-central-".$acc_ename."-".$x->id;

    //content untuk kredit
    $kredit=array();
    $kredit['akun']    = getSettings($db,"smis-rs-acc-d-oksigen-central-".$acc_ename,"");
    $kredit['debet']   = $x->harga;
    $kredit['kredit']  = 0;
    $kredit['ket']     = "Piutang Oksigen Central ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']    = "debet-oksigen-central-".$acc_ename."-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->selesai;
    $header['keterangan']   = "Oksigen Central Pasien ".$ename." - ".$x->skala." (".ArrayAdapter::format("date d M Y H:i",$x->mulai)." s/d ".ArrayAdapter::format("date d M Y H:i",$x->selesai)." ) - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $header['code']         = "oksigen-central-".$acc_ename."-".$x->id;
    $header['nomor']        = "OC-".$ename."-".$x->id;
    $header['debet']        = $x->harga;
    $header['kredit']       = $x->harga;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;
    
    $final=array();
    $final[]=$transaction_karcis;
    
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>