<?php
$entity=$_GET["entity"];
global $db;
if (isset ( $_POST ['command'] )) {
    require_once "rawat/class/responder/RawatResponder.php";
	require_once "rawat/class/responder/AokConnectedResponder.php";
	require_once "rawat/class/responder/AokConnectedServiceResponder.php";
    require_once "smis-base/smis-include-service-consumer.php";
	$dbtable = new DBTable ( $db, "smis_rwt_aok_".$entity );
	$dbtable->setOrder ( " nama ASC " );
	$service = new AokConnectedServiceResponder ( $dbtable,$entity );
	$pack = $service->command ( $_POST ['command'] );
	echo json_encode ( $pack );
}
?>