<?php
	global $db;
	
	/// param			: noreg_pasien, (opt) not_in_elements, (opt) not_in_conjunction, (opt) in_elements, (opt) in_conjunction
	/// json response	: [0].ruangan, [0].jasa_keperawatan
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$slug_rawat = $_GET['entity'];
		$filter = "";
		if (isset($_POST['not_in_elements'])) {
			$not_in_elements = $_POST['not_in_elements'];
			$filter = " AND (";
			foreach ($not_in_elements as $nie) {
				$filter .= $nie . " AND ";
			}
			$filter = rtrim($filter, " AND ") . ") ";
		}
		if (isset($_POST['in_elements'])) {
			$in_elements = $_POST['in_elements'];
			$filter = " AND (";
			foreach ($in_elements as $ie) {
				$filter .= $ie . " OR ";
			}
			$filter = rtrim($filter, " OR ") . ") ";
		}
		
		// tindakan perawat :
		$dbtable = new DBTable($db, "smis_rwt_tindakan_perawat_" . $slug_rawat);
		$tindakan_perawat_row = $dbtable->get_row("
			SELECT SUM(harga_tindakan) AS 'jasa_keperawatan'
			FROM smis_rwt_tindakan_perawat_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' " . $filter . "
		");
		// bagian tindakan dokter :
		$dbtable = new DBTable($db, "smis_rwt_tindakan_dokter_" . $slug_rawat);
		$tindakan_dokter_row = $dbtable->get_row("
			SELECT SUM(harga_perawat) AS 'jasa_keperawatan'
			FROM smis_rwt_tindakan_dokter_" . $slug_rawat . "
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' " . $filter . "
		");
		$jasa_keperawatan = 0;
		if ($tindakan_perawat_row != null) {
			$jasa_keperawatan += $tindakan_perawat_row->jasa_keperawatan;
		}
		if ($tindakan_dokter_row != null) {
			$jasa_keperawatan += $tindakan_dokter_row->jasa_keperawatan;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"			=> $slug_rawat,
			"jasa_keperawatan"	=> $jasa_keperawatan
		);
		echo json_encode($data);
	}
?>