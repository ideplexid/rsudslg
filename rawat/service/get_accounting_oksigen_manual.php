<?php 
    global $db;
    $id      = $_POST['data'];
    $entity  = $_GET['entity'];
    $ename   = ArrayAdapter::format("unslug",$_GET['entity']);
    
    $dbtable = new DBTable($db,"smis_rwt_oksigen_manual_".$entity);
    $x = $dbtable ->selectEventDel($id);
    $acc_ename = $entity."-".$x->carabayar;
    //content untuk debit
    $debet=array();
    $debet['akun']    = getSettings($db,"smis-rs-acc-k-oksigen-manual-".$acc_ename,"");
    $debet['debet']   = 0;
    $debet['kredit']  = $x->harga;
    $debet['ket']     = "Pendapatan Oksigen Manual ".$ename." - ".$x->nama_petugas." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']    = "kredit-oksigen-manual-".$acc_ename."-".$x->id;

    //content untuk kredit
    $kredit=array();
    $kredit['akun']    = getSettings($db,"smis-rs-acc-d-oksigen-manual-".$acc_ename,"");
    $kredit['debet']   = $x->harga;
    $kredit['kredit']  = 0;
    $kredit['ket']     = "Piutang Oksigen Manual ".$ename." - ".$x->skala." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']    = "debet-oksigen-manual-".$acc_ename."-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Oksigen Manual Pasien ".$ename." - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $header['code']         = "oksigen-manual-".$acc_ename."-".$x->id;
    $header['nomor']        = "OM-".$ename."-".$x->id;
    $header['debet']        = $x->harga;
    $header['kredit']       = $x->harga;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;
    
    $final=array();
    $final[]=$transaction_karcis;
    
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
    
?>