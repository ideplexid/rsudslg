<?php 
    global $db;
    $id      = $_POST['data'];
    $entity  = $_GET['entity'];
    $ename   = ArrayAdapter::format("unslug",$_GET['entity']);
    
    $dbtable = new DBTable($db,"smis_rwt_audiometry_".$entity);
    $x = $dbtable ->selectEventDel($id);
    $acc_ename = $entity."-".$x->carabayar;
    
    //content untuk debit
    $debet=array();
    $debet['akun']     = getSettings($db,"smis-rs-acc-k-audiometry-".$acc_ename,"");
    $debet['debet']    = 0;
    $debet['kredit']   = $x->biaya;
    $debet['ket']      = "Pendapatan Audiometry ".$ename." - ".$x->nama_dokter." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $debet['code']     = "kredit-audiometry-".$acc_ename."-".$x->id;

    //content untuk kredit
    $kredit=array();
    $kredit['akun']    = getSettings($db,"smis-rs-acc-d-audiometry-".$acc_ename,"");
    $kredit['debet']   = $x->biaya;
    $kredit['kredit']  = 0;
    $kredit['ket']     = "Piutang Audiometry ".$ename." - ".$x->nama_dokter." - pasien ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $kredit['code']    = "debet-audiometry-".$acc_ename."-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Audiometry Pasien ".$ename." - ".$x->nama_dokter." - ".$x->nama_pasien." dengan No.Reg ".$x->noreg_pasien;
    $header['code']         = "audiometry-".$acc_ename."-".$x->id;
    $header['nomor']        = "ADT-".$ename."-".$x->id;
    $header['debet']        = $x->biaya;
    $header['kredit']       = $x->biaya;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;
    
    $final=array();
    $final[]=$transaction_karcis;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>