<?php 

global $wpdb;
global $NUMBER;
global $db;
global $RWT_SLUG;

require_once 'smis-libs-inventory/install.php';
$proto=getAllActivePrototypeOf($db, "rawat");
foreach($proto as $slug=>$name){
    $RWT_SLUG=$slug;
    
    require "rawat/resource/install/table/smis_rwt_alok_.php";
    require "rawat/resource/install/table/smis_rwt_antrian_.php";
    require "rawat/resource/install/table/smis_rwt_aok_.php";
    require "rawat/resource/install/table/smis_rwt_audiometry_.php";
    require "rawat/resource/install/table/smis_rwt_bed_.php";
    require "rawat/resource/install/table/smis_rwt_bed_kamar_.php";
    require "rawat/resource/install/table/smis_rwt_bronchoscopy_.php";
    require "rawat/resource/install/table/smis_rwt_ekg_.php";
    require "rawat/resource/install/table/smis_rwt_endoscopy_.php";
    require "rawat/resource/install/table/smis_rwt_faal_paru_.php";
    require "rawat/resource/install/table/smis_rwt_fisiotherapy_.php";
    require "rawat/resource/install/table/smis_rwt_gizi_.php";
    require "rawat/resource/install/table/smis_rwt_konsul_dokter_.php";
    require "rawat/resource/install/table/smis_rwt_konsultasi_dokter_.php";
    require "rawat/resource/install/table/smis_rwt_ok_.php";
    require "rawat/resource/install/table/smis_rwt_oksigen_central_.php";
    require "rawat/resource/install/table/smis_rwt_oksigen_manual_.php";
    require "rawat/resource/install/table/smis_rwt_rr_.php";
    require "rawat/resource/install/table/smis_rwt_rujukan_.php";
    require "rawat/resource/install/table/smis_rwt_spirometry_.php";
    require "rawat/resource/install/table/smis_rwt_tindakan_dokter_.php";
    require "rawat/resource/install/table/smis_rwt_tindakan_igd_.php";
    require "rawat/resource/install/table/smis_rwt_tindakan_perawat_.php";
    require "rawat/resource/install/table/smis_rwt_visite_dokter_.php";
    require "rawat/resource/install/table/smis_rwt_vk_.php"; 
    require "rawat/resource/install/table/smis_rwt_in_log_.php"; 
    require "rawat/resource/install/view/smis_rwt_vgizi_.php";
    
    
	$inventory=new InventoryInstallator($wpdb, $slug, "");
	$inventory->extendInstall("rwt");
	$inventory->install();
}

?>