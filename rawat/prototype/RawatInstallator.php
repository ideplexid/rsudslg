<?php 

require_once ("smis-framework/smis/template/InstallatorTemplate.php");
class RawatInstallator extends InstallatorTemplate {
	public function __construct($db, $slug, $code) {
		parent::__construct ( $db, $slug, $code );
	}
	public function init($params) {
        global $RWT_SLUG;
        global $wpdb;
        $RWT_SLUG=$this->slug;
        
        require_once "rawat/resource/install/table/smis_rwt_alok_.php";
        require_once "rawat/resource/install/table/smis_rwt_antrian_.php";
        require_once "rawat/resource/install/table/smis_rwt_aok_.php";
        require_once "rawat/resource/install/table/smis_rwt_audiometry_.php";
        require_once "rawat/resource/install/table/smis_rwt_bed_.php";
        require_once "rawat/resource/install/table/smis_rwt_bed_kamar_.php";
        require_once "rawat/resource/install/table/smis_rwt_bronchoscopy_.php";
        require_once "rawat/resource/install/table/smis_rwt_ekg_.php";
        require_once "rawat/resource/install/table/smis_rwt_endoscopy_.php";
        require_once "rawat/resource/install/table/smis_rwt_faal_paru_.php";
        require_once "rawat/resource/install/table/smis_rwt_fisiotherapy_.php";
        require_once "rawat/resource/install/table/smis_rwt_gizi_.php";
        require_once "rawat/resource/install/table/smis_rwt_konsul_dokter_.php";
        require_once "rawat/resource/install/table/smis_rwt_konsultasi_dokter_.php";
        require_once "rawat/resource/install/table/smis_rwt_ok_.php";
        require_once "rawat/resource/install/table/smis_rwt_oksigen_central_.php";
        require_once "rawat/resource/install/table/smis_rwt_oksigen_manual_.php";
        require_once "rawat/resource/install/table/smis_rwt_rr_.php";
        require_once "rawat/resource/install/table/smis_rwt_rujukan_.php";
        require_once "rawat/resource/install/table/smis_rwt_spirometry_.php";
        require_once "rawat/resource/install/table/smis_rwt_tindakan_dokter_.php";
        require_once "rawat/resource/install/table/smis_rwt_tindakan_igd_.php";
        require_once "rawat/resource/install/table/smis_rwt_tindakan_perawat_.php";
        require_once "rawat/resource/install/table/smis_rwt_visite_dokter_.php";
        require_once "rawat/resource/install/table/smis_rwt_vk_.php"; 

        require_once "rawat/resource/install/view/smis_rwt_vgizi_.php";        
	}
}


?>