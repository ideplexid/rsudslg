<?php 
require_once ("smis-framework/smis/template/NavigatorTemplate.php");
class RawatNavigator extends NavigatorTemplate {
	public function __construct($navigation, $tooltip, $name, $page) {
		parent::__construct ( $navigation, $tooltip, $name, $page,"fa fa-hospital-o" );
	}
	public function topMenu() {
		$this->addPrototype ( $this->name, $this->page, "rawat", "Antrian of " . $this->name, "Antrian", "antrian" ,"fa fa-sort-alpha-asc");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Diagnosa of " . $this->name, "Diagnosa", "antriandiagnosa" ,"fa fa-paperclip");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Layanan of " . $this->name, "Layanan", "layanan" ,"fa fa-stethoscope");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Status Operasi of " . $this->name, "Status Operasi", "status_operasi" ,"fa fa-sort-alpha-asc");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Gizi of " . $this->name, "Gizi", "gizi","fa fa-coffee" );
		$this->addPrototype ( $this->name, $this->page, "rawat", "History of " . $this->name, "Riwayat", "riwayat" ,"fa fa-user-md");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Daftar " . $this->name, "Daftar", "daftar" ,"fa fa-list-alt");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Settings " . $this->name, "Settings", "setting_layanan" ,"fa fa-gears");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Laporan Pendapatan" . $this->name, "Laporan Pendapatan", "laporan_pendapatan" ,"fa fa-book");
		$this->addPrototype ( $this->name, $this->page, "rawat", "Rekapitulasi Stok Obat" . $this->name, "Rekap Stok Obat", "rekap_stok_depo" ,"fa fa-list-alt");
		global $db;
		if(getSettings($db,"smis-rs-laporan-diagnosa-gizi-".$this->page,"0")=="1"){
			$this->addPrototype ( $this->name, $this->page, "rawat", "Laporan Pendapatan" . $this->name, "Laporan Diagnosa Gizi", "laporan_diagnosa_gizi" ,"fa fa-book");
		}		
		
		global $db;
		if (getSettings($db, "smis-rs-laporan_diagnosa_rehab_medis-" . $this->page, "0") == "1")
			$this->addPrototype ( $this->name, $this->page, "rawat", "Laporan Diagnosa Rehab Medik " . $this->name, "Laporan Diagnosa Rehab Medik", "laporan_diagnosa_rehab_medis", "fa fa-book");
        $this->addPrototype ( $this->name, $this->page, "rawat", "Rekap Tagihan RM" . $this->name, "Rekap Tagihan RM", "rekap_tagihan_pasien_rm" ,"fa fa-money");
        if(getSettings($db,"smis-rs-tampil-tarif-operasi-".$this->page,"0")=="1"){
            $this->addPrototype ( $this->name, $this->page, "rawat", "Melihat Tarif Operasi" . $this->name, "Melihat Tarif Operasi", "tarif_operasi" ,"fa fa-money");
        }
	}
}