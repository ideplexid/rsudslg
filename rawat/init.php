<?php
global $PLUGINS;

$init ['name'] = 'rawat';
$init ['path'] = SMIS_DIR . "rawat/";
$init ['description'] = "Template Dari Rawat Poliklinik, Kamar Inap dan Lain-lain, Merupakan sebuah Prototype";
$init ['require'] = "administrator, poliklinik";
$init ['service'] = "stok_status, disease_status, diagnose_status, patient_queue, income_status";
$init ['version'] = "4.6.9";
$init ['number'] = "70";
$init ['type'] = "prototype";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;