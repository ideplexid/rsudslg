<?php
require_once ("rawat/prototype/RawatNavigator.php");
global $db;
global $NAVIGATOR;

require_once 'smis-libs-inventory/navigation.php';
$prototype = get_all_prototype ( $db, "rawat" );
foreach ( $prototype as $p ) {
	$inav=new InventoryNavigator($NAVIGATOR,"",$p->nama,$p->slug);
	$prototype_navigation = new RawatNavigator ( $NAVIGATOR, $p->keterangan, $p->nama, $p->slug );
	$menu = $prototype_navigation->init();
	$b=getSettings($db, "smis-is-inventory-barang-".$p->slug , "1");
	$o=getSettings($db, "smis-is-inventory-obat-".$p->slug , "1");
	
	if($b=="1" || $o=="1"){
		$inav->extendMenu($menu,$p->nama,$p->slug,"rawat");
	}
		
}

?>