var alok;	
var tarif_alok;
var ALOK_POLINAME;
var ALOK_POLISLUG;
var ALOK_CARABAYAR;

$(document).ready(function(){
	ALOK_POLINAME=$("#alok_poliname").val();
	ALOK_POLISLUG=$("#alok_polislug").val();
	ALOK_CARABAYAR=$("#alok_carabayar").val();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id','tanggal','nama','satuan','kode','harga','jumlah');
	alok=new RawatAction("alok",ALOK_POLISLUG,"alok",column);
	alok.setEnableAutofocus(true);
	alok.setMultipleInput(true);
	alok.setNextEnter();
	alok.setPrototipe(ALOK_POLINAME,ALOK_POLISLUG,"rawat");
	alok.getRegulerData=function(){
		reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#alok_noreg_pasien").val();
		reg_data['nama_pasien']=$("#alok_nama_pasien").val();
		reg_data['nrm_pasien']=$("#alok_nrm_pasien").val();						
		reg_data['carabayar']=ALOK_CARABAYAR;
		return reg_data;
	};
	alok.view();

	tarif_alok=new TableAction("tarif_alok",ALOK_POLISLUG,"alok",new Array());
	tarif_alok.setPrototipe(ALOK_POLINAME,ALOK_POLISLUG,"rawat");
	tarif_alok.setSuperCommand("tarif_alok");
	tarif_alok.selected=function(json){
		$("#alok_nama").val(json.nama);
		$("#alok_satuan").val(json.satuan);
		$("#alok_kode").val(json.kode);
		$("#alok_harga").maskMoney('mask',Number(json.harga) );
	};

	stypeahead("#alok_nama",3,tarif_alok,"nama",function(item){
		$("#alok_nama").val(item.nama);
		$("#alok_satuan").val(item.satuan);
		$("#alok_kode").val(item.kode);
		$("#alok_harga").maskMoney('mask',Number(item.harga) );	
		$("#alok_jumlah").focus();				
	});
	
});