var vk;					
var vk_operator_satu;
var vk_operator_dua;
var vk_anastesi;
var vk_asisten_anastesi;
var vk_team_vk;
var vk_team_bidan;
var vk_team_perawat;
var vk_tarif_vk;	
var VK_POLISLUG;
var VK_POLINAME;
var VK_CARABAYAR;
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    VK_POLISLUG=$("#vk_polislug").val();
	VK_POLINAME=$("#vk_poliname").val();
	VK_CARABAYAR=$("#vk_carabayar").val();
    var column=new Array(
            'id',"nama_pasien","noreg_pasien","nrm_pasien","waktu",
            "id_operator_satu","harga_operator_satu","nama_operator_satu","jenis_operator_satu",
            "id_operator_dua","harga_operator_dua","nama_operator_dua","jenis_operator_dua",
            "id_anastesi","nama_anastesi","harga_anastesi",'anastesi_hadir',
            'id_asisten_anastesi','nama_asisten_anastesi',"harga_asisten_anastesi",
            'id_team_vk',"harga_team_vk","nama_team_vk",
            'id_bidan',"harga_bidan","nama_bidan",
            "id_perawat","harga_perawat","nama_perawat",
            "kelas",
            "nama_sewa_kamar","harga_sewa_kamar"
            );
    
    vk=new RawatAction("vk",VK_POLISLUG,"vk",column);
    vk.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#vk_noreg_pasien").val();
        reg_data['nama_pasien']=$("#vk_nama_pasien").val();
        reg_data['nrm_pasien']=$("#vk_nrm_pasien").val();						
        reg_data['carabayar']=VK_CARABAYAR;
        return reg_data;
    };
    vk.view();

    vk_operator_satu=new TableAction("vk_operator_satu",VK_POLISLUG,"vk",new Array());
    vk_operator_satu.setSuperCommand("vk_operator_satu");
    vk_operator_satu.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_operator_satu.selected=function(json){
        $("#vk_id_operator_satu").val(json.id);
        $("#vk_nama_operator_satu").val(json.nama);
        $("#vk_jenis_operator_satu").val(json.slug);
    };

    vk_operator_dua=new TableAction("vk_operator_dua",VK_POLISLUG,"vk",new Array());
    vk_operator_dua.setSuperCommand("vk_operator_dua");
    vk_operator_dua.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_operator_dua.selected=function(json){
        $("#vk_id_operator_dua").val(json.id);
        $("#vk_nama_operator_dua").val(json.nama);
        $("#vk_jenis_operator_dua").val(json.slug);
    };

    vk_anastesi=new TableAction("vk_anastesi",VK_POLISLUG,"vk",new Array());
    vk_anastesi.setSuperCommand("vk_anastesi");
    vk_anastesi.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_anastesi.selected=function(json){
        $("#vk_id_anastesi").val(json.id);
        $("#vk_nama_anastesi").val(json.nama);
    };

    vk_asisten_anastesi=new TableAction("vk_asisten_anastesi",VK_POLISLUG,"vk",new Array());
    vk_asisten_anastesi.setSuperCommand("vk_asisten_anastesi");
    vk_asisten_anastesi.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_asisten_anastesi.selected=function(json){
        $("#vk_id_asisten_anastesi").val(json.id);
        $("#vk_nama_asisten_anastesi").val(json.nama);
    };

    vk_team_vk=new TableAction("vk_team_vk",VK_POLISLUG,"vk",new Array());
    vk_team_vk.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_team_vk.setSuperCommand("vk_team_vk");
    vk_team_vk.selected=function(json){
        $("#vk_id_team_vk").val(json.id);
        $("#vk_nama_team_vk").val(json.nama);
    };

    vk_team_bidan=new TableAction("vk_team_bidan",VK_POLISLUG,"vk",new Array());
    vk_team_bidan.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_team_bidan.setSuperCommand("vk_team_bidan");
    vk_team_bidan.selected=function(json){
        $("#vk_id_bidan").val(json.id);
        $("#vk_nama_bidan").val(json.nama);
    };

    vk_team_perawat=new TableAction("vk_team_perawat",VK_POLISLUG,"vk",new Array());
    vk_team_perawat.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_team_perawat.setSuperCommand("vk_team_perawat");
    vk_team_perawat.selected=function(json){
        $("#vk_id_perawat").val(json.id);
        $("#vk_nama_perawat").val(json.nama);
    };
    
    vk_tarif_vk=new TableAction("vk_tarif_vk",VK_POLISLUG,"vk",new Array());
    vk_tarif_vk.setPrototipe(VK_POLINAME,VK_POLISLUG,"rawat");
    vk_tarif_vk.setSuperCommand("vk_tarif_vk");
    vk_tarif_vk.selected=function(json){
        var harga=Number(json.tarif);
        $("#vk_nama_sewa_kamar").val(json.nama);
        $("#vk_harga_sewa_kamar").maskMoney('mask',harga);						
        $("#vk_kelas").val(json.kelas);
    };
    
});