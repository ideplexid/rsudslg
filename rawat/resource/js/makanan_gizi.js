var makanan_gizi;
var makanan_gizi_carabayar;
var makanan_gizi_poliname;
var makanan_gizi_polislug;
var diet_pagi;
var diet_siang;
var diet_malam;
var menu_pagi;
var menu_siang;
var menu_malam;

$(document).ready(function(){
	makanan_gizi_carabayar  = $("#makanan_gizi_carabayar").val();
	makanan_gizi_poliname   = $("#makanan_gizi_poliname").val();
	makanan_gizi_polislug   = $("#makanan_gizi_polislug").val();
    
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({minuteStep:1});
	var column = new Array(
        "id",
        "nama_pasien","noreg_pasien","nrm_pasien",
        "menu_pagi","diet_pagi","pk_pagi",
        "menu_siang","diet_siang","pk_siang",
        "menu_malam","diet_malam","pk_malam"
    );
	makanan_gizi = new TableAction("makanan_gizi",makanan_gizi_polislug,"makanan_gizi",column);
	makanan_gizi.addNoClear("id");
    makanan_gizi.addNoClear("nama_pasien");
    makanan_gizi.addNoClear("noreg_pasien");
    makanan_gizi.addNoClear("nrm_pasien");
    makanan_gizi.addNoClear("menu_pagi");
    makanan_gizi.addNoClear("diet_pagi");
    makanan_gizi.addNoClear("pk_pagi");
    makanan_gizi.addNoClear("menu_siang");
    makanan_gizi.addNoClear("diet_siang");
    makanan_gizi.addNoClear("pk_siang");
    makanan_gizi.addNoClear("menu_malam");
    makanan_gizi.addNoClear("diet_malam");
    makanan_gizi.addNoClear("pk_malam");    
    makanan_gizi.setPrototipe(makanan_gizi_poliname,makanan_gizi_polislug,"rawat");
	makanan_gizi.getRegulerData=function(){
		var reg_data = {	
            page                : this.page,
            action              : this.action,
            super_command       : this.super_command,
            prototype_name      : this.prototype_name,
            prototype_slug      : this.prototype_slug,
            prototype_implement : this.prototype_implement
        };
		reg_data['noreg_pasien']    = $("#makanan_gizi_noreg_pasien").val();
		reg_data['nama_pasien']     = $("#makanan_gizi_nama_pasien").val();
		reg_data['nrm_pasien']      = $("#makanan_gizi_nrm_pasien").val();
		reg_data['carabayar']       = makanan_gizi_carabayar;
		return reg_data;
	};
    
    makanan_gizi.postAction=function(id){
        $("#makanan_gizi_id").val(id);
    };
	
	diet_pagi = new TableAction("diet_pagi",makanan_gizi_polislug,"makanan_gizi",new Array());
    diet_pagi.setPrototipe(makanan_gizi_poliname,makanan_gizi_polislug,"rawat");
    diet_pagi.setSuperCommand("diet_pagi");
    diet_pagi.selected=function(json){
        $("#makanan_gizi_diet_pagi").val(json.nama);
    };

    diet_siang = new TableAction("diet_siang",makanan_gizi_polislug,"makanan_gizi",new Array());
    diet_siang.setPrototipe(makanan_gizi_poliname,makanan_gizi_polislug,"rawat");
    diet_siang.setSuperCommand("diet_siang");
    diet_siang.selected=function(json){
        $("#makanan_gizi_diet_siang").val(json.nama);
    };

    diet_malam = new TableAction("diet_malam",makanan_gizi_polislug,"makanan_gizi",new Array());
    diet_malam.setPrototipe(makanan_gizi_poliname,makanan_gizi_polislug,"rawat");
    diet_malam.setSuperCommand("diet_malam");
    diet_malam.selected=function(json){
        $("#makanan_gizi_diet_malam").val(json.nama);
    };
    
    menu_pagi = new TableAction("menu_pagi",makanan_gizi_polislug,"makanan_gizi",new Array());
    menu_pagi.setPrototipe(makanan_gizi_poliname,makanan_gizi_polislug,"rawat");
    menu_pagi.setSuperCommand("menu_pagi");
    menu_pagi.selected=function(json){
        $("#makanan_gizi_menu_pagi").val(json.nama);
    };

    menu_siang = new TableAction("menu_siang",makanan_gizi_polislug,"makanan_gizi",new Array());
    menu_siang.setPrototipe(makanan_gizi_poliname,makanan_gizi_polislug,"rawat");
    menu_siang.setSuperCommand("menu_siang");
    menu_siang.selected=function(json){
        $("#makanan_gizi_menu_siang").val(json.nama);
    };

    menu_malam = new TableAction("menu_malam",makanan_gizi_polislug,"makanan_gizi",new Array());
    menu_malam.setPrototipe(makanan_gizi_poliname,makanan_gizi_polislug,"rawat");
    menu_malam.setSuperCommand("menu_malam");
    menu_malam.selected=function(json){
        $("#makanan_gizi_menu_malam").val(json.nama);
    };
    var id_default=$("#makanan_gizi_id").val();
    if(id_default!="" && id_default!="0" && id_default!=0){        
        makanan_gizi.edit(id_default);    
    }
});