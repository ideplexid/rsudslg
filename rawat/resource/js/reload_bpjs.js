/**
 * this function used to check
 * when to activate, when to stop
 * and when to reactivated again
 * the bpjs checker of the patient
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 02 Aug 2017
 * @version     : 1.0.0
 * @license     : LGPLv3
 * */

/** 
 * this function used to 
 * reload the plafon and the money that
 * left after being used in patient treatment 
 * automatically when user edit or change the patient price
 * */
var reload_bpjs;
reload_bpjs = function(json,plafon){
    $("link[href='rawat/resource/css/lock_layanan.css']").remove();
	if(json.bpjs=="1"){
        load_plafon_system(json.total_tagihan,plafon,true);    
    }
};


/** 
 * this function used to 
 * reload the plafon and the money that
 * left after being used in patient treatment 
 * when user cliked on the menu
 * */
var load_plafon_update = function(entity){
    var data             = entity.getRegulerData();
    data['action']       = "get_total_tagihan";
    data['noreg_pasien'] = $("#noreg_pasien").val();
    showLoading();
    $.post("",data,function(res){
        var json          = getContent(res);
        var total_tagihan = json.total_tagihan;
        var plafon        = getMoney("#plafon_bpjs");
        load_plafon_system(total_tagihan,plafon,false);
        dismissLoading();
    });
    
};

/**
 * this function used to udpate 
 * when user clicked the button update plafon
 * */
function load_plafon_system(total_tagihan,plafon,show_warning){ 
    $("link[href='rawat/resource/css/lock_layanan.css']").remove();   
    var total = Number(total_tagihan);
    var sisa  = plafon-total;
    if(sisa<0){
        sisa  = 0;
    }
    setMoney("#plafon_bpjs",plafon);
    var persen_warning  = Number($("#warning_plafon").val());
    var persen_stop     = Number($("#stop_plafon").val());
    var warning_plafon  = persen_warning*plafon/100;
    var stop_plafon     = persen_stop*plafon/100;    
    setMoney("#total_tagihan",total);
    setMoney("#sisa_plafon",sisa);	
    if(total >= warning_plafon && show_warning){
        $("#"+this.prefix+"_add_form").smodal('hide');
        $("#peringatan_bpjs").show();
    }else{
        $("#peringatan_bpjs").hide();
    }
    if( $("#activate_stop_plafon").val()=="1" && total>=stop_plafon ){
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'rawat/resource/css/lock_layanan.css') );
    }
};

