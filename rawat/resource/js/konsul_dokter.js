var konsul_dokter;		
var tarif_konsul;
var dokter_konsul;
var KDOKTER_POLISLUG;
var KDOKTER_POLINAME;
var KDOKTER_CARABAYAR;
$(document).ready(function(){
    $("#konsul_dokter_nama_dokter").attr("empty","n");
    KDOKTER_POLISLUG=$("#konsul_dokter_polislug").val();
	KDOKTER_POLINAME=$("#konsul_dokter_poliname").val();
	KDOKTER_CARABAYAR=$("#konsul_dokter_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array(
            'id',"nama_pasien","noreg_pasien","nrm_pasien",
            'waktu','kelas','harga','nama_dokter','id_dokter',
            "nama_konsul","id_konsul","jaspel_nilai","jaspel_persen");
    konsul_dokter=new RawatAction("konsul_dokter",KDOKTER_POLISLUG,"konsul_dokter",column);
    konsul_dokter.setPrototipe(KDOKTER_POLINAME,KDOKTER_POLISLUG,"rawat");
    konsul_dokter.setEnableAutofocus(true);
    konsul_dokter.setMultipleInput(true);
    konsul_dokter.addEmployee("id_dokter","nama_dokter","Dokter");
    konsul_dokter.setNextEnter();
    konsul_dokter.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#konsul_dokter_noreg_pasien").val();
        reg_data['nama_pasien']=$("#konsul_dokter_nama_pasien").val();
        reg_data['nrm_pasien']=$("#konsul_dokter_nrm_pasien").val();
        reg_data['carabayar']=KDOKTER_CARABAYAR;
        return reg_data;
    };
    konsul_dokter.view();

    tarif_konsul=new TableAction("tarif_konsul",KDOKTER_POLISLUG,"konsul_dokter",new Array());
    tarif_konsul.setPrototipe(KDOKTER_POLINAME,KDOKTER_POLISLUG,"rawat");
    tarif_konsul.setSuperCommand("tarif_konsul");
    tarif_konsul.addRegulerData=function(d){
        d['noreg_pasien']=$("#konsul_dokter_noreg_pasien").val();
        return d;
    };
    tarif_konsul.selected=function(json){
        var nama=json.nama_dokter;
        var id_dokter=json.id_dokter;		
        var kelas=json.kelas;
        var harga=Number(json.tarif);
        if($("#is_konsul_separated").val()!="1"){
            $("#konsul_dokter_nama_dokter").val(nama);
            $("#konsul_dokter_id_dokter").val(id_dokter);
        }
        $("#konsul_dokter_nama_konsul").val(json.nama_konsul);
        $("#konsul_dokter_id_konsul").val(json.id);
        $("#konsul_dokter_kelas").val(kelas);
        $("#konsul_dokter_jaspel_nilai").val(json.jaspel);
        $("#konsul_dokter_jaspel_persen").val(json.jaspel_persen);

        $("#konsul_dokter_harga").maskMoney('mask',harga);		
    };

    dokter_konsul=new TableAction("dokter_konsul",KDOKTER_POLISLUG,"konsul_dokter",new Array());
    dokter_konsul.setPrototipe(KDOKTER_POLINAME,KDOKTER_POLISLUG,"rawat");
    dokter_konsul.setSuperCommand("dokter_konsul");
    dokter_konsul.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        var slug=json.slug;
        $("#konsul_dokter_nama_dokter").val(nama);
        $("#konsul_dokter_id_dokter").val(nip);
        $("#konsul_dokter_nama_konsul").focus();
    };

    if($("#is_konsul_separated").val()!="1"){
        stypeahead("#konsul_dokter_nama_dokter",3,tarif_konsul,"nama_dokter",function(item){
            tarif_konsul.selected(item);
            $("#konsul_dokter_save").focus();
            return item.name;
        });
    }else{
        stypeahead("#konsul_dokter_nama_dokter",3,dokter_konsul,"nama",function(item){
            dokter_konsul.selected(item);
            $("#konsul_dokter_nama_konsul").focus();
            return item.name;
        });
    }

    stypeahead("#konsul_dokter_nama_konsul",3,tarif_konsul,"nama_konsul",function(item){
        tarif_konsul.selected(item);
        $("#konsul_dokter_save").focus();
        return item.name;
    });
    
    
});