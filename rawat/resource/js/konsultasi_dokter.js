var perawat_konsultasi_dokter_satu;
var perawat_konsultasi_dokter_dua;
var perawat_konsultasi_dokter_tiga;
var perawat_konsultasi_dokter_empat;
var perawat_konsultasi_dokter_lima;
var perawat_konsultasi_dokter_enam;
var perawat_konsultasi_dokter_tujuh;
var perawat_konsultasi_dokter_depalan;
var perawat_konsultasi_dokter_sembilan;
var perawat_konsultasi_dokter_sepuluh;
var konsultasi_dokter;
var dokter_konsultasi_dokter;		
var tarif_konsul;
var KSD_POLISLUG;
var KSD_POLINAME;
var KSD_CARABAYAR;	
var NUMBER_PERAWAT_KONSULTASI_DOKTER;
var NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION;


$(document).ready(function(){
    $("#konsultasi_dokter_nama_dokter").attr("empty","n");
    KSD_POLISLUG    = $("#ksd_polislug").val();
	KSD_POLINAME    = $("#ksd_poliname").val();
    KSD_CARABAYAR   = $("#ksd_carabayar").val();    
    NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION   = new Array();
	NUMBER_PERAWAT_KONSULTASI_DOKTER                = new Array();
	NUMBER_PERAWAT_KONSULTASI_DOKTER[1]             = "_satu";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[2]             = "_dua";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[3]             = "_tiga";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[4]             = "_empat";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[5]             = "_lima";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[6]             = "_enam";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[7]             = "_tujuh";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[8]             = "_delapan";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[9]             = "_sembilan";
	NUMBER_PERAWAT_KONSULTASI_DOKTER[10]            = "_sepuluh";

    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id',"nama_pasien","noreg_pasien","nrm_pasien",'waktu','harga','nama_dokter','id_dokter',"harga_perawat","jaspel_nilai","jaspel_persen","jaspel_nilai_perawat","jaspel_persen_perawat");
    for(var i=1;i<=NUMBER_PERAWAT_KONSULTASI_DOKTER.length;i++){
		var num=NUMBER_PERAWAT_KONSULTASI_DOKTER[i];
		column.push("nama_perawat"+num);
		column.push("id_perawat"+num);
	}
    konsultasi_dokter=new RawatAction("konsultasi_dokter",KSD_POLISLUG,"konsultasi_dokter",column);
    konsultasi_dokter.setPrototipe(KSD_POLINAME,KSD_POLISLUG,"rawat");
    konsultasi_dokter.setEnableAutofocus(true);
    konsultasi_dokter.setMultipleInput(true);
    konsultasi_dokter.setNextEnter();
    konsultasi_dokter.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#konsultasi_dokter_noreg_pasien").val();
        reg_data['nama_pasien']=$("#konsultasi_dokter_nama_pasien").val();
        reg_data['nrm_pasien']=$("#konsultasi_dokter_nrm_pasien").val();					
        reg_data['carabayar']=KSD_CARABAYAR;
        return reg_data;
	};
	
	konsultasi_dokter.addEmployee("id_dokter","nama_dokter","Dokter");
	konsultasi_dokter.addEmployee("id_perawat_satu","nama_perawat_satu","Perawat I");
	konsultasi_dokter.addEmployee("id_perawat_dua","nama_perawat_dua","Perawat II");
	konsultasi_dokter.addEmployee("id_perawat_tiga","nama_perawat_tiga","Perawat III");
	konsultasi_dokter.addEmployee("id_perawat_empat","nama_perawat_empat","Perawat IV");
	konsultasi_dokter.addEmployee("id_perawat_lima","nama_perawat_lima","Perawat V");
	konsultasi_dokter.addEmployee("id_perawat_enam","nama_perawat_enam","Perawat VI");
	konsultasi_dokter.addEmployee("id_perawat_tujuh","nama_perawat_tujuh","Perawat VII");
	konsultasi_dokter.addEmployee("id_perawat_delapan","nama_perawat_delapan","Perawat VIII");
	konsultasi_dokter.addEmployee("id_perawat_sembilan","nama_perawat_sembilan","Perawat IX");
	konsultasi_dokter.addEmployee("id_perawat_sepuluh","nama_perawat_sepuluh","Perawat X");

    konsultasi_dokter.view();
    konsultasi_dokter.beforesave=function(){		
		for(var i=1;i<=NUMBER_PERAWAT_KONSULTASI_DOKTER.length;i++){
			var num=NUMBER_PERAWAT_KONSULTASI_DOKTER[i];
			if($("#"+this.prefix+"_nama_perawat"+num).val()==""){
				$("#"+this.prefix+"_id_perawat"+num).val(0);
			}
            /*reseting defaule value*/
            var dv_nama=$("#"+this.prefix+"_nama_perawat"+num).val();
            var dv_id=$("#"+this.prefix+"_id_perawat"+num).val();
            
            $("#"+this.prefix+"_nama_perawat"+num).attr("dv",dv_nama);
            $("#"+this.prefix+"_id_perawat"+num).attr("dv",dv_id);
		}		
    };
    var PREFIX_KSD="konsultasi_dokter";
    for(var i=1;i<=NUMBER_PERAWAT_KONSULTASI_DOKTER.length;i++){
		var num=NUMBER_PERAWAT_KONSULTASI_DOKTER[i];
		var pksd=new TableAction("perawat_"+PREFIX_KSD+num,KSD_POLISLUG,"konsultasi_dokter",new Array());
			pksd.setPrototipe(KSD_POLINAME,KSD_POLISLUG,"rawat");
			pksd.numero=num;
			pksd.setSuperCommand("perawat_"+PREFIX_KSD+pksd.numero);
			pksd.selected=function(json){
				console.log(this);
				var nama=json.nama;
				var nip=json.id;
				$("#"+PREFIX_KSD+"_nama_perawat"+this.numero).val(nama);
				$("#"+PREFIX_KSD+"_id_perawat"+this.numero).val(nip);
			};
		$("#"+PREFIX_KSD+"_nama_perawat"+num).data("numero",num);
		$("#"+PREFIX_KSD+"_nama_perawat"+num).data("i",i);
		stypeahead("#"+PREFIX_KSD+"_nama_perawat"+num,3,pksd,"nama",function(item,id){
			var numero=$(id).data("numero");
			var the_i=Number($(id).data("i"));
			$("#"+PREFIX_KSD+"_id_perawat"+numero).val(item.id);
			$("#"+PREFIX_KSD+"_nama_perawat"+numero).val(item.nama);
			if(the_i<NUMBER_PERAWAT_KONSULTASI_DOKTER.length){
				var next_num=NUMBER_PERAWAT_KONSULTASI_DOKTER[the_i+1];
				if($("#"+PREFIX_KSD+"_nama_perawat"+next_num).attr("type")=="hidden"){
					$("#"+PREFIX_KSD+"_nama_tindakan").focus();
				}else{
					$("#"+PREFIX_KSD+"_nama_perawat"+next_num).focus();
				}
			}
		});
		NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[i]=pksd;			
    }
    
    perawat_konsultasi_dokter_satu      = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[1];
	perawat_konsultasi_dokter_dua       = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[2];
	perawat_konsultasi_dokter_tiga      = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[3];
	perawat_konsultasi_dokter_empat     = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[4];
	perawat_konsultasi_dokter_lima      = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[5];
	perawat_konsultasi_dokter_enam      = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[6];
	perawat_konsultasi_dokter_tujuh     = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[7];
	perawat_konsultasi_dokter_delapan   = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[8];
	perawat_konsultasi_dokter_sembilan  = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[9];
	perawat_konsultasi_dokter_sepuluh   = NUMBER_PERAWAT_KONSULTASI_DOKTER_TABLE_ACTION[10];


    dokter_konsultasi_dokter=new TableAction("dokter_konsultasi_dokter",KSD_POLISLUG,"konsultasi_dokter",new Array());
    dokter_konsultasi_dokter.setSuperCommand("dokter_konsultasi_dokter");
    dokter_konsultasi_dokter.setPrototipe(KSD_POLINAME,KSD_POLISLUG,"rawat");
    dokter_konsultasi_dokter.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#konsultasi_dokter_nama_dokter").val(nama);
        $("#konsultasi_dokter_id_dokter").val(nip);
    };
    stypeahead("#konsultasi_dokter_nama_dokter",3,dokter_konsultasi_dokter,"nama",function(item){
        $("#konsultasi_dokter_id_dokter").val(item.id);
        $("#konsultasi_dokter_nama_dokter").val(item.nama);
        $("#konsultasi_dokter_harga").focus();
    });
});