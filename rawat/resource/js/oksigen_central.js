var OCT_POLISLUG;
var OCT_POLINAME;
var OCT_CARABAYAR;	
var oksigen_central;		
$(document).ready(function(){
    OCT_POLISLUG=$("#oct_polislug").val();
	OCT_POLINAME=$("#oct_poliname").val();
	OCT_CARABAYAR=$("#oct_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydatetime').datetimepicker({ minuteStep: 1});
    var column=new Array('id','nama_pasien','nrm_pasien','noreg_pasien','mulai','selesai','skala');
    oksigen_central=new RawatAction("oksigen_central",OCT_POLISLUG,"oksigen_central",column);
    oksigen_central.setPrototipe(OCT_POLINAME,OCT_POLISLUG,"rawat");
    oksigen_central.getRegulerData=function(){
        var reg=RawatAction.prototype.getRegulerData.call(this);
        reg['noreg_pasien']=$("#"+this.prefix+"_noreg_pasien").val();
        reg['nrm_pasien']=$("#"+this.prefix+"_nrm_pasien").val();
        reg['nama_pasien']=$("#"+this.prefix+"_nama_pasien").val();
        reg['carabayar']=OCT_CARABAYAR;
        return reg;
    };
    oksigen_central.view();
});