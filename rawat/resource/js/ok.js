var ok;					
var ok_dokter_pendamping;
var ok_operator_satu;
var ok_operator_dua;
var ok_asisten_operator_satu;
var ok_asisten_operator_dua;
var ok_anastesi;
var ok_asisten_anastesi;
var ok_asisten_anastesi_dua;
var ok_instrument;
var ok_instrument_dua;
var ok_oomloop_satu;
var ok_oomloop_dua;
var ok_perujuk;
var ok_team_ok;
var ok_team_bidan;
var ok_team_bidan_dua;
var ok_tarif_ok;
var ok_nama_tindakan;	
var ok_nama_operasi;
var ok_nama_vk;	
var ok_icd_tindakan;

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({minuteStep:1});
	var pname=$("#ok_hidden_poliname").val();
	var pslug=$("#ok_hidden_polislug").val();
	var cbayar=$("#ok_hidden_carabayar").val();
    init_ok(pslug,pname,cbayar);
    
    $("#ok_jenis_bius").on("click",function(){
        var jns = $("#ok_jenis_bius").val();
       /* if(jns=="Lokal"){
            $(".fcontainer_ok_nama_operator_dua").addClass("hide");
            $(".fcontainer_ok_harga_operator_dua").addClass("hide");
            $(".fcontainer_ok_nama_oomloop_satu").addClass("hide");
            $(".fcontainer_ok_harga_oomloop_satu").addClass("hide");
            $(".fcontainer_ok_nama_oomloop_dua").addClass("hide");
            $(".fcontainer_ok_harga_oomloop_dua").addClass("hide");
            $(".fcontainer_ok_harga_anastesi").addClass("hide");
            $(".fcontainer_ok_nama_anastesi").addClass("hide");

            $(".fcontainer_ok_nama_asisten_anastesi").addClass("hide");
            $(".fcontainer_ok_harga_asisten_anastesi").addClass("hide");
            $(".fcontainer_ok_nama_asisten_anastesi_satu").addClass("hide");
            $(".fcontainer_ok_harga_asisten_anastesi_satu").addClass("hide");
            $(".fcontainer_ok_nama_asisten_anastesi_dua").addClass("hide");
            $(".fcontainer_ok_harga_asisten_anastesi_dua").addClass("hide");

            $(".fcontainer_ok_nama_instrument").addClass("hide");
            $(".fcontainer_ok_harga_instrument").addClass("hide");            
            $(".fcontainer_ok_nama_instrument_dua").addClass("hide");
            $(".fcontainer_ok_harga_instrument_dua").addClass("hide"); 
        }else if(jns=="Regional / General"){*/
            $(".fcontainer_ok_nama_operator_dua").removeClass("hide");
            $(".fcontainer_ok_harga_operator_dua").removeClass("hide");
            $(".fcontainer_ok_nama_oomloop_satu").removeClass("hide");
            $(".fcontainer_ok_harga_oomloop_satu").removeClass("hide");
            $(".fcontainer_ok_nama_oomloop_dua").removeClass("hide");
            $(".fcontainer_ok_harga_oomloop_dua").removeClass("hide");
            $(".fcontainer_ok_harga_anastesi").removeClass("hide");
            $(".fcontainer_ok_nama_anastesi").removeClass("hide");
            
            $(".fcontainer_ok_nama_asisten_anastesi").removeClass("hide");
            $(".fcontainer_ok_harga_asisten_anastesi").removeClass("hide");
            $(".fcontainer_ok_nama_asisten_anastesi_satu").removeClass("hide");
            $(".fcontainer_ok_harga_asisten_anastesi_satu").removeClass("hide");
            $(".fcontainer_ok_nama_asisten_anastesi_dua").removeClass("hide");
            $(".fcontainer_ok_harga_asisten_anastesi_dua").removeClass("hide");

            $(".fcontainer_ok_nama_instrument").removeClass("hide");
            $(".fcontainer_ok_harga_instrument").removeClass("hide");            
            $(".fcontainer_ok_nama_instrument_dua").removeClass("hide");
            $(".fcontainer_ok_harga_instrument_dua").removeClass("hide"); 
        //}
    });
	
});

function init_ok(POLISLUG,POLINAME,CARABAYAR){
	var column=new Array(
			'id',"nama_pasien","noreg_pasien","nrm_pasien","waktu",
            "nama_tindakan","nama_tindakan_dua","recovery_room",
            "harga_tindakan_satu","harga_tindakan_dua","id_dokter_pendamping","harga_dokter_pendamping","nama_dokter_pendamping",
			"id_operator_satu","harga_operator_satu","nama_operator_satu","jenis_operator_satu",
			"id_operator_dua","harga_operator_dua","nama_operator_dua","jenis_operator_dua",
			"id_asisten_operator_satu","harga_asisten_operator_satu","nama_asisten_operator_satu",
			"id_asisten_operator_dua","harga_asisten_operator_dua","nama_asisten_operator_dua",							
			"id_anastesi","nama_anastesi","harga_anastesi",'anastesi_hadir',
			'id_asisten_anastesi','nama_asisten_anastesi',"harga_asisten_anastesi",
			'id_team_ok',"harga_team_ok","nama_team_ok",'id_bidan',"harga_bidan","nama_bidan",
			'id_bidan_dua',"harga_bidan_dua","nama_bidan_dua",
			"kelas","harga_perawat",
			"id_perawat","nama_perawat",
			"id_perawat_dua","nama_perawat_dua",
			"id_perawat_tiga","nama_perawat_tiga",
			"id_perawat_empat","nama_perawat_empat",
			"id_perawat_lima","nama_perawat_lima",
			"id_perawat_enam","nama_perawat_enam",
			"id_perawat_tujuh","nama_perawat_tujuh",
			"id_perawat_delapan","nama_perawat_delapan",
			"id_perawat_sembilan","nama_perawat_sembilan",
			"id_perawat_sepuluh","nama_perawat_sepuluh",
			"nama_asisten_anastesi_dua","id_asisten_anastesi_dua","harga_asisten_anastesi_dua",
			"nama_oomloop_satu","id_oomloop_satu","harga_oomloop_satu",
			"nama_oomloop_dua","id_oomloop_dua","harga_oomloop_dua",
			"nama_instrument","id_instrument","harga_instrument",
			"nama_instrument_dua","id_instrument_dua","harga_instrument_dua",
			"nama_sewa_kamar","harga_sewa_kamar","harga_sewa_alat",
            "id_perujuk","nama_perujuk","kode_perujuk",
			"keterangan","bagi_dokter","bagi_asisten_operator","bagi_oomloop",
            "kode_icd","nama_icd","ket_icd",
            "jenis_operasi","jenis_bius"
            
			);
	
	ok=new RawatAction("ok",POLISLUG,"ok",column);
	ok.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok.setEnableAutofocus(true);
	ok.setMultipleInput(true);
	ok.setNextEnter();
	ok.more_option=function(name){
		var data=this.getRegulerData();
		data['id_operator']=$("#ok_id_operator_satu").val();
		data['nama_operator']=$("#ok_nama_operator_satu").val();
		data['tanggal_operasi']=$("#ok_waktu").val();
		if(data['id_operator']=="" || data['nama_operator']=="" || data['tanggal_operasi']=="" || is_date(data['tanggal_operasi']) ){
			smis_alert("Kesalahan", "Dokter Operator I dan Waktu (YYYY-MM-DD HH:ii) Wajib Terisi dengan Benar ", "alert-danger");
			return;
		}
		data['command']="find_perujuk";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			if(json!=null){
				$("#ok_id_perujuk").val(json.id_dokter);
				$("#ok_nama_perujuk").val(json.nama_dokter);
				$("#ok_keterangan").val(json.keterangan);
                $("#ok_kode_perujuk").val(json.kode_perujuk);
			}
			dismissLoading();
		});
	};
	ok.cekSave=function(){
		var good=true;
		var msg="";
		for(var i=0;i<this.column.length;i++){
			var name=this.column[i];
			var the_id="#"+this.prefix+"_"+name;
			var type=$(the_id).attr('type');
			$(the_id).removeClass("error_field");
			
			if(type=='text' || type=='textarea' || type=="select" ){
				var empty=$(the_id).attr('empty');
				var typical=$(the_id).attr('typical');
				var val=$(the_id).val();
				var name=$(the_id).attr('name');
				if(empty=='n' && (val==null || val==''  )    ){
					msg=msg+" </br> <strong>"+name+"</strong> Tidak Boleh Kosong";
					good=false;
					$(the_id).addClass("error_field");
				}
				
				if(empty=='n' && $(the_id).hasClass('mydate') && val=='0000-00-00'  ){
					msg=msg+" </br> <strong>"+name+"</strong> Tidak Boleh 0000-00-00";
					good=false;
					$(the_id).addClass("error_field");
				}
				
				if(empty=='n' && $(the_id).hasClass('idate') && val=='00-00-0000'  ){
					msg=msg+" </br> <strong>"+name+"</strong> Tidak Boleh 00-00-0000";
					good=false;
					$(the_id).addClass("error_field");
				}
		
				if(empty=='n' && typical=="money" && getMoney(the_id)==0  ){
					msg=msg+" </br> <strong>"+name+"</strong> Tidak Boleh Nol";
					good=false;
					$(the_id).addClass("error_field");
				}
				
				if(empty=='n' && $(the_id).hasClass('mydatetime') && val=='0000-00-00 00:00:00'  ){
					msg=msg+" </br> <strong>"+name+"</strong> Tidak Boleh 0000-00-00 00:00:00";
					good=false;
					$(the_id).addClass("error_field");
				}
				
				if(empty=='n' && $(the_id).hasClass('idatetime') && val=='00-00-0000 00:00:00'  ){
					msg=msg+" </br> <strong>"+name+"</strong> Tidak Boleh 00-00-0000 00:00:00";
					good=false;
					$(the_id).addClass("error_field");
				}
				
				if(typical=='alphanumeric'  && !is_alphanumeric(val)){
					msg=msg+" </br> <strong>"+name+"</strong> Seharusnya Alphanumberic (A-Z,a-z,0-9)";
					good=false;
					$(the_id).addClass("error_field");
				}else if(typical=='alpha' && !is_alpha(val)){
					msg=msg+" </br> <strong>"+name+"</strong> Seharusnya Alpha (A-Z,a-z)";
					good=false;
					$(the_id).addClass("error_field");
				}else if(typical=='numeric' && !is_numeric(val)){
					msg=msg+" </br> <strong>"+name+"</strong> Seharusnya Numeric (0-9)";
					good=false;
					$(the_id).addClass("error_field");
				}else if(typical=='date' && !is_date(val)){
					msg=msg+" </br> <strong>"+name+"</strong> Seharusnya Beformat YYYY-MM-DD";
					good=false;
					$(the_id).addClass("error_field");
				}
			}
		}
		
		if(!good){
			var id_alt="#modal_alert_"+this.prefix+"_add_form";		
			var warn='<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>'+msg+'</div>';
			$(id_alt).html(warn);
			$(id_alt).focus();
		}
		return good;
	};
	ok.getRegulerData=function(){
		reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#ok_noreg_pasien").val();
		reg_data['nama_pasien']=$("#ok_nama_pasien").val();
		reg_data['nrm_pasien']=$("#ok_nrm_pasien").val();						
		reg_data['carabayar']=CARABAYAR;
		return reg_data;
    };
    ok.after_form_shown=function(){
        $("#ok_jenis_bius").trigger("click");
    };
	

	ok_operator_satu=new TableAction("ok_operator_satu",POLISLUG,"ok",new Array());
	ok_operator_satu.setSuperCommand("ok_operator_satu");
	ok_operator_satu.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_operator_satu.selected=function(json){
		$("#ok_id_operator_satu").val(json.id);
		$("#ok_nama_operator_satu").val(json.nama);
		$("#ok_jenis_operator_satu").val(json.slug);
	};

	ok_asisten_operator_satu=new TableAction("ok_asisten_operator_satu",POLISLUG,"ok",new Array());
	ok_asisten_operator_satu.setSuperCommand("ok_asisten_operator_satu");
	ok_asisten_operator_satu.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_asisten_operator_satu.selected=function(json){
		$("#ok_id_asisten_operator_satu").val(json.id);
		$("#ok_nama_asisten_operator_satu").val(json.nama);
	};

	ok_operator_dua=new TableAction("ok_operator_dua",POLISLUG,"ok",new Array());
	ok_operator_dua.setSuperCommand("ok_operator_dua");
	ok_operator_dua.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_operator_dua.selected=function(json){
		$("#ok_id_operator_dua").val(json.id);
		$("#ok_nama_operator_dua").val(json.nama);
		$("#ok_jenis_operator_dua").val(json.slug);
	};

	ok_asisten_operator_dua=new TableAction("ok_asisten_operator_dua",POLISLUG,"ok",new Array());
	ok_asisten_operator_dua.setSuperCommand("ok_asisten_operator_dua");
	ok_asisten_operator_dua.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_asisten_operator_dua.selected=function(json){
		$("#ok_id_asisten_operator_dua").val(json.id);
		$("#ok_nama_asisten_operator_dua").val(json.nama);
	};

	ok_anastesi=new TableAction("ok_anastesi",POLISLUG,"ok",new Array());
	ok_anastesi.setSuperCommand("ok_anastesi");
	ok_anastesi.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_anastesi.selected=function(json){
		$("#ok_id_anastesi").val(json.id);
		$("#ok_nama_anastesi").val(json.nama);
	};

	ok_asisten_anastesi=new TableAction("ok_asisten_anastesi",POLISLUG,"ok",new Array());
	ok_asisten_anastesi.setSuperCommand("ok_asisten_anastesi");
	ok_asisten_anastesi.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_asisten_anastesi.selected=function(json){
		$("#ok_id_asisten_anastesi").val(json.id);
		$("#ok_nama_asisten_anastesi").val(json.nama);
	};

	ok_instrument=new TableAction("ok_instrument",POLISLUG,"ok",new Array());
	ok_instrument.setSuperCommand("ok_instrument");
	ok_instrument.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_instrument.selected=function(json){
		$("#ok_id_instrument").val(json.id);
		$("#ok_nama_instrument").val(json.nama);
	};

	ok_instrument_dua=new TableAction("ok_instrument_dua",POLISLUG,"ok",new Array());
	ok_instrument_dua.setSuperCommand("ok_instrument_dua");
	ok_instrument_dua.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_instrument_dua.selected=function(json){
		$("#ok_id_instrument_dua").val(json.id);
		$("#ok_nama_instrument_dua").val(json.nama);
	};

	ok_oomloop_satu=new TableAction("ok_oomloop_satu",POLISLUG,"ok",new Array());
	ok_oomloop_satu.setSuperCommand("ok_oomloop_satu");
	ok_oomloop_satu.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_oomloop_satu.selected=function(json){
		$("#ok_id_oomloop_satu").val(json.id);
		$("#ok_nama_oomloop_satu").val(json.nama);
	};

	ok_oomloop_dua=new TableAction("ok_oomloop_dua",POLISLUG,"ok",new Array());
	ok_oomloop_dua.setSuperCommand("ok_oomloop_dua");
	ok_oomloop_dua.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_oomloop_dua.selected=function(json){
		$("#ok_id_oomloop_dua").val(json.id);
		$("#ok_nama_oomloop_dua").val(json.nama);
	};

	ok_asisten_anastesi_dua=new TableAction("ok_asisten_anastesi_dua",POLISLUG,"ok",new Array());
	ok_asisten_anastesi_dua.setSuperCommand("ok_asisten_anastesi_dua");
	ok_asisten_anastesi_dua.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_asisten_anastesi_dua.selected=function(json){
		$("#ok_id_asisten_anastesi_dua").val(json.id);
		$("#ok_nama_asisten_anastesi_dua").val(json.nama);
	};

	ok_team_ok=new TableAction("ok_team_ok",POLISLUG,"ok",new Array());
	ok_team_ok.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_team_ok.setSuperCommand("ok_team_ok");
	ok_team_ok.selected=function(json){
		$("#ok_id_team_ok").val(json.id);
		$("#ok_nama_team_ok").val(json.nama);
	};

	ok_team_bidan=new TableAction("ok_team_bidan",POLISLUG,"ok",new Array());
	ok_team_bidan.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_team_bidan.setSuperCommand("ok_team_bidan");
	ok_team_bidan.selected=function(json){
		$("#ok_id_bidan").val(json.id);
		$("#ok_nama_bidan").val(json.nama);
	};

	ok_team_bidan_dua=new TableAction("ok_team_bidan_dua",POLISLUG,"ok",new Array());
	ok_team_bidan_dua.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_team_bidan_dua.setSuperCommand("ok_team_bidan_dua");
	ok_team_bidan_dua.selected=function(json){
		$("#ok_id_bidan_dua").val(json.id);
		$("#ok_nama_bidan_dua").val(json.nama);
	}; 

	ok_tarif_ok=new TableAction("ok_tarif_ok",POLISLUG,"ok",new Array());
	ok_tarif_ok.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_tarif_ok.setSuperCommand("ok_tarif_ok");
	ok_tarif_ok.selected=function(json){
		var harga=Number(json.tarif);
		$("#ok_nama_sewa_kamar").val(json.nama);
		$("#ok_harga_sewa_kamar").maskMoney('mask',harga);						
		$("#ok_kelas").val(json.kelas);
	};

	ok_nama_tindakan=new TableAction("ok_nama_tindakan",POLISLUG,"ok",new Array());
	ok_nama_tindakan.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_nama_tindakan.setSuperCommand("ok_nama_tindakan");
	ok_nama_tindakan.addRegulerData=function(d){
		d['noreg_pasien']=$("#ok_noreg_pasien").val();
		d['carabayar']=$("#carabayar").val();
		return d;
	};
	ok_nama_tindakan.selected=function(json){
		var harga=Number(json.tarif);
		$("#ok_nama_operator_satu").val(json.nama_dokter);
		$("#ok_id_operator_satu").val(json.id_dokter);
		$("#ok_nama_tindakan").val(json.nama);
		$("#ok_harga_operator_satu").maskMoney('mask',harga);
		$("#ok_nama_operator_dua").focus();
	};
	
	ok_nama_tindakan_anastesi=new TableAction("ok_nama_tindakan_anastesi",POLISLUG,"ok",new Array());
	ok_nama_tindakan_anastesi.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_nama_tindakan_anastesi.setSuperCommand("ok_nama_tindakan_anastesi");
	ok_nama_tindakan_anastesi.addRegulerData=function(d){
		d['noreg_pasien']=$("#ok_noreg_pasien").val();
		d['carabayar']=$("#carabayar").val();
		return d;
	};
	ok_nama_tindakan_anastesi.selected=function(json){
		var harga=Number(json.tarif);
		$("#ok_nama_tindakan").val(json.nama);
		$("#ok_harga_anastesi").maskMoney('mask',harga);
	};

	ok_nama_operasi=new TableAction("ok_nama_operasi",POLISLUG,"ok",new Array());
	ok_nama_operasi.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_nama_operasi.setSuperCommand("ok_nama_operasi");
	ok_nama_operasi.addRegulerData=function(d){
		d['noreg_pasien']=$("#ok_noreg_pasien").val();
		d['carabayar']=$("#carabayar").val();
		return d;
	};
	ok_nama_operasi.selected=function(json){
		var harga=Number(json.tarif);
		var harga_ok=Number(json.ok);
		var harga_sewa_ok=Number(json.tarif_sewa_ok);			
		var harga_sewa_alat=Number(json.tarif_sewa_alat);			
		//$("#ok_nama_operator_satu").val(json.nama_dokter);
		//$("#ok_id_operator_satu").val(json.id_dokter);
		$("#ok_nama_tindakan").val(json.nama);
		$("#ok_harga_tindakan_satu").maskMoney('mask',harga);
		//$("#ok_harga_operator_dua").maskMoney('mask',Number(json.tarif_operator_dua));

		//$("#ok_harga_asisten_operator_satu").maskMoney('mask',Number(json.tarif_asisten_operator_satu));
		//$("#ok_harga_asisten_operator_dua").maskMoney('mask',Number(json.tarif_asisten_operator_dua));
		//$("#ok_harga_anastesi").maskMoney('mask',Number(json.tarif_anastesi));
		//$("#ok_harga_asisten_anastesi").maskMoney('mask',Number(json.tarif_asisten_anastesi));
		//$("#ok_harga_asisten_anastesi_dua").maskMoney('mask',Number(json.tarif_asisten_anastesi_dua));
		//$("#ok_harga_bidan").maskMoney('mask',Number(json.tarif_bidan));
		//$("#ok_harga_bidan_dua").maskMoney('mask',Number(json.tarif_bidan_dua));
		//$("#ok_harga_perawat").maskMoney('mask',Number(json.tarif_perawat));
		//$("#ok_harga_oomloop_satu").maskMoney('mask',Number(json.tarif_oomloop_satu));
		//$("#ok_harga_oomloop_dua").maskMoney('mask',Number(json.tarif_oomloop_dua));
		//$("#ok_harga_instrument").maskMoney('mask',Number(json.tarif_instrument_satu));
		//$("#ok_harga_instrument_dua").maskMoney('mask',Number(json.tarif_instrument_dua));
		//$("#ok_recovery_room").maskMoney('mask',Number(json.tarif_recovery_room));
		//
		//$("#ok_harga_team_ok").maskMoney('mask',harga_ok);
		//$("#ok_harga_sewa_kamar").maskMoney('mask',harga_sewa_ok);
        //$("#ok_harga_sewa_alat").maskMoney('mask',harga_sewa_alat);
		//$("#ok_nama_sewa_kamar").val(json.jenis_operasi);
        //$("#ok_bagi_dokter").val(json.bagi_dokter);
        //$("#ok_bagi_oomloop").val(json.bagi_oomloop);
		//$("#ok_bagi_asisten_operator").val(json.bagi_asisten_operasi);
		//$("#ok_kelas").val(json.kelas);
		//$("#ok_harga_operator_satu").trigger("change");
		//$("#ok_nama_operator_dua").focus();
    };
    
    ok_nama_operasi_dua=new TableAction("ok_nama_operasi_dua",POLISLUG,"ok",new Array());
	ok_nama_operasi_dua.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_nama_operasi_dua.setSuperCommand("ok_nama_operasi_dua");
	ok_nama_operasi_dua.addRegulerData=function(d){
		d['noreg_pasien']=$("#ok_noreg_pasien").val();
		d['carabayar']=$("#carabayar").val();
		return d;
	};
	ok_nama_operasi_dua.selected=function(json){
        var harga=Number(json.tarif);        			
		//$("#ok_nama_operator_dua").val(json.nama_dokter);
		//$("#ok_id_operator_dua").val(json.id_dokter);
        $("#ok_harga_tindakan_dua").maskMoney('mask',harga);
        $("#ok_nama_tindakan_dua").val(json.nama);
	};

	ok_nama_vk=new TableAction("ok_nama_vk",POLISLUG,"ok",new Array());
	ok_nama_vk.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_nama_vk.setSuperCommand("ok_nama_vk");
	ok_nama_vk.addRegulerData=function(d){
		d['noreg_pasien']=$("#ok_noreg_pasien").val();
		d['carabayar']=$("#carabayar").val();
		return d;
	};
	ok_nama_vk.selected=function(json){
		ok_nama_operasi.selected(json);
	};
	
	ok_perujuk=new TableAction("ok_perujuk",POLISLUG,"ok",new Array());
	ok_perujuk.setSuperCommand("ok_perujuk");
	ok_perujuk.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_perujuk.selected=function(json){
		$("#ok_id_perujuk").val(json.id);
		$("#ok_nama_perujuk").val(json.nama);
	};

	ok_dokter_pendamping=new TableAction("ok_dokter_pendamping",POLISLUG,"ok",new Array());
	ok_dokter_pendamping.setSuperCommand("ok_dokter_pendamping");
	ok_dokter_pendamping.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_dokter_pendamping.selected=function(json){
		$("#ok_id_dokter_pendamping").val(json.id);
		$("#ok_nama_dokter_pendamping").val(json.nama);
	};
	
	
    
    
    ok_icd_tindakan=new TableAction("ok_icd_tindakan",POLISLUG,"ok",new Array("id","nama","icd","grup","dtd"));
	ok_icd_tindakan.setSuperCommand("ok_icd_tindakan");
	ok_icd_tindakan.setPrototipe(POLINAME,POLISLUG,"rawat");
	ok_icd_tindakan.selected=function(json){
		$("#ok_kode_icd").val(json.icd);
		$("#ok_nama_icd").val(json.nama);
		$("#ok_ket_icd").val(json.terjemah);
	};


	stypeahead("#ok_nama_tindakan",3,ok_nama_tindakan,"nama",function(item){
		var harga=Number(json.tarif);
		$("#ok_nama_operator_satu").val(json.nama_dokter);
		$("#ok_id_operator_satu").val(json.id_dokter);
		$("#ok_nama_tindakan").val(json.nama);
		$("#ok_harga_operator_satu").maskMoney('mask',harga);
		$("#nama_sewa_kamar").focus();				
	});
	
	stypeahead("#ok_nama_operator_satu",3,ok_operator_satu,"nama",function(item){
		$("#ok_id_operator_satu").val(item.id);
		$("#ok_nama_operator_satu").val(item.nama);
		$("#ok_jenis_operator_satu").val(item.slug);
		$("#ok_harga_operator_satu").focus();						
	});

	stypeahead("#ok_nama_asisten_operator_satu",3,ok_asisten_operator_satu,"nama",function(item){
		$("#ok_id_asisten_operator_satu").val(item.id);
		$("#ok_nama_asisten_operator_satu").val(item.nama);
		$("#ok_harga_asisten_operator_satu").focus();						
	});

	stypeahead("#ok_nama_asisten_operator_dua",3,ok_asisten_operator_dua,"nama",function(item){
		$("#ok_id_asisten_operator_dua").val(item.id);
		$("#ok_nama_asisten_operator_dua").val(item.nama);
		$("#ok_harga_asisten_operator_dua").focus();						
	});
    
    stypeahead("#ok_nama_oomloop_satu",3,ok_oomloop_satu,"nama",function(item){
		$("#ok_id_oomloop_satu").val(item.id);
		$("#ok_nama_oomloop_satu").val(item.nama);
        $("#ok_nama_oomloop_dua").focus();						
	});
    
	stypeahead("#ok_nama_perujuk",3,ok_perujuk,"nama",function(item){
		$("#ok_id_perujuk").val(item.id);
		$("#ok_nama_perujuk").val(item.nama);
		$("#ok_nama_operator_satu").focus();						
	});

	stypeahead("#ok_nama_dokter_pendamping",3,ok_perujuk,"nama",function(item){
		$("#ok_id_dokter_pendamping").val(item.id);
		$("#ok_nama_dokter_pendamping").val(item.nama);
		$("#ok_harga_dokter_pendamping").focus();						
	});
	

	stypeahead("#ok_nama_operator_dua",3,ok_operator_dua,"nama",function(item){
		$("#ok_id_operator_dua").val(item.id);
		$("#ok_nama_operator_dua").val(item.nama);
		$("#ok_jenis_operator_dua").val(item.slug);
		$("#ok_harga_operator_dua").focus();						
	});

	stypeahead("#ok_nama_anastesi",3,ok_anastesi,"nama",function(item){
		$("#ok_id_anastesi").val(item.id);
		$("#ok_nama_anastesi").val(item.nama);
		$("#ok_jenis_anastesi").val(item.slug);
		$("#ok_harga_anastesi").focus();						
	});

	stypeahead("#ok_nama_asisten_anastesi",3,ok_asisten_anastesi,"nama",function(item){
		$("#ok_id_asisten_anastesi").val(item.id);
		$("#ok_nama_asisten_anastesi").val(item.nama);
		$("#ok_jenis_asisten_anastesi").val(item.slug);
		$("#ok_harga_asisten_anastesi").focus();						
	});

	stypeahead("#ok_nama_asisten_anastesi_dua",3,ok_asisten_anastesi,"nama",function(item){
		$("#ok_id_asisten_anastesi_dua").val(item.id);
		$("#ok_nama_asisten_anastesi_dua").val(item.nama);
		$("#ok_jenis_asisten_anastesi_dua").val(item.slug);
		$("#ok_harga_asisten_anastesi_dua").focus();						
	});

	stypeahead("#ok_nama_team_ok",3,ok_team_ok,"nama",function(item){
		$("#ok_id_team_ok").val(item.id);
		$("#ok_nama_team_ok").val(item.nama);
		$("#ok_jenis_team_ok").val(item.slug);
		$("#ok_harga_team_ok").focus();						
	});

	stypeahead("#ok_nama_team_bidan",3,ok_team_bidan,"nama",function(item){
		$("#ok_id_team_bidan").val(item.id);
		$("#ok_nama_team_bidan").val(item.nama);
		$("#ok_jenis_team_bidan").val(item.slug);
		$("#ok_harga_team_bidan").focus();						
	});

	stypeahead("#ok_nama_team_bidan_dua",3,ok_team_bidan,"nama",function(item){
		$("#ok_id_team_bidan_dua").val(item.id);
		$("#ok_nama_team_bidan_dua").val(item.nama);
		$("#ok_jenis_team_bidan_dua").val(item.slug);
		$("#ok_harga_team_bidan_dua").focus();						
	});

	stypeahead("#ok_nama_instrument_dua",3,ok_team_bidan,"nama",function(item){
		$("#ok_id_instrument_dua").val(item.id);
		$("#ok_nama_instrument_dua").val(item.nama);
		$("#ok_jenis_instrument_dua").val(item.slug);
		$("#ok_harga_instrument_dua").focus();						
	});

	stypeahead("#ok_nama_instrument",3,ok_team_bidan,"nama",function(item){
		$("#ok_id_instrument").val(item.id);
		$("#ok_nama_instrument").val(item.nama);
		$("#ok_jenis_instrument").val(item.slug);
		$("#ok_harga_instrument").focus();						
	});

	stypeahead("#ok_nama_sewa_kamar",3,ok_tarif_ok,"nama",function(json){
		var harga=Number(json.tarif);
		$("#ok_nama_sewa_kamar").val(json.nama);
		$("#ok_harga_sewa_kamar").maskMoney('mask',harga);						
		$("#ok_kelas").val(json.kelas);
		$("#ok_nama_operator_satu").focus();						
	});
	
	var lis_ok_perawat = new Array("","_dua","_tiga","_empat","_lima","_enam","_tujuh","_delapan","_sembilan","_sepuluh");
	var action_perawat = new Array();

	for(var x=0;x<lis_ok_perawat.length;x++){
		var value=lis_ok_perawat[x];
		var named="ok_perawat"+value;
		var perawat;
		perawat=new TableAction(named,POLISLUG,"ok",new Array());
		perawat.setPrototipe(POLINAME,POLISLUG,"rawat");
		perawat.setSuperCommand(named);
		perawat.name_tag=value;
		perawat.selected=function(json){
			$("#ok_id_perawat"+this.name_tag).val(json.id);
			$("#ok_nama_perawat"+this.name_tag).val(json.nama);
		};
		window[named]=perawat;
		action_perawat[named]=perawat;
	}

	stypeahead("#ok_nama_perawat",3,action_perawat["ok_perawat"],"nama",function(json){
		$("#ok_id_perawat").val(json.id);
		$("#ok_nama_perawat").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_dua",3,action_perawat["ok_perawat_dua"],"nama",function(json){
		$("#ok_id_perawat_dua").val(json.id);
		$("#ok_nama_perawat_dua").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_tiga",3,action_perawat["ok_perawat_tiga"],"nama",function(json){
		$("#ok_id_perawat_tiga").val(json.id);
		$("#ok_nama_perawat_tiga").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_empat",3,action_perawat["ok_perawat_empat"],"nama",function(json){
		$("#ok_id_perawat_empat").val(json.id);
		$("#ok_nama_perawat_empat").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_lima",3,action_perawat["ok_perawat_lima"],"nama",function(json){
		$("#ok_id_perawat_lima").val(json.id);
		$("#ok_nama_perawat_lima").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_enam",3,action_perawat["ok_perawat_enam"],"nama",function(json){
		$("#ok_id_perawat_enam").val(json.id);
		$("#ok_nama_perawat_enam").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_tujuh",3,action_perawat["ok_perawat_tujuh"],"nama",function(json){
		$("#ok_id_perawat_tujuh").val(json.id);
		$("#ok_nama_perawat_tujuh").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_delapan",3,action_perawat["ok_perawat_delapan"],"nama",function(json){
		$("#ok_id_perawat_delapan").val(json.id);
		$("#ok_nama_perawat_delapan").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_sembilan",3,action_perawat["ok_perawat_sembilan"],"nama",function(json){
		$("#ok_id_perawat_sembilan").val(json.id);
		$("#ok_nama_perawat_sembilan").val(json.nama);					
	});
	stypeahead("#ok_nama_perawat_sepuluh",3,action_perawat["ok_perawat_sepuluh"],"nama",function(json){
		$("#ok_id_perawat_sepuluh").val(json.id);
		$("#ok_nama_perawat_sepuluh").val(json.nama);					
	});
    ok.view();
    

   
}