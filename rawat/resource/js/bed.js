var bed;	
var nomor_bed;
var nama_tarif;
var BED_POLINAME;
var BED_POLISLUG;

$(document).ready(function(){
	BED_POLINAME=$("#bed_poliname").val();
	BED_POLISLUG=$("#bed_polislug").val();
	
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	var column=new Array('id',"nama_pasien","nama_tarif","noreg_pasien","nrm_pasien",'id_bed','nama_bed','waktu_keluar','waktu_masuk','harga','pilihan_tarif');
	bed = new TableAction("bed",BED_POLISLUG,"bed",column);
	bed.setPrototipe(BED_POLINAME,BED_POLISLUG,"rawat");
	bed.reload_bpjs = function(){
        if (typeof reload_bpjs === "function" && $("#autoload_plafon")=="1") {
            reload_bpjs(json,getMoney("#plafon_bpjs"));
        }
    };
	bed.addRegulerData=function(reg){
        reg['noreg_pasien']=$("#"+this.prefix+"_noreg_pasien").val();//utnuk kepentingan search
		reg['nrm_pasien']=$("#"+this.prefix+"_nrm_pasien").val();
		reg['nama_pasien']=$("#"+this.prefix+"_nama_pasien").val();
		reg['carabayar']=$("#carabayar").val();
        reg['titipan']=layanan.titipan();
		return reg;
	};
	
	bed.save_and_empty=function(id){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            if(!this.cekSave()){
                dismissLoading();
                return;
            }
            var self=this;
            var a=this.getSaveData();
            a['empty']="empty";
            $.ajax({url:"",data:a,type:'post',success:function(res){
                var json=getContent(res);
                if(json==null) return;
                self.view();
                self.clear();
                dismissLoading();
                self.reload_bpjs(json);
            }});
        }else{
            dismissLoading();	
        }
	};
	
	bed.after_form_shown=function(){
		var nama=$("#bed_nama_tarif").val();
		nilai=0;
		if(nama==""){
			var nilai=getMoney("#bed_harga");	
		}
		$("#bed_pilihan_tarif").val(nilai);
    };

    bed.save=function (){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            if(!this.cekSave()){
                dismissLoading();
                return;
            }
            this.beforesave();	
            var self=this;
            var a=this.getSaveData();
            $.ajax({url:"",data:a,type:'post',success:function(res){
                var json=getContent(res);
                if(json==null) {
                    $("#"+self.prefix+"_add_form").smodal('hide');
                    return;		
                }else if(json.success=="1"){
                    if(self.multiple_input) {
                        if(self.focus_on_multi!=""){
                            $('#'+self.action+'_'+self.focus_on_multi).focus();
                        }else{
                            $('#'+self.action+'_add_form').find('[autofocus]').focus();					
                        }
                    }else {
                        $("#"+self.prefix+"_add_form").smodal('hide');
                    }
                    self.view();
                    self.clear();
                }
                dismissLoading();
                self.reload_bpjs(json);
                self.postAction(a['id']);
            }});
            this.aftersave();  
        }else{
            dismissLoading();	
        }
    };
    
    
    bed.cekTutupTagihan = function(){
        var reg_data = this.getRegulerData();			
        var noreg    = $("#"+this.prefix+"_noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;
    
        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

	bed.view();

	nomor_bed=new TableAction("nomor_bed",BED_POLISLUG,"bed",new Array());
	nomor_bed.setPrototipe(BED_POLINAME,BED_POLISLUG,"rawat");
	nomor_bed.setSuperCommand("nomor_bed");
	nomor_bed.selected=function(json){
		$("#bed_id_bed").val(json.id);
		$("#bed_nama_bed").val(json.nama);				
	};
	
	nama_tarif=new TableAction("nama_tarif",BED_POLISLUG,"bed",new Array());
	nama_tarif.setPrototipe(BED_POLINAME,BED_POLISLUG,"rawat");
	nama_tarif.setSuperCommand("nama_tarif");
	nama_tarif.selected=function(json){
		$("#bed_nama_tarif").val(json.name);
		setMoney("#bed_harga",json.value);
		$("#bed_pilihan_tarif").val("");
	};
	
	$("#bed_pilihan_tarif").on('click',function(){
		var uang=$(this).val();
		setMoney("#bed_harga",Number(uang))
		$("#bed_nama_tarif").val("");
	});
	
});