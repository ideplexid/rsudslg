var dokter_audiometry;
var perawat_audiometry;
var audiometry;	
var tarif_audiometry;
var ADM_POLISLUG;
var ADM_POLINAME;
var ADM_CARABAYAR;	
$(document).ready(function(){
    ADM_POLISLUG=$("#adm_polislug").val();
	ADM_POLINAME=$("#adm_poliname").val();
	ADM_CARABAYAR=$("#adm_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id','id_dokter','id_perawat','waktu','kelas','biaya','nama_dokter','nama_perawat');
    audiometry=new RawatAction("audiometry",ADM_POLISLUG,"audiometry",column);
    audiometry.setPrototipe(ADM_POLINAME,ADM_POLISLUG,"rawat");
    audiometry.setEnableAutofocus(true);
    audiometry.setMultipleInput(true);
    audiometry.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#audiometry_noreg_pasien").val();
        reg_data['nama_pasien']=$("#audiometry_nama_pasien").val();
        reg_data['nrm_pasien']=$("#audiometry_nrm_pasien").val();
        reg_data['carabayar']=ADM_CARABAYAR;
        return reg_data;
    };
    audiometry.view();

    dokter_audiometry=new TableAction("dokter_audiometry",ADM_POLISLUG,"audiometry",new Array());
    dokter_audiometry.setSuperCommand("dokter_audiometry");
    dokter_audiometry.setPrototipe(ADM_POLINAME,ADM_POLISLUG,"rawat");
    dokter_audiometry.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#audiometry_nama_dokter").val(nama);
        $("#audiometry_id_dokter").val(nip);
    };
    stypeahead("#audiometry_nama_dokter",3,dokter_audiometry,"nama",function(item){
        $("#audiometry_id_dokter").val(item.id);
        $("#audiometry_nama_dokter").val(item.nama);
        $("#audiometry_nama_perawat").focus();
    });

    perawat_audiometry=new TableAction("perawat_audiometry",ADM_POLISLUG,"audiometry",new Array());
    perawat_audiometry.setPrototipe(ADM_POLINAME,ADM_POLISLUG,"rawat");
    perawat_audiometry.setSuperCommand("perawat_audiometry");
    perawat_audiometry.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#audiometry_nama_perawat").val(nama);
        $("#audiometry_id_perawat").val(nip);
    };
    stypeahead("#audiometry_nama_perawat",3,perawat_audiometry,"nama",function(item){
        $("#audiometry_id_perawat").val(item.id);
        $("#audiometry_nama_perawat").val(item.nama);
        $("#audiometry_kelas").focus();
    });

    tarif_audiometry=new TableAction("tarif_audiometry",ADM_POLISLUG,"audiometry",new Array());
    tarif_audiometry.setPrototipe(ADM_POLINAME,ADM_POLISLUG,"rawat");
    tarif_audiometry.setSuperCommand("tarif_audiometry");
    tarif_audiometry.addRegulerData=function(d){
        d['noreg_pasien']=$("#audiometry_noreg_pasien").val();
        return d;
    };
    tarif_audiometry.selected=function(json){
        var kelas=json.kelas;
        var harga=Number(json.tarif);
        $("#audiometry_kelas").val(kelas);
        $("#audiometry_biaya").maskMoney('mask',harga);
    };
    stypeahead("#audiometry_kelas",3,tarif_audiometry,"kelas",function(item){
        $("#audiometry_kelas").val(item.kelas);
        setMoney("#audiometry_biaya",Number(item.tarif));
        $("#audiometry_save").focus();
    });
                    
});