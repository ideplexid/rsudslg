var gizi;		
var diet_pagi;
var diet_siang;
var diet_malam;
var menu_pagi;
var menu_siang;
var menu_malam;
var GZ_POLINAME;
var GZ_POLISLUG;

$(document).ready(function(){
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    GZ_POLINAME=$("#gz_poliname").val();
    GZ_POLISLUG=$("#gz_polislug").val();
    var column=new Array('id',
            "nama_pasien","noreg_pasien",'nrm_pasien',
            'menu_pagi','diet_pagi','pk_pagi',
            'menu_siang','diet_siang','pk_siang',
            'menu_malam','diet_malam','pk_malam'
            );
    gizi=new TableAction("gizi",GZ_POLISLUG,"gizi",column);
    gizi.setPrototipe(GZ_POLINAME,GZ_POLISLUG,"rawat");			
    gizi.view();

    diet_pagi=new TableAction("diet_pagi",GZ_POLISLUG,"gizi",new Array());
    diet_pagi.setPrototipe(GZ_POLINAME,GZ_POLISLUG,"rawat");
    diet_pagi.setSuperCommand("diet_pagi");
    diet_pagi.selected=function(json){
        $("#gizi_diet_pagi").val(json.nama);
    };

    diet_siang=new TableAction("diet_siang",GZ_POLISLUG,"gizi",new Array());
    diet_siang.setPrototipe(GZ_POLINAME,GZ_POLISLUG,"rawat");
    diet_siang.setSuperCommand("diet_siang");
    diet_siang.selected=function(json){
        $("#gizi_diet_siang").val(json.nama);
    };

    diet_malam=new TableAction("diet_malam",GZ_POLISLUG,"gizi",new Array());
    diet_malam.setPrototipe(GZ_POLINAME,GZ_POLISLUG,"rawat");
    diet_malam.setSuperCommand("diet_malam");
    diet_malam.selected=function(json){
        $("#gizi_diet_malam").val(json.nama);
    };


    menu_pagi=new TableAction("menu_pagi",GZ_POLISLUG,"gizi",new Array());
    menu_pagi.setPrototipe(GZ_POLINAME,GZ_POLISLUG,"rawat");
    menu_pagi.setSuperCommand("menu_pagi");
    menu_pagi.selected=function(json){
        $("#gizi_menu_pagi").val(json.nama);
    };

    menu_siang=new TableAction("menu_siang",GZ_POLISLUG,"gizi",new Array());
    menu_siang.setPrototipe(GZ_POLINAME,GZ_POLISLUG,"rawat");
    menu_siang.setSuperCommand("menu_siang");
    menu_siang.selected=function(json){
        $("#gizi_menu_siang").val(json.nama);
    };

    menu_malam=new TableAction("menu_malam",GZ_POLISLUG,"gizi",new Array());
    menu_malam.setPrototipe(GZ_POLINAME,GZ_POLISLUG,"rawat");
    menu_malam.setSuperCommand("menu_malam");
    menu_malam.selected=function(json){
        $("#gizi_menu_malam").val(json.nama);
    };


    
});