var dokter_spirometry;
var perawat_spirometry;
var spirometry;	
var tarif_spirometry;	
var SPR_POLISLUG;
var SPR_POLINAME;
var SPR_CARABAYAR;
$(document).ready(function(){
    SPR_POLISLUG=$("#spr_polislug").val();
	SPR_POLINAME=$("#spr_poliname").val();
	SPR_CARABAYAR=$("#spr_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id','id_dokter','id_perawat','waktu','kelas','biaya','nama_dokter','nama_perawat');
    spirometry=new RawatAction("spirometry",SPR_POLISLUG,"spirometry",column);
    spirometry.setPrototipe(SPR_POLINAME,SPR_POLISLUG,"rawat");
    spirometry.setEnableAutofocus(true);
    spirometry.setMultipleInput(true);
    spirometry.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#spirometry_noreg_pasien").val();
        reg_data['nama_pasien']=$("#spirometry_nama_pasien").val();
        reg_data['nrm_pasien']=$("#spirometry_nrm_pasien").val();
        reg_data['carabayar']=SPR_CARABAYAR;
        return reg_data;
    };
    spirometry.view();

    dokter_spirometry=new TableAction("dokter_spirometry",SPR_POLISLUG,"spirometry",new Array());
    dokter_spirometry.setSuperCommand("dokter_spirometry");
    dokter_spirometry.setPrototipe(SPR_POLINAME,SPR_POLISLUG,"rawat");
    dokter_spirometry.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#spirometry_nama_dokter").val(nama);
        $("#spirometry_id_dokter").val(nip);
    };
    stypeahead("#spirometry_nama_dokter",3,dokter_spirometry,"nama",function(item){
        $("#spirometry_id_dokter").val(item.id);
        $("#spirometry_nama_dokter").val(item.nama);
        $("#spirometry_nama_perawat").focus();
    });
    perawat_spirometry=new TableAction("perawat_spirometry",SPR_POLISLUG,"spirometry",new Array());
    perawat_spirometry.setPrototipe(SPR_POLINAME,SPR_POLISLUG,"rawat");
    perawat_spirometry.setSuperCommand("perawat_spirometry");
    perawat_spirometry.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#spirometry_nama_perawat").val(nama);
        $("#spirometry_id_perawat").val(nip);
        $("#spirometry_nama_perawat").focus();
    };
    stypeahead("#spirometry_nama_perawat",3,perawat_spirometry,"nama",function(item){
        $("#spirometry_id_perawat").val(item.id);
        $("#spirometry_nama_perawat").val(item.nama);
        $("#spirometry_kelas").focus();
    });

    tarif_spirometry=new TableAction("tarif_spirometry",SPR_POLISLUG,"spirometry",new Array());
    tarif_spirometry.setPrototipe(SPR_POLINAME,SPR_POLISLUG,"rawat");
    tarif_spirometry.setSuperCommand("tarif_spirometry");
    tarif_spirometry.addRegulerData=function(d){
        d['noreg_pasien']=$("#spirometry_noreg_pasien").val();
        return d;
    };
    tarif_spirometry.selected=function(json){
        var kelas=json.kelas;
        var harga=Number(json.tarif);
        $("#spirometry_kelas").val(kelas);
        $("#spirometry_biaya").maskMoney('mask',harga);
    };
    stypeahead("#spirometry_kelas",3,tarif_spirometry,"kelas",function(item){
        $("#spirometry_kelas").val(item.kelas);
        setMoney("#spirometry_biaya",Number(item.tarif));
        $("#spirometry_save").focus();
    });
                    
});