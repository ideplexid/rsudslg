var dokter_pengirim_ekg;
var dokter_pembaca_ekg;
var petugas_ekg;
var ekg;	
var tarif_ekg;
var EKG_POLISLUG;
var EKG_POLINAME;
var EKG_CARABAYAR;	
$(document).ready(function(){
    EKG_POLISLUG=$("#ekg_polislug").val();
	EKG_POLINAME=$("#ekg_poliname").val();
	EKG_CARABAYAR=$("#ekg_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id','id_dokter_pengirim','id_dokter_pembaca','id_petugas','waktu','kelas','biaya','nama_dokter_pengirim','nama_dokter_pembaca','nama_petugas');
    ekg=new RawatAction("ekg",EKG_POLISLUG,"ekg",column);
    ekg.setPrototipe(EKG_POLINAME,EKG_POLISLUG,"rawat");
    ekg.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#ekg_noreg_pasien").val();
        reg_data['nama_pasien']=$("#ekg_nama_pasien").val();
        reg_data['nrm_pasien']=$("#ekg_nrm_pasien").val();
        reg_data['carabayar']=EKG_CARABAYAR;
        return reg_data;
    };
    ekg.view();

    dokter_pengirim_ekg=new TableAction("dokter_pengirim_ekg",EKG_POLISLUG,"ekg",new Array());
    dokter_pengirim_ekg.setSuperCommand("dokter_pengirim_ekg");
    dokter_pengirim_ekg.setPrototipe(EKG_POLINAME,EKG_POLISLUG,"rawat");
    dokter_pengirim_ekg.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#ekg_nama_dokter_pengirim").val(nama);
        $("#ekg_id_dokter_pengirim").val(nip);
    };
    stypeahead("#ekg_nama_dokter_pengirim",3,dokter_pengirim_ekg,"nama",function(item){
        $("#ekg_id_dokter_pengirim").val(item.id);
        $("#ekg_nama_dokter_pengirim").val(item.nama);
        $("#ekg_nama_dokter_pembaca").focus();
    });

    dokter_pembaca_ekg=new TableAction("dokter_pembaca_ekg",EKG_POLISLUG,"ekg",new Array());
    dokter_pembaca_ekg.setPrototipe(EKG_POLINAME,EKG_POLISLUG,"rawat");
    dokter_pembaca_ekg.setSuperCommand("dokter_pembaca_ekg");
    dokter_pembaca_ekg.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#ekg_nama_dokter_pembaca").val(nama);
        $("#ekg_id_dokter_pembaca").val(nip);
    };
    stypeahead("#ekg_nama_dokter_pembaca",3,dokter_pembaca_ekg,"nama",function(item){
        $("#ekg_id_dokter_pembaca").val(item.id);
        $("#ekg_nama_dokter_pembaca").val(item.nama);
        $("#ekg_nama_petugas").focus();
    });

    petugas_ekg=new TableAction("petugas_ekg",EKG_POLISLUG,"ekg",new Array());
    petugas_ekg.setPrototipe(EKG_POLINAME,EKG_POLISLUG,"rawat");
    petugas_ekg.setSuperCommand("petugas_ekg");
    petugas_ekg.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#ekg_nama_petugas").val(nama);
        $("#ekg_id_petugas").val(nip);
    };
    stypeahead("#ekg_nama_petugas",3,dokter_pembaca_ekg,"nama",function(item){
        $("#ekg_id_petugas").val(item.id);
        $("#ekg_nama_petugas").val(item.nama);
        $("#ekg_kelas").focus();
    });

    tarif_ekg=new TableAction("tarif_ekg",EKG_POLISLUG,"ekg",new Array());
    tarif_ekg.setPrototipe(EKG_POLINAME,EKG_POLISLUG,"rawat");
    tarif_ekg.setSuperCommand("tarif_ekg");
    tarif_ekg.addRegulerData=function(d){
        d['noreg_pasien']=$("#ekg_noreg_pasien").val();
        return d;
    };
    tarif_ekg.selected=function(json){
        var kelas=json.kelas;
        var harga=Number(json.tarif);
        $("#ekg_kelas").val(kelas);
        $("#ekg_biaya").maskMoney('mask',harga);
    };
    stypeahead("#ekg_kelas",3,tarif_ekg,"kelas",function(item){
        $("#ekg_kelas").val(item.kelas);
        setMoney("#ekg_biaya",Number(item.tarif));
        $("#ekg_save").focus();
    });
    
});