var alok;	
var tarif_alok;
var ALOK_POLINAME;
var ALOK_POLISLUG;
var ALOK_CARABAYAR;
var ALOK_RUANGAN;

$(document).ready(function(){
	ALOK_POLINAME=$("#alok_poliname").val();
	ALOK_POLISLUG=$("#alok_polislug").val();
	ALOK_CARABAYAR=$("#alok_carabayar").val();
	ALOK_RUANGAN=$("#alok_ruangan").val();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id','id_obat','tanggal','nama','satuan','kode','harga','jumlah',"sisa");
	alok=new RawatAction("alok",ALOK_POLISLUG,"alok",column);
	alok.setEnableAutofocus(true);
	alok.setMultipleInput(true);
	alok.setNextEnter();
	alok.setPrototipe(ALOK_POLINAME,ALOK_POLISLUG,"rawat");
	alok.getRegulerData=function(){
		reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#alok_noreg_pasien").val();
		reg_data['nama_pasien']=$("#alok_nama_pasien").val();
		reg_data['nrm_pasien']=$("#alok_nrm_pasien").val();						
		reg_data['carabayar']=ALOK_CARABAYAR;
		return reg_data;
	};
	alok.view();

	tarif_alok=new TableAction("tarif_alok",ALOK_POLISLUG,"alok",new Array());
	tarif_alok.setPrototipe(ALOK_POLINAME,ALOK_POLISLUG,"rawat");
	tarif_alok.setSuperCommand("tarif_alok");
	tarif_alok.selected=function(json){
		$("#alok_sisa").val(json.sisa);
		$("#alok_id_obat").val(json.id_obat);
		$("#alok_nama").val(json.nama);
		$("#alok_satuan").val(json.satuan);
		$("#alok_kode").val(json.kode);
		$("#alok_harga").maskMoney('mask',Number(json.harga) );
	};

	stypeahead("#alok_nama",3,tarif_alok,"nama",function(item){
		tarif_alok.select(item.id);	
		$("#alok_jumlah").focus();	
	});
	
	$("#alok_id_obat").keyup(function(e){
		if($(this).attr("type")=="text" && e.which == 13){
			var data=tarif_alok.getRegulerData();
			data['command']="scan";
			data['id_obat']=$(this).val();
			showLoading();
			$.post("",data,function(res){
				var json=getContent(res);
				if(json!=null){
					$("#alok_sisa").val(json.sisa);
					$("#alok_id_obat").val(json.id_obat);
					$("#alok_nama").val(json.nama);
					$("#alok_satuan").val(json.satuan);
					$("#alok_kode").val(json.kode);
					$("#alok_harga").maskMoney('mask',Number(json.harga) );
					$("#alok_jumlah").focus();					
				}else{
					$("#alok_sisa").val("");
					$("#alok_id_obat").val("");
					$("#alok_nama").val("");
					$("#alok_satuan").val("");
					$("#alok_kode").val("");
					$("#alok_harga").maskMoney('mask',0 );
					$("#alok_id_obat").focus();
				}
				dismissLoading();
			});
		}
	});
	
	
});