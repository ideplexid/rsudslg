var dokter_bronchoscopy;
var perawat_bronchoscopy;
var bronchoscopy;	
var tarif_bronchoscopy;
var BCC_POLISLUG;
var BCC_POLINAME;
var BCC_CARABAYAR;
$(document).ready(function(){
    BCC_POLISLUG=$("#bcc_polislug").val();
	BCC_POLINAME=$("#bcc_poliname").val();
	BCC_CARABAYAR=$("#bcc_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id','id_dokter','id_perawat','waktu','kelas','biaya','nama_dokter','nama_perawat');
    bronchoscopy=new RawatAction("bronchoscopy",BCC_POLISLUG,"bronchoscopy",column);
    bronchoscopy.setPrototipe(BCC_POLINAME,BCC_POLISLUG,"rawat");
    bronchoscopy.setEnableAutofocus(true);
    bronchoscopy.setMultipleInput(true);
    bronchoscopy.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#bronchoscopy_noreg_pasien").val();
        reg_data['nama_pasien']=$("#bronchoscopy_nama_pasien").val();
        reg_data['nrm_pasien']=$("#bronchoscopy_nrm_pasien").val();
        reg_data['carabayar']=$("#carabayar").val();
        return reg_data;
    };
    bronchoscopy.view();

    dokter_bronchoscopy=new TableAction("dokter_bronchoscopy",BCC_POLISLUG,"bronchoscopy",new Array());
    dokter_bronchoscopy.setSuperCommand("dokter_bronchoscopy");
    dokter_bronchoscopy.setPrototipe(BCC_POLINAME,BCC_POLISLUG,"rawat");
    dokter_bronchoscopy.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#bronchoscopy_nama_dokter").val(nama);
        $("#bronchoscopy_id_dokter").val(nip);
    };
    stypeahead("#bronchoscopy_nama_dokter",3,dokter_bronchoscopy,"nama",function(item){
        $("#bronchoscopy_id_dokter").val(item.id);
        $("#bronchoscopy_nama_dokter").val(item.nama);
        $("#bronchoscopy_nama_perawat").focus();
    });

    perawat_bronchoscopy=new TableAction("perawat_bronchoscopy",BCC_POLISLUG,"bronchoscopy",new Array());
    perawat_bronchoscopy.setPrototipe(BCC_POLINAME,BCC_POLISLUG,"rawat");
    perawat_bronchoscopy.setSuperCommand("perawat_bronchoscopy");
    perawat_bronchoscopy.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#bronchoscopy_nama_perawat").val(nama);
        $("#bronchoscopy_id_perawat").val(nip);
    };
    stypeahead("#bronchoscopy_nama_perawat",3,perawat_bronchoscopy,"nama",function(item){
        $("#bronchoscopy_id_perawat").val(item.id);
        $("#bronchoscopy_nama_perawat").val(item.nama);
        $("#bronchoscopy_kelas").focus();
    });

    tarif_bronchoscopy=new TableAction("tarif_bronchoscopy",BCC_POLISLUG,"bronchoscopy",new Array());
    tarif_bronchoscopy.setPrototipe(BCC_POLINAME,BCC_POLISLUG,"rawat");
    tarif_bronchoscopy.setSuperCommand("tarif_bronchoscopy");
    tarif_bronchoscopy.addRegulerData=function(d){
        d['noreg_pasien']=$("#bronchoscopy_noreg_pasien").val();
        return d;
    };
    tarif_bronchoscopy.selected=function(json){
        var kelas=json.kelas;
        var harga=Number(json.tarif);
        $("#bronchoscopy_kelas").val(kelas);
        $("#bronchoscopy_biaya").maskMoney('mask',harga);					
    };
    stypeahead("#bronchoscopy_kelas",3,tarif_bronchoscopy,"kelas",function(item){
        $("#bronchoscopy_kelas").val(item.kelas);
        setMoney("#bronchoscopy_biaya",Number(item.tarif));
        $("#bronchoscopy_save").focus();
    });
    
});