var antrian;
var proto_name;
var proto_slug;
var proto_imp;
var polislug;
var poliname;
var antrian_mode;
var unit_rujukan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({minuteStep:1});
	proto_name	 = $("#antrian_proto_name").val();
	proto_slug	 = $("#antrian_proto_slug").val();
	proto_imp	 = $("#antrian_proto_implement").val();
	polislug	 = $("#antrian_poliname").val();
	poliname	 = $("#antrian_polislug").val();
	antrian_mode = $("#antrian_mode").val();
	var column	 = new Array("id","no_register","selesai",
							 "nama_pasien","nrm_pasien","waktu_keluar",
							 "cara_keluar","keterangan_keluar","asal",
							 "kelas","jk","carabayar","ibu_kandung","dokumen",
							 "id_rs_rujuk","nama_rs","nama_unit","is_spesialistik");
	antrian		 = new TableAction("antrian",polislug,"antrian",column);
	antrian.setPrototipe(polislug,poliname,"rawat");
	antrian.getSaveData = function(){
		var save_data		     = this.getRegulerData();
		save_data['command']     = "save";
		for(var i=0;i<this.column.length;i++){
			var name		     = this.column[i];
			save_data[name]	     = $("#"+this.prefix+"_"+name).val();
		}
		if("Integrated"==antrian_mode){
			save_data['selesai'] = 1;
		}
		return save_data;
	};

	antrian.resume_medis = function(id){
		var data   = {
			page                : polislug,
			action              : "resumemedis",
			id_antrian          : id,
			id					: id,
			prototype_name      : proto_name,
			prototype_slug      : proto_slug,
			prototype_implement : proto_imp
		};
		showLoading();
		$.post("",data,function(res){
			showFullWarning("Resume Medis",res,"half_model");
			dismissLoading();
		})
	}
	
	antrian.layani = function(id){
		var data   = {
			page                : polislug,
			action              : "layanan",
			id_antrian          : id,
			prototype_name      : proto_name,
			prototype_slug      : proto_slug,
			prototype_implement : proto_imp
		};
		LoadSmisPage(data);
	};
    
    antrian.titipan = function(id){
		var data		= this.getRegulerData();
		data['command']	= "titipan";
		data['id']		= id;
        showLoading();
		$.post("",data,function(res){
			var json	= getContent(res);
            antrian.view();
            dismissLoading();
		});
	};

	antrian.skb	= function(id){
		var data		= this.getRegulerData();
		data['command']	= "skb";
		data['id']		= id;
		$.post("",data,function(res){
			var json	= getContent(res);
			smis_print(json);
		});
	};

	antrian.save_action = function(a){
		var self = this;
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			var json = getContent(res);
			if(json==null) {
				$("#"+self.prefix+"_add_form").smodal('hide');
				return;		
			}else if(json.success=="1"){
				if(self.multiple_input) {
					if(self.focus_on_multi!=""){
						$('#'+self.action+'_'+self.focus_on_multi).focus();
					}else{
						$('#'+self.action+'_add_form').find('[autofocus]').focus();					
					}
				}else {
					$("#"+self.prefix+"_add_form").smodal('hide');
				}
				self.view();
				self.clear();
			}
			dismissLoading();
			self.is_post_action_del=false;
			if(a['id']!=""){
				self.postAction(a['id']);    
			}else{
				self.postAction(json['id']);
			}
		}});
		this.aftersave();
	};

	antrian.edit = function(id) {
		$("#modal_alert_antrian_add_form").empty();
		TableAction.prototype.edit.call(this, id);
	};

	antrian.save = function (){
		var self	= this;

		var data = this.getRegulerData();
		data['super_command'] = "validasi_spesialisasi";
		data['id'] = $("#antrian_id").val();
		data['carapulang'] = $("#antrian_cara_keluar").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return self;
				if (json.valid === "1") {
					self.beforesave();
					var a		= self.getSaveData();
					$("#"+self.prefix+"_add_form").smodal('hide');
					if($("#antrian_exit_mode").val()=="1"){
						bootbox.confirm("Apakah anda yakin mengeluarkan Pasien tersebut dari Rumah Sakit ? ", function(result) {
							if(result){
								bootbox.hideAll();
								self.save_action(a);
								return true;
							}else{
								$("#"+self.prefix+"_add_form").smodal('show');
							}
						 });	
					}else{
						self.save_action(a);
					}
					return self;
				}
				$("#modal_alert_antrian_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<b>Jenis Spesialisasi Pasien Harus Diisi</b><br/>" +
					"</div>"
				);
				return self;
			}
		);
	};

	antrian.save_document = function(id,nilai){
		var data		  = this.getRegulerData();
		data['command']	  = "save_dokumen";
		data['id']		  = id;
		data['dokumen']	  = nilai;
		showLoading();
		$.post("",data,function(res){
			var json	  = getContent(res);
			dismissLoading();
		});
	};
	$("#"+antrian.prefix+"_cara_keluar").on("change",function(){
		var nilai = $(this).val();
		if(nilai.indexOf("Rujuk") !== -1){
			$(".fcontainer_"+antrian.prefix+"_id_rs_rujuk").show();
			$(".fcontainer_"+antrian.prefix+"_nama_rs").show();
			$(".fcontainer_"+antrian.prefix+"_nama_unit").show();
			$(".fcontainer_"+antrian.prefix+"_is_spesialistik").show();
		}else{
			smis_edit("#"+antrian.prefix+"_id_rs_rujuk","");
			smis_edit("#"+antrian.prefix+"_nama_rs","");
			smis_edit("#"+antrian.prefix+"_nama_unit","");
			smis_edit("#"+antrian.prefix+"_is_spesialistik","0");
			$(".fcontainer_"+antrian.prefix+"_id_rs_rujuk").hide();
			$(".fcontainer_"+antrian.prefix+"_nama_rs").hide();
			$(".fcontainer_"+antrian.prefix+"_nama_unit").hide();
			$(".fcontainer_"+antrian.prefix+"_is_spesialistik").hide();
		}
	});
	$("#"+antrian.prefix+"_cara_keluar").trigger("change");
	antrian.view();

	unit_rujukan = new TableAction("unit_rujukan",polislug,"antrian",new Array());
	unit_rujukan.setPrototipe(polislug,poliname,"rawat");
	unit_rujukan.setSuperCommand("unit_rujukan");
	unit_rujukan.selected = function(json){
		smis_edit("#"+antrian.prefix+"_id_rs_rujuk",json.id);
		smis_edit("#"+antrian.prefix+"_nama_rs",json.nama);
		smis_edit("#"+antrian.prefix+"_nama_unit",json.poli);
		smis_edit("#"+antrian.prefix+"_is_spesialistik",json.is_spesialis);
	};
	
});