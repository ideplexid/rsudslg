var tindakan_dokter;		
var tarif_tindakan_dokter;
var dokter_tindakan_dokter;
var perawat_tindakan_dokter;
var perawat_tindakan_dokter_dua;
var perawat_tindakan_dokter_tiga;	
var perawat_tindakan_dokter_empat;
var perawat_tindakan_dokter_lima;	
var perawat_tindakan_dokter_enam;	
var perawat_tindakan_dokter_tujuh;	
var perawat_tindakan_dokter_delapan;	
var perawat_tindakan_dokter_sembilan;	
var perawat_tindakan_dokter_sepuluh;	

var TDOKTER_POLISLUG;
var TDOKTER_POLINAME;
var TDOKTER_CARABAYAR;
var TDOKTER_JML_PERAWAT;
var TDOKTER_JML_PERAWAT_ACTION;
$(document).ready(function(){
	$("#tindakan_dokter_nama_dokter").attr("empty","n");
	TDOKTER_POLISLUG=$("#tdk_polislug").val();
	TDOKTER_POLINAME=$("#tdk_poliname").val();
	TDOKTER_CARABAYAR=$("#tdk_carabayar").val();
	TDOKTER_JML_PERAWAT_ACTION=new Array();
	TDOKTER_JML_PERAWAT=new Array();
	TDOKTER_JML_PERAWAT[1]="";
	TDOKTER_JML_PERAWAT[2]="_dua";
	TDOKTER_JML_PERAWAT[3]="_tiga";
	TDOKTER_JML_PERAWAT[4]="_empat";
	TDOKTER_JML_PERAWAT[5]="_lima";
	TDOKTER_JML_PERAWAT[6]="_enam";
	TDOKTER_JML_PERAWAT[7]="_tujuh";
	TDOKTER_JML_PERAWAT[8]="_delapan";
	TDOKTER_JML_PERAWAT[9]="_sembilan";
	TDOKTER_JML_PERAWAT[10]="_sepuluh";
	
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	$('.mydatetime').datetimepicker({minuteStep:1});
	var column=new Array(	'id',"nama_icd","kode_icd","kode_dtd","kode_grup",
							'id_dokter','nama_dokter','jenis_dokter',
							'nama_tindakan','harga_perawat',"nama_pasien",
                            "noreg_pasien","nrm_pasien",'waktu','harga',
							"bagi_dokter","id_tindakan","jaspel_nilai","jaspel_persen",
							"jaspel_nilai_perawat","jaspel_persen_perawat"
							);
	
	for(var i=1;i<=TDOKTER_JML_PERAWAT.length;i++){
		var num=TDOKTER_JML_PERAWAT[i];
		column.push("nama_perawat"+num);
		column.push("id_perawat"+num);
	}							
	
	tindakan_dokter=new RawatAction("tindakan_dokter",TDOKTER_POLISLUG,"tindakan_dokter",column);
	tindakan_dokter.setPrototipe(TDOKTER_POLINAME,TDOKTER_POLISLUG,"rawat");
	tindakan_dokter.setEnableAutofocus(true);
	tindakan_dokter.setMultipleInput(true);
	tindakan_dokter.setNextEnter();
	tindakan_dokter.addEmployee("id_dokter","nama_dokter","Dokter");
	tindakan_dokter.addEmployee("id_perawat","nama_perawat","Perawat I");
	tindakan_dokter.addEmployee("id_perawat_dua","nama_perawat_dua","Perawat II");
	tindakan_dokter.addEmployee("id_perawat_tiga","nama_perawat_tiga","Perawat III");
	tindakan_dokter.addEmployee("id_perawat_empat","nama_perawat_empat","Perawat IV");
	tindakan_dokter.addEmployee("id_perawat_lima","nama_perawat_lima","Perawat V");
	tindakan_dokter.addEmployee("id_perawat_enam","nama_perawat_enam","Perawat VI");
	tindakan_dokter.addEmployee("id_perawat_tujuh","nama_perawat_tujuh","Perawat VII");
	tindakan_dokter.addEmployee("id_perawat_delapan","nama_perawat_delapan","Perawat VIII");
	tindakan_dokter.addEmployee("id_perawat_sembilan","nama_perawat_sembilan","Perawat IX");
	tindakan_dokter.addEmployee("id_perawat_sepuluh","nama_perawat_sepuluh","Perawat X");
	tindakan_dokter.getRegulerData=function(){
		reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#tindakan_dokter_noreg_pasien").val();
		reg_data['nama_pasien']=$("#tindakan_dokter_nama_pasien").val();
		reg_data['nrm_pasien']=$("#tindakan_dokter_nrm_pasien").val();
		reg_data['carabayar']=TDOKTER_CARABAYAR;
		return reg_data;
	};
	tindakan_dokter.view();
    
    

	dokter_tindakan_dokter=new TableAction("dokter_tindakan_dokter",TDOKTER_POLISLUG,"tindakan_dokter",new Array());
	dokter_tindakan_dokter.setPrototipe(TDOKTER_POLINAME,TDOKTER_POLISLUG,"rawat");
	dokter_tindakan_dokter.setSuperCommand("dokter_tindakan_dokter");
	dokter_tindakan_dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var slug=json.slug;
		$("#tindakan_dokter_nama_dokter").val(nama);
		$("#tindakan_dokter_id_dokter").val(nip);
		$("#tindakan_dokter_jenis_dokter").val(slug);
	};
	stypeahead("#tindakan_dokter_nama_dokter",3,dokter_tindakan_dokter,"nama",function(item){
		$("#tindakan_dokter_id_dokter").val(item.id);
		$("#tindakan_dokter_nama_dokter").val(item.nama);
		$("#tindakan_dokter_harga").focus();
	});
	
	for(var i=1;i<=TDOKTER_JML_PERAWAT.length;i++){
		var num=TDOKTER_JML_PERAWAT[i];
		var pttd=new TableAction("perawat_tindakan_dokter"+num,TDOKTER_POLISLUG,"tindakan_dokter",new Array());
			pttd.setPrototipe(TDOKTER_POLINAME,TDOKTER_POLISLUG,"rawat");
			pttd.numero=num;
			pttd.setSuperCommand("perawat_tindakan_dokter"+pttd.numero);
			pttd.selected=function(json){
				console.log(this);
				var nama=json.nama;
				var nip=json.id;
				$("#tindakan_dokter_nama_perawat"+this.numero).val(nama);
				$("#tindakan_dokter_id_perawat"+this.numero).val(nip);
			};
		$("#tindakan_dokter_nama_perawat"+num).data("numero",num);
		$("#tindakan_dokter_nama_perawat"+num).data("i",i);
		stypeahead("#tindakan_dokter_nama_perawat"+num,3,pttd,"nama",function(item,id){
			var numero=$(id).data("numero");
			var the_i=Number($(id).data("i"));
			$("#tindakan_dokter_id_perawat"+numero).val(item.id);
			$("#tindakan_dokter_nama_perawat"+numero).val(item.nama);
			if(the_i<TDOKTER_JML_PERAWAT.length){
				var next_num=TDOKTER_JML_PERAWAT[the_i+1];
				if($("#tindakan_dokter_nama_perawat"+next_num).attr("type")=="hidden"){
					$("#tindakan_dokter_harga_perawat").focus();
				}else{
					$("#tindakan_dokter_nama_perawat"+next_num).focus();
				}
			}
		});
		TDOKTER_JML_PERAWAT_ACTION[i]=pttd;			
	}
	
	perawat_tindakan_dokter=TDOKTER_JML_PERAWAT_ACTION[1];
	perawat_tindakan_dokter_dua=TDOKTER_JML_PERAWAT_ACTION[2];
	perawat_tindakan_dokter_tiga=TDOKTER_JML_PERAWAT_ACTION[3];	
	perawat_tindakan_dokter_empat=TDOKTER_JML_PERAWAT_ACTION[4];
	perawat_tindakan_dokter_lima=TDOKTER_JML_PERAWAT_ACTION[5];	
	perawat_tindakan_dokter_enam=TDOKTER_JML_PERAWAT_ACTION[6];	
	perawat_tindakan_dokter_tujuh=TDOKTER_JML_PERAWAT_ACTION[7];	
	perawat_tindakan_dokter_delapan=TDOKTER_JML_PERAWAT_ACTION[8];	
	perawat_tindakan_dokter_sembilan=TDOKTER_JML_PERAWAT_ACTION[9];	
	perawat_tindakan_dokter_sepuluh=TDOKTER_JML_PERAWAT_ACTION[10];

	mr_icd_tindakan=new TableAction("mr_icd_tindakan",TDOKTER_POLISLUG,"tindakan_dokter",new Array("id","nama","icd","grup","dtd"));
	mr_icd_tindakan.setSuperCommand("mr_icd_tindakan");
	mr_icd_tindakan.setPrototipe(TDOKTER_POLINAME,TDOKTER_POLISLUG,"rawat");
	mr_icd_tindakan.selected=function(json){
		$("#tindakan_dokter_kode_icd").val(json.icd);
		$("#tindakan_dokter_nama_icd").val(json.nama);
		$("#tindakan_dokter_kode_dtd").val(json.dtd);						
		$("#tindakan_dokter_kode_grup").val(json.grup);						
	};

	tarif_tindakan_dokter=new TableAction("tarif_tindakan_dokter",TDOKTER_POLISLUG,"tindakan_dokter",new Array());
	tarif_tindakan_dokter.setPrototipe(TDOKTER_POLINAME,TDOKTER_POLISLUG,"rawat");
	tarif_tindakan_dokter.setSuperCommand("tarif_tindakan_dokter");
	tarif_tindakan_dokter.addRegulerData=function(d){
		d['noreg_pasien']=$("#tindakan_dokter_noreg_pasien").val();
		d['carabayar']=$("#carabayar").val();
		return d;
	};
	tarif_tindakan_dokter.selected=function(json){
		$("#tindakan_dokter_nama_dokter").val(json.nama_dokter);
		$("#tindakan_dokter_id_dokter").val(json.id_dokter);
		setMoney("#tindakan_dokter_harga",Number(json.tarif));
		setMoney("#tindakan_dokter_harga_perawat",Number(json.perawat));
		$("#tindakan_dokter_nama_tindakan").val(json.nama);
        $("#tindakan_dokter_bagi_dokter").val(json.jaspel);
        $("#tindakan_dokter_id_tindakan").val(json.id);

        $("#tindakan_dokter_jaspel_nilai").val(json.jaspel);
		$("#tindakan_dokter_jaspel_persen").val(json.jaspel_persen);
		$("#tindakan_dokter_jaspel_nilai_perawat").val(json.asisten);
        $("#tindakan_dokter_jaspel_persen_perawat").val(json.asisten_persen);
	};
	stypeahead("#tindakan_dokter_nama_tindakan",3,tarif_tindakan_dokter,"nama",function(item){
		$("#tindakan_dokter_nama_dokter").val(item.nama_dokter);
		$("#tindakan_dokter_id_dokter").val(item.id_dokter);
		setMoney("#tindakan_dokter_harga",Number(item.tarif));
		setMoney("#tindakan_dokter_harga_perawat",Number(item.perawat));
        $("#tindakan_dokter_bagi_dokter").val(item.jaspel);
		$("#tindakan_dokter_nama_perawat").focus();
        $("#tindakan_dokter_id_tindakan").val(item.id);
        $("#tindakan_dokter_jaspel_nilai").val(item.jaspel);
		$("#tindakan_dokter_jaspel_persen").val(item.jaspel_persen);
		$("#tindakan_dokter_jaspel_nilai_perawat").val(item.asisten);
        $("#tindakan_dokter_jaspel_persen_perawat").val(item.asisten_persen);
	});
});

