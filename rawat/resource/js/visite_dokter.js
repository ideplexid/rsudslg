var visite_dokter;		
var tarif_visite;
var dokter_visite;
var dokter_visite_dua;
var dokter_visite_tiga;

var vst_polislug;
var vst_poliname;
var vst_carabayar;

$(document).ready(function(){
    $("#visite_dokter_nama_dokter").attr("empty","n");
	vst_polislug=$("#vst_polislug").val();
	vst_poliname=$("#vst_poliname").val();
	vst_carabayar=$("#vst_carabayar").val();
	
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	var column=new Array(
			'id',"nama_pasien","noreg_pasien","nrm_pasien",
			'waktu','kelas','harga',
			'nama_dokter','id_dokter',
			'nama_dokter_dua','id_dokter_dua',
			'nama_dokter_tiga','id_dokter_tiga',
			"nama_visite","id_visite","jaspel_nilai","jaspel_persen");
	visite_dokter=new RawatAction("visite_dokter",vst_polislug,"visite_dokter",column);
	visite_dokter.setPrototipe(vst_poliname,vst_polislug,"rawat");
	visite_dokter.setEnableAutofocus(true);
	visite_dokter.setMultipleInput(true);
	visite_dokter.setNextEnter();
	visite_dokter.addEmployee("id_dokter","nama_dokter","Dokter");
	visite_dokter.addEmployee("id_dokter_dua","nama_dokter_dua","Dokter II");
	visite_dokter.addEmployee("id_dokter_tiga","nama_dokter_tiga","Dokter III");
	visite_dokter.getRegulerData=function(){
		reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#visite_dokter_noreg_pasien").val();
		reg_data['nama_pasien']=$("#visite_dokter_nama_pasien").val();
		reg_data['nrm_pasien']=$("#visite_dokter_nrm_pasien").val();
		reg_data['carabayar']=vst_carabayar;
		return reg_data;
	};
	visite_dokter.view();

	tarif_visite=new TableAction("tarif_visite",vst_polislug,"visite_dokter",new Array());
	tarif_visite.setPrototipe(vst_poliname,vst_polislug,"rawat");
	tarif_visite.setSuperCommand("tarif_visite");
	tarif_visite.addRegulerData=function(d){
		d['noreg_pasien']=$("#visite_dokter_noreg_pasien").val();
		return d;
	};
	tarif_visite.selected=function(json){
		var nama=json.nama_dokter;
		var id_dokter=json.id_dokter;		
		var kelas=json.kelas;
		var harga=Number(json.tarif);
		if($("#is_visite_separated").val()!="1"){
			$("#visite_dokter_nama_dokter").val(nama);
			$("#visite_dokter_id_dokter").val(id_dokter);
		}				
		
		$("#visite_dokter_nama_visite").val(json.nama_visite);
		$("#visite_dokter_id_visite").val(json.id);
        $("#visite_dokter_kelas").val(kelas);
        $("#visite_dokter_jaspel_nilai").val(json.jaspel);
        $("#visite_dokter_jaspel_persen").val(json.jaspel_persen);
		$("#visite_dokter_harga").maskMoney('mask',harga);
		
	};

	dokter_visite=new TableAction("dokter_visite",vst_polislug,"visite_dokter",new Array());
	dokter_visite.setPrototipe(vst_poliname,vst_polislug,"rawat");
	dokter_visite.setSuperCommand("dokter_visite");
	dokter_visite.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var slug=json.slug;
		$("#visite_dokter_nama_dokter").val(nama);
		$("#visite_dokter_id_dokter").val(nip);
		$("#visite_dokter_nama_visite").focus();
	};
	
	dokter_visite_dua=new TableAction("dokter_visite_dua",vst_polislug,"visite_dokter",new Array());
	dokter_visite_dua.setPrototipe(vst_poliname,vst_polislug,"rawat");
	dokter_visite_dua.setSuperCommand("dokter_visite_dua");
	dokter_visite_dua.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var slug=json.slug;
		$("#visite_dokter_nama_dokter_dua").val(nama);
		$("#visite_dokter_id_dokter_dua").val(nip);
		$("#visite_dokter_nama_visite_dua").focus();
	};
	
	dokter_visite_tiga=new TableAction("dokter_visite_tiga",vst_polislug,"visite_dokter",new Array());
	dokter_visite_tiga.setPrototipe(vst_poliname,vst_polislug,"rawat");
	dokter_visite_tiga.setSuperCommand("dokter_visite_tiga");
	dokter_visite_tiga.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var slug=json.slug;
		$("#visite_dokter_nama_dokter_tiga").val(nama);
		$("#visite_dokter_id_dokter_tiga").val(nip);
		$("#visite_dokter_nama_visite_tiga").focus();
	};

	if($("#is_visite_separated").val()!="1"){
		stypeahead("#visite_dokter_nama_dokter",3,tarif_visite,"nama_dokter",function(item){
			tarif_visite.selected(item);
			if($("#visite_dokter_nama_dokter_dua").is("hidden")){
				$("#visite_dokter_save").focus();
			}else{
				$("#visite_dokter_nama_dokter_dua").focus();
			}
			return item.name;
		});
	}else{
		stypeahead("#visite_dokter_nama_dokter",3,dokter_visite,"nama",function(item){
			dokter_visite.selected(item);
			if($("#visite_dokter_nama_dokter_dua").is("hidden")){
				$("#visite_dokter_nama_visite").focus();
			}else{
				$("#visite_dokter_nama_dokter_dua").focus();
			}
			return item.name;
		});
	}
	

	stypeahead("#visite_dokter_nama_visite",3,tarif_visite,"nama_visite",function(item){
		tarif_visite.selected(item);		
		if($("#visite_dokter_nama_dokter_dua").is("hidden")){
				$("#visite_dokter_save").focus();
		}else{
			$("#visite_dokter_nama_dokter_dua").focus();
		}
		return item.name;
	});
	
	stypeahead("#visite_dokter_nama_visite_dua",3,dokter_visite_dua,"nama",function(item){
		tarif_visite.selected(item);		
		if($("#visite_dokter_nama_dokter_dua").is("hidden")){
				$("#visite_dokter_save").focus();
		}else{
			$("#visite_dokter_nama_dokter_dua").focus();
		}
		return item.name;
	});
	
	stypeahead("#visite_dokter_nama_visite_tiga",3,dokter_visite_tiga,"nama",function(item){
		tarif_visite.selected(item);		
		if($("#visite_dokter_nama_dokter_tiga").is("hidden")){
				$("#visite_dokter_save").focus();
		}else{
			$("#visite_dokter_nama_dokter_tiga").focus();
		}
		return item.name;
	});
	
	
});