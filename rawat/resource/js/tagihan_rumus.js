var tagihan_rumus;		
var rumus_tagihan_rumus;
var tagihan_rumus_carabayar;
var tagihan_rumus_poliname;
var tagihan_rumus_polislug;
var dpjp_satu_tagihan_rumus;
var dpjp_dua_tagihan_rumus;
var dpjp_tiga_tagihan_rumus;
var konsul_satu_tagihan_rumus;
var konsul_dua_tagihan_rumus;
var anak_tagihan_rumus;
var umum_tagihan_rumus;
var spesialis_tagihan_rumus;
$(document).ready(function(){	
    
    $(".mydatetime").datetimepicker({minuteStep:1});
    
	tagihan_rumus_carabayar=$("#tagihan_rumus_carabayar").val();
	tagihan_rumus_poliname=$("#tagihan_rumus_poliname").val();
	tagihan_rumus_polislug=$("#tagihan_rumus_polislug").val();

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array(
			"nama_pasien","noreg_pasien","nrm_pasien",
			'kelas_plafon','kode_inacbg','deskripsi_inacbg',
            'ruang_pelayanan','id_rumus','nama_rumus',
            'rumus_tagihan','rumus_biaya','rumus_potongan',
            'tarif_plafon_inacbg','tarif_plafon_kelas1',
            'total_tagihan','total_potongan','total_biaya',
            'id_dpjp_satu','nama_dpjp_satu','id_dpjp_dua','nama_dpjp_dua','id_dpjp_tiga','nama_dpjp_tiga',
            'id_konsul_satu','nama_konsul_satu','id_konsul_dua','nama_konsul_dua',
            'id_anak','nama_anak','id_igd','nama_igd','id_anastesi','nama_anastesi',
            'id_umum','nama_umum','id_spesialis','nama_spesialis',
            'biaya_dpjp_satu','biaya_dpjp_dua','biaya_dpjp_tiga',
            'id_rumus_pv','keterangan_pv',
            'mulai_dpjp_satu','mulai_dpjp_dua','mulai_dpjp_tiga',
            'selesai_dpjp_satu','selesai_dpjp_dua','selesai_dpjp_tiga'
            );
	tagihan_rumus=new RawatAction("tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",column);
	tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	tagihan_rumus.getRegulerData=function(){
		reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#tagihan_rumus_noreg_pasien").val();
		reg_data['nama_pasien']=$("#tagihan_rumus_nama_pasien").val();
		reg_data['nrm_pasien']=$("#tagihan_rumus_nrm_pasien").val();
		return reg_data;
	};
	
	tagihan_rumus.save=function(){
        if(!this.cekSave()){
            return;
        }
        
        this.beforesave();
        var self=this;
    
		var ds=this.getSaveData();
		ds['command']="save";
		showLoading();
		$.post("",ds,function(res){
			layanan.ReLoadData('tagihan_rumus');
			dismissLoading();
		});					
	};
	
	rumus_tagihan_rumus=new TableAction("rumus_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	rumus_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	rumus_tagihan_rumus.setSuperCommand("rumus_tagihan_rumus");
	rumus_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_rumus").val(json.id);
		$("#tagihan_rumus_nama_rumus").val(json.nama);
        $("#tagihan_rumus_rumus_tagihan").val(json.rumus_tagihan);
		$("#tagihan_rumus_rumus_biaya").val(json.rumus_biaya);
        $("#tagihan_rumus_rumus_potongan").val(json.rumus_potongan);
        $("#tagihan_rumus_deskripsi_inacbg").val(json.keterangan);
	};
    
    anastesi_tagihan_rumus=new TableAction("anastesi_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	anastesi_tagihan_rumus.setSuperCommand("anastesi_tagihan_rumus");
	anastesi_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	anastesi_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_anastesi").val(json.id);
		$("#tagihan_rumus_nama_anastesi").val(json.nama);
	};
    
    dpjp_satu_tagihan_rumus=new TableAction("dpjp_satu_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	dpjp_satu_tagihan_rumus.setSuperCommand("dpjp_satu_tagihan_rumus");
	dpjp_satu_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	dpjp_satu_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_dpjp_satu").val(json.id);
		$("#tagihan_rumus_nama_dpjp_satu").val(json.nama);
	};
    
    dpjp_dua_tagihan_rumus=new TableAction("dpjp_dua_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	dpjp_dua_tagihan_rumus.setSuperCommand("dpjp_dua_tagihan_rumus");
	dpjp_dua_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	dpjp_dua_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_dpjp_dua").val(json.id);
		$("#tagihan_rumus_nama_dpjp_dua").val(json.nama);
	};
    
    dpjp_tiga_tagihan_rumus=new TableAction("dpjp_tiga_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	dpjp_tiga_tagihan_rumus.setSuperCommand("dpjp_tiga_tagihan_rumus");
	dpjp_tiga_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	dpjp_tiga_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_dpjp_tiga").val(json.id);
		$("#tagihan_rumus_nama_dpjp_tiga").val(json.nama);
	};
    
    konsul_satu_tagihan_rumus=new TableAction("konsul_satu_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	konsul_satu_tagihan_rumus.setSuperCommand("konsul_satu_tagihan_rumus");
	konsul_satu_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	konsul_satu_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_konsul_satu").val(json.id);
		$("#tagihan_rumus_nama_konsul_satu").val(json.nama);
	};
    
    konsul_dua_tagihan_rumus=new TableAction("konsul_dua_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	konsul_dua_tagihan_rumus.setSuperCommand("konsul_dua_tagihan_rumus");
	konsul_dua_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	konsul_dua_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_konsul_dua").val(json.id);
		$("#tagihan_rumus_nama_konsul_dua").val(json.nama);
	};
    
    anak_tagihan_rumus=new TableAction("anak_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	anak_tagihan_rumus.setSuperCommand("anak_tagihan_rumus");
	anak_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	anak_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_anak").val(json.id);
		$("#tagihan_rumus_nama_anak").val(json.nama);
	};
    
    umum_tagihan_rumus=new TableAction("umum_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	umum_tagihan_rumus.setSuperCommand("umum_tagihan_rumus");
	umum_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	umum_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_umum").val(json.id);
		$("#tagihan_rumus_nama_umum").val(json.nama);
	};
    
    spesialis_tagihan_rumus=new TableAction("spesialis_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	spesialis_tagihan_rumus.setSuperCommand("spesialis_tagihan_rumus");
	spesialis_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	spesialis_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_spesialis").val(json.id);
		$("#tagihan_rumus_nama_spesialis").val(json.nama);
	};
    
    igd_tagihan_rumus=new TableAction("igd_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	igd_tagihan_rumus.setSuperCommand("igd_tagihan_rumus");
	igd_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	igd_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_igd").val(json.id);
		$("#tagihan_rumus_nama_igd").val(json.nama);
	};
    
    keterangan_pv_tagihan_rumus=new TableAction("keterangan_pv_tagihan_rumus",tagihan_rumus_polislug,"tagihan_rumus",new Array());
	keterangan_pv_tagihan_rumus.setSuperCommand("keterangan_pv_tagihan_rumus");
	keterangan_pv_tagihan_rumus.setPrototipe(tagihan_rumus_poliname,tagihan_rumus_polislug,"rawat");
	keterangan_pv_tagihan_rumus.selected=function(json){
		$("#tagihan_rumus_id_rumus_pv").val(json.id);
		$("#tagihan_rumus_keterangan_pv").val(json.keterangan);
	};
    
    
	
});