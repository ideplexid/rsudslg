var diskon_kasir;
var diskon_kasir_pegawai;
$(document).ready(function(){
	diskon_kasir_carabayar  = $("#diskon_kasir_carabayar").val();
	diskon_kasir_poliname   = $("#diskon_kasir_poliname").val();
	diskon_kasir_polislug   = $("#diskon_kasir_polislug").val();
    
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({minuteStep:1});
	var column = new Array(
        "id",
        "nama_pasien","noreg_pasien","nrm_pasien",
        "waktu_ajukan","keterangan","pegawai",
        "id_pegawai","nilai","persen","operator"
    );
	diskon_kasir = new TableAction("diskon_kasir",diskon_kasir_polislug,"diskon_kasir",column);
    diskon_kasir.setPrototipe(diskon_kasir_poliname,diskon_kasir_polislug,"rawat");
    diskon_kasir.addNoClear("nama_pasien");
    diskon_kasir.addNoClear("noreg_pasien");
    diskon_kasir.addNoClear("nrm_pasien");
	diskon_kasir.getRegulerData=function(){
		var reg_data = {	
            page                : this.page,
            action              : this.action,
            super_command       : this.super_command,
            prototype_name      : this.prototype_name,
            prototype_slug      : this.prototype_slug,
            prototype_implement : this.prototype_implement
        };
		reg_data['noreg_pasien']    = $("#diskon_kasir_noreg_pasien").val();
		reg_data['nama_pasien']     = $("#diskon_kasir_nama_pasien").val();
		reg_data['nrm_pasien']      = $("#diskon_kasir_nrm_pasien").val();
		reg_data['carabayar']       = diskon_kasir_carabayar;
		return reg_data;
	};
    diskon_kasir.view();
    
    diskon_kasir_pegawai = new TableAction("diskon_kasir_pegawai",diskon_kasir_polislug,"diskon_kasir",column);
    diskon_kasir_pegawai.setPrototipe(diskon_kasir_poliname,diskon_kasir_polislug,"rawat");
    diskon_kasir_pegawai.setSuperCommand("diskon_kasir_pegawai");
    diskon_kasir_pegawai.selected = function(json){
        $("#diskon_kasir_id_pegawai").val(json.id);
        $("#diskon_kasir_pegawai").val(json.nama);
    };
});