var tindakan_dokter_detail;		
var tarif_tindakan_dokter_detail;
var dokter_tindakan_dokter_detail; 	
var TD_DT_POLISLUG;
var TD_DT_POLINAME;	
var IS_tindakan_dokter_detail_RUNNING=false;
$(document).ready(function(){
    TD_DT_POLISLUG=$("#TD_DT_POLISLUG").val();
    TD_DT_POLINAME=$("#TD_DT_POLINAME").val();
    
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id',"no_register","selesai","nama_pasien","nrm_pasien",'waktu_keluar','cara_keluar','keterangan_keluar',"asal","kelas","jk","carabayar");
    tindakan_dokter_detail=new TableAction("tindakan_dokter_detail",TD_DT_POLISLUG,"tindakan_dokter_detail",column);
    tindakan_dokter_detail.setPrototipe(TD_DT_POLINAME,TD_DT_POLISLUG,"rawat");
    tindakan_dokter_detail.addRegulerData=function(save_data){
        save_data['dari']=$("#tindakan_dokter_detail_dari").val();
        save_data['sampai']=$("#tindakan_dokter_detail_sampai").val();
        save_data['carabayar']=$("#tindakan_dokter_detail_carabayar").val();
        save_data['nama_tindakan']=$("#tindakan_dokter_detail_nama_tindakan").val();
        save_data['nama_dokter']=$("#tindakan_dokter_detail_nama_dokter").val();
        save_data['grup']=$("#tindakan_dokter_detail_grup").val();
        save_data['orderby']=$("#tindakan_dokter_detail_orderby").val();
        return save_data;
    };	
    
    
    dokter_tindakan_dokter_detail=new TableAction("dokter_tindakan_dokter_detail",TD_DT_POLISLUG,"tindakan_dokter_detail",new Array());
    dokter_tindakan_dokter_detail.setPrototipe(TD_DT_POLINAME,TD_DT_POLISLUG,"rawat");
    dokter_tindakan_dokter_detail.setSuperCommand("dokter_tindakan_dokter_detail");
    dokter_tindakan_dokter_detail.selected=function(json){
        var nama=json.nama;
        $("#tindakan_dokter_detail_nama_dokter").val(nama);
    };

    tindakan_dokter_detail.fix_carabayar=function(id){
        var a=this.getRegulerData();
        a['command']="list";
        a['fix_carabayar']="1";
        a['id']=id;
        var self=this;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            $("#"+self.prefix+"_list").html(json.list);
            $("#"+self.prefix+"_pagination").html(json.pagination);	
            dismissLoading();
        });
    };
    
    tindakan_dokter_detail.rekaptotal=function(){
        if(IS_tindakan_dokter_detail_RUNNING) return;
        $("#tindakan_dokter_detail_bar").sload("true","Fetching total data",0);
        $("#tindakan_dokter_detail_modal").smodal("show");
        IS_tindakan_dokter_detail_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var total=Number(getContent(res));
            if(total>0) {
                tindakan_dokter_detail.rekaploop(0,total);
            } else {
                $("#tindakan_dokter_detail_modal").smodal("hide");
                IS_tindakan_dokter_detail_RUNNING=false;
            }
        });
    };
    tindakan_dokter_detail.rekaploop=function(current,total){
        $("#tindakan_dokter_detail_bar").sload("true","Processing... [ "+current+" / "+total+" ] ",(current*100/total));
        if(current>=total || !IS_tindakan_dokter_detail_RUNNING) {
            $("#tindakan_dokter_detail_modal").smodal("hide");
            IS_tindakan_dokter_detail_RUNNING=false;
            tindakan_dokter_detail.view();
            return;
        }
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['limit_start']=current;			
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){tindakan_dokter_detail.rekaploop(++current,total)},3000);
        });
    };
    
    
    tarif_tindakan_dokter_detail=new TableAction("tarif_tindakan_dokter_detail",TD_DT_POLISLUG,"tindakan_dokter_detail",new Array());
    tarif_tindakan_dokter_detail.setPrototipe(TD_DT_POLINAME,TD_DT_POLISLUG,"rawat");
    tarif_tindakan_dokter_detail.setSuperCommand("tarif_tindakan_dokter_detail");
    tarif_tindakan_dokter_detail.selected=function(json){
        var nama=json.nama;
        $("#tindakan_dokter_detail_nama_tindakan").val(nama);
    };
    
    
});