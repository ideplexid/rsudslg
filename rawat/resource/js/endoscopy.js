var dokter_endoscopy;
var asisten_endoscopy;
var endoscopy;	
var tarif_endoscopy;	
var EDC_POLISLUG;
var EDC_POLINAME;
var EDC_CARABAYAR;	
$(document).ready(function(){
    EDC_POLISLUG=$("#edc_polislug").val();
	EDC_POLINAME=$("#edc_poliname").val();
	EDC_CARABAYAR=$("#edc_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id','id_dokter','id_asisten','waktu','kelas','biaya','nama_dokter','nama_asisten');
    endoscopy=new RawatAction("endoscopy",EDC_POLISLUG,"endoscopy",column);
    endoscopy.setPrototipe(EDC_POLINAME,EDC_POLISLUG,"rawat");
    endoscopy.setEnableAutofocus(true);
    endoscopy.setMultipleInput(true);
    endoscopy.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#endoscopy_noreg_pasien").val();
        reg_data['nama_pasien']=$("#endoscopy_nama_pasien").val();
        reg_data['nrm_pasien']=$("#endoscopy_nrm_pasien").val();						
        reg_data['carabayar']=EDC_CARABAYAR;
        return reg_data;
    };
    endoscopy.view();

    dokter_endoscopy=new TableAction("dokter_endoscopy",EDC_POLISLUG,"endoscopy",new Array());
    dokter_endoscopy.setSuperCommand("dokter_endoscopy");
    dokter_endoscopy.setPrototipe(EDC_POLINAME,EDC_POLISLUG,"rawat");
    dokter_endoscopy.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#endoscopy_nama_dokter").val(nama);
        $("#endoscopy_id_dokter").val(nip);
    };
    stypeahead("#endoscopy_nama_dokter",3,dokter_endoscopy,"nama",function(item){
        $("#endoscopy_id_dokter").val(item.id);
        $("#endoscopy_nama_dokter").val(item.nama);
        $("#endoscopy_nama_asisten").focus();
    });

    asisten_endoscopy=new TableAction("asisten_endoscopy",EDC_POLISLUG,"endoscopy",new Array());
    asisten_endoscopy.setPrototipe(EDC_POLINAME,EDC_POLISLUG,"rawat");
    asisten_endoscopy.setSuperCommand("asisten_endoscopy");
    asisten_endoscopy.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#endoscopy_nama_asisten").val(nama);
        $("#endoscopy_id_asisten").val(nip);						
    };
    stypeahead("#endoscopy_nama_asisten",3,asisten_endoscopy,"nama",function(item){
        $("#endoscopy_id_asisten").val(item.id);
        $("#endoscopy_nama_asisten").val(item.nama);
        $("#endoscopy_kelas").focus();
    });

    tarif_endoscopy=new TableAction("tarif_endoscopy",EDC_POLISLUG,"endoscopy",new Array());
    tarif_endoscopy.setPrototipe(EDC_POLINAME,EDC_POLISLUG,"rawat");
    tarif_endoscopy.setSuperCommand("tarif_endoscopy");
    tarif_endoscopy.addRegulerData=function(d){
        d['noreg_pasien']=$("#endoscopy_noreg_pasien").val();
        return d;
    };
    tarif_endoscopy.selected=function(json){
        var kelas=json.kelas;
        var harga=Number(json.tarif);
        $("#endoscopy_kelas").val(kelas);
        $("#endoscopy_biaya").maskMoney('mask',harga);
    };
    stypeahead("#endoscopy_kelas",3,tarif_endoscopy,"kelas",function(item){
        $("#endoscopy_kelas").val(item.kelas);
        setMoney("#endoscopy_biaya",Number(item.tarif));
        $("#endoscopy_save").focus();
    });
    
});