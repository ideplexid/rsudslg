function RawatAction(name,page,action,column){
    TableAction.call( this, name,page,action,column);
    this.employee = {};
}
RawatAction.prototype.constructor = RawatAction;
RawatAction.prototype=new TableAction();

RawatAction.prototype.reload_bpjs=function(json){
    if (typeof reload_bpjs === "function" && $("#autoload_plafon")=="1") {
        reload_bpjs(json,getMoney("#plafon_bpjs"));
    }
};

RawatAction.prototype.del=function(id){
	var self=this;
	var del_data=this.getDelData(id);	
	bootbox.confirm("Anda yakin Ingin Menghapus ?", function(result) {
	   if(result){
		   showLoading();
		    $.post('',del_data,function(res){
				var json=getContent(res);
				if(json==null) return;
				self.view();
				dismissLoading();
				self.reload_bpjs(json);
				self.postAction(id);
			});
	   }
	}); 
};


RawatAction.prototype.getRegulerData=function(){
	var reg_data = TableAction.prototype.getRegulerData.call(this);
    reg_data ['titipan'] = layanan.titipan();
	return reg_data;
};

RawatAction.prototype.save=function (){
    showLoading();
    var cek = this.cekTutupTagihan();
    var cek_employee = this.cekEmployee();
    if(cek && cek_employee){
        if(!this.cekSave()){
            dismissLoading();
            return;
        }
        this.beforesave();	
        var self=this;
        var a=this.getSaveData();
        $.ajax({url:"",data:a,type:'post',success:function(res){
            var json=getContent(res);
            if(json==null) {
                $("#"+self.prefix+"_add_form").smodal('hide');
                return;		
            }else if(json.success=="1"){
                if(self.multiple_input) {
                    if(self.focus_on_multi!=""){
                        $('#'+self.action+'_'+self.focus_on_multi).focus();
                    }else{
                        $('#'+self.action+'_add_form').find('[autofocus]').focus();					
                    }
                }else {
                    $("#"+self.prefix+"_add_form").smodal('hide');
                }
                self.view();
                self.clear();
            }
            dismissLoading();
            self.reload_bpjs(json);
            self.postAction(a['id']);
        }});
        this.aftersave();  
    }else{
        dismissLoading();	
    }
};


RawatAction.prototype.cekTutupTagihan = function(){
    var reg_data = this.getRegulerData();			
    var noreg    = $("#"+this.prefix+"_noreg_pasien").val();
    if(noreg==""){
        smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
        return false;
    }
	reg_data['command']        = 'cek_tutup_tagihan';
	reg_data['noreg_pasien']  = noreg;
    
    var res = $.ajax({
        type: "POST",
        url: "",
        data:reg_data,
        async: false
    }).responseText;

    var json = getContent(res);
    if(json=="1"){
        return false;
    }else{
        return true;
    }
};

RawatAction.prototype.addEmployee = function(id,nama,title){
    this.employee[""+id] = {
        "id": id,
        "nama": nama,
        "title":title,
    };
};

RawatAction.prototype.cekEmployee = function(){
    if($.isEmptyObject(this.employee)){
        return true;
    }
    var emp = {};
    var self = this;
    $.each(this.employee,function(key,value){
        if( ($("#"+self.prefix+"_"+value.id).val()=="" || $("#"+self.prefix+"_"+value.id).val()=="0") && $("#"+self.prefix+"_"+value.nama).val()==""){
            
        }else{
            emp[""+key] = {
                "id": $("#"+self.prefix+"_"+value.id).val(),
                "nama": $("#"+self.prefix+"_"+value.nama).val(),
                "title":value.title,
                "warn":value.nama
            }
        }
    });

    var reg_data = this.getRegulerData();	
    reg_data['action'] = "cek_employee";
    reg_data['employee'] = emp;
    
    var noreg    = $("#"+this.prefix+"_noreg_pasien").val();
    if(noreg==""){
        smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
        return;
    }
    var res = $.ajax({
        type: "POST",
        url: "",
        data:reg_data,
        async: false
    }).responseText;

    var json = getContent(res);
    if(json.resume=="1"){
        return true;
    }else{
        var warn = json.warn;
        $(warn).each(function(index,value){
            $("#"+self.prefix+"_"+value).addClass("error_field");
        });
        var id_alt	= "#modal_alert_"+self.prefix+"_add_form";		
		var warnx	= '<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>'+json.msg+'</div>';
		$(id_alt).html(warnx);
		$(id_alt).focus();
        return false;
    }
};
