function RujukanAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
RujukanAction.prototype.constructor = RujukanAction;
RujukanAction.prototype=new TableAction();
RujukanAction.prototype.getRegulerData=function(){
	var reg=TableAction.prototype.getRegulerData.call(this);
	reg['noreg_pasien']=$("#"+this.prefix+"_noreg_pasien").val();//utnuk kepentingan search
	reg['nrm_pasien']=$("#"+this.prefix+"_nrm_pasien").val();
	reg['nama_pasien']=$("#"+this.prefix+"_nama_pasien").val();
	return reg;
};

var rujukan;		
var rujukan_harga;	
var rujukan_dokter;	
var rjk_poliname;
var rjk_polislug;
var chooser_kamar;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	rjk_poliname=$("#rjk_poliname").val();
	rjk_polislug=$("#rjk_polislug").val();
	var column=new Array('id','id_antrian',"bed_kamar","id_bed_kamar","nama_pasien","noreg_pasien","nrm_pasien",'waktu','id_dokter','nama_dokter','ruangan','keterangan','keterangan_salah_kamar');
	rujukan=new RujukanAction("rujukan",rjk_polislug,"rujukan",column);
	rujukan.setPrototipe(rjk_poliname,rjk_polislug,"rawat");
    rujukan.save_out=function(){
        var data=this.getSaveData();
        data['save_and_out']=1;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            if(json['success']==1){
                 location.reload();
                 return;
            }
            dismissLoading();
            if(json['success']==0){
                rujukan.show_form();
            }
        });
    };
    
    rujukan.salah_kamar=function(){
        var data=this.getSaveData();
        data['save_and_out']=1;
        data['cara_keluar']='Tidak Datang';
        data['salah_kamar']=1;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            if(json['success']==1){
                 location.reload();
                 return;
            }
            dismissLoading();
            if(json['success']==0){
                rujukan.show_form();
            }
        });
    };
    
	rujukan.view();
	
	rujukan_dokter=new TableAction("rujukan_dokter",rjk_polislug,"rujukan",new Array());
	rujukan_dokter.setPrototipe(rjk_poliname,rjk_polislug,"rawat");
	rujukan_dokter.setSuperCommand("rujukan_dokter");
	rujukan_dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#rujukan_nama_dokter").val(nama);
		$("#rujukan_id_dokter").val(nip);
    };
    stypeahead("#rujukan_nama_dokter",3,rujukan_dokter,"nama",function(item){
		$("#rujukan_nama_dokter").val(item.nama);
		$("#rujukan_id_dokter").val(item.nip);
		return item.nama;
    });
    
    chooser_kamar=new TableAction("chooser_kamar",rjk_polislug,"chooser_kamar",new Array());
	chooser_kamar.setPrototipe(rjk_poliname,rjk_polislug,"rawat");
    chooser_kamar.setSuperCommand("chooser_kamar");
    chooser_kamar.addRegulerData=function(x){
        x['ruangan'] = rujukan.get("ruangan");
        return x;
    };
	chooser_kamar.selected=function(json){
        rujukan.set("id_bed_kamar",json.id);
        rujukan.set("bed_kamar",json.nama);
    };
    stypeahead("#rujukan_bed_kamar",3,rujukan_dokter,"nama",function(item){
		chooser_kamar.selected(item);
	});
});