var lap_pendapatan=new Array();
function loadPendapatan(action,polislug,poliname){
    var found =$.inArray(action, this.data_loaded) > -1;
	if(found){
		 return;
	}else{
		reLoadPendapatan(action,polislug,poliname);
	}
}

function reLoadPendapatan(action,polislug,poliname){
    lap_pendapatan.push(action);
	var data={
        page:polislug,
        action:action,
        prototype_implement:"rawat",
        prototype_slug:polislug,
        prototype_name:poliname
    };
	data['super_command']="";
	showLoading();
	$.post('',data,function(res){
		$("#"+action).html(res);
		dismissLoading();
	});
}

$("div#laporan_pendapatan_header > ul > li.active > a").trigger("click");