var perawat_tindakan_perawat_igd;
var perawat_tindakan_perawat_igd_dua;
var perawat_tindakan_perawat_igd_tiga;	
var perawat_tindakan_perawat_igd_empat;
var perawat_tindakan_perawat_igd_lima;	
var perawat_tindakan_perawat_igd_enam;	
var perawat_tindakan_perawat_igd_tujuh;	
var perawat_tindakan_perawat_igd_delapan;	
var perawat_tindakan_perawat_igd_sembilan;	
var perawat_tindakan_perawat_igd_sepuluh;	

var tindakan_perawat_igd;
var tarif_keperawatan_igd;	
var dokter_tindakan_perawat_igd;
var icdtindakan_tindakan_perawat_igd;
var POLISLUG_TINDAKAN_PERAWAT_IGD;
var POLINAME_TINDAKAN_PERAWAT_IGD;
var CARABAYAR_TINDAKAN_PERAWAT_IGD;
var NUMBER_PERAWAT_IGD;
var NUMBER_PERAWAT_IGD_TABLE_ACTION;

function reload_tarif_igd(next){
	var jumlah=Number($("#tindakan_perawat_igd_jumlah").val());
	var satuan=getMoney("#tindakan_perawat_igd_satuan");
	var total=jumlah*satuan;
	if($("#tindakan_perawat_igd_jumlah").attr("type")=="text" ){
		$("#tindakan_perawat_igd_harga_tindakan").maskMoney('mask',total);
	}else{
		$("#tindakan_perawat_igd_harga_tindakan").val(total);
	}
	
	if(next=="jumlah" && $("#tindakan_perawat_igd_jumlah").attr("type")=="text" ){
		$("#tindakan_perawat_igd_jumlah").focus();
	}else if(next=="jumlah" ){
		$("#tindakan_perawat_igd_save").focus();
	}
}

$(document).ready(function(){
	
	POLISLUG_TINDAKAN_PERAWAT_IGD=$("#tpi_polislug").val();
	POLINAME_TINDAKAN_PERAWAT_IGD=$("#tpi_poliname").val();
	CARABAYAR_TINDAKAN_PERAWAT_IGD=$("#tpi_carabayar").val();
	NUMBER_PERAWAT_IGD_TABLE_ACTION=new Array();
	NUMBER_PERAWAT_IGD=new Array();
	NUMBER_PERAWAT_IGD[1]="";
	NUMBER_PERAWAT_IGD[2]="_dua";
	NUMBER_PERAWAT_IGD[3]="_tiga";
	NUMBER_PERAWAT_IGD[4]="_empat";
	NUMBER_PERAWAT_IGD[5]="_lima";
	NUMBER_PERAWAT_IGD[6]="_enam";
	NUMBER_PERAWAT_IGD[7]="_tujuh";
	NUMBER_PERAWAT_IGD[8]="_delapan";
	NUMBER_PERAWAT_IGD[9]="_sembilan";
	NUMBER_PERAWAT_IGD[10]="_sepuluh";
	
	
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	$('.mydatetime').datetimepicker({minuteStep:1});
	$("#tindakan_perawat_igd_jumlah").on("keyup",function(e){
		reload_tarif_igd("save");
	});

	$("#tindakan_perawat_igd_jumlah").on("change",function(e){
		reload_tarif_igd("save");
	});
	
	
	var column=new Array(
			'id','id_dokter',
			'nama_dokter','waktu','kelas','harga_tindakan',
			'nama_tindakan','id_tindakan',
			"jumlah","satuan",
			"kode_icd","nama_icd","ket_icd","jaspel",
            "jaspel_lain_lain","jaspel_penunjang"
			);
	
	for(var i=1;i<=NUMBER_PERAWAT_IGD.length;i++){
		var num=NUMBER_PERAWAT_IGD[i];
		column.push("nama_perawat"+num);
		column.push("id_perawat"+num);
	}		
			
	tindakan_perawat_igd=new RawatAction("tindakan_perawat_igd",POLISLUG_TINDAKAN_PERAWAT_IGD,"tindakan_perawat_igd",column);
	tindakan_perawat_igd.setPrototipe(POLINAME_TINDAKAN_PERAWAT_IGD,POLISLUG_TINDAKAN_PERAWAT_IGD,"rawat");
	tindakan_perawat_igd.setEnableAutofocus(true);
	tindakan_perawat_igd.setNextEnter();
	tindakan_perawat_igd.setMultipleInput(true);

	tindakan_perawat_igd.addEmployee("id_dokter","nama_dokter","Dokter");
	tindakan_perawat_igd.addEmployee("id_perawat","nama_perawat","Perawat I");
	tindakan_perawat_igd.addEmployee("id_perawat_dua","nama_perawat_dua","Perawat II");
	tindakan_perawat_igd.addEmployee("id_perawat_tiga","nama_perawat_tiga","Perawat III");
	tindakan_perawat_igd.addEmployee("id_perawat_empat","nama_perawat_empat","Perawat IV");
	tindakan_perawat_igd.addEmployee("id_perawat_lima","nama_perawat_lima","Perawat V");
	tindakan_perawat_igd.addEmployee("id_perawat_enam","nama_perawat_enam","Perawat VI");
	tindakan_perawat_igd.addEmployee("id_perawat_tujuh","nama_perawat_tujuh","Perawat VII");
	tindakan_perawat_igd.addEmployee("id_perawat_delapan","nama_perawat_delapan","Perawat VIII");
	tindakan_perawat_igd.addEmployee("id_perawat_sembilan","nama_perawat_sembilan","Perawat IX");
	tindakan_perawat_igd.addEmployee("id_perawat_sepuluh","nama_perawat_sepuluh","Perawat X");

	tindakan_perawat_igd.getRegulerData=function(){
		reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#tindakan_perawat_igd_noreg_pasien").val();
		reg_data['nama_pasien']=$("#tindakan_perawat_igd_nama_pasien").val();
		reg_data['nrm_pasien']=$("#tindakan_perawat_igd_nrm_pasien").val();
		reg_data['carabayar']=CARABAYAR_TINDAKAN_PERAWAT_IGD;
		return reg_data;
	};
	tindakan_perawat_igd.beforesave=function(){
		if($("#tindakan_perawat_igd_nama_dokter").val()==""){
			$("#tindakan_perawat_igd_id_dokter").val(0);
		}
		for(var i=1;i<=NUMBER_PERAWAT_IGD.length;i++){
			var num=NUMBER_PERAWAT_IGD[i];
			if($("#tindakan_perawat_igd_nama_perawat"+num).val()==""){
				$("#tindakan_perawat_igd_id_perawat"+num).val(0);
			}
            /*reseting defaule value*/
            var dv_nama=$("#tindakan_perawat_igd_nama_perawat"+num).val();
            var dv_id=$("#tindakan_perawat_igd_id_perawat"+num).val();
            
            $("#tindakan_perawat_igd_nama_perawat"+num).attr("dv",dv_nama);
            $("#tindakan_perawat_igd_id_perawat"+num).attr("dv",dv_id);
		}		
	};
	tindakan_perawat_igd.setEditClearForNoClear(true);
	tindakan_perawat_igd.addNoClear("id_perawat");
	tindakan_perawat_igd.addNoClear("id_dokter");
	tindakan_perawat_igd.addNoClear("nama_perawat");
	tindakan_perawat_igd.addNoClear("nama_dokter");
	tindakan_perawat_igd.show_add_form=function(){
		smis_clear("#"+this.prefix+"_nama_perawat");
		smis_clear("#"+this.prefix+"_nama_dokter");
		this.clear();
		this.show_form();
	};
	if($("#tindakan_perawat_igd_nama_perawat_dua").attr("type")=="hidden"){
		tindakan_perawat_igd.setFocusOnMultiple("nama_tindakan");
	}else{
		tindakan_perawat_igd.setFocusOnMultiple("nama_perawat_dua");
	}
	tindakan_perawat_igd.view();
	
	for(var i=1;i<=NUMBER_PERAWAT_IGD.length;i++){
		var num=NUMBER_PERAWAT_IGD[i];
		var ptpi=new TableAction("perawat_tindakan_perawat_igd"+num,POLISLUG_TINDAKAN_PERAWAT_IGD,"tindakan_perawat_igd",new Array());
			ptpi.setPrototipe(POLINAME_TINDAKAN_PERAWAT_IGD,POLISLUG_TINDAKAN_PERAWAT_IGD,"rawat");
			ptpi.numero=num;
			ptpi.setSuperCommand("perawat_tindakan_perawat_igd"+ptpi.numero);
			ptpi.selected=function(json){
				console.log(this);
				var nama=json.nama;
				var nip=json.id;
				$("#tindakan_perawat_igd_nama_perawat"+this.numero).val(nama);
				$("#tindakan_perawat_igd_id_perawat"+this.numero).val(nip);
			};
		$("#tindakan_perawat_igd_nama_perawat"+num).data("numero",num);
		$("#tindakan_perawat_igd_nama_perawat"+num).data("i",i);
		stypeahead("#tindakan_perawat_igd_nama_perawat"+num,3,ptpi,"nama",function(item,id){
			var numero=$(id).data("numero");
			var the_i=Number($(id).data("i"));
			$("#tindakan_perawat_igd_id_perawat"+numero).val(item.id);
			$("#tindakan_perawat_igd_nama_perawat"+numero).val(item.nama);
			if(the_i<NUMBER_PERAWAT_IGD.length){
				var next_num=NUMBER_PERAWAT_IGD[the_i+1];
				if($("#tindakan_perawat_igd_nama_perawat"+next_num).attr("type")=="hidden"){
					$("#tindakan_perawat_igd_nama_tindakan").focus();
				}else{
					$("#tindakan_perawat_igd_nama_perawat"+next_num).focus();
				}
			}
		});
		NUMBER_PERAWAT_IGD_TABLE_ACTION[i]=ptpi;
	}		
	
	/*STRAT NAMA PERAWAT*/
	perawat_tindakan_perawat_igd=NUMBER_PERAWAT_IGD_TABLE_ACTION[1];
	perawat_tindakan_perawat_igd_dua=NUMBER_PERAWAT_IGD_TABLE_ACTION[2];
	perawat_tindakan_perawat_igd_tiga=NUMBER_PERAWAT_IGD_TABLE_ACTION[3];
	perawat_tindakan_perawat_igd_empat=NUMBER_PERAWAT_IGD_TABLE_ACTION[4];
	perawat_tindakan_perawat_igd_lima=NUMBER_PERAWAT_IGD_TABLE_ACTION[5];
	perawat_tindakan_perawat_igd_enam=NUMBER_PERAWAT_IGD_TABLE_ACTION[6];
	perawat_tindakan_perawat_igd_tujuh=NUMBER_PERAWAT_IGD_TABLE_ACTION[7];
	perawat_tindakan_perawat_igd_delapan=NUMBER_PERAWAT_IGD_TABLE_ACTION[8];
	perawat_tindakan_perawat_igd_sembilan=NUMBER_PERAWAT_IGD_TABLE_ACTION[9];
	perawat_tindakan_perawat_igd_sepuluh=NUMBER_PERAWAT_IGD_TABLE_ACTION[10];
	/*END NAMA PERAWAT*/

	dokter_tindakan_perawat_igd=new TableAction("dokter_tindakan_perawat_igd",POLISLUG_TINDAKAN_PERAWAT_IGD,"tindakan_perawat_igd",new Array());
	dokter_tindakan_perawat_igd.setPrototipe(POLINAME_TINDAKAN_PERAWAT_IGD,POLISLUG_TINDAKAN_PERAWAT_IGD,"rawat");
	dokter_tindakan_perawat_igd.setSuperCommand("dokter_tindakan_perawat_igd");
	dokter_tindakan_perawat_igd.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#tindakan_perawat_igd_nama_dokter").val(nama);
		$("#tindakan_perawat_igd_id_dokter").val(nip);
	};

	stypeahead("#tindakan_perawat_igd_nama_dokter",3,dokter_tindakan_perawat_igd,"nama",function(item){
		$("#tindakan_perawat_igd_id_dokter").val(item.id);
		$("#tindakan_perawat_igd_nama_dokter").val(item.nama);
		$("#tindakan_perawat_igd_nama_perawat").focus();
	});
	
	
	icdtindakan_tindakan_perawat_igd=new TableAction("icdtindakan_tindakan_perawat_igd",POLISLUG_TINDAKAN_PERAWAT_IGD,"tindakan_perawat_igd",new Array());
	icdtindakan_tindakan_perawat_igd.setPrototipe(POLINAME_TINDAKAN_PERAWAT_IGD,POLISLUG_TINDAKAN_PERAWAT_IGD,"rawat");
	icdtindakan_tindakan_perawat_igd.setSuperCommand("icdtindakan_tindakan_perawat_igd");
	icdtindakan_tindakan_perawat_igd.selected=function(json){
		$("#tindakan_perawat_igd_kode_icd").val(json.icd);
		$("#tindakan_perawat_igd_nama_icd").val(json.nama);
		$("#tindakan_perawat_igd_ket_icd").val(json.terjemah);
	};

	stypeahead("#tindakan_perawat_igd_nama_icd",3,icdtindakan_tindakan_perawat_igd,"nama",function(item){
		$("#tindakan_perawat_igd_kode_icd").val(item.icd);
		$("#tindakan_perawat_igd_nama_icd").val(item.nama);
		$("#tindakan_perawat_igd_ket_icd").val(item.terjemah);
	});

	tarif_keperawatan_igd=new RawatAction("tarif_keperawatan_igd",POLISLUG_TINDAKAN_PERAWAT_IGD,"tindakan_perawat_igd",new Array());
	tarif_keperawatan_igd.setPrototipe(POLINAME_TINDAKAN_PERAWAT_IGD,POLISLUG_TINDAKAN_PERAWAT_IGD,"rawat");
	tarif_keperawatan_igd.setSuperCommand("tarif_keperawatan_igd");
	tarif_keperawatan_igd.addRegulerData=function(d){
		d['noreg_pasien']=$("#tindakan_perawat_igd_noreg_pasien").val();
		return d;
	};
	tarif_keperawatan_igd.selected=function(json){
		var kelas=json.kelas;
		var nama=json.nama;
		var harga=Number(json.tarif);
		$("#tindakan_perawat_igd_kelas").val(kelas);
		$("#tindakan_perawat_igd_nama_tindakan").val(nama);
		$("#tindakan_perawat_igd_satuan").maskMoney('mask',harga);
        $("#tindakan_perawat_igd_jaspel").val(json.jaspel);        
        $("#tindakan_perawat_igd_jaspel_lain_lain").val(json.lain_lain);
        $("#tindakan_perawat_igd_jaspel_penunjang").val(json.penunjang);
		reload_tarif_igd("jumlah");
	};
	stypeahead("#tindakan_perawat_igd_nama_tindakan",3,tarif_keperawatan_igd,"nama",function(item){
		$("#tindakan_perawat_igd_id_tindakan").val(item.id);
		$("#tindakan_perawat_igd_kelas").val(item.kelas);
		$("#tindakan_perawat_igd_nama_tindakan").val(item.name);
		$("#tindakan_perawat_igd_satuan").maskMoney('mask',Number(item.tarif));
        $("#tindakan_perawat_igd_jaspel").val(item.jaspel);
        $("#tindakan_perawat_igd_jaspel_lain_lain").val(item.lain_lain);
        $("#tindakan_perawat_igd_jaspel_penunjang").val(item.penunjang);
		reload_tarif_igd("jumlah");
	});
});