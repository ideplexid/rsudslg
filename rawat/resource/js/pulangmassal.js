var pulangmassal;
var pulangmassal_proto_name;
var pulangmassal_proto_slug;
var pulangmassal_proto_imp;
var pulangmassal_polislug;
var pulangmassal_poliname;

$(document).ready(function(){
    pulangmassal_proto_name=$("#pulangmassal_proto_name").val();
    pulangmassal_proto_slug=$("#pulangmassal_proto_slug").val();
    pulangmassal_proto_imp=$("#pulangmassal_proto_implement").val();
    pulangmassal_polislug=$("#pulangmassal_poliname").val();
    pulangmassal_poliname=$("#pulangmassal_polislug").val();

    $(".mydatetime").datetimepicker({minuteStep:1});
    $(".mydate").datepicker();
    var column=new Array('id',"selesai","nama_pasien",'waktu_keluar','cara_keluar','keterangan_keluar');
    pulangmassal=new TableAction("pulangmassal",pulangmassal_proto_slug,"pulangmassal",column);
    pulangmassal.setPrototipe(pulangmassal_proto_name,pulangmassal_proto_slug,"rawat");
    pulangmassal.addRegulerData=function(a){
        a["status_keluar"]=$("#pulangmassal_status_keluar").val();
        a["dari"]=$("#pulangmassal_dari").val();
        a["sampai"]=$("#pulangmassal_sampai").val();
        a["aksi_grup"]=$("#pulangmassal_aksi_grup").val();
        a["aksi_filter"]=$("#pulangmassal_aksi_filter").val();
        return a;
    };
    pulangmassal.aktifkan=function(){
        var data=this.getRegulerData();
        data['command']="aktifkan";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            pulangmassal.view();
            dismissLoading();
        });
    };
    pulangmassal.pulangkan=function(){
        var data=this.getRegulerData();
        data['command']="pulangkan";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            pulangmassal.view();
            dismissLoading();
        });
    };
    pulangmassal.view();
});