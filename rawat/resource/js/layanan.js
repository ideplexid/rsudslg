function LayananAction(name,page,action,column){
	this.initialize(name, page, action, column);
	this.data_loaded = new Array();
    this.px          = null;
}

LayananAction.prototype.constructor = LayananAction;
LayananAction.prototype=new TableAction();
LayananAction.prototype.select = function(id){
	$("#layanan_modal").smodal('hide');
	showLoading();
	var self             = this;
	var edit_data        = this.getEditData(id);
	edit_data['command'] = "select";

	$.post('',edit_data,function(res){
			
		var json         = getContent(res);
		if(json==null) {
			return;
		}
        $("#locker_plebitis").val("");
		$("#locker_cauti").val("");
		$("#locker_plebitis").val("");
        $("#locker_cauti").val("");
        if(typeof json['px']!== 'undefined' ){
            this.px             = json.px;
            var rm              = (this.px.rekam_medis!=null?this.px.rekam_medis:"[]");
            try{
                var rekam_medis = $.parseJSON(rm);
                if(typeof rekam_medis['plebitis'] !== 'undefined' ){
                    $("#locker_plebitis").val(rekam_medis['plebitis']);
                }            
                if(typeof rekam_medis['cauti'] !== 'undefined' ){
                    $("#locker_cauti").val(rekam_medis['cauti']);
                } 
            }catch(e){}
		}    

		var dtlxxxx = $.parseJSON(json.detail);
        $("#id_antrian").val(id);
		$("#noreg_pasien").val(json['No Register']);
		$("#nama_pasien").val(json.Nama);
		$("#tanggal_lahir").val(dtlxxxx.tgl_lahir);
		$("#nrm_pasien").val(json.nrm_pasien);
		$("#asal_pasien").val(json.Asal);
		$("#umur_pasien").val(json.Umur);
		$("#kunjungan_pasien").val(json.Kunjungan);
		$("#kelas_asal").val(json.kelas_asal);
		$("#rl52").val(json.rl52);
		$("#operation_state_name").val(json.state_name);
		$("#operation_room_name").val(json.room_name);
		$("#operation_status_pasien").val(json.status_pasien);
		
		$("#dokumen").val(json.dokumen);
		$("#gol_umur").val(json.golongan_umur);
		$("#carabayar").val(json.carabayar);
		$("#alamat").val(json.alamat);
		$("#peringatan_bpjs").hide();
        
        $("link[href='rawat/resource/css/lock_layanan.css']").remove();
		if(json.bpjs=="1"){
			if (typeof reload_bpjs === "function") {
                reload_bpjs(json,Number(json.plafon_bpjs));
			}
			try{
				$("#kelas_bpjs").val(json['px']['kelas_bpjs']);
				$("#naik_kelas_bpjs").val(json['nama_naik_kelas']);		
			}catch(e){
				
			}
			$(".plafon_bpjs, .sisa_plafon, .total_tagihan , .kelas_bpjs , .naik_kelas_bpjs").show();
		}else{
			$(".plafon_bpjs, .sisa_plafon, .total_tagihan , .kelas_bpjs , .naik_kelas_bpjs").hide();
		}

		$("#jk").val(json.jk=="0"?"Laki-Laki":"Perempuan");
		self.load_tab(id,json.Nama,json.nrm_pasien,json['No Register'],json.golongan_umur,json.jk,json.kunjungan,json.carabayar,json.kelas_asal);
        var keterangan_data              = self.getRegulerData();
        keterangan_data['super_command'] = "get_info_titipan";
        keterangan_data['noreg_pasien']  = json['No Register'];
        
        $("#spesialisasi").val(json.spesialisasi);

        $.post(
            "",
            keterangan_data,
            function(response) {
                var json_keterangan      = JSON.parse(response);
                if ( json_keterangan == null ) return;
                $("#titipan").val(json_keterangan);
                if(json_keterangan=="Titipan" && $("#layanan_hidden_titipan").val()=="1"){
                    $("#peringatan_titipan").css("display","block");
                }else{
                    $("#peringatan_titipan").css("display","none");
                }
            }
        );
	});
};


LayananAction.prototype.loadData=function(action){
	var found = $.inArray(action, this.data_loaded) > -1;
	var loadonce = $("#layanan_hidden_load_once").val();
	if(found && loadonce=="1"){
		 scrolToAnimate("#smis_container",100);
		 return;
	}else{
		this.ReLoadData(action);
	}
};

LayananAction.prototype.ReLoadData=function(action){
	this.data_loaded.push(action);
	var data                = this.getRegulerData();
	data['super_command']   = "";
	data['action']          = action;
	data['mode_layanan']    = $("#mode_layanan").val();
	data['id']              = $("#id_antrian").val();
	data['id_antrian']      = $("#id_antrian").val();
	data['nama']            = $("#nama_pasien").val();
	data['no_reg']          = $("#noreg_pasien").val();
	data['nrm_pasien']      = $("#nrm_pasien").val();
	data['gol_umur']        = $("#gol_umur").val();
	data['jk']              = $("#jk").val();
	data['kunjungan']       = $("#kunjungan_pasien").val();
	data['carabayar']       = $("#carabayar").val();
	data['kelas_asal']      = $("#kelas_asal").val();
    data['titipan']         = this.titipan();    
	showLoading();
	$.post('',data,function(res){
		$("#"+action).html(res);
		scrolToAnimate("#smis_container",100);
		dismissLoading();
	});
};

LayananAction.prototype.titipan = function(){
    if( $("#layanan_hidden_titipan").val()=="1" 
        && $("#asal_pasien").val()!="Pendaftaran" 
        && $("#asal_pasien").val()!="" 
        && $("#titipan").val()=="Titipan"){
        return $("#asal_pasien").val();
    }
    return "";
};

LayananAction.prototype.view = function(){
	showLoading();
	var self        = this;
	var view_data   = this.getViewData();
	$.post('',view_data,function(res){
		var json    = getContent(res);
		if(json==null) {
			return;
		}
		$("#"+self.prefix+"_list").html(json.list);
		$("#"+self.prefix+"_pagination").html(json.pagination);		
		dismissLoading();
	});
};

LayananAction.prototype.list_antrian = function(){
	var self        = this;
	var data        = this.getRegulerData();
	showLoading();
	$.post('',data,function(res){
		$("#layanan_modal .modal-body").html(res);
		$("#layanan_modal").smodal('show');
		layanan.view();
		dismissLoading();
	});
};

LayananAction.prototype.come_back = function(){
	var self              = this;
	var data              = this.getRegulerData();
	if($("#mode_layanan").val()=="diagnosa_only"){
		data['action']    = "antriandiagnosa";
	}else{
		data['action']    = "antrian";
	}
	data['super_command'] = "";
	LoadSmisPage(data);
};


LayananAction.prototype.load_tab = function(id,nama,nrm,no_reg,gol,jk,kunjungan,carabayar,kelas_asal){
	var data                = this.getRegulerData();
	data['load_tab']        = "load_tab";
	data['super_command']   = "";
	data['id']              = id;
	data['id_antrian']      = id;
	data['nama']            = nama;
	data['mode_layanan']    = $("#mode_layanan").val();
	data['no_reg']          = no_reg;
	data['nrm_pasien']      = nrm;
	data['gol_umur']        = gol;
	data['jk']              = jk;
	data['kunjungan']       = kunjungan;
	data['carabayar']       = carabayar;
	data['kelas_asal']      = kelas_asal;
	var self                = this;
	$.post('',data,function(res){
		$("#table_content").html(res);
		self.data_loaded    = new Array();
		$("div#layanan_tabs_header > ul > li.active a").trigger('click');
		dismissLoading();
	});
};

LayananAction.prototype.load_plafon_update = function(){
    if(typeof load_plafon_update === "function"){
        load_plafon_update(layanan);
    }
};


LayananAction.prototype.request_tinjau_plafon  = function(){
	var data                = this.getRegulerData();
	data['noreg_pasien']    = $("#noreg_pasien").val();
	data['nrm_pasien']    	= $("#nrm_pasien").val();
	data['nama_pasien']    	= $("#nama_pasien").val();
	data['ruangan']    		= data['prototype_name'];
	data['total_tagihan']   = getMoney("#total_tagihan");
	data['plafon']   		= getMoney("#plafon_bpjs");
	data['action']			= "request_plafon";
	bootbox.prompt("Sertakan Keterangan Peninjauan Plafon", function(result){ 
		if(result!=""){
			data['keterangan']	= result;
			showLoading();
			$.post("",data,function(json){
				var json 		= getContent(json);
				dismissLoading();
			});
		}
	});
};