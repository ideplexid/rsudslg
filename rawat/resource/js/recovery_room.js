var rr;					
var asisten_rr;
var petugas_rr;	
var RR_POLISLUG;
var RR_POLINAME;
var RR_CARABAYAR;			
$(document).ready(function(){
    RR_POLISLUG=$("#rr_polislug").val();
	RR_POLINAME=$("#rr_poliname").val();
	RR_CARABAYAR=$("#rr_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    
    var column = new Array('id',"nama_pasien","noreg_pasien","nrm_pasien","waktu",
                          "nama_asisten","id_asisten","nama_petugas","id_petugas",
                          "harga");
    
    rr=new RawatAction("rr",RR_POLISLUG,"rr",column);
    rr.setEnableAutofocus(true);
    rr.setNextEnter();
    rr.setPrototipe(RR_POLINAME,RR_POLISLUG,"rawat");
    rr.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#rr_noreg_pasien").val();
        reg_data['nama_pasien']=$("#rr_nama_pasien").val();
        reg_data['nrm_pasien']=$("#rr_nrm_pasien").val();						
        reg_data['carabayar']=RR_CARABAYAR;
        return reg_data;
    };
    rr.view();
    
    asisten_rr=new TableAction("asisten_rr",RR_POLISLUG,"rr",new Array());
    asisten_rr.setSuperCommand("asisten_rr");
    asisten_rr.setPrototipe(RR_POLINAME,RR_POLISLUG,"rawat");
    asisten_rr.selected=function(json){
        $("#rr_id_asisten").val(json.id);
        $("#rr_nama_asisten").val(json.nama);
    };
    petugas_rr=new TableAction("petugas_rr",RR_POLISLUG,"rr",new Array());
    petugas_rr.setPrototipe(RR_POLINAME,RR_POLISLUG,"rawat");
    petugas_rr.setSuperCommand("petugas_rr");
    petugas_rr.selected=function(json){
        $("#rr_id_petugas").val(json.id);
        $("#rr_nama_petugas").val(json.nama);
    };


    $('#rr_nama_asisten').typeahead({
        minLength:3,
        source: function (query, process) {
         var data_dokter=asisten_rr.getViewData();
         data_dokter["kriteria"]=$('#rr_nama_asisten').val();
         var $items = new Array;
           $items = [""];				                
          $.ajax({
            url: '',
            type: 'POST',
            data: data_dokter,
            success: function(res) {
              var json=getContent(res);
              var the_data_proses=json.d.data;
               $items = [""];	
              $.map(the_data_proses, function(data){
                  var group;
                  group = {
                      id: data.id,
                      name: data.nama,
                      toString: function () {
                          return JSON.stringify(this);
                      },
                      toLowerCase: function () {
                          return this.name.toLowerCase();
                      },
                      indexOf: function (string) {
                          return String.prototype.indexOf.apply(this.name, arguments);
                      },
                      replace: function (string) {
                          var value = '';
                          value +=  this.name;
                          if(typeof(this.level) != 'undefined') {
                              value += ' <span class="pull-right muted">';
                              value += this.level;
                              value += '</span>';
                          }
                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
                      }
                  };
                  $items.push(group);
              });
              
              process($items);
            }
          });
        },
        updater: function (item) {
            var item = JSON.parse(item);
            asisten_rr.selected(item);
            $("#rr_nama_petugas").focus();
            return item.name;
        }
     });

    $('#rr_nama_petugas').typeahead({
        minLength:3,
        source: function (query, process) {
         var data_dokter=petugas_rr.getViewData();
         data_dokter["kriteria"]=$('#rr_nama_petugas').val();
         var $items = new Array;
           $items = [""];				                
          $.ajax({
            url: '',
            type: 'POST',
            data: data_dokter,
            success: function(res) {
              var json=getContent(res);
              var the_data_proses=json.d.data;
               $items = [""];	
              $.map(the_data_proses, function(data){
                  var group;
                  group = {
                      id: data.id,
                      name: data.nama,                   
                      toString: function () {
                          return JSON.stringify(this);
                      },
                      toLowerCase: function () {
                          return this.name.toLowerCase();
                      },
                      indexOf: function (string) {
                          return String.prototype.indexOf.apply(this.name, arguments);
                      },
                      replace: function (string) {
                          var value = '';
                          value +=  this.name;
                          if(typeof(this.level) != 'undefined') {
                              value += ' <span class="pull-right muted">';
                              value += this.level;
                              value += '</span>';
                          }
                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
                      }
                  };
                  $items.push(group);
              });
              
              process($items);
            }
          });
        },
        updater: function (item) {
            var item = JSON.parse(item);
            petugas_rr.selected(item);
            $("#rr_harga").focus();
            return item.name;
        }
     });
    
});