var biaya;	
var by_poliname;
var by_polislug;
var by_carabayar;
var by_namapx;
var by_nrmpx;
var by_noregpx;
$(document).ready(function(){
	by_poliname=$("#by_poliname").val();
	by_polislug=$("#by_polislug").val();
	by_carabayar=$("#by_carabayar").val();
	by_namapx=$("#by_namapx").val();
	by_nrmpx=$("#by_nrmpx").val();
	by_noregpx=$("#by_noregpx").val();
	$(".mydatetime").datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array();
	biaya=new TableAction("biaya",by_polislug,"biaya",column);
	biaya.setPrototipe(by_poliname,by_polislug,"rawat");
	biaya.getRegulerData=function(){
		var reg_data={
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement
		};
		reg_data['noreg_pasien']=by_noregpx;
		reg_data['nama_pasien']=by_namapx;
		reg_data['nrm_pasien']=by_nrmpx;
		reg_data['carabayar']=by_carabayar;
		return reg_data;
	};

	biaya.validasi=function(){
		var self=this;
		var data = this.getRegulerData();
		data['super_command'] = "validasi_spesialisasi";
		data['carapulang'] = $("#by_carapulang").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return false;
				if (json.valid === "1") {
					var rdata = self.getRegulerData();
					rdata['keterangan_keluar']="Melakukan Validasi";
					rdata['action']="antrian";
					rdata['command']="save";
					rdata['id']=$("#id_antrian").val();
					rdata['no_register']=by_noregpx;
					rdata['selesai']="1";
					rdata['cara_keluar']=$("#by_carapulang").val();
					rdata['waktu_keluar']=$("#by_tgl_keluar").val();
					if(rdata['cara_keluar']==""){
						showWarning("Kesalahan !!","Cara Pulang Harus di Isi");
						return;
					}else if(rdata['waktu_keluar']==""){
						showWarning("Kesalahan !!","Waktu Keluar Harus di Isi");
						return;
					}
					showLoading();
					$.post('',rdata,function(res){
						var jsn=getContent(res);
						if(jsn==null) {
							return;
						}
						self.afterview(jsn);
						dismissLoading();
						layanan.come_back();
					});
					return;
				}
				showWarning("Peringatan", "Jenis Spesialisasi Pasien Harus Diisi");
			}			
		);
	};

	biaya.view=function(){	
		var self=this;
		var view_data=this.getViewData();
		if(view_data==null){
			showWarning("Error",this.view_data_null_message);
			return;
		}
		showLoading();
		$.post('',view_data,function(res){
			var json=getContent(res);
			if(json==null) {
				
			}else{
				$("#"+self.prefix+"_list").html(json);
			}
			self.afterview(json);
			dismissLoading();
		});
	};
	biaya.view();
});