var dokter_dpjp;		
var dokter_pj;
var dokter_dpjp_carabayar;
var dokter_dpjp_poliname;
var dokter_dpjp_polislug;

$(document).ready(function(){
	
	dokter_dpjp_carabayar=$("#dpjp_carabayar").val();
	dokter_dpjp_poliname=$("#dpjp_poliname").val();
	dokter_dpjp_polislug=$("#dpjp_polislug").val();

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array(
			'id',"nama_pasien","noreg_pasien","nrm_pasien",
			'nama_dokter','id_dokter');
	dokter_dpjp=new RawatAction("dokter_dpjp",dokter_dpjp_polislug,"dokter_dpjp",column);
	dokter_dpjp.setPrototipe(dokter_dpjp_poliname,dokter_dpjp_polislug,"rawat");
	dokter_dpjp.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
				};
		reg_data['noreg_pasien']=$("#dokter_dpjp_noreg_pasien").val();
		reg_data['nama_pasien']=$("#dokter_dpjp_nama_pasien").val();
		reg_data['nrm_pasien']=$("#dokter_dpjp_nrm_pasien").val();
		reg_data['carabayar']=dokter_dpjp_carabayar;
		return reg_data;
	};
	
	dokter_dpjp.save=function(){
		var ds=this.getSaveData();
		ds['command']="save";
		showLoading();
		$.post("",ds,function(res){
			layanan.ReLoadData('dokter_dpjp');
			dismissLoading();
		});					
	};
	
	dokter_pj=new TableAction("dokter_pj",dokter_dpjp_polislug,"dokter_dpjp",new Array());
	dokter_pj.setPrototipe(dokter_dpjp_poliname,dokter_dpjp_polislug,"rawat");
	dokter_pj.setSuperCommand("dokter_pj");
	dokter_pj.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		var slug=json.slug;
		$("#dokter_dpjp_nama_dokter").val(nama);
		$("#dokter_dpjp_id_dokter").val(nip);
	};
	
});