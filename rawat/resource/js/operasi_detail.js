var operasi_detail;		
var asisten_operasi_detail;
var oomloop_operasi_detail;
var dokter_operasi_detail; 
var tarif_operasi_detail;
var operator_operasi_detail;
var perujuk_operasi_detail;
var OP_DT_POLISLUG;
var OP_DT_POLINAME;

var operasi_detail;
var operasi_detail_karyawan;
var operasi_detail_data;
var IS_operasi_detail_RUNNING;

$(document).ready(function(){
    OP_DT_POLISLUG=$("#OP_DT_POLISLUG").val();
    OP_DT_POLINAME=$("#OP_DT_POLINAME").val();
    
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array("id","carabayar","nama_perujuk","id_perujuk","nama_operator_satu","id_operator_satu","id_asisten_operator_satu","nama_asisten_operator_satu","id_oomloop_satu","nama_oomloop_satu");
    operasi_detail=new TableAction("operasi_detail",OP_DT_POLISLUG,"operasi_detail",column);
    operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    operasi_detail.addViewData=function(save_data){
        save_data['command']="list";
        save_data['dari']=$("#operasi_detail_dari").val();
        save_data['sampai']=$("#operasi_detail_sampai").val();
        save_data['filter_carabayar']=$("#operasi_detail_filter_carabayar").val();
        save_data['nama_tindakan']=$("#operasi_detail_nama_tindakan").val();
        save_data['nama_dokter']=$("#operasi_detail_nama_dokter").val();
        save_data['nama_anastesi']=$("#operasi_detail_nama_anastesi").val();
        save_data['jenis_bius']=$("#operasi_detail_jenis_bius").val();
        save_data['jenis_operasi']=$("#operasi_detail_jenis_operasi").val();
        save_data['grup']=$("#operasi_detail_grup").val();
        save_data['orderby']=$("#operasi_detail_orderby").val();
        return save_data;
    };
    
    
    operasi_detail.detail_history=function(id){
        var data=this.getRegulerData();
        data['super_command']="detail_history";
        data['id']=id;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };

    operasi_detail.limit_one=function(id){
        var data=this.getRegulerData();
        data['id']=id;
        data['super_command']='limit_one';
        showLoading();
        $.post("",data,function(res){
            var content=getContent(res);
            showWarning("Operation Result...",content);
            operasi_detail.view();
            dismissLoading();
        });
    };
    
    operasi_detail.addRegulerData=function(data){
        data['dari']=$("#operasi_detail_dari").val();
        data['sampai']=$("#operasi_detail_sampai").val();
        data['autokoreksi']=$("#operasi_detail_autokoreksi").val();
        return data;
    };
    operasi_detail.batal=function(){
        IS_operasi_detail_RUNNING=false;
        $("#operasi_detail_modal").smodal("hide");
    };    
    
    operasi_detail.rekaptotal=function(){
        if(IS_operasi_detail_RUNNING) return;
        $("#operasi_detail_bar").sload("true","Fetching total data",0);
        $("#operasi_detail_modal").smodal("show");
        IS_operasi_detail_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var total=Number(getContent(res));
            if(total>0) {
                operasi_detail.rekaploop(0,total);
            } else {
                $("#operasi_detail_modal").smodal("hide");
                IS_operasi_detail_RUNNING=false;
            }
        });
    };
    operasi_detail.rekaploop=function(current,total){
        $("#operasi_detail_bar").sload("true","Processing... [ "+current+" / "+total+" ] ",(current*100/total));
        if(current>=total || !IS_operasi_detail_RUNNING) {
            $("#operasi_detail_modal").smodal("hide");
            IS_operasi_detail_RUNNING=false;
            operasi_detail.view();
            return;
        }
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['limit_start']=current;			
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){operasi_detail.rekaploop(++current,total)},3000);
        });
    };
    
    dokter_operasi_detail=new TableAction("dokter_operasi_detail",OP_DT_POLISLUG,"operasi_detail",new Array());
    dokter_operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    dokter_operasi_detail.setSuperCommand("dokter_operasi_detail");
    dokter_operasi_detail.selected=function(json){
        var nama=json.nama;
        $("#operasi_detail_nama_dokter").val(nama);
    };
    
    operator_operasi_detail=new TableAction("operator_operasi_detail",OP_DT_POLISLUG,"operasi_detail",new Array());
    operator_operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    operator_operasi_detail.setSuperCommand("operator_operasi_detail");
    operator_operasi_detail.selected=function(json){
        var nama=json.nama;
        $("#operasi_detail_nama_operator_satu").val(nama);
        $("#operasi_detail_id_operator_satu").val(json.id);
    };

    anastesi_operasi_detail=new TableAction("anastesi_operasi_detail",OP_DT_POLISLUG,"operasi_detail",new Array());
    anastesi_operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    anastesi_operasi_detail.setSuperCommand("anastesi_operasi_detail");
    anastesi_operasi_detail.selected=function(json){
        var nama=json.nama;
        $("#operasi_detail_nama_anastesi").val(nama);
        $("#operasi_detail_id_anastesi").val(json.id);
    };
    
    perujuk_operasi_detail=new TableAction("perujuk_operasi_detail",OP_DT_POLISLUG,"operasi_detail",new Array());
    perujuk_operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    perujuk_operasi_detail.setSuperCommand("perujuk_operasi_detail");
    perujuk_operasi_detail.selected=function(json){
        var nama=json.nama;
        $("#operasi_detail_nama_perujuk").val(nama);
        $("#operasi_detail_id_perujuk").val(json.id);
    };
    
    asisten_operasi_detail=new TableAction("asisten_operasi_detail",OP_DT_POLISLUG,"operasi_detail",new Array());
    asisten_operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    asisten_operasi_detail.setSuperCommand("asisten_operasi_detail");
    asisten_operasi_detail.selected=function(json){
        $("#operasi_detail_nama_asisten_operator_satu").val(json.nama);
        $("#operasi_detail_id_asisten_operator_satu").val(json.id);
    };
    
    oomloop_operasi_detail=new TableAction("oomloop_operasi_detail",OP_DT_POLISLUG,"operasi_detail",new Array());
    oomloop_operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    oomloop_operasi_detail.setSuperCommand("oomloop_operasi_detail");
    oomloop_operasi_detail.selected=function(json){
        $("#operasi_detail_nama_oomloop_satu").val(json.nama);
        $("#operasi_detail_id_oomloop_satu").val(json.id);
    };
    
    tarif_operasi_detail=new TableAction("tarif_operasi_detail",OP_DT_POLISLUG,"operasi_detail",new Array());
    tarif_operasi_detail.setPrototipe(OP_DT_POLINAME,OP_DT_POLISLUG,"rawat");
    tarif_operasi_detail.setSuperCommand("tarif_operasi_detail");
    tarif_operasi_detail.selected=function(json){
        var nama=json.nama;
        $("#operasi_detail_nama_tindakan").val(nama);
    };

    operasi_detail.fix_carabayar=function(id){
        var a=this.getRegulerData();
        a['command']="list";
        a['fix_carabayar']="1";
        a['id']=id;
        var self=this;
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            $("#"+self.prefix+"_list").html(json.list);
            $("#"+self.prefix+"_pagination").html(json.pagination);	
            dismissLoading();
        });
    };
    
});