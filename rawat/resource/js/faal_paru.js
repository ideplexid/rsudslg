var dokter_faal_paru;
var asisten_faal_paru;
var faal_paru;	
var tarif_faalparu;
var FPR_POLISLUG;
var FPR_POLINAME;
var FPR_CARABAYAR;
$(document).ready(function(){
    FPR_POLISLUG=$("#fpr_polislug").val();
	FPR_POLINAME=$("#fpr_poliname").val();
	FPR_CARABAYAR=$("#fpr_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    var column=new Array('id','id_dokter','id_asisten','waktu','kelas','biaya','nama_dokter','nama_asisten');
    faal_paru=new RawatAction("faal_paru",FPR_POLISLUG,"faal_paru",column);
    faal_paru.setEnableAutofocus(true);
    faal_paru.setMultipleInput(true);
    faal_paru.setPrototipe(FPR_POLINAME,FPR_POLISLUG,"rawat");
    faal_paru.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);
        reg_data['noreg_pasien']=$("#faal_paru_noreg_pasien").val();
        reg_data['nama_pasien']=$("#faal_paru_nama_pasien").val();
        reg_data['nrm_pasien']=$("#faal_paru_nrm_pasien").val();
        reg_data['carabayar']=FPR_CARABAYAR;
        return reg_data;
    };
    faal_paru.view();

    dokter_faal_paru=new TableAction("dokter_faal_paru",FPR_POLISLUG,"faal_paru",new Array());
    dokter_faal_paru.setSuperCommand("dokter_faal_paru");
    dokter_faal_paru.setPrototipe(FPR_POLINAME,FPR_POLISLUG,"rawat");
    dokter_faal_paru.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#faal_paru_nama_dokter").val(nama);
        $("#faal_paru_id_dokter").val(nip);
    };
    stypeahead("#faal_paru_nama_dokter",3,dokter_faal_paru,"nama",function(item){
        $("#faal_paru_id_dokter").val(item.id);
        $("#faal_paru_nama_dokter").val(item.nama);
        $("#faal_paru_nama_asisten").focus();
    });

    asisten_faal_paru=new TableAction("asisten_faal_paru",FPR_POLISLUG,"faal_paru",new Array());
    asisten_faal_paru.setPrototipe(FPR_POLINAME,FPR_POLISLUG,"rawat");
    asisten_faal_paru.setSuperCommand("asisten_faal_paru");
    asisten_faal_paru.selected=function(json){
        var nama=json.nama;
        var nip=json.id;		
        $("#faal_paru_nama_asisten").val(nama);
        $("#faal_paru_id_asisten").val(nip);
    };
    stypeahead("#faal_paru_nama_asisten",3,asisten_faal_paru,"nama",function(item){
        $("#faal_paru_id_asisten").val(item.id);
        $("#faal_paru_nama_asisten").val(item.nama);
        $("#faal_paru_kelas").focus();
    });

    tarif_faalparu=new TableAction("tarif_faalparu",FPR_POLISLUG,"faal_paru",new Array());
    tarif_faalparu.setPrototipe(FPR_POLINAME,FPR_POLISLUG,"rawat");
    tarif_faalparu.setSuperCommand("tarif_faalparu");
    tarif_faalparu.addRegulerData=function(d){
        d['noreg_pasien']=$("#faal_paru_noreg_pasien").val();
        return d;
    };
    tarif_faalparu.selected=function(json){
        var kelas=json.kelas;
        var harga=Number(json.tarif);
        $("#faal_paru_kelas").val(kelas);
        $("#faal_paru_biaya").maskMoney('mask',harga);
    };
    stypeahead("#faal_paru_kelas",3,tarif_faalparu,"kelas",function(item){
        $("#faal_paru_kelas").val(item.kelas);
        setMoney("#faal_paru_biaya",Number(item.tarif));
        $("#faal_paru_save").focus();
    });
    
});