$(document).ready(function(){
	$("#ok_harga_operator_satu, #ok_harga_operator_dua, #ok_anastesi_hadir ").on("change",function(){
		var satu=getMoney("#ok_harga_operator_satu");
		var dua=getMoney("#ok_harga_operator_dua");
		var hadir=$("#ok_anastesi_hadir ").val()=="1";
		var max=satu>dua?satu:dua;
		var anastesi=0;
		var asisten=0;

		if(hadir){
			anastesi=0.3*0.85*max;
			asisten=0.3*0.15*max;
		}else{
			anastesi=0.3*0.5*max;
			asisten=0.3*0.5*max;			
		}
		setMoney("#ok_harga_anastesi",anastesi);
		setMoney("#ok_harga_asisten_anastesi",asisten);
	});
});