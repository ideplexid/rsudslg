
var resumemedis;		
var resumemedis_harga;	
var resumemedis_dokter;	
var rsm_poliname;
var rsm_polislug;
var RSM_NOREG_PASIEN=0;

var rsm_diagnosa;
var rsm_lab;
var rsm_rad;
var rsm_obat;
var rsm_tindakan;
var rad;
var lab;

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	rsm_poliname=$("#rsm_poliname").val();
	rsm_polislug=$("#rsm_polislug").val();
	var column=new Array();
	resumemedis=new TableAction("resumemedis",rsm_polislug,"resumemedis",column);
	resumemedis.setPrototipe(rsm_poliname,rsm_polislug,"rawat");
	resumemedis.addRegulerData=function(a){
		a['nrm_pasien']=$("#rsm_nrm_pasien").val();
		a['nama_pasien']=$("#rsm_nama_pasien").val();
		a['noreg_pasien']=$("#rsm_noreg_pasien").val();
		return a;
	};
	
	resumemedis.rsm_diagnosa=function(id){
		var data=this.getRegulerData();
		data['super_command']="rsm_diagnosa";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RSM_NOREG_PASIEN=id;
			showFullWarning("Diagnosa",res,"full_model");
			rsm_diagnosa.view();
		});			
	};
	resumemedis.rsm_tindakan=function(id){
		var data=this.getRegulerData();
		data['super_command']="rsm_tindakan";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RSM_NOREG_PASIEN=id;
			showFullWarning("Tindakan",res,"full_model");
			rsm_tindakan.view();
		});			
	};

	resumemedis.rsm_lab=function(id){
		var data=this.getRegulerData();
		data['super_command']="rsm_lab";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RSM_NOREG_PASIEN=id;
			showFullWarning("Laboratory",res,"full_model");
			rsm_lab.view();
		});			
	};

	resumemedis.rsm_rad=function(id){
		var data=this.getRegulerData();
		data['super_command']="rsm_rad";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RSM_NOREG_PASIEN=id;
			showFullWarning("Radiology",res,"full_model");
			rsm_rad.view();
		});			
	};

	resumemedis.rsm_obat=function(id){
		var data=this.getRegulerData();
		data['super_command']="rsm_obat";
		data['noreg_pasien']=id;
		$.post("",data,function(res){
			RSM_NOREG_PASIEN=id;
			showFullWarning("Obat",res,"full_model");
			rsm_obat.view();
		});			
	};

	resumemedis.file_data = function(id){
		var d = this.getRegulerData();
		d['noreg_pasien'] = id;
		d['action'] = "file_resume_medis"
		showLoading();
		$.post("",d,function(res){
			var j = getContent(res);
			if(j!=null && j!=""){
				window.open(j, '_blank').focus();
			}
			dismissLoading();
		});
	}
	

	
	
	rsm_diagnosa=new TableAction("rsm_diagnosa",rsm_polislug,"resumemedis");
	rsm_diagnosa.setPrototipe(rsm_poliname,rsm_polislug,"rawat");
	rsm_diagnosa.setSuperCommand("rsm_diagnosa");
	rsm_diagnosa.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RSM_NOREG_PASIEN;
		return reg_data;
	};

	rsm_tindakan=new TableAction("rsm_tindakan",rsm_polislug,"resumemedis");
	rsm_tindakan.setPrototipe(rsm_poliname,rsm_polislug,"rawat");
	rsm_tindakan.setSuperCommand("rsm_tindakan");
	rsm_tindakan.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RSM_NOREG_PASIEN;
		return reg_data;
	};
    
    rsm_lab=new TableAction("rsm_lab",rsm_polislug,"resumemedis");
	rsm_lab.setPrototipe(rsm_poliname,rsm_polislug,"rawat");
	rsm_lab.setSuperCommand("rsm_lab");
	rsm_lab.hasil=function(id){
		var r = this.getRegulerData();
		r['command'] = "hasil";
		r['id'] = id;
		showLoading();
		$.post("",r,function(res){
			var json = getContent(res);
			if(json!=""){
				window.open(json, '_blank').focus();
			}
			dismissLoading();
		});
	};
	rsm_lab.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RSM_NOREG_PASIEN;
		return reg_data;
	};

	rsm_rad=new TableAction("rsm_rad",rsm_polislug,"resumemedis");
	rsm_rad.setPrototipe(rsm_poliname,rsm_polislug,"rawat");
	rsm_rad.setSuperCommand("rsm_rad");
	rsm_rad.hasil=function(id){
		var hasil=$("#hasil_rad_"+id).html();
		$("#print_table_rsm_rad").html(hasil);
		$("#table_rsm_rad").hide();
	};
	rsm_rad.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RSM_NOREG_PASIEN;
		return reg_data;
	};

	rsm_obat=new TableAction("rsm_obat",rsm_polislug,"resumemedis");
	rsm_obat.setPrototipe(rsm_poliname,rsm_polislug,"rawat");
	rsm_obat.setSuperCommand("rsm_obat");
	rsm_obat.hasil=function(id){
		var hasil=$("#hasil_rsm_obat_"+id).html();
		$("#rsm_obat_modal .modal-body").html(hasil);
		$("#rsm_obat_modal").smodal("show");
	};
	rsm_obat.addRegulerData=function(reg_data){
		reg_data['noreg_pasien']=RSM_NOREG_PASIEN;
		return reg_data;
	};
	lab=rsm_lab;
    rad=rsm_rad;
	resumemedis.view();
});