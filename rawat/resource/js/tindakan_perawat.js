var perawat_tindakan_perawat;
var perawat_tindakan_perawat_dua;
var perawat_tindakan_perawat_tiga;	
var perawat_tindakan_perawat_empat;
var perawat_tindakan_perawat_lima;	
var perawat_tindakan_perawat_enam;	
var perawat_tindakan_perawat_tujuh;	
var perawat_tindakan_perawat_delapan;	
var perawat_tindakan_perawat_sembilan;	
var perawat_tindakan_perawat_sepuluh;	

var perawat_tindakan_perawat;
var perawat_tindakan_perawat_dua;
var perawat_tindakan_perawat_tiga;
var tindakan_perawat;	
var tarif_keperawatan;	
var POLISLUG_TINDAKAN_PERAWAT;
var POLINAME_TINDAKAN_PERAWAT;
var CARABAYAR_TINDAKAN_PERAWAT;

var NUMBER_PERAWAT;
var NUMBER_PERAWAT_TABLE_ACTION;

function reload_tarif(next){
    var jumlah=Number($("#tindakan_perawat_jumlah").val());
    var satuan=getMoney("#tindakan_perawat_satuan");
    var total=jumlah*satuan;
    if($("#tindakan_perawat_jumlah").attr("type")=="text" ){
        $("#tindakan_perawat_harga_tindakan").maskMoney('mask',total);
    }else{
        $("#tindakan_perawat_harga_tindakan").val(total);
    }
    
    if(next=="jumlah" && $("#tindakan_perawat_jumlah").attr("type")=="text" ){
        $("#tindakan_perawat_jumlah").focus();
    }else if(next=="jumlah" ){
        $("#tindakan_perawat_save").focus();
    }
}
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    $('.mydate').datepicker();
    POLISLUG_TINDAKAN_PERAWAT=$("#tp_polislug").val();
    POLINAME_TINDAKAN_PERAWAT=$("#tp_poliname").val(); 
    CARABAYAR_TINDAKAN_PERAWAT=$("#tp_carabayar").val();
    
    NUMBER_PERAWAT_TABLE_ACTION=new Array();
	NUMBER_PERAWAT=new Array();
	NUMBER_PERAWAT[1]="";
	NUMBER_PERAWAT[2]="_dua";
	NUMBER_PERAWAT[3]="_tiga";
	NUMBER_PERAWAT[4]="_empat";
	NUMBER_PERAWAT[5]="_lima";
	NUMBER_PERAWAT[6]="_enam";
	NUMBER_PERAWAT[7]="_tujuh";
	NUMBER_PERAWAT[8]="_delapan";
	NUMBER_PERAWAT[9]="_sembilan";
	NUMBER_PERAWAT[10]="_sepuluh";
    
    var column=new Array('id','id_tindakan','id_perawat','waktu','kelas',
                        'harga_tindakan','nama_tindakan',
                        "jumlah","satuan","jaspel","jaspel_persen","jaspel_lain_lain","jaspel_penunjang");
    
    for(var i=1;i<=NUMBER_PERAWAT.length;i++){
        var num=NUMBER_PERAWAT[i];
        column.push("nama_perawat"+num);
        column.push("id_perawat"+num);
    }		                    

    tindakan_perawat=new RawatAction("tindakan_perawat",POLISLUG_TINDAKAN_PERAWAT,"tindakan_perawat",column);
    tindakan_perawat.setPrototipe(POLINAME_TINDAKAN_PERAWAT,POLISLUG_TINDAKAN_PERAWAT,"rawat");
    tindakan_perawat.setEnableAutofocus(true);
    tindakan_perawat.setNextEnter();
    tindakan_perawat.setMultipleInput(true);
    tindakan_perawat.getRegulerData=function(){
        reg_data = RawatAction.prototype.getRegulerData.call(this);        
        reg_data['noreg_pasien']=$("#tindakan_perawat_noreg_pasien").val();
        reg_data['nama_pasien']=$("#tindakan_perawat_nama_pasien").val();
        reg_data['nrm_pasien']=$("#tindakan_perawat_nrm_pasien").val();
        reg_data['carabayar']=CARABAYAR_TINDAKAN_PERAWAT;
        return reg_data;
    };
    tindakan_perawat.addEmployee("id_perawat","nama_perawat","Perawat I");
	tindakan_perawat.addEmployee("id_perawat_dua","nama_perawat_dua","Perawat II");
	tindakan_perawat.addEmployee("id_perawat_tiga","nama_perawat_tiga","Perawat III");
	tindakan_perawat.addEmployee("id_perawat_empat","nama_perawat_empat","Perawat IV");
	tindakan_perawat.addEmployee("id_perawat_lima","nama_perawat_lima","Perawat V");
	tindakan_perawat.addEmployee("id_perawat_enam","nama_perawat_enam","Perawat VI");
	tindakan_perawat.addEmployee("id_perawat_tujuh","nama_perawat_tujuh","Perawat VII");
	tindakan_perawat.addEmployee("id_perawat_delapan","nama_perawat_delapan","Perawat VIII");
	tindakan_perawat.addEmployee("id_perawat_sembilan","nama_perawat_sembilan","Perawat IX");
	tindakan_perawat.addEmployee("id_perawat_sepuluh","nama_perawat_sepuluh","Perawat X");
    
    tindakan_perawat.beforesave=function(){
		for(var i=1;i<=NUMBER_PERAWAT.length;i++){
			var num=NUMBER_PERAWAT[i];
			if($("#tindakan_perawat_nama_perawat"+num).val()==""){
				$("#tindakan_perawat_id_perawat"+num).val(0);
			}
            /*reseting default value*/
            var dv_nama=$("#tindakan_perawat_nama_perawat"+num).val();
            var dv_id=$("#tindakan_perawat_id_perawat"+num).val();
            
            $("#tindakan_perawat_nama_perawat"+num).attr("dv",dv_nama);
            $("#tindakan_perawat_id_perawat"+num).attr("dv",dv_id);
            console.log(dv_nama);
		}		
	};
    
    tindakan_perawat.setEditClearForNoClear(true);
    tindakan_perawat.addNoClear("nama_perawat");
    tindakan_perawat.addNoClear("id_perawat");
    tindakan_perawat.addNoClear("nama_perawat_dua");
    tindakan_perawat.addNoClear("id_perawat_dua");
    tindakan_perawat.addNoClear("nama_perawat_tiga");
    tindakan_perawat.addNoClear("id_perawat_tiga");
    
    tindakan_perawat.show_add_form=function(){
        var the_id="#"+this.prefix+"_nama_perawat";
        smis_clear(the_id);
        this.clear();
        this.show_form();
    };
    if($("#tindakan_perawat_nama_perawat_dua").attr("type")=="hidden"){
        tindakan_perawat.setFocusOnMultiple("nama_tindakan");
    }else{
        tindakan_perawat.setFocusOnMultiple("nama_perawat_dua");
    }
    tindakan_perawat.view();
    
    /*STRAT NAMA PERAWAT*/
    for(var i=1;i<=NUMBER_PERAWAT.length;i++){
		var num=NUMBER_PERAWAT[i];
		var ptpi=new TableAction("perawat_tindakan_perawat"+num,POLISLUG_TINDAKAN_PERAWAT,"tindakan_perawat",new Array());
			ptpi.setPrototipe(POLINAME_TINDAKAN_PERAWAT,POLISLUG_TINDAKAN_PERAWAT,"rawat");
			ptpi.numero=num;
			ptpi.setSuperCommand("perawat_tindakan_perawat"+ptpi.numero);
			ptpi.selected=function(json){
				console.log(this);
				var nama=json.nama;
				var nip=json.id;
				$("#tindakan_perawat_nama_perawat"+this.numero).val(nama);
				$("#tindakan_perawat_id_perawat"+this.numero).val(nip);
			};
		$("#tindakan_perawat_nama_perawat"+num).data("numero",num);
		$("#tindakan_perawat_nama_perawat"+num).data("i",i);
		stypeahead("#tindakan_perawat_nama_perawat"+num,3,ptpi,"nama",function(item,id){
			var numero=$(id).data("numero");
			var the_i=Number($(id).data("i"));
			$("#tindakan_perawat_id_perawat"+numero).val(item.id);
			$("#tindakan_perawat_nama_perawat"+numero).val(item.nama);
			if(the_i<NUMBER_PERAWAT.length){
				var next_num=NUMBER_PERAWAT[the_i+1];
				if($("#tindakan_perawat_nama_perawat"+next_num).attr("type")=="hidden"){
					$("#tindakan_perawat_nama_tindakan").focus();
				}else{
					$("#tindakan_perawat_nama_perawat"+next_num).focus();
				}
			}
		});
        NUMBER_PERAWAT_TABLE_ACTION[i]=ptpi;
        /*
        $("#tindakan_perawat_nama_perawat"+num).on("change",function(){
			var numero=$(this).data("numero");
			var the_i=Number($(this).data("i"));
			var ptpi = NUMBER_PERAWAT_TABLE_ACTION[the_i];
			var data 		= ptpi.getRegulerData();
			data['id'] 		= tindakan_perawat.get("id_perawat"+numero);
			data['nama'] 	= tindakan_perawat.get("nama_perawat"+numero);
			data['command'] = "cek_pegawai";	
			if(data['nama']==""){
				return;
			}	
			showLoading();
			$.post("",data,function(res){
				var json = getContent(res);
				if(json=="0"){
					smis_alert("Error Perawat "+numero, "Silakan Pilih Nama Perawat "+the_i+" pastikan memilih jangan di ketik manual", "alert-danger");
					tindakan_perawat.set("id_perawat"+numero,"");
                    tindakan_perawat.set("nama_perawat"+numero,"");
                    $("#tindakan_perawat_nama_perawat").focus();
				}
				dismissLoading();
			});
		});*/
			
	}		
	
	
	perawat_tindakan_perawat=NUMBER_PERAWAT_TABLE_ACTION[1];
	perawat_tindakan_perawat_dua=NUMBER_PERAWAT_TABLE_ACTION[2];
	perawat_tindakan_perawat_tiga=NUMBER_PERAWAT_TABLE_ACTION[3];
	perawat_tindakan_perawat_empat=NUMBER_PERAWAT_TABLE_ACTION[4];
	perawat_tindakan_perawat_lima=NUMBER_PERAWAT_TABLE_ACTION[5];
	perawat_tindakan_perawat_enam=NUMBER_PERAWAT_TABLE_ACTION[6];
	perawat_tindakan_perawat_tujuh=NUMBER_PERAWAT_TABLE_ACTION[7];
	perawat_tindakan_perawat_delapan=NUMBER_PERAWAT_TABLE_ACTION[8];
	perawat_tindakan_perawat_sembilan=NUMBER_PERAWAT_TABLE_ACTION[9];
	perawat_tindakan_perawat_sepuluh=NUMBER_PERAWAT_TABLE_ACTION[10];
	/*END NAMA PERAWAT*/
    

    tarif_keperawatan=new TableAction("tarif_keperawatan",POLISLUG_TINDAKAN_PERAWAT,"tindakan_perawat",new Array());
    tarif_keperawatan.setPrototipe(POLINAME_TINDAKAN_PERAWAT,POLISLUG_TINDAKAN_PERAWAT,"rawat");
    tarif_keperawatan.setSuperCommand("tarif_keperawatan");
    tarif_keperawatan.addRegulerData=function(d){
        d['noreg_pasien']=$("#tindakan_perawat_noreg_pasien").val();
        return d;
    };
    tarif_keperawatan.selected=function(json){
        var kelas=json.kelas;
        var nama=json.nama;
        var harga=Number(json.tarif);					
        $("#tindakan_perawat_kelas").val(kelas);
        $("#tindakan_perawat_nama_tindakan").val(nama);
        $("#tindakan_perawat_id_tindakan").val(json.id);
        $("#tindakan_perawat_satuan").maskMoney('mask',harga);
        $("#tindakan_perawat_jaspel").val(json.jaspel);
        $("#tindakan_perawat_jaspel_persen").val(json.jaspel_persen);
        $("#tindakan_perawat_jaspel_lain_lain").val(json.lain_lain);
        $("#tindakan_perawat_jaspel_penunjang").val(json.penunjang);
        reload_tarif("jumlah");
    };
    
    $("#tindakan_perawat_jumlah").on("keyup",function(e){
        reload_tarif("save");
    });

    $("#tindakan_perawat_jumlah").on("change",function(e){
        reload_tarif("save");
    });
    stypeahead("#tindakan_perawat_nama_perawat",3,perawat_tindakan_perawat,"nama",function(item){
            $("#tindakan_perawat_id_perawat").val(item.id);
            $("#tindakan_perawat_nama_perawat").val(item.nama);
            if($("#tindakan_perawat_nama_perawat_dua").attr("type")=="hidden"){
                $("#tindakan_perawat_nama_tindakan").focus();
            }else{
                $("#tindakan_perawat_nama_perawat_dua").focus();
            }
    });

    stypeahead("#tindakan_perawat_nama_perawat_dua",3,perawat_tindakan_perawat_dua,"nama",function(item){
        $("#tindakan_perawat_id_perawat_dua").val(item.id);
        $("#tindakan_perawat_nama_perawat_dua").val(item.nama);
        $("#tindakan_perawat_nama_perawat_tiga").focus();
    });

    stypeahead("#tindakan_perawat_nama_perawat_tiga",3,perawat_tindakan_perawat_tiga,"nama",function(item){
        $("#tindakan_perawat_id_perawat_tiga").val(item.id);
        $("#tindakan_perawat_nama_perawat_tiga").val(item.nama);
        $("#tindakan_perawat_nama_tindakan").focus();
    });
    
    stypeahead("#tindakan_perawat_nama_tindakan",3,tarif_keperawatan,"nama",function(item){
        $("#tindakan_perawat_id_tindakan").val(item.id);
        $("#tindakan_perawat_kelas").val(item.kelas);
        $("#tindakan_perawat_nama_tindakan").val(item.name);
        $("#tindakan_perawat_id_tindakan").val(item.id);
        $("#tindakan_perawat_satuan").maskMoney('mask',Number(item.tarif));
        $("#tindakan_perawat_jaspel").val(item.jaspel);
        $("#tindakan_perawat_jaspel_persen").val(item.jaspel_persen);
        $("#tindakan_perawat_jaspel_lain_lain").val(item.lain_lain);
        $("#tindakan_perawat_jaspel_penunjang").val(item.penunjang);		
        reload_tarif("jumlah");		
    });


});