var layanan;
var previous_kelas_asal;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$("#plafon_bpjs").maskMoney();
	$("#total_tagihan").maskMoney();
	$("#sisa_plafon").maskMoney();	
	var pname = $("#layanan_hidden_poliname").val();
	var pslug = $("#layanan_hidden_polislug").val();
	layanan_init(pslug,pname);
});

function trigger_first(a){
	var id		= $(a).attr("href");
	var tid		= id+" div ul li.active a ";
	var dtid	= $(tid).parent().html();
	$(dtid).trigger('click');
}

function layanan_init(POLISLUG,POLINAME){
	layanan					= new LayananAction("layanan",POLISLUG,"layanan",new Array());
	layanan.setSuperCommand("antrian");
	layanan.addRegulerData	= function(d){
		d['mode_layanan']	= $("#mode_layanan").val();
		return d;
	};
	layanan.setPrototipe(POLINAME,POLISLUG,"rawat");
	if($("#id_antrian").val()!=null && $("#id_antrian").val()!="" && $("#id_antrian").val()!="0" ){
		layanan.select($("#id_antrian").val());
	}

	$("#operation_state_name").on("change",function(){
		var data				= layanan.getRegulerData();
		data['command']			= "save_state_room";
		data['super_command']	= "save_state_room";
		data['noreg_pasien']	= $("#noreg_pasien").val();
		data['state_name']	    = $(this).val();
		if(data['state_name']==""){
			$("#operation_room_name").val("");
		}else if(data['state_name']=="Persiapan Operasi"){
			$("#operation_room_name").val("Ruang Premedikasi");
		}else if(data['state_name']=="Sedang Operasi"){
			$("#operation_room_name").val("Ruang Kamar Operasi");
		}else if(data['state_name']=="Pasca Operasi"){
			if($("#operation_room_name").val()!="Ruang Pulih Sadar (RR)" && $("#operation_room_name").val()!="ICU"){
				$("#operation_room_name").val("Ruang Pulih Sadar (RR)");
			}
		}
		data['room_name']	    = $("#operation_room_name").val();
		data['status_pasien']	= $("#operation_status_pasien").val();

		showLoading();
		$.post("",data,function(response) {
				var json 		= getContent(response);
				dismissLoading();
			}
		);
	});

	$("#operation_room_name").on("change",function(){
		var data				= layanan.getRegulerData();
		data['command']			= "save_state_room";
		data['super_command']	= "save_state_room";
		data['noreg_pasien']	= $("#noreg_pasien").val();
		data['room_name']	    = $(this).val();
		if(data['room_name']==""){
			$("#operation_state_name").val("");
		}else if(data['room_name']=="Ruang Premedikasi"){
			$("#operation_state_name").val("Persiapan Operasi");
		}else if(data['room_name']=="Ruang Kamar Operasi"){
			$("#operation_state_name").val("Sedang Operasi");
		}else if(data['room_name']=="ICU"){
			$("#operation_state_name").val("Pasca Operasi");
		}else if(data['room_name']=="Ruang Pulih Sadar (RR)"){
			$("#operation_state_name").val("Pasca Operasi");
		}
		data['state_name']	    = $("#operation_state_name").val();
		data['status_pasien']	= $("#operation_status_pasien").val();

		showLoading();
		$.post("",data,function(response) {
				var json 		= getContent(response);
				dismissLoading();
			}
		);
	});

	$("#operation_status_pasien").on("change",function(){
		$("#operation_room_name").trigger("change");
	});

	
	
	$("#rl52").on("change",function(){
		var data				= layanan.getRegulerData();
		data['command']			= "save_rl52";
		data['super_command']	= "save_rl52";
		data['noreg_pasien']	= $("#noreg_pasien").val();
		data['rl52']			= $("#rl52").val();
		showLoading();
		$.post("",data,function(response) {
				var json 		= getContent(response);
				dismissLoading();
			}
		);
	});
    
    $("#locker_plebitis").on("change",function(){
		var data				= layanan.getRegulerData();
		data['action']			= "set_plebitis";
        data['super_command']	= "";
		data['noreg_pasien']	= $("#noreg_pasien").val();
        data['status']			= $("#locker_plebitis").val();        
		showLoading();
		$.post("",data,function(response) {
				var json 		= getContent(response);
				dismissLoading();
			}
		);
	});
    
     $("#locker_cauti").on("change",function(){
		var data				= layanan.getRegulerData();
		data['action']			= "set_cauti";
		data['noreg_pasien']	= $("#noreg_pasien").val();
        data['status']			= $("#locker_cauti").val();        
		showLoading();
		$.post("",data,function(response) {
				var json 		= getContent(response);
				dismissLoading();
			}
		);
	});

	$("#dokumen").on("change",function(){
		var data				= layanan.getRegulerData();
		data['command']			= "save_dokumen";
		data['super_command']	= "save_dokumen";
		data['noreg_pasien']	= $("#noreg_pasien").val();
		data['dokumen']			= $("#dokumen").val();
		showLoading();
		$.post("",data,function(response) {
				var json 		= getContent(response);
				dismissLoading();
			}
		);
	});
	
	$("#kelas_asal").on("focus",function(){
		previous_kelas_asal		= $("#kelas_asal").val();
	}).on("change",function(){
		var data				= layanan.getRegulerData();
		data['command']			= "save_kelas_asal";
		data['super_command']	= "save_kelas_asal";
		data['noreg_pasien']	= $("#noreg_pasien").val();
		data['kelas_asal']		= $("#kelas_asal").val();
		var msg="Anda Yakin Mengubah Kelas Asal Pasien ini ? </br>"+
				"Peringatan Mengubah Kelas Asal Pasien dalam beberapa Ruang dapat menyebabkan Pilihan Tagihan Berubah, Pastikan anda merubah dengan benar !!!";
		bootbox.confirm(msg,function(result) {
				if (result) {
					showLoading();
					$.post("",data,function(response) {
							var json = getContent(response);
							dismissLoading();
						}
					);
				}else{
					$("#kelas_asal").val(previous_kelas_asal);
				}
			}
		);
	});

	$("#spesialisasi").on("focus", function() {
		previous_value = $(this).val();
	}).on("change", function() {
		if ($("#id_antrian").val() != "") {
			if ($(this).val() == "") {
				bootbox.alert("Jenis Spesialisasi Harus Diisi");
				$(this).val(previous_value);
				return;
			}
			var data				= layanan.getRegulerData();
			data['command']			= "save_spesialisasi";
			data['super_command']	= "save_spesialisasi";
			data['id_antrian']		= $("#id_antrian").val();
			data['noreg_pasien']	= $("#noreg_pasien").val();
			data['spesialisasi']	= $("#spesialisasi").val();
			showLoading();
			$.post("",data,function(response) {
					var json = getContent(response);
					dismissLoading();
				}
			);
		}
	});

	$("#spesialisasi2").on("focus", function() {
		previous_value = $(this).val();
	}).on("change", function() {
		if ($("#id_antrian").val() != "") {
			if ($(this).val() == "") {
				bootbox.alert("Jenis Spesialisasi Harus Diisi");
				$(this).val(previous_value);
				return;
			}
			var data				= layanan.getRegulerData();
			data['command']			= "save_spesialisasi2";
			data['super_command']	= "save_spesialisasi2";
			data['id_antrian']		= $("#id_antrian").val();
			data['noreg_pasien']	= $("#noreg_pasien").val();
			data['spesialisasi2']	= $("#spesialisasi2").val();
			showLoading();
			$.post("",data,function(response) {
					var json = getContent(response);
					dismissLoading();
				}
			);
		}
	});
}