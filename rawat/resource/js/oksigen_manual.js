var oksigen_manual;
var OMN_POLISLUG;
var OMN_POLINAME;
var OMN_CARABAYAR;
var oksigen_manual_list_harga;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    OMN_POLISLUG    = $("#omn_polislug").val();
	OMN_POLINAME    = $("#omn_poliname").val();
	OMN_CARABAYAR   = $("#omn_carabayar").val();
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column      = new Array('id','nama_pasien','nrm_pasien','jumlah_liter','biaya_lain',"range_harga","jumlah_jam","harga_jam","harga","waktu_pasang","waktu_lepas");
    oksigen_manual  = new RawatAction("oksigen_manual",OMN_POLISLUG,"oksigen_manual",column);
    oksigen_manual.setEnableAutofocus(true);
    oksigen_manual.setNextEnter();
    oksigen_manual.setPrototipe(OMN_POLINAME,OMN_POLISLUG,"rawat");
    oksigen_manual.getRegulerData = function(){
        var reg             = RawatAction.prototype.getRegulerData.call(this);
        reg['noreg_pasien'] = $("#"+this.prefix+"_noreg_pasien").val();
        reg['nrm_pasien']   = $("#"+this.prefix+"_nrm_pasien").val();
        reg['nama_pasien']  = $("#"+this.prefix+"_nama_pasien").val();
        reg['carabayar']    = OMN_CARABAYAR;
        return reg;
    };

    $("#oksigen_manual_jumlah_jam").on("change",function(){
        var money = Number(getMoney("#oksigen_manual_harga_jam"));
        var tjam = Number($(this).val());
        var total = money*tjam;
        setMoney("#oksigen_manual_harga",total);
    });

    oksigen_manual_list_harga=new TableAction("oksigen_manual_list_harga",OMN_POLISLUG,"oksigen_manual",new Array());
	oksigen_manual_list_harga.setPrototipe(OMN_POLINAME,OMN_POLISLUG,"rawat");
	oksigen_manual_list_harga.setSuperCommand("oksigen_manual_list_harga");
	oksigen_manual_list_harga.selected=function(json){
        oksigen_manual.set("range_harga",json.liter);
        setMoney("#oksigen_manual_harga_jam",Number(json.tarif));
		$("#oksigen_manual_jumlah_jam").trigger("change");
	};

    oksigen_manual.view();
});