<?php 
global $RWT_SLUG;
global $wpdb;
$query = "
    CREATE OR REPLACE VIEW smis_rwt_vgizi_" . $RWT_SLUG . " AS
    select ifnull(smis_rwt_gizi_" . $RWT_SLUG . ".id,
    smis_rwt_antrian_" . $RWT_SLUG . ".id) AS id,
    smis_rwt_gizi_" . $RWT_SLUG . ".menu_pagi AS menu_pagi,
    smis_rwt_gizi_" . $RWT_SLUG . ".diet_pagi AS diet_pagi,
    smis_rwt_gizi_" . $RWT_SLUG . ".pk_pagi AS pk_pagi,
    smis_rwt_gizi_" . $RWT_SLUG . ".menu_siang AS menu_siang,
    smis_rwt_gizi_" . $RWT_SLUG . ".diet_siang AS diet_siang,
    smis_rwt_gizi_" . $RWT_SLUG . ".pk_siang AS pk_siang,
    smis_rwt_gizi_" . $RWT_SLUG . ".menu_malam AS menu_malam,
    smis_rwt_gizi_" . $RWT_SLUG . ".diet_malam AS diet_malam,
    smis_rwt_gizi_" . $RWT_SLUG . ".pk_malam AS pk_malam,
    smis_rwt_antrian_" . $RWT_SLUG . ".nama_pasien AS nama_pasien,
    smis_rwt_antrian_" . $RWT_SLUG . ".no_register AS noreg_pasien,
    smis_rwt_antrian_" . $RWT_SLUG . ".nrm_pasien AS nrm_pasien,
    smis_rwt_antrian_" . $RWT_SLUG . ".umur AS umur,
    smis_rwt_antrian_" . $RWT_SLUG . ".golongan_umur AS golongan_umur,
    smis_rwt_antrian_" . $RWT_SLUG . ".selesai AS selesai,
    smis_rwt_antrian_" .  $RWT_SLUG  . ".prop AS prop,
    smis_rwt_bed_kamar_" .  $RWT_SLUG  . ".nama AS bed,
    smis_rwt_antrian_" . $RWT_SLUG . ".ibu_kandung AS ibu_kandung,
    smis_rwt_antrian_" . $RWT_SLUG . ".cara_keluar AS cara_keluar,
    smis_rwt_antrian_" . $RWT_SLUG . ".alamat AS alamat
    from smis_rwt_antrian_" . $RWT_SLUG . " left join smis_rwt_gizi_" . $RWT_SLUG . "
    on smis_rwt_antrian_" . $RWT_SLUG . ".no_register = smis_rwt_gizi_" . $RWT_SLUG . ".noreg_pasien
    left join smis_rwt_bed_kamar_".$RWT_SLUG." 
    on smis_rwt_antrian_" . $RWT_SLUG . ".no_register = smis_rwt_bed_kamar_" . $RWT_SLUG . ".nrm_pasien				
    where smis_rwt_antrian_" . $RWT_SLUG . ".prop != 'del'
    and smis_rwt_antrian_" . $RWT_SLUG . ".selesai = '0';
";
    
$wpdb->query($query);
?>