<?php
	require_once 'smis-libs-class/Policy.php';
	global $user;
	$slug 		= $_POST ['prototype_slug'];
	$implement 	= $_POST ['prototype_implement'];
	
	

	require_once 'smis-libs-inventory/policy.php';
	$ipolicy	= new InventoryPolicy($slug, $user,"modul/");
	$ipolicy	->setPrototype(true, $implement);	
	$policy		= new Policy($slug, $user);
	$policy		->setPrototype(true,$implement);
	$policy		->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
    $policy		->addPolicy("laporan_diagnosa_gizi", "laporan_diagnosa_gizi", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_diagnosa_gizi");

    $policy		->addPolicy("tarif_operasi", "tarif_operasi", Policy::$DEFAULT_COOKIE_CHANGE,"modul/tarif_operasi");
	$policy		->addPolicy("antrian", "antrian", Policy::$DEFAULT_COOKIE_CHANGE,"modul/antrian")
				->addPolicy("status_operasi", "status_operasi", Policy::$DEFAULT_COOKIE_CHANGE,"modul/status_operasi")			
				->addPolicy("riwayat", "riwayat", Policy::$DEFAULT_COOKIE_CHANGE,"modul/riwayat")
				->addPolicy("antriandiagnosa", "antriandiagnosa", Policy::$DEFAULT_COOKIE_CHANGE,"modul/antriandiagnosa")
				->addPolicy("help/user/antrian", "antrian", Policy::$DEFAULT_COOKIE_KEEP,"help/user/antrian")
				->addPolicy("setting_layanan", "setting_layanan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/setting_layanan")
				->addPolicy("daftar", "daftar", Policy::$DEFAULT_COOKIE_CHANGE,"modul/daftar")
				->addPolicy("bed_kamar", "daftar", Policy::$DEFAULT_COOKIE_KEEP,"modul/daftar")
				->addPolicy("aok", "daftar", Policy::$DEFAULT_COOKIE_KEEP,"modul/daftar")
				->addPolicy("aokgudang", "daftar", Policy::$DEFAULT_COOKIE_KEEP,"modul/daftar")
				->addPolicy("aokruang", "daftar", Policy::$DEFAULT_COOKIE_KEEP,"modul/daftar")
				->addPolicy("aokgudangheroff", "daftar", Policy::$DEFAULT_COOKIE_KEEP,"modul/daftar")
				->addPolicy("pulangmassal", "daftar", Policy::$DEFAULT_COOKIE_KEEP,"modul/daftar")
				->addPolicy("gizi", "gizi", Policy::$DEFAULT_COOKIE_CHANGE,"modul/gizi")
				->addPolicy("laporan_diagnosa_rehab_medis", "laporan_diagnosa_rehab_medis", Policy::$DEFAULT_COOKIE_CHANGE, "modul/laporan_diagnosa_rehab_medis");
	
	$policy		->addPolicy("layanan", "layanan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/layanan")
				->addPolicy("diagnosa","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("diagnosa_gz","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("diagnosa_rehab_medis", "layanan", Policy::$DEFAULT_COOKIE_KEEP, "modul/layanan")
				->addPolicy("asuhan_obat","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("alergi_obat","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("odontogram","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("tindakan_perawat","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("tindakan_perawat_igd","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("tindakan_dokter","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("konsul_dokter","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("konsultasi_dokter","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("visite_dokter","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("vk","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("ok","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("rr","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("faal_paru","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("endoscopy","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("bronchoscopy","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("audiometry","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("spirometry","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("ekg","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("aol","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("oksigen_central","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("oksigen_manual","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("bed","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("alok","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("rujukan","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("laboratory","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("radiology","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("fisiotherapy","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("elektromedis","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("mcu","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("bank_darah","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("indikator","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_gigimulut","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_prolanis","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_operasi","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_igd","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_kb","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_rehabilitasi_medik","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_keluarga_berencana","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_persalinan","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_jiwa","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("total_tagihan_kasir","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("tagihan_rumus","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("mr_icd_tindakan","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_pkhusus","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("lap_perinatologi","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("biaya","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("rencana_bpjs","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("e_resep","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("dokter_dpjp","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("resumemedis","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("plebitis","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("cauti","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("imunisasi","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("set_plebitis","layanan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/set_plebitis")
				->addPolicy("set_cauti","layanan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/set_cauti")
				->addPolicy("get_total_tagihan","layanan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_total_tagihan")
				->addPolicy("makanan_gizi","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("makanan_gizi_layanan","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("diskon_kasir","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("surat_sakit","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("request_plafon","layanan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/request_plafon")
				->addPolicy("chooser_kamar","layanan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/chooser_kamar")
				->addPolicy("infeksi","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("jenis_perawatan","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("cek_employee","layanan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/cek_employee")
				->addPolicy("file_resume_medis","layanan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/file_resume_medis");
	
	$policy		->addPolicy("asesmen_syaraf","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
				->addPolicy("asesmen_anak","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
    			->addPolicy("asesmen_perawat","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
    			->addPolicy("asesmen_jantung","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
    			->addPolicy("asesmen_psikologi","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan")
    			->addPolicy("asesmen_psikiatri","layanan", Policy::$DEFAULT_COOKIE_KEEP,"modul/layanan");
	
	$policy		->addPolicy("rekap_tagihan_pasien_rm","rekap_tagihan_pasien_rm",Policy::$DEFAULT_COOKIE_CHANGE,"modul/rekap_tagihan_pasien_rm")
				->addPolicy("total_tagihan_kasir_rm","rekap_tagihan_pasien_rm",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/rekap_tagihan_pasien_rm/total_tagihan_kasir_rm");
	
	$policy		->addPolicy("laporan_pendapatan", "laporan_pendapatan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_pendapatan")
				->addPolicy("tindakan_perawat_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("tindakan_igd_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("tindakan_dokter_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("konsul_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("visite_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("periksa_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("alat_obat_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("oksigen_central_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("oksigen_manual_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("bed_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan")
				->addPolicy("operasi_detail","laporan_pendapatan", Policy::$DEFAULT_COOKIE_KEEP,"modul/laporan_pendapatan");
	
	$policy		->addPolicy("rekap_stok_depo","rekap_stok_depo",Policy::$DEFAULT_COOKIE_CHANGE,"modul/rekap_stok_depo");
				
				
	$policy		->combinePolicy($ipolicy);
	$policy		->initialize();