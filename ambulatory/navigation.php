<?php
global $NAVIGATOR;

// Administrator Top Menu
$mr = new Menu ("fa fa-ambulance");
$mr->addProperty ( 'title', 'Ambulans' );
$mr->addProperty ( 'name', 'Ambulan' );
$mr->addSubMenu ( "Pemakaian Ambulans", "ambulatory", "ambulan", "Pemakaian Ambulan" ,"fa fa-ambulance");
$mr->addSubMenu ( "Pemakaian Ambulan/KM", "ambulatory", "ambulan_km", "Pemakaian Ambulan menggunakan KM" ,"fa fa-ambulance");
$mr->addSubMenu ( "Pemakaian Ambulan/BBM", "ambulatory", "ambulan_bbm", "Pemakaian Ambulan menggunakan BBM" ,"fa fa-ambulance");
$mr->addSubMenu ( "Sewa Ambulans", "ambulatory", "sewa_ambulan", "Pemakaian Ambulan" ,"fa fa-ambulance");
$mr->addSubMenu ( "Laporan", "ambulatory", "laporan", "Laporan" ,"fa fa-book");
$mr->addSubMenu ( "Data Induk", "ambulatory", "datainduk", "Laporan" ,"fa fa-database");
$mr->addSubMenu ( "Settings", "ambulatory", "settings", "Settings" ,"fa fa-cog");
require_once 'smis-libs-inventory/navigation.php';
$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Ambulan", "ambulatory");
$mr = $inventory_navigator->extendMenu($mr);
$NAVIGATOR->addMenu ( $mr, 'ambulatory' );
?>
