<?php 
require_once 'smis-libs-class/MasterTemplate.php';
global $db;
$fakultas=new MasterTemplate($db, "smis_amb_mobil", "ambulatory", "mobil");
$uitable=$fakultas->getUItable();
$header=array("No.","Mobil","Status","Plat","Keterangan");
$uitable->setHeader($header);
$uitable->addModal("id", "hidden", "", "")
		->addModal("mobil", "text", "Mobil", "")
		->addModal("nomor", "text", "Plat", "")
		->addModal("keterangan","textarea", "Keterangan", "");
$adapter=$fakultas->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Mobil", "mobil")
		->add("Plat", "nomor")
		->add("Keterangan","keterangan")
		->add("Status","status_mobil", "trivial_0_Tidak Dipakai_Dipakai");
$fakultas->setModalTitle("Mobil");
$fakultas->initialize();
?>