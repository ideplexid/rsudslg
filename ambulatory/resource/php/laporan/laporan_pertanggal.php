<?php 
global $db;


if(isset($_POST['super_command']) && $_POST['super_command']=="dokter") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("dokter");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"dokter");
	$super=new SuperCommand();
	$super->addResponder("dokter",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

if(isset($_POST['super_command']) && $_POST['super_command']=="supir") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("supir");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"supir");
	$super=new SuperCommand();
	$super->addResponder("supir",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}


$head=array("Tanggal","NRM","Noreg","Pasien","Jenis Mobil","KM","Kelas","Supir","Biaya Jarak","Jasa Perawat","Biaya Desinfektan","Total Biaya","Tujuan","Dokter","Perawat");
$uitable=new Table($head);
$uitable->setAction(false);
$uitable->setName("laporan_pertanggal");
if(isset($_POST['command'])){
    require_once 'ambulatory/class/adapter/LapSupirAdapter.php';
    require_once 'ambulatory/class/responder/LaporanPerTanggalResponder.php';
    
	$adapter=new SummaryAdapter();
    $adapter->add("Pasien", "nama_pasien")
            ->add("Noreg", "noreg_pasien")
            ->add("NRM", "nrm_pasien")
            ->add("KM", "jarak","back KM")
           ->add("Jenis Mobil", "jenis_mobil")
           ->add("Kelas", "kelas","unslug")
		   ->add("Tanggal", "waktu","date d M Y")
           ->add("Supir", "nama_sopir")
           ->add("Dokter", "nama_dokter")
           ->add("Perawat", "nama_perawat")
		   ->add("Kondisi", "kondisi")
		   ->add("Biaya Jarak", "biaya_perjalanan","money Rp.")
		   ->add("Total Biaya", "total_biaya","money Rp.")
		   ->add("Jasa Perawat", "biaya_perawat","money Rp.")
		   ->add("Biaya Desinfektan", "biaya_desinfektan","money Rp.")
           ->add("Tujuan", "tujuan");
    $adapter->addFixValue("Supir","<strong>Total</strong>");
    $adapter->addSummary("Biaya Jarak", "biaya_perjalanan","money Rp.")
            ->addSummary("Total Biaya", "total_biaya","money Rp.")
            ->addSummary("Jasa Perawat", "biaya_perawat","money Rp.")
			->addSummary("Biaya Desinfektan", "biaya_desinfektan", "money Rp.");
$dbtable=new DBTable($db, "smis_amb_ambulan");
	$dbres=new LaporanPerTanggalResponder($dbtable, $uitable, $adapter);
	if($dbres->isView() || $dbres->isExcel()){
		$dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'");
        if(isset($_POST['id_supir']) && $_POST['id_supir'] != "" && $_POST['nama_supir']!="") {
            $dbtable->addCustomKriteria("id_sopir", "='".$_POST['id_supir']."'");
        }
        if(isset($_POST['id_dokter']) && $_POST['id_dokter'] != "" && $_POST['nama_dokter']!="") {
            $dbtable->addCustomKriteria("id_dokter", "='".$_POST['id_dokter']."'");
        }
		$dbtable->addCustomKriteria(" jenis_bbm ", " = '' ");
	}
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$load=new Button("", "", "");
$load->setClass("btn-primary");
$load->setIsButton(Button::$ICONIC);
$load->setIcon("fa fa-refresh");
$load->setAction("laporan_pertanggal.view()");

$excel=new Button("", "", "");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setAction("laporan_pertanggal.excel()");

$btgrup=new ButtonGroup("");
$btgrup->addButton($load)
	   ->addButton($excel);


$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$uitable->addModal("nama_dokter", "chooser-laporan_pertanggal-dokter", "Dokter", "","y",NULL);
$uitable->addModal("id_dokter", "hidden", "", "");
$uitable->addModal("nama_supir", "chooser-laporan_pertanggal-supir", "Supir", "","y",NULL);
$uitable->addModal("id_supir", "hidden", "", "");
$form=$uitable->getModal()->getForm();
$form->addElement("", $btgrup);

echo $form->getHtml();
echo $uitable->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script type="text/javascript">
var laporan_pertanggal;
var supir;
var dokter;
$(document).ready(function(){
	$(".mydate").datepicker();
	laporan_pertanggal=new TableAction("laporan_pertanggal","ambulatory","laporan_pertanggal");
	laporan_pertanggal.addRegulerData=function(reg_data){
		reg_data['dari']=$("#laporan_pertanggal_dari").val();
        reg_data['sampai']=$("#laporan_pertanggal_sampai").val();
        reg_data['nama_dokter']=$("#laporan_pertanggal_nama_dokter").val();
        reg_data['id_dokter']=$("#laporan_pertanggal_id_dokter").val();
        reg_data['nama_supir']=$("#laporan_pertanggal_nama_supir").val();
        reg_data['id_supir']=$("#laporan_pertanggal_id_supir").val();
		return reg_data;
    };
    
    supir=new TableAction("supir","ambulatory","laporan_pertanggal");
	supir.setSuperCommand("supir");
	supir.selected=function(json){
		$("#laporan_pertanggal_nama_supir").val(json.nama);
		$("#laporan_pertanggal_id_supir").val(json.id);
    };
    
    dokter=new TableAction("dokter","ambulatory","laporan_pertanggal");
	dokter.setSuperCommand("dokter");
	dokter.selected=function(json){
		$("#laporan_pertanggal_nama_dokter").val(json.nama);
		$("#laporan_pertanggal_id_dokter").val(json.id);
	};
});

</script>