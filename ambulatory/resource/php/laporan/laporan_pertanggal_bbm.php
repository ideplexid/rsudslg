<?php 
global $db;


if(isset($_POST['super_command']) && $_POST['super_command']=="dokter_bbm") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("dokter_bbm");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"dokter");
	$super=new SuperCommand();
	$super->addResponder("dokter_bbm",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

if(isset($_POST['super_command']) && $_POST['super_command']=="supir_bbm") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("supir_bbm");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"supir");
	$super=new SuperCommand();
	$super->addResponder("supir_bbm",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}


$head=array("Tanggal","NRM","Noreg","Pasien","Jenis Mobil","Jenis BBM","Jarak","Kelas","Supir","Total Biaya","Lokasi","Dokter","Keterangan");
$uitable=new Table($head);
$uitable->setAction(false);
$uitable->setName("laporan_pertanggal_bbm");
if(isset($_POST['command'])){
    require_once 'ambulatory/class/adapter/LapSupirAdapter.php';
    require_once 'ambulatory/class/responder/LaporanPerTanggalBBMResponder.php';
    
	$adapter=new SummaryAdapter();
    $adapter->add("Pasien", "nama_pasien")
            ->add("Noreg", "noreg_pasien")
            ->add("NRM", "nrm_pasien")
            ->add("Jarak", "jarak","back KM")
            ->add("Jenis BBM", "jenis_bbm")
            ->add("Jenis Mobil", "jenis_mobil")
           ->add("Kelas", "kelas","unslug")
		   ->add("Tanggal", "waktu","date d M Y")
           ->add("Supir", "nama_sopir")
           ->add("Dokter", "nama_dokter")
           ->add("Perawat", "nama_perawat")
		   ->add("Kondisi", "kondisi")
		   ->add("Biaya Jarak", "biaya_perjalanan","money Rp.")
		   ->add("Total Biaya", "total_biaya","money Rp.")
		   ->add("Jasa Perawat", "biaya_perawat","money Rp.")
		   ->add("Biaya Desinfektan", "biaya_desinfektan","money Rp.")
           ->add("Lokasi", "tujuan")
           ->add("Keterangan", "keterangan");
    $adapter->addFixValue("Supir","<strong>Total</strong>");
    $adapter->addSummary("Biaya Jarak", "biaya_perjalanan","money Rp.")
            ->addSummary("Total Biaya", "total_biaya","money Rp.")
            ->addSummary("Jasa Perawat", "biaya_perawat","money Rp.")
			->addSummary("Biaya Desinfektan", "biaya_desinfektan", "money Rp.");
$dbtable=new DBTable($db, "smis_amb_ambulan");
	$dbres=new LaporanPerTanggalBBMResponder($dbtable, $uitable, $adapter);
	if($dbres->isView() || $dbres->isExcel()){
		$dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'");
        if(isset($_POST['id_supir']) && $_POST['id_supir'] != "" && $_POST['nama_supir']!="") {
            $dbtable->addCustomKriteria("id_sopir", "='".$_POST['id_supir']."'");
        }
        if(isset($_POST['id_dokter']) && $_POST['id_dokter'] != "" && $_POST['nama_dokter']!="") {
            $dbtable->addCustomKriteria("id_dokter", "='".$_POST['id_dokter']."'");
        }
        $dbtable->addCustomKriteria(" jenis_bbm ", " !='' ");
	}
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$load=new Button("", "", "");
$load->setClass("btn-primary");
$load->setIsButton(Button::$ICONIC);
$load->setIcon("fa fa-refresh");
$load->setAction("laporan_pertanggal_bbm.view()");

$excel=new Button("", "", "");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setAction("laporan_pertanggal_bbm.excel()");

$btgrup=new ButtonGroup("");
$btgrup->addButton($load)
	   ->addButton($excel);


$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$uitable->addModal("nama_dokter", "chooser-laporan_pertanggal_bbm-dokter_bbm", "Dokter", "","y",NULL);
$uitable->addModal("id_dokter", "hidden", "", "");
$uitable->addModal("nama_supir", "chooser-laporan_pertanggal_bbm-supir_bbm", "Supir", "","y",NULL);
$uitable->addModal("id_supir", "hidden", "", "");
$form=$uitable->getModal()->getForm();
$form->addElement("", $btgrup);

echo $form->getHtml();
echo $uitable->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script type="text/javascript">
var laporan_pertanggal_bbm;
var supir_bbm;
var dokter_bbm;
$(document).ready(function(){
	$(".mydate").datepicker();
	laporan_pertanggal_bbm=new TableAction("laporan_pertanggal_bbm","ambulatory","laporan_pertanggal_bbm");
	laporan_pertanggal_bbm.addRegulerData=function(reg_data){
		reg_data['dari']=$("#laporan_pertanggal_bbm_dari").val();
        reg_data['sampai']=$("#laporan_pertanggal_bbm_sampai").val();
        reg_data['nama_dokter']=$("#laporan_pertanggal_bbm_nama_dokter").val();
        reg_data['id_dokter']=$("#laporan_pertanggal_bbm_id_dokter").val();
        reg_data['nama_supir']=$("#laporan_pertanggal_bbm_nama_supir").val();
        reg_data['id_supir']=$("#laporan_pertanggal_bbm_id_supir").val();
		return reg_data;
    };
    
    supir_bbm=new TableAction("supir_bbm","ambulatory","laporan_pertanggal_bbm");
	supir_bbm.setSuperCommand("supir_bbm");
	supir_bbm.selected=function(json){
		$("#laporan_pertanggal_bbm_nama_supir").val(json.nama);
		$("#laporan_pertanggal_bbm_id_supir").val(json.id);
    };
    
    dokter_bbm=new TableAction("dokter_bbm","ambulatory","laporan_pertanggal_bbm");
	dokter_bbm.setSuperCommand("dokter_bbm");
	dokter_bbm.selected=function(json){
		$("#laporan_pertanggal_bbm_nama_dokter").val(json.nama);
		$("#laporan_pertanggal_bbm_id_dokter").val(json.id);
	};
});

</script>