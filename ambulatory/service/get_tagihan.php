<?php
global $db;

if (isset ( $_POST ['noreg_pasien'] )) {    
	$noreg = $_POST ['noreg_pasien'];
	$response ['selesai'] = "1";
	$response ['exist'] = "1";
	$response ['reverse'] = "0";
	$response ['cara_keluar'] = "Selesai";
	$dbtable = new DBTable ( $db, "smis_amb_ambulan" );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$rows = $data ['data'];
	
    require_once "smis-base/smis-include-synchronize-db.php";
    require_once "ambulatory/class/adapter/AmbulanTagihanAdapter.php";
    
    $adapter=new AmbulanTagihanAdapter();
    $result=$adapter->getContent($rows);
	$unit_data = array (
			"result" => $result,
			"jasa_pelayanan" => "0" 
	);
	$ldata ['ambulan'] = $unit_data;
	$response ['data'] = $ldata;
	echo json_encode ( $response );
}

?>