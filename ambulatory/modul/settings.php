<?php
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	global $db;	
	$smis = new SettingsBuilder ( $db, "ambulan_settings", "ambulatory", "settings" );
	$smis->setShowDescription ( true );
	$smis->setTabulatorMode ( Tabulator::$POTRAIT );
	$option=new OptionBuilder();
	
    $smis->addTabs ( "ambulan", "Ambulan"," fa fa-ambulance" );
	if($smis->isGroup("ambulan")){
        $smis->addItem ( "ambulan", new SettingsItem ( $db, "smis-rs-ambulan-mobil", "Tampilkan Pilihan Ambulan", "", "checkbox", "Tampilkan Ambulan" ) );
        $smis->addItem ( "ambulan", new SettingsItem ( $db, "smis-ambulan-autosync-cashier", "Auto Synch Ambulan", "", "checkbox", "Auto Sinkronisasi data tagihan Ambulan ke Kasir" ) );
        $smis->addItem ( "ambulan", new SettingsItem ( $db, "smis-ambulan-slug-supir", "Slug untuk Supir", "supir", "text", "Slug untuk Pilihan Petugas Supir" ) );
        $smis->addItem ( "ambulan", new SettingsItem ( $db, "smis-ambulan-active-tutup-tagihan", "Aktifkan Tutup Tagihan", "", "checkbox", "" ) );
        $smis->addItem ( "ambulan", new SettingsItem ( $db, "smis-ambulan-persen-jasa-perawat", "Persentase Jasa Perawat", "", "text", "" ) );
    }
    $smis->addTabs ( "accounting", "Accounting","fa fa-usd" );
	if($smis->isGroup("accounting")){
        $smis->addItem ( "accounting", new SettingsItem ( $db, "ambulan-accounting-auto-notif", "Aktifkan Setting Auto Notif ke Accounting", "0", "checkbox", "Jika Dicentang Maka Sistem Akan Menotifikasi ke Accounting Secara Otomatis" ) );
        
        $smis->addItem ( "accounting", new SettingsItem ( $db, "ambulan-accounting-debet-penggunaan-mobil", "Kode Accounting untuk Debet Penggunaan Mobil Ambulan", "", "chooser-settings-debet_penggunaan_mobil-Debet", "Kode Accounting untuk Debet Penggunaan Mobil Ambulan" ) );
        $smis->addItem ( "accounting", new SettingsItem ( $db, "ambulan-accounting-kredit-penggunaan-mobil", "Kode Accounting untuk Kredit Penggunaan Mobil Ambulan", "", "chooser-settings-kredit_penggunaan_mobil-Kredit", "Kode Accounting untuk Kredit Penggunaan Mobil Ambulan" ) );
        $smis->addSuperCommandAction("debet_penggunaan_mobil","kode_akun");
        $smis->addSuperCommandAction("kredit_penggunaan_mobil","kode_akun");
        $smis->addSuperCommandArray("debet_penggunaan_mobil","ambulan-accounting-debet-penggunaan-mobil","nomor",true);
        $smis->addSuperCommandArray("kredit_penggunaan_mobil","ambulan-accounting-kredit-penggunaan-mobil","nomor",true);
    }
    $smis->setPartialLoad(true);
    $smis->init ();
    
?>