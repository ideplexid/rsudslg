<?php
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
global $db;
$header=array ('Nama','Jabatan',"NIP" );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dktable = new Table ( $header );
$dktable->setName ( "perawat" );
$dktable->setModel ( Table::$SELECT );
$perawat = new EmployeeResponder ( $db, $dktable, $dkadapter, "" );

$dktable = new Table ( $header );
$dktable->setName ( "supir" );
$dktable->setModel ( Table::$SELECT );
$supir = new EmployeeResponder ( $db, $dktable, $dkadapter, getSettings($db,"smis-ambulan-slug-supir","") );


/* LOKASI */
$header=array ("Jenis Mobil","Durasi Hari",'Durasi Jam' );
$lokasi = new Table ( $header );
$lokasi->setName ( "lokasi" );
$lokasi->setModel ( Table::$SELECT );
$adapter = new SimpleAdapter ();
$adapter->add ( "Jenis Mobil", "jenis_mobil" );
$adapter->add ( "Durasi Hari", "durasi_hari","money Rp." );
$adapter->add ( "Durasi Jam", "durasi_jam", "money Rp." ); 
$lresponder = new ServiceResponder ( $db, $lokasi, $adapter, "get_sewa_ambulan" );

$super = new SuperCommand ();
$super->addResponder ( "perawat", $perawat );
$super->addResponder ( "supir", $supir );
$super->addResponder ( "lokasi", $lresponder );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}

$header=array ("No.",'Tanggal','Jenis Mobil',"Durasi Jam","Durasi Hari","Biaya","Tujuan","Paramedis","Supir","Selesai");
$uitable = new Table ( $header, "&nbsp;", NULL, true );
$uitable->setName ( "sewa_ambulan" );



/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    //$dbtable = new DBTable ( $db, "smis_amb_ambulan" );
    $dbtable = new DBTable ( $db, "smis_amb_sewa_ambulan" );
    $adapter = new SimpleAdapter();
    $adapter ->setUseNumber(true,"No.","back.");
    $adapter ->add("Tanggal","tanggal","date d M Y");
    $adapter ->add("Jenis Mobil","jenis_mobil");
    $adapter ->add("Durasi Jam","durasi_jam","back Jam");
    $adapter ->add("Durasi Hari","durasi_hari","back Hari");
    $adapter ->add("Biaya","biaya_perjalanan","money Rp.");
    $adapter ->add("Paramedis","nama_perawat");
    $adapter ->add("Tujuan","tujuan");
    $adapter ->add("Supir","nama_sopir");
    $adapter ->add("Selesai","selesai","trivial_0_Belum_Selesai");
    $dbres= new DBResponder($dbtable,$uitable,$adapter);
    

    if($dbres->isSave()){
        $harga_hari = $_POST['harga_hari'];
        $harga_jam = $_POST['harga_jam'];

        $durasi_hari = $_POST['durasi_hari'];
        $durasi_jam = $_POST['durasi_jam'];

        if($durasi_hari*1>0){
            $dbres->addColumnFixValue("biaya_perjalanan",$durasi_hari*$harga_hari);
        }else if($durasi_jam*1>0){
            if($durasi_jam*1<24){
                $dbres->addColumnFixValue("biaya_perjalanan",$durasi_jam*$harga_jam);
            }else if($durasi_jam*1==24){
                $dbres->addColumnFixValue("biaya_perjalanan",$harga_hari);
            }else{
                $durasi_hari = ceil($durasi_jam/24);
                $dbres->addColumnFixValue("biaya_perjalanan",$durasi_hari*$harga_hari);
            }
        }
    }

    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}



$pemakaian=getSettings($db, "smis-rs-ambulan-mobil", "0")=="1";
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_perawat", "hidden", "", "" );
$uitable->addModal ( "id_sopir", "hidden", "", "" );
$uitable->addModal ( "tanggal", "date", "Tanggal", "" );
$uitable->addModal ( "nama", "text", "Nama Penyewa", "" );

$uitable->addModal ( "jenis_mobil", "chooser-sewa_ambulan-lokasi", "Jenis Mobil", "" );
$uitable->addModal ( "id_jenis_mobil", "hidden", "", "" );
$uitable->addModal ( "harga_jam", "hidden", "", "","n",null,true );
$uitable->addModal ( "harga_hari", "hidden", "", "","n",null,true );
$uitable->addModal ( "durasi_jam", "text", "Durasi Jam", "","y",NULL,false );
$uitable->addModal ( "durasi_hari", "text", "Durasi Hari", "","y",NULL,false );
$uitable->addModal ( "tujuan", "text", "Tujuan", "" );

$uitable->addModal ( "nama_perawat", "chooser-sewa_ambulan-perawat", "Paramedis", "" );
$uitable->addModal ( "nama_sopir", "chooser-sewa_ambulan-supir-Supir", "Supir", "" );
$uitable->addModal ( "selesai", "checkbox", "Selesai", "" );


$modal = $uitable->getModal ();
$modal->setTitle ( "Sewa Ambulance" );


/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "ambulatory/resource/js/sewa_ambulan.js",false );

?>
