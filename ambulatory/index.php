<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	
	$policy=new Policy("ambulatory", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
    
    $policy->addPolicy("testing","testing",Policy::$DEFAULT_COOKIE_CHANGE,"modul/testing");
    $policy->addPolicy("tab_satu","testing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/testing/tab_satu");
    $policy->addPolicy("tab_dua","testing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/testing/tab_dua");
    $policy->addPolicy("tab_tiga","testing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/testing/tab_tiga");
    $policy->addPolicy("tab_empat","testing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/testing/tab_empat");
    $policy->addPolicy("tab_lima","testing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/testing/tab_lima");
    $policy->addPolicy("tab_enam","testing",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/testing/tab_enam");
    
	$policy->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
	
    $policy->addPolicy("datainduk","datainduk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/datainduk");
	$policy->addPolicy("mobil", "datainduk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/datainduk/mobil");
	$policy->addPolicy("tarif", "datainduk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/datainduk/tarif");
	
    $policy->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
	$policy->addPolicy("laporan_persupir", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_persupir");
	$policy->addPolicy("laporan_pertanggal", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_pertanggal");
	$policy->addPolicy("laporan_pertanggal_bbm", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_pertanggal_bbm");
	
	$policy->addPolicy("laporan_sewa_ambulan", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_sewa_ambulan");
	$policy->addPolicy("ambulan", "ambulan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/ambulan");
	$policy->addPolicy("ambulan_km", "ambulan_km", Policy::$DEFAULT_COOKIE_CHANGE,"modul/ambulan_km");
	$policy->addPolicy("ambulan_bbm", "ambulan_bbm", Policy::$DEFAULT_COOKIE_CHANGE,"modul/ambulan_bbm");
    $policy->addPolicy("sewa_ambulan", "sewa_ambulan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/sewa_ambulan");

    $policy->addPolicy("kode_akun", "settings", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
	
    $inventory_policy = new InventoryPolicy("ambulatory", $user,"modul/");
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>
