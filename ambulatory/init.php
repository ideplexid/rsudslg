<?php
global $PLUGINS;

$init ['name'] = 'ambulatory';
$init ['path'] = SMIS_DIR . "ambulatory/";
$init ['description'] = "Ambulans untuk pasien";
$init ['require'] = "administrator, registration";
$init ['service'] = "";
$init ['type'] = "";
$init ['version'] = "2.1.6";
$init ['number'] = "16";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>