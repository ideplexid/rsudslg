<?php 

/**
 * 
 * this class used for determining
 * and auto synchronize data automatically  
 * to cashier based on it's current needed
 * 
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * @database    : smis_amb_ambulan
 * @version     : 1.0.1
 * @since       : 09-Mar-2017
 * @copyright   : goblooge@gmail.com
 * 
 * */

class AmbulatoryDBTable extends DBTable{
    private $autosynch;
    public function __construct($db,$name,$column=NULL){
        parent::__construct($db,$name,$column);
        $this->autosynch=false;
    }
    /**
     * @brief set whether should auto synchronize or not
     * @param boolean $auto 
     * @return  
     */
    public function setAutoSynch($auto){
        $this->autosynch=$auto;
        return $this;
    }
    
    /**
     * @brief override DBTable->insert();
     */
    public function insert($data,$use_prop=true,$warp=null){
        $result=parent::insert($data,$use_prop,$warp);
        if($this->autosynch){
            $row=$this->select($this->get_inserted_id());
            $this->synchToCashier($row);
        }
		return $result;
	}
    
    /**
     * @brief override DBTable->update();
     */
    public function update($data,$id,$warp=null){
		$result=parent::update($data,$id,$warp);
        if($this->autosynch){
            if(isset($data['prop']) && $data['prop']=="del"){
                $row=$this->select($id,false,false);
                $this->synchToCashier($row,"del");
            }else{
                $row=$this->select($id);
                $this->synchToCashier($row);
            }
            
        }
		return $result;
	}
    
    /**
     * @brief synchronize data through service to cashier
     * @param Array/Object $raw 
     * @param String $prop, if delete then prop should be filled with string 'del' 
     * @return  null
     */
    private function synchToCashier($raw,$prop=""){
        require_once "smis-base/smis-include-service-consumer.php";
        $data=array();
        $data['grup_name']      = "ambulan";
        $data['nama_pasien']    = $raw->nama_pasien;
        $data['noreg_pasien']   = $raw->noreg_pasien;
        $data['nrm_pasien']     = $raw->nrm_pasien;
        $data['entity']         = "ambulan";
        $data['list']           = $this->adapt($raw,$prop);
        $serv = new ServiceConsumer($this->get_db(),"proceed_receivable",$data,"kasir");
        $serv->execute();
        $serv->getContent();
    }
	
    /**
     * @brief this function used to adapt one record in this entity
     *          should become how much record in cashier
     * @param Array/Object $d 
     * @param String $prop , if del, then prop should be filled with string 'del' 
     * @return  Array $result that contains data of one record becoming.
     */
    public function adapt($d,$prop=""){
        $result=array();
        require_once "ambulatory/class/adapter/AmbulanTagihanAdapter.php";
        $adapter=new AmbulanTagihanAdapter();
        $adapter->setProp($prop);
        $result=$adapter->adapt($d);
        return $result;
    }
    
}


?>