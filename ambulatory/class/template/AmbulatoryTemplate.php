<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'gizi/service/RuanganService.php';
require_once 'ambulatory/class/adapter/AmbulanAdapter.php';

class AmbulatoryTemplate extends ModulTemplate {
    protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
    protected $action;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    public static $MODE_DAFTAR  = "pendaftaran";
    
    public function __construct ($db, $mode, $polislug = "all", $page = "ambulatory", $action = "ambulan", $protoslug = "", $protoname = "", $protoimplement = "", $noreg = "", $nama = "", $nrm = "") {
        $this->db               = $db;
        $this->mode             = $mode;
        $this->polislug         = $polislug;
        $this->page             = $page;
        $this->action           = $action;
        $this->protoslug        = $protoslug;
        $this->protoname        = $protoname;
        $this->protoimplement   = $protoimplement;
        $this->dbtable          = new DBTable ( $this->db, "smis_amb_ambulan" );
        $this->noreg_pasien     = $noreg;
        $this->nama_pasien      = $nama;
        $this->nrm_pasien       = $nrm;
        $thead                  = array("Tanggal", "Biaya", "Keterangan");
        $this->uitable          = new Table($thead, "", NULL, true);
        $this->uitable->setName("ambulatory");
    }
    
    public function command($command) {
        $adapter = new AmbulanAdapter ();
        $dbres   = new DBResponder( $this->dbtable, $this->uitable, $adapter );
        if ($this->noreg_pasien != ""){
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $this->noreg_pasien. "'" );
        }
        $data    = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    }
    
    public function phpPreLoad() {
        $pemakaian=getSettings($db, "smis-rs-ambulan-mobil", "0")=="1";
        $this->uitable->addModal ( "id", "hidden", "", "" );
        $this->uitable->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i:s" ) );
        if($this->mode == self::$MODE_DAFTAR) {
            $this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
            $this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
            $this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "ruangan", "hidden", "", $this->polislug, "n", null, true );
            $this->uitable->addModal ( "tanggal", "date", "", date ( "Y-m-d" ), "n", null, true );
            if($pemakaian){
                $this->uitable->addModal ( "mobil", "chooser-ambulan-mobil_ambulan-Mobil", "Mobil", "" );
                $this->uitable->addModal ( "id_mobil", "hidden", "", "" );
            }else{
                $this->uitable->addModal ( "mobil", "hidden", "", "" );
                $this->uitable->addModal ( "id_mobil", "hidden", "", "" );
            }
            $this->uitable->addModal ( "nama_sopir", "chooser-ambulan-supir-Supir", "Supir", "" );
            $this->uitable->addModal ( "lokasi", "chooser-ambulan-lokasi-Lokasi", "Lokasi", "" );
            $this->uitable->addModal ( "kondisi", "text", "Kondisi", "","y",NULL,true );
            $this->uitable->addModal ( "gaji_sopir", "money", "Gaji Supir", "" );
            $this->uitable->addModal ( "biaya_perjalanan", "money", "Biaya", "" );
            $this->uitable->addModal ( "biaya_perawat", "money", "Biaya Perawat", "" );
            $this->uitable->addModal ( "biaya_dokter", "money", "Biaya Dokter", "" );
            $this->uitable->addModal ( "biaya_alat", "money", "Biaya Alat", "" );
            $this->uitable->addModal ( "biaya_lain", "money", "Biaya Lain", "" );
            $this->uitable->addModal ( "nama_dokter", "chooser-ambulan-dokter-Dokter", "Dokter", "" );
            $this->uitable->addModal ( "nama_perawat", "chooser-ambulan-perawat-Perawat", "Perawat", "" );
            $this->uitable->addModal (" keterangan", "textarea", "Keterangan", "" );
        }
        $modal = $this->uitable->getModal();
        $modal ->setTitle("Ambulan");
        echo $modal ->getHtml();
        echo $this  ->uitable->getHtml();
    }
    
    public function superCommand($super_command) {
        $super = new SuperCommand ();
        if($super_command == 'dokter') {
            $header=array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ( $header );
            $dktable->setName ( "dokter" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add ( "Jabatan", "nama_jabatan" );
            $dkadapter->add ( "Nama", "nama" );
            $dkadapter->add ( "NIP", "nip" );
            $dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );
            $super->addResponder ( "dokter", $dokter );
        } if($super_command == 'perawat') {
            $header=array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ( $header );
            $dktable->setName ( "perawat" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add ( "Jabatan", "nama_jabatan" );
            $dkadapter->add ( "Nama", "nama" );
            $dkadapter->add ( "NIP", "nip" );
            $dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "perawat" );
            $super->addResponder ( "perawat", $perawat );
        } if($super_command == 'supir') {
            $header=array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ( $header );
            $dktable->setName ( "supir" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter = new SimpleAdapter ();
            $dkadapter->add ( "Jabatan", "nama_jabatan" );
            $dkadapter->add ( "Nama", "nama" );
            $dkadapter->add ( "NIP", "nip" );
            $dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "supir" );
        } if($super_command == 'mobil_ambulan') {
            $dktable = new Table ( array("Mobil","Plat","Keterangan") );
            $dktable->setName ( "mobil_ambulan" );
            $dktable->setModel ( Table::$SELECT );
            $adapter=new SimpleAdapter();
            $adapter->add("Mobil", "mobil");
            $adapter->add("Plat", "nomor");
            $adapter->add("Keterangan", "keterangan");
            $dbtable=new DBTable($db, "smis_amb_mobil");
            $mobil = new DBResponder ( $dbtable,$dktable, $adapter );
            $super->addResponder ( "mobil_ambulan", $mobil);
        } if($super_command == 'lokasi') {
            $header=array ('Nama','Biaya',"Jasa Sopir" ,"Kondisi");
            $lokasi = new Table ( $header );
            $lokasi->setName ( "lokasi" );
            $lokasi->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Nama", "lokasi" );
            $padapter->add ( "Kondisi", "doa" );
            $padapter->add ( "Biaya", "biaya", "money Rp." );
            $padapter->add ( "Jasa Sopir", "jasa_supir", "money Rp." );
            $lresponder = new ServiceResponder ( $db, $lokasi, $padapter, "get_ambulan" );
            $super->addResponder ( "lokasi", $lresponder );
        }
        $init = $super->initialize ();
        if ($init != null) {
            echo $init;
            return;
        }
    }
    
    public function jsLoader() {
		echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS  ("framework/smis/js/table_action.js" );
		loadLibrary ("smis-libs-function-javascript" );
	}
    
	public function cssLoader() {
		echo addCSS ("framework/bootstrap/css/datepicker.css" );
		echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
    
    public function jsPreLoad() {
?>
    <script type="text/javascript">
        var <?php echo $this->action; ?>;
        var page            = "<?php echo $this->page; ?>";
        var protoslug       = "<?php echo $this->protoslug; ?>";
		var protoname       = "<?php echo $this->protoname; ?>";
		var protoimplement  = "<?php echo $this->protoimplement; ?>";
        var noreg           = "<?php echo $this->noreg_pasien; ?>";
        var nrm             = "<?php echo $this->nrm_pasien; ?>";
        var nama            = "<?php echo $this->nama_pasien; ?>";
        var dokter;
        var perawat;
        var supir;
        var lokasi;
        var mobil_ambulan;
        $(document).ready(function() {
            $(".mydate").datepicker();
            $(".mydatetime").datetimepicker({ minuteStep: 1});
            var column=new Array(
                "id","id_perawat","id_dokter","id_sopir","mobil","id_mobil",
                "nama_sopir","nama_perawat","nama_dokter",
                "lokasi","tanggal","gaji_sopir","biaya_perjalanan",
                "biaya_dokter","biaya_alat","biaya_lain","biaya_perawat","kondisi", "keterangan"	
            );
            <?php echo $this->action; ?> = new TableAction("<?php echo $this->action; ?>", page, "<?php echo $this->action; ?>", column);
            <?php echo $this->action; ?>.setPrototipe(protoname,protoslug,protoimplement);
			<?php echo $this->action; ?>.addRegulerData=function(a){
                reg_data['noreg_pasien'] = noreg;
                reg_data['nama_pasien'] = nama;
                reg_data['nrm_pasien'] = nrm;
                return reg_data;
            };
            
            lokasi=new TableAction("lokasi", page,"<?php echo $this->action; ?>",new Array());
            lokasi.setSuperCommand("lokasi");
            lokasi.setPrototipe(protoname,protoslug,protoimplement);
            lokasi.selected=function(json){
                $("#<?php echo $this->action; ?>_lokasi").val(json.lokasi);
                setMoney("#<?php echo $this->action; ?>_biaya_perjalanan",json.biaya);
                setMoney("#<?php echo $this->action; ?>_gaji_sopir",json.jasa_supir);
                $("#<?php echo $this->action; ?>_kondisi").val(json.doa);
            };
            
            dokter=new TableAction("dokter", page,"<?php echo $this->action; ?>",new Array());
            dokter.setSuperCommand("dokter");
            dokter.setPrototipe(protoname,protoslug,protoimplement);
            dokter.selected=function(json){
                var nama=json.nama;
                var nip=json.id;		
                $("#<?php echo $this->action; ?>_nama_dokter").val(nama);
                $("#<?php echo $this->action; ?>_id_dokter").val(nip);
            };
            
            perawat=new TableAction("perawat", page,"<?php echo $this->action; ?>",new Array());
            perawat.setSuperCommand("perawat");
            perawat.setPrototipe(protoname,protoslug,protoimplement);
            perawat.selected=function(json){
                var nama=json.nama;
                var nip=json.id;		
                $("#<?php echo $this->action; ?>_nama_perawat").val(nama);
                $("#<?php echo $this->action; ?>_id_perawat").val(nip);
            };

            supir=new TableAction("supir", page,"<?php echo $this->action; ?>",new Array());
            supir.setSuperCommand("supir");
            supir.setPrototipe(protoname,protoslug,protoimplement);
            supir.selected=function(json){
                var nama=json.nama;
                var nip=json.id;		
                $("#<?php echo $this->action; ?>_nama_sopir").val(nama);
                $("#<?php echo $this->action; ?>_id_sopir").val(nip);
            };

            mobil_ambulan=new TableAction("mobil_ambulan", page,"<?php echo $this->action; ?>",new Array());
            mobil_ambulan.setSuperCommand("mobil_ambulan");
            mobil_ambulan.setPrototipe(protoname,protoslug,protoimplement);
            mobil_ambulan.selected=function(json){
                $("#<?php echo $this->action; ?>_mobil").val(json.mobil);
                $("#<?php echo $this->action; ?>_id_mobil").val(json.id);
            };

            <?php echo $this->action; ?>.view();
        });
    </script>
<?php
    }
}

?>