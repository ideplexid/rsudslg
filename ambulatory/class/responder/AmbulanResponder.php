<?php
class AmbulanResponder extends DBResponder {
    
    public function delete(){
        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		if($this->dbtable->isRealDelete()){
			$result				= $this->dbtable->delete($id['id']);
		}else{
			$data['prop']		= "del";
			$result				= $this->dbtable->update($data,$id);
        }

        $result = parent::delete();
        if(getSettings($this->dbtable->get_db(),"ambulan-accounting-auto-notif","0")=="1"){
            $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
        }
        return $result;
    }    
    
    public function save(){
		$data=$this->postToArray();
		$id['id']=$this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
            
            if($data['selesai'] == 0 || $data['selesai'] == "0") {
                // Mengupdate status mobil menjadi 1 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 1;
                $tarif_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 1 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            } else {
                // Mengupdate status mobil menjadi 0 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 0;
                $tarif_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 0 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            }
            
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
			$result=$this->dbtable->update($data,$id,$this->upsave_condition);
            
            if($data['selesai'] == 0 || $data['selesai'] == "0") {
                // Mengupdate status mobil menjadi 1 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 1;
                $tarif_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 1 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            } else {
                // Mengupdate status mobil menjadi 0 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 0;
                $tarif_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 0 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($this->dbtable->get_db(), "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            }
            
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) 
            $success['success']=0;
        if(getSettings($this->dbtable->get_db(),"ambulan-accounting-auto-notif","0")=="1"){
            $this-> synchronizeToAccounting($this->dbtable->get_db(),$success ['id'] ,"");
        }
		return $success;
    }
    
    public function cek_tutup_tagihan(){
        global $db;
        if(getSettings($db,"smis-ambulan-active-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        $total = $x->gaji_sopir + $x->tarif + $x->biaya_perjalanan + $x->biaya_perawat + $x->biaya_dokter + $x->biaya_alat + $x->biaya_lain;
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";
        $data['id_data']    = $id;
        $data['entity']     = "ambulan";
        $data['service']    = "get_detail_accounting";
        $data['data']       = $id;
        $data['code']       = "amb-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu_daftar;
        $data['uraian']     = "Ambulan Pasien ".$x->nama_pasien." Pada Noreg ".$x->id;
        $data['nilai']      = $total;
        
        $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }

    public function command($command){
        if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
            $result = $this->cek_tutup_tagihan();
            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
            return parent::command($command);
        }
    }
}