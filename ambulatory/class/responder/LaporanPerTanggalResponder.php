<?php 

class LaporanPerTanggalResponder extends DBResponder{
    public function excel(){
        
        $dbtable = $this->getDBTable();
        $dbtable ->setShowAll(true);
        $data = $dbtable ->view("","0");
        $list = $data['data'];
        require_once "smis-libs-out/php-excel/PHPExcel.php";
       
        
        $fill = array();
        $fill['borders'] = array();
        $fill['borders']['allborders'] = array();
        $fill['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

        $file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
        $sheet  ->setTitle ("Rekapitulasi");

        $sheet->mergeCells("A1:O1")->setCellValue ( "A1", "LAPORAN PEMAKAIAN AMBULAN RSUD SLG" );
        $sheet->mergeCells("A2:O2")->setCellValue ( "A2", "TANGGAL : ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']) );
        
        
        $sheet->setCellValue("A4","Tanggal");
        $sheet->setCellValue("B4","NRM");
        $sheet->setCellValue("C4","No. Reg");
        $sheet->setCellValue("D4","Pasien");
        $sheet->setCellValue("E4","Jenis Mobil");
        $sheet->setCellValue("F4","KM");
        $sheet->setCellValue("G4","Kelas");
        $sheet->setCellValue("H4","Supir");
        $sheet->setCellValue("I4","Biaya Jarak");
        $sheet->setCellValue("J4","Jasa Perawat");
        $sheet->setCellValue("K4", "Biaya Desinfektan");
        $sheet->setCellValue("L4","Total Biaya");
        $sheet->setCellValue("M4","Tujuan");
        $sheet->setCellValue("N4","Dokter");
        $sheet->setCellValue("O4","Perawat");

   
        $row = 4;
        foreach($list as $x){
            $row++;
            $sheet->setCellValue("A".$row,ArrayAdapter::format("date d M Y",$x->tanggal));
            $sheet->setCellValue("B".$row,$x->nrm_pasien);
            $sheet->setCellValue("C".$row,$x->noreg_pasien);
            $sheet->setCellValue("D".$row,$x->nama_pasien);
            $sheet->setCellValue("E".$row,ArrayAdapter::format("date d M Y",$x->jenis_mobil));
            $sheet->setCellValue("F".$row,$x->km." KM");
            $sheet->setCellValue("G".$row,ArrayAdapter::format("unslug",$x->kelas));
            $sheet->setCellValue("H".$row,$x->nama_sopir);
            $sheet->setCellValue("I".$row,$x->biaya_perjalanan);
            $sheet->setCellValue("J".$row,$x->biaya_perawat);
            $sheet->setCellValue("K".$row,$x->biaya_desinfektan);
            $sheet->setCellValue("L".$row,$x->total_biaya);
            $sheet->setCellValue("M".$row,$x->tujuan);
            $sheet->setCellValue("N".$row,$x->nama_dokter);
            $sheet->setCellValue("O".$row,$x->nama_perawat);
            
        }
        $sheet->getStyle('I5:L'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle ( "A1:O5")->getFont ()->setBold ( true );
        $sheet->getStyle ( 'A4:O'.$row )->applyFromArray ( $fill );
       

        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Pemakaian Ambulan RSUD SLG.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );        
    }
}