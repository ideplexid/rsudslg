<?php 

class LaporanSewaResponder extends DBResponder{
    public function excel(){
        
        $dbtable = $this->getDBTable();
        $dbtable ->setShowAll(true);
        $data = $dbtable ->view("","0");
        $list = $data['data'];
        require_once "smis-libs-out/php-excel/PHPExcel.php";
       
        
        $fill = array();
        $fill['borders'] = array();
        $fill['borders']['allborders'] = array();
        $fill['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

        $file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
        $sheet  ->setTitle ("Rekapitulasi");

        $sheet->mergeCells("A1:K1")->setCellValue ( "A1", "LAPORAN SEWA AMBULAN RSUD SLG" );
        $sheet->mergeCells("A2:K2")->setCellValue ( "A2", "TANGGAL : ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']) );
        
        
        $sheet->setCellValue("A4","Tanggal");
        $sheet->setCellValue("B4","Penyewa");
        $sheet->setCellValue("C4","Jenis Mobil");
        $sheet->setCellValue("D4","Durasi Jam");
        $sheet->setCellValue("E4","Durasi Hari");
        $sheet->setCellValue("F4","Tujuan");
        $sheet->setCellValue("G4","Paramedis");
        $sheet->setCellValue("H4","Supir");
        $sheet->setCellValue("I4","Biaya");

   
        $row = 4;
        foreach($list as $x){
            $row++;
            $sheet->setCellValue("A".$row,ArrayAdapter::format("date d M Y",$x->tanggal));
            $sheet->setCellValue("B".$row,$x->nama);
            $sheet->setCellValue("C".$row,$x->jenis_mobil);
            $sheet->setCellValue("D".$row,$x->durasi_jam." Jam");
            $sheet->setCellValue("E".$row,$x->durasi_hari." Hari");
            $sheet->setCellValue("F".$row,$x->tujuan);
            $sheet->setCellValue("G".$row,$x->nama_perawat);
            $sheet->setCellValue("H".$row,$x->nama_sopir);
            $sheet->setCellValue("I".$row,$x->biaya_perjalanan);
            
        }
        $sheet->getStyle('I5:I'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle ( "A1:I5")->getFont ()->setBold ( true );
        $sheet->getStyle ( 'A4:I'.$row )->applyFromArray ( $fill );
       

        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Sewa Ambulan RSUD SLG.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );        
    }
}