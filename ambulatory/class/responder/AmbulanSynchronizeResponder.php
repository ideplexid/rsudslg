<?php 

/**
 * this class used to communicate with 
 * the server how to synchronize the whole data
 * 
 * */

class AmbulanSynchronizeResponder extends DBSynchronousResponder{
    public function synchronousData($original,$adapted,$id,$prop=""){
        $data=array();
        $data['grup_name']      = "ambulan";
        $data['nama_pasien']    = $original->nama_pasien;
        $data['noreg_pasien']   = $original->noreg_pasien;
        $data['nrm_pasien']     = $original->nrm_pasien;
        $data['entity']         = "ambulan";
        $data['list']           = $adapted;
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"proceed_receivable",$data,"kasir");
        $serv->execute();
        $hasil=$serv->getContent();
        return $hasil;
    }
    
    public function delete()
    {
        global $db;
        $result = parent::delete();
        $this-> synchronizeToAccounting($db, $_POST['id'] ,"del");
        return $result;
    }    
    
    public function save(){
        global $db;
		$data=$this->postToArray();
		$id['id']=$this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
            
            
            if($data['selesai'] == 0 || $data['selesai'] == "0") {
                // Mengupdate status mobil menjadi 1 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 1;
                $tarif_dbtable = new DBTable($db, "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 1 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($db, "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            } else {
                // Mengupdate status mobil menjadi 0 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 0;
                $tarif_dbtable = new DBTable($db, "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 0 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($db, "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            }
            
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
			$result=$this->dbtable->update($data,$id,$this->upsave_condition);
            
            if($data['selesai'] == 0 || $data['selesai'] == "0") {
                // Mengupdate status mobil menjadi 1 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 1;
                $tarif_dbtable = new DBTable($db, "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 1 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($db, "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            } else {
                // Mengupdate status mobil menjadi 0 pada table smis_amb_tarif, karena mobil sedang digunakan
                $tarif['id_mobil'] = $data['id_mobil'];
                $update = array();
                $update['status_mobil'] = 0;
                $tarif_dbtable = new DBTable($db, "smis_amb_tarif");
                $tarif_dbtable->update($update, $tarif);
                
                // Mengupdate status mobil menjadi 0 pada table smis_amb_mobil, karena mobil sedang digunakan
                $mobil['id'] = $data['id_mobil'];
                $mobil_dbtable = new DBTable($db, "smis_amb_mobil");
                $mobil_dbtable->update($update, $mobil);
            }
            
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) 
            $success['success']=0;
        $this-> synchronizeToAccounting($db,$success ['id'] ,"");

        if($success['id']!="0" || $success['id']!=""){
            $row=$this->getDBTable()->select($success['id']);
            $data=$this->getSyncData($row);
            $result=$this->synchronousData($row,$data,$success['id']);
        }

		return $success;
	}
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        global $db;
        $x=$this->dbtable->selectEventDel($id);
        $total = $x->gaji_sopir + $x->tarif + $x->biaya_perjalanan + $x->biaya_perawat + $x->biaya_dokter + $x->biaya_alat + $x->biaya_lain;
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "ambulan";
        $data['id_data']    = $id;
        $data['entity']     = "ambulatory";
        $data['service']    = "get_detail_accounting";
        $data['data']       = $id;
        $data['code']       = "amb-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu;
        $data['uraian']     = "Ambulan Pasien ".$x->nama_pasien." Pada Noreg ".$x->id;
        $data['nilai']      = $total;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }
}

?>