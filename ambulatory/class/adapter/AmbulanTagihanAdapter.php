<?php 
/**
 * this class used to adapt 
 * the cost of patient that need to be determine
 * that one data in ambulan would become several data in cashier
 * @author : Nurul Huda
 * @license : Apache 2.0
 * @since : 09-Mar-2017
 * @copyright : goblooge@gmail.com
 * */

 
class AmbulanTagihanAdapter extends SynchronousSenderAdapter{
    private $content;
    public function __construct(){
        parent::__construct();
        $this->content=array();
    }
    
    public function adapt($d){
		global $db;
        $urjigd = $d->uri==1?"URI":($d->ruangan=="igd"?"IGD":"URJ");
        $onedata=array();
		$onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata ['nama'] = "Perjalanan Ambulan - " . ucfirst ( $d->lokasi );
		$onedata ['id'] = $d->id."_ambulan";
		$onedata ['start'] = $d->tanggal;
		$onedata ['end'] = $d->tanggal;
		$onedata ['biaya'] = $d->biaya_perjalanan ;
		$onedata ['jumlah'] = 1;
        $onedata ['keterangan'] = "Biaya Menuju/Dari " . $d->lokasi . " Senilai " . ArrayAdapter::format ( "only-money Rp.", $d->biaya_perjalanan ) . "";
		$onedata ['prop'] = $this->getProp();
		$onedata ['debet'] = getSettings($db, "ambulan-accounting-debet-penggunaan-mobil", "");
		$onedata ['kredit'] = getSettings($db, "ambulan-accounting-kredit-penggunaan-mobil", "");
        
        $onedata ['urjigd'] = $urjigd;
        $onedata ['tanggal_tagihan'] = $d->tanggal;
        
        $this->content [] = $onedata;
		
		if ($d->biaya_desinfektan>0){
			$onedata=array();
			$onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
			$onedata ['nama'] = "Biaya Disinfektan";
			$onedata ['id'] = $d->id."_disinfektan";
			$onedata ['start'] = $d->tanggal;
			$onedata ['end'] = $d->tanggal;
			$onedata ['biaya'] = $d->biaya_desinfektan ;
			$onedata ['jumlah'] = 1 ;
			$onedata ['keterangan'] = "Biaya Disinfektan " . ArrayAdapter::format ( "only-money Rp.", $d->biaya_desinfektan ) . "";
            $onedata ['prop'] = $this->getProp();
            $onedata ['urjigd'] = $urjigd;
            $onedata ['tanggal_tagihan'] = $d->tanggal;
            $this->content [] = $onedata;
		}

		if ($d->id_perawat != "" || $d->biaya_perawat>0){
			$onedata=array();
			$onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
			$onedata ['nama'] = "Perawat Ambulan - " . ucfirst ( $d->lokasi );
			$onedata ['id'] = $d->id."_perawat";
			$onedata ['start'] = $d->tanggal;
			$onedata ['end'] = $d->tanggal;
			$onedata ['biaya'] = $d->biaya_perawat ;
			$onedata ['jumlah'] = 1 ;
			$onedata ['keterangan'] = "Biaya Perawat " . $d->nama_perawat . " " . ArrayAdapter::format ( "only-money Rp.", $d->biaya_perawat ) . "";
            $onedata ['prop'] = $this->getProp();
            $onedata ['urjigd'] = $urjigd;
            $onedata ['tanggal_tagihan'] = $d->tanggal;
            $this->content [] = $onedata;
		}
			
		if ($d->id_dokter != "" || $d->biaya_dokter>0 ){
			$onedata=array();
			$onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
			$onedata ['nama'] = "Dokter Ambulan - " . ucfirst ( $d->lokasi );
			$onedata ['id'] = $d->id."_dokter";
			$onedata ['start'] = $d->tanggal;
			$onedata ['end'] = $d->tanggal;
			$onedata ['biaya'] = $d->biaya_dokter ;
			$onedata ['jumlah'] = 1;
			$onedata ['keterangan'] = "Biaya Dokter " . $d->nama_dokter . " " . ArrayAdapter::format ( "only-money Rp.", $d->biaya_perawat ) . "";
            $onedata ['prop'] = $this->getProp();
            $onedata ['urjigd'] = $urjigd;
            $onedata ['tanggal_tagihan'] = $d->tanggal;
            $this->content [] = $onedata;
		}
			
		if ($d->biaya_alat != "0" || $d->biaya_alat > 0){
			$onedata=array();
			$onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
			$onedata ['nama'] = "Alat Ambulan - " . ucfirst ( $d->lokasi );
			$onedata ['id'] = $d->id."_alat";
			$onedata ['start'] = $d->tanggal;
			$onedata ['end'] = $d->tanggal;
			$onedata ['biaya'] = $d->biaya_alat ;
			$onedata ['jumlah'] = 1 ;
			$onedata ['keterangan'] = "Biaya Alat " . ArrayAdapter::format ( "only-money Rp.", $d->biaya_alat ) . "";
            $onedata ['prop'] = $this->getProp();
            $onedata ['urjigd'] = $urjigd;
            $onedata ['tanggal_tagihan'] = $d->tanggal;
            $this->content [] = $onedata;
		}
			
		if ($d->biaya_lain != "0" || $d->biaya_lain != ""){
			$onedata=array();
			$onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
			$onedata ['nama'] = "Baiya Lain Ambulan - " . ucfirst ( $d->lokasi );
			$onedata ['id'] = $d->id."_lain";
			$onedata ['start'] = $d->tanggal;
			$onedata ['end'] = $d->tanggal;
			$onedata ['biaya'] = $d->biaya_lain ;
			$onedata ['jumlah'] = 1 ;
			$onedata ['keterangan'] = "Biaya Lain " . ArrayAdapter::format ( "only-money Rp.", $d->biaya_lain ) . "";
            $onedata ['prop'] = $this->getProp();
            $onedata ['urjigd'] = $urjigd;
            $onedata ['tanggal_tagihan'] = $d->tanggal;
            $this->content [] = $onedata;
		}
        return $this->content;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
    
}

?>