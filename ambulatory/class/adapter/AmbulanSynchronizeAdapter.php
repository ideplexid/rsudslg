<?php 

class AmbulanSynchronizeAdapter extends SynchronousViewAdapter{
	public function adapt($d) {
		$array = array ();
		$array ['id'] = $d->id;
		$array ['Tanggal'] = self::format ( "date d M Y", $d->tanggal );
		$array ['Supir'] = $d->nama_sopir;
		$biaya = $d->biaya_perjalanan + $d->biaya_perawat + $d->biaya_dokter + $d->biaya_alat + $d->biaya_lain + $d->biaya_desinfektan;
		$array ['Biaya'] = self::format ( "money Rp.", $biaya );
		$array ['Lokasi'] = $d->lokasi;
		$array ['Kondisi'] = $d->kondisi;
        $array ['synch'] = $d->synch;
        $array ['Keterangan'] = $d->keterangan;
        $array ['Selesai'] = $d->selesai == 0 ? "":"<i class='fa fa-check'></i>";
		return $array;
	}
}


?>