<?php

global $wpdb;
global $NUMBER;

require_once 'smis-libs-class/DBCreator.php';
require_once 'smis-libs-inventory/install.php';
$install = new InventoryInstallator($wpdb, "", "");
$install->extendInstall("bd");
$install->install();

require_once 'smis-libs-class/DBCreator.php';
require_once "bank_darah/resource/install/smis_bd_pesanan_pasien.php";
require_once "bank_darah/resource/install/smis_bd_stok_darah.php";
require_once "bank_darah/resource/install/smis_bd_layanan.php";