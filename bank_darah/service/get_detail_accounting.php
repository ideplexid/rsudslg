<?php

global $db;
$id         = $_POST['data'];
$dbtable    = new DBTable($db,"smis_bd_pesanan_pasien");
$x          = $dbtable->selectEventDel($id);

$list       = array();

$debet=array();
$debet['akun']      = getSettings($db, "smis-bd-accounting-debet-darah-keluar", "");
$debet['debet']     = $x->biaya;
$debet['kredit']    = 0;
$debet['ket']       = "Piutang Bank Darah - Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$debet['code']      = "debet-bank_darah_keluar-".$x->id;
$list[]             = $debet;

$kredit=array();
$kredit['akun']     = getSettings($db, "smis-bd-accounting-kredit-darah-keluar", "");
$kredit['debet']    = 0;
$kredit['kredit']   = $x->biaya;
$kredit['ket']      = "Pendapatan Bank Darah - Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$kredit['code']     = "kredit-bank_darah_keluar-".$x->id;
$list[]             = $kredit;

//content untuk header
$header=array();
$header['tanggal']      = $x->waktu;
$header['keterangan']   = "Transaksi Penjualan Darah Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id." No.Bag ".$x->nomor_bag;
$header['code']         = "Penjualan Darah-".$x->id;
$header['nomor']        = "BDM-".$x->id;
$header['debet']        = $x->biaya;
$header['kredit']       = $x->biaya;
$header['io']           = "1";

$transaction_Bank_Darah               = array();
$transaction_Bank_Darah['header']     = $header;
$transaction_Bank_Darah['content']    = $list;

/*transaksi keseluruhan*/
$transaction    = array();
$transaction[]  = $transaction_Bank_Darah;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting'] = 1;
$id['id']           = $x->id;
$dbtable->setName("smis_bd_pesanan_pasien");
$dbtable->update($update,$id);

?>