<?php
show_error();
global $db;
require_once 'bank_darah/class/template/BankDarahTemplate.php';

$polislug                       = $_POST['polislug'];
$page                           = $_POST['page'];
$action                         = $_POST['action'];
$protoslug                      = $_POST['prototype_slug'];
$protoname                      = $_POST['prototype_name'];
$protoimplement                 = $_POST['prototype_implement'];
$noreg_pasien                   = $_POST['noreg_pasien'];
$nama_pasien                    = $_POST['nama_pasien'];
$nrm_pasien                     = $_POST['nrm_pasien'];


ob_start ();
$template = new BankDarahTemplate($db, BankDarahTemplate::$MODE_DAFTAR, $polislug, $page, $action, $protoslug, $protoname, $protoimplement, $noreg_pasien, $nama_pasien, $nrm_pasien);
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>