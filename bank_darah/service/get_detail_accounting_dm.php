<?php

global $db;
$id         = $_POST['data'];
$dbtable    = new DBTable($db,"smis_bd_stok_darah");
$x          = $dbtable->selectEventDel($id);

$list       = array();

$debet=array();
$debet['akun']      = getSettings($db, "smis-bd-accounting-debet-darah-masuk", "");
$debet['debet']     = $x->biaya;
$debet['kredit']    = 0;
$debet['ket']       = "Pembelian Persediaan Darah - No. Bag ".$x->nomor_bag;
$debet['code']      = "debet-bank_darah_masuk-".$x->id;
$list[]             = $debet;

$kredit=array();
$kredit['akun']     = getSettings($db, "smis-bd-accounting-kredit-darah-masuk", "");
$kredit['debet']    = 0;
$kredit['kredit']   = $x->biaya;
$kredit['ket']      = "Hutang Pembelian Darah - No. Bag ".$x->nomor_bag;
$kredit['code']     = "kredit-bank_darah_masuk-".$x->id;
$list[]             = $kredit;

//content untuk header
$header=array();
$header['tanggal']      = $x->waktu_masuk;
$header['keterangan']   = "Pembelian Persediaan Darah No. Bag ".$x->nomor_bag;
$header['code']         = "Pembelian Persediaan Darah-".$x->id;
$header['nomor']        = "BDK-".$x->id;
$header['debet']        = $x->biaya;
$header['kredit']       = $x->biaya;
$header['io']           = "1";

$transaction_Bank_Darah               = array();
$transaction_Bank_Darah['header']     = $header;
$transaction_Bank_Darah['content']    = $list;

/*transaksi keseluruhan*/
$transaction    = array();
$transaction[]  = $transaction_Bank_Darah;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting'] = 1;
$id['id']           = $x->id;
$dbtable->setName("smis_bd_stok_darah");
$dbtable->update($update,$id);

?>