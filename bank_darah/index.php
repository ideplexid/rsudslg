<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("bank_darah", $user,"modul/");
	
	$policy=new Policy("bank_darah", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("darah_masuk","darah_masuk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/darah_masuk");
	$policy->addPolicy("darah_keluar","darah_keluar",Policy::$DEFAULT_COOKIE_CHANGE,"modul/darah_keluar");
	$policy->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
    $policy->addPolicy("kode_akun", "settings", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
	$policy->addPolicy("lap_bank","lap_bank",Policy::$DEFAULT_COOKIE_CHANGE,"modul/lap_bank");
	$policy->addPolicy("layanan","lap_bank",Policy::$DEFAULT_COOKIE_CHANGE,"modul/layanan");
    
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>