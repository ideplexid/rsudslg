<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once "bank_darah/class/responder/DarahKeluarResponder.php";

class BankDarahTemplate extends ModulTemplate {
    protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
    protected $action;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    public static $MODE_DAFTAR  = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
    
    public function __construct ($db, $mode, $polislug = "all", $page = "bank_darah", $action = "darah_keluar", $protoslug = "", $protoname = "", $protoimplement = "", $noreg = "", $nama = "", $nrm = "") {
        $this->db               = $db;
        $this->mode             = $mode;
        $this->polislug         = $polislug;
        $this->page             = $page;
        $this->action           = $action;
        $this->protoslug        = $protoslug;
        $this->protoname        = $protoname;
        $this->protoimplement   = $protoimplement;
        $this->dbtable          = new DBTable ( $this->db, "smis_bd_pesanan_pasien" );
        $this->noreg_pasien     = $noreg;
        $this->nama_pasien      = $nama;
        $this->nrm_pasien       = $nrm;
        $thead                  = array("Tanggal", "Dokter", "Biaya", "G. Darah", "Jenis", "No. Bag", "Alamat");
        $this->uitable          = new Table($thead, "", NULL, true);
        $this->uitable->setName("bank_darah");
        
    }
    
    public function command($command) {
        $adapter = new SimpleAdapter();
        $adapter ->setUseNumber(true, "No.","back.");
        $adapter ->add ( "Tanggal", "waktu", "date d M Y" )
                 ->add ( "Dokter","dokter" )
                 ->add ( "Biaya","biaya","money Rp." )
                 ->add ( "G. Darah","golongan_darah" )
                 ->add ( "Jenis","jenis" )
                 ->add ( "No. Bag","nomor_bag" )
                 ->add ( "Alamat","alamat" );
        $dbres   = new DarahKeluarResponder( $this->dbtable, $this->uitable, $adapter );
        if(isset($_POST['dari']) &&  $_POST['dari']!="" ){
            $this->dbtable->addCustomKriteria(" waktu>= ", "'".$_POST['dari']."' ");
        }
        if(isset($_POST['sampai']) &&  $_POST['sampai']!="" ){
            $this->dbtable->addCustomKriteria(" waktu<= ", "'".$_POST['sampai']."' ");
        }
        if(isset($_POST['ruangan']) && $_POST['ruangan'] != "") {
            $this->dbtable->addCustomKriteria(" ruang = ", "'".$_POST['ruangan']."' ");
        }
        if ($this->noreg_pasien != ""){
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $this->noreg_pasien. "'" );
        }
        $data    = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    }
    
    public function phpPreLoad() {
        global $db;
        $gdarah = new OptionBuilder();
        $gdarah ->add("","",1)
                ->add("A+","A+",0)
                ->add("A-","A-",0)
                ->add("B+","B+",0)
                ->add("AB+","AB+",0)
                ->add("AB-","AB-",0)
                ->add("0-","0-",0)
                ->add("0+","0+",0);

        $jdarah = new OptionBuilder();
        $dbt = new DBTable($db,"smis_bd_layanan");
        $dbt->setShowAll(true);
        $data = $dbt->view("","0");
        $list = $data['data'];  
        $jdarah ->add("","",1);
        foreach($list as $l){
            $jdarah ->add($l->nama_layanan,$l->nama_layanan);
        }


        $this->uitable      ->addModal ( "id", "hidden", "", "" )
                            ->addModal ( "waktu", "datetime", "Tanggal", date ( "Y-m-d H:i:s" ) );
        if($this->mode == self::$MODE_DAFTAR) {
            $this->uitable  ->addModal("nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true )
                            ->addModal("nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true )
                            ->addModal("noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true )
                            ->addModal("ruangan", "hidden", "", $this->polislug, "n", null, true )
                            ->addModal("nomor_bag", "chooser-bank_darah-stok_darah-Stok Darah", "Nomor Bag", "" )
                            ->addModal("id_bag", "hidden", "", "" )
                            ->addModal("golongan_darah","select", "Golongan", $gdarah->getContent() )
                            ->addModal("jenis", "select", "Jenis", $jdarah->getContent())
                            ->addModal("biaya", "money", "Biaya", "" )
                            ->addModal("dokter", "chooser-bank_darah-dokter_bank_darah-Dokter", "Dokter", "" )
                            ->addModal("id_dokter", "hidden", "", "" )
                            ->addModal("keterangan", "textarea", "Keterangan", "" );
        }
        $modal = $this->uitable->getModal();
        $modal ->setTitle("Bank Darah");
        echo $modal ->getHtml();
        echo $this  ->uitable->getHtml();        
    }
    
    public function superCommand($super_command) {
        $super = new SuperCommand ();
        if($super_command == 'dokter_bank_darah') {
            $header  = array ('Nama','Jabatan',"NIP" );
            $dktable = new Table ( $header );
            $dktable->setName ( "dokter_bank_darah" );
            $dktable->setModel ( Table::$SELECT );
            $dkadapter  = new SimpleAdapter ();
            $dkadapter  ->add ( "Jabatan", "nama_jabatan" )
                        ->add ( "Nama", "nama" )
                        ->add ( "NIP", "nip" );
            $dokter     = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "dokter" );
            $super->addResponder("dokter_bank_darah",$dokter);
        } if($super_command == 'stok_darah') {
            $header   = array ('No. Bag','Expired',"Jenis","Gol Darah","Biaya" );
            $dbtable  = new DBTable($this->db,"smis_bd_stok_darah");
            $dbtable->addCustomKriteria("keluar","=''");
            $ptable   = new Table ($header);
            $ptable   ->setName ( "stok_darah" );
            $ptable   ->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter ->add ( "No. Bag", "nomor_bag" )
                      ->add ( "Expired", "tanggal_kedaluarsa", "date d M Y" )
                      ->add ( "Jenis", "jenis")
                      ->add ( "Biaya", "biaya","money Rp." )
                      ->add ( "Gol Darah", "gol_darah" );
            $dbresponder = new DBResponder (  $dbtable, $ptable, $padapter );
            $super->addResponder("stok_darah",$dbresponder);
        }
        $init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
    
    public function jsLoader() {
		echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS  ("framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS  ("framework/smis/js/table_action.js" );
		loadLibrary ("smis-libs-function-javascript" );
	}
    
	public function cssLoader() {
		echo addCSS ("framework/bootstrap/css/datepicker.css" );
		echo addCSS ("framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
    
    public function jsPreLoad() {
?>
    <script type="text/javascript">
        var <?php echo $this->action; ?>;
        var page             = "<?php echo $this->protoslug; ?>";
        var protoslug        = "<?php echo $this->protoslug; ?>";
		var protoname        = "<?php echo $this->protoname; ?>";
		var protoimplement   = "<?php echo $this->protoimplement; ?>";
        var bank_darah_noreg = "<?php echo $this->noreg_pasien; ?>";
        var stok_darah;
        var dokter_bank_darah;
        $(document).ready(function() {
            $(".mydate").datepicker();
            $(".mydatetime").datetimepicker({ minuteStep: 1});
            var column=new Array("id","nama_pasien","noreg_pasien","nrm_pasien","nomor_bag","id_bag","ruangan","golongan_darah","alamat","dokter","id_dokter","jenis","keterangan","waktu","biaya");
            <?php echo $this->action; ?> = new TableAction("<?php echo $this->action; ?>", page, "<?php echo $this->action; ?>", column);
            <?php echo $this->action; ?>.setPrototipe(protoname,protoslug,protoimplement);
			<?php echo $this->action; ?>.addRegulerData=function(a){
                a['noreg_pasien']=bank_darah_noreg;
                return a;
            };
            <?php echo $this->action; ?>.view();
            <?php echo $this->action; ?>.cekTutupTagihan = function(){
                var reg_data={	
                    page:this.page,
                    action:this.action,
                    super_command:this.super_command,
                    prototype_name:this.prototype_name,
                    prototype_slug:this.prototype_slug,
                    prototype_implement:this.prototype_implement
                    };			
                var noreg                 = $("#noreg_pasien").val();
                if(noreg==""){
                    smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
                    return;
                }
                reg_data['command']        = 'cek_tutup_tagihan';
                reg_data['noreg_pasien']  = noreg;
                
                var res = $.ajax({
                    type: "POST",
                    url: "",
                    data:reg_data,
                    async: false
                }).responseText;

                var json = getContent(res);
                if(json=="1"){
                    return false;
                }else{
                    return true;
                }
            };

            <?php echo $this->action; ?>.save = function(){
                showLoading();
                var cek = this.cekTutupTagihan();
                if(cek){
                    TableAction.prototype.save.call(this);        
                }
                dismissLoading();
            };


            stok_darah=new TableAction("stok_darah", page,"<?php echo $this->action; ?>",new Array());
			stok_darah.setSuperCommand("stok_darah");
            stok_darah.setPrototipe(protoname,protoslug,protoimplement);
			stok_darah.selected=function(json){
                $("#<?php echo $this->action; ?>_nomor_bag").val(json.nomor_bag);
                $("#<?php echo $this->action; ?>_id_bag").val(json.id);
                $("#<?php echo $this->action; ?>_golongan_darah").val(json.gol_darah);
                setMoney("#<?php echo $this->action; ?>_biaya",json.biaya);
                $("#<?php echo $this->action; ?>_jenis").val(json.jenis);
			};
           
            
            dokter_bank_darah=new TableAction("dokter_bank_darah",page,"<?php echo $this->action; ?>",new Array());
            dokter_bank_darah.setPrototipe(protoname,protoslug,protoimplement);
			dokter_bank_darah.setSuperCommand("dokter_bank_darah");
            dokter_bank_darah.selected=function(json){
                var nama=json.nama;
                var nip=json.id;		
                $("#<?php echo $this->action; ?>_dokter").val(nama);
                $("#<?php echo $this->action; ?>_id_dokter").val(nip);
            };

            <?php echo $this->action; ?>.view();
        });
    </script>
<?php
    }
}

?>