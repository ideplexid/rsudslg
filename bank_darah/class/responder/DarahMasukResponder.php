<?php
require_once ("smis-base/smis-include-service-consumer.php");
class DarahMasukResponder extends DBResponder {
    public function delete()
    {
        $result = parent::delete();
        $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
        return $result;
    }
    
    public function save(){
		$data=$this->postToArray();
		$id['id']=$this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
			$result=$this->dbtable->update($data,$id,$this->upsave_condition);
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) 
            $success['success']=0;
        $this-> synchronizeToAccounting($this->dbtable->get_db(),$success ['id'] ,"");
		return $success;
	}
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "pembelian";
        $data['id_data']    = $id;
        $data['entity']     = "bank_darah";
        $data['service']    = "get_detail_accounting_dm";
        $data['data']       = $id;
        $data['code']       = "bd-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu_masuk;
        $data['uraian']     = "Pembelian Persediaan Darah No. Bag ".$x->nomor_bag;
        $data['nilai']      = $x->biaya;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }
}

?>