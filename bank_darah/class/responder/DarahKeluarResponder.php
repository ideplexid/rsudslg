<?php

class DarahKeluarResponder extends DBResponder {
    public function delete(){

        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		if($this->dbtable->isRealDelete()){
			$result				= $this->dbtable->delete($id['id']);
		}else{
			$data['prop']		= "del";
			$result				= $this->dbtable->update($data,$id);
        }

        $result = parent::delete();
        if(getSettings($db,"bd-accounting-auto-notif","0")=="1"){
            $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
        }
        return $result;
    }
    
    public function save(){
		$data=$this->postToArray();
		$id['id']=$this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
			$result=$this->dbtable->update($data,$id,$this->upsave_condition);
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) 
            $success['success']=0;
        if(getSettings($db,"bd-accounting-auto-notif","0")=="1"){
            $this-> synchronizeToAccounting($this->dbtable->get_db(),$success ['id'] ,"");
        }
		return $success;
	}
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";
        $data['id_data']    = $id;
        $data['entity']     = "bank_darah";
        $data['service']    = "get_detail_accounting";
        $data['data']       = $id;
        $data['code']       = "bd-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu;
        $data['uraian']     = "Penjualan Darah Pasien ".$x->nama_pasien." Pada Noreg ".$x->id;
        $data['nilai']      = $x->biaya;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }

    public function cek_tutup_tagihan(){
        global $db;
        if(getSettings($db,"smis-bd-active-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }

    public function command($command){
        if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
            $result = $this->cek_tutup_tagihan();
            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
            return parent::command($command);
        }
    }
}