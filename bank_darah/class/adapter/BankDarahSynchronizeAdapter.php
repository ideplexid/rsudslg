<?php 

class BankDarahSynchronizeAdapter extends SynchronousSenderAdapter{
    private $content; 
    public function __construct(){
        parent::__construct();
        $this->content=array();
    }
    
    public function adapt($d){
        $onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->waktu );
		$onedata ['nama'] = $d->jenis;
		$onedata ['biaya'] = $d->biaya;
		$onedata ['jumlah'] = 1;
		$onedata ['start'] = $d->tanggal;
		$onedata ['end'] = $d->tanggal;
		$onedata ['id'] = $d->id;
		$onedata ['keterangan'] = "Bank Darah " . $d->layanan . " Senilai " . ArrayAdapter::format ( "only-money Rp.", $d->biaya );
        $onedata ['prop']       = $this->getProp();
        
        $urjigd = $d->uri==1?"URI":($d->ruangan=="igd"?"IGD":"URJ");
        $onedata ['urjigd'] = $urjigd;
        $onedata ['tanggal_tagihan'] = $d->tanggal;
        $this->content [] = $onedata;
        return $this->content;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
    
}

?>