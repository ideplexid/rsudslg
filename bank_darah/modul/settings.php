<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
setChangeCookie ( false );
changeCookie ();
$settings = new SettingsBuilder ( $db, "bank_darah", "bank_darah", "settings", "Settings Bank Darah" );
$settings->setShowDescription ( true );

$settings->addTabs ( "settings", "Setting" );
if($settings->isGroup("settings")) {
    $settings->addItem ( "settings", new SettingsItem ( $db, "smis-bd-autosynch-cashier", "Aktifkan Auto Synch data tagihan langsung ke kasir", "0", "checkbox", "Mengaktifkan Autosynch data tagihan kasir ") );
    $settings->addItem ( "settings", new SettingsItem ( $db, "smis-bd-active-tutup-tagihan", "Aktifkan Tutup Tagihan", "", "checkbox", "" ) );
    
}
$settings->addTabs ( "accounting", "Accounting" );
if($settings->isGroup("accounting")) {
    $settings->addItem ( "accounting", new SettingsItem ( $db, "bd-accounting-auto-notif", "Aktifkan Setting Auto Notif ke Accounting Ketika Ada Transaksi Penjualan", "0", "checkbox", "Jika Dicentang Maka Sistem Akan Menotifikasi ke Accounting Secara Otomatis Ketika Ada Transaksi Penjualan" ) );
    
    $settings->addItem ( "accounting", new SettingsItem ( $db, "smis-bd-accounting-debet-darah-masuk", "Kode Accounting untuk Debet Transaksi Darah Masuk", "", "chooser-settings-debet_darah_masuk-Debet", "Kode Accounting untuk Debet Transaksi Darah Masuk") );
    $settings->addItem ( "accounting", new SettingsItem ( $db, "smis-bd-accounting-kredit-darah-masuk", "Kode Accounting untuk Kredit Transaksi Darah Masuk", "", "chooser-settings-kredit_darah_masuk-Kredit", "Kode Accounting untuk Kredit Transaksi Darah Masuk") );
    $settings->addSuperCommandAction("debet_darah_masuk","kode_akun");
    $settings->addSuperCommandAction("kredit_darah_masuk","kode_akun");
    $settings->addSuperCommandArray("debet_darah_masuk","smis-bd-accounting-debet-darah-masuk","nomor",true);
    $settings->addSuperCommandArray("kredit_darah_masuk","smis-bd-accounting-kredit-darah-masuk","nomor",true);
    $settings->addItem ( "accounting", new SettingsItem ( $db, "smis-bd-accounting-debet-darah-keluar", "Kode Accounting untuk Debet Transaksi Darah Keluar", "", "chooser-settings-debet_darah_keluar-Debet", "Kode Accounting untuk Debet Transaksi Darah Keluar") );
    $settings->addItem ( "accounting", new SettingsItem ( $db, "smis-bd-accounting-kredit-darah-keluar", "Kode Accounting untuk Kredit Transaksi Darah Keluar", "", "chooser-settings-kredit_darah_keluar-Kredit", "Kode Accounting untuk Kredit Transaksi Darah Keluar") );
    $settings->addSuperCommandAction("debet_darah_keluar","kode_akun");
    $settings->addSuperCommandAction("kredit_darah_keluar","kode_akun");
    $settings->addSuperCommandArray("debet_darah_keluar","smis-bd-accounting-debet-darah-keluar","nomor",true);
    $settings->addSuperCommandArray("kredit_darah_keluar","smis-bd-accounting-kredit-darah-keluar","nomor",true);
}
$settings->setPartialLoad(true);
$response = $settings->init ();
?>