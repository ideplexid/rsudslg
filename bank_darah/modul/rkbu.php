<?php 
	/*
	*
	*	Nama 		: RKBU (Rencana Kebutuhan Barang Unit)
	*	Author 		: Dwi Al Aji Suseno
	*	Keterangan	: modul ini dipanggil dari library inventory/modul
	*	created 	: 07 Oktober 2016
	*	updated 	: 07 Oktober 2016
	*
	*/
	require_once 'smis-libs-inventory/modul/RKBU.php';
	global $db;
	$rkbu = new RKBU($db, "Bank Darah", "bd", "bank_darah");
	$rkbu->initialize();
?>