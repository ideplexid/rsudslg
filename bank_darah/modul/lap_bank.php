<?php 
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterSlaveTemplate.php';
global $db;
$fakultas = new MasterSlaveTemplate($db, "smis_bd_pesanan_pasien ", "bank_darah", "lap_bank");
$uitable  = $fakultas->getUItable();
$header=array("No.","Tanggal Keluar","Nama Pasien","Jenis Pasien","NRM","No. Reg","No Bag","Gol Darah","Jenis","Kelas","Ruangan","Biaya");
$uitable->setHeader($header);
$uitable->setAction(false);
$uitable->setFooterVisible(false);
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$form = $fakultas->getForm(false,"Laporan Pendapatan Bank Darah");
$fakultas->setFlag("dari","Dari","Dari tidak boleh Kosong");
$fakultas->setFlag("sampai","Sampai","Sampai tidak boleh Kosong");

if($fakultas->getDBResponder()->isView()){
    $fakultas->getDBtable()->addCustomKriteria(" DATE(waktu) ",">='".$_POST['dari']."'");
    $fakultas->getDBtable()->addCustomKriteria(" DATE(waktu) ","<='".$_POST['sampai']."'");
}

$a = $fakultas->getAdapter();
$a->setUseNumber(true,"No.","back.");
$a->add("Tanggal Keluar","waktu","date d M Y H:i");
$a->add("Nama Pasien","nama_pasien");
$a->add("Jenis Pasien","carabayar","unslug");
$a->add("NRM","nrm_pasien");
$a->add("No. Reg","noreg_pasien");
$a->add("No Bag","nomor_bag");
$a->add("Gol Darah","golongan_darah");
$a->add("Jenis","jenis");
$a->add("Kelas","kelas");
$a->add("Ruangan","ruangan","unslug");
$a->add("Biaya","biaya","money Rp.");


$b = new Button("","","Refresh");
$b->setAction("lap_bank.view()");
$b->setIsButton(Button::$ICONIC_TEXT);
$b->setIcon("fa fa-refresh");
$form->addElement("",$b);

$c = new Button("","","Print");
$c->setAction("lap_bank.print()");
$c->setIsButton(Button::$ICONIC_TEXT);
$c->setIcon("fa fa-print");
$form->addElement("",$c);


$adapter=$fakultas->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Mobil", "mobil")
		->add("Plat", "nomor")
		->add("Keterangan","keterangan")
		->add("Status","status_mobil", "trivial_0_Tidak Dipakai_Dipakai");
$fakultas->setModalTitle("Mobil");
$fakultas->setDateEnable(true);
$fakultas->addRegulerData("dari","dari","id-value");
$fakultas->addRegulerData("sampai","sampai","id-value");
$fakultas->addResouce("js","bank_darah/resource/js/lap_bank.js","after",false);
$fakultas->initialize();
?>