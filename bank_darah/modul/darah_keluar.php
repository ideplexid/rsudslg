<?php
global $db;
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="set_gol_darah"){
	require_once "bank_darah/snippet/set_gol_darah.php";
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="get_last_position"){
	require_once "bank_darah/snippet/get_last_position.php";
	return;
}


$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header );
$dktable->setName ( "dokter" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );

/* PASIEN */
$header=array ('Nama','NRM',"No Reg" );
$ptable = new Table ($header);
$ptable->setName ( "pasien" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "No Reg", "id" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );

/* PASIEN */
$header=array ('No. Bag','Expired',"Jenis","Gol Darah","Biaya" );
$dbtable=new DBTable($db,"smis_bd_stok_darah");
$dbtable->addCustomKriteria("keluar","=''");
$ptable = new Table ($header);
$ptable->setName ( "darah" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "No. Bag", "nomor_bag" );
$padapter->add ( "Expired", "tanggal_kedaluarsa", "date d M Y" );
$padapter->add ( "Jenis", "jenis");
$padapter->add ( "Biaya", "biaya","money Rp." );
$padapter->add ( "Gol Darah", "gol_darah" );
$dbresponder = new DBResponder (  $dbtable, $ptable, $padapter );

$super = new SuperCommand ();
$super->addResponder ( "dokter", $dokter );
$super->addResponder ( "pasien", $presponder );
$super->addResponder ( "darah", $dbresponder );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}
$header=array ('Tanggal',"Ruangan",'Dokter','Biaya','G. Darah',"Jenis","No. Bag","Alamat");
$_synchronous=getSettings($db,"smis-bd-autosynch-cashier","0")=="1";
$uitable=null;
if($_synchronous){
    $uitable = new TableSynchronous ( $header, "&nbsp;", NULL, true );
}else{
    $uitable = new Table ( $header, "&nbsp;", NULL, true );
}

$uitable->setName ( "darah_keluar" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    
    $dbtable = new DBTable ( $db, "smis_bd_pesanan_pasien" );
    $dbtable->addCustomKriteria ( "noreg_pasien", " ='" . $_POST ['noreg_pasien'] . "'" );
    $adapter=null;
    $dbres=null;
    if($_synchronous){
        require_once "bank_darah/class/responder/BankDarahSynchronousResponder.php";
        require_once "bank_darah/class/adapter/BankDarahSynchronizeAdapter.php";
        $sync_adapter=new BankDarahSynchronizeAdapter();
        $dbtable->activateTableSynchronous($_synchronous);
        $adapter = new SynchronousViewAdapter ();
        $dbres = new BankDarahSynchronousResponder ( $dbtable, $uitable, $adapter,$sync_adapter );
    }else{
        require_once "bank_darah/class/responder/DarahKeluarResponder.php";
        $adapter = new SimpleAdapter ();
        $dbres = new DarahKeluarResponder ( $dbtable, $uitable, $adapter );
    }
    
	$adapter->add("Tanggal","waktu","date d M Y");
	$adapter->add("Dokter","dokter");
	$adapter->add("Alamat","alamat");
	$adapter->add("G. Darah","golongan_darah");
	$adapter->add("No. Bag","nomor_bag");
	$adapter->add("G. Darah","golongan_darah");
	$adapter->add("Ruangan","ruangan","unslug");
	$adapter->add("Jenis","jenis");
	$adapter->add("Biaya","biaya","money Rp.");
    
	if($dbres->isSave() || $dbres->isDel()){
		$stok=new DBTable($db,"smis_bd_stok_darah");
		if($_POST['id']!=""){
			$pesanan=$dbtable->select($_POST['id']);				
			if($pesanan->id_bag!=0 && ( $dbres->isDel() || $pesanan->id_bag!=$_POST['id_bag'])  ){
				$up["keluar"]="";
				$up["nama_pasien"]="";
				$up["nrm_pasien"]="";
				$up["noreg_pasien"]="";
				$id=array("id"=>$pesanan->id_bag);
				$stok->update($up,$id);
			}
		}
		
		if(!$dbres->isDel() && $_POST['id_bag']!="" ){
			$up=array();
			$up["keluar"]="Dipakai Pasien";
			$up["nama_pasien"]=$_POST['nama_pasien'];
			$up["nrm_pasien"]=$_POST['nrm_pasien'];
			$up["noreg_pasien"]=$_POST['noreg_pasien'];
			$id=array("id"=>$_POST['id_bag']);
			$stok->update($up,$id);
		}		
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		$option=array();
		$option['value']=$nama_ruang;
		$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
		$ruangan[]=$option;
	}
}

$gdarah=new OptionBuilder();
$gdarah->add("","",1);
$gdarah->add("A+","A+",0);
$gdarah->add("A-","A-",0);
$gdarah->add("B+","B+",0);
$gdarah->add("AB+","AB+",0);
$gdarah->add("AB-","AB-",0);
$gdarah->add("0-","0-",0);
$gdarah->add("0+","0+",0);

$jdarah=new OptionBuilder();
$dbt = new DBTable($db,"smis_bd_layanan");
$dbt->setShowAll(true);
$data = $dbt->view("","0");
$list = $data['data'];  
$jdarah ->add("","",1);
foreach($list as $l){
	$jdarah ->add($l->nama_layanan,$l->nama_layanan);
}

$pemakaian=getSettings($db, "smis-rs-darah_keluar-mobil", "0")=="1";
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_dokter", "hidden", "", "" );
$uitable->addModal ( "id_bag", "hidden", "", "" );
$uitable->addModal ( "waktu", "datetime", "Waktu", date("Y-m-d H:i:s") );
$uitable->addModal ( "nomor_bag", "chooser-darah_keluar-darah-Stok Darah", "Nomor Bag", "" );
$uitable->addModal ( "golongan_darah","select", "Golongan", $gdarah->getContent() );
$uitable->addModal ( "jenis", "select", "Jenis", $jdarah->getContent());
$uitable->addModal ( "biaya", "money", "Biaya", "" );
$uitable->addModal ( "ruangan", "select", "Ruangan", $ruangan);
$uitable->addModal ( "dokter", "chooser-darah_keluar-dokter", "Dokter", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Pemintaan Pasien" );

$nrm = new Text ( "nrm_pasien", "nrm_pasien", "" );
$noreg = new Text ( "noreg_pasien", "noreg_pasien", "" );
$nama = new Text ( "nama_pasien", "nama_pasien", "" );
$alamat = new Text ( "alamat_pasien", "alamat_pasien", "" );
$gol_darah = new Select ( "gol_darah", "gol_darah", $gdarah->getContent() );
$uri = new Text ( "uri", "uri", "" );
$ruangan = new Text ( "ruangan", "ruangan", "" );
$carabayar = new Text ( "carabayar", "carabayar", "" );
$kelas = new Text ( "kelas", "kelas", "" );

$action = new Button ( "", "", "Select" );
$action->setIsButton(Button::$ICONIC_TEXT);
$action->setClass(" btn-primary ");
$action->setIcon(" fa fa-check");
$action->setAction ( "darah_keluar.chooser('proyek_plpp','nama_pasien','pasien',pasien)" );
$nrm->setDisabled ( true );
$noreg->setDisabled ( true );
$nama->setDisabled ( true );
$alamat->setDisabled ( true );
$gol_darah->setDisabled ( false );
$carabayar->setDisabled ( false );
$kelas->setDisabled ( false );

// form for proyek
$form = new Form ( "form_pasien", "", "Permintaan Pasien" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "No Registrasi", $noreg );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Alamat", $alamat );
$form->addElement ( "G. Darah", $gol_darah );
$form->addElement ( "Jenis Pasien", $carabayar );
$form->addElement ( "Kelas", $kelas );
$form->addElement ( "", $uri );
$form->addElement ( "", $ruangan );
$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "bank_darah/resource/js/darah_keluar.js",false );
?>




