<?php 
require_once 'smis-libs-class/MasterTemplate.php';

global $db;
$fakultas = new MasterTemplate($db, "smis_bd_layanan", "bank_darah", "layanan");
$uitable  = $fakultas->getUItable();
$header=array("No.","Layanan");
$uitable->setHeader($header);

$a = $fakultas->getAdapter();
$a->setUseNumber(true,"No.","back.");
$a->add("Layanan","nama_layanan");

$fakultas->getUItable()->addModal("id","hidden","","");
$fakultas->getUItable()->addModal("nama_layanan","text","Layanan","");
$fakultas->setModalTitle("Layanan");
$fakultas->initialize();
?>