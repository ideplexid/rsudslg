<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";
require_once "bank_darah/class/responder/DarahMasukResponder.php";
$master=new MasterTemplate($db,"smis_bd_stok_darah","bank_darah","darah_masuk");
$master->getDBtable()->setOrder("waktu_masuk DESC");
$master->setDateEnable(true);
$master->setDateTimeEnable(true);
$master->getUItable()->setHeader(array("No.","Masuk","No Bag","Gol Darah","Jenis","Biaya","Keluar","Expired"));
$gdarah=new OptionBuilder();
$gdarah	->add("","",1)
		->add("A+","A+",0)
		->add("A-","A-",0)
		->add("B+","B+",0)
		->add("AB+","AB+",0)
		->add("AB-","AB-",0)
		->add("0-","0-",0)
		->add("0+","0+",0);
$jkeluar=new OptionBuilder();
$jkeluar->add("","","1");
$jkeluar->add("Dipakai Pasien","Dipakai Pasien");
$jkeluar->add("Kedaluarsa","Kedaluarsa");
$jkeluar->add("Rusak","Rusak");
$jkeluar->add("Diminta Instansi Lain","Diminta Instansi Lain");
$jkeluar->add("Di Distribusikan ke RS Lain","Di Distribusikan ke RS Lain");

$jdarah=new OptionBuilder();

$dbt = new DBTable($db,"smis_bd_layanan");
$dbt->setShowAll(true);
$data = $dbt->view("","0");
$list = $data['data'];  
$jdarah ->add("","",1);
foreach($list as $l){
	$jdarah ->add($l->nama_layanan,$l->nama_layanan);
}

$jasal=new OptionBuilder();
$jasal	->addSingle("PMI","1")
		->addSingle("Keluarga Pasien")
		->addSingle("Pasien Lain")
		->addSingle("Donor Karyawan")
		->addSingle("Donor Umum");
$master	->getUItable()->addModal("id","hidden","","")
					  ->addModal("nomor_bag","text","Nomor Bag","")
					  ->addModal("jenis","select","Jenis",$jdarah->getContent())
					  ->addModal("gol_darah","select","Gol Darah",$gdarah->getContent())
					  ->addModal("waktu_masuk","datetime","Waktu Masuk",date("Y-m-d H:i:s"))
					  ->addModal("tanggal_kedaluarsa","date","Expired","")
					  ->addModal("keterangan","textarea","Keterangan","")
					  ->addModal("asal","select","Asal",$jasal->getContent())
					  ->addModal("keluar","select","Keluar",$jkeluar->getContent())
					  ->addModal("biaya","money","Biaya","");
$master->getAdapter()->setUseNumber(true,"No.","back.");
$master->getAdapter()->add("Masuk","waktu_masuk","date d M Y H:i:s");
$master->getAdapter()->add("Expired","tanggal_kedaluarsa","date d M Y");
$master->getAdapter()->add("No Bag","nomor_bag");
$master->getAdapter()->add("Gol Darah","gol_darah");
$master->getAdapter()->add("Jenis","jenis");
$master->getAdapter()->add("Keluar","keluar");
$master->getAdapter()->add("Biaya","biaya","money Rp.");

$dbres = new DarahMasukResponder($master->getDBtable(), $master->getUItable(), $master->getAdapter());
$master->setDBresponder($dbres);

$master->getModal()->setTitle("Darah Masuk");
$master->initialize();
?>