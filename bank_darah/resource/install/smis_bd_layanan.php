<?php 
require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_bd_layanan",DBCreator::$ENGINE_MYISAM);
$dbcreator->addColumn("nama_layanan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->setDuplicate(false);
$dbcreator->initialize();