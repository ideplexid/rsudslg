$(document).ready(function(){
    lap_bank.view=function(){	
        var self		= this;
        var view_data	= this.getViewData();
        if(view_data==null){
            showWarning("Error",this.view_data_null_message);
            return;
        }
        if($("#lap_bank_dari").val()=="" || $("#lap_bank_sampai").val()==""){
            showWarning("Error","Dari dan Sampai Tidak Boleh Kosong");
            return;
        }


        showLoading();
        $.post('',view_data,function(res){
            var json=getContent(res);
            if(json==null) {
                
            }else{
                $("#"+self.prefix+"_list").html(json.list);
                $("#"+self.prefix+"_pagination").html(json.pagination);	
                self.initPopover();
            }
            self.afterview(json);
            dismissLoading();
        });
        return this;
    };
    
});