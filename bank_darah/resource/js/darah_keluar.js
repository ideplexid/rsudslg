/*ACTION SCRIPT*/
var dokter;
var darah_keluar;
var darah;
var pasien;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	//setMoney("#darah_keluar_biaya",0);
	var column=new Array(
			"id","id_dokter","golongan_darah",
			"biaya","dokter","nomor_bag","jenis",
			"ruangan","keterangan","id_bag",
			"waktu"
			);
	darah_keluar=new TableAction("darah_keluar","bank_darah","darah_keluar",column);
	darah_keluar.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
				};
		reg_data['noreg_pasien']=$("#noreg_pasien").val();
		reg_data['nama_pasien']=$("#nama_pasien").val();
		reg_data['nrm_pasien']=$("#nrm_pasien").val();
        reg_data['alamat']=$("#alamat_pasien").val();
        reg_data['carabayar']=$("#carabayar").val();
		reg_data['kelas']=$("#kelas").val();
		reg_data['ruangan']=$("#ruangan").val();
		reg_data['uri']=$("#uri").val();
		return reg_data;
    };
    
    darah_keluar.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    darah_keluar.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };

	darah_keluar.setMultipleInput(true);
	darah_keluar.addNoClear("biaya");
	darah_keluar.addNoClear("ruangan");
	darah_keluar.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		
		this.clear();
		this.setLastPosition();
		this.show_form();
	};
	
	darah_keluar.setLastPosition=function(){
		var a=this.getRegulerData();
		a['super_command']="get_last_position";
		a['noreg_pasien']=$("#noreg_pasien").val();
		showLoading();
		$.post("",a,function(res){
			json=getContent(res);
			$("#darah_keluar_ruangan").val(json);
			dismissLoading();
		});
	};
	
	pasien=new TableAction("pasien","bank_darah","darah_keluar",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		var nama=json.nama_pasien;
		var nrm=json.nrm;
		var noreg=json.id;
		var alamat=json.alamat_pasien+" "+json.nama_kelurahan+" "+json.nama_kecamatan+" "+json.nama_kabupaten;
		$("#nama_pasien").val(nama);
        $("#nrm_pasien").val(nrm);
        $("#uri").val(json.uri);
        $("#ruangan").val(json.last_ruangan);
		$("#noreg_pasien").val(noreg);
		$("#gol_darah").val(json.gol_darah);
		$("#alamat_pasien").val(alamat);
		$("#carabayar").val(json.carabayar);
		$("#kelas").val(json.kelas_bpjs);
		darah_keluar.view();
	};

	
	dokter=new TableAction("dokter","bank_darah","darah_keluar",new Array());
	dokter.setSuperCommand("dokter");
	dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#darah_keluar_dokter").val(nama);
		$("#darah_keluar_id_dokter").val(nip);
	};
	
	darah=new TableAction("darah","bank_darah","darah_keluar",new Array());
	darah.setSuperCommand("darah");
	darah.selected=function(json){
		$("#darah_keluar_nomor_bag").val(json.nomor_bag);
		$("#darah_keluar_id_bag").val(json.id);
		$("#darah_keluar_golongan_darah").val(json.gol_darah);
		setMoney("#darah_keluar_biaya",json.biaya);
		$("#darah_keluar_jenis").val(json.jenis);
	};
	
	$("#gol_darah").on("change",function(res){
		if($("#nrm_pasien").val()==""){
			return;
		}
		var data= darah_keluar.getRegulerData();
		data['super_command']="set_gol_darah";
		data['golongan_darah']=$("#gol_darah").val();
		showLoading();
		$.post("",data,function(res){
			var g=getContent(res);
			dismissLoading();
		});
	});	
});