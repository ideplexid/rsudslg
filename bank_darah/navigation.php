<?php
	global $NAVIGATOR;

	// Administrator Top Menu
	$mr = new Menu ("fa fa-cutlery");
	$mr->addProperty ( 'title', 'Bank Darah' );
	$mr->addProperty ( 'name', 'Bank Darah' );
	$mr->addSubMenu ( "Darah Masuk", "bank_darah", "darah_masuk", "Stok Darah" ,"fa fa-medkit");
	$mr->addSubMenu ( "Darah Keluar", "bank_darah", "darah_keluar", "Permintaan Pasien Darah" ,"fa fa-ticket");
	$mr->addSubMenu ( "Laporan Pendapatan Bank Darah", "bank_darah", "lap_bank", "Pendapatan Pasien Darah" ,"fa fa-ticket");
    $mr->addSubMenu ( "Settings", "bank_darah", "settings", "Setting" ,"fa fa-cog");
	$mr->addSubMenu ( "Layanan", "bank_darah", "layanan", "Setting" ,"fa fa-list");

	//$mr->addSubMenu ( "Pemesanan PMI", "bank_darah", "pemesanan_pmi", "Pemesanan Darah ke PMI" ,"fa fa-tint");
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Bank Darah", "bank_darah");
	// extend menu untuk RKBU, Pengaturan Stok, dan BOI
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'bank_darah' );
?>
