<?php 

/**
 * khusus untuk mencari posisi terakhir dari seorang pasien
 * 
 * @author 		: Nurul Huda
 * @license 	: LGPLv2
 * @service 	: set_golongan_darah
 * @copyright 	: goblooge@gmail.com
 * @used		: bank_darah/modul/darah_keluar.php
 * 
 * */

$serv=new ServiceConsumer($db,"get_last_position",NULL,"medical_record");
$serv->addData("noreg_pasien",$_POST['noreg_pasien']);
$serv->execute();
$pack=new ResponsePackage();
$pack->setStatus(ResponsePackage::$STATUS_OK);
$pack->setAlertVisible(false);
$pack->setContent($serv->getContent());
echo json_encode($pack->getPackage());
return;

?>