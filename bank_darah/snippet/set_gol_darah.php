<?php 

/**
 * khusus untuk melakukan perubahan data pasien yang dipakai untuk melakukan set
 * data pasien. sehingga kedepanya pasien akan tahu golongan darah dia apa.
 * 
 * @author 		: Nurul Huda
 * @license 	: LGPLv2
 * @service 	: set_golongan_darah
 * @copyright 	: goblooge@gmail.com
 * @used		: bank_darah/modul/darah_keluar.php
 * 
 * */

$serv=new ServiceConsumer($db,"set_golongan_darah",NULL,"registration");
$serv->addData("nrm_pasien",$_POST['nrm_pasien']);
$serv->addData("golongan_darah",$_POST['golongan_darah']);
$serv->execute();
$pack=new ResponsePackage();
$pack->setStatus(ResponsePackage::$STATUS_OK);
$pack->setAlertVisible(true);
$pack->setAlertContent("Tersimpan","Golongan Darah di Update");
echo json_encode($pack->getPackage());
return;

?>