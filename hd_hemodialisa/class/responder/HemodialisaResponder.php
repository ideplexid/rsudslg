<?php 

class HemodialisaResponder extends DBResponder{
    
    public function command($command){
        if($command=="back_to_room"){
            $row=$this->back_to_room();
            
            $response=new ResponsePackage();
		    $response->setStatus(ResponsePackage::$STATUS_OK);
            $response->setAlertVisible(true);
            $response->setAlertContent("Pengembalian Berhasil","Pasien ".$row->nama_pasien." [ ".$row->nrm_pasien." / ".$row->noreg_pasien." ] telah dikembalikan ke Ruangan ".$row->ruangan);
            return $response->getPackage();
        }else if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
            $result = $this->cek_tutup_tagihan();
            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
           return parent::command($command);
        }
    }
    
    public function back_to_room(){
        $id=$_POST['id'];
        $row=$this->getDBTable()->select(array("id"=>$id));
        $noreg_pasien=$row->noreg_pasien;
        $ruangan=$row->ruangan;
        require_once "smis-base/smis-include-service-consumer.php";
        $array['noreg_pasien']=$noreg_pasien;
		$serv=new ServiceConsumer( $this->getDBTable()->get_db(), "reactived_antrian", $array, $ruangan );
        $serv->execute();
        return $row;
    }
    
    public function save(){
		$data=$this->postToArray();
		$id['id']=$this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data,$this->is_use_prop,$this->upsave_condition);
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
			$result=$this->dbtable->update($data,$id,$this->upsave_condition);
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false)
            $success['success']=0;
        if(getSettings($db,"hmd-accounting-auto-notif","0")=="1"){
            $this-> synchronizeToAccounting($this->dbtable->get_db(),$success ['id'] ,"");
        }
		return $success;
	}
    

    public function delete(){
        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		$result = parent::delete();
        if(getSettings($db,"hmd-accounting-auto-notif","0")=="1"){
            $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
        }
        return $result;
	}
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";
        $data['id_data']    = $id;
        $data['entity']     = "hd_hemodialisa";
        $data['service']    = "get_detail_accounting";
        $data['data']       = $id;
        $data['code']       = "hmd-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->tanggal;
        $data['uraian']     = "Hemodialisa Pasien ".$x->nama_pasien." Pada Noreg ".$x->id;
        $data['nilai']      = $x->total;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }

    public function cek_tutup_tagihan(){
        global $db;
        if(getSettings($db,"hmd-aktifkan-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }
    
}