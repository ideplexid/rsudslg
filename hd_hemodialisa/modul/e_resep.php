<?php
	require_once("smis-framework/smis/template/ModulTemplate.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	class EResep extends ModulTemplate {
		private $db;

		public function __construct($db) {
			parent::__construct();
			$this->db = $db;
		}

		public function initialize() {
			global $user;			
			$data = $_POST;
			$data['page'] = "hd_hemodialisa";
			$data['action'] = "e_resep";
			$data['proto_name'] = "Hemodialisa";
			$data['proto_slug'] = "hd_hemodialisa";
			$data['proto_implement'] ="";
			$data['noreg_pasien']=isset ( $_POST ['no_reg'] ) ? $_POST ['no_reg'] : (isset ( $_POST ['noreg_pasien'] ) ? $_POST ['noreg_pasien'] : "");
			$data['username_operator'] = $user->getUsername();
			$data['nama_operator'] = $user->getNameOnly();

			$service = new ServiceConsumer(
				$this->db,
				"get_eresep_request_list_table",
				$data,
				"e_resep"
			);
			$service->execute();
			echo $service->getContent();
			
		}
	}

	$ob = new EResep($db);
	$ob->initialize();
?>