<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
setChangeCookie ( false );
changeCookie ();
$settings = new SettingsBuilder ( $db, "hd_hemodialisa", "hd_hemodialisa", "settings", "Settings Hemodialisa" );
$settings->setShowDescription ( true );
$settings->addTabs ( "tindakan", "Tindakan"," fa fa-user-md" );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "hmd-disabled-harga", "Disable Harga", "0", "checkbox", "Melakukan Disabled Harga saat memasukan Layanan Hemodialisa" ) );
$settings->addItem ( "tindakan", new SettingsItem ( $db, "hmd-aktifkan-tutup-tagihan", "Aktifkan Tutup Tagihan", "0", "checkbox", "mengaktifkan tutup tagihan" ) );

$settings->addTabs ( "accounting", "Accounting"," fa fa-usd" );
$settings->addItem ( "accounting", new SettingsItem ( $db, "hmd-accounting-auto-notif", "Aktifkan Setting Auto Notif ke Accounting", "0", "checkbox", "Jika Dicentang Maka Sistem Akan Menotifikasi ke Accounting Secara Otomatis" ) );

$response = $settings->init ();
?>