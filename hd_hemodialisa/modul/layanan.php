<?php
	global $db;
	require_once 'smis-libs-class/MasterTemplate.php';
    require_once ("smis-base/smis-include-service-consumer.php");
	$layanan=new MasterTemplate($db, "smis_hmd_layanan", "hd_hemodialisa", "layanan");
	$header=array("No.","Nama", "Kode Debet", "Kode Kredit");
    
	$layanan->setNextEnter(true);
	$layanan->getDBtable()
			->setOrder(" nama ASC ");
	$layanan->getUItable()
			->setHeader($header)
			->setTitle("HD Hemodialisa : Layanan Hemodialisa")
			->addModal("id", "text", "ID", "","y",NULL,true)
			->addModal("nama", "text", "Nama", "")
            ->addModal("hdset", "text", "HD Set", "")
			
			->addModal("harga_hdset", "money", "HD Set", "")			
			->addModal("onemedpack", "text", "HD Pack", "")
			->addModal("harga_onemedpack", "money", "HD Pack", "")			
			->addModal("eritropoitin", "text", "Eritropoitin", "")
			->addModal("harga_eritropoitin", "money", "Eritropoitin", "")			
			->addModal("spuit20cc", "text", "Spuit 20 cc", "")
			->addModal("harga_spuit20cc", "money", "Spuit 20 cc", "")			
			->addModal("spuit3cc", "text", "Spuit 3 cc", "")
			->addModal("harga_spuit3cc", "money", "Spuit 3 cc", "")			
			->addModal("spuit5cc", "text", "Spuit 5 cc", "")
			->addModal("harga_spuit5cc", "money", "Spuit 5 cc", "")			
			->addModal("alkohol", "text", "Alkohol 70%", "")
			->addModal("harga_alkohol", "money", "Alkohol 70%", "")
            ->addModal("p2", "text", "P2", "")
			->addModal("harga_p2", "money", "P2", "")	
            ->addModal("abocath", "text", "Abocath", "")
			->addModal("harga_abocath", "money", "Abocath", "")
            ->addModal("pz500cc", "text", "PZ 500 cc", "")
			->addModal("harga_pz500cc", "money", "PZ 500 cc", "")			
			->addModal("pz1000cc", "text", "PZ 1000 cc", "")
			->addModal("harga_pz1000cc", "money", "PZ 1000 cc", "")			
			->addModal("neurobion", "text", "Neurobion", "")
			->addModal("harga_neurobion", "money", "Neurobion", "")			
			->addModal("neurosanbe", "text", "Neurosanbe", "")
			->addModal("harga_neurosanbe", "money", "Neurosanbe", "")			
			->addModal("bayclin", "text", "Bayclin Acid", "")
			->addModal("harga_bayclin", "money", "Bayclin Acid", "")			
			->addModal("infuset", "text", "Infus Set", "")
			->addModal("harga_infuset", "money", "Infus Set", "")
            ->addModal("gentamicyn", "text", "Gentamicyn", "")
			->addModal("harga_gentamicyn", "money", "Gentamicyn", "")			
            ->addModal("heparin", "text", "Heparin", "")
			->addModal("harga_heparin", "money", "Heparin", "")			
			->addModal("lidocain", "text", "Lidocain", "")
			->addModal("harga_lidocain", "money", "Lidocain", "")			
			->addModal("amlodipin", "text", "Amlodipin", "")
			->addModal("harga_amlodipin", "money", "Amlodipin", "")			
			->addModal("cek_hb", "text", "Cek HB", "")
			->addModal("harga_cek_hb", "money", "Cek HB", "")			
			->addModal("sewa_ruang", "text", "Sewa Ruang HD", "")
			->addModal("harga_sewa_ruang", "money", "Sewa Ruang HD", "")			
			->addModal("jasa_perawat", "text", "Jasa Perawat", "")
			->addModal("harga_jasa_perawat", "money", "Jasa Perawat", "")			
			->addModal("dokter", "text", "Dokter", "")
			->addModal("harga_dokter", "money", "Dokter", "")			
			->addModal("admin", "text", "Admin", "")
			->addModal("harga_admin", "money", "Admin", "")			
			->addModal("renalin", "text", "Renalin + H202", "")
			->addModal("harga_renalin", "money", "Renalin + H202", "")			
			->addModal("masker", "text", "Masker", "")
			->addModal("harga_masker", "money", "Masker", "")
			->addModal("cyto", "text", "Cyto", "")	
			->addModal("harga_cyto", "money", "Cyto", "")
            ->addModal("mesin_reuse", "text", "Mesin Reuse", "")	
			->addModal("harga_mesin_reuse", "money", "Mesin Reuse", "")            
            ->addModal("dialyzer", "text", "Dialyzer", "")	
			->addModal("harga_dialyzer", "money", "Harga Dialyzer", "")
            ->addModal("paket", "text", "Paket Hemodialisa", "")	
			->addModal("harga_paket", "money", "Harga Paket Hemodialisa", "")
            ->addModal("oksigen", "text", "Oksigen", "")	
			->addModal("harga_oksigen", "money", "Harga Oksigen", "")
            ->addModal("debet", "chooser-layanan-debet_layanan-Kode Debet", "Kode Debet", "","y",NULL,true,NULL,false)
            ->addModal("kredit", "chooser-layanan-kredit_layanan-Kode Debet", "Kode Kredit", "","y",NULL,true,NULL,false);

	$layanan->getAdapter()
			->setUseNumber(true, "No.","back.")
			->add("Nama", "nama")
			->add("Kode Debet", "debet")
			->add("Kode Kredit", "kredit");
	$layanan->getModal()
			->setModalSize(Modal::$HALF_MODEL)
			->setTitle("Layanan Hemodialisa");
    
if($layanan->getDBResponder()->isPreload() || $_POST['super_command']!=""  ){
    if(strpos($_POST['super_command'],"debet")!==false || strpos($_POST['super_command'],"kredit")!==false ){
        $uitable   = new Table(array('Nomor','Nama'),"");
        $uitable   ->setName($_POST['super_command'])
                   ->setModel(Table::$SELECT);
        $adapter   = new SimpleAdapter();
        $adapter   ->add("Nomor","nomor")
                   ->add("Nama","nama");
        $responder = new ServiceResponder($db, $uitable, $adapter, "get_account", "accounting");
        $layanan   ->getSuperCommand()
                   ->addResponder($_POST['super_command'],$responder);
        $layanan   ->addSuperCommand($_POST['super_command'],array())
                   ->addSuperCommandArray($_POST['super_command'],substr($_POST['super_command'],0,5),"nomor");
    }
}

if($layanan->getDBResponder()->isPreload() && $_POST['super_command']==""  ){
    $serv = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv ->execute();
    $cons = $serv->getContent();
    global $wpdb;
    foreach($cons as $x){
        require_once "smis-libs-class/DBCreator.php";
        $dbcreator = new DBCreator($wpdb,"smis_hmd_layanan",DBCreator::$ENGINE_MYISAM);
        $dbcreator ->addColumn("debet_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->addColumn("kredit_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->initialize();
        $uitable   = new Table(array('Nomor','Nama'),"");
        $uitable   ->addModal("debet_".$x['value'], "chooser-layanan-debet_layanan_".$x['value']."-Kode Debet ".$x['name'], "Kode Debet ".$x['name'], "","y",NULL,true,NULL,false);
        $uitable   ->addModal("kredit_".$x['value'], "chooser-layanan-kredit_layanan_".$x['value']."-Kode Kredit".$x['name'], "Kode Kredit ".$x['name'], "","y",NULL,true,NULL,false);
        $layanan   ->addSuperCommand("debet_layanan_".$x['value'],array())
                   ->addSuperCommandArray("debet_layanan_".$x['value'],"debet_".$x['value'],"nomor");
        $layanan   ->addSuperCommand("kredit_layanan_".$x['value'],array())
                   ->addSuperCommandArray("kredit_layanan_".$x['value'],"kredit_".$x['value'],"nomor");
    }    
    $layanan  ->addSuperCommand("debet_layanan",array())
              ->addSuperCommandArray("debet_layanan","debet","nomor");
    $layanan  ->addSuperCommand("kredit_layanan",array())
              ->addSuperCommandArray("kredit_layanan","kredit","nomor");
}   
    
	$layanan->initialize();
?>