<?php

global $db;
require_once 'smis-libs-class/MasterTemplate.php';
require_once "smis-libs-hrd/EmployeeResponder.php";

$header=array ("No.",'Nama');
$dbtable= new DBTable($db, "smis_hmd_layanan");
$dktable = new Table ( $header );
$dktable->setName ( "pemeriksaan_layanan" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->setUseNumber(true, "No.","back.");
$dkadapter->add ( "Nama", "nama" );
$layanan= new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ($header , "", NULL, true );
$dktable->setName ( "pemeriksaan_dokter" );
$dktable->setModel ( Table::$SELECT );
$kktable = new Table ($header,"", NULL, true );
$kktable->setName ( "pemeriksaan_petugas" );
$kktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dkresponder = new EmployeeResponder($db, $dktable, $dkadapter, "");
$kkresponder = new EmployeeResponder($db, $kktable, $dkadapter, "");
	/* PASIEN */
$ptable = new Table ( array ('Nama','NRM',"No Reg","Ruangan"), "", NULL, true );
$ptable->setName ( "pemeriksaan_pasien" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "Ruangan", "kamar_inap","unslug" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "No Reg", "id" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );

$jk=new OptionBuilder();
$jk->add("Laki-Laki","0","1");
$jk->add("Perempuan","1","0");

$uri=new OptionBuilder();
$uri->add("Rawat Jalan","0","1");
$uri->add("Rawat Inap","1","0");

require_once "hd_hemodialisa/class/responder/HemodialisaResponder.php";
$header=array ("Nama Pasien","NRM","No Reg", "Ruangan", "Tanggal","HD",'Total');
$pemreg=new MasterTemplate($db, "smis_hmd_pesanan", "hd_hemodialisa", "pemeriksaan");
$hmd=new HemodialisaResponder($pemreg->getDBtable(),$pemreg->getUItable(),$pemreg->getAdapter());
$pemreg->setDBresponder($hmd);
$pemreg->setDateEnable(true);
$pemreg->getAdapter()
		->add("Nama Pasien", "nama_pasien")
		->add("NRM", "nrm_pasien","only-digit8")
		->add("No Reg", "noreg_pasien","only-digit8")
		->add("Ruangan", "ruangan","unslug")
		->add("Tanggal", "tanggal", "date d M Y")
		->add("HD","layanan")
		->add("Total", "total","money Rp.");
$pemreg->addResouce("css", "hd_hemodialisa/resource/css/pemeriksaan.css");
if($pemreg->getDBResponder()->isSave()){
	$total=0;
    $total+=$_POST['dialyzer']*$_POST['harga_dialyzer'];
    $total+=$_POST['paket']*$_POST['harga_paket'];
    $total+=$_POST['oksigen']*$_POST['harga_oksigen'];
    $total+=$_POST['hdset']*$_POST['harga_hdset'];
	$total+=$_POST['onemedpack']*$_POST['harga_onemedpack'];
	$total+=$_POST['eritropoitin']*$_POST['harga_eritropoitin'];
	$total+=$_POST['spuit20cc']*$_POST['harga_spuit20cc'];
	$total+=$_POST['spuit3cc']*$_POST['harga_spuit3cc'];
	$total+=$_POST['spuit5cc']*$_POST['harga_spuit5cc'];
	$total+=$_POST['alkohol']*$_POST['harga_alkohol'];
    $total+=$_POST['p2']*$_POST['harga_p2'];
    $total+=$_POST['abocath']*$_POST['harga_abocath'];
	$total+=$_POST['pz500cc']*$_POST['harga_pz500cc'];
	$total+=$_POST['pz1000cc']*$_POST['harga_pz1000cc'];
	$total+=$_POST['neurobion']*$_POST['harga_neurobion'];
	$total+=$_POST['neurosanbe']*$_POST['harga_neurosanbe'];
	$total+=$_POST['bayclin']*$_POST['harga_bayclin'];
	$total+=$_POST['infuset']*$_POST['harga_infuset'];
    $total+=$_POST['gentamicyn']*$_POST['harga_gentamicyn'];
	$total+=$_POST['heparin']*$_POST['harga_heparin'];
	$total+=$_POST['lidocain']*$_POST['harga_lidocain'];
	$total+=$_POST['amlodipin']*$_POST['harga_amlodipin'];
	$total+=$_POST['cek_hb']*$_POST['harga_cek_hb'];
	$total+=$_POST['sewa_ruang']*$_POST['harga_sewa_ruang'];
	$total+=$_POST['jasa_perawat']*$_POST['harga_jasa_perawat'];
	$total+=$_POST['dokter']*$_POST['harga_dokter'];
	$total+=$_POST['admin']*$_POST['harga_admin'];
	$total+=$_POST['renalin']*$_POST['harga_renalin'];
	$total+=$_POST['masker']*$_POST['harga_masker'];
	$total+=$_POST['cyto']*$_POST['harga_cyto'];
    $total+=$_POST['mesin_reuse']*$_POST['harga_mesin_reuse'];
	$pemreg->getDBResponder()->addColumnFixValue("total", $total);
}

$option=new OptionBuilder();
$option->addSingle(1);
$option->addSingle(2);
$option->addSingle(3);
$option->addSingle(4);
$option->addSingle(5);
$option->addSingle(6);
$option->addSingle(7);
$option->addSingle(8);

$eresep_btn = new Button("", "", "E-Resep");
$eresep_btn->setAtribute("data-content='E-Resep' data-toggle='popover'");
$eresep_btn->setIcon("fa fa-file-o");
$eresep_btn->setClass("btn-inverse");
$eresep_btn->setIsButton(Button::$ICONIC);

$return_pasien = new Button("", "", "Kembali ke Ruang");
$return_pasien->setIcon("fa fa-sign-out");
$return_pasien->setClass("btn-primary");
$return_pasien->setIsButton(Button::$ICONIC);

$btn1 = new Button("", "", "Rehabilitasi Medik");
$btn1 ->setClass("btn-inverse");
$btn1 ->setIcon("fa fa-file");
$btn1 ->setIsButton(Button::$ICONIC);

$btn2 = new Button("", "", "Pelayanan Khusus");
$btn2 ->setClass("btn-inverse");
$btn2 ->setIcon("fa fa-file");
$btn2 ->setIsButton(Button::$ICONIC);

$btn3 = new Button("", "", "Jenis Pelayanan");
$btn3 ->setClass("btn-inverse");
$btn3 ->setIcon("fa fa-file");
$btn3 ->setIsButton(Button::$ICONIC);

$disabled_harga=getSettings($db,"hmd-disabled-harga","0")=="1";
$pemreg->getUItable()
		->setHeader($header)
		->addContentButton("eresep", $eresep_btn)
        ->addContentButton("back_to_room", $return_pasien)
        ->addContentButton("rehab_medik", $btn1)
        ->addContentButton("pelayanan_khusus", $btn2)
        ->addContentButton("lap_rl52", $btn3)
		->setTitle("HD Hemodialisa : Pemeriksaan")
		->addModal("id", "hidden", "", "")
		->addModal("tanggal", "date", "Tanggal", date("Y-m-d"))
		->addModal("nama_pasien", "chooser-pemeriksaan-pemeriksaan_pasien-Pilih Pasien", "Nama Pasien", "")
		->addModal("nrm_pasien", "text", "NRM Pasien", "")
		->addModal("noreg_pasien", "text", "Noreg Pasien", "")
		->addModal("ruangan", "text", "Ruangan", "")
		->addModal("umur", "text", "Umur", "")
		->addModal("jk", "select", "Jenis Kelamin", $jk->getContent())
		->addModal("uri", "select", "RJ/RI", $uri->getContent())
		->addModal("carabayar", "text", "Cara Bayar", "")
        ->addModal("pemeriksaan", "select", "Pemeriksaan", $option->getContent())
		->addModal("id_dokter", "hidden", "", "")
		->addModal("nama_dokter", "chooser-pemeriksaan-pemeriksaan_dokter-Pilih Dokter", "Nama Dokter", "")
		->addModal("id_petugas", "hidden", "", "")
		->addModal("nama_petugas", "chooser-pemeriksaan-pemeriksaan_petugas-Pilih Petugas", "Nama Petugas", "")
		->addModal("layanan", "chooser-pemeriksaan-pemeriksaan_layanan-Pilih Layanan", "Layanan", "")
		->addModal("harga_hdset", "money", "HD Set", "","y",null,$disabled_harga)
		->addModal("hdset", "text", "Jumlah HD Set", "")
		->addModal("harga_onemedpack", "money", "HD Pack", "","y",null,$disabled_harga)
		->addModal("onemedpack", "text", "Jumlah HD Pack", "")
		->addModal("harga_eritropoitin", "money", "Eritropoitin", "","y",null,$disabled_harga)
		->addModal("eritropoitin", "text", "Jumlah Eritropoitin", "")
		->addModal("harga_spuit20cc", "money", "Spuit 20 CC", "","y",null,$disabled_harga)
		->addModal("spuit20cc", "text", "Jumlah Spuit 20 CC", "")
		->addModal("harga_spuit3cc", "money", "Spuit 3 CC", "","y",null,$disabled_harga)
		->addModal("spuit3cc", "text", "Jumlah Spuit 3 CC", "")
		->addModal("harga_spuit5cc", "money", "Spuit 5 CC", "","y",null,$disabled_harga)
		->addModal("spuit5cc", "text", "Jumlah Spuit 3 CC", "")
		->addModal("harga_alkohol", "money", "Alkohol", "","y",null,$disabled_harga)
		->addModal("alkohol", "text", "Jumlah Alkohol", "")
        ->addModal("harga_p2", "money", "P2", "","y",null,$disabled_harga)
        ->addModal("p2", "text", "Jumlah P2", "")	
        ->addModal("harga_abocath", "money", "Abocath", "","y",null,$disabled_harga)
        ->addModal("abocath", "text", "Jumlah Abocath", "")            
		->addModal("harga_pz500cc", "money", "PZ 500 CC", "","y",null,$disabled_harga)
		->addModal("pz500cc", "text", "Jumlah PZ 500 CC", "")
		->addModal("harga_pz1000cc", "money", "PZ 1000 CC", "","y",null,$disabled_harga)
		->addModal("pz1000cc", "text", "Jumlah PZ 1000 CC", "")
		->addModal("harga_neurobion", "money", "Neurobion", "","y",null,$disabled_harga)
		->addModal("neurobion", "text", "Jumlah Neurobion", "")
		->addModal("harga_neurosanbe", "money", "Neurosanbe", "","y",null,$disabled_harga)
		->addModal("neurosanbe", "text", "Jumlah Neurosanbe", "")
		->addModal("harga_bayclin", "money", "Bayclin", "","y",null,$disabled_harga)
		->addModal("bayclin", "text", "Jumlah Bayclin", "")
		->addModal("harga_infuset", "money", "Infuset", "","y",null,$disabled_harga)
		->addModal("infuset", "text", "Jumlah Infuset", "")
        ->addModal("harga_gentamicyn", "money", "Gentamicyn", "","y",null,$disabled_harga)
        ->addModal("gentamicyn", "text", "Gentamicyn", "")
        ->addModal("harga_heparin", "money", "Heparin", "","y",null,$disabled_harga)
		->addModal("heparin", "text", "Jumlah Heparin", "")
		->addModal("harga_lidocain", "money", "Lidocain", "","y",null,$disabled_harga)
		->addModal("lidocain", "text", "Harga Lidocain", "")
		->addModal("harga_amlodipin", "money", "Amlodipin", "","y",null,$disabled_harga)
		->addModal("amlodipin", "text", "Harga Amlodipin", "")
		->addModal("harga_renalin", "money", "Renalin", "","y",null,$disabled_harga)
		->addModal("renalin", "text", "Jumlah Renalin", "")
		->addModal("harga_masker", "money", "Masker", "","y",null,$disabled_harga)
		->addModal("masker", "text", "Jumlah Masker", "")
		->addModal("harga_admin", "money", "Harga Admin", "","y",null,$disabled_harga)
		->addModal("admin", "checkbox", "Admin", "")
		->addModal("harga_cek_hb", "money", "Harga Cek HB", "","y",null,$disabled_harga)
		->addModal("cek_hb", "checkbox", "Cek HB", "")
		->addModal("harga_sewa_ruang", "money", "Sewa Ruang", "","y",null,$disabled_harga)
		->addModal("sewa_ruang", "checkbox", "Sewa Ruang", "")
		->addModal("harga_jasa_perawat", "money", "Jasa Perawat", "","y",null,$disabled_harga)
		->addModal("jasa_perawat", "checkbox", "Perawat", "")
		->addModal("harga_dokter", "money", "Dokter", "","y",null,$disabled_harga)
		->addModal("dokter", "checkbox", "Dokter", "")
		->addModal("harga_cyto", "money", "Cyto", "","y",null,$disabled_harga)
		->addModal("cyto", "checkbox", "Cyto", "")
        ->addModal("harga_mesin_reuse", "money", "Mesin Reuse", "","y",null,$disabled_harga)
		->addModal("mesin_reuse", "checkbox", "Mesin Reuse", "")
        ->addModal("harga_dialyzer", "money", "Harga Dialyzer", "","y",null,$disabled_harga)
		->addModal("dialyzer", "text", "Dialyzer", "")
        ->addModal("harga_paket", "money", "Harga Paket", "","y",null,$disabled_harga)
		->addModal("paket", "text", "Paket", "")
        ->addModal("harga_oksigen", "money", "Harga Oksigen", "","y",null,$disabled_harga)
		->addModal("oksigen", "text", "Oksigen", "Oksigen");

$pemreg->getSuperCommand()
		->addResponder("pemeriksaan_layanan", $layanan)
		->addResponder("pemeriksaan_dokter", $dkresponder)
		->addResponder("pemeriksaan_pasien", $presponder)
		->addResponder("pemeriksaan_petugas", $kkresponder);
$pemreg->addSuperCommand("pemeriksaan_pasien", array())
		->addSuperCommandArray("pemeriksaan_pasien", "nrm_pasien", "nrm")
		->addSuperCommandArray("pemeriksaan_pasien", "nama_pasien", "nama_pasien")
		->addSuperCommandArray("pemeriksaan_pasien", "noreg_pasien", "id")
		->addSuperCommandArray("pemeriksaan_pasien", "carabayar", "carabayar")
		->addSuperCommandArray("pemeriksaan_pasien", "umur", "umur")
		->addSuperCommandArray("pemeriksaan_pasien", "jk", "kelamin")
		->addSuperCommandArray("pemeriksaan_pasien", "ruangan", "kamar_inap")
		->addSuperCommand("pemeriksaan_petugas", array())
		->addSuperCommandArray("pemeriksaan_petugas", "id_petugas", "id")
		->addSuperCommandArray("pemeriksaan_petugas", "nama_petugas", "nama")
		->addSuperCommand("pemeriksaan_dokter", array())
		->addSuperCommandArray("pemeriksaan_dokter", "id_dokter", "id")
		->addSuperCommandArray("pemeriksaan_dokter", "nama_dokter", "nama")
		->addSuperCommand("pemeriksaan_layanan", array())
		 ->addSuperCommandArray("pemeriksaan_layanan", "layanan", "nama")
		 ->addSuperCommandArray("pemeriksaan_layanan", "hdset", "hdset")
		 ->addSuperCommandArray("pemeriksaan_layanan", "onemedpack", "onemedpack")
		 ->addSuperCommandArray("pemeriksaan_layanan", "eritropoitin", "eritropoitin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "spuit20cc", "spuit20cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "spuit3cc", "spuit3cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "spuit5cc", "spuit5cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "alkohol", "alkohol")
         ->addSuperCommandArray("pemeriksaan_layanan", "p2", "p2")
         ->addSuperCommandArray("pemeriksaan_layanan", "abocath", "abocath")
		 ->addSuperCommandArray("pemeriksaan_layanan", "pz500cc", "pz500cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "pz1000cc", "pz1000cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "neurobion", "neurobion")
		 ->addSuperCommandArray("pemeriksaan_layanan", "neurosanbe", "neurosanbe")
		 ->addSuperCommandArray("pemeriksaan_layanan", "bayclin", "bayclin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "infuset", "infuset")
         ->addSuperCommandArray("pemeriksaan_layanan", "gentamicyn", "gentamicyn")
		 ->addSuperCommandArray("pemeriksaan_layanan", "heparin", "heparin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "lidocain", "lidocain")
		 ->addSuperCommandArray("pemeriksaan_layanan", "amlodipin", "amlodipin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "cek_hb", "cek_hb")
		 ->addSuperCommandArray("pemeriksaan_layanan", "sewa_ruang", "sewa_ruang")
		 ->addSuperCommandArray("pemeriksaan_layanan", "jasa_perawat", "jasa_perawat")
		 ->addSuperCommandArray("pemeriksaan_layanan", "dokter", "dokter")
		 ->addSuperCommandArray("pemeriksaan_layanan", "admin", "admin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "masker", "masker")
		 ->addSuperCommandArray("pemeriksaan_layanan", "cyto", "cyto")
         ->addSuperCommandArray("pemeriksaan_layanan", "mesin_reuse", "mesin_reuse")
		 ->addSuperCommandArray("pemeriksaan_layanan", "renalin", "renalin")
         ->addSuperCommandArray("pemeriksaan_layanan", "dialyzer", "dialyzer")
         ->addSuperCommandArray("pemeriksaan_layanan", "paket", "paket")
         ->addSuperCommandArray("pemeriksaan_layanan", "oksigen", "oksigen")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_hdset", "harga_hdset")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_onemedpack", "harga_onemedpack")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_eritropoitin", "harga_eritropoitin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_spuit20cc", "harga_spuit20cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_spuit3cc", "harga_spuit3cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_spuit5cc", "harga_spuit5cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_alkohol", "harga_alkohol")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_p2", "harga_p2")
         ->addSuperCommandArray("pemeriksaan_layanan", "harga_abocath", "harga_abocath")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_pz500cc", "harga_pz500cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_pz1000cc", "harga_pz1000cc")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_neurobion", "harga_neurobion")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_neurosanbe", "harga_neurosanbe")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_bayclin", "harga_bayclin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_infuset", "harga_infuset")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_gentamicyn", "harga_gentamicyn")
         ->addSuperCommandArray("pemeriksaan_layanan", "harga_heparin", "harga_heparin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_lidocain", "harga_lidocain")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_amlodipin", "harga_amlodipin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_cek_hb", "harga_cek_hb")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_sewa_ruang", "harga_sewa_ruang")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_jasa_perawat", "harga_jasa_perawat")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_dokter", "harga_dokter")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_admin", "harga_admin")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_masker", "harga_masker")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_cyto", "harga_cyto")
         ->addSuperCommandArray("pemeriksaan_layanan", "harga_mesin_reuse", "harga_mesin_reuse")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_masker", "harga_masker")
		 ->addSuperCommandArray("pemeriksaan_layanan", "harga_renalin", "harga_renalin")         
         ->addSuperCommandArray("pemeriksaan_layanan", "harga_dialyzer", "harga_dialyzer")
         ->addSuperCommandArray("pemeriksaan_layanan", "harga_paket", "harga_paket")
         ->addSuperCommandArray("pemeriksaan_layanan", "harga_oksigen", "harga_oksigen")
		 ->getModal()
		 ->setModalSize(Modal::$FULL_MODEL)
		 ->setComponentSize(Modal::$MEDIUM)
		 ->setTitle("Pembayaran Registrasi");
		 $pemreg->addResouce("js","hd_hemodialisa/resource/js/pemeriksaan.js");
$pemreg->initialize();
?>