<?php
global $db;
global $user;
require_once 'smis-libs-class/Policy.php';
require_once 'smis-libs-inventory/policy.php';
$ipolicy=new InventoryPolicy("hd_hemodialisa", $user,"modul/");

$policy=new Policy("hd_hemodialisa", $user);
$policy->addPolicy("pemeriksaan", "pemeriksaan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/pemeriksaan");
$policy->addPolicy("rehab_medik","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/rehab_medik");
$policy->addPolicy("pelayanan_khusus","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/pelayanan_khusus");
$policy->addPolicy("lap_rl52","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/pemeriksaan/lap_rl52");

$policy->addPolicy("e_resep","pemeriksaan",Policy::$DEFAULT_COOKIE_KEEP,"modul/e_resep");
$policy->addPolicy("arsip", "arsip", Policy::$DEFAULT_COOKIE_CHANGE,"modul/arsip");
$policy->addPolicy("layanan", "layanan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/layanan");
$policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
$policy->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
$policy->combinePolicy($ipolicy);
$policy->initialize();

?>
