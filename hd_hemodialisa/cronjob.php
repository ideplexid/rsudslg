<?php 
	require_once 'smis-libs-inventory/cronjob/StockBarangED.php';
	require_once 'smis-libs-inventory/cronjob/StockObatED.php';
	global $db;
	global $notification;
	$barang=new CronjobStockBarangED($db, $notification, "smis_hmd_stok_barang", "hd_hemodialisa");
	$barang->initialize();
	$obat=new CronjobStockObatED($db, $notification, "smis_hmd_stok_obat", "hd_hemodialisa");
	$obat->initialize();
?>