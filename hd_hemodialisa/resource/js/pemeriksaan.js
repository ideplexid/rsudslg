$(document).ready(function() {
	pemeriksaan.eresep = function(id) {
		var self = this;
		showLoading();	
		var data = this.getEditData(id);
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				var noreg_pasien = json.noreg_pasien;
				var load_data = {
					page 			: "hd_hemodialisa",
					action			: "e_resep",
					proto_implement	: "",
					proto_slug		: "",
					proto_name		: "",
					noreg_pasien	: noreg_pasien
				};
				LoadSmisPage(load_data);
				dismissLoading();
		});
	};
    
    pemeriksaan.back_to_room = function(id) {
		var self = this;
		showLoading();	
		var data = this.getRegulerData();
        data['command']="back_to_room";
        data['id']=id;
		$.post("",data,function(res){
            var json=getContent(res);
            pemeriksaan.view();
            dismissLoading();
        });
    };
    
    pemeriksaan.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };


    pemeriksaan.rehab_medik=function(id){
        LoadSmisPage({
            page:this.page,
            action:"rehab_medik",
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement,
            id_antrian:id
        });
    };

    pemeriksaan.pelayanan_khusus=function(id){
        LoadSmisPage({
            page:this.page,
            action:"pelayanan_khusus",
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement,
            id_antrian:id
        });	
    };

    pemeriksaan.lap_rl52=function(id){
        LoadSmisPage({
            page:this.page,
            action:"lap_rl52",
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement,
            id_antrian:id
        });	
    };
    
    pemeriksaan.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#"+this.prefix+"_noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;
    
        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };
});