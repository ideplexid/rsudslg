<?php
global $db;
$array = array ();

$dbtable=new DBTable($db, "smis_hmd_layanan");
$dbtable->setShowAll(true);
$d=$dbtable->view("", "0");
$list=$d['data'];
foreach($list as $one){
	$array [$one->slug] = $one->nama;
}

//Mendapatkan nama dan kode akun tiap layanan
$list_name          = array();
$list_debit_kredit  = array();
$hmd_dbtable        = new DBTable($db, "smis_hmd_layanan");
$hmd_dbtable->setShowAll(true);
$hmd_dbtable->setOrder(" nama ASC ");
$data               = $hmd_dbtable->view("", "0");
$list_hmd           = $data['data'];
foreach($list_hmd as $d){
    $list_name[$d->nama]            = $d->nama;
    $dray = (array) $d;
    $postfix = "";
    if($d->carabayar!=""){
        $postfix = "_".$d->carabayar;
    }
    $list_debit_kredit[$d->nama]    = array("d"=>$dray['debet'.$postfix],"k"=>['kredit'.$postfix]);
}

if (isset ( $_POST ['noreg_pasien'] )) {
	$noreg = $_POST ['noreg_pasien'];
	$response ['selesai'] = "1";
	$response ['exist'] = "1";
	$response ['reverse'] = "0";
	$response ['cara_keluar'] = "Selesai";
	$response ['jasa_pelayanan'] = "1";
	$dbtable = new DBTable ( $db, "smis_hmd_pesanan" );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$rows = $data ['data'];
	$result = array ();
	foreach ( $rows as $d ) {
        $dk                 = $list_debit_kredit[$d->layanan];
		$onedata ['waktu']  = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata ['nama']   = "Hemodialisa - " . $d->layanan;
		$onedata ['id']     = $d->id;
		$onedata ['biaya']  = $d->total;
		$onedata ['jumlah'] = $d->jumlah;
		$onedata ['start']  = $d->tanggal;
		$onedata ['end']    = $d->tanggal;
		$onedata ['debet']  = $dk['d'];
		$onedata ['kredit'] = $dk['k'];
		
		$ket_res = array ();
		if($d->harga_hdset>0 && $d->hdset>0) 
			$ket_res ['HDSet'] = "HD Set: " . $d->hdset." x @".ArrayAdapter::format("only-money", $d->harga_hdset);
		if($d->harga_onemedpack>0 && $d->onemedpack>0) 
			$ket_res ['One Med Pack'] = "Onemed Pack: " . $d->onemedpack." x @".ArrayAdapter::format("only-money", $d->harga_onemedpack);
		if($d->harga_eritropoitin>0 && $d->eritropoitin>0) 
			$ket_res ['Eritropoitin'] = "Eritropoitin: " . $d->eritropoitin." x @".ArrayAdapter::format("only-money", $d->harga_eritropoitin);
		if($d->harga_spuit20cc>0 && $d->spuit20cc>0) 
			$ket_res ['Spuit20CC'] = "Spuit 20 cc: " . $d->spuit20cc." x @".ArrayAdapter::format("only-money", $d->harga_spuit20cc);
		if($d->harga_spuit3cc>0 && $d->spuit3cc>0) 
			$ket_res ['Spuit3CC'] = "Spuit 3 cc: " . $d->spuit3cc." x @".ArrayAdapter::format("only-money", $d->harga_spuit3cc);
		if($d->harga_spuit5cc>0 && $d->spuit5cc>0) 
			$ket_res ['Spuit5CC'] = "Spuit 5 cc: " . $d->spuit5cc." x @".ArrayAdapter::format("only-money", $d->harga_spuit5cc);
		if($d->harga_alkohol>0 && $d->alkohol>0) 
			$ket_res ['Alkohol'] = "Alcohol 70 %: " . $d->alkohol." x @".ArrayAdapter::format("only-money", $d->harga_alkohol);
		if($d->harga_pz500cc>0 && $d->pz500cc>0) 
			$ket_res ['PZ 500CC'] = "PZ 500 cc: " . $d->pz500cc." x @".ArrayAdapter::format("only-money", $d->harga_pz500cc);
		if($d->harga_pz1000cc>0 && $d->pz1000cc>0) 
			$ket_res ['PZ 1000cc'] = "PZ 1000 cc: " . $d->pz1000cc." x @".ArrayAdapter::format("only-money", $d->harga_pz1000cc);
		if($d->harga_neurobion>0 && $d->neurobion>0) 
			$ket_res ['Neurobion'] = "Neurobion: " . $d->neurobion." x @".ArrayAdapter::format("only-money", $d->harga_neurobion);
		if($d->harga_neurosanbe>0 && $d->neurosanbe>0) 
			$ket_res ['Neurosanbe'] = "Neurosanbe: " . $d->neurosanbe." x @".ArrayAdapter::format("only-money", $d->harga_neurosanbe);
		if($d->harga_bayclin>0 && $d->bayclin>0) 
			$ket_res ['Bayclin'] = "Bayclin / Acid: " . $d->bayclin." x @".ArrayAdapter::format("only-money", $d->harga_bayclin);
		if($d->harga_infuset>0 && $d->infuset>0) 
			$ket_res ['Infuset'] = "Infuset: " . $d->infuset." x @".ArrayAdapter::format("only-money", $d->harga_infuset);
		if($d->harga_heparin>0 && $d->heparin>0) 
			$ket_res ['Heparin'] = "Heparin: " . $d->heparin." x @".ArrayAdapter::format("only-money", $d->harga_heparin);
		if($d->harga_lidocain>0 && $d->lidocain>0) 
			$ket_res ['Lidocain'] = "Lidocain: " . $d->lidocain." x @".ArrayAdapter::format("only-money", $d->harga_lidocain);
		if($d->harga_amlodipin>0 && $d->amlodipin>0) 
			$ket_res ['Amlodipin'] = "Amlodipin: " . $d->amlodipin." x @".ArrayAdapter::format("only-money", $d->harga_amlodipin);
		if($d->harga_cek_hb>0 && $d->cek_hb>0) 
			$ket_res ['Cek HB'] = "Cek HB: " . $d->cek_hb." x @".ArrayAdapter::format("only-money", $d->harga_cek_hb);
		if($d->harga_sewa_ruang>0 && $d->sewa_ruang>0) 
			$ket_res ['Sewa Ruang'] = "Sewa Ruang: " . $d->sewa_ruang." x @".ArrayAdapter::format("only-money", $d->harga_sewa_ruang);
		if($d->harga_jasa_perawat>0 && $d->jasa_perawat>0) 
			$ket_res ['Jasa Perawat'] = "Jasa Perawat: " . $d->jasa_perawat." x @".ArrayAdapter::format("only-money", $d->harga_jasa_perawat);
		if($d->harga_dokter>0 && $d->dokter>0) 
			$ket_res ['Jasa Dokter'] = "Jasa Dokter: " . $d->dokter." x @".ArrayAdapter::format("only-money", $d->harga_dokter);
		if($d->harga_admin>0 && $d->admin>0) 
			$ket_res ['Admin'] = "Admin: " . $d->admin." x @".ArrayAdapter::format("only-money", $d->harga_admin);
		if($d->harga_cyto>0 && $d->cyto>0) 
			$ket_res ['Cyto'] = "Cyto: " . $d->cyto." x @".ArrayAdapter::format("only-money", $d->harga_cyto);
		if($d->harga_renalin>0 && $d->renalin>0) 
			$ket_res ['Renalin'] = "Renalin + H2O2: " . $d->renalin." x @".ArrayAdapter::format("only-money", $d->harga_renalin);
		if($d->harga_masker>0 && $d->masker>0) 
			$ket_res ['Masker'] = "Masker: " . $d->masker." x @".ArrayAdapter::format("only-money", $d->harga_masker);
		
		$ket_res ['Dokter'] = "Dokter: " . $d->nama_dokter;
		$ket_res ['Petugas'] = "Petugas: " . $d->nama_petugas;
		$onedata ['keterangan'] = implode(" , ", $ket_res );
		$result [] = $onedata;
	}
	$unit_data = array (
			"result" => $result,
			"jasa_pelayanan" => "0" 
	);
	$ldata ['hemodialisa'] = $unit_data;
	$response ['data'] = $ldata;
	echo json_encode ( $response );
}

?>