<?php

global $db;
$id         = $_POST['data'];
$dbtable    = new DBTable($db,"smis_hmd_pesanan");
$x          = $dbtable->selectEventDel($id);

//Mendapatkan nama dan kode akun tiap layanan
$list_name          = array();
$list_debit_kredit  = array();
$hmd_dbtable        = new DBTable($db, "smis_hmd_layanan");
$hmd_dbtable->setShowAll(true);
$hmd_dbtable->setOrder(" nama ASC ");
$data               = $hmd_dbtable->view("", "0");
$list_hmd           = $data['data'];
foreach($list_hmd as $d){
    $list_name[$d->nama]            = $d->nama;
    $list_debit_kredit[$d->nama]    = array("d"=>$d->debet,"k"=>$d->kredit);
}

/*transaksi untuk pasien Fisiotherapy*/
$list       = array();
$periksa    = $x->layanan;
$harga      = $x->total;

$debet=array();
$dk                 = $list_debit_kredit[$periksa];
$nama               = $list_name[$periksa];
$debet['akun']      = $dk['d'];
$debet['debet']     = $harga;
$debet['kredit']    = 0;
$debet['ket']       = "Piutang Hemodialisa - ".$nama." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$debet['code']      = "debet-hemodialisa-".$x->id;
$list[]             = $debet;
 
$kredit=array();
$kredit['akun']     = $dk['k'];
$kredit['debet']    = 0;
$kredit['kredit']   = $harga;
$kredit['ket']      = "Pendapatan Hemodialisa - ".$nama." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$kredit['code']     = "kredit-hemodialisa-".$x->id;
$list[]             = $kredit;

//content untuk header
$header=array();
$header['tanggal']      = $x->tanggal;
$header['keterangan']   = "Transaksi Hemodialisa Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Hemodialisa-".$x->id;
$header['nomor']        = "HMD-".$x->id;
$header['debet']        = $x->total;
$header['kredit']       = $x->total;
$header['io']           = "1";

$transaction_Hemodialisa               = array();
$transaction_Hemodialisa['header']     = $header;
$transaction_Hemodialisa['content']    = $list;

/*transaksi keseluruhan*/
$transaction    = array();
$transaction[]  = $transaction_Hemodialisa;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting'] = 1;
$id['id']           = $x->id;
$dbtable->setName("smis_hmd_pesanan");
$dbtable->update($update,$id);

?>