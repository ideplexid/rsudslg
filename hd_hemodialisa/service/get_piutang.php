<?php 
global $db;

require_once 'smis-base/smis-include-service-consumer.php';
$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		if($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="URJI"){
			$ruangan[]=$nama_ruang;
		}
	}
}

$awal=$_POST['awal'];
$akhir=$_POST['akhir'];
$origin=isset($_POST['origin'])?$_POST['origin']:"%";
$response=array();
$query="SELECT ruangan, SUM(biaya) as total 
        FROM smis_hmd_pesanan 
        WHERE tanggal>='$awal' 
        AND tanggal<'$akhir' 
        AND origin LIKE '".$origin."'
        AND prop!='del' 
        GROUP BY ruangan";
$data=$db->get_result($query);
$response=array();
foreach($data as $one){
	$x=array();
	$x['layanan']="hemodialisa";
	$x['ruangan']=$one->ruangan;
	$x['urjip']=in_array($one->ruangan, $ruangan)?"uri":"urj";
	$x['nilai']=$one->total;
	$response[]=$x;
}

echo json_encode($response);
?>