<?php
	global $db;

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah") {
			$depo_farmasi = $_POST['depo_farmasi'];
			$dbtable = new DBTable($db, "data_rekon_" . $depo_farmasi . "_koreksi_penyesuaian");
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM data_rekon_" . $depo_farmasi . "_koreksi_penyesuaian
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			$data = array(
				"jumlah"	=> $jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "update_info") {
			$timestamp = date("d-m-Y, H:i:s");
			$depo_farmasi = $_POST['depo_farmasi'];
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$num = $_POST['num'];
			$limit = $_POST['limit'];
			$dbtable = new DBTable($db, "data_rekon_" . $depo_farmasi . "_koreksi_penyesuaian");
			$row = $dbtable->get_row("
				SELECT *
				FROM data_rekon_" . $depo_farmasi . "_koreksi_penyesuaian
				LIMIT " . $num . ", 1
			");
			$html = "";
			if ($row != null) {
				if ($row->koreksi_penyesuaian != 0) {
					$suffix = str_replace("depo_", "", $depo_farmasi);
					$stok_obat_row = $dbtable->get_row("
						SELECT *
						FROM smis_ap_" . $suffix . "_stok_obat
						WHERE id_obat = '" . $row->id_obat . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					$penyesuaian_stok_row = $dbtable->get_row("
						SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
						FROM smis_ap_" . $suffix . "_penyesuaian_stok_obat a LEFT JOIN smis_ap_" . $suffix . "_stok_obat b ON a.id_stok_obat = b.id
						WHERE a.prop NOT LIKE 'del' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND b.id_obat = '" . $row->id_obat . "'
					");
					$koreksi_penyesuaian = $row->koreksi_penyesuaian - $row->penyesuaian;
					$dbtable = new DBTable($db, "smis_ap_" . $suffix . "_penyesuaian_stok_obat");
					$jumlah_lama = 0;
					$jumlah_baru = 0;
					if ($koreksi_penyesuaian > 0)
						$jumlah_baru = abs($koreksi_penyesuaian);
					else
						$jumlah_lama = abs($koreksi_penyesuaian);
					$html =  "<small>" . $timestamp . " - " . ArrayAdapter::format("only-digit5", ($num + 1)) . " / " . $limit . " - " . ArrayAdapter::format("only-digit6", $row->id_obat) . " - " . $row->nama_obat . " - (" . $koreksi_penyesuaian . " = " . $row->koreksi_penyesuaian . " - " . $row->penyesuaian . ")</small><br/>";
					$data = array(
						"id_stok_obat"	=> $stok_obat_row->id,
						"tanggal"		=> "2016-07-03",
						"jumlah_lama"	=> $jumlah_lama,
						"jumlah_baru"	=> $jumlah_baru,
						"keterangan"	=> "Koreksi Stok Awal",
						"nama_user"		=> "SYSADMIN"
					);
					$dbtable->insert($data);
				}
			}
			$data = array(
				"html"	=> $html
			);
			echo json_encode($data);
		}
		return;
	}

	$form = new Form("", "", "Monitoring dan Validasi : Koreksi Penyesuaian Stok");
	$tanggal_from_text = new Text("normalisai_stok_tanggal_from", "normalisasi_stok_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("normalisai_stok_tanggal_to", "normalisasi_stok_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Akhir", $tanggal_to_text);
	$depo_option = new OptionBuilder();
	$depo_option->add("Rawat Jalan", "depo_irja", "1");
	$depo_option->add("IGD", "depo_igd");
	$depo_option->add("Rawat Inap", "depo_irna");
	$depo_select = new Select("normalisasi_stok_depo", "normalisasi_stok_depo", $depo_option->getContent());
	$form->addElement("Depo Farmasi", $depo_select);
	$button = new Button("", "", "Jalankan!");
	$button->setIsButton(Button::$ICONIC);
	$button->setClass("btn-primary");
	$button->setIcon("icon-white icon-repeat");
	$button->setAction("normalisasi_stok.proceed()");
	$form->addElement("", $button);

	echo $form->getHtml();
	echo "<br/><br/><div id='content'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function NormalisasiStokAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	NormalisasiStokAction.prototype.constructor = NormalisasiStokAction;
	NormalisasiStokAction.prototype = new TableAction();
	NormalisasiStokAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['tanggal_from'] = $("#normalisasi_stok_tanggal_from").val();
		data['tanggal_to'] = $("#normalisasi_stok_tanggal_to").val();
		data['depo_farmasi'] = $("#normalisasi_stok_depo").val();
		return data;
	};
	NormalisasiStokAction.prototype.proceed = function() {
		var self = this;
		var data = this.getRegulerData();
		FINISHED = false;
		$(".smis_form").hide();
		$("#content").html("");
		data['command'] = "get_jumlah";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				self.correct_adjustment(0, json.jumlah);
			}
		);
	};
	NormalisasiStokAction.prototype.correct_adjustment = function(num, limit) {
		if (num == limit || FINISHED) {
			$(".smis_form").show();
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "update_info";
		data['num'] = num;
		data['limit'] = limit;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				$("#content").html(json.html + $("#content").html());
				self.correct_adjustment(num + 1, limit);
			}
		);
	};

	var FINISHED;
	var normalisasi_stok;
	$(document).ready(function() {
		$(".mydate").datepicker();
		normalisasi_stok = new NormalisasiStokAction(
			"normalisasi_stok",
			"monitoring_validasi",
			"normalisasi_stok_depo",
			new Array()
		);
		$("body").keyup(function(e) {
			if (e.which == 27)
				FINISHED = true;
		});
	});
</script>