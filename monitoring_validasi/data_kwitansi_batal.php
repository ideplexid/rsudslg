<?php
	global $db;
	
	$form = new Form("", "", "Monitoring dan Validasi : Data Kwitansi Batal");
	$from_date_text = new Text("dkb_from", "dkb_from", date("Y-m") . "-01");
	$from_date_text->setClass("mydate");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("dkb_to", "dkb_to", date("Y-m-d"));
	$to_date_text->setClass("mydate");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setAction("dkb.view()");
	$form->addElement("", $show_button);
	$table = new Table(
		array("No.", "No. Kwitansi", "Jenis Bayar", "Tanggal", "No. Reg.", "No. RM", "Nama Pasien", "Jenis Pasien", "Nominal"),
		"",
		null,
		true
	);
	$table->setName("dkb");
	$table->setAction(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("No. Kwitansi", "no_kwitansi");
		$adapter->add("Jenis Bayar", "metode", "unslug");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("No. Reg.", "noreg_pasien", "digit6");
		$adapter->add("No. RM", "nrm_pasien");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("Jenis Pasien", "jenis_pasien", "unslug");
		$adapter->add("Nominal", "nominal", "money Rp. ");
		
		$dbtable = new DBTable($db, "smis_ksr_bayar");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (a.waktu LIKE '%" . $_POST['kriteria'] . "%' OR a.noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR b.nrm LIKE '%" . $_POST['kriteria'] . "%' OR c.nama LIKE '%" . $_POST['kriteria'] . "%' OR b.carabayar LIKE '%" . $_POST['kriteria'] . "%' OR a.metode LIKE '%" . $_POST['kriteria'] . "%' OR a.id LIKE '%" . $_POST['kriteria'] . "%' ) ";
		}
		$query_value = "
			SELECT a.id AS 'no_kwitansi', a.metode, a.waktu AS 'tanggal', a.noreg_pasien, b.nrm AS 'nrm_pasien', c.nama AS 'nama_pasien', b.carabayar AS 'jenis_pasien', a.nilai AS 'nominal'
			FROM (smis_ksr_bayar a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id) LEFT JOIN smis_rg_patient c ON b.nrm = c.id
			WHERE a.waktu >= '" . $_POST['tanggal_from'] . "' AND a.waktu <= '" . $_POST['tanggal_to'] . "' " . $filter . " AND a.prop LIKE 'del'
		";
		$query_count = "
			SELECT COUNT(*) AS 'jumlah'
			FROM (" . $query_value . ") v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $form->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function DKBAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DKBAction.prototype.constructor = DKBAction;
	DKBAction.prototype = new TableAction();
	DKBAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['tanggal_from'] = $("#dkb_from").val();
		data['tanggal_to'] = $("#dkb_to").val();
		return data;
	};
	var dkb;
	$(document).ready(function() {
		$(".mydate").datepicker();
		dkb = new DKBAction(
			"dkb",
			"monitoring_validasi",
			"data_kwitansi_batal",
			new Array()
		);
		$(".pagination").html("");
	});
</script>