<?php
	global $db;
	$form = new Form("", "", "Monitoring dan Validasi : Data Pasien Daftar Dobel");
	$from_date_text = new Text("dpd_from", "dpd_from", date("Y-m") . "-01");
	$from_date_text->setClass("mydate");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("dpd_to", "dpd_to", date("Y-m-d"));
	$to_date_text->setClass("mydate");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$form->addELement("Kecuali Pasien:", "");
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setAction("dpd.view()");
	$excel_button = new Button("", "", "Ekspor Berkas Excel");
	$excel_button->setIsButton(Button::$ICONIC);
	$excel_button->setClass("btn-inverse");
	$excel_button->setIcon("fa fa-download");
	$excel_button->setAction("dpd.export_xls()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($show_button);
	$button_group->addButton($excel_button);
		
	class FilterPasienTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup('noprint');
			$btn = new Button($this->name . "_add", "", "Add");
			$btn->setAction("dpd_rm.chooser('dpd_rm', 'dpd_rm_button', 'dpd_rm', dpd_rm)");
			$btn->setClass("btn-primary");
			$btn->setIcon("fa fa-plus");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button($this->name . "_clear", "", "Clear");
			$btn->setAction("dpd_rm.clear()");
			$btn->setClass("btn-danger");
			$btn->setIcon("fa fa-trash");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$filter_pasien_table = new FilterPasienTable(
		array("NRM", "Pasien"),
		"",
		null,
		true
	);
	$filter_pasien_table->setName("dpd_pasien_filter");
	$filter_pasien_table->setFooterVisible(false);
	
	$table = new Table(
		array("No.", "Tanggal", "NRM", "Nama", "Alamat", "Keterangan"),
		"",
		null,
		true
	);
	$table->setName("dpd");
	$table->setAction(false);
	
	//chooser rm:
	$rm_table = new Table(
		array("NRM", "Pasien"),
		"",
		null,
		true
	);
	$rm_table->setName("dpd_rm");
	$rm_table->setModel(Table::$SELECT);
	$rm_adapter = new SimpleAdapter();
	$rm_adapter->add("NRM", "id", "digit6");
	$rm_adapter->add("Pasien", "nama");
	$rm_dbtable = new DBTable($db, "smis_rg_patient");
	$rm_dbtable->setOrder(" id DESC ");
	$rm_dbresponder = new DBResponder(
		$rm_dbtable,
		$rm_table,
		$rm_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("dpd_rm", $rm_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_excel") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$filter_nrm = " a.nrm NOT IN (-1) ";
			if (isset($_POST['nrm_list'])) {
				$nrm = $_POST['nrm_list'];
				if (count($nrm) > 0) {
					$filter_nrm = " a.nrm NOT IN ('" . implode("','", $nrm) . "') ";
				}
			}
			$dbtable = new DBTable($db, "smis_rg_layananpasien");
			$rows = $dbtable->get_result("
				SELECT *
				FROM (
					SELECT a.tanggal, a.nrm AS 'nrm_pasien', b.alamat, b.nama AS 'nama_pasien', GROUP_CONCAT(
						CONCAT(a.id, ' - ', a.carabayar, ' - ', a.jenislayanan, ' - ', karcis) 
					SEPARATOR '; ') AS 'keterangan', COUNT(a.id) AS 'jumlah_daftar'
					FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
					WHERE a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND " . $filter_nrm . "
					GROUP BY a.tanggal, a.nrm 
					HAVING jumlah_daftar > 1 
					ORDER BY a.tanggal, a.nrm ASC
				) v
			");
			$row_num = "1";
			$col_num = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Tanggal");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "NRM");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Nama");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Alamat");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Keterangan");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Jumlah Daftar");
			$row_num++;
			foreach($rows as $r) {
				$col_num = "A";
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("date d-m-Y", $r->tanggal));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("only-digit6", $r->nrm_pasien));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->nama_pasien));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->alamat));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->keterangan));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, $r->jumlah_daftar);
				$row_num++;
			}
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename='data_pasien_dobel_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx'");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("NRM", "nrm_pasien", "digit6");
		$adapter->add("Nama", "nama_pasien", "unslug");
		$adapter->add("Alamat", "alamat", "unslug");
		$adapter->add("Keterangan", "keterangan", "unslug");
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$numeric_filter = "";
			if (is_numeric($_POST['kriteria'])) {
				$numeric_filter = " OR nrm_pasien LIKE '%" . ($_POST['kriteria'] * 1) . "%'";
			}
			$filter = "WHERE (tanggal LIKE '%" . $_POST['kriteria'] . "%' OR nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR keterangan LIKE '%" . $_POST['kriteria'] . "%' " . $numeric_filter . ")";
		}
		$filter_nrm = " a.nrm NOT IN (-1) ";
		if (isset($_POST['nrm_list'])) {
			$nrm = $_POST['nrm_list'];
			if (count($nrm) > 0) {
				$filter_nrm = " a.nrm NOT IN ('" . implode("','", $nrm) . "') ";
			}
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT a.tanggal, a.nrm AS 'nrm_pasien', b.alamat, b.nama AS 'nama_pasien', GROUP_CONCAT(
					CONCAT(a.id, ' - ', a.carabayar, ' - ', a.jenislayanan, ' - ', karcis) 
				SEPARATOR '<br/>') AS 'keterangan', COUNT(a.id) AS 'jumlah_daftar'
				FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
				WHERE a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND " . $filter_nrm . "
				GROUP BY a.tanggal, a.nrm 
				HAVING jumlah_daftar > 1 
				ORDER BY a.tanggal, a.nrm ASC
			) v
			" . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT a.tanggal, a.nrm AS 'nrm_pasien', b.alamat, b.nama AS 'nama_pasien', GROUP_CONCAT(
					CONCAT(a.id, ' - ', a.carabayar, ' - ', a.jenislayanan, ' - ', karcis) 
				SEPARATOR '<br/>') AS 'keterangan', COUNT(a.id) AS 'jumlah_daftar'
				FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
				WHERE a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND " . $filter_nrm . "
				GROUP BY a.tanggal, a.nrm 
				HAVING jumlah_daftar > 1 
				ORDER BY a.tanggal, a.nrm ASC
			) v
			" . $filter . "
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>" .
				 "<div class='span6'>" .
					 $filter_pasien_table->getHtml() .
				 "</div>" .
			 "</div>" .
			 "<div class='row-fluid'>" .
				 $button_group->getHtml() .
			 "</div>" .
		 "</div>";
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function DPDAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPDAction.prototype.constructor = DPDAction;
	DPDAction.prototype = new TableAction();
	DPDAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		var rm_list = new Array();
		var nor_rm =  $("tbody#dpd_pasien_filter_list").children("tr").length;
		for (var i = 0; i < nor_rm; i++) {
			var rm_prefix = $("tbody#dpd_pasien_filter_list").children("tr").eq(i).prop("id");
			var nrm = $("#" + rm_prefix + "_nrm").text();
			rm_list.push(nrm);
		}
		data['nrm_list'] = rm_list;
		data['from'] = $("#dpd_from").val();
		data['to'] = $("#dpd_to").val();
		return data;
	};
	DPDAction.prototype.export_xls = function() {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "export_excel";
		postForm(data);
	};

	function DPDRMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPDRMAction.prototype.constructor = DPDRMAction;
	DPDRMAction.prototype = new TableAction();
	DPDRMAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		if (is_numeric(data['kriteria'])) {
			data['kriteria'] = 1 * Number(data['kriteria']);
		}
		return data;
	};
	DPDRMAction.prototype.clear = function() {
		$("#dpd_pasien_filter_list").empty();		
	};
	DPDRMAction.prototype.delete = function(rnum) {
		$("#dpd_rm_" + rnum).remove();		
	};
	DPDRMAction.prototype.selected = function(json) {
		$("tbody#dpd_pasien_filter_list").append(
			"<tr id='dpd_rm_" + dpd_rm_num + "'>" +
				"<td id='dpd_rm_" + dpd_rm_num + "_nrm'>" + json.id + "</td>" +
				"<td id='dpd_rm_" + dpd_rm_num + "_nama'>" + json.nama + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='dpd_rm.delete(" + dpd_rm_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		dpd_rm_num++;
	};
	
	var dpd;
	var dpd_rm;
	var dpd_rm_num;
	$(document).ready(function() {
		$('.mydate').datepicker();
		dpd_rm_num = 0;
		dpd_rm = new DPDRMAction(
			"dpd_rm",
			"monitoring_validasi",
			"monitoring_pendaftaran_pasien_dobel",
			new Array()
		);
		dpd_rm.setSuperCommand("dpd_rm");
		dpd = new DPDAction(
			"dpd",
			"monitoring_validasi",
			"monitoring_pendaftaran_pasien_dobel",
			new Array("nominal_kso", "persentase_ideplex")
		);
		$("#dpd_pagination").html("");
	});
</script>
