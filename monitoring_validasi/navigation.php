<?php
	global $NAVIGATOR;
	// Administrator Top Menu
	$mr = new Menu ("fa fa-cube");
	$mr->addProperty ( 'title', 'Monitoring dan Validasi' );
	$mr->addProperty ( 'name', 'Monitoring dan Validasi' );
	
	require_once 'smis-libs-inventory/navigation.php';
	$mr->addSubMenu("Data Pasien Non-IGD-HD", "monitoring_validasi", "monitoring_pendaftaran_pasien_rajal_non_igd_hd", "Monitoring Pendaftaran Pasien Rajal Non-IGD-HD", "fa fa-file-text-o");
	$mr->addSubMenu("Data Pasien Hemodialisa", "monitoring_validasi", "monitoring_pendataan_pasien_hd", "Monitoring Pendataan Pasien Hemodialisa", "fa fa-file-text-o");
	$mr->addSubMenu("Data Pasien IGD", "monitoring_validasi", "monitoring_pendataan_pasien_igd", "Monitoring Pendataan Pasien IGD", "fa fa-file-text-o");
	$mr->addSubMenu("Data Pasien Daftar Dobel", "monitoring_validasi", "monitoring_pendaftaran_pasien_dobel", "Monitoring Pendaftaran Pasien Dobel", "fa fa-file-text-o");
	$mr->addSubMenu("Data Pasien Non-IGD-HD Dobel", "monitoring_validasi", "monitoring_pendaftaran_pasien_dobel_non_igd_hd", "Monitoring Pendaftaran Pasien Non-IGD-HD Dobel", "fa fa-file-text-o");
	$mr->addSubMenu("Data Pasien Hemodialisa Dobel", "monitoring_validasi", "monitoring_pendaftaran_pasien_dobel_hemodialisa", "Monitoring Pendaftaran Pasien Hemodialisa Dobel", "fa fa-file-text-o");
	$mr->addSubMenu("Data Pasien IGD Dobel", "monitoring_validasi", "monitoring_pendaftaran_pasien_dobel_igd", "Monitoring Pendaftaran Pasien IGD Dobel", "fa fa-file-text-o");
	$mr->addSubMenu("Data Kwitansi Batal", "monitoring_validasi", "data_kwitansi_batal", "Data Kwitansi Batal", "fa fa-close");
	$mr->addSubMenu("Norm. Stok Depo Farmasi", "monitoring_validasi", "normalisasi_stok_depo", "Normalisasi Stok Depo", "fa fa-adjust");
	$mr->addSubMenu("Rekonsiliasi Auto", "monitoring_validasi", "rekonsiliasi_auto", "Rekonsiliasi Auto", "fa fa-auto");
	$NAVIGATOR->addMenu ( $mr, 'monitoring_validasi' );
?>
