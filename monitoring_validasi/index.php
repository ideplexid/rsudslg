<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	
	$policy=new Policy("monitoring_validasi", $user);
	$policy->addPolicy("monitoring_pendaftaran_pasien_rajal_non_igd_hd", "monitoring_pendaftaran_pasien_rajal_non_igd_hd", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("monitoring_pendataan_pasien_hd", "monitoring_pendataan_pasien_hd", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("monitoring_pendataan_pasien_igd", "monitoring_pendataan_pasien_igd", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("monitoring_pendaftaran_pasien_dobel", "monitoring_pendaftaran_pasien_dobel", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("monitoring_pendaftaran_pasien_dobel_non_igd_hd", "monitoring_pendaftaran_pasien_dobel_non_igd_hd", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("monitoring_pendaftaran_pasien_dobel_hemodialisa", "monitoring_pendaftaran_pasien_dobel_hemodialisa", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("monitoring_pendaftaran_pasien_dobel_igd", "monitoring_pendaftaran_pasien_dobel_igd", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("data_kwitansi_batal", "data_kwitansi_batal", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("normalisasi_stok_depo", "normalisasi_stok_depo", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("rekonsiliasi_auto", "rekonsiliasi_auto", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->initialize();
?>
