<?php
	global $db;

	$form = new Form("", "", "Monitoring dan Validasi : Auto-Rekonsiliasi");
	$depo_option = new OptionBuilder();
	$depo_option->add("DEPO IGD", "ap_igd");
	$depo_option->add("DEPO IRJA", "ap_irja", "1");
	$depo_option->add("DEPO IRNA", "ap_irna");
	$depo_option->add("DEPO IRNA II", "ap_irna_ii");
	$depo_option->add("DEPO IRNA III", "ap_irna_iii");
	$depo_select = new Select("rekonsiliasi_auto_depo", "rekonsiliasi_auto_depo", $depo_option->getContent());
	$form->addElement("Depo Farmasi", $depo_select);
	$data_source_option = new OptionBuilder();
	$data_source_option->add("Data Rekon. D. IGD", "data_rekon_auto_depo_igd_20160731");
	$data_source_option->add("Data Rekon. D. IRJA", "data_rekon_auto_depo_irja_20160731", "1");
	$data_source_option->add("Data Rekon. D. IRNA", "data_rekon_auto_depo_irna_20160731");
	$data_source_option->add("Data Rekon. D. IRNA 2", "data_rekon_auto_depo_irna_ii_20160731");
	$data_source_option->add("Data Rekon. D. IRNA 3", "data_rekon_auto_depo_irna_iii_20160731");
	$data_source_select = new Select("rekonsiliasi_auto_data_source", "rekonsiliasi_auto_data_source", $data_source_option->getContent());
	$form->addElement("Sumber Data", $data_source_select);
	$save_button = new Button("", "", "Proses");
	$save_button->setClass("btn-success");
	$save_button->setIcon("fa fa-recycle");
	$save_button->setIsButton(Button::$ICONIC);
	$save_button->setAction("rekonsiliasi_auto.proceed()");
	$form->addElement("", $save_button);

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah") {
			$depo = $_POST['depo'];
			$data_source = $_POST['data_source'];
			$dbtable = new DBTable($db, $data_source);
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM " . $data_source . "
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			$data = array(
				"jumlah"	=> $jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$timestamp = date("d-m-Y, H:i:s");
			$depo = $_POST['depo'];
			$data_source = $_POST['data_source'];
			$num = $_POST['num'];
			$limit = $_POST['limit'];
			$dbtable = new DBTable($db, $data_source);
			$row = $dbtable->get_row("
				SELECT *
				FROM " . $data_source . "
				LIMIT " . $num . ", 1
			");
			$html = "";
			if ($row != null) {
				$id_obat = $row->id_obat;
				$stok_riil = $row->stok_riil;
				$satuan = $row->satuan;
				$satuan_konversi = $row->satuan_konversi;
				$sisa_row = $dbtable->get_row("
					SELECT SUM(sisa) AS 'jumlah'
					FROM smis_" . $depo . "_stok_obat a LEFT JOIN smis_" . $depo . "_obat_masuk b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '1' AND a.satuan_konversi = '" . $satuan_konversi . "'
				");
				$sisa = 0;
				if ($sisa_row != null)
					$sisa = $sisa_row->jumlah;
				$selisih = $stok_riil - $sisa;
				$keterangan = "Rekonsiliasi Stok Obat* : Otomasi Rekons. Berdasarkan XLS SO";

				// REKONSILIASI :
				$stok_dbtable = new DBTable($db, "smis_" . $depo . "_stok_obat");
				if ($selisih > 0) {
					$stok_row = $stok_dbtable->get_row("
						SELECT a.*
						FROM smis_" . $depo . "_stok_obat a LEFT JOIN smis_" . $depo . "_obat_masuk b ON a.id_obat_masuk = b.id
						WHERE a.id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '1' AND satuan_konversi = '" . $satuan . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah'
						ORDER BY a.id DESC
						LIMIT 0, 1
					");
					$jumlah_lama = $stok_row->sisa;
					$jumlah_baru = $stok_row->sisa + $selisih;
					$stok_data = array(
						"sisa"	=> $stok_row->sisa + $selisih
					);
					$stok_id = array(
						"id"	=> $stok_row->id
					);
					$stok_dbtable->update($stok_data, $stok_id);

					$penyesuaian_dbtable = new DBTable($db, "smis_" . $depo . "_penyesuaian_stok_obat");
					$penyesuaian_data = array(
						"id_stok_obat"  => $stok_row->id,
						"tanggal"		=> "2016-07-31",
						"jumlah_lama"	=> $jumlah_lama,
						"jumlah_baru"	=> $jumlah_baru,
						"keterangan"	=> $keterangan,
						"nama_user"		=> "SYSADMIN"
					);
					$penyesuaian_dbtable->insert($penyesuaian_data);

					$riwayat_stok_dbtable = new DBTable($db, "smis_" . $depo . "_riwayat_stok_obat");
					$riwayat_data = array(
						"tanggal"		=> "2016-07-31",
						"id_stok_obat"	=> $stok_row->id,
						"jumlah_masuk"	=> $selisih,
						"jumlah_keluar"	=> 0,
						"sisa"			=> ($stok_row->sisa + $selisih),
						"keterangan"	=> $keterangan,
						"nama_user"		=> "SYSADMIN"
					);
					$riwayat_stok_dbtable->insert($riwayat_data);

					// $sisa_row = $dbtable->get_row("
					// 	SELECT SUM(a.sisa) AS 'sisa'
					// 	FROM smis_" . $depo . "_stok_obat a LEFT JOIN smis_" . $depo . "_obat_masuk b ON a.id_obat_masuk = b.id
					// 	WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $stok_row->id_obat . "' AND a.satuan = '" . $stok_row->satuan . "' AND a.konversi = '1' AND b.prop NOT LIKE 'del' AND b.status = 'sudah'
					// ");
					// $kartu_stok_dbtable = new DBTable($db, "smis_" . $depo . "_kartu_stok_obat");
					// $kartu_stok_data = array(
					// 	"f_id"				=> 0,
					// 	"no_bon"			=> "-",
					// 	"unit"				=> "Rekonsiliasi Stok : " . $keterangan,
					// 	"id_obat"			=> $stok_row->id_obat,
					// 	"kode_obat"			=> $stok_row->kode_obat,
					// 	"nama_obat"			=> $stok_row->nama_obat,
					// 	"nama_jenis_obat"	=> $stok_row->nama_jenis_obat,
					// 	"tanggal"			=> date("Y-m-d"),
					// 	"masuk"				=> $selisih,
					// 	"keluar"			=> 0,
					// 	"sisa"				=> $sisa_row->sisa
					// );
					// $kartu_stok_dbtable->insert($kartu_stok_data);
				} else if ($selisih < 0) {
					$stok_rows = $stok_dbtable->get_result("
						SELECT a.*
						FROM smis_" . $depo . "_stok_obat a LEFT JOIN smis_" . $depo . "_obat_masuk b ON a.id_obat_masuk = b.id
						WHERE a.id_obat = '" . $id_obat . "' AND satuan = '" . $satuan. "' AND konversi = '1' AND satuan_konversi = '" . $satuan . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.sisa > 0
					");
					$info_selisih = $selisih;
					foreach ($stok_rows as $stok_row) {
						$sisa_stok = $stok_row->sisa;
						if ($sisa_stok + $selisih >= 0) {
							$jumlah_lama = $sisa_stok;
							$sisa_stok = $sisa_stok + $selisih;
							$jumlah_baru = $sisa_stok;
							$stok_data = array(
								"sisa"	=> $sisa_stok
							);
							$stok_id = array(
								"id"	=> $stok_row->id
							);
							$stok_dbtable->update($stok_data, $stok_id);

							$penyesuaian_dbtable = new DBTable($db, "smis_" . $depo . "_penyesuaian_stok_obat");
							$penyesuaian_data = array(
								"id_stok_obat"  => $stok_row->id,
								"tanggal"		=> "2016-07-31",
								"jumlah_lama"	=> $jumlah_lama,
								"jumlah_baru"	=> $jumlah_baru,
								"keterangan"	=> $keterangan,
								"nama_user"		=> "SYSADMIN"
							);
							$penyesuaian_dbtable->insert($penyesuaian_data);

							$riwayat_stok_dbtable = new DBTable($db, "smis_" . $depo . "_riwayat_stok_obat");
							$riwayat_data = array(
								"tanggal"		=> "2016-07-31",
								"id_stok_obat"	=> $stok_row->id,
								"jumlah_masuk"	=> 0,
								"jumlah_keluar"	=> abs($selisih),
								"sisa"			=> $sisa_stok,
								"keterangan"	=> $keterangan,
								"nama_user"		=> "SYSADMIN"
							);
							$riwayat_stok_dbtable->insert($riwayat_data);
							$selisih = 0;
						} else {
							$jumlah_lama = $sisa_stok;
							$jumlah_baru = 0;
							$jumlah_keluar = $sisa_stok;
							$selisih = $selisih + $sisa_stok;
							$sisa_stok = 0;
							$stok_data = array(
								"sisa"	=> $sisa_stok
							);
							$stok_id = array(
								"id"	=> $stok_row->id
							);
							$stok_dbtable->update($stok_data, $stok_id);

							$penyesuaian_dbtable = new DBTable($db, "smis_" . $depo . "_penyesuaian_stok_obat");
							$penyesuaian_data = array(
								"id_stok_obat"  => $stok_row->id,
								"tanggal"		=> "2016-07-31",
								"jumlah_lama"	=> $jumlah_lama,
								"jumlah_baru"	=> $jumlah_baru,
								"keterangan"	=> $keterangan,
								"nama_user"		=> "SYSADMIN"
							);
							$penyesuaian_dbtable->insert($penyesuaian_data);
							
							$riwayat_stok_dbtable = new DBTable($db, "smis_" . $depo . "_riwayat_stok_obat");
							$riwayat_data = array(
								"tanggal"		=> "2016-07-31",
								"id_stok_obat"	=> $stok_row->id,
								"jumlah_masuk"	=> 0,
								"jumlah_keluar"	=> $jumlah_keluar,
								"sisa"			=> $sisa_stok,
								"keterangan"	=> $keterangan,
								"nama_user"		=> "SYSADMIN"
							);
							$riwayat_stok_dbtable->insert($riwayat_data);
						}
						if ($selisih == 0)
							break;
					}
					// $sisa_row = $dbtable->get_row("
					// 	SELECT SUM(a.sisa) AS 'sisa'
					// 	FROM smis_" . $depo . "_stok_obat a LEFT JOIN smis_" . $depo . "_obat_masuk b ON a.id_obat_masuk = b.id
					// 	WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $stok_rows[0]->id_obat . "' AND a.satuan = '" . $stok_rows[0]->satuan . "' AND a.konversi = '1' AND b.prop NOT LIKE 'del' AND b.status = 'sudah'
					// ");
					// $kartu_stok_dbtable = new DBTable($db, "smis_" . $depo . "_kartu_stok_obat");
					// $kartu_stok_data = array(
					// 	"f_id"				=> 0,
					// 	"no_bon"			=> "-",
					// 	"unit"				=> "Rekonsiliasi Stok : " . $keterangan,
					// 	"id_obat"			=> $stok_rows[0]->id_obat,
					// 	"kode_obat"			=> $stok_rows[0]->kode_obat,
					// 	"nama_obat"			=> $stok_rows[0]->nama_obat,
					// 	"nama_jenis_obat"	=> $stok_rows[0]->nama_jenis_obat,
					// 	"tanggal"			=> date("Y-m-d"),
					// 	"masuk"				=> $info_selisih >= 0 ? $info_selisih : 0,
					// 	"keluar"			=> $info_selisih < 0 ? (-1) * $info_selisih : 0,
					// 	"sisa"				=> $sisa_row->sisa
					// );
					// $kartu_stok_dbtable->insert($kartu_stok_data);
				}

				$html = ArrayAdapter::format("only-digit4", $num) . " / " . $limit . " (" . $timestamp . ") - " . ArrayAdapter::format("only-digit8", $row->id_obat) . " - " . $row->kode . " - " . $row->nama . " - SISA : " . $sisa . " - RIIL : " . $stok_riil . " <br/>";
			}
			$data = array(
				"id_obat"	=> $row->id_obat,
				"kode"		=> $row->kode,
				"nama"		=> $row->nama,
				"html"		=> $html
			);
			echo json_encode($data);
		}
		return;
	}

	echo $form->getHtml();
	echo "<br/><br/>";
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function AutoRekonsiliasiAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	AutoRekonsiliasiAction.prototype.constructor = AutoRekonsiliasiAction;
	AutoRekonsiliasiAction.prototype = new TableAction();
	AutoRekonsiliasiAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['depo'] = $("#rekonsiliasi_auto_depo").val();
		data['data_source']	= $("#rekonsiliasi_auto_data_source").val();
		return data;
	};
	AutoRekonsiliasiAction.prototype.proceed = function() {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_jumlah";
		$("#info").empty();
		console.log(data);
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	AutoRekonsiliasiAction.prototype.fillHtml = function(num, limit) {
		if (num == limit) {
			$("#info").append("DONE<br/>");
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info";
		data['num'] = num;
		data['limit'] = limit;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#info").append(json.html);
				self.fillHtml(num + 1, limit);
			}
		);
	};

	var rekonsiliasi_auto;
	$(document).ready(function() {
		rekonsiliasi_auto = new AutoRekonsiliasiAction(
			"rekonsiliasi_auto",
			"monitoring_validasi",
			"rekonsiliasi_auto",
			new Array()
		);
	});
</script>