<?php
	global $db;
	$form = new Form("", "", "Monitoring dan Validasi : Data Pasien Hemodialisa");
	$from_date_text = new Text("dprh_from", "dprh_from", date("Y-m") . "-01");
	$from_date_text->setClass("mydate");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("dprh_to", "dprh_to", date("Y-m-d"));
	$to_date_text->setClass("mydate");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$nominal_kso_text = new Text("dprh_nominal_kso", "dprh_nominal_kso", "15000");
	$nominal_kso_text->setTypical("money");
	$nominal_kso_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. ' data-precision='2'");
	$form->addElement("Nominal KSO", $nominal_kso_text);
	$persentase_ideplex_text = new Text("dprh_persentase_ideplex", "dprh_persentase_ideplex", "77");
	$persentase_ideplex_text->setTypical("money");
	$persentase_ideplex_text->setAtribute("data-thousands='.' data-decimal=',' data-precision='2'");
	$form->addElement("Persen Ideplex", $persentase_ideplex_text);
	$form->addELement("Kecuali Pasien:", "");
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setAction("dprh.view()");
	$excel_button = new Button("", "", "Ekspor Berkas Excel");
	$excel_button->setIsButton(Button::$ICONIC);
	$excel_button->setClass("btn-inverse");
	$excel_button->setIcon("fa fa-download");
	$excel_button->setAction("dprh.export_xls()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($show_button);
	$button_group->addButton($excel_button);
		
	class FilterPasienTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup('noprint');
			$btn = new Button($this->name . "_add", "", "Add");
			$btn->setAction("dprh_rm.chooser('dprh_rm', 'dprh_rm_button', 'dprh_rm', dprh_rm)");
			$btn->setClass("btn-primary");
			$btn->setIcon("fa fa-plus");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button($this->name . "_clear", "", "Clear");
			$btn->setAction("dprh_rm.clear()");
			$btn->setClass("btn-danger");
			$btn->setIcon("fa fa-trash");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$filter_pasien_table = new FilterPasienTable(
		array("NRM", "Pasien"),
		"",
		null,
		true
	);
	$filter_pasien_table->setName("dprh_pasien_filter");
	$filter_pasien_table->setFooterVisible(false);
	
	$table = new Table(
		array("No.", "Tanggal", "No. Reg.", "NRM", "Nama", "Jenis Pasien", "Unit Awal", "Status Pulang", "Biaya Reg.", "KSO", "Hak Ideplex"),
		"",
		null,
		true
	);
	$table->setName("dprh");
	$table->setAction(false);
	
	//chooser rm:
	$rm_table = new Table(
		array("NRM", "Pasien"),
		"",
		null,
		true
	);
	$rm_table->setName("dprh_rm");
	$rm_table->setModel(Table::$SELECT);
	$rm_adapter = new SimpleAdapter();
	$rm_adapter->add("NRM", "id", "digit6");
	$rm_adapter->add("Pasien", "nama");
	$rm_dbtable = new DBTable($db, "smis_rg_patient");
	$rm_dbtable->setOrder(" id DESC ");
	$rm_dbresponder = new DBResponder(
		$rm_dbtable,
		$rm_table,
		$rm_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("dprh_rm", $rm_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_excel") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$filter_nrm = " a.nrm NOT IN (-1) ";
			if (isset($_POST['nrm_list'])) {
				$nrm = $_POST['nrm_list'];
				if (count($nrm) > 0) {
					$filter_nrm = " a.nrm NOT IN ('" . implode("','", $nrm) . "') ";
				}
			}
			$dbtable = new DBTable($db, "smis_rg_layananpasien");
			$rows = $dbtable->get_result("
				SELECT *
				FROM (
					SELECT a.tanggal, a.id AS 'noreg_pasien', a.nrm AS 'nrm_pasien', b.nama AS 'nama_pasien', a.jenislayanan AS 'unit', a.karcis AS 'biaya_registrasi', a.carabayar, a.carapulang, " . $_POST['nominal_kso'] . " AS 'nominal_kso', " . ($_POST['persentase_ideplex'] * $_POST['nominal_kso'] / 100) . " AS 'hak_ideplex'
					FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
					WHERE a.prop NOT LIKE 'del' AND a.jenislayanan = 'hemodialisa' AND a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND " . $filter_nrm . "
				) v
				ORDER BY tanggal, noreg_pasien ASC
			");
			$row_num = "1";
			$col_num = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Tanggal");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "No. Reg.");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "NRM");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Nama");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Jenis Pasien");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Unit Awal");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Status Pulang");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Biaya Reg.");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "KSO");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Hak Ideplex");
			$row_num++;
			foreach($rows as $r) {
				$col_num = "A";
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("date d-m-Y", $r->tanggal));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("only-digit6", $r->noreg_pasien));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("only-digit6", $r->nrm_pasien));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->nama_pasien));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->carabayar));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->unit));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->carapulang));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, $r->biaya_registrasi);
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, $r->nominal_kso);
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, $r->hak_ideplex);
				$row_num++;
			}
			$col_num = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "TOTAL");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "=SUM(H2:H" . ($row_num-1) . ")");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "=SUM(I2:I" . ($row_num-1) . ")");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "=SUM(J2:J" . ($row_num-1) . ")");
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename='data_pasien_hemodialisa_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx'");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		$adapter = new SummaryAdapter(true, "No.");
		$adapter->addFixValue("Status Pulang", "<strong>Total</strong>");
		$adapter->addSummary("Biaya Reg.", "biaya_registrasi", "money");
		$adapter->addSummary("KSO", "nominal_kso", "money");
		$adapter->addSummary("Hak Ideplex", "hak_ideplex", "money");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("No. Reg.", "noreg_pasien", "digit6");
		$adapter->add("NRM", "nrm_pasien", "digit6");
		$adapter->add("Nama", "nama_pasien", "unslug");
		$adapter->add("Jenis Pasien", "carabayar", "unslug");
		$adapter->add("Unit Awal", "unit", "unslug");
		$adapter->add("Biaya Reg.", "biaya_registrasi", "money");
		$adapter->add("Status Pulang", "carapulang", "unslug");
		$adapter->add("KSO", "nominal_kso", "money");
		$adapter->add("Hak Ideplex", "hak_ideplex", "money");
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$numeric_filter = "";
			if (is_numeric($_POST['kriteria'])) {
				$numeric_filter = " OR noreg_pasien LIKE '%" . ($_POST['kriteria'] * 1) . "%' OR nrm_pasien LIKE '%" . ($_POST['kriteria'] * 1) . "%'";
			}
			$filter = "WHERE (tanggal LIKE '%" . $_POST['kriteria'] . "%' OR nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%' " . $numeric_filter . ")";
		}
		$filter_nrm = " a.nrm NOT IN (-1) ";
		if (isset($_POST['nrm_list'])) {
			$nrm = $_POST['nrm_list'];
			if (count($nrm) > 0) {
				$filter_nrm = " a.nrm NOT IN ('" . implode("','", $nrm) . "') ";
			}
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT a.tanggal, a.id AS 'noreg_pasien', a.nrm AS 'nrm_pasien', b.nama AS 'nama_pasien', a.jenislayanan AS 'unit', a.karcis AS 'biaya_registrasi', a.carabayar, a.carapulang, " . $_POST['nominal_kso'] . " AS 'nominal_kso', " . ($_POST['persentase_ideplex'] * $_POST['nominal_kso'] / 100) . " AS 'hak_ideplex'
				FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
				WHERE a.prop NOT LIKE 'del' AND a.jenislayanan = 'hemodialisa' AND a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND " . $filter_nrm . "
			) v
			" . $filter . "
			ORDER BY tanggal, noreg_pasien ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT a.tanggal, a.id AS 'noreg_pasien', a.nrm AS 'nrm_pasien', b.nama AS 'nama_pasien', a.jenislayanan AS 'unit', a.karcis AS 'biaya_registrasi', a.carabayar, a.carapulang, " . $_POST['nominal_kso'] . " AS 'nominal_kso', " . ($_POST['persentase_ideplex'] * $_POST['nominal_kso'] / 100) . " AS 'hak_ideplex'
				FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
				WHERE a.prop NOT LIKE 'del' AND a.jenislayanan = 'hemodialisa' AND a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND " . $filter_nrm . "
			) v
			" . $filter . "
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>" .
				 "<div class='span6'>" .
					 $filter_pasien_table->getHtml() .
				 "</div>" .
			 "</div>" .
			 "<div class='row-fluid'>" .
				 $button_group->getHtml() .
			 "</div>" .
		 "</div>";
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function DPRHAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPRHAction.prototype.constructor = DPRHAction;
	DPRHAction.prototype = new TableAction();
	DPRHAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		var rm_list = new Array();
		var nor_rm =  $("tbody#dprh_pasien_filter_list").children("tr").length;
		for (var i = 0; i < nor_rm; i++) {
			var rm_prefix = $("tbody#dprh_pasien_filter_list").children("tr").eq(i).prop("id");
			var nrm = $("#" + rm_prefix + "_nrm").text();
			rm_list.push(nrm);
		}
		data['nrm_list'] = rm_list;
		data['from'] = $("#dprh_from").val();
		data['to'] = $("#dprh_to").val();
		var v_nominal_kso = parseFloat($("#dprh_nominal_kso").val().replace(/[^0-9-,]/g, '').replace(",", "."));
		data['nominal_kso'] = v_nominal_kso;
		var v_persentase_ideplex = parseFloat($("#dprh_persentase_ideplex").val().replace(/[^0-9-,]/g, '').replace(",", "."));
		data['persentase_ideplex'] = v_persentase_ideplex;
		return data;
	};
	DPRHAction.prototype.export_xls = function() {
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "export_excel";
		postForm(data);
		dismissLoading();
	};

	function DPRHRMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPRHRMAction.prototype.constructor = DPRHRMAction;
	DPRHRMAction.prototype = new TableAction();
	DPRHRMAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		if (is_numeric(data['kriteria'])) {
			data['kriteria'] = 1 * Number(data['kriteria']);
		}
		return data;
	};
	DPRHRMAction.prototype.clear = function() {
		$("#dprh_pasien_filter_list").empty();		
	};
	DPRHRMAction.prototype.delete = function(rnum) {
		$("#dprh_rm_" + rnum).remove();		
	};
	DPRHRMAction.prototype.selected = function(json) {
		$("tbody#dprh_pasien_filter_list").append(
			"<tr id='dprh_rm_" + dprh_rm_num + "'>" +
				"<td id='dprh_rm_" + dprh_rm_num + "_nrm'>" + json.id + "</td>" +
				"<td id='dprh_rm_" + dprh_rm_num + "_nama'>" + json.nama + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='dprh_rm.delete(" + dprh_rm_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		dprh_rm_num++;
	};
	
	var dprh;
	var dprh_rm;
	var dprh_rm_num;
	$(document).ready(function() {
		$('.mydate').datepicker();
		dprh_rm_num = 0;
		dprh_rm = new DPRHRMAction(
			"dprh_rm",
			"monitoring_validasi",
			"monitoring_pendataan_pasien_hd",
			new Array()
		);
		dprh_rm.setSuperCommand("dprh_rm");
		dprh = new DPRHAction(
			"dprh",
			"monitoring_validasi",
			"monitoring_pendataan_pasien_hd",
			new Array("nominal_kso", "persentase_ideplex")
		);
		$("#dprh_pagination").html("");
	});
</script>