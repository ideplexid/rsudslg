<?php
	global $db;
	$form = new Form("", "", "Monitoring dan Validasi : Data Pasien Daftar Non-IGD-HD Dobel");
	$from_date_text = new Text("dpnihd_from", "dpnihd_from", date("Y-m") . "-01");
	$from_date_text->setClass("mydate");
	$from_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Awal", $from_date_text);
	$to_date_text = new Text("dpnihd_to", "dpnihd_to", date("Y-m-d"));
	$to_date_text->setClass("mydate");
	$to_date_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addELement("Tanggal Akhir", $to_date_text);
	$nominal_kso_text = new Text("dpnihd_nominal_kso", "dpnihd_nominal_kso", "1000");
	$nominal_kso_text->setTypical("money");
	$nominal_kso_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. ' data-precision='2'");
	$form->addElement("Nominal KSO", $nominal_kso_text);
	$persentase_ideplex_text = new Text("dpnihd_persentase_ideplex", "dpnihd_persentase_ideplex", "77");
	$persentase_ideplex_text->setTypical("money");
	$persentase_ideplex_text->setAtribute("data-thousands='.' data-decimal=',' data-precision='2'");
	$form->addElement("Persen Ideplex", $persentase_ideplex_text);
	$form->addELement("Kecuali Pasien:", "");
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setAction("dpnihd.view()");
	$excel_button = new Button("", "", "Ekspor Berkas Excel");
	$excel_button->setIsButton(Button::$ICONIC);
	$excel_button->setClass("btn-inverse");
	$excel_button->setIcon("fa fa-download");
	$excel_button->setAction("dpnihd.export_xls()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addButton($show_button);
	$button_group->addButton($excel_button);
		
	class FilterPasienTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup('noprint');
			$btn = new Button($this->name . "_add", "", "Add");
			$btn->setAction("dpnihd_rm.chooser('dpnihd_rm', 'dpnihd_rm_button', 'dpnihd_rm', dpnihd_rm)");
			$btn->setClass("btn-primary");
			$btn->setIcon("fa fa-plus");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button($this->name . "_clear", "", "Clear");
			$btn->setAction("dpnihd_rm.clear()");
			$btn->setClass("btn-danger");
			$btn->setIcon("fa fa-trash");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$filter_pasien_table = new FilterPasienTable(
		array("NRM", "Pasien"),
		"",
		null,
		true
	);
	$filter_pasien_table->setName("dpnihd_pasien_filter");
	$filter_pasien_table->setFooterVisible(false);
	
	$table = new Table(
		array("No.", "Tanggal", "NRM", "Nama", "Alamat", "Keterangan", "KSO", "Hak Ideplex"),
		"",
		null,
		true
	);
	$table->setName("dpnihd");
	$table->setAction(false);
	
	//chooser rm:
	$rm_table = new Table(
		array("NRM", "Pasien"),
		"",
		null,
		true
	);
	$rm_table->setName("dpnihd_rm");
	$rm_table->setModel(Table::$SELECT);
	$rm_adapter = new SimpleAdapter();
	$rm_adapter->add("NRM", "id", "digit6");
	$rm_adapter->add("Pasien", "nama");
	$rm_dbtable = new DBTable($db, "smis_rg_patient");
	$rm_dbtable->setOrder(" id DESC ");
	$rm_dbresponder = new DBResponder(
		$rm_dbtable,
		$rm_table,
		$rm_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("dpnihd_rm", $rm_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_excel") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$filter_nrm = " a.nrm NOT IN (-1) ";
			if (isset($_POST['nrm_list'])) {
				$nrm = $_POST['nrm_list'];
				if (count($nrm) > 0) {
					$filter_nrm = " a.nrm NOT IN ('" . implode("','", $nrm) . "') ";
				}
			}
			$dbtable = new DBTable($db, "smis_rg_layananpasien");
			$rows = $dbtable->get_result("
				SELECT *
				FROM (
					SELECT a.tanggal, a.nrm AS 'nrm_pasien', b.alamat, b.nama AS 'nama_pasien', GROUP_CONCAT(
						CONCAT(a.id, ' - ', a.carabayar, ' - ', a.jenislayanan, ' - ', karcis) 
					SEPARATOR '; ') AS 'keterangan', COUNT(a.id) AS 'jumlah_daftar', " . $_POST['nominal_kso'] . " AS 'nominal_kso', " . ($_POST['persentase_ideplex'] * $_POST['nominal_kso'] / 100) . " AS 'hak_ideplex'
					FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
					WHERE a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND a.nrm IN (
						SELECT nrm
						FROM smis_rg_layananpasien
						WHERE prop NOT LIKE 'del' AND uri = '0' AND jenislayanan NOT LIKE 'instalasi_gawat_darurat' AND jenislayanan NOT LIKE 'hemodialisa' AND tanggal >= '" . $_POST['from'] . "' AND tanggal <= '" . $_POST['to'] . "'
					) AND " . $filter_nrm . "
					GROUP BY a.tanggal, a.nrm 
					HAVING jumlah_daftar > 1 
					ORDER BY a.tanggal, a.nrm ASC
				) v
			");
			$row_num = "1";
			$col_num = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Tanggal");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "NRM");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Nama");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Alamat");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Keterangan");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Jumlah Daftar");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "KSO");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "Hak Ideplex");
			$row_num++;
			foreach($rows as $r) {
				$col_num = "A";
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("date d-m-Y", $r->tanggal));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("only-digit6", $r->nrm_pasien));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->nama_pasien));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->alamat));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, ArrayAdapter::format("unslug", $r->keterangan));
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, $r->jumlah_daftar);
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, $r->nominal_kso);
				$col_num++;
				$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, $r->hak_ideplex);
				$row_num++;
			}
			$col_num = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "TOTAL");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "=SUM(G2:G" . ($row_num-1) . ")");
			$col_num++;
			$objPHPExcel->getActiveSheet()->setCellValue($col_num . $row_num, "=SUM(H2:H" . ($row_num-1) . ")");
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename='data_pasien_dobel_non_igd_hd_" . $_POST['from'] . "_" . $_POST['to'] . "_" . date("Ymd_His") . ".xlsx'");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		$adapter = new SummaryAdapter(true, "No.");
		$adapter->addFixValue("Keterangan", "<strong>Total</strong>");
		$adapter->addSummary("KSO", "nominal_kso", "money");
		$adapter->addSummary("Hak Ideplex", "hak_ideplex", "money");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("NRM", "nrm_pasien", "digit6");
		$adapter->add("Nama", "nama_pasien", "unslug");
		$adapter->add("Alamat", "alamat", "unslug");
		$adapter->add("Keterangan", "keterangan", "unslug");
		$adapter->add("KSO", "nominal_kso", "money");
		$adapter->add("Hak Ideplex", "hak_ideplex", "money");
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$numeric_filter = "";
			if (is_numeric($_POST['kriteria'])) {
				$numeric_filter = " OR nrm_pasien LIKE '%" . ($_POST['kriteria'] * 1) . "%'";
			}
			$filter = "WHERE (tanggal LIKE '%" . $_POST['kriteria'] . "%' OR nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR keterangan LIKE '%" . $_POST['kriteria'] . "%' " . $numeric_filter . ")";
		}
		$filter_nrm = " a.nrm NOT IN (-1) ";
		if (isset($_POST['nrm_list'])) {
			$nrm = $_POST['nrm_list'];
			if (count($nrm) > 0) {
				$filter_nrm = " a.nrm NOT IN ('" . implode("','", $nrm) . "') ";
			}
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT a.tanggal, a.nrm AS 'nrm_pasien', b.alamat, b.nama AS 'nama_pasien', GROUP_CONCAT(
					CONCAT(a.id, ' - ', a.carabayar, ' - ', a.jenislayanan, ' - ', karcis) 
				SEPARATOR '<br/>') AS 'keterangan', COUNT(a.id) AS 'jumlah_daftar', " . $_POST['nominal_kso'] . " AS 'nominal_kso', " . ($_POST['persentase_ideplex'] * $_POST['nominal_kso'] / 100) . " AS 'hak_ideplex'
				FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
				WHERE a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND a.nrm IN (
					SELECT nrm
					FROM smis_rg_layananpasien
					WHERE prop NOT LIKE 'del' AND uri = '0' AND jenislayanan NOT LIKE 'instalasi_gawat_darurat' AND jenislayanan NOT LIKE 'hemodialisa' AND tanggal >= '" . $_POST['from'] . "' AND tanggal <= '" . $_POST['to'] . "'
				) AND " . $filter_nrm . "
				GROUP BY a.tanggal, a.nrm 
				HAVING jumlah_daftar > 1 
				ORDER BY a.tanggal, a.nrm ASC
			) v
			" . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT a.tanggal, a.nrm AS 'nrm_pasien', b.alamat, b.nama AS 'nama_pasien', GROUP_CONCAT(
					CONCAT(a.id, ' - ', a.carabayar, ' - ', a.jenislayanan, ' - ', karcis) 
				SEPARATOR '<br/>') AS 'keterangan', COUNT(a.id) AS 'jumlah_daftar', " . $_POST['nominal_kso'] . " AS 'nominal_kso', " . ($_POST['persentase_ideplex'] * $_POST['nominal_kso'] / 100) . " AS 'hak_ideplex'
				FROM smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id
				WHERE a.tanggal >= '" . $_POST['from'] . "' AND a.tanggal <= '" . $_POST['to'] . "' AND a.nrm IN (
					SELECT nrm
					FROM smis_rg_layananpasien
					WHERE prop NOT LIKE 'del' AND uri = '0' AND jenislayanan NOT LIKE 'instalasi_gawat_darurat' AND jenislayanan NOT LIKE 'hemodialisa' AND tanggal >= '" . $_POST['from'] . "' AND tanggal <= '" . $_POST['to'] . "'
				) AND " . $filter_nrm . "
				GROUP BY a.tanggal, a.nrm 
				HAVING jumlah_daftar > 1 
				ORDER BY a.tanggal, a.nrm ASC
			) v
			" . $filter . "
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>" .
				 "<div class='span6'>" .
					 $filter_pasien_table->getHtml() .
				 "</div>" .
			 "</div>" .
			 "<div class='row-fluid'>" .
				 $button_group->getHtml() .
			 "</div>" .
		 "</div>";
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function DPNIHDAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPNIHDAction.prototype.constructor = DPNIHDAction;
	DPNIHDAction.prototype = new TableAction();
	DPNIHDAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		var rm_list = new Array();
		var nor_rm =  $("tbody#dpnihd_pasien_filter_list").children("tr").length;
		for (var i = 0; i < nor_rm; i++) {
			var rm_prefix = $("tbody#dpnihd_pasien_filter_list").children("tr").eq(i).prop("id");
			var nrm = $("#" + rm_prefix + "_nrm").text();
			rm_list.push(nrm);
		}
		data['nrm_list'] = rm_list;
		data['from'] = $("#dpnihd_from").val();
		data['to'] = $("#dpnihd_to").val();
		var v_nominal_kso = parseFloat($("#dpnihd_nominal_kso").val().replace(/[^0-9-,]/g, '').replace(",", "."));
		data['nominal_kso'] = v_nominal_kso;
		var v_persentase_ideplex = parseFloat($("#dpnihd_persentase_ideplex").val().replace(/[^0-9-,]/g, '').replace(",", "."));
		data['persentase_ideplex'] = v_persentase_ideplex;
		return data;
	};
	DPNIHDAction.prototype.export_xls = function() {
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "export_excel";
		postForm(data);
		dismissLoading();
	};

	function DPNIHDRMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPNIHDRMAction.prototype.constructor = DPNIHDRMAction;
	DPNIHDRMAction.prototype = new TableAction();
	DPNIHDRMAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		if (is_numeric(data['kriteria'])) {
			data['kriteria'] = 1 * Number(data['kriteria']);
		}
		return data;
	};
	DPNIHDRMAction.prototype.clear = function() {
		$("#dpnihd_pasien_filter_list").empty();		
	};
	DPNIHDRMAction.prototype.delete = function(rnum) {
		$("#dpnihd_rm_" + rnum).remove();		
	};
	DPNIHDRMAction.prototype.selected = function(json) {
		$("tbody#dpnihd_pasien_filter_list").append(
			"<tr id='dpnihd_rm_" + dpnihd_rm_num + "'>" +
				"<td id='dpnihd_rm_" + dpnihd_rm_num + "_nrm'>" + json.id + "</td>" +
				"<td id='dpnihd_rm_" + dpnihd_rm_num + "_nama'>" + json.nama + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='dpnihd_rm.delete(" + dpnihd_rm_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		dpnihd_rm_num++;
	};
	
	var dpnihd;
	var dpnihd_rm;
	var dpnihd_rm_num;
	$(document).ready(function() {
		$('.mydate').datepicker();
		dpnihd_rm_num = 0;
		dpnihd_rm = new DPNIHDRMAction(
			"dpnihd_rm",
			"monitoring_validasi",
			"monitoring_pendaftaran_pasien_dobel_non_igd_hd",
			new Array()
		);
		dpnihd_rm.setSuperCommand("dpnihd_rm");
		dpnihd = new DPNIHDAction(
			"dpnihd",
			"monitoring_validasi",
			"monitoring_pendaftaran_pasien_dobel_non_igd_hd",
			new Array("nominal_kso", "persentase_ideplex")
		);
		$("#dpnihd_pagination").html("");
	});
</script>