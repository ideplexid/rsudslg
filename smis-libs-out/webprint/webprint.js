function webprint(json){
	socket = new WebSocket('ws://localhost:9999/print');
	socket.onopen = function() {
		socket.send(JSON.stringify(json));
		socket.close();
	};		
	socket.onerror = function() {
		alert('Tidak bisa mengirim ke printer. Pastikan anda menjalankan aplikasi webprint.');
	}
}
