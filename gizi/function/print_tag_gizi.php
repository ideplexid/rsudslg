<?php 

function print_dummy_tag(){
    return "<div class='dogtag'></div>";
}

function print_tag_gizi($db,$px,$max,$waktu){
    $RESULT="<div class='dogtag'>";
    if(getSettings($db,"gizi-tag-noreg","1")=="1"){
        $RESULT.="<div id='dogtag_noreg'>
                    <div class='dogtag_title'>NORG</div>
                    <div class='dogtag_colon'>:</div>
                    <div class='dogtag_value'>".ArrayAdapter::format("only-digit10", $px->noreg_pasien)."</div>
                </div>";
    }

    if(getSettings($db,"gizi-tag-nrm","1")=="1"){
        $RESULT.="<div id='dogtag_nrm'>
                    <div class='dogtag_title'>NRM</div>
                    <div class='dogtag_colon'>:</div>
                    <div class='dogtag_value'>".ArrayAdapter::format("only-digit10", $px->nrm_pasien)."</div>
                </div>";
    }

    if(getSettings($db,"gizi-tag-nama","1")=="1"){
        $RESULT.="<div id='dogtag_nama'>
                    <div class='dogtag_title'>NAMA</div>
                    <div class='dogtag_colon'>:</div>
                    <div class='dogtag_value'>".substr(strtoupper($px->nama_pasien), 0,$max)."</div>
                </div>";
    }

    if(getSettings($db,"gizi-tag-ibu","1")=="1"){
        $RESULT.="<div id='dogtag_ibu'>
                    <div class='dogtag_title'>IBU</div>
                    <div class='dogtag_colon'>:</div>
                    <div class='dogtag_value'>".substr(strtoupper($px->ibu), 0,$max)."</div>
                </div>";
    }

    if(getSettings($db,"gizi-tag-ruangan","1")=="1"){
        $RESULT.="<div id='dogtag_ruangan'>
                    <div class='dogtag_title'>RUANG</div>
                    <div class='dogtag_colon'>:</div>
                    <div class='dogtag_value'>".ArrayAdapter::format("unslug", $px->nrm_ruang).($px->bed!=""?" / ".$px->bed:"")."</div>
                </div>";
    }

    if(getSettings($db,"gizi-tag-menu","1")=="1"){
        $menu=$px->menu_pagi;
        if($waktu=="siang" && $px->menu_siang!=="") $menu=$px->menu_siang;
        if($waktu=="malam" && $px->menu_malam!=="") $menu=$px->menu_malam;
            
        $RESULT.="<div id='dogtag_menu'>
                    <div class='dogtag_title'>MENU</div>
                    <div class='dogtag_colon'>:</div>
                    <div class='dogtag_value'>".substr($menu,0, $max)."</div>
                </div>";
    }
    $RESULT.="</div>";
    return $RESULT;
}

?>