<?php

global $wpdb;
require_once "gizi/update.php";

/*
	global $wpdb;
	
	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->extendInstall("gz");
	$install->install();
	
	require_once("gizi/resource/install/install_stok_min_maks_data.php");
	
	$query = "
		CREATE TABLE IF NOT EXISTS `smis_gz_menu` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `nama` varchar(64) NOT NULL,
		  `keterangan` text NOT NULL,
		  `waktu` varchar(10) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyIsam  ;
	";
	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_gz_dana` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `tanggal` date NOT NULL,
		  `no_bukti` varchar(32) NOT NULL,
		  `keterangan` varchar(64) NOT NULL,
		  `nilai` int(11) NOT NULL,
		  `markup` float NOT NULL,
		  `total` int(11) NOT NULL,
		  `dari` varchar(64) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyIsam;
	";
	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_gz_belanja` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `id_dana` int(11) NOT NULL,
		  `nama` varchar(32) NOT NULL,
		  `jumlah` varchar(32) NOT NULL,
		  `satuan` varchar(16) NOT NULL,
		  `harga_satuan` int(11) NOT NULL,
		  `biaya` int(11) NOT NULL,
		  `no_faktur` varchar(32) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
) ENGINE=MyIsam;
	";
	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_gz_asuhan` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `tanggal` date NOT NULL,
		  `nama_pasien` varchar(32) NOT NULL,
		  `noreg_pasien` int(11) NOT NULL,
		  `nrm_pasien` int(11) NOT NULL,
		  `id_pengasuh` varchar(32) NOT NULL,
		  `nama_pengasuh` varchar(32) NOT NULL,
		  `biaya` int(11) NOT NULL,
		  `keterangan` text NOT NULL,
          `ruangan` text NOT NULL,
		  `carabayar` varchar(16) NOT NULL,
		  `lunas` tinyint(1) NOT NULL,
		  `pulang` tinyint(1) NOT NULL,
          `synch` tinyint(1) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyIsam   ;
	";
	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_gz_diet` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `nama` varchar(64) NOT NULL,
		  `penyakit` text NOT NULL,
		  `keterangan` text NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyIsam  ;
	";
	$wpdb->query ( $query );

	$query = "
		INSERT INTO `smis_gz_diet` (`id`, `nama`, `penyakit`, `keterangan`, `prop`) VALUES
		(1, 'Diet DM 1100 Kal', 'Diabetes Melitus', '', ''),
		(2, 'Diet DM 1300  kal', 'Diabetes Melitus', '', ''),
		(3, 'Diet DM  1500 kal', 'Diabetes Melitus', '', ''),
		(4, 'Diet DM 1700  kal', 'Diabetes Melitus', '', ''),
		(5, 'Diet DM 1900  kal', 'Diabetes Melitus', '', ''),
		(6, 'Diet DM 2100  kal', 'Diabetes Melitus', '', ''),
		(7, 'Diet DM 2300  kal', 'Diabetes Melitus', '', ''),
		(8, 'Diet DM 2500  kal', 'Diabetes Melitus', '', ''),
		(9, 'Diet DM 2700  kal', 'DIabetes Melitus', '', ''),
		(10, 'Diet Jantung', 'Jantung Koroner, Cardiovaskular', '', ''),
		(11, 'Diet Pascabedah', 'Pasca Operasi', '', ''),
		(12, 'Diet Saluran Cerna', 'penyakit saluran cerna, usus, lambung, maag', '', ''),
		(13, 'Diet Rendah Garam', 'Hipertensi, Darah Tinggi', '', ''),
		(14, 'Diet Gout Artritis', 'rheumatik, asam urat, encok', '', ''),
		(15, 'Diet Tinggi Kalori Tinggi Protein (TKTP)', 'pasien melahirkan, pasien hamil ', '', '');
	";
	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_gz_pesanan` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `tanggal` date NOT NULL,
		  `code` varchar(32) NOT NULL COMMENT 'berasal dari tanggal noreg dll',
		  `nama_pasien` varchar(32) NOT NULL,
		  `nrm_pasien` int(11) NOT NULL,
		  `noreg_pasien` int(11) NOT NULL,
		  `ibu_kandung` varchar(32) NOT NULL,
		  `cara_keluar` varchar(32) NOT NULL,
		  `umur` varchar(64) NOT NULL,
		  `golongan_umur` varchar(64) NOT NULL,
		  `ruang` varchar(32) NOT NULL,
		  `menu_pagi` varchar(32) NOT NULL,
		  `diet_pagi` varchar(32) NOT NULL,
		  `pk_pagi` text NOT NULL,
		  `menu_siang` varchar(32) NOT NULL,
		  `diet_siang` varchar(32) NOT NULL,
		  `pk_siang` text NOT NULL,
		  `menu_malam` varchar(32) NOT NULL,
		  `diet_malam` varchar(32) NOT NULL,
		  `pk_malam` text NOT NULL,
		  `selesai` tinyint(1) NOT NULL,
		  `bed` varchar(32) NOT NULL,
          `cetak` tinyint(1) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `code` (`code`)
		) ENGINE=MyIsam  ;
	";
	$wpdb->query ( $query );
	
	$query="
	CREATE TABLE IF NOT EXISTS `smis_gz_bahan` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`kode` varchar(32) NOT NULL,
	`nama` varchar(64) NOT NULL,
	`keterangan` text NOT NULL,
	`prop` varchar(10) NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=MyIsam";
	$wpdb->query($query);
	
	$query="create or replace view smis_vgz_belanja as
	SELECT smis_gz_bahan.kode, 
	UPPER(smis_gz_belanja.nama) as nama,
	smis_gz_belanja.prop,
	smis_gz_dana.tanggal,
	smis_gz_belanja.biaya,
	smis_gz_dana.markup,
	smis_gz_dana.id as no_nota,
	smis_gz_dana.markup*smis_gz_belanja.biaya/100+smis_gz_belanja.biaya as total_biaya
	FROM smis_gz_belanja LEFT JOIN smis_gz_dana ON smis_gz_belanja.id_dana=smis_gz_dana.id
	LEFT JOIN smis_gz_bahan ON smis_gz_belanja.id_belanja=smis_gz_bahan.id
	WHERE smis_gz_dana.prop!='del' AND smis_gz_belanja.prop!='del';";
	$wpdb->query($query);
	
	$query="create  or replace view smis_vgz_laporan as  SELECT smis_vgz_belanja.nama, smis_vgz_belanja.kode,
	sum(smis_vgz_belanja.total_biaya) as total,smis_vgz_belanja.tanggal,'' as prop
	FROM `smis_vgz_belanja` GROUP BY smis_vgz_belanja.kode, smis_vgz_belanja.nama,  smis_vgz_belanja.tanggal";
	$wpdb->query($query);
*/

?>
