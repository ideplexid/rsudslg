<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
setChangeCookie ( false );
changeCookie ();
$settings = new SettingsBuilder ( $db, "gizi", "gizi", "settings", "Settings Gizi" );
$settings->setShowDescription ( true );
$settings->addTabs ( "settings", "Setting"," fa fa-cog" );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_kepala", "Kepala Asuhan Gizi", "", "text", "Kepala Asuhan Gizi" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_town", "Kota", "", "text", "Kota" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_biaya_asuhan", "Biaya Asuhan Gizi", "0", "money", "Biaya Konsultasi Asuhan Gizi" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_persen_rs", "Persen BHP & RS Asuhan Gizi", "0", "text", "Nilai Persentase BHP & RS" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_persen_jaspel", "Persen Jaspel Asuhan Gizi", "0", "text", "Nilai Persentase Jaspel" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_pagi_saja", "Tampilkan Pagi Saja", "0", "checkbox", "Tampilkan Pagi Saja" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_non_chooser", "Tampilan Non Chooser", "1", "checkbox", "Tampilkan Tanpa Chooser" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_non_pindah", "Pasien Pindah dan Tidak Datang tidak perlu dimasukan", "1", "checkbox", "Pasien Pindah dan Tidak Datang tidak perlu ditampilkan" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_autosynch_tagihan", "Auto Synch Tagihan langsung ke kasir", "0", "checkbox", "Lakukan Otomasi Auto Sinkronisasi Tagihan Pasien" ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_pesanan_penunggu_pasien", "Setting pesanan makanan untuk penunggu pasien", "0", "checkbox", "Jika ini dicentang, maka ketika menambah pesanan makanan untuk pasien akan muncul pesanan untuk penunggu pasien." ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis_gz_auto_pesanan_penunggu_pasien", "Auto pesan makan untuk penunggu pasien", "0", "checkbox", "Jika ini dicentang, maka ketika user input menu makan pasien otomatis akan muncul pesanan untuk penunggu pasien berdasarkan jadwal menu makanan yang ada." ) );
$settings->addItem ( "settings", new SettingsItem ( $db, "smis-gizi-active-tutup-tagihan", "Aktifkan Tutup Tagihan", "0", "checkbox", "" ) );

$css=".tag_paper{background:red; float:left; width:740px; page-break-after:always;   }
.dogtag{background:yellow; float:left; padding:1px; margin:1px; width:180px;}
.dogtag > div{float:left; font-size:10px; line-height:12px;}
.dogtag_title{float:left; width:52px; height:12px; overflow:hidden;}
.dogtag_value{float:left; width:100px; height:12px; overflow:hidden;}
.dogtag_colon{float:left; width:5px; height:12px; overflow:hidden;}
";
$js="window.print();";
$jsprint="
 jsPrintSetup.clearSilentPrint();
 jsPrintSetup.setOption('printSilent', 1);
 jsPrintSetup.setOption('orientation', jsPrintSetup.kPotraitOrientation);
 jsPrintSetup.definePaperSize(150, 150, 'GIZI_TAG_SMIS_PRINT', 'GIZI TAG LABEL', 'GIZI LABEL PAPER', 150, 210,jsPrintSetup.kPaperSizeMillimeters);
 jsPrintSetup.setPaperSizeData(150);
 jsPrintSetup.setPrinter('150');
 bootbox_choose_print();
";
$settings->addTabs ( "tag", "Tag"," fa fa-tag" );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-noreg","Tampilkan No.REG", "0", "checkbox", "ID #dogtag_noreg" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-nrm","Tampilkan NRM", "0", "checkbox", "ID #dogtag_nrm" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-nama","Tampilkan Nama", "0", "checkbox", "ID #dogtag_nama" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-ibu","Tampilkan Ibu", "0", "checkbox", "ID #dogtag_ibu" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-ruangan","Tampilkan Ruangan", "0", "checkbox", "ID #dogtag_ruangan" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-menu","Tampilkan Menu makanan", "0", "checkbox", "ID #dogtag_menu" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-max-per-paper","Jumlah Tag per Kertas", "10", "text", "jumlah tag pada satu kertas" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-char-per-value","Jumlah Huruf Pada Tag per Baris", "10", "text", "jumlah huruf pada setiap baris pada tag" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-css", "CSS Tag","", "textarea", "CSS Tag gunakan .tag_paper, .dogtag, .dogtag_title, .dogtag_colon, .dogtag_value example <pre>$css</pre>" ) );
$settings->addItem ( "tag", new SettingsItem ( $db, "gizi-tag-js","JS Tag", "", "textarea", "Javascript Tag, example <pre>$js</pre> or if using jsprint <pre>$jsprint</pre>" ) );


$settings->addTabs ( "label", "Label"," fa fa-tag" );
$option = new OptionBuilder();
$option->add("HTML", "html", "1");
$option->add("XLS", "xls");
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-jenis_luaran","Jenis Luaran", $option->getContent(), "select", "Jenis Luaran (Default : HTML)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-header_1","Header #1", "Rumah Sakit Umum", "text", "Header #1 Label (Default : Rumah Sakit Umum)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-header_2","Header #2", "Inovasi Ide Utama", "text", "Header #2 Label (Default : Inovasi Ide Utama)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-header_3","Header #3", "Jl. Sultan Agung No. 1", "text", "Header #3 Label (Default : Jl. Sultan Agung No. 1)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-header_4","Header #4", "Surabaya", "text", "Header #4 Label (Default : Surabaya)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-height","Tinggi (mm)", "60", "text", "Tinggi Label (Default : 60)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-width","Lebar (mm)", "70", "text", "Lebar Label (Default : 70)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-logo_height","Tinggi Logo (px)", "100", "text", "Tinggi Logo (Default : 100)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-logo_width","Lebar Logo (px)", "100", "text", "Lebar Logo (Default : 100)" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-keterangan_pagi","Ket. Label Menu Pagi", "Baik dimakan sebelum jam 08.00", "textarea", "Keterangan Label Menu Pagi" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-keterangan_siang","Ket. Label Menu Siang", "Baik dimakan sebelum jam 13.00", "textarea", "Keterangan Label Menu Siang" ) );
$settings->addItem ( "label", new SettingsItem ( $db, "gizi-label-keterangan_malam","Ket. Label Menu Malam", "Baik dimakan sebelum jam 19.00", "textarea", "Keterangan Label Menu Malam" ) );


$settings->addTabs ( "accounting", "Accounting"," fa fa-usd" );
$settings->addItem ( "accounting", new SettingsItem ( $db, "gizi-accounting-auto-notif", "Aktifkan Setting Auto Notif ke Accounting", "0", "checkbox", "Jika Dicentang Maka Sistem Akan Menotifikasi ke Accounting Secara Otomatis" ) );

$settings->addItem ( "accounting", new SettingsItem ( $db, "smis-rs-accounting-debet-asuhan-gizi", "Kode Accounting untuk Debet Asuhan Gizi", "", "chooser-settings-debet_asuhan_gizi-Debet", "Kode Accounting untuk Debet Asuhan Gizi" ) );
$settings->addItem ( "accounting", new SettingsItem ( $db, "smis-rs-accounting-kredit-asuhan-gizi", "Kode Accounting untuk kredit Asuhan Gizi", "", "chooser-settings-kredit_asuhan_gizi-Kredit", "Kode Accounting untuk Kredit Asuhan Gizi" ) );
$settings->addSuperCommandAction("debet_asuhan_gizi","kode_akun");
$settings->addSuperCommandAction("kredit_asuhan_gizi","kode_akun");
$settings->addSuperCommandArray("debet_asuhan_gizi","smis-rs-accounting-debet-asuhan-gizi","nomor",true);
$settings->addSuperCommandArray("kredit_asuhan_gizi","smis-rs-accounting-kredit-asuhan-gizi","nomor",true);

$response = $settings->init ();
?>