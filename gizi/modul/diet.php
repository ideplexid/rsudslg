<?php
$uitable = new Table ( array (
		'Nama',
		"Penyakit",
		'Keterangan' 
), "Jenis Diet", NULL, true );
$uitable->setName ( "diet" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Penyakit", "penyakit" );
	$dbtable = new DBTable ( $db, "smis_gz_diet" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "penyakit", "textarea", "Penyakit", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Menu Makanan" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "gizi/resource/js/diet.js",false );
?>

