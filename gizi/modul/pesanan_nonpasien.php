<?php

global $db;

if(isset($_POST['super_command'])) {
    $header=array ("Nama Menu", "Jenis", "Tarif", "Keterangan" );
    $ctable = new Table ( $header );
    $ctable->setName ( "menu" );
    $ctable->setModel ( Table::$SELECT );
    $cadapter = new SimpleAdapter ();
    $cadapter->add ( "Nama Menu", "nama" );
    $cadapter->add ( "Jenis", "jenis" );
    $cadapter->add ( "Tarif", "tarif_sekarang" );
    $cadapter->add ( "Keterangan", "keterangan" );
    $cdbtable = new DBTable($db, "smis_gz_menu");
    $cdbtable->addCustomKriteria("jenis", "= 'Non Pasien'");
    $cresponder = new DBResponder($cdbtable, $ctable, $cadapter);
    
    $super_command = new SuperCommand();
    $super_command->addResponder ( "menu", $cresponder );
    $init = $super_command->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

$uitable = new Table ( array (
		'No.',
		'Nama Pemesan',
        'Jenis Pemesan',
        'Menu',
		'Tarif',
        'Jumlah',
        'Total Tarif',
        'Status',
), "Pesanan Non Pasien", NULL, true );

$uitable->setName("pesanan_nonpasien");

if(isset($_POST['command'])) {
    $adapter = new SimpleAdapter ();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Nama Pemesan","nama_pemesan");
    $adapter->add("Jenis Pemesan","jenis_pemesan");
    $adapter->add("Waktu Pesan","waktu", "date d M Y H:i");
    $adapter->add("Menu","menu");
    $adapter->add("Tarif","tarif", "money Rp.");
    $adapter->add("Jumlah","jumlah");
    $adapter->add("Total Tarif","total_tarif", "money Rp.");
    $adapter->add("Status","status", "trivial_1_Lunas_Belum Lunas");
    
    $dbtable = new DBTable ( $db, "smis_gz_pesanan_nonpasien" );
	//$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	
    $dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$jenis = array (
		array ("name" => "Dokter",
				"value" => "Dokter"),
		array ("name" => "Tamu",
				"value" => "Tamu"),
		array ("name" => "Pengunjung",
				"value" => "Pengunjung") 
);

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "waktu", "datetime", "Waktu Pesan", date("Y-m-d H:i:s") );
$uitable->addModal ( "nama_pemesan", "text", "Nama Pemesan","" );
$uitable->addModal ( "jenis_pemesan", "select", "Nama Pemesan", $jenis );
$uitable->addModal ( "menu", "chooser-pesanan_nonpasien-menu-Menu Makanan", "Menu Makanan", "" );
$uitable->addModal ( "tarif", "money", "Tarif", "","y",NULL,true );
$uitable->addModal ( "jumlah", "text", "Jumlah Pesanan", "" );
$uitable->addModal ( "total_tarif", "money", "Total", "","y",NULL,true );
$uitable->addModal ( "status", "checkbox", "Lunas", "0" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Pesanan Non Pasien" );

echo $uitable->getHtml ();
echo $modal->getHtml ();

echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS("gizi/resource/js/pesanan_nonpasien.js",false);

?>