<?php
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
global $db;

$dktable = new Table ( array ('Nama','Jabatan',"NIP"), "", NULL, true );
$dktable->setName ( "pengasuh" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$pengasuh = new EmployeeResponder ( $db, $dktable, $dkadapter, "gizi" );

/* PASIEN */
$ptable = new Table ( array ('Nama','NRM',"No Reg"), "", NULL, true );
$ptable->setName ( "pasien" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "No Reg", "id" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );

$super = new SuperCommand ();
$super->addResponder ( "pasien", $presponder );
$super->addResponder ( "pengasuh", $pengasuh );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}
$_synchronous=getSettings($db,"smis_gz_autosynch_tagihan","0")=="1";
$uitable=null;
$header=array ('Tanggal','Pengasuh','Biaya','Keterangan','Ruangan');
if($_synchronous){
    $uitable = new TableSynchronous ( $header, "&nbsp;", NULL, true );
}else{
    $uitable = new Table ( $header, "&nbsp;", NULL, true );
}
$uitable->setName ( "asuhan" );
/* this is respond when system have to response */

if (isset ( $_POST ['command'] )) {

    //$dbtable = new DBTable ( $db, "smis_amb_ambulan" );
    require_once "gizi/class/dbtable/GiziDBTable.php";
    $dbtable = new GiziDBTable ( $db, "smis_gz_asuhan" );
    $dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");

    //$dbtable = new DBTable ( $db, "smis_gz_asuhan" );
    

    $dbtable->addCustomKriteria ( "noreg_pasien", " ='" . $_POST ['noreg_pasien'] . "'" );
    $adapter=null;
    $dbres=null;
    if($_synchronous){
        require_once "gizi/class/responder/AsuhanSynchronousResponder.php";
        require_once "gizi/class/adapter/AsuhanSynchronousAdapter.php";
        $sync_adapter=new AsuhanSynchronousAdapter();
        $dbtable->activateTableSynchronous($_synchronous);
        $adapter = new SynchronousViewAdapter ();
        $dbres = new AsuhanSynchronousResponder ( $dbtable, $uitable, $adapter,$sync_adapter );
    }else{
        require_once "gizi/class/responder/AsuhanResponder.php";
        $adapter = new SimpleAdapter ();
        $dbres = new AsuhanResponder ( $dbtable, $uitable, $adapter );
    }
    
    $adapter->add ( "Tanggal", "tanggal", "date d M Y" );
	$adapter->add ( "Pengasuh", "nama_pengasuh" );
	$adapter->add ( "Biaya", "biaya", "money Rp." );
	$adapter->add ( "Keterangan", "keterangan" );
    $adapter->add ( "Ruangan", "ruangan","unslug" );
	
    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}


$biaya = getSettings ( $db, "smis_gz_biaya_asuhan", "" );
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_pengasuh", "hidden", "", "" );
$uitable->addModal ( "tanggal", "date", "Tanggal", "" );
$uitable->addModal ( "biaya", "money", "Biaya", $biaya );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$uitable->addModal ( "nama_pengasuh", "chooser-asuhan-pengasuh", "Pengasuh", "" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Asuhan Gizi" );

$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
    foreach($ruang as $nama_ruang=>$jip){
        if($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="URJI"){
            $label_ruangan = $nama_ruang;
            $row = $db->get_row("
                SELECT nama
                FROM smis_adm_prototype
                WHERE slug = '" . $nama_ruang . "'
            ");
            if ($row != null)
                $label_ruangan = $row->nama;
            $option=array();
            $option['value']=$nama_ruang;
            $option['name']=$label_ruangan;
            $ruangan[]=$option;
        }
    }
}


$nrm = new Text ( "nrm_pasien", "nrm_pasien", "" );
$noreg = new Text ( "noreg_pasien", "noreg_pasien", "" );
$nama = new Text ( "nama_pasien", "nama_pasien", "" );
$ruangan = new Select ( "ruangan", "ruangan", $ruangan );
$action = new Button ( "", "", "Select" );
$action->setAction ( "asuhan.chooser('asuhan','nama_pasien','pasien',pasien)" );
$nrm->setDisabled ( true );
$noreg->setDisabled ( true );
$nama->setDisabled ( true );




// form for proyek
$form = new Form ( "form_pasien", "", "Asuhan" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "No Registrasi", $noreg );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Ruangan", $ruangan );
$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS("gizi/resource/js/asuhan.js",false);
?>