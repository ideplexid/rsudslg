<?php
global $db;
$head=array ('Kode','Nama','Keterangan');
$uitable = new Table ( $head, "Bahan", NULL, true );
$uitable->setName ( "bahan" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Kode", "kode" );
	$adapter->add ( "Keterangan", "keterangan" );
	$dbtable = new DBTable ( $db, "smis_gz_bahan" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "","n",NULL,false,NULL,true,"kode" );
$uitable->addModal ( "kode", "text", "Kode", "","y",NULL,false,NULL,false,"keterangan" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "","y",NULL,false,NULL,false,"save" );
$modal = $uitable->getModal ();
$modal->setTitle ( "bahan" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "gizi/resource/js/bahan.js",false );
?>
