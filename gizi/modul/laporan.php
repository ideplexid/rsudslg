<?php 
$tab = new Tabulator ( "laporan", "" );
$tab->add ( "laporan_menu_makanan", "Laporan Menu Makanan", "", Tabulator::$TYPE_HTML,"fa fa-ambulance" );
$tab->add ( "laporan_per_menu_makanan", "Laporan Per Menu Makanan", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->add ( "laporan_diet", "Laporan Diet", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "laporan_per_diet", "Laporan per Diet", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "laporan_asuhan", "Laporan Asuhan", "", Tabulator::$TYPE_HTML,"fa fa-money" );
$tab->setPartialLoad(true,"gizi","laporan","laporan",true);
echo $tab->getHtml ();
?>