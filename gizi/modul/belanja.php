<?php
global $db;
require_once 'gizi/class/table/RincianBelanjaTable.php';
$phead=array ('Nama','Kode',"Keterangan");
$ptable = new Table($phead, "", NULL, true);
$ptable->setName("bahan");
$ptable->setModel(Table::$SELECT);
$padapter = new SimpleAdapter ();
$padapter->add("Nama", "nama");
$padapter->add("Kode", "kode");
$padapter->add("Keterangan", "keterangan");
$pdbtable=new DBTable($db, "smis_gz_bahan");
$presponder = new DBResponder($pdbtable, $ptable, $padapter);

$super = new SuperCommand ();
$super->addResponder("bahan", $presponder);
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}

require_once ("smis-framework/smis/database/DBParentChildResponder.php");
$header=array("Tanggal","No Bukti","Dari","Nilai","Markup","Total","Keterangan");
$parent_uitable = new RincianBelanjaTable ( $header, "Dana", NULL, true );
$parent_uitable->setName ( "belanja_parent" );
$parent_uitable->setActionName ( "belanja.parent" );
$parent_uitable->setPrintButtonEnable(false);
$parent_uitable->setPrintElementButtonEnable(true);
$parent_uitable->setReloadButtonEnable(false);

$chead=array('Nama',"Satuan",'Jumlah',"Harga Satuan",'Biaya','No Faktur');
$child_uitable = new Table ($chead, "", NULL, true );
$child_uitable->setName ( "belanja_child" );
$child_uitable->setActionName ( "belanja.child" );
$child_uitable->setPrintButtonEnable(false);
$child_uitable->setReloadButtonEnable(false);
$child_uitable->setFooterControlVisible(false);

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	$parent_adapter = new SimpleAdapter ();
	$parent_adapter->add ( "Tanggal", "tanggal", "date d M Y" );
	$parent_adapter->add ( "No Bukti", "no_bukti" );
	$parent_adapter->add ( "Keterangan", "keterangan" );
	$parent_adapter->add ( "Dari", "dari" );
	$parent_adapter->add ( "Nilai", "nilai", "money Rp." );
	$parent_adapter->add ( "Markup", "markup","back%" );
	$parent_adapter->add ( "Total", "total","money Rp." );
	$parent_dbtable = new DBTable ( $db, "smis_gz_dana" );
	$parent = new DBParentResponder ( $parent_dbtable, $parent_uitable, $parent_adapter );
	$child_adapter = new SummaryAdapter();
	$child_adapter->addFixValue("Nama", "<strong>Total</strong>");
	$child_adapter->addSummary("Biaya", "biaya","money Rp.");
	$child_adapter->add ( "Satuan", "satuan" );
	$child_adapter->add ( "Nama", "nama" );
	$child_adapter->add ( "Jumlah", "jumlah" );
	$child_adapter->add ( "Harga Satuan", "harga_satuan","money Rp." );	
	$child_adapter->add ( "Biaya", "biaya", "money Rp." );
	$child_adapter->add ( "No Faktur", "no_faktur" );
    
	$child_dbtable = new DBTable ( $db, "smis_gz_belanja" );
	$child_dbtable->setShowAll(true);
	$child = new DBChildResponder ( $child_dbtable, $child_uitable, $child_adapter, "id_dana" );
	$dbres = new DBParentChildResponder ( $parent, $child );
	if($dbres->isParentProcess($_POST ['super_command']) && $parent->isSave()){
		$id=$_POST['id'];
		$query="SELECT SUM(biaya) as total FROM smis_gz_belanja WHERE id_dana='".$id."' AND prop!='del' ";
		$nilai=$db->get_var($query);
		$markup=$_POST['markup'];
		$total=$nilai*(1+$markup/100);
		$parent->addColumnFixValue("nilai", $nilai);
		$parent->addColumnFixValue("total", $total);
	}
	$data = $dbres->command ( $_POST ['super_command'], $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$non=getSettings($db, "smis_gz_non_chooser", "1")=="1";
$child_uitable->addModal ( "id", "hidden", "", "","y",NULL,false,NULL,false,"id_dana" );
$child_uitable->addModal ( "id_dana", "hidden", "", "","y",NULL,false,NULL,false,"nama" );
$child_uitable->addModal ( "id_bahan", "hidden", "", "","y",NULL,false,NULL,false,"nama" );
$child_uitable->addModal ( "nama", ($non?"text":"chooser-belanja.child-bahan"), "Nama", "","y",NULL,false,NULL,true,"satuan" );
$child_uitable->addModal ( "satuan", "text", "Satuan", "","y",NULL,false,NULL,false,"jumlah" );
$child_uitable->addModal ( "jumlah", "text", "Jumlah", "","y",NULL,false,NULL,false,"harga_satuan" );
$child_uitable->addModal ( "harga_satuan", "money", "Harga Satuan", "","y",NULL,false,NULL,false,"biaya" );
$child_uitable->addModal ( "biaya", "money", "Biaya", "","y",NULL,false,NULL,false,"no_faktur" );
$child_uitable->addModal ( "no_faktur", "text", "No Faktur", "","y",NULL,false,NULL,false,"save" );
$child_modal = $child_uitable->getModal ();
$child_modal->setTitle ( "Belanja" );

// parent
$parent_uitable->addModal ( "id", "hidden", "", "" );
$parent_uitable->addModal ( "tanggal", "date", "Tanggal", "" );
$parent_uitable->addModal ( "no_bukti", "text", "No Bukti", "" );
$parent_uitable->addModal ( "keterangan", "text", "Keterangan", "" );
$parent_uitable->addModal ( "dari", "text", "Dari", "" );
$parent_uitable->addModal ( "markup", "text", "Markup (%)", "" );
$parent_modal = $parent_uitable->getModal ();
$parent_modal->setTitle ( "Dana" );
$parent_modal->addBody ( "dana", $child_uitable );
$parent_modal->setClass ( Modal::$FULL_MODEL );

echo $parent_uitable->getHtml ();
echo $parent_modal->getHtml ();
echo $child_modal->getHtml ();

echo addJS  ( "framework/smis/js/table_action.js" );
echo addJS  ( "framework/smis/js/child_parent.js" );
echo addJS  ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS  ( "gizi/resource/js/belanja.js",false );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addCSS ( "gizi/resource/css/belanja.css",false );
?>
