<?php
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
global $db;


if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    $super = new SuperCommand ();
    if($_POST['super_command']=="jenis_diet"){
        $dktable = new Table ( array ('No.','Nama',"Penyakit","Keterangan"), "", NULL, true );
        $dbtable = new DBTable($db,"smis_gz_diet");
        $dktable->setName ( "jenis_diet" );
        $dktable->setModel ( Table::$SELECT );
        $dkadapter = new SimpleAdapter ();
        $dkadapter->setUseNumber(true,"No.","back.");
        $dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "Penyakit", "penyakit" );
        $dkadapter->add ( "keterangan", "keterangan" );
        $jenis_diet = new DBResponder ( $dbtable, $dktable, $dkadapter );
        $super->addResponder ( "jenis_diet", $jenis_diet );    
    }else if($_POST['super_command']=="diagnosa_gizi"){
        $dktable = new Table ( array ('No.','Nama',"ICD"), "", NULL, true );
        $dbtable = new DBTable($db,"smis_gz_icd");
        $dktable->setName ( "diagnosa_gizi" );
        $dktable->setModel ( Table::$SELECT );
        $dkadapter = new SimpleAdapter ();
        $dkadapter->setUseNumber(true,"No.","back.");
        $dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "ICD", "icd" );
        $diagnosa_gizi = new DBResponder ( $dbtable, $dktable, $dkadapter );
        $super->addResponder ( "diagnosa_gizi", $diagnosa_gizi );
    }else if($_POST['super_command']=="pengasuh"){
        $dktable = new Table ( array ('Nama','Jabatan',"NIP"), "", NULL, true );
        $dktable->setName ( "pengasuh" );
        $dktable->setModel ( Table::$SELECT );
        $dkadapter = new SimpleAdapter ();
        $dkadapter->add ( "Jabatan", "nama_jabatan" );
        $dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "NIP", "nip" );
        $pengasuh = new EmployeeResponder ( $db, $dktable, $dkadapter, "gizi" );
        $super->addResponder ( "pengasuh", $pengasuh );
    }else if($_POST['super_command']=="dokter"){
        $dktable = new Table ( array ('Nama','Jabatan',"NIP"), "", NULL, true );
        $dktable->setName ( "dokter" );
        $dktable->setModel ( Table::$SELECT );
        $dkadapter = new SimpleAdapter ();
        $dkadapter->add ( "Jabatan", "nama_jabatan" );
        $dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "NIP", "nip" );
        $dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );
        $super->addResponder ( "dokter", $dokter );
    }else if($_POST['super_command']=="pasien"){
        $ptable = new Table ( array ('Nama','NRM',"No Reg"), "", NULL, true );
        $ptable->setName ( "pasien" );
        $ptable->setModel ( Table::$SELECT );
        $padapter = new SimpleAdapter ();
        $padapter->add ( "Nama", "nama_pasien" );
        $padapter->add ( "NRM", "nrm", "digit8" );
        $padapter->add ( "No Reg", "id" );
        $presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );
        $super->addResponder ( "pasien", $presponder );
    }else if($_POST['super_command']=="biaya"){
        $ptable = new Table ( array ('Jenis Tindakan','Kelas',"Biaya"), "", NULL, true );
        $ptable->setName ( "biaya" );
        $ptable->setModel ( Table::$SELECT );
        $padapter = new SimpleAdapter ();
        $padapter->add ( "Jenis Tindakan", "jenis_tindakan" );
        $padapter->add ( "Kelas", "kelas", "unslug" );
        $padapter->add ( "Biaya", "biaya","money Rp." );
        $biaya = new ServiceResponder ( $db, $ptable, $padapter, "get_konsul_gizi" );
        $super->addResponder ( "biaya", $biaya );
    }
    
    $init = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}



$uitable=null;
$header=array ('Tanggal',"Dokter",'Ahli Gizi',"Kode Diagnosa Gizi","Diagnosa Gizi",
"Pasien","Jenis Kelamin","Tanggal Lahir","Status Gizi","Jenis Diet",
"Antropometri","Konsul Gizi","Kelas",'Biaya','Keterangan','Hasil Skrining','Sisa Makanan','Ruangan');
$uitable = new Table ( $header, "&nbsp;", NULL, true );

$uitable->setName ( "konsul_gizi" );
/* this is respond when system have to response */

if (isset ( $_POST ['command'] )) {

    //$dbtable = new DBTable ( $db, "smis_amb_ambulan" );
    require_once "gizi/class/dbtable/KonsulGiziDBTable.php";
    $dbtable = new KonsulGiziDBTable ( $db, "smis_gz_konsul" );
    $dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");

    $dbtable->addCustomKriteria ( "noreg_pasien", " ='" . $_POST ['noreg_pasien'] . "'" );
    $adapter=null;
    $dbres=null;
    
    require_once "gizi/class/responder/KonsulGiziResponder.php";
    $adapter = new SimpleAdapter ();
    $dbres = new KonsulGiziResponder ( $dbtable, $uitable, $adapter );
    
    $adapter->add ( "Tanggal", "tanggal", "date d M Y" );
	$adapter->add ( "Kode Diagnosa Gizi", "kode_diagnosa_gizi" );
    $adapter->add ( "Diagnosa Gizi", "diagnosa_gizi" );
    $adapter->add ( "Diagnosa Gizi Manual", "diagnosa_gizi_manual" );
    $adapter->add ( "Pasien", "nama_pasien" );
    $adapter->add ( "Jenis Kelamin", "jk","trivial_1_Perempuan_Laki-Laki" );
	$adapter->add ( "Dokter", "nama_dokter" );
	$adapter->add ( "Ahli Gizi", "nama_pengasuh" );
	$adapter->add ( "Biaya", "biaya_konsul_gizi", "money Rp." );
    $adapter->add ( "Keterangan", "keterangan" );
    $adapter->add ( "Hasil Skrining", "hasil_skrining" );
    $adapter->add ( "Sisa Makanan", "sisa_makanan" );
    $adapter->add ( "Ruangan", "ruangan","unslug" );
    $adapter->add ( "Tanggal Lahir", "tanggal_lahir_pasien","date d M Y" );
    $adapter->add ( "Status Gizi", "status_gizi" );
    $adapter->add ( "Antropometri", "antropometri" );
    $adapter->add ( "Konsul Gizi", "konsul_gizi" );
    $adapter->add ( "Kelas", "kelas","unslug" );
    $adapter->add ( "Jenis Diet", "jenis_diet" );
    
    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}


$service = new ServiceConsumer ( $this->db, "get_kelas" );
$service->setCached(true,"get_kelas");
$service->execute ();
$kelasx = $service->getContent ();
$option_kelas = new OptionBuilder ();
foreach ( $kelasx as $k ) {
    $nama = $k ['nama'];
    $slug = $k ['slug'];
    $option_kelas->add ( $nama, $slug, $slug == $this->kelas ? "1" : "0" );
}
$kelas = $option_kelas->getContent();



$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "tanggal", "date", "Tanggal", "" );
$uitable->addModal ( "id_dokter", "hidden", "", "" );
$uitable->addModal ( "nama_dokter", "chooser-konsul_gizi-dokter-Dokter", "Dokter", "" );
$uitable->addModal ( "id_pengasuh", "hidden", "", "" );
$uitable->addModal ( "nama_pengasuh", "chooser-konsul_gizi-pengasuh-Ahli Gizi", "Ahli Gizi", "" );
$uitable->addModal ( "kode_diagnosa_gizi", "chooser-konsul_gizi-diagnosa_gizi-Kode Diagnosa Gizi", "kode Diagnosa Gizi", "" );
$uitable->addModal ( "diagnosa_gizi", "text", "Diagnosa Gizi", "","y",null,true );
$uitable->addModal ( "diagnosa_gizi_manual", "text", "Diagnosa Gizi Manual", "","y" );
$uitable->addModal ( "status_gizi", "text", "Status Gizi", "","y" );

$uitable->addModal ( "antropometri", "text", "Antropometri", "","y" );
$uitable->addModal ( "jenis_diet", "chooser-konsul_gizi-jenis_diet-Jenis Diet", "Jenis Diet", "","y" );
$uitable->addModal ( "konsul_gizi", "chooser-konsul_gizi-biaya-Konsul Gizi", "Konsul", "","y" );
$uitable->addModal ( "kelas_konsul_gizi", "select", "Kelas", $kelas );
$uitable->addModal ( "biaya_konsul_gizi", "money", "Biaya", "0");
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );

$uitable->addModal ( "hasil_skrining", "textarea", "Hasil Skrining", "" );
$uitable->addModal ( "sisa_makanan", "textarea", "Sisa Makanan", "" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Konsul Gizi" );



$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->setCached(true,"get_urjip");
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
    foreach($ruang as $nama_ruang=>$jip){
        
            $label_ruangan = $nama_ruang;
            $row = $db->get_row("
                SELECT nama
                FROM smis_adm_prototype
                WHERE slug = '" . $nama_ruang . "'
            ");
            if ($row != null)
                $label_ruangan = $row->nama;
            $option=array();
            $option['value']=$nama_ruang;
            $option['name']=$label_ruangan;
            $ruangan[]=$option;
        
    }
}

$jk = new OptionBuilder();
$jk ->add("Laki-Laki","0");
$jk ->add("Perempuan","1");


$nrm = new Text ( "nrm_pasien", "nrm_pasien", "" );
$noreg = new Text ( "noreg_pasien", "noreg_pasien", "" );
$nama = new Text ( "nama_pasien", "nama_pasien", "" );
$tanggal = new Text ( "tanggal_lahir_pasien", "tanggal_lahir_pasien", "" );
$ruangan = new Select ( "ruangan", "ruangan", $ruangan );

$usia = new Text ( "usia", "usia", "" );
$jenis_pasien = new Select( "jenis_pasien", "jenis_pasien", $jenis_pembayaran );
$jenis_kelamin = new Select ( "jenis_kelamin", "jenis_kelamin", $jk->getContent() );


$action = new Button ( "", "", "Select" );
$action->setAction ( "konsul_gizi.chooser('konsul_gizi','nama_pasien','pasien',pasien)" );
$nrm->setDisabled ( true );
$noreg->setDisabled ( true );
$nama->setDisabled ( true );
$tanggal->setDisabled(true);
$ruangan->setDisabled(true);
$usia ->setDisabled(true);
$jenis_pasien ->setDisabled(true);
$jenis_kelamin ->setDisabled(true);


// form for proyek
$form = new Form ( "form_pasien", "", "Konsul Gizi" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "No Registrasi", $noreg );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Usia", $usia );
$form->addElement ( "Jenis Pasien", $jenis_pasien );
$form->addElement ( "Jenis Kelamin", $jenis_kelamin );
$form->addElement ( "Tanggal Lahir", $tanggal );
$form->addElement ( "Ruangan", $ruangan );
$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS("gizi/resource/js/konsul_gizi.js",false);
?>