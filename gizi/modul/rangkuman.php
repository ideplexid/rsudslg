<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
global $db;


$button=new Button("", "", "");
$button->setAction("rangkuman.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak=new Button("", "", "");
$cetak->setAction("rangkuman.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$fakultas=new MasterSlaveTemplate($db, "smis_vgz_laporan", "gizi", "rangkuman");
$fakultas->getDBtable()->setOrder(" tanggal ASC, nama ASC");

if(isset($_POST['dari']) && $_POST['dari']!="" && isset($_POST['sampai']) && $_POST['sampai']!=""){
	$fakultas	->getDBtable()
			->setShowAll(true)
			->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
			->addCustomKriteria(NULL, "tanggal<='".$_POST['sampai']."'");
}

$uitable=$fakultas->getUItable();
$header=array("No.","Tanggal","Nama","Total");
$uitable->setHeader($header);

$adapter=new SummaryAdapter();
$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
$adapter->addSummary("Total", "total","money Rp.");
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y")
		->add("Nama", "nama")
		->add("Total", "total","money Rp.");
$fakultas->setAdapter($adapter);
$fakultas->addFlag("dari", "Tanggal Awal", "Silakan Masukan Tanggal Mulai")
->addFlag("sampai", "Tanggal Akhir", "Silakan Masukan Tanggal Akhir")
->addNoClear("dari")
->addNoClear("sampai")
->setDateEnable(true)
->getUItable()
->setActionEnable(false)
->setFooterVisible(false)
->addModal("dari", "date", "Awal", "")
->addModal("sampai", "date", "Akhir", "");
$fakultas->getForm()->addElement("", $button)->addElement("", $cetak);

$fakultas ->addViewData("dari", "dari")
		->addViewData("sampai", "sampai");
$fakultas->initialize();
?>