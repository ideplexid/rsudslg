<?php 
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-libs-class/MasterSlaveTemplate.php";
require_once "gizi/class/responder/LaporanKonsulGiziResponder.php";

$header=array ('Tanggal',"Dokter",'Ahli Gizi',"Kode Diagnosa Gizi","Diagnosa Gizi",
"Pasien","Jenis Kelamin","Tanggal Lahir","Status Gizi","Jenis Diet",
"Antropometri","Konsul Gizi","Kelas",'Biaya','Keterangan','Ruangan');

$m = new MasterSlaveTemplate($db,"smis_gz_konsul","gizi","laporan_konsul_gizi");
$responder = new LaporanKonsulGiziResponder($m->getDBtable(),$m->getUItable(),$m->getAdapter());
$m ->setDBresponder($responder);
$uitable = $m->getUItable();
$adapter = $m->getAdapter();
$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
$adapter->add ( "Kode Diagnosa Gizi", "kode_diagnosa_gizi" );
$adapter->add ( "Diagnosa Gizi", "diagnosa_gizi" );
$adapter->add ( "Diagnosa Gizi Manual", "diagnosa_gizi_manual" );
$adapter->add ( "Pasien", "nama_pasien" );
$adapter->add ( "Jenis Kelamin", "jk","trivial_1_Perempuan_Laki-Laki" );
$adapter->add ( "Dokter", "nama_dokter" );
$adapter->add ( "Ahli Gizi", "nama_pengasuh" );
$adapter->add ( "Biaya", "biaya_konsul_gizi", "money Rp." );
$adapter->add ( "Keterangan", "keterangan" );
$adapter->add ( "Ruangan", "ruangan","unslug" );
$adapter->add ( "Tanggal Lahir", "tanggal_lahir_pasien","date d M Y" );
$adapter->add ( "Status Gizi", "status_gizi" );
$adapter->add ( "Antropometri", "antropometri" );
$adapter->add ( "Konsul Gizi", "konsul_gizi" );
$adapter->add ( "Kelas", "kelas","unslug" );
$adapter->add ( "Jenis Diet", "jenis_diet" );

$uitable ->addModal("dari","date","Dari","");
$uitable ->addModal("sampai","date","Sampai","");
$uitable ->setHeader($header);
$uitable ->setAction(false);
$form = $m ->getForm();
$form ->setTitle("Laporan Konsul Gizi");

$proses = new Button("","","Proses");
$proses ->setIcon("fa fa-refresh");
$proses ->setIsButton(Button::$ICONIC_TEXT);
$proses ->setClass("btn btn-primary");
$proses ->setAction("laporan_konsul_gizi.view()");

$excel = new Button("","","Excel");
$excel ->setIcon("fa fa-file-excel-o");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setClass("btn btn-primary");
$excel ->setAction("laporan_konsul_gizi.excel()");

$form->addElement("",$proses);
$form->addElement("",$excel);

if($m->getDBResponder()->isView() || $m->getDBResponder()->isExcel()){
    if(isset($_POST['dari']) && $_POST['dari']!=""){
        $m->getDBtable()->addCustomKriteria(" tanggal >= "," '".$_POST['dari']."' "); 
    }

    if(isset($_POST['sampai']) && $_POST['sampai']!=""){
        $m->getDBtable()->addCustomKriteria(" tanggal <= "," '".$_POST['sampai']."' "); 
    }

    if($m->getDBResponder()->isExcel()){
        $adapter->add ( "Biaya", "biaya_konsul_gizi" );
    }
}

$m ->setDateEnable(true);
$m ->addRegulerData("dari","dari","id-value");
$m ->addRegulerData("sampai","sampai","id-value");
$m ->initialize();


