<?php
	global $NAVIGATOR;

	// Administrator Top Menu
	$mr = new Menu ("fa fa-cutlery");
	$mr->addProperty ( 'title', 'Gizi' );
	$mr->addProperty ( 'name', 'Gizi' );

	$mr->addSubMenu ( "Asuhan Gizi", "gizi", "asuhan", "Asuhan Gizi Pasien" ,"fa fa-spoon");
	$mr->addSubMenu ( "Konsul Gizi", "gizi", "konsul_gizi", "Konsul Gizi Pasien" ,"fa fa-spoon");
	$mr->addSubMenu ( "Tambahan Biaya Gizi", "gizi", "tambahan_biaya", "Tambahan Biaya Gizi", "fa fa-plus");
	$mr->addSubMenu ( "Rekap Pesanan Pasien", "gizi", "pesanan", "Rekap Pesanan Pasien" ,"fa fa-birthday-cake");
	$mr->addSubMenu ( "Pesanan Pasien", "gizi", "pesanan_pasien", "Pesanan Pasien" ,"fa fa-cutlery");
	$mr->addSubMenu ( "Pesanan Non Pasien", "gizi", "pesanan_nonpasien", "Pesanan Non Pasien" ,"fa fa-cutlery");
	$mr->addSubMenu ( "Data Induk", "gizi", "data_induk", "Data Induk" ,"fa fa-database");

	$mr->addSubMenu ( "Laporan Konsul Gizi", "gizi", "laporan_konsul_gizi", "Laporan Konsul Gizi" ,"fa fa-user-md");
	$mr->addSubMenu ( "Laporan Pesanan Makanan", "gizi", "laporan_pesanan_makanan", "Laporan Pesanan Pasien" ,"fa fa-book");

	$mr->addSeparator ();
	
	$mr->addSubMenu ( "Belanja Bahan", "gizi", "belanja", "Belanja Bahan Gizi" ,"fa fa-shopping-cart");
	$mr->addSubMenu ( "Jenis Diet", "gizi", "diet", "Jenis-Jenis Diet Makanan" ,"fa fa-pie-chart");
	//$mr->addSubMenu ( "Master Bahan", "gizi", "bahan", "Bahan Makanan" ,"fa fa-money");
    $mr->addSubMenu ( "Laporan", "gizi", "laporan", "Laporan Gizi" ,"fa fa-bar-chart");
	$mr->addSubMenu ( "Laporan Asuhan", "gizi", "laporan_asuhan", "Laporan Gizi" ,"fa fa-bar-chart");
	$mr->addSubMenu ( "Laporan Tambahan Biaya", "gizi", "laporan_tambahan_biaya", "Laporan Tambahan Biaya", "fa fa-bar-chart");
	$mr->addSeparator ();
	$mr->addSubMenu ( "Settings", "gizi", "settings", "Settings Gizi" ,"fa fa-cog");
	$mr->addSubMenu ( "Rekap Belanja", "gizi", "rekap", "Rekapitulasi Belanja" ,"fa fa-book");
	$mr->addSubMenu ( "Rangkuman Belanja", "gizi", "rangkuman", "Rangkuman Belanja Gizi" ,"fa fa-file");
	$mr->addSubMenu ( "Rangkuman Bulanan", "gizi", "rangkuman_bulanan", "Rangkuman Bulanan Gizi" ,"fa fa-file-o");
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Gizi", "gizi");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'gizi' );
?>
