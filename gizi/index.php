<?php

/*
 * 
 * Dokumentasi modul
 * Author: Nurul Huda
 * Editor: Dwi Al Aji Suseno
 * Keterangan: pada file ini digunakan untuk pemberian hak akses file yang akan diakses dari menu pada file navigation.php
 * 
 * date: 14 Desember 2016
 */
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("gizi", $user,"modul/");
	
	$policy=new Policy("gizi", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("asuhan","asuhan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/asuhan");
	$policy->addPolicy("konsul_gizi","konsul_gizi",Policy::$DEFAULT_COOKIE_CHANGE,"modul/konsul_gizi");
	$policy->addPolicy("rekap","rekap",Policy::$DEFAULT_COOKIE_CHANGE,"modul/rekap");
    $policy->addPolicy("laporan","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
    $policy->addPolicy("laporan_menu_makanan","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_menu_makanan");
    $policy->addPolicy("laporan_per_menu_makanan","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_per_menu_makanan");
    $policy->addPolicy("laporan_diet","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_diet");
    $policy->addPolicy("laporan_per_diet","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_per_diet");
	$policy->addPolicy("laporan_asuhan","laporan",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan//laporan_asuhan");
	$policy->addPolicy("rangkuman","rangkuman",Policy::$DEFAULT_COOKIE_CHANGE,"modul/rangkuman");
	$policy->addPolicy("rangkuman_bulanan","rangkuman_bulanan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/rangkuman_bulanan");
	$policy->addPolicy("belanja","belanja",Policy::$DEFAULT_COOKIE_CHANGE,"modul/belanja");
	$policy->addPolicy("bahan","bahan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/bahan");
	$policy->addPolicy("diet","diet",Policy::$DEFAULT_COOKIE_CHANGE,"modul/diet");
	$policy->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/setting");
	
    $policy->addPolicy("data_induk","data_induk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk");
    $policy->addPolicy("menu_makanan","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/menu_makanan");
    $policy->addPolicy("group_menu","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/group_menu");
    $policy->addPolicy("jadwal","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/jadwal");
	$policy->addPolicy("diagnosa_gizi","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/diagnosa_gizi");
	$policy->addPolicy("jenis_menu","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/jenis_menu");

	
	$policy->addPolicy("laporan_pesanan_makanan","laporan_pesanan_makanan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_pesanan_makanan");
	$policy->addPolicy("laporan_tambahan_biaya", "laporan_tambahan_biaya", Policy::$DEFAULT_COOKIE_CHANGE, "modul/laporan_tambahan_biaya");

    $policy->addPolicy("pesanan","pesanan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/pesanan");
    $policy->addPolicy("pesanan_pasien","pesanan_pasien",Policy::$DEFAULT_COOKIE_CHANGE,"modul/pesanan_pasien");
    $policy->addPolicy("pesanan_nonpasien","pesanan_nonpasien",Policy::$DEFAULT_COOKIE_CHANGE,"modul/pesanan_nonpasien");
	$policy->addPolicy("print_all_tag_gizi","pesanan",Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_all_tag_gizi");
	$policy->addPolicy("get_bed_patient","pesanan",Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_bed_patient");

	$policy->addPolicy("tambahan_biaya", "tambahan_biaya", Policy::$DEFAULT_COOKIE_CHANGE, "modul/tambahan_biaya");
	
	$policy->addPolicy("laporan_konsul_gizi","laporan_konsul_gizi",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_konsul_gizi");
	
    $policy->addPolicy("kode_akun", "settings", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun");
    
    $policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>