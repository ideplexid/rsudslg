<?php
	global $db;
	require_once ('gizi/class/template/PemesananMakanForm.php');

	$noreg_pasien 	= $_POST ['noreg_pasien'];
	$nama_pasien 	= $_POST ['nama_pasien'];
	$nrm_pasien 	= $_POST ['nrm_pasien'];
	$jenis_pasien   = $_POST ['jenis_pasien'];
	$page 			= $_POST ['page'];
	$action 		= $_POST ['action'];
	$polislug 		= $_POST ['polislug'];
	$bed			= $_POST ['bed'];
	$kelas 			= $_POST ['kelas'];
	$pslug 			= $_POST ['prototype_slug'];
	$pname 			= $_POST ['prototype_name'];
	$pimplement 	= $_POST ['prototype_implement']; 

	ob_start ();
	$pemesanan_makan_form = new PemesananMakanForm( 
		$db, 
		$polislug, 
		$bed,
		$kelas,
		$noreg_pasien, 
		$nrm_pasien, 
		$nama_pasien, 
		$jenis_pasien,
		$page, 
		$action, 
		$pslug, 
		$pname, 
		$pimplement
	);
	$pemesanan_makan_form->initialize ();
	$result = ob_get_clean ();
	echo json_encode ( $result );
?>