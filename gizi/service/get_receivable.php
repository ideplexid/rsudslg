<?php 
global $db;
$awal=$_POST['awal'];
$akhir=$_POST['akhir'];

$response=array();
$response['page']="0"; 													// always 0
$response['max_page']="0"; 												// always 0.

$query="SELECT SUM(biaya) FROM smis_gz_asuhan WHERE tanggal>='$awal' AND tanggal<='$akhir'";
$nilai=$db->get_var($query);

$response['data']=array(); 												// array of string
$response['data']["0"]=array();											// array of string number 0
$response['data']["0"]["layanan"]="Asuhan Gizi";						// based on layanan sistem, resep obat, tindakan perawat, tindakan dokter dll
$response['data']["0"]["nilai"]=$nilai;									// jumlahan total per tanggal
$response['data']["0"]["urjip"]="up";									// berisi tiga tempat URI, URJ atau Unit Penunjang

echo json_encode($response);

?>