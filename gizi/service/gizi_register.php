<?php

global $db;
require_once 'gizi/class/template/GiziTemplate.php';

$polislug                       = $_POST['polislug'];
$page                           = $_POST['page'];
$action                         = $_POST['action'];
$protoslug                      = $_POST['prototype_slug'];
$protoname                      = $_POST['prototype_name'];
$protoimplement                 = $_POST['prototype_implement'];
$noreg_pasien                   = $_POST['noreg_pasien'];
$nama_pasien                    = $_POST['nama_pasien'];
$nrm_pasien                     = $_POST['nrm_pasien'];
$bed                            = $_POST['bed'];
$umur                           = $_POST['umur'];
$ibu_kandung                    = $_POST['ibu_kandung'];
$setting_pesan_penunggu         = $_POST['setting_pesan_penunggu'];
$setting_auto_pesan_penunggu    = $_POST['setting_auto_pesan_penunggu'];

ob_start ();
$template = new GiziTemplate($db, GiziTemplate::$MODE_DAFTAR, $polislug, $page, $action, $protoslug, $protoname, $protoimplement, $noreg_pasien, $nama_pasien, $nrm_pasien, $bed, $umur, $ibu_kandung, $setting_pesan_penunggu, $setting_auto_pesan_penunggu);
$template->initialize ();
$result = ob_get_clean ();
echo json_encode ( $result );

?>