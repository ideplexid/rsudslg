<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$dbtable = new DBTable($db, "smis_gz_asuhan");
		$row = $dbtable->get_row("
			SELECT SUM(biaya) AS 'gizi'
			FROM smis_gz_asuhan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$gizi = 0;
		if ($row != null) {
			$gizi += $row->gizi;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"	=> "gizi",
			"gizi"		=> $gizi
		);
		echo json_encode($data);
	}
?>