<?php
class RuanganService extends ServiceConsumer {
	private $ruangan;
	public function __construct($db,$ruangan="") {
		parent::__construct ( $db, "get_entity", "push_antrian" );
		$this->ruangan=$ruangan;
        $this->setCached(true,"get_entity_push_antrian");
	}
	
	public function proceedResult() {
		$content = array ();
		$option = array ();
		$option ['value'] = "Pendaftaran";
		$option ['name'] = "PENDAFTARAN" ;
		$option ['default']="Pendaftaran"==$this->ruangan?"1":"0";
		$content [] = $option;
				
		$result = json_decode ( $this->result, true );
		foreach ( $result as $autonomous ) {
			foreach ( $autonomous as $entity ) {
				$option = array ();
				$option ['value'] = $entity;
				$option ['name'] = ArrayAdapter::format("unslug", $entity ) ;
				$option ['default']=$entity==$this->ruangan?"1":"0";
				$content [] = $option;
			}
		}
		return $content;
	}
	
}
?>