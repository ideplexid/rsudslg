<?php
global $db;

if (isset ( $_POST ['noreg_pasien'] )) {
	$noreg = $_POST ['noreg_pasien'];
	$response ['selesai'] = "1";
	$response ['exist'] = "1";
	$response ['reverse'] = "0";
	$response ['cara_keluar'] = "Selesai";
	
	$dbtable = new DBTable ( $db, "smis_gz_asuhan" );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$rows = $data ['data'];
    
	require_once "smis-base/smis-include-synchronize-db.php";
    require_once "gizi/class/adapter/AsuhanSynchronousAdapter.php";
    
    $adapter=new AsuhanSynchronousAdapter();
    $result=$adapter->getContent($rows);
    $unit_data = array (
			"result" => $result,
			"jasa_pelayanan" => "1" 
	);
	$ldata ['asuhan_gizi'] = $unit_data;

	$dbtable = new DBTable($db, "smis_gz_tambahan_biaya");
	$dbtable->addCustomKriteria("noreg_pasien", "='" . $noreg . "'");
	$dbtable->setShowAll(true);
	$data = $dbtable->view("", "0");
	$rows = $data['data'];

	require_once "gizi/class/adapter/TambahanBiayaSynchronousAdapter.php";

	$adapter = new TambahanBiayaSynchronousAdapter();
	$result = $adapter->getContent($rows);
	$unit_data = array(
		"result" => $result,
		"jasa_pelayanan" => "1"
	);
	$ldata['tambahan_biaya'] = $unit_data;
    
    require_once "gizi/class/adapter/PesananSynchronousAdapter.php";
    $pesanan_dbtable = new DBTable($db, "smis_gz_pesanan");
    $pesanan_dbtable->addCustomKriteria("noreg_pasien = ", "'".$noreg."'");
    $pesanan_dbtable->setShowAll ( true );
	$data_pesanan = $pesanan_dbtable->view ( "", "0" );
	$rows_pesanan = $data_pesanan ['data'];
    $adapter_pesanan = new PesananSynchronousAdapter();
    $result_pesanan = $adapter_pesanan->getContent($rows_pesanan);
    $unit_data_pesanan = array(
        "result" => $result_pesanan,
        "jasa_pelayanan" => "0"
    );
    
    $ldata ['pesanan_makanan'] = $unit_data_pesanan;	

	$dbtable = new DBTable ( $db, "smis_gz_konsul" );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$rows = $data ['data'];
    
	require_once "smis-base/smis-include-synchronize-db.php";
    require_once "gizi/class/adapter/KonsulGiziSynchronousAdapter.php";
    
    $adapter=new KonsulGiziSynchronousAdapter();
    $result=$adapter->getContent($rows);
    $unit_data = array (
			"result" => $result,
			"jasa_pelayanan" => "1" 
	);
	$ldata ['konsul_gizi'] = $unit_data;
	$response ['data'] = $ldata;
	echo json_encode ( $response );
}

?>