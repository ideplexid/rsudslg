<?php
    $settings    = array();
    $settings[]  = array("smis_gz_menu_gizi_layanan", "Mengaktifkan Menu Gizi di Layanan", "0", "checkbox", "jika setting ini dicentang maka menu gizi akan tampil di layanan pasien");
    $settings[]  = array("smis_gz_pesanan_penunggu_pasien", "Setting pesanan makanan untuk penunggu pasien", "0", "checkbox", "Jika ini dicentang, maka ketika menambah pesanan makanan untuk pasien akan muncul pesanan untuk penunggu pasien.");
    $settings[]  = array("smis_gz_auto_pesanan_penunggu_pasien", "Auto pesan makan untuk penunggu pasien", "0", "checkbox", "Jika ini dicentang, maka ketika user input menu makan pasien otomatis akan muncul pesanan untuk penunggu pasien berdasarkan jadwal menu makanan yang ada.");
    echo json_encode($settings);
?>