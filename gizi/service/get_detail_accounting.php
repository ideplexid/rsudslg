<?php

global $db;
$id         = $_POST['data'];
$dbtable    = new DBTable($db,"smis_gz_asuhan");
$x          = $dbtable->selectEventDel($id);

$list       = array();

$debet=array();
$debet['akun']      = getSettings($db, "smis-rs-accounting-debet-asuhan-gizi", "");
$debet['debet']     = $x->biaya;
$debet['kredit']    = 0;
$debet['ket']       = "Piutang Asuhan Gizi - Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$debet['code']      = "debet-gizi-".$x->id;
$list[]             = $debet;

$kredit=array();
$kredit['akun']     = getSettings($db, "smis-rs-accounting-kredit-asuhan-gizi", "");
$kredit['debet']    = 0;
$kredit['kredit']   = $x->biaya;
$kredit['ket']      = "Pendapatan Asuhan Gizi - Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$kredit['code']     = "kredit-gizi-".$x->id;
$list[]             = $kredit;

//content untuk header
$header=array();
$header['tanggal']      = $x->tanggal;
$header['keterangan']   = "Transaksi Asuhan Gizi Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Gizi-".$x->id;
$header['nomor']        = "GIZI-".$x->id;
$header['debet']        = $x->biaya;
$header['kredit']       = $x->biaya;
$header['io']           = "1";

$transaction_Gizi               = array();
$transaction_Gizi['header']     = $header;
$transaction_Gizi['content']    = $list;

/*transaksi keseluruhan*/
$transaction    = array();
$transaction[]  = $transaction_Gizi;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting'] = 1;
$id['id']           = $x->id;
$dbtable->setName("smis_gz_asuhan");
$dbtable->update($update,$id);

?>