<?php

global $db;
require_once ("smis-base/smis-include-service-consumer.php");

$data = array();
$data['command'] = 'list';

$polislug = $_POST['polislug'];
$nrm_pasien = $_POST['nrm_pasien'];
$noreg_pasien = $_POST['noreg_pasien'];

$serv = new ServiceConsumer($db, "get_bed_provider", $data, $polislug);
$serv->execute();
$hasil = $serv->getContent();
$has = $hasil['data'];

$result = array();
for($i = 0; $i < sizeof($has); $i++) {
    if($has[$i]['nrm_pasien'] == $nrm_pasien && $has[$i]['noreg_pasien'] == $noreg_pasien) {
      $result = $has[$i];
    }
}

echo json_encode($result);

?>