<?php 

global $db;
require_once "gizi/function/print_tag_gizi.php";
$dbtable=new DBTable($db, "smis_gz_pesanan");
$dbtable->addCustomKriteria(" tanggal ","=CURDATE()");
$dbtable->addCustomKriteria(" cetak ","=1");
$dbtable->setShowAll(true);

$data=$dbtable->view("","0");
$list=$data['data'];
$gap=$_POST['gap']*1;
$max=getSettings($db,"gizi-tag-char-per-value",10);
$tag=array();
while($gap!=0){
    $gap--;
    $tag[]=print_dummy_tag();
}
foreach($list as $px){
    $tag[]=print_tag_gizi($db,$px,$max,$_POST['waktu']);
}
$result="";
$max_tag_per_paper=getSettings($db,"gizi-tag-max-per-paper",10);
$total_tag=count($tag);

$paper="<div class='tag_paper'>";
$num_tag=0;
foreach($tag as $t){
    $num_tag++;
    $paper.=$t;
    if($num_tag>=$max_tag_per_paper){
        /*reset tag while reach maximum tag per paper*/
        $num_tag=0;
        $paper.="</div>";
        $result.=$paper;
        $paper="<div class='tag_paper'>";
    }
}
if($total_tag%$max_tag_per_paper!=0){
    $paper.="</div>";
    $result.=$paper;
}

$CSS="<style type='text/css'>".getSettings($db, "gizi-tag-css", "")."</style>";
$JS="<script type='text/javascript'>".getSettings($db, "gizi-tag-js", "")."</script>";
$final=$result.$CSS.$JS;
$pack=new ResponsePackage();
$pack->setStatus(ResponsePackage::$STATUS_OK);
$pack->setContent($final);
echo json_encode($pack->getPackage());

?>