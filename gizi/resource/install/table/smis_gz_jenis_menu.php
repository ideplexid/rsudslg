<?php

require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_gz_jenis_menu",DBCreator::$ENGINE_INNODB);

$dbcreator->addColumn("jenis_menu", "varchar(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->setDuplicate(false);
$dbcreator->initialize();

?>