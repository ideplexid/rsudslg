<?php

require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_gz_pesanan_nonpasien",DBCreator::$ENGINE_MYISAM);

$dbcreator->addColumn("waktu", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_pemesan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("jenis_pemesan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("status", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("menu", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tarif", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("total_tarif", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");

$dbcreator->setDuplicate(false);
$dbcreator->initialize();


?>