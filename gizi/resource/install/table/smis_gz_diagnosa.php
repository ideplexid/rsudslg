<?php
require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_gz_diagnosa",DBCreator::$ENGINE_MYISAM);
$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tanggal_lahir", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("noreg_pasien", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nrm_pasien", "varchar(10)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_pasien", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("jk", "tinyint(1)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("id_dokter", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_dokter", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nip_dokter", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("kode_icd", "varchar(10)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_icd", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("bbtb", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("status_gizi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("ruangan_rujukan", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("carabayar", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->initialize();