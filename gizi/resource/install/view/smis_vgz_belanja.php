<?php 
global $wpdb;
$query="create or replace view smis_vgz_belanja as
	SELECT smis_gz_bahan.kode, 
	UPPER(smis_gz_belanja.nama) as nama,
	smis_gz_belanja.prop,
	smis_gz_dana.tanggal,
	smis_gz_belanja.biaya,
	smis_gz_dana.markup,
	smis_gz_dana.id as no_nota,
	smis_gz_dana.markup*smis_gz_belanja.biaya/100+smis_gz_belanja.biaya as total_biaya
	FROM smis_gz_belanja LEFT JOIN smis_gz_dana ON smis_gz_belanja.id_dana=smis_gz_dana.id
	LEFT JOIN smis_gz_bahan ON smis_gz_belanja.id_belanja=smis_gz_bahan.id
	WHERE smis_gz_dana.prop!='del' AND smis_gz_belanja.prop!='del';";
$wpdb->query($query);
?>