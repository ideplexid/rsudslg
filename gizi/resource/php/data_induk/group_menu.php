<?php
	global $db;
	
	$table = new Table(
		array("No.", "Group Menu"),
		"Group Menu",
		null,
		true
	);
	$table->setName("group_menu");
	$table->setDelButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Group Menu", "group_menu");
		$dbtable = new DBTable($db, "smis_gz_group_menu");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("group_menu_add_form", "smis_form_container", "group_menu");
	$modal->setTitle("Data Group Menu");
	$id_hidden = new Hidden("group_menu_id", "group_menu_id", "");
	$modal->addElement("", $id_hidden);
	$nama_text = new Text("group_menu_group_menu", "group_menu_group_menu", "");
	$modal->addElement("Group Menu", $nama_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("group_menu.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("gizi/resource/js/group_menu.js", false);
?>