<?php
	global $db;
	
	$table = new Table(
		array("No.", "Jenis Menu"),
		"Jenis Menu",
		null,
		true
	);
	$table->setName("jenis_menu");
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Jenis Menu", "jenis_menu");
		$dbtable = new DBTable($db, "smis_gz_jenis_menu");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("jenis_menu_add_form", "smis_form_container", "jenis_menu");
	$modal->setTitle("Data Jenis Menu");
	$id_hidden = new Hidden("jenis_menu_id", "jenis_menu_id", "");
	$modal->addElement("", $id_hidden);
	$nama_text = new Text("jenis_menu_jenis_menu", "jenis_menu_jenis_menu", "");
	$modal->addElement("Jenis Menu", $nama_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("jenis_menu.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("gizi/resource/js/jenis_menu.js", false);
?>