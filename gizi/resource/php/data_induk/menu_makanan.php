<?php

global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once("gizi/class/responder/MenuMakananResponder.php");

if(isset($_POST['super_command'])) {
    //Table Chooser Bahan
    $chooser_bahan_table = new Table(
        array("Nama", "Satuan", "Satuan Terkecil"),
        "",
        null,
        true
    );
    $chooser_bahan_table->setName("nama_bahan");
    $chooser_bahan_table->setModel(Table::$SELECT);
    $chooser_bahan_adapter = new SimpleAdapter();
    $chooser_bahan_adapter  ->add("Nama", "nama")
                            ->add("Satuan", "satuan")
                            ->add("Satuan Terkecil", "satuan_konversi");
    $chooser_bahan_dbresponder = new ServiceResponder(
        $db,
        $chooser_bahan_table,
        $chooser_bahan_adapter,
        "get_daftar_barang_nm"
    );

    //Table Chooser Jenis Menu
    $jenis_menu_table = new Table(
        array("Jenis Menu"),
        "",
        null,
        true
    );
    $jenis_menu_table->setName("mm_jenis_menu");
    $jenis_menu_table->setModel(Table::$SELECT);
    $jenis_menu_adapter = new SimpleAdapter();
    $jenis_menu_adapter->add("Jenis Menu", "jenis_menu");
    $jenis_menu_dbtable = new DBTable($db, "smis_gz_jenis_menu");
    $jenis_menu_dbresponder = new DBResponder(
        $jenis_menu_dbtable,
        $jenis_menu_table,
        $jenis_menu_adapter   
    );

    $super_command = new SuperCommand();
    $super_command->addResponder("nama_bahan", $chooser_bahan_dbresponder);
    $super_command->addResponder("mm_jenis_menu", $jenis_menu_dbresponder);
    $init = $super_command->initialize();
    if ($init != null) {
        echo $init;
        return;
    }
}


/*======= Tampilan Awal Master Menu Makanan =======*/
$uitable = new Table ( array (
		'No.',
        'Group',
		'Nama Menu',
        'Jenis',
        'Jenis Menu',
        'Bentuk Makanan',
        'Energi (kal)',
        'Karbohidrat (gram)',
        'Protein (gram)',
        'Lemak (gram)',
        'Kelas',
		'Keterangan',
		'Waktu',
        'Tarif Lama',
        'Tarif Sekarang',
        'Tarif Baru'
), "", NULL, true );
$uitable->setName ( "menu_makanan" );

//Menambahkan Tombol untuk Master Detail Bahan
$btn     = new Button("", "", "Add Bahan");
$btn     ->setIsButton(Button::$ICONIC)
         ->setIcon("fa fa-list-ul")
         ->setAtribute("data-content='Add_Bahan' data-toggle='popover'")
         ->setClass("btn-success");
$uitable ->addContentButton("add_bahan", $btn);

if (isset ( $_POST ['command'] )) {
	if($_POST['command'] == 'get_menu') {
        $id = $_POST['id'];
        $dbtable_menu = new DBTable($db, "smis_gz_menu");
        $data['menu'] = $dbtable_menu->get_row("
            SELECT *
            FROM smis_gz_menu
            WHERE id = '" . $id . "'
        ");
        $dbtable_bahan = new DBTable($db, "smis_gz_bahan");
        $data['bahan'] = $dbtable_bahan->get_result("
            SELECT *
            FROM smis_gz_bahan
            WHERE id_menu = '" . $id . "'
        ");
        echo json_encode($data);
        return;
    } elseif($_POST['command'] == 'save_bahan') {
        if (isset($_POST['detail'])) {
            $detail_dbtable = new DBTable($db, "smis_gz_bahan");
			$detail = $_POST['detail'];
            foreach($detail as $d) {
                $detail_data = array();
                if($d['id'] == NULL || $d['id'] == "") {
                    $detail_data['id_menu'] = $_POST['id_menu'];
                    $detail_data['id_bahan'] = $d['id_bahan'];
                    $detail_data['nama_bahan'] = $d['nama_bahan'];
                    $detail_data['jumlah'] = $d['jumlah_bahan'];
                    $detail_data['satuan_terkecil'] = $d['satuan_terkecil'];
                    $success['success'] = $detail_dbtable->insert($detail_data);
                } else {
                    $id['id'] = $d['id'];
                    $detail_data['id_menu'] = $_POST['id_menu'];
                    $detail_data['id_bahan'] = $d['id_bahan'];
                    $detail_data['nama_bahan'] = $d['nama_bahan'];
                    $detail_data['jumlah'] = $d['jumlah_bahan'];
                    $detail_data['satuan_terkecil'] = $d['satuan_terkecil'];
                    $success['success'] = $detail_dbtable->update($detail_data, $id);
                }
            }
        }
        echo json_encode ( $success );
        return;
    } else {
        $adapter = new SimpleAdapter ();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add ( "Group", "group_menu" );
        $adapter->add ( "Nama Menu", "nama" );
        $adapter->add ( "Jenis", "jenis" );
        $adapter->add ( "Jenis Menu", "jenis_menu" );
        $adapter->add ( "Bentuk Makanan", "kategori" );
        $adapter->add ( "Energi (kal)", "energi" );
        $adapter->add ( "Karbohidrat (gram)", "karbohidrat" );
        $adapter->add ( "Protein (gram)", "protein" );
        $adapter->add ( "Lemak (gram)", "lemak" );
        $adapter->add ( "Kelas", "kelas", "unslug" );
        $adapter->add ( "Keterangan", "keterangan" );
        $adapter->add ( "Waktu", "waktu" );
        $adapter->add ( "Tarif Lama", "tarif_lama", "money Rp." );
        $adapter->add ( "Tarif Sekarang", "tarif_sekarang", "money Rp." );
        $adapter->add ( "Tarif Baru", "tarif_baru", "money Rp." );
        $dbtable = new DBTable ( $db, "smis_gz_menu" );
        if ($_POST['command'] == "list") {
            $filter = " 
                        AND id_group LIKE '" . $_POST['filter_group_menu'] . "' 
                        AND jenis_menu LIKE '" . $_POST['filter_jenis_menu'] . "' 
                        AND kelas LIKE '" . $_POST['filter_kelas'] . "'
                        AND waktu LIKE '" . $_POST['filter_waktu'] . "'
                      ";
            if (isset($_POST['kriteria'])) {
                $filter .= "    AND (
                                    group_menu LIKE '%" . $_POST['kriteria'] . "%' OR 
                                    nama LIKE '%" . $_POST['kriteria'] . "%' OR 
                                    jenis LIKE '%" . $_POST['kriteria'] . "%' OR 
                                    jenis_menu LIKE '%" . $_POST['kriteria'] . "%' OR 
                                    kelas LIKE '%" . $_POST['kriteria'] . "%'
                                ) ";
                $query_value = "
                    SELECT a.*, b.group_menu
                    FROM smis_gz_menu a LEFT JOIN smis_gz_group_menu b ON a.id_group = b.id
                    WHERE a.prop = '' " . $filter . "
                    ORDER BY b.group_menu ASC, a.id DESC
                ";
                $query_count = "
                    SELECT COUNT(*)
                    FROM (
                        " . $query_value . "
                    ) v
                ";
                $dbtable->setPreferredQuery(true, $query_value, $query_count);
            }
        }
        $dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
        $dbres = new MenuMakananResponder ( $dbtable, $uitable, $adapter );
        $dbres->setDB($db);
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    }
}

// Dropdown Modal Tambah/Edit Menu Makanan
$waktu = array (
		array (
				"name" => "Pagi",
				"value" => "Pagi" 
		),
		array (
				"name" => "Siang",
				"value" => "Siang" 
		),
		array (
				"name" => "Malam",
				"value" => "Malam" 
		) 
);
$jenis = array (
		array (
				"name" => "Pasien",
				"value" => "Pasien" 
		),
		array (
				"name" => "Penunggu",
				"value" => "Penunggu" 
		),
		array (
				"name" => "Non Pasien",
				"value" => "Non Pasien" 
		) 
);
$service = new ServiceConsumer ( $db, "get_kelas" );
$service->setCached(true, "get_kelas");
$service->execute ();
$content = $service->getContent ();
$kelas = new OptionBuilder ();
foreach ( $content as $k ) {
    $nama = $k ['nama'];
    $slug = $k ['slug'];
    $kelas->add ( $nama, $slug );
}
$group_menu_rows = $db->get_result("
    SELECT *
    FROM smis_gz_group_menu
    WHERE prop = ''
    ORDER BY group_menu ASC
");
$group_menu = new OptionBuilder();
if ($group_menu_rows != null) {
    foreach ($group_menu_rows as $gm)
        $group_menu->add($gm->group_menu, $gm->id);
}

// Modal Tambah/Edit Menu Makanan
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama Menu", "" );
$uitable->addModal ( "waktu", "select", "Waktu", $waktu );
$uitable->addModal ( "jenis", "select", "Jenis", $jenis );
$uitable->addModal ( "id_jenis_menu", "hidden", "", "" );
$uitable->addModal ( "jenis_menu", "chooser-menu_makanan-mm_jenis_menu-Jenis Menu", "Jenis Menu", "", "y", null,true,null, false );
$uitable->addModal ( "id_group", "select", "Group Menu", $group_menu->getContent() );
$uitable->addModal ( "kategori", "textarea", "Bentuk Makanan", "" );
$uitable->addModal ( "kelas", "select", "Kelas", $kelas->getContent() );
$uitable->addModal ( "energi", "text", "Energi (kal)", "" );
$uitable->addModal ( "karbohidrat", "text", "Karbohidrat (gram)", "" );
$uitable->addModal ( "protein", "text", "Protein (gram)", "" );
$uitable->addModal ( "lemak", "text", "Lemak (gram)", "" );
$uitable->addModal ( "keterangan", "text", "Keterangan", "" );
$uitable->addModal ( "tarif_lama", "money", "Tarif Lama", "" );
$uitable->addModal ( "tarif_sekarang", "money", "Tarif Sekarang", "" );
$uitable->addModal ( "tarif_baru", "money", "Tarif Baru", "" );

/*======= Detail Bahan untuk Menu Makanan =======*/
// Modal untuk Menampilkan Detail Bahan pada Menu Makanan
$bahan_modal = new Modal("bahan_add_form", "smis_form_container", "menu_makanan");
$bahan_modal->setTitle("Bahan");
$bahan_modal->setClass(Modal::$FULL_MODEL);
$menu_id_text = new Text("menu_id", "menu_id", "", "text");
$menu_id_text->setAtribute("disabled='disabled'");
$bahan_modal->addElement("ID Menu", $menu_id_text);
$menu_nama_text = new Text("menu_nama", "menu_nama", "", "text");
$menu_nama_text->setAtribute("disabled='disabled'");
$bahan_modal->addElement("Nama Menu", $menu_nama_text);
$menu_tarif_text = new Text("menu_tarif", "menu_tarif", "", "text");
$menu_tarif_text->setAtribute("disabled='disabled'");
$bahan_modal->addElement("Tarif Menu", $menu_tarif_text);

$bahan_table = new Table(
    array("No.", "Nama", "Jumlah", "Satuan Terkecil"),
    "",
    null,
    true
);
$bahan_table->setName("bahan");
$bahan_table->setPrintButtonEnable(false);
$bahan_table->setReloadButtonEnable(false);
$bahan_table->setFooterVisible(false);
$bahan_modal->addBody("bahan_menu_makanan", $bahan_table);
$save_button = new Button("", "", "Simpan");
$save_button->setClass("btn-success");
$save_button->setAtribute("id='bahan_save'");
$save_button->setAction("menu_makanan.save_bahan()");
$save_button->setIcon("fa fa-floppy-o");
$save_button->setIsButton(Button::$ICONIC);
$bahan_modal->addFooter($save_button);

//Modal untuk Tambah Detail Bahan
$detail_bahan_modal = new Modal("detail_bahan_add_form", "smis_form_container", "bahan");
$detail_bahan_modal->setTitle("Detail Bahan");
$id_detail_bahan = new Hidden("id_detail_bahan", "id_detail_bahan", "");
$detail_bahan_modal->addElement("", $id_detail_bahan);
$id_menu = new Hidden("id_menu", "id_menu", "");
$detail_bahan_modal->addElement("", $id_menu);
$bahan_chooser = new Button("", "", "Pilih Menu");
$bahan_chooser->setClass("btn-info");
$bahan_chooser->setAction("nama_bahan.chooser('nama_bahan', 'nama_bahan_chooser', 'nama_bahan', nama_bahan)");
$bahan_chooser->setIcon("icon-white icon-list-alt");
$bahan_chooser->setAtribute("id='bahan_browse'");
$bahan_chooser->setIsButton(Button::$ICONIC);
$bahan_text = new Text("nama_bahan_text", "nama_bahan_text", "");
$bahan_text->setClass("smis-one-option-input");
$menu_input_group = new InputGroup("");
$menu_input_group->addComponent($bahan_text);
$menu_input_group->addComponent($bahan_chooser);
$detail_bahan_modal->addElement("Nama Bahan", $menu_input_group);
$id_bahan = new Hidden("id_bahan", "id_bahan", "");
$detail_bahan_modal->addElement("", $id_bahan);
$jumlah_bahan = new Text("jumlah_bahan", "jumlah_bahan", "");
$detail_bahan_modal->addElement("Jumlah", $jumlah_bahan);
$satuan_terkecil_bahan = new Text("satuan_terkecil_bahan", "satuan_terkecil_bahan", "");
$satuan_terkecil_bahan->setAtribute("disabled='disabled'");
$detail_bahan_modal->addElement("Satuan Terkecil", $satuan_terkecil_bahan);
$save_bahan_button = new Button("", "", "Simpan");
$save_bahan_button->setClass("btn-success");
$save_bahan_button->setAction("bahan.save()");
$save_bahan_button->setAtribute("id='detail_bahan_save'");
$save_bahan_button->setIcon("fa fa-floppy-o");
$save_bahan_button->setIsButton(Button::$ICONIC);
$detail_bahan_modal->addFooter($save_bahan_button);

$modal = $uitable->getModal ();
$modal->setTitle ( "Menu Makanan" );
$modal->setClass( Modal::$HALF_MODEL );
$modal->addHTML("
    <div class='alert alert-block alert-inverse' id='help_menu_makanan'>
        <h4>Perhatian</h4>
        <ul>
            <li>Pengisian <b>Bentuk Makanan</b> diisikan dalam bentuk SCSV (Semicolon Separated Value), misal : Nasi;Bubur;Lontong;Roti</li>
            <li>Pilihan <b>Kelas</b> berdasarkan data di Manajer (SMIS Cache Berpengaruh)</li>
        <ul>
    </div>
", "before");

// Form. Filter Menu Makanan
$form = new Form("", "", "Menu Makanan");
$group_menu = new OptionBuilder();
$group_menu->add("Semua", "%%", "1");
if ($group_menu_rows != null) {
    foreach ($group_menu_rows as $gm)
        $group_menu->add($gm->group_menu, $gm->id);
}
$group_menu_filter_select = new Select("menu_makanan_filter_group_menu", "menu_makanan_filter_group_menu", $group_menu->getContent());
$form->addElement("Group Menu", $group_menu_filter_select);
$jenis_menu = new OptionBuilder();
$jenis_menu->add("Semua", "%%", "1");
if ($jenis_menu_rows != null) {
    foreach ($jenis_menu_rows as $jm)
        $jenis_menu->addSingle($jm->jenis_menu);
}
$jenis_menu_filter_select = new Select("menu_makanan_filter_jenis_menu", "menu_makanan_filter_jenis_menu", $jenis_menu->getContent());
$form->addElement("Jenis Menu", $jenis_menu_filter_select);
$kelas = new OptionBuilder ();
$kelas->add("Semua", "%%", "1");
foreach ( $content as $k ) {
    $nama = $k ['nama'];
    $slug = $k ['slug'];
    $kelas->add ( $nama, $slug );
}
$kelas_filter_select = new Select("menu_makanan_filter_kelas", "menu_makanan_filter_kelas", $kelas->getContent());
$form->addElement("Kelas", $kelas_filter_select);
$waktu[] = array(
    "name"      => "Semua",
    "value"     => "%%",
    "default"   => "1"
);
$waktu_filter_select = new Select("menu_makanan_filter_waktu", "menu_makanan_filter_waktu", $waktu);
$form->addElement("Waktu", $waktu_filter_select);

echo $form->getHtml ();
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo $bahan_modal->getHtml();
echo $detail_bahan_modal->getHtml();

echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "gizi/resource/js/menu_makanan.js",false );

?>
<style type="text/css">
    #menu_makanan_add_form_form label {
        width: 125px !important;
    }
</style>
