<?php
    global $db;
    
    $table = new Table(
        array("No.", "Tanggal", "Menu Pasien"),
        "Jadwal Menu",
        null,
        true
    );
    $table->setName("jadwal");
    
    if (isset($_POST['command'])) {
        $adapter = new SimpleAdapter(true, "No.");
        $adapter->add("Tanggal", "tanggal", "date d-m-Y");
        $adapter->add("Menu Pasien", "group_menu");
        $dbtable = new DBTable($db, "smis_gz_jadwal");
        $dbtable->setOrder(" tanggal, id ASC ");
        $dbresponder = new DBResponder(
            $dbtable,
            $table,
            $adapter
        );
        $data = $dbresponder->command($_POST['command']);
        echo json_encode($data);
        return;
    }
    
    $modal = new Modal("jadwal_add_form", "smis_form_container", "jadwal");
    $modal->setTitle("Data Jadwal Menu");
    $id_hidden = new Hidden("jadwal_id", "jadwal_id", "");
    $modal->addElement("", $id_hidden);
    $tanggal_text = new Text("jadwal_tanggal", "jadwal_tanggal", "");
    $tanggal_text->setClass("mydate");
    $tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
    $modal->addElement("Tanggal", $tanggal_text);
    $group_menu_rows = $db->get_result("
        SELECT *
        FROM smis_gz_group_menu
        WHERE prop = ''
        ORDER BY group_menu ASC
    ");
    $group_menu = new OptionBuilder();
    if ($group_menu_rows != null) {
        foreach ($group_menu_rows as $gm)
            $group_menu->add($gm->group_menu, $gm->id);
    }
    $id_group_select = new Select("jadwal_id_group", "jadwal_id_group", $group_menu->getContent());
    $modal->addElement("Menu Makanan", $id_group_select);
    $save_button = new Button("", "", "Simpan");
    $save_button->setClass("btn-success");
    $save_button->setAction("jadwal.save()");
    $save_button->setIcon("fa fa-floppy-o");
    $save_button->setIsButton(Button::$ICONIC);
    $modal->addFooter($save_button);
    
    echo $modal->getHtml();
    echo $table->getHtml();
    echo addJS("framework/smis/js/table_action.js");
    echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
    echo addCSS("framework/bootstrap/css/datepicker.css");
    echo addJS("gizi/resource/js/jadwal.js", false);
?>
