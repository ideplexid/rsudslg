/**
 * this js used to present the 
 * ui of BranchCompany. it's only regular 
 * javascript but need to be separated
 * 
 * @version 	: 1.0.0
 * @author		: Nurul Huda
 * @license		: LGLv2
 * @copyright 	: goblooge@gmail.com
 * @used 		: gizi/modul/bahan.php
 * @since		: 15 Sept 2016
 * @database	: - smis_hrd_branch
 * 
 * */

var bahan;
$(document).ready(function(){	
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','kode','nama','keterangan');
	bahan=new TableAction("bahan","gizi","bahan",column);
	bahan.view();	
});