var pasien;
var konsul_gizi;
var pengasuh;
var dokter;
var diagnosa_gizi;
var jenis_diet;
var biaya;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array("id",
    "tanggal",
    "id_dokter",
    "nama_dokter",
    "id_pengasuh",
    "nama_pengasuh",
    "kode_diagnosa_gizi",
    "diagnosa_gizi",
    "status_gizi",
    "diagnosa_gizi_manual",
    "antropometri",
    "jenis_diet",
    "konsul_gizi",
    "kelas_konsul_gizi",
    "biaya_konsul_gizi",
    "keterangan",
    "hasil_skrining",
    "sisa_makanan"
    );
	konsul_gizi=new TableAction("konsul_gizi","gizi","konsul_gizi",column);
	konsul_gizi.getRegulerData=function(){
		var reg_data={
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
				};
		reg_data['noreg_pasien']=$("#noreg_pasien").val();
		reg_data['nama_pasien']=$("#nama_pasien").val();
		reg_data['nrm_pasien']=$("#nrm_pasien").val();
        reg_data['ruangan']=$("#ruangan").val();
        reg_data['tanggal_lahir_pasien']=$("#tanggal_lahir_pasien").val();
        reg_data['usia']=$("#usia").val();
        reg_data['jenis_pasien']=$("#jenis_pasien").val();
        reg_data['jk']=$("#jenis_kelamin").val();        
		return reg_data;
	};
	konsul_gizi.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
    };
    
    konsul_gizi.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };
    
    konsul_gizi.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };

	pasien=new TableAction("pasien","gizi","konsul_gizi",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		$("#nama_pasien").val(json.nama_pasien);
		$("#nrm_pasien").val(json.nrm);
		$("#noreg_pasien").val(json.id);
        $("#ruangan").val(json.last_ruangan);
        $("#tanggal_lahir_pasien").val(json.tgl_lahir);
        $("#usia").val(json.id);
        $("#jenis_pasien").val(json.carabayar);
        $("#jenis_kelamin").val(json.kelamin);
		konsul_gizi.view();
	};

	pengasuh=new TableAction("pengasuh","gizi","konsul_gizi",new Array());
	pengasuh.setSuperCommand("pengasuh");
	pengasuh.selected=function(json){
		konsul_gizi.set("nama_pengasuh",json.nama);
        konsul_gizi.set("id_pengasuh",json.id)
    };
    
    dokter=new TableAction("dokter","gizi","konsul_gizi",new Array());
	dokter.setSuperCommand("dokter");
	dokter.selected=function(json){
        konsul_gizi.set("nama_dokter",json.nama);
        konsul_gizi.set("id_dokter",json.id)
    };
    
    diagnosa_gizi = new TableAction("diagnosa_gizi","gizi","konsul_gizi",new Array());
	diagnosa_gizi.setSuperCommand("diagnosa_gizi");
	diagnosa_gizi.selected=function(json){
        konsul_gizi.set("kode_diagnosa_gizi",json.icd);
        konsul_gizi.set("diagnosa_gizi",json.nama);
    };

    jenis_diet = new TableAction("jenis_diet","gizi","konsul_gizi",new Array());
	jenis_diet.setSuperCommand("jenis_diet");
	jenis_diet.selected=function(json){
        konsul_gizi.set("jenis_diet",json.nama);
    };

    biaya = new TableAction("biaya","gizi","konsul_gizi",new Array());
	biaya.setSuperCommand("biaya");
	biaya.selected=function(json){
        konsul_gizi.set("kelas_konsul_gizi",json.kelas);
        konsul_gizi.set("biaya_konsul_gizi",json.biaya);
        konsul_gizi.set("konsul_gizi",json.jenis_tindakan);
    };


	$(".mydate").datepicker();

});