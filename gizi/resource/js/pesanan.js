var pesanan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	pesanan=new TableAction("pesanan","gizi","pesanan",column);
	pesanan.sync=function(set,curpoint){
		var total=set.length;
		var data=this.getRegulerData();
		var current=set[curpoint];		
		data['ruang']=current.value;
		data['super_command']="sync";		
		$.post('',data,function(res){
			getContent(res);
			$("#rekap_ksr_bar").sload("true","[ "+curpoint+" / "+total+" ] "+current.name+"...",(curpoint*100/total));
			var json=getContent(res);
			$("#layanan_table_list").append(json.row);
			
			if(curpoint+1>=total){
				$("#rekap_ksr_modal").modal("hide");
				pesanan.view();
				return;
			} else {
				curpoint++;
				setTimeout(function(){
					pesanan.sync(set,curpoint);
				},500);
			}
			
		});
	};

	pesanan.collect_ruang=function(){
		var data=this.getRegulerData();
		data['super_command']="collect_ruang";
		showLoading();
		$.post('',data,function(res){
			dismissLoading();
			var json=getContent(res);
			if(json==null){
				return;
			}else{
				$("#rekap_ksr_bar").sload("true","Loading Data",0);
				$("#rekap_ksr_modal").modal("show");
				$("#pesanan_list").html("");
				pesanan.sync(json,0);
			}
			
		});
	};
    
    pesanan.activate_print=function(id){
        var data=this.getRegulerData();
		data['command']="save";
        data['id']=id;
		showLoading();
		$.post('',data,function(res){
			var json=getContent(res);
			pesanan.view();
            dismissLoading();
		});
    };
    
    pesanan.proceed=function(){
        var data=this.getRegulerData();
		data['command']="proceed";
        data['cek']=$("#pesanan_cek").val();
		showLoading();
		$.post('',data,function(res){
			var json=getContent(res);
			pesanan.view();
            dismissLoading();
		});
    };
    
    pesanan.print_all=function(){
        var data=this.getRegulerData();
        data['action']="print_all_tag_gizi";
		data['command']="proceed";
        data['cek']=$("#pesanan_cek").val();
        data['waktu']=$("#pesanan_waktu").val();
        data['gap']=$("#pesanan_gap").val();
		showLoading();
		$.post('',data,function(res){
			var json=getContent(res);
            try{
                $("#printing_area").html(json);
            }catch(err){
                smis_print(json);
            }
            dismissLoading();
		});
    };    
    pesanan.view();
});