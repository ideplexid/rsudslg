var jadwal;
$(document).ready(function() {
	$(".mydate").datepicker();
	var columns = new Array("id", "id_group", "tanggal");
	jadwal = new TableAction(
		"jadwal",
		"gizi",
		"jadwal",
		columns
	);
	jadwal.getSaveData = function() {
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#jadwal_id").val();
		data['tanggal'] = $("#jadwal_tanggal").val();
		data['id_group'] = $("#jadwal_id_group").val();
		data['group_menu'] = $("#jadwal_id_group option:selected").text();
		return data;
	}
	jadwal.view();
});