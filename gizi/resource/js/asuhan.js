var pasien;
var asuhan;
var pengasuh;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id","biaya","keterangan","tanggal",'id_pengasuh','nama_pengasuh');
	asuhan=new TableAction("asuhan","gizi","asuhan",column);
	asuhan.getRegulerData=function(){
		var reg_data={
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
				};
		reg_data['noreg_pasien']=$("#noreg_pasien").val();
		reg_data['nama_pasien']=$("#nama_pasien").val();
		reg_data['nrm_pasien']=$("#nrm_pasien").val();
        reg_data['ruangan']=$("#ruangan").val();
		return reg_data;
	};
	asuhan.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
    };
    
    asuhan.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    asuhan.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };

	pasien=new TableAction("pasien","gizi","asuhan",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		$("#nama_pasien").val(json.nama_pasien);
		$("#nrm_pasien").val(json.nrm);
		$("#noreg_pasien").val(json.id);
        $("#ruangan").val(json.kamar_inap);
		asuhan.view();
	};

	pengasuh=new TableAction("pengasuh","gizi","asuhan",new Array());
	pengasuh.setSuperCommand("pengasuh");
	pengasuh.selected=function(json){
		var nama=json.nama;
		var nip=json.id;
		$("#asuhan_nama_pengasuh").val(nama);
		$("#asuhan_id_pengasuh").val(nip);
	};

	$(".mydate").datepicker();

});