var pesanan_pasien;

$(document).ready(function() {
    $('.mydatetime').datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column = new Array();
	pesanan_pasien = new TableAction("pesanan_pasien","gizi","pesanan_pasien",column);
    
    pesanan_pasien.addRegulerData = function(data) {
        data['dari'] = $("#pesanan_pasien_dari").val();
        data['sampai'] = $("#pesanan_pasien_sampai").val();
        return data;
    };

    pesanan_pasien.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = pesanan_pasien.get("noreg_pasien");
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    pesanan_pasien.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };

});