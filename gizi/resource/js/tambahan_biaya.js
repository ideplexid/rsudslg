var pasien;
var tambahan_biaya;
var pengasuh;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id","biaya","keterangan","tanggal",'id_pengasuh','nama_pengasuh');
	tambahan_biaya=new TableAction("tambahan_biaya","gizi","tambahan_biaya",column);
	tambahan_biaya.getRegulerData=function(){
		var reg_data={
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
				};
		reg_data['noreg_pasien']=$("#noreg_pasien").val();
		reg_data['nama_pasien']=$("#nama_pasien").val();
		reg_data['nrm_pasien']=$("#nrm_pasien").val();
        reg_data['ruangan']=$("#ruangan").val();
		return reg_data;
	};
	tambahan_biaya.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
    };
    
    tambahan_biaya.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    tambahan_biaya.validate = function() {
        var valid = true;
        var keterangan = $("#tambahan_biaya_keterangan").val();
        var invalid_msg = "";
        $(".error_field").removeClass("error_field");
        if (keterangan == "") {
            valid = false;
            invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
            $("#tambahan_biaya_keterangan").addClass("error_field");
        }
        if (!valid) {
            $("#modal_alert_tambahan_biaya_add_form").html(
                "<div class='alert alert-block alert-danger'>" +
                "<h4>Peringatan</h4>" +
                invalid_msg +
                "</div>"
            );
        }
        return valid;
    };

    tambahan_biaya.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek && this.validate()){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };

	pasien=new TableAction("pasien","gizi","tambahan_biaya",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		$("#nama_pasien").val(json.nama_pasien);
		$("#nrm_pasien").val(json.nrm);
		$("#noreg_pasien").val(json.id);
        $("#ruangan").val(json.kamar_inap);
		tambahan_biaya.view();
	};

	pengasuh=new TableAction("pengasuh","gizi","tambahan_biaya",new Array());
	pengasuh.setSuperCommand("pengasuh");
	pengasuh.selected=function(json){
		var nama=json.nama;
		var nip=json.id;
		$("#tambahan_biaya_nama_pengasuh").val(nama);
		$("#tambahan_biaya_id_pengasuh").val(nip);
	};

    $(".mydate").datepicker();
    tambahan_biaya.view();

});