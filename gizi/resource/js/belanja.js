var belanja;
var bahan;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var child_column=new Array('id','id_dana','nama','jumlah','biaya','no_faktur','harga_satuan','satuan');
	var parent_column=new Array('id','tanggal','no_bukti','keterangan','dari','markup');	
	bahan=new TableAction("bahan","gizi","belanja");
	bahan.setSuperCommand("bahan");
	bahan.selected=function(json){
		$("#belanja_child_nama").val(json.nama);
		$("#belanja_child_id_bahan").val(json.id);
		$("#belanja_child_jumlah").focus();
	};

	stypeahead("#belanja_child_nama",3,bahan,"nama",function(item){
		$("#belanja_child_nama").val(item.nama);
		$("#belanja_child_id_belanja").val(item.id);
		$("#belanja_child_jumlah").focus();					
	});

	$("#belanja_child_harga_satuan, #belanja_child_jumlah").on("change",function(){
		var h_satu=getMoney("#belanja_child_harga_satuan");
		var jumlah=Number($("#belanja_child_jumlah").val());
		var total=h_satu*jumlah;
		setMoney("#belanja_child_biaya",total);
	});

	$("#belanja_child_biaya, #belanja_child_jumlah").on("change",function(){
		var h_satu=getMoney("#belanja_child_biaya");
		var jumlah=Number($("#belanja_child_jumlah").val());
		var total=h_satu/jumlah;
		setMoney("#belanja_child_harga_satuan",total);
	});
	
	belanja=new ParentChildAction("belanja","gizi","belanja",parent_column,child_column,'id_dana');
	belanja.child.setMultipleInput(true);
	belanja.child.setNextEnter(true);
	belanja.child.setEnableAutofocus();
	belanja.child.setFocusOnMultiple("child_nama");
	belanja.parent.view();
});