rupiah = function(num) {
    return 'Rp. ' + num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ',00';
};

function MenuMakananAction(name, page, action, column) {
    this.initialize(name, page, action, column);
}
MenuMakananAction.prototype.constructor = MenuMakananAction;
MenuMakananAction.prototype = new TableAction();
MenuMakananAction.prototype.addViewData = function(data) {
    data['filter_group_menu'] = $("#menu_makanan_filter_group_menu").val();
    data['filter_jenis_menu'] = $("#menu_makanan_filter_jenis_menu").val();
    data['filter_kelas'] = $("#menu_makanan_filter_kelas").val();
    data['filter_waktu'] = $("#menu_makanan_filter_waktu").val();
    return data;
};
MenuMakananAction.prototype.edit=function (id){
	var self=this;
    var json_obj=new Array();
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
				continue;
			}
            var name=self.column[i];
			var the_id="#"+self.prefix+"_"+name;
            if( name in self.json_column && self.json_column.length>0){
				/** this function handling if there is a column that part of another column */
                var json_grup_name=self.json_column[name];
                if(json[json_grup_name]==""){
                    continue;
                }
                if(!(json_grup_name in json_obj)){
				    json_obj[json_grup_name]=$.parseJSON(json[json_grup_name]);
                }
                smis_edit(the_id,json_obj[json_grup_name][""+name]);    
            }else{
                /** this function handling reguler column **/
                smis_edit(the_id,json[""+name]);    
            }
		}
		dismissLoading();
		self.disabledOnEdit(self.column_disabled_on_edit);
		self.show_form();
	});
};
MenuMakananAction.prototype.add_bahan = function(id) {
    row_id = 0;
    var data = this.getRegulerData();
    data['command'] = 'get_menu';
    data['id']      = id;
    $.post(
        "",
        data,
        function(response) {
            var json = JSON.parse(response);
            if (json == null) return;
            if(json['bahan'] == null) {
                $("#menu_id").val(json['menu'].id);
                $("#menu_nama").val(json['menu'].nama);
                $("#menu_tarif").val(rupiah(json['menu'].tarif_sekarang));
                $("#bahan_save").removeAttr("onclick");
                $("#bahan_save").attr("onclick", "menu_makanan.save_bahan()");
                $("#bahan_add_form").smodal("show");
            } else {
                $("#menu_id").val(json['menu'].id);
                $("#menu_nama").val(json['menu'].nama);
                $("#menu_tarif").val(rupiah(json['menu'].tarif_sekarang));
                row_id = 0;
                $("#bahan_list").children().remove();
                for(var i = 0; i < json['bahan'].length; i++) {
                    var id              = json['bahan'][i].id;
                    var id_bahan        = json['bahan'][i].id_bahan;
                    var nama            = json['bahan'][i].nama_bahan;
                    var jumlah          = json['bahan'][i].jumlah;
                    var satuan_terkecil = json['bahan'][i].satuan_terkecil;
                    $("tbody#bahan_list").append(
                        "<tr id='data_" + row_id + "'>" +
                            "<td id='data_" + row_id + "_id_detail_bahan' style='display: none;'>" + id + "</td>" +
                            "<td id='data_" + row_id + "_id_bahan' style='display: none;'>" + id_bahan + "</td>" +
                            "<td id='data_" + row_id + "_nomor'></td>" +
                            "<td id='data_" + row_id + "_nama_bahan'>" + nama + "</td>" +
                            "<td id='data_" + row_id + "_jumlah_bahan'>" + jumlah + "</td>" +
                            "<td id='data_" + row_id + "_satuan_terkecil_bahan'>" + satuan_terkecil + "</td>" +
                            "<td>" +
                                "<div class='btn-group noprint'>" +
                                    "<a href='#' onclick='bahan.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
                                        "<i class='icon-edit icon-white'></i>" +
                                    "</a>" +
                                    "<a href='#' onclick='bahan.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
                                        "<i class='icon-remove icon-white'></i>" + 
                                    "</a>" +
                                "</div>" +
                            "</td>" +
                        "</tr>"
                    );
                    row_id++;
                }
                bahan.refreshBahan();
                $("#bahan_save").removeAttr("onclick");
                $("#bahan_save").attr("onclick", "menu_makanan.update_bahan()");
                $("#bahan_add_form").smodal("show");
            }
            
        }
    );
};
MenuMakananAction.prototype.save_bahan = function() {
    showLoading();
    var self = this;
    var data = this.getRegulerData();
    data['command'] = "save_bahan";
    data['id_menu'] = $("#menu_id").val();
    var detail = {};
    var nor = $("tbody#bahan_list").children("tr").length;
    for(var i = 0; i < nor; i++) {
        var d_data = {};
        var prefix = $("tbody#bahan_list").children("tr").eq(i).prop("id");
        d_data['id'] = $("#" + prefix + "_id_detail_bahan").text();
        d_data['id_bahan'] = $("#" + prefix + "_id_bahan").text();
        d_data['nama_bahan'] = $("#" + prefix + "_nama_bahan").text();
        d_data['jumlah_bahan'] = $("#" + prefix + "_jumlah_bahan").text();
        d_data['satuan_terkecil'] = $("#" + prefix + "_satuan_terkecil_bahan").text();
        detail[i] = d_data;
    }
    data['detail'] = detail;
    $.post(
        "",
        data,
        function(response) {
            var json = JSON.parse(response);
            if (json == null) {
                $("#bahan_add_form").smodal("show");
            } else {
                $("#bahan_add_form").smodal("hide");
            }
            dismissLoading();
        }
    );
};
MenuMakananAction.prototype.update_bahan = function() {
    showLoading();
    var self = this;
    var data = this.getRegulerData();
    data['command'] = "save_bahan";
    data['id_menu'] = $("#menu_id").val();
    var detail = {};
    var nor = $("tbody#bahan_list").children("tr").length;
    for(var i = 0; i < nor; i++) {
        var d_data = {};
        var prefix = $("tbody#bahan_list").children("tr").eq(i).prop("id");
        d_data['id'] = $("#" + prefix + "_id_detail_bahan").text();
        d_data['id_bahan'] = $("#" + prefix + "_id_bahan").text();
        d_data['nama_bahan'] = $("#" + prefix + "_nama_bahan").text();
        d_data['jumlah_bahan'] = $("#" + prefix + "_jumlah_bahan").text();
        d_data['satuan_terkecil'] = $("#" + prefix + "_satuan_terkecil_bahan").text();
        detail[i] = d_data;
    }
    data['detail'] = detail;
    $.post(
        "",
        data,
        function(response) {
            var json = JSON.parse(response);
            if (json == null) {
                $("#bahan_add_form").smodal("show");
            } else {
                $("#bahan_add_form").smodal("hide");
            }
            dismissLoading();
        }
    );
};

function BahanAction(name, page, action, column) {
    this.initialize(name, page, action, column);
}
BahanAction.prototype.constructor = BahanAction;
BahanAction.prototype = new TableAction();
BahanAction.prototype.show_add_form = function() {
    $("#id_detail_bahan").val("");
    $("#nama_bahan_text").val("");
    $("#id_bahan").val("");
    $("#jumlah_bahan").val("");
    $("#satuan_terkecil_bahan").val("");
    $("#detail_bahan_save").removeAttr("onclick");
    $("#detail_bahan_save").attr("onclick", "bahan.save()");
    $("#detail_bahan_add_form").smodal("show");
};
BahanAction.prototype.refreshBahan = function() {
    var no = 1;
    var no_bahan = $("tbody#bahan_list").children("tr").length;
    for(var i = 0; i < no_bahan; i++) {
        var dr_prefix = $("tbody#bahan_list").children("tr").eq(i).prop("id");
        $("#" + dr_prefix + "_nomor").html("<div align='right'>" + no + ".</div>");
        no++;
    }
};
BahanAction.prototype.save = function() {
    var id              = $("#id_detail_bahan").val();
    var id_bahan        = $("#id_bahan").val();
    var nama            = $("#nama_bahan_text").val();
    var jumlah          = $("#jumlah_bahan").val();
    var satuan_terkecil = $("#satuan_terkecil_bahan").val();
    var class_attr      = "";
    $("tbody#bahan_list").append(
        "<tr id='data_" + row_id + "' " + class_attr + ">" +
            "<td id='data_" + row_id + "_id_detail_bahan' style='display: none;'>" + id + "</td>" +
            "<td id='data_" + row_id + "_id_bahan' style='display: none;'>" + id_bahan + "</td>" +
            "<td id='data_" + row_id + "_nomor'></td>" +
            "<td id='data_" + row_id + "_nama_bahan'>" + nama + "</td>" +
            "<td id='data_" + row_id + "_jumlah_bahan'>" + jumlah + "</td>" +
            "<td id='data_" + row_id + "_satuan_terkecil_bahan'>" + satuan_terkecil + "</td>" +
            "<td>" +
                "<div class='btn-group noprint'>" +
                    "<a href='#' onclick='bahan.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
                        "<i class='icon-edit icon-white'></i>" +
                    "</a>" +
                    "<a href='#' onclick='bahan.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
                        "<i class='icon-remove icon-white'></i>" + 
                    "</a>" +
                "</div>" +
            "</td>" +
        "</tr>"
    );
    row_id++;
    bahan.refreshBahan();
    $("#detail_bahan_add_form").smodal("hide");
};
BahanAction.prototype.edit = function(r_num) {
    var id = $("#data_" + r_num + "_id_detail_bahan").text();
    var id_bahan = $("#data_" + r_num + "_id_bahan").text();
    var nama = $("#data_" + r_num + "_nama_bahan").text();
    var jumlah = $("#data_" + r_num + "_jumlah_bahan").text();
    var satuan_terkecil = $("#data_" + r_num + "_satuan_terkecil_bahan").text();
    $("#id_detail_bahan").val(id);
    $("#nama_bahan_text").val(nama);
    $("#id_bahan").val(id_bahan);
    $("#jumlah_bahan").val(jumlah);
    $("#satuan_terkecil_bahan").val(satuan_terkecil);
    $("#detail_bahan_save").removeAttr("onclick");
    $("#detail_bahan_save").attr("onclick", "bahan.update(" + r_num + ")");
    $("#detail_bahan_add_form").smodal("show");
};
BahanAction.prototype.update = function(r_num) {
    var id              = $("#id_detail_bahan").val();
    var id_bahan        = $("#id_bahan").val();
    var nama            = $("#nama_bahan_text").val();
    var jumlah          = $("#jumlah_bahan").val();
    var satuan_terkecil = $("#satuan_terkecil_bahan").val();
    $("#data_" + r_num + "_id_detail_bahan").text(id);
    $("#data_" + r_num + "_id_bahan").text(id_bahan);
    $("#data_" + r_num + "_nama_bahan_text").text(nama);
    $("#data_" + r_num + "_jumlah_bahan").text(jumlah);
    $("#data_" + r_num + "_satuan_terkecil_bahan").text(satuan_terkecil);
    $("#detail_bahan_add_form").smodal("hide");
};
BahanAction.prototype.delete = function(r_num) {
    var id = $("#data_" + r_num + "_id_detail_bahan").text();
    if (id.length == 0) {
        $("#data_" + r_num).remove();
    } else {
        $("#data_" + r_num).attr("style", "display: none;");
        $("#data_" + r_num).attr("class", "deleted");
    }
    bahan.refreshBahan();
};

function DetailBahanAction(name, page, action, column) {
    this.initialize(name, page, action, column);
}
DetailBahanAction.prototype.constructor = DetailBahanAction;
DetailBahanAction.prototype = new TableAction();
DetailBahanAction.prototype.selected = function(json) {
    $("#nama_bahan_text").val(json.nama);
    $("#id_bahan").val(json.id);
    $("#satuan_terkecil_bahan").val(json.satuan_konversi);
};


function JenisMenuAction(name, page, action, column) {
    this.initialize(name, page, action, column);
    console.log("WOI");
}
JenisMenuAction.prototype.constructor = JenisMenuAction;
JenisMenuAction.prototype = new TableAction(); 
JenisMenuAction.prototype.selected = function(json) {
    $("#menu_makanan_id_jenis_menu").val(json.id);
    $("#menu_makanan_jenis_menu").val(json.jenis_menu);
};

var menu_makanan;
var bahan;
var nama_bahan;
var mm_jenis_menu;
$(document).ready(function(){	
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	column_menu = new Array('id','nama','waktu','jenis','kategori','keterangan','tarif_lama', 'tarif_sekarang', 'tarif_baru','kelas','energi','karbohidrat','protein','lemak','jenis_menu', 'id_jenis_menu','id_group');
    //column_menu = new Array('id','nama','waktu','jenis','kategori','keterangan','tarif_lama', 'tarif_sekarang', 'tarif_baru','nominal_jaspel','nominal_rs','persen_jaspel','persen_rs');
    menu_makanan = new MenuMakananAction("menu_makanan", "gizi", "menu_makanan", column_menu);
    menu_makanan.view();
    
    bahan = new BahanAction("bahan", "gizi", "menu_makanan", new Array());
    
    nama_bahan = new DetailBahanAction("nama_bahan", "gizi", "menu_makanan", new Array());
    nama_bahan.setSuperCommand("nama_bahan");

    mm_jenis_menu  = new JenisMenuAction(
        "mm_jenis_menu",
        "gizi",
        "menu_makanan",
        new Array()
    );
    mm_jenis_menu.setSuperCommand("mm_jenis_menu");



    $("#menu_makanan_nominal_jaspel , #menu_makanan_nominal_rs ").on("keyup",function(){
        var tarif = getMoney("#menu_makanan_tarif_sekarang");
        var nominal_jaspel = getMoney("#menu_makanan_nominal_jaspel");
        var nominal_rs = getMoney("#menu_makanan_nominal_rs");

        var persen_rs = nominal_rs*100/tarif;
        var persen_jaspel = nominal_jaspel*100/tarif;
        
        $("#menu_makanan_persen_jaspel").val(persen_jaspel.toFixed(3));
        $("#menu_makanan_persen_rs").val(persen_rs.toFixed(3));
    });

    $("#menu_makanan_tarif_sekarang, #menu_makanan_persen_jaspel , #menu_makanan_persen_rs").on("keyup",function(){
        var tarif = getMoney("#menu_makanan_tarif_sekarang");

        var persen_rs = Number($("#menu_makanan_persen_rs").val());
        var persen_jaspel = Number($("#menu_makanan_persen_jaspel").val());
        
        var nominal_jaspel = persen_jaspel*tarif/100;
        var nominal_rs = persen_rs*tarif/100;
        
        setMoney("#menu_makanan_nominal_jaspel",nominal_jaspel);
        setMoney("#menu_makanan_nominal_rs",nominal_rs);
    });
    
    $("#menu_makanan_filter_group_menu").on("change", function() {
        menu_makanan.view();
    });
    $("#menu_makanan_filter_jenis_menu").on("change", function() {
        menu_makanan.view();
    });
    $("#menu_makanan_filter_kelas").on("change", function() {
        menu_makanan.view();
    });
    $("#menu_makanan_filter_waktu").on("change", function() {
        menu_makanan.view();
    });

});