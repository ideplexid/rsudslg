var pesanan_nonpasien;
var menu;
var tarif_menu;

$(document).ready(function(){
    $('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id", "waktu", "nama_pemesan", "jenis_pemesan", "menu", "tarif", "jumlah", "status","total_tarif");
	//var column=new Array();
	pesanan_nonpasien=new TableAction("pesanan_nonpasien","gizi","pesanan_nonpasien",column);
	pesanan_nonpasien.view();
    
    menu=new TableAction("menu","gizi","pesanan_nonpasien",new Array());
	menu.setSuperCommand("menu");
	menu.selected=function(json){
		$("#pesanan_nonpasien_menu").val(json.nama);
		setMoney("#pesanan_nonpasien_tarif",json.tarif_sekarang);
        tarif_menu = json.tarif_sekarang;
	};
    
    $("#pesanan_nonpasien_jumlah").keyup(function(){
        var jumlah = $("#pesanan_nonpasien_jumlah").val();
        var total = tarif_menu * jumlah;
        console.log(total);
        setMoney("#pesanan_nonpasien_total_tarif",total);
    });
	
});