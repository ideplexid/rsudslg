<?php 

class AsuhanSynchronousAdapter extends SynchronousSenderAdapter{
    private $content;
    public function __construct(){
        parent::__construct();
        $this->content=array();
    }
    
    public function adapt($d){
        global $db;
        $result                    = array();
        $onedata['waktu']          = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata['nama']           = "Asuhan Gizi - " . $d->nama_pengasuh;
		$onedata['biaya']          = $d->biaya;
		$onedata['jumlah']         = 1;
		$onedata['start']          = $d->tanggal;
		$onedata['end']            = $d->tanggal;
		$onedata['id']             = $d->id;
		$onedata['keterangan']     = "Biaya Asuhan Gizi " . $d->nama_pengasuh . " Senilai " . ArrayAdapter::format ( "only-money Rp.", $d->biaya );
		$onedata['prop']           = $this->getProp();
		$onedata['debet']          = getSettings($db, "smis-rs-accounting-debet-asuhan-gizi", "");
		$onedata['kredit']         = getSettings($db, "smis-rs-accounting-kredit-asuhan-gizi", "");
        $onedata ['urjigd'] = "URI";
        $onedata ['tanggal_tagihan'] = $d->tanggal;
        $this->content[]           = $onedata;
        return $this->content;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
}

?>