<?php 

class TambahanBiayaSynchronousAdapter extends SynchronousSenderAdapter{
    private $content;
    public function __construct(){
        parent::__construct();
        $this->content=array();
    }
    
    public function adapt($d){
        global $db;
        $result                    = array();
        $onedata['waktu']          = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata['nama']           = "Tambahan Biaya Gizi - " . $d->keterangan;
		$onedata['biaya']          = $d->biaya;
		$onedata['jumlah']         = 1;
		$onedata['start']          = $d->tanggal;
		$onedata['end']            = $d->tanggal;
		$onedata['id']             = $d->id;
		$onedata['keterangan']     = "Tambahan Biaya Gizi " . $d->keterangan . " Senilai " . ArrayAdapter::format ( "only-money Rp.", $d->biaya );
		$onedata['prop']           = $this->getProp();
		$onedata['debet']          = getSettings($db, "smis-rs-accounting-debet-tambahan_biaya-gizi", "");
		$onedata['kredit']         = getSettings($db, "smis-rs-accounting-kredit-tambahan_biaya-gizi", "");
        $onedata ['urjigd'] = "URI";
        $onedata ['tanggal_tagihan'] = $d->tanggal;
        $this->content[]           = $onedata;
        return $this->content;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
}

?>