<?php 

class PesananSynchronousAdapter extends SynchronousSenderAdapter{
    private $content;
    public function __construct(){
        parent::__construct();
        $this->content=array();
    }
    
    public function adapt($d){
        $result                    = array();
        $onedata['waktu']          = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata['nama']           = "Pesanan Makanan - ".$d->nama_pasien." - ".$d->noreg_pasien;
		$onedata['biaya']          = ($d->menu_pagi_tarif + $d->menu_siang_tarif + $d->menu_malam_tarif) + ($d->menu_penunggu_tarif * $d->jumlah_menu_penunggu);
		$onedata['jumlah']         = 1;
		$onedata['start']          = $d->tanggal;
		$onedata['end']            = $d->tanggal;
		$onedata['id']             = $d->id;
		$onedata['keterangan']     = "Biaya Pesanan Makanan Pasien " . $d->nama_pasien . " Senilai " . ArrayAdapter::format ( "only-money Rp.", $onedata['biaya'] );
		$onedata['prop']           = $this->getProp();
		$onedata['debet']          = "-";
        $onedata['kredit']         = "";
        $onedata ['urjigd'] = "URI";
        $onedata ['tanggal_tagihan'] = $d->tanggal;
        $this->content[]           = $onedata;
        return $this->content;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
}

?>