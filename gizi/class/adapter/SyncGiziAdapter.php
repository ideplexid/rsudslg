<?php 
/**
 * this adapter used for convert data from rawat
 * the put it on gizi model
 * 
 * @author      : Nurul Huda
 * @license     : LGPLv3
 * @copyright   : goblooge@gmail.com
 * @since       : 3 Feb 2017
 * @database    : smis_gz_pesanan
 */
class SyncGiziAdapter extends ArrayAdapter {
    public function adapt($d) {
        $a = array ();
        $a ['noreg_pasien'] = $d ['noreg_pasien'];
        $a ['nama_pasien'] = $d ['nama_pasien'];
        $a ['nrm_pasien'] = $d ['nrm_pasien'];
        $a ['diet_pagi'] = $d ['diet_pagi'];
        $a ['diet_siang'] = $d ['diet_siang'];
        $a ['diet_malam'] = $d ['diet_malam'];			
        $a ['menu_pagi'] = $d ['menu_pagi'];
        $a ['menu_siang'] = $d ['menu_siang'];
        $a ['menu_malam'] = $d ['menu_malam'];
        $a ['pk_pagi'] = $d ['pk_pagi'];
        $a ['pk_siang'] = $d ['pk_siang'];
        $a ['pk_malam'] = $d ['pk_malam'];
        $a ['ruang'] = $d ['entity'];
        $a ['ibu_kandung'] = $d ['ibu_kandung'];
        $a ['cara_keluar'] = $d ['cara_keluar'];
        $a ['bed'] = $d ['bed'];
        $a ['umur'] = $d ['umur'];
        $a ['golongan_umur'] = $d ['golongan_umur'];
        $a ['tanggal'] = date ( 'Y-m-d' );
        $a ['selesai'] = $d ['selesai'];
        $a ['code'] = md5 ( $a ['tanggal'] . " " . $a ['noreg_pasien'] . " " . $a ['ruang'] );
        return $a;
    }
}

?>