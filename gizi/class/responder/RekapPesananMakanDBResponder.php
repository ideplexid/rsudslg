<?php
class RekapPesananMakanDBResponder extends DBResponder {
	public function command($command) {
		if ($command != "archive_all" && $command != "print_all_tag" && $command != "print_all_bon" && $command != "print_tag")
			return parent::command($command);
		$response_package = new ResponsePackage();
		if ($command == "archive_all") {
			$content = $this->archiveAll();
			$response_package->setContent($content);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
			$response_package->setAlertContent("Proses Berhasil", "Data Telah Diarsipkan", ResponsePackage::$TIPE_INFO);
			$response_package->setAlertVisible(true);
		} else if ($command == "print_all_tag") {
			$content = $this->printAllTag();
			$response_package->setContent($content);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($command == "print_tag") {
			$content = $this->printTag();
			$response_package->setContent($content);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($command == "print_all_bon") {
			$content = $this->printAllBon();
			$response_package->setContent($content);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		}
		return $response_package->getPackage();
	}

	private function archiveAll() {
		$tanggal 			= 	$_POST['tanggal'];
		$ruang 				= 	$_POST['ruang'];
		$this->dbtable->query("
			UPDATE 
				smis_gz_pesanan
			SET 
				selesai = '1'
			WHERE 
				tanggal = '" . $tanggal . "' AND
				ruang LIKE '" . $ruang . "'
		");

		$data['type'] 		= 	"update";
		$data['id']			=	json_encode(
									array(
										'tanggal' 	=> $tanggal, 
										'ruang' 	=> $ruang
									)
								);
		$data['success'] 	= 1;
		return $data;
	}

	private function printAllTag() {
		require_once ("smis-libs-out/php-excel/PHPExcel.php");
		$tanggal = $_POST['tanggal'];
		$ruang = $_POST['ruang'];
		$waktu = $_POST['waktu'];

		$rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_gz_pesanan
			WHERE tanggal = '" . $tanggal . "' AND ruang LIKE '" . $ruang . "'
		");

		if (getSettings($this->dbtable->get_db(), "gizi-label-jenis_luaran", "html") == "xls") {
			$file = new PHPExcel();
			$file->getProperties()->setCreator("PT. Inovasi Ide Utama");
			$file->getProperties()->setTitle("Label Gizi");
			$file->getProperties()->setSubject("Label Gizi");
			$file->getProperties()->setDescription("Label Gizi");
			$file->getProperties()->setKeywords("Label Gizi");
			$file->getProperties()->setCategory("Label Gizi");

			$sheet = $file->getActiveSheet();
			$sheet->setTitle("Label Gizi");

			if ($rows != null) {
				for ($i = 0; $i < count($rows); $i++) {
					$border_start = 2 + ($i * 10);
					$border_end = 10 + ($i * 10);

					$tanggal_lahir = "-";
					$unit_service = new ServiceConsumer(
				        $this->dbtable->get_db(), 
				        "get_biodata", 
				        array(
				        	"nrm_pasien" => $rows[$i]->nrm_pasien
				        ), 
				        "registration"
				    );
				    $content = $unit_service->execute()->getContent();
				    if ($content != null)
				    	$tanggal_lahir = ArrayAdapter::format("date d-m-Y", $content['tgl_lahir']);

					$sheet->setCellValue("A" . ($border_start + 2), substr($rows[$i]->nama_pasien, 0, 10));
					$sheet->setCellValue("D" . ($border_start + 2), $rows[$i]->nrm_pasien);
					$sheet->setCellValue("A" . ($border_start + 3), $tanggal_lahir);
					$sheet->setCellValue("A" . ($border_start + 4), ArrayAdapter::format("unslug", $rows[$i]->ruang));

					$id_menu = "";
					$keterangan = "Baik dimakan sebelum jam N/A";
					$jenis_karbohidrat = "N/A";
					$jenis_menu = "N/A";
					$jenis_diet = $rows[$i]->jenis_diet;
					$kalori = "0 kal";
					if ($waktu == "Pagi") {
						$id_menu = $rows[$i]->id_menu_pagi;
						$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_pagi", "Baik dimakan sebelum jam 08.00");
						$jenis_karbohidrat = $rows[$i]->pk_pagi;
						$jenis_menu_row = $this->dbtable->get_row("
							SELECT a.energi, b.jenis_menu
							FROM smis_gz_menu a INNER JOIN smis_gz_jenis_menu b ON a.id_jenis_menu = b.id
							WHERE a.id = '" . $rows[$i]->id_menu_pagi . "'
						");
						if ($jenis_menu_row != null) {
							$jenis_menu = $jenis_menu_row->jenis_menu;
							$kalori = $jenis_menu_row->energi . " kal";
						}
					} else if ($waktu == "Siang") {
						$id_menu = $rows[$i]->id_menu_siang;
						$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_siang", "Baik dimakan sebelum jam 13.00");
						$jenis_karbohidrat = $rows[$i]->pk_siang;
						$jenis_menu_row = $this->dbtable->get_row("
							SELECT a.energi, b.jenis_menu
							FROM smis_gz_menu a INNER JOIN smis_gz_jenis_menu b ON a.id_jenis_menu = b.id
							WHERE a.id = '" . $rows[$i]->id_menu_siang . "'
						");
						if ($jenis_menu_row != null) {
							$jenis_menu = $jenis_menu_row->jenis_menu;
							$kalori = $jenis_menu_row->energi . " kal";
						}
					} else if ($waktu == "Malam") {
						$id_menu = $rows[$i]->id_menu_malam;
						$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_malam", "Baik dimakan sebelum jam 19.00");
						$jenis_karbohidrat = $rows[$i]->pk_malam;
						$jenis_menu_row = $this->dbtable->get_row("
							SELECT a.energi, b.jenis_menu
							FROM smis_gz_menu a INNER JOIN smis_gz_jenis_menu b ON a.id_jenis_menu = b.id
							WHERE a.id = '" . $rows[$i]->id_menu_malam . "'
						");
						if ($jenis_menu_row != null) {
							$jenis_menu = $jenis_menu_row->jenis_menu;
							$kalori = $jenis_menu_row->energi . " kal";
						}
					}

					$sheet->setCellValue("A" . ($border_start + 5), $jenis_menu . " - " . $jenis_diet);
					$sheet->setCellValue("D" . ($border_start + 5), $kalori);
					$sheet->mergeCells("A" . ($border_start + 7) . ":E" . ($border_start + 7))->setCellValue("A" . ($border_start + 7), $keterangan);
					$sheet->getStyle("A" . ($border_start + 7))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

					$style = array();
					$style['borders'] = array();
					$style['borders']['outline'] = array();
					$style['borders']['outline']['style'] = PHPExcel_Style_Border::BORDER_THICK;
					$sheet->getStyle("A" . $border_start . ":E" . $border_end)->applyFromArray($style);
					$sheet->getStyle("A" . $border_start . ":E" . $border_end)->getFont()->setSize(10);
				}
			}
			unset($style);
			$sheet->getColumnDimension("A")->setWidth(5.42);
			$sheet->getColumnDimension("B")->setWidth(5.56);
			$sheet->getColumnDimension("C")->setWidth(5.70);
			$sheet->getColumnDimension("D")->setWidth(7.13);
			$sheet->getColumnDimension("E")->setWidth(5.70);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Label Gizi.xls"');
			header('Cache-Control: max-age=0');
			$writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
			$writer->save('php://output');
		} else {
			$header_1 = getSettings($this->dbtable->get_db(), "gizi-label-header_1", "Rumah Sakit Umum");
			$header_2 = getSettings($this->dbtable->get_db(), "gizi-label-header_2", "Inovasi Ide Utama");
			$header_3 = getSettings($this->dbtable->get_db(), "gizi-label-header_3", "Jl. Sultan Agung No. 1");
			$header_4 = getSettings($this->dbtable->get_db(), "gizi-label-header_4", "Surabaya");
			$height = getSettings($this->dbtable->get_db(), "gizi-label-height", "60");
			$width = getSettings($this->dbtable->get_db(), "gizi-label-width", "70");

			$html = "";
			if ($rows != null) {
				foreach ($rows as $row) {
					$jenis_karbohidrat = "N/A";
					$jenis_diet = $row->jenis_diet;
					$keterangan = "";
					if ($waktu == "Pagi") {
						$jenis_karbohidrat = $row->pk_pagi;
						$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_pagi", "Baik dimakan sebelum jam 08.00");
					} else if ($waktu == "Siang") {
						$jenis_karbohidrat = $row->pk_siang;
						$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_siang", "Baik dimakan sebelum jam 13.00");
					} else if ($waktu == "Malam") {
						$jenis_karbohidrat = $row->pk_malam;
						$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_malam", "Baik dimakan sebelum jam 19.00");
					}

					$html .= "
						<table border='1' style='width: " . $width . "mm; height: " . $height . "mm;' cellpadding='5mm' cellspacing='0'>
							<tr>
								<td valign='top' align='center'>
									<table border='0' style='width: " . $width . "mm; height: " . $height . "mm;' cellpadding='0' cellspacing='0'>
										<tr>
											<td align='center' rowspan='4' valign='center'><img width='100px' height='100px' src='" . get_fileurl(getSettings($this->dbtable->get_db(), "smis_autonomous_logo", "")) . "'/></td>
											<td colspan='2'><center><strong>" . $header_1 . "</strong></center></td>
										</tr>
										<tr>
											<td colspan='2'><center><strong>" . $header_2 . "</strong></center></td>
										</tr>
										<tr>
											<td colspan='2'><center><strong>" . $header_3 . "</strong></center></td>
										</tr>
										<tr>
											<td colspan='2'><center><strong>" . $header_4 . "</strong></center></td>
										</tr>
										<tr>
											<td colspan='3'></td>
										</tr>
										<tr>
											<td colspan='3'></td>
										</tr>
										<tr>
											<td colspan='3'></td>
										</tr>
										<tr>
											<td width='30%'>Tanggal / Jam</td>
											<td width='5%'>:</td>
											<td width='65%'>" . ArrayAdapter::format("date d-m-Y", $row->tanggal) . " / " . $waktu . "</td>
										</tr>
										<tr>
											<td width='30%'>Nama</td>
											<td width='5%'>:</td>
											<td width='65%'>" . $row->nama_pasien . "</td>
										</tr>
										<tr>
											<td width='30%'>NRM</td>
											<td width='5%'>:</td>
											<td width='65%'>" . $row->nrm_pasien . "</td>
										</tr>
										<tr>
											<td width='30%'>Kelas</td>
											<td width='5%'>:</td>
											<td width='65%'>" . ArrayAdapter::format("unslug", $row->kelas) . "</td>
										</tr>
										<tr>
											<td width='30%'>Kamar/Bed</td>
											<td width='5%'>:</td>
											<td width='65%'>" . ArrayAdapter::format("unslug", $row->ruang) . " / " . $row->bed . "</td>
										</tr>
										<tr>
											<td width='30%'>Jenis Pelayanan</td>
											<td width='5%'>:</td>
											<td width='65%'>" . ArrayAdapter::format("unslug", $row->jenis_pasien). "</td>
										</tr>
										<tr>
											<td width='30%'>Diet</td>
											<td width='5%'>:</td>
											<td width='65%'>" . $jenis_diet . "</td>
										</tr>
										<tr>
											<td width='30%'>Bentuk Makanan</td>
											<td width='5%'>:</td>
											<td width='65%'>" . $jenis_karbohidrat . "</td>
										</tr>
										<tr>
											<td width='30%'>Keterangan</td>
											<td width='5%'>:</td>
											<td width='65%'>" . $keterangan . "</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br/>
					";
				}
			}
			return $html;
		}
	}

	private function printTag() {
		require_once ("smis-libs-out/php-excel/PHPExcel.php");
		$id = $_POST['id'];
		$waktu = $_POST['waktu'];

		$row = $this->dbtable->get_row("
			SELECT *
			FROM smis_gz_pesanan
			WHERE id = '" . $id . "'
		");

		if (getSettings($this->dbtable->get_db(), "gizi-label-jenis_luaran", "html") == "xls") {
			$file = new PHPExcel();
			$file->getProperties()->setCreator("PT. Inovasi Ide Utama");
			$file->getProperties()->setTitle("Label Gizi");
			$file->getProperties()->setSubject("Label Gizi");
			$file->getProperties()->setDescription("Label Gizi");
			$file->getProperties()->setKeywords("Label Gizi");
			$file->getProperties()->setCategory("Label Gizi");

			$sheet = $file->getActiveSheet();
			$sheet->setTitle("Label Gizi");

			if ($row != null) {
				$border_start = 2 + ($i * 10);
				$border_end = 10 + ($i * 10);

				$tanggal_lahir = "-";
				$unit_service = new ServiceConsumer(
			        $this->dbtable->get_db(), 
			        "get_biodata", 
			        array(
			        	"nrm_pasien" => $row->nrm_pasien
			        ), 
			        "registration"
			    );
			    $content = $unit_service->execute()->getContent();
			    if ($content != null)
			    	$tanggal_lahir = ArrayAdapter::format("date d-m-Y", $content['tgl_lahir']);

				$sheet->setCellValue("A" . ($border_start + 2), substr($row->nama_pasien, 0, 10));
				$sheet->setCellValue("D" . ($border_start + 2), $row->nrm_pasien);
				$sheet->setCellValue("A" . ($border_start + 3), $tanggal_lahir);
				$sheet->setCellValue("A" . ($border_start + 4), ArrayAdapter::format("unslug", $row->ruang));

				$id_menu = "";
				$keterangan = "Baik dimakan sebelum jam N/A";
				$jenis_karbohidrat = "N/A";
				$jenis_diet = $row->jenis_diet;
				if ($waktu == "Pagi") {
					$id_menu = $row->id_menu_pagi;
					$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_pagi", "Baik dimakan sebelum jam 08.00");
					$jenis_karbohidrat = $row->pk_pagi;
				} else if ($waktu == "Siang") {
					$id_menu = $row->id_menu_siang;
					$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_siang", "Baik dimakan sebelum jam 13.00");
					$jenis_karbohidrat = $row->pk_siang;
				} else if ($waktu == "Malam") {
					$id_menu = $row->id_menu_malam;
					$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_malam", "Baik dimakan sebelum jam 19.00");
					$jenis_karbohidrat = $row->pk_malam;
				}
				$jenis_menu = "N/A";
				$kalori = "0 kal";

				if ($id_menu != "") {
					$menu_row = $this->dbtable->get_row("
						SELECT a.energi, b.jenis_menu
						FROM smis_gz_menu a INNER JOIN smis_gz_jenis_menu b ON a.id_jenis_menu = b.id
						WHERE a.id = '" . $id_menu . "'
					");
					if ($menu_row != null) {
						$jenis_menu = $menu_row->jenis_menu;
						$kalori = $menu_row->energi . " kal";
					}
				}

				$sheet->setCellValue("A" . ($border_start + 5), $jenis_menu . " - " . $jenis_diet);
				$sheet->setCellValue("D" . ($border_start + 5), $kalori);
				$sheet->mergeCells("A" . ($border_start + 7) . ":E" . ($border_start + 7))->setCellValue("A" . ($border_start + 7), $keterangan);
				$sheet->getStyle("A" . ($border_start + 7))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$style = array();
				$style['borders'] = array();
				$style['borders']['outline'] = array();
				$style['borders']['outline']['style'] = PHPExcel_Style_Border::BORDER_THICK;
				$sheet->getStyle("A" . $border_start . ":E" . $border_end)->applyFromArray($style);
				$sheet->getStyle("A" . $border_start . ":E" . $border_end)->getFont()->setSize(10);
			}
			unset($style);
			$sheet->getColumnDimension("A")->setWidth(5.42);
			$sheet->getColumnDimension("B")->setWidth(5.56);
			$sheet->getColumnDimension("C")->setWidth(5.70);
			$sheet->getColumnDimension("D")->setWidth(7.13);
			$sheet->getColumnDimension("E")->setWidth(5.70);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Label Gizi.xls"');
			header('Cache-Control: max-age=0');
			$writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
			$writer->save('php://output');
		} else {

			require_once ("smis-base/smis-include-service-consumer.php");
			$header     = array ();
			$adapter    = new SimpleAdapter ();
			$uitable    = new Table ( $header );
			$responder  = new ServiceResponder ( $db, $uitable, $adapter, "get_registered" );
			$responder  ->addData("id", $row->noreg_pasien);
			$responder  ->addData("command", "edit");
			$data       = $responder->command ( "edit" );
			$px         = $data['content'];

			$header_1 = getSettings($this->dbtable->get_db(), "gizi-label-header_1", "Rumah Sakit Umum");
			$header_2 = getSettings($this->dbtable->get_db(), "gizi-label-header_2", "Inovasi Ide Utama");
			$header_3 = getSettings($this->dbtable->get_db(), "gizi-label-header_3", "Jl. Sultan Agung No. 1");
			$header_4 = getSettings($this->dbtable->get_db(), "gizi-label-header_4", "Surabaya");
			$height = getSettings($this->dbtable->get_db(), "gizi-label-height", "60");
			$width = getSettings($this->dbtable->get_db(), "gizi-label-width", "70");
			$logo_height = getSettings($this->dbtable->get_db(), "gizi-label-logo_height", "50");
			$logo_width = getSettings($this->dbtable->get_db(), "gizi-label-logo_width", "50");

			$jenis_karbohidrat = "";
			$keterangan = "";
			if ($waktu == "Pagi") {
				$id_menu = $row->id_menu_pagi;
				$jenis_karbohidrat = $row->pk_pagi;
				$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_pagi", "Baik dimakan sebelum jam 08.00");
			} else if ($waktu == "Siang") {
				$id_menu = $row->id_menu_siang;
				$jenis_karbohidrat = $row->pk_siang;
				$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_siang", "Baik dimakan sebelum jam 13.00");
			} else if ($waktu == "Malam") {
				$id_menu = $row->id_menu_malam;
				$jenis_karbohidrat = $row->pk_malam;
				$keterangan = getSettings($this->dbtable->get_db(), "gizi-label-keterangan_malam", "Baik dimakan sebelum jam 19.00");
			}

			$jenis_menu = "N/A";
			$jenis_diet = $row->jenis_diet;
			$kalori = "0 kal";

			if ($id_menu != "") {
				$menu_row = $this->dbtable->get_row("
					SELECT a.energi, b.jenis_menu
					FROM smis_gz_menu a INNER JOIN smis_gz_jenis_menu b ON a.id_jenis_menu = b.id
					WHERE a.id = '" . $id_menu . "'
				");
				if ($menu_row != null) {
					$jenis_menu = $menu_row->jenis_menu;
					$kalori = $menu_row->energi . " kal";
				}
			}
			$ruangan = ArrayAdapter::format("unslug", $row->kelas) . " - ". ArrayAdapter::format("unslug", $row->ruang) ."(". $row->bed .")";
			$ruangan = str_replace("RUANGAN","",strtoupper($ruangan));
			$ruangan = trim(str_replace("BED ","",$ruangan));
			$umur = $px['umur'];
			if(strpos($umur,"VALID")!==false){
				$umur = "x";
			}else{
				$u = explode(" ",trim($umur) );
				$umur = $u[0]." ".$u[1];
			}


			$html .= "
				<table border='1' style='width: " . $width . "mm; height: " . $height . "mm;' cellpadding='5mm' cellspacing='0'>
					<tr>
						<td valign='top' align='center'>
							<table border='0' id='tags-table' style='width: " . $width . "mm; ' cellpadding='0' cellspacing='0'>
								<tr>
									<td align='center' valign='center'><img width='100px' height='auto' src='" . get_fileurl(getSettings($this->dbtable->get_db(), "smis_autonomous_logo", "")) . "'/></td>
									<td >" . ArrayAdapter::format("date d-m-Y", $row->tanggal)  ."(". $waktu. ")</td>
								</tr>

								
								<tr><td class='center' colspan='2' >&nbsp;</td></tr>
								<tr><td class='center' colspan='2' >". $row->nama_pasien."</td></tr>
								<tr><td class='center' colspan='2' >(". ArrayAdapter::format("only-digit8",$row->nrm_pasien).")</td></tr>
								<tr><td class='center' colspan='2' >&nbsp;</td></tr>
								<tr><td colspan='2' >TL : ". ArrayAdapter::format("date d-m-Y", $px['tgl_lahir'])." ($umur)</td></tr>
								<tr><td colspan='2' >Alergi : ".  $row->alergi."</td></tr>
								<tr><td colspan='2' >Diet : ".  $jenis_diet ."</td></tr>
								<tr><td colspan='2' >Bentuk Makanan : ".  $jenis_karbohidrat ."</td></tr>
								<tr><td colspan='2'>" .$ruangan. "</td></tr>
								<tr><td class='center' colspan='2' >".  $keterangan  ."</td></tr>
							</table>
						</td>
					</tr>
				</table>
				</br>
			";
			return $html.$html.$html;
		}
	}

	private function printAllBon() {
		require_once ("smis-libs-out/php-excel/PHPExcel.php");
		$tanggal = $_POST['tanggal'];
		$ruang = $_POST['ruang'];
		$waktu = $_POST['waktu'];

		$rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_gz_pesanan
			WHERE tanggal = '" . $tanggal . "' AND ruang LIKE '" . $ruang . "'
		");

		$file = new PHPExcel();
		$file->getProperties()->setCreator("PT. Inovasi Ide Utama");
		$file->getProperties()->setTitle("Label Gizi");
		$file->getProperties()->setSubject("Label Gizi");
		$file->getProperties()->setDescription("Label Gizi");
		$file->getProperties()->setKeywords("Label Gizi");
		$file->getProperties()->setCategory("Label Gizi");

		$sheet = $file->getActiveSheet();
		$sheet->setTitle("Bon Pesanan Gizi");

		$sheet->setCellValue("A1", getSettings($this->dbtable->get_db(), "smis_autonomous_title", "RUMAH SAKIT"));
		$sheet->getStyle("A1")->getFont()->setBold(true);
		$sheet->setCellValue("A2", getSettings($this->dbtable->get_db(), "smis_autonomous_address", "Jl. Indonesia Raya, No. 1"));
		$sheet->getStyle("A2")->getFont()->setBold(true);
		$sheet->setCellValue("A3", "");
		$sheet->getStyle("A3")->getFont()->setBold(true);
		$sheet->mergeCells("A5:J5")->setCellValue("A5", "Daftar Pesanan/Pengiriman Diet Pasien");
		$sheet->getStyle("A5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A5")->getFont()->setBold(true);
		$sheet->mergeCells("A6:J6")->setCellValue("A6", "Instalasi Gizi " . getSettings($this->dbtable->get_db(), "smis_autonomous_title", "RUMAH SAKIT"));
		$sheet->getStyle("A6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A6")->getFont()->setBold(true);
		$sheet->setCellValue("A8", "Tanggal : " . ArrayAdapter::format("date d-m-Y", $tanggal));
		$sheet->setCellValue("A9", "Waktu Makan : " . $waktu);
		$sheet->setCellValue("I9", "Ruang : " . ArrayAdapter::format("unslug", $ruang));
		$sheet->getStyle("A1:J9")->getFont()->setSize(10);

		$border_start = 11;
		$row_index = $border_start;
		$sheet->setCellValue("A" . $row_index, "No.");
		$sheet->getStyle("A" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A" . $row_index)->getFont()->setBold(true);
		$sheet->setCellValue("B" . $row_index, "Nama & NRM");
		$sheet->getStyle("B" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("B" . $row_index)->getFont()->setBold(true);
		$sheet->setCellValue("C" . $row_index, "Kamar");
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("C" . $row_index)->getFont()->setBold(true);
		$sheet->mergeCells("D" . $row_index . ":H" . $row_index)->setCellValue("D" . $row_index, "Diet");
		$sheet->getStyle("D" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("D" . $row_index)->getFont()->setBold(true);
		$sheet->setCellValue("I" . $row_index, "Kelas");
		$sheet->getStyle("I" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("I" . $row_index)->getFont()->setBold(true);
		$sheet->setCellValue("J" . $row_index, "Terkirim");
		$sheet->getStyle("J" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("J" . $row_index)->getFont()->setBold(true);
		$row_index++;
		
		if ($rows != null) {
			$nomor = 1;
			for ($i = 0; $i < count($rows); $i++) {
				$sheet->setCellValue("A" . $row_index, $nomor++);
				$sheet->setCellValue("B" . $row_index, $rows[$i]->nama_pasien);
				$sheet->setCellValue("C" . $row_index, ArrayAdapter::format("unslug", $rows[$i]->ruang));

				$id_menu = "";
				$jenis_karbohidrat = "N/A";
				if ($waktu == "Pagi") {
					$id_menu = $rows[$i]->id_menu_pagi;
					$jenis_karbohidrat = $rows[$i]->pk_pagi;
				} else if ($waktu == "Siang") {
					$id_menu = $rows[$i]->id_menu_siang;
					$jenis_karbohidrat = $rows[$i]->pk_siang;
				} else if ($waktu == "Malam") {
					$id_menu = $rows[$i]->id_menu_malam;
					$jenis_karbohidrat = $rows[$i]->pk_malam;
				}
				$jenis_menu = "N/A";

				if ($id_menu != "") {
					$menu_row = $this->dbtable->get_row("
						SELECT jenis_menu
						FROM smis_gz_menu
						WHERE id = '" . $id_menu . "'
					");
					if ($menu_row != null)
						$jenis_menu = $menu_row->jenis_menu;
				}

				$sheet->mergeCells("D" . $row_index . ":H" . $row_index)->setCellValue("D" . $row_index, $jenis_menu . " - " . $jenis_karbohidrat);
				$sheet->setCellValue("I" . $row_index, ArrayAdapter::format("unslug", $rows[$i]->kelas));
				$sheet->setCellValue("J" . $row_index, "");
				$row_index++;
			}
			$sheet->mergeCells("A" . $row_index . ":H" . $row_index)->setCellValue("A" . $row_index, "Subtotal");
			$sheet->getStyle("A" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("A" . $row_index)->getFont()->setBold(true);
			$sheet->setCellValue("I" . $row_index, count($rows));
			$border_end = $row_index;
			$style = array();
			$style['borders'] = array();
			$style['borders']['allborders'] = array();
			$style['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
			$sheet->getStyle("A" . $border_start . ":J" . $border_end)->applyFromArray($style);
			$sheet->getStyle("A" . $border_start . ":J" . $border_end)->getFont()->setSize(10);
			unset($style);
			$row_index += 2;
		}

		$border_start = $row_index;
		$sheet->mergeCells("C" . $row_index . ":H" . $row_index)->setCellValue("C" . $row_index, "Serah Terima Diet Pasien " . date("d-m-Y H:i:s"));
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$row_index++;
		$sheet->mergeCells("C" . $row_index . ":H" . $row_index)->setCellValue("C" . $row_index, "(TTD + Nama Terang)");
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$row_index++;
		$sheet->mergeCells("C" . $row_index . ":H" . $row_index)->setCellValue("C" . $row_index, "Pemeriksa (Petugas Gizi)");
		$sheet->getRowDimension($row_index)->setRowHeight(48.00);
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
		$row_index++;
		$sheet->mergeCells("C" . $row_index . ":H" . $row_index)->setCellValue("C" . $row_index, "Pengirim (Petugas Distribusi)");
		$sheet->getRowDimension($row_index)->setRowHeight(48.00);
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
		$row_index++;
		$sheet->mergeCells("C" . $row_index . ":H" . $row_index)->setCellValue("C" . $row_index, "Penerima (Petugas R.Inap)                         Jam :");
		$sheet->getRowDimension($row_index)->setRowHeight(48.00);
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$sheet->getStyle("C" . $row_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
		$border_end = $row_index;
		$style = array();
		$style['borders'] = array();
		$style['borders']['allborders'] = array();
		$style['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
		$sheet->getStyle("C" . $border_start . ":H" . $border_end)->applyFromArray($style);
		$sheet->getStyle("C" . $border_start . ":H" . $border_end)->getFont()->setSize(10);
		unset($style);

		$sheet->getColumnDimension("A")->setWidth(4.43);
		$sheet->getColumnDimension("B")->setWidth(18.29);
		$sheet->getColumnDimension("C")->setWidth(6.57);
		$sheet->getColumnDimension("D")->setWidth(8.29);
		$sheet->getColumnDimension("E")->setWidth(8.29);
		$sheet->getColumnDimension("F")->setWidth(8.29);
		$sheet->getColumnDimension("G")->setWidth(8.29);
		$sheet->getColumnDimension("H")->setWidth(8.29);
		$sheet->getColumnDimension("I")->setWidth(14.86);
		$sheet->getColumnDimension("J")->setWidth(8.29);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Bon Pesanan Gizi.xls"');
		header('Cache-Control: max-age=0');
		$writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
		$writer->save('php://output');
	}



	//print rekapitulasi
	public function excel() {
		require_once ("smis-libs-out/php-excel/PHPExcel.php");
		$this->getDBTable()->SetShowALl(true);
		$dt = $this->getDBTable()->view("","0");
	
		$list = $dt['data'];
		$file = new PHPExcel();
		$file->getProperties()->setCreator("PT. Inovasi Ide Utama");

		$sheet = $file->getActiveSheet();
		$sheet->setTitle("Laporan Pesanan Makanan");

		$sheet->mergeCells("A1:N1")->setCellValue("A1","LAPORAN PESANAN MAKANAN PASIEN");
		$sheet->mergeCells("A2:N2")->setCellValue("A2","TANGGAL : ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

		$sheet->mergeCells("A4:A5")->setCellValue("A4","No.");
		$sheet->mergeCells("B4:B5")->setCellValue("B4","Ruangan");
		$sheet->mergeCells("C4:C5")->setCellValue("C4","Bed");
		$sheet->mergeCells("D4:D5")->setCellValue("D4","Nama Pasien");
		$sheet->mergeCells("E4:E5")->setCellValue("E4","NRM");
		$sheet->mergeCells("F4:F5")->setCellValue("F4","No. Reg");

		$sheet->mergeCells("G4:H4")->setCellValue("G4","Pagi");
		$sheet->setCellValue("G5","Jenis Menu");
		$sheet->setCellValue("H5","Bentuk Makanan");
		
		$sheet->mergeCells("I4:J4")->setCellValue("I4","Siang");
		$sheet->setCellValue("I5","Jenis Menu");
		$sheet->setCellValue("J5","Bentuk Makanan");

		$sheet->mergeCells("K4:L4")->setCellValue("K4","Malam");
		$sheet->setCellValue("K5","Jenis Menu");
		$sheet->setCellValue("L5","Bentuk Makanan");
		
		$sheet->mergeCells("M4:M5")->setCellValue("M4","Status");
		$sheet->mergeCells("N4:N5")->setCellValue("N4","Alergi");
		$no = 0;
		$row = 5;


			$sheet->setCellValue("A3","     ");
			$sheet->setCellValue("B3","     ");
			$sheet->setCellValue("C3","     ");
			$sheet->setCellValue("D3","     ");
			$sheet->setCellValue("E3","     ");
			$sheet->setCellValue("F3","     ");
			$sheet->setCellValue("G3","     ");
			$sheet->setCellValue("H3","     ");
			$sheet->setCellValue("I3","     ");
			$sheet->setCellValue("J3","     ");
			$sheet->setCellValue("K3","     ");
			$sheet->setCellValue("L3","     ");
			$sheet->setCellValue("M3","     ");
			$sheet->setCellValue("N3","     ");

		foreach($list as $x){
			$no++;
			$row++;
			$sheet->setCellValue("A".$row,$no.". ");
			$sheet->setCellValue("B".$row,ArrayAdapter::format("unslug",$x->ruang));
			$sheet->setCellValue("C".$row,$x->bed);
			$sheet->setCellValue("D".$row,$x->nama_pasien);
			$sheet->setCellValue("E".$row,"'".$x->nrm_pasien);
			$sheet->setCellValue("F".$row,"'".$x->noreg_pasien);
			$sheet->setCellValue("G".$row,$x->menu_pagi);
			$sheet->setCellValue("H".$row,$x->pk_pagi);
			$sheet->setCellValue("I".$row,$x->menu_siang);
			$sheet->setCellValue("J".$row,$x->pk_siang);
			$sheet->setCellValue("K".$row,$x->menu_malam);
			$sheet->setCellValue("L".$row,$x->pk_malam);
			$sheet->setCellValue("M".$row,$x->status==1?"Diarsipkan":"");
			$sheet->setCellValue("N".$row,$x->alergi);
		}
		$sheet->getStyle("A1:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A1:N5")->getFont()->setBold(true);
		
		foreach(range('A','N') as $columnID) {
			$sheet->getColumnDimension($columnID)
				->setAutoSize(true);
		}

		$fillcabang = array();
        $fillcabang['borders'] = array();
        $fillcabang['borders']['allborders'] = array();
        $fillcabang['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

		$sheet->getStyle ( "A4:N".($row) )->applyFromArray ( $fillcabang );
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Rekap Pesanan Makanan Pasien.xls"');
		header('Cache-Control: max-age=0');
		$writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
		$writer->save('php://output');
	}
}
?>
