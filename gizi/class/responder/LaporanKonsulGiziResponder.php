<?php

class LaporanKonsulGiziResponder extends DBResponder{
    public function excel(){
        require_once "smis-libs-out/php-excel/PHPExcel.php";
        $fillcabang = array();
        $fillcabang['borders'] = array();
        $fillcabang['borders']['allborders'] = array();
        $fillcabang['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;
       
        $file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
        $sheet  ->setTitle ("Laporan Konsul Gizi");

        $this->getDBTable()->SetShowAll(true);
        $data = $this->getDBTable()->view("","0");
        $list = $data['data'];

        $fix = $this->adapter->getContent($list);

        $sheet->mergeCells("A1:P1")->setCellValue ( "A1", "Laporan Konsul Gizi Pasien" );
        $sheet->mergeCells("A2:P2")->setCellValue ( "A2", "Tanggal : ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']) );
        
        $sheet->setCellValue("A4","Tanggal");
        $sheet->setCellValue("B4","Dokter");
        $sheet->setCellValue("C4","Ahli Gizi");
        $sheet->setCellValue("D4","Kode Diagnosa Gizi");
        $sheet->setCellValue("E4","Diagnosa Gizi");
        $sheet->setCellValue("F4","Pasien");
        $sheet->setCellValue("G4","Jenis Kelamin");
        $sheet->setCellValue("H4","Tanggal Lahir");
        $sheet->setCellValue("I4","Status Gizi");
        $sheet->setCellValue("J4","Jenis Diet");
        $sheet->setCellValue("K4","Antropometri");
        $sheet->setCellValue("L4","Konsul Gizi");
        $sheet->setCellValue("M4","Kelas");
        $sheet->setCellValue("N4","Biaya");
        $sheet->setCellValue("O4","Keterangan");
        $sheet->setCellValue("P4","Ruangan");

        $row = 4;
        foreach($fix as $x){
            $row++;
            $sheet->setCellValue("A".$row,$x["Tanggal"]);
            $sheet->setCellValue("B".$row,$x["Dokter"]);
            $sheet->setCellValue("C".$row,$x["Ahli Gizi"]);
            $sheet->setCellValue("D".$row,$x["Kode Diagnosa Gizi"]);
            $sheet->setCellValue("E".$row,$x["Diagnosa Gizi"]);
            $sheet->setCellValue("F".$row,$x["Pasien"]);
            $sheet->setCellValue("G".$row,$x["Jenis Kelamin"]);
            $sheet->setCellValue("H".$row,$x["Tanggal Lahir"]);
            $sheet->setCellValue("I".$row,$x["Status Gizi"]);
            $sheet->setCellValue("J".$row,$x["Jenis Diet"]);
            $sheet->setCellValue("K".$row,$x["Antropometri"]);
            $sheet->setCellValue("L".$row,$x["Konsul Gizi"]);
            $sheet->setCellValue("M".$row,$x["Kelas"]);
            $sheet->setCellValue("N".$row,$x["Biaya"]);
            $sheet->setCellValue("O".$row,$x["Keterangan"]);
            $sheet->setCellValue("P".$row,$x["Ruangan"]);            
        }
        $sheet->getStyle ( "A1:P4")->getFont ()->setBold ( true );
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
    
        $sheet->getStyle("A1:P4")->applyFromArray($style);
        $sheet->getStyle ( 'A4:P'.$row )->applyFromArray ( $fillcabang );
        
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);
        $sheet->getColumnDimension("J")->setAutoSize(true);
        $sheet->getColumnDimension("K")->setAutoSize(true);
        $sheet->getColumnDimension("L")->setAutoSize(true);
        $sheet->getColumnDimension("M")->setAutoSize(true);
        $sheet->getColumnDimension("N")->setAutoSize(true);
        $sheet->getColumnDimension("O")->setAutoSize(true);
        $sheet->getColumnDimension("P")->setAutoSize(true);
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Konsul Gizi Pasien.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );

    }
}

