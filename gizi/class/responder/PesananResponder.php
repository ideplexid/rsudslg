<?php 
class PesananResponder extends DBResponder{
    
    public function command($command){
        if($command=="proceed"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setContent("1");
            $this->proceed();
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    public function proceed(){
        $cek=$_POST['cek'];
        $query="";
       switch($cek){
           case "cek_bed"       : $query="UPDATE smis_gz_pesanan set cetak=1 WHERE tanggal=CURDATE() and bed!='' AND prop!='del' "; break;
           case "cek_nbed"      : $query="UPDATE smis_gz_pesanan set cetak=1 WHERE tanggal=CURDATE() and bed='' AND prop!='del' "; break;
           case "cek_all"       : $query="UPDATE smis_gz_pesanan set cetak=1 WHERE tanggal=CURDATE() AND prop!='del' "; break;
           case "inverse_bed"   : $query="UPDATE smis_gz_pesanan set cetak=(cetak XOR 1) WHERE tanggal=CURDATE() and bed!='' AND prop!='del' "; break;
           case "inverse_nbed"  : $query="UPDATE smis_gz_pesanan set cetak=(cetak XOR 1) WHERE tanggal=CURDATE() and bed='' AND prop!='del' "; break;
           case "inverse_all"   : $query="UPDATE smis_gz_pesanan set  cetak=(cetak XOR 1) WHERE tanggal=CURDATE() AND prop!='del' "; break;
           case "remove_bed"       : $query="UPDATE smis_gz_pesanan set cetak=0 WHERE tanggal=CURDATE() and bed!='' AND prop!='del' "; break;
           case "remove_nbed"      : $query="UPDATE smis_gz_pesanan set cetak=0 WHERE tanggal=CURDATE() and bed='' AND prop!='del' "; break;
           case "remove_all"       : $query="UPDATE smis_gz_pesanan set cetak=0 WHERE tanggal=CURDATE() AND prop!='del' "; break;
           
       }
       $db=$this->getDBTable()->get_db();
       $db->query($query);
       return 1;
    }
    
    public function save(){
		$data=$this->postToArray();
		$id['id']=$this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data);
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
            $data['cetak']="(cetak XOR 1)";
            $warp['cetak']=1;
			$result=$this->dbtable->update($data,$id,$warp);
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}
    
}


?>