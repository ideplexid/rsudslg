<?php

global $db;

class GiziTable extends Table {
    private $db;
    
    public function __construct($db,$header,$title="",$content=NULL,$action=true){
		parent::__construct($header,$title,$content,$action);
		$this->db = $db;
	}
    
    public function getContentButton($id){
		$btn_group=new ButtonGroup('noprint');
		if($id=="" || $id=="0") return $btn_group;
        $query = " SELECT * FROM smis_gz_pesanan WHERE id = '".$id."' ";
        $res = $this->db->get_result($query);
		$btn_group->setMax($this->content_button_max, $this->content_button_default);
		if($this->model==self::$EDIT || $this->model==self::$BOTH || $this->enabled_all_content_button){
			if($this->edit_button_enable){
                if($res[0]->selesai == 0 || $res[0]->selesai == '0' || $res[0]->selesai == NULL) {
                    $btn=new Button($this->name."_edit", "","Edit");
                    $btn->setAction($this->action.".edit('".$id."')");
                    $btn->setClass("btn-warning");
                    $btn->setAtribute("data-content='Edit' data-toggle='popover'");
                    $btn->setIcon("fa fa-pencil");
                    $btn->setIsButton(Button::$ICONIC);
                    $btn_group->addElement($btn);
                }
			}
			
			if($this->delete_button_enable){
				$btn=new Button($this->name."_del", "","Delete");
				$btn->setAction($this->action.".del('".$id."')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Delete' data-toggle='popover'");
				$btn->setIcon("fa fa-trash");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			
			if($this->print_element_button_enable) {
				$btn=new Button($this->name."_printelement", "","Print");
				$btn->setAction($this->action.".printelement('".$id."')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Print' data-toggle='popover'");
				$btn->setIcon("fa fa-print");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			
			if($this->print_element_preview_button_enable) {
				$btn=new Button($this->name."_printpreviewelement", "","Print");
				$btn->setAction($this->action.".print_preview('".$id."')");
				$btn->setClass("btn-info");
				$btn->setAtribute("data-content='Print Preview' data-toggle='popover'");
				$btn->setIcon("fa fa-print");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			
			if($this->excel_element_button_enable) {
				$btn=new Button($this->name."_excel_element", "","Print");
				$btn->setAction($this->action.".excel_element('".$id."')");
				$btn->setClass("btn-succes");
				$btn->setAtribute("data-content='Excel' data-toggle='popover'");
				$btn->setIcon("fa fa-file-excel-o");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
            
            if($this->detail_button_enable){
				$btn=new Button($this->name."_detail", "","Delete");
				$btn->setAction($this->action.".detail('".$id."')");
				$btn->setClass("btn-info");
				$btn->setAtribute("data-content='Detail' data-toggle='popover'");
				$btn->setIcon("fa fa-eye");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			
			foreach($this->content_button as  $slug=>$btn){
				$btn->setAction($this->action.".".$slug."('".$id."')");
				$btn->setId($this->name."_".$slug);
				$btn_group->addElement($btn);
			}
			
		}
		
		if($this->model==self::$SELECT || $this->model==self::$BOTH || $this->enabled_all_content_button){
			$btn=new Button($this->name."_select", "","Select");
			$btn->setAction($this->action.".select('".$id."')");
			$btn->setClass("btn-info");
			$btn->setAtribute("data-content='Pilih' data-toggle='popover'");
			$btn->setIcon("fa fa-check");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		}
		return $btn_group;
	}
}

?>