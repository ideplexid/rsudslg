<?php 

class RincianBelanjaTable extends Table{
	
	
	public function getPrintedElement($p,$f){
		global $db;
		$dbtable=new DBTable($db, "smis_gz_belanja");
		$dbtable->setShowAll(true);
		$dbtable->addCustomKriteria("id_dana ", "='".$p->id."'");
		$data=$dbtable->view("", "0");
		
		$tbl=new TablePrint("daftar_rincian_belanja");
		$tbl->addColumn("<h2>Daftar Rincian Belanja<h2>", 6, 1,NULL,NULL,"center");
		$tbl->commit("title");
		$tbl->addColumn("<h3>No Nota. ".ArrayAdapter::format("only-digit8", $p->id)."<h3>", 6, 1,NULL,NULL,"left");
		$tbl->commit("title");
		$tbl->addColumn("No.", 1, 1,NULL,NULL,"center bold");
		$tbl->addColumn("NAMA", 1, 1,NULL,NULL,"center bold");
		$tbl->addColumn("JUMLAH", 1, 1,NULL,NULL,"center bold");
		$tbl->addColumn("SATUAN", 1, 1,NULL,NULL,"center bold");
		$tbl->addColumn("HARGA", 1, 1,NULL,NULL,"center bold");
		$tbl->addColumn("TOTAL", 1, 1,NULL,NULL,"center bold");
		$tbl->commit("body");
		
		$list=$data['data'];
		$no=1;
		foreach($list as $x){
			$tbl->addColumn(($no++).".", 1, 1,NULL,NULL,"left");
			$tbl->addColumn($x->nama, 1, 1,NULL,NULL,"left");
			$tbl->addColumn($x->jumlah, 1, 1,NULL,NULL,"left");
			$tbl->addColumn($x->satuan, 1, 1,NULL,NULL,"left");
			$tbl->addColumn(ArrayAdapter::format("money Rp.", $x->harga_satuan), 1, 1,NULL,NULL,"left");
			$tbl->addColumn(ArrayAdapter::format("money Rp.", $x->biaya), 1, 1,NULL,NULL,"left");
			$tbl->commit("body");
		}
		
		$tbl->addSpace(4,1);
		$tbl->addColumn("Sub Total", 1, 1,NULL,NULL,"left bold");
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->nilai), 1, 1,NULL,NULL,"left bold");
		$tbl->commit("footer");
		
		$tbl->addSpace(4,1);
		$tbl->addColumn("Markup", 1, 1,NULL,NULL,"left bold");
		$tbl->addColumn($p->markup."%", 1, 1,NULL,NULL,"right bold");
		$tbl->commit("footer");
		
		$tbl->addSpace(4,1);
		$tbl->addColumn("Total", 1, 1,NULL,NULL,"left bold");
		$tbl->addColumn(ArrayAdapter::format("money Rp.", $p->total), 1, 1,NULL,NULL,"left bold");
		$tbl->commit("footer");
		
		$town=getSettings($db, "smis_gz_town", "");
		$kepala=getSettings($db, "smis_gz_kepala", "");
		
		$tbl->addSpace(6,1);
		$tbl->commit("footer");
		
		$tbl->addSpace(4,1);
		$tbl->addColumn($town.", ".ArrayAdapter::format("date d M Y", date("Y-m-d")), 2, 1,NULL,NULL,"center bold");
		$tbl->commit("footer");
		
		
		
		$tbl->addSpace(4,1);
		$tbl->addColumn("Kepala Instalasi Gizi", 2, 1,NULL,NULL,"center bold");
		$tbl->commit("footer");
		
		$tbl->addSpace(4,1);
		$tbl->addColumn("</br></br>( ".$kepala." )", 2, 1,NULL,NULL,"center bold");
		$tbl->commit("footer");
		
		return $tbl->getHtml();
	}
	
}

?>