<?php
class PemesananMakanTable extends Table {
	public function getBodyContent() {
		$content = "";
		if ($this->content!=NULL) {
			foreach ($this->content as $d) {
				$content .= "<tr>";
				foreach ($this->header as $h) {
					$content .= "<td>" . $d[$h] . "</td>";
				}
				if ($this->is_action) {
					$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['selesai'])->getHtml() . "</td>";
				}
				$content .= "</tr>";
			}
		}
		return $content;
	}
	public function getFilteredContentButton($id, $selesai) {
		$btn_group = new ButtonGroup("noprint");
		if ($selesai == 0) {
			$btn = new Button("", "", "Ubah");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("fa fa-pencil");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Hapus");
			$btn->setAction($this->action . ".del('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
			$btn->setIcon("fa fa-trash");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		}
		return $btn_group;
	}
}
?>