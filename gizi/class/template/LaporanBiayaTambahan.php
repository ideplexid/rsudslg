<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class LaporanBiayaTambahan extends ModulTemplate {

	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
		$header=array ("No.",'Waktu',"Pengasuh","Nama Pasien","No. Reg",'NRM','Harga','Jumlah',"Total","Cara Bayar","Ruangan");
		$uitable = new Table ( $header, "", NULL, false );
		$uitable->setDelButtonEnable(false);
		$uitable->setEditButtonEnable(false);
		$uitable->setReloadButtonEnable(false);
		$uitable->setAddButtonEnable(false);
		$uitable->setName ( "laporan_tambahan_biaya" );
		$this->uitable=$uitable;
		
	}
	
	public function getCaraBayar(){
		$ser=new ServiceConsumer($this->db,"get_carabayar",NULL,"registration");
		$ser->execute();
		$content=$ser->getContent();
		$result=array();
		$result[]=array("name"=>"","value"=>"%","default"=>"1");
		foreach($content as $x){
			$result[]=array("name"=>$x['nama'],"value"=>$x['slug'],"default"=>"0");
		}
		return $result;
	}
	
	private function detail(Table &$uitable,SummaryAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<strong>Total</strong>");
		$adapter->addSummary("Total","total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Pengasuh", "nama_pengasuh");
		$adapter->add("Harga", "harga","money Rp.");
		$adapter->add("Harga Asli", "harga");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "total","money Rp.");
		$adapter->add("Total Asli", "total");
		$adapter->add("Cara Bayar", "carabayar","unslug");
        $adapter->add("Ruangan", "ruangan","unslug");
	}
	
	
	
	public function command($command) {	
		$this->dbtable = new DBTable ( $this->db, 'smis_gz_tambahan_biaya' );
        
		$adapter=new SummaryAdapter();

        $filter = "a.prop = '' AND a.tanggal >= '" . $_POST['dari'] . "' AND a.tanggal <= '" . $_POST['sampai'] . "'";
        if (isset($_POST['carabayar']) && trim($_POST['carabayar']) != "")
            $filter .= "AND b.carabayar LIKE '" . $_POST['carabayar'] . "'";
        if (isset($_POST['nama_pengasuh']) && trim($_POST['nama_pengasuh']) != "")
            $filter .= "AND a.nama_pengasuh LIKE '" . $_POST['nama_pengasuh'] . "'";

        $qv="
            SELECT 
                a.tanggal as waktu,
                a.biaya as harga,
                1 as jumlah,
                a.biaya as total,
                a.noreg_pasien as noreg_pasien,
                a.nrm_pasien as nrm_pasien,
                a.nama_pengasuh as nama_pengasuh,
                a.nama_pasien as nama_pasien,
                a.ruangan as ruangan,
                b.carabayar as carabayar
            FROM 
                smis_gz_tambahan_biaya a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id
            WHERE
                " . $filter . "
            ORDER BY
                a.tanggal ASC
        ";			
        $qc="SELECT COUNT(*) as total FROM (" . $qv . ") v";
        $this->dbtable->setPreferredQuery(true,$qv,$qc);		
        
        $this->detail($this->uitable,$adapter,$this->dbtable);
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}	
		require_once "gizi/class/responder/LaporanBiayaTambahanResponder.php";
		$this->dbres = new LaporanBiayaTambahanResponder( $this->dbtable, $this->uitable, $adapter );
		//$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	
	
	public function superCommand($super_command) {
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( "pengasuh_laporan_tambahan_biaya" );
		$dktable->setModel ( Table::$SELECT );
		$pengasuh = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "gizi" );
		
		$super = new SuperCommand ();
		$super->addResponder ( "pengasuh_laporan_tambahan_biaya", $pengasuh );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", date("Y-m-") . "01");
		$this->uitable->addModal("sampai", "date", "Sampai", date("Y-m-d"));
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_pengasuh", "chooser-laporan_tambahan_biaya-pengasuh_laporan_tambahan_biaya-pengasuh", "Pengasuh","", "y", null, true);
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("laporan_tambahan_biaya.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("laporan_tambahan_biaya.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("laporan_tambahan_biaya.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
				
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var laporan_tambahan_biaya;		
		var pengasuh_laporan_tambahan_biaya; 		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array("");
			laporan_tambahan_biaya=new TableAction("laporan_tambahan_biaya","gizi","laporan_tambahan_biaya",column);
			laporan_tambahan_biaya.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#laporan_tambahan_biaya_dari").val();
				save_data['sampai']=$("#laporan_tambahan_biaya_sampai").val();
				save_data['carabayar']=$("#laporan_tambahan_biaya_carabayar").val();
				save_data['nama_pengasuh']=$("#laporan_tambahan_biaya_nama_pengasuh").val();
				return save_data;
			};	
			
			
			pengasuh_laporan_tambahan_biaya=new TableAction("pengasuh_laporan_tambahan_biaya","gizi","laporan_tambahan_biaya",new Array());
			pengasuh_laporan_tambahan_biaya.setSuperCommand("pengasuh_laporan_tambahan_biaya");
			pengasuh_laporan_tambahan_biaya.selected=function(json){
				var nama=json.nama;
				$("#laporan_tambahan_biaya_nama_pengasuh").val(nama);
			};
            
            laporan_tambahan_biaya.view();
		});
		</script>
<?php
	}
}?>