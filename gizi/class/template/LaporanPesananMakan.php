<?php
require_once("smis-base/smis-include-service-consumer.php");
require_once("gizi/class/responder/RekapPesananMakanDBResponder.php");

class LaporanPesananMakan extends ModulTemplate {
	private $db;
	private $table;
	private $modal;
	private $page;
	private $action;

	public function __construct($db, $page, $action) {
		$this->db = $db;
		$this->page = $page;
		$this->action = $action;

		$this->table = new Table(
			array(
				"No.", "Ruangan", "Bed", "Nama Pasien", "No. RM", "No. Reg.", 
				"Pagi - Jenis Menu", "Pagi - Jns. Karbohidrat",
				"Siang - Jenis Menu", "Siang - Jns. Karbohidrat",
				"Malam - Jenis Menu", "Malam - Jns. Karbohidrat",
				"Status","Alergi","Jenis Diet"
			),
			"",
			null,
			true
		);
		$this->table->setName("laporan_pesanan_makanan");
		$this->table->setEditButtonEnable(false);
		$this->table->setDelButtonEnable(false);
		$this->table->setHeaderVisible(false);
		$button = new Button("", "", "Cetak Tag");
		$button->setClass("btn-inverse");
		$button->setIsButton(Button::$ICONIC);
		$button->setIcon("fa fa-print");
        $this->table->addContentButton("print_tag", $button);
        $this->table->setAction(false);
		$this->table->addHeader("before","
			<tr class='inverse'>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No.</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Ruangan</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Bed</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Nama Pasien</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No. RM</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No. Registrasi</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Pagi</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Siang</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Malam</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Status</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Alergi</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Jenis Diet</center></small></th>
			</tr>
			<tr class='inverse'>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
			</tr>
		");
	}

	public function command($command) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Ruangan", "ruang", "unslug");
		$adapter->add("Bed", "bed");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. RM", "nrm_pasien", "digit6");
		$adapter->add("No. Reg.", "noreg_pasien", "digit6");
		$adapter->add("Kelas", "kelas", "unslug");
		$adapter->add("Pagi - Jenis Menu", "menu_pagi");
		$adapter->add("Pagi - Jns. Karbohidrat", "pk_pagi");
		$adapter->add("Siang - Jenis Menu", "menu_siang");
		$adapter->add("Siang - Jns. Karbohidrat", "pk_siang");
		$adapter->add("Malam - Jenis Menu", "menu_malam");
		$adapter->add("Malam - Jns. Karbohidrat", "pk_malam");
        $adapter->add("Status", "selesai", "trivial_1_Diarsipkan_-");
		$adapter->add("Alergi", "alergi");
		$adapter->add("Jenis Diet", "jenis_diet");
		$dbtable = new DBTable($this->db, "smis_gz_pesanan");
		if (isset($_POST['dari']))
			$dbtable->addCustomKriteria(" tanggal >= ", " '" . $_POST['dari'] . "' ");
        if (isset($_POST['sampai']))
			$dbtable->addCustomKriteria(" tanggal <= ", " '" . $_POST['sampai'] . "' ");
		if (isset($_POST['ruang'])) {			
			if ($_POST['ruang'] == "SEMUA")
				$dbtable->addCustomKriteria(" ruang ", " LIKE '%%' ");
			else
				$dbtable->addCustomKriteria(" ruang ", " LIKE '" . $_POST['ruang'] . "' ");
		}
		if (isset($_POST['jenis_diet']) && trim($_POST['jenis_diet']) != "")
			$dbtable->addCustomKriteria(" jenis_diet ", " LIKE '" . $_POST['jenis_diet'] . "' ");
		if (isset($_POST['bentuk_makanan']) AND trim($_POST['bentuk_makanan']) != "")
			$dbtable->addCustomKriteria("", " (pk_pagi LIKE '" . $_POST['bentuk_makanan'] . "' OR pk_siang LIKE '" . $_POST['bentuk_makanan'] . "' OR pk_malam LIKE '" . $_POST['bentuk_makanan'] . "') ");
		$dbtable->setOrder(" tanggal DESC, id DESC ");
		$dbresponder = new RekapPesananMakanDBResponder(
			$dbtable,
			$this->table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
	}

	public function jsLoader() {
		echo addJS("framework/smis/js/table_action.js");
	    echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		
	}

	public function cssLoader() {
		echo addCSS("framework/bootstrap/css/datepicker.css");
		echo addCSS("gizi/resource/css/pesanan.css",false);
	}

	public function phpPreload() {
		$form = new Form("", "", "Laporan Pesanan Makanan");
		$tanggal_text = new Text("laporan_pesanan_makanan_dari", "laporan_pesanan_makanan_dari", date("Y-m-d"));
		$tanggal_text->setClass("mydate");
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
        $form->addElement("Dari", $tanggal_text);
        
        $tanggal_text = new Text("laporan_pesanan_makanan_sampai", "laporan_pesanan_makanan_sampai", date("Y-m-d"));
		$tanggal_text->setClass("mydate");
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$form->addElement("Sampai", $tanggal_text);

		$jenis_diet_option = new OptionBuilder();
		$jenis_diet_option->add("SEMUA", "%%", "1");
		$jenis_diet_rows = $this->db->get_result("
			SELECT id, nama
			FROM smis_gz_diet
			WHERE prop = ''
			ORDER BY nama ASC
		");
		if ($jenis_diet_rows != null) {
			foreach ($jenis_diet_rows as $jenis_diet_row)
				$jenis_diet_option->addSingle($jenis_diet_row->nama);
		}
		$jenis_diet_select = new Select("laporan_pesanan_makanan_jenis_diet", "laporan_pesanan_makanan_jenis_diet", $jenis_diet_option->getContent());
		$form->addElement("Jenis Diet", $jenis_diet_select);

		$bentuk_makanan_option = new OptionBuilder();
		$bentuk_makanan_option->add("SEMUA", "%%", "1");
		$bentuk_makanan_rows = $this->db->get_result("
			SELECT DISTINCT kategori
			FROM smis_gz_menu
			WHERE prop = ''
			ORDER BY kategori ASC
		");
		if ($bentuk_makanan_rows != null) {
			foreach ($bentuk_makanan_rows as $bentuk_makanan_row)
				$bentuk_makanan_option->addSingle($bentuk_makanan_row->kategori);
		}
		$bentuk_makanan_select = new Select("laporan_pesanan_makanan_bentuk_makanan", "laporan_pesanan_makanan_bentuk_makanan", $bentuk_makanan_option->getContent());
		$form->addElement("Bentuk Makanan", $bentuk_makanan_select);

		$unit_option = new OptionBuilder();
		$unit_option->add("SEMUA", "SEMUA", "1");
		$unit_service = new ServiceConsumer($this->db, "get_urjip");
	    $unit_service->setCached(true, "get_urjip");
	    $unit_service->setMode(ServiceConsumer::$MULTIPLE_MODE);
	    $unit_service->execute();
	    $content = $unit_service->getContent();
	    foreach ($content as $c) {
	    	foreach ($c as $unit => $urjip)
	    		if ($urjip[$unit] == "URI") {
	    			$ruang_row = $this->db->get_row("
						SELECT id, slug, nama
						FROM smis_adm_prototype
						WHERE slug = '" . $unit . "'
					");
					$ruangan = $unit;
					if ($ruang_row != null)
						$ruangan = $ruang_row->nama;
	    			$unit_option->addSingle(ArrayAdapter::format("unslug", $ruangan));
	    		}
	    }
	    $unit_select = new Select("laporan_pesanan_makanan_unit", "laporan_pesanan_makanan_unit", $unit_option->getContent());
	    $form->addElement("Ruangan", $unit_select);
		$waktu_option = new OptionBuilder();
	    $waktu_option->add("Pagi", "Pagi", "1");
	    $waktu_option->add("Siang", "Siang");
	    $waktu_option->add("Malam", "Malam");
	    $waktu_select = new Select("laporan_pesanan_makanan_waktu", "laporan_pesanan_makanan_waktu", $waktu_option->getContent());
		//$form->addElement("Waktu", $waktu_select);
		
		$proses_button = new Button("", "", "Proses");
		$proses_button->setClass("btn-inverse");
		$proses_button->setIsButton(Button::$ICONIC_TEXT);
		$proses_button->setIcon("fa fa-refresh");
		$proses_button->setAction("laporan_pesanan_makanan.view()");
		$cetak_tag_button = new Button("", "", "Cetak Tag");
		$cetak_tag_button->setClass("btn-inverse");
		$cetak_tag_button->setIsButton(Button::$ICONIC_TEXT);
		$cetak_tag_button->setIcon("fa fa-print");
		$cetak_tag_button->setAction("laporan_pesanan_makanan.print_all_tag()");
		$cetak_bon_button = new Button("", "", "Cetak Bon Pesanan");
		$cetak_bon_button->setClass("btn-inverse");
		$cetak_bon_button->setIsButton(Button::$ICONIC_TEXT);
		$cetak_bon_button->setIcon("fa fa-tag");
		$cetak_bon_button->setAction("laporan_pesanan_makanan.print_all_bon()");
        
        $archive_button = new Button("", "", "Arsipkan");
		$archive_button->setClass("btn-inverse");
		$archive_button->setIsButton(Button::$ICONIC_TEXT);
		$archive_button->setIcon("fa fa-archive");
		$archive_button->setAction("laporan_pesanan_makanan.archive_all()");
        
        $excel_button = new Button("", "", "Excel");
		$excel_button->setClass("btn-inverse");
		$excel_button->setIsButton(Button::$ICONIC_TEXT);
		$excel_button->setIcon("fa fa-file-excel-o");
		$excel_button->setAction("laporan_pesanan_makanan.excel()");

        $button_group = new ButtonGroup("no-print");
        $button_group->setMax(5, "Aksi");
        
        $button_group->addButton($proses_button);
        $button_group->addButton($excel_button);
		//$button_group->addButton($cetak_tag_button);
		//$button_group->addButton($cetak_bon_button);
		//$button_group->addButton($archive_button);
		$form->addElement("", $button_group);

		echo $form->getHtml();
		echo $this->table->getHtml();
	}

	public function cssPreLoad() {
		?>
		<style type="text/css">
			label {
				width: 120px !important;
			}
		</style>
		<?php
	}

	public function jsPreLoad() {
		?>
		<script type="text/javascript">
			var laporan_pesanan_makanan;
			var PAGE = "<?php echo $this->page; ?>";
			var ACTION = "<?php echo $this->action; ?>";
			$(document).ready(function() {
				$(".mydate").datepicker();

				laporan_pesanan_makanan = new TableAction(
					"laporan_pesanan_makanan",
					PAGE,
					ACTION,
					new Array()
				);
				laporan_pesanan_makanan.getRegulerData = function() {
					var data = TableAction.prototype.getRegulerData.call(this);
					data['dari'] = $("#laporan_pesanan_makanan_dari").val();
					data['sampai'] = $("#laporan_pesanan_makanan_sampai").val();
					data['ruang'] = $("#laporan_pesanan_makanan_unit").val();
					data['waktu'] = $("#laporan_pesanan_makanan_waktu").val();
					data['jenis_diet'] = $("#laporan_pesanan_makanan_jenis_diet").val();
					data['bentuk_makanan'] = $("#laporan_pesanan_makanan_bentuk_makanan").val();
					return data;
				};
				laporan_pesanan_makanan.archive_all = function() {
					var self = this;
					bootbox.confirm(
						"Yakin mengarsipkan data tersebut ?",
						function(result) {
							if (result) {
								showLoading();
								var data = self.getRegulerData();
								data['command'] = "archive_all";
								$.post(
									"",
									data,
									function(response) {
										var json = getContent(response);
										self.view();
										dismissLoading();
									}
								);
							}
						}
					);
				};
				laporan_pesanan_makanan.print_all_tag = function() {
					if ($("#laporan_pesanan_makanan_unit").val() == "SEMUA") {
						bootbox.alert("<b>Ruangan</b> harus dipilih terlebih dahulu untuk dapat mencetak Label.");
						return;	
					}
					showLoading();
					var data = this.getRegulerData();
					data['command'] = "print_all_tag";
					<?php if (getSettings($this->db, "gizi-label-jenis_luaran", "html") == "xls") { ?>
						postForm(data);
						dismissLoading();
					<?php } else { ?> 
						$.post(
							"",
							data,
							function(response) {
								var json = getContent(response);
								if (json == null) {
									dismissLoading();
									return;
								}
								smis_print(json);
								dismissLoading();
							}
						);
					<?php } ?>
				};
				laporan_pesanan_makanan.print_tag = function(id) {
					showLoading();
					var data = this.getRegulerData();
					data['command'] = "print_tag";
					data['id'] = id;
					data['command'] = "print_tag";
					<?php if (getSettings($this->db, "gizi-label-jenis_luaran", "html") == "xls") { ?>
						postForm(data);
						dismissLoading();
					<?php } else { ?> 
						$.post(
							"",
							data,
							function(response) {
								var json = getContent(response);
								if (json == null) {
									dismissLoading();
									return;
								}
								smis_print(json);
								dismissLoading();
							}
						);
					<?php } ?>
				};
				laporan_pesanan_makanan.print_all_bon = function() {
					if ($("#laporan_pesanan_makanan_waktu").val() == "%%") {
						bootbox.alert("<b>Waktu</b> harus dipilih terlebih dahulu untuk dapat mencetak Bon.");
						return;
					}
					if ($("#laporan_pesanan_makanan_unit").val() == "%%") {
						bootbox.alert("<b>Ruangan</b> harus dipilih terlebih dahulu untuk dapat mencetak Bon.");
						return;	
					}
					showLoading();
					var data = this.getRegulerData();
					data['command'] = "print_all_bon";
					postForm(data);
					dismissLoading();
				};
				laporan_pesanan_makanan.view();
				$("#table_laporan_pesanan_makanan > tfoot > tr > td:nth-child(1) > span > div").hide();
			});
		</script>
		<?php
	}
}
?>
