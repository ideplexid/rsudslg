<?php
require_once("smis-base/smis-include-service-consumer.php");
require_once("gizi/class/table/PemesananMakanTable.php");

class PesananMakan extends ModulTemplate {
	private $db;
	private $table;
	private $modal;
	private $page;
	private $action;

	public function __construct($db, $page, $action) {
		$this->db = $db;
		$this->page = $page;
		$this->action = $action;

		$this->table = new PemesananMakanTable(
			array(
				"No.", "Tanggal", "No. Reg.", "No. RM", "Nama Pasien", "Ruangan", "Bed", "Kelas", 
				"Pagi - Jenis Menu", "Pagi - Jns. Karbohidrat",
				"Siang - Jenis Menu", "Siang - Jns. Karbohidrat",
				"Malam - Jenis Menu", "Malam - Jns. Karbohidrat",
				"Alergi", "Jenis Diet"
			),
			"Pesanan Makan Pasien",
			null,
			true
		);
		$this->table->setName("pesanan_pasien");
		$this->table->setHeaderVisible(false);
		$this->table->addHeader("before","
			<tr class='inverse'>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No.</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Tanggal</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No. Registrasi</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No. RM</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Nama Pasien</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Ruangan</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Bed</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Kelas</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Pagi</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Siang</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Malam</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Alergi</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Jenis Diet</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'>
					<div align='center'>
						<a href='#' onclick=' pesanan_pasien.show_add_form() ' data-content='Tambah Data' data-toggle='popover' class='input btn btn-info'> 
							<i class='fa fa-plus'></i> 
						</a>
					</div>
				</th>
			</tr>
			<tr class='inverse'>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
			</tr>
		");
	}

	public function superCommand($scommand) {
		$pasien_table = new Table(
			array("No. Reg.", "NRM", "Nama", "Umur", "Alamat", "Kecamatan", "Jns. Pasien"),
			"",
			null,
			true
		);
		$pasien_table->setName("pasien");
		$pasien_table->setModel(Table::$SELECT);
		$noreg_s_text = new Text("search_noreg", "search_noreg", "");
		$noreg_s_text->setClass("search search-header-tiny search-text");
		$nrm_s_text = new Text("search_nrm", "search_nrm", "");
		$nrm_s_text->addAtribute("autofocus");
		$nrm_s_text->setClass("search search-header-tiny search-text");
		$nama_s_text = new Text("search_nama", "search_nama", "");
		$nama_s_text->setClass("search search-header-med search-text ");
		$umur_s_text = new Text("search_umur", "search_umur", "");
		$umur_s_text->setClass("search search-header-big search-text");
		$alamat_s_text = new Text("search_alamat", "search_alamat", "");
		$alamat_s_text->setClass("search search-header-big search-text");
		$kecamatan_s_text = new Text("search_kecamatan", "search_kecamatan", "");
		$kecamatan_s_text->setClass("search search-header-big search-text");
		$carabayar_s_text = new Text("search_carabayar", "search_carabayar", "");
		$carabayar_s_text->setClass("search search-header-tiny search-text");
		$header = "<tr class = 'header_pasien'>" .
						"<td>" . $noreg_s_text->getHtml() . "</td>" .
						"<td>" . $nrm_s_text->getHtml() . "</td>" .
						"<td>" . $nama_s_text->getHtml() . "</td>" .
						"<td>" . $umur_s_text->getHtml() . "</td>" .
						"<td>" . $alamat_s_text->getHtml() . "</td>" .
						"<td>" . $kecamatan_s_text->getHtml() . "</td>" .
						"<td>" . $carabayar_s_text->getHtml() . "</td>" .
						"<td></td>" .
				  "</tr>";
		$pasien_table->addHeader("after", $header);
		$pasien_adapter = new SimpleAdapter();
		$pasien_adapter->add('id', 'id');
		$pasien_adapter->add('No. Reg.', 'id', 'digit6');
		$pasien_adapter->add('NRM', 'nrm', 'digit6');
		$pasien_adapter->add('Nama', 'nama_pasien');
		$pasien_adapter->add('Umur', 'umur');
		$pasien_adapter->add('Alamat', 'alamat_pasien');
		$pasien_adapter->add('Kecamatan', 'nama_kecamatan');
		$pasien_adapter->add('Jns. Pasien', 'carabayar', 'unslug');
		$pasien_adapter->add('Perusahaan', 'n_perusahaan');
		$pasien_adapter->add('Asuransi', 'nama_asuransi');
		$pasien_service_responder = new ServiceResponder(
			$this->db,
			$pasien_table,
			$pasien_adapter,
			"get_registered"
		);

		$jenis_diet_table = new Table(
			array("No.", "Jenis Diet", "Penyakit"),
			"",
			null,
			true
		);
		$jenis_diet_table->setName("jenis_diet");
		$jenis_diet_table->setModel(Table::$SELECT);
		$jenis_diet_adapter = new SimpleAdapter(true, "No.");
		$jenis_diet_adapter->add('id', 'id');
		$jenis_diet_adapter->add('Jenis Diet', 'nama');
		$jenis_diet_adapter->add('Penyakit', 'penyakit');
		$jenis_diet_dbtable = new DBTable($this->db, "smis_gz_diet");
		$jenis_diet_dbtable->setOrder("nama ASC");
		$jenis_diet_dbrepsonder = new DBResponder(
			$jenis_diet_dbtable,
			$jenis_diet_table,
			$jenis_diet_adapter
		);

		$menu_pagi_table = new Table(
			array("No.", "Nama Menu", "Jenis Menu", "Kelas", "Waktu", "Karbohidrat", "Energi", "Protein", "Lemak"),
			"",
			null,
			true
		);
		$menu_pagi_table->setName("menu_pagi");
		$menu_pagi_table->setModel(Table::$SELECT);
		$menu_pagi_adapter = new SimpleAdapter(true, "No.");
		$menu_pagi_adapter->add('id', 'id');
		$menu_pagi_adapter->add('Nama Menu', 'nama');
		$menu_pagi_adapter->add('Jenis Menu', 'jenis_menu');
		$menu_pagi_adapter->add('Kelas', 'kelas', 'unslug');
		$menu_pagi_adapter->add('Waktu', 'waktu');
		$menu_pagi_adapter->add('Karbohidrat', 'karbohidrat', "number");
		$menu_pagi_adapter->add('Energi', 'energi', "number");
		$menu_pagi_adapter->add('Protein', 'protein', "number");
		$menu_pagi_adapter->add('Lemak', 'lemak', "number");
		$menu_pagi_dbtable = new DBTable($this->db, "smis_gz_menu");

		$kelas = isset($_POST['kelas'])?$_POST['kelas']:"";
		$waktu = isset($_POST['waktu'])?$_POST['waktu']:"";
		$tanggal = isset($_POST['tanggal'])?$_POST['tanggal']:"";

		$filter = " AND a.kelas LIKE '" . $kelas . "' AND a.waktu LIKE '" . $waktu . "' AND b.tanggal = '" . $tanggal . "' ";

		if (isset($_POST['kriteria']))
			$filter .= " AND (a.nama LIKE '%" . $_POST['kriteria'] . "%' OR a.jenis_menu LIKE '%" . $_POST['kriteria'] . "%') ";
		$query_value = "
			SELECT a.*, b.tanggal
			FROM smis_gz_menu a INNER JOIN smis_gz_jadwal b ON a.id_group = b.id_group
			WHERE a.prop = '' AND b.prop = '' " . $filter . "
			ORDER BY a.jenis_menu ASC, a.nama ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$menu_pagi_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$menu_pagi_dbresponder = new DBResponder(
			$menu_pagi_dbtable,
			$menu_pagi_table,
			$menu_pagi_adapter
		);

		$menu_siang_table = new Table(
			array("No.", "Nama Menu", "Jenis Menu", "Kelas", "Waktu", "Karbohidrat", "Energi", "Protein", "Lemak"),
			"",
			null,
			true
		);
		$menu_siang_table->setName("menu_siang");
		$menu_siang_table->setModel(Table::$SELECT);
		$menu_siang_adapter = new SimpleAdapter(true, "No.");
		$menu_siang_adapter->add('id', 'id');
		$menu_siang_adapter->add('Nama Menu', 'nama');
		$menu_siang_adapter->add('Jenis Menu', 'jenis_menu');
		$menu_siang_adapter->add('Kelas', 'kelas', 'unslug');
		$menu_siang_adapter->add('Waktu', 'waktu');
		$menu_siang_adapter->add('Karbohidrat', 'karbohidrat', "number");
		$menu_siang_adapter->add('Energi', 'energi', "number");
		$menu_siang_adapter->add('Protein', 'protein', "number");
		$menu_siang_adapter->add('Lemak', 'lemak', "number");
		$menu_siang_dbtable = new DBTable($this->db, "smis_gz_menu");
		$filter = " AND a.kelas LIKE '" . $_POST['kelas'] . "' AND a.waktu LIKE '" . $_POST['waktu'] . "' AND b.tanggal = '" . $_POST['tanggal'] . "' ";
		if (isset($_POST['kriteria']))
			$filter .= " AND (a.nama LIKE '%" . $_POST['kriteria'] . "%' OR a.jenis_menu LIKE '%" . $_POST['kriteria'] . "%') ";
		$query_value = "
			SELECT a.*, b.tanggal
			FROM smis_gz_menu a INNER JOIN smis_gz_jadwal b ON a.id_group = b.id_group
			WHERE a.prop = '' AND b.prop = '' " . $filter . "
			ORDER BY a.jenis_menu ASC, a.nama ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$menu_siang_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$menu_siang_dbresponder = new DBResponder(
			$menu_siang_dbtable,
			$menu_siang_table,
			$menu_siang_adapter
		);

		$menu_malam_table = new Table(
			array("No.", "Nama Menu", "Jenis Menu", "Kelas", "Waktu", "Karbohidrat", "Energi", "Protein", "Lemak"),
			"",
			null,
			true
		);
		$menu_malam_table->setName("menu_malam");
		$menu_malam_table->setModel(Table::$SELECT);
		$menu_malam_adapter = new SimpleAdapter(true, "No.");
		$menu_malam_adapter->add('id', 'id');
		$menu_malam_adapter->add('Nama Menu', 'nama');
		$menu_malam_adapter->add('Jenis Menu', 'jenis_menu');
		$menu_malam_adapter->add('Kelas', 'kelas', 'unslug');
		$menu_malam_adapter->add('Waktu', 'waktu');
		$menu_malam_adapter->add('Karbohidrat', 'karbohidrat', "number");
		$menu_malam_adapter->add('Energi', 'energi', "number");
		$menu_malam_adapter->add('Protein', 'protein', "number");
		$menu_malam_adapter->add('Lemak', 'lemak', "number");
		$menu_malam_dbtable = new DBTable($this->db, "smis_gz_menu");
		

		$kelas = isset($_POST['kelas'])?$_POST['kelas']:"";
		$waktu = isset($_POST['waktu'])?$_POST['waktu']:"";
		$tanggal = isset($_POST['tanggal'])?$_POST['tanggal']:"";

		$filter = " AND a.kelas LIKE '" . $kelas . "' AND a.waktu LIKE '" . $waktu . "' AND b.tanggal = '" . $tanggal . "' ";

		if (isset($_POST['kriteria']))
			$filter .= " AND (a.nama LIKE '%" . $_POST['kriteria'] . "%' OR a.jenis_menu LIKE '%" . $_POST['kriteria'] . "%') ";
		$query_value = "
			SELECT a.*, b.tanggal
			FROM smis_gz_menu a INNER JOIN smis_gz_jadwal b ON a.id_group = b.id_group
			WHERE a.prop = '' AND b.prop = '' " . $filter . "
			ORDER BY a.jenis_menu ASC, a.nama ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$menu_malam_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$menu_malam_dbresponder = new DBResponder(
			$menu_malam_dbtable,
			$menu_malam_table,
			$menu_malam_adapter
		);

		$super_command = new SuperCommand();
		$super_command->addResponder("pasien", $pasien_service_responder);
		$super_command->addResponder("jenis_diet", $jenis_diet_dbrepsonder);
		$super_command->addResponder("menu_pagi", $menu_pagi_dbresponder);
		$super_command->addResponder("menu_siang", $menu_siang_dbresponder);
		$super_command->addResponder("menu_malam", $menu_malam_dbresponder);
		$init = $super_command->initialize();
		if ($init != null) {
			echo $init;
			return;
		}
	}

	public function command($command) {
		if ($_POST['command'] == "get_ruangan_dan_kelas") {
			$params = array(
				"noreg_pasien"	=> $_POST['noreg_pasien']
			);
			$service_consumer = new ServiceConsumer(
				$this->db,
				"get_last_bed_and_room_position",
				$params,
				"medical_record"
			);
			$content = $service_consumer->execute()->getContent();
			$kelas = getSettings($this->db, "smis-rs-kelas-" . $content['ruangan'], "");
			$ruang_row = $this->db->get_row("
				SELECT id, slug, nama
				FROM smis_adm_prototype
				WHERE slug = '" . $content['ruangan'] . "' AND parent = 'rawat'
			");
			$ruangan = $content['ruangan'];
			if ($ruang_row != null)
				$ruangan = $ruang_row->nama;
			$data = array(
				'slug_ruang'=> $content['ruangan'],
				'ruangan'	=> $ruangan,
				'bed'		=> $content['bed'],
				'kelas'		=> $kelas
			);
			echo json_encode($data);
			return;
		}
		if ($_POST['command'] == "save")
			$_POST['code'] = md5($_POST['tanggal'] . " " . $_POST['noreg_pasien'] . " " . $_POST['slug_ruang']);
		else if ($_POST['del'] == "del")
			$_POST['code'] = md5(date("Ymd_His") . " " . $_POST['id']);

		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("id", "id");
		$adapter->add("selesai", "selesai");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("No. Reg.", "noreg_pasien", "digit6");
		$adapter->add("No. RM", "nrm_pasien", "digit6");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("Ruangan", "ruang", "unslug");
		$adapter->add("Bed", "bed");
		$adapter->add("Kelas", "kelas", "unslug");
		$adapter->add("Pagi - Jenis Menu", "menu_pagi");
		$adapter->add("Pagi - Jns. Karbohidrat", "pk_pagi");
		$adapter->add("Siang - Jenis Menu", "menu_siang");
		$adapter->add("Siang - Jns. Karbohidrat", "pk_siang");
		$adapter->add("Malam - Jenis Menu", "menu_malam");
		$adapter->add("Malam - Jns. Karbohidrat", "pk_malam");
		$adapter->add("Alergi", "alergi");
		$adapter->add("Jenis Diet", "jenis_diet");
		require_once "gizi/class/responder/PesananPasienResponder.php";
		require_once "gizi/class/dbtable/PesananMakanDBTable.php";
		
		$dbtable = new PesananMakanDBTable($this->db, "smis_gz_pesanan");
		$dbtable->setOrder(" tanggal DESC, id DESC ");
		
		$dbresponder = new PesananPasienResponder(
			$dbtable,
			$this->table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
	}

	public function jsLoader() {
		echo addJS("framework/smis/js/table_action.js");
	    echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	}

	public function cssLoader() {
	    echo addCSS("framework/bootstrap/css/datepicker.css");
	}

	public function phpPreload() {
		$modal = new Modal("pesanan_pasien_add_form", "smis_form_container", "pesanan_pasien");
		$modal->setClass(Modal::$HALF_MODEL);
		$modal->setTitle("Data Pemesanan Makan Pasien");

		$form_1 = new Form("pesanan_pasien_1", "", "");
		$id_hidden = new Hidden("pesanan_pasien_id", "pesanan_pasien_id", "");
		$form_1->addElement("", $id_hidden);
		$tanggal_text = new Text("pesanan_pasien_tanggal", "pesanan_pasien_tanggal", date("Y-m-d"));
		$tanggal_text->setClass("mydate");
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$form_1->addElement("Tanggal", $tanggal_text);
		$noreg_pasien_text = new Text("pesanan_pasien_noreg_pasien", "pesanan_pasien_noreg_pasien", "");
		$noreg_pasien_text->setAtribute("disabled='disabled'");
		$form_1->addElement("No. Registrasi", $noreg_pasien_text);
		$nrm_pasien_text = new Text("pesanan_pasien_nrm_pasien", "pesanan_pasien_nrm_pasien", "");
		$nrm_pasien_text->setAtribute("disabled='disabled'");
		$form_1->addElement("No. RM", $nrm_pasien_text);
		$pasien_button = new Button("", "", "Pilih");
		$pasien_button->setClass("btn-info");
		$pasien_button->setIsButton(Button::$ICONIC);
		$pasien_button->setIcon("icon-white ".Button::$icon_list_alt);
		$pasien_button->setAction("pasien.chooser('pasien', 'pasien_button', 'pasien', pasien, 'Pasien Rawat Inap Aktif')");
		$pasien_button->setAtribute("id='pasien_browse'");
		$pasien_text = new Text("pesanan_pasien_nama_pasien", "pesanan_pasien_nama_pasien", "");
		$pasien_text->setAtribute("disabled='disabled'");
		$pasien_text->setClass("smis-one-option-input");
		$pasien_input_group = new InputGroup("");
		$pasien_input_group->addComponent($pasien_text);
		$pasien_input_group->addComponent($pasien_button);
		$form_1->addElement("Nama Pasien", $pasien_input_group);
		$jenis_pasien_text = new Text("pesanan_pasien_jenis_pasien", "pesanan_pasien_jenis_pasien", "");
		$jenis_pasien_text->setAtribute("disabled='disabled'");
		$form_1->addElement("Jenis Layanan", $jenis_pasien_text);
		$ruangan_text = new Text("pesanan_pasien_ruang", "pesanan_pasien_ruang", "");
		$ruangan_text->setAtribute("disabled='disabled'");
		$form_1->addElement("Ruangan", $ruangan_text);
		$ruangan_slug_hidden = new Hidden("pesanan_pasien_slug_ruang", "pesanan_pasien_slug_ruang", "");
		$form_1->addElement("", $ruangan_slug_hidden);
		$kelas_text = new Text("pesanan_pasien_kelas", "pesanan_pasien_kelas", "");
		$kelas_text->setAtribute("disabled='disabled'");
		$form_1->addElement("Kelas", $kelas_text);
		$bed_text = new Text("pesanan_pasien_bed", "pesanan_pasien_bed", "");
		$bed_text->setAtribute("disabled='disabled'");
		$form_1->addElement("Bed", $bed_text);
		$jenis_diet_browse_button = new Button("", "", "Pilih");
		$jenis_diet_browse_button->setClass("btn-info");
		$jenis_diet_browse_button->setIsButton(Button::$ICONIC);
		$jenis_diet_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
		$jenis_diet_browse_button->setAction("jenis_diet.chooser('jenis_diet', 'jenis_diet_button', 'jenis_diet', jenis_diet, 'Jenis Diet')");
		$jenis_diet_browse_button->setAtribute("id='jenis_diet_browse'");
		$jenis_diet_clear_button = new Button("", "", "Hapus");
		$jenis_diet_clear_button->setClass("btn-inverse");
		$jenis_diet_clear_button->setIsButton(Button::$ICONIC);
		$jenis_diet_clear_button->setIcon("fa fa-times");
		$jenis_diet_clear_button->setAction("jenis_diet.clear()");
		$jenis_diet_clear_button->setAtribute("id='jenis_diet_clear'");
		$jenis_diet_text = new Text("pesanan_pasien_jenis_diet", "pesanan_pasien_jenis_diet", "");
		$jenis_diet_text->setAtribute("disabled='disabled'");
		$jenis_diet_text->setClass("smis-two-option-input");
		$jenis_diet_input_group = new InputGroup("");
		$jenis_diet_input_group->addComponent($jenis_diet_text);
		$jenis_diet_input_group->addComponent($jenis_diet_browse_button);
		$jenis_diet_input_group->addComponent($jenis_diet_clear_button);
		$form_1->addElement("Jenis Diet", $jenis_diet_input_group);

		$alergi_textarea = new TextArea("pesanan_pasien_alergi", "pesanan_pasien_alergi", "");
		$alergi_textarea->setLine(2);
		$form_1->addElement("Alergi", $alergi_textarea);
		

		$keterangan_textarea = new TextArea("pesanan_pasien_keterangan", "pesanan_pasien_keterangan", "");
		$keterangan_textarea->setLine(2);
		$form_1->addElement("Keterangan", $keterangan_textarea);
		

		$id_menu_pagi_hidden = new Hidden("pesanan_pasien_id_menu_pagi", "pesanan_pasien_id_menu_pagi", "");
		$form_1->addElement("", $id_menu_pagi_hidden);
		$menu_pagi_tarif_hidden = new Hidden("pesanan_pasien_menu_pagi_tarif", "pesanan_pasien_menu_pagi_tarif", "");
		$form_1->addElement("", $menu_pagi_tarif_hidden);
		$id_menu_siang_hidden = new Hidden("pesanan_pasien_id_menu_siang", "pesanan_pasien_id_menu_siang", "");
		$form_1->addElement("", $id_menu_siang_hidden);
		$menu_siang_tarif_hidden = new Hidden("pesanan_pasien_menu_siang_tarif", "pesanan_pasien_menu_siang_tarif", "");
		$form_1->addElement("", $menu_siang_tarif_hidden);
		$id_menu_malam_hidden = new Hidden("pesanan_pasien_id_menu_malam", "pesanan_pasien_id_menu_malam", "");
		$form_1->addElement("", $id_menu_malam_hidden);
		$menu_malam_tarif_hidden = new Hidden("pesanan_pasien_menu_malam_tarif", "pesanan_pasien_menu_malam_tarif", "");
		$form_1->addElement("", $menu_malam_tarif_hidden);
		$modal->addHtml("<div class='clear'>" . $form_1->getHtml() . "</div>", "before");

		$form_2 = new Form("pesanan_pasien_2", "", "");
		$menu_pagi_browse_button = new Button("", "", "Pilih");
		$menu_pagi_browse_button->setClass("btn-info");
		$menu_pagi_browse_button->setIsButton(Button::$ICONIC);
		$menu_pagi_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
		$menu_pagi_browse_button->setAction("menu_pagi.chooser('menu_pagi', 'menu_pagi_button', 'menu_pagi', menu_pagi, 'Menu Pagi')");
		$menu_pagi_browse_button->setAtribute("id='menu_pagi_browse'");
		$menu_pagi_clear_button = new Button("", "", "Hapus");
		$menu_pagi_clear_button->setClass("btn-inverse");
		$menu_pagi_clear_button->setIsButton(Button::$ICONIC);
		$menu_pagi_clear_button->setIcon("fa fa-times");
		$menu_pagi_clear_button->setAction("menu_pagi.clear()");
		$menu_pagi_clear_button->setAtribute("id='menu_pagi_clear'");
		$menu_pagi_text = new Text("pesanan_pasien_menu_pagi", "pesanan_pasien_menu_pagi", "");
		$menu_pagi_text->setAtribute("disabled='disabled'");
		$menu_pagi_text->setClass("smis-two-option-input");
		$menu_pagi_input_group = new InputGroup("");
		$menu_pagi_input_group->addComponent($menu_pagi_text);
		$menu_pagi_input_group->addComponent($menu_pagi_browse_button);
		$menu_pagi_input_group->addComponent($menu_pagi_clear_button);
		$pk_pagi_select = new Select("pesanan_pasien_pk_pagi", "pesanan_pasien_pk_pagi", "");
		$form_2->addElement("", "
			<div id='pesanan_pasien_grup_menu_pagi' class='clear'>
				<div class='panel panel-default'>
					<div class='panel-heading'>Menu Pagi</div>
					<div class='panel-body'>
						<div class='pesanan_pasien_menu_pagi'> 
							<label>Menu</label>
							" . $menu_pagi_input_group->getHtml() . "
						</div>
						<div class='pesanan_pasien_pk_pagi'> 
							<label>Bentuk Makanan</label>
							" . $pk_pagi_select->getHtml() . "
						</div>
					</div>
				</div>	
			</div>
		");
		$menu_siang_browse_button = new Button("", "", "Pilih");
		$menu_siang_browse_button->setClass("btn-info");
		$menu_siang_browse_button->setIsButton(Button::$ICONIC);
		$menu_siang_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
		$menu_siang_browse_button->setAction("menu_siang.chooser('menu_siang', 'menu_siang_button', 'menu_siang', menu_siang, 'Menu Siang')");
		$menu_siang_browse_button->setAtribute("id='menu_siang_browse'");
		$menu_siang_clear_button = new Button("", "", "Hapus");
		$menu_siang_clear_button->setClass("btn-inverse");
		$menu_siang_clear_button->setIsButton(Button::$ICONIC);
		$menu_siang_clear_button->setIcon("fa fa-times");
		$menu_siang_clear_button->setAction("menu_siang.clear()");
		$menu_siang_clear_button->setAtribute("id='menu_siang_clear'");
		$menu_siang_text = new Text("pesanan_pasien_menu_siang", "pesanan_pasien_menu_siang", "");
		$menu_siang_text->setAtribute("disabled='disabled'");
		$menu_siang_text->setClass("smis-two-option-input");
		$menu_siang_input_group = new InputGroup("");
		$menu_siang_input_group->addComponent($menu_siang_text);
		$menu_siang_input_group->addComponent($menu_siang_browse_button);
		$menu_siang_input_group->addComponent($menu_siang_clear_button);
		$pk_siang_select = new Select("pesanan_pasien_pk_siang", "pesanan_pasien_pk_siang", "");
		$form_2->addElement("", "
			<div id='pesanan_pasien_grup_menu_siang' class='clear'>
				<div class='panel panel-default'>
					<div class='panel-heading'>Menu Siang</div>
					<div class='panel-body'>
						<div class='pesanan_pasien_menu_siang'> 
							<label>Menu</label>
							" . $menu_siang_input_group->getHtml() . "
						</div>
						<div class='pesanan_pasien_pk_siang'> 
							<label>Bentuk Makanan</label>
							" . $pk_siang_select->getHtml() . "
						</div>
					</div>
				</div>	
			</div>
		");
		$modal->addHtml("<div class='clear'>" . $form_2->getHtml() . "</div>", "before");

		$form_3 = new Form("pesanan_pasien_3", "", "");
		$menu_malam_browse_button = new Button("", "", "Pilih");
		$menu_malam_browse_button->setClass("btn-info");
		$menu_malam_browse_button->setIsButton(Button::$ICONIC);
		$menu_malam_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
		$menu_malam_browse_button->setAction("menu_malam.chooser('menu_malam', 'menu_malam_button', 'menu_malam', menu_malam, 'Menu Malam')");
		$menu_malam_browse_button->setAtribute("id='menu_malam_browse'");
		$menu_malam_clear_button = new Button("", "", "Hapus");
		$menu_malam_clear_button->setClass("btn-inverse");
		$menu_malam_clear_button->setIsButton(Button::$ICONIC);
		$menu_malam_clear_button->setIcon("fa fa-times");
		$menu_malam_clear_button->setAction("menu_malam.clear()");
		$menu_malam_clear_button->setAtribute("id='menu_malam_clear'");
		$menu_malam_text = new Text("pesanan_pasien_menu_malam", "pesanan_pasien_menu_malam", "");
		$menu_malam_text->setAtribute("disabled='disabled'");
		$menu_malam_text->setClass("smis-two-option-input");
		$menu_malam_input_group = new InputGroup("");
		$menu_malam_input_group->addComponent($menu_malam_text);
		$menu_malam_input_group->addComponent($menu_malam_browse_button);
		$menu_malam_input_group->addComponent($menu_malam_clear_button);
		$pk_malam_select = new Select("pesanan_pasien_pk_malam", "pesanan_pasien_pk_malam", "");
		$form_3->addElement("", "
			<div id='pesanan_pasien_grup_menu_malam' class='clear'>
				<div class='panel panel-default'>
					<div class='panel-heading'>Menu Malam</div>
					<div class='panel-body'>
						<div class='pesanan_pasien_menu_malam'> 
							<label>Menu</label>
							" . $menu_malam_input_group->getHtml() . "
						</div>
						<div class='pesanan_pasien_pk_malam'> 
							<label>Bentuk Makanan</label>
							" . $pk_malam_select->getHtml() . "
						</div>
					</div>
				</div>	
			</div>
		");
		$modal->addHtml("<div class='clear'>" . $form_3->getHtml() . "</div>", "before");

		$save_button = new Button("", "", "Simpan");
		$save_button->setClass("btn-success");
		$save_button->setAction("pesanan_pasien.save()");
		$save_button->setIcon("fa fa-floppy-o");
		$save_button->setIsButton(Button::$ICONIC);
		$modal->addFooter($save_button);

		echo $modal->getHtml();
		echo $this->table->getHtml();
	}

	public function jsPreLoad() {
		?>
		<script type="text/javascript">
			var pesanan_pasien;
			var pasien;
			var menu_pagi;
			var menu_siang;
			var menu_malam;
			var jenis_diet;
			var PAGE = "<?php echo $this->page; ?>";
			var ACTION = "<?php echo $this->action; ?>";
			$(document).ready(function() {
				$(".mydate").datepicker();

				var columns = new Array(
					"id", "tanggal", "noreg_pasien", "nrm_pasien", "nama_pasien", "jenis_pasien", "slug_ruang", "ruang", "kelas", "bed", 
					"id_menu_pagi", "menu_pagi", "pk_pagi", "menu_pagi_tarif",
					"id_menu_siang", "menu_siang", "pk_siang", "menu_siang_tarif",
					"id_menu_malam", "menu_malam", "pk_malam", "menu_malam_tarif",
					"jenis_diet", "keterangan","alergi"
				);
				pesanan_pasien = new TableAction(
					"pesanan_pasien",
					PAGE,
					ACTION,
					columns
				);
				pesanan_pasien.edit = function(id) {
					var self = this;
				    var json_obj = new Array();
					showLoading();
					var edit_data = this.getEditData(id);
					$.post(
						"",
						edit_data,
						function(res) {		
							var json = getContent(res);
							if (json == null) return;
							for (var i = 0; i < self.column.length; i++) {
								if ($.inArray(self.column[i], self.noclear) != -1 && !self.edit_clear_for_no_clear)
									continue;
					            var name = self.column[i];
								var the_id = "#" + self.prefix + "_" + name;
					            if (name in self.json_column && self.json_column.length > 0) {
					                var json_grup_name = self.json_column[name];
					                if (json[json_grup_name] == "")
					                    continue;
					                if (!(json_grup_name in json_obj))
									    json_obj[json_grup_name] = $.parseJSON(json[json_grup_name]);
					                smis_edit(the_id, json_obj[json_grup_name]["" + name]);    
					            } else
					                smis_edit(the_id, json["" + name]);
							}

							menu_pagi.selectMenu(json.id_menu_pagi, json.pk_pagi, json.menu_pagi_tarif);
							menu_siang.selectMenu(json.id_menu_siang, json.pk_siang, json.menu_siang_tarif);
							menu_malam.selectMenu(json.id_menu_malam, json.pk_malam, json.menu_malam_tarif);

							dismissLoading();
							self.disabledOnEdit(self.column_disabled_on_edit);
							self.show_form();
						}
					);
				};
				pesanan_pasien.view();
				pesanan_pasien.cekTutupTagihan = function(){
					var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement
						};			
					var noreg                 = pesanan_pasien.get("noreg_pasien");
					if(noreg==""){
						smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
						return;
					}
					reg_data['command']        = 'cek_tutup_tagihan';
					reg_data['noreg_pasien']  = noreg;
					
					var res = $.ajax({
						type: "POST",
						url: "",
						data:reg_data,
						async: false
					}).responseText;

					var json = getContent(res);
					if(json=="1"){
						return false;
					}else{
						return true;
					}
				};

				pesanan_pasien.save = function(){
					showLoading();
					var cek = this.cekTutupTagihan();
					if(cek){
						TableAction.prototype.save.call(this);        
					}
					dismissLoading();
				};

				pasien = new TableAction(
					"pasien",
					PAGE,
					ACTION,
					new Array()
				);
				pasien.addViewData = function(data) {
					data['noreg_pasien'] = $("#search_noreg").val();
					data['nrm_pasien'] = $("#search_nrm").val();
					data['nama_pasien'] = $("#search_nama").val();
					data['alamat_pasien'] = $("#search_alamat").val();
					data['jenis_pasien'] = $("#search_carabayar").val();
					data['umur'] = $("#search_umur").val();
					data['nama_kecamatan'] = $("#search_kecamatan").val();
					data['uri'] = 1;
					return data;
				};
				pasien.selected = function(json) {
					$("#pesanan_pasien_noreg_pasien").val(json.id);
					$("#pesanan_pasien_nrm_pasien").val(json.nrm);
					$("#pesanan_pasien_nama_pasien").val(json.nama_pasien);
					$("#pesanan_pasien_jenis_pasien").val(json.carabayar);
					var data = this.getRegulerData();
					data['command'] = "get_ruangan_dan_kelas";
					data['super_command'] = "";
					data['noreg_pasien'] = json.id;
					showLoading();
					$.post(
						"",
						data,
						function(response) {
							var json_ruangan_kelas = JSON.parse(response);
							if (json_ruangan_kelas == null) return;
							$("#pesanan_pasien_slug_ruang").val(json_ruangan_kelas.slug_ruang);
							$("#pesanan_pasien_ruang").val(json_ruangan_kelas.ruangan);
							$("#pesanan_pasien_kelas").val(json_ruangan_kelas.kelas);
							$("#pesanan_pasien_bed").val(json_ruangan_kelas.bed);
							$("#modal_alert_pesanan_pasien_add_form").html("");
							dismissLoading();
						}
					);
				};
				pasien.setSuperCommand("pasien");

				jenis_diet = new TableAction(
					"jenis_diet",
					PAGE,
					ACTION,
					new Array()
				);
				jenis_diet.selected = function(json) {
					$("#pesanan_pasien_jenis_diet").val(json.nama);
				};
				jenis_diet.clear = function() {
					$("#pesanan_pasien_jenis_diet").val("");
				};
				jenis_diet.setSuperCommand("jenis_diet");

				$(document).on("keyup", ".search-text", function(e) {
			        if (e.which == 13) {
			        	pasien.view();
			        }
			    });

				menu_pagi = new TableAction(
					"menu_pagi",
					PAGE,
					ACTION,
					new Array()
				);
				menu_pagi.chooser = function(modal, elm_id, param, action, modal_title) {
					if ($("#pesanan_pasien_kelas").val() == "") {
						$("#modal_alert_pesanan_pasien_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								"<b>Kelas</b> tidak boleh kosong." +
							"</div>"
						);
						return;
					}
				    var data = action.getChooserData();
				    if(data == null) {
				       var d = this.getRegulerData();
				       data = this.addChooserData(d);
				    }
				    var self = this;
					data['super_command'] = param;
					$.post(
						"",
						data,
						function(response) {
							show_chooser(self, param, response, action.getShowParentModalInChooser(), modal_title);
							action.view();
							action.focusSearch();
							this.current_chooser = action;
							CURRENT_SMIS_CHOOSER = action;
						}
					);
					return this;
				};
				menu_pagi.addViewData = function(data) {
					data['kelas'] = $("#pesanan_pasien_kelas").val();
					data['waktu'] = "pagi";
					data['tanggal'] = $("#pesanan_pasien_tanggal").val();
					return data;
				};
				menu_pagi.selected = function(json) {
					$("#pesanan_pasien_id_menu_pagi").val(json.id);
					$("#pesanan_pasien_menu_pagi_tarif").val(json.tarif_sekarang);
					$("#pesanan_pasien_menu_pagi").val(json.nama);
					var option_html = "";
					var option = json.kategori.split(";");
					if (option.length > 0) {
						var counter = 1;
						option.forEach(function(item, index) {
							if (counter == 1)
								option_html += "<option selected>" + item + "</option>";
							else
								option_html += "<option>" + item + "</option>";
							counter++;
						});
					}
					$("#pesanan_pasien_pk_pagi").html(option_html);
				};
				menu_pagi.selectMenu = function(id_menu, jenis_karbohidrat) {
					if (id_menu == 0 || id_menu == null)
						return;
					hide_chooser();
					showLoading();
					var self = this;
					var edit_data = this.getEditData(id_menu);
					$.post(
						"",
						edit_data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								return;
							}
							self.selectedMenu(json, jenis_karbohidrat);
					        dismissLoading();
						}
					);
				};
				menu_pagi.selectedMenu = function(json, jenis_karbohidrat, tarif) {
					$("#pesanan_pasien_id_menu_pagi").val(json.id);
					$("#pesanan_pasien_menu_pagi_tarif").val(tarif);
					$("#pesanan_pasien_menu_pagi").val(json.nama);
					var option_html = "";
					var option = json.kategori.split(";");
					if (option.length > 0) {
						option.forEach(function(item, index) {
							if (item == jenis_karbohidrat)
								option_html += "<option selected>" + item + "</option>";
							else
								option_html += "<option>" + item + "</option>";
						});
					}
					$("#pesanan_pasien_pk_pagi").html(option_html);
				};
				menu_pagi.clear = function() {
					$("#pesanan_pasien_id_menu_pagi").val("");
					$("#pesanan_pasien_menu_pagi").val("");
					$("#pesanan_pasien_menu_pagi_tarif").val(0);
					$("#pesanan_pasien_pk_pagi").html("");
				};
				menu_pagi.setSuperCommand("menu_pagi");

				menu_siang = new TableAction(
					"menu_siang",
					PAGE,
					ACTION,
					new Array()
				);
				menu_siang.chooser = function(modal, elm_id, param, action, modal_title) {
					if ($("#pesanan_pasien_kelas").val() == "") {
						$("#modal_alert_pesanan_pasien_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								"<b>Kelas</b> tidak boleh kosong." +
							"</div>"
						);
						return;
					}
				    var data = action.getChooserData();
				    if(data == null) {
				       var d = this.getRegulerData();
				       data = this.addChooserData(d);
				    }
				    var self = this;
					data['super_command'] = param;
					$.post(
						"",
						data,
						function(response) {
							show_chooser(self, param, response, action.getShowParentModalInChooser(), modal_title);
							action.view();
							action.focusSearch();
							this.current_chooser = action;
							CURRENT_SMIS_CHOOSER = action;
						}
					);
					return this;
				};
				menu_siang.addViewData = function(data) {
					data['kelas'] = $("#pesanan_pasien_kelas").val();
					data['waktu'] = "siang";
					data['tanggal'] = $("#pesanan_pasien_tanggal").val();
					return data;
				};
				menu_siang.selected = function(json) {
					$("#pesanan_pasien_id_menu_siang").val(json.id);
					$("#pesanan_pasien_menu_siang_tarif").val(json.tarif_sekarang);
					$("#pesanan_pasien_menu_siang").val(json.nama);
					var option_html = "";
					var option = json.kategori.split(";");
					if (option.length > 0) {
						var counter = 1;
						option.forEach(function(item, index) {
							if (counter == 1)
								option_html += "<option selected>" + item + "</option>";
							else
								option_html += "<option>" + item + "</option>";
							counter++;
						});
					}
					$("#pesanan_pasien_pk_siang").html(option_html);
				};
				menu_siang.selectMenu = function(id_menu, jenis_karbohidrat, tarif) {
					if (id_menu == 0 || id_menu == null)
						return;
					hide_chooser();
					showLoading();
					var self = this;
					var edit_data = this.getEditData(id_menu);
					$.post(
						"",
						edit_data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								return;
							}
							self.selectedMenu(json, jenis_karbohidrat, tarif);
					        dismissLoading();
						}
					);
				};
				menu_siang.selectedMenu = function(json, jenis_karbohidrat, tarif) {
					$("#pesanan_pasien_id_menu_siang").val(json.id);
					$("#pesanan_pasien_menu_siang_tarif").val(tarif);
					$("#pesanan_pasien_menu_siang").val(json.nama);
					var option_html = "";
					var option = json.kategori.split(";");
					if (option.length > 0) {
						option.forEach(function(item, index) {
							if (item == jenis_karbohidrat)
								option_html += "<option selected>" + item + "</option>";
							else
								option_html += "<option>" + item + "</option>";
						});
					}
					$("#pesanan_pasien_pk_siang").html(option_html);
				};
				menu_siang.clear = function() {
					$("#pesanan_pasien_id_menu_siang").val("");
					$("#pesanan_pasien_menu_siang").val("");
					$("#pesanan_pasien_menu_siang_tarif").val(0);
					$("#pesanan_pasien_pk_siang").html("");
				};
				menu_siang.setSuperCommand("menu_siang");

				menu_malam = new TableAction(
					"menu_malam",
					PAGE,
					ACTION,
					new Array()
				);
				menu_malam.chooser = function(modal, elm_id, param, action, modal_title) {
					if ($("#pesanan_pasien_kelas").val() == "") {
						$("#modal_alert_pesanan_pasien_add_form").html(
							"<div class='alert alert-block alert-danger'>" +
								"<h4>Peringatan</h4>" +
								"<b>Kelas</b> tidak boleh kosong." +
							"</div>"
						);
						return;
					}
				    var data = action.getChooserData();
				    if(data == null) {
				       var d = this.getRegulerData();
				       data = this.addChooserData(d);
				    }
				    var self = this;
					data['super_command'] = param;
					$.post(
						"",
						data,
						function(response) {
							show_chooser(self, param, response, action.getShowParentModalInChooser(), modal_title);
							action.view();
							action.focusSearch();
							this.current_chooser = action;
							CURRENT_SMIS_CHOOSER = action;
						}
					);
					return this;
				};
				menu_malam.addViewData = function(data) {
					data['kelas'] = $("#pesanan_pasien_kelas").val();
					data['waktu'] = "malam";
					data['tanggal'] = $("#pesanan_pasien_tanggal").val();
					return data;
				};
				menu_malam.selected = function(json) {
					$("#pesanan_pasien_id_menu_malam").val(json.id);
					$("#pesanan_pasien_menu_malam_tarif").val(json.tarif_sekarang);
					$("#pesanan_pasien_menu_malam").val(json.nama);
					var option_html = "";
					var option = json.kategori.split(";");
					if (option.length > 0) {
						var counter = 1;
						option.forEach(function(item, index) {
							if (counter == 1)
								option_html += "<option selected>" + item + "</option>";
							else
								option_html += "<option>" + item + "</option>";
							counter++;
						});
					}
					$("#pesanan_pasien_pk_malam").html(option_html);
				};
				menu_malam.selectMenu = function(id_menu, jenis_karbohidrat, tarif) {
					if (id_menu == 0 || id_menu == null)
						return;
					hide_chooser();
					showLoading();
					var self = this;
					var edit_data = this.getEditData(id_menu);
					$.post(
						"",
						edit_data,
						function(response) {
							var json = getContent(response);
							if (json == null) {
								return;
							}
							self.selectedMenu(json, jenis_karbohidrat, tarif);
					        dismissLoading();
						}
					);
				};
				menu_malam.selectedMenu = function(json, jenis_karbohidrat, tarif) {
					$("#pesanan_pasien_id_menu_malam").val(json.id);
					$("#pesanan_pasien_menu_malam_tarif").val(tarif);
					$("#pesanan_pasien_menu_malam").val(json.nama);
					var option_html = "";
					var option = json.kategori.split(";");
					if (option.length > 0) {
						option.forEach(function(item, index) {
							if (item == jenis_karbohidrat)
								option_html += "<option selected>" + item + "</option>";
							else
								option_html += "<option>" + item + "</option>";
						});
					}
					$("#pesanan_pasien_pk_malam").html(option_html);
				};
				menu_malam.clear = function() {
					$("#pesanan_pasien_id_menu_malam").val("");
					$("#pesanan_pasien_menu_malam").val("");
					$("#pesanan_pasien_menu_malam_tarif").val(0);
					$("#pesanan_pasien_pk_malam").html("");
				};
				menu_malam.setSuperCommand("menu_malam");

				$("#smis-chooser-modal").on("show", function() {
					if ($("#smis-chooser-modal .modal-header h3").text() == "PASIEN RAWAT INAP AKTIF") {
						$("#smis-chooser-modal").removeClass("half_model");
						$("#smis-chooser-modal").removeClass("full_model");
						$("#smis-chooser-modal").addClass("full_model");
						$("table#table_pasien tfoot tr").eq(0).hide();
					} else {
						$("#smis-chooser-modal").removeClass("half_model");
						$("#smis-chooser-modal").removeClass("full_model");
						$("#smis-chooser-modal").addClass("half_model");
						$("table tfoot tr").eq(0).show();
					}
				});
			});
		</script>
		<?php
	}

	public function cssPreLoad() {
		?>
		<style type="text/css">
			#pesanan_pasien_1 label,
			#pesanan_pasien_2 label,
			#pesanan_pasien_3 label {
			    float: left;
			    width: 100px;
			    height: 20px;
			}
			.pesanan_pasien_pk_pagi label,
			.pesanan_pasien_menu_pagi label,
			.pesanan_pasien_pk_siang label,
			.pesanan_pasien_menu_siang label,
			.pesanan_pasien_pk_malam label,
			.pesanan_pasien_menu_malam label {
				width: 125px !important;
			}
			form#pesanan_pasien_2 > div, 
			form#pesanan_pasien_3 > div {
				width: 47%;
			}
			.search-header-tiny {
				width: 100px !important;
			}
			.search-header-med {
				width: 150px !important;
			}
			.search-header-big {
				width: 200px !important;
			}
			.modal-body {
				max-height: 425px !important;
			}
		</style>
		<?php
	}
}
?>