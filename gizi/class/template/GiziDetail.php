<?php
require_once ("smis-framework/smis/template/ModulTemplate.php");
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';

 class GiziDetail extends ModulTemplate {

	private $uitable;
	private $dbres;
	private $dbtable;
	private $db;
	private $mode;
	
	public function __construct($db) {
		parent::__construct ();
		$this->db = $db;
		$header=array ("No.",'Waktu',"Pengasuh","Nama Pasien","No. Reg",'NRM','Harga','Jumlah',"Total","Cara Bayar","Ruangan");
		$uitable = new Table ( $header, "", NULL, false );
		$uitable->setDelButtonEnable(false);
		$uitable->setEditButtonEnable(false);
		$uitable->setReloadButtonEnable(false);
		$uitable->setAddButtonEnable(false);
		$uitable->setName ( "laporan_asuhan" );
		$this->uitable=$uitable;
		
	}
	
	public function getCaraBayar(){
		$ser=new ServiceConsumer($this->db,"get_carabayar",NULL,"registration");
		$ser->execute();
		$content=$ser->getContent();
		$result=array();
		$result[]=array("name"=>"","value"=>"%","default"=>"1");
		foreach($content as $x){
			$result[]=array("name"=>$x['nama'],"value"=>$x['slug'],"default"=>"0");
		}
		return $result;
	}
	
	private function detail(Table &$uitable,SummaryAdapter &$adapter,DBTable &$dbtable){
		$adapter->addFixValue("Waktu","<strong>Total</strong>");
		$adapter->addSummary("Total","total","money Rp.");
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Waktu", "waktu","date d M Y");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. Reg", "noreg_pasien");
		$adapter->add("NRM", "nrm_pasien");
		$adapter->add("Pengasuh", "nama_pengasuh");
		$adapter->add("Harga", "harga","money Rp.");
		$adapter->add("Harga Asli", "harga");
		$adapter->add("Jumlah", "jumlah");
		$adapter->add("Total", "total","money Rp.");
		$adapter->add("Total Asli", "total");
		$adapter->add("Cara Bayar", "carabayar","unslug");
        $adapter->add("Ruangan", "ruangan","unslug");
		
		/*if($_POST['command']=="excel"){
			$adapter->add("Waktu", "waktu","date d/m/Y");
			$adapter->add("Harga", "harga");
			$adapter->add("Total", "total");
			$adapter->addFixValue("Waktu","Total");
			$adapter->addSummary("Total","harga");
			$adapter->addFixValue("Nama Pasien", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("No. Reg", "");
			$adapter->addFixValue("NRM", "");
			$adapter->addFixValue("Jumlah", "");
			$adapter->addFixValue("No", "");
            $adapter->addFixValue("Ruangan", "");
		}*/
	}
	
	
	private function carabayar(Table &$uitable,SummaryAdapter &$adapter,DBTable &$dbtable){
		$qv="
			SELECT 
				'-' as waktu,
				smis_gz_asuhan.biaya as harga,
				sum(1) as jumlah,
                sum(smis_gz_asuhan.biaya) as total,
				'-' as noreg_pasien,
				'-' as nrm_pasien,
				'-' as nama_pasien,
				'-' as nama_pengasuh,
				'-' as ruangan,
                smis_rg_layananpasien.carabayar as carabayar
			FROM 
				smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
		";
			
		$qc="
			SELECT 
				COUNT(smis_gz_asuhan.id) as total 
			FROM 
				smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
		";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," smis_rg_layananpasien.carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
	}
    
    private function ruangan(Table &$uitable,SummaryAdapter &$adapter,DBTable &$dbtable){
		$qv="
			SELECT 
				'-' as waktu,
				smis_gz_asuhan.biaya as harga,
				sum(1) as jumlah,
				sum(smis_gz_asuhan.biaya) as total,
				'-' as noreg_pasien,
				'-' as nrm_pasien,
				'-' as nama_pasien,
				'-' as nama_pengasuh,
				smis_gz_asuhan.ruangan as ruangan,
				'-' as carabayar
			FROM 
				smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
		";
			
			
		$qc="
			SELECT 
				COUNT(smis_gz_asuhan.id) as total 
			FROM 
				smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
		";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," smis_gz_asuhan.ruangan ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
    
    private function ruangan_carabayar(Table &$uitable,SummaryAdapter &$adapter,DBTable &$dbtable){
		$qv="
			SELECT 
				'-' as waktu,
				smis_gz_asuhan.biaya as harga,
				sum(1) as jumlah,
				sum(smis_gz_asuhan.biaya) as total,
				sum(smis_gz_asuhan.biaya)  as harga_total,					
				'-' as noreg_pasien,
				'-' as nrm_pasien,
				'-' as nama_pasien,
				'-' as nama_pengasuh,
				smis_gz_asuhan.ruangan as ruangan,
				smis_rg_layananpasien.carabayar as carabayar
			FROM 
				smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
		";
			
			
		$qc="
			SELECT 
				COUNT(smis_gz_asuhan.id) as total 
			FROM 
				smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
		";
		$dbtable->setPreferredQuery(true,$qv,$qc);
		$dbtable->setMaskingCount(true," SELECT count(*) as total FROM ([query])X ");
		$dbtable->setGroupBy(true," smis_gz_asuhan.ruangan, smis_rg_layananpasien.carabayar ");
		$dbtable->setUseWhereforView(true);
		$this->detail($uitable,$adapter,$dbtable);
		$adapter->add("Waktu", "waktu");
		

	}
	
	
	
	public function command($command) {		
		$this->dbtable = new DBTable ( $this->db, 'smis_gz_asuhan' );
		$this->dbtable->addCustomKriteria(NULL,"smis_gz_asuhan.tanggal>='".$_POST['dari']."'");
		$this->dbtable->addCustomKriteria(NULL,"smis_gz_asuhan.tanggal<='".$_POST['sampai']."'");
		$this->dbtable->addCustomKriteria("smis_rg_layananpasien.carabayar"," LIKE '".$_POST['carabayar']."'");
        
		if($_POST['nama_pengasuh']!=""){
			$this->dbtable->addCustomKriteria("smis_gz_asuhan.nama_pengasuh"," = '".$_POST['nama_pengasuh']."'");
		}
		$this->dbtable->setOrder ( " smis_gz_asuhan.tanggal ASC " );
		$adapter=new SummaryAdapter();
		
		if(isset($_POST['grup']) && $_POST['grup']=="carabayar" ){
			$this->carabayar($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="ruangan" ){
			$this->ruangan($this->uitable,$adapter,$this->dbtable);
		}else if(isset($_POST['grup']) && $_POST['grup']=="ruangan_carabayar" ){
			$this->ruangan_carabayar($this->uitable,$adapter,$this->dbtable);
		}else{			
			$qv="
				SELECT 
					smis_gz_asuhan.tanggal as waktu,
					smis_gz_asuhan.biaya as harga,
					1 as jumlah,
                    smis_gz_asuhan.biaya as total,
					smis_gz_asuhan.noreg_pasien as noreg_pasien,
					smis_gz_asuhan.nrm_pasien as nrm_pasien,
					smis_gz_asuhan.nama_pengasuh as nama_pengasuh,
					smis_gz_asuhan.nama_pasien as nama_pasien,
                    smis_gz_asuhan.ruangan as ruangan,
					smis_rg_layananpasien.carabayar as carabayar
				FROM 
					smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
			";			
			$qc="
				SELECT 
					COUNT(smis_gz_asuhan.id) as total 
				FROM 
					smis_gz_asuhan LEFT JOIN smis_rg_layananpasien ON smis_gz_asuhan.noreg_pasien = smis_rg_layananpasien.id
			";
			$this->dbtable->setPreferredQuery(true,$qv,$qc);
			$this->dbtable->setUseWhereforView(true);			
			
			$this->detail($this->uitable,$adapter,$this->dbtable);
		}
		
		if(isset($_POST['orderby']) && $_POST['orderby']!="" ){
			$this->dbtable->setOrder($_POST['orderby'],true);
		}	
		require_once "gizi/class/responder/GiziDetailResponder.php";
		$this->dbres = new GiziDetailResponder( $this->dbtable, $this->uitable, $adapter );
		//$this->dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter );
		
		
		$data = $this->dbres->command ( $_POST ['command'] );
		if($data!=null){
			echo json_encode ( $data );
		}
	}
	
	
	
	public function superCommand($super_command) {
		$dkadapter = new SimpleAdapter ();
		$dkadapter->add ( "Jabatan", "nama_jabatan" );
		$dkadapter->add ( "Nama", "nama" );
		$dkadapter->add ( "NIP", "nip" );
		$header=array ('Nama','Jabatan',"NIP" );
		$dktable = new Table ( $header);
		$dktable->setName ( "pengasuh_laporan_asuhan" );
		$dktable->setModel ( Table::$SELECT );
		$pengasuh = new EmployeeResponder ( $this->db, $dktable, $dkadapter, "" );
		
		$super = new SuperCommand ();
		$super->addResponder ( "pengasuh_laporan_asuhan", $pengasuh );
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	/* when it's star build */
	public function phpPreload() {
		
		$grup=new OptionBuilder();
		$grup->add("","","1");	
        $grup->add("Grup By Carabayar","carabayar");		
		$grup->add("Grup By Ruangan","ruangan");		
		$grup->add("Grup By Ruangan/Carabayar","ruangan_carabayar");		
		
		$order=new OptionBuilder();
		$order->add("","","1");
		$order->add("Order By Carabayar"," carabayar ASC ");
		$order->add("Order By Ruangan"," ruangan ASC ");
		
		
		$carabayar=$this->getCaraBayar();
		$this->uitable->addModal("dari", "date", "Dari", "");
		$this->uitable->addModal("sampai", "date", "Sampai", "");
		$this->uitable->setFooterVisible(true);
		$this->uitable->addModal("carabayar", "select", "Cara Bayar",$carabayar);
		$this->uitable->addModal("nama_pengasuh", "chooser-laporan_asuhan-pengasuh_laporan_asuhan-pengasuh", "pengasuh","");
		$this->uitable->addModal("grup", "select", "Grup",$grup->getContent());
		$this->uitable->addModal("orderby", "select", "Order By",$order->getContent());
		$form=$this->uitable->getModal()->getForm();
		
		$btg=new ButtonGroup("");
		$btg->setMax(100,"");
		$btn=new Button("", "", "View");
		$btn->setAction("laporan_asuhan.view()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Print");
		$btn->setAction("laporan_asuhan.print()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-print");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
		$btn=new Button("", "", "Excel");
		$btn->setAction("laporan_asuhan.excel()");
		$btn->setClass("btn btn-primary");
		$btn->setIcon(" fa fa-file-excel-o");
		$btn->setIsButton(Button::$ICONIC_TEXT);
		$btg->addButton($btn);
		
				
		$form->addElement("", $btg);
		
		
		echo $form->getHtml();
		echo $this->uitable->getHtml ();
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	}
	public function jsPreLoad() {
		?>
		<script type="text/javascript">
		var laporan_asuhan;		
		var pengasuh_laporan_asuhan; 		
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
			$('.mydate').datepicker();
			var column=new Array("");
			laporan_asuhan=new TableAction("laporan_asuhan","gizi","laporan_asuhan",column);
			laporan_asuhan.addViewData=function(save_data){
				save_data['command']="list";
				save_data['dari']=$("#laporan_asuhan_dari").val();
				save_data['sampai']=$("#laporan_asuhan_sampai").val();
				save_data['carabayar']=$("#laporan_asuhan_carabayar").val();
				save_data['nama_pengasuh']=$("#laporan_asuhan_nama_pengasuh").val();
				save_data['grup']=$("#laporan_asuhan_grup").val();
				save_data['orderby']=$("#laporan_asuhan_orderby").val();
				return save_data;
			};	
			
			
			pengasuh_laporan_asuhan=new TableAction("pengasuh_laporan_asuhan","gizi","laporan_asuhan",new Array());
			pengasuh_laporan_asuhan.setSuperCommand("pengasuh_laporan_asuhan");
			pengasuh_laporan_asuhan.selected=function(json){
				var nama=json.nama;
				$("#laporan_asuhan_nama_pengasuh").val(nama);
			};
			
		});
		</script>
<?php
	}
}?>
