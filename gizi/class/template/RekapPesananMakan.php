<?php
require_once("smis-base/smis-include-service-consumer.php");
require_once("gizi/class/responder/RekapPesananMakanDBResponder.php");

class RekapPesananMakan extends ModulTemplate {
	private $db;
	private $table;
	private $modal;
	private $page;
	private $action;

	public function __construct($db, $page, $action) {
		$this->db = $db;
		$this->page = $page;
		$this->action = $action;

		$this->table = new Table(
			array(
				"No.", "Ruangan", "Bed", "Nama Pasien", "No. RM", "No. Reg.", 
				"Pagi - Jenis Menu", "Pagi - Jns. Karbohidrat",
				"Siang - Jenis Menu", "Siang - Jns. Karbohidrat",
				"Malam - Jenis Menu", "Malam - Jns. Karbohidrat",
				"Status"
			),
			"",
			null,
			true
		);
		$this->table->setName("rekap_pesanan_pasien");
		$this->table->setEditButtonEnable(false);
		$this->table->setDelButtonEnable(false);
		$this->table->setHeaderVisible(false);
		$button = new Button("", "", "Cetak Tag");
		$button->setClass("btn-inverse");
		$button->setIsButton(Button::$ICONIC);
		$button->setIcon("fa fa-print");
		$this->table->addContentButton("print_tag", $button);
		$this->table->addHeader("before","
			<tr class='inverse'>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No.</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Ruangan</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Bed</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Nama Pasien</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No. RM</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>No. Registrasi</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Pagi</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Siang</center></small></th>
				<th colspan='2' style='vertical-align: middle !important;'><small><center>Malam</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Status</center></small></th>
				<th rowspan='2' style='vertical-align: middle !important;'><small><center>Aksi</center></small></th>
			</tr>
			<tr class='inverse'>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Jenis Menu</center></small></th>
				<th style='vertical-align: middle !important;'><small><center>Bentuk Makanan</center></small></th>
			</tr>
		");
	}

	public function command($command) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Ruangan", "ruang", "unslug");
		$adapter->add("Bed", "bed");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("No. RM", "nrm_pasien", "digit6");
		$adapter->add("No. Reg.", "noreg_pasien", "digit6");
		$adapter->add("Kelas", "kelas", "unslug");
		$adapter->add("Pagi - Jenis Menu", "menu_pagi");
		$adapter->add("Pagi - Jns. Karbohidrat", "pk_pagi");
		$adapter->add("Siang - Jenis Menu", "menu_siang");
		$adapter->add("Siang - Jns. Karbohidrat", "pk_siang");
		$adapter->add("Malam - Jenis Menu", "menu_malam");
		$adapter->add("Malam - Jns. Karbohidrat", "pk_malam");
		$adapter->add("Status", "selesai", "trivial_1_Diarsipkan_-");
		$dbtable = new DBTable($this->db, "smis_gz_pesanan");
		if (isset($_POST['tanggal']))
			$dbtable->addCustomKriteria(" tanggal ", " = '" . $_POST['tanggal'] . "' ");
		if (isset($_POST['ruang'])) {			
			if ($_POST['ruang'] == "SEMUA")
				$dbtable->addCustomKriteria(" ruang ", " LIKE '%%' ");
			else
				$dbtable->addCustomKriteria(" ruang ", " LIKE '" . $_POST['ruang'] . "' ");
		}
		$dbtable->setOrder(" tanggal DESC, id DESC ");
		$dbresponder = new RekapPesananMakanDBResponder(
			$dbtable,
			$this->table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
	}

	public function jsLoader() {
		echo addJS("framework/smis/js/table_action.js");
	    echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		
	}

	public function cssLoader() {
		echo addCSS("framework/bootstrap/css/datepicker.css");
		echo addCSS("gizi/resource/css/pesanan.css",false);
	}

	public function phpPreload() {
		$form = new Form("", "", "Rekap Pesanan Pasien");
		$tanggal_text = new Text("rekap_pesanan_pasien_tanggal", "rekap_pesanan_pasien_tanggal", date("Y-m-d"));
		$tanggal_text->setClass("mydate");
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$form->addElement("Tanggal", $tanggal_text);
		$unit_option = new OptionBuilder();
		$unit_option->add("SEMUA", "SEMUA", "1");
		$unit_service = new ServiceConsumer($this->db, "get_urjip");
	    $unit_service->setCached(true, "get_urjip");
	    $unit_service->setMode(ServiceConsumer::$MULTIPLE_MODE);
	    $unit_service->execute();
	    $content = $unit_service->getContent();
	    foreach ($content as $c) {
	    	foreach ($c as $unit => $urjip)
	    		if ($urjip[$unit] == "URI") {
	    			$ruang_row = $this->db->get_row("
						SELECT id, slug, nama
						FROM smis_adm_prototype
						WHERE slug = '" . $unit . "'
					");
					$ruangan = $unit;
					if ($ruang_row != null)
						$ruangan = $ruang_row->nama;
	    			$unit_option->addSingle(ArrayAdapter::format("unslug", $ruangan));
	    		}
	    }
	    $unit_select = new Select("rekap_pesanan_pasien_unit", "rekap_pesanan_pasien_unit", $unit_option->getContent());
	    $form->addElement("Ruangan", $unit_select);
		$waktu_option = new OptionBuilder();
	    $waktu_option->add("Pagi", "Pagi", "1");
	    $waktu_option->add("Siang", "Siang");
	    $waktu_option->add("Malam", "Malam");
	    $waktu_select = new Select("rekap_pesanan_pasien_waktu", "rekap_pesanan_pasien_waktu", $waktu_option->getContent());
		$form->addElement("Waktu", $waktu_select);
		
		$proses_button = new Button("", "", "Proses");
		$proses_button->setClass("btn-inverse");
		$proses_button->setIsButton(Button::$ICONIC_TEXT);
		$proses_button->setIcon("fa fa-refresh");
		$proses_button->setAction("rekap_pesanan_pasien.view()");
		$cetak_tag_button = new Button("", "", "Cetak Tag");
		$cetak_tag_button->setClass("btn-inverse");
		$cetak_tag_button->setIsButton(Button::$ICONIC_TEXT);
		$cetak_tag_button->setIcon("fa fa-print");
		$cetak_tag_button->setAction("rekap_pesanan_pasien.print_all_tag()");
		$cetak_bon_button = new Button("", "", "Cetak Bon Pesanan");
		$cetak_bon_button->setClass("btn-inverse");
		$cetak_bon_button->setIsButton(Button::$ICONIC_TEXT);
		$cetak_bon_button->setIcon("fa fa-tag");
		$cetak_bon_button->setAction("rekap_pesanan_pasien.print_all_bon()");
		$archive_button = new Button("", "", "Arsipkan");
		$archive_button->setClass("btn-inverse");
		$archive_button->setIsButton(Button::$ICONIC_TEXT);
		$archive_button->setIcon("fa fa-archive");
		$archive_button->setAction("rekap_pesanan_pasien.archive_all()");
		$button_group = new ButtonGroup("no-print");
		$button_group->setMax(5, "Aksi");
		$button_group->addButton($proses_button);
		$button_group->addButton($cetak_tag_button);
		$button_group->addButton($cetak_bon_button);
		$button_group->addButton($archive_button);
		$form->addElement("", $button_group);

		echo $form->getHtml();
		echo $this->table->getHtml();
	}

	public function jsPreLoad() {
		?>
		<script type="text/javascript">
			var rekap_pesanan_pasien;
			var PAGE = "<?php echo $this->page; ?>";
			var ACTION = "<?php echo $this->action; ?>";
			$(document).ready(function() {
				$(".mydate").datepicker();

				rekap_pesanan_pasien = new TableAction(
					"rekap_pesanan_pasien",
					PAGE,
					ACTION,
					new Array()
				);
				rekap_pesanan_pasien.getRegulerData = function() {
					var data = TableAction.prototype.getRegulerData.call(this);
					data['tanggal'] = $("#rekap_pesanan_pasien_tanggal").val();
					data['ruang'] = $("#rekap_pesanan_pasien_unit").val();
					data['waktu'] = $("#rekap_pesanan_pasien_waktu").val();
					return data;
				};
				rekap_pesanan_pasien.archive_all = function() {
					var self = this;
					bootbox.confirm(
						"Yakin mengarsipkan data tersebut ?",
						function(result) {
							if (result) {
								showLoading();
								var data = self.getRegulerData();
								data['command'] = "archive_all";
								$.post(
									"",
									data,
									function(response) {
										var json = getContent(response);
										self.view();
										dismissLoading();
									}
								);
							}
						}
					);
				};
				rekap_pesanan_pasien.print_all_tag = function() {
					if ($("#rekap_pesanan_pasien_unit").val() == "SEMUA") {
						bootbox.alert("<b>Ruangan</b> harus dipilih terlebih dahulu untuk dapat mencetak Label.");
						return;	
					}
					showLoading();
					var data = this.getRegulerData();
					data['command'] = "print_all_tag";
					<?php if (getSettings($this->db, "gizi-label-jenis_luaran", "html") == "xls") { ?>
						postForm(data);
						dismissLoading();
					<?php } else { ?> 
						$.post(
							"",
							data,
							function(response) {
								var json = getContent(response);
								if (json == null) {
									dismissLoading();
									return;
								}
								smis_print(json);
								dismissLoading();
							}
						);
					<?php } ?>
				};
				rekap_pesanan_pasien.print_tag = function(id) {
					showLoading();
					var data = this.getRegulerData();
					data['command'] = "print_tag";
					data['id'] = id;
					data['command'] = "print_tag";
					<?php if (getSettings($this->db, "gizi-label-jenis_luaran", "html") == "xls") { ?>
						postForm(data);
						dismissLoading();
					<?php } else { ?> 
						$.post(
							"",
							data,
							function(response) {
								var json = getContent(response);
								if (json == null) {
									dismissLoading();
									return;
								}
								smis_print(json);
								dismissLoading();
							}
						);
					<?php } ?>
				};
				rekap_pesanan_pasien.print_all_bon = function() {
					if ($("#rekap_pesanan_pasien_waktu").val() == "%%") {
						bootbox.alert("<b>Waktu</b> harus dipilih terlebih dahulu untuk dapat mencetak Bon.");
						return;
					}
					if ($("#rekap_pesanan_pasien_unit").val() == "%%") {
						bootbox.alert("<b>Ruangan</b> harus dipilih terlebih dahulu untuk dapat mencetak Bon.");
						return;	
					}
					showLoading();
					var data = this.getRegulerData();
					data['command'] = "print_all_bon";
					postForm(data);
					dismissLoading();
				};
				rekap_pesanan_pasien.view();
				$("#table_rekap_pesanan_pasien > tfoot > tr > td:nth-child(1) > span > div").hide();
			});
		</script>
		<?php
	}
}
?>
