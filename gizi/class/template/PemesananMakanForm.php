<?php
	require_once ('smis-framework/smis/template/ModulTemplate.php');
	require_once ("smis-base/smis-include-service-consumer.php");

	class PemesananMakanForm extends ModulTemplate {
		protected $db;
		protected $page;
		protected $protoslug;
		protected $protoname;
		protected $protoimplement;
		protected $action;
		protected $dbtable;
		protected $uitable;

		private $id;
		private $tanggal;
		private $noreg_pasien;
		private $nama_pasien;
		private $nrm_pasien;
		private $jenis_pasien;
		private $polislug;
		private $nama_ruangan;
		private $bed;
		private $kelas;
		private $jenis_diet;
		private $jenis_diet_option;
		private $keterangan;
		private $alergi;
		private $id_menu_pagi;
		private $menu_pagi;
		private $menu_pagi_tarif;
		private $pk_pagi;
		private $pk_pagi_option;
		private $id_menu_siang;
		private $menu_siang;
		private $menu_siang_tarif;
		private $pk_siang;
		private $pk_siang_option;
		private $id_menu_malam;
		private $menu_malam;
		private $menu_malam_tarif;
		private $pk_malam;
		private $pk_malam_option;
		private $selesai;

		public function __construct(
			$db, 
			$polislug = "", $bed = "", $kelas = "",
			$noreg_pasien = "", $nrm_pasien = "", $nama_pasien = "", $jenis_pasien = "", 
			$page, $action, $protoslug, $protoname, $protoimplement
		) {
			$this->db = $db;
			$this->page = $page;
			$this->protoslug = $protoslug;
			$this->bed = $bed;
			$this->protoimplement = $protoimplement;
			$this->protoname = $protoname;
			$this->action = $action;

			$this->dbtable = new DBTable ( $this->db, "smis_gz_pesanan" );
			$this->uitable = new Table ( array(), "", null, true );
			$this->uitable->setName( "makanan_gizi" );

			$this->id = "";
			$this->tanggal = date("Y-m-d");
			$this->noreg_pasien = $noreg_pasien;
			$this->nrm_pasien = $nrm_pasien;
			$this->nama_pasien = $nama_pasien;
			$this->jenis_pasien = $jenis_pasien;
			$this->polislug = $polislug;

			$ruang_row = $this->db->get_row("
				SELECT id, slug, nama
				FROM smis_adm_prototype
				WHERE slug = '" . $polislug . "' AND parent = 'rawat'
			");
			$this->nama_ruangan = "-";
			if ($ruang_row != null)
				$this->nama_ruangan = $ruang_row->nama;

			$this->kelas = $kelas;
			$this->jenis_diet = "";
			$this->keterangan = "";
			$this->alergi = "";
			$this->id_menu_pagi = 0;
			$this->menu_pagi = "";
			$this->menu_pagi_tarif = 0;
			$this->pk_pagi = "";
			$this->pk_pagi_option = new OptionBuilder();
			$this->id_menu_siang = 0;
			$this->menu_siang = "";
			$this->menu_siang_tarif = 0;
			$this->pk_siang = "";
			$this->pk_siang_option = new OptionBuilder();
			$this->id_menu_malam = 0;
			$this->menu_malam = "";
			$this->menu_malam_tarif = 0;
			$this->pk_malam = "";
			$this->pk_malam_option = new OptionBuilder();
			$this->selesai = 0;

			$code = md5($this->tanggal . " " . $this->noreg_pasien . " " . $this->polislug);
			$row = $this->dbtable->get_row("
				SELECT *
				FROM smis_gz_pesanan
				WHERE code = '" . $code . "'
			");
			if ($row != null) {
				$this->id = $row->id;
				$this->jenis_diet = $row->jenis_diet;
				$this->keterangan = $row->keterangan;
				$this->alergi = $row->alergi;
				$this->id_menu_pagi = $row->id_menu_pagi;
				$this->menu_pagi = $row->menu_pagi;
				$this->menu_pagi_tarif = $row->menu_pagi_tarif;
				$this->pk_pagi = $row->pk_pagi;
				$pk_row = $this->dbtable->get_row("
					SELECT kategori
					FROM smis_gz_menu
					WHERE id = '" . $this->id_menu_pagi . "'
				");
				if ($pk_row != null) {
					$pk_arr = explode(";", $pk_row->kategori);
					foreach ($pk_arr as $pk) {
						if ($pk == $this->pk_pagi)
							$this->pk_pagi_option->add($pk, $pk, "1");
						else
							$this->pk_pagi_option->add($pk, $pk);
					}
				}
				$this->id_menu_siang = $row->id_menu_siang;
				$this->menu_siang = $row->menu_siang;
				$this->menu_siang_tarif = $row->menu_siang_tarif;
				$this->pk_siang = $row->pk_siang;
				$pk_row = $this->dbtable->get_row("
					SELECT kategori
					FROM smis_gz_menu
					WHERE id = '" . $this->id_menu_siang . "'
				");
				if ($pk_row != null) {
					$pk_arr = explode(";", $pk_row->kategori);
					foreach ($pk_arr as $pk) {
						if ($pk == $this->pk_siang)
							$this->pk_siang_option->add($pk, $pk, "1");
						else
							$this->pk_siang_option->add($pk, $pk);
					}
				}
				$this->id_menu_malam = $row->id_menu_malam;
				$this->menu_malam = $row->menu_malam;
				$this->menu_malam_tarif = $row->menu_malam_tarif;
				$this->pk_malam = $row->pk_malam;
				$pk_row = $this->dbtable->get_row("
					SELECT kategori
					FROM smis_gz_menu
					WHERE id = '" . $this->id_menu_malam . "'
				");
				if ($pk_row != null) {
					$pk_arr = explode(";", $pk_row->kategori);
					foreach ($pk_arr as $pk) {
						if ($pk == $this->pk_malam)
							$this->pk_malam_option->add($pk, $pk, "1");
						else
							$this->pk_malam_option->add($pk, $pk);
					}
				}
				$this->selesai = $row->selesai;
			}
		}

		public function phpPreLoad() {
			$form_1 = new Form("pesanan_pasien_1", "", "");
			$id_hidden = new Hidden("pesanan_pasien_id", "pesanan_pasien_id", $this->id);
			$form_1->addElement("", $id_hidden);
			$tanggal_text = new Text("pesanan_pasien_tanggal", "pesanan_pasien_tanggal", date("Y-m-d"));
			$tanggal_text->setClass("mydate");
			$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd' disabled='disabled'");
			$form_1->addElement("Tanggal", $tanggal_text);
			$noreg_pasien_hidden = new Hidden("pesanan_pasien_noreg_pasien", "pesanan_pasien_noreg_pasien", $this->noreg_pasien);
			$form_1->addElement("", $noreg_pasien_hidden);
			$nrm_pasien_hidden = new Hidden("pesanan_pasien_nrm_pasien", "pesanan_pasien_nrm_pasien", $this->nrm_pasien);
			$form_1->addElement("", $nrm_pasien_hidden);
			$pasien_hidden = new Hidden("pesanan_pasien_nama_pasien", "pesanan_pasien_nama_pasien", $this->nama_pasien);
			$form_1->addElement("", $pasien_hidden);
			$jenis_pasien_hidden = new Hidden("pesanan_pasien_jenis_pasien", "pesanan_pasien_jenis_pasien", $this->jenis_pasien);
			$form_1->addElement("", $jenis_pasien_hidden);
			$ruangan_hidden = new Hidden("pesanan_pasien_ruang", "pesanan_pasien_ruang", $this->nama_ruangan);
			$form_1->addElement("", $ruangan_hidden);
			$bed_hidden = new Hidden("pesanan_pasien_bed", "pesanan_pasien_bed", $this->bed);
			$form_1->addElement("", $bed_hidden);
			$kelas_hidden = new Hidden("pesanan_pasien_kelas", "pesanan_pasien_kelas", $this->kelas);
			$form_1->addElement("", $kelas_hidden);
			$slug_ruang_hidden = new Hidden("pesanan_pasien_slug_ruang", "pesanan_pasien_slug_ruang", $this->polislug);
			$form_1->addElement("", $slug_ruang_hidden);
			if ($this->selesai == 0) {
				$jenis_diet_browse_button = new Button("", "", "Pilih");
				$jenis_diet_browse_button->setClass("btn-info");
				$jenis_diet_browse_button->setIsButton(Button::$ICONIC);
				$jenis_diet_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
				$jenis_diet_browse_button->setAction("jenis_diet.chooser('jenis_diet', 'jenis_diet_button', 'jenis_diet', jenis_diet, 'Jenis Diet')");
				$jenis_diet_browse_button->setAtribute("id='jenis_diet_browse'");
				$jenis_diet_clear_button = new Button("", "", "Hapus");
				$jenis_diet_clear_button->setClass("btn-inverse");
				$jenis_diet_clear_button->setIsButton(Button::$ICONIC);
				$jenis_diet_clear_button->setIcon("fa fa-times");
				$jenis_diet_clear_button->setAction("jenis_diet.clear()");
				$jenis_diet_clear_button->setAtribute("id='jenis_diet_clear'");
				$jenis_diet_text = new Text("pesanan_pasien_jenis_diet", "pesanan_pasien_jenis_diet", $this->jenis_diet);
				$jenis_diet_text->setAtribute("disabled='disabled'");
				$jenis_diet_text->setClass("smis-two-option-input");
				$jenis_diet_input_group = new InputGroup("");
				$jenis_diet_input_group->addComponent($jenis_diet_text);
				$jenis_diet_input_group->addComponent($jenis_diet_browse_button);
				$jenis_diet_input_group->addComponent($jenis_diet_clear_button);
				$form_1->addElement("Jenis Diet", $jenis_diet_input_group);
			} else {
				$jenis_diet_text = new Text("pesanan_pasien_jenis_diet", "pesanan_pasien_jenis_diet", $this->jenis_diet);
				$jenis_diet_text->setAtribute("disabled='disabled'");
				$form_1->addElement("Jenis Diet", $jenis_diet_text);
			}
			$keterangan_textarea = new TextArea("pesanan_pasien_keterangan", "pesanan_pasien_keterangan", $this->keterangan);
			$keterangan_textarea->setLine(2);
			if ($this->selesai == 1)
				$keterangan_textarea->setAtribute("disabled='disabled'");

			$alergi_textarea = new TextArea("pesanan_pasien_alergi", "pesanan_pasien_alergi", $this->alergi);
			$alergi_textarea->setLine(2);
			if ($this->selesai == 1)
				$alergi_textarea->setAtribute("disabled='disabled'");
	
				
				$form_1->addElement("Alergi", $alergi_textarea);
				$form_1->addElement("Keterangan", $keterangan_textarea);

			$id_menu_pagi_hidden = new Hidden("pesanan_pasien_id_menu_pagi", "pesanan_pasien_id_menu_pagi", $this->id_menu_pagi);
			$form_1->addElement("", $id_menu_pagi_hidden);
			$menu_pagi_tarif_hidden = new Hidden("pesanan_pasien_menu_pagi_tarif", "pesanan_pasien_menu_pagi_tarif", $this->menu_pagi_tarif);
			$form_1->addElement("", $menu_pagi_tarif_hidden);
			$id_menu_siang_hidden = new Hidden("pesanan_pasien_id_menu_siang", "pesanan_pasien_id_menu_siang", $this->id_menu_siang);
			$form_1->addElement("", $id_menu_siang_hidden);
			$menu_siang_tarif_hidden = new Hidden("pesanan_pasien_menu_siang_tarif", "pesanan_pasien_menu_siang_tarif", $this->menu_siang_tarif);
			$form_1->addElement("", $menu_siang_tarif_hidden);
			$id_menu_malam_hidden = new Hidden("pesanan_pasien_id_menu_malam", "pesanan_pasien_id_menu_malam", $this->id_menu_malam);
			$form_1->addElement("", $id_menu_malam_hidden);
			$menu_malam_tarif_hidden = new Hidden("pesanan_pasien_menu_malam_tarif", "pesanan_pasien_menu_malam_tarif", $this->menu_malam_tarif);
			$form_1->addElement("", $menu_malam_tarif_hidden);

			$form_2 = new Form("pesanan_pasien_2", "", "");
			$form_3 = new Form("pesanan_pasien_3", "", "");
			if ($this->selesai == 0) {
				$menu_pagi_browse_button = new Button("", "", "Pilih");
				$menu_pagi_browse_button->setClass("btn-info");
				$menu_pagi_browse_button->setIsButton(Button::$ICONIC);
				$menu_pagi_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
				$menu_pagi_browse_button->setAction("menu_pagi.chooser('menu_pagi', 'menu_pagi_button', 'menu_pagi', menu_pagi, 'Menu Pagi')");
				$menu_pagi_browse_button->setAtribute("id='menu_pagi_browse'");
				$menu_pagi_clear_button = new Button("", "", "Hapus");
				$menu_pagi_clear_button->setClass("btn-inverse");
				$menu_pagi_clear_button->setIsButton(Button::$ICONIC);
				$menu_pagi_clear_button->setIcon("fa fa-times");
				$menu_pagi_clear_button->setAction("menu_pagi.clear()");
				$menu_pagi_clear_button->setAtribute("id='menu_pagi_clear'");
				$menu_pagi_text = new Text("pesanan_pasien_menu_pagi", "pesanan_pasien_menu_pagi", $this->menu_pagi);
				$menu_pagi_text->setAtribute("disabled='disabled'");
				$menu_pagi_text->setClass("smis-two-option-input");
				$menu_pagi_input_group = new InputGroup("");
				$menu_pagi_input_group->addComponent($menu_pagi_text);
				$menu_pagi_input_group->addComponent($menu_pagi_browse_button);
				$menu_pagi_input_group->addComponent($menu_pagi_clear_button);
				$pk_pagi_select = new Select("pesanan_pasien_pk_pagi", "pesanan_pasien_pk_pagi", $this->pk_pagi_option->getContent());
				$form_2->addElement("", "
					<div id='pesanan_pasien_grup_menu_pagi' class='clear'>
						<div class='panel panel-default'>
							<div class='panel-heading'>Menu Pagi</div>
							<div class='panel-body'>
								<div class='pesanan_pasien_menu_pagi'> 
									<label>Menu</label>
									" . $menu_pagi_input_group->getHtml() . "
								</div>
								<div class='pesanan_pasien_pk_pagi'> 
									<label>Karbohidrat</label>
									" . $pk_pagi_select->getHtml() . "
								</div>
							</div>
						</div>	
					</div>
				");
				$menu_siang_browse_button = new Button("", "", "Pilih");
				$menu_siang_browse_button->setClass("btn-info");
				$menu_siang_browse_button->setIsButton(Button::$ICONIC);
				$menu_siang_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
				$menu_siang_browse_button->setAction("menu_siang.chooser('menu_siang', 'menu_siang_button', 'menu_siang', menu_siang, 'Menu Siang')");
				$menu_siang_browse_button->setAtribute("id='menu_siang_browse'");
				$menu_siang_clear_button = new Button("", "", "Hapus");
				$menu_siang_clear_button->setClass("btn-inverse");
				$menu_siang_clear_button->setIsButton(Button::$ICONIC);
				$menu_siang_clear_button->setIcon("fa fa-times");
				$menu_siang_clear_button->setAction("menu_siang.clear()");
				$menu_siang_clear_button->setAtribute("id='menu_siang_clear'");
				$menu_siang_text = new Text("pesanan_pasien_menu_siang", "pesanan_pasien_menu_siang", $this->menu_siang);
				$menu_siang_text->setAtribute("disabled='disabled'");
				$menu_siang_text->setClass("smis-two-option-input");
				$menu_siang_input_group = new InputGroup("");
				$menu_siang_input_group->addComponent($menu_siang_text);
				$menu_siang_input_group->addComponent($menu_siang_browse_button);
				$menu_siang_input_group->addComponent($menu_siang_clear_button);
				$pk_siang_select = new Select("pesanan_pasien_pk_siang", "pesanan_pasien_pk_siang", $this->pk_siang_option->getContent());
				$form_2->addElement("", "
					<div id='pesanan_pasien_grup_menu_siang' class='clear'>
						<div class='panel panel-default'>
							<div class='panel-heading'>Menu Siang</div>
							<div class='panel-body'>
								<div class='pesanan_pasien_menu_siang'> 
									<label>Menu</label>
									" . $menu_siang_input_group->getHtml() . "
								</div>
								<div class='pesanan_pasien_pk_siang'> 
									<label>Karbohidrat</label>
									" . $pk_siang_select->getHtml() . "
								</div>
							</div>
						</div>	
					</div>
				");
				$menu_malam_browse_button = new Button("", "", "Pilih");
				$menu_malam_browse_button->setClass("btn-info");
				$menu_malam_browse_button->setIsButton(Button::$ICONIC);
				$menu_malam_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
				$menu_malam_browse_button->setAction("menu_malam.chooser('menu_malam', 'menu_malam_button', 'menu_malam', menu_malam, 'Menu Malam')");
				$menu_malam_browse_button->setAtribute("id='menu_malam_browse'");
				$menu_malam_clear_button = new Button("", "", "Hapus");
				$menu_malam_clear_button->setClass("btn-inverse");
				$menu_malam_clear_button->setIsButton(Button::$ICONIC);
				$menu_malam_clear_button->setIcon("fa fa-times");
				$menu_malam_clear_button->setAction("menu_malam.clear()");
				$menu_malam_clear_button->setAtribute("id='menu_malam_clear'");
				$menu_malam_text = new Text("pesanan_pasien_menu_malam", "pesanan_pasien_menu_malam", $this->menu_malam);
				$menu_malam_text->setAtribute("disabled='disabled'");
				$menu_malam_text->setClass("smis-two-option-input");
				$menu_malam_input_group = new InputGroup("");
				$menu_malam_input_group->addComponent($menu_malam_text);
				$menu_malam_input_group->addComponent($menu_malam_browse_button);
				$menu_malam_input_group->addComponent($menu_malam_clear_button);
				$pk_malam_select = new Select("pesanan_pasien_pk_malam", "pesanan_pasien_pk_malam", $this->pk_malam_option->getContent());
				$form_3->addElement("", "
					<div id='pesanan_pasien_grup_menu_malam' class='clear'>
						<div class='panel panel-default'>
							<div class='panel-heading'>Menu Malam</div>
							<div class='panel-body'>
								<div class='pesanan_pasien_menu_malam'> 
									<label>Menu</label>
									" . $menu_malam_input_group->getHtml() . "
								</div>
								<div class='pesanan_pasien_pk_malam'> 
									<label>Karbohidrat</label>
									" . $pk_malam_select->getHtml() . "
								</div>
							</div>
						</div>	
					</div>
				");
			} else {
				$menu_pagi_text = new Text("pesanan_pasien_menu_pagi", "pesanan_pasien_menu_pagi", $this->menu_pagi);
				$menu_pagi_text->setAtribute("disabled='disabled'");
				$pk_pagi_text = new Text("pesanan_pasien_pk_pagi", "pesanan_pasien_pk_pagi", $this->pk_pagi);
				$pk_pagi_text->setAtribute("disabled='disabled'");
				$form_2->addElement("", "
					<div id='pesanan_pasien_grup_menu_pagi' class='clear'>
						<div class='panel panel-default'>
							<div class='panel-heading'>Menu Pagi</div>
							<div class='panel-body'>
								<div class='pesanan_pasien_menu_pagi'> 
									<label>Menu</label>
									" . $menu_pagi_text->getHtml() . "
								</div>
								<div class='pesanan_pasien_pk_pagi'> 
									<label>Karbohidrat</label>
									" . $pk_pagi_text->getHtml() . "
								</div>
							</div>
						</div>	
					</div>
				");
				$menu_siang_text = new Text("pesanan_pasien_menu_siang", "pesanan_pasien_menu_siang", $this->menu_siang);
				$menu_siang_text->setAtribute("disabled='disabled'");
				$pk_siang_text = new Text("pesanan_pasien_pk_siang", "pesanan_pasien_pk_siang", $this->pk_siang);
				$pk_siang_text->setAtribute("disabled='disabled'");
				$form_2->addElement("", "
					<div id='pesanan_pasien_grup_menu_siang' class='clear'>
						<div class='panel panel-default'>
							<div class='panel-heading'>Menu Siang</div>
							<div class='panel-body'>
								<div class='pesanan_pasien_menu_siang'> 
									<label>Menu</label>
									" . $menu_siang_text->getHtml() . "
								</div>
								<div class='pesanan_pasien_pk_siang'> 
									<label>Karbohidrat</label>
									" . $pk_siang_text->getHtml() . "
								</div>
							</div>
						</div>	
					</div>
				");
				$menu_malam_text = new Text("pesanan_pasien_menu_malam", "pesanan_pasien_menu_malam", $this->menu_malam);
				$menu_malam_text->setAtribute("disabled='disabled'");
				$pk_malam_text = new Text("pesanan_pasien_pk_malam", "pesanan_pasien_pk_malam", $this->pk_malam);
				$pk_malam_text->setAtribute("disabled='disabled'");
				$form_3->addElement("", "
					<div id='pesanan_pasien_grup_menu_malam' class='clear'>
						<div class='panel panel-default'>
							<div class='panel-heading'>Menu Malam</div>
							<div class='panel-body'>
								<div class='pesanan_pasien_menu_malam'> 
									<label>Menu</label>
									" . $menu_malam_text->getHtml() . "
								</div>
								<div class='pesanan_pasien_pk_malam'> 
									<label>Karbohidrat</label>
									" . $pk_malam_text->getHtml() . "
								</div>
							</div>
						</div>	
					</div>
				");
			}

			$save_button = new Button("", "", "Simpan");
			$save_button->setClass("btn-success");
			$save_button->setAction("pesanan_pasien.save()");
			$save_button->setIcon("fa fa-floppy-o");
			$save_button->setIsButton(Button::$ICONIC);

			echo "<div class='clear'>";
			echo $form_1->getHtml();
			echo "</div>";
			echo "<div class='clear'>";
			echo $form_2->getHtml();
			echo "</div>";
			echo "<div class='clear'>";
			echo $form_3->getHtml();
			echo "</div>";
			if ($this->selesai == 0) {
				echo "<div class='clear pull-right'>";
				echo $save_button->getHtml();
				echo "</div>";
			}
		}

		public function command($command) {
			if ($_POST['command'] == "get_id_by_code") {
				$code = md5($_POST['tanggal'] . " " . $_POST['noreg_pasien'] . " " . $_POST['ruang']);
				$row = $this->dbtable->get_row("
					SELECT id
					FROM smis_gz_pesanan
					WHERE code = '" . $code . "'
				");
				$id = "";
				if ($row != null)
					$id = $row->id;
				echo json_encode($id);
				return;
			}
			$adapter = new SimpleAdapter();
			if ($_POST['command'] == "save") {
				$_POST['code'] = md5($_POST['tanggal'] . " " . $_POST['noreg_pasien'] . " " . $_POST['slug_ruang']);
			}
			$dbresponder = new DBResponder(
				$this->dbtable,
				$this->uitable,
				$adapter
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
		}

		public function superCommand($scommand) {
			$jenis_diet_table = new Table(
				array("No.", "Jenis Diet", "Penyakit"),
				"",
				null,
				true
			);
			$jenis_diet_table->setName("jenis_diet");
			$jenis_diet_table->setModel(Table::$SELECT);
			$jenis_diet_adapter = new SimpleAdapter(true, "No.");
			$jenis_diet_adapter->add('id', 'id');
			$jenis_diet_adapter->add('Jenis Diet', 'nama');
			$jenis_diet_adapter->add('Penyakit', 'penyakit');
			$jenis_diet_dbtable = new DBTable($this->db, "smis_gz_diet");
			$jenis_diet_dbtable->setOrder("nama ASC");
			$jenis_diet_dbrepsonder = new DBResponder(
				$jenis_diet_dbtable,
				$jenis_diet_table,
				$jenis_diet_adapter
			);

			$menu_pagi_table = new Table(
				array("No.", "Nama Menu", "Jenis Menu", "Kelas", "Waktu", "Karbohidrat", "Energi", "Protein", "Lemak"),
				"",
				null,
				true
			);
			$menu_pagi_table->setName("menu_pagi");
			$menu_pagi_table->setModel(Table::$SELECT);
			$menu_pagi_adapter = new SimpleAdapter(true, "No.");
			$menu_pagi_adapter->add('id', 'id');
			$menu_pagi_adapter->add('Nama Menu', 'nama');
			$menu_pagi_adapter->add('Jenis Menu', 'jenis_menu');
			$menu_pagi_adapter->add('Kelas', 'kelas', 'unslug');
			$menu_pagi_adapter->add('Waktu', 'waktu');
			$menu_pagi_adapter->add('Karbohidrat', 'karbohidrat', "number");
			$menu_pagi_adapter->add('Energi', 'energi', "number");
			$menu_pagi_adapter->add('Protein', 'protein', "number");
			$menu_pagi_adapter->add('Lemak', 'lemak', "number");
			$menu_pagi_dbtable = new DBTable($this->db, "smis_gz_menu");
			$filter = " AND a.kelas LIKE '" . $_POST['kelas'] . "' AND a.waktu LIKE '" . $_POST['waktu'] . "' AND b.tanggal = '" . $_POST['tanggal'] . "' ";
			if (isset($_POST['kriteria']))
				$filter .= " AND (a.nama LIKE '%" . $_POST['kriteria'] . "%' OR a.jenis_menu LIKE '%" . $_POST['kriteria'] . "%') ";
			$query_value = "
				SELECT a.*, b.tanggal
				FROM smis_gz_menu a INNER JOIN smis_gz_jadwal b ON a.id_group = b.id_group
				WHERE a.prop = '' AND b.prop = '' " . $filter . "
				ORDER BY a.jenis_menu ASC, a.nama ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$menu_pagi_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$menu_pagi_dbresponder = new DBResponder(
				$menu_pagi_dbtable,
				$menu_pagi_table,
				$menu_pagi_adapter
			);

			$menu_siang_table = new Table(
				array("No.", "Nama Menu", "Jenis Menu", "Kelas", "Waktu", "Karbohidrat", "Energi", "Protein", "Lemak"),
				"",
				null,
				true
			);
			$menu_siang_table->setName("menu_siang");
			$menu_siang_table->setModel(Table::$SELECT);
			$menu_siang_adapter = new SimpleAdapter(true, "No.");
			$menu_siang_adapter->add('id', 'id');
			$menu_siang_adapter->add('Nama Menu', 'nama');
			$menu_siang_adapter->add('Jenis Menu', 'jenis_menu');
			$menu_siang_adapter->add('Kelas', 'kelas', 'unslug');
			$menu_siang_adapter->add('Waktu', 'waktu');
			$menu_siang_adapter->add('Karbohidrat', 'karbohidrat', "number");
			$menu_siang_adapter->add('Energi', 'energi', "number");
			$menu_siang_adapter->add('Protein', 'protein', "number");
			$menu_siang_adapter->add('Lemak', 'lemak', "number");
			$menu_siang_dbtable = new DBTable($this->db, "smis_gz_menu");
			$filter = " AND a.kelas LIKE '" . $_POST['kelas'] . "' AND a.waktu LIKE '" . $_POST['waktu'] . "' AND b.tanggal = '" . $_POST['tanggal'] . "' ";
			if (isset($_POST['kriteria']))
				$filter .= " AND (a.nama LIKE '%" . $_POST['kriteria'] . "%' OR a.jenis_menu LIKE '%" . $_POST['kriteria'] . "%') ";
			$query_value = "
				SELECT a.*, b.tanggal
				FROM smis_gz_menu a INNER JOIN smis_gz_jadwal b ON a.id_group = b.id_group
				WHERE a.prop = '' AND b.prop = '' " . $filter . "
				ORDER BY a.jenis_menu ASC, a.nama ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$menu_siang_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$menu_siang_dbresponder = new DBResponder(
				$menu_siang_dbtable,
				$menu_siang_table,
				$menu_siang_adapter
			);

			$menu_malam_table = new Table(
				array("No.", "Nama Menu", "Jenis Menu", "Kelas", "Waktu", "Karbohidrat", "Energi", "Protein", "Lemak"),
				"",
				null,
				true
			);
			$menu_malam_table->setName("menu_malam");
			$menu_malam_table->setModel(Table::$SELECT);
			$menu_malam_adapter = new SimpleAdapter(true, "No.");
			$menu_malam_adapter->add('id', 'id');
			$menu_malam_adapter->add('Nama Menu', 'nama');
			$menu_malam_adapter->add('Jenis Menu', 'jenis_menu');
			$menu_malam_adapter->add('Kelas', 'kelas', 'unslug');
			$menu_malam_adapter->add('Waktu', 'waktu');
			$menu_malam_adapter->add('Karbohidrat', 'karbohidrat', "number");
			$menu_malam_adapter->add('Energi', 'energi', "number");
			$menu_malam_adapter->add('Protein', 'protein', "number");
			$menu_malam_adapter->add('Lemak', 'lemak', "number");
			$menu_malam_dbtable = new DBTable($this->db, "smis_gz_menu");
			$filter = " AND a.kelas LIKE '" . $_POST['kelas'] . "' AND a.waktu LIKE '" . $_POST['waktu'] . "' AND b.tanggal = '" . $_POST['tanggal'] . "' ";
			if (isset($_POST['kriteria']))
				$filter .= " AND (a.nama LIKE '%" . $_POST['kriteria'] . "%' OR a.jenis_menu LIKE '%" . $_POST['kriteria'] . "%') ";
			$query_value = "
				SELECT a.*, b.tanggal
				FROM smis_gz_menu a INNER JOIN smis_gz_jadwal b ON a.id_group = b.id_group
				WHERE a.prop = '' AND b.prop = '' " . $filter . "
				ORDER BY a.jenis_menu ASC, a.nama ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$menu_malam_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$menu_malam_dbresponder = new DBResponder(
				$menu_malam_dbtable,
				$menu_malam_table,
				$menu_malam_adapter
			);

			$super_command = new SuperCommand();
			$super_command->addResponder("jenis_diet", $jenis_diet_dbrepsonder);
			$super_command->addResponder("menu_pagi", $menu_pagi_dbresponder);
			$super_command->addResponder("menu_siang", $menu_siang_dbresponder);
			$super_command->addResponder("menu_malam", $menu_malam_dbresponder);
			$init = $super_command->initialize();
			if ($init != null) {
				echo $init;
				return;
			}
		}

		public function jsLoader() {
			echo addJS("framework/smis/js/table_action.js");
	    	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		}

		public function cssLoader() {
			echo addCSS("framework/bootstrap/css/datepicker.css");
		}

		public function jsPreLoad() {
			?>
			<script type="text/javascript">
				var pesanan_pasien;
				var jenis_diet;
				var menu_pagi;
				var menu_siang;
				var menu_malam;
				var PAGE = "<?php echo $this->page; ?>";
				var ACTION = "<?php echo $this->action; ?>";
				var PROTO_SLUG = "<?php echo $this->protoslug; ?>";
				var PROTO_NAME = "<?php echo $this->protoname; ?>";
				var PROTO_IMPLEMENT = "<?php echo $this->protoimplement; ?>";
				$(document).ready(function() {
					$(".mydate").datepicker();

					var columns = new Array(
						"id", "tanggal", "noreg_pasien", "nrm_pasien", "nama_pasien", "jenis_pasien", "slug_ruang", "ruang", "bed", "kelas", 
						"id_menu_pagi", "menu_pagi", "pk_pagi", "menu_pagi_tarif",
						"id_menu_siang", "menu_siang", "pk_siang", "menu_siang_tarif",
						"id_menu_malam", "menu_malam", "pk_malam", "menu_malam_tarif",
						"jenis_diet", "keterangan","alergi"
					);
					pesanan_pasien = new TableAction(
						"pesanan_pasien",
						PAGE,
						ACTION,
						columns
					);
					pesanan_pasien.setPrototipe(PROTO_NAME, PROTO_SLUG, PROTO_IMPLEMENT);
					pesanan_pasien.clear = function() {
						var data = this.getRegulerData();
						data['command'] = "get_id_by_code";
						data['super_command'] = "";
						data['tanggal'] = $("#pesanan_pasien_tanggal").val();
						data['noreg_pasien'] = $("#pesanan_pasien_noreg_pasien").val();
						data['ruang'] = $("#pesanan_pasien_slug_ruang").val();
						$.post(
							"",
							data,
							function(response) {
								var json = JSON.parse(response);
								if (json == null) return;
								$("#pesanan_pasien_id").val(json);
							}
						);
					};
					pesanan_pasien.save = function() {
						if (<?php echo getSettings($this->db, "smis-rs-use-gizi-strict_entri_pemesanan-" . $this->polislug, 1); ?> == 1) {
							if ($("#pesanan_pasien_bed").val() == "") {
								bootbox.alert("Bed Tidak Boleh Kosong");
								return;
							}
						}
						TableAction.prototype.save.call(this);
					};

					jenis_diet = new TableAction(
						"jenis_diet",
						PAGE,
						ACTION,
						new Array()
					);
					jenis_diet.selected = function(json) {
						$("#pesanan_pasien_jenis_diet").val(json.nama);
					};
					jenis_diet.clear = function() {
						$("#pesanan_pasien_jenis_diet").val("");	
					};
					jenis_diet.setPrototipe(PROTO_NAME, PROTO_SLUG, PROTO_IMPLEMENT);
					jenis_diet.setSuperCommand("jenis_diet");

					menu_pagi = new TableAction(
						"menu_pagi",
						PAGE,
						ACTION,
						new Array()
					);
					menu_pagi.chooser = function(modal, elm_id, param, action, modal_title) {
						if ($("#pesanan_pasien_kelas").val() == "") {
							$("#modal_alert_pesanan_pasien_add_form").html(
								"<div class='alert alert-block alert-danger'>" +
									"<h4>Peringatan</h4>" +
									"<b>Kelas</b> tidak boleh kosong." +
								"</div>"
							);
							return;
						}
					    var data = action.getChooserData();
					    if(data == null) {
					       var d = this.getRegulerData();
					       data = this.addChooserData(d);
					    }
					    var self = this;
						data['super_command'] = param;
						$.post(
							"",
							data,
							function(response) {
								show_chooser(self, param, response, action.getShowParentModalInChooser(), modal_title);
								action.view();
								action.focusSearch();
								this.current_chooser = action;
								CURRENT_SMIS_CHOOSER = action;
							}
						);
						return this;
					};
					menu_pagi.addViewData = function(data) {
						data['kelas'] = $("#pesanan_pasien_kelas").val();
						data['waktu'] = "pagi";
						data['tanggal'] = $("#pesanan_pasien_tanggal").val();
						return data;
					};
					menu_pagi.selected = function(json) {
						$("#pesanan_pasien_id_menu_pagi").val(json.id);
						$("#pesanan_pasien_menu_pagi_tarif").val(json.tarif_sekarang);
						$("#pesanan_pasien_menu_pagi").val(json.nama);
						var option_html = "";
						var option = json.kategori.split(";");
						if (option.length > 0) {
							var counter = 1;
							option.forEach(function(item, index) {
								if (counter == 1)
									option_html += "<option selected>" + item + "</option>";
								else
									option_html += "<option>" + item + "</option>";
								counter++;
							});
						}
						$("#pesanan_pasien_pk_pagi").html(option_html);
					};
					menu_pagi.selectMenu = function(id_menu, jenis_karbohidrat) {
						hide_chooser();
						showLoading();
						var self = this;
						var edit_data = this.getEditData(id_menu);
						$.post(
							"",
							edit_data,
							function(response) {
								var json = getContent(response);
								if (json == null) {
									return;
								}
								self.selectedMenu(json, jenis_karbohidrat);
						        dismissLoading();
							}
						);
					};
					menu_pagi.selectedMenu = function(json, jenis_karbohidrat, tarif) {
						$("#pesanan_pasien_id_menu_pagi").val(json.id);
						$("#pesanan_pasien_menu_pagi_tarif").val(tarif);
						$("#pesanan_pasien_menu_pagi").val(json.nama);
						var option_html = "";
						var option = json.kategori.split(";");
						if (option.length > 0) {
							option.forEach(function(item, index) {
								if (item == jenis_karbohidrat)
									option_html += "<option selected>" + item + "</option>";
								else
									option_html += "<option>" + item + "</option>";
							});
						}
						$("#pesanan_pasien_pk_pagi").html(option_html);
					};
					menu_pagi.clear = function() {
						$("#pesanan_pasien_id_menu_pagi").val("");
						$("#pesanan_pasien_menu_pagi").val("");
						$("#pesanan_pasien_menu_pagi_tarif").val(0);
						$("#pesanan_pasien_pk_pagi").html("");
					};
					menu_pagi.setPrototipe(PROTO_NAME, PROTO_SLUG, PROTO_IMPLEMENT);
					menu_pagi.setSuperCommand("menu_pagi");

					menu_siang = new TableAction(
						"menu_siang",
						PAGE,
						ACTION,
						new Array()
					);
					menu_siang.chooser = function(modal, elm_id, param, action, modal_title) {
						if ($("#pesanan_pasien_kelas").val() == "") {
							$("#modal_alert_pesanan_pasien_add_form").html(
								"<div class='alert alert-block alert-danger'>" +
									"<h4>Peringatan</h4>" +
									"<b>Kelas</b> tidak boleh kosong." +
								"</div>"
							);
							return;
						}
					    var data = action.getChooserData();
					    if(data == null) {
					       var d = this.getRegulerData();
					       data = this.addChooserData(d);
					    }
					    var self = this;
						data['super_command'] = param;
						$.post(
							"",
							data,
							function(response) {
								show_chooser(self, param, response, action.getShowParentModalInChooser(), modal_title);
								action.view();
								action.focusSearch();
								this.current_chooser = action;
								CURRENT_SMIS_CHOOSER = action;
							}
						);
						return this;
					};
					menu_siang.addViewData = function(data) {
						data['kelas'] = $("#pesanan_pasien_kelas").val();
						data['waktu'] = "siang";
						data['tanggal'] = $("#pesanan_pasien_tanggal").val();
						return data;
					};
					menu_siang.selected = function(json) {
						$("#pesanan_pasien_id_menu_siang").val(json.id);
						$("#pesanan_pasien_menu_siang_tarif").val(json.tarif_sekarang);
						$("#pesanan_pasien_menu_siang").val(json.nama);
						var option_html = "";
						var option = json.kategori.split(";");
						if (option.length > 0) {
							var counter = 1;
							option.forEach(function(item, index) {
								if (counter == 1)
									option_html += "<option selected>" + item + "</option>";
								else
									option_html += "<option>" + item + "</option>";
								counter++;
							});
						}
						$("#pesanan_pasien_pk_siang").html(option_html);
					};
					menu_siang.selectMenu = function(id_menu, jenis_karbohidrat, tarif) {
						hide_chooser();
						showLoading();
						var self = this;
						var edit_data = this.getEditData(id_menu);
						$.post(
							"",
							edit_data,
							function(response) {
								var json = getContent(response);
								if (json == null) {
									return;
								}
								self.selectedMenu(json, jenis_karbohidrat, tarif);
						        dismissLoading();
							}
						);
					};
					menu_siang.selectedMenu = function(json, jenis_karbohidrat, tarif) {
						$("#pesanan_pasien_id_menu_siang").val(json.id);
						$("#pesanan_pasien_menu_siang_tarif").val(tarif);
						$("#pesanan_pasien_menu_siang").val(json.nama);
						var option_html = "";
						var option = json.kategori.split(";");
						if (option.length > 0) {
							option.forEach(function(item, index) {
								if (item == jenis_karbohidrat)
									option_html += "<option selected>" + item + "</option>";
								else
									option_html += "<option>" + item + "</option>";
							});
						}
						$("#pesanan_pasien_pk_siang").html(option_html);
					};
					menu_siang.clear = function() {
						$("#pesanan_pasien_id_menu_siang").val("");
						$("#pesanan_pasien_menu_siang").val("");
						$("#pesanan_pasien_menu_siang_tarif").val(0);
						$("#pesanan_pasien_pk_siang").html("");
					};
					menu_siang.setPrototipe(PROTO_NAME, PROTO_SLUG, PROTO_IMPLEMENT);
					menu_siang.setSuperCommand("menu_siang");

					menu_malam = new TableAction(
						"menu_malam",
						PAGE,
						ACTION,
						new Array()
					);
					menu_malam.chooser = function(modal, elm_id, param, action, modal_title) {
						if ($("#pesanan_pasien_kelas").val() == "") {
							$("#modal_alert_pesanan_pasien_add_form").html(
								"<div class='alert alert-block alert-danger'>" +
									"<h4>Peringatan</h4>" +
									"<b>Kelas</b> tidak boleh kosong." +
								"</div>"
							);
							return;
						}
					    var data = action.getChooserData();
					    if(data == null) {
					       var d = this.getRegulerData();
					       data = this.addChooserData(d);
					    }
					    var self = this;
						data['super_command'] = param;
						$.post(
							"",
							data,
							function(response) {
								show_chooser(self, param, response, action.getShowParentModalInChooser(), modal_title);
								action.view();
								action.focusSearch();
								this.current_chooser = action;
								CURRENT_SMIS_CHOOSER = action;
							}
						);
						return this;
					};
					menu_malam.addViewData = function(data) {
						data['kelas'] = $("#pesanan_pasien_kelas").val();
						data['waktu'] = "malam";
						data['tanggal'] = $("#pesanan_pasien_tanggal").val();
						return data;
					};
					menu_malam.selected = function(json) {
						$("#pesanan_pasien_id_menu_malam").val(json.id);
						$("#pesanan_pasien_menu_malam_tarif").val(json.tarif_sekarang);
						$("#pesanan_pasien_menu_malam").val(json.nama);
						var option_html = "";
						var option = json.kategori.split(";");
						if (option.length > 0) {
							var counter = 1;
							option.forEach(function(item, index) {
								if (counter == 1)
									option_html += "<option selected>" + item + "</option>";
								else
									option_html += "<option>" + item + "</option>";
								counter++;
							});
						}
						$("#pesanan_pasien_pk_malam").html(option_html);
					};
					menu_malam.selectMenu = function(id_menu, jenis_karbohidrat, tarif) {
						hide_chooser();
						showLoading();
						var self = this;
						var edit_data = this.getEditData(id_menu);
						$.post(
							"",
							edit_data,
							function(response) {
								var json = getContent(response);
								if (json == null) {
									return;
								}
								self.selectedMenu(json, jenis_karbohidrat, tarif);
						        dismissLoading();
							}
						);
					};
					menu_malam.selectedMenu = function(json, jenis_karbohidrat, tarif) {
						$("#pesanan_pasien_id_menu_malam").val(json.id);
						$("#pesanan_pasien_menu_malam_tarif").val(tarif);
						$("#pesanan_pasien_menu_malam").val(json.nama);
						var option_html = "";
						var option = json.kategori.split(";");
						if (option.length > 0) {
							option.forEach(function(item, index) {
								if (item == jenis_karbohidrat)
									option_html += "<option selected>" + item + "</option>";
								else
									option_html += "<option>" + item + "</option>";
							});
						}
						$("#pesanan_pasien_pk_malam").html(option_html);
					};
					menu_malam.clear = function() {
						$("#pesanan_pasien_id_menu_malam").val("");
						$("#pesanan_pasien_menu_malam").val("");
						$("#pesanan_pasien_menu_malam_tarif").val(0);
						$("#pesanan_pasien_pk_malam").html("");
					};
					menu_malam.setPrototipe(PROTO_NAME, PROTO_SLUG, PROTO_IMPLEMENT);
					menu_malam.setSuperCommand("menu_malam");

					$("#smis-chooser-modal").on("show", function() {
						$("#smis-chooser-modal").removeClass("half_model");
						$("#smis-chooser-modal").removeClass("full_model");
						$("#smis-chooser-modal").addClass("half_model");
						$("table tfoot tr").eq(0).show();
					});
				});
			</script>
			<?php
		}

		public function cssPreLoad() {
			?>
			<style type="text/css">
				#pesanan_pasien_1 label,
				#pesanan_pasien_2 label,
				#pesanan_pasien_3 label {
				    float: left;
				    width: 100px;
				    height: 20px;
				}
				form#pesanan_pasien_2 > div, 
				form#pesanan_pasien_3 > div {
					width: 47%;
				}
			</style>
			<?php
		}
	}
?>