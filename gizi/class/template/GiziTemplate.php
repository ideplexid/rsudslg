<?php

require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'gizi/service/RuanganService.php';
require_once 'gizi/class/table/GiziTable.php';

class GiziTemplate extends ModulTemplate {
    protected $db;
	protected $mode;
	protected $polislug;
	protected $page;
    protected $action;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    protected $bed;
    protected $umur;
	protected $ibu_kandung;
	protected $setting_pesan_penunggu;
	protected $setting_auto_pesan_penunggu;
    public static $MODE_DAFTAR = "pendaftaran";
	public static $MODE_PERIKSA = "pemeriksaan";
    
    public function __construct ($db, $mode, $polislug = "all", $page = "gizi", $action = "pesanan_pasien", $protoslug = "", $protoname = "", $protoimplement = "", $noreg = "", $nama = "", $nrm = "", $bed = "", $umur = "", $ibu_kandung = "", $setting_pesan_penunggu = 0, $setting_auto_pesan_penunggu = 0) {
        $this->db = $db;
        $this->mode = $mode;
        $this->polislug = $polislug;
        $this->page = $page;
        $this->action = $action;
        $this->protoslug = $protoslug;
        $this->protoname = $protoname;
        $this->protoimplement = $protoimplement;
        $this->dbtable = new DBTable ( $this->db, "smis_gz_pesanan" );
        $pagi_saja=getSettings($db, "smis_gz_pagi_saja", "0")==1;
        if($pagi_saja) {
            if($this->mode == self::$MODE_DAFTAR) {
                if($this->setting_pesan_penunggu == 1 || $this->setting_pesan_penunggu == '1') {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Menu Penunggu","Jumlah Menu Penunggu","Status");
                } else {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Status");
                }
            } else {
                if(getSettings($this->db, "smis_gz_pesanan_penunggu_pasien", "0") == 1 || getSettings($this->db, "smis_gz_pesanan_penunggu_pasien", "0") == '1') {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Menu Penunggu","Jumlah Menu Penunggu","Status");
                } else {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Status");
                }
            }
        } else {
            if($this->mode == self::$MODE_DAFTAR) {
                if($this->setting_pesan_penunggu == 1 || $this->setting_pesan_penunggu == '1') {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Menu Siang","Diet Siang","PK Siang","Menu Malam","Diet Malam","PK Malam","Menu Penunggu","Jumlah Menu Penunggu","Status");
                } else {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Menu Siang","Diet Siang","PK Siang","Menu Malam","Diet Malam","PK Malam","Status");
                }
            } else {
                if(getSettings($this->db, "smis_gz_pesanan_penunggu_pasien", "0") == 1 || getSettings($this->db, "smis_gz_pesanan_penunggu_pasien", "0") == '1') {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Menu Siang","Diet Siang","PK Siang","Menu Malam","Diet Malam","PK Malam","Menu Penunggu","Jumlah Menu Penunggu","Status");
                } else {
                    $thead=array ("No.","Tanggal","Ruangan","Nama","NRM","No Reg","Ibu Kandung","Keluar","Umur","Menu Pagi","Diet Pagi","PK Pagi","Menu Siang","Diet Siang","PK Siang","Menu Malam","Diet Malam","PK Malam","Status");
                }
            }
        }
        
        if($this->polislug != 'all') {
            $this->uitable = new GiziTable ( $this->db, $thead, "Pesanan Makan Pasien - ".ucfirst($this->protoname), NULL, true );
        } else {
            $this->uitable = new GiziTable ( $this->db, $thead, "Pesanan Makan Pasien", NULL, true );
        }
        $this->uitable->setName ( $action );
        $this->noreg_pasien = $noreg;
        if ($this->noreg_pasien != ""){
            $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $this->noreg_pasien. "'" );
        }
        $this->nama_pasien = $nama;
        $this->nrm_pasien = $nrm;
        $this->bed = $bed;
        $this->umur = $umur;
        $this->ibu_kandung = $ibu_kandung;
        $this->setting_pesan_penunggu = $setting_pesan_penunggu;
        $this->setting_auto_pesan_penunggu = $setting_auto_pesan_penunggu;
        
        if ($this->mode == self::$MODE_DAFTAR) {
            $this->uitable->setAddButtonEnable ( true );
            $this->uitable->setDelButtonEnable ( true );
            $this->uitable->setDetailButtonEnable ( true );
            $this->uitable->setEditButtonEnable ( true );
        } else {
            $this->uitable->setAddButtonEnable ( true );
            $this->uitable->setDelButtonEnable ( true );
            $this->uitable->setDetailButtonEnable ( true );
            $this->uitable->setEditButtonEnable ( true );
            $btn     = new Button("", "", "Selesai");
            $btn     ->setIsButton(Button::$ICONIC)
                     ->setIcon("fa fa-check")
                     ->setAtribute("data-content='Add_Bahan' data-toggle='popover'")
                     ->setClass("btn-success");
            $this->uitable ->addContentButton("selesai", $btn);
        }
    }
    
    public function command($command) {
        if($command == 'get_jadwal_menu') {
            $query = "SELECT * FROM smis_gz_jadwal WHERE tanggal = '".$_POST['tanggal_jadwal']."'";
            $res = $this->db->get_result($query);
            echo json_encode($res);
            return;
        } else {
            $adapter = new SimpleAdapter();
            $adapter->setUseNumber(true, "No.","back.");
            $adapter->add ( "Tanggal", "tanggal", "date d M Y" );
            $adapter->add ( "Ruangan", "ruang", "unslug" );
            $adapter->add ( "Bed", "bed" );
            $adapter->add ( "Nama", "nama_pasien" );
            $adapter->add ( "NRM", "nrm_pasien", "digit8" );
            $adapter->add ( "No Reg", "noreg_pasien" );
            $adapter->add ( "Ibu Kandung", "ibu_kandung" );
            $adapter->add ( "Keluar", "cara_keluar" );
            $adapter->add ( "Umur", "umur" );
            $adapter->add ( "Kategori Menu", "kategori_menu" );
            $adapter->add ( "Menu Pagi", "menu_pagi" );
            $adapter->add ( "Diet pagi", "diet_pagi" );
            $adapter->add ( "PK pagi", "pk_pagi" );
            $adapter->add ( "Menu Siang", "menu_siang" );
            $adapter->add ( "Diet Siang", "diet_siang" );
            $adapter->add ( "PK Siang", "pk_siang" );
            $adapter->add ( "Menu Malam", "menu_malam" );
            $adapter->add ( "Diet Malam", "diet_malam" );
            $adapter->add ( "PK Malam", "pk_malam" );
            $adapter->add ( "Menu Penunggu", "menu_penunggu" );
            $adapter->add ( "Jumlah Menu Penunggu", "jumlah_menu_penunggu" );
            $adapter->add ( "Status", "selesai","trivial_1_<i class='fa fa-check'></i>_" );
            $dbres = new DBResponder( $this->dbtable, $this->uitable, $adapter );
            if($dbres->isSave()) {
                $code = md5($_POST["noreg_pasien"].$_POST["tanggal"]);
                $dbres->addColumnFixValue("code",$code);
            }
            if($dbres->isView()){
                if(isset($_POST['dari']) &&  $_POST['dari']!="" ){
                    $this->dbtable->addCustomKriteria(" tanggal>= ", "'".$_POST['dari']."' ");
                }
                if(isset($_POST['sampai']) &&  $_POST['sampai']!="" ){
                    $this->dbtable->addCustomKriteria(" tanggal<= ", "'".$_POST['sampai']."' ");
                }
                if(isset($_POST['ruangan']) && $_POST['ruangan'] != "") {
                    $this->dbtable->addCustomKriteria(" ruang = ", "'".$_POST['ruangan']."' ");
                }
                if ($this->noreg_pasien != ""){
                    $this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $this->noreg_pasien. "'" );
                }
            }
            $data = $dbres->command ( $_POST ['command'] );
            echo json_encode ( $data );
            return;
        }
    }
    
    public function phpPreLoad() {
        loadClass ( "ServiceProviderList" );
		$service = new ServiceProviderList ( $this->db, "push_antrian" );
		$service->execute ();
		$ruangan = $service->getContent ();
        if($this->polislug != 'all') {
            for($i = 0; $i <= sizeof($ruangan); $i++) {
                 if($ruangan[$i]['value'] == $this->polislug) {
                      $ruangan[$i]['default'] = '1';
                 }
            }
        }
        
        $current_date = date("Y-m-d");
        $query = "SELECT * FROM smis_gz_jadwal WHERE tanggal = '".$current_date."'";
        $res = $this->db->get_result($query);
        $menu['pagi_nasi'] = '';
        $menu['siang_nasi'] = '';
        $menu['malam_nasi'] = '';
        $menu['penunggu'] = '';
        if($res[0] != NULL) {
            $menu['pagi_nasi'] = $res[0]->menu_pagi_pasien_nasi;
            $menu['siang_nasi'] = $res[0]->menu_siang_pasien_nasi;
            $menu['malam_nasi'] = $res[0]->menu_malam_pasien_nasi;
            $menu['penunggu'] = $res[0]->menu_penunggu;
        }
        
        $kategori_menu = new OptionBuilder();
		$kategori_menu->addSingle("Nasi","1");
		$kategori_menu->addSingle("Bubur","0");
		$kategori_menu->addSingle("Tim","0");
        
        $this->uitable->addModal ( "id", "hidden", "", "" );
        $this->uitable->addModal ( "tanggal", "date", "Tanggal", date ( "Y-m-d" ) );
        
        if($this->mode == self::$MODE_DAFTAR) {
            $this->uitable->addModal ( "nama_pasien", "hidden", "", $this->nama_pasien, "n", null, true );
            $this->uitable->addModal ( "nrm_pasien", "hidden", "", $this->nrm_pasien, "n", null, true );
            $this->uitable->addModal ( "noreg_pasien", "hidden", "", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "ibu_kandung", "hidden", "", $this->ibu_kandung, "y", null, false );
            $this->uitable->addModal ( "umur", "hidden", "", $this->umur, "y", null, true );
            $this->uitable->addModal ( "golongan_umur", "hidden", "", $this->golongan_umur, "y", null, true );
            $this->uitable->addModal ( "ruang", "hidden", "", $this->polislug, "n", null, true );
            $this->uitable->addModal ( "bed", "hidden", "", $this->golongan_umur, "y", null, false );
            $this->uitable->addModal ( "kategori_menu_pagi", "select", "Kategori Menu Pagi", $kategori_menu->getContent(), "y", null, false );
            $this->uitable->addModal ( "kategori_menu_siang", "select", "Kategori Menu Siang", $kategori_menu->getContent(), "y", null, false );
            $this->uitable->addModal ( "kategori_menu_malam", "select", "Kategori Menu Malam", $kategori_menu->getContent(), "y", null, false );
            $this->uitable->addModal ( "menu_pagi", "chooser-" . $this->action . "-menu_pagi", "Menu Pagi", $menu['pagi_nasi'], "y", null, true );
            $this->uitable->addModal ( "menu_pagi_tarif", "hidden", "", "" );
            $this->uitable->addModal ( "diet_pagi", "chooser-" . $this->action . "-diet_pagi", "Diet Pagi", "", "y", null, true );
            $this->uitable->addModal ( "pk_pagi", "textarea", "Pesanan Khusus Pagi", "", "y", null, false );
            $this->uitable->addModal ( "menu_siang", "chooser-" . $this->action . "-menu_siang", "Menu Siang", $menu['siang_nasi'], "y", null, true );
            $this->uitable->addModal ( "menu_siang_tarif", "hidden", "", "" );
            $this->uitable->addModal ( "diet_siang", "chooser-" . $this->action . "-diet_siang", "Diet Siang", "", "y", null, true );
            $this->uitable->addModal ( "pk_siang", "textarea", "Pesanan Khusus Siang", "", "y", null, false );
            $this->uitable->addModal ( "menu_malam", "chooser-" . $this->action . "-menu_malam", "Menu Malam", $menu['malam_nasi'], "y", null, true );
            $this->uitable->addModal ( "menu_malam_tarif", "hidden", "", "" );
            $this->uitable->addModal ( "diet_malam", "chooser-" . $this->action . "-diet_malam", "Diet Malam", "", "y", null, true );
            $this->uitable->addModal ( "pk_malam", "textarea", "Pesanan Khusus Malam", "", "y", null, false );
            if($this->setting_pesan_penunggu == 1 || $this->setting_pesan_penunggu == '1') {
                if($this->setting_auto_pesan_penunggu == 1 || $this->setting_auto_pesan_penunggu == '1') {
                    $this->uitable->addModal ( "menu_penunggu", "text", "Menu Penunggu", $menu['penunggu'], "y", null, true );
                    $this->uitable->addModal ( "menu_penunggu_tarif", "hidden", "", "" );
                } else {
                    $this->uitable->addModal ( "menu_penunggu", "chooser-" . $this->action . "-menu_penunggu", "Menu Penunggu", $menu['penunggu'], "y", null, true );
                    $this->uitable->addModal ( "menu_penunggu_tarif", "hidden", "", "" );
                }
                $this->uitable->addModal ( "jumlah_menu_penunggu", "text", "Jumlah Pesanan Penunggu", "", "y", null, false );
            }
        } else {
            $this->uitable->addModal ( "nama_pasien", "chooser-" . $this->action . "-pasien", "Pasien", $this->nama_pasien, "n", null, true );
            $this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
            $this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
            $this->uitable->addModal ( "ibu_kandung", "text", "Ibu Kandung", $this->ibu_kandung, "y", null, false );
            $this->uitable->addModal ( "umur", "text", "Umur", $this->umur, "y", null, true );
            $this->uitable->addModal ( "golongan_umur", "text", "Gol Umur", $this->golongan_umur, "y", null, true );
            $this->uitable->addModal ( "ruang", "select", "Ruangan", $ruangan, "n", null, true );
            $this->uitable->addModal ( "bed", "text", "Bed", $this->golongan_umur, "y", null, false );
            $this->uitable->addModal ( "kategori_menu_pagi", "select", "Kategori Menu Pagi", $kategori_menu->getContent(), "y", null, false );
            $this->uitable->addModal ( "kategori_menu_siang", "select", "Kategori Menu Siang", $kategori_menu->getContent(), "y", null, false );
            $this->uitable->addModal ( "kategori_menu_malam", "select", "Kategori Menu Malam", $kategori_menu->getContent(), "y", null, false );
            $this->uitable->addModal ( "menu_pagi", "chooser-" . $this->action . "-menu_pagi", "Menu Pagi", $menu['pagi_nasi'], "y", null, true );
            $this->uitable->addModal ( "menu_pagi_tarif", "hidden", "", "" );
            $this->uitable->addModal ( "diet_pagi", "chooser-" . $this->action . "-diet_pagi", "Diet Pagi", "", "y", null, true );
            $this->uitable->addModal ( "pk_pagi", "textarea", "Pesanan Khusus Pagi", "", "y", null, false );
            $this->uitable->addModal ( "menu_siang", "chooser-" . $this->action . "-menu_siang", "Menu Siang", $menu['siang_nasi'], "y", null, true );
            $this->uitable->addModal ( "menu_siang_tarif", "hidden", "", "" );
            $this->uitable->addModal ( "diet_siang", "chooser-" . $this->action . "-diet_siang", "Diet Siang", "", "y", null, true );
            $this->uitable->addModal ( "pk_siang", "textarea", "Pesanan Khusus Siang", "", "y", null, false );
            $this->uitable->addModal ( "menu_malam", "chooser-" . $this->action . "-menu_malam", "Menu Malam", $menu['malam_nasi'], "y", null, true );
            $this->uitable->addModal ( "menu_malam_tarif", "hidden", "", "" );
            $this->uitable->addModal ( "diet_malam", "chooser-" . $this->action . "-diet_malam", "Diet Malam", "", "y", null, true );
            $this->uitable->addModal ( "pk_malam", "textarea", "Pesanan Khusus Malam", "", "y", null, false );
            if(getSettings($this->db, "smis_gz_pesanan_penunggu_pasien", "0") == 1 || getSettings($this->db, "smis_gz_pesanan_penunggu_pasien", "0") == '1') {
                if(getSettings($this->db, "smis_gz_auto_pesanan_penunggu_pasien", "0") == 1 || getSettings($this->db, "smis_gz_auto_pesanan_penunggu_pasien", "0") == '1') {
                    $this->uitable->addModal ( "menu_penunggu", "text", "Menu Penunggu", $menu['penunggu'], "y", null, true );
                    $this->uitable->addModal ( "menu_penunggu_tarif", "hidden", "", "" );
                } else {
                    $this->uitable->addModal ( "menu_penunggu", "chooser-" . $this->action . "-menu_penunggu", "Menu Penunggu", $menu['penunggu'], "y", null, true );
                    $this->uitable->addModal ( "menu_penunggu_tarif", "hidden", "", "" );
                }
                $this->uitable->addModal ( "jumlah_menu_penunggu", "text", "Jumlah Pesanan Penunggu", "", "y", null, false );
            }
        }
        
        $modal = $this->uitable->getModal ();
        $modal->setTitle ( "Pesanan Makan Pasien" );
        $modal->setClass ( Modal::$FULL_MODEL );
        
        if($this->mode == self::$MODE_PERIKSA) {
            $urjip=new ServiceConsumer($db, "get_urjip",array());
            $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
            $urjip->setCached(true,"get_urjip");
            $urjip->execute();
            $content=$urjip->getContent();
            $r=array();
            foreach ($content as $autonomous=>$ruang){
                foreach($ruang as $nama_ruang=>$jip){
                    if($jip[$nama_ruang] == "URI"){
                        $option=array();
                        $option['value']=$nama_ruang;
                        $option['name']=ArrayAdapter::format("unslug", $nama_ruang);
                        $r[]=$option;
                    }
                }
            }
            $option['value'] = '';
            $option['name'] = '- SEMUA -';
            $r[]=$option;
            
            $this->uitable->clearContent();
            $this->uitable->addModal("dari", "date", "Dari", "");
            $this->uitable->addModal("sampai", "date", "Sampai", "");
            $this->uitable->addModal("ruangan", "select", "Ruangan", $r);
            
            $form = $this->uitable->getModal()->getForm();
        
        $btn=new Button("", "", "");
		$btn->setClass("btn-primary");
		$btn->setIcon("fa fa-refresh");
		$btn->setIsButton(Button::$ICONIC);
		$btn->setAction($this->action.".view()");
		$form->addElement("", $btn);
		
        echo $form->getHtml();
        }
        
        echo $this->uitable->getHtml ();
		echo $modal->getHtml ();
    }
    
    public function superCommand($super_command) {
        $super = new SuperCommand ();
        if($super_command == 'pasien') {   // PASIEN
            $aname=array ('Nama','NRM',"No Reg" );
            $ptable = new Table ( $aname);
            $ptable->setName ( "pasien" );
            $ptable->setModel ( Table::$SELECT );
            $padapter = new SimpleAdapter ();
            $padapter->add ( "Nama", "nama_pasien" );
            $padapter->add ( "NRM", "nrm", "digit8" );
            $padapter->add ( "No Reg", "id" );
            $presponder = new ServiceResponder ( $this->db, $ptable, $padapter, "get_registered" );
            $super->addResponder ( "pasien", $presponder );         
        } if($super_command == 'menu_pagi') {
            $mp_dbtable = new DBTable ( $this->db, "smis_gz_menu" );
            $mp_dbtable->addCustomKriteria("waktu", "='Pagi'");
            $mp_dbtable->addCustomKriteria("jenis", "='Pasien'");
            $mp_dbtable->addCustomKriteria("kategori", "= '".$_POST['kategori_menu_pagi']."'");
            $mp_uitable = new Table(array('Nama Menu','Jenis','Kategori', 'Waktu', 'Keterangan'),"");
            $mp_uitable->setName("menu_pagi");
            $mp_uitable->setModel (Table::$SELECT);
            $mp_adapter = new SimpleAdapter();
            $mp_adapter->add("Nama Menu", "nama");
            $mp_adapter->add("Jenis", "jenis");
            $mp_adapter->add("Kategori", "kategori");
            $mp_adapter->add("Waktu", "waktu");
            $mp_adapter->add("Keterangan", "keterangan");
            $mp_responder = new DBResponder($mp_dbtable, $mp_uitable, $mp_adapter);
            $super->addResponder ( "menu_pagi", $mp_responder );
        } if($super_command == 'menu_siang') {
            $mp_dbtable = new DBTable ( $this->db, "smis_gz_menu" );
            $mp_dbtable->addCustomKriteria("waktu", "='Siang'");
            $mp_dbtable->addCustomKriteria("jenis", "='Pasien'");
            $mp_dbtable->addCustomKriteria("kategori", "='".$_POST['kategori_menu_siang']."'");
            $mp_uitable = new Table(array('Nama Menu','Jenis','Kategori', 'Waktu', 'Keterangan'),"");
            $mp_uitable->setName("menu_siang");
            $mp_uitable->setModel (Table::$SELECT);
            $mp_adapter = new SimpleAdapter();
            $mp_adapter->add("Nama Menu", "nama");
            $mp_adapter->add("Jenis", "jenis");
            $mp_adapter->add("Kategori", "kategori");
            $mp_adapter->add("Waktu", "waktu");
            $mp_adapter->add("Keterangan", "keterangan");
            $mp_responder = new DBResponder($mp_dbtable, $mp_uitable, $mp_adapter);
            $super->addResponder ( "menu_siang", $mp_responder );
        } if($super_command == 'menu_malam') {
            $mp_dbtable = new DBTable ( $this->db, "smis_gz_menu" );
            $mp_dbtable->addCustomKriteria("waktu", "='Malam'");
            $mp_dbtable->addCustomKriteria("jenis", "='Pasien'");
            $mp_dbtable->addCustomKriteria("kategori", "='".$_POST['kategori_menu_malam']."'");
            $mp_uitable = new Table(array('Nama Menu','Jenis','Kategori', 'Waktu', 'Keterangan'),"");
            $mp_uitable->setName("menu_malam");
            $mp_uitable->setModel (Table::$SELECT);
            $mp_adapter = new SimpleAdapter();
            $mp_adapter->add("Nama Menu", "nama");
            $mp_adapter->add("Jenis", "jenis");
            $mp_adapter->add("Kategori", "kategori");
            $mp_adapter->add("Waktu", "waktu");
            $mp_adapter->add("Keterangan", "keterangan");
            $mp_responder = new DBResponder($mp_dbtable, $mp_uitable, $mp_adapter);
            $super->addResponder ( "menu_malam", $mp_responder );
        } if(startsWith($super_command,"diet")) {
            $mp_dbtable = new DBTable ( $this->db, "smis_gz_diet" );
            $mp_uitable = new Table(array('Nama','Penyakit','Keterangan'),"");
            $mp_uitable->setName($super_command);
            $mp_uitable->setModel (Table::$SELECT);
            $mp_adapter = new SimpleAdapter();
            $mp_adapter ->add("Nama", "nama")
                        ->add("Penyakit", "penyakit")
                        ->add("Keterangan", "keterangan");
            $mp_responder = new DBResponder($mp_dbtable, $mp_uitable, $mp_adapter);
            $super->addResponder ( $super_command, $mp_responder );
        } if($super_command == 'menu_penunggu') {
            $c_dbtable = new DBTable ( $this->db, "smis_gz_menu" );
            $c_dbtable->addCustomKriteria("jenis", "='Penunggu'");
            $c_uitable = new Table(array('Nama Menu','Jenis','Keterangan'),"");
            $c_uitable->setName("menu_penunggu");
            $c_uitable->setModel (Table::$SELECT);
            $c_adapter = new SimpleAdapter();
            $c_adapter->add("Nama Menu", "nama");
            $c_adapter->add("Jenis", "jenis");
            $c_adapter->add("Keterangan", "keterangan");
            $jadwal_menu_penunggu_responder = new DBResponder($c_dbtable, $c_uitable, $c_adapter);
            $super->addResponder ( "menu_penunggu", $jadwal_menu_penunggu_responder );
        }
		$init = $super->initialize ();
		if ($init != null) {
			echo $init;
			return;
		}
    }
    
    public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		loadLibrary ( "smis-libs-function-javascript" );
	}
    
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
    
    /* when it's star build */
    public function jsPreLoad() {
?>
    <script type="text/javascript">
        var <?php echo $this->action; ?>;
        var mode = "<?php echo $this->mode; ?>";
        var noreg_pasien = "<?php echo $this->noreg_pasien; ?>";
		var nama_pasien="<?php echo $this->nama_pasien; ?>";
		var nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var polislug="<?php echo $this->polislug; ?>";
		var page="<?php echo $this->page; ?>";
		var protoslug="<?php echo $this->protoslug; ?>";
		var protoname="<?php echo $this->protoname; ?>";
		var protoimplement="<?php echo $this->protoimplement; ?>";
		var setting_menu_penunggu_rawat="<?php echo $this->setting_pesan_penunggu; ?>";
        var setting_menu_penunggu_gizi = "<?php echo getSettings($this->db, "smis_gz_pesanan_penunggu_pasien", "0"); ?>";
        
        var pasien;
        var menu_pagi;
        var menu_siang;
        var menu_malam;
        var diet_pagi;
        var diet_siang;
        var diet_malam;
        var menu_penunggu;
        
        $(document).ready(function() {
            $(".mydate").datepicker();
            $(".mydatetime").datetimepicker({ minuteStep: 1});
            console.log(mode);
            var column=new Array(
                                "id",
                                "tanggal",
                                "nama_pasien",
                                "noreg_pasien",
                                "nrm_pasien",
                                "ibu_kandung",
                                "cara_keluar",
                                "umur",
                                "golongan_umur",
                                "ruang",
                                "menu_pagi",
                                "menu_pagi_tarif",
                                "diet_pagi",
                                "pk_pagi",
                                "menu_siang",
                                "menu_siang_tarif",
                                "diet_siang",
                                "pk_siang",
                                "menu_malam",
                                "menu_malam_tarif",
                                "diet_malam",
                                "pk_malam",
                                "menu_penunggu",
                                "menu_penunggu_tarif",
                                "jumlah_menu_penunggu",
                                "selesai",
                                "bed",
                                "cetak"
                                );
            <?php echo $this->action; ?> = new TableAction("<?php echo $this->action; ?>", page, "<?php echo $this->action; ?>", column);
            <?php echo $this->action; ?>.setPrototipe(protoname, protoslug, protoimplement);
            <?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:polislug,
						noreg_pasien:noreg_pasien,
						nama_pasien:nama_pasien,
						nrm_pasien:nrm_pasien,
						dari:$("#<?php echo $this->action; ?>_dari").val(),
						sampai:$("#<?php echo $this->action; ?>_sampai").val(),
						ruangan:$("#<?php echo $this->action; ?>_ruangan").val()
					};
				return reg_data;
			};
            
            <?php echo $this->action; ?>.selesai=function(id){
                var data=this.getRegulerData();
                data['command']="save";
                data['id']=id;
                data['selesai']=1;
                showLoading();
                $.post('',data,function(res){
                    var json=getContent(res);
                    <?php echo $this->action; ?>.view();
                    dismissLoading();
                });
            };
            if(mode == 'pendaftaran') {
                <?php echo $this->action; ?>.view();
            }
            
            $("#<?php echo $this->action; ?>_tanggal").on("changeDate", function(){
                var date = $("#<?php echo $this->action; ?>_tanggal").val();
                console.log(date);
                var data = <?php echo $this->action; ?>.getRegulerData();
                data['command'] = 'get_jadwal_menu';
                data['tanggal_jadwal'] = date;
                $.post(
                    "",
                    data,
                    function(response) {
                        var json = JSON.parse(response);
                        console.log(json[0]);
                        var kategori_menu_pagi = $("#<?php echo $this->action; ?>_kategori_menu_pagi").val();
                        var kategori_menu_siang = $("#<?php echo $this->action; ?>_kategori_menu_siang").val();
                        var kategori_menu_malam = $("#<?php echo $this->action; ?>_kategori_menu_malam").val();
                        if (mode == 'pendaftaran') {
                            if(setting_menu_penunggu_rawat == 0 || setting_menu_penunggu_rawat == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        } else {
                            if(setting_menu_penunggu_gizi == 0 || setting_menu_penunggu_gizi == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        }
                    }
                );
            });
            
            $("#<?php echo $this->action; ?>_kategori_menu_pagi").on("change", function() {
                console.log("kategori_menu_pagi");
                var data = <?php echo $this->action; ?>.getRegulerData();
                var date = $("#<?php echo $this->action; ?>_tanggal").val();
                data['command'] = 'get_jadwal_menu';
                data['tanggal_jadwal'] = date;
                $.post(
                    "",
                    data,
                    function(response) {
                        var json = JSON.parse(response);
                        console.log(json[0]);
                        var kategori_menu_pagi = $("#<?php echo $this->action; ?>_kategori_menu_pagi").val();
                        var kategori_menu_siang = $("#<?php echo $this->action; ?>_kategori_menu_siang").val();
                        var kategori_menu_malam = $("#<?php echo $this->action; ?>_kategori_menu_malam").val();
                        if (mode == 'pendaftaran') {
                            if(setting_menu_penunggu_rawat == 0 || setting_menu_penunggu_rawat == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        } else {
                            if(setting_menu_penunggu_gizi == 0 || setting_menu_penunggu_gizi == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        }
                    }
                );
            });
            
            $("#<?php echo $this->action; ?>_kategori_menu_siang").on("change", function() {
                console.log("kategori_menu_siang");
                var data = <?php echo $this->action; ?>.getRegulerData();
                var date = $("#<?php echo $this->action; ?>_tanggal").val();
                data['command'] = 'get_jadwal_menu';
                data['tanggal_jadwal'] = date;
                $.post(
                    "",
                    data,
                    function(response) {
                        var json = JSON.parse(response);
                        console.log(json[0]);
                        var kategori_menu_pagi = $("#<?php echo $this->action; ?>_kategori_menu_pagi").val();
                        var kategori_menu_siang = $("#<?php echo $this->action; ?>_kategori_menu_siang").val();
                        var kategori_menu_malam = $("#<?php echo $this->action; ?>_kategori_menu_malam").val();
                        if (mode == 'pendaftaran') {
                            if(setting_menu_penunggu_rawat == 0 || setting_menu_penunggu_rawat == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        } else {
                            if(setting_menu_penunggu_gizi == 0 || setting_menu_penunggu_gizi == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        }
                    }
                );
            });
            
            $("#<?php echo $this->action; ?>_kategori_menu_malam").on("change", function() {
                console.log("kategori_menu_malam");
                var data = <?php echo $this->action; ?>.getRegulerData();
                var date = $("#<?php echo $this->action; ?>_tanggal").val();
                data['command'] = 'get_jadwal_menu';
                data['tanggal_jadwal'] = date;
                $.post(
                    "",
                    data,
                    function(response) {
                        var json = JSON.parse(response);
                        console.log(json[0]);
                        var kategori_menu_pagi = $("#<?php echo $this->action; ?>_kategori_menu_pagi").val();
                        var kategori_menu_siang = $("#<?php echo $this->action; ?>_kategori_menu_siang").val();
                        var kategori_menu_malam = $("#<?php echo $this->action; ?>_kategori_menu_malam").val();
                        if (mode == 'pendaftaran') {
                            if(setting_menu_penunggu_rawat == 0 || setting_menu_penunggu_rawat == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        } else {
                            if(setting_menu_penunggu_gizi == 0 || setting_menu_penunggu_gizi == "0") {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                            } else {
                                if(kategori_menu_pagi == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_nasi_tarif);
                                } if(kategori_menu_pagi == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_bubur_tarif);
                                } if(kategori_menu_pagi == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_pagi").val(json[0].menu_pagi_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json[0].menu_pagi_pasien_tim_tarif);
                                } if(kategori_menu_siang == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_nasi_tarif);
                                } if(kategori_menu_siang == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_bubur_tarif);
                                } if(kategori_menu_siang == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_siang").val(json[0].menu_siang_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_siang_tarif").val(json[0].menu_siang_pasien_tim_tarif);
                                } if(kategori_menu_malam == 'Nasi') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_nasi);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_nasi_tarif);
                                } if(kategori_menu_malam == 'Bubur') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_bubur);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_bubur_tarif);
                                } if(kategori_menu_malam == 'Tim') {
                                    $("#<?php echo $this->action; ?>_menu_malam").val(json[0].menu_malam_pasien_tim);
                                    $("#<?php echo $this->action; ?>_menu_malam_tarif").val(json[0].menu_malam_pasien_tim_tarif);
                                }
                                $("#<?php echo $this->action; ?>_menu_penunggu").val(json[0].menu_penunggu);
                                $("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json[0].menu_penunggu_tarif);
                            }
                        }
                    }
                );
            });
            
            pasien=new TableAction("pasien", page,"<?php echo $this->action; ?>",new Array());
			pasien.setSuperCommand("pasien");
			pasien.setPrototipe(protoname, protoslug, protoimplement);
			pasien.selected=function(json){
                console.log(json);
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				var umur=json.umur;		
				var gol_umur=json.gol_umur;		
				var ibu=json.ibu;
                var polislug;
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
				$("#<?php echo $this->action; ?>_umur").val(umur);
				$("#<?php echo $this->action; ?>_golongan_umur").val(gol_umur);
				$("#<?php echo $this->action; ?>_ibu_kandung").val(ibu);
                if(json.kamar_inap == "" || json.kamar_inap == null) {
                    $("#<?php echo $this->action; ?>_ruang").val(json.jenislayanan);
                    polislug = json.jenislayanan;
                } else {
                    $("#<?php echo $this->action; ?>_ruang").val(json.kamar_inap);
                    polislug = json.kamar_inap;
                }
                
                var data = <?php echo $this->action; ?>.getRegulerData();
                data['action'] = 'get_bed_patient';
                data['nrm_pasien'] = $("#<?php echo $this->action; ?>_nrm_pasien").val();
                data['noreg_pasien'] = $("#<?php echo $this->action; ?>_noreg_pasien").val();
                data['polislug'] = polislug;
                $.post("",data,function(res){
                    var result = JSON.parse(res);
                    console.log(result);
                    $("#<?php echo $this->action; ?>_bed").val(result.nama);
                });
			};
            
            menu_pagi=new TableAction("menu_pagi", page, "<?php echo $this->action; ?>", new Array());
            menu_pagi.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:polislug,
						noreg_pasien:noreg_pasien,
						nama_pasien:nama_pasien,
						nrm_pasien:nrm_pasien,
						kategori_menu_pagi:$("#<?php echo $this->action; ?>_kategori_menu_pagi").val()
					};
				return reg_data;
			};
			menu_pagi.setSuperCommand("menu_pagi");
			menu_pagi.setPrototipe(protoname, protoslug, protoimplement);
			menu_pagi.selected=function(json){
				var nama=json.nama;	
				$("#<?php echo $this->action; ?>_menu_pagi").val(nama);
				$("#<?php echo $this->action; ?>_menu_pagi_tarif").val(json.tarif_sekarang);
			};
            
            menu_siang=new TableAction("menu_siang", page, "<?php echo $this->action; ?>", new Array());
            menu_siang.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:polislug,
						noreg_pasien:noreg_pasien,
						nama_pasien:nama_pasien,
						nrm_pasien:nrm_pasien,
						kategori_menu_siang:$("#<?php echo $this->action; ?>_kategori_menu_siang").val()
					};
				return reg_data;
			};
			menu_siang.setSuperCommand("menu_siang");
			menu_siang.setPrototipe(protoname, protoslug, protoimplement);
			menu_siang.selected=function(json){
				var nama=json.nama;	
				$("#<?php echo $this->action; ?>_menu_siang").val(nama);
				$("#<?php echo $this->action; ?>_menu_siang_tarif").val(json.tarif_sekarang);
			};
            
            menu_malam=new TableAction("menu_malam", page, "<?php echo $this->action; ?>", new Array());
            menu_malam.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:polislug,
						noreg_pasien:noreg_pasien,
						nama_pasien:nama_pasien,
						nrm_pasien:nrm_pasien,
						kategori_menu_malam:$("#<?php echo $this->action; ?>_kategori_menu_malam").val()
					};
				return reg_data;
			};
			menu_malam.setSuperCommand("menu_malam");
			menu_malam.setPrototipe(protoname, protoslug, protoimplement);
			menu_malam.selected=function(json){
				var nama=json.nama;	
				$("#<?php echo $this->action; ?>_menu_malam").val(nama);
				$("#<?php echo $this->action; ?>_menu_malam_tarif").val(json.tarif_sekarang);
			};
            
            menu_penunggu=new TableAction("menu_penunggu", page, "<?php echo $this->action; ?>", new Array());
			menu_penunggu.setSuperCommand("menu_penunggu");
			menu_penunggu.setPrototipe(protoname, protoslug, protoimplement);
			menu_penunggu.selected=function(json){
				var nama=json.nama;	
				$("#<?php echo $this->action; ?>_menu_penunggu").val(nama);
				$("#<?php echo $this->action; ?>_menu_penunggu_tarif").val(json.tarif_sekarang);
			};
            
            diet_pagi=new TableAction("diet_pagi", page, "<?php echo $this->action; ?>", new Array());
			diet_pagi.setSuperCommand("diet_pagi");
			diet_pagi.setPrototipe(protoname, protoslug, protoimplement);
			diet_pagi.selected=function(json){
				var nama=json.nama;	
				$("#<?php echo $this->action; ?>_diet_pagi").val(nama);
			};
            
            diet_siang=new TableAction("diet_siang", page, "<?php echo $this->action; ?>", new Array());
			diet_siang.setSuperCommand("diet_siang");
			diet_siang.setPrototipe(protoname, protoslug, protoimplement);
			diet_siang.selected=function(json){
				var nama=json.nama;	
				$("#<?php echo $this->action; ?>_diet_siang").val(nama);
			};
            
            diet_malam=new TableAction("diet_malam", page, "<?php echo $this->action; ?>", new Array());
			diet_malam.setSuperCommand("diet_malam");
			diet_malam.setPrototipe(protoname, protoslug, protoimplement);
			diet_malam.selected=function(json){
				var nama=json.nama;	
				$("#<?php echo $this->action; ?>_diet_malam").val(nama);
			};
        });
    </script>
<?php
    }
}

?>