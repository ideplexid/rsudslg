<?php

global $wpdb;

require_once "smis-libs-class/DBCreator.php";

require_once 'smis-libs-inventory/install.php';
$install = new InventoryInstallator($wpdb, "", "");
$install->extendInstall("gz");
$install->install();

require_once "gizi/resource/install/table/smis_gz_asuhan.php";
require_once "gizi/resource/install/table/smis_gz_bahan.php";
require_once "gizi/resource/install/table/smis_gz_belanja.php";
require_once "gizi/resource/install/table/smis_gz_dana.php";
require_once "gizi/resource/install/table/smis_gz_diet.php";
require_once "gizi/resource/install/table/smis_gz_menu.php";
require_once "gizi/resource/install/table/smis_gz_pesanan.php";
require_once "gizi/resource/install/table/smis_gz_pesanan_pasien.php";
require_once "gizi/resource/install/table/smis_gz_pesanan_nonpasien.php";
require_once "gizi/resource/install/table/smis_gz_tarif.php";
require_once "gizi/resource/install/table/smis_gz_jadwal.php";
require_once "gizi/resource/install/table/smis_gz_diagnosa.php";
require_once "gizi/resource/install/table/smis_gz_icd.php";
require_once "gizi/resource/install/table/smis_gz_konsul.php";

require_once "gizi/resource/install/table/smis_gz_jenis_menu.php";
require_once "gizi/resource/install/table/smis_gz_group_menu.php";

require_once "gizi/resource/install/table/smis_gz_tambahan_biaya.php";