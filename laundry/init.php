<?php
global $PLUGINS;

$init ['name'] = 'laundry';
$init ['path'] = SMIS_DIR . "laundry/";
$init ['description'] = "Laundry System";
$init ['require'] = "administrator";
$init ['service'] = "";
$init ['version'] = "1.0.1";
$init ['number'] = "2";
$init ['type'] = "";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>