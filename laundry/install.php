<?php
	global $wpdb;

	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->setUsing(true, true, true);
	$install->extendInstall("laundry");
	$install->install();
		
	/*require_once("laundry/resource/php/install/install_stok_min_maks_data.php");*/
	
	$query = "
		CREATE TABLE IF NOT EXISTS `smis_laundry_formula` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `nama_formula` varchar(64) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM;
	";
	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_laundry_material` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `nama` varchar(64) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM;

	";
	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_laundry_stock_linen` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `nama_linen` varchar(64) NOT NULL,
		  `stock` int(11) NOT NULL,
		  `status` varchar(64) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM;
	";

	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_laundry_transaksi` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `id_linen` int(11) NOT NULL,
		  `id_formula` int(11) NOT NULL,
		  `jumlah` int(11) NOT NULL,
		  `tanggal` date NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM;
	";

	$wpdb->query ( $query );

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_laundry_detail_formula` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `id_formula` int(11) NOT NULL,
		  `id_material` int(11) NOT NULL,
		  `jumlah` double NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM;
	";
	$wpdb->query ( $query );
	
	$query ="
		CREATE VIEW IF NOT EXISTS `smis_laundry_detail_report` AS(
		SELECT smis_laundry_transaksi.*, smis_laundry_detail_formula.id_material as material, SUM(smis_laundry_detail_formula.jumlah)*smis_laundry_transaksi.jumlah as kuantitas
		FROM smis_laundry_transaksi
		INNER JOIN smis_laundry_detail_formula
		ON smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula
		GROUP by smis_laundry_detail_formula.id_material, smis_laundry_transaksi.tanggal)
		ENGINE=MyISAM;
		;
	";
	$wpdb->query ( $query );
    
    $query="ALTER TABLE  `smis_laundry_detail_formula` ADD  `satuan` VARCHAR(10) NOT NULL AFTER  `jumlah`";
	$wpdb->query($query);
    
    $query = "CREATE VIEW smis_laundry_detail_report_laundry AS SELECT smis_laundry_transaksi.* from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id";
    $wpdb->query($query);
    
    $query="CREATE VIEW smis_laundry_detail_report_rangkuman AS SELECT smis_laundry_transaksi.* from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id INNER JOIN smis_laundry_formula on smis_laundry_transaksi.id_formula = smis_laundry_formula.id group by tanggal,id_formula,id_linen";
    $wpdb->query($query);
    
    $query="CREATE VIEW smis_laundry_detail_report_bahan_per_waktu AS SELECT smis_laundry_transaksi.*, smis_laundry_detail_formula.id_material, sum(smis_laundry_detail_formula.jumlah) as total,smis_laundry_detail_formula.satuan from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id INNER JOIN smis_laundry_detail_formula on smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula group by smis_laundry_detail_formula.id_material";
    $wpdb->query($query);
    
    $query="CREATE VIEW smis_laundry_detail_report_penggunaan_per_bahan AS SELECT smis_laundry_transaksi.*, sum(smis_laundry_detail_formula.jumlah*smis_laundry_transaksi.jumlah) as jumlah_bahan,smis_laundry_detail_formula.id_material, smis_laundry_detail_formula.satuan from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id INNER JOIN smis_laundry_detail_formula on smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula group by smis_laundry_transaksi.tanggal";
    $wpdb->query($query);
    
    $query="CREATE VIEW smis_laundry_detail_report_cucian_per_linen AS SELECT smis_laundry_transaksi.*, smis_laundry_detail_formula.satuan , smis_laundry_detail_formula.jumlah*smis_laundry_transaksi.jumlah as jumlah_bahan from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id INNER JOIN smis_laundry_detail_formula on smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula";
    $wpdb->query($query);
	
	
	$query="
	CREATE VIEW IF NOT EXISTS `smis_laundry_detail_report_per_linen` AS 
SELECT smis_laundry_transaksi.*, smis_laundry_stock_linen.nama_linen, smis_laundry_transaksi.id_linen as linen , sum(smis_laundry_transaksi.jumlah) as totalKg 
from smis_laundry_transaksi 
INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id 
GROUP by smis_laundry_transaksi.id_linen
	";
    $wpdb->query($query);
	
	$query="
	CREATE VIEW IF NOT EXISTS `smis_laundry_detail_report_per_formula` 
AS SELECT smis_laundry_transaksi.*, smis_laundry_formula.nama_formula as NamaFormula, count(smis_laundry_transaksi.id_formula) as JumlahPakai , sum(smis_laundry_transaksi.jumlah) as totalKg from smis_laundry_transaksi
INNER JOIN smis_laundry_formula
on smis_laundry_transaksi.id_formula = smis_laundry_formula.id 
GROUP by smis_laundry_transaksi.id_formula
	";
    $wpdb->query($query);
	
	
	$query="
	CREATE VIEW IF NOT EXISTS `smis_laundry_detail_report_per_formula_detail` 
AS SELECT smis_laundry_transaksi.*, smis_laundry_formula.nama_formula as NamaFormula, count(smis_laundry_transaksi.id_formula) as JumlahPakai , sum(smis_laundry_transaksi.jumlah) as totalKg from smis_laundry_transaksi
INNER JOIN smis_laundry_formula
on smis_laundry_transaksi.id_formula = smis_laundry_formula.id 
GROUP by smis_laundry_transaksi.id_formula, smis_laundry_transaksi.tanggal
	";
    $wpdb->query($query);
	
?>
