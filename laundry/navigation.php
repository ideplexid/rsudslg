<?php
	global $NAVIGATOR;

	// Administrator Top Menu
	$mr = new Menu ("fa fa-graduation-cap");
	$mr->addProperty ( 'title', 'Laundry' );
	$mr->addProperty ( 'name', 'Laundry' );

	$mr->addSubMenu ( "Stock Linen", "laundry", "linen", "Data Stock Linen","fa fa-database" );
	$mr->addSubMenu ( "Data Material", "laundry", "material", "Data Material","fa fa-database" );
	$mr->addSubMenu ( "Edit Formula", "laundry", "formula", "Data Formula Linen","fa fa-database" );
	$mr->addSubMenu ( "Catat Laundry", "laundry", "catat", "Catat Laundry","fa fa-database" );
	$mr->addSubMenu ( "Rekap Laundry", "laundry", "reportLaundry", "Laporan Laundry","fa fa-database" );
    $mr->addSubMenu ( "Laporan Laundry", "laundry", "laporan", "Laporan","fa fa-database" );
	
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "LAUNDRY", "laundry");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'laundry' );
?>
