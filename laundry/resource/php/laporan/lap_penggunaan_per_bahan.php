<?php 
if($data!=null){
	echo $data;
	return;
}


$btn=new Button("", "", "");
$btn->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setAction("lap_penggunaan_per_bahan.cair()")
	->setIcon("fa fa-money");
$header=array("No.","Tanggal","Jumlah Bahan","Satuan Bahan", "Jumlah Cucian");
$uitable=new Table($header,"",NULL,false);
$uitable->setExcelButtonEnable(true);
$btngrup=$uitable->setFooterVisible(false)
			 ->setAddButtonEnable(false)
			 ->setPrintButtonEnable(false)
			 ->setName("lap_penggunaan_per_bahan")
			 ->addHeaderButton($btn)
			 ->getHeaderButton();


if($_POST['command']){
	//require_once 'registration/class/adapter/BonusAdapter.php';
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true,"No.")
			->add("Tanggal", "tanggal","date d M Y")
			->add("Jumlah Bahan", "jumlah_bahan")
            ->add("Satuan Bahan", "satuan")
			->add("Jumlah Cucian", "jumlah","back Kg")
			/*->add("Tanggal","tanggal", "date d M Y")
			->add("URI/URJ", "uri","trivial_0_<strong class='badge badge-important'>Rawat Jalan</strong>_<strong class='badge badge-success'>Rawat Inap</strong>")
			*/
			-//>add("Bonus", "besarbonus","money Rp.");
	$dbtable=new DBTable($db, "smis_laundry_detail_report_penggunaan_per_bahan");
	
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
			->addCustomKriteria(NULL, "tanggal<='".$_POST['sampai']."'")
			->addCustomKriteria(NULL, "id_material='".$_POST['material']."'")
			//->addCustomKriteria(NULL, "id_rujukan='".$_POST['id_perujuk']."'")
			->setShowAll(true);
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$dt=$dbres->command($_POST['command']);
	echo json_encode($dt);
	return;
}

/*$uitable->addModal("perujuk", "chooser-reportLaundry-perujuk", "Perujuk", "");
$uitable->addModal("alamat", "text", "Alamat", "","y",NULL,true);
$uitable->addModal("instansi", "text", "Instansi", "","y",NULL,true);
$uitable->addModal("id_perujuk", "hidden", "", "");*/

$material_table=new DBTable($db,"smis_laundry_material");
$material_table->setShowAll(true);
$material=$material_table->view("","0");
$material_adapter=new SelectAdapter("nama","id");
$hasil=$material_adapter->getContent($material['data']);


$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$uitable->addModal("material","select","Bahan",$hasil);


echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo $uitable->getModal()->getForm()->addElement("", $btngrup)->getHtml();
echo $uitable->getHtml();

?>

<script type="text/javascript">

var lap_penggunaan_per_bahan;
//var perujuk;
$(document).ready(function(){
    var column=new Array("baru","bonus");
    
    lap_penggunaan_per_bahan=new TableAction("lap_penggunaan_per_bahan","laundry","lap_penggunaan_per_bahan",column);
    lap_penggunaan_per_bahan.addRegulerData=function(reg_data){
		reg_data['dari']=$("#lap_penggunaan_per_bahan_dari").val();
		reg_data['sampai']=$("#lap_penggunaan_per_bahan_sampai").val();
        reg_data['material']=$("#lap_penggunaan_per_bahan_material").val();
		//reg_data['id_perujuk']=$("#reportLaundry_id_perujuk").val();
		return reg_data;
	};
	/*perujuk=new TableAction("perujuk","laundry","reportLaundry",column);
    perujuk.setSuperCommand("perujuk");
    perujuk.selected=function(json){
		$("#reportLaundry_perujuk").val(json.nama);
		$("#reportLaundry_id_perujuk").val(json.id);
		$("#reportLaundry_alamat").val(json.alamat);
		$("#reportLaundry_instansi").val(json.instansi);
    };*/
    $('.mydate').datepicker();
});
</script>
