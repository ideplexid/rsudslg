<?php
if($data!=null){
	echo $data;
	return;
}

$btn=new Button("", "", "");
$btn->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setAction("lap_bahan_per_waktu.cair()")
	->setIcon("fa fa-money");
$header=array("No.","Nama Bahan","Jumlah","Satuan");
$uitable=new Table($header,"",NULL,false);
$uitable->setExcelButtonEnable(true);
$btngrup=$uitable->setFooterVisible(false)
			 ->setAddButtonEnable(false)
			 ->setPrintButtonEnable(false)
			 ->setName("lap_bahan_per_waktu")
			 ->addHeaderButton($btn)
			 ->getHeaderButton();

if($_POST['command']){
	//require_once 'registration/class/adapter/BonusAdapter.php';
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true,"No.")
			->add("Nama Bahan", "nama_material")
			->add("Jumlah", "total")
            ->add("Satuan", "satuan");
			/*->add("Tanggal","tanggal", "date d M Y")
			->add("URI/URJ", "uri","trivial_0_<strong class='badge badge-important'>Rawat Jalan</strong>_<strong class='badge badge-success'>Rawat Inap</strong>")
			*/
			//>add("Bonus", "besarbonus","money Rp.");
	$dbtable=new DBTable($db, "smis_laundry_detail_report_bahan_per_waktu");
	
	
	$qv="SELECT smis_laundry_detail_report_bahan_per_waktu.*, smis_laundry_material.nama as nama_material 
	FROM smis_laundry_detail_report_bahan_per_waktu 
	INNER JOIN smis_laundry_material ON smis_laundry_detail_report_bahan_per_waktu.id_material = smis_laundry_material.id
		 ";
	$qc="SELECT count(*) as total
		 FROM smis_laundry_detail_report_bahan_per_waktu ";
		 
	$dbtable->setPreferredQuery(true,$qv,$qc);
	$dbtable->setUseWhereForView(true);
	
	
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
			->addCustomKriteria(NULL, "tanggal<='".$_POST['sampai']."'")
			//->addCustomKriteria(NULL, "id_rujukan='".$_POST['id_perujuk']."'")
			->setShowAll(true);
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$dt=$dbres->command($_POST['command']);
	echo json_encode($dt);
	return;
}

$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");


echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo $uitable->getModal()->getForm()->addElement("", $btngrup)->getHtml();
echo $uitable->getHtml();

// sql
/*
 * select tanggal, (select nama_formula from smis_laundry_formula where id = smis_laundry_transaksi.id_formula) as nama_formula, (select nama_linen from smis_laundry_stock_linen where id = smis_laundry_transaksi.id_linen) as nama_linen, smis_laundry_transaksi.jumlah 
from smis_laundry_transaksi, smis_laundry_detail_formula 
where smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula
*/
?>

<script type="text/javascript">

var lap_bahan_per_waktu;
//var perujuk;
$(document).ready(function(){
    var column=new Array("baru","bonus");
    
    lap_bahan_per_waktu=new TableAction("lap_bahan_per_waktu","laundry","lap_bahan_per_waktu",column);
    lap_bahan_per_waktu.addRegulerData=function(reg_data){
		reg_data['dari']=$("#lap_bahan_per_waktu_dari").val();
		reg_data['sampai']=$("#lap_bahan_per_waktu_sampai").val();
		//reg_data['id_perujuk']=$("#reportLaundry_id_perujuk").val();
		return reg_data;
	};
	/*perujuk=new TableAction("perujuk","laundry","reportLaundry",column);
    perujuk.setSuperCommand("perujuk");
    perujuk.selected=function(json){
		$("#reportLaundry_perujuk").val(json.nama);
		$("#reportLaundry_id_perujuk").val(json.id);
		$("#reportLaundry_alamat").val(json.alamat);
		$("#reportLaundry_instansi").val(json.instansi);
    };*/
    $('.mydate').datepicker();
});
</script>

