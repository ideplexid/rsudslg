<?php
if($data!=null){
	echo $data;
	return;
}

$btn=new Button("", "", "");
$btn->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setAction("lap_rangkuman_laundry.cair()")
	->setIcon("fa fa-money");
$header=array("No.","Tanggal","Formula","Jenis Linen","Jumlah Linen");
$uitable=new Table($header,"",NULL,false);
$uitable->setExcelButtonEnable(true);
$btngrup=$uitable->setFooterVisible(false)
			 ->setAddButtonEnable(false)
			 ->setPrintButtonEnable(false)
			 ->setName("lap_rangkuman_laundry")
			 ->addHeaderButton($btn)
			 ->getHeaderButton();

if($_POST['command']){
	//require_once 'registration/class/adapter/BonusAdapter.php';
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true,"No.")
			->add("Tanggal", "tanggal","date d M Y")
			->add("Formula", "nama_formula")
            ->add("Jenis Linen", "nama_linen")
            ->add("Jumlah Linen", "jumlah","back Kg");
	$dbtable=new DBTable($db, "smis_laundry_detail_report_rangkuman");
	
	
	$qv="SELECT smis_laundry_detail_report_rangkuman.*, smis_laundry_formula.nama_formula as nama_formula, smis_laundry_stock_linen.nama_linen as nama_linen
	FROM smis_laundry_detail_report_rangkuman
	INNER JOIN smis_laundry_formula ON smis_laundry_detail_report_rangkuman.id_formula = smis_laundry_formula.id
    INNER JOIN smis_laundry_stock_linen ON smis_laundry_detail_report_rangkuman.id_linen = smis_laundry_stock_linen.id
		 ";
	$qc="SELECT count(*) as total
		 FROM smis_laundry_detail_report_rangkuman ";
		 
	$dbtable->setPreferredQuery(true,$qv,$qc);
	$dbtable->setUseWhereForView(true);
	
	
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
			->addCustomKriteria(NULL, "tanggal<='".$_POST['sampai']."'")
			->addCustomKriteria(NULL, "id_linen='".$_POST['linen']."'")
            ->addCustomKriteria(NULL, "id_formula='".$_POST['formula']."'")
			->setShowAll(true);
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$dt=$dbres->command($_POST['command']);
	echo json_encode($dt);
	return;
}

// tabel linen
$linen_table=new DBTable($db,"smis_laundry_stock_linen");
$linen_table->setShowAll(true);
$linen=$linen_table->view("","0");
$linen_adapter=new SelectAdapter("nama_linen","id");
$hasil_linen=$linen_adapter->getContent($linen['data']);

// tabel formula
$formula_table=new DBTable($db,"smis_laundry_formula");
$formula_table->setShowAll(true);
$formula=$formula_table->view("","0");
$formula_adapter=new SelectAdapter("nama_formula","id");
$hasil_formula=$formula_adapter->getContent($formula['data']);

$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$uitable->addModal("linen", "select", "Jenis Linen", $hasil_linen);
$uitable->addModal("formula", "select", "Jenis Formula", $hasil_formula);


echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo $uitable->getModal()->getForm()->addElement("", $btngrup)->getHtml();
echo $uitable->getHtml();

?>


<script type="text/javascript">

var lap_rangkuman_laundry;
//var perujuk;
$(document).ready(function(){
    var column=new Array("baru","bonus");
    
    lap_rangkuman_laundry=new TableAction("lap_rangkuman_laundry","laundry","lap_rangkuman_laundry",column);
    lap_rangkuman_laundry.addRegulerData=function(reg_data){
		reg_data['dari']=$("#lap_rangkuman_laundry_dari").val();
		reg_data['sampai']=$("#lap_rangkuman_laundry_sampai").val();
        reg_data['linen']=$("#lap_rangkuman_laundry_linen").val();
        reg_data['formula']=$("#lap_rangkuman_laundry_formula").val();
		//reg_data['id_perujuk']=$("#reportLaundry_id_perujuk").val();
		return reg_data;
	};

    $('.mydate').datepicker();
});
</script>
