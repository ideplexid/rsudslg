<?php 

/*$phead=array("Nama","Alamat","Instansi");
$ptable=new Table($phead);
$ptable->setName("perujuk")
	   ->setModel(Table::$SELECT);
$padapter=new SimpleAdapter();
$padapter->add("Nama","nama")
		 ->add("Alamat", "alamat")
		 ->add("Instansi", "instansi");
$pdbtable=new DBTable($db,"smis_rg_perujuk");
$pdbres=new DBResponder($pdbtable, $ptable, $padapter);
$super=new SuperCommand();
$super->addResponder("perujuk", $pdbres);
$data=$super->initialize();*/
if($data!=null){
	echo $data;
	return;
}


$btn=new Button("", "", "");
$btn->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setAction("lap_cucian_per_waktu.cair()")
	->setIcon("fa fa-money");
$header=array("No.","Material","Jumlah Dipakai");
$uitable=new Table($header,"",NULL,false);
$uitable->setExcelButtonEnable(true);
$btngrup=$uitable->setFooterVisible(false)
			 ->setAddButtonEnable(false)
			 ->setPrintButtonEnable(false)
			 ->setName("lap_cucian_per_waktu")
			 ->addHeaderButton($btn)
			 ->getHeaderButton();


if($_POST['command']){
	//require_once 'registration/class/adapter/BonusAdapter.php';
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true,"No.")
			->add("Material", "nama_linen")
			->add("Jumlah Dipakai", "totalKg")
			/*->add("Tanggal","tanggal", "date d M Y")
			->add("URI/URJ", "uri","trivial_0_<strong class='badge badge-important'>Rawat Jalan</strong>_<strong class='badge badge-success'>Rawat Inap</strong>")
			*/
			-//>add("Bonus", "besarbonus","money Rp.");
	$dbtable=new DBTable($db, "smis_laundry_detail_report_per_linen");
	
	
	$qv="SELECT smis_laundry_detail_report_per_linen.*
	FROM smis_laundry_detail_report_per_linen";
	$qc="SELECT count(*) as total
		 FROM smis_laundry_detail_report_per_linen ";
		 
	$dbtable->setPreferredQuery(true,$qv,$qc);
	$dbtable->setUseWhereForView(true);
	
	
	$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
			->addCustomKriteria(NULL, "tanggal<='".$_POST['sampai']."'")
			//->addCustomKriteria(NULL, "id_rujukan='".$_POST['id_perujuk']."'")
			->setShowAll(true);
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$dt=$dbres->command($_POST['command']);
	echo json_encode($dt);
	return;
}

/*$uitable->addModal("perujuk", "chooser-reportLaundry-perujuk", "Perujuk", "");
$uitable->addModal("alamat", "text", "Alamat", "","y",NULL,true);
$uitable->addModal("instansi", "text", "Instansi", "","y",NULL,true);
$uitable->addModal("id_perujuk", "hidden", "", "");*/
$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");


echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo $uitable->getModal()->getForm()->addElement("", $btngrup)->getHtml();
echo $uitable->getHtml();

?>

<script type="text/javascript">

var lap_cucian_per_waktu;
//var perujuk;
$(document).ready(function(){
    var column=new Array("baru","bonus");
    
    lap_cucian_per_waktu=new TableAction("lap_cucian_per_waktu","laundry","lap_cucian_per_waktu",column);
    lap_cucian_per_waktu.addRegulerData=function(reg_data){
		reg_data['dari']=$("#lap_cucian_per_waktu_dari").val();
		reg_data['sampai']=$("#lap_cucian_per_waktu_sampai").val();
		//reg_data['id_perujuk']=$("#reportLaundry_id_perujuk").val();
		return reg_data;
	};
	/*perujuk=new TableAction("perujuk","laundry","reportLaundry",column);
    perujuk.setSuperCommand("perujuk");
    perujuk.selected=function(json){
		$("#reportLaundry_perujuk").val(json.nama);
		$("#reportLaundry_id_perujuk").val(json.id);
		$("#reportLaundry_alamat").val(json.alamat);
		$("#reportLaundry_instansi").val(json.instansi);
    };*/
    $('.mydate').datepicker();
});
</script>
