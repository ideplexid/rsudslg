<?php
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	global $db;	
	$smis = new SettingsBuilder ( $db, "hrd_settings", "hrd", "settings","Pengaturan Personalia" );
	$smis->setShowDescription ( true );
	$smis->setTabulatorMode ( Tabulator::$POTRAIT );
	$smis->addTabs ( "kepegawaian", "Kepegawaian","fa fa-users" );	
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-nidn", "Tampilkan NIDN", "", "checkbox", "Tampilkan NIDN dalam Menu Pegawai" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-gaji-pokok", "Tampilkan Gaji Pokok", "", "checkbox", "Tampilkan Gaji Pokok" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-jasa-pelayanan", "Tampilkan Jasa Pelayanan", "", "checkbox", "Tampilkan Jasa Pelayanan" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-struktural", "Tampilkan Tunjangan Struktural", "", "checkbox", "Tampilkan Tunjangan Struktural" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-fungsional", "Tampilkan Tunjangan Fungsional", "", "checkbox", "Tampilkan Tunjangan Fungsional" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-golongan", "Tampilkan Tunjangan Golongan", "", "checkbox", "Tampilkan Tunjangan Golongan" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-pengabdian", "Tampilkan Tunjangan Pengabdian", "", "checkbox", "Tampilkan Tunjangan Pengabdian" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-jabatan", "Tampilkan Tunjangan Jabatan", "", "checkbox", "Tampilkan Tunjangan Jabatan" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-pendidikan", "Tampilkan Tunjangan Pendidikan", "", "checkbox", "Tampilkan Tunjangan Pendidikan" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-pasutri", "Tampilkan Tunjangan Suami / Istri", "", "checkbox", "Tampilkan Tunjangan Suami / Istri" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-anak", "Tampilkan Tunjangan Anak", "", "checkbox", "Tampilkan Tunjangan Anak" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-tunjangan-kehadiran", "Tampilkan Tunjangan Absensi dan Kehadiran", "", "checkbox", "Tampilkan Tunjangan Absensi dan Kehadiran" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-uang-makan", "Tampilkan Uang Makan", "", "checkbox", "Tampilkan Uang Makan" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-uang-transportasi", "Tampilkan Uang Transportasi", "", "checkbox", "Tampilkan Uang Transportasi" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-dana-pensiun", "Tampilkan Dana Pensiun", "", "checkbox", "Tampilkan Dana Pensiun" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-insentif", "Tampilkan Dana Insentif", "", "checkbox", "Tampilkan Dana Insentif" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-bpjs-kesehatan", "Tampilkan Dana BPJS Kesehatan", "", "checkbox", "Tampilkan Dana BPJS Kesehatan" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-bpjs-ketenagakerjaan", "Tampilkan BPJS Ketenaga Kerjaan", "", "checkbox", "Tampilkan BPJS Ketenagakerjaan" ) );
	$smis->addItem ( "kepegawaian", new SettingsItem ( $db, "hrd-employee-lain", "Tampilkan Lain-Lain", "", "checkbox", "Tampilkan Lain-Lain" ) );
	
	$op=new OptionBuilder();
	$op->add("Bulatkan Ke Bawah Sampai Jam ","floor-jam","1");
	$op->add("Bulatkan Ke Atas Sampai Jam ","ceil-jam","1");
	$op->add("Bulatkan Sampai Jam (0.5 Keatas dianggap 1) ","jam","1");
	$op->add("Bulatkan Kebawah Sampai Menit (2 Digit di belakang Koma) ","menit");

	$smis->addTabs ( "perhitungan", "Perhitungan","fa fa-calculator" );
	$smis->addItem ( "perhitungan", new SettingsItem ( $db, "hrd-employee-nilai-lembur", "Nilai Per Jam Lembur", "0", "money", "Nilai Lembur Per Jam" ) );
	$smis->addItem ( "perhitungan", new SettingsItem ( $db, "hrd-employee-hitung-lembur", "Perhitungan Lembur", $op->getContent(), "select", "Perhitungan Lembur" ) );
	$response = $smis->init ();
?>