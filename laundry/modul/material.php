<?php
	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
	$mahasiswa=new MasterTemplate($db, "smis_laundry_material", "laundry", "material");
	$uitable=$mahasiswa->getUItable();
	$uitable->addHeaderElement("No.");
	$uitable->addHeaderElement("Nama Material");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("nama", "text", "Nama Material", "");
	$adapter=$mahasiswa->getAdapter();
	$adapter->add("Nama Material", "nama")
	->setUseNumber(true,"No.");
	$mahasiswa->setModalTitle("Material");
	$mahasiswa->initialize();
?>
	