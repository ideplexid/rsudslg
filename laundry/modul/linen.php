<?php
	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
	$mahasiswa=new MasterTemplate($db, "smis_laundry_stock_linen", "laundry", "linen");
	$uitable=$mahasiswa->getUItable();
	$uitable->addHeaderElement("Nama Linen")
			->addHeaderElement("Stock")
			->addHeaderElement("Status");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("nama_linen", "text", "Nama", "")
			->addModal("stock", "text", "Stock", "")
			->addModal("status", "text", "Status", "");
	$adapter=$mahasiswa->getAdapter();
	$adapter->add("Nama Linen", "nama_linen")
			->add("Stock", "stock")
			->add("Status", "status");
	$mahasiswa->setModalTitle("Linen");
	$mahasiswa->initialize();
?>
	


