<?php
/**
 * this is handling about element where used for patient management
 * @author goblooge
 *
 * 
 */
global $db;

/*UNTUK CHOOSER*/
$linen_table=new Table(array("No.","Linen","Jumlah"));
$linen_table->setName("linen");
$linen_table->setModel(Table::$SELECT);
$linen_dbtable=new DBTable($db,"smis_laundry_stock_linen");
$linen_adapter = new SimpleAdapter(true,"No.");
$linen_adapter->add("Linen","nama_linen");
$linen_adapter->add("Jumlah","stock");
$linen_dbresponder=new DBResponder($linen_dbtable,$linen_table,$linen_adapter);
$super=new SuperCommand();
$super->addResponder("linen",$linen_dbresponder);
$hasil=$super->initialize();
if($hasil!=NULL){
	echo $hasil;
	return;
}



$header=array('Tanggal','Jenis','Formula','Jumlah (KG)');
$uitable = new Table($header,"catat Laundry",NULL,true);
$uitable->setName("catat");
$uitable->setExcelButtonEnable(true);
if(isset($_POST['command'])) {
	$adapter = new SimpleAdapter();
	$adapter->add("Tanggal","tanggal","date d M Y");
	$adapter->add("Jenis","nama_linen");//
	$adapter->add("Formula","nama_formula");//
	$adapter->add("Jumlah (KG)","jumlah","back Kg");
	$dbtable = new DBTable($db,"smis_laundry_transaksi");
	
	$qv="SELECT smis_laundry_transaksi.*, smis_laundry_formula.nama_formula as nama_formula , smis_laundry_stock_linen.nama_linen as nama_linen
		 FROM smis_laundry_transaksi 
		 LEFT JOIN smis_laundry_formula ON smis_laundry_transaksi.id_formula=smis_laundry_formula.id 
		 LEFT JOIN smis_laundry_stock_linen ON smis_laundry_transaksi.id_linen=smis_laundry_stock_linen.id
		 ";
	$qc="SELECT count(*) as total
		 FROM smis_laundry_transaksi ";
		 
	$dbtable->setPreferredQuery(true,$qv,$qc);
	$dbtable->setUseWhereForView(true);
	
	$dbres = new DBResponder($dbtable,$uitable,$adapter);
	$data = $dbres->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$linen_table=new DBTable($db,"smis_laundry_stock_linen");
$linen_table->setShowAll(true);
$linen=$linen_table->view("","0");
$linen_adapter=new SelectAdapter("nama_linen","id");
$hasil=$linen_adapter->getContent($linen['data']);

$formula_table=new DBTable($db,"smis_laundry_formula");
$formula_table->setShowAll(true);
$formula=$formula_table->view("","0");
$formula_adapter=new SelectAdapter("nama_formula","id");
$hasil_formula=$formula_adapter->getContent($formula['data']);

/*
$array=array();
$array[]=array("name"=>"LINE 1","value"=>"1","default"=>"0");
$array[]=array("name"=>"LINE 2","value"=>"2","default"=>"1");
$array[]=array("name"=>"LINE 3","value"=>"3","default"=>"0");
$array[]=array("name"=>"LINE 4","value"=>"4","default"=>"0");
*/

$uitable->addModal("id","hidden","","");
$uitable->addModal("id_linen","select","Nama Linen",$hasil);
$uitable->addModal("id_formula","select","Nama Formula",$hasil_formula);
//$uitable->addModal("nama_linen","chooser-catat-linen-Pilih Linen","Nama Linen","",'n',null,false,null,true);
//$uitable->addModal("id_linen","hidden","","");

$uitable->addModal("jumlah","text","Jumlah (KG)","",'n',"alphanumeric",false,null,false,"save");
$uitable->addModal("tanggal","date","Tanggal",date("Y-m-d"),"n","date");
$uitable->addModal("prop","hidden","","");
$modal = $uitable->getModal();
$modal->setTitle("catat");
echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );

?>
<script type="text/javascript">
var catat;
var linen;
$(document).ready(function(){
	$(".mydate").datepicker();
	//$("#catat_id_linen").select2();
	
	linen=new TableAction("linen","laundry","catat",new Array(""));
	linen.setSuperCommand("linen");
	
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement':'bottom'});
	var column=new Array('id','id_linen','id_formula','jumlah','tanggal','prop');
	catat=new TableAction("catat","laundry","catat",column);
	catat.view();
	/*
	catat.setMultipleInput(true);
	catat.setNextEnter(true);
	catat.setEnableAutofocus();
	catat.setFocusOnMultiple("jumlah");
	catat.addNoClear("nama_linen");
	catat.addNoClear("id_linen");
	catat.setDisabledOnEdit(new Array("jumlah","tanggal"));
	

	
	linen.selected=function(json){
		$("#catat_id_linen").val(json.id);
		$("#catat_nama_linen").val(json.nama_material);
		$("#catat_jumlah").focus();
	};
	//CHOOSER AUTOCOMPLETE
	stypeahead("#catat_nama_linen",3,linen,"nama_material",function(json){
		$("#catat_id_linen").val(json.id);
		$("#catat_nama_linen").val(json.nama_material);	
		$("#catat_jumlah").focus();
	});*/
	
	
});
</script>