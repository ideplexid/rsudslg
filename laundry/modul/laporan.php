<?php 
$tab = new Tabulator ( "laporan", "" );
$tab->add ( "lap_detail_laundry", "Laporan Detail Laundry", "", Tabulator::$TYPE_HTML,"fa fa-ambulance" );
$tab->add ( "lap_rangkuman_laundry", "Laporan Rangkuman Laundry", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->add ( "lap_bahan_per_waktu", "Laporan Bahan Per Waktu", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "lap_penggunaan_per_bahan", "Laporan Penggunaan Per Bahan", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "lap_cucian_per_waktu", "Laporan Cucian Per Waktu", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "lap_cucian_per_linen", "Laporan Cucian Per Linen", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "lap_formula_per_waktu", "Laporan Formula Per Waktu", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "lap_formula_per_formula", "Laporan Formula Per Formula", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->setPartialLoad(true,"laundry","laporan","laporan",true);
echo $tab->getHtml ();
?>