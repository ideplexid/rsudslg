<?php
	
	global $db;
	//require_once ("laundry/class/responder/DetailResponder.php");
	require_once ("smis-framework/smis/database/DBParentChildResponder.php");

	$head=array('No.','Nama Formula' );
	$parent_table = new Table ( $head, "", NULL, true );
	$parent_table->setName ( "formula_parent" );
	$parent_table->setActionName ( "formula.parent" );


	$child_table = new Table ( array (
			"No.",
			'Nama Material',
			"Jumlah",
            "Satuan"
	), "", NULL, true );
	$child_table->setName ( "formula_child" );
	$child_table->setActionName ( "formula.child" );

	if (isset ( $_POST ['command'] )) {
		$parent_adapter = new SimpleAdapter ();
		$parent_adapter->add("Nama Formula", "nama_formula");
        $parent_adapter->setUseNumber(true,"No.");
		$parent_column = array ('id','nama_formula','prop');
		$parent_dbtable = new DBTable ( $db, "smis_laundry_formula", $parent_column );
		$parent = new DBResponder ( $parent_dbtable, $parent_table, $parent_adapter );

		$child_adapter = new SimpleAdapter();
		$child_adapter->setUseNumber(true,"No.");
		$child_adapter->add("Nama Material", "nama_material");
		$child_adapter->add("Jumlah", "jumlah");
        $child_adapter->add("Satuan", "satuan");
		$child_column = array (
				'id',
				'id_formula',
				'id_material',
				'jumlah',
                'satuan',
		);
		$child_dbtable = new DBTable ( $db, "smis_laundry_detail_formula", $child_column );
		$child = new DBChildResponder ( $child_dbtable, $child_table, $child_adapter, "id_formula","" );
		$child_dbtable->setShowAll(true);

		$qv="SELECT smis_laundry_detail_formula.*,  smis_laundry_material.nama as nama_material 
		 FROM smis_laundry_material 
		 LEFT JOIN smis_laundry_detail_formula ON smis_laundry_detail_formula.id_material=smis_laundry_material.id ";
		$qc="SELECT count(*) as total
			 FROM smis_laundry_detail_formula ";
		$child_dbtable->setPreferredQuery(true,$qv,$qc);
		$child_dbtable->setUseWhereForView(true);

		$dbres = new DBParentChildResponder ( $parent, $child );
		$data = $dbres->command ( $_POST ['super_command'], $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}

	/* Parent Modal */
	$mcolumn = array (
			'id',
			'nama_formula'
	);
	$mname = array (
			"",
			"Nama Formula",
	);
	$mtype = array (
			"hidden",
			"text" 
	);
	$mvalue = array (
			"",
			"" 
	);
	$parent_table->setModal ( $mcolumn, $mtype, $mname, $mvalue );
	$modal_parent = $parent_table->getModal ();
	$modal_parent->setTitle ( "Data Formula" );
	$modal_parent->addBody ( "smis_laundry_formula", $child_table );
	$modal_parent->setClass ( Modal::$FULL_MODEL );

	/* Child Modal */
	$dbtable_account = new DBTable ( $db, "smis_laundry_detail_formula" );
	$dbtable_account->setShowAll ( true );
	$dbtable_account->setOrder ( "nomor ASC" );

	$material_table=new DBTable($db,"smis_laundry_material");
	$material_table->setShowAll(true);
	$material=$material_table->view("","0");
	$material_adapter=new SelectAdapter("nama","id");
	$hasil=$material_adapter->getContent($material['data']);
    
    $arr = array();
    $arr[]=array("name"=>"Gram","value"=>"Gram","default"=>"1");
    $arr[]=array("name"=>"ml","value"=>"ml","default"=>"0");
    
	$child_table->addModal("id", "hidden", "", "")
				->addModal("id_formula", "hidden", "", "")
				->addModal("id_material", "select", "Kode Material",$hasil)
				->addModal("jumlah", "text", "Jumlah","")
                ->addModal("satuan", "select", "Satuan",$arr);

	$modal_child = $child_table->getModal ();
	$modal_child->setTitle ( "Detail Formula" );

	echo $parent_table->getHtml ();
	echo $modal_parent->getHtml ();
	echo $modal_child->getHtml ();

	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
    echo addJS("framework/smis/js/table_action.js");
    echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS ( "framework/smis/js/child_parent.js" );
	echo addJS("framework/bootstrap/js/bootstrap-select.js");
	echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );
?>

<script type="text/javascript">
	var formula;
	$(document).ready(function(){
		var parent_column=new Array('id','nama_formula');
		var child_column=new Array('id','id_formula','id_material','jumlah','satuan');

		formula=new ParentChildAction("formula","laundry","formula",parent_column,child_column,'id_formula');
		formula.parent.view();
	});
</script>


