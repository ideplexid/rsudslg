<?php 
global $db;
$qv="SELECT 0 as id,
			pendidikan, 
			count(*) as total,
			sum(if(jk=0,1,0)) as lk,
			sum(if(jk=1,1,0)) as pr,
			'' as prop
			FROM smis_hrd_employee_aktif";

$qc="SELECT count(*) FROM smis_hrd_employee_aktif";
if (isset ( $_POST ['command'] )) {
	$dbtable=new DBTable($db, "smis_hrd_employee_aktif");
	$dbtable->setPreferredQuery(true, $qv, $qc);
	$dbtable->setShowAll(true);
	$dbtable->setOrder(" pendidikan ASC  ");
	$dbtable->setGroupBy(true, " pendidikan ");
	$service = new ServiceProvider ( $dbtable );
	$pack = $service->command ( $_POST ['command'] );
	echo json_encode ( $pack );
}
?>