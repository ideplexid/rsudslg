<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("laundry", $user,"modul/");	
	$policy=new Policy("laundry", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("linen","linen",Policy::$DEFAULT_COOKIE_CHANGE,"modul/linen");
	$policy->addPolicy("material","material",Policy::$DEFAULT_COOKIE_CHANGE,"modul/material");
	$policy->addPolicy("formula","formula",Policy::$DEFAULT_COOKIE_CHANGE,"modul/formula");
	$policy->addPolicy("catat","catat",Policy::$DEFAULT_COOKIE_CHANGE,"modul/catat");
	$policy->addPolicy("reportLaundry","reportLaundry",Policy::$DEFAULT_COOKIE_CHANGE,"modul/reportLaundry");
    $policy->addPolicy("laporan","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
    $policy->addPolicy("lap_detail_laundry","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_detail_laundry");
	$policy->addPolicy("lap_rangkuman_laundry","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_rangkuman_laundry");
    $policy->addPolicy("lap_bahan_per_waktu","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_bahan_per_waktu");
    $policy->addPolicy("lap_penggunaan_per_bahan","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_penggunaan_per_bahan");
    $policy->addPolicy("lap_cucian_per_waktu","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_cucian_per_waktu");
    $policy->addPolicy("lap_cucian_per_linen","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_cucian_per_linen");
    $policy->addPolicy("lap_formula_per_waktu","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_formula_per_waktu");
    $policy->addPolicy("lap_formula_per_formula","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"resource/php/laporan/lap_formula_per_formula");
    
    
    $policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");
	
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>
