<?php
global $wpdb;
global $NUMBER;

if($NUMBER < 2){
	$query="	ALTER TABLE  `smis_laundry_stock_linen` MODIFY COLUMN `status` VARCHAR(64) NOT NULL;";
	$wpdb->query($query);

	$query="CREATE TABLE IF NOT EXISTS `smis_laundry_formula` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `nama_formula` varchar(64) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM;
	";
	$wpdb->query($query);

	$query = "
		CREATE TABLE IF NOT EXISTS `smis_laundry_detail_formula` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `id_formula` int(11) NOT NULL,
		  `jumlah` double NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM;
	";
	$wpdb->query ( $query );
    
    $query ="
		CREATE VIEW IF NOT EXISTS `smis_laundry_detail_report` AS
		SELECT smis_laundry_transaksi.*, smis_laundry_detail_formula.id_material as material, SUM(smis_laundry_detail_formula.jumlah)*smis_laundry_transaksi.jumlah as kuantitas
		FROM smis_laundry_transaksi
		INNER JOIN smis_laundry_detail_formula
		ON smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula
		GROUP by smis_laundry_detail_formula.id_material, smis_laundry_transaksi.tanggal
		;
	";
    
    $wpdb->query ( $query );
    
}



if($NUMBER<3){
	$query="ALTER TABLE  `smis_laundry_detail_formula` ADD  `satuan` VARCHAR(10) NOT NULL AFTER  `jumlah`";
	$wpdb->query($query);
    
    $query = "CREATE VIEW smis_laundry_detail_report_laundry AS SELECT smis_laundry_transaksi.* from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id";
    $wpdb->query($query);
    
    $query="CREATE VIEW smis_laundry_detail_report_rangkuman AS SELECT smis_laundry_transaksi.* from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id INNER JOIN smis_laundry_formula on smis_laundry_transaksi.id_formula = smis_laundry_formula.id group by tanggal,id_formula,id_linen";
    $wpdb->query($query);
    
    $query="CREATE VIEW smis_laundry_detail_report_bahan_per_waktu AS SELECT smis_laundry_transaksi.*, smis_laundry_detail_formula.id_material, sum(smis_laundry_detail_formula.jumlah) as total,smis_laundry_detail_formula.satuan from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id INNER JOIN smis_laundry_detail_formula on smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula group by smis_laundry_detail_formula.id_material";
    $wpdb->query($query);
    
    $query="CREATE VIEW smis_laundry_detail_report_penggunaan_per_bahan AS SELECT smis_laundry_transaksi.*, sum(smis_laundry_detail_formula.jumlah*smis_laundry_transaksi.jumlah) as jumlah_bahan,smis_laundry_detail_formula.id_material, smis_laundry_detail_formula.satuan from smis_laundry_transaksi INNER JOIN smis_laundry_stock_linen on smis_laundry_transaksi.id_linen = smis_laundry_stock_linen.id INNER JOIN smis_laundry_detail_formula on smis_laundry_transaksi.id_formula = smis_laundry_detail_formula.id_formula group by smis_laundry_transaksi.tanggal";
    $wpdb->query($query);
}


/*
if($NUMBER<4){
	$query="ALTER TABLE  `smis_hrd_employee` ADD  `tgl_lahir` DATE NOT NULL AFTER  `keterangan` ,
			ADD  `tmp_lahir` VARCHAR( 32 ) NOT NULL AFTER  `tgl_lahir` ,
			ADD  `noktp` VARCHAR( 32 ) NOT NULL AFTER  `tmp_lahir` ,
			ADD  `nokk` VARCHAR( 32 ) NOT NULL AFTER  `noktp` ,
			ADD  `menikah` VARCHAR( 32 ) NOT NULL AFTER  `nokk` ,
			ADD  `jk` BOOLEAN NOT NULL AFTER  `menikah` ,
			ADD  `suami_istri` VARCHAR( 32 ) NOT NULL AFTER  `jk` ,
			ADD  `anak_lk` INT NOT NULL AFTER  `suami_istri` ,
			ADD  `anak_pr` INT NOT NULL AFTER  `anak_lk` ,
			ADD  `alamat` VARCHAR( 32 ) NOT NULL AFTER  `anak_pr` ,
			ADD  `ayah` VARCHAR( 32 ) NOT NULL AFTER  `alamat` ,
			ADD  `ibu` VARCHAR( 32 ) NOT NULL AFTER  `ayah`";
	$wpdb->query ( $query );
}

if($NUMBER<5){
	$query="ALTER TABLE  `smis_hrd_employee` ADD  `nidn` VARCHAR( 32 ) NOT NULL AFTER  `nip` ;";
	$wpdb->query ( $query );
}

if($NUMBER<6){
	$query="ALTER TABLE  `smis_hrd_employee` ADD  `struktural` VARCHAR( 32 ) NOT NULL AFTER  `jabatan` ,
		ADD  `pelatihan` TEXT NOT NULL AFTER  `struktural` ;
		";
	$wpdb->query ( $query );
	
	$query="ALTER TABLE  `smis_hrd_employee` ADD  `no_ijin` VARCHAR( 64 ) NOT NULL AFTER  `struktural` ;";
	$wpdb->query ( $query );
	
}

if($NUMBER<7){
	$query="ALTER TABLE  `smis_hrd_employee` ADD  `organik` BOOLEAN NOT NULL AFTER  `menikah` ,
		ADD  `tenaga` VARCHAR( 32 ) NOT NULL AFTER  `organik` ;";
	$wpdb->query ( $query );
}

if($NUMBER<8){
	$query="ALTER TABLE  `smis_hrd_employee` 
			ADD  `ruangan` TEXT NOT NULL AFTER  `tenaga`,
			ADD  `finger_print` TEXT NOT NULL AFTER  `ruangan` ,
			ADD  `finger_time` DATETIME NOT NULL AFTER  `finger_print` ,
			ADD  `finger_duration` INT NOT NULL AFTER  `finger_time` ; ;";
	$wpdb->query ( $query );
}

if($NUMBER<9){
	$query="ALTER TABLE  `smis_hrd_employee` ADD  `gaji_pokok` INT NOT NULL AFTER  `finger_duration` ,
			ADD  `jaspel` INT NOT NULL AFTER  `gaji_pokok` ,
			ADD  `tunj_struktural` INT NOT NULL AFTER  `jaspel` ,
			ADD  `tunj_fungsional` INT NOT NULL AFTER  `tunj_struktural` ,
			ADD  `tunj_pasutri` INT NOT NULL AFTER  `tunj_fungsional` ,
			ADD  `tunj_anak` INT NOT NULL AFTER  `tunj_pasutri` ,
			ADD  `tunj_kehadiran` INT NOT NULL AFTER  `tunj_anak` ,
			ADD  `uang_transport` INT NOT NULL AFTER  `tunj_kehadiran` ,
			ADD  `uang_makan` INT NOT NULL AFTER  `uang_transport` ;";
	$wpdb->query ( $query );
	
	
}

if($NUMBER<10){
	$query="CREATE TABLE IF NOT EXISTS `smis_hrd_berkas` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `id_pegawai` int(11) NOT NULL,
	  `nama` varchar(32) NOT NULL,
	  `keterangan` varchar(128) NOT NULL,
	  `tanggal_dapat` date NOT NULL,
	  `tanggal_kedaluarsa` date NOT NULL,
	  `file` text NOT NULL,
	  `jenis_berkas` varchar(32) NOT NULL,
	  `prop` varchar(10) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB;";
	$wpdb->query ( $query );
	
	$query="
	CREATE TABLE IF NOT EXISTS `smis_hrd_bon` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `id_pegawai` int(11) NOT NULL,
	  `keterangan` varchar(128) NOT NULL,
	  `nilai` int(11) NOT NULL,
	  `tanggal` date NOT NULL,
	  `prop` varchar(10) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB;";
	$wpdb->query ( $query );
	
	$query="CREATE TABLE IF NOT EXISTS `smis_hrd_lembur` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `tanggal` date NOT NULL,
		  `id_pegawai` int(11) NOT NULL,
		  `keterangan` varchar(128) NOT NULL,
		  `nilai` int(11) NOT NULL,
		  `dari` datetime NOT NULL,
		  `sampai` datetime NOT NULL,
		  `jam` int(11) NOT NULL,
		  `total` int(11) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB;";
	$wpdb->query ( $query );
	
}

if($NUMBER<11){
	$query="ALTER TABLE  `smis_hrd_bon` ADD  `jenis` VARCHAR( 32 ) NOT NULL AFTER  `id` ;";
	$wpdb->query ( $query );
	
	$query="ALTER TABLE  `smis_hrd_employee` ADD  `tunjangan_golongan` INT NOT NULL AFTER  `uang_makan` ,
						ADD  `tunjangan_pengabdian` INT NOT NULL AFTER  `tunjangan_golongan` ,
						ADD  `dana_pensiun` INT NOT NULL AFTER  `tunjangan_pengabdian` ,
						ADD  `bpjs_ketenagakerjaan` INT NOT NULL AFTER  `dana_pensiun` ,
						ADD  `bpjs_kesehatan` INT NOT NULL AFTER  `bpjs_ketenagakerjaan` ,
						ADD  `insentif` INT NOT NULL AFTER  `bpjs_kesehatan` ,
						ADD  `lain_lain` INT NOT NULL AFTER  `insentif` ;
	";
	$wpdb->query ( $query );
	
	$query="CREATE TABLE IF NOT EXISTS `smis_hrd_jenis_bon` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `jenis_potongan` varchar(32) NOT NULL,
		  `prop` varchar(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB;";
	$wpdb->query ( $query );
}

if($NUMBER<12){
	$query="ALTER TABLE  `smis_hrd_employee` 
			ADD  `tunj_jabatan` INT NOT NULL AFTER  `tunj_fungsional` ,
			ADD  `tunj_pendidikan` INT NOT NULL AFTER  `tunj_jabatan` ;";
	$wpdb->query ( $query );
	
	
}


if($NUMBER<13){
	$query="ALTER TABLE `smis_hrd_employee` ADD `anak_satu` VARCHAR(64) NOT NULL AFTER `anak_pr`, ADD `anak_dua` VARCHAR(64) NOT NULL AFTER `anak_satu`;";
	$wpdb->query ( $query );
}

if($NUMBER<14){
	$query="ALTER TABLE `smis_hrd_employee` ADD `tanggal_masuk` DATE NOT NULL AFTER `nama`;";
	$wpdb->query ( $query );
}

if($NUMBER<15){
	$query="ALTER TABLE `smis_hrd_employee` ADD `keluar` BOOLEAN NOT NULL AFTER `nama`;";
	$wpdb->query ( $query );
	
	$query="create or replace view smis_vhrd_employee as SELECT smis_hrd_employee.*, smis_hrd_job.nama as nama_jabatan, smis_hrd_job.slug as slug  FROM smis_hrd_employee LEFT JOIN smis_hrd_job ON smis_hrd_employee.jabatan=smis_hrd_job.id;  ";
	$wpdb->query ( $query );
	
	$query="create view smis_hrd_employee_aktif as SELECT smis_hrd_employee.*, smis_hrd_job.nama as nama_jabatan, smis_hrd_job.slug as slug  FROM smis_hrd_employee LEFT JOIN smis_hrd_job ON smis_hrd_employee.jabatan=smis_hrd_job.id WHERE smis_hrd_employee.keluar=0";
	$wpdb->query ( $query );
}*/
?>