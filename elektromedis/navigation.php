<?php
	global $NAVIGATOR;
	$mr = new Menu ("fa fa-heartbeat ");
	$mr->addProperty ( 'title', 'Elektromedis' );
	$mr->addProperty ( 'name', 'Elektromedis' );
	$mr->addSubMenu ( "Check Up", "elektromedis", "pemeriksaan", "Pemeriksaan elektromedis" ,"fa fa-line-chart");
    
    // bagian arsip modul elektromedis
    $mr->addSubMenu ( "Arsip", "elektromedis", "arsip_menu", "Arsip Elektromedis" ,"fa fa-archive");
    $mr->addSubMenu ( "Data Induk", "elektromedis", "data_induk", "Data Induk" ,"fa fa-check");
    $mr->addSubMenu ( "Laporan", "elektromedis", "laporan_menu", "Laporan" ,"fa fa-bar-chart");
	$mr->addSubMenu ( "Settings", "elektromedis", "settings", "Settings Dari Elektromedis" ,"fa fa-gear");
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Elektromedis", "elektromedis");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'elektromedis' );
?>
