<?php 
	require_once 'smis-libs-inventory/cronjob/StockBarangED.php';
	require_once 'smis-libs-inventory/cronjob/StockObatED.php';
	global $db;
	global $notification;
	$barang=new CronjobStockBarangED($db, $notification, "smis_emd_stok_barang", "elektromedis");
	$barang->initialize();
	$obat=new CronjobStockObatED($db, $notification, "smis_emd_stok_obat", "elektromedis");
	$obat->initialize();
    
    /*auto archive pasien*/
    $auto=getSettings($db,"elektromedis-ui-pemeriksaan-auto-archive","0")*1;
    if($auto>0){
        loadLibrary("smis-libs-function-time");
        $max_date=sub_date(date("Y-m-d"),2);
        $query="update smis_emd_pesanan selesai=1 WHERE tanggal<'".$max_date."' AND selesai=0 ";
    }
    
    
?>