<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		// x-ray, echo, ct-scan:
		$dbtable = new DBTable($db, "smis_emd_pesanan");
		$rows = $dbtable->get_result("
			SELECT *
			FROM smis_emd_pesanan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$pemeriksaan_x_ray = 0;
		$pemeriksaan_ct_scan = 0;
		$pemeriksaan_usg_echo = 0;
		if ($rows != null) {
			foreach ($rows as $row) {
				$kelas = $row->kelas;
				$layanan_arr = json_decode($row->periksa, true);
				$harga_arr = json_decode($row->harga, true);
				if ($layanan_arr != null) {
					foreach ($layanan_arr as $key => $value) {
						if ($value == 1) {
							$id_layanan = str_replace("emd_", "", $key);
							$row_layanan = $dbtable->get_row("
								SELECT *
								FROM smis_emd_layanan
								WHERE id = '" . $id_layanan . "'
							");
							$harga_layanan = $harga_arr[$kelas . "_" . $key];
							if (strpos($row_layanan->layanan, "ct scan") != "") {
								$pemeriksaan_ct_scan += $harga_layanan;
							} else if (strpos($row_layanan->layanan, "echo") != "") {
								$pemeriksaan_usg_echo += $harga_layanan;
							} else {
								$pemeriksaan_x_ray += $harga_layanan;
							}
						}
					}
				}
			}
		}
		// pemeriksaan cito:
		$dbtable = new DBTable($db, "smis_emd_dpesanan_lain");
		$row = $dbtable->get_row("
			SELECT SUM(a.jumlah * a.harga_layanan) AS 'pemeriksaan_cito'
			FROM smis_emd_dpesanan_lain a LEFT JOIN smis_emd_pesanan b ON a.id_pesanan = b.id
			WHERE b.noreg_pasien = '" . $noreg_pasien . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
		");
		$pemeriksaan_cito = 0;
		if ($row != null) {
			$pemeriksaan_cito += $row->pemeriksaan_cito;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"				=> "elektromedis",
			"pemeriksaan_x_ray"		=> $pemeriksaan_x_ray,
			"pemeriksaan_ct_scan"	=> $pemeriksaan_ct_scan,
			"pemeriksaan_usg_echo"	=> $pemeriksaan_usg_echo,
			"pemeriksaan_cito"		=> $pemeriksaan_cito
		);
		echo json_encode($data);
	}
?>