<?php

require_once "elektromedis/resource/ElektromedisResource.php";
global $db;
$id=$_POST['data'];

$dbtable= new DBTable($db,"smis_emd_pesanan");
$x          = $dbtable->selectEventDel($id);
$resource   = new ElektromedisResource();

/*transaksi untuk pasien Elektromedis*/
$list   = array();
$periksa=json_decode($x->periksa,true);
$harga=json_decode($x->harga,true);
foreach($periksa as $slug=>$v){
    if($v=="0")
        continue;
    
    $biaya=$harga[$x->kelas."_".$slug];
    $nama=$resource->getNameMap($slug);
    $debet=array();
    $dk=$resource->getDebitKredit($slug);
    $debet['akun']    = $dk['d'];
    $debet['debet']   = $biaya;
    $debet['kredit']  = 0;
    $debet['ket']     = "Piutang Elektromedis - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']    = "debet-elektromedis-".$x->id;
    $list[] = $debet;
     
    $kredit=array();
    $kredit['akun']    = $dk['k'];
    $kredit['debet']   = 0;
    $kredit['kredit']  = $biaya;
    $kredit['ket']     = "Pendapatan Elektromedis - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']    = "kredit-elektromedis-".$x->id;
    $list[] = $kredit;
}

//Mapping untuk Biaya Konsul Elektromedis
if($x->biaya_konsul > 0) {
    $debet              = array();
    $debet['akun']      = getSettings($db, "elektromedis-accounting-debit-biaya-konsul", "");
    $debet['debet']     = $x->biaya_konsul;
    $debet['kredit']    = 0;
    $debet['ket']       = "Piutang Elektromedis Biaya Konsul - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']      = "debet-elektromedis-".$x->id;
    $list[] = $debet;
    
    $kredit = array();
    $kredit['akun']     = getSettings($db, "elektromedis-accounting-kredit-biaya-konsul", "");
    $kredit['debet']    = 0;
    $kredit['kredit']   = $x->biaya_konsul;
    $kredit['ket']      = "Pendapatan elektromedis Biaya Konsul - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']     = "kredit-elektromedis-".$x->id;
    $list[] = $kredit;
}

//Mapping untuk Pesanan Lain-Lain ELektromedis
$pesananlain_dbtable = new DBTable($db, "smis_emd_dpesanan_lain");
$pesananlain_dbtable->addCustomKriteria ( "id_pesanan", "='" . $id . "'" );
$pesananlain_dbtable->setShowAll ( true );
$data = $pesananlain_dbtable->view ( "", "0" );
$rows = $data ['data'];
$biaya_total = 0;
foreach ( $rows as $d ){
    //biaya layanan lain:
    $layanan_lain_row = $dbtable->get_row("
        SELECT SUM(jumlah * harga_layanan) AS 'biaya_lain'
        FROM smis_emd_dpesanan_lain
        WHERE id_pesanan = '$id' AND prop != 'del'
        GROUP BY id_pesanan");
    $biaya_lain = $layanan_lain_row->biaya_lain != null ? $layanan_lain_row->biaya_lain : 0;
    //$biaya_total = $biaya_total + $biaya_lain;
}
if($biaya_lain > 0) {
    $debet              = array();
    $debet['akun']      = getSettings($db, "elektromedis-accounting-debit-pesanan-lain", "");
    $debet['debet']     = $biaya_lain;
    $debet['kredit']    = 0;
    $debet['ket']       = "Piutang Elektromedis Pesanan Lain - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']      = "debet-elektromedis-".$x->id;
    $list[] = $debet;
    
    $kredit = array();
    $kredit['akun']     = getSettings($db, "elektromedis-accounting-kredit-pesanan-lain", "");
    $kredit['debet']    = 0;
    $kredit['kredit']   = $biaya_lain;
    $kredit['ket']      = "Pendapatan Elektromedis Pesanan Lain - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']     = "kredit-elektromedis-".$x->id;
    $list[] = $kredit;
}

//content untuk header
$header=array();
$header['tanggal']      = $x->waktu_daftar;
$header['keterangan']   = "Transaksi Elektromedis ".$x->no_lab." Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Elektromedis-".$x->id;
$header['nomor']        = "EMD-".$x->id;
$header['debet']        = $x->biaya + $x->biaya_konsul + $biaya_lain;
$header['kredit']       = $x->biaya + $x->biaya_konsul + $biaya_lain;
$header['io']           = "1";

$transaction_Elektromedis=array();
$transaction_Elektromedis['header']=$header;
$transaction_Elektromedis['content']=$list;


/*transaksi keseluruhan*/
$transaction=array();
$transaction[]=$transaction_Elektromedis;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting']=1;
$id['id']=$x->id;
$dbtable->setName("smis_emd_pesanan");
$dbtable->update($update,$id);

?>