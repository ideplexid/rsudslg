<?php
global $db;
require_once 'elektromedis/resource/ElektromedisResource.php';
$elektromedis = new ElektromedisResource();
$names = $elektromedis->list_harga;
if (isset($_POST['noreg_pasien'])) {
	$noreg = $_POST['noreg_pasien'];	
	$dbtable = new DBTable ($db, "smis_emd_pesanan");
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );
	$data = $dbtable->view ( "", "0" );
	$rows = $data ['data'];
	$result = array();
	foreach ( $rows as $d ) {
		// biaya layanan lain:
		$layanan_lain_row = $dbtable->get_row("
			SELECT SUM(jumlah * harga_layanan) AS 'biaya_lain'
			FROM smis_emd_dpesanan_lain
			WHERE id_pesanan = '$d->id' AND prop != 'del'
			GROUP BY id_pesanan");
		$biaya_lain = $layanan_lain_row->biaya_lain != null ? $layanan_lain_row->biaya_lain : 0;
		
		$periksa = json_decode($d->periksa, true);
		$harga = json_decode($d->harga, true);
		$kls = str_replace (" ", "_", $d->kelas);
		$ket = array (
			'no' => $d->no_lab,
			'dokter' => $d->nama_dokter,
			'biaya_lain' => $biaya_lain,
			'biaya_konsul' => $d->biaya_konsul,
			'kelas' => $d->kelas
		);
		
		foreach ($periksa as $p => $val) {
			if ($val == "1") {
				$ket['periksa'][$names[$p]] = $harga[$kls.'_'.$p];
			}
		}		
				
		$result[] = array(
			'id' => $d->id,
			'nama' => empty($d->no_lab) ? "Elektromedis #$d->id" : "Elektromedis $d->no_lab",	
			'waktu' => ArrayAdapter::format("date d M Y", $d->tanggal),
			'ruangan' => $d->ruangan,		
			'start' =>$d->tanggal,
			'end' => $d->tanggal,
			'biaya' => $d->biaya + $biaya_lain,
			'jumlah' => 1,
			'keterangan' => $ket,
			'debet' => getSettings($db, "elektromedis-accounting-debit-global", ""),
			'kredit' => getSettings($db, "elektromedis-accounting-kredit-global", ""),
		);
	}
		
	echo json_encode (array(
		'selesai' => "1",
		'exist' => "1",
		'reverse' => "0",
		'cara_keluar' => "Selesai",
		'jasa_pelayanan' => "1",
		'data' => array( 
			'elektromedis' => array(
				'result' => $result,
				'jasa_pelayanan' => '1'
			)
		)
	));
}

?>
