<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$dbtable = new DBTable($db, "smis_emd_pesanan");
		$rows = $dbtable->get_result("
			SELECT periksa
			FROM smis_emd_pesanan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$jaspel = 0;
		foreach ($rows as $row) {
			$periksa = json_decode($row->periksa, true);
			foreach ($periksa as $key => $value)
				if ($value == "1") {
					$id_layanan = str_replace("emd_", "", $key);
					$layanan_row = $dbtable->get_row("
						SELECT nama
						FROM smis_emd_layanan
						WHERE id = '" . $id_layanan . "'
					");
					if ($layanan_row != null) {
						$jaspel_row = $dbtable->get_row("
							SELECT jaspel
							FROM jaspel_elektromedis_2016
							WHERE layanan LIKE '" . $layanan_row->nama . "'
						");
						$jaspel += $jaspel_row->jaspel;
					}
				}
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"	=> "elektromedis",
			"jaspel" 	=> $jaspel
		);
		echo json_encode($data);
	}
?>