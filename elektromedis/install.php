<?php
	global $wpdb;

	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->extendInstall("emd");
	$install->install();
	
    require_once "elektromedis/resource/install/table/smis_emd_dpesanan_lain.php";
	require_once "elektromedis/resource/install/table/smis_emd_layanan.php";
	require_once "elektromedis/resource/install/table/smis_emd_lrangkuman.php";
	require_once "elektromedis/resource/install/table/smis_emd_lrangkuman_pertanggal.php";
	require_once "elektromedis/resource/install/table/smis_emd_pembanding.php";
	require_once "elektromedis/resource/install/table/smis_emd_pendapatan.php";
	require_once "elektromedis/resource/install/table/smis_emd_pertindakan.php";
	require_once "elektromedis/resource/install/table/smis_emd_pesanan.php";
    require_once "elektromedis/resource/install/table/smis_emd_grup.php";
	
?>