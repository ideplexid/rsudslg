<?php
class ElektromedisResponder extends DBResponder { 

    public function delete(){
        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		$result = parent::delete();
        if(getSettings($this->getDBTable()->get_db(),"elektromedis-accounting-auto-notif","0")=="1") {
            $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
        }
        return $result;
	}
    
	public function save() {
        $data = $this->postToArray ();
        $id ['id'] = $_POST ['id'];
        
		/*echo "<pre>";
        var_dump($data);
        echo "<pre>";*/
            
		if( isset($_POST['selesai']) && $_POST['selesai']=="-2"){
			unset($data['noreg_pasien']);
			unset($data['nama_pasien']);
			unset($data['nrm_pasien']);
			unset($data['jk']);
			unset($data['kelas']);
		}
		
        		
		$cur=$this->select($_POST ['id']);
		if($cur==null || $cur->selesai!="1"){
            if (isset ( $data ['periksa'] )) {
				$sklas = $data ['kelas'];
				//$sklas = strtolower ( $kelas );
				$sklas = str_replace ( " ", "_", $sklas );
				$harga = $data ['harga'];
				$data ['harga'] = $harga;
				$periksa = json_decode ( $data ['periksa'], true );
				$biaya = 0;
				if ($harga != null) {
					$dharga = json_decode ( $harga, true );
					foreach ( $periksa as $k => $v ) {
						if ($v == "1") {
							$the_key = $sklas . "_" . $k;
							if( isset($dharga [$the_key]) )
								$biaya += ($dharga [$the_key] * 1);
						}
					}
				}
				$data ['biaya'] = $biaya;
                
                /*menambahkan data biaya konsul*/
                $data ['biaya'] += $_POST['biaya_konsul'];
                /*biaya konsul*/
                
                
			}
			
			if(	isset($_POST['waktu_datang']) &&
				isset($_POST['waktu_selesai']) &&
				$_POST['waktu_datang']!="" && 
				$_POST['waktu_datang']!="0000-00-00 00:00:00" && 
				$_POST['waktu_selesai']!="" && 
				$_POST['waktu_selesai']!="0000-00-00 00:00:00"){
                    loadLibrary("smis-libs-function-time");
				$data['respontime']=minute_different($_POST['waktu_selesai'],$_POST['waktu_datang']);
			}
			
			if ($_POST ['id'] == 0 || $_POST ['id'] == "") {				
				$nrm_pasien=$_POST['nrm_pasien'];
				$query="SELECT COUNT(*) as total FROM smis_emd_pesanan WHERE nrm_pasien='".$nrm_pasien."' AND prop!='del'";
				$total=$this->dbtable->get_db()->get_var($query);
				if($total*1>0){
					$data['barulama']="1";
				}else{
					$data['barulama']="0";
				}
				
				$data['waktu_daftar']=date("Y-m-d H:i:s");
				$result = $this->dbtable->insert ( $data );
				$id ['id'] = $this->dbtable->get_inserted_id ();
				$success ['type'] = 'insert';
				$this->notifInsert();
			} else {
				if(isset($_POST['nrm_pasien'])){
                    $nrm_pasien=$_POST['nrm_pasien'];
                    $query="SELECT COUNT(*) as total FROM smis_emd_pesanan WHERE nrm_pasien='".$nrm_pasien."' 
                                AND prop!='del' 
                                AND id!='".$_POST['id']."' 
                                AND id<".$_POST['id']."";
                    $total=$this->dbtable->get_db()->get_var($query);
                    if($total*1>0){
                        $data['barulama']="1";
                    }else{
                        $data['barulama']="0";
                    }
                }				
				
				if(isset($_POST['selesai']) && $_POST['selesai']=="-2"){
					$this->notifBatal($cur);
				}
				
				/*berfungsi untuk mengurangi kapastias data agar tidak besar*/
				if(isset($_POST['selesai']) && $_POST['selesai']=="1"){
					$x=$this->dbtable->select($id);					
					$emd_hasil=json_decode($x->hasil,true);
					$emd_periksa=json_decode($x->periksa,true);
					foreach($emd_periksa as $name=>$value){
						if($value=="0" || $value==0){
							$emd_hasil[$name]="";
							unset($emd_hasil[$name]);
						}
					}
					$data['hasil']=json_encode($emd_hasil);
				}
				
				$result = $this->dbtable->update ( $data, $id );
				$success ['type'] = 'update';
			}
		}
		
		// save layanan lain:
		if (isset($_POST['layanan_lain'])) {
			$layanan_lain_dbtable = new DBTable($this->getDBTable()->get_db(),"smis_emd_dpesanan_lain");
			$id_pesanan = $id['id'];
			$layanan_lain_data = json_decode($_POST['layanan_lain'], true);
			foreach($layanan_lain_data as $lld) {
				if ($lld['cmd'] == "insert") {
					$save_data = array();
					$save_data['nama_layanan'] = $lld['nama'];
					$save_data['harga_layanan'] = $lld['harga'];
					$save_data['jumlah'] = $lld['jumlah'];
					$save_data['id_pesanan'] = $id_pesanan;
					$layanan_lain_dbtable->insert($save_data);
				} else if ($lld['cmd'] == "update") {
					$update_data = array();
					$update_data['nama_layanan'] = $lld['nama'];
					$update_data['harga_layanan'] = $lld['harga'];
					$update_data['jumlah'] = $lld['jumlah'];
					$update_data['id_pesanan'] = $id_pesanan;
					$update_id = array();
					$update_id['id'] = $lld['id'];
					$layanan_lain_dbtable->update($update_data, $update_id);
				} else if ($lld['cmd'] == "delete") {
					$del_data = array();
					$del_data['prop'] = "del";
					$del_id = array();
					$del_id['id'] = $lld['id'];
					$layanan_lain_dbtable->update($del_data, $del_id);
				}
			}
		}
		// end of save layanan lain
		
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		if ($result === false)
			$success ['success'] = 0;
            
        if(getSettings($this->db,"elektromedis-accounting-auto-notif","0")=="1") {
            $this-> synchronizeToAccounting($this->dbtable->get_db(),$success ['id'] ,"");
        }
		return $success;
	}
	
	function edit() {
		$row = parent::edit();
		// layanan lain:
		$id_pesanan = $_POST['id'];
		$layanan_lain_rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_emd_dpesanan_lain
			WHERE id_pesanan = '" . $id_pesanan . "'
		");
		$layanan_lain_html = "";
		$layanan_lain_num = 0;
		foreach($layanan_lain_rows as $llr) {
			$layanan_lain_html .= 	"<tr id='layanan_lain_" . $layanan_lain_num . "'>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_id' style='display: none;'>" . $llr->id . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_nomor'></td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_nama'>" . $llr->nama_layanan . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_harga'>" . ArrayAdapter::format("only-money Rp. ", $llr->harga_layanan) . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_jumlah'>" . $llr->jumlah . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_subtotal'>" . ArrayAdapter::format("only-money Rp. ", $llr->harga_layanan * $llr->jumlah) . "</td>" .
										"<td>" .
											"<div class='btn-group noprint'>" .
												"<a href='#' onclick='layanan_lain.edit(" . $layanan_lain_num . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" . 
													"<i class='icon-edit icon-white'></i>" .
												"</a>" .
												"<a href='#' onclick='layanan_lain.del(" . $layanan_lain_num . ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" . 
													"<i class='icon-remove icon-white'></i>" .
												"</a>" .
											"</div>" .
										"</td>" .
									"</tr>";
			$layanan_lain_num++;
		}
		$row->layanan_lain_html = $layanan_lain_html;
		$row->layanan_lain_num = $layanan_lain_num;
		return $row;
	}
	
	public function printing() {
		$id = $_POST['id'];
		$single_row = $this->dbtable->get_row("
			SELECT *, (biaya + biaya_lain + biaya_konsul) AS 'total_biaya'
			FROM (
				SELECT a.*, SUM(CASE WHEN b.prop = 'del' OR b.jumlah IS NULL THEN 0 ELSE b.jumlah END * CASE WHEN b.prop = 'del' OR b.harga_layanan IS NULL THEN 0 ELSE b.harga_layanan END) AS 'biaya_lain'
				FROM smis_emd_pesanan a LEFT JOIN smis_emd_dpesanan_lain b ON a.id = b.id_pesanan
				WHERE a.id = '" . $id . "'
				GROUP BY a.id
			) v
		");
		$uidata = $this->adapter->getContent((array)$single_row);
		$raw = $single_row;
		$row = $uidata[0];
		$slug=$_POST['slug'];
		$print_data="";
		
		if($slug=="print-element"){
			$print_data=$this->uitable->getPrintedElement($raw,$row); 
		}else{
			$print_data=$this->uitable->getPrinted($raw,$row,$slug);
		}
		return $print_data;
	}
	
	private function notifInsert(){
		global $notification;
		$slug="Masuk Elektromedis";
		$tgl=ArrayAdapter::format("date d M Y H:i:s", $_POST['tanggal']." ".date("H:i:s"));
		$ruang=ArrayAdapter::format("unslug", $_POST['ruangan']);
		$message="Pasien <strong>".$_POST['nama_pasien']."</strong> Masuk Elektromedis Pada <strong>".$tgl."</strong> Dari <strong>".$ruang."</strong>";
		$key=md5($message.$slug);
		$notification->addNotification($slug, $key, $message,"elektromedis","pemeriksaan");
	}
	
	private function notifBatal($curent){
		global $notification;
		$slug="Batal Masuk Elektromedis";
		$ruang=ArrayAdapter::format("unslug", $curent->ruangan);
		$message="Pasien <strong>".$curent->nama_pasien."</strong> dengan Nomor <strong>".$curent->no_lab."</strong> Dari <strong>".$ruang."</strong> Dibatalkan";
		$key=md5($message.$slug.$curent->id);
		$notification->addNotification($slug, $key, $message,"elektromedis","pemeriksaan");
	}
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";
        $data['id_data']    = $id;
        $data['entity']     = "elektromedis";
        $data['service']    = "get_detail_accounting";
        $data['data']       = $id;
        $data['code']       = "emd-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu_daftar;
        $data['uraian']     = "Elektromedis Pasien ".$x->nama_pasien." Pada Noreg ".$x->id;;
        $data['nilai']      = $x->biaya;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }

    public function cek_tutup_tagihan(){
        if(getSettings($db,"elektromedis-aktifkan-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }

    public function command($command){
        global $db;
        if($command=="last_position"){
            $pack=new ResponsePackage();
			$service = new ServiceConsumer ( $db, "get_last_position",array("noreg_pasien"=> $_POST['noreg_pasien']),"medical_record" );
	        $service->execute();
            $content=$service->getContent();
    
            $pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
        }else if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
            $result = $this->cek_tutup_tagihan();
            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
            return parent::command($command);
        }
    }
	
}

?>