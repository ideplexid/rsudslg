<?php 

class LaporanDetailAdapter extends ArrayAdapter{
    private $map;
    
    public function setMap($map){
        $this->map=$map;
    }
    
    public function getMap($slug){
        return $this->map[$slug];
    }
    
    
    public function adapt($d){
        $one=array();
        $one['id']=$d->id;
        $one['Tanggal']=ArrayAdapter::dateFormat("date d M Y",$d->tanggal);
        $one['Nama']=$d->nama_pasien;
        $one['NRM']=ArrayAdapter::digitFormat("digit6",$d->nrm_pasien);
        $one['NRM Asli']        = $d->nrm_pasien;
        $one['No Reg']=ArrayAdapter::digitFormat("digit6",$d->noreg_pasien);
        $one['No Reg Asli']     = $d->noreg_pasien;
        $one['Nomor']=$d->no_lab;
        $one['Kelas']=ArrayAdapter::slugFormat("unslug",$d->kelas);
        $one['Jenis']=ArrayAdapter::slugFormat("unslug",$d->carabayar);
        $one['Ruangan']=ArrayAdapter::slugFormat("unslug",$d->ruangan);
        $one['Total Tagihan']=ArrayAdapter::moneyFormat("money Rp.",$d->biaya);
        $one['Total']           = $d->biaya;
        $one['Layanan']=$this->getAllLayanan($d->periksa);
        
        return $one;
    }
    
    private function getAllLayanan($periksa){
        $hasil="";
        $json=json_decode($periksa,true);
        if($json==null){
            return $hasil;
        }
        foreach($json as $x=>$y){
            if($y==1){
                $hasil.=$this->getMap($x).", ";
            }
        }
        return $hasil;
    }
    
}

?>