<?php 
require_once 'elektromedis/class/MapRuangan.php';
class ElektromedisAdapter extends SimpleAdapter{
	public function adapt($d){
		$a=parent::adapt($d);
        //$a['Nomor']=substr($d->tanggal, 2,2).self::format("only-digit8", $d->id);
        $a['Ruangan']=MapRuangan::getRealName($d->ruangan);
		return $a;
	}	
}