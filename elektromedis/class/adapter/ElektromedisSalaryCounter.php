<?php 

require_once 'elektromedis/class/adapter/ElektromedisSalaryAdapter.php';
class ElektromedisSalaryCounter extends ElektromedisSalaryAdapter{
	
	
	private $p_plain=0;
	private $p_contrast=0;
	private $p_usg=0;
	private $p_ctscan=0;
	private $p_dental=0;
	
	private $d_plain=0;
	private $d_contrast=0;
	private $d_usg=0;
	private $d_ctscan=0;
	private $d_dental=0;
	
	private $k_plain=0;
	private $k_contrast=0;
	private $k_usg=0;
	private $k_ctscan=0;
	private $k_dental=0;
	
	
	private $pp_plain=0;
	private $pp_contrast=0;
	private $pp_usg=0;
	private $pp_ctscan=0;
	private $pp_dental=0;
	
	private $pd_plain=0;
	private $pd_contrast=0;
	private $pd_usg=0;
	private $pd_ctscan=0;
	private $pd_dental=0;
	
	private $pk_plain=0;
	private $pk_contrast=0;
	private $pk_usg=0;
	private $pk_ctscan=0;
	private $pk_dental=0;
	
	
	private $plain=0;
	private $contrast=0;
	private $usg=0;
	private $ctscan=0;
	private $dental=0;
	
		
	public function __construct(){
		parent::__construct("-1",self::$TYPE_COMMUNAL);
	}
	
	public function adapt($x){
		//global $querylog;
		//$querylog->addMessage($x->id." - ".$x->nama_pasien." - ".$x->kelas);
		
		$pembagian = json_decode ( $x->pembagian, true );
		$periksa = json_decode($x->periksa,true);
		$harga = json_decode($x->harga,true);
		
		foreach(self::$CODESET as $code=>$codename){
			$nilai_asli=$this->getTotalPrice($periksa, $x->kelas, $harga, $code);
			if($nilai_asli>0 && $nilai_asli!=""){
				$this->calculateSalary($x, $pembagian, $nilai_asli,$code,$codename);
			}
		}
		
		$result=array();
		$result["nama_pengirim"]=$x->nama_dokter;
		$result["id_pengirim"]=$x->id_dokter;
		$result["nama_konsultan"]=$x->nama_konsultan;
		$result["id_konsultan"]=$x->id_konsultan;
		$result["nama_petugas"]=$x->nama_petugas;
		$result["id_petugas"]=$x->id_petugas;
		$result["nama_pasien"]=$x->nama_pasien;
		$result["noreg_pasien"]=$x->noreg_pasien;
		$result["nrm_pasien"]=$x->nrm_pasien;
		$result["kelas"]=$x->kelas;
		$result["noemd"]=$x->no_lab;
		$result["biaya"]=$x->biaya;
		$result["p_plain"]=$this->p_plain;
		$result["p_contrast"]=$this->p_contrast;
		$result["p_ctscan"]=$this->p_ctscan;
		$result["p_usg"]=$this->p_usg;
		$result["p_dental"]=$this->p_dental;
		$result["plain"]=$this->plain;
		$result["contrast"]=$this->contrast;
		$result["ctscan"]=$this->ctscan;
		$result["usg"]=$this->usg;
		$result["dental"]=$this->dental;
		$result["d_plain"]=$this->d_plain;
		$result["d_contrast"]=$this->d_contrast;
		$result["d_ctscan"]=$this->d_ctscan;
		$result["d_usg"]=$this->d_usg;
		$result["d_dental"]=$this->d_dental;
		$result["k_plain"]=$this->k_plain;
		$result["k_contrast"]=$this->k_contrast;
		$result["k_ctscan"]=$this->k_ctscan;
		$result["k_usg"]=$this->k_usg;
		$result["k_dental"]=$this->k_dental;
		$result["pp_plain"]=$this->pp_plain;
		$result["pp_contrast"]=$this->pp_contrast;
		$result["pp_ctscan"]=$this->pp_ctscan;
		$result["pp_usg"]=$this->pp_usg;
		$result["pp_dental"]=$this->pp_dental;
		$result["pd_plain"]=$this->pd_plain;
		$result["pd_contrast"]=$this->pd_contrast;
		$result["pd_ctscan"]=$this->pd_ctscan;
		$result["pd_usg"]=$this->pd_usg;
		$result["pd_dental"]=$this->pd_dental;
		$result["pk_plain"]=$this->pk_plain;
		$result["pk_contrast"]=$this->pk_contrast;
		$result["pk_ctscan"]=$this->pk_ctscan;
		$result["pk_usg"]=$this->pk_usg;
		$result["pk_dental"]=$this->pk_dental;
		$result["id"]=$x->id;
		return $result;
	}
	
	

	public function calculateSalary($d,$pembagian,$nilai_asli,$code,$codename){
		$nilai=$this->hasilCalculasi($d, $pembagian, $nilai_asli, $code, $codename);		
		if($code=="plain"){
			$this->p_plain=$nilai['petugas'];
			$this->d_plain=$nilai['dokter'];
			$this->k_plain=$nilai['konsultan'];
			$this->pp_plain=$nilai['p-petugas'];
			$this->pd_plain=$nilai['p-dokter'];
			$this->pk_plain=$nilai['p-konsultan'];
			$this->plain=$nilai_asli;
		}else if($code=="contrast"){
			$this->p_contrast=$nilai['petugas'];
			$this->d_contrast=$nilai['dokter'];
			$this->k_contrast=$nilai['konsultan'];
			$this->pp_contrast=$nilai['p-petugas'];
			$this->pd_contrast=$nilai['p-dokter'];
			$this->pk_contrast=$nilai['p-konsultan'];
			$this->contrast=$nilai_asli;
		}else if($code=="usg"){
			$this->p_usg=$nilai['petugas'];
			$this->d_usg=$nilai['dokter'];
			$this->k_usg=$nilai['konsultan'];
			$this->pp_usg=$nilai['p-petugas'];
			$this->pd_usg=$nilai['p-dokter'];
			$this->pk_usg=$nilai['p-konsultan'];
			$this->usg=$nilai_asli;
		}else if($code=="ctscan"){
			$this->p_ctscan=$nilai['petugas'];
			$this->d_ctscan=$nilai['dokter'];
			$this->k_ctscan=$nilai['konsultan'];
			$this->pp_ctscan=$nilai['p-petugas'];
			$this->pd_ctscan=$nilai['p-dokter'];
			$this->pk_ctscan=$nilai['p-konsultan'];
			$this->ctscan=$nilai_asli;
		}else if($code=="dental"){
			$this->p_dental=$nilai['petugas'];
			$this->d_dental=$nilai['dokter'];
			$this->k_dental=$nilai['konsultan'];
			$this->pp_dental=$nilai['p-petugas'];
			$this->pd_dental=$nilai['p-dokter'];
			$this->pk_dental=$nilai['p-konsultan'];
			$this->dental=$nilai_asli;
		}
	}
	
	
	public function hasilCalculasi($d,$pembagian,$nilai_asli,$code,$codename){
		$nilai=array();
		$nilai["p-dokter"]= $pembagian ['dokter-pengirim-'.$code];
		$nilai["dokter"]= $nilai["p-dokter"] * $nilai_asli / 100;
		
		$nilai["p-konsultan"]= $pembagian ['dokter-konsultan-'.$code];
		$nilai["konsultan"]= $nilai["p-konsultan"] * $nilai_asli / 100;
		
		$nilai["p-petugas"]= $pembagian ['petugas-'.$code];
		$nilai["petugas"]= $nilai["p-petugas"] * $nilai_asli / 100;
		
		return $nilai;
		
	}
	
	
	
}

?>