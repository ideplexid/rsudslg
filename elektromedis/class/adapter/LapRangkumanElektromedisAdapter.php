<?php 

class LapRangkumanElektromedisAdapter extends ArrayAdapter {
	private $jumlah_pasien_daftar;
	private $jumlah_pasien_selesai;
	private $jumlah_pasien_proses;
	private $jumlah_pasien_batal;
	private $jumlah_pasien_terbaca;
	private $jumlah_pasien_tidak_terbaca;
	private $jumlah_persen;
	private $jumlah_detail;
	private $group_laporan;


	public function __construct($list_layanan,$group_laporan) {
		$this->group_laporan=$group_laporan;
		$this->jumlah_pasien_daftar = array (
				"Nama" => "Jumlah Pasien Terdaftar",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0
		);
		$this->jumlah_pasien_selesai = array (
				"Nama" => "Jumlah Yang Selesai",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0
		);
		$this->jumlah_pasien_proses = array (
				"Nama" => "Jumlah Yang Sedang Proses",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0
		);
		$this->jumlah_pasien_batal = array (
				"Nama" => "Jumlah Yang Proses Pembatalan",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0
		);
		$this->jumlah_pasien_terbaca = array (
				"Nama" => "Jumlah Selesai Yang Terbaca",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0
		);
		$this->jumlah_pasien_tidak_terbaca = array (
				"Nama" => "Jumlah Selesai Tidak Terbaca",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0
		);
		$this->jumlah_persen = array (
				"Nama" => "Persentase (Perbandingan Terbaca dgn Yang Selesai)",
				"Total" => 0,
				"Pria" => 0,
				"Wanita" => 0
		);
		$this->jumlah_detail = array ();
		$this->initDetail ( $list_layanan );
	}
	public function initDetail($resource) {
		foreach ( $resource as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$this->jumlah_detail [$cid] = array (
						"Nama" => $cname,
						"Total" => 0,
						"Pria" => 0,
						"Wanita" => 0
				);
			}
		}
	}
	public function adapt($d) {
		$jk = $d->jk == "0" ? "Pria" : "Wanita";
		$this->jumlah_pasien_daftar [$jk] ++;
		$this->jumlah_pasien_daftar ['Total'] ++;
		if ($d->selesai == "0") {
			$this->jumlah_pasien_proses [$jk] ++;
			$this->jumlah_pasien_proses ['Total'] ++;
		}

		if ($d->selesai == "-2") {
			$this->jumlah_pasien_batal [$jk] ++;
			$this->jumlah_pasien_batal ['Total'] ++;
		}

		if ($d->selesai == "1") {
			$this->jumlah_pasien_selesai [$jk] ++;
			$this->jumlah_pasien_terbaca [$jk] ++;
			$this->jumlah_pasien_selesai ['Total'] ++;
			$this->jumlah_pasien_terbaca ['Total'] ++;
		}
		if ($d->selesai == "-1") {
			$this->jumlah_pasien_selesai [$jk] ++;
			$this->jumlah_pasien_tidak_terbaca [$jk] ++;
			$this->jumlah_pasien_selesai ['Total'] ++;
			$this->jumlah_pasien_tidak_terbaca ['Total'] ++;
		}
		$this->jumlah_persen [$jk] = ($this->jumlah_pasien_terbaca [$jk] * 100 / $this->jumlah_pasien_selesai [$jk]);
		$this->jumlah_persen ['Total'] = ($this->jumlah_pasien_terbaca ['Total'] * 100 / $this->jumlah_pasien_selesai ['Total']);

		$detail = json_decode ( $d->periksa, true );
		if ($detail == null)
			return;

		foreach ( $detail as $key => $value ) {
			if ($value == "1") {
				$this->jumlah_detail [$key] [$jk] ++;
				$this->jumlah_detail [$key] ['Total'] ++;
			}
		}
	}

	public function groupingDetail($ungroup){

		$grouped_detail=array();
		foreach ( $this->group_laporan as $name => $laporan) {
			$grouped_detail[$laporan] = array (
					"Nama" => $laporan,
					"Total" => 0,
					"Pria" => 0,
					"Wanita" => 0
			);
		}

		foreach($ungroup as $cid=>$content){
			$nama=$content['Nama'];
			$total=$content['Total'];
			$pria=$content['Pria'];
			$wanita=$content['Wanita'];
			$cnama=$this->group_laporan[$nama];
				
			$grouped_detail[$cnama]["Total"]+=$total;
			$grouped_detail[$cnama]["Pria"]+=$pria;
			$grouped_detail[$cnama]["Wanita"]+=$wanita;
		}

		return $grouped_detail;

	}

	public function getContent($data) {
		parent::getContent ( $data );
		$result = array ();
		//result header
		$result [] = $this->jumlah_pasien_daftar;
		$result [] = $this->jumlah_pasien_proses;
		$result [] = $this->jumlah_pasien_batal;
		$result [] = $this->jumlah_pasien_selesai;
		$result [] = $this->jumlah_pasien_terbaca;
		$result [] = $this->jumlah_pasien_tidak_terbaca;
		$result [] = $this->jumlah_persen;
		$result [] = array (
				"Nama" => "<strong>DETAIL PER PELAYANAN</strong>",
				"Total" => "<strong>TOTAL</strong>",
				"Pria" => "<strong>PRIA</strong>",
				"Wanita" => "<strong>WANITA</strong>"
		);

		$group_detail=$this->groupingDetail($this->jumlah_detail);
		foreach ( $group_detail as $name => $content ) {
			$result [] = $content;
		}

		$final = $this->removeZero ( $result );
		return $final;
	}


}

?>