<?php
global $db;

if(isset($_POST ['super_command']) || $_POST['super_command']!="") {
    require_once ("smis-base/smis-include-service-consumer.php");
    $super = new SuperCommand ();
    if($_POST['super_command'] == 'debet_layanan' ) {
        $d_table = new Table(array('Nomor','Nama'),"");
        $d_table->setName("debet_layanan");
        $d_table->setModel (Table::$SELECT);
        $d_adapter = new SimpleAdapter();
        $d_adapter->add("Nomor", "nomor");
        $d_adapter->add("Nama", "nama");
        $d_responder = new ServiceResponder($db, $d_table, $d_adapter, "get_account", "accounting");
        $super->addResponder ( "debet_layanan", $d_responder ); 
    }
    if($_POST['super_command'] == 'kredit_layanan') {
        $k_table = new Table(array('Nomor', 'Nama'), "");
        $k_table->setName("kredit_layanan");
        $k_table->setModel( Table::$SELECT );
        $k_adapter = new SimpleAdapter();
        $k_adapter->add("Nomor", "nomor");
        $k_adapter->add("Nama", "nama");
        $k_responder = new ServiceResponder($db, $k_table, $k_adapter, "get_account", "accounting");
        $super->addResponder ( "kredit_layanan", $k_responder );
    }
    $init = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

$head=array ('Nama','Layanan','Laporan', 'Kode Debet', 'Kode Kredit');
$uitable = new Table ( $head, "Layanan Elektromedis", NULL, true );
$uitable->setName ( "layanan" );
$uitable->setFixHeaderOnScroll(true);
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Layanan", "layanan" );
	$adapter->add ( "Laporan", "laporan" );
	$adapter->add ( "Kode Debet", "debet" );
	$adapter->add ( "Kode Kredit", "kredit" );
	
	$dbtable = new DBTable ( $db, "smis_emd_layanan" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$dbtable=new DBTable($db,"smis_emd_grup");
$dbtable->setShowAll(true);
$data=$dbtable->view("","0");

$adapter=new SelectAdapter("nama","slug");
$layanan=$adapter->getContent($data['data']);

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "laporan", "text", "Kelompok Laporan", "" );
$uitable->addModal ( "layanan", "select", "Layanan", $layanan );
//$uitable->addModal ( "debet", "text", "Kode Debet", "" );
//$uitable->addModal ( "kredit", "text", "Kode Kredit", "" );
$uitable->addModal("debet", "chooser-layanan-debet_layanan-Kode Debet", "Kode Debet", "","y",NULL,true,NULL,false);
$uitable->addModal("kredit", "chooser-layanan-kredit_layanan-Kode Kredit", "Kode Kredit", "","y",NULL,true,NULL,false);
$uitable->addModal ( "pria", "summernote", "Normal Pria", "" );
$uitable->addModal ( "wanita", "summernote", "Normal Wanita", "" );
$uitable->setFixHeaderOnScroll(true);


$modal = $uitable->getModal ();
$modal->setTitle ( "Layanan Elektromedis" );
$modal->setComponentSize(Modal::$MEDIUM);

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "elektromedis/resource/js/layanan.js",false );

?>