<?php 
require_once "smis-libs-class/MasterTemplate.php";
require_once ("smis-base/smis-include-service-consumer.php");
$head=array("No.","Nama","Slug","Persen", "Kode Debet", "Kode Kredit");
$master=new MasterTemplate($db,"smis_emd_grup","elektromedis","group_data");
$master->getUItable()->setHeader($head);
$master->getAdapter()->setUseNumber(true,"No.","back.");
$master->getAdapter()->add("Nama","nama");
$master->getAdapter()->add("Slug","slug");
$master->getAdapter()->add("Persen","persen");
$master->getAdapter()->add("Kode Debet","debet");
$master->getAdapter()->add("Kode Kredit","kredit");
$master->getUItable()->addModal("id","hidden","","")
                     ->addModal("nama","text","Nama","")
                     ->addModal("slug","text","Slug","")
                     ->addModal("persen","text","Persentase","")
                     //->addModal("debet","text","Kode Debet","")
                     //->addModal("kredit","text","Kode Kredit","")
                     ->addModal("debet", "chooser-group_data-debet_grup_layanan-Kode Debet", "Kode Debet", "","y",NULL,true,NULL,false)
                     ->addModal("kredit", "chooser-group_data-kredit_grup_layanan-Kode Kredit", "Kode Kredit", "","y",NULL,true,NULL,false);
$master->setModalTitle("Nama Grup");

if($master->getDBResponder()->isPreload() || $_POST['super_command']!=""  ){
    if($master->getDBResponder()->isPreload() || $_POST['super_command']=="debet_grup_layanan" ){
        $uitable=new Table(array('Nomor','Nama'),"");
        $uitable->setName("debet_grup_layanan");
        $uitable->setModel(Table::$SELECT);
        $adapter=new SimpleAdapter();
        $adapter->add("Nomor","nomor");
        $adapter->add("Nama","nama");
        $responder=new ServiceResponder($db, $uitable, $adapter, "get_account", "accounting");
        $master->getSuperCommand()->addResponder("debet_grup_layanan",$responder);
        $master->addSuperCommand("debet_grup_layanan",array());
        $master->addSuperCommandArray("debet_grup_layanan","debet","nomor");
    }
    if($master->getDBResponder()->isPreload() || $_POST['super_command']=="kredit_grup_layanan" ){
        $uitable=new Table(array('Nomor','Nama'),"");
        $uitable->setName("kredit_grup_layanan");
        $uitable->setModel(Table::$SELECT);
        $adapter=new SimpleAdapter();
        $adapter->add("Nomor","nomor");
        $adapter->add("Nama","nama");
        $responder=new ServiceResponder($db, $uitable, $adapter, "get_account", "accounting");
        $master->getSuperCommand()->addResponder("kredit_grup_layanan",$responder);
        $master->addSuperCommand("kredit_grup_layanan",array());
        $master->addSuperCommandArray("kredit_grup_layanan","kredit","nomor");
    }
    
}

$master->initialize();
?>