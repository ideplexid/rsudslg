<?php
global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
$presensi=new MasterSlaveTemplate($db, "smis_emd_pesanan", "elektromedis", "laporan_foto");
$presensi->setDateEnable(true);
$header=array ("No.","Tanggal",'Nama',"F Roll",'F 1824','F 2430',"F 3040","F 3535","F 3543",'RF 1824','RF 2430',"RF 3040","RF 3535","RF 3543");
$column=array("tanggal","nama_pasien","froll","f1824","f2430","f3040","f3535","f3543","f1824r","f2430r","f3040r","f3535r","f3543r");
$dbtable=$presensi->getDBtable()->setView($column);
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$dbtable->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$dbtable->setShowAll(true);
}
$btn=$presensi->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$presensi->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "date", "Dari", "")
		 ->addModal("sampai", "date", "Sampai", "");
$presensi->getForm(true,"Laporan Penggunaan Foto")
		 ->addElement("", $btn);
$presensi->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new SummaryAdapter();
$presensi->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.")
		 ->add("Tanggal", "tanggal","date d M Y")
		 ->add("Nama", "nama_pasien")
		 ->add("F Roll", "froll")
		 ->add("F 1824", "f1824")
		 ->add("F 2430", "f2430")
		 ->add("F 3040", "f3040")
		 ->add("F 3535", "f3535")
		 ->add("F 3543", "f3543")
		 ->add("RF 1824", "f1824r")
		 ->add("RF 2430", "f2430r")
		 ->add("RF 3040", "f3040r")
		 ->add("RF 3535", "f3535r")
		 ->add("RF 3543", "f3543r")
		 ->addFixValue("Tanggal","-")
		 ->addFixValue("Nama","Total")
		 ->addSummary("F Roll", "froll")
		 ->addSummary("F 1824", "f1824")
		 ->addSummary("F 2430", "f2430")
		 ->addSummary("F 3040", "f3040")
		 ->addSummary("F 3535", "f3535")
		 ->addSummary("F 3543", "f3543")
		 ->addSummary("RF 1824", "f1824r")
		 ->addSummary("RF 2430", "f2430r")
		 ->addSummary("RF 3040", "f3040r")
		 ->addSummary("RF 3535", "f3535r")
		 ->addSummary("RF 3543", "f3543r");

$presensi->addViewData("dari", "dari")
		 ->addViewData("sampai", "sampai");
$presensi->initialize();
?>


