<?php
global $db;
if(isset($_POST['super_command']) && $_POST['super_command']=="count"){
	$dbtable=new DBTable($db, "smis_emd_pertindakan");
	$dbtable->truncate();
	$dbtable=new DBTable($db, "smis_emd_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$total=$dbtable->count("");
	$resp=new ResponsePackage();
	$resp->setContent($total);
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="posting"){
	require_once 'elektromedis/resource/ElektromedisResource.php';
	$dbtable=new DBTable($db, "smis_emd_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$dbtable->setMaximum(1);
	$data=$dbtable->view("", $_POST["limit"]);
	$dlist=$data['data'];
	$onelist=$dlist[0];
	$lisperiksa=json_decode($onelist->periksa,true);
	$resr=new ElektromedisResource();
	$layanan=$resr->list_reporting;
	$periksa=json_decode($onelist->periksa,true);
	$kelas=$onelist->kelas;
	$harga=json_decode($onelist->harga,true);
	$dbtable_insert=new DBTable($db, "smis_emd_pertindakan");
	
	foreach($periksa as $slug=>$value){
		if($value=="1" || $value==1){
			$slug_harga=$kelas."_".$slug;
			$name=$layanan[$slug]['name'];
			$harga_satuan=$harga[$slug_harga];
			$cek=array();
			$cek['nama']=$name;
			$cek['kelas']=$kelas;
			$cek['harga_satuan']=$harga_satuan;
			if($dbtable_insert->is_exist($cek)){
				$query="UPDATE smis_emd_pertindakan 
						SET harga_total=harga_total+harga_satuan,total=total+1
						WHERE 	nama='".$name."' 
								AND kelas='".$kelas."' 
								AND harga_satuan='".$harga_satuan."' ";
				$db->query($query);
			}else{
				$cek['total']=1;
				$cek['harga_total']=$harga_satuan;
				$dbtable_insert->insert($cek);
			}
		}
	}	
	$resp=new ResponsePackage();
	$resp->setContent("");
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}

$header = array ("Nama","Kelas","Total Tindakan","Harga Satuan","Total Pendapatan");
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_pend_perlayanan" );
$uitable->setFooterVisible ( false );

if (isset ( $_POST ['command'] )) {
	$adapter = new SummaryADapter();
	$adapter->add("Nama", "nama");
	$adapter->add("Kelas", "kelas","unslug");
	$adapter->add("Total Tindakan", "total","number");
	$adapter->add("Harga Satuan", "harga_satuan","money Rp.");
	$adapter->add("Total Pendapatan", "harga_total","money Rp.");
	$adapter->addFixValue("Harga Satuan", "<strong>Total Pendapatan</strong>");
	$adapter->addSummary("Total Pendapatan", "harga_total","money Rp.");
	$dbtable = new DBTable ( $db, "smis_emd_pertindakan" );
	$dbtable->setOrder("nama ASC ");
	if($_POST['model']=="rata_rata"){
		$qv="SELECT nama, '-' as kelas, 
				sum(total) as total, 
				sum(harga_total) as harga_total,
				sum(harga_total)/sum(total) as harga_satuan
				FROM smis_emd_pertindakan
				GROUP BY nama
				";
		$qc="SELECT * FROM smis_emd_pertindakan ";
		$dbtable->setPreferredQuery(true, $qv, $qc);
	}else if($_POST['model']=="maksimal"){
		$qv="SELECT nama, '-' as kelas, 
				sum(total) as total, 
				max(harga_satuan)*sum(total) as harga_total,
				max(harga_satuan) as harga_satuan
				FROM smis_emd_pertindakan
				GROUP BY nama
				";
		$qc="SELECT * FROM smis_emd_pertindakan ";
		$dbtable->setPreferredQuery(true, $qv, $qc);
	}else if($_POST['model']=="minimal"){
		$qv="SELECT nama, '-' as kelas, 
				sum(total) as total, 
				min(harga_satuan)*sum(total) as harga_total,
				min(harga_satuan) as harga_satuan
				FROM smis_emd_pertindakan
				GROUP BY nama
				";
		$qc="SELECT * FROM smis_emd_pertindakan ";
		$dbtable->setPreferredQuery(true, $qv, $qc);
	}
	
	$dbtable->setShowAll(true);
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$penjelasan=array();
$detail="<strong> Detail Perkelas</strong> : akan menampilkan data detail 
		dari setiap kelas sendiri sendiri, sehingga dapat dianalisa
		tiap kelas berapa jumlahnya, dan berapa perolehanya, karena setiap kelas
		memiliki harga yang berbeda-beda sehingga pendapatanya berbeda. 
		ini adalah pendapatan Real";
$rata="<strong>Harga Rata-Rata</strong> : digunakan jika ingin menganalisa beradasarkan 
		total pendapatanya saja, untuk setiap jenis tindakan tanpa memandang dari kelas berapa.
		biasanya dipakai untuk perencanaan budget ruangan. dipakai jika ingin melihat pendapatan 
		Real per Tindakan Tanpa memandang kelas. <u>Harga Satuan</u> rata-rata diperoleh dari jumlah dari <u>Total Pendapatan</u>
		dibagi dengan jumlah dari <u>Total Tindakan</u> dari masing-masing tindakan";
$maks="<strong>Harga Maksimal</strong> : digunakan jika ingin menganalisa beradasarkan
		Harga satuan dianggap sebagai Harga Maksimal tanpa memandang kelas untuk pendapatan ruangan.
		bisa ditentukan batas atas budget yang ingin dikeluarkan. Harga Satuan dicari dari Harga Maksimal dari semua kelas.
		sedangkan <u>Total Pendapatan</u> didapatkan dari Maksimal <u>Harga Satuan</u> dikali dengan 
		jumlah <u>Total Tindakan</u> untuk masing-masing Tindakan";
$min="<strong>Harga Minimal</strong> : digunakan jika ingin menganalisa beradasarkan
		Harga satuan dianggap sebagai Harga Minimal tanpa memandang kelas 
		biasa dipakai untuk menentukan batas bawah sebuah budget. Harga Satuan dicari dari Harga Minimal dari semua kelas.
		sedangkan <u>Total Pendapatan</u> didapatkan dari Minimal <u>Harga Satuan</u> dikali dengan 
		jumlah <u>Total Tindakan</u> untuk masing-masing Tindakan";
$help=new Alert("", "Penjelasan", "");
$help->setModel(Alert::$INVERSE);
$help->addList("", $detail);
$help->addList("", $rata);
$help->addList("", $maks);
$help->addList("", $min);
$help->setRemovable(true);
echo $help->getHTML();

$option=new OptionBuilder();
$option->add("Detail Per Kelas ","per_kelas","1");
$option->add("Harga Rata-Rata","rata_rata","0");
$option->add("Harga Maksimal","maksimal","0");
$option->add("Harga Minimal","minimal","0");
$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "model", "select", "Model", $option->getContent() );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Rangkuman");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan_pend_perlayanan.laporan_pend_perlayanan_count()" );
$btngroup->addElement($button);
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_laporan_pend_perlayanan').html())" );
$btngroup->addElement($button);
$form->addElement ( "", $btngroup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("location.reload()");
$person=new LoadingBar("emd_person_bar", "");
$modal=new Modal("rload_modal", "", "Process in Progress");
$modal	->addHTML($person->getHtml(),"after")
		->addFooter($close);

echo $modal->getHtml();
echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "base-js/smis-base-loading.js");
?>
<script type="text/javascript">
var laporan_pend_perlayanan;
$(document).ready(function(){

	$("#laporan_pend_perlayanan_model").on("change",function(){
		laporan_pend_perlayanan.view();
	});
	
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	laporan_pend_perlayanan=new TableAction("laporan_pend_perlayanan","elektromedis","laporan_pend_perlayanan",column);
	laporan_pend_perlayanan.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
				model:$("#"+this.prefix+"_model").val(),
				};
		return reg_data;
	};
	laporan_pend_perlayanan.laporan_pend_perlayanan_count=function(){
		$("#rload_modal").modal("show");
		var clear_data=this.getRegulerData();
		clear_data['super_command']="count";
		$.post("",clear_data,function(res){
			var json=getContent(res);
			var num=Number(json);
			$("#rload_modal").modal("show");			
			setTimeout(laporan_pend_perlayanan.laporan_pend_perlayanan_posting(num-1,num),100);	
		});
	};

	laporan_pend_perlayanan.laporan_pend_perlayanan_posting=function(number,total){
		if(number<0) {
			$("#rload_modal").modal("hide");
			laporan_pend_perlayanan.view();
			return;
		}
		$("#emd_person_bar").sload("true","Loading... [ "+(total-number)+" / "+total+" ]",(total-number)*100/total);
		var clear_data=this.getRegulerData();
		clear_data['super_command']="posting";
		clear_data['limit']=number;
		$.post("",clear_data,function(res){
			var json=getContent(res);
			console.log(json);
			number--;
			setTimeout(laporan_pend_perlayanan.laporan_pend_perlayanan_posting(number,total),100);
		});
	};


	laporan_pend_perlayanan.view=function(){	
		var self=this;
		var view_data=this.getViewData();
		if(view_data==null){
			showWarning("Error",this.view_data_null_message);
			return;
		}
		showLoading();
		$.post('',view_data,function(res){
			var json=getContent(res);
			if(json==null) {
				
			}else{
				$("#"+self.prefix+"_list").html(json.list);
				$("#"+self.prefix+"_pagination").html(json.pagination);	
			}
			self.afterview(json);
			dismissLoading();

			$(".emd_title_h5").parents().attr("colspan","4");
			$(".emd_title_h5").parentsUntil("tr").siblings().remove();
		});
	};
	
	laporan_pend_perlayanan.view();	
});
</script>

<style type="text/css">
#table_laporan_pend_perlayanan {
	font-size: 12px;
}
</style>