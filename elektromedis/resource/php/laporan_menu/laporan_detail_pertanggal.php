<?php
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']=="count"){
	$dbtable=new DBTable($db, "smis_emd_lrangkuman_pertanggal");
	$dbtable->truncate();
	$dbtable=new DBTable($db, "smis_emd_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$total=$dbtable->count("");
	$resp=new ResponsePackage();
	$resp->setContent($total);
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="posting"){
	require_once 'elektromedis/resource/ElektromedisResource.php';
	$dbtable=new DBTable($db, "smis_emd_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$dbtable->setMaximum(1);
	$data=$dbtable->view("", $_POST["limit"]);
	$dlist=$data['data'];
	$onelist=$dlist[0];
	$lisperiksa=json_decode($onelist->periksa,true);
	$tanggal=$onelist->tanggal;
	$resr=new ElektromedisResource();
	$layanan=$resr->list_reporting;
	$lrangkuman=new DBTable($db, "smis_emd_lrangkuman_pertanggal");
	
	if($lisperiksa!=NULL){
		foreach($lisperiksa as $id=>$yn){
			if($yn=="1"){
				$c=$layanan[$id];
				$nama=$db->escaped_string($c['name']);
				$laporan=$c['laporan'];
				$grup=$c['grup'];
				$query=" INSERT INTO smis_emd_lrangkuman_pertanggal VALUES(NULL,'".$grup."','".$laporan."','".$tanggal."','".strtoupper($nama)."','1','') ON DUPLICATE KEY UPDATE total=total+1 ";
				$db->query($query);
			}
		}
	}
	$resp=new ResponsePackage();
	$resp->setContent("");
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}



$header = array ();
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_detail_pertanggal" );
$uitable->setFooterVisible ( false );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
	$adapter->add("Total", "total");
	$adapter->add("Nama", "nama");
	$adapter->setRemoveZeroEnable(true);
	$dbtable = new DBTable ( $db, "smis_emd_lrangkuman_pertanggal" );
	$dbtable->setOrder(" nama ASC ");
	$dbtable->setShowAll(true);
	
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if($dbres ->isReload()){
		$dari=$_POST['dari'];
		$sampai=$_POST['sampai'];
        loadLibrary("smis-libs-function-time");
		$date_range=createDateRangeArray($dari,$sampai);
		$month_range=createMonthDateRange($date_range);
		$year_range=createYearMonthRange($month_range);		
		$head_year="<tr><th rowspan='3'>NAMA</th>";
		
		foreach($year_range as $year=>$span){
			$head_year.="<th colspan='".$span."'>".$year."</th>";
		}
		$head_year.="<th rowspan='3'>TOTAL</th></tr>";		
		$head_month="<tr>";
		foreach($month_range as $month=>$span){
			$m=enid_month(substr($month, 5,7)*1);
			$head_month.="<th colspan='".$span."'>".$m."</th>";
		}
		$head_month.="</tr>";		
		$head_date="<tr>";
		foreach($date_range as $date){
			$d=substr($date, 8,10);
			$head_date.="<th>".$d."</th>";
		}
		$head_date.="</tr>";
		$uitable->addHeader("before", $head_year);
		$uitable->addHeader("before", $head_month);
		$uitable->addHeader("before", $head_date);
	}
	
	if($dbres->isView()){
        loadLibrary("smis-libs-function-time");
		$dari=$_POST['dari'];
		$sampai=$_POST['sampai'];
		$qv="SELECT nama ";		
		$date_range=createDateRangeArray($dari,$sampai);
		$header=array();
		$header[]="Nama";
		foreach($date_range as $date){
			$head_name=str_replace("-", "_", $date);
			$header[]=$head_name;
			$qv.=" , SUM(IF(tanggal='".$date."',total,0)) as ".$head_name." ";
			$adapter->add($head_name, $head_name);
		}
		$header[]="Total";
		$qv.=" , SUM(total) as total FROM smis_emd_lrangkuman_pertanggal ";
		$qc=" SELECT COUNT(*) as total FROM smis_emd_lrangkuman_pertanggal ";
		$uitable->setHeader($header);
		$dbtable->setPreferredQuery(true, $qv, $qc);
		$dbtable->setUseWhereforView(true);
		$dbtable->setGroupBy(true, " nama ");
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Detail Per Tanggal Rangkuman");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan_detail_pertanggal.laporan_detail_pertanggal_count()" );
$btngroup->addElement($button);
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_laporan_detail_pertanggal').html())" );
$btngroup->addElement($button);
$form->addElement ( "", $btngroup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("location.reload()");
$person=new LoadingBar("emd_person_bar", "");
$modal=new Modal("rload_modal", "", "Process in Progress");
$modal	->addHTML($person->getHtml(),"after")
		->addFooter($close);

echo $modal->getHtml();
echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "base-js/smis-base-loading.js");
?>
<script type="text/javascript">
var laporan_detail_pertanggal;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	laporan_detail_pertanggal=new TableAction("laporan_detail_pertanggal","elektromedis","laporan_detail_pertanggal",column);
	laporan_detail_pertanggal.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	laporan_detail_pertanggal.laporan_detail_pertanggal_count=function(){
		$("#rload_modal").modal("show");
		var clear_data=this.getRegulerData();
		clear_data['super_command']="count";
		$.post("",clear_data,function(res){
			var json=getContent(res);
			var num=Number(json);
			$("#rload_modal").modal("show");			
			setTimeout(laporan_detail_pertanggal.laporan_detail_pertanggal_posting(num-1,num),100);	
		});
	};

	laporan_detail_pertanggal.laporan_detail_pertanggal_posting=function(number,total){
		if(number<0) {
			$("#rload_modal").modal("hide");
			laporan_detail_pertanggal.reload();
			return;
		}
		$("#emd_person_bar").sload("true","Loading... [ "+(total-number)+" / "+total+" ]",(total-number)*100/total);
		var clear_data=this.getRegulerData();
		clear_data['super_command']="posting";
		clear_data['limit']=number;
		$.post("",clear_data,function(res){
			var json=getContent(res);
			console.log(json);
			number--;
			setTimeout(laporan_detail_pertanggal.laporan_detail_pertanggal_posting(number,total),100);
		});
	};

	laporan_detail_pertanggal.view=function(){	
		var self=this;
		var view_data=this.getViewData();
		if(view_data==null){
			showWarning("Error",this.view_data_null_message);
			return;
		}
		showLoading();
		$.post('',view_data,function(res){
			var json=getContent(res);
			if(json==null) {
				
			}else{
				$("#"+self.prefix+"_list").html(json.list);
				$("#"+self.prefix+"_pagination").html(json.pagination);	
			}
			self.afterview(json);
			dismissLoading();

			$(".emd_title_h5").parents().attr("colspan","4");
			$(".emd_title_h5").parentsUntil("tr").siblings().remove();
		});
	};
	
});
</script>

<style type="text/css">
#table_laporan_detail_pertanggal {
	font-size: 12px;
}
</style>