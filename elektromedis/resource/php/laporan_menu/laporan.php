<?php
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']=="count"){
	$dbtable=new DBTable($db, "smis_emd_lrangkuman");
	$dbtable->truncate();
	
	$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'<strong class=\"emd_title_h5\"><h4>RESUME</h4></strong>','0','0','0','00','') ";
	$db->query($query);
	$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'<strong><h4 class=\"emd_title_h5\">PER PELAYANAN</h4></strong>','0','0','0','11','') ";
	$db->query($query);
	$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'<strong><h4 class=\"emd_title_h5\">PER BAGIAN</h4></strong>','0','0','0','21','') ";
	$db->query($query);
	$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'<strong><h4 class=\"emd_title_h5\">PER JENIS LAYANAN</h4></strong>','0','0','0','31','') ";
	$db->query($query);
	
	$dbtable=new DBTable($db, "smis_emd_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$total=$dbtable->count("");
	$resp=new ResponsePackage();
	$resp->setContent($total);
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="posting"){
	require_once 'elektromedis/resource/ElektromedisResource.php';
	$dbtable=new DBTable($db, "smis_emd_pesanan");
	$dari = $_POST ['dari'];
	$sampai = $_POST ['sampai'];
	$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
	$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
	$dbtable->setMaximum(1);
	$data=$dbtable->view("", $_POST["limit"]);
	$dlist=$data['data'];
	$onelist=$dlist[0];
	$lisperiksa=json_decode($onelist->periksa,true);
	$resr=new ElektromedisResource();
	$layanan=$resr->list_reporting;
	$lrangkuman=new DBTable($db, "smis_emd_lrangkuman");
	
	$lk=$onelist->jk=="0"?1:0;
	$pr=$onelist->jk=="1"?1:0;
	
	$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'Jumlah Pasien Daftar','".$lk."','".$pr."','1','01','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
	$db->query($query);
	if($onelist->selesai=="0"){
		$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'Jumlah Pasien Proses','".$lk."','".$pr."','1','02','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}else if($onelist->selesai=="1"){
		$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'Jumlah Pasien Selesai','".$lk."','".$pr."','1','03','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
		$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'Jumlah Terbaca','".$lk."','".$pr."','1','04','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}else if($onelist->selesai=="-1"){
		$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'Jumlah Pasien Selesai','".$lk."','".$pr."','1','03','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
		$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'Jumlah Tidak Terbaca','".$lk."','".$pr."','1','05','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}else if($onelist->selesai=="-2"){
		$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'Jumlah Pasien Batal','".$lk."','".$pr."','1','06','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
		$db->query($query);
	}
	
	$ldone=array();
	$gdone=array();
	foreach($lisperiksa as $id=>$yn){
		if($yn=="1"){
			$c=$layanan[$id];
			$nama=$db->escaped_string($c['name']);
			$laporan=$c['laporan'];
			$grup=$c['grup'];
			$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'".strtoupper($nama)."','".$lk."','".$pr."','1','12','') ON DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
			$db->query($query);
			if(!in_array($laporan, $ldone)){
				$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'".strtoupper($laporan)."','".$lk."','".$pr."','1','22','') ON  DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
				$db->query($query);
				$ldone[]=$laporan;
			}
			if(!in_array($grup, $gdone)){
				$query=" INSERT INTO smis_emd_lrangkuman VALUES(NULL,'".strtoupper($grup)."','".$lk."','".$pr."','1','32','') ON  DUPLICATE KEY UPDATE pria=pria+'".$lk."', wanita=wanita+'".$pr."' , total=total+1 ";
				$db->query($query);
				$gdone[]=$grup;
			}
		}
	}
	$resp=new ResponsePackage();
	$resp->setContent("");
	$resp->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($resp->getPackage());
	return;
}



$header = array ("Nama","Pria","Wanita","Total");
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan" );
$uitable->setFooterVisible ( false );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
	$adapter->add("Nama", "nama");
	$adapter->add("Pria", "pria");
	$adapter->add("Wanita", "wanita");
	$adapter->add("Total", "total");
	$adapter->add("Jenis", "jenis");
	$dbtable = new DBTable ( $db, "smis_emd_lrangkuman" );
	$dbtable->setOrder("jenis ASC, nama ASC ");
	$dbtable->setShowAll(true);
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );

$modal = $uitable->getModal ();
$form = $modal->getForm ();
$form->setTitle("Laporan Rangkuman");

$btngroup=new ButtonGroup("");
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setAction ( "laporan.laporan_count()" );
$btngroup->addElement($button);
$button = new Button ( "", "", "" );
$button->setClass("btn-primary");
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-print" );
$button->setAction ( "smis_print($('#print_table_laporan').html())" );
$btngroup->addElement($button);
$form->addElement ( "", $btngroup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("location.reload()");
$person=new LoadingBar("emd_person_bar", "");
$modal=new Modal("rload_modal", "", "Process in Progress");
$modal	->addHTML($person->getHtml(),"after")
		->addFooter($close);

echo $modal->getHtml();
echo $form->getHtml ();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "base-js/smis-base-loading.js");
?>
<script type="text/javascript">
var laporan;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	laporan=new TableAction("laporan","elektromedis","laporan",column);
	laporan.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val()
				};
		return reg_data;
	};
	laporan.laporan_count=function(){
		$("#rload_modal").modal("show");
		var clear_data=this.getRegulerData();
		clear_data['super_command']="count";
		$.post("",clear_data,function(res){
			var json=getContent(res);
			var num=Number(json);
			$("#rload_modal").modal("show");			
			setTimeout(laporan.laporan_posting(num-1,num),100);	
		});
	};

	laporan.laporan_posting=function(number,total){
		if(number<0) {
			$("#rload_modal").modal("hide");
			laporan.view();
			return;
		}
		$("#emd_person_bar").sload("true","Loading... [ "+(total-number)+" / "+total+" ]",(total-number)*100/total);
		var clear_data=this.getRegulerData();
		clear_data['super_command']="posting";
		clear_data['limit']=number;
		$.post("",clear_data,function(res){
			var json=getContent(res);
			console.log(json);
			number--;
			setTimeout(laporan.laporan_posting(number,total),100);
		});
	};


	laporan.view=function(){	
		var self=this;
		var view_data=this.getViewData();
		if(view_data==null){
			showWarning("Error",this.view_data_null_message);
			return;
		}
		showLoading();
		$.post('',view_data,function(res){
			var json=getContent(res);
			if(json==null) {
				
			}else{
				$("#"+self.prefix+"_list").html(json.list);
				$("#"+self.prefix+"_pagination").html(json.pagination);	
			}
			self.afterview(json);
			dismissLoading();

			$(".emd_title_h5").parents().attr("colspan","4");
			$(".emd_title_h5").parentsUntil("tr").siblings().remove();
		});
	};
	
	laporan.view();	
});
</script>

<style type="text/css">
#table_laporan {
	font-size: 12px;
}
</style>