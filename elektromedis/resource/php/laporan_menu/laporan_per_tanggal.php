<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterSlaveTemplate.php';
$presensi=new MasterSlaveTemplate($db, "smis_emd_pesanan", "elektromedis", "laporan_per_tanggal");
$presensi->setDateEnable(true);

$urjip=new ServiceConsumer($db, "get_urjip",array());
$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
$urjip->execute();
$content=$urjip->getContent();
$ruangan=array();
foreach ($content as $autonomous=>$ruang){
	foreach($ruang as $nama_ruang=>$jip){
		if( $jip[$nama_ruang]!="UP"){
			$option=array();
			$option['value']=$nama_ruang;
			$option['name']=ArrayAdapter::format("unslug", $nama_ruang);
			$ruangan[]=$option;
		}
	}
}
$dbtable=$presensi->getDBtable()->setView($column);
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$qv=" SELECT tanggal ";
	
	foreach($ruangan as $one){
		$qv.=" , SUM( IF (ruangan='".$one['value']."',1,0) ) as ".$one['value']."  ";
	}
	$qv.=" , SUM( IF (ruangan='Pendaftaran' OR ruangan='',1,0) ) as lain ";
	$qv.=" , SUM(1) as total";
	$qv.=" FROM smis_emd_pesanan ";
	$qc="SELECT COUNT(*) FROM smis_emd_pesanan ";
	
	$dbtable->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$dbtable->setPreferredQuery(true, $qv, $qc);
	$dbtable->setUseWhereforView(true);
	$dbtable->setGroupBy(true, " tanggal ");
	$dbtable->setOrder(" tanggal ASC ");
	$dbtable->setShowAll(true);
}
$btn=$presensi->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$presensi->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "date", "Dari", "")
		 ->addModal("sampai", "date", "Sampai", "");
$presensi->getForm(true,"Laporan Per Tanggal")
		 ->addElement("", $btn);
$summary=new SummaryAdapter();
$presensi->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.")
		 ->add("Tanggal", "tanggal","date d M Y")
		 ->addFixValue("Tanggal","TOTAL");

$header=array ("No.","Tanggal");
foreach($ruangan as $one){
	$header[]=$one['name'];
	$summary->add($one['name'], $one['value']);
	$summary->addSummary($one['name'], $one['value']);
}
$header[]="Lain-Lain";
$summary->add("Lain-Lain", "lain");
$summary->addSummary("Lain-Lain", "lain");
$header[]="Total";
$summary->add("Total", "total");
$summary->addSummary("Total", "total");

$presensi->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new SummaryAdapter();


		 
$presensi->addViewData("dari", "dari")
		 ->addViewData("sampai", "sampai");
$presensi->initialize();
?>


