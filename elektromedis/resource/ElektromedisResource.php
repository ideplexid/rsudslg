<?php
class ElektromedisResource {
	public $list_layanan;
	public $list_default;
	public $list_name;
	public $list_harga;
	public $list_laporan;
	public $list_default_pria;
	public $list_default_wanita;
	public $list_reporting;
	public $name_map;
    public $list_grup;
    public $list_debit_kredit;
	
	
	public function __construct() {
		$this->list_name = array ();
        $this->name_map = array ();
		$this->list_harga = array ();
		$this->list_grup=array();
        $this->list_debit_kredit=array();
		$this->initListLayanan ();
        //$this->initDefault ();
		$this->initName ();
		$this->listHarga ();
	}
    
    public function getDebitKredit($slug){
        return $this->list_debit_kredit[$slug];
    }
    
    public function getNameMap($slug){
        return $this->name_map[$slug];
    }
	
	
	/**
	 * this ini used for crawling 
	 * all price of layanan without knowing 
	 * the group layanan 
	 * (remove palin foto, contrast study, ultrasonograhy and ct scan indexing)
	 */
	public function listHarga() {
		foreach ( $this->list_layanan as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$this->list_harga [$cid] = $cname;
                $this->name_map[$cid]=$cname;
			}
		}
	}
	
	/**
	 * get the list of name only without the id
	 * return array contains name only with id is number 0,1,2,3
	 */
	public function initName() {
		foreach ( $this->list_layanan as $name => $content ) {
			foreach ( $content as $cid => $cname ) {
				$this->list_name [] = $cid;
			}
		}
	}
    
    public function getGroup(){
        return $this->list_grup;
    }
	
	public function initListLayanan(){
		$all=array();
		$list_default_pria=array();
		$list_default_wanita=array();
		$list_laporan=array();
		$list_reporting=array();
		
		global $db;
		$dbtable=new DBTable($db, "smis_emd_layanan");
		$dbtable->setOrder("layanan ASC, nama ASC");
		$dbtable->setShowAll(true);
		$ldata=$dbtable->view("", "0");
		$data=$ldata['data'];
		
		foreach($data as $layanan){
			$slug="emd_".$layanan->id;
			$name=$layanan->nama;
			$grup=$layanan->layanan;
			$laporan=$layanan->laporan;
			$pria=$layanan->pria;
			$wanita=$layanan->wanita;
				
			if(!isset($all[$grup])){
				$all[$grup]=array();
			}
			$list_laporan[$name]=$laporan;
			$list_default_pria[$slug]=array("default"=>$pria,"name"=>$name);
			$list_default_wanita[$slug]=array("default"=>$wanita,"name"=>$name);
			$all[$grup][$slug]=$name;
			$list_reporting[$slug]=array("name"=>$name,"laporan"=>$laporan,"grup"=>$grup);
            $this->list_debit_kredit[$slug]=array("d"=>$layanan->debet,"k"=>$layanan->kredit);
		}
        
        $dbtable=new DBTable($db,"smis_emd_grup");
        $dbtable->setShowAll(true);
        $data=$dbtable->view("",0);
        $grup=$data['data'];
        $this->list_layanan = array();
        foreach($grup as $x){
            $this->list_layanan[$x->slug]=isset($all[$x->slug])?($all[$x->slug]):array();
            $this->list_grup[]=ArrayAdapter::slugFormat("slug",$x->slug);
        }
        $this->list_default_pria=&$list_default_pria;
		$this->list_default_wanita=&$list_default_wanita;
		$this->list_laporan=&$list_laporan;		
		$this->list_reporting=&$list_reporting;
	}
	
}

?>