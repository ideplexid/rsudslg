function diagnosa_elektromedis(){
    var a=EMD_ACTION.getRegulerData();
    a['action']='show_diagnosa';
    a['nrm_pasien']=$("#"+EMD_PREFIX+"_nrm_pasien").val();
    a['noreg_pasien']=$("#"+EMD_PREFIX+"_noreg_pasien").val();
    
    if(a['nrm_pasien']==""){
        showWarning("Pasien Kosong","Silakan Pilih Pasien dahulu !!!");
    }
    
    showLoading();
    $.post("",a,function(res){
        var json=getContent(res);
        $("#page_diagnosa_elektromedis").html(json);
        dismissLoading();
    });
}