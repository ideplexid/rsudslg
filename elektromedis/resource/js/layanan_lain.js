function LayananLainAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LayananLainAction.prototype.constructor = LayananLainAction;
LayananLainAction.prototype = new TableAction();
LayananLainAction.prototype.refresh_number = function() {
	var no = 1;
	var nor_layanan_lain = $("tbody#layanan_lain_list").children("tr").length;
	for(var i = 0; i < nor_layanan_lain; i++) {
		var dr_prefix = $("tbody#layanan_lain_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html(no++);
	}
};
LayananLainAction.prototype.show_add_form = function() {
	$("#layanan_lain_id").val("");
	$("#layanan_lain_nama").val("");
	$("#layanan_lain_harga").val("Rp. 0,00");
	$("#layanan_lain_jumlah").val("1");
	$("#layanan_lain_subtotal").val("Rp. 0,00");
	$("#layanan_lain_save_btn").removeAttr("onclick");
	$("#layanan_lain_save_btn").attr("onclick", "layanan_lain.save()");
	TableAction.prototype.show_add_form.call(this);
};
LayananLainAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama = $("#layanan_lain_nama").val();
	var harga = $("#layanan_lain_harga").val();
	var jumlah = $("#layanan_lain_jumlah").val();
	var subtotal = $("#layanan_lain_subtotal").val();
	$(".error_field").removeClass("error_field");
	if (nama == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama</strong> tidak boleh kosong";
		$("#layanan_lain_nama").addClass("error_field");
		$("#layanan_lain_nama").focus();
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#layanan_lain_jumlah").addClass("error_field");
		$("#layanan_lain_jumlah").focus();
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#layanan_lain_jumlah").addClass("error_field");
		$("#layanan_lain_jumlah").focus();
	} else if (parseFloat(jumlah) <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kurang dari atau sama dengan 0";
		$("#layanan_lain_jumlah").addClass("error_field");
		$("#layanan_lain_jumlah").focus();
	}
	if (harga == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga</strong> tidak boleh kosong";
		$("#layanan_lain_harga").addClass("error_field");
		$("#layanan_lain_harga").focus();
	}
	if (!valid) {
		$("#modal_alert_layanan_lain_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
LayananLainAction.prototype.save = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var id = $("#layanan_lain_id").val();
	var nama = $("#layanan_lain_nama").val();
	var harga = $("#layanan_lain_harga").val();
	var jumlah = $("#layanan_lain_jumlah").val();
	var subtotal = $("#layanan_lain_subtotal").val();
	$("tbody#layanan_lain_list").append(
		"<tr id='layanan_lain_" + layanan_lain_num + "'>" +
			"<td id='layanan_lain_" + layanan_lain_num + "_id' style='display: none;'>" + id + "</td>" +
			"<td id='layanan_lain_" + layanan_lain_num + "_nomor'></td>" +
			"<td id='layanan_lain_" + layanan_lain_num + "_nama'>" + nama + "</td>" +
			"<td id='layanan_lain_" + layanan_lain_num + "_harga'>" + harga + "</td>" +
			"<td id='layanan_lain_" + layanan_lain_num + "_jumlah'>" + jumlah + "</td>" +
			"<td id='layanan_lain_" + layanan_lain_num + "_subtotal'>" + subtotal + "</td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='layanan_lain.edit(" + layanan_lain_num + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='layanan_lain.del(" + layanan_lain_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	layanan_lain_num++;
	$("#layanan_lain_id").val("");
	$("#layanan_lain_nama").val("");
	$("#layanan_lain_harga").val("Rp. 0,00");
	$("#layanan_lain_jumlah").val("1");
	$("#layanan_lain_subtotal").val("Rp. 0,00");
	$("#layanan_lain_nama").focus();
	this.refresh_number();
	$(".btn").removeAttr("disabled");
};
LayananLainAction.prototype.getEditData = function(ll_num) {
	var data = new Array();
	data['id'] = $("#layanan_lain_" + ll_num + "_id").text();
	data['nama'] = $("#layanan_lain_" + ll_num + "_nama").text();
	data['harga'] = $("#layanan_lain_" + ll_num + "_harga").text();
	data['jumlah'] = $("#layanan_lain_" + ll_num + "_jumlah").text();
	data['subtotal'] = $("#layanan_lain_" + ll_num + "_subtotal").text();
	return data;
};
LayananLainAction.prototype.edit = function(ll_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getEditData(ll_num);
	$("#layanan_lain_id").val(data['id']);
	$("#layanan_lain_nama").val(data['nama']);
	$("#layanan_lain_harga").val(data['harga']);
	$("#layanan_lain_jumlah").val(data['jumlah']);
	$("#layanan_lain_subtotal").val(data['subtotal']);
	$("#layanan_lain_add_form").smodal("show");
	$("#layanan_lain_save_btn").removeAttr("onclick");
	$("#layanan_lain_save_btn").attr("onclick", "layanan_lain.update(" + ll_num + ")");
	$(".btn").removeAttr("disabled");
};
LayananLainAction.prototype.update = function(ll_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var nama = $("#layanan_lain_nama").val();
	var harga = $("#layanan_lain_harga").val();
	var jumlah = $("#layanan_lain_jumlah").val();
	var subtotal = $("#layanan_lain_subtotal").val();
	$("#layanan_lain_" + ll_num + "_nama").text(nama);
	$("#layanan_lain_" + ll_num + "_harga").text(harga);
	$("#layanan_lain_" + ll_num + "_jumlah").text(jumlah);
	$("#layanan_lain_" + ll_num + "_subtotal").text(subtotal);
	$("#layanan_lain_add_form").smodal("hide");
	$(".btn").removeAttr("disabled");
};
LayananLainAction.prototype.del = function(ll_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var id = $("#layanan_lain_" + ll_num + "_id").text();
	if (id.length == 0) {
		$("#layanan_lain_" + ll_num).remove();
	} else {
		$("#layanan_lain_" + ll_num).attr("style", "display: none;");
		$("#layanan_lain_" + ll_num).attr("class", "deleted");
	}
	this.refresh_number();
	$(".btn").removeAttr("disabled");
};