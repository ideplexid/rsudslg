/**
 * this function was create to clear hasil_print so it 
 * back to default when new system or new data inserted
 */
function clear_hasil(){
	for (var i = 0; i < EMD_LIST_HASIL.length; i++) {
		var name_list=EMD_LIST_HASIL[i];
		var dv=$("#elektromedis_hasil_"+name_list).attr("dv");
		$("#elektromedis_hasil_"+name_list).val(dv);
	}
}

/**
 * when user click on edit button this will be trigerred so it can be
 * clear and header ready for printing
 */
function fill_header_elektromedis(prefix,json){
	$("#"+prefix+"_carabayar").val(json["carabayar"]);
	$("#ph_nama").html(": "+json['nama_pasien']);
	$("#ph_nrm").html(": "+json['nrm_pasien']);
	$("#ph_tgl").html(": "+json['tanggal']);
	$("#ph_nolab").html(": "+json['no_lab']);
	$("#ph_ibu").html(": "+json['ibu']);
	$("#ph_umur").html(": "+json['umur']);
	$("#ph_datang").html(": "+getFormattedTime(json['waktu_datang']));
	$("#ph_alamat").html(": "+json['alamat']);
	$("#ph_selesai").html(": "+getFormattedTime(json['waktu_selesai']));
	$("#ph_response").html(": "+json['respontime']+" Menit");
	
}

/**
 * this function used to
 * print data in one group of elektromedis
 * usually one gruop data of elektromedis
 * have different paper.
 * */
function get_emd_print_one(code){
	var content="";
	for (var i = 0; i < EMD_LIST_PESAN.length; i++) {
		var name_list=EMD_LIST_PESAN[i];
        var name_grup=$("#elektromedis_"+name_list).data("elektromedis");
		if($("#elektromedis_"+name_list).is(':checked') && name_grup==code){
			var n_layanan = $("#elektromedis_" + name_list).attr("name").toUpperCase();
			var cty=$("#elektromedis_hasil_"+name_list).code();
			content+="<div class='print_div'>" +
						 "<font size='2'><u><b>" +
							n_layanan +
						"</b></u></font>" +
						 "<br/>" +
						cty +
					 "</div>";
		}
	}
	if(content=="") {
        return "";
    }

	var header=$("#print_header_elektromedis").html();
	var footer=$("#print_footer_elektromedis").html();
	var cara=$("#"+EMD_PREFIX+"_carabayar").val();
	var full_content="";

	if(cara=="Asuransi"){
		$("#utk_rm").hide();
		$("#utk_elektromedis").hide();
		$("#utk_dokter").hide();
		$("#utk_asuransi").show();
		header=$("#print_header_elektromedis").html();
		p_content=header+content+footer;
		full_content+="<div>"+p_content+"</div>";
	}
	
	$("#utk_rm").show();
	$("#utk_elektromedis").hide();
	$("#utk_dokter").hide();
	$("#utk_asuransi").hide();
	header=$("#print_header_elektromedis").html();
	p_content=header+content+footer;
	if(full_content!="") full_content+="<div class='pagebreak'></div>";
	full_content+="<div>"+p_content+"</div>";
	
	$("#utk_rm").hide();
	$("#utk_elektromedis").show();
	$("#utk_dokter").hide();
	$("#utk_asuransi").hide();
	header=$("#print_header_elektromedis").html();
	p_content=header+content+footer;
	if(full_content!="") full_content+="<div class='pagebreak'></div>";
	full_content+="<div>"+p_content+"</div>";

	$("#utk_rm").hide();
	$("#utk_elektromedis").hide();
	$("#utk_dokter").show();
	$("#utk_asuransi").hide();
	header=$("#print_header_elektromedis").html();
	p_content=header+content+footer;
	if(full_content!="") full_content+="<div class='pagebreak'></div>"+"<div>"+p_content+"</div>";
	
	$("#utk_rm").show();
	$("#utk_elektromedis").show();
	$("#utk_dokter").show();
	$("#utk_asuransi").show();
    console.log(full_content);
	return full_content;
}

/**
 * this used to print a tag in elektromedis
 * for creating warp up
 * tag in elektromedis film
 * */
function tag_print(){
	var nama=$("#"+EMD_PREFIX+"_nama_pasien").val();
	var nrm=$("#"+EMD_PREFIX+"_nrm_pasien").val();
	var noreg=$("#"+EMD_PREFIX+"_noreg_pasien").val();
	var noemd=$("#"+EMD_PREFIX+"_no_lab").val();

	var tprint=""+
		"<table border='1' cellpadding='5' cellspacing='0' style='font-size:20px; font-weight:800;'>"+
			"<tr> <td>Nama</td> 	<td>"+nama+"</td> </tr>"+
			"<tr> <td>NRM</td> 		<td>"+nrm+"</td> </tr>"+
			"<tr> <td>No. Reg</td> 	<td>"+noreg+"</td> </tr>"+
			"<tr> <td>No. Rad</td> 	<td>"+noemd+"</td> </tr>"+
		"</table>";		
	smis_print(tprint);	
}

/**
 * function to get current time
 * */
function js_yyyy_mm_dd_hh_mm_ss () {
  now = new Date();
  year = "" + now.getFullYear();
  month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
  day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
  hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
  minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
  second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
  return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

/**
 * print the bill for elektromedis
 * */
function cetak_kwitansi(id){
	var data=EMD_ACTION.getRegulerData();
	data['command']='print-element';
	data['slug']='print-element';
	data['id']=id;
	$.post("",data,function(res){
		var json=getContent(res);
		if(json==null) return;
		smis_print(json);
	});
}

/**
 * gunction to hide or show elektromedis result
 * */
function emd_hide_show_hasil(id_attr){
	var id=id_attr.replace("elektromedis_", "");
	if($("#"+id_attr).is(":checked")) $("."+id).show();
	else $("."+id).hide();		
}

/**
 * this function used to print 
 * elektromedis and all the result 
 * */
function emd_print(){
	 if(EMD_MODE=="pemeriksaan"){
		 var selesai=js_yyyy_mm_dd_hh_mm_ss().replace("-","/").replace("-","/");
		 $("#"+EMD_PREFIX+"_waktu_selesai").val(selesai);
		 var datang=$("#"+EMD_PREFIX+"_waktu_datang").val().replace("-","/").replace("-","/");
		 var rp=Math.ceil(Math.abs(new Date(selesai) - new Date(datang))/60000);
		 $("#ph_response").html(": "+rp+" Menit");
	 }
	
    var list=$("#EMD_LIST_GRUP").val();
    var object_list_group=$.parseJSON(list);
    
    var full_print= "";
    $.each(object_list_group, function( index, value ) {
        full_print+=get_emd_print_one(value);
    });
	smis_print_force(full_print);
	if(EMD_MODE=="pemeriksaan"){
		EMD_ACTION.save();
	}
	
}