function ElektromedisAction(name, page, action, column) {
this.initialize(name, page, action, column);
}
ElektromedisAction.prototype.constructor = ElektromedisAction;
ElektromedisAction.prototype = new TableAction();

ElektromedisAction.prototype.getRegulerData=function(){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:emd_polislug,
			noreg_pasien:emd_noreg,
			nama_pasien:emd_nama_pasien,
			nrm_pasien:emd_nrm_pasien,
			mode:EMD_MODE,
			jk:EMD_JK
			};
	return reg_data;
};

ElektromedisAction.prototype.selesai=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:emd_polislug,
			mode:EMD_MODE,
			};
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=1;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
	
};

ElektromedisAction.prototype.kembalikan=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:emd_polislug,
			mode:EMD_MODE,
			};
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=0;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
	
};

ElektromedisAction.prototype.show_add_form=function(){
    $("#"+this.prefix+"_save").show();
	this.clear();
	this.show_form();
	clear_hasil();
};

ElektromedisAction.prototype.tidak_terbaca=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:emd_polislug,
			mode:EMD_MODE,
			jk:EMD_JK
			};
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=-1;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
};


ElektromedisAction.prototype.tidak_jadi=function(id){
    showLoading();
    var cek = this.cekTutupTagihan();
    if(cek){
        var reg_data=this.getRegulerData();
        reg_data['command']='save';
        reg_data['id']=id;
        reg_data['selesai']=-2;
        var self=this;
        showLoading();
        $.post('',reg_data,function(res){
            var json=getContent(res);
            self.view();
            dismissLoading();					
        });
    }
    dismissLoading();	
};

ElektromedisAction.prototype.getSaveData=function(){
	var save_data=this.getRegulerData();
	save_data['command']="save";
	save_data['id']=$("#"+this.prefix+"_id").val();
	if(EMD_MODE=="arsip_terbaca"){
		/* pada mode arsip usr tidak diperkenankan untuk melaklukan penyimpanan data_hasil
		 * sehingga data yang ada akan dibuat sedemikian rupa , 
		 * ketika save tidak ada data yang di post
		 * kan*/
		return save_data;
	}
	for(var i=0;i<this.column.length;i++){
		var name=this.column[i];
		var typical=$("#"+this.prefix+"_"+name).attr('typical');
		var type=$("#"+this.prefix+"_"+name).attr('type');
		if(typical=="money"){
			save_data[name]=$("#"+this.prefix+"_"+name).maskMoney('unmasked')[0];
		}else if(type=="checkbox"){
			save_data[name]=$("#"+this.prefix+"_"+name).is(':checked')?1:0;
		}else{
			save_data[name]=$("#"+this.prefix+"_"+name).val();
		}
	}
	
	if(EMD_MODE=="pendaftaran" || EMD_EDIT_HASIL=="1"){
        /**
		 * ketika berada pada mode daftar
		 * yang mana dipakai oleh petugas ruangan yang melakukan inputan
		 * maka hasil tidak boleh diubah, tetapi data pemesanan boleh berubah
		 * */
		var data_pe
		var data_pesan={};
		for (var i = 0; i < EMD_LIST_PESAN.length; i++) {
			var name_list=EMD_LIST_PESAN[i];
			data_pesan[name_list]=$("#elektromedis_"+name_list).is(':checked')?1:0;
		}
        save_data['periksa']=data_pesan;

        	
	}
	
	if(EMD_MODE=="pemeriksaan" || EMD_EDIT_LAYANAN=="1"){
		/**
		 * ketika berada pada mode pemeriksaan
		 * yang mana dilakukan oleh petugas laboratory
		 * maka hasil boleh berubah tetapi pemesiksaan tidak boleh berubah
		 * */
		var data_hasil={};
		for (var i = 0; i < EMD_LIST_HASIL.length; i++) {
			var name_list=EMD_LIST_HASIL[i];
			data_hasil[name_list]=$("#elektromedis_hasil_"+name_list).code();
		}
		save_data['hasil']=data_hasil;
	}
	
	// layanan lain :
	var data_layanan_lain = {};
	var nor = $("tbody#layanan_lain_list").children("tr").length;
	for (var i = 0; i < nor; i++) {
		var layanan_prefix = $("tbody#layanan_lain_list").children("tr").eq(i).prop("id");
		var id = $("#" + layanan_prefix + "_id").text();
		var nama = $("#" + layanan_prefix + "_nama").text();
		var harga = $("#" + layanan_prefix + "_harga").text();
		var jumlah = $("#" + layanan_prefix + "_jumlah").text();
		var d_data = {};
		d_data['id'] = id;
		d_data['nama'] = nama;
		d_data['harga'] = parseFloat(harga.replace(/[^0-9-,]/g, '').replace(",", "."));
		d_data['jumlah'] = jumlah;
		if ($("#" + layanan_prefix).attr("class") == "deleted") {
			d_data['cmd'] = "delete";
		} else if (id == "") {
			d_data['cmd'] = 'insert';
		} else {
			d_data['cmd'] = 'update';
		}
		data_layanan_lain[i] = d_data;
	}
    save_data['layanan_lain'] = JSON.stringify(data_layanan_lain);
    

    

	return save_data;
};

/*CLEAR*/
ElektromedisAction.prototype.clear=function(){
	for(var i=0;i<this.column.length;i++){
		var name=this.column[i];	
			if($("#"+this.prefix+"_"+name).is(':checkbox')){
				$("#"+this.prefix+"_"+name).attr('checked', false).change();
				$("."+name).hide();
			}else if($("#"+this.prefix+"_"+name).attr('typical')=="money"){
				var val=$("#"+this.prefix+"_"+name).attr("dv");
				$("#"+this.prefix+"_"+name).maskMoney('mask',Number(val));
			}else{
				var val=$("#"+this.prefix+"_"+name).attr("dv");
				$("#"+this.prefix+"_"+name).val(val);
			}
	}
	this.enabledOnNotEdit(this.column_disabled_on_edit);	
	for (var i = 0; i < EMD_LIST_HASIL.length; i++) {
		var name_list=EMD_LIST_HASIL[i];
		$("#elektromedis_hasil_"+name_list).html("");
	}
	
	for (var i = 0; i < EMD_LIST_PESAN.length; i++) {
		var name_list=EMD_LIST_PESAN[i];
		$("#elektromedis_"+name_list).prop('checked', false).change();
		
	}
	
	//clear layanan lain:
	$("tbody#layanan_lain_list").html("");
	layanan_lain_num = 0;
};
/*END OF CLEAR*/

var EMD_EDIT_PRINT=false;
ElektromedisAction.prototype.printelement=function(id){
	EMD_EDIT_PRINT=true;
	this.edit(id);
};

ElektromedisAction.prototype.edit=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		
		for(var i=0;i<self.column.length;i++){
			var name=self.column[i];
			var typical=$("#"+self.prefix+"_"+name).attr('typical');
			var type=$("#"+self.prefix+"_"+name).attr('type');
			if(typical=="money"){
				$("#"+self.prefix+"_"+name).maskMoney('mask',Number(json[""+name]));
			}else if(type=="checkbox"){
				if(json[""+name]=="1") {
					$("#"+self.prefix+"_"+name).prop('checked', true).change();
					$("."+name).show();
				}else{
					$("#"+self.prefix+"_"+name).prop('checked', false).change();
					$("."+name).hide();
				}
			}else{
				$("#"+self.prefix+"_"+name).val(json[""+name]);
			}
		}
		
		// layanan lain:
		layanan_lain_num = json['layanan_lain_num'];
		$("tbody#layanan_lain_list").html(json['layanan_lain_html']);
		layanan_lain.refresh_number();
		
		fill_header_elektromedis(self.prefix,json);
		if(json['hasil']!=""){
			try{
				list_hasil_json=$.parseJSON(json['hasil']);
				for (var i = 0; i < EMD_LIST_HASIL.length; i++) {
					var name_list=EMD_LIST_HASIL[i];
					var value_list=list_hasil_json[name_list];
					$("#elektromedis_hasil_"+name_list).val(value_list);
				}
			}catch(e){
				console.log(e);
			}
		}try{
			if(json['periksa']!=""){
				list_periksa_json=$.parseJSON(json['periksa']);
				for (var i = 0; i < EMD_LIST_PESAN.length; i++) {
					var name_list=EMD_LIST_PESAN[i];
					var value_list=list_periksa_json[name_list];
					if(value_list=="1") $("#elektromedis_"+name_list).prop('checked', true).change();
					else $("#elektromedis_"+name_list).prop('checked', false).change();
					emd_hide_show_hasil("elektromedis_"+name_list);
				}
			}else{
				$(".elektromedis_checkbox").prop('checked', false);
				$(".div_hasil_elektromedis").hide();
			}						
		}catch(e){}					
		dismissLoading();
		self.disabledOnEdit(self.column_disabled_on_edit);
		if(EMD_EDIT_PRINT){
			emd_print();
			EMD_EDIT_PRINT=false;
		}else{
            var p=$("#lab_diagnosa_anchor").parent();
            if($(p).hasClass("active")){
                $("#lab_diagnosa_anchor").trigger("click");
            }
			self.show_form();
        }
        
       
        //if( (EMD_MODE=="pendaftaran" || EMD_EDIT_HASIL=="1") && $("#"+self.prefix+"_ruangan").val()!=emd_polislug){
        //    $("#"+self.prefix+"_save").hide();
        //}else{
        //    $("#"+self.prefix+"_save").show();
        //}
        
	});
};

ElektromedisAction.prototype.eresep=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		var noreg_pasien=json.noreg_pasien;
		var x={
			page:"elektromedis",
			action:"e_resep",
			proto_implement:"",
			proto_slug:"",
			proto_name:"",
			noreg_pasien:noreg_pasien
		};
		LoadSmisPage(x);
		dismissLoading();
	});
};


ElektromedisAction.prototype.save = function(){
    showLoading();
    var cek = this.cekTutupTagihan();
    if(cek){
        TableAction.prototype.save.call(this);        
    }
    dismissLoading();
};

ElektromedisAction.prototype.cekTutupTagihan = function(){
    var reg_data={	
        page:this.page,
        action:this.action,
        super_command:this.super_command,
        prototype_name:this.prototype_name,
        prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        polislug:emd_polislug,
        };			
    var noreg                 = $("#"+this.prefix+"_noreg_pasien").val();
    if(noreg==""){
        smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
        return;
    }
	reg_data['command']        = 'cek_tutup_tagihan';
	reg_data['noreg_pasien']  = noreg;
    
    var res = $.ajax({
        type: "POST",
        url: "",
        data:reg_data,
        async: false
    }).responseText;

    var json = getContent(res);
    if(json=="1"){
        return false;
    }else{
        return true;
    }
};