var data_petugas=emd_petugas.getViewData();	
$("#"+EMD_PREFIX+"_nama_petugas").typeahead({
	minLength:3,
	source: function (query, process) {
		data_petugas['kriteria']=$("#"+EMD_PREFIX+"_nama_petugas").val();
	 var $items = new Array;
	   $items = [""];                
	  $.ajax({
		url: '',
		type: 'POST',
		data: data_petugas,
		success: function(res) {
		  var json=getContent(res);
		  var the_data_proses=json.d.data;
		  $items = [""];      				
		  $.map(the_data_proses, function(data){
			  var group;
			  group = {
				  id: data.id,
				  name: data.nama,                            
				  toString: function () {
					  return JSON.stringify(this);
				  },
				  toLowerCase: function () {
					  return this.name.toLowerCase();
				  },
				  indexOf: function (string) {
					  return String.prototype.indexOf.apply(this.name, arguments);
				  },
				  replace: function (string) {
					  var value = '';
					  value +=  this.name;
					  if(typeof(this.level) != 'undefined') {
						  value += ' <span class="pull-right muted">';
						  value += this.level;
						  value += '</span>';
					  }
					  return String.prototype.replace.apply('<div style="padding: 0px; font-size: 12px; color:black !important;">' + value + '</div>', arguments);
				  }
			  };
			  $items.push(group);
		  });
		  
		  process($items);
		}
	  });
	},
	updater: function (item) {
		var item = JSON.parse(item);
		$("#"+EMD_PREFIX+"_nama_petugas").focus();
		$("#"+EMD_PREFIX+"_id_petugas").val(item.id);
		//pemeriksaan_
		return item.name;
	}
  });

var data_dokter=emd_dokter.getViewData();	
$("#"+EMD_PREFIX+"_nama_dokter").typeahead({
	minLength:3,
	source: function (query, process) {
	 data_dokter['kriteria']=$("#"+EMD_PREFIX+"_nama_dokter").val();
	 var $items = new Array;
	   $items = [""];                
	  $.ajax({
		url: '',
		type: 'POST',
		data: data_dokter,
		success: function(res) {
		  var json=getContent(res);
		  var the_data_proses=json.d.data;
		  $items = [""];      				
		  $.map(the_data_proses, function(data){
			  var group;
			  group = {
				  id: data.id,
				  name: data.nama,                            
				  toString: function () {
					  return JSON.stringify(this);
				  },
				  toLowerCase: function () {
					  return this.name.toLowerCase();
				  },
				  indexOf: function (string) {
					  return String.prototype.indexOf.apply(this.name, arguments);
				  },
				  replace: function (string) {
					  var value = '';
					  value +=  this.name;
					  if(typeof(this.level) != 'undefined') {
						  value += ' <span class="pull-right muted">';
						  value += this.level;
						  value += '</span>';
					  }
					  return String.prototype.replace.apply('<div style="padding: 0px; font-size: 12px; color:black !important;">' + value + '</div>', arguments);
				  }
			  };
			  $items.push(group);
		  });
		  
		  process($items);
		}
	  });
	},
	updater: function (item) {
		var item = JSON.parse(item);
		$("#"+EMD_PREFIX+"_nama_dokter").focus();
		$("#"+EMD_PREFIX+"_id_dokter").val(item.id);
		return item.name;
	}
  });