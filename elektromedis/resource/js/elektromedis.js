var EMD_ACTION;
var EMD_PREFIX=$("#EMD_PREFIX").val();
var EMD_JK=$("#EMD_JK").val();		
var EMD_LIST_HASIL=$.parseJSON($("#EMD_LIST_HASIL").val());
var EMD_LIST_PESAN=$.parseJSON($("#EMD_LIST_PESAN").val());

var EMD_MODE=$("#EMD_MODE").val();
var emd_noreg=$("#EMD_NOREG").val();
var emd_nama_pasien=$("#EMD_NAMA").val();
var emd_nrm_pasien=$("#EMD_NRM").val();
var emd_polislug=$("#EMD_POLISLUG").val();
var emd_the_page=$("#EMD_PAGE").val();
var emd_the_protoslug=$("#EMD_PROTOSLUG").val();
var emd_the_protoname=$("#EMD_PROTONAME").val();
var emd_the_protoimplement=$("#EMD_PROTOIMPLEMENT").val();			
var layanan_lain;
var layanan_lain_num;

var EMD_EDIT_HASIL=$("#EMD_EDIT_HASIL").val();	
var EMD_EDIT_LAYANAN=$("#EMD_EDIT_LAYANAN").val();	

$(document).ready(function() {
	layanan_lain = new LayananLainAction("layanan_lain",emd_the_page,EMD_PREFIX,new Array("harga", "subtotal"));
	$("#layanan_lain_harga, #layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13)
			return;
		$(".btn").removeAttr("disabled");
		$(".btn").attr("disabled", "disabled");
		var harga = $("#layanan_lain_harga").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var jumlah = $("#layanan_lain_jumlah").val();
		if (!is_numeric(jumlah))
			jumlah = 0;
		var subtotal = parseFloat(jumlah) * parseFloat(harga);
		subtotal = "Rp. " + (parseFloat(subtotal)).formatMoney("2", ".", ",");
		$("#layanan_lain_subtotal").val(subtotal);
		$(".btn").removeAttr("disabled");
	});
	
	$("#layanan_lain_nama").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_harga").focus();
		}
	});
	
	$("#layanan_lain_harga").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_jumlah").focus();
		}
	});
	
	$("#layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13) {
			$('#layanan_lain_save_btn').trigger('click');
		}
	});
	
	
				
	$(".mydate").datepicker();
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	$(document).delegate('textarea', 'keydown', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode == 9) {
			e.preventDefault();
			var start = $(this).get(0).selectionStart;
			var end = $(this).get(0).selectionEnd;
			$(this).val($(this).val().substring(0, start) + "\t" + $(this).val().substring(end));
			$(this).get(0).selectionStart =
			$(this).get(0).selectionEnd = start + 1;
		  }
		});
		
	$(".elektromedis_checkbox input").on("change",function(e){
		var id_attr=$(this).attr("id");
		emd_hide_show_hasil(id_attr);
	});
	
	clear_hasil();
	
	
	var column=new Array("id","tanggal","ruangan","kelas",
							"nama_pasien","noreg_pasien","nrm_pasien",
							"no_lab",
                            "id_marketing", "marketing", "id_pengirim", "pengirim",
							"id_dokter","nama_dokter",
							"id_konsultan","nama_konsultan",
							"nama_petugas","id_petugas",
							"jk","carabayar","umur",
							"waktu_daftar","waktu_datang",
							"waktu_selesai","waktu_ditangani",
							"uri","carabayar","froll","frollr","f1824", "f2430", 
							"f3040", "f3535","f3543", "f1824r",
							"f2430r","f3040r","f3535r","f3543r","alamat", "file",
							"sdq4335","dhf3543","dhf2636","dhf2025",
							"dvb3543","dvb3528","dvb2025","fdental",
							"sdq4335r","dhf3543r","dhf2636r","dhf2025r",
							"dvb3543r","dvb3528r","dvb2025r","fdentalr",
							"biaya_konsul"
							);
	EMD_ACTION=new ElektromedisAction(EMD_PREFIX,emd_the_page,EMD_PREFIX,column);
	EMD_ACTION.setPrototipe(emd_the_protoname,emd_the_protoslug,emd_the_protoimplement);
	EMD_ACTION.view();
    console.log(EMD_ACTION);
});