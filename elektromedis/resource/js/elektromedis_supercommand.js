var emd_dokter;
var emd_konsultan;
var emd_petugas;
var emd_pasien;
var emd_marketing;
var emd_pengirim;

emd_pasien=new TableAction("emd_pasien",emd_the_page,EMD_PREFIX,new Array());
emd_pasien.setSuperCommand("emd_pasien");
emd_pasien.setPrototipe(emd_the_protoname,emd_the_protoslug,emd_the_protoimplement);
emd_pasien.selected=function(json){
	$("#"+EMD_PREFIX+"_nama_pasien").val(json.nama_pasien);
	$("#"+EMD_PREFIX+"_nrm_pasien").val(json.nrm);
	$("#"+EMD_PREFIX+"_noreg_pasien").val(json.id);
	$("#"+EMD_PREFIX+"_jk").val(json.kelamin);
	$("#"+EMD_PREFIX+"_umur").val(json.umur);
	$("#"+EMD_PREFIX+"_ibu").val(json.ibu);
	$("#"+EMD_PREFIX+"_alamat").val(json.alamat_pasien);
    $("#"+EMD_PREFIX+"_carabayar").val(json.carabayar);
    emd_pasien.get_last_ruangan(json.id,json.jenislayanan,json.kamar_inap);
};

emd_pasien.get_last_ruangan = function(noreg,rajal,ranap){
    var cur_ruang = (ranap==null || ranap=="") ? rajal:ranap;
    var data = emd_pasien.getRegulerData();
    data['super_command'] = "";
    data['command'] = "last_position";
    data['noreg_pasien'] = noreg;
    showLoading();
    $.post("",data,function(res){
        var json = getContent(res);
        if(json!=""){
            cur_ruang = json;
        }
        $("#"+EMD_PREFIX+"_ruangan").val(cur_ruang);
        if($("#"+EMD_PREFIX+"_setting_kelas").val() == "Sesuai Kelas Pasien") {
            var data=emd_pasien.getRegulerData();
            data['action']="get_kelas_ruangan";
            data['polislug']=cur_ruang;
            $.post("",data,function(res){
                var hasil=getContent(res);
                console.log(hasil.slug_kelas+" "+cur_ruang);
                $("#"+EMD_PREFIX+"_kelas").val(hasil.slug_kelas);
            });
        }
        dismissLoading();
    });
};

emd_dokter=new TableAction("emd_dokter",emd_the_page,EMD_PREFIX,new Array());
emd_dokter.setSuperCommand("emd_dokter");
emd_dokter.setPrototipe(emd_the_protoname,emd_the_protoslug,emd_the_protoimplement);
emd_dokter.selected=function(json){
	$("#"+EMD_PREFIX+"_nama_dokter").val(json.nama);
	$("#"+EMD_PREFIX+"_id_dokter").val(json.id);
};

emd_petugas=new TableAction("emd_petugas",emd_the_page,EMD_PREFIX,new Array());
emd_petugas.setSuperCommand("emd_petugas");
emd_petugas.setPrototipe(emd_the_protoname,emd_the_protoslug,emd_the_protoimplement);
emd_petugas.selected=function(json){
	$("#"+EMD_PREFIX+"_nama_petugas").val(json.nama);
	$("#"+EMD_PREFIX+"_id_petugas").val(json.id);
};

emd_konsultan=new TableAction("emd_konsultan",emd_the_page,EMD_PREFIX,new Array());
emd_konsultan.setSuperCommand("emd_konsultan");
emd_konsultan.setPrototipe(emd_the_protoname,emd_the_protoslug,emd_the_protoimplement);
emd_konsultan.selected=function(json){
	$("#"+EMD_PREFIX+"_nama_konsultan").val(json.nama);
	$("#"+EMD_PREFIX+"_id_konsultan").val(json.id);
};

emd_marketing=new TableAction("emd_marketing",emd_the_page,EMD_PREFIX,new Array());
emd_marketing.setSuperCommand("emd_marketing");
emd_marketing.setPrototipe(emd_the_protoname,emd_the_protoslug,emd_the_protoimplement);
emd_marketing.selected=function(json){
	$("#"+EMD_PREFIX+"_marketing").val(json.nama);
	$("#"+EMD_PREFIX+"_id_marketing").val(json.id);
};

emd_pengirim=new TableAction("emd_pengirim",emd_the_page,EMD_PREFIX,new Array());
emd_pengirim.setSuperCommand("emd_pengirim");
emd_pengirim.setPrototipe(emd_the_protoname,emd_the_protoslug,emd_the_protoimplement);
emd_pengirim.selected=function(json){
	$("#"+EMD_PREFIX+"_pengirim").val(json.nama);
	$("#"+EMD_PREFIX+"_id_pengirim").val(json.id);
};