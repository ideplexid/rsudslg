
function UpdateTableHeaders() {
   $(".persist-area").each(function() {
   
       var el             = $(this),
           offset         = el.offset(),
           scrollTop      = $(window).scrollTop(),
           floatingHeader = $(".floatingHeader", this)
       
       if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
           floatingHeader.css({
            "visibility": "visible"
           });
       } else {
           floatingHeader.css({
            "visibility": "hidden"
           });      
       };
   });
}

var layanan;
var debet_layanan;
var kredit_layanan;

$(document).ready(function(){
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','nama','layanan','laporan',
                'debet', 'kredit',
                'f1824','f2430','f3040','f3535','f3543',
                "sdq4335","dhf3543","dhf2636","dhf2025",
                "dvb3543","dvb3528","dvb2025","fdental",
                'pria','wanita');
    layanan=new TableAction("layanan","elektromedis","layanan",column);
    layanan.view();	
    
    debet_layanan = new TableAction("debet_layanan", "elektromedis", "layanan", new Array(""));
	debet_layanan.setSuperCommand("debet_layanan");
    debet_layanan.selected = function(json) {
		$("#layanan_debet").val(json.nomor);
	};
    
    kredit_layanan = new TableAction("kredit_layanan", "elektromedis", "layanan", new Array(""));
    kredit_layanan.setSuperCommand("kredit_layanan");
    kredit_layanan.selected = function(json) {
        $("#layanan_kredit").val(json.nomor);
    }
});