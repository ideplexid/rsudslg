<?php
class ElektromedisModalPesanan extends HTML {
	private $all_component = array ();
	public function __construct($all_component) {
		parent::__construct ( "", "", "" );
		$this->all_component = $all_component;
	}
	public function setComponent($component) {
		$this->all_component = $component;
	}
	public function getHtml() {
		$ret = "<div class='row' style='margin-top: 0px;margin-left: 0px;'>";
		$slug = "elektromedis_";
		$all = $this->all_component;
		$set_result = "";
		$LD="";
		$LU="";
		$RD="";
		$RU="";
		 
		foreach ( $all as $name => $value ) {
			$pname=ArrayAdapter::format("slug", $name);			
			if($name=="dental"){				
				foreach ( $value as $key => $val ) {
					$check = new CheckBox ( $slug . $key, substr($val,2), false );
					$check->setClass ( "elektromedis_checkbox" );
					$check->addAtribute("data-elektromedis", $pname);
					if(startsWith($val, "LD")) {$LD.=$check->getHtml();}
					else if(startsWith($val, "LU")) {$LU.=$check->getHtml();}
					else if(startsWith($val, "RD")) {$RD.=$check->getHtml();}
					else {$RU.=$check->getHtml();}
				}				
			}else{
				$set_result .= "<div class='head-title'><strong>" . strtoupper ( $name ) . "</strong></div>";
				foreach ( $value as $key => $val ) {
					$check = new CheckBox ( $slug . $key, $val, false );
					$check->setClass ( "elektromedis_checkbox" );
					$check->addAtribute("data-elektromedis", $pname);
					$set_result .= $check->getHtml ();
				}
			}
			$ret .= "<div class='span3'>";
			$ret .= $set_result;
			$ret .= "</div>";
			$set_result = "";
		}
		$ret .= "</div>";
		return $ret;
	}
}

?>