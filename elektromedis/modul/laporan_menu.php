<?php 
$tab = new Tabulator ( "laporan_menu", "",Tabulator::$LANDSCAPE );
$tab->add ( "laporan", "Laporan Rangkuman", "", Tabulator::$TYPE_HTML,"fa fa-ambulance" );
$tab->add ( "laporan_detail", "Laporan Per Layanan", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->add ( "laporan_detail_pertanggal", "Laporan Detail per Tanggal", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "laporan_foto", "Laporan Foto", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "laporan_pend_perlayanan", "Laporan Pend perlayanan", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "laporan_per_jenis_pasien", "Laporan per Jenis Pasien", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "laporan_per_tanggal", "Laporan per Tanggal", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "laporan_sensus_harian", "Laporan Sensus Harian", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->add ( "pv_dokter", "PV Dokter", "", Tabulator::$TYPE_HTML,"fa fa-heartbeat" );
$tab->setPartialLoad(true,"elektromedis","laporan_menu","laporan_menu",true);
echo $tab->getHtml ();
?>