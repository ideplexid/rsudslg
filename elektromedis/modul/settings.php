<?php
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'elektromedis/class/settings/ElektromedisSettingsBuilder.php';
global $db;




$smis = new ElektromedisSettingsBuilder ( $db, "emd_settings", "elektromedis", "settings" );
$smis->setShowDescription ( true );
$smis->setTabulatorMode ( Tabulator::$POTRAIT );

$smis->addTabs ( "dokter", "Dokter Konsultan","fa fa-user-md" );    
if($smis->isGroup("dokter")){
    $option=new OptionBuilder();
    $option->addSingle("Stand Alone");
    $option->addSingle("Integrated");
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-konsultan-nama", "Nama Konsultan Elektromedis", "", "chooser-settings-dokter_settings_elektromedis", "Pilih Konsultan (Jangan Edit Manual) " ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-konsultan-id", "ID Konsultan Elektromedis", "", "text", "AKan Otomatis Terisi Sendiri (Jangan Edit Manual)" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-sistem-model", "Model System", $option->getContent(), "select", "System Model Elektromedis untuk persiapan keseluruhan, ketika Stand Alone berarti sendiri - sendiri, ketika Integrated berarti terintegrasi" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-activate-provit-share", "Aktifkan Model Provit Share", "0", "checkbox", "jika di centang maka perhitungan provit share akan langsung dihitung, tapi system lebih berat di ruangan." ) );
    $option=new OptionBuilder();
    $option->addSingle("show");
    $option->addSingle("hide");
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-print-header", "Print Header Model", $option->getContent(), "select", "Memilih Model Print Haader Elektromedis" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-print-header-gap-top", "Print Header Gap Top", "0", "text", "Nilai Gap Top (pixel) Margin Header Elektromedis" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-print-header-gap-bottom", "Print Header Gap Bottom", "0", "text", "Nilai Gap Bottom (pixel) Margin Header Elektromedis" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-print-header-size", "Print Header Font Size", "18", "text", "Ukuran Huruf (pixel) Margin Header Elektromedis" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-print-header-ttd", "Print Header TTD Gap", "20", "text", "Nilai Gap untuk Tanda Tangan" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-print-margin-left", "Print Margin Left", "0", "text", "Margin Kiri Print Hasil" ) );
    $smis->addItem ( "dokter", new SettingsItem ( $db, "elektromedis-print-margin-right", "Print Margin Right", "0", "text", "Margin Kanan Print Hasil" ) );
}


$smis->addTabs ( "film", "Film","fa fa-film" );
if($smis->isGroup("film")){
    $film=array();
    $film['1824']="18 x 24 cm";
    $film['2430']="24 x 30 cm";
    $film['3040']="30 x 40 cm";
    $film['3535']="35 x 35 cm";
    $film['3543']="35 x 43 cm";
    $film['roll']=" Roll ";
    foreach($film as $ukuran=>$name){
        $smis->addItem ( "film", new SettingsItem ( $db, "elektromedis-film-".$ukuran."-nama", "Nama Ukuran Film ".$name."", "", "chooser-settings-film_".$ukuran."", "Pilih ID Ukuran Film ".$name." cm" ) );
        $smis->addItem ( "film", new SettingsItem ( $db, "elektromedis-film-".$ukuran."-id", "ID Ukuran Film ".$name."", "", "text", "ID Ukuran Film ".$name." cm, AKan Otomatis Terisi Sendiri (Jangan Edit Manual)" ) );
    }
    
    
}

if($_POST['super_command']!=""){
    if($_POST['super_command']=="dokter_settings_elektromedis"){
        $dkadapter = new SimpleAdapter ();
        $dkadapter->add ( "Jabatan", "nama_jabatan" );
        $dkadapter->add ( "Nama", "nama" );
        $dkadapter->add ( "NIP", "nip" );
        $header=array ('Nama','Jabatan',"NIP");
        $dktable = new Table ( $header);
        $dktable->setName ( "dokter_settings_elektromedis" );
        $dktable->setModel ( Table::$SELECT );
        $dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );
        $smis->addSuperCommandResponder ( "dokter_settings_elektromedis", $dokter );        
    }else if(strpos($_POST['super_command'],"film_")!==false){
        $barang_adapter = new SimpleAdapter();
        $barang_adapter->add("Nomor", "id", "digit8");
        $barang_adapter->add("Nama", "nama");
        $barang_adapter->add("Jenis", "nama_jenis_barang");
        $barang_head=array("Nomor", "Nama", "Jenis");
        
        $ukuran=str_replace("film_","",$_POST['super_command']);
        $name=$film[$ukuran];
        $barang_table = new Table($barang_head);
        $barang_table->setModel(Table::$SELECT);
        $barang_table->setName("film_".$ukuran);
        $barang_service_responder = new ServiceResponder($db,$barang_table,	$barang_adapter,"get_daftar_barang_reguler");
        $smis->addSuperCommandResponder ( "film_".$ukuran, $barang_service_responder);
    }
}

$smis->addTabs ( "ui", "Tampilan" ," fa fa-desktop");
if($smis->isGroup("ui")){
    $smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-show-film", "Tampilkan Penggunaan Film", "1", "checkbox", "Tampilkan Penggunaan Film" ));
    $smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-show-simple", "Tampilkan Sederhana", "0", "checkbox", "Tampilkan Ui Sederhana" ));
    $smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-mode-get-tagihan", "Model Tagihan untuk Kasir", "0", "checkbox", "(v) Sederhana, (x) Komplek" ));
    $smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-mode-e-resep", "Aktifkan E-Resep", "0", "checkbox", "Mengaktifkan E-Resep pada Elektromedis" ));
    //$smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-show-reject", "Tampilkan Reject Photo", "0", "checkbox", "Menampilkan Penggunaan Reject Photo" ));
    $smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-show-diagnosa", "Tampilkan Diagnosa Pasien", "0", "checkbox", "Menampilkan Dagnosa Pasien" ));
    $smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-show-marketing", "Tampilkan Marketing", "0", "checkbox", "Menampilkan Marketing" ));
    $smis->addItem ( "ui", new SettingsItem ( $db, "elektromedis-aktifkan-tutup-tagihan", "Aktifkan Tutup Tagihan", "0", "checkbox", "mengaktifkan tutup tagihan" ) );

}


$smis->addTabs ( "ui_pemeriksaan","UI Pemeriksaan"," fa fa-desktop" );
if($smis->isGroup("ui_pemeriksaan")){
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_kelas",null,"manajemen");
    $serv->execute();
    $opt_kelas=new OptionBuilder();
    $opt_kelas->add("","",1);
    $jns=$serv->getContent();
    foreach($jns as $x){
        $opt_kelas->add($x['nama'],$x['slug'],0);
    }
    
    $smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "elektromedis-ui-pemeriksaan-edit-tindakan", "Edit Tindakan", "0", "checkbox", "Jika di centang maka Petugas Elektromedis Bisa Melakukan Edit Tindakan" ) );
    $smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "elektromedis-ui-pemeriksaan-add-tindakan", "Menambah Tindakan", "1", "checkbox", "Jika di Centang Petugas Elektromedis bisa menambah data pasien, jadi bisa ngisi sendiri" ) );
    $smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "elektromedis-ui-pemeriksaan-auto-archive", "Auto Archive", "0", "text", "Sistem Secara Otomatis Meng-arsipkan data Elektromedis jika Sudah Lebih dari x Hari (0 berarti tidak aktif)" ) );    
    $smis->addItem ( "ui_pemeriksaan", new SettingsItem ( $db, "elektromedis-ui-pemeriksaan-default-kelas", "Default Kelas Pasien", $opt_kelas->getContent(), "select", "Default Kelas Pasien pada Laboratory" ) );    
}

$smis->addTabs ( "ui_pendaftaran","UI Pendaftaran" ," fa fa-desktop");
if($smis->isGroup("ui_pendaftaran")){    
    $smis->addItem ( "ui_pendaftaran", new SettingsItem ( $db, "elektromedis-ui-pemeriksaan-edit-hasil", "Edit Hasil", "0", "checkbox", "Jika di centang maka Petugas Ruangan Bisa Melakukan Edit Hasil" ) );
}

$smis->addTabs ( "kwitansi", "Kwitansi"," fa fa-ticket" );
if($smis->isGroup("kwitansi")){    
    $kwitansi=new OptionBuilder();
    $kwitansi->addSingle("Reguler");
    $kwitansi->addSingle("Mini");
    $kwitansi->addSingle("Detail");
    $smis->addItem ( "kwitansi", new SettingsItem ( $db, "elektromedis-kwitansi-model", "Jenis Model Kwitansi", $kwitansi->getContent(), "select", "Model Kwitansi" ) );
    $smis->addItem ( "kwitansi", new SettingsItem ( $db, "elektromedis-kwitansi-jumlah", "Jumlah Kwitansi", "3", "text", "Jumlah Cetak Kwitansi" ) );
    $smis->addItem ( "kwitansi", new SettingsItem ( $db, "elektromedis-kwitansi-tampil-tindakan", "Tampilkan Tindakanya", "", "checkbox", "Menampilkan Tindakan" ) );
    $smis->addItem ( "kwitansi", new SettingsItem ( $db, "elektromedis-kwitansi-tampil-jenis-pasien", "Tampilkan Jenis Pasien", "1", "checkbox", "Menampilkan Jenis Pasien" ) );
    $smis->addItem ( "kwitansi", new SettingsItem ( $db, "elektromedis-kwitansi-css", "CSS Kwitansi", "", "textarea", "CSS Kwitansi" ) );    
}

$smis->addTabs ( "accounting", "Accounting"," fa fa-usd" );
if($smis->isGroup("accounting")){
    $smis->addItem ( "accounting", new SettingsItem ( $db, "elektromedis-accounting-auto-notif", "Aktifkan Setting Auto Notif ke Accounting", "0", "checkbox", "Jika Dicentang Maka Sistem Akan Menotifikasi ke Accounting Secara Otomatis" ) );
    
    $smis->addItem ( "accounting", new SettingsItem ( $db, "elektromedis-accounting-debit-biaya-konsul", "Kode Accounting untuk Debit Biaya Konsul", "", "chooser-settings-debet_biaya_konsul-Debet", "Kode Accounting untuk Debit Biaya Konsul" ) ); 
    $smis->addItem ( "accounting", new SettingsItem ( $db, "elektromedis-accounting-kredit-biaya-konsul", "Kode Accounting untuk Kredit Biaya Konsul", "", "chooser-settings-kredit_biaya_konsul-Kredit", "Kode Accounting untuk Kredit Biaya Konsul" ) );
    $smis->addSuperCommandAction("debet_biaya_konsul","kode_akun");
    $smis->addSuperCommandAction("kredit_biaya_konsul","kode_akun");
    $smis->addSuperCommandArray("debet_biaya_konsul","elektromedis-accounting-debit-biaya-konsul","nomor",true);
    $smis->addSuperCommandArray("kredit_biaya_konsul","elektromedis-accounting-kredit-biaya-konsul","nomor",true);
    
    $smis->addItem ( "accounting", new SettingsItem ( $db, "elektromedis-accounting-debit-pesanan-lain", "Kode Accounting untuk Debit Pesanan Lain", "", "chooser-settings-debet_pesanan_lain-Debet", "Kode Accounting untuk Debit Pesanan Lain" ) ); 
    $smis->addItem ( "accounting", new SettingsItem ( $db, "elektromedis-accounting-kredit-pesanan-lain", "Kode Accounting untuk Kredit Pesanan Lain", "", "chooser-settings-kredit_pesanan_lain-Kredit", "Kode Accounting untuk Kredit Pesanan Lain" ) ); 
    $smis->addSuperCommandAction("debet_pesanan_lain","kode_akun");
    $smis->addSuperCommandAction("kredit_pesanan_lain","kode_akun");
    $smis->addSuperCommandArray("debet_pesanan_lain","elektromedis-accounting-debit-pesanan-lain","nomor",true);
    $smis->addSuperCommandArray("debet_pesanan_lain","elektromedis-accounting-kredit-pesanan-lain","nomor",true);
    
    $smis->addItem ( "accounting", new SettingsItem ( $db, "elektromedis-accounting-debit-global", "Kode Accounting Debit untuk Global (Kwitansi Simple)", "", "chooser-settings-debet_global-Debet", "Kode Accounting Debit untuk Global (Kwitansi Simple)" ) ); 
    $smis->addItem ( "accounting", new SettingsItem ( $db, "elektromedis-accounting-kredit-global", "Kode Accounting Kredit untuk Global (Kwitansi Simple)", "", "chooser-settings-kredit_global-Kredit", "Kode Accounting Kredit untuk Global (Kwitansi Simple)" ) ); 
    $smis->addSuperCommandAction("debet_global","kode_akun");
    $smis->addSuperCommandAction("kredit_global","kode_akun");
    $smis->addSuperCommandArray("debet_global","elektromedis-accounting-debit-global","nomor",true);
    $smis->addSuperCommandArray("kredit_global","elektromedis-accounting-kredit-global","nomor",true);
}

$smis->setPartialLoad(true);
$response = $smis->init ();
?>