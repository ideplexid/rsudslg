<?php 
$tab = new Tabulator ( "data_induk", "",Tabulator::$POTRAIT );
// Pembanding, Evaluasi Penetapan Waktu, Pembersihan Data, Koreksi PV Dokter
$tab->add ( "pembanding_pv_elektromedis", "Pembanding", "", Tabulator::$TYPE_HTML,"fa fa-ambulance" );
$tab->add ( "evaluasi_penetapan_waktu_lap_hasil_elektromedis", "Evaluasi Penetapan Waktu", "", Tabulator::$TYPE_HTML,"fa fa-medkit" );
$tab->add ( "cleaning_data", "Pembersihan Data", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->add ( "koreksi_pv_dokter", "Koreksi PV Dokter", "", Tabulator::$TYPE_HTML,"fa fa-user-md" );
$tab->add ( "group_data", "Grup Layanan", "", Tabulator::$TYPE_HTML,"fa fa-list" );
$tab->add ( "layanan", "Layanan", "", Tabulator::$TYPE_HTML,"fa fa-list-alt" );
$tab->setPartialLoad(true,"elektromedis","data_induk","data_induk",true);
echo $tab->getHtml ();
?>