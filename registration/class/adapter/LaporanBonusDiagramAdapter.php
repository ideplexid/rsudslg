<?php 
class LaporanBonusDiagramAdapter extends ArrayAdapter{
		public function adapt($onerow){
			$array=array();
			$array['Date']=$onerow->the_time;
			$array['Dokter']=$onerow->dokter;
			$array['Mantri']=$onerow->mantri;
			$array['Bidan']=$onerow->bidan;
			$array['Puskesmas']=$onerow->puskesmas;
			$array['RS_Lain']=$onerow->rslain;
			$array['Balai_Lain']=$onerow->balailain;
			return $array;
		}
	}
?>