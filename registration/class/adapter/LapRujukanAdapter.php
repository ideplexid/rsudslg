<?php

class LapRujukanAdapter extends ArrayAdapter {
    private $arr = array();
    private $rujukan = 0;
    private $non_rujukan = 0;
    private $jumlah = 0;
    public function adapt($d) {
        $this->number++;
        $a = array();
        
        $a['no']                = $this->number;
        $a['id']                = $d->id;
        $a['tanggal']           = self::format("date d M Y", $d->tanggal);
        $a['no_kartu_peserta']  = $d->nrm;
        $a['nama_peserta']      = $d->nama_pasien;
        $a['diagnosa']          = $d->diagnosa_bpjs;
        $a['dirujuk']           = $d->rujukan;
        $a['alasan']            = $d->keterangan_bpjs;
        
        if(isset($d->rujukan) == 'Lainnya'){
            $a['rujukan'] = "v";
            $this->rujukan ++;
        }else {
            $a['non_rujukan'] = " ";
            $this->non_rujukan ++;
        }
        $this->jumlah ++;
        return $a;
    }
    
    public function getContent($data) {
        $result = parent::getContent ( $data );
		$last_data = array ();
		$last_data ['tanggal'] 			= "Jumlah";
        $last_data ['nama_peserta'] 	= $this->jumlah;
        $last_data ['dirujuk']  	    = $this->rujukan;
        $last_data ['non_rujukan']      = $this->non_rujukan;
        $result [] = $last_data;
        return $result;
    }
}

?>