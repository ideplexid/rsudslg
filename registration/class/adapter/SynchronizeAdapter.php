<?php 

class SynchronizeAdapter extends ArrayAdapter{
    
    protected $prop;
    public function setProp($prop){
        $this->prop=$prop;
    }
    
    public function adapt($data){
       
        //data karcis untuk registrasi
        $karcis=array();        
        $karcis['waktu']=ArrayAdapter::format("date d M Y", $data->waktu);
		$karcis['nama']="Karcis Pendaftaran Pasien (".strtoupper($data->jenislayanan).")";
		$karcis['biaya']=$data->lunas=="1"?"0":$data->karcis;
		$karcis['jumlah']=1;
		$karcis['id'] = $data->id;
		$karcis['start'] = $data->waktu;
		$karcis['end'] = $data->waktu;
		$karcis['dokter'] = $data->nama_dokter;
        $karcis['prop']=$this->prop;
        $karcis['grup_name'] = "registration";
		$karcis['keterangan']="Karcis ".$data->jenislayanan." Senilai ".ArrayAdapter::format("only-money Rp.", $data->karcis)." ".($data->lunas=="1"?"Telah Lunas":"Belum dibayar");
		
        //data untuk administrasi rawat inap
        $administrasi['waktu']=ArrayAdapter::format("date d M Y", $data->tanggal_inap);
        $administrasi['nama']="Administrasi Rawat Inap ".ArrayAdapter::format("unslug",$data->kamar_inap);
        $administrasi['biaya']=$data->administrasi_inap;
        $administrasi['jumlah']=1;
        $administrasi['prop']=$this->prop==""?($data->uri=="0"?"del":""):$this->prop;
        $administrasi['id'] = $data->id;
        $administrasi['grup_name'] = "administrasi";
        $administrasi['start'] = $data->tanggal_inap;
        $administrasi['end'] = $data->tanggal_inap;
        $administrasi['keterangan']="Administrasi Rawat Inap ".ArrayAdapter::format("unslug",$data->kamar_inap)." Senilai ".ArrayAdapter::format("only-money Rp.", $data->administrasi_inap);            
        
        //final result
        $result=array();
        $result[]=$karcis;
        $result[]=$administrasi;
        return $result;
    }
    
    public function getContent($x){
        $data=array();
        $data['grup_name']      = "-";
        $data['nama_pasien']    = $x->nama;
        $data['noreg_pasien']   = $x->id;
        $data['nrm_pasien']     = $x->nrm;
        $data['entity']         = "registration";
        $data['list']           = $this->adapt($x);
        return $data;
    }
    
}

?>