<?php 

/**
 * this class used for adding Badge for 
 * Reporting of Bonus Patient.
 *  
 * @author Nurul Huda
 * @copyright goblooge@gmail.com
 * @license LGPLv2
 * @since 17 June 2016
 * @version 1.2.0
 * @used		: registration/resource/php/laporan/laporan_bonus_perperujuk.php
 * @database	: smis_rgv_layananpasien
 * */

class BonusAdapter extends SimpleAdapter{
	
	private $total			= 0;
	private $total_bonus	= 0;
	
	public function adapt($d){
		$array	= parent::adapt($d);
		$ambil	= $d->keteranganbonus=="Belum Diambil" ? "-":"<strong class='badge badge-important'>".$d->ambilbonus."</strong>";
		$nilai	= $d->uri=="1" && $d->keteranganbonus!="Sudah Diambil" ? $d->besarbonus:"0";
		if($d->uri=="0"){
			$array["Keterangan"]	= "Rawat Jalan Tidak Ada Bonus";
		}else if($d->besarbonus=="0"){
			$array["Keterangan"]	= "Bonus 0";
		}else if($nilai!="0"){
			$array["Keterangan"]	= "Bonus Belum di Ambil";
		}		
		$array["Nilai"]				= self::format("money Rp.", $nilai);
		$array["Ambil"]				= $ambil;
		$this->total			   += $nilai;
		return $array;
	}
	
	public function getContent($data){
		$dt				= parent::getContent($data);
		$array['Ambil']	= "<strong>Total Belum Lunas</strong>";
		$array['Nilai']	= self::format("money Rp.", $this->total);
		$dt[]			= $array;
		return $dt;
	}
	
}

?>