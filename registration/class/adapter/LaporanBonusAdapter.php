<?php 

class LaporanBonusAdapter extends ArrayAdapter{	
		public function adapt($d){
			$array=array();
			$array['id']=$d->id;
			$array['Nama']=$d->nama;
			$array['Tanggal']=self::format("date d-M-Y",$d->waktu);
			$array['NRM']=self::format("digit8",$d->id);
			$array['Alamat']=$d->alamat;
			$array['Pasien']=$d->baru;
			$array['Rujukan']=$d->rujukan;
			$array['Perujuk']=$d->nama_rujukan;
			$array['Ambil Bonus']=$d->ambilbonus;
			$array['Nilai Bonus']=self::format("money Rp. ",$d->besarbonus);				
			$array['Bonus']=$d->bonus;
			return $array;
		}
	}

?>