<?php 

class LapURJAdapter extends SimpleAdapter{
	
	private $list_ruangan=NULL;
	private $list_total=NULL;
	public function __construct($use_number=false,$number_name="",$ruangan){
		parent::__construct($use_number,$number_name);
		$this->list_ruangan=$ruangan;
		$this->list_total=NULL;
	}
	
	public function adapt($d){	
		$a=parent::adapt($d);
		foreach($this->list_ruangan as $one){
			if($d->jenislayanan==$one['value']){
				if(!isset($this->list_total[$one['value']])) $this->list_total[$one['value']]=0;
				$this->list_total[$one['value']]++;
				$a[$one['name']]="X";
			}else{
				$a[$one['name']]="";
			}
		}
		return $a;
	}
	
	public function getContent($data){
		$con=parent::getContent($data);
		$array=array();
		$array['id']="";
		$array['No.']="";
		$array['NRM']="";
		$array['No. Reg']="";
		$array['Tanggal']="";
		$array['Jam']="";
		$array['Carabayar']="";
		$array['Nama']="";
		$array['Umur']="";
		$array['L/P']="";
		$array['Desa']="";
		$array['Kecamatan']="";		
		$array['Kabupaten']="TOTAL";
		foreach($this->list_ruangan as $one){
			if(!isset($this->list_total[$one['value']])) $this->list_total[$one['value']]=0;
			$array[$one['name']]=$this->list_total[$one['value']];
		}
		$con[]=$array;
		return $con;
	}
	
}

?>