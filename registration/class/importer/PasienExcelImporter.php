<?php 
require_once "smis-libs-class/ExcelImporter.php";
class PasienExcelImporter extends ExcelImporter{
    public function oneSetup(array &$onerow,array &$idx,array &$setup_column,int $rowindex,$actived){
        global $db;
        $dbtable_provinsi = new DBTable($db, "smis_rg_propinsi");
        $dbtable_kabupaten = new DBTable($db, "smis_rg_kabupaten");
        $dbtable_kecamatan = new DBTable($db, "smis_rg_kec");
        $dbtable_kelurahan = new DBTable($db, "smis_rg_kelurahan");
        foreach($setup_column as $index=>$name){
            $cell=$actived->getCellByColumnAndRow($index,$rowindex);
            $value=$cell->getValue();
            $value=ltrim($value);
            $value=rtrim($value); 
            
            if(PHPExcel_Shared_Date::isDateTime($cell)) {
                $value = date("Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($value)); 
            }
            
            if($name=="kelamin"){
                $value=($value=="L")?0:1;
            }
            //Memasukkan kode provinsi berdasarkan nama provinsi di file excel
            if($name=="nama_provinsi"){
                $kriteria['nama']=$value;
                $x=$dbtable_provinsi->select($kriteria);
                if($x==null){
                    $value = $value;
                    $onerow['provinsi'] = "0";
                }
                else{
                    $onerow['provinsi'] = $x->id;
                }
            }
            //Memasukkan kode kabupaten berdasarkan nama kabupaten, kode provinsi di file excel
            if($name=="nama_kabupaten"){
                $kriteria['no_prop']=$onerow['provinsi'];
                $kriteria['nama']=$value;
                $x=$dbtable_kabupaten->select($kriteria);
                if($x==null){
                    $value = $value;
                    $onerow['kabupaten'] = "0";
                }
                else{
                    $onerow['kabupaten'] = $x->id;
                }
            }
            //Memasukkan kode kecamatan berdasarkan nama kecamatan, kode kabupaten, kode provinsi di file excel
            if($name=="nama_kecamatan"){
                $kriteria['no_prop']=$onerow['provinsi'];
                $kriteria['no_kab']=$onerow['kabupaten'];
                $kriteria['nama']=$value;
                $x=$dbtable_kecamatan->select($kriteria);
                if($x==null){
                    $value = $value;
                    $onerow['kecamatan'] = "0";
                }
                else{
                    $onerow['kecamatan'] = $x->id;
                }
            }
            //Memasukkan kode kelurahan berdasarkan nama kelurahan, kode kecamatan, kode kabupaten, kode provinsi di file excel
            if($name=="nama_kelurahan"){
                $kriteria['no_prop']=$onerow['provinsi'];
                $kriteria['no_kab']=$onerow['kabupaten'];
                $kriteria['no_kec']=$onerow['kecamatan'];
                $kriteria['nama']=$value;
                $x=$dbtable_kelurahan->select($kriteria);
                if($x==null){
                    $value = $value;
                    $onerow['kelurahan'] = "0";
                }
                else{
                    $onerow['kelurahan'] = $x->id;
                }
            }
            
            $onerow[$name]=$value;
            if(in_array($name,$this->id)){
                $idx[$name]=$value;
            }
        }
    }
}

?>