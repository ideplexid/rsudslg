<?php 

/**
 * this class used to provide the registration data resource
 * 
 * @since       : 14 mei 2014
 * @version     : 5.6.9
 * @used        :   registration/class/responder/AbsensiDokterResponder.php
 *                  registration/class/responder/AbsensiDokterResponder.php
 *                  registration/class/responder/DaftarResponder.php
 *                  registration/class/responder/PasienResponder.php
 *                  registration/modul/registration_patient.php
 *                  registration/modul/settings.php
 *                  registration/resource/php/laporan/lap_uri.php
 *                  registration/resource/php/laporan/lap_urj.php
 *                  registration/resource/php/laporan/lap_urj_undetail.php
 *                  registration/resource/php/laporan/laporan_karcis.php
 *                  registration/resource/php/laporan/laporan_pasien.php
 *                  registration/resource/php/laporan/laporan_pasien_daftar.php
 *                  registration/resource/php/perubahan_pasien/plafon_bpjs.php
 *                  registration/resource/php/perubahan_pasien/recheck_pasien.php
 *                  registration/resource/php/perubahan_pasien/recheck_status.php
 *                  registration/resource/php/perubahan_pasien/ubah_carabayar.php
 *                  registration/resource/php/register_inap/register_menginap.php
 *                  registration/resource/php/registration_patient/reg_layanan.php
 *                  registration/resource/php/registration_patient/reg_patient.php
 *                  registration/service/get_jenis_patient.php
 *                  registration/service/get_laporan_karcis.php
 *                  registration/service/set_uri.php
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @database    :   smis_rgv_layananpasien
 *                  smis_rg_jenispasien
 *                  smis_rg_suku
 *                  smis_rg_bahasa
 *                  smis_rg_asuransi
 *                  smis_rg_perusahaan
 *                  smis_rg_jadwal
 *                  smis_rg_layananpasien
 * 
 * */
class RegistrationResource {
	
	public static $propinsi;
	public static $ruangan_layanan;
	public static $boolean_ruangan=false;
	public static function  getJenisPasien(){
		 $jenis_pasien	= array(
			array("name"=>"Lama","value"=>"Lama"),
			array("name"=>"Baru","value"=>"Baru")
		);
		return $jenis_pasien;
	}
	
	public static function  getJenisKelamin(){
		$jenis_kelamin=array(array("value"=>"0","name"=>"Laki-Laki"),array("value"=>"1","name"=>"Perempuan"));
		return $jenis_kelamin;
	}
	
	public static function synchronize($id){
		global $db;
		$dbtable=new DBTable($db,"smis_rgv_layananpasien");
		$layanan=$dbtable->select(array("id"=>$id));
		require_once "smis-base/smis-include-service-consumer.php";
		$serv=new ServiceConsumer($db,"update_sosial",$layanan);
		$serv->execute();
	}
	
	public static function getJenisBonus(){
		$jenis_bonus		= array(
				array("name"=>"Sudah Diambil","value"=>"Sudah Diambil"),
				array("name"=>"Belum Diambil","value"=>"Belum Diambil")
		);
		return $jenis_bonus;
		
	}
	
	public static function getJenisPerujuk(){
		$jenis_perujuk		= array(
				array("name"=>"Dokter","value"=>"Dokter"),
				array("name"=>"Mantri","value"=>"Mantri"),
				array("name"=>"Bidan","value"=>"Bidan"),
				array("name"=>"Puskesmas","value"=>"Puskesmas"),
				array("name"=>"RS Lain","value"=>"RS Lain"),
				array("name"=>"Balai Pengobatan Lain","value"=>"Balai Pengobatan Lain"),
				array("name"=>"Karyawan","value"=>"Karyawan"),
				array("name"=>"Lainya","value"=>"Lainya")
		);
		return $jenis_perujuk;
	}
	
	public static function getJenisKedatangan(){
		$jenis_kedatangan	= array(
				array("name"=>"Datang Sendiri","value"=>"Datang Sendiri"),
				array("name"=>"Rujukan","value"=>"Rujukan"),
				array("name"=>"Diterima Kembali","value"=>"Diterima Kembali"),
		);
		return $jenis_kedatangan;
	}
	
	
	public static function getJenisPembayaran(){
		global $db;
		$dbtable_asuransi=new DBTable($db,'smis_rg_jenispasien',array('id','slug'));
		$dbtable_asuransi->setShowAll(true);
		$packasuransi=$dbtable_asuransi->view("","0");
		$dataasuransi=$packasuransi['data'];
		$adapter=new SelectAdapter("nama", "slug");
		$jenis_pembayaran=$adapter->getContent($dataasuransi);
		return $jenis_pembayaran;
	}
	
	
	
	public static function getSukuBangsa($add=true){
		global $db;
		$dbtable_suku=new DBTable($db,'smis_rg_suku',array('id','nama'));
		$dbtable_suku->setOrder("nama");
		$dbtable_suku->setShowAll(true);
		$packsuku=$dbtable_suku->view("","0");
		$datasuku=$packsuku['data'];
		$adapter=new SelectAdapter("nama", "nama");
		$suku=$adapter->getContent($datasuku);
		if($add){
			$suku[]=array("name"=>"--- Semua ---","value"=>"%");
		}else{
			array_unshift($suku, array("name"=>"","value"=>"","default"=>"1"));
		}
		return $suku;
	}
	
	public static function getBahasa($add=false){
		global $db;
		$dbtable_bhs=new DBTable($db,'smis_rg_bahasa',array('id','nama'));
		$dbtable_bhs->setOrder("indoasing ASC, nama ASC");
		$dbtable_bhs->setShowAll(true);
		$packbhs=$dbtable_bhs->view("","0");
		$databhs=$packbhs['data'];
		$builder=new OptionBuilder();
		if($add){
			$builder->add("-- Semua --","%","1");
		}else{
			$builder->add("","","1");
			
		}
		foreach($databhs as $one){
			$grup=$one->indoasing=="1"?"Bahasa Asing":"Bahasa Domestik";
			$builder->add($one->nama,$one->nama,"0",$grup);
		}
		return $builder->getContent();
	}
	
	public static function getAsuransi(){
		global $db;
		$dbtable_asuransi=new DBTable($db,'smis_rg_asuransi',array('id','nama'));
		$dbtable_asuransi->setShowAll(true);
		$packasuransi=$dbtable_asuransi->view("","0");
		$dataasuransi=$packasuransi['data'];
		$adapter=new SelectAdapter("nama", "id");
		$option_asuransi=$adapter->getContent($dataasuransi);
		return $option_asuransi;
	}
	
		
	public static function getJenisPendidikan($add=false){
		$jenis_pendidikan=array(
				array("name"=>"TIDAK TAMAT SD","value"=>"TIDAK TAMAT SD")
				,array("name"=>"SD","value"=>"SD")
				,array("name"=>"SLTP","value"=>"SLTP")
				,array("name"=>"SLTA","value"=>"SLTA")
				,array("name"=>"SETINGKAT D1","value"=>"SETINGKAT D1")
				,array("name"=>"SETINGKAT AKADEMI","value"=>"SETINGKAT AKADEMI")
				,array("name"=>"SETINGKAT SARJANA","value"=>"SETINGKAT SARJANA")
				,array("name"=>"LAINNYA","value"=>"LAINNYA")
		);
		if($add){
			$jenis_pendidikan[]=array("name"=>"--- Semua ---","value"=>"%");
		}
		return $jenis_pendidikan;
	}
	
	public static function getSebutan($df="Ny."){
		$sebutan = array(
				array("name"=>"Tuan","value"=>"Tn.","default"=>$df=="Tn."?"1":"0"),
				array("name"=>"Nyonya","value"=>"Ny.","default"=>$df=="Ny."?"1":"0"),
				array("name"=>"Saudara","value"=>"Sdr.","default"=>$df=="Sdr."?"1":"0"),
				array("name"=>"Nona","value"=>"Nn.","default"=>$df=="Nn."?"1":"0"),
				array("name"=>"Anak","value"=>"An.","default"=>$df=="An."?"1":"0"),
				array("name"=>"Bayi","value"=>"By.","default"=>$df=="By."?"1":"0"),
				array("name"=>"Bayi Nyonya","value"=>"By. Ny.","default"=>$df=="By. Ny."?"1":"0")
		);
		return $sebutan;
	}

	public static function  getKelasBPJS(){
		$kls=array(
			array("name"=>"Kelas III","value"=>"3")
			,array("name"=>"Kelas II","value"=>"2")
			,array("name"=>"Kelas I","value"=>"1")
			,array("name"=>"","value"=>"","default"=>"1")
		);
		return $kls;
	}
	
	public static function getAgama($add=true){
		$pilih_agama=array(array("name"=>"Islam","value"=>"Islam"),array("name"=>"Katolik","value"=>"Katolik"),array("name"=>"Kristen","value"=>"Kristen"),array("name"=>"Hindu","value"=>"Hindu"),array("name"=>"Buddha","value"=>"Buddha"),array("name"=>"Kong Hu Chu","value"=>"Kong Hu Chu"));
		if($add){
			$pilih_agama[]=array("name"=>"--- Semua ---","value"=>"%");
		}
		return $pilih_agama;
	}
	
	public static function getJenisKerja($add=true){
		$jenis_kerja=array(
				array("name"=>"PNS","value"=>"PNS")
				,array("name"=>"SWASTA","value"=>"SWASTA")
				,array("name"=>"TNI","value"=>"TNI")
				,array("name"=>"POLRI","value"=>"POLRI")
				,array("name"=>"KARYAWAN BUMN","value"=>"KARYAWAN BUMN")
				,array("name"=>"PELAJAR","value"=>"PELAJAR")
				,array("name"=>"MAHASISWA","value"=>"MAHASISWA")
				,array("name"=>"TANI","value"=>"TANI")
				,array("name"=>"BURUH","value"=>"BURUH")
				,array("name"=>"NELAYAN","value"=>"NELAYAN")
				,array("name"=>"LAINNYA","value"=>"LAINNYA")
		);
		if($add){
			$jenis_kerja[]=array("name"=>"--- Semua ---","value"=>"%");
		}
		return $jenis_kerja;
	}
	
	public static function getMarital($add=false){
		$marital=array(
				array("name"=>"Belum Menikah","value"=>"Belum Menikah"),
				array("name"=>"Menikah","value"=>"Menikah"),
				array("name"=>"Duda","value"=>"Duda"),
				array("name"=>"Janda","value"=>"Janda")
		);
		if($add){
			$marital[]=array("name"=>"--- Semua ---","value"=>"%");
		}
		return $marital;
	}
	
	public static function getBaruLama(){
		$br=array(
				array("name"=>"Baru","value"=>"0","default"=>"1"),
				array("name"=>"Lama","value"=>"1")
		);
		return $br;
	}
	
	public static function getPatientJenis(){
		$marital=array(
				array("name"=>"Poli Terpadu","value"=>"umum"),
				array("name"=>"Poli Spesialis","value"=>"spesialis"),
				array("name"=>"","value"=>"","default"=>"1")
		);
		return $marital;
	}
	
	public static function getProvinsi(){
		if(RegistrationResource::$propinsi==null){
			global $db;
			
			$kode=getSettings($db, "registration-provinsi-id", "0");
			$propinsi_dbtable = new DBTable($db,"smis_rg_propinsi");
			$propinsi_dbtable->setShowAll(true);
			$propinsi_dbtable->setOrder(" nama ASC ");
			$data=$propinsi_dbtable->view("","0");
			$prop_adapter=new SelectAdapter("nama", "id",false,$kode);
			$propinsi=$prop_adapter->getContent($data['data']);
			RegistrationResource::$propinsi=$propinsi;
		}
		return RegistrationResource::$propinsi;
	}
	
	public static function getPerusahaan(){
		global $db;
		$dbtable_perusahaan=new DBTable($db,'smis_rg_perusahaan',array('id','nama'));
		$dbtable_perusahaan->setShowAll(true);
		$packperusahaan=$dbtable_perusahaan->view("","0");
		$dataperusahaan=$packperusahaan['data'];
		$adapter=new SelectAdapter("nama", "id");
		$option_perusahaan=$adapter->getContent($dataperusahaan);
		return $option_perusahaan;
	}
	
	public static function getRuanganDaftar(){
		global $db;
		require_once 'smis-base/smis-include-service-consumer.php';
		$content=self::getURJIP($db);
		return self::getURJFromContent($content);
	}
	
	public static function getURI($db){
		$content=self::getURJIP($db);
		return self::getURIFromContent($content);
	}
	
	public static function getURIFromContent($content){
		$ruangan						= array();
		foreach ($content as $autonomous=>$ruang){
			foreach($ruang as $nama_ruang=>$jip){
				if($jip[$nama_ruang]=="URI" || $jip[$nama_ruang]=="URJI"){
					$option				 = array();
					$option['value']	 = $nama_ruang;
					if(isset($jip['name'])){
						$option['name']	 = $jip['name'];
					}else{
						$option['name']	 = ArrayAdapter::format("unslug", $nama_ruang);
					}
					$ruangan[]			 = $option;
				}
			}
		}
		return $ruangan;
	}
	
	public static function getURJFromContent($content){
		$ruangan					 	 = array();
		foreach ($content as $autonomous=>$ruang){
			foreach($ruang as $nama_ruang=>$jip){
				if($jip[$nama_ruang]=="URJ" || $jip[$nama_ruang]=="UP" || $jip[$nama_ruang]=="URJI"){
					$option			 	 = array();
					$option['value'] 	 = $nama_ruang;
					if(isset($jip['name'])){
						$option['name']	 = $jip['name'];
					}else{
						$option['name']	 = ArrayAdapter::format("unslug", $nama_ruang);
					}
					$ruangan[]			 = $option;
				}
			}
		}	
		return $ruangan;
	}
    
    public static function getURJ($db){
		$content=self::getURJIP($db);
		return self::getURJFromContent($content);
	}
	
	public static function getURJIP($db){
		if(!self::$boolean_ruangan){
			$urjip=new ServiceConsumer($db, "get_urjip",array());
            $urjip->setCached(true,"get_urjip");
			$urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
			$urjip->execute();
			$content=$urjip->getContent();
			self::$ruangan_layanan=$content;
			self::$boolean_ruangan=true;
		}
		return self::$ruangan_layanan;
	}
	
	public static function getJadwalDokter($db){
		$btable=new DBTable($db,"smis_rg_jadwal");
		$btable->setShowAll(true);
		$c=$btable->view("","0");
		return $c['data'];
	}
	
	public static function getKodeDokter($content){
		$kode=array();
		foreach($content as $x){
			if(!in_array($x->kode,$kode)){
				$kode[]=$x->kode;
			}
		}
		return $kode;
	}
    
    public static function synchronizeToCashier($db,$id,$prop="",$set=false){
        if(getSettings($db,"cashier-real-time-tagihan","0")=="0")
            return;
            
        $dbtable=new DBTable($db,"smis_rg_layananpasien");
        if($set){
            //set when not yet updated for del especially
            $dbtable->update(array("synch"=>"0"),array("id"=>$id));
        }
        $dbtable->setName("smis_rg_vregister");
        $dbtable->setColumn(array("id","uri","nama","nrm","jenislayanan","kamar_inap","karcis","administrasi_inap","waktu","tanggal_inap","nama_dokter","lunas","prop"));
        $x=$dbtable->select($id);
        
        //preparing data
        require_once "registration/class/adapter/SynchronizeAdapter.php";
        $adapter=new SynchronizeAdapter();
        $adapter->setProp($prop);
        $data_send=$adapter->getContent($x);
        
        $serv=new ServiceConsumer($db,"proceed_receivable",$data_send,"kasir");
        $serv->execute();
        $hasil=$serv->getContent();
        if($hasil==1){
            $dbtable->setName("smis_rg_layananpasien");
            $dbtable->update(array("synch"=>"1"),array("id"=>$id));
        }
    }
    
    public static function synchronizeToAccounting($db,$id,$is_del=""){
        $sync = getSettings($db,"reg-auto-sync-accounting","0");
        if($sync =="0"){
            return;
        }
        if($sync=="1"){
            self::synchronizeToAccountingAcruelBase($db,$id,$is_del);
        }else if($sync=="2"){
            self::synchronizeToAccountingCashBase($db,$id,$is_del);
        }
    }
    
    public static function synchronizeToAccountingCashBase($db,$id,$is_del=""){
        $dbtable = new DBTable($db,"smis_rg_vregister");
        $x       = $dbtable->selectEventDel($id);
        $grup_data=array();
        $grup_data['noreg_pasien']  = $x->id;
        $grup_data['carabayar']     = $x->carabayar;
        $grup_data['nama_pasien']   = $x->nama;
        $grup_data['nrm_pasien']    = $x->nrm;
        $grup_data['tgl_masuk']     = $x->waktu;
        $grup_data['tgl_pulang']    = $x->tanggal_pulang;
        $grup_data['tgl_notif']     = $x->tanggal_post;
        
        $data=array();
        $data['jenis_akun']         = "transaction";
        $data['jenis_data']         = "penjualan";
        $data['jenis_transaksi']    = $x->tanggal_post;
        $data['id_data']            = $id;
        $data['entity']             = "kasir";
        $data['service']            = "get_detail_accounting_pasien_cash_base";
        $data['data']               = json_encode($grup_data);
        $data['code']               = "reg-".$id;
        $data['operation']          = $is_del==""?$x->prop:$is_del;
        $data['tanggal']            = $x->waktu;
        $data['uraian']             = " Pasien ".$x->nama." Pada Noreg ".$x->id;
        $data['nilai']              = ($x->total_tagihan*1==0)?($x->administrasi_inap + $x->karcis):$x->total_tagihan;
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $serv->getContent();
    }
    
    public function synchronizeToAccountingAcruelBase($db,$id,$is_del=""){
        $dbtable = new DBTable($db,"smis_rg_vregister");
        $x       = $dbtable->selectEventDel($id);
        
        $data=array();
        $data['jenis_akun']         = "transaction";
        $data['jenis_data']         = "penjualan";
        $data['jenis_transaksi']    = $x->waktu;
        $data['id_data']            = $id;
        $data['entity']             = "kasir";
        $data['service']            = "get_detail_accounting";
        $data['data']               = $id;
        $data['code']               = "reg-".$id;
        $data['operation']          = $is_del;
        $data['tanggal']            = $x->waktu;
        $data['uraian']             = "Registrasi Pasien ".$x->nama." Pada Noreg ".$x->id;
        $data['nilai']              = $x->administrasi_inap + $x->karcis;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }
    
    
	
}

?>