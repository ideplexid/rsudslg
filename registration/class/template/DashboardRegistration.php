<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once "smis-base/smis-include-service-consumer.php";
require_once 'medical_record/class/adapter/DiagnosaAdapter.php';
require_once 'smis-libs-hrd/EmployeeResponder.php';

class DashboardRegistration extends ModulTemplate {
	protected $db;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
    protected $action;
    protected $from;
    protected $to;
    protected $rentang;
    public function __construct($db,$page,$action,$protoslug,$protoname,$protoimplement,$from,$to,$rentang) {
		$this->db               = $db;
		$this->page             = $page;
		$this->protoslug        = $protoslug;
		$this->protoimplement   = $protoimplement;
		$this->protoname        = $protoname;
		$this->action           = $action;
        $this->from             = $from;
        $this->to               = $to;
        $this->rentang          = $rentang;
    }
    
	public function command($command) {
        $tableprint = new TablePrint();
        $tableprint ->setDefaultBootrapClass(false);
        $tableprint ->setMaxWidth(false);

        $dari       = ArrayAdapter::dateFormat("date Y M d",$this->from);
        $sampai     = ArrayAdapter::dateFormat("date Y M d",$this->to);
        $tableprint ->addColumn($this->getJumlahPengunjung(),1,1,null,"","well")
                    ->addColumn($this->getJumlahRJ(),1,1,null,"","well")
                    ->addColumn($this->getJumlahRI(),1,1,null,"","well")
                    ->addColumn($this->getPersentaseBed(),1,3,"body","","vertical-top well")
                    ->addColumn($this->getJenisKunjungan(),1,1,null,"dashboard_barulama","well")
                    ->addColumn($this->getCaraBayar(),2,1,"body","dashboard_carabayar","well ctx ctx1")                    
                    ->addColumn($this->getJenisKelamin(),1,1,null,"dashboard_jk","well")
                    ->addColumn($this->getKecamatan(),2,1,"body","dashboard_kecamatan","well ctx ctx3");
        
        $content['list']        = $tableprint->getHtml();
        $content['pagination']  = "";
        

        $response = new ResponsePackage();
        $response ->setStatus(ResponsePackage::$STATUS_OK);
        $response ->setContent($content);
        echo json_encode($response->getPackage());
		return;
    }
    
    /**number */
    public function getJumlahRJ(){
        $dbtable = new DBTable($this->db,"smis_rg_layananpasien");
        $dbtable ->addCustomKriteria(" tanggal< ","'".$this->to."'")
                 ->addCustomKriteria(" tanggal>= ","'".$this->from."'")
                 ->addCustomKriteria(" carapulang ","!='Tidak Datang'")
                 ->addCustomKriteria(" uri ","=0");
        $total   = $dbtable ->count();
        $jumlah  = ArrayAdapter::format("unit Orang",$total);
        $panel   = new Panel("panel_pengunjung","Rawat Jalan","<i class='fa fa-wheelchair'></i><font>".$jumlah."</font>");
        $panel   ->setClass("panel-info panel-dashboard");
        return $panel->getHtml();
    }

    public function getJumlahRI(){
        $dbtable = new DBTable($this->db,"smis_rg_layananpasien");
        $dbtable ->addCustomKriteria(" tanggal< ","'".$this->to."'")
                 ->addCustomKriteria(" tanggal>= ","'".$this->from."'")
                 ->addCustomKriteria(" carapulang ","!='Tidak Datang'")
                 ->addCustomKriteria(" uri ","=1");
        $total   = $dbtable ->count();
        $jumlah  = ArrayAdapter::format("unit Orang",$total);
        $panel   = new Panel("panel_pengunjung","Rawat Inap","<i class='fa fa-bed'></i><font>".$jumlah."</font>");
        $panel   ->setClass("panel-danger panel-dashboard");
        return $panel->getHtml();
    }

    private function getDonutDataModel($data,$id,$title){
        $source['element']  = "dashboard_".$id."_holder";
        $source['data']     = $data;
        $content            = array();
        $content['html']    = $html;
        $content['title']   = $title;
        $content['sources'] = $source;
        return $content;
    }

    private function getBarDataModel($data,$id,$xkey,$ykeys,$labels,$ymax,$title){
        $source['element']      = "dashboard_".$id."_holder";
        $source['data']         = $data;
        $source['xkey']         = $xkey;
        $source['xLabelAngle']  = 90;
        $source['ykeys']        = $ykeys;
        $source['labels']       = $labels;
        $source['ymin']         = 0;
        $source['ymax']         = $ymax;
        $source['resize']       = false;    
        $content                = array();
        $content['html']        = $html;
        $content['title']       = $title;
        $content['sources']     = $source;
        return $content;
    }

    /**number */
    public function getJumlahPengunjung(){
        $dbtable = new DBTable($this->db,"smis_rg_layananpasien");
        $dbtable ->addCustomKriteria(" tanggal< ","'".$this->to."'")
                 ->addCustomKriteria(" tanggal>= ","'".$this->from."'")
                 ->addCustomKriteria(" carapulang ","!='Tidak Datang'");
        $total   = $dbtable ->count();
        $jumlah  = ArrayAdapter::format("unit Orang",$total);
        $panel   = new Panel("panel_pengunjung","Jumlah Pengunjung","<i class='fa fa-users'></i><font>".$jumlah."</font>");
        $panel   ->setClass("panel-success panel-dashboard");
        return $panel->getHtml();
    }

    /**pie diagram */
    public function getJenisKelamin(){
        $query = "  SELECT sum(if(kelamin=1,1,0)) as wanita, sum(if(kelamin=0,1,0)) as pria 
                    FROM smis_rgv_layananpasien 
                    WHERE tanggal<'".$this->to."' 
                    AND tanggal>='".$this->from."'
                    AND prop!='del' 
                    AND carapulang!='Tidak Datang' ";
        $row   = $this->db->get_row($query);
        $content[]=array("label"=>"Pria","value"=>$row->pria);
        $content[]=array("label"=>"Wanita","value"=>$row->wanita);
        return json_encode($this->getDonutDataModel($content,"jk","Jenis Kelamin"));
    }

    /**pie diagram */
    public function getJenisKunjungan(){
        $query = "  SELECT sum(if(barulama=1,1,0)) as baru, sum(if(barulama=0,1,0)) as lama 
                    FROM smis_rg_layananpasien 
                    WHERE tanggal<'".$this->to."' 
                    AND tanggal>='".$this->from."'
                    AND prop!='del' 
                    AND carapulang!='Tidak Datang' ";
        $row   = $this->db->get_row($query);
        $content[]=array("label"=>"Baru","value"=>$row->baru);
        $content[]=array("label"=>"Lama","value"=>$row->lama);
        return json_encode($this->getDonutDataModel($content,"barulama","Baru Lama"));
    }

    /*tabular diagram */
    public function getCaraBayar(){
        $query = "  SELECT carabayar , count(*) as jumlah 
                    FROM smis_rg_layananpasien 
                    WHERE tanggal<'".$this->to."' 
                    AND tanggal>='".$this->from."'
                    AND prop!='del' 
                    AND carapulang!='Tidak Datang' 
                    GROUP BY carabayar";
        $list   = $this->db->get_result($query);
        $content=array();
        $max = 0;
        foreach($list as $x){
            $content[]=array("carabayar"=>$x->carabayar,"jumlah"=>$x->jumlah);
            if($x->jumlah>$max){
                $max = $x->jumlah;
            }
        }
        return json_encode($this->getBarDataModel($content,"carabayar","carabayar",array("jumlah"),array("Jumlah"),$max,"Cara Bayar" ));
    }

    /**10 Biggest Disctrict - tbaulat*/
    public function getKecamatan(){
        $query = "  SELECT ifnull(nama_kecamatan,'Kosong') as nama_kecamatan , count(*) as jumlah
                    FROM smis_rgv_layananpasien 
                    WHERE tanggal<'".$this->to."' 
                    AND tanggal>='".$this->from."'
                    AND prop!='del' 
                    AND carapulang!='Tidak Datang'
                    GROUP BY  nama_kecamatan
                    LIMIT 0,10";
        $list   = $this->db->get_result($query);
        $content=array();
        $max = 0;
        foreach($list as $x){
            $content[]=array("kecamatan"=>$x->nama_kecamatan,"jumlah"=>$x->jumlah);
            if($x->jumlah>$max){
                $max = $x->jumlah;
            }
        }
        return json_encode($this->getBarDataModel($content,"kecamatan","kecamatan",array("jumlah"),array("Jumlah"),$max,"10 Kecamatan Tertinggi " ));
    }

    /**angka terisi kuota*/
    public function getPersentaseBed(){
        $serv    = new ServiceConsumer($this->db,"get_kelas_sisa_kamar");
        $serv    ->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
        $serv    ->execute();
        $content = $serv->getContent();
        $sisa    = 0;
        $total   = 0;
        $peruang = "<ul>";
        foreach($content as $x){
            $sisa    += $x["sisa"];
            $total   += $x["total"];
            $peruang .= "<li><div class='left'>".ArrayAdapter::format("unslug",$x['entity'])."</div><div class='right'> [ ".$x["sisa"]."/".$x["total"]." ]</div> </li>";
        }
        $peruang .= "</ul>";
        $persen   = ($sisa*100/$total)."%";
        $panel    = new Panel("panel_bed","Persentase Bed","<i class='fa fa-bed'></i><font><div>".$persen."</div></font><div>".$peruang."</div>");
        $panel    ->setClass("panel-warning panel-dashboard");        
        return $panel->getHtml();
    }

	public function phpPreLoad() {
        $rentang = new OptionBuilder();
        $rentang ->add("Hari ini","1")
                 ->add("Seminggu","7")
                 ->add("Sebulan","30")
                 ->add("3 Bulan","90")
                 ->add("Setahun","366");

        $uitable = new Table(array());
        $uitable ->setName($this->action);
        $uitable ->addModal("rentang","select","Waktu",$rentang->getContent())
                 ->addModal("dari","date","Dari","")
                 ->addModal("sampai","date","Sampai","");
		
		$form = $uitable->getModal()->getForm();
        $view = new Button("Proses","Proses","Proses");
        $view ->setAction("dashboard_registration.view()");
        $view ->setIcon("fa fa-refresh");
        $view ->setIsButton(Button::$ICONIC_TEXT);
        $view ->setClass("btn btn-primary");
        $form ->addElement("",$view);

		$protoname  = new Hidden("dashreg_protoname","",$this->protoname);
		$protoslug  = new Hidden("dashreg_protoslug","",$this->protoslug);
		$implement  = new Hidden("dashreg_protoimplement","",$this->protoimplement);
		$page       = new Hidden("dashreg_page","",$this->page);		
		$action     = new Hidden("dashreg_action","",$this->action);	
        
        echo $protoname ->getHtml();
		echo $protoslug ->getHtml();	
		echo $implement ->getHtml();
		echo $page      ->getHtml();
        echo $action    ->getHtml();        
        echo $form      ->getHtml();
        
        echo "<div class='clear'></div>";
        echo "<div id='".$this->action."_list'></div>";
        
		echo addCSS ("framework/bootstrap/css/datepicker.css");
        echo addCSS ("framework/bootstrap/css/morris.css");
        echo addCSS ("registration/resource/css/dashboard_registration.css",false);
        echo addJS  ("framework/jquery/raphael-min.js");
        echo addJS  ("framework/bootstrap/js/morris.min.js");
        echo addJS  ("framework/smis/js/table_action.js");
        echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
        echo addJS  ("framework/bootstrap/js/bootstrap-select.js");
        echo addJS  ("registration/resource/js/dashboard_registration.js",false);
	}
}

?>