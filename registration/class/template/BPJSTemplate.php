<?php 
require_once "smis-libs-class/MasterTemplate.php";
require_once "registration/class/responder/BPJSBackEndResponder.php";
require_once "registration/class/responder/BPJSBackEndVClaimResponder.php";
global $db;

$action  = $_POST['action'];
$mas     = new MasterTemplate($db,"smis_rg_sep","registration",$action);
$mas     ->setDateTimeEnable(true);
$uitable = $mas->getUItable();
$uitable ->addModal("id","hidden","","")
         ->addModal("nama","text","Nama","")
         ->addModal("kode","text","Kode","")
         ->addModal("status","checkbox","Status","")
         ->setDelButtonEnable(false)
         ->setHeader(array("No.","No.SEP","No.RM","No.Reg","No.BPJS","Tanggal","Tgl Rujukan","No.RJK","PPK","Diagnosa","Tujuan","Kelas","Server","Bridging"));

$detail  = new Button("","","Detail");
$detail  ->setIsButton(Button::$ICONIC)
         ->setIcon(" fa fa-list")
         ->setClass("btn-primary");
$uitable ->addContentButton("sep_detail",$detail);

$detail  = new Button("","","Cetak");
$detail  ->setIsButton(Button::$ICONIC)
         ->setIcon(" fa fa-print")
         ->setClass("btn-inverse");
$uitable ->addContentButton("sep_print",$detail);

$pulang  = new Button("","","Pulang");
$pulang  ->setIsButton(Button::$ICONIC)
         ->setIcon(" fa fa-sign-out")
         ->setClass("btn-info");
$uitable ->addContentButton("show_sep_pulang",$pulang);


$remove  = new Button("","","Hapus");
$remove  ->setIsButton(Button::$ICONIC)
         ->setIcon(" fa fa-trash")
         ->setClass("btn-danger");
$uitable ->addContentButton("sep_remove",$remove);

$rujukan = new Button("","","Rujukan");
$rujukan ->setIsButton(Button::$ICONIC)
         ->setIcon(" fa fa-forward")
         ->setClass("btn-danger");
$uitable ->addContentButton("sep_rujukan",$rujukan);


$jenis   = new OptionBuilder();
$jenis   ->add("IGD","0",0)
         ->add("Rawat Inap","1",0)
         ->add("Rawat Jalan","2",0);

$asal    = new OptionBuilder();
$asal    ->add("Faskes Tingkat 1 (Puskesmas)","1","1")
         ->add("Faskes Tingkat 2 (RS)","2","0");

$polidb  = new DBTable($db,"smis_rg_refpolibpjs");
$polidb  ->addCustomKriteria("status","='1'")
         ->setShowAll(true);
$d       = $polidb->view("",0);
$list    = $d['data'];
$adapter = new SelectAdapter("nama","kode");
$content = $adapter->getContent($list);
array_push($content,array("name"=>"","value"=>"","default"=>1));

$kelas   = new OptionBuilder();
$kelas   ->add("","",1)
         ->add("Kelas I","1",0)
         ->add("Kelas II","2",0)
         ->add("Kelas III/Rawat Jalan","3",0);

$laka    = new OptionBuilder();
$laka    ->add("Bukan Kasus Kecelakaan","0",1)
         ->add("Kasus Kecelakaan","1",0);
$cob     = new OptionBuilder();
$cob     ->add("Ya","1","0")
         ->add("Tidak","0","1");
$pjmn    = new OptionBuilder();
$pjmn    ->add("","","1")
         ->add("Jasa raharja ","1","0")
         ->add("BPJS Ketenagakerjaan","2","0")
         ->add("TASPEN PT","3","0")
         ->add("ASABRI PT","4","0");

$uitable ->clearContent();
$uitable ->addModal("id","hidden","","")
         ->addModal("noSEP","text","No Sep","","n",null,true)
         ->addModal("noKartu","text","No BPJS","","n",null,true)
         ->addModal("tglSep","datetime","Tgl SEP","","n",null,false)
         ->addModal("tglRujukan","datetime","Tgl Rujukan","","n",null,false)
         ->addModal("asalRujukan","select","Asal Rujukan",$asal->getContent(),"y",null,true) 
         ->addModal("noRujukan","text","No Rujukan","","y",null,true)
         ->addModal("ppkRujukan","text","PPK Rujukan","","y",null,false)
         ->addModal("ppkPelayanan","text","PPK Pelayanan","","n",null,false)
         ->addModal("jnsPelayanan","select","Jenis Layanan",$jenis->getContent(),"n",null,true)
         ->addModal("diagAwal","text","Kode Diagnosa","A010","n",null,false)
         ->addModal("poliTujuan","select","Poli Tujuan",$content,"y",null,false)
         ->addModal("eksekutif","checkbox","Eksekutif","","y",null)
         ->addModal("klsRawat","select","Kelas",$kelas->getContent(),"n",null)
         ->addModal("cob","select","Cost Bersama (COB)",$cob->getContent(),"y",null)
         ->addModal("penjamin","multiple-select","Penjamin",$pjmn->getContent(),"y",null)
         ->addModal("lakaLantas","select","Kasus Laka",$laka->getContent(),"n",null)
         ->addModal("lokasiLaka","text","Lokasi Laka","","y",null)
         ->addModal("catatan","textarea","Catatan","","y",null)
         ->addModal("user","text","User SEP",getSettings($db,"reg-bpjs-vclaim-api-user","-"),"y",null,true)
         ->addModal("noMr","text","NRM Pasien","","n",null,true)
         ->addModal("noTelp","text","No. Telp","","n",null,true);

$adapter = $mas->getAdapter();
$adapter ->setUseNumber(true,"No.","back.")
         ->add("No.SEP","noSEP")
         ->add("No.RM","noMr","only-digit8")
         ->add("No.Reg","noReg","only-digit8")
         ->add("No.BPJS","noKartu")
         ->add("Tanggal","tglSep","date d M Y H:i")
         ->add("Tgl Rujukan","tglRujukan","date d M Y H:i")
         ->add("No.RJK","noRujukan")
         ->add("PPK","ppkRujukan")
         ->add("Diagnosa","diagAwal")
         ->add("Tujuan","poliTujuan")
         ->add("Kelas","klsRawat")
         ->add("Server","delServer","trivial_1_Deleted_Saved")
         ->add("Bridging","createBy","unslug");


$dbtable = $mas->getDBtable();

//$dbtable->addCustomKriteria("jnsPelayanan","='1'");
$dbres      = new BPJSBackEndResponder($dbtable,$uitable,$adapter);
if($dbres->isPreload()){
    $modal  = new Modal("update_tgl_pulang_".$action,"",$action);
    $modal  ->addModalElement("datetime","","Tanggal","tanggal_pulang",null,"n");
    $modal  ->addModalElement("hidden","","","id_pulang",null,"n");
    $button = new Button("", "", "Update Pulang");
    $button ->setClass("btn-primary")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setIcon("fa fa-sign-out")
            ->setAction($action.".sep_pulang()");
    $modal  ->addFooter($button);
    $modal  ->setTitle("Update Tanggal Pulang");
    echo $modal->getHtml();
}else if(!$dbres->isView()) {
    $data      = $dbtable->select($_POST['id']);
    if($data->createBy=="vclaim"){
        $dbres = new BPJSBackEndVClaimResponder($dbtable,$uitable,$adapter);
    }
}
$mas->setTableActionClass("BPJSActionTable");
$mas->setDBresponder($dbres);    
$mas->setModalTitle("Map Referensi Poli BPJS");
$mas->getModal()->setComponentSize(Modal::$MEDIUM);
$mas->addResouce("js","registration/resource/js/BPJSActionTable.js","before");
$mas->addResouce("css","registration/resource/css/sep_table.css");
$mas->initialize();
?>