<?php 
class KarcisService extends ServiceConsumer {
	public function __construct($db, $polislug,$barulama,$urji="urj") {
		$array = array (
				"ruangan" => $polislug,
				"barulama" => $barulama,
				"urji"=>$urji
		);
		parent::__construct ( $db, "get_karcis", $array, "manajemen" );
	}
	public function proceedResult() {
		$content = array ();
		$result = json_decode ( $this->result, true );
		foreach ( $result as $autonomous ) {
			foreach ( $autonomous as $entity ) {
				if ($entity != null || $entity != "")
					return $entity;
			}
		}
		return $content;
	}
}
?>