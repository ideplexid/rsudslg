<?php 
class AntrianService extends ServiceConsumer{
	public function __construct($db,$tanggal,$nama_pasien,$nrm_pasien,$carabayar,$ruangan,$no_register,$umur,$gm,$jk,$alamat,$detail){
		$array					= array();
		$array['waktu']			= $tanggal;
		$array['nrm_pasien']	= $nrm_pasien;
		$array['nama_pasien']	= $nama_pasien;
		$array['id']			= "";
		$array['command']		= 'save';
		$array['carabayar']		= $carabayar;
		$array['no_register']	= $no_register;
		$array['umur']			= $umur;
		$array['jk']			= $jk;
		$array['golongan_umur']	= $gm;
		$array['asal']			= "Pendaftaran";
		$array['ibu_kandung']	= $detail['ibu'];
		$array['waktu_register']= $detail['waktu_register'];
		$array['kelas']			= " - ";
		$array['alamat']		= $alamat;		
		$array['detail']		= json_encode($detail);
		parent::__construct($db, "push_antrian",$array,$ruangan);
	}

	public function setBedKamar($id_bed,$nama_bed){
		$this->addData("id_bed_kamar",$id_bed);
		$this->addData("bed_kamar",$nama_bed);
	}

	
	public function proceedResult(){
		parent::proceedResult();
        $content		 = array();
        if($this->getMode()==ServiceConsumer::$KEY_ENTITY){
            $result		 = json_decode($this->result,true);
            foreach($result as $autonomous){
                $content = array_merge($content,$autonomous);
            }
        }else{
            $content	 = array();
            $result		 = json_decode($this->result,true);
            foreach($result as $autonomous){
                foreach($autonomous as $entity){
                    if($entity!=null || $entity!="")
                        return $entity;
                }
            }            
        }
		return $content;
	}
}
?>