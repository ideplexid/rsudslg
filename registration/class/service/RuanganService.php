<?php
class RuanganService extends ServiceConsumer {
	public function __construct($db) {
		parent::__construct ( $db, "get_entity", "push_antrian" );
	}
	public function proceedResult() {
		$content = array ();
		$result = json_decode ( $this->result, true );
		foreach ( $result as $autonomous ) {
			foreach ( $autonomous as $entity ) {
				$option = array ();
				$option ['value'] = $entity;
				$option ['name']  = ArrayAdapter::format("unslug",str_replace("poli","poli ", $entity )) ;
				$number = count ( $content );
				$content [$number] = $option;
			}
		}
		return $content;
	}
}

?>