<?php
class RujukanVClaimTable extends Table{

    public function getPrintedElement($pure,$formatted){
        $tp = new TablePrint("rujukan_vclaim_table");
        $tp ->setMaxWidth(false);
        $tp ->setDefaultBootrapClass(false);
        
        $tipe = $pure->tipe=="0"?"RUJUKAN PENUH":($pure->tipe=="1"?"RUJUKAN PARSIAL":"RUJUK BALIK");
        $tp->addColumn("<img src='registration/resource/image/bpjs-logo.png' id='logo_bpjs' />",3,2);
        if(getSettings($db,"reg-bpjs-print-logo","0")=="1"){
            $logo = getLogo();
            $tp->addColumn("<div id='title_bpjs'>SURAT RUJUKAN BPJS</div><div id='sub_title_bpjs'>".$tipe."</div>",5,1);
            $tp->addColumn("<img src='".$logo."' id='logo_system' />",2,2);
            $tp->commit("header");
        }else{
            $tp->addColumn("<div id='title_bpjs'>SURAT RUJUKAN BPJS</div><div id='sub_title'>".getSettings($db,"smis_autonomous_title","")."</div>",7,1);
            $tp->commit("header");
        }
        
        $tp ->addColumn("Kepada Yth",1,1)
            ->addColumn(":",1,1)
            ->addColumn("<strong>".$pure->nama_ppk."</strong>",3,1)
            ->addSpace(1,1)
            ->addColumn("No. Rujukan",1,1)
            ->addColumn(":",1,1)
            ->addColumn("<strong>".$pure->no_rujukan."</strong>",2,1)
            ->commit("body");

        $tp ->addSpace(2,1)
            ->addColumn("<strong>".$pure->nama_poli."</strong>",3,1)
            ->addSpace(1,1)
            ->addColumn("Asal Rumah Sakit",1,1)
            ->addColumn(":",1,1)
            ->addColumn("<strong>".getSettings($db,"smis_autonomous_title","")."</strong>",2,1)
            ->commit("body");

        $tp ->addColumn("Mohon pemeriksa dan penanganan lebih lanjut penderita : ",10,1)
            ->commit("body");

        $tp ->addColumn("Nama",1,1)
            ->addColumn(":",1,1)
            ->addColumn($pure->nama_pasien,7,1)
            ->commit("body");

        $tp ->addColumn("No Kartu BPJS",1,1)
            ->addColumn(":",1,1)
            ->addColumn($pure->no_bpjs,3,1)
            ->addSpace(1,1)
            ->addColumn("Kelamin",1,1)
            ->addColumn(":",1,1)
            ->addColumn($pure->jk=="0"?"Laki-Laki":"Perempuan",2,1)
            ->commit("body");

        $tp ->addColumn("Diagnosa",1,1)
            ->addColumn(":",1,1)
            ->addColumn($pure->diagnosa." - ".$pure->nama_diagnosa,3,1)
            ->addSpace(1,1)
            ->addColumn("Rawat",1,1)
            ->addColumn(":",1,1)
            ->addColumn($pure->jenis=="1"?"RAWAT INAP":"RAWAT JALAN",2,1)
            ->commit("body");
        
        $tp ->addColumn("Mohon pemeriksa dan penanganan lebih lanjut penderita : ",10,1)
            ->commit("body");
        $tp ->addColumn($pure->catatan,10,1)
            ->commit("body");

        $tp ->addSpace(7,1)
            ->addColumn(getSettings($db,"smis_autonomous_town","").", ".ArrayAdapter::format("date d M Y",$pure->tanggal),3,1,null,null,"center")
            ->commit("body");
        
        $tp ->addSpace(7,1)
            ->addColumn("Mengetahui",3,1,null,null,"center")
            ->commit("body");
        for($x=0;$x<10;$x++){
            $tp ->addSpace("",11,1);
        }
        $tp ->commit("body");
        $tp ->addSpace(10,1)
            ->commit("body");
        $tp ->addSpace(7,1)
            ->addColumn("____________________",3,1,null,null,"center")
            ->commit("body");

        $css=getSettings($db,"reg-bpjs-rujukan-print-css","");
        return $tp->getHtml().$css.addCSS("registration/resource/css/rujukan_bpjs.css",false);
	}

}
?>