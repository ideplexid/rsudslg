<?php 
/**
 * this class used to handle PasienAktif
 * both in Registrasion Pasien -> tab pasien Aktif
 * and Pasien Inap.
 * 
 * in pasien aktif, if set it will determine wether 
 * edit disabled , enabled or based the pasien register time 
 * 
 * in pasien inap the different only in the 
 * report that show and different in patient Label
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @version 	: 1.0.0
 * @since		: 6 Aug 2014
 * @used		: registration/resource/php/register_inap/register_menginap.php
 * @database	: smis_rg_patient
 * 				: smis_rg_layananpasien
 * 
 * */
class DaftarTableInap extends DaftarTable{
    
    private $_edit_mode;
	public static $MODE_FREE=-1;
	public static $MODE_STRICT_TIME=0;//berisi integer number sesuai inputan user
	public static $MODE_MODE_DISABLED=-2;
    
	protected function showLabel($db){
		if(getSettings($db, "reg-show-inap-label", "0")=="1"){
			$button=new Button("", "", "Label");
			$button->setIsButton(Button::$ICONIC);
			$button->setClass("btn btn-primary");
			$button->setIcon(" fa fa-trello");
			$this->addContentButton("label_inap", $button);
		}
	}
    
    public function initComponentButton(){
		global $db;
		parent::initComponentButton();
		$this->showSEP($db);
	}
    
    protected function showSEP($db){
        if(getSettings($db,"reg-bpjs-activate-bpjs-reg-uri","0")=="1"){
            $btn=new Button("", "", "SEP");
            $btn->setIsButton(Button::$ICONIC);
            $btn->setIcon("fa fa-id-badge");
            $btn->setClass(" btn-primary");
            $this->addContentButton("sep_bpjs_ri", $btn);
        }
    }
    
    public function setEditMode($mode){
		$this->_mode=$mode;
	}
    
    /**
	 * @brief used to determine whether the 
	 * 		  user register more than the time that have been 
	 * 		  set. if yes, then no edit button
	 * 		  	   if no , then there edit button
	 * 		  if set to disabled thenforever disabled
	 * @param int $id 
	 * @return  
	 */
	public function getContentButton($id){
		if($this->_mode==self::$MODE_FREE){
			$this->setEditButtonEnable(true);
			return parent::getContentButton($id);
		}else if($this->_mode==self::$MODE_MODE_DISABLED){
			$this->setEditButtonEnable(false);
			return parent::getContentButton($id);
		}else{
            loadLibrary("smis-libs-function-time");
			$tanggal=$this->current_data['tgl_inap'];
			$diff=minute_different(date("Y-m-d H:i:s"),$tanggal)*1;
			if($diff>=$this->_mode){
				$this->setEditButtonEnable(false);
			}else{
				$this->setEditButtonEnable(true);
			}
			return parent::getContentButton($id);
		}
	}
}

?>