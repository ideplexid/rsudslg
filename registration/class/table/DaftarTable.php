<?php 
/**
 * this class used to handle PasienAktif
 * both in Registrasion Pasien -> tab pasien Aktif
 * and Pasien Inap.
 * 
 * in pasien aktif, if set it will determine wether 
 * edit disabled , enabled or based the pasien register time 
 * 
 * in pasien inap the different only in the 
 * report that show.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @version 	: 1.0.2
 * @since		: 6 Aug 2014
 * @used		: registration/resource/php/registration_patient/reg_layanan.php
 * @database	: smis_rg_patient
 * 				: smis_rg_layananpasien
 * 
 * */
class  DaftarTable extends TableSynchronous{
	
	private $_edit_mode;
	public static $MODE_FREE=-1;
	public static $MODE_STRICT_TIME=0;//berisi integer number sesuai inputan user
	public static $MODE_MODE_DISABLED=-2;
	
	private $mode="0";
	private $db;
	
	public function __construct($header,$title="",$content=NULL,$action=true){
		parent::__construct($header,$title,$content,$action);
		$this->_mode=self::$MODE_FREE;
		$this->initComponentButton();
		loadLibrary("smis-libs-function-time");
	}
	
	/**
	 * @brief 
	 * this function used for adding
	 * extrea button in the system so 
	 * it will showing data like 
	 * Label Button, Ring Button, Tag Button etc 
	 * @return nothing, only affect in User Interface
	 */
	public function initComponentButton(){
		global $db;
		$this->showSocial($db);
		$this->showTag($db);
		$this->showLabel($db);		
		$this->showAntrian($db);
		$this->showGelangBayi($db);
		$this->showGelangDewasa($db);
		$this->showSynchronize($db);
		$this->showCancel($db);
		$this->showLocation($db);
	}

	private function showAntrian($db){
		if(getSettings($db,"reg-antrian-active","0")=="1"){
			$antri	= new Button("", "", "Cetak Antrian");
			$antri	->setIcon(" fa fa-list-alt")
				->setIsButton(Button::$ICONIC)
				->setClass("btn-primary");
			$this	->addContentButton("cetak_antrian", $antri);
		}
	}

	private function showCancel($db){
		if(getSettings($db,"reg-show-cancel","0")=="1"){
			$batal	= new Button("", "", "Batal Pelayanan");
			$batal	->setIcon(" fa fa-bed")
					->setIsButton(Button::$ICONIC)
					->setClass("btn-inverse");
			$this	->addContentButton("batal_pasien", $batal);
		}
	}
	
	private function showSocial($db){
		if(getSettings($db, "reg-print-data-social-jsetup", "0")=="1"){
			$jsetup=new Button("", "", "Print Data Sosial");
			$jsetup->setIcon(" fa fa-print");
			$jsetup->setIsButton(Button::$ICONIC);
			$jsetup->setClass("btn-inverse");
			$this->addContentButton("jsetup_sosial", $jsetup);
		}else{
			$this->setPrintElementButtonEnable(true);
		}
	}
	
	private function  showTag($db){
		if(getSettings($db, "reg-show-tag", "0")=="1"){
			$button=new Button("", "", "Tag");
			$button->setIsButton(Button::$ICONIC);
			$button->setClass("btn btn-primary");
			$button->setIcon(" fa fa-tag");
			if(getSettings($db, "reg-tag-jsprintsetup", "0")=="1"){
				$this->addContentButton("dogtag_jsprint", $button);
			}else{
				$this->addContentButton("dogtag", $button);
			}
		}
	}

	private function showLocation($db){
		if(getSettings($db, "reg-show-location", "0")=="1"){
			$button=new Button("", "", "Lokasi Pasien");
			$button->setIsButton(Button::$ICONIC);
			$button->setClass("btn btn-primary");
			$button->setIcon(" fa fa-map");
			$this->addContentButton("show_location", $button);
		}
	}
	
	protected function showLabel($db){
		if(getSettings($db, "reg-show-label", "0")=="1"){
			$button=new Button("", "", "Label RJ");
			$button->setIsButton(Button::$ICONIC);
			$button->setClass("btn btn-primary");
			$button->setIcon(" fa fa-trello");
			$this->addContentButton("label", $button);
		}
		
		if(getSettings($db, "reg-show-inap-label", "0")=="1"){
			$button=new Button("", "", "Label RI");
			$button->setIsButton(Button::$ICONIC);
			$button->setClass("btn btn-inverse");
			$button->setIcon(" fa fa-trello");
			$this->addContentButton("label_inap", $button);
		}
	}
	
	private function showSynchronize($db){
		if(getSettings($db, "reg-show-sinkronisasi", "0")=="1"){
			$btn=new Button("", "", "Broadcast");
			$btn->setIsButton(Button::$ICONIC);
			$btn->setIcon("fa fa-volume-up");
			$btn->setClass(" btn-primary");
			$this->addContentButton("sinkronisasi", $btn);
		}
	}
	
	private function showGelangBayi($db){
		if(getSettings($db, "reg-show-gelang-bayi", "0")=="1"){
			$button = new Button("", "", "Gelang Pasien Bayi");
			$button->setIsButton(Button::$ICONIC);
			$button->setClass("btn btn-inverse");
			$button->setIcon("fa fa-child");
			$this->addContentButton("b_wbpatient_print", $button);
		}
	}
	
	private function showGelangDewasa($db){
		if(getSettings($db, "reg-show-gelang-dewasa", "0")=="1"){
			$button = new Button("", "", "Gelang Pasien Dewasa");
			$button->setIsButton(Button::$ICONIC);
			$button->setClass("btn");
			$button->setIcon("fa fa-user");
			$this->addContentButton("a_wbpatient_print", $button);
		}
	}
	
	public function getPrintedElement($p,$f){
		global $db;
		$this->db=$db;
		$pdbtable=new DBTable($db,"smis_rg_patient");
		$pasien=$pdbtable->select($p->nrm);
		return $this->print_data_social_kunjugan_pasien($pasien,$p,$f);
	}
	
	public function setMode($mode){
		$this->mode=$mode;
		return $this;
	}
	
	public function setEditMode($mode){
		$this->_mode=$mode;
		return $this;
	}
	
	/**
	 * @brief used to determine whether the 
	 * 		  user register more than the time that have been 
	 * 		  set. if yes, then no edit button
	 * 		  	   if no , then there edit button
	 * 		  if set to disabled thenforever disabled
	 * @param int $id 
	 * @return  
	 */
	public function getContentButton($id){
		if($this->_mode==self::$MODE_FREE){
			$this->setEditButtonEnable(true);
			return parent::getContentButton($id);
		}else if($this->_mode==self::$MODE_MODE_DISABLED){
			$this->setEditButtonEnable(false);
			return parent::getContentButton($id);
		}else{
            loadLibrary("smis-libs-function-time");
			$tanggal=$this->current_data['tgl'];
			$diff=minute_different(date("Y-m-d H:i:s"),$tanggal)*1;
			if($diff>=$this->_mode){
				$this->setEditButtonEnable(false);
			}else{
				$this->setEditButtonEnable(true);
			}
			return parent::getContentButton($id);
		}
	}
	

	/**
	 * used to print data of patient
	 * this one deletegate to another 
	 * file bcaue make this class too big
	 * it's delegate to : 
	 * - registration/snippet/print_data_social.php
	 *
	 * @param unknown $pr : Pasien Raw, data asli pasien
	 * @param unknown $pf : Pasien Formatted , data pasien yang terformat
	 * @param unknown $rr : Register Raw , data register asli
	 * @param unknown $rf : Register Formatted, data register yang terformat
	 */
	
	function print_data_social_kunjugan_pasien($pr,$rr,$rf){
		require_once "registration/snippet/print_data_social.php";
		return print_data_social($pr,$rr,$rf,$this->mode);
	}
}

?>