<?php 

class LapRegisterTable extends Table{

    public function getPrintedElement($p,$f){
        $noreg_pasien = $p['id'];
        global $db;
        $dbtable = new DBTable($db,"smis_rg_bed_pasien");
        $dbtable->setShowAll(true);
        $dbtable->setOrder(" c_time DESC ",true);
        $dbtable->addCustomKriteria(" noreg_pasien ","='".$noreg_pasien."'");
        $data =  $dbtable->view("","0");
        $list = $data['data'];
        $table = new TablePrint("");
        $table->setDefaultBootrapClass(true);
        $table->setMaxWidth(false);
        $table->addColumn("No.","1","1");
        $table->addColumn("Time","1","1");
        $table->addColumn("Ruangan","1","1");
        $table->addColumn("Bed","1","1");
        $table->addColumn("User","1","1");
        $table->commit("title");
        $no = 0;
        foreach($list as $x){
            $no++;
            $table->addColumn($no.".","1","1");
            $table->addColumn(ArrayAdapter::format("date d M Y H:i:s",$x->c_time),"1","1");
            $table->addColumn(ArrayAdapter::format("unslug",$x->last_ruangan),"1","1");
            $table->addColumn($x->last_bed,"1","1");
            $table->addColumn($x->c_user,"1","1");
            $table->commit("body");
        }

        return $table->getHtml();
    }


}