<?php 

class ImportBPJSTable extends Table{
    
    public function getContentButton($id){
        $btn_group=parent::getContentButton($id);
        if($this->current_data['NRM']!="0"){
            $btn=new Button($this->name."_register", "","Register");
            $btn->setAction($this->action.".register('".$id."')");
            $btn->setClass("btn-primary");
            $btn->setIcon("fa fa-sign-in");
            $btn->setIsButton(Button::$ICONIC_TEXT);
            $btn_group->addElement($btn);            
        }else if($this->current_data['NRM']=="0"){
            $btn=new Button($this->name."_daftar", "","Daftar");
            $btn->setAction($this->action.".daftar('".$id."')");
            $btn->setClass("btn-primary");
            $btn->setIcon("fa fa-user");
            $btn->setIsButton(Button::$ICONIC_TEXT);
            $btn_group->addElement($btn); 
        }
        return $btn_group;
    }
    
}

?>