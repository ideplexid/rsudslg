<?php 

class ExcelImporterResponder extends DBResponder{
    /* @var ExcelImporter */
    private $importer;
    
    public function command($command){
        if($command=="import_proceed"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $content=$this->import_proceed();
            $pack->setContent("");
            $pack->setWarning(true,"Import Done"," Total ".$content." Data Successfully Imported");
            return $pack->getPackage();
        }else if($command=="clearall"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $content=$this->clearall();
            $pack->setContent("");
            $pack->setWarning(true,"All Data Removed"," Total ".$content." Data Successfully Removed");
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }

    private function clearall(){
        global $db;
        $update=array();
        $update['time_updated']=date("Y-m-d H:i:s");
        $update['autonomous']="[".getSettings($db,"smis_autonomous_id","")."]";   
        $update['prop']="del";
        $dbtable=new DBTable($db,"smis_rg_excel_bpjs");
        return $dbtable->update($update,array("prop"=>""));
    }
    
    private function import_proceed(){
        global $db;
        $id=$_POST['id'];
        $row=$this->dbtable->select($id);
        $this->importer->setFilePath($row->file);
        if($row->max_memory!=""){
            ini_set('memory_limit',$row->max_memory);
        }
        if($row->max_time*1>0){
            ini_set('max_execution_time',$row->max_time);
        }
        $column_map=array();
        $column_map[0]="nama_faskes";
        $column_map[1]="nama_ppk";
        $column_map[2]="no_pst";
        $column_map[3]="nama_pasien";
        $column_map[4]="nrm";
        $column_map[5]="jk";
        $column_map[6]="tgl_lahir";
        $column_map[7]="pisa";
        $column_map[8]="alamat";
        $column_map[9]="jenis";
        $column_map[10]="kode_asuransi";
        $column_map[11]="periode";
        for($i=0;$i<$row->sheets;$i++){
            $this->importer->addSetup($i,2,$column_map,null);
        }
        $this->importer->addFixValue("last_update",$row->tanggal);
        $this->importer->addFixValue("origin_id","");
        $this->importer->addFixValue("origin",getSettings($db,"smis_autonomous_id",""));
        $exe=$this->importer->execute();
        
        $dbtable=new DBTable($db,"smis_rg_excel_bpjs");
        $update=array();
        $update['origin_id']="id";
        $warp=array();
        $warp['origin_id']=DBController::$NO_STRIP_ESCAPE_WRAP;
        $dbtable->update($update,array("origin_id"=>"0"),$warp);
        return $exe;
    }
    
    public function postToArray(){
        $result=parent::postToArray();
        $this->importer->setFilePath($result['file']);
        $sheet=$this->importer->countSheet();
        $result['sheets']=$sheet;
        return $result;
    }
    
    public function setImporter(ExcelImporter $importer){
        $this->importer=$importer;
        return $this;
    }
    
}


?>