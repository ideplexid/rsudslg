<?php 

class BonusPerujukResponder extends DBResponder{
	
	public function command($command){
		$pack=new ResponsePackage();
		$content=NULL;
		$status='not-authorized';	//not-authorized, not-command, fail, success
		$alert=array();		
		if($command=="cair"){
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setContent($this->cair());
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Data sudah dianggap Lunas","Data Sudah Dianggap Lunas");
			return $pack->getPackage();
		}else if($command=="cair_satu"){
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setContent($this->cair_satu());
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Data sudah dilunaskan","Data Sudah  dilunaskan");
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
	}
	
	public function cair_satu(){
		$id=$_POST['id'];
		$sampai=$_POST['sampai'];
		$query="UPDATE smis_rg_layananpasien SET besarambil=besarbonus,keteranganbonus='Sudah Diambil'
					WHERE id='".$id."'";
		$this->getDBTable()->get_db()->query($query);
		return "";
	}
	
	public function cair(){
		$dari=$_POST['dari'];
		$sampai=$_POST['sampai'];
		$query="UPDATE smis_rg_layananpasien SET besarambil=besarbonus,keteranganbonus='Sudah Diambil' 
					WHERE tanggal>='".$dari."' AND tanggal<='".$sampai."'";
		if($_POST['id_perujuk']!="" && $_POST['id_perujuk']*1!=0 ){
			$query.=" AND id_rujukan='".$_POST['id_perujuk']."' ";
		}
		$this->getDBTable()->get_db()->query($query);
		return "";
	}
	
}

?>