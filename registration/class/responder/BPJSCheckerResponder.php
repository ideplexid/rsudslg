<?php 
/**
 * this class Spesifically for Handling Response and Request to BPJS SEP Server
 * this class can handle the create SEP request, Delete , Update , Cek, and etc
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.3
 * @since       : 09-Feb-2017
 * @database    : - smis_rg_layananpasien
 * 
 * */

require_once "registration/class/responder/BPJSResponder.php";
class BPJSCheckerResponder extends BPJSResponder{    
    public function command($command){
        if($command=="cek_bpjs"){
             $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setContent($this->cek_bpjs());
            return $pack->getPackage();
        }else if($command=="reg_sep"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->reg_sep();
            $pack->setWarning(true,"Pemberitahuan !!!",$r['msg']);
            $pack->setContent($r['response']);
            return $pack->getPackage();
        }else if($command=="fix_register"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->fixRegister();
            $pack->setWarning(true,"Pemberitahuan !!!",$r['msg']);
            $pack->setContent($r['response']);
            return $pack->getPackage();
        }else if($command=="print_sep"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->print_sep();
            $pack->setContent($r);
            return $pack->getPackage();
        }else if($command=="cek_rujukan"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->cek_rujukan($_POST['faskes']=="1"?"":"RS");
            $pack->setContent($r);
            return $pack->getPackage();
        }else{
           return parent::command($command);
        }
    }
    
    private function print_sep(){
        $id=$_POST['id'];
        $row=$this->getDBTable()->select(array("id"=>$id));
        $nosep=$row->noSEP;
        return $this->print_sep_nosep($nosep);
    }
    
    public function print_sep_nosep($nosep){
        $url=$this->url_base."SEP/".$nosep;
        $metode="GET";
        $request="";
        $this->do_curl($this->port,$url,$metode,$request);

        if ($this->error) {
          return "cURL Error #:" . $err;
        } else {
            
            $data=json_decode($this->response,true);
            if($data==null ){
                return "Terjadi Kesalahan Akibat Koneksi Server BPJS";
            }else if( $data['metadata']['code']!="200" ){
                return $data['metadata']['message'];
            }else{
                global $db;
                require_once "registration/function/bpjs_array_adapter.php";
                require_once "registration/function/bpjs_print_format.php";
                $res=bpjs_array_adapter($data);
                return bpjs_print_format($db,$nosep,$res);
            }
        }
    }
    
    /**
     * @brief this function used to cek whether the 
     *          BPJS account of patient is active, exist or not
     *          by using the BPJS Number OR NIK/KTP Number
     * @return  array("table"=>"HTML Table,"raw"=>array data that contains BPJS data);
     */
    public function cek_bpjs(){
        $nobpjs=isset($_POST['no_bpjs'])?$_POST['no_bpjs']:"";
        $noktp=isset($_POST['no_ktp'])?$_POST['no_ktp']:"";        
        $methode="GET";
        $request="";        
        if($nobpjs!=""){
            $url=$this->url_base."Peserta/peserta/".$nobpjs;
        }else{
            $url=$this->url_base."Peserta/peserta/nik/".$noktp;
        }
       
        $this->do_curl($this->port,$url,$methode,$request);
        $data=null;
        if ($this->error) {
            require_once "registration/function/bpjs_null.php";
            $data=bpjs_null();
        } else {
            $response=json_decode($this->response,true);
            if($response==null || $response['metadata']['code']!="200" ){
                require_once "registration/function/bpjs_null.php";
                $data=bpjs_null();
            }else{
                require_once "registration/function/bpjs_found.php";
                $querylog->addMessage("Cek VClaim Berdasarkan Nomor BPJS ");
                $data=bpjs_found($response,$this->getKey());
            }
        }
        require_once "registration/function/bpjs_table_checker.php";
        $table=bpjs_table_checker($data);
        $result=array("table"=>$table,"raw"=>$data);
        return $result;
    }
   
    
   
    /**
     * @brief this function used for creating the BPJS SEP
     *          or in other word registrating patient that used BPJS
     *          that init it's treatment
     * @return array("msg"=>"Message From BPJS Server","nosep"=>"SEP Number");  
     */
    public function reg_sep(){
        $sep=$this->getBPJSPostData();
        $dt=array("request"=>array("t_sep"=>$sep));
        $data=json_encode($dt);
        $sep['noReg']=$_POST['noReg'];
        
        $url=$this->url_base."SEP/insert";
        $methode="POST";
        $request=$data;
        $this->do_curl($this->port,$url,$methode,$request);
        if ($this->error) {
            $message= "cURL Error #:" . $this->error;
            $nosep="";
            return array("msg"=>$message,"nosep"=>$nosep);
        } else {
            return $this->formateCreateSEP($this->response,$sep);
        }
    }
    
    /**
     * @brief this used only to format the response from BPJS Server
     *          so it's fit with ResponsePackage data when Creating SEP
     * @database : smis_rg_sep
     * @param String $response : JSON String from BPJS 
     * @param data $insertsep : aray data of SEP that will insert into smis_rg_sep 
     * @return array("msg"=>"Message","nosep"=>"SEP Number if Succed if Fail the it will be blank");
     */
    public function formateCreateSEP($response,$insertsep){
        $json=json_decode($response,true);
        
        $msg="";
        if($json==null  ){
            $msg= "Pendaftaran Gagal, disebabkan Masalah Koneksi ke Server BPJS";
        }else if($json['metadata']['code']!="200"){
            $msg= $json['metadata']['message'];
        }else{
            $msg= "Pendaftaran Berhasil, dengan Nomor SEP : <strong>".$json['response']."</strong> , Tekan Tombol Cetak untuk Mencetak Form BPJS";
            $rsp['noSEP']=$json['response'];
            $insertsep['noSEP']=$json['response'];
            $insertsep['createBy']="sep";
            $this->getDBTable()->insert($insertsep);
            
            /* binding dengan noreg dan nosep */
            $this->cekBindingBPJS($rsp,$msg);
        }
        return array("msg"=>$msg,"response"=>$rsp);
    }
    
    /**
     * @brief untuk melakukan pengecekan ulang jika terdapat binding
     *          atau registrasi lama yang belum di pulanglan.
     * @return  array
     */
    public function fixRegister(){
        $rsp['noSEP']=$_POST['noSEP'];
        $msg="";
        $this->cekBindingBPJS($rsp,$msg);        
        return array("msg"=>$msg,"response"=>$rsp);
    }
    
    
    protected function getBPJSPostData(){
        $sep=array();
        $sep['noKartu']     =$_POST['noKartu'];
        $sep['tglSep']      =$_POST['tglSep'];
        $sep['tglRujukan']  =$_POST['tglRujukan'];
        $sep['noRujukan']   =$_POST['noRujukan'];
        $sep['ppkRujukan']  =$_POST['ppkRujukan'];
        $sep['ppkPelayanan']=$_POST['ppkPelayanan'];
        $sep['jnsPelayanan']=$_POST['jnsPelayanan'];
        $sep['diagAwal']    =$_POST['diagAwal'];
        $sep['poliTujuan']  =$_POST['poliTujuan'];
        $sep['klsRawat']    =$_POST['klsRawat'];
        $sep['lakaLantas']  =$_POST['lakaLantas'];
        $sep['lokasiLaka']  =$_POST['lokasiLaka'];
        $sep['catatan']     =$_POST['catatan'];
        $sep['user']        =$_POST['user'];
        $sep['noMr']        =$_POST['noMr'];
        return $sep;
    }
    
    
    /**
     * @brief this function to get detail of rujukan
     *          based on rujukan number.
     * @param $asal , default is empty but if there is rujukan that from puskesmas then search in rujukan rs
     * @return  array("table"=>"HTML Table,"raw"=>array data that contains BPJS data);
     */
    public function cek_rujukan($asal=""){
        $no_rujukan=isset($_POST['noRujukan'])?$_POST['noRujukan']:"";
        $nobpjs=isset($_POST['noBPJS'])?$_POST['noBPJS']:"";        
        $methode="GET";
        $request="";
        if($asal!=""){
            $asal = $asal."/";
        }
        if($no_rujukan!=""){
            $url=$this->url_base."Rujukan/".$asal.$no_rujukan;
        }else{
            $url=$this->url_base."Rujukan/".$asal."Peserta/".$nobpjs;
        }

        global $querylog;
        $querylog->addMessage($url);
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data=null;
        if ($this->error) {
            require_once "registration/function/bpjs_rujukan_null.php";
            $data=bpjs_rujukan_null("Terjadi Masalah Koneksi dengan Server BPJS");
        } else {
            $response=json_decode($this->response,true);
            if($response==null || $response['metadata']['code']!="200" ){
                require_once "registration/function/bpjs_rujukan_null.php";
                //if($asal==""){
                //    /* try search from another RS */
                //    return $this->cek_rujukan("RS/");
                //}
                $data=bpjs_rujukan_null($response['metadata']['message']);
            }else{
                require_once "registration/function/bpjs_rujukan_found.php";
                $data=bpjs_rujukan_found($response['response']);
            }
        }
        return $data;
    }
    
    
}
?>