<?php



class LapPasienTdkAktifBPJSResponder extends DBResponder {
    public function excel() {       
        global $db;
        $this->dbtable->setShowAll(true);
        $data = $this->dbtable->view("",0);
        $fix = $this->adapter->getContent($data['data']);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $file->getProperties ()->setCreator ( "SIMRS Inovasi Ide Utama" );
        $file->getProperties ()->setLastModifiedBy ( "SIMRS Bagian Administrator" );
        $file->getProperties ()->setTitle ( "Laporan Pasien Tidak Aktif BPJS" );
        $file->getProperties ()->setSubject ( "Laporan Pasien Tidak Aktif BPJS" );
        $file->getProperties ()->setDescription ( "Laporan Pasien Tidak Aktif BPJS Generated From SIMRS" );
        $file->getProperties ()->setKeywords ( "Laporan Pasien Tidak Aktif BPJS" );
        $file->getProperties ()->setCategory ( "Laporan Pasien Tidak Aktif BPJS" );
        $sheet = $file->getActiveSheet ();
        
        $row = 1;
        $sheet->setCellValue("A".$row, "LAPORAN PASIEN TIDAK AKTIF BPJS");
        $sheet->mergeCells("A".$row.":J".$row);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $row += 2;
        
        $sheet->setCellValue("A".$row, "Dari");
        $sheet->setCellValue("B".$row, ":");
        $sheet->setCellValue("C".$row, ArrayAdapter::format("date d M Y ", $_POST['dari']));
        $sheet->getStyle('A'.$row.':'.'C'.$row)->getFont()->setBold(true);
        $row++;
        
        $sheet->setCellValue("A".$row, "Sampai");
        $sheet->setCellValue("B".$row, ":");
        $sheet->setCellValue("C".$row, ArrayAdapter::format("date d M Y ", $_POST['sampai']));
        $sheet->getStyle('A'.$row.':'.'C'.$row)->getFont()->setBold(true);
        $row++;
        
        $sheet->setCellValue("A".$row, "Tanggal Cetak");
        $sheet->setCellValue("B".$row, ":");
        $sheet->setCellValue("C".$row, date("d-M-Y"));
        $sheet->getStyle('A'.$row.':'.'C'.$row)->getFont()->setBold(true);
        $row += 2;
        
        $sheet->setCellValue("A".$row, "No.");
        $sheet->setCellValue("B".$row, "NRM");
        $sheet->setCellValue("C".$row, "No.KTP");
        $sheet->setCellValue("D".$row, "No.BPJS");
        $sheet->setCellValue("E".$row, "Nama Pasien");
        $sheet->setCellValue("F".$row, "No.Register");
        $sheet->setCellValue("G".$row, "Tanggal Berobat");
        $sheet->setCellValue("H".$row, "Aktif");
        $sheet->setCellValue("I".$row, "Periode");
        $sheet->setCellValue("J".$row, "Poli");
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        $sheet->getStyle('A'.$row.':'.'J'.$row)->applyFromArray($styleArray);
        $row++;
        
        $no = 1;
        for($i = 0; $i < sizeof($fix); $i++) {
            $sheet->setCellValue("A".$row, $no);
            $sheet->setCellValue("B".$row, $fix[$i]["NRM"]);
            $sheet->setCellValue("C".$row, $fix[$i]["No.KTP"]);
            $sheet->setCellValue("D".$row, $fix[$i]["No.BPJS"]);
            $sheet->setCellValue("E".$row, $fix[$i]["Nama Pasien"]);
            $sheet->setCellValue("F".$row, $fix[$i]["No.Register"]);
            $sheet->setCellValue("G".$row, $fix[$i]["Tanggal Berobat"]);
            $sheet->setCellValue("H".$row, $fix[$i]["Aktif"]);
            $sheet->setCellValue("I".$row, $fix[$i]["Periode"]);
            $sheet->setCellValue("J".$row, $fix[$i]["Poli"]);
            $sheet->getStyle('A'.$row.':'.'J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A'.$row.':'.'J'.$row)->applyFromArray($styleArray);
            $row++;
            $no++;
        }
        
        $total_poli = $no - 1;
        $sheet->setCellValue("A".$row, "Total Jumlah Kunjungan Per Poli");
        $sheet->mergeCells("A".$row.":C".$row);
        $sheet->setCellValue("D".$row, $total_poli);
        $sheet->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row.':'.'D'.$row)->applyFromArray($styleArray);
        $row++;
        
        $dari = $_POST['dari'];
        $sampai = $_POST['sampai'];

        $query = "SELECT COUNT(id) as total 
                    FROM smis_rgv_layananpasien 
                    WHERE prop!='del' AND tanggal >= '".$dari."' AND tanggal < '".$sampai."' AND status_bpjs = '0' AND uri = '0'
                ";
        $res = $db->get_result($query);
        
        $sheet->setCellValue("A".$row, "Total Jumlah Kunjungan Per Klinik");
        $sheet->mergeCells("A".$row.":C".$row);
        $sheet->setCellValue("D".$row, $res[0]->total);
        $sheet->getStyle('A'.$row.':'.'D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$row.':'.'D'.$row)->getFont()->setBold(true);
        $sheet->getStyle('A'.$row.':'.'D'.$row)->applyFromArray($styleArray);
        $row++;
        
        foreach(range('A','J') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Pasien Tidak Aktif BPJS.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
    }
}

?>