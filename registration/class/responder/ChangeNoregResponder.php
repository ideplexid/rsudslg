<?php 
/**
 * the save data now embed with a function to broadcast
 * a command to change the whole database of the current patient
 * if the patient only have one noreg_pasien, it will only change the current noreg_patient
 * it have more than one noreg_patient it will change the whole data of noreg_patient
 * this file called in /registration/resource/php/laporan/replace_pasien.php
 * 
 * @author Nurul Huda
 * @copyright <goblooge@gmail.com>
 * @since 30 August 2016
 * @license LGPLv2
 * */
class ChangeNoregResponder extends DBResponder{
	public function save(){
		$ret=parent::save();
		$ser=new ServiceConsumer($this->getDBTable()->get_db(),"change_noreg");
        $ser->addData("noreg_pasien",$_POST['noreg_pasien']);
		$ser->addData("noreg_pasien_baru",$_POST['noreg_pasien_baru']);
		$ser->execute();
		return $ret;
	}
}

 ?>