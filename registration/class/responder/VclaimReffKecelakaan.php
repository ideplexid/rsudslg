<?php 
require_once "registration/class/responder/BPJSResponder.php";
class VclaimReffKecelakaan extends BPJSResponder{
    public function view(){	
		$d					 = $this->getKecelakaan($_POST['nokartu']);
		$page				 = 1;
		$data				 = $d;
		$max_page			 = 1;
		$this->adapter->setNumber($page*1000);
		$uidata				 = $this->adapter->getContent($data);
		$this->uitable->setContent($uidata);
		$list				 = $this->uitable->getBodyContent();
		$pagination			 = $this->uitable->getPagination($page,3,$max_page);		
		$json['list']		 = $list;
		if(!$this->debug) {
			unset($d['custom_kriteria']);
			unset($d['query']);
		}
		$json['debug']		 = $this->debug;
		$json['dbtable']	 = $d;
		if($this->include_adapter_data){
			$json['adapter'] = $uidata;
		}
		if($this->include_raw_data){
			$json['raw']	 = $data;
		}
		$json['pagination']	 = $pagination->getHtml();
		$json['number']		 = $page;
		$json['number_p']	 = "1";
		return $json;
	}

     public function getKecelakaan($reff){
         global $db;
         $url  = $this->url_base."sep/KllInduk/List/".$reff;
         
         $this->do_curl($this->port,$url,"GET",NULL);  
         if ($this->error) {
            return "cURL Error #:" . $this->error;
          } else {
              $data=json_decode($this->response,true);
              if($data==null ){
                  return array();
              }else if( $data['metaData']['code']!="200" ){
                  return array();
              }else{
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($data['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $repx = json_decode($hasil,true);
                    $rep = $repx['list'];
                }else{
                    $rep = $data['response'];
                }
                return $rep;
              }
          }

     }
}