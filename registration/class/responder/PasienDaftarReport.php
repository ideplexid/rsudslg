<?php 

class PasienDaftarReport extends DBReport{
	
    public function command($command){
        if($command=="get_total_id"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $result=$this->getTotalBackup();
            $pack->setContent($result);
            return $pack->getPackage();
        }else if($command=="backup_current_id"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $result=$this->getBackupCurrent();
            $pack->setContent($result);
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    public function getTotalBackup(){
        $query = "SELECT count(*) as total 
                 FROM smis_rg_vregister 
                 WHERE id NOT IN (
                     SELECT id FROM smis_rg_vregisterb
                 ) AND prop!='del' ";
        $total = $this->getDBTable()->get_db()->get_var($query);
        return $total;
    }
    
    public function getBackupCurrent(){
        $query = "SELECT * 
                 FROM smis_rg_vregister 
                 WHERE id NOT IN (
                     SELECT id FROM smis_rg_vregisterb
                 ) AND prop!='del' LIMIT 0,1";
        $x     = (array)$this->getDBTable()->get_db()->get_row($query);
        $this->dbtable->insert($x);
        return $x;
    }
    
    
	public function excel(){
		require_once "smis-libs-out/php-excel/PHPExcel.php";
		$file = new PHPExcel ();
		$file->getProperties ()->setCreator ( "Kunjungan Pasien" );
		$file->getProperties ()->setLastModifiedBy ( "Kunjungan Pasien Rumah Sakit" );
		$file->getProperties ()->setTitle ( "Data Kunjungan Pasien" );
		$file->getProperties ()->setSubject ( "Data Kunjungan Pasien" );
		$file->getProperties ()->setDescription ( "Data Kunjungan Pasien" );
		$file->getProperties ()->setKeywords ( "Kunjungan Pasien " );
		$file->getProperties ()->setCategory ( "Kunjungan Pasien" );
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
		$sheet->setTitle ("Detail Pasien");
        
		$this->getDBTable()->setDebuggable(true);
		$this->getDBTable()->setShowAll(true);
		$this->getDBTable()->setFetchMethode(DBTable::$OBJECT_FETCH);
        $this->getDBTable()->setOrder(" waktu ASC ");
        $this->setDebuggable(true);
		$package=$this->view("","0");
		
        $pasien = array();
		$list=$package['dbtable']['data'];
        
        $index=1;
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogo());
        $objDrawing->setCoordinates('A'.$index);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(70); 
        $objDrawing->setHeight(70);
        $objDrawing->setWorksheet($sheet);
        
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_title",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true)->setSize(14);
        $index++;
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_address",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true);
        $index++;
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_contact",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true);
        $index += 3;
		
        $border_start = $index;
		$sheet->setCellValue ( "A" . $index, "No.");
        $sheet->setCellValue ( "B" . $index, "Nama" );
        $sheet->setCellValue ( "C" . $index, "Kunjungan" );
        $sheet->setCellValue ( "D" . $index, "NRM" );
        $sheet->setCellValue ( "E" . $index, "No. BPJS" );
        $sheet->setCellValue ( "F" . $index, "L/P" );
        $sheet->setCellValue ( "G" . $index, "Umur" );
        $sheet->setCellValue ( "H" . $index, "Tanggal" );
        $sheet->setCellValue ( "I" . $index, "Alamat" );
		$sheet->setCellValue ( "J" . $index, "Provinsi" );
		$sheet->setCellValue ( "K" . $index, "Kabupaten" );
		$sheet->setCellValue ( "L" . $index, "Kecamatan" );
        $sheet->setCellValue ( "M" . $index, "Kelurahan" );
        $sheet->setCellValue ( "N" . $index, "Dusun" );
        $sheet->setCellValue ( "O" . $index, "Pasien" );
        $sheet->setCellValue ( "P" . $index, "Datang" );
		$sheet->setCellValue ( "Q" . $index, "Rujukan" );
		$sheet->setCellValue ( "R" . $index, "Perujuk" );
		$sheet->setCellValue ( "S" . $index, "Instansi" );
        $sheet->setCellValue ( "T" . $index, "Cara Bayar" );
		$sheet->setCellValue ( "U" . $index, "Rawat");
        $sheet->setCellValue ( "V" . $index, "Rawat Jalan");
        $sheet->setCellValue ( "W" . $index, "Kamar Inap");
        $sheet->setCellValue ( "X" . $index, "Pulang");
        $sheet->setCellValue ( "Y" . $index, "Perusahaan");
        $sheet->setCellValue ( "Z" . $index, "Asuransi");
        $sheet->setCellValue ( "AA" . $index, "Asal Data");
        $sheet->setCellValue ( "AB" . $index, "Telpon");
        
		$no=0;
		$sheet->getStyle ( "A".$index.":AA".$index)->getFont ()->setBold ( true );
		foreach($list as $x){
			$no++;
			$index++;
			$sheet->setCellValue ( "A" . $index, $no."." );
            $sheet->setCellValue ( "B" . $index, $x->nama );
            $sheet->setCellValue ( "C" . $index, $x->no_kunjungan );
            $sheet->setCellValue ( "D" . $index, $x->nrm==''?"":"'".$x->nrm );
            $sheet->setCellValue ( "E" . $index, $x->no_bpjs==''?"":"'".$x->no_bpjs );
            $sheet->setCellValue ( "F" . $index, $x->kelamin );
            $sheet->setCellValue ( "G" . $index, $x->umur );
            $sheet->setCellValue ( "H" . $index, ArrayAdapter::format("date d M Y H:i",$x->waktu)  );
            $sheet->setCellValue ( "I" . $index, $x->alamat );
			$sheet->setCellValue ( "J" . $index, $x->nama_provinsi );
			$sheet->setCellValue ( "K" . $index, $x->nama_kabupaten );
			$sheet->setCellValue ( "L" . $index, $x->nama_kecamatan );
			$sheet->setCellValue ( "M" . $index, $x->nama_kelurahan );
            $sheet->setCellValue ( "N" . $index, $x->nama_kedusunan );
            $sheet->setCellValue ( "O" . $index, $x->baru );
            $sheet->setCellValue ( "P" . $index, $x->caradatang);
			$sheet->setCellValue ( "Q" . $index, $x->rujukan );
			$sheet->setCellValue ( "R" . $index, $x->nama_rujukan );
			$sheet->setCellValue ( "S" . $index, $x->instansi_rujukan );
            $sheet->setCellValue ( "T" . $index, $x->carabayar );
            $sheet->setCellValue ( "U" . $index, $x->uri==0?"Rawat Jalan":"Rawat Inap" );
            $sheet->setCellValue ( "V" . $index, ArrayAdapter::format("unslug",$x->jenislayanan) );
            $sheet->setCellValue ( "W" . $index, ArrayAdapter::format("unslug",$x->kamar_inap) );
            $sheet->setCellValue ( "X" . $index, $x->carapulang );
            $sheet->setCellValue ( "Y" . $index, $x->nama_perusahaan );
            $sheet->setCellValue ( "Z" . $index, $x->nama_asuransi );
            $sheet->setCellValue ( "AA" . $index, ArrayAdapter::format("unslug",$x->origin) );
            $sheet->setCellValue ( "AB" . $index, ArrayAdapter::format("unslug",$x->telpon) );
            if(!isset($pasien[$x->nrm])){
                $pasien[$x->nrm]=array();
                $pasien[$x->nrm]['nrm']         = "'".$x->nrm;
                $pasien[$x->nrm]['nama']        = $x->nama;
                $pasien[$x->nrm]['alamat']      = $x->alamat;
                $pasien[$x->nrm]['provinsi']    = $x->nama_provinsi;
                $pasien[$x->nrm]['kabupaten']   = $x->nama_kabupaten;
                $pasien[$x->nrm]['kecamatan']   = $x->nama_kecamatan;
                $pasien[$x->nrm]['kelurahan']   = $x->nama_kelurahan;
                $pasien[$x->nrm]['kelamin']     = $x->kelamin ;
                $pasien[$x->nrm]['jumlah']      = 0;
            }
            $pasien[$x->nrm]['jumlah']++;
		}
		$sheet->getStyle ( 'A'.$border_start.':AB' . $index )->applyFromArray ( $fillcabang );
		
        $no=0;
        $index=1;
		$file->createSheet(NULL,1);
		$file->setActiveSheetIndex(1);
		$sheet = $file->getActiveSheet(1);
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogo());
        $objDrawing->setCoordinates('A'.$index);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(70); 
        $objDrawing->setHeight(70);
        $objDrawing->setWorksheet($sheet);
        
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_title",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true)->setSize(14);
        $index++;
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_address",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true);
        $index++;
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_contact",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true);
        $index += 3;
        
        $border_start = $index;
        $sheet->setTitle ("Jumlah Kunjungan");
		$sheet->setCellValue ( "A" . $index, "No.");
        $sheet->setCellValue ( "B" . $index, "NRM" );
        $sheet->setCellValue ( "C" . $index, "NAMA" );
		$sheet->setCellValue ( "D" . $index, "ALAMAT" );
		$sheet->setCellValue ( "E" . $index, "PROVINSI" );
		$sheet->setCellValue ( "F" . $index, "KABUPATEN" );
		$sheet->setCellValue ( "G" . $index, "KECAMATAN" );
		$sheet->setCellValue ( "H" . $index, "KELURAHAN" );
		$sheet->setCellValue ( "I" . $index, "L/P" );
		$sheet->setCellValue ( "J" . $index, "JUMLAH" );
        $sheet->getStyle ( "A".$index.":J".$index)->getFont ()->setBold ( true );
        foreach($pasien as $x){
            $no++;
            $index++;
            $sheet->setCellValue ( "A" . $index, $no."." );
            $sheet->setCellValue ( "B" . $index, $x['nrm'] );
            $sheet->setCellValue ( "C" . $index, $x['nama'] );
			$sheet->setCellValue ( "D" . $index, $x['alamat'] );
			$sheet->setCellValue ( "E" . $index, $x['provinsi'] );
			$sheet->setCellValue ( "F" . $index, $x['kabupaten'] );
			$sheet->setCellValue ( "G" . $index, $x['kecamatan'] );
			$sheet->setCellValue ( "H" . $index, $x['kelurahan'] );
			$sheet->setCellValue ( "I" . $index, $x['kelamin'] );
			$sheet->setCellValue ( "J" . $index, $x['jumlah'] );
        }
        $sheet->getStyle ( 'A'.$border_start.':J' . $index )->applyFromArray ( $fillcabang );
        
        
		$list=$package['ddiagram']['data'];
		$file->createSheet ( NULL, 2 );
		$file->setActiveSheetIndex ( 2 );
		$sheet = $file->getActiveSheet ( 2 );
        $index=1;
		$no=0;
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('logo_img');
        $objDrawing->setDescription('logo_img');
        $objDrawing->setPath(getLogo());
        $objDrawing->setCoordinates('A'.$index);
        $objDrawing->setOffsetX(20); 
        $objDrawing->setOffsetY(5);
        $objDrawing->setWidth(70); 
        $objDrawing->setHeight(70);
        $objDrawing->setWorksheet($sheet);
        
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_title",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true)->setSize(14);
        $index++;
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_address",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true);
        $index++;
        $sheet->setCellValue("C".$index, getSettings($db,"smis_autonomous_contact",""));
        $sheet->getStyle("C".$index)->getFont()->setBold(true);
        $index += 3;
        
        $border_start = $index;
		$sheet->setTitle ("Resume Pasien");
		$sheet->setCellValue ( "A" . $index, "No.");
		$sheet->setCellValue ( "B" . $index, "NAMA" );
		$sheet->setCellValue ( "C" . $index, "JUMLAH" );
		
		$sheet->getStyle ( "A".$index.":C".$index)->getFont ()->setBold ( true );
		foreach($list as $x){
			$no++;
			$index++;
			$sheet->setCellValue ( "A" . $index, $no."." );
			$sheet->setCellValue ( "B" . $index, $x->nama );
			$sheet->setCellValue ( "C" . $index, $x->jumlah );
		}
		$sheet->getStyle ( 'A'.$border_start.':C' . $index )->applyFromArray ( $fillcabang );
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="Data Kunjungan Pasien.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );
	}
	
}

?>