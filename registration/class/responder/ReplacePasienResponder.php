<?php 
/**
 * this class create for edit data
 * the edit data now include
 * an the noreg_patient of patient data
 * all of the noreg_patient of current
 * patient now will be included in edit data
 * this file called in /registration/resource/php/laporan/replace_pasien.php
 * 
 * @author Nurul Huda
 * @copyright <goblooge@gmail.com>
 * @since 30 August 2016
 * @license LGPLv2
 * */
class ReplacePasienResponder extends DBResponder{	
	
	public function edit(){
		$this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$row=parent::edit();
		$query="SELECT id FROM smis_rg_layananpasien WHERE nrm='".$_POST['id']."' AND prop!='del' ";
		$db=$this->dbtable->get_db();
		$res=$db->get_result($query);
		$idreg=array();
		foreach($res as $x){
			$idreg[]=$x->id*1;	
		}
		$row['noreg_pasien']=json_encode($idreg);
		return $row;
	}
	

}

?>