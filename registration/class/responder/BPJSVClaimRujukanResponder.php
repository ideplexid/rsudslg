<?php
/**
 * this class Spesifically for Handling Response and Request to BPJS SEP Server
 * this class can handle the create SEP request, Delete , Update , Cek, and etc
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.3
 * @since       : 09-Feb-2017
 * @database    : - smis_rg_sep_rujukan
 * 
 * 
 * */
require_once "registration/class/responder/BPJSVClaimCheckerResponder.php";
class BPJSVClaimRujukanResponder extends BPJSVClaimCheckerResponder{


    private function getDataInsertRujukan(){
        $data                   = array();
        $data['noSep']          = $_POST['nosep'];
        $data['tglRujukan']     = $_POST['tanggal'];
        $data['ppkDirujuk']     = $_POST['ppk'];
        $data['jnsPelayanan']   = $_POST['jenis'];
        $data['catatan']        = $_POST['catatan'];
        $data['diagRujukan']    = $_POST['diagnosa'];
        $data['tipeRujukan']    = $_POST['tipe'];
        $data['poliRujukan']    = $_POST['poli'];
        $data['user']           = $_POST['user'];
        return $data;
    }

    private function getDataUpdateRujukan(){
        $data                   = array();
        $data['noRujukan']      = $_POST['no_rujukan'];
        $data['noSep']          = $_POST['nosep'];
        $data['tglRujukan']     = $_POST['tanggal'];
        $data['ppkDirujuk']     = $_POST['ppk'];
        $data['jnsPelayanan']   = $_POST['jenis'];
        $data['catatan']        = $_POST['catatan'];
        $data['diagRujukan']    = $_POST['diagnosa'];
        $data['tipeRujukan']    = $_POST['tipe'];
        $data['poliRujukan']    = $_POST['poli'];
        $data['user']           = $_POST['user'];
        return $data;
    }

    public function updateRujukan(){
        $sep=$this->getDataUpdateRujukan();
        $dt=array("request"=>array("t_rujukan"=>$sep));
        $data=json_encode($dt);
        $sep['noReg']=$_POST['noReg'];
        
        $url=$this->url_base."Rujukan/update";
        $methode="POST";
        $request=$data;
        $this->do_curl($this->port,$url,$methode,$request);
        echo json_encode($this->response);
        if ($this->error) {
            $message= "cURL Error #:" . $this->error;
            $nosep="";
            return array("msg"=>$message,"nosep"=>$nosep,"data"=>$data);
        } else {
            return $this->formateUpdateRujukan($this->response,$sep,$request);
        }
    }

    public function save(){
        if(isset($_POST['no_rujukan']) && $_POST['no_rujukan']!==""){
            return $this->updateRujukan();
        }else{
            return $this->saveRujukan();
        }
        
    }

    public function saveRujukan(){
        $sep=$this->getDataInsertRujukan();
        $dt=array("request"=>array("t_rujukan"=>$sep));
        $data=json_encode($dt);
        $sep['noReg']=$_POST['noReg'];
        
        $url=$this->url_base."Rujukan/insert";
        $methode="POST";
        $request=$data;
       
        $this->do_curl($this->port,$url,$methode,$request);
        if ($this->error) {
            $message= "cURL Error #:" . $this->error;
            $nosep="";
            return array("msg"=>$message,"nosep"=>$nosep,"data"=>$data);
        } else {
            return $this->formateInsertRujukan($this->response,$sep,$request);
        }
    }

    public function delRujukan(){
        $sep['noRujukan']=$_POST['noRujukan'];
        $sep['user']=$_POST['user'];
        
        $dt=array("request"=>array("t_sep"=>$sep));
        $data=json_encode($dt);
        $sep['noReg']=$_POST['noReg'];
        
        $url=$this->url_base."Rujukan/insert";
        $methode="POST";
        $request=$data;
        $this->do_curl($this->port,$url,$methode,$request);
        if ($this->error) {
            $message= "cURL Error #:" . $this->error;
            $nosep="";
            return array("msg"=>$message,"nosep"=>$nosep,"data"=>$data);
        } else {
            return $this->formateInsertRujukan($this->response,$sep,$request);
        }
    }

    public function formateInsertRujukan($response,$data){
        $json=json_decode($response,true);        
        $msg="";
        if($json==null  ){
            $msg= "Pembuatan Gagal, disebabkan Masalah Koneksi ke Server BPJS";
        }else if($json['metaData']['code']!="200"){
            $msg= $json['metaData']['message'];
        }else{
            echo json_encode($json);
            $rsp['noRujukan']        = $json['response']['rujukan']['noRujukan'];
            $msg= "Pembuatan Berhasil, dengan Nomor Rujukan : <strong>".$rsp['noRujukan']."</strong> , Tekan Tombol Cetak untuk Mencetak Form Rujukan";
            $data['noRujukan']       = $rsp['noRujukan'];
            $data['namaRujukan']     = $json['response']['rujukan']['tujuanRujukan']['nama'];
            $this->getDBTable()->insert($data);
        }
        return array("msg"=>$msg,"response"=>$rsp);
    }

    public function formateUpdateRujukan($response,$data){
        $json=json_decode($response,true);        
        $msg="";
        if($json==null  ){
            $msg= "Pembuatan Gagal, disebabkan Masalah Koneksi ke Server BPJS";
        }else if($json['metaData']['code']!="200"){
            $msg= $json['metaData']['message'];
        }else{
            $rsp['noRujukan']        = $data['noRujukan'];
            $msg= "Pembaruan Berhasil, dengan Nomor Rujukan : <strong>".$rsp['noRujukan']."</strong> , Tekan Tombol Cetak untuk Mencetak Form Rujukan";
        }
        return array("msg"=>$msg,"response"=>$rsp);
    }

    public function formateDelRujukan($response,$data){
        $json=json_decode($response,true);        
        $msg="";
        if($json==null  ){
            $msg= "Pembuatan Gagal, disebabkan Masalah Koneksi ke Server BPJS";
        }else if($json['metaData']['code']!="200"){
            $msg= $json['metaData']['message'];
        }else{
            $rsp['noRujukan']        = $data['noRujukan'];
            $msg= "Penghapusan Berhasil, dengan Nomor Rujukan : <strong>".$rsp['noRujukan']."</strong> , Tekan Tombol Cetak untuk Mencetak Form Rujukan";
            $idx['noRujukan']=$data['noRujukan'];
            $prop['prop']="del";           
            $this->dbtable->update($prop,$idx);
        }
        return array("msg"=>$msg,"response"=>$rsp);
    }
    

}

?>