<?php 

require_once "registration/class/responder/BPJSResponder.php";
require_once "registration/class/responder/BPJSVClaimCheckerResponder.php";

class RefBPJSResponder extends BPJSVClaimCheckerResponder{
    
    public function command($command){
        if($command=="ref_poli"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setContent("");
            $msg=$this->proceedRefPoli();
            $pack->setWarning(true,"Warning",$msg);
            return $pack->getPackage();
        }else if($command=="ref_kamar"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setContent("");
            $msg=$this->proceedRefKamar();
            $pack->setWarning(true,"Warning",$msg);
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    private function proceedRefPoli(){
        $this->fetchRefPoli();
        $poli=$this->getRefPoli();

        if($poli==null){
            return "Error Fetching To BPJS Server, No Data Available";
        }
        $query="DELETE FROM ".$this->dbtable->getName()."  WHERE grup='poli'";
        $db=$this->getDBTable()->get_db();
        $db->query($query);
        $all_total=count($poli);
        $total=0;
        foreach($poli as $p){
            $data=array();
            $data['grup']="poli";
            $data['nama']=$p['nmPoli'];
            $data['kode']=$p['kdPoli'];
            $data['status']=1;
            $this->getDBTable()->insert($data);
            $total++;
        }
        return $total." Data Fetched From ".$all_total." Data Available";
    }
    
    private function proceedRefKamar(){
        $this->fetchRefKamar();
        $poli=$this->getRefKamar();
        if($poli==null){
            return "Error Fetching To BPJS Server, No Data Available";
        }
        $query="DELETE FROM ".$this->dbtable->getName()."  WHERE grup='kelas'";
        $db=$this->getDBTable()->get_db();
        $db->query($query);
        $all_total=count($poli);
        $total=0;
        foreach($poli as $p){
            $data=array();
            $data['grup']="kelas";
            $data['nama']=$p['nama'];
            $data['kode']=$p['kode'];
            $data['status']=1;
            $this->getDBTable()->insert($data);
            $total++;
        }
        return $total." Data Fetched From ".$all_total." Data Available";
    }
    
    
    /**
     * @brief fetch all poli reff
     * @return  string
     */
    public function fetchRefPoli(){
        $url=$this->url_base."referensi/poli/int";
        $metode="GET";
        $request="";
        $this->do_curl($this->port,$url,$metode,$request);
        if ($this->error) {           
          return "cURL Error #:" . $err;
        } else {
          return "success";
        }
    }
    
    /**
     * @brief get all poli ref
     * @return  array all poli reff
     */
    public function getRefPoli(){
        if($this->error){
            return null;
        }
        $json=json_decode($this->response,true);
        if($json['metaData']["code"]!="200"){
            return null;
        }
        return $json["response"]["list"];
    }
    
    
    /**
     * @brief fetch all kamar reff
     * @return  string
     */
    public function fetchRefKamar(){
        $url=$this->url_base."referensi/kelasrawat";
        $metode="GET";
        $request="";
        $this->do_curl($this->port,$url,$metode,$request);
        
        if ($this->error) {
          return "cURL Error #:" . $err;
        } else {
          return $this->response;
        }
    }
    
    /**
     * @brief get all kamar ref
     * @return  array all kamar reff
     */
    public function getRefKamar(){
        
        if($this->response==null){
            return null;
        }
       
        $json=json_decode($this->response,true);
        if($json['metaData']["code"]!="200"){
            return null;
        }
        return $json["response"]["list"];
    }
    
    /**
     * @brief fetch all kamar reff
     * @return  string
     */
    public function fetchDiagnosa(){
        $url=$this->url_base."diagnosa/ref/diagnosa/".$_POST['diagnosa'];
        $metode="GET";
        $request="";
        $this->do_curl($this->port,$url,$metode,$request);
        if ($this->error) {
          return "cURL Error #:" . $err;
        } else {
          return $this->response;
        }
    }
    
} 

?>