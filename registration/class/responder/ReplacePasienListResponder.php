<?php 
/**
 * the save data now embed with a function to broadcast
 * a command to change the whole database of the current patient
 * if the patient only have one noreg_pasien, it will only change the current noreg_patient
 * it have more than one noreg_patient it will change the whole data of noreg_patient
 * this file called in /registration/resource/php/laporan/replace_pasien.php
 * 
 * @author Nurul Huda
 * @copyright <goblooge@gmail.com>
 * @since 30 August 2016
 * @license LGPLv2
 * */
class ReplacePasienListResponder extends DBResponder{
	public function save(){
		$ret=parent::save();
		$json=$_POST['noreg_pasien'];
		$lnoreg=json_decode($json,true);
		$total=count($lnoreg);
		$ser=new ServiceConsumer($this->getDBTable()->get_db(),"set_update_patient");
		if($total==1){
			/*broadcast in Single Case*/			
			$ser->addData("noreg_pasien",$lnoreg[0]);
			$ser->addData("nrm_pasien_baru",$_POST['nrm_pasien_baru']);
			$ser->addData("nama_pasien_baru",$_POST['nama_pasien_baru']);
		}else{
			/*broadcast in Multiple Case*/
			$ser->addData("nrm_pasien",$_POST['nrm_pasien']);
			$ser->addData("nrm_pasien_baru",$_POST['nrm_pasien_baru']);
			$ser->addData("nama_pasien_baru",$_POST['nama_pasien_baru']);
		}
		$ser->execute();
		return $ret;
	}
}

 ?>