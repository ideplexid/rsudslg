<?php 

class AbsensiDokterResponder extends DBResponder{
	
	private $_JADWAL_DOKTER=null;
	private $_TOTAL_KODE;
	private $_TOTAL_JENIS;
	private $_TOTAL_SCHEDULE;
	private $_TOTAL_PX;
	private $_MAX_COLUMN;
	private $_DATE_MAX;
	private $_DAY_MAX;
	private $_DAY_MIN;
	private $_LAST_COLUMN_RESULT;
	private $_LAST_COLUMN_RESULT_PENGGANTI;
	private $_TOTAL_PENGGANTI;
	
	
	private function getJadwalDokter(){
		if($this->_JADWAL_DOKTER==null){
			$this->_JADWAL_DOKTER=RegistrationResource::getJadwalDokter($this->getDBTable()->get_db());
		}
		return $this->_JADWAL_DOKTER;
	}
	
	public function excel(){
		ini_set('memory_limit','256M');
		global $user;
		require_once "smis-libs-out/php-excel/PHPExcel.php";		
		$file = new PHPExcel ();
		$file->getProperties ()->setCreator ( "Goblooge" );
		$file->getProperties ()->setLastModifiedBy ( "Nurul Huda" );
		$file->getProperties ()->setTitle ( "Data Keluarga" );
		$file->getProperties ()->setSubject ( "Inheritance Keluarga" );
		$file->getProperties ()->setDescription ( "Data Inheritance Keluarga" );
		$file->getProperties ()->setKeywords ( "Keluarga Nurul Huda" );
		$file->getProperties ()->setCategory ( "Keluarga" );
		
		//$file->createSheet ( NULL,0);
		$file->setActiveSheetIndex ( 0 );
		$view = $file->getActiveSheet ( 0 );
		$this->_TOTAL_PX=$this->createView($view);
		
		$file->createSheet (1);
		$file->setActiveSheetIndex ( 1 );
		$schedule = $file->getActiveSheet (1);		
		$this->_TOTAL_JENIS=$this->createJenisPasien($schedule);
		$this->_TOTAL_KODE=$this->createKODEDokter($schedule);
		$this->_TOTAL_SCHEDULE=$this->createJadwalDokter($schedule);
		$this->_TOTAL_PENGGANTI=$this->createPengganti($schedule);
		
		$file->createSheet (2);
		$file->setActiveSheetIndex ( 2 );
		$settings = $file->getActiveSheet (2);	
		$this->_MAX_COLUMN=$this->createSettings($settings);
		$this->createSetup($settings);		
		$this->createFormulaView($view);
		
		$this->_DATE_MAX=$settings->getCell("B11")->getCalculatedValue();
		$this->_LAST_COLUMN_RESULT=$settings->getCell("B30")->getCalculatedValue();
		$this->_LAST_COLUMN_RESULT_PENGGANTI=$settings->getCell("B37")->getCalculatedValue();
		
		$file->createSheet (3);
		$file->setActiveSheetIndex ( 3 );
		$result = $file->getActiveSheet (3);	
		$this->createResult($result);
		
		$file->createSheet (4);
		$file->setActiveSheetIndex ( 4 );
		$result = $file->getActiveSheet (4);	
		$this->createFinal($result);
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="LAP_ABD_DOK.xls"' ); 
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->setPreCalculateFormulas(FALSE);
		$writer->save ( 'php://output' );
		return NULL;
	}
	
	public function createResult($sheet){		
		$thin = array ();
		$thin['borders']=array();
		$thin['borders']['allborders']=array();
		$thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$sheet->setTitle("RESULT");
		$sheet->mergeCells("A1:A3")->setCellValue("A1","NO.");
		$sheet->mergeCells("B1:B3")->setCellValue("B1","TANGGAL");
		$sheet->setCellValueExplicit("C1",'=IF(COLUMN()=3;"KODE DOKTER";IF(COLUMN()=SETTINGS!$B$18+1;"TOTAL";""))');
		$sheet->setCellValueExplicit("C2",'=IF(COLUMN()>SETTINGS!$B$18;"";VLOOKUP(CEILING((COLUMN()-2)/SETTINGS!$B$16;1);INDIRECT(SETTINGS!$B$21);2;FALSE))');
		$sheet->setCellValueExplicit("C3",'=IF(COLUMN()>SETTINGS!$B$18;"";VLOOKUP(MOD(COLUMN();SETTINGS!$B$16);INDIRECT(SETTINGS!$B$20);2;FALSE))');
		$sheet->setCellValueExplicit("C4",'=IF(COLUMN()=SETTINGS!$B$18+1;IF(ROW()-1>SETTINGS!$B$14;"";SUM(INDIRECT(CONCATENATE("C";ROW();":";SETTINGS!$B$19;ROW()))));IF(COLUMN()>SETTINGS!$B$18;"";IF($B4="-";SUM(INDIRECT(SETTINGS!C$4));IF($B4<>"";COUNTIF(INDIRECT(SETTINGS!$B$12);CONCATENATE(YEAR($B4);"-";MONTH($B4);"-";DAY($B4);"-";C$2;"-";C$3));""))))');
		
		$start=3;
		for($i=0;$i<=$this->_DATE_MAX;$i++ ){
			$start++;
			$sheet->setCellValue("A".$start,'=IF(ROW()-SETTINGS!$B$14<=0,ROW()-SETTINGS!$B$13,IF(ROW()-SETTINGS!$B$14=1,"TOTAL";""))');
			$sheet->setCellValue("B".$start,'=IF(A'.$start.'="TOTAL","-",IF(A'.$start.'<>"",SETTINGS!$B$9+(A'.$start.'-1),""))');
		}
		$sheet	->getStyle("B4:B".$start)
				->getNumberFormat()
				->setFormatCode("dd-mm-yyyy hh:mm");
		$sheet	->getStyle("A1:".$this->_LAST_COLUMN_RESULT.$start)->applyFromArray ($thin);
		return $start;		
	}
	
	private function createFinal($sheet){
		$thin = array ();
		$thin['borders']=array();
		$thin['borders']['allborders']=array();
		$thin['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$sheet->setTitle("FINAL RESULT");
		$sheet->mergeCells("A1:A4")->setCellValue("A1","NO.");
		$sheet->mergeCells("B1:B4")->setCellValue("B1","TANGGAL");
		$sheet->setCellValueExplicit("C1",'=IF(COLUMN()=3;"KODE DOKTER";IF(COLUMN()=SETTINGS!$B$32+1;"TOTAL";""))');
		$sheet->setCellValueExplicit("C2",'=IF(COLUMN()>SETTINGS!$B$32;"";VLOOKUP(CEILING((COLUMN()-2)/(SETTINGS!$B$16*3);1);INDIRECT(SETTINGS!$B$21);2;FALSE))');
		$sheet->setCellValueExplicit("C3",'=IF(COLUMN()>SETTINGS!$B$32;"";VLOOKUP(MOD(COLUMN();SETTINGS!$B$16);INDIRECT(SETTINGS!$B$20);2;FALSE))');
		$sheet->setCellValueExplicit("C4",'=IF(COLUMN()>SETTINGS!$B$32;"";IF(MOD(CEILING((COLUMN()-2)/SETTINGS!$B$16;1);3)=1;"P";IF(MOD(CEILING((COLUMN()-2)/SETTINGS!$B$16;1);3)=2;"S";"M")))');
		$sheet->setCellValueExplicit("C5",'=IF(COLUMN()=SETTINGS!$B$32+1;IF(ROW()-1>SETTINGS!$B$33;"";SUM(INDIRECT(CONCATENATE("C";ROW();":";SETTINGS!$B$34;ROW()))));IF(COLUMN()>SETTINGS!$B$32;"";IF($B5="-";SUM(INDIRECT(SETTINGS!C$5));IF($B5<>"";COUNTIF(INDIRECT(SETTINGS!$B$35);CONCATENATE(YEAR($B5);"-";MONTH($B5);"-";DAY($B5);"-";C$2;"-";C$3;"-";C$4));""))))');
		
		$start=4;
		for($i=0;$i<=$this->_DATE_MAX;$i++ ){
			$start++;
			$sheet->setCellValue("A".$start,'=IF(ROW()-SETTINGS!$B$14-1<=0;ROW()-SETTINGS!$B$13-1;IF(ROW()-SETTINGS!$B$14-1=1;"TOTAL";""))');
			$sheet->setCellValue("B".$start,'=IF(A'.$start.'="TOTAL","-",IF(A'.$start.'<>"",SETTINGS!$B$9+(A'.$start.'-1),""))');
		}
		$sheet	->getStyle("B5:B".$start)
				->getNumberFormat()
				->setFormatCode("dd-mm-yyyy hh:mm");
		$sheet	->getStyle("A1:".$this->_LAST_COLUMN_RESULT_PENGGANTI.$start)->applyFromArray ($thin);
		return $start;		
	}
	
	private function createPengganti($sheet){
		$dari=$_POST["dari"];
		$sampai=$_POST["sampai"];	
		$dbtable=new DBTable($this->dbtable->get_db(),"smis_rg_dokter_ganti");
		$dbtable->setShowAll(true);
		$dbtable->addCustomKriteria("tanggal>=","'".$dari."'");
		$dbtable->addCustomKriteria("tanggal<","'".$sampai."'");
		$data=$dbtable->view("","0");
		$list=$data['data'];
		$number=1;
		$sheet->setCellValue("O1","KODE GANTI");
		foreach($list as $x){
			$number++;
			$dto = new DateTime($x->tanggal);
			$dateVal = PHPExcel_Shared_Date::PHPToExcel($dto);
			$sheet ->getStyle("O".$number)
					->getNumberFormat()
					->setFormatCode("dd-mm-yyyy");
			
			
			$sheet->setCellValue("O".$number,$dateVal);
			$sheet->setCellValue("P".$number,$x->shift);
			$sheet->setCellValue("Q".$number,'=CONCATENATE(P'.$number.',"-",YEAR(O'.$number.'),"-",MONTH(O'.$number.'),"-",DAY(O'.$number.'))');
			$sheet->setCellValue("R".$number,$x->kode);
		}
		return $number;
	}
	
	private function createSettings($sheet){
		$total_column=$this->_TOTAL_KODE * $this->_TOTAL_JENIS+2;
		$sheet	->setTitle("SETTINGS");
		$sheet	->setCellValue("A1","COLUMN NAME IN RESULT");
		$total_loop=$this->_TOTAL_KODE * $this->_TOTAL_JENIS*3+2;
		for($i=0;$i<=$total_column;$i++){
			$sheet->setCellValueByColumnAndRow($i,2,($i+1));
			$sheet->setCellValueByColumnAndRow($i,3,'=SUBSTITUTE(ADDRESS(1,COLUMN(),4),"1","")');
			$sheet->setCellValueByColumnAndRow($i,4,'=CONCATENATE(SUBSTITUTE(ADDRESS(1,COLUMN(),4),"1",""),"4:",SUBSTITUTE(ADDRESS(1,COLUMN(),4),"1",""),$B$14)');
		}
		
		for($i=0;$i<=$total_loop;$i++){
			$sheet->setCellValueByColumnAndRow($i,5,'=CONCATENATE(SUBSTITUTE(ADDRESS(1,COLUMN(),4),"1",""),"5:",SUBSTITUTE(ADDRESS(1,COLUMN(),4),"1",""),$B$33)');
		}
		
		
		return $total_column;
	}
	
	private function createKODEDokter($sheet){
		$sheet->mergeCells("K1:M1")->setCellValue("K1","KODE DOKTER");			  
		require_once "registration/class/RegistrationResource.php";
		$kodedr=RegistrationResource::getKodeDokter($this->getJadwalDokter());
		$start=1;
		foreach($kodedr as $x){
			$start++;
			$sheet	->setCellValue ( "K".$start, ($start-1) )
					->setCellValue ( "L".$start, $x )
					->setCellValue ( "M".$start, '=COUNTIF(INDIRECT(SETTINGS!$B$25),L'.$start.')' );
		}
		return $start-1;
	}
	
	private function createSetup($sheet){
		
		$dto = new DateTime($this->_DAY_MAX);
		$dayMax = PHPExcel_Shared_Date::PHPToExcel($dto);
		$dto = new DateTime($this->_DAY_MIN);
		$dayMin = PHPExcel_Shared_Date::PHPToExcel($dto);
		
		$sheet->setCellValue("A6","TOTAL DATA")					->setCellValue("B6",'=COUNTA(VIEW!A2:A'.$this->_TOTAL_PX.')+2');
		$sheet->setCellValue("A7","ROWSTART")					->setCellValue("B7",'VIEW!A2');
		$sheet->setCellValue("A8","ROWEND")						->setCellValue("B8",'=CONCATENATE("VIEW!A",B6)');
		
		
		$sheet->setCellValue("A9","START DATE");
		$sheet->setCellValue("A10","END DATE");
		$sheet	->getStyle("B9:B10")
				->getNumberFormat()
				->setFormatCode("dd-mm-yyyy hh:mm");
		$sheet	->setCellValue("B9",$dayMin);
		$sheet	->setCellValue("B10",$dayMax);
		
		
		$sheet->setCellValue("A11","DIFFERENT")					->setCellValue("B11",'=DATEDIF(B9,B10,"d")+1');
		$sheet->setCellValue("A12","COUNTRANGE")				->setCellValue("B12",'=CONCATENATE("VIEW!F","2:F",B6)');
		$sheet->setCellValue("A13","UNUSEDROW")					->setCellValue("B13",'3');
		$sheet->setCellValue("A14","ROWDIFF")					->setCellValue("B14",'=B11+B13');
		$sheet->setCellValue("A15","UNUSEDCOLUMN")				->setCellValue("B15",'2');
		$sheet->setCellValue("A16","CARABAYAR PASIEN")			->setCellValue("B16",$this->_TOTAL_JENIS);//=COUNTA((SCHEDULE!G2:G30))
		$sheet->setCellValue("A17","JENIS KODE")				->setCellValue("B17",$this->_TOTAL_KODE);//=COUNTA(SCHEDULE!K2:K35)
		$sheet->setCellValue("A18","MAXIMUM COLUMN")			->setCellValue("B18",'=B17*B16+B15');
		$sheet->setCellValue("A19","LAST COLUMN SUM")			->setCellValue("B19",'=SUBSTITUTE(ADDRESS(1,B18,4),"1","")');
		$sheet->setCellValue("A20","SETTING LOOKUP PASIEN")		->setCellValue("B20",'=CONCATENATE("SCHEDULE!","$G$2:$H$",1+B16)');
		$sheet->setCellValue("A21","SETTING LOOKUP DOKTER")		->setCellValue("B21",'=CONCATENATE("SCHEDULE!","$K$2:$L$",1+B17)');
		$sheet->setCellValue("A22","TOTAL SCHEDULE")			->setCellValue("B22",$this->_TOTAL_SCHEDULE);//=COUNTA(SCHEDULE!A2:A89)
		$sheet->setCellValue("A23","SETTING LOOKUP SCHEDULE")	->setCellValue("B23",'=CONCATENATE("SCHEDULE!","$D$2:$E$",B22+1)');
		$sheet->setCellValue("A24","VIEW CARABAYAR RANGE")		->setCellValue("B24",'=CONCATENATE("VIEW!","$B$2:$B$",B6)');
		$sheet->setCellValue("A25","VIEW KODE DOKTER RANGE")	->setCellValue("B25",'=CONCATENATE("VIEW!","$G$2:$G$",B6)');
		$sheet->setCellValue("A26","VIEW KODE DOKTER SCHEDULE")	->setCellValue("B26",'=CONCATENATE("VIEW!","$H$2:$H$",B6)');
		$sheet->setCellValue("A27","ERROR BY JENIS PASIEN")		->setCellValue("B27",'=B6-1-SUM(INDIRECT(CONCATENATE("SCHEDULE!","I2:I",B16+1)))');
		$sheet->setCellValue("A28","ERROR BY DOKTER")			->setCellValue("B28",'=B6-1-SUM(INDIRECT(CONCATENATE("SCHEDULE!","M2:M",B17+1)))');
		$sheet->setCellValue("A29","ERROR BY SCHEDULE")			->setCellValue("B29",'=B6-1-SUM(INDIRECT(CONCATENATE("SCHEDULE!","F2:F",B22+1)))');
		$sheet->setCellValue("A30","LAST COLUMN RESULT")		->setCellValue("B30",'=SUBSTITUTE(ADDRESS(1,B18+1,4),"1","")');
		
		$sheet->setCellValue("A31","LOOKUP GANTI")				->setCellValue("B31",'SCHEDULE!$Q$2:$R$3');
		$sheet->setCellValue("A32","MAX COLUMN PENGGANTI")		->setCellValue("B32",'=(B17*B16)*3+B15');
		$sheet->setCellValue("A33","ROW DIFF PENGGANTI")		->setCellValue("B33",'=B14+1');
		$sheet->setCellValue("A34","LAST COLUMN SUM PENGGANTI")	->setCellValue("B34",'=SUBSTITUTE(ADDRESS(1,B32,4),"1","")');
		$sheet->setCellValue("A35","COUNTRANGE PENGGANTI")		->setCellValue("B35",'=CONCATENATE("VIEW!K","2:K",B6)');
		$sheet->setCellValue("A36","RANGE CODE GANTI")			->setCellValue("B36",'SCHEDULE!$Q$2:$R$'.$this->_TOTAL_PENGGANTI);
		$sheet->setCellValue("A37","LAST COLUMN SUM BORDER")	->setCellValue("B37",'=SUBSTITUTE(ADDRESS(1,B32+1,4),"1","")');
		
		
	 }
	
	
	private function createJadwalDokter($sheet){
        loadLibrary("smis-libs-function-time");
		$sheet->mergeCells("A1:F1")->setCellValue("A1","SCHEDULE SETTINGS");			  
		require_once "registration/class/RegistrationResource.php";
		$schedule=$this->getJadwalDokter();
		$start=1;
		foreach($schedule as $x){
			$start++;
			$sheet	->setCellValue ( "A".$start, $x->hari )
					->setCellValue ( "B".$start, reverseDayName($x->hari)+1 )
					->setCellValue ( "C".$start, $x->jam_mulai)
					->setCellValue ( "D".$start, '=CONCATENATE(IF(VALUE(LEFT(C'.$start.',2))<14,"P",IF(VALUE(LEFT(C'.$start.',2))<18,"S","M")),"-",B'.$start.')')
					->setCellValue ( "E".$start,  $x->kode)
					->setCellValue ( "F".$start, '=COUNTIF(INDIRECT(SETTINGS!$B$26),CONCATENATE(D'.$start.',"-",E'.$start.'))' );
		}
		return $start-1;
	}
	
	private function createJenisPasien($sheet){
		$sheet->setTitle("SCHEDULE");
		$sheet->mergeCells("G1:I1")->setCellValue("G1","JENIS PASIEN");			  
		require_once "registration/class/RegistrationResource.php";
		$jenispx=RegistrationResource::getJenisPembayaran();
		$start=1;
		foreach($jenispx as $x){
			$start++;
			$sheet	->setCellValue ( "G".$start, ($start-2) )
					->setCellValue ( "H".$start, $x['value'] )
					->setCellValue ( "I".$start, '=COUNTIF(INDIRECT(SETTINGS!$B$24),H'.$start.')' );
		}
		return $start-1;
	}
	
	private function createView($sheet){
		$dari=$_POST["dari"];
		$sampai=$_POST["sampai"];	
		$sheet->setTitle("VIEW");		
		$sheet	->setCellValue ( "A1", "WAKTU" )
				->setCellValue ( "B1", "CARABAYAR" )
				->setCellValue ( "C1", "NO. RM" )
				->setCellValue ( "D1", "JAM" )
				->setCellValue ( "E1", "SHIFT" )
				->setCellValue ( "F1", "FULL KODE" )
				->setCellValue ( "G1", "KODE" )
				->setCellValue ( "H1", "DOKTER HARIAN" )
				->setCellValue ( "I1", "KODE GANTI" )
				->setCellValue ( "J1", "DOKTER GANTI" )
				->setCellValue ( "K1", "FULL KODE GANTI" );
				
		$start=1;
		$dbtable=$this->getDBTable();
		$dbtable->setShowAll(true);
		$dbtable->addCustomKriteria(" tanggal ",">='".$dari."'");
		$dbtable->addCustomKriteria(" tanggal ","<'".$sampai."'");
		$list=$dbtable->view("","0");
		$data=$list['data'];
		
		
		foreach($data as $x){
			
			if($this->_DAY_MAX==null || $this->_DAY_MAX<$x->tanggal){
				$this->_DAY_MAX=$x->tanggal;
			}			
			if($this->_DAY_MIN==null || $this->_DAY_MIN>$x->tanggal){
				$this->_DAY_MIN=$x->tanggal;
			}
			
			$start++;
			$dto = new DateTime($x->tanggal);
			$dateVal = PHPExcel_Shared_Date::PHPToExcel($dto);
			$sheet ->getStyle("A".$start)
					->getNumberFormat()
					->setFormatCode("dd-mm-yyyy hh:mm");
			$sheet	->setCellValue ( "A".$start, $dateVal )
					->setCellValue ( "B".$start, $x->carabayar )
					->setCellValue ( "C".$start, "'".ArrayAdapter::format("only-digit8",$x->nrm) )
					->setCellValue ( "D".$start, '=HOUR(A'.$start.')' )
					->setCellValue ( "E".$start, '=IF(D'.$start.'<14,"P",IF(D'.$start.'<18,"S","M")))' )
					->setCellValue ( "H".$start, '=CONCATENATE(E'.$start.',"-",WEEKDAY(A'.$start.'),"-",G'.$start.')' )					
					->setCellValue ( "I".$start, '=CONCATENATE(E'.$start.',"-",YEAR(A'.$start.'),"-",MONTH(A'.$start.'),"-",DAY(A'.$start.'))' )
					->setCellValue ( "K".$start, '=CONCATENATE(YEAR(A'.$start.'),"-",MONTH(A'.$start.'),"-";DAY(A'.$start.'),"-",IF(J'.$start.'="-",G'.$start.',J'.$start.'),"-",B'.$start.',"-",E'.$start.')' );
		}
		return $start-1;
	}
	
	public function createFormulaView($sheet){
		$start=1;
		for($i=0;$i < $this->_TOTAL_PX ;$i++){
			$start++;
			$sheet->setCellValueExplicit("G".$start,'=VLOOKUP(CONCATENATE(E'.$start.';"-";WEEKDAY(A'.$start.'));INDIRECT(SETTINGS!$B$23);2;FALSE)');
			//$sheet->setCellValue ( "G".$start, '=VLOOKUP(I'.$start.',SCHEDULE!D$2:$E$'.($this->_TOTAL_SCHEDULE+1).',2,FALSE)' );//
			$sheet->setCellValueExplicit ( "F".$start, '=CONCATENATE(YEAR(A'.$start.');"-";MONTH(A'.$start.');"-";DAY(A'.$start.');"-";VLOOKUP(CONCATENATE(E'.$start.';"-";WEEKDAY(A'.$start.'));INDIRECT(SETTINGS!$B$23);2;FALSE);"-";B'.$start.')');
			$sheet->setCellValueExplicit ( "J".$start, '=IFERROR(VLOOKUP(I'.$start.';INDIRECT(SETTINGS!$B$36);2;FALSE);"-")' );
					
		}					
	}
	
	
	
	
}


?>