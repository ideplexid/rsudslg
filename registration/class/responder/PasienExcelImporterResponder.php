<?php 

class PasienExcelImporterResponder extends DBResponder{
    /* @var ExcelImporter */
    private $importer;
    
    public function command($command){
        if($command=="import_proceed"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $content=$this->import_proceed();
            $pack->setContent("");
            $pack->setWarning(true,"Import Done"," Total Data ".$content." Imported");
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    private function import_proceed(){
        $id=$_POST['id'];
        $row=$this->dbtable->select($id);
        $this->importer->setFilePath($row->file);
        if($row->max_memory!=""){
            ini_set('memory_limit',$row->max_memory);
        }
        if($row->max_time*1>0){
            ini_set('max_execution_time',$row->max_time);
        }
        $column_map=array();
        $column_map[0]="nama";
        $column_map[1]="kelamin";
        $column_map[2]="ktp";
        $column_map[3]="alamat";
        $column_map[4]="rt";
        $column_map[5]="rw";
        $column_map[6]="nama_provinsi";
        $column_map[7]="nama_kabupaten";
        $column_map[8]="nama_kecamatan";
        $column_map[9]="nama_kelurahan";
        $column_map[10]="tempat_lahir";
        $column_map[11]="tgl_lahir";
        $column_map[12]="telpon";
        for($i=0;$i<$row->sheets;$i++){
            $this->importer->addSetup($i,3,$column_map,null);
        }
        $this->importer->addKey("nama");
        $this->importer->addKey("ktp");
        $this->importer->addKey("alamat");
        $this->importer->addFixValue("tanggal",$row->tanggal);
        //$this->importer->truncate();
        return $this->importer->execute();
    }
    
    public function postToArray(){
        $result=parent::postToArray();
        $this->importer->setFilePath($result['file']);
        $sheet=$this->importer->countSheet();
        $result['sheets']=$sheet;
        return $result;
    }
    
    public function setImporter(PasienExcelImporter $importer){
        $this->importer=$importer;
        return $this;
    }
    
}


?>