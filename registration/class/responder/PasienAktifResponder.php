<?php

function sortWaktu($item1,$item2){
	if ($item1['waktu'] == $item2['waktu']) return 0;
	return ($item1['waktu'] > $item2['waktu']) ? 1 : -1;
}

class PasienAktifResponder extends DBResponder{
	
	public function command($command){
		if($command=="track"){
			$pack=new ResponsePackage();
			$content=$this->getTraker();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
	}
	
	public function getTraker(){
		require_once 'smis-base/smis-include-service-consumer.php';
		$serv=new ServiceConsumer($this->getDBTable()->get_db(), "track_pasien");
		$serv->addData("noreg_pasien", $_POST['id']);
		$serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
		$serv->execute();
		$res=$serv->getContent();
		
		usort($res,'sortWaktu');
		$table=new TablePrint();
		$table->setMaxWidth(false);
		$table->setDefaultBootrapClass(true);
		$table->addColumn("<strong>No.</strong>", 1, 1);
		$table->addColumn("<strong>Waktu</strong>", 1, 1);
		$table->addColumn("<strong>Ruangan</strong>", 1, 1);
		$table->addColumn("<strong>Status</strong>", 1, 1);
		$table->addColumn("<strong>Dokumen</strong>", 1, 1);
		$table->commit("header");		
		$no=0;
		foreach($res as $one){
			$table->addColumn(++$no.".", 1, 1);
			$table->addColumn(ArrayAdapter::format("date d M Y H:i:s", $one['waktu']), 1, 1);
			$table->addColumn(ArrayAdapter::format("unslug", $one['ruangan']), 1, 1);
			$table->addColumn($one['cara'], 1, 1);
			$table->addColumn($one['dokumen'], 1, 1);
			$table->commit("body");			
		}
		return $table->getHtml();
	}
	
}

?>