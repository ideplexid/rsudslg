<?php 
class KarcisResponder extends DBResponder {

        

    public function excel(){
		require_once "smis-libs-out/php-excel/PHPExcel.php";
		$file = new PHPExcel ();
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
		$sheet->setTitle ("Pendapatan Karcis");
		
		$this->getDBTable()->setShowAll(true);
		$this->getDBTable()->setFetchMethode(DBTable::$OBJECT_FETCH);
		$package=$this->view("","0");
		
		$list=$package['dbtable']['data'];
        $sheet->mergeCells("A1:J1")->setCellValue ( "A" . 1, "Pendapatan Karcis Periode ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

		$sheet->setCellValue ( "A" . 2, "Tanggal");
		$sheet->setCellValue ( "B" . 2, "Nama" );
		$sheet->setCellValue ( "C" . 2, "NRM" );
		$sheet->setCellValue ( "D" . 2, "No Reg" );
		$sheet->setCellValue ( "E" . 2, "Ruangan" );
		$sheet->setCellValue ( "F" . 2, "Jenis Pasien" );
		$sheet->setCellValue ( "G" . 2, "Kamar Inap" );
		$sheet->setCellValue ( "H" . 2, "URI/URJ" );
		$sheet->setCellValue ( "I" . 2, "Karcis" );
		$sheet->setCellValue ( "J" . 2, "Administrasi" );   
        

        $index=2;
		$no=0;
		$sheet->getStyle ( "A1:J2")->getFont ()->setBold ( true );
		foreach($list as $x){
			$no++;
			$index++;
			$sheet->setCellValue ( "A" . $index, ArrayAdapter::format("date d M Y H:i",$x->tanggal) );
			$sheet->setCellValue ( "B" . $index, $x->nama_pasien );
			$sheet->setCellValue ( "C" . $index, "'".$x->nrm );
			$sheet->setCellValue ( "D" . $index, $x->id );
			$sheet->setCellValue ( "E" . $index, ArrayAdapter::format("unslug",$x->jenislayanan) );
			$sheet->setCellValue ( "F" . $index, ArrayAdapter::format("unslug",$x->carabayar) );
			$sheet->setCellValue ( "G" . $index, ArrayAdapter::format("unslug",$x->kamar_inap) );
			$sheet->setCellValue ( "H" . $index, $x->uri=="0"?"URJ":"URI" );
			$sheet->setCellValue ( "I" . $index, $x->karcis );
			$sheet->setCellValue ( "J" . $index, $x->administrasi_inap );   
        }
        $index++;
        $sheet->setCellValue ( "H" . $index, "Total" );
		$sheet->setCellValue ( "I" . $index, "=sum(I3:I".($index-1).")" );   
        $sheet->setCellValue ( "J" . $index, "=sum(J3:J".($index-1).")" );
        $sheet->getStyle ( 'A2:J' . $index )->applyFromArray ( $fillcabang );
        $sheet->getStyle ( "A".$index.":J".$index)->getFont ()->setBold ( true );
        $sheet->getStyle('I3:J'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="Data Pendapatan Karcis.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );
	}
}