<?php 
/**
 * this class Spesifically for Handling Response and Request to BPJS SEP Server
 * this class can handle the create SEP request, Delete , Update , Cek, and etc
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.3
 * @since       : 09-Feb-2017
 * @database    : - smis_rg_layananpasien
 * 
 * */

require_once "registration/class/responder/BPJSResponder.php";
class BPJSVClaimCheckerResponder extends BPJSResponder{ 

    function __construct(DBTable $dbtable,Table $uitable=NULL,ArrayAdapter $adapter=NULL){
        parent::__construct($dbtable,$uitable,$adapter);
        $this->error            = null;
        $this->response         = null;
        $this->consumer_id      = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-cons-id","24619");
        $this->consumer_secret  = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-cons-secret","6vOEA10FF6");
        $this->ppkPelayanan     = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-ppk-pelayanan","1334R001");
        $this->url_base         = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-url-base","http://dvlp.bpjs-kesehatan.go.id:8081/devwslokalrest/");
        $this->port             = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-port",8081);
        $this->is_pcare         = false;
        $this->consumer_id      = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-cons-id","24619");
    }
   
    public function command($command){
        if($command=="cek_vklaim"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setContent($this->cek_bpjs());
            return $pack->getPackage();
        }else if($command=="reg_sep"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->reg_sep();
            $pack->setWarning(true,"Pemberitahuan !!!",$r['msg']);
            $pack->setContent($r['response']);
            return $pack->getPackage();
        }else if($command=="cek_nomor_rujukan_pernah_dipakai"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->cek_nomor_rujukan_pernah_dipakai();
            $pack->setAlertContent("Pemberitahuan !!!",$r['msg']);
            $pack->setAlertVisible(true);
            $pack->setContent($r['response']);
            return $pack->getPackage();
        }else if($command=="fix_register"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->fixRegister();
            $pack->setWarning(true,"Pemberitahuan !!!",$r['msg']);
            $pack->setContent($r['response']);
            return $pack->getPackage();
        }else if($command=="print_vklaim"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->print_sep();
            $pack->setContent($r);
            return $pack->getPackage();
        }else if($command=="cek_rujukan"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->cek_rujukan($_POST['faskes']=="1"?"":"RS");
            $pack->setContent($r);
            return $pack->getPackage();
        }else if($command=="fingerprint"){
            $pack = new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->cek_fingerprint();
            $pack->setContent($r);
            return $pack->getPackage();
        }else{
           return parent::command($command);
        }
    }

    public function cek_nomor_rujukan_pernah_dipakai(){
        loadLibrary("smis-libs-function-time");
        $nokartu     = $_POST['no_kartu'];
        $norujukan   = $_POST['no_rujukan'];        
        $sampai = date("Y-m-d");
        $mulai = sub_date($sampai,90);
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."/monitoring/HistoriPelayanan/NoKartu/".$nokartu."/tglMulai//".$mulai."/tglAkhir//".$sampai;
       
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        $result = array();
        if ($this->error) {
            return array("msg"=>"Eror Komunikasi dengan BPJS","response"=>0);
        } else {
            $response   = json_decode($this->response,true);
            global $querylog;
            if($response==null || $response['metaData']['code']!="200" ){
                return array("msg"=>$response['metaData']['message'],"response"=>0);
            }else{
                $querylog->addMessage($response);
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($response['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $response['response']);
                    $hasil = decompress($hasilcompress);
                    $querylog->addMessage($hasil);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                $result = $rep['histori'];
                $querylog->addMessage(json_encode($result));
            }
        }

        foreach($result as $r){
            if($r['noRujukan']==$norujukan){
                return array("msg"=>"No Rujukan Sudah Pernah Dipakai","response"=>1);
            }
        }
        return array("msg"=>"No Rujukan Belum Pernah dipakai","response"=>0);
    }


    public function cari_history($nokartu){
        loadLibrary("smis-libs-function-time");   
        $sampai = date("Y-m-d");
        $mulai = sub_date($sampai,90);
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."/monitoring/HistoriPelayanan/NoKartu/".$nokartu."/tglMulai//".$mulai."/tglAkhir//".$sampai;
       
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        $result = array();
        if ($this->error) {
            return array("msg"=>"Eror Komunikasi dengan BPJS","response"=>0);
        } else {
            $response   = json_decode($this->response,true);
            global $querylog;
            if($response==null || $response['metaData']['code']!="200" ){
                return array("msg"=>$response['metaData']['message'],"response"=>0);
            }else{
                $querylog->addMessage($response);
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($response['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $response['response']);
                    $hasil = decompress($hasilcompress);
                    $querylog->addMessage($hasil);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                $result = $rep['histori'];
                $querylog->addMessage(json_encode($result));
            }
        }

        return $result;
    }
    
    protected function print_sep(){
        //$id     = $_POST['id'];
        //$row    = $this->getDBTable()->select(array("id"=>$id));
        //$nosep  = $row->noSEP;
        return $this->print_sep_nosep($_POST['nosep']);
    }
    
    public function print_sep_nosep($nosep){
        $url        = $this->url_base."SEP/".$nosep;
        $metode     = "GET";
        $this->do_curl($this->port,$url,$metode,NULL);
        //$one        = (array) $this->dbtable->select(array("noSEP"=>$nosep));


        if ($this->error) {
          return "cURL Error #:" . $err;
        } else {
            $data   = json_decode($this->response,true);
            if($data==null ){
                return "Terjadi Kesalahan Akibat Koneksi Server BPJS";
            }else if( $data['metaData']['code']!="200" ){
                return $data['metaData']['message'];
            }else{
                
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($data['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $data['response']);
                    $hasil = decompress($hasilcompress);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                //echo json_encode($rep);
                
                global $db;
                global $querylog;
                require_once "registration/function/bpjs_array_adapter.php";
                require_once "registration/function/bpjs_print_format.php";
                $querylog->addMessage(json_encode($rep));
                if(trim($rep['noRujukan'])==""){
                    $rep['asalRujukan'] = "-";
                }else{
                    //file_put_contents("smis-temp/ngegetch.json",json_encode($rep));
                    //$rujukan = $this->cari_rujukan($rep["noRujukan"]);
                    //$rep['asalRujukan'] = $rujukan["nama_provider"]." (".$rujukan['kd_provider'].") ";
                }
                $res    = bpjs_array_adapter_vclaim($rep,$one);
                return bpjs_print_format($db,$nosep,$res);
            }
        }
    }

    
    
    /**
     * @brief this function used to cek whether the 
     *          BPJS account of patient is active, exist or not
     *          by using the BPJS Number OR NIK/KTP Number
     * @return  array("table"=>"HTML Table,"raw"=>array data that contains BPJS data);
     */
    public function cek_bpjs(){
        $nobpjs     = isset($_POST['no_bpjs'])?$_POST['no_bpjs']:"";
        $noktp      = isset($_POST['no_ktp'])?$_POST['no_ktp']:"";        
        $methode    = "GET";
        $request    = "";        
        if($nobpjs!=""){
            $url    = $this->url_base."Peserta/nokartu/".$nobpjs."/tglSEP/".date("Y-m-d");
        }else{
            $url    = $this->url_base."Peserta/nik/".$noktp."/tglSEP/".date("Y-m-d");
        }
       
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        if ($this->error) {
            require_once "registration/function/bpjs_null.php";
            $data   = bpjs_null();
        } else {
            $response   = json_decode($this->response,true);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_null.php";
                $data   = bpjs_null($response['metaData']['message']);
            }else{
                require_once "registration/function/bpjs_found.php";
                global $querylog;
                $querylog->addMessage("Cek BPJS ");
                $data   = bpjs_found($response,$this->getKey());
            }
        }
        global $querylog;
        $querylog->addMessage($data);
        require_once "registration/function/bpjs_table_checker.php";
        $table  = bpjs_table_checker($data);
        $result = array("table"=>$table,"raw"=>$data);
        return $result;
    }

    public function cek_fingerprint(){
        $nobpjs     = $_POST['peserta'];
        $tgl      = $_POST['tgl'];        
        $methode    = "GET";
        $request    = "";        
        $url    = $this->url_base."/SEP/FingerPrint/Peserta/$nobpjs/TglPelayanan/".date("Y-m-d");
       
        $this->do_curl($this->port,$url,$methode,$request);
        $data   = null;
        if ($this->error) {
            return "Eror Komunikasi dengan BPJS";
        } else {
            $response   = json_decode($this->response,true);
            global $querylog;
            if($response==null || $response['metaData']['code']!="200" ){
                return $response['metaData']['message'];
            }else{
                $querylog->addMessage($response);
                require_once "registration/function/vclaim_decrypt.php";
                $rep = array();
                if(is_string($response['response'])){
                    $hasilcompress = stringDecrypt($this->getKey(), $response['response']);
                    $hasil = decompress($hasilcompress);
                    $rep = json_decode($hasil,true);
                }else{
                    $rep = $data['response'];
                }
                return $rep['status'];
            }
        }
    }

   
    
   
    /**
     * @brief this function used for creating the BPJS SEP
     *          or in other word registrating patient that used BPJS
     *          that init it's treatment
     * @return array("msg"=>"Message From BPJS Server","nosep"=>"SEP Number");  
     */
    public function reg_sep(){
        $sep            = $this->getBPJSPostData();
        $dt             = array("request"=>array("t_sep"=>$sep));
        $data           = json_encode($dt);
        $url            = $this->url_base."/SEP/2.0/insert";
        $methode        = "POST";
        $request        = $data;
        $this->do_curl($this->port,$url,$methode,$request,"");
        if ($this->error) {
            $message    = "cURL Error #:" . $this->error;
            $nosep      = "";
            return array("msg"=>$message,"nosep"=>$nosep,"data"=>$data);
        } else {
            return $this->formateCreateSEP($this->response,$sep,$request);
        }
    }
    
    
    //khusus vclaim
    protected function getBPJSPostData(){
        $sep                            = array();
        $sep['noKartu']                 = $_POST['noKartu'];
        $sep['tglSep']                  = substr($_POST['tglSep'],0,10);
        $sep['ppkPelayanan']            = $_POST['ppkPelayanan'];
        $sep['jnsPelayanan']            = $_POST['jnsPelayanan'];
        $sep['klsRawat']                = array();
        
        $sep['klsRawat']['klsRawatHak']     = $_POST['klsRawat'];
        $sep['klsRawat']['klsRawatNaik']    = $_POST['naikKelas'];
        $sep['klsRawat']['pembiayaan']      = $_POST['pembiayaan'];
        $sep['klsRawat']['penanggungJawab'] = $_POST['penanggunjwb'];

        $sep['noMR']                    = $_POST['noMr'];
        $sep['rujukan']                 = array();
        $sep['rujukan']['asalRujukan']  = trim($_POST['asalRujukan'])==""?2:trim($_POST['asalRujukan']);
        $sep['rujukan']['tglRujukan']   = substr($_POST['tglRujukan'],0,10);
        $sep['rujukan']['noRujukan']    = $_POST['noRujukan'];
        $sep['rujukan']['ppkRujukan']   = $_POST['ppkRujukan'];

        /*
        if($_POST['noRujukan']==""){
            $sep['rujukan']                 = array();
            $sep['rujukan']['asalRujukan']  = trim($_POST['asalRujukan'])==""?2:trim($_POST['asalRujukan']);
            $sep['rujukan']['tglRujukan']   = "";
            $sep['rujukan']['noRujukan']    = "";
            $sep['rujukan']['ppkRujukan']   = "";
        }*/

        if($_POST['jnsPelayanan']=="1"){
            $sep['rujukan']['asalRujukan'] = "2";
            $sep['rujukan']['noRujukan']    = $_POST['noSurat'];
            $sep['rujukan']['tglRujukan']   = substr($_POST['tglRujukan'],0,10);
            $sep['rujukan']['ppkRujukan']   = $_POST['ppkPelayanan'];
        }
        
        
        $sep['catatan']                 = $_POST['catatan'];
        $sep['diagAwal']                = $_POST['diagAwal'];
        $sep['poli']['tujuan']          = $_POST['poliTujuan'];
        $sep['poli']['eksekutif']       = $_POST['eksekutif'];
        $sep['cob']['cob']              = $_POST['cob'];
        $sep['katarak']['katarak']      = "0";//$_POST['katarak'];
        $sep['jaminan']                 = array();
        $sep['jaminan']['lakaLantas']   = $_POST['lakaLantas'];
        $sep['jaminan']['penjamin']     = array();//is_array($_POST['penjamin'])?implode(",",$_POST['penjamin']):$_POST['penjamin'];
        $sep['jaminan']['penjamin']['tglKejadian']     = $_POST['tgl_kejadian'];
        $sep['jaminan']['penjamin']['keterangan']     = $_POST['keterangan'];
        $sep['jaminan']['penjamin']['suplesi']     = array();
        $sep['jaminan']['penjamin']['suplesi']['suplesi']     = trim($_POST['no_suplesi'])==""?"0":"1";
        $sep['jaminan']['penjamin']['suplesi']['noSepSuplesi']     = trim($_POST['no_suplesi']);
        $sep['jaminan']['penjamin']['suplesi']['lokasiLaka']     = array();
        $sep['jaminan']['penjamin']['suplesi']['lokasiLaka']["kdPropinsi"]  = $_POST['kdPropinsi'];
        $sep['jaminan']['penjamin']['suplesi']['lokasiLaka']["kdKabupaten"] = $_POST['kdKabupaten'];
        $sep['jaminan']['penjamin']['suplesi']['lokasiLaka']["kdKecamatan"] = $_POST['kdKecamatan'];
        
        
        $sep['tujuanKunj'] = $_POST['tujuanKunj'];
        $sep['flagProcedure'] = $_POST['flagProcedure'];
        $sep['kdPenunjang'] = $_POST['kdPenunjang'];
        $sep['assesmentPel'] = $_POST['assesmentPel'];
        
        $sep['skdp'] = array();
        $sep['skdp']['noSurat'] = $_POST['noSurat'];
        $sep['skdp']['kodeDPJP'] = $_POST['kodeDPJP'];

        $sep['dpjpLayan']               = $_POST['dpjpLayan'];        
        $sep['noTelp']                  = $_POST['noTelp'];
        $sep['user']                    = $_POST['user'];
        
        return $sep;
    }
    
    public function formateCreateSEP($response,$insertsep,$request=null){
        $json   = json_decode($response,true);
        $msg    = "";
        $rsp = array();
        if($json==null  ){
            $msg    = "Pendaftaran Gagal, disebabkan Masalah Koneksi ke Server BPJS";
        }else if($json['metaData']['code']!="200"){
            $msg    = $json['metaData']['message'];
        }else{
            require_once "registration/function/vclaim_decrypt.php";
            global $querylog;
            $rep = array();
            if(is_string($json['response'])){
                $hasilcompress = stringDecrypt($this->key, $json['response']);
                $hasil = decompress($hasilcompress);
                $rep = json_decode($hasil,true);
                $querylog->addMessage($json['response']);
                $querylog->addMessage("DECYPRT BPJS ".$this->key);
                $querylog->addMessage($hasil);
            }else{
                $rep = $json['response'];
                $querylog->addMessage("RESPONSE BPJS NON ENKRIPSI");
                $querylog->addMessage(json_encode($data['response']));                
            }
            $rsp['noSEP']        = $rep['sep']['noSep'];
            $msg                 = "Pendaftaran Berhasil, dengan Nomor SEP : <strong>".$rsp['noSEP']."</strong>";// , Tekan Tombol Cetak untuk Mencetak Form BPJS
            
            $sep                 = array();
            $sep['noKartu']      = $insertsep['noKartu'];
            $sep['tglSep']       = $insertsep['tglSep'];
            $sep['tglRujukan']   = $insertsep['rujukan']['tglRujukan'];
            $sep['asalRujukan']  = $insertsep['rujukan']['asalRujukan'];
            $sep['noRujukan']    = $insertsep['rujukan']['noRujukan'];
            $sep['ppkRujukan']   = $insertsep['rujukan']['ppkRujukan'];
            $sep['ppkPelayanan'] = $insertsep['ppkPelayanan'];
            $sep['jnsPelayanan'] = $insertsep['jnsPelayanan'];
            $sep['diagAwal']     = $insertsep['diagAwal'];
            $sep['klsRawat']     = $insertsep['klsRawat']['klsRawatHak'];
            $sep['lakaLantas']   = $insertsep['jaminan']['lakaLantas'];
            $sep['lokasiLaka']   = $insertsep['jaminan']['penjamin']['suplesi']['lokasiLaka'];
            $sep['catatan']      = $insertsep['catatan'];
            $sep['user']         = $insertsep['user'];
            $sep['noMr']         = $insertsep['noMR'];
            //$sep['id_pasien']    = $insertsep['id_pasien'];
            $sep['eksekutif']    = $insertsep['poli']['eksekutif'];
            $sep['poliTujuan']   = $insertsep['poli']['tujuan'];
            $sep['cob']          = $insertsep['cob']['cob'];
            $sep['noTelp']       = $insertsep['noTelp'];
            $sep['noSEP']        = $rsp['noSEP'];
            $sep['penjamin']     = $insertsep['jaminan']['penjamin']['keterangan'];//is_array($_POST['penjamin'])?json_encode($_POST['penjamin']):$_POST['penjamin'];
            $sep['createBy']     = "vclaim";
            $sep['namaRujukan']  = $this->getFaskesRujukan($sep['ppkRujukan'],$sep['asalRujukan']);
            
            $sep['TujuanKunjungan']     = $_POST['n_tujuanKunjungan'];
            $sep['Prosedur']            = $_POST['n_flagProcedure'];
            $sep['Penunjang']           = $_POST['n_kdPenunjang'];
            $sep['AssesmentPelayanan']  = $_POST['n_assesmentPel'];
            $sep['PoliPerujuk']         = $_POST['n_poliTujuan'];
            $sep['namaRujukan']         = $_POST['n_namarujukan'];
            $sep['potensiprb']          = $_POST['n_namarujukan'];
            
            /**INSERT RG SEP */
            $this->getDBTable()->insert($sep);
            
            /** save telpon number */
            /*
            $idx['id']           = $sep['noMr'];
            $upx['telpon']       = $sep['noTelp'];
            $dbtable             = new DBTable($this->getDBTable()->get_db(),"smis_rg_patient");
            $dbtable             ->update($upx,$idx);*/

            /* binding dengan noreg dan nosep */
            $this->cekBindingBPJS($rsp,$msg);
        }
        return array("msg"=>$msg,"response"=>$rsp);
    }


    public function getFaskesRujukan($kode,$jenis){
        $url            = $this->url_base."referensi/faskes/".$kode."/".$jenis;
        $methode        = "GET";
        $this->do_curl($this->port,$url,$methode,NULL);
        if ($this->error) {
            $message    = "#".$this->error;
            return $message;
        } else {
            $json   = json_decode($this->response,true);
            if($json==null  ){
                return "#NULL";
            }else if($json['metaData']['code']!="200"){
                return  "#".$json['metaData']['message'];
            }else{
                $list = $json['response']['faskes'];
                foreach($list as $f){
                    if($f['kode']==$kode){
                        return $f['nama'];
                    }
                }
                return  "#NOT-FOUND";
            }
        }
    }
    
    /**
     * @brief untuk melakukan pengecekan ulang jika terdapat binding
     *          atau registrasi lama yang belum di pulanglan.
     * @return  array
     */
    public function fixRegister(){
        $rsp['noSEP']   = $_POST['noSEP'];
        $msg            = "";
        $this->cekBindingBPJS($rsp,$msg);        
        return array("msg"=>$msg,"response"=>$rsp);
    }
    
       
    /**
     * @brief   terbaru yang dipakai
     *          this function to get detail of rujukan
     *          based on rujukan number.
     * @param $asal , default is empty but if there is rujukan that from puskesmas then search in rujukan rs
     * @return  array("table"=>"HTML Table,"raw"=>array data that contains BPJS data);
     */
    public function cek_rujukan($asal=""){
        $no_rujukan = isset($_POST['noRujukan'])?$_POST['noRujukan']:"";
        $nobpjs     = isset($_POST['noBPJS'])?$_POST['noBPJS']:"";        
        $methode    = "GET";
        $request    = "";
        if($no_rujukan!=""){
            $url    = $this->url_base."Rujukan/".$asal."/".$no_rujukan;
        }else{
            $url    = $this->url_base."Rujukan/".$asal."/Peserta/".$nobpjs;
        }

        global $querylog;
        $querylog->addMessage($url);
        
        $this->do_curl($this->port,$url,$methode,$request);
        $data=null;
        if ($this->error) {
            require_once "registration/function/bpjs_rujukan_null.php";
            $data=bpjs_rujukan_null("Terjadi Masalah Koneksi dengan Server BPJS");
        } else {
            $response=json_decode($this->response,true);
            //echo $this->response;
            $querylog->addMessage($this->response);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_rujukan_null.php";
                if($response['metaData']['code']=="202"){
                    $data = bpjs_rujukan_null($response['metaData']['message']);
                }else if($asal==""){
                    /* try search from another RS */
                    //return $this->cek_rujukan("RS/");
                }
                $data = bpjs_rujukan_null($response['metaData']['message']);
            }else{
                
                require_once "registration/function/bpjs_rujukan_found.php";
                $data = bpjs_rujukan_found_vclaim($response['response'],$this->getKey(),$asal);
            }
        }
        return $data;
    }


    public function cari_rujukan($no_rujukan,$asal=""){
        $methode    = "GET";
        $request    = "";
        $url    = $this->url_base."Rujukan/".$asal.$no_rujukan;

        global $querylog;
        $querylog->addMessage($url);
        $this->do_curl($this->port,$url,$methode,$request);
        $querylog->addMessage(json_encode($this->response));
        $data=null;
        if ($this->error) {
            require_once "registration/function/bpjs_rujukan_null.php";
            $data=bpjs_rujukan_null("Terjadi Masalah Koneksi dengan Server BPJS");
        } else {
            $response=json_decode($this->response,true);
            //echo $this->response;
            $querylog->addMessage($this->response);
            if($response==null || $response['metaData']['code']!="200" ){
                require_once "registration/function/bpjs_rujukan_null.php";
                if($response['metaData']['code']=="202"){
                    $data = bpjs_rujukan_null($response['metaData']['message']);
                }else if($asal==""){
                    /* try search from another RS */
                    return $this->cari_rujukan($no_rujukan,"RS/");
                }
                $data = bpjs_rujukan_null($response['metaData']['message']);
            }else{
                
                require_once "registration/function/bpjs_rujukan_found.php";
                $data = bpjs_rujukan_found_vclaim($response['response'],$this->getKey(),$asal);
                $data['asalRujukan'] = $asal==""?1:2;
            }
        }
        return $data;
    }
    
    
}
?>