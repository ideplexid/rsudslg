<?php 

class DaftarResponder extends DuplicateResponder{
	private $pasien=null;
	private $service_entity=null;
	public function command($command){		
		if($command=="check_baru_lama"){
			$pack		 = new ResponsePackage();
			$content	 = NULL;
			$status		 = 'not-command';	//not-authorized, not-command, fail, success
			$alert		 = array();
			$content	 = $this->baruLama();
			$pack		 ->setContent($content);
			$pack		 ->setStatus(ResponsePackage::$STATUS_OK);
			$pack		 ->setAlertVisible(false);
			return $pack ->getPackage();
		}else if($command=="dogtag"){
			$pack		 = new ResponsePackage();
			$content	 = NULL;
			$status		 = 'not-command';	//not-authorized, not-command, fail, success
			$alert		 = array();
			$content	 = $this->getResource("dogtag");
			$pack		 ->setContent($content);
			$pack		 ->setStatus(ResponsePackage::$STATUS_OK);
			$pack		 ->setAlertVisible(false);
			return $pack ->getPackage();
		}else if($command=="label"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->getResource("label");
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if($command=="label_inap"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->getResource("label_inap");
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if($command == "get_last_visited_data") {
			$pack = new ResponsePackage();
			$content = $this->getLastVisitedData();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		} else if ($command == "b_wbpatient_print") {
			$pack = new ResponsePackage();
			$content = NULL;
			$status = 'not-command';
			$alert=array();
			$content=$this->getResource("gelang_bayi");
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		} else if ($command == "a_wbpatient_print") {
			$pack = new ResponsePackage();
			$content = NULL;
			$status = 'not-command';
			$alert = array();
			$content=$this->getResource("gelang_dewasa");
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if ($command == "sinkronisasi") {
			$pack = new ResponsePackage();
			$content = NULL;
			$status = 'not-command';
			$alert = array();
			$content=$this->synchronize();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if($command=='save' && $this->getAddCapability()){
			$pack=$this->save();
			return $pack->getPackage();
		}else{
			return parent::command($command);
		}
	}
	
	/**
	 * @brief get the detail of the pastient record based on it's NRM
	 * @param String $nrm 
	 * @return  the object or array row of the patient
	 */
	public function getPasien($nrm){
		if($this->pasien==null){
			$pasien_table=new DBTable($this->getDBTable()->get_db(),'smis_rg_patient');
			$this->pasien=$pasien_table->select($nrm,false);
		}
		return $this->pasien;
	}
	
	/**
	 * @brief search the last visited row of this patient
	 * 		 used for suggest to user.
	 * @return  the last visited data
	 */
	private function getLastVisitedData() {
		$nrm = $_POST['nrm'];
		$this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$row = $this->dbtable->get_row("
			SELECT *
			FROM smis_rg_layananpasien
			WHERE prop NOT LIKE 'del' AND nrm = '" . $nrm . "'
			ORDER BY id DESC
			LIMIT 0, 1
		");
		$row['last_time']=date("Y-m-d H:i:s");
		return $row;
	}
	
	/**
	 * @brief ceck wether the patient is new or old
	 * @return  old if 1 and new if 0
	 */
	private function baruLama(){
		global $db;
		$id=$_POST['id'];
		$last=getSettings($db, "reg-last-number", "0");
		if($id<=$last){
			return 1;
		}else{
			$query="SELECT count(*) FROM smis_rg_layananpasien WHERE prop!='del' AND nrm='$id' ";
			$total=$db->get_var($query);
			if($total!="0") return 1;
		}
		return 0;
	}
	
	/**
	 * @brief used for getting snippet of simple resource
	 * for creating printing like, gelang_bayi.php, gelang_dewasa.php, label.php, dogtag.php
	 * @param String $resource 
	 * @return  String of HTML element that ready for printer.
	 */
	private function getResource($resource){
		ob_start();
		require_once "registration/snippet/".$resource.".php";
		$result=ob_get_clean();
		return $result;
	}
	
	 
	/**
	 * @brief 	this methode used for check the primary dokter 
	 * 			of this patient
	 * 			- check whether the data need to be preserver is complete
	 * 			- check the patient doesn't have the primary docter
	 * 			- check if the suggest doctor is same or not
	 * 			- if same, there will be no problem if not record the different
	 * 			- save the data
	 * @param String $nrm of patient 
	 * @param String $tanggal the datetime in YYYY-MM-DD / YYYY-MM-DD HH:ii:ss format 
	 * @return  null
	 */
	public function saveDokterUtama($nrm,$tanggal){
		$db=$this->getDBTable()->get_db();
		$dokter_utama=getSettings($db,"reg-activate-dokter-utama","0")=="1";
		if(	!isset($_POST['kode_du']) || !isset($_POST['suggest_du'] ) || !isset($_POST['keterangan_du']) || !isset($_POST['editable_du']) ){
			return;	
		}
		
		$save_each	= getSettings($db,"reg-activate-dokter-utama-each-time","0")=="1";
		$tanggal	= substr($tanggal,0,16);
		$dbtable	= new DBTable($db,"smis_rg_dokter_utama");
		$dbtable	->addCustomKriteria("nrm_pasien","='".$nrm."'");
		if($save_each){
			/* khusus untuk penyimpanan yang mana setiap 
			 * tanggal selesainya tidak sama, 
			 * maka disimpan. sehingga setiap dokter dapat menjadi pemilik pasien
			 * geser setiap salah satu selesai.*/
             loadLibrary("smis-libs-function-time");
			 $tgl_selesai=addYear($tanggal,1);
			$dbtable->addCustomKriteria("selesai","='".$tgl_selesai."'");
		}else{
			/* setiap ada yang belum selesai maka tidak boleh insert baru lagi
			 * nunggu selesai dulu baru insert lagi baru */
			$dbtable->addCustomKriteria("selesai",">='".$tanggal."'");			
		}
		$dbtable->setMaximum(1);
		$dbtable->setOrder(" selesai DESC ");
		$du		= $dbtable->view("","0");
		$ddu	= $du['data'];
		if(!isset($ddu[0]) && $_POST['editable_du']=="1" ){
            global $user;
            loadLibrary("smis-libs-function-time");
			$keterangan="Dimasukan Melalui Menu Pendaftaran Pasien ".($save_each?"dengan mode Per Kunjungan di Aktifkan":"");
			if($_POST['kode_du']!=$_POST['suggest_du']){
				$keterangan.=" Kode Dokter Utama dari System adalah [".$_POST['suggest_du']."]";
				$keterangan.=" Tetapi di Pilih Dokter Utama Oleh Petugas adalah [".$_POST['suggest_du']."]";
			}
			
			$pasien=$this->getPasien($nrm);
			$save=array();
			$save['kode']			= $_POST['kode_du'];
			$save['nama_pasien']	= $pasien->nama;
			$save['nrm_pasien']		= $nrm;
			$save['mulai']			= $tanggal;
			$save['selesai']		= addYear(substr($tanggal,0,10),1);
			$save['petugas']		= $user->getNameOnly();
			$save['keterangan']		= $keterangan;
			$dbtable->insert($save);
		}
	}
	
    /* every update will say that not syncronize yet */
    public function postToArray(){
        $data			= parent::postToArray();
        $data['synch']	= 0;
        return $data;
    }
    
    private function getAutoRegPartial(){
        $last			= getSettings($this->getDBTable()->get_db(),"smis-reg-noreg-partial-last",0)*1+1;
        setSettings($this->getDBTable()->get_db(),"smis-reg-noreg-partial-last",$last);
        return $last;
    }
    
	public function save(){
		$register_ganda=$this->cekRegisterGanda();
		if($register_ganda!=null){
			return $register_ganda;
		}
		$kabur=$this->cekKabur();
		if($kabur!=null){
			return $kabur;
		}
        $db		= $this->dbtable->get_db();
		$data	= $this->postToArray();		
        $list	= $_POST['jenislayanan'];
        if(is_array($list) && count($list)==1){
            $data['jenislayanan']	= $list[0];
            $this->service_entity	= $list[0];
        }else{
            $this->service_entity	= $list;
        }
        $data['tanggal'] = $_POST['tanggal'];
		$query			 = "SELECT * FROM smis_rg_patient WHERE id='".$_POST['nrm']."' AND prop!='del' ";
		$px				 = $db->get_row($query);
		$start  		 = $px->tgl_lahir;
		$end			 = $_POST['tanggal'];
		if($start=="" || $start=="0000-00-00"){
			$data['gol_umur']	= "TGL LAHIR TDK VALID";
			$data['umur']		= "TGL LAHIR TDK VALID";
		}else{
			$data['gol_umur']	= medical_gol_umur($start,$end);
			$data['umur']		= medical_umur($start,$end);
		}
		/**pendambahan data-data lain untuk kepentingan cache */
		$data['alamat_pasien']	= $px->alamat;
		$data['nama_kelurahan']	= $px->nama_kelurahan;
		$data['nama_kecamatan']	= $px->nama_kecamatan;
		$data['nama_kabupaten']	= $px->nama_kabupaten;
		$data['nama_provinsi']	= $px->nama_provinsi;
		$data['nama_pasien']	= $px->nama;
		$data['kelamin']		= $px->kelamin;

		/// Info. Jenis Kegiatan :
		if (getSettings($this->dbtable->get_db(), "smis-rs-rl52-" . $data['jenislayanan'], "0") == "1")
			$data['jenis_kegiatan'] = getSettings($this->dbtable->get_db(), "smis-rs-rl52-default-" . $data['jenislayanan'], "");
		
		$id['id']	= $_POST['id'];
		if($_POST['id']==0 || $_POST['id']==""){
            $autoformat		= getSettings($this->getDBTable()->get_db(),"reg-layanan-duplicate",0);
            if($autoformat && isset($data['id']) &&  ($data['id']=="" || $data['id']=="0" || $data['id']==0 )){
                $data['id'] = $this->getAutoRegPartial();
            }
			$query			 	  = "SELECT MAX(no_urut) as nomor FROM smis_rg_layananpasien WHERE tanggal='".$_POST['tanggal']."' AND prop!='del' ";
			$data['no_urut'] 	  = $db->get_var($query)+1;
			$query				  = "SELECT MAX(no_kunjungan) as nomor FROM smis_rg_layananpasien WHERE nrm='".$_POST['nrm']."' AND prop!='del' ";
			$data['no_kunjungan'] = $db->get_var($query)+1;
            global $user;
            $data['oprj']		  = $user->getUsername();
			$result				  = $this->dbtable->insert($data);
			$id['id']			  = $this->dbtable->get_inserted_id();
			$success['type']	  = 'insert';
		}else{
			$result=$this->dbtable->update($data,$id);
			$success['type']	  = 'update';
		}
		$this->saveDokterUtama($_POST['nrm'],$_POST['tanggal']);
		
		$success['id']			  = $id['id'];
		$success['success']		  = 1;
		if($result===false) {
            $success['success']	  = 0;
        }else{
            /* synchronize the data to server */
            require_once "registration/class/RegistrationResource.php";
            RegistrationResource::synchronizeToCashier($this->dbtable->get_db(),$success['id']);
            if($success['success']==1 && $this->isAutoSync()){
                $id=$success['id'];
                $this->notify($id,$success['type']=="insert");
            }            
            /*synchronize data to accounting*/
            RegistrationResource::synchronizeToAccounting($this->dbtable->get_db(),$success['id']);
            /*backup data for synchronize data without nrm*/
            require_once "registration/function/backup_vregister.php";
            backup_vregister($this->getDBTable()->get_db(),$success['id']);            
            /* update dan save data SEP */
            if($success['success']==1 && isset($_POST['no_sep_rj']) && $_POST['no_sep_rj']!=""){
                $this->bindingSEP($_POST['no_sep_rj'],$success['id']);
            }            
            /* save data nomor asuransi */
            $this->saveDataAsuransi();            
        }
		return $this->pushAntrian($success);
	}
    
    /**
     * @brief binding the SEP data with Registration data
     * @param string $nosep is the number of SEP 
     * @param string $noreg is the number of Registration 
     * @return  null
     */
    private function bindingSEP($nosep,$noreg){
        $dbtable=new DBTable($this->getDBTable()->get_db(),"smis_rg_sep");
        $update['noReg']=$noreg;
        $id['noSEP']=$nosep;
        $dbtable->update($update,$id);
    }
    
    /**
     * @brief dipakai untuk menyimpan data nomor asuransi
     * @return  null
     */
    private function saveDataAsuransi(){
        if(getSettings($this->getDBTable()->get_db(),"reg-activate-master-card","0")=="0"){
            return;
        }
        $data['nama_asuransi']=$_POST['nama_asuransi'];
        $data['id_asuransi']=$_POST['asuransi'];
        $data['nrm_pasien']=$_POST['nrm'];
        if($data['id_asuransi']=="" || $data['id_asuransi']=="0" || $data['nama_asuransi']=="" || $_POST['nobpjs']==""){
            return;
        }
        
        $xtable=new DBTable($this->getDBTable()->get_db(),"smis_rg_nomor_asuransi");
        if(!$xtable->is_exist($data)){
            $data['nomor_asuransi']=$_POST['nobpjs'];
            $xtable->insert($data);
        }else{
            $update['nomor_asuransi']=$_POST['nobpjs'];
            $xtable->update($update,$data);
        }
    }
	
	public function synchronize(){
		$id=$_POST['id'];
		RegistrationResource::synchronize($id);
		return "Sinkronisasi Berhasil";
	}
	
	/**
	 * @brief this methode used for checking whetrer the patient 
	 * 		  ever running away from the Hospital
	 * @return  ResponsePackage
	 */
	private function cekKabur(){
		$db=$this->getDBTable()->get_db();
		$query="SELECT * FROM smis_rg_layananpasien 
				WHERE nrm='".$_POST['nrm']."' AND prop!='del' 
				AND selesai='1' AND carapulang='Kabur'";
		$row=$db->get_row($query);
		if($row!=null){
			$noreg=ArrayAdapter::format("only-digit8", $row->id);
			$package=new ResponsePackage();
			$pesan='Pasien dengan NRM '.$_POST['nrm']." Pernah Kabur dengan No Registrasi <strong>".$noreg."</strong>";
			$pesan.=" pada Tanggal Registrasi <strong>".ArrayAdapter::format("date d M Y", $row->tanggal)."</strong> ";
			$pesan.=" dan Tanggal Pulang <strong>".ArrayAdapter::format("date d M Y", $row->tanggal_pulang)."</strong>";

			$button=new Button("", "", "Reaktivasi");
			$button ->setClass("btn-primary")
					->setIsButton(Button::$ICONIC_TEXT)
					->setIcon(" fa fa-refresh")
					->setAction("daftar_pasien.reactivated('".$row->id."')");
			
			$pesan=$pesan."<div class='clear'></div> <div class='center'>".$button->getHtml()."</div> <div class='clear' id='reaktivasi'></div>";
			$package->setWarning(true, "Pendaftaran Gagal", $pesan);
			$package->setStatus(ResponsePackage::$STATUS_OK);
			$package->setContent("kabur");
			return $package;
		}
		return null;
	}
	
	/**
	 mencegah terjadinya registrasi ganda, menghitung nomor urut, dan menghitung nomor kunjungan
	 nomor_urut adalah nomor urut pasien hari ini.
	 nomor_kunjungan adalah nomor berapa kali pasien ini berkunjung ke sini
	 */
	private function cekRegisterGanda(){		
		if($_POST['id']=="" || $_POST['id']=="0"){
			$query="SELECT COUNT(*) as total FROM smis_rg_layananpasien WHERE nrm='".$_POST['nrm']."' AND prop!='del' AND selesai='0' ";
			$total=$this->getDBTable()->get_db()->get_var($query);
			if($total>0){
				$package=new ResponsePackage();
				$package->setContent('Pasien dengan NRM '.$_POST['nrm']." Sudah Terdaftar");
				$package->setStatus(ResponsePackage::$STATUS_OK);
				return $package;
			}
		}
		return null;
	}
	
	/**
	 * @brief this function used for to push the 
	 * patient list to certain room using service consumer
	 * 
	 * @return  ResponsePackage
	 */
	private function pushAntrian($datapasien){
		require_once 'registration/class/service/AntrianService.php';
		$pasien		= $this->getPasien($_POST['nrm']);
        $this->getDBTable()->setFetchMethode(DBTable::$OBJECT_FETCH);
		$reg_pasien	= $this->getDBTable()->select($datapasien['id'],false);
        
		$detail					    = array();
		$detail ['alamat']		    = $pasien->alamat;
		$detail ['ibu'] 		    = $pasien->ibu;
		$detail ['caradatang']	    = $reg_pasien->caradatang;
		$detail ['waktu_register']	= $reg_pasien->tanggal;
		$detail ['tgl_lahir']	    = $pasien->tgl_lahir;
		$detail ['no_profile']      = $pasien->profile_number;
		$alamat					    = $pasien->alamat." - ".$pasien->nama_kelurahan." - ".$pasien->nama_kecamatan." - ".$pasien->nama_kabupaten." - ".$pasien->nama_provinsi;
		$service				    = new AntrianService($this->getDBTable()->get_db(),$_POST['tanggal'],$pasien->nama,$_POST['nrm'],$_POST['carabayar'],$this->service_entity,$datapasien['id'],$reg_pasien->umur,$reg_pasien->gol_umur,$pasien->kelamin,$alamat,$detail);
		$service->execute();
            
		
		$rp = new ResponsePackage();
		$rp->setAlertVisible(false);
		$rp->setStatus(ResponsePackage::$STATUS_OK);
		
        global $notification;
        if(is_string($this->service_entity)){  
            $response			= $service->getContent();            
            //update nomor urut pasien yang bersangkutan
            $id_urut['id']		= $datapasien['id'];
            $no_urut['no_urut']	= $response['content']['nomor'];
            $this->getDBTable()->update($no_urut, $id_urut);
            
            if($reg_pasien->no_kunjungan*1!=1){
                $notification->addNotification("Pasien Masuk", md5($datapasien['id']), "Pasien ".$pasien->nama." dengan NRM <strong>[ ".$_POST['nrm']." ]</strong> Masuk ke <strong>".$reg_pasien->jenislayanan."</strong>", "registration", "pasien_aktif");
            }            
			$rp->setContent($datapasien);
			

			$button = new Button("","","Cetak Antrian");
			$button ->setClass("btn btn-primary")
				->setIcon("fa fa-list-alt")
				->setIsButton(Button::$ICONIC_TEXT)
				->setAction("daftar_pasien.cetak_antrian('".$datapasien['id']."')");
					$warn = "<strong >".$pasien->nama."</strong>
							dengan NRM : <strong class='label label-inverse'>".ArrayAdapter::format("digit8",$_POST['nrm'])."</strong>
							Berhasil di Register ke
							<strong>".strtoupper($this->service_entity)."</strong>
							dengan nomor Antrian
							<strong class='badge badge-success'>".$no_urut['no_urut']."</strong> untuk Tanggal
				<strong class='label label-info'>".ArrayAdapter::format("date d-M-Y",$_POST['tanggal'])."</strong>
				<div class='clear'></div>";
			if(getSettings($this->dbtable->get_db(),"reg-antrian-active","0")=="1"){
				$warn .= $button->getHtml();
			}                                    
        }else{
            $service->setMode(ServiceConsumer::$KEY_ENTITY);
            $full_response = $service->getContent();
            
            $warn	= "";
            $first	= true;
            foreach($full_response as $entity=>$response){
                if($first){
                    $first=false;
                    //update nomor urut pasien yang bersangkutan
                    $id_urut['id']		= $datapasien['id'];
                    $no_urut['no_urut']	= $response['content']['nomor'];                    
                    $this->getDBTable()->update($no_urut, $id_urut);
                    $rp->setContent($datapasien);
                    if($reg_pasien->no_kunjungan*1!=1){
                        $notification->addNotification("Pasien Masuk", md5($datapasien['id']), "Pasien ".$pasien->nama." dengan NRM <strong>[ ".$_POST['nrm']." ]</strong> Masuk ke <strong>".$reg_pasien->jenislayanan."</strong>", "registration", "pasien_aktif");
					}
                }
                
                $warn.=  "<div class='well'><strong >".$pasien->nama."</strong>
                    dengan NRM : <strong class='label label-inverse'>".ArrayAdapter::format("digit8",$_POST['nrm'])."</strong>
                    Berhasil di Register ke
                    <strong>".strtoupper($entity)."</strong>
                    dengan nomor Antrian
                    <strong class='badge badge-success'>".$response['content']['nomor']."</strong> untuk Tanggal
                    <strong class='label label-info'>".ArrayAdapter::format("date d-M-Y",$_POST['tanggal'])."</strong>
                    </div>";              
            }
        }
        
        if(getSettings($this->getDBTable()->get_db(),"registration-show-cashier-button","0")=="1"){
               $fastlink = new Button("","","Menuju Kasir");
			   $fastlink ->setAction("registration_patient.fast_cashier('".$datapasien['id']."');")
						 ->setIsButton(Button::$ICONIC_TEXT)
						 ->setClass("btn-primary")
						 ->setIcon(" fa fa-money");
               $warn	.= "</br></br><div class='clear'>".$fastlink->getHtml()."</div>";
        }
		$rp->setWarning(true, "Berhasil", $warn);
		return $rp;
	}
}

?>