<?php 
class PasienResponder extends DuplicateResponder{
    private $no_bpjs_enabled;
    private $no_profile_enabled;
    
	public function command($command) {
		if ($command == "check_exist_nrm") {
			$pack = new ResponsePackage();
			$content = $this->check_exist_nrm();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($command == "check_max_nrm") {
			$pack = new ResponsePackage();
			$content = $this->check_max_nrm();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($command == "check_exist_name_address") {
			$pack = new ResponsePackage();
			$content = $this->check_exist_name_address();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($command == "sinkronisasi") {
			$pack = new ResponsePackage();
			$content = $this->synchronize();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		} else{
			return parent::command($command);
		}
		
		return $pack->getPackage();
	}
    
    public function setNoProfileUsability($enability){
        $this->no_profile_enabled=$enability;
        return $this;
    }
    
    public function setNoBPJSUsability($enability){
        $this->no_bpjs_enabled=$enability;
        return $this;
    }
	
	public function postToArray(){
		$post=parent::postToArray();
		$db=$this->dbtable->get_db();
        $autoformat=getSettings($this->getDBTable()->get_db(),"reg-format-rm",0);
		if( $autoformat!="0" 
				&& isset($post['id']) 
				&&  ($post['id']=="" || $post['id']=="0" || $post['id']==0 ) ){
            if($autoformat==1) $post['id']=$this->getAutoNRM();
            else if($autoformat==2) $post['id']=$this->getAutoRMPartial();
		}
		return $post;
	}
    
    private function getAutoRMPartial(){
        $last=getSettings($this->getDBTable()->get_db(),"smis-reg-nrm-partial-last",0)*1+1;
        setSettings($this->getDBTable()->get_db(),"smis-reg-nrm-partial-last",$last);
        return $last;
    }
	
	private function getAutoNRM(){
		$dt=(date("Ym")."000")*1;
		$query="SELECT MAX(id) FROM smis_rg_patient WHERE id >".$dt."  ";
		$last_number=$this->dbtable->get_db()->get_var($query);
		if($last_number==NULL){
			return ($dt+1);
		}
		return ($last_number*1)+1;
	}
	
	public function synchronize(){
		loadLibrary("smis-libs-function-medical");
		$kriteria['nrm']=$_POST['id'];
		$kriteria['selesai']=0;
        
		$pasien=$this->select($_POST['id']);		
		$tanggal_lahir=$pasien['tgl_lahir'];
		$dbtable=new DBTable($this->dbtable->get_db(),"smis_rg_layananpasien");
		$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$layanan=$dbtable->select($kriteria);
		
		$start=$tanggal_lahir;
		$end=$layanan['tanggal'];
		$data=array();
		$keterangan="";
		if($start=="" || $start=="0000-00-00"){
			$data['gol_umur']="TGL LAHIR TDK VALID";
			$data['umur']="TGL LAHIR TDK VALID";
			$keterangan="Tanggal Lahir Tidak Valid Sinkronisasi Data Tidak Dilakukan !! ";
		}else{
			$data['gol_umur']=medical_gol_umur($start,$end);
			$data['umur']=medical_umur($start,$end);
			$keterangan="Sinkronisasi Data Berhasil dilakukan !!, ".$data['gol_umur']." dan usia ".$data['umur'];
			$dbtable->update($data,array("id"=>$layanan['id']));
			$layanan['gol_umur']=$data['gol_umur'];
			$layanan['umur']=$data['umur'];
			RegistrationResource::synchronize($layanan['id']);
		}
		return $keterangan;
	}
	
	public function view(){
		$dbtable=$this->getDBTable();
		$search_id=$_POST['search_id'];
		if($search_id!="" && is_numeric($search_id)) {
			$search_id*=1;
			$dbtable->addCustomKriteria(" id "," = '".($search_id)."' ");
		}
		$dbtable->addCustomKriteria(" tanggal "," LIKE '%".$_POST['search_tanggal']."%' ");
		$dbtable->addCustomKriteria(" nama "," LIKE '%".$_POST['search_nama']."%' ");
		$dbtable->addCustomKriteria(" alamat "," LIKE '%".$_POST['search_alamat']."%' ");
		$dbtable->addCustomKriteria(" kelamin "," LIKE '%".$_POST['search_kelamin']."%' ");		
        if($this->no_bpjs_enabled && $_POST['search_nobpjs']!=""){
            $dbtable ->addCustomKriteria(" nobpjs "," LIKE '%".$_POST['search_nobpjs']."%' ");
		}
        if($this->no_profile_enabled && $_POST['search_profnum']!=""){
            $dbtable ->addCustomKriteria(" profile_number "," LIKE '%".$_POST['search_profnum']."%' ");
		}
		if($_POST['search_ktp']!=""){
            $dbtable ->addCustomKriteria(" ktp "," LIKE '%".$_POST['search_ktp']."%' ");
        }
        if($_POST['search_provinsi']!=" "){
            $dbtable ->addCustomKriteria(" nama_provinsi "," LIKE '%".$_POST['search_provinsi']."%' ");
        }
		if($_POST['search_kabupaten']!=" "){
            $dbtable ->addCustomKriteria(" nama_kabupaten "," LIKE '%".$_POST['search_kabupaten']."%' ");
        }
		if($_POST['search_kecamatan']!=" "){
            $dbtable ->addCustomKriteria(" nama_kecamatan "," LIKE '%".$_POST['search_kecamatan']."%' ");
        }
		if($_POST['search_kelurahan']!=" "){
            $dbtable ->addCustomKriteria(" nama_kelurahan "," LIKE '%".$_POST['search_kelurahan']."%' ");
		}
		if($_POST['search_panggilan']!=""){
            $dbtable ->addCustomKriteria(" sebutan "," LIKE '".$_POST['search_panggilan']."' ");
		}
		if($_POST['search_tgl_lhr']!=""){
            $dbtable ->addCustomKriteria(" tgl_lahir "," LIKE '".$_POST['search_tgl_lhr']."' ");
		}
		
		return parent::view();
	}
	
	public function check_exist_nrm() {
		$id = $_POST['id'] * 1;
		$row = $this->dbtable->select($id, false);
		if ($row == null)
			return false;
		return true;
	}
	
	public function check_max_nrm() {
		$id = $_POST['id'] * 1;
		$row = $this->dbtable->get_row("
			SELECT MAX(id) AS 'id_max'
			FROM " . $this->dbtable->getName() . "
		");
		return $id > $row->id_max;
	}
	
	public function check_exist_name_address() {
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$row = $this->dbtable->get_row("
			SELECT id
			FROM " . $this->dbtable->getName() .  "
			WHERE nama = '" . $nama . "' AND alamat = '" . $alamat . "'
		");
		if ($row == null)
			return false;
		return true;
	}
	
	public function edit(){
		$id=$_POST['id'];
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$row=$this->dbtable->select($id,false);
		if($row['tgl_lahir']!="0000-00-00"){
            $row['umur']=medical_umur($row['tgl_lahir'],date("Y-m-d"));
            
        }
		return $row;
	}

	public function save(){
		$data=$this->postToArray();
		if($_POST['tgl_lahir']=="" || $_POST['tgl_lahir']=="0000-00-00" || $_POST['tgl_lahir']=="00-00-0000"  || $_POST['tgl_lahir']=="undefined-undefined-"){
			$umur=$_POST['umur'];
			if($umur!=""){
				$date="06-30";//date("m-d"); diambul 30 juni
				$year=date("Y");
				$year=$year-$umur;
				$data['tgl_lahir']=$year."-".$date;
			}
			$data['umur']=$umur." Tahun ";
		}else{
			$data['umur']="";
		}
		
		$id['id']=$_POST['id'];
		if($_POST['id']==0 || $_POST['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data);
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
			$result=$this->dbtable->update($data,$id);
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}

}


?>