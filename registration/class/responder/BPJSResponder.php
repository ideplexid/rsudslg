<?php 


class BPJSResponder extends DBResponder{
    
    protected $error;
    /**@var string */
    protected $response;
    protected $consumer_id;
    protected $consumer_secret;
    protected $consumer_user_key;
    protected $ppkPelayanan;
    protected $url_base;
    protected $port;
    protected $is_pcare;
    protected $pcare_username;
    protected $pcare_password;
    protected $pcare_code;
    protected $key;
    
    
    function __construct(DBTable $dbtable,Table $uitable=NULL,ArrayAdapter $adapter=NULL){
        parent::__construct($dbtable,$uitable,$adapter);
        $this->error=null;
        $this->response=null;
        $this->consumer_user_key=getSettings($dbtable->get_db(),"reg-bpjs-vclaim-user-key","6vOEA10FF6");
        $this->is_pcare=false;
        $this->pcare_username=getSettings($dbtable->get_db(),"reg-bpjs-pcare-api-username","");
        $this->pcare_password=getSettings($dbtable->get_db(),"reg-bpjs-pcare-api-password","");
        $this->pcare_code=getSettings($dbtable->get_db(),"reg-bpjs-pcare-api-password","");

        $this->consumer_id      = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-cons-id","24619");
        $this->consumer_secret  = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-cons-secret","6vOEA10FF6");
        $this->ppkPelayanan     = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-ppk-pelayanan","1334R001");
        $this->url_base         = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-url-base","http://dvlp.bpjs-kesehatan.go.id:8081/devwslokalrest/");
        $this->port             = getSettings($dbtable->get_db(),"reg-bpjs-vclaim-api-port",8081);
        $this->is_pcare         = false;

    }


    protected function getKey(){
        return $this->key;
    }
     
    /**
     * @brief set wether used pcare or not
     * @param <unknown> $is_pcare 
     * @return  
     */
    public function setPCare($is_pcare){
        $this->is_pcare=$is_pcare;
        return $this;
    }
    
    /**
     * @brief this function used to do the curl methode
     * @param int $port         : the Port number that need to take
     * @param String $url       : the Url that need to be crawler
     * @param sending $methode  : the methode of sending request
     * @param String $request 
     * @return  
     */
    protected function do_curl($port,$url,$methode,$request){
        global $querylog;
        $querylog->addMessage($url);
        $querylog->addMessage($methode);
        $querylog->addMessage($request);

        $curl = curl_init();
        $generated=$this->generateBPJSCode($this->consumer_id,$this->consumer_secret);
        $this->key = $this->consumer_id.$this->consumer_secret.$generated['timestamp'];

        $array_object=array();
        $array_object[]="accept: application/json";
        $array_object[]="x-cons-id: ".$this->consumer_id;
        $array_object[]="x-signature: ".$generated['signature'];
        $array_object[]="x-timestamp: ".$generated['timestamp'];
        $array_object[]="user_key: ".$this->consumer_user_key;
        if($this->is_pcare && false){
            $pcare=$this->generatePCareAuthorization($this->pcare_username,$this->pcare_password,$this->pcare_code);
            $array_object[]="x-authorization: ".$pcare;
        }

        
        curl_setopt_array($curl, array(
          CURLOPT_PORT => $port,
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $methode,
          CURLOPT_POSTFIELDS => $request,
          CURLOPT_HTTPHEADER =>$array_object,
        ));
       
        $this->response = curl_exec($curl);
        $this->error = curl_error($curl);
        $querylog->addMessage($this->response);
        $querylog->addMessage("ERROR : ".$this->error);
        curl_close($curl);
    }
    
    
    /**
     * @brief this function used to generate BPJS Code from given
     *          consumer id and secret id
     * @param String $consmer_id that got from BPJS 
     * @param String $consumer_secret that got from BPJS 
     * @return  array("timestamp"=>"the Time Stamp When Create","signature"=>"String Encoded Signature")
     */
    protected function generateBPJSCode($consmer_id,$consumer_secret){
        $cur_zone = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $signature = hash_hmac('sha256', $consmer_id."&".$tStamp, $consumer_secret, true);
        $encodedSignature = base64_encode($signature);
        date_default_timezone_set($cur_zone);
        return array("timestamp"=>$tStamp,"signature"=>$encodedSignature);
    }
    
    /**
     * @brief this function used to generate PCARE X-Authorization
     *          by using this methdode the pcare model could be easyly
     *          manage.
     * @param String $username is the pcare Username 
     * @param String $password is the pcare password 
     * @param String $kode is the kode for application, default is 095 
     * @return  
     */
    protected function generatePCareAuthorization($username,$password,$kode="095"){
        return " Basic ".base64_encode($username.":".$password.":".$kode);
    }

     /**
     * @brief this function used to cek
     *          is there is active noreg of current nrm patient
     * @param array $rsp 
     * @param string $msg 
     * @return  null
     */
    protected function cekBindingBPJS(&$rsp,&$msg){
        /* cek is there any active registration number */
        $dbtable=new DBTable($this->getDBTable()->get_db(),"smis_rg_layananpasien");
        $x=$dbtable->select(array("nrm"=>$_POST['noMr'],"selesai"=>0));
        
        $reg=new Button("","","Register Pasien");
        $reg->setIcon("fa fa-sign-in");
        $reg->setClass("btn-primary");
        $reg->setIsButton(Button::$ICONIC_TEXT);
        $reg->setAction("cek_vklaim.register_pasien_bpjs()");
        
        $fix=new Button("","","Fix Register");
        $fix->setIcon("fa fa-eraser");
        $fix->setClass("btn-success");
        $fix->setIsButton(Button::$ICONIC_TEXT);
        $fix->setAction("cek_vklaim.fix_register()");
        
        if($x==null){                
            $rsp['noreg']=0;
            $msg.="<p>Pasien ini Belum Memiliki Nomor Registrasi yang Aktif, Silakan klik Tombol ".$reg->getHtml()."</p>";
        }else{
            $rsp['noreg']=$x->id;
            if($_POST['jnsPelayanan']=="2" && $x->no_sep_rj!=="" && $x->no_sep_rj!=$rsp['noSEP']){
                /* rawat jalan */
                $msg.="<h4>Peringatan</h4>";
                $msg.="<ul>";
                    $msg.="<li>Pasien ini Memiliki Satu Nomor Registrasi yang Aktif <font class='badge badge-warning'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, Pastikan bahwa nomor registrasi ini yang memang ingin di Bind dengan Nomor SEP</li>";
                    $msg.="<li>Nomor Registrasi <font class='badge badge-important'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, untuk Rawat Jalan Telah di Bind dengan Nomor SEP lain yakni <strong>".$x->no_sep_rj."</strong></li>";
                    $msg.="<li>jika Nomor Registrasi tersebut adalah Nomor Registrasi yang Lama tetapi masih aktif, silakan di pulangkan terlebih dahulu, lalu tekan tombol ".$fix->getHtml()." untuk melakukan pengecekan ulang</li>";
                    $msg.="<li>jika Nomor Registrasi <font class='badge badge-important'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, adalah nomor Registrasi yang memang ingin di binding dengan nomor SEP <strong>".$rsp['noSEP']."</strong>, cukup tekan tombol ".$reg->getHtml()." dan Nomor SEP <strong>".$x->no_sep_rj."</strong> akan di Replace (Pastikan Terlebih Dahulu) </li>";
                $msg.="</ul>";
            }else if($_POST['jnsPelayanan']=="1" && $x->no_sep_ri!=="" && $x->no_sep_ri!=$rsp['noSEP']){
                /* rawat inap */
                $msg.="<h4>Peringatan</h4>";
                $msg.="<ul>";
                    $msg.="<li>Pasien ini Memiliki Satu Nomor Registrasi yang Aktif <font class='badge badge-warning'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, Pastikan bahwa nomor registrasi ini yang memang ingin di Bind dengan Nomor SEP</li>";
                    $msg.="<li>Nomor Registrasi <font class='badge badge-important'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, untuk Rawat Jalan Telah di Bind dengan Nomor SEP lain yakni <strong>".$x->no_sep_ri."</strong></li>";
                    $msg.="<li>jika Nomor Registrasi tersebut adalah Nomor Registrasi yang Lama tetapi masih aktif, silakan di pulangkan terlebih dahulu, lalu tekan tombol ".$fix->getHtml()." untuk melakukan pengecekan ulang</li>";
                    $msg.="<li>jika Nomor Registrasi <font class='badge badge-important'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, adalah nomor Registrasi yang memang ingin di binding dengan nomor SEP <strong>".$rsp['noSEP']."</strong>, cukup tekan tombol ".$reg->getHtml()." dan Nomor SEP <strong>".$x->no_sep_ri."</strong> akan di Replace (di ganti) dengan nomor SEP ".$rsp['noSEP']." (Hati-hati Pastikan Terlebih Dahulu) </li>";
                $msg.="</ul>";
            }else{
                $msg.="<h4>Peringatan</h4>";
                $msg.="<ul>";
                    $msg.="<li>Pasien ini Memiliki Satu Nomor Registrasi yang Aktif <font class='badge badge-warning'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, Pastikan bahwa nomor registrasi ini yang memang ingin di Bind dengan Nomor SEP</li>";
                    $msg.="<li>jika Nomor Registrasi <font class='badge badge-warning'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, adalah Nomor Registrasi yang Lama tetapi masih aktif, silakan di pulangkan terlebih dahulu, lalu tekan tombol ".$fix->getHtml()." untuk melakukan pengecekan ulang.</li>";
                    $msg.="<li>jika Nomor Registrasi <font class='badge badge-warning'>".ArrayAdapter::format("only-digit8",$x->id)."</font>, adalah nomor Registrasi yang memang ingin di binding dengan nomor SEP <strong>".$rsp['noSEP']."</strong>, cukup tekan tombol ".$reg->getHtml()."</li>";
                $msg.="</ul>";
            }
        }
    }
    
}

?>