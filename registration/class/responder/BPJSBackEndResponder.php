<?php 
/**
 * this class Spesifically for Handling Response and Request to BPJS SEP Server
 * this class can handle the create SEP request, Delete , Update , Cek, and etc
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.3
 * @since       : 09-Feb-2017
 * @database    : - smis_rg_sep
 * 
 * */

require_once "registration/class/responder/BPJSResponder.php";
require_once "registration/class/responder/BPJSCheckerResponder.php";
class BPJSBackEndResponder extends BPJSCheckerResponder{
    
    public function command($command){
        if($command=="remove_sep"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->remove_sep();
            if($r['content']['success']==1){
                $pack->setAlertVisible(true);
                $pack->setAlertContent("Pemberitahuan !!!",$r['msg']);
                $pack->setContent($r['content']);    
            }else{
                $pack->setWarning(true,"Pemberitahuan !!!",$r['msg']);
                $pack->setContent("");
            }
            return $pack->getPackage();
        }else if($command=="pulang_sep"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->pulang_sep();
            if($r['content']['success']==1){
                $pack->setAlertVisible(true);
                $pack->setAlertContent("Pemberitahuan !!!",$r['msg']);
                $pack->setContent($r['content']);    
            }else{
                $pack->setWarning(true,"Pemberitahuan !!!",$r['msg']);
                $pack->setContent("");
            }
            return $pack->getPackage();
        }else if($command=="save"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->update_sep();
            if($r['content']['success']==1){
                $pack->setAlertVisible(true);
                $pack->setAlertContent("Pemberitahuan !!!",$r['msg']);
                $pack->setContent($r['content']);    
            }else{
                $pack->setWarning(true,"Pemberitahuan !!!",$r['msg']);
                $pack->setContent("");
            }
            return $pack->getPackage();
        }else if($command=="cek_sep"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $r=$this->cek_sep();
            $pack->setWarning(true,"Pemberitahuan !!!",$r);
            return $pack->getPackage();
        }else{
           return parent::command($command);
        }
    }
    
    
    /**
     * @brief this function handling how to make one sep
     *         especially in rawat inap, set the day when the patient out from hospital 
     * @return  array("msg"=>"string","content"=>array("success"=>0/1,"id"=>0/1));
     */
    public function pulang_sep(){
        $id=$_POST['id'];
        $pulang=$_POST['tgl_pulang'];
        $row=$this->getDBTable()->select(array("id"=>$id));
        $nosep=$row->noSEP;
        
        $sep=array();
        $sep['noSep']=$nosep;
        $sep['tglPlg']=$pulang;
        $sep['ppkPelayanan']=$this->ppkPelayanan;
        $dt=array("request"=>array("t_sep"=>$sep));
        $data=json_encode($dt);
        
        $url=$this->url_base."Sep/updtglplg";
        $metode="PUT";
        $request=$data;
        $this->do_curl($this->port,$url,$metode,$request);
        if ($this->error) {
            $message= "cURL Error #:" . $this->error;
            $success['id']=0;
            $success['success']=0;
            return array("msg"=>$message,"content"=>$success);
        } else {
            return $this->formatePulangSEP($this->response,$_POST['id'],$data,$pulang);
        }
    }
    
    /**
     * @brief this function merely for formatting and read the response from BPJS Server
     *          the format on whats the ResponsePackage Need when Updating Time of Go Home in SEP
     * @param String $response : String JSON response from BPJS Server 
     * @param int $id : id of smis_rg_sep that need to be update 
     * @param String $data : JSON String that sent to BPJS 
     * @param date $tanggal : the date in yyyy-mm-dd hh:mm:ii  
     * @return   array("msg"=>"string","content"=>array("success"=>0/1,"id"=>0/1));
     */
    private function formatePulangSEP($response,$id,$data,$tanggal){
        $json=json_decode($response,true);
        $success['id']=0;
		$success['success']=0;
        $msg="";
        if($json==null  ){
            $msg= "Update Tanggal Pulang Gagal, disebabkan Masalah Koneksi ke Server BPJS </br> Response Server BPJS </br> <pre>".json_encode($response)."</pre> </br> Response Server BPJS </br> <pre>".$data."</pre>";
        }else if($json['metadata']['code']!="200"){
            $msg= $json['metadata']['message'];
        }else{
            $msg= "Update Tanggal Pulang Berhasil pada Nomor SEP : <strong>".$json['response']."<strong>";
            $success['id']=$id;
            $update=array();
            $update['tglPulang']=$tanggal;
            $idx['id']=$id;
            $success['success']=$this->getDBTable()->update($update,$idx);
        }
        return array("msg"=>$msg,"content"=>$success);
    }
    
    /**
     * @brief this handling for removing sep both from database and from BPJS SEP Server
     * @return  array("msg"=>"string","content"=>array("success"=>0/1,"id"=>0/1));
     */
    public function remove_sep(){
        $id=$_POST['id'];
        $row=$this->getDBTable()->select(array("id"=>$id));
        $nosep=$row->noSEP;
        
        $sep=array();
        $sep['noSep']=$nosep;
        $sep['ppkPelayanan']=$this->ppkPelayanan;
        $dt=array("request"=>array("t_sep"=>$sep));
        $data=json_encode($dt);
        
        $url=$this->url_base."SEP/2.0/Delete";
        $metode="DELETE";
        $request=$data;
        $this->do_curl($this->port,$url,$metode,$request);
        if ($this->error) {
            $message= "cURL Error #:" . $this->error;
            $success['id']=0;
            $success['success']=0;
            return array("msg"=>$message,"content"=>$success);
        } else {
            return $this->formateRemoveSEP($this->response,$_POST['id'],$data);
        }
    }
    
    /**
     * @brief this function merely for formatting and read the response from BPJS Server
     *          the format on whats the ResponsePackage Need when removing SEP
     * @param String $response : String JSON response from BPJS Server 
     * @param int $id : id of smis_rg_sep that need to be update 
     * @param String $data : JSON String that sent to BPJS 
     * @return   array("msg"=>"string","content"=>array("success"=>0/1,"id"=>0/1));
     */
    private function formateRemoveSEP($response,$id,$data){
        $json=json_decode($response,true);
        $success['id']=0;
		$success['success']=0;
        $msg="";
        if($json==null  ){
            $msg= "Remove Gagal, disebabkan Masalah Koneksi ke Server BPJS </br> Response Server BPJS </br> <pre>".json_encode($response)."</pre> </br> Response Server BPJS </br> <pre>".$data."</pre>";
        }else if($json['metadata']['code']!="200"){
            $msg= $json['metadata']['message'];
        }else{
            $msg= "Remove Berhasil pada Nomor SEP : <strong>".$json['response']."<strong>";
            $success['id']=$id;
            $update=array();
            $update['delServer']=1;
            $update['prop']='del';
            $idx['id']=$id;
            $success['success']=$this->getDBTable()->update($update,$idx);
        }
        return array("msg"=>$msg,"content"=>$success);
    }
    
    /**
     * @brief this funtion used for updating sep 
     *         like the ICD code and etc. this function merely handling
     *         the formatting that needed by ResponsePackage
     * @return  array("msg"=>"string","content"=>array("success"=>0/1,"id"=>0/1));
     */
    public function update_sep(){
        $sep=$this->getBPJSPostData();
        $sep['noSep']=$_POST['noSEP'];
        $dt=array("request"=>array("t_sep"=>$sep));
        $data=json_encode($dt);
        
        $url=$this->url_base."SEP/Update";
        $metode="PUT";
        $request=$data;
        $this->do_curl($this->port,$url,$metode,$request);
        if ($this->error) {
            $message= "cURL Error #:" . $this->error;
            $success['id']=0;
            $success['success']=0;
            return array("msg"=>$message,"content"=>$success);
        } else {
            return $this->formateUpdateSEP($this->response,$data);
        }
    }
    
    /**
     * @brief this function merely for formatting and read the response from BPJS Server
     *          the format on whats the ResponsePackage Need when Updating SEP
     * @param String $data : JSON String that sent to BPJS 
     * @param date $tanggal : the date in yyyy-mm-dd hh:mm:ii  
     * @return   array("msg"=>"string","content"=>array("success"=>0/1,"id"=>0/1));
     */
    private function formateUpdateSEP($response,$data){
        $json=json_decode($response,true);
        $success['id']=0;
		$success['success']=0;
        $msg="";
        if($json==null  ){
            $msg= "Update Gagal, disebabkan Masalah Koneksi ke Server BPJS </br> Response Server BPJS </br> <pre>".json_encode($response)."</pre> </br> Response Server BPJS </br> <pre>".$data."</pre>";
        }else if($json['metadata']['code']!="200"){
            $msg= $json['metadata']['message'];
        }else{
            $msg= "Update Berhasil pada Nomor SEP : <strong>".$json['response']."<strong>";
            $success=$this->save();
        }
        return array("msg"=>$msg,"content"=>$success);
    }
    
    
    /**
     * @brief this function handling how to make get the detail of a SEP
     *          that synchronize to BPJS SEP Server 
     * @return  array("msg"=>"string","content"=>array("success"=>0/1,"id"=>0/1));
     */
    private function cek_sep(){
        $id=$_POST['id'];
        $row=$this->getDBTable()->select(array("id"=>$id));
        $nosep=$row->noSEP;
        $url=$this->url_base."SEP/".$nosep;        
        $metode="GET";
        $request="";
        $this->do_curl($this->port,$url,$metode,$request);

        if ($this->error) {
          return "cURL Error #:" . $this->error;
        } else {
            $data=json_decode($this->response,true);
            if($data==null ){
                return "Terjadi Kesalahan Akibat Koneksi Server BPJS";
            }else if( $data['metadata']['code']!="200" ){
                return $data['metadata']['message'];
            }else{
                require_once "registration/function/bpjs_array_adapter.php";
                $res=bpjs_array_adapter($data);
                $tp=new TablePrint("");
                $tp->setMaxWidth(false);
                $tp->setDefaultBootrapClass(true);
                $tp->addColumn("<big><strong>".$nosep."</strong></big>",10,1,"","center");
                $tp->commit("header");
                foreach($res as $x=>$v){
                    if(strpos($x,"Tanggal")!==false && $v!=null ){
                        $tp->addColumn($x,1,1)->addColumn(ArrayAdapter::format("date d M Y",$v),1,1,"body");
                    }else{
                        $tp->addColumn($x,1,1)->addColumn($v,1,1,"body");                
                    }
                }
                return $tp->getHtml();
            }
        }
    }
}
?>