<?php 

class RiwayatPasienResponder extends DBResponder{
    public function save(){
		$data=$this->postToArray();
		$id['id']=$this->getPost("id", 0);
		if($id['id']==0 || $id['id']=="" || !$this->dbtable->is_exist($id,$this->is_exist_include_del)){
			$result=$this->dbtable->insert($data);
			$id['id']=$this->dbtable->get_inserted_id();
			$success['type']='insert';
		}else {
            global $user;
            $username=$user->getUsername();
            $warp=null;
            /*mengubah status rawat jalan dan inap pasien*/
            if(isset($_POST['uri']) && $_POST['uri']==0){
                $warp['hbi']=DBController::$NO_STRIP_ESCAPE_WRAP;
                $data['hbi']="concat(hbi,' ','[-|4|".$username."|".date("Y-m-d h:i")."]')";
            }else if(isset($_POST['uri']) && $_POST['uri']==1){
                $warp['hbi']=DBController::$NO_STRIP_ESCAPE_WRAP;
                $data['hbi']="concat(hbi,' ','[+|4|".$username."|".date("Y-m-d h:i")."]')";
            }
            
            /*mengubah status aktif dan selesai pasien*/
            if(isset($_POST['selesai']) && $_POST['selesai']==1){
                $opp="[8|".$username."]";                
                $warp['obs']=1;
                $warp['opp']=2;
                $data['obs']="obs|8";
                $data['opp']="concat(opp,' ','".$opp."')";
            }else if(isset($_POST['selesai']) && $_POST['selesai']==0){
                $opp="[0|".$username."]";
                $warp['opp']=2;
                $data['opp']="concat(opp,' ','".$opp."')"; 
            }
            $result=$this->dbtable->update($data,$id,$warp);
			$success['type']='update';
		}
		$success['id']=$id['id'];
		$success['success']=1;
		if($result===false) $success['success']=0;
		return $success;
	}
}

?>