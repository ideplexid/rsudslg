<?php
require_once "registration/class/responder/BPJSResponder.php";
require_once "registration/class/responder/BPJSVClaimCheckerResponder.php";
class BPJSRujukanResponder extends BPJSVClaimCheckerResponder{
    private $no_rujukan;
    public function save(){
        $result = $this->saveServerSEP();
        if($result==""){
            return parent::save();
        }else{
            $this->response_pack->setWarning(true,"Terjadi Kesalahan",$result);
            return array("success"=>0,"id"=>0);
        }
    }

    function postToArray(){
        $data = parent::postToArray();
        if($data['no_rujukan']==""){
            $data['no_rujukan']=$this->no_rujukan;
        }
        return $data;
    }

    public function delete(){

        $x = $this->dbtable->select($_POST['id']);

        $result                                            = array();
        $result["request"]                                 = array();
        $result["request"]['t_rujukan']                    = array();
        $result["request"]['t_rujukan']['noRujukan']       = $x->no_rujukan;
        $result["request"]['t_rujukan']['user']            = getSettings($this->dbtable->get_db(),"reg-bpjs-vclaim-api-user","");  
        $url  = $this->url_base."Rujukan/delete";
        $data = json_encode($result);
        $this->do_curl($this->port,$url,"DELETE",$data);
        if ($this->error) {
            return "cURL Error #:" . $err;
        } else {
            $data     = json_decode($this->response,true);
            if($data==null ){
                $this->response_pack->setWarning(true,"Terjadi Kesalahan","Terjadi Kesalahan Akibat Koneksi Server BPJS");
                return "";
            }else if( $data['metaData']['code']!="200" ){
                $this->response_pack->setWarning(true,"Terjadi Kesalahan",$data['metaData']['message']);
                return "";
            }else{
                return parent::delete();
            }
        }
    }
    
    public function saveServerSEP(){        
         $result                                            = array();
         $result["request"]                                 = array();
         $result["request"]['t_rujukan']                    = array();
         $result["request"]['t_rujukan']['noSep']           = $_POST['nosep'];
         $result["request"]['t_rujukan']['tglRujukan']      = $_POST['tanggal'];
         $result["request"]['t_rujukan']['ppkDirujuk']      = $_POST['ppk'];
         $result["request"]['t_rujukan']['jnsPelayanan']    = $_POST['jenis'];
         $result["request"]['t_rujukan']['catatan']         = $_POST['catatan'];
         $result["request"]['t_rujukan']['diagRujukan']     = $_POST['diagnosa'];
         $result["request"]['t_rujukan']['tipeRujukan']     = $_POST['tipe'];
         $result["request"]['t_rujukan']['poliRujukan']     = $_POST['poli'];
         $result["request"]['t_rujukan']['user']            = getSettings($this->dbtable->get_db(),"reg-bpjs-vclaim-api-user","");
         $metode = "";
         if(isset($_POST['no_rujukan']) && $_POST['no_rujukan']!=""){
            $url    = $this->url_base."Rujukan/update";
            $result["request"]['t_rujukan']['noRujukan']    = $_POST['no_rujukan'];
            $metode = "PUT"; 
        }else{
            $url    = $this->url_base."Rujukan/insert";
            $metode = "POST";
         }
         $data = json_encode($result);
         
         $this->do_curl($this->port,$url,$metode,$data);
         if ($this->error) {
            return "cURL Error #:" . $err;
          } else {
              $data     = json_decode($this->response,true);
              if($data==null ){
                  return "Terjadi Kesalahan Akibat Koneksi Server BPJS";
              }else if( $data['metaData']['code']!="200" ){
                  return $data['metaData']['message'];
              }else{
                  if(isset($data["response"]["rujukan"])){
                    $this->no_rujukan = $data["response"]["rujukan"]["noRujukan"];
                  }else{
                    $this->no_rujukan = $data["response"];
                  }
                  return "";
              }
          }
    }
}
?>