<?php 

class PasienReport extends DBReport{
	
	public function excel(){
		require_once "smis-libs-out/php-excel/PHPExcel.php";
		$file = new PHPExcel ();
		$file->getProperties ()->setCreator ( "Register Pasien" );
		$file->getProperties ()->setLastModifiedBy ( "Register Pasien Rumah Sakit" );
		$file->getProperties ()->setTitle ( "Data Register Pasien" );
		$file->getProperties ()->setSubject ( "Data Register Pasien" );
		$file->getProperties ()->setDescription ( "Data Register Pasien" );
		$file->getProperties ()->setKeywords ( "Register Pasien " );
		$file->getProperties ()->setCategory ( "Register Pasien" );
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
		$sheet->setTitle ("Detail Pasien");
		
		$this->getDBTable()->setShowAll(true);
		$this->getDBTable()->setFetchMethode(DBTable::$OBJECT_FETCH);
		$package=$this->view("","0");
		
		$list=$package['dbtable']['data'];
		
		$sheet->setCellValue ( "A" . 1, "No.");
		$sheet->setCellValue ( "B" . 1, "NAMA" );
		$sheet->setCellValue ( "C" . 1, "NRM" );
		$sheet->setCellValue ( "D" . 1, "L/P" );
		$sheet->setCellValue ( "E" . 1, "Tgl Lahir" );
		$sheet->setCellValue ( "F" . 1, "Alamat" );
		$sheet->setCellValue ( "G" . 1, "Provinsi" );
		$sheet->setCellValue ( "H" . 1, "Kabupaten" );
		$sheet->setCellValue ( "I" . 1, "Kecamatan" );
		$sheet->setCellValue ( "J" . 1, "Kelurahan" );
        
		$sheet->setCellValue ( "K" . 1, "Tanggal" );
        $sheet->setCellValue ( "L" . 1, "Marital" );
        $sheet->setCellValue ( "M" . 1, "Pendidikan" );
        $sheet->setCellValue ( "N" . 1, "Agama" );
        $sheet->setCellValue ( "O" . 1, "Suku" );
        $sheet->setCellValue ( "P" . 1, "Bahasa" );
        $sheet->setCellValue ( "Q" . 1, "Pekerjaan" );
        
        $index=1;
		$no=0;
		$sheet->getStyle ( "A1:Q1")->getFont ()->setBold ( true );
		foreach($list as $x){
			$no++;
			$index++;
			$sheet->setCellValue ( "A" . $index, $no."." );
			$sheet->setCellValue ( "B" . $index, $x->nama );
			$sheet->setCellValue ( "C" . $index, "'".$x->id );
			$sheet->setCellValue ( "D" . $index, $x->kelamin==0?"L":"P" );
			$sheet->setCellValue ( "E" . $index, ArrayAdapter::format("date d M Y",$x->tgl_lahir) );
			$sheet->setCellValue ( "F" . $index, $x->alamat );
			$sheet->setCellValue ( "G" . $index, $x->nama_provinsi );
			$sheet->setCellValue ( "H" . $index, $x->nama_kabupaten );
			$sheet->setCellValue ( "I" . $index, $x->nama_kecamatan );
			$sheet->setCellValue ( "J" . $index, $x->nama_kelurahan );            
			$sheet->setCellValue ( "K" . $index, ArrayAdapter::format("date d M Y",$x->tanggal) );
			$sheet->setCellValue ( "L" . $index, $x->status);
			$sheet->setCellValue ( "M" . $index, $x->pendidikan );
			$sheet->setCellValue ( "N" . $index, $x->suku );
			$sheet->setCellValue ( "O" . $index, $x->bahasa );
            $sheet->setCellValue ( "P" . $index, $x->agama );
            $sheet->setCellValue ( "Q" . $index, $x->agama );
		}
		$sheet->getStyle ( 'A1:Q' . $index )->applyFromArray ( $fillcabang );
		
		
		$list=$package['ddiagram']['data'];
		$file->createSheet ( NULL, 1 );
		$file->setActiveSheetIndex ( 1 );
		$sheet = $file->getActiveSheet ( 1 );
		$sheet->setTitle ("Resume Pasien");
		$sheet->setCellValue ( "A" . 1, "No.");
		$sheet->setCellValue ( "B" . 1, "NAMA" );
		$sheet->setCellValue ( "C" . 1, "JUMLAH" );
		$index=1;
		$no=0;
		$sheet->getStyle ( "A1:C1")->getFont ()->setBold ( true );
		foreach($list as $x){
			$no++;
			$index++;
			$sheet->setCellValue ( "A" . $index, $no."." );
			$sheet->setCellValue ( "B" . $index, $x->nama );
			$sheet->setCellValue ( "C" . $index, $x->jumlah );
		}
		$sheet->getStyle ( 'A1:C' . $index )->applyFromArray ( $fillcabang );
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="Data Register Pasien.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );
	}
	
}

?>