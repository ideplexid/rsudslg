<?php 
/**
 * this class used for Setting in Registration
 * used this one because we need to 
 * call chooser that placed default base District 
 * of Regitration Patient
 * 
 * @author 		: Nurul Huda
 * @since		: 7 Sept 2016
 * @version		: 1.0.0
 * @licence		: LGPLv2
 * @copyright	: goblooge@gmail.com
 * */

class RegistrasiSettingBuilder extends SettingsBuilder {
	public function getJS() {
		$result = parent::getJS ();
		ob_start ();
		?>
		<script type="text/javascript">	
			var kabupaten_settings_registration;
			$(document).ready(function(){
				kabupaten_settings_registration=new TableAction("kabupaten_settings_registration","registration","settings",new Array());
				kabupaten_settings_registration.setSuperCommand("kabupaten_settings_registration");
				kabupaten_settings_registration.selected=function(json){
					$("#registration-kabupaten-nama").val(json.nama);
					$("#registration-kabupaten-id").val(json.id);
					$("#registration-provinsi-id").val(json.no_prop);
				};
                kabupaten_settings_registration.addRegulerData=function(data){
                    data['slug']="registration";
                    return data;
                };
			});
		</script>
		<?php
		$r2 = ob_get_clean ();
		$result .= $r2;
		return $result;
	}
}

?>