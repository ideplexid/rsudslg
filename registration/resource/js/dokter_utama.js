var pasien;
var dokter_utama;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id","kode","mulai","selesai");
	dokter_utama=new TableAction("dokter_utama","registration","dokter_utama",column);
	dokter_utama.getRegulerData=function(){
		var reg_data={
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
				};
		reg_data['nrm_pasien']=$("#nrm_pasien").val();
		reg_data['nama_pasien']=$("#nama_pasien").val();
		reg_data['alamat_pasien']=$("#alamat_pasien").val();
        reg_data['filter_tanggal']=$("#filter_tanggal").val();
		return reg_data;
	};
	dokter_utama.show_add_form=function(){
		if($("#nrm_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
	};
    
    dokter_utama.autocorrect=function(){
        var data=this.getRegulerData();
        data['super_command']="autocorrect";
        data['command']="list";
        showLoading();
        $.post("",data,function(res){
            var content=getContent(res);
            data['command']="one";
            dismissLoading();
            smis_loader.showLoader();
            dokter_utama.proceed_loop(content,data,0);
            
        });
    };
    
    dokter_utama.full_autocorrect=function(){
        var data=this.getRegulerData();
        data['super_command']="autocorrect";
        data['command']="full_list";
        showLoading();
        $.post("",data,function(res){
            var content=getContent(res);
            data['command']="one";
            dismissLoading();
            smis_loader.showLoader();
            dokter_utama.proceed_loop(content,data,0);
            
        });
    };
    
    dokter_utama.proceed_loop=function(list_id,data,current){
        if(!smis_loader.isShown() || current>=list_id.length){
            smis_loader.hideLoader();
            return;
        }   
        
        data['id']=list_id[current]['id'];
        current++;
        $.post("",data,function(res){
            var json=getContent(res);
            
            smis_loader.updateLoader('true',json+"..... "+current+" / "+list_id.length,current*100/list_id.length);
            dokter_utama.proceed_loop(list_id,data,current);
        });
    };
    

	pasien=new TableAction("pasien","registration","dokter_utama",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		var nama=json.nama;
		var nrm=json.id;
		var alamat=json.alamat;
		$("#nama_pasien").val(nama);
		$("#nrm_pasien").val(nrm);
		$("#alamat_pasien").val(alamat);
		dokter_utama.view();
	};
    
    
    
	
	$("#dokter_utama_mulai").on("change",function(){
		var value=$(this).val();
		var date_only=value.substring(0,11);
		var year_only=Number(date_only.substring(0,4))+1;
		var date_month_only=date_only.substring(4);
		$("#dokter_utama_selesai").val(year_only+""+date_month_only);
		var data=dokter_utama.getRegulerData();
		data["action"]="get_jadwal_dokter";
		data["tanggal_masuk"]=value;
		$.post("",data,function(res){
			var json=getContent(res);
			$("#dokter_utama_kode").val(json);
		});
	});


	$(".mydate").datepicker();
	$(".mydatetime").datetimepicker({minuteStep:1});
    
    $('#nrm_pasien').keyup(function(e) {
        if(e.keyCode==13){
            var vall=$(this).val();
            pasien.select(vall);
        };
    });
    
});