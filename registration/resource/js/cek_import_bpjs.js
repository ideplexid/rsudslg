var CURRENT_CEK_IMPORT_BPJS_ID=0;
$(document).ready(function(){
    $("#cek_import_bpjs_nama, #cek_import_bpjs_no_pst").keyup(function(e){
         if (e.keyCode == 13) {
            cek_import_bpjs.view();
         }
    });
        
    cek_import_bpjs.register=function(id){
        showLoading();
        var a=this.getEditData();
        a['id']=id;
        $.post("",a,function(res){
           var json=getContent(res);
           registration_patient.register(json.nrm);
           setTimeout(function(){
                $("#daftar_pasien_nobpjs").val(json.no_pst);
                $("#daftar_pasien_asuransi").val(json.kode_asuransi);
                $("#daftar_pasien_carabayar").val("bpjs");
                dismissLoading();
           },5000);
        });
    }
    
    cek_import_bpjs.daftar=function(id){
        showLoading();
        var a=this.getEditData();
        a['id']=id;
        $.post("",a,function(res){
           var json=getContent(res);
           setCurrentImportBPJSId(id);
           $("#registration_patient_tab_anchor").trigger("click");
           registration_patient.show_add_form();
           $("#registration_patient_nama").val(json.nama_pasien);
           $("#registration_patient_kelamin").val(json.jk-1);
           smis_edit("#registration_patient_tgl_lahir",json.tgl_lahir);
           $("#registration_patient_alamat").val(json.alamat);
           $("#registration_patient_tgl_lahir").trigger("change");
           registration_patient.aftersave=function(json){
               nrm=json.id;
               saveCurrentBPJSNRM(nrm);
           };
           dismissLoading();
        });
    }
});

function getCurrentImportBPJSId(){
    return CURRENT_CEK_IMPORT_BPJS_ID;
}

function setCurrentImportBPJSId(id){
    CURRENT_CEK_IMPORT_BPJS_ID=id;
}

function saveCurrentBPJSNRM(nrm){
    if(CURRENT_CEK_IMPORT_BPJS_ID==0){
        registration_patient.aftersave=function(json){};
        return;
    }else{
        showLoading();
        data=cek_import_bpjs.getRegulerData();
        data['command']="save";
        data['nrm']=nrm;
        data['id']=getCurrentImportBPJSId();        
        $.post("",data,function(res){
            var json=getContent(res);
            cek_import_bpjs.register(getCurrentImportBPJSId());
            setCurrentImportBPJSId(0);
            dismissLoading();
        });
    }
    
}