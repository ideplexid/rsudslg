var register_menginap;
var dokter;
var urj;
$(document).ready(function(){

	shortcut.add("ctrl+b", function() {
		if(!$("#register_menginap_add_form").hasClass("in")){
			$("#register_menginap_add").trigger("click");
		}
	});
	shortcut.add("ctrl+s", function() {
		if($("#register_menginap_add_form").hasClass("in")){
			$("#register_menginap_save").trigger("click");
		}
	});

	shortcut.add("ctrl+c", function() {
		if(! $("#register_menginap_add_form").hasClass("in")){
			$("#register_menginap_kriteria").focus();
		}
	});
	shortcut.add("ctrl+m", function() {
		if(! $("#register_menginap_add_form").hasClass("in") ){
			if( $("#menginap_anchor").parent().hasClass("active")){
				activeTab("#empty_room");
			}else if($("#empty_room_anchor").parent().hasClass("active")){
				activeTab("#menginap");
			}
		}
	});

	shortcut.add("ctrl+h", function() {
		helptable('registration','rawat_inap','','','');
	});
		
	$(".mydate").datepicker();
	var column 		  = new Array('id','uri','id_dokter','carabayar','tanggal_inap','nrm','nama_pasien','nama_dokter','kamar_inap','administrasi_inap','no_sep_ri',"bed_kamar","id_bed_kamar");
	register_menginap = new TableAction("register_menginap","registration","register_menginap",column);
	register_menginap.setEnableAutofocus(true);
	register_menginap.setNextEnter();
	
	register_menginap.dogtag_jsprint	= function(id){daftar_pasien.dogtag_jsprint(id);};
	register_menginap.dogtag			= function(id){daftar_pasien.dogtag(id);};
	register_menginap.b_wbpatient_print	= function(id){daftar_pasien.b_wbpatient_print(id);};
	register_menginap.a_wbpatient_print	= function(id){daftar_pasien.a_wbpatient_print(id);};
	register_menginap.label_inap		= function (id){daftar_pasien.label_inap(id);};
	register_menginap.batal_pasien		= function (id){
		var data		= daftar_pasien.getRegulerData();
		data['action']	= "batal_pasien";
		data['id']		= id;
		showLoading();
		$.post("",data,function(res){
			getContent(res);
			register_menginap.view();
			dismissLoading();
		});
	};
    
    register_menginap.sep_bpjs_ri=function(id){
        var a		 = this.getRegulerData();
        a['command'] = "edit";
        a['id']		 = id;
        showLoading();
        $.post("",a,function(res){
            var json = getContent(res);
            cek_bpjs_inap_load(json.nrm,id,json.ktp);
            dismissLoading();
        });
    };
    
        /**
     * editing sebuah registrasi pasien yang secara otomatis
     * nosep untuk rawat jalan langsung di binding dengan data yang 
     * sudah nosep yang baru.
     * */
    register_menginap.edit_nosep=function (id,nosep,nobpjs){
        var self		= this;
        showLoading();
        var edit_data	= this.getEditData(id);
        $.post('',edit_data,function(res){		
            var json	= getContent(res);
            if(json==null) return;
            for(var i=0;i<self.column.length;i++){
                if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
                    continue;
                }
                var name	= self.column[i];
                var the_id="#"+self.prefix+"_"+name;
                smis_edit(the_id,json[""+name]);
            }
            $("#"+self.prefix+"_no_sep_ri").val(nosep);
            $("#"+self.prefix+"_nobpjs").val(nobpjs);
            dismissLoading();
            self.disabledOnEdit(self.column_disabled_on_edit);
            self.show_form();
        });
    };


	register_menginap.jsetup_sosial = function(id){
		var data		= this.getRegulerData();
		data['command']	= 'print-element';
		data['slug']	= 'print-element';
		data['id']		= id;
		showLoading();
		$.post("",data,function(res){
			var json	= getContent(res);
			dismissLoading();
			if(json==null) {
                return;
            }			
			try{
				$("#printing_area").html(json);
			}catch(err){
				smis_print(json);
			}
		});
	};
	
	register_menginap.save = function (){
		if(!this.cekSave()){
			return;
		}
		var self		= this;
		var a			= this.getSaveData();
		showLoading();
		$.ajax({url:"",data:a,type:'post',success:function(res){
			$("#"+self.prefix+"_add_form").smodal('hide');
			var json	= getContent(res);		
			if(json==null || json=="") {
				self.view();
				dismissLoading();
				return;
			}
			if($("#reg_print_data_social_jsetup").val()=="1"){
				register_menginap.jsetup_sosial(json.id); 	
			}else{
				register_menginap.printelement(json.id); 	
			}	
			self.view();
			dismissLoading();				
		}});
	};

	register_menginap.addViewData=function(data){
		data['dari']	= $("#"+this.prefix+"_dari").val();
		data['sampai']	= $("#"+this.prefix+"_sampai").val();
		return data;
	};
	
	
	register_menginap.karcis=function(){
		var data				= this.getRegulerData();
		data['action']			= "karcis";
		data['super_command']	= "karcis";
		data['polislug']		= $("#"+this.prefix+"_kamar_inap").val();
		data['urji']			= "uri";
		var self				= this;
		showLoading();
		$.post("",data,function(res){
			var json			= getContent(res);
			setMoney("#"+self.prefix+"_administrasi_inap", json.administrasi);
			$("#"+self.prefix+"_sisa_kamar").val(json.sisa);
			$("#"+self.prefix+"_kelas_kamar").val(json.kelas);
			dismissLoading();
		});
	};

	$("#register_menginap_kamar_inap").click(function(){
		register_menginap.karcis();
		$("#register_menginap_bed_kamar").val("");
        $("#register_menginap_id_bed_kamar").val("");
	});	
	
	register_menginap.cancel=function (id){
		var self						= this;
		var a							= this.getRegulerData();
		bootbox.confirm("Anda yakin Membatalkan Rawat Inap Pasien ini ?", function(result) {
           if(result){
               showLoading();
               a['uri']					= 0;
               a['id_dokter']			= "";
               a['nama_dokter']			= "";
               a['kamar_inap']			= "";
               a['administrasi_inap']	= 0;
               a['tanggal_inap']		= "0000-00-00";
               a['command']				= "save";
               a['id']					= id;					   					   
                $.post('',a,function(res){
                    var json			= getContent(res);
                    if(json==null) return;
                    self.view();
                    dismissLoading();
                });
           }
        }); 
	};
	register_menginap.view();
	dokter = new TableAction("dokter","registration","register_menginap",new Array());
	dokter.setSuperCommand("dokter");
	dokter.selected = function(json){
		$("#register_menginap_nama_dokter").val(json.nama);
		$("#register_menginap_id_dokter").val(json.id);
	};

	urj = new TableAction("urj","registration","register_menginap",new Array());
	urj.setSuperCommand("urj");
	urj.selected = function(json){
		$("#register_menginap_id").val(json.id);
		$("#register_menginap_nama_pasien").val(json.nama_pasien);
		$("#register_menginap_nrm").val(json.nrm);
		$("#register_menginap_carabayar").val(json.carabayar);
	};

	stypeahead("#register_menginap_nama_pasien",3,urj,"nama_pasien",function(json){
		$("#register_menginap_id").val(json.id);
		$("#register_menginap_nama_pasien").val(json.nama_pasien);
		$("#register_menginap_nrm").val(json.nrm);
		$("#register_menginap_carabayar").val(json.carabayar);
		$("#register_menginap_nama_dokter").focus();
		return item.name;
	});
	
	stypeahead("#register_menginap_nama_dokter",3,dokter,"nama",function(item){
		$("#register_menginap_id_dokter").val(item.id);
		$("#register_menginap_nama_dokter").val(item.nama);
		$("#register_menginap_kamar_inap").focus();
		return item.name;
	});		
});