var lap_rujukan;
$(document).ready(function() {
    $('.mydatetime').datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column = new Array('id','nama','keterangan','slug');
    lap_rujukan = new TableAction("lap_rujukan","registration","lap_rujukan",column);
    lap_rujukan.getRegulerData = function() {
        var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
                origin:$("#"+this.prefix+"_origin").val()
				};
		return reg_data;
    }
});
