var rekap_jadwal_dokter;
var rekap_jadwal_dokter_karyawan;
var rekap_jadwal_dokter_data;
var IS_rekap_jadwal_dokter_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    rekap_jadwal_dokter=new TableAction("rekap_jadwal_dokter","registration","rekap_jadwal_dokter",new Array());
    rekap_jadwal_dokter.addRegulerData=function(data){
        data['dari']=$("#rekap_jadwal_dokter_dari").val();
        data['sampai']=$("#rekap_jadwal_dokter_sampai").val();
        $("#dari_table_rekap_jadwal_dokter").html(getFormattedDate(data['dari']));
        $("#sampai_table_rekap_jadwal_dokter").html(getFormattedDate(data['sampai']));			
        return data;
    };

    rekap_jadwal_dokter.batal=function(){
        IS_rekap_jadwal_dokter_RUNNING=false;
        $("#rekap_jadwal_dokter_modal").smodal("hide");
    };
    
    rekap_jadwal_dokter.afterview=function(json){
        if(json!=null){
            $("#kode_table_rekap_jadwal_dokter").html(json.nomor);
            $("#waktu_table_rekap_jadwal_dokter").html(json.waktu);
            rekap_jadwal_dokter_data=json;
        }
    };

    rekap_jadwal_dokter.rekaptotal=function(){
        if(IS_rekap_jadwal_dokter_RUNNING) return;
        $("#rekap_jadwal_dokter_bar").sload("true","Fetching total data",0);
        $("#rekap_jadwal_dokter_modal").smodal("show");
        IS_rekap_jadwal_dokter_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var json=getContent(res);
            if(json.total>0) {
                rekap_jadwal_dokter.rekaploop(0,json.total,json.data);
            } else {
                $("#rekap_jadwal_dokter_modal").smodal("hide");
                IS_rekap_jadwal_dokter_RUNNING=false;
            }
        });
    };

    rekap_jadwal_dokter.rekaploop=function(current,total,data){
        if(current>=total || !IS_rekap_jadwal_dokter_RUNNING) {
            $("#rekap_jadwal_dokter_modal").smodal("hide");
            IS_rekap_jadwal_dokter_RUNNING=false;
            rekap_jadwal_dokter.view();
            return;
        }
        $("#rekap_jadwal_dokter_bar").sload("true","Processing "+getFormattedDate(data[current])+"... [ "+current+" / "+total+" ] ",(current*100/total));
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['tanggal']=data[current];
        d['limit_start']=current;			
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){rekap_jadwal_dokter.rekaploop(++current,total,data)},100);
        });
    };
            
});