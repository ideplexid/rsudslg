function load_empty_room(){
	var a={	
			page:"registration",
			action:"empty_room",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	showLoading();
	$.post("",a,function(res){
		var c=getContent(res);
		$("#empty_room").html(c);
		dismissLoading();
	});
}

function load_register_menginap(){
	if($("#register_menginap").html()!=""){
			return;
	}
	var a={	
			page:"registration",
			action:"register_menginap",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	showLoading();
	$.post("",a,function(res){
		$("#register_menginap").html(res);
		dismissLoading();
	});
}

function cek_bpjs_inap_load_only(){
	var a={	
			page:"registration",
			action:"cek_bpjs_inap",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	showLoading();
	$.post("",a,function(res){
		$("#cek_bpjs_inap").html(res);
		dismissLoading();
	});
}


function cek_bpjs_inap_load(nrm,noreg,ktp){
	var a={	
			page:"registration",
			action:"cek_bpjs_inap",
			super_command:"",
			prototype_name:"",
			prototype_slug:"",
			prototype_implement:""
			};
	showLoading();
	$.post("",a,function(res){
		$("#cek_bpjs_inap").html(res);
		$("#cek_bpjs_inap_no_ktp").val(ktp);
		$("#cek_bpjs_inap_noMr").val(nrm);
        $("#cek_bpjs_inap_noREG").val(noreg);
        activeTab("#cek_bpjs_inap");
        cek_bpjs_inap.fetch_server();
		dismissLoading();
	});
}



$(document).ready(function(){
	load_register_menginap();
});