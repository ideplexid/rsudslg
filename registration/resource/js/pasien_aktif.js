$(document).ready(function(){
	
	pasien_aktif.track=function(id){
		var data=this.getRegulerData();
		data['id']=id;
		data['command']="track";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			dismissLoading();
			showFullWarning("Traking Ruangan Pasien",json,"half_model");
		});
	};
	
	
	pasien_aktif.sudah=function(id){
			var d=this.getRegulerData();
			d['command']="save";
			d['id']=id;
			d['status_dokumen']="terkirim";
			showLoading();
			$.post("",d,function(res){
				var json=getContent(res);
				pasien_aktif.view();
				dismissLoading();
			});
		};

		pasien_aktif.belum=function(id){
			var d=this.getRegulerData();
			d['command']="save";
			d['id']=id;
			d['status_dokumen']="";
			showLoading();
			$.post("",d,function(res){
				var json=getContent(res);
				pasien_aktif.view();
				dismissLoading();
			});
		};

		pasien_aktif.kembali=function(id){
			var d=this.getRegulerData();
			d['command']="save";
			d['id']=id;
			d['status_dokumen']="kembali";
			showLoading();
			$.post("",d,function(res){
				var json=getContent(res);
				pasien_aktif.view();
				dismissLoading();
			});
		};
		
		$('#pasien_aktif_nrm').keydown(function(e){
			if(e.keyCode == 13){
				 e.preventDefault();
				 pasien_aktif.view();
				 return false;
			}
		});
		
		$("#pasien_aktif_jenis, #pasien_aktif_uri, #pasien_aktif_dokumen, #pasien_aktif_selesai, #pasien_aktif_urutan").on("change",function(e){
			pasien_aktif.view();
		});
			
});