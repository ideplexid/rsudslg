var cek_bpjs;
var cek_bpjs_icd;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});    
    $("#cek_bpjs_no_bpjs, #cek_bpjs_no_ktp ").keyup(function(e){ 
        var code = e.which;
        if(code==13)e.preventDefault();
        if(code==32||code==13||code==188||code==186){
            cek_bpjs.fetch_server();
        }
    });
    
    cek_bpjs =new TableAction("cek_bpjs","registration","cek_bpjs");
    cek_bpjs.fetch_server=function(){
        $("#cek_bpjs_noSEP").val("");
        data=this.getRegulerData();
        data['no_bpjs']=$("#cek_bpjs_no_bpjs").val();
        data['no_ktp']=$("#cek_bpjs_no_ktp").val();
        data['command']="cek_bpjs";
        
        if(data['no_bpjs']=="" && (data['no_ktp']=="" || data['no_ktp']=="-") ){
            showWarning("Terjadi Kesalahan !!!","Masukan No. BPJS atau No. KTP, jika No. BPJS terisi No. KTP tidak dihiraukan");
        }
        
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_page").html(json.table);
              cek_bpjs.reset_part();
              if(json.raw != null && json.raw.status=="AKTIF"){
                  $("#cek_bpjs_noKartu").val(json.raw.nobpjs);
                  $("#cek_bpjs_no_ktp").val(json.raw.nik);
                  $("#cek_bpjs_no_bpjs").val(json.raw.nobpjs);
                  $("#cek_bpjs_ppkRujukan").val(json.raw.kd_provider);
              }
              dismissLoading();
        });
    };
    
    cek_bpjs.cek_rujukan=function(){
        $("#cek_bpjs_noSEP").val("");
        
        data=this.getRegulerData();
        data['noRujukan']=$("#cek_bpjs_noRujukan").val();
        data['noBPJS']=$("#cek_bpjs_no_bpjs").val();
        data['command']="cek_rujukan";
        
        if(data['noBPJS']=="" && data['noRujukan']=="" ){
            showWarning("Terjadi Kesalahan !!!","Silakan Isikan Nomor Rujukanya atau Nomor BPJS Terlebih Dahulu, kemudian tekan enter atau Klik Tombol Pencarian Rujukan");
        }
        
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_noRujukan").val(data['noRujukan']);
              if(json.message=="Data Ditemukan"){
                  cek_bpjs.reset_part();
                  $("#cek_bpjs_no_bpjs").val(json.nobpjs);     
                  $("#cek_bpjs_noKartu").val(json.nobpjs);                  
                  $("#cek_bpjs_tglRujukan").val(json.tgl_kunjungan);
                  $("#cek_bpjs_ppkRujukan").val(json.kd_provider);
                  $("#cek_bpjs_diagAwal").val(json.kode_diagnosa);
                  $("#cek_bpjs_poliTujuan").val(json.kd_poli);
                  $("#cek_bpjs_catatan").val(json.catatan);
                  $("#cek_bpjs_noRujukan").val(json.no_kunjungan);
                  $("#cek_bpjs_catatan").val(json.catatan);
                  $("#cek_bpjs_no_ktp").val(json.nik);                  
                  if(json.kd_poli!=null && json.kd_poli!=""){
                    $("#cek_bpjs_jnsPelayanan").val(2);
                  }else{
                    $("#cek_bpjs_jnsPelayanan").val(1); 
                  }
                  $("#cek_bpjs_jnsPelayanan").trigger("change");
              }else{
                  showWarning("Terjadi Kesalahan !!!",json.message);
              }
              dismissLoading();
        });
    };
    
    cek_bpjs.reset=function(){
        $("#cek_bpjs_noSEP").val("");
        $("#cek_bpjs_no_ktp").val(""); 
        $("#cek_bpjs_noMr").val("");
        cek_bpjs.reset_part();
    };
    
    cek_bpjs.reset_part=function(){
        var currentdate = new Date(); 
        var month=(currentdate.getMonth()+1);
        if(month<10){
                month="0"+month;
        }
        
        var dt=currentdate.getDate();
        if(dt<10){
                dt="0"+dt;
        }
        
        var hour=currentdate.getHours();
        if(hour<10){
                hour="0"+hour;
        }
        
        var minute=currentdate.getMinutes();
        if(minute<10){
                minute="0"+minute;
        }
        
        var second=currentdate.getSeconds();
        if(second<10){
                second="0"+second;
        }
        
        var datetime = currentdate.getFullYear() + "-"
                        + month + "-" 
                        + dt + " "  
                        + hour + ":"  
                        + minute + ":" 
                        + second;
         
        $("#cek_bpjs_noKartu").val("");
        $("#cek_bpjs_no_bpjs").val("");
        $("#cek_bpjs_tglSep").val(datetime);
        $("#cek_bpjs_tglRujukan").val(datetime);
        $("#cek_bpjs_noRujukan").val("");
        $("#cek_bpjs_ppkRujukan").val("");
        $("#cek_bpjs_jnsPelayanan").val("");
        $("#cek_bpjs_diagAwal").val("");
        $("#cek_bpjs_poliTujuan").val("");
        $("#cek_bpjs_klsRawat").val("");
        $("#cek_bpjs_lakaLantas").val("");
        $("#cek_bpjs_lokasiLaka").val("");
        $("#cek_bpjs_catatan").val("");
        $("#cek_bpjs_noREG").val("");
    };
    
     cek_bpjs.print_sep=function(){
         var nosep=$("#cek_bpjs_noSEP").val();
         if(nosep==""){
            showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
            return;
        }
         data=this.getRegulerData();
         data['nosep']=nosep;
         data['action']="print_sep";
         showLoading();
         $.post("",data,function(res){
             var json=getContent(res);
             smis_print(json);
             dismissLoading();
         });
     };
     
     cek_bpjs.sep_sementara=function(){
         var self=this;
         bootbox.confirm("SEP Sementara tidak terhubung dengan SIM SEP BPJS, Hanya Dipakai ketika Jaringan BPJS Bermasalah dan harus diinput ulang oleh petugas maksimal 3 hari", function(result){ 
            if(result){
                data                    = self.getRegulerData();
                data['tanggal']         = $("#"+self.prefix+"_tglSep").val();
                data['no_bpjs']         = $("#"+self.prefix+"_no_bpjs").val()==""?$("#"+self.prefix+"_no_bpjs").val():$("#"+self.prefix+"_noKartu").val();
                data['no_rujukan']      = $("#"+self.prefix+"_noRujukan").val();
                data['no_ktp']          = $("#"+self.prefix+"_no_ktp").val();
                data['nrm']             = $("#"+self.prefix+"_noMr").val();
                data['poli_tujuan']     = $("#"+self.prefix+"_poliTujuan option:selected").text();;
                data['diagnosa_awal']   = $("#"+self.prefix+"_diagAwal").val();
                data['catatan']         = $("#"+self.prefix+"_catatan").val();
                data['jns_rawat']       = $("#"+self.prefix+"_jnsPelayanan option:selected").text();
                data['kasus_laka']      = $("#"+self.prefix+"_lakaLantas option:selected").text();
                data['lokasi_laka']     = $("#"+self.prefix+"_lokasiLaka").val();;
                data['action']          = "print_sep_sementara";
                showLoading();
                $.post("",data,function(res){
                    var json=getContent(res);
                    smis_print(json);
                    dismissLoading();
                });
            }
        });
     };
     
    cek_bpjs.register_pasien_bpjs=function(){
        var nosep=$("#"+this.prefix+"_noSEP").val();
        var noreg=$("#"+this.prefix+"_noREG").val();
        var nrm=$("#"+this.prefix+"_noMr").val();
        var nobpjs=$("#"+this.prefix+"_noKartu").val();
        var kelas=$("#"+this.prefix+"_klsRawat").val();
        
        if(nosep==""){
            showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
            return;
        }
        
        if(noreg=="0"){
            daftar_pasien.show_add_form_nosep(nrm,nosep,nobpjs,kelas);
        }else{
            daftar_pasien.edit_nosep(noreg,nosep,nobpjs,kelas);    
        }
    };
    
    cek_bpjs.cek_nomor_rm=function(nrm,ktp){
        cek_bpjs.reset();
        $("#cek_bpjs_noMr").val(nrm);
        $("#cek_bpjs_no_ktp").val(ktp);
        $("#cek_bpjs_no_bpjs").val("");
        if(ktp=="" || ktp=="-"){
            showWarning("KTP/NIK Kosong","dikarenakan KTP/NIK kosong, silakan isikan nomor BPJS pasien yang bersangkutan");
        }else{
            $("#cek_bpjs_button").trigger("click");
        }
    };
    
    cek_bpjs.fix_register=function(){
        data=this.getRegulerData();
        data['noSEP']=$("#cek_bpjs_noSEP").val();
        data['noMr']=$("#cek_bpjs_noMr").val();
        
        if(data['noSEP']=="" || data['noMr']==""){
            showWarning("Peringatan !!!","Silakan Bikin BPJS SEP terlebih dahulu, Jika No RM kosong, silakan buka Menu Registrasi Pasien, Pilih pasienya, kemudian Tekan Opsi SEP. Pastikan No. KTP/NIK terisi untuk kemudahan");
            return;
        }
        
        data['command']="fix_register";
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_noREG").val(json.noreg);
              dismissLoading();
        });
    };

    
    cek_bpjs.reg_sep=function(){
        $("#cek_bpjs_noSEP").val("");
        data=this.getRegulerData();
        data['noKartu']=$("#cek_bpjs_noKartu").val();
        data['tglSep']=$("#cek_bpjs_tglSep").val();
        data['tglRujukan']=$("#cek_bpjs_tglRujukan").val();
        data['noRujukan']=$("#cek_bpjs_noRujukan").val();
        data['ppkRujukan']=$("#cek_bpjs_ppkRujukan").val();
        data['ppkPelayanan']=$("#cek_bpjs_ppkPelayanan").val();
        data['jnsPelayanan']=$("#cek_bpjs_jnsPelayanan").val();
        data['diagAwal']=$("#cek_bpjs_diagAwal").val();
        data['poliTujuan']=$("#cek_bpjs_poliTujuan").val();
        data['klsRawat']=$("#cek_bpjs_klsRawat").val();
        data['lakaLantas']=$("#cek_bpjs_lakaLantas").val();
        data['lokasiLaka']=$("#cek_bpjs_lokasiLaka").val();
        data['catatan']=$("#cek_bpjs_catatan").val();
        data['user']=$("#cek_bpjs_user").val();
        data['noMr']=$("#cek_bpjs_noMr").val();
        data['command']="reg_sep";
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_noSEP").val(json.noSEP);
              $("#cek_bpjs_noREG").val(json.noreg);
              dismissLoading();
        });
    };
    
    $("#cek_bpjs_noRujukan").keyup(function(e){
        console.log("DUDE");
        if (e.keyCode==13) {
            cek_bpjs.cek_rujukan();
        }
    });
    
    $("#cek_bpjs_lakaLantas").on("change",function(e){
        if( $("#cek_bpjs_lakaLantas").val()=="1"){
            $("#cek_bpjs_lokasiLaka").prop("disabled",false);
        }else{
            $("#cek_bpjs_lokasiLaka").prop("disabled",true);
            $("#cek_bpjs_lokasiLaka").val("");
        }
    });
    
    $("#cek_bpjs_jnsPelayanan").on("change",function(e){
        if( $(this).val()=="2"){
            /*rawat jalan*/
            $("#cek_bpjs_klsRawat").val("3");
            $("#cek_bpjs_klsRawat").attr('disabled','disabled');
            $("#cek_bpjs_poliTujuan").removeAttr('disabled');
        }else{
            /*rawat inap*/
           $("#cek_bpjs_klsRawat").val("");
           $("#cek_bpjs_klsRawat").removeAttr('disabled');
           $("#cek_bpjs_poliTujuan").val("");
           $("#cek_bpjs_poliTujuan").attr('disabled','disabled');
        }
    });
    
    
    cek_bpjs_icd=new TableAction("cek_bpjs_icd","registration","cek_bpjs",new Array());
    cek_bpjs_icd.setSuperCommand("cek_bpjs_icd");
    cek_bpjs_icd.selected=function(json){
        var kode=json.icd;		
        $("#cek_bpjs_diagAwal").val(kode);
    };
    
    
    
});