/* all action here
 * is reply by file in 
 * registration / modul/reg_layanan.php
 * this one used for register the exist patient to layanan
 * 
 * @since 14 Mei 2014
 * @author goblooge@gmail.com
 * @license LGPLv3
 * @version 15.05
 * */

var daftar_pasien;
var column_daftar_patient=new Array(
        'id','tanggal','nobpjs','nrm','jenislayanan','carabayar','caradatang',
		'rujukan','nama_rujukan','id_rujukan','nama_perusahaan','asuransi',
		'keteranganbonus','besarbonus','ambilbonus','namapenanggungjawab',"no_sep_rj",
		'telponpenanggungjawab','karcis','lunas','barulama',
        "kodebonus","namakodebonus","besarambil","plafon_bpjs","kelas_bpjs",
		"periode_bpjs","ppk_bpjs","status_bpjs","alamat_pj","desa_pj","kecamatan_pj","kabupaten_pj",
		"pekerjaan_pj","umur_pj","hubungan_pj"
        );
daftar_pasien = new TableAction("daftar_pasien","registration","registration_patient",column_daftar_patient);
daftar_pasien.setSuperCommand("daftar_pasien");
daftar_pasien.setEnableAutofocus(true);
daftar_pasien.setNextEnter();

$(document).ready(function(){
	if($("#autoload_pasien_aktif").val()=="1"){
		daftar_pasien.view();
	}
});

function pasien_getContent(res){
	try{
		var json	= $.parseJSON(res);
		var status	= json.status;
		var alert	= json.alert;
		var content	= json.content;
		var warning	= json.warning;
		var splash	= json.splash;		
		if(alert.status=='y'){
			var acolor		= alert.color;
			var acontent	= alert.content;
			var atitle		= alert.title;
			smis_alert(atitle, acontent, acolor);
		}		
		if(status=='unauthorized'){
				dismissLoading();
				bootbox.confirm("Your Are Not Authorize to Access This Page , Reload Now ?", function(result) {
				   if(result){
					   location.reload();
				   }
				}); 
		}else if(warning.show=='y'){
			dismissLoading();
			$("#pendaftaran_pasien_add_form").smodal('hide');
			showWarning(warning.title, warning.content);
		}		
		if(status=='ok'){
			return content;
		}else if(status=='no-install' || status=="logout" ){
			location.reload();
		}else{
			return null;
		}
	}catch(e){
		total_loading = 1;
		stop		  = false;
		dismissLoading();
		var problem	  = " <h5> Error Komunikasi dengan Server, Reload Sytem ? </h5>";
		problem		 += " <label class='label label-warning'> Javascript  </label> ";
		problem		 += "<pre>"+e.stack+"</pre>";
		problem		 += "</br>";
		problem		 += "</br>";
		problem		 += " <label class='label label-info'> Server  </label> ";
		problem		 += "<pre>"+res+"</pre>";
		bootbox.confirm(problem, function(result) {
			if(result){
				location.reload();
			}
		}); 
	}
}

daftar_pasien.edit	= function (id){
	var self		= this;
	showLoading();	
	var edit_data	= this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json	= getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
				continue;
			}	
			var name	= self.column[i];
			var the_id	= "#"+self.prefix+"_"+name;
			smis_edit(the_id,json[""+name]);
		}
		dismissLoading();
		self.disabledOnEdit(self.column_disabled_on_edit);
		self.show_form();
		self.afterSelect(json);
        /*bind kode dokter utama untuk kmu*/
        if($("#daftar_pasien_kode_du").attr("type")=="select"){
            registration_patient.getDokterUtama();
        }	
	});
};


daftar_pasien.afterSelect=function(json){
	$("#daftar_pasien_caradatang").trigger("change");
};


daftar_pasien.show_add_form_nosep=function(nrm,nosep,nobpjs,kelas){
    this.clear();
    var self=this;
    $("#"+this.prefix+"_nrm").val(nrm);
	$("#"+this.prefix+"_no_sep_rj").val(nosep);
    $("#"+this.prefix+"_kelas_bpjs").val(kelas);
    $("#"+this.prefix+"_carabayar").val("bpjs");
    $("#"+self.prefix+"_nobpjs").val(nobpjs);
    $("#"+self.prefix+"_nama_perusahaan").val("");
    $("#"+self.prefix+"_asuransi").val("");    
    $("#"+self.prefix+"_asuransi").attr('disabled','disabled');   
    $("#"+self.prefix+"_nama_perusahaan").attr('disabled','disabled');   
    this.show_form();    
}

/**
 * editing sebuah registrasi pasien yang secara otomatis
 * nosep untuk rawat jalan langsung di binding dengan data yang 
 * sudah nosep yang baru.
 * */
daftar_pasien.edit_nosep=function (id,nosep,nobpjs,kelas){
	var self			= this;
	showLoading();
	var edit_data		= this.getEditData(id);
	$.post('',edit_data,function(res){		
		var json		= getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
				continue;
			}				
			var name	= self.column[i];
			var the_id	= "#"+self.prefix+"_"+name;
			smis_edit(the_id,json[""+name]);
		}
        $("#"+self.prefix+"_no_sep_rj").val(nosep);
		$("#"+self.prefix+"_nobpjs").val(nobpjs);
		$("#"+self.prefix+"_kelas_bpjs").val(kelas);    
		dismissLoading();
		self.disabledOnEdit(self.column_disabled_on_edit);
		self.show_form();
		/*bind kode dokter utama untuk kmu*/
        if($("#daftar_pasien_kode_du").attr("type")=="select"){
            registration_patient.getDokterUtama();
        }
	});
};

daftar_pasien.save=function (){
	if(!this.cekSave()){
		return;
	}
	var self			= this;
	var a				= this.getSaveData();	
	a['kode_du']		= $("#daftar_pasien_kode_du").val();
	a['suggest_du']		= $("#daftar_pasien_suggest_du").val();
	a['keterangan_du']	= $("#daftar_pasien_keterangan_du").val();
	a['editable_du']	= $("#daftar_pasien_editable_du").val();
    a['nama_asuransi']	= $("#daftar_pasien_asuransi option:selected").text();
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json		= pasien_getContent(res);		
		if(json==null || json=="") {
			$("#"+self.prefix+"_add_form").smodal('hide');
			self.view();
			self.clear();
			dismissLoading();
			return;
		}
		if(json=="kabur"){
			return;
		}
		if(json!="" && typeof json==='string'){
			dismissLoading();
			var cur		= $("").smodal("current");
				cur		= cur.replace("#","");
			var id_alt	= "#modal_alert_"+self.prefix+"_add_form";
			var warn	= '<div class="alert alert-block alert-info "><h4>Peringatan</h4>'+json+'</div>';
			$(id_alt).html(warn);
			$(id_alt).focus();
			return;
		}else{
			
			self.view();
			self.clear();
			
			if($("#autoprint_registration").val()=="1"){
				if($("#reg_print_data_social_jsetup").val()=="1"){
					daftar_pasien.jsetup_sosial(json.id); 	
				}else{
					daftar_pasien.printelement(json.id); 	
				}	
			}
			setTimeout(function(){
				if(a['id']!="" && a['id']!="0" ){
					daftar_pasien.sinkronisasi_silent(a['id']);
				}
			},1000);
			dismissLoading();
			dismissLoading();
			
		}
	}});
};

/*PRINTING*/
daftar_pasien.jsetup_sosial=function(id){
	var data		= this.getRegulerData();
	data['command']	= 'print-element';
	data['slug']	= 'print-element';
	data['id']		= id;
	showLoading();
	$.post("",data,function(res){
		var json	= getContent(res);
		dismissLoading();
		if(json==null){
			return;
		}		
		try{
			$("#printing_area").html(json);
		}catch(err){
			smis_print(json);
		}
	});
};

daftar_pasien.cetak_antrian = function(noreg){
	var data			= this.getRegulerData();
	data['super_command']	= 'cetak_antrian';
	data['id']		= noreg;
	showLoading();
	$.post("",data,function(res){
		var json		= getContent(res);
		dismissLoading();
		if(json==null){
			return;
		}		
		try{
			$("#printing_area").html(json);
		}catch(err){
			smis_print(json);
		}
	});
};


/**melakukan sinkron data 
 * sehingga carabayar pasien per saat sinkron sesuai dengan pendaftaran
 * @param int id
 */
daftar_pasien.sinkronisasi = function(id){
	var data		= this.getRegulerData();
	data['command']	= "sinkronisasi";
    data['id']		= id;
	showLoading();
	$.post("",data,function(res){
    	var json	= getContent(res);
		showWarning("Peringatan !!!",json);
    	dismissLoading();
    });
};

daftar_pasien.sinkronisasi_silent = function(id){
	var data		= this.getRegulerData();
	data['command']	= "sinkronisasi";
    data['id']		= id;
	//showLoading();
	$.post("",data,function(res){
    	var json	= getContent(res);
		smis_alert("Sinkronisasi Selesai","Proses Sinkronisasi Berhasil Di Lakukan","alert-info");
    	//dismissLoading();
    });
};

daftar_pasien.batal_pasien=function(id){
	var data		= this.getRegulerData();
	data['action']	= "batal_pasien";
    data['id']		= id;
	showLoading();
	$.post("",data,function(res){
		getContent(res);
		daftar_pasien.view();
		dismissLoading();
    });
};

daftar_pasien.show_location=function(id){
	var data		= this.getRegulerData();
	data['action']	= "show_location";
    data['id']		= id;
	showLoading();
	$.post("",data,function(res){
		getContent(res);
		dismissLoading();
    });
};

daftar_pasien.kembalikan = function (entity,noreg_pasien){
	var a 				= this.getRegulerData();
	a ['entity'] 		= entity;
	a ['noreg_pasien'] 	= noreg_pasien;
	a ['action'] 		= "reactived_antrian";
	var self			= this;
	showLoading();
	$.post("",a,function(res){
		var json = getContent(res);
		daftar_pasien.show_location(noreg_pasien);
		dismissLoading();
	});
};

daftar_pasien.addViewData	= function(data){
	data['dari']			= $("#"+this.prefix+"_dari").val();
	data['sampai']			= $("#"+this.prefix+"_sampai").val();
	return data;
};


/* this function will be catch by file
 * to -> registration/modul/reg_layanan.php and
 * will redirect to -> registration/resource/php/registration_patient/reg_layanan.php
 * and would be redirect to file -> registration/class/DaftarResponder.php 
 * 
 * @id : id of noreg_pasien from database (smis_rg_layananpasien.id)
 * */
daftar_pasien.dogtag	= function (id){
	var self			= this;
	var a				= this.getRegulerData();
	a['command']		= "dogtag";
	a['id']				= id;
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json		= getContent(res);
		smis_print_barcode(json,"#sbarcode");
		dismissLoading();
	}});
};


/* this function will be catch by file
 * to -> registration/modul/reg_layanan.php and
 * will redirect to -> registration/resource/php/registration_patient/reg_layanan.php
 * and would be redirect to file -> registration/class/DaftarResponder.php 
 * the finally would catch by  
 * registration/modul/label.php 
 * 
 * @id : id of noreg_pasien from database (smis_rg_layananpasien.id)
 * */
daftar_pasien.label		= function (id){
	var self			= this;
	var a				= this.getRegulerData();
	a['command']		= "label";
	a['id']				= id;
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json		= getContent(res);
		try{
			$("#printing_area").html(json);			
		}catch(err){
			smis_print(json);
		}
		dismissLoading();
	}});
};



/* this function will be catch by file
 * to -> registration/modul/reg_layanan.php and
 * will redirect to -> registration/resource/php/registration_patient/reg_layanan.php
 * and would be redirect to file -> registration/class/DaftarResponder.php 
 * the finally would catch by  
 * registration/modul/label_inap.php 
 * 
 * @id : id of noreg_pasien from database (smis_rg_layananpasien.id)
 * */
daftar_pasien.label_inap	= function (id){
	var self				= this;
	var a					= this.getRegulerData();
	a['command']			= "label_inap";
	a['id']					= id;
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json			= getContent(res);
		try{
			$("#printing_area").html(json);			
		}catch(err){
			smis_print(json);
		}
		dismissLoading();
	}});
};

/* this function would be catch by file 
 * registration/modul/reg_layanan.php
 * where would be redirect to file
 * registration/class/DarftarResponder.php 
 * in function dogtag().
 * there mus be setting in the system that would 
 * be used for 'jsPrintSetup'
 * here the simple example of 'jsPrintSetup'
 * that must be put in setting
 * 
 * var width=Number($("#jsprint_dogtag_width").val());
 * var height=Number($("#jsprint_dogtag_height").val());
 * var mright=Number($("#jsprint_dogtag_mright").val());
 * var mleft=Number($("#jsprint_dogtag_mleft").val());
 * var mtop=Number($("#jsprint_dogtag_mtop").val());
 * var mbottom=Number($("#jsprint_dogtag_mbottom").val());
 * jsPrintSetup.setOption('orientation', jsPrintSetup.kPotraitOrientation);
 * jsPrintSetup.definePaperSize(101, 101, "DOGTAG_SMIS_PRINT", "CUSTOM DOGTAG", "DOGTAG PAPER", width, height,jsPrintSetup.kPaperSizeMillimeters);
 * jsPrintSetup.setPaperSizeData(101);
 * jsPrintSetup.setOption('headerStrLeft', '');
 * jsPrintSetup.setOption('headerStrCenter', '');
 * jsPrintSetup.setOption('headerStrRight', '');
 * jsPrintSetup.setOption('footerStrLeft', '');
 * jsPrintSetup.setOption('footerStrCenter', '');
 * jsPrintSetup.setOption('footerStrRight', '');
 * jsPrintSetup.setOption('marginTop', mtop);
 * jsPrintSetup.setOption('marginBottom', mbottom);
 * jsPrintSetup.setOption('marginLeft', mleft);
 * jsPrintSetup.setOption('marginRight', mright);
 * $("#sbarcode").sbarcode();
 * $("#printing_area").html(json);
 * bootbox_choose_print();
 * */
daftar_pasien.dogtag_jsprint = function (id){
	var self		= this;
	var a			= this.getRegulerData();
	a['command']	= "dogtag";
	a['id']			= id;
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json	= getContent(res);
		try{
			$("#printing_area").html(json);
		}catch(err){
			smis_print(json);
		}
		dismissLoading();
	}});
};

daftar_pasien.b_wbpatient_print = function(id) {
	var self 		= this;
	var data 		= this.getRegulerData();
	data['command'] = "b_wbpatient_print";
	data['id'] 		= id;
	showLoading();
	$.post("",data,function(response) {
			var json = getContent(response);
			try{
				$("#printing_area").html(json);
			}catch(err){
				smis_print(json);
			}
			dismissLoading();
		}
	);
};



daftar_pasien.a_wbpatient_print = function(id) {
	var self 		= this;
	var data 		= this.getRegulerData();
	data['command'] = "a_wbpatient_print";
	data['id'] 		= id;
	showLoading();
	$.post("",data,function(response) {
			var json = getContent(response);
			try{
				$("#printing_area").html(json);
			}catch(err){
				smis_print(json);
			}
			dismissLoading();
		}
	);
};

daftar_pasien.karcis	= function(){
	var data				= this.getRegulerData();
	data['action']			= "karcis";
    data['super_command']	= "karcis";
    data['polislug']		= $("#"+this.prefix+"_jenislayanan").val();
    data['barulama']		= $("#"+this.prefix+"_barulama").val();
    data['urji']			= "urj";
    if(data['polislug']=="") return;
    var self=this;
    showLoading();
    $.post("",data,function(res){
		var json	= getContent(res);
		console.log(json);
        setMoney("#"+self.prefix+"_karcis",json.administrasi);
        dismissLoading();
    });
};

daftar_pasien.reactivated	= function(id){
	var data				= this.getRegulerData();
	data['action']			= "reaktivasi";
    data['super_command']	= "reaktivasi";
    data['id']				= id;
    var self				= this;
    showLoading();
    $.post("",data,function(res){
        var json	= getContent(res);
        $("#reaktivasi").html(json);
        dismissLoading();
    });
};

daftar_pasien.more_option = function(di_tag){
    var data			= this.getRegulerData();
    data['action']		= "offline_bpjs_cek";
    data['no_bpjs']		= $("#"+di_tag).val();
    var self			= this;
    showLoading();
    $.post("",data,function(res){
        var json		= getContent(res);
		$("#"+self.prefix+"_periode_bpjs").val(json.periode);
		$("#"+self.prefix+"_ppk_bpjs").val(json.nama_ppk);
        $("#"+self.prefix+"_status_bpjs").val(json.status);
        dismissLoading();
    });
};

daftar_pasien.fix_asuransi = function(id_asuransi,carabayar){
	var self				= this;
	var a				= this.getRegulerData();
	a['action']			= "fix_asuransi";
	a['id_asuransi']			= id_asuransi;
	a['carabayar']			= carabayar;
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		var json			= getContent(res);
		$("#daftar_pasien_asuransi").html(json);
		dismissLoading();
	}});
};