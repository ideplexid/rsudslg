var laporan_bonus;
$(document).ready(function(){
        var column=new Array('provinsi','kabupaten','kecamatan','kelurahan',"baru","caradatang","rujukan","carabayar","uri","diagram");
		laporan_bonus=new ReportAction("laporan_bonus","registration","laporan_bonus",column);
		laporan_bonus.setDiagramHolder("laporan_bonus_mychart");
		laporan_bonus.setXKey('Lokasi');
		laporan_bonus.setYKey(['jumlah']);
		laporan_bonus.setLabels(["Jumlah"]);

        laporan_bonus.getViewData=function(){
        	var view_data=this.getRegulerData();
        	view_data['command']="list";
        	view_data['kriteria']=$("#"+this.prefix+"_kriteria").val();
        	view_data['number']=$("#"+this.prefix+"_number").val();
        	view_data['max']=$("#"+this.prefix+"_max").val();
        	view_data['mode']=$("#"+this.prefix+"_mode_model").val();
        	for(var i=0;i<this.column.length;i++){
        		var name=this.column[i];
        		view_data[name]=$("#"+this.prefix+"_"+name).val();
        	}
        	if(view_data["provinsi"]=="" || view_data["provinsi"]==null){
        		view_data["provinsi"]="%";
            }
            if(view_data["kabupaten"]=="" || view_data["kabupaten"]==null){
            	view_data["kabupaten"]="%";
            }
            //alert(view_data["kecamatan"]);
            if(view_data["kecamatan"]=="" || view_data["kecamatan"]==null){
            	view_data["kecamatan"]="%";
            }
            
			if(view_data["kelurahan"]=="" || view_data["kelurahan"]==null){
            	view_data["kelurahan"]="%";
            }
			
        	return view_data;
        };

        laporan_bonus.propinsi=function(id_propinsi){
            var data=this.getRegulerData();
            data['super_command']="kabupaten";
            data['id_provinsi']=id_propinsi;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_bonus_kabupaten").html(json);
                $("#laporan_bonus_kabupaten").select2();
				laporan_bonus.kabupaten('%');
            });
        };

        laporan_bonus.sourcePieAdapter=function(json){
			var so=new Array();
    		$(json).each(function(idx, obj){
    			var a={label:obj.Lokasi,value:obj.jumlah};
    			so.push(a);
    		});
    		return so;
    	};
        
        laporan_bonus.kabupaten=function(id_kabupaten){
            var data=this.getRegulerData();
            data['super_command']="kecamatan";
            data['id_kabupaten']=id_kabupaten;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_bonus_kecamatan").html(json);
                $("#laporan_bonus_kecamatan").select2();
                laporan_bonus.kecamatan('%');
            });
            
        };
        
        laporan_bonus.kecamatan=function(id_kecamatan){
            var data=this.getRegulerData();
            data['super_command']="kelurahan";
            data['id_kecamatan']=id_kecamatan;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_bonus_kelurahan").html(json);
                $("#laporan_bonus_kelurahan").select2();
            });
        };
    
        $("#laporan_bonus_provinsi").select2().on("change",function(e){
            laporan_bonus.propinsi(e.val);
        });
        
         $("#laporan_bonus_kabupaten").select2().on("change",function(e){
            laporan_bonus.kabupaten(e.val);
        });
        
        $("#laporan_bonus_kecamatan").select2().on("change",function(e){
            laporan_bonus.kecamatan(e.val);
        });
		
		$("#laporan_bonus_kelurahan").select2();
       
        
        
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	laporan_bonus.initDiagram("");
	laporan_bonus.setTitle("Pendaftaran Pasien");
	
	$("#laporan_bonus_caradatang").on("change",function(){
			$("#laporan_bonus_rujukan").val("%");
		});
});