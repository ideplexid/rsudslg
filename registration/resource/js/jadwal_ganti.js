var jadwal_ganti;
var dokter_jadwal_ganti;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id',"shift","tanggal","kode","id_dokter","nama_dokter");
	jadwal_ganti=new TableAction("jadwal_ganti","registration","jadwal_ganti",column);
	jadwal_ganti.view();

    dokter_jadwal_ganti=new TableAction("dokter_jadwal_ganti","registration","jadwal_ganti",new Array());
    dokter_jadwal_ganti.setSuperCommand("dokter_jadwal_ganti");
    dokter_jadwal_ganti.selected=function(json){
        $("#jadwal_ganti_id_dokter").val(json.id_dokter_satu);
        $("#jadwal_ganti_nama_dokter").val(json.nama_dokter_satu);
        $("#jadwal_ganti_kode").val(json.kode);
    };
});