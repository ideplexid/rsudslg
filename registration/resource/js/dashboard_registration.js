var dashboard_registration;
$(document).ready(function(){
    $(".mydate").datepicker();
    var pname   = $("#dashreg_protoname").val();
    var pslug   = $("#dashreg_protoslug").val();
    var pimpl   = $("#dashreg_protoimplement").val();
    var page    = $("#dashreg_page").val();
    var act     = $("#dashreg_action").val();
    dashboard_registration = new TableAction(act,page,act,new Array());
    dashboard_registration.setPrototipe(pname,pslug,pimpl);
    dashboard_registration.addViewData=function(data){
        data['dari']    = $("#"+this.prefix+"_dari").val();
        data['sampai']  = $("#"+this.prefix+"_sampai").val();
        data['rentang']  = $("#"+this.prefix+"_rentang").val();
        return data;
    };
    dashboard_registration.afterview = function (json){
        dashboard_registration.donut("jk");
        dashboard_registration.donut("barulama");
        dashboard_registration.bar("kecamatan");
        dashboard_registration.bar("carabayar");
    };

    dashboard_registration.donut = function(id){
        var datax = $.parseJSON($("#dashboard_"+id).html());
        var html  = "<h5>"+datax.title+"</h5><div id='"+datax.sources.element+"' style='width:200px;height:200px;' ></div>";
        $("#dashboard_"+id).html(html);
        Morris.Donut(datax.sources);
    };

    dashboard_registration.bar = function(id){
        var datax = $.parseJSON($("#dashboard_"+id).html());
        var html  = "<h5>"+datax.title+"</h5><div id='"+datax.sources.element+"' style='width:100%;height:200px;' ></div>";
        $("#dashboard_"+id).html(html);
        Morris.Bar(datax.sources);
    };

    $("#"+dashboard_registration.prefix+"_rentang").on("change",function(){
        var sub   = Number($(this).val());
        var today = new Date(); 
        var last  = new Date();
        last.setDate(last.getDate()-sub);
        t_month = today.getMonth();
        if(t_month<10){
            t_month="0"+t_month;
        }

        l_month = last.getMonth();
        if(l_month<10){
            l_month="0"+l_month;
        }
        var sampai=today.getFullYear()+"-"+t_month+"-"+today.getDate();
        var dari = last.getFullYear()+"-"+l_month+"-"+last.getDate();
        $("#"+dashboard_registration.prefix+"_sampai").val(sampai);
        $("#"+dashboard_registration.prefix+"_dari").val(dari);
        console.log(dari+"  "+sampai);
    });
});