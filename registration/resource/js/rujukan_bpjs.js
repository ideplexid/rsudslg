var rujukan_bpjs;
var rujukan_bpjs_icd;
var rujukan_bpjs_ppk;
var rujukan_bpjs_vclaim;
$(document).ready(function(){
    column       = new Array("id","nosep","no_rujukan","jenis","tanggal","ppk","nama_ppk","diagnosa","tipe","poli","catatan","nama_diagnosa","jk","nama_pasien","no_bpjs");
    rujukan_bpjs = new TableAction("rujukan_bpjs","registration","rujukan_bpjs",column);
    rujukan_bpjs.addSaveData=function(data){
        data['nama_poli'] = $("#"+this.prefix+"_poli option:selected").text();
        return data;
    }
    rujukan_bpjs.view();


    rujukan_bpjs_icd=new TableAction("rujukan_bpjs_icd","registration","rujukan_bpjs",new Array());
    rujukan_bpjs_icd.setSuperCommand("rujukan_bpjs_icd");
    rujukan_bpjs_icd.selected=function(json){
        rujukan_bpjs.set("diagnosa",json.icd);
        rujukan_bpjs.set("nama_diagnosa",json.nama);
    };

    rujukan_bpjs_ppk=new TableAction("rujukan_bpjs_ppk","registration","rujukan_bpjs",new Array());
    rujukan_bpjs_ppk.setSuperCommand("rujukan_bpjs_ppk");
    rujukan_bpjs_ppk.selected=function(json){
        rujukan_bpjs.set("ppk",json.kode);
        rujukan_bpjs.set("nama_ppk",json.nama);
    };

    rujukan_bpjs_vclaim=new TableAction("rujukan_bpjs_vclaim","registration","rujukan_bpjs",new Array());
    rujukan_bpjs_vclaim.setSuperCommand("rujukan_bpjs_vclaim");
    rujukan_bpjs_vclaim.selected=function(json){
        rujukan_bpjs.set("jk",json.kelamin);
        rujukan_bpjs.set("nama_pasien",json.nama);
        rujukan_bpjs.set("nosep",json.noSEP);
        rujukan_bpjs.set("no_bpjs",json.noKartu);
        
    };
});