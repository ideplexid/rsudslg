/**
 * this used for viewing the history of the patient 
 * that come to the Hospital
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @used        : registration/resource/php/admin/lap_register.php
 * */
var lap_register;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id',"selesai","uri","carapulang","tanggal_pulang","tanggal","tanggal_inap");
	lap_register=new TableAction("lap_register","registration","lap_register",column);
    lap_register.addViewData=function(a){
        a['nrm_pasien']=$("#lap_register_nrm_pasien").val();
        a['nama_pasien']=$("#lap_register_nama_pasien").val();
        a['noreg_pasien']=$("#lap_register_noreg_pasien").val();
        return a;
    };
	lap_register.view();
    $("#lap_register_nrm_pasien, #lap_register_nama_pasien").keyup(function(e){
        if(e.keyCode==13){
            lap_register.view();
        }
    });
});