var laporan_bonus_perperujuk;
var perujuk;
$(document).ready(function(){
    var column=new Array("baru","bonus");
    
    laporan_bonus_perperujuk=new TableAction("laporan_bonus_perperujuk","registration","laporan_bonus_perperujuk",column);
    laporan_bonus_perperujuk.addRegulerData=function(reg_data){
		reg_data['dari']=$("#laporan_bonus_perperujuk_dari").val();
		reg_data['sampai']=$("#laporan_bonus_perperujuk_sampai").val();
		reg_data['id_perujuk']=$("#laporan_bonus_perperujuk_id_perujuk").val();
		return reg_data;
	};
	laporan_bonus_perperujuk.cair=function(){
		var data=this.getRegulerData();
		data['command']="cair";
		
		bootbox.confirm("Anda Yakin, Proses ini akan membuat semua Yang Belum cair menjadi cari, dan tidak dapat dikembalikan !!",function(r){
			if(r){
				showLoading();
				$.post("",data,function(res){
					var json=getContent(res);
					laporan_bonus_perperujuk.view();
					dismissLoading();
				});
			}
		});
	};
	perujuk=new TableAction("perujuk","registration","laporan_bonus_perperujuk",column);
    perujuk.setSuperCommand("perujuk");
    perujuk.selected=function(json){
		$("#laporan_bonus_perperujuk_perujuk").val(json.nama);
		$("#laporan_bonus_perperujuk_id_perujuk").val(json.id);
		$("#laporan_bonus_perperujuk_alamat").val(json.alamat);
		$("#laporan_bonus_perperujuk_instansi").val(json.instansi);
    };
    $('.mydate').datepicker();
});