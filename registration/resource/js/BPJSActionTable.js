function BPJSActionTable(name,page,action,column){
	TableAction.call( this, name,page,action,column);
	this.preload();
}
BPJSActionTable.prototype.constructor = BPJSActionTable;
BPJSActionTable.prototype = Object.create( TableAction.prototype );

BPJSActionTable.prototype.preload=function(){
    $("#"+this.action+"_lakaLantas").on("change",function(e){
        if( $("#"+this.action+"_lakaLantas").val()=="1"){
            $("#"+this.action+"_lokasiLaka").prop("disabled",false);
        }else{
            $("#"+this.action+"_lokasiLaka").prop("disabled",true);
            $("#"+this.action+"_lokasiLaka").val("");
        }
    });    
    $("#"+this.action+"_jnsPelayanan").on("change",function(e){
        if( $("#"+this.action+"_jnsPelayanan").val()=="0"){
            $("#"+this.action+"_poliTujuan").val("IGD");
        }else{
            $("#"+this.action+"_poliTujuan").val("");
        }
    });
};

BPJSActionTable.prototype.sep_detail=function(id){
    var data=this.getRegulerData();
    data['id']=id;
    data['command']="cek_sep";
    showLoading();
    var self=this;
    $.post("",data,function(res){
        var json=getContent(res);
        self.view();
        dismissLoading();
    });
};

BPJSActionTable.prototype.sep_print=function(id){
    var data=this.getRegulerData();
    data['id']=id;
    data['command']="print_sep";
    showLoading();
    $.post("",data,function(res){
        var json=getContent(res);
        smis_print(json);
        dismissLoading();
    });
};

BPJSActionTable.prototype.sep_remove=function(id){
    var data=this.getRegulerData();
    data['id']=id;
    data['command']="remove_sep";
    showLoading();
    var self=this;
    $.post("",data,function(res){
        var json=getContent(res);
        self.view();
        dismissLoading();
    });
};

BPJSActionTable.prototype.sep_pulang=function(){
    var data=this.getRegulerData();
    data['id']=$("#"+this.action+"_id_pulang").val();
    data['tgl_pulang']=$("#"+this.action+"_tanggal_pulang").val();
    data['command']="pulang_sep";
    showLoading();
    var self = this;
    $.post("",data,function(res){
        var json=getContent(res);
        if(json==null) {
            $("#update_tgl_pulang_"+self.action).smodal('hide');
            return;		
        }else if(json.success=="1"){
            $("#update_tgl_pulang_"+self.action).smodal("hide");
            self.view();
        }
        dismissLoading();   
    });
};

BPJSActionTable.prototype.show_sep_pulang=function(id){
    $("#update_tgl_pulang_"+this.action).smodal("show");
    $("#"+this.action+"_id_pulang").val(id);
};