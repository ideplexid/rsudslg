var cek_bpjs_inap;
var cek_bpjs_inap_icd;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});    
    $("#cek_bpjs_inap_no_bpjs, #cek_bpjs_inap_no_ktp ").keyup(function(e){ 
        var code = e.which;
        if(code==13)e.preventDefault();
        if(code==32||code==13||code==188||code==186){
            cek_bpjs_inap.fetch_server();
        }
    });
    
    cek_bpjs_inap =new TableAction("cek_bpjs_inap","registration","cek_bpjs_inap");
    cek_bpjs_inap.fetch_server=function(){
        $("#cek_bpjs_inap_noSEP").val("");
        data=this.getRegulerData();
        data['no_bpjs']=$("#cek_bpjs_inap_no_bpjs").val();
        data['no_ktp']=$("#cek_bpjs_inap_no_ktp").val();
        data['command']="cek_bpjs";
        
        if(data['no_bpjs']=="" && (data['no_ktp']=="" || data['no_ktp']=="-") ){
            showWarning("Terjadi Kesalahan !!!","Masukan No. BPJS atau No. KTP, jika No. BPJS terisi No. KTP tidak dihiraukan");
        }
        
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_inap_page").html(json.table);
              cek_bpjs_inap.reset_part();
              if(json.raw != null && json.raw.status=="AKTIF"){
                  $("#cek_bpjs_inap_noKartu").val(json.raw.nobpjs);
                  $("#cek_bpjs_inap_no_ktp").val(json.raw.nik);
                  $("#cek_bpjs_inap_no_bpjs").val(json.raw.nobpjs);
                  $("#cek_bpjs_inap_ppkRujukan").val(json.raw.kd_provider);
              }
              dismissLoading();
        });
    };
    
    cek_bpjs_inap.reset=function(){
        $("#cek_bpjs_inap_noSEP").val("");
        $("#cek_bpjs_inap_no_ktp").val(""); 
        $("#cek_bpjs_inap_noMr").val("");
        cek_bpjs_inap.reset_part();
    };
    
    cek_bpjs_inap.reset_part=function(){
        var currentdate = new Date(); 
        var month=(currentdate.getMonth()+1);
        if(month<10){
                month="0"+month;
        }
        
        var dt=currentdate.getDate();
        if(dt<10){
                dt="0"+dt;
        }
        
        var hour=currentdate.getHours();
        if(hour<10){
                hour="0"+hour;
        }
        
        var minute=currentdate.getMinutes();
        if(minute<10){
                minute="0"+minute;
        }
        
        var second=currentdate.getSeconds();
        if(second<10){
                second="0"+second;
        }
        
        var datetime = currentdate.getFullYear() + "-"
                        + month + "-" 
                        + dt + " "  
                        + hour + ":"  
                        + minute + ":" 
                        + second;
         
        $("#cek_bpjs_inap_noKartu").val("");
        $("#cek_bpjs_inap_no_bpjs").val("");
        $("#cek_bpjs_inap_tglSep").val(datetime);
        $("#cek_bpjs_inap_tglRujukan").val(datetime);
        $("#cek_bpjs_inap_noRujukan").val("");
        $("#cek_bpjs_inap_ppkRujukan").val("");
        $("#cek_bpjs_inap_jnsPelayanan").val("");
        $("#cek_bpjs_inap_diagAwal").val("");
        $("#cek_bpjs_inap_poliTujuan").val("");
        $("#cek_bpjs_inap_klsRawat").val("");
        $("#cek_bpjs_inap_lakaLantas").val("");
        $("#cek_bpjs_inap_lokasiLaka").val("");
        $("#cek_bpjs_inap_catatan").val("");
        $("#cek_bpjs_inap_user").val("");
        $("#cek_bpjs_inap_noREG").val("");
    };
    
    cek_bpjs_inap.cek_rujukan=function(){
        $("#cek_bpjs_inap_noSEP").val("");
        
        data=this.getRegulerData();
        data['noRujukan']=$("#cek_bpjs_inap_noRujukan").val();
        data['noBPJS']=$("#cek_bpjs_inap_no_bpjs").val();
        data['command']="cek_rujukan";
        
        if(data['noBPJS']=="" && data['noRujukan']=="" ){
            showWarning("Terjadi Kesalahan !!!","Silakan Isikan Nomor Rujukanya atau Nomor BPJS Terlebih Dahulu, kemudian tekan enter atau Klik Tombol Pencarian Rujukan");
        }
        
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_inap_noRujukan").val(data['noRujukan']);
              if(json.message=="Data Ditemukan"){
                  cek_bpjs_inap.reset_part();
                  $("#cek_bpjs_inap_no_bpjs").val(json.nobpjs);
                  $("#cek_bpjs_inap_noKartu").val(json.nobpjs);                  
                  $("#cek_bpjs_inap_tglRujukan").val(json.tgl_kunjungan);
                  $("#cek_bpjs_inap_ppkRujukan").val(json.kd_provider);
                  $("#cek_bpjs_inap_diagAwal").val(json.kode_diagnosa);
                  $("#cek_bpjs_inap_poliTujuan").val(json.kd_poli);
                  $("#cek_bpjs_inap_catatan").val(json.catatan);                  
                  $("#cek_bpjs_inap_noRujukan").val(json.no_kunjungan);
                  $("#cek_bpjs_inap_catatan").val(json.catatan);
                  $("#cek_bpjs_inap_no_ktp").val(json.nik);                  
                  if(json.kd_poli!=null && json.kd_poli!=""){
                    $("#cek_bpjs_inap_jnsPelayanan").val(2);
                  }else{
                    $("#cek_bpjs_inap_jnsPelayanan").val(1);
                  }
                  $("#cek_bpjs_inap_jnsPelayanan").trigger("change");    
              }else{
                  showWarning("Terjadi Kesalahan !!!",json.message);
              }
              dismissLoading();
        });
    };
    
    cek_bpjs_inap.print_sep=function(){
        var nosep=$("#cek_bpjs_inap_noSEP").val();
        if(nosep==""){
            showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
            return;
        }
        data=this.getRegulerData();
        data['nosep']=nosep;
        data['action']="print_sep";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            smis_print(json);
            dismissLoading();
        });
    };
     
    cek_bpjs_inap.register_pasien_bpjs=function(){
        var nosep=$("#"+this.prefix+"_noSEP").val();
        var noreg=$("#"+this.prefix+"_noREG").val();
        var nrm=$("#"+this.prefix+"_noMr").val();
        var nobpjs=$("#"+this.prefix+"_noKartu").val();
        
        if(nosep==""){
            showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
            return;
        }
        activeTab("#register_menginap");
        register_menginap.edit_nosep(noreg,nosep,nobpjs);  
    };
    
    cek_bpjs_inap.cek_nomor_rm=function(nrm,ktp){
        cek_bpjs_inap.reset();
        $("#cek_bpjs_inap_noMr").val(nrm);
        $("#cek_bpjs_inap_no_ktp").val(ktp);
        $("#cek_bpjs_inap_no_bpjs").val("");
        if(ktp=="" || ktp=="-"){
            showWarning("KTP/NIK Kosong","dikarenakan KTP/NIK kosong, silakan isikan nomor BPJS pasien yang bersangkutan");
        }else{
            $("#cek_bpjs_inap_button").trigger("click");
        }
    };
    
    cek_bpjs_inap.fix_register=function(){
        data=this.getRegulerData();
        data['noSEP']=$("#cek_bpjs_inap_noSEP").val();
        data['noMr']=$("#cek_bpjs_inap_noMr").val();
        
        if(data['noSEP']=="" || data['noMr']==""){
            showWarning("Peringatan !!!","Silakan Bikin BPJS SEP terlebih dahulu, Jika No RM kosong, silakan buka Menu Registrasi Pasien, Pilih pasienya, kemudian Tekan Opsi SEP. Pastikan No. KTP/NIK terisi untuk kemudahan");
            return;
        }
        
        data['command']="fix_register";
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_inap_noREG").val(json.noreg);
              dismissLoading();
        });
    };
    
    cek_bpjs_inap.reg_sep=function(){
        $("#cek_bpjs_inap_noSEP").val("");
        data=this.getRegulerData();
        data['noKartu']=$("#cek_bpjs_inap_noKartu").val();
        data['tglSep']=$("#cek_bpjs_inap_tglSep").val();
        data['tglRujukan']=$("#cek_bpjs_inap_tglRujukan").val();
        data['noRujukan']=$("#cek_bpjs_inap_noRujukan").val();
        data['ppkRujukan']=$("#cek_bpjs_inap_ppkRujukan").val();
        data['ppkPelayanan']=$("#cek_bpjs_inap_ppkPelayanan").val();
        data['jnsPelayanan']=$("#cek_bpjs_inap_jnsPelayanan").val();
        data['diagAwal']=$("#cek_bpjs_inap_diagAwal").val();
        data['poliTujuan']=$("#cek_bpjs_inap_poliTujuan").val();
        data['klsRawat']=$("#cek_bpjs_inap_klsRawat").val();
        data['lakaLantas']=$("#cek_bpjs_inap_lakaLantas").val();
        data['lokasiLaka']=$("#cek_bpjs_inap_lokasiLaka").val();
        data['catatan']=$("#cek_bpjs_inap_catatan").val();
        data['user']=$("#cek_bpjs_inap_user").val();
        data['noMr']=$("#cek_bpjs_inap_noMr").val();
        data['command']="reg_sep";
        showLoading();
        $.post("",data,function(res){
              var json=getContent(res);
              $("#cek_bpjs_inap_noSEP").val(json.noSEP);
              $("#cek_bpjs_inap_noREG").val(json.noreg);
              dismissLoading();
        });
    };
    
    $("#cek_bpjs_inap_noRujukan").keyup(function(e){
        if (e.keyCode==13) {
            cek_bpjs_inap.cek_rujukan();
        }
    });
    
    $("#cek_bpjs_inap_lakaLantas").on("change",function(e){
        if( $("#cek_bpjs_inap_lakaLantas").val()=="1"){
            $("#cek_bpjs_inap_lokasiLaka").prop("disabled",false);
        }else{
            $("#cek_bpjs_inap_lokasiLaka").prop("disabled",true);
            $("#cek_bpjs_inap_lokasiLaka").val("");
        }
    });
    
    $("#cek_bpjs_inap_jnsPelayanan").on("change",function(e){
        if( $(this).val()=="2"){
            /*rawat jalan*/
            $("#cek_bpjs_inap_klsRawat").val("3");
            $("#cek_bpjs_inap_klsRawat").attr('disabled','disabled');
            $("#cek_bpjs_inap_poliTujuan").removeAttr('disabled');
        }else{
            /*rawat inap*/
           $("#cek_bpjs_inap_klsRawat").val("");
           $("#cek_bpjs_inap_klsRawat").removeAttr('disabled');
           $("#cek_bpjs_inap_poliTujuan").val("");
           $("#cek_bpjs_inap_poliTujuan").attr('disabled','disabled');
        }
    });
    
    cek_bpjs_inap_icd=new TableAction("cek_bpjs_inap_icd","registration","cek_bpjs_inap",new Array());
    cek_bpjs_inap_icd.setSuperCommand("cek_bpjs_inap_icd");
    cek_bpjs_inap_icd.selected=function(json){
        var kode=json.icd;		
        $("#cek_bpjs_inap_diagAwal").val(kode);
    };
    
    
});