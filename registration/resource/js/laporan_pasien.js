var laporan_pasien;
$(document).ready(function(){
        var column=new Array('provinsi','kabupaten','kecamatan','kelurahan','status','pendidikan','pekerjaan','agama','suku','bahasa','diagram','kelamin');
		laporan_pasien=new ReportAction("laporan_pasien","registration","laporan_pasien",column);
		laporan_pasien.setDiagramHolder("laporan_pasien_mychart");
		laporan_pasien.setXKey('Lokasi');
		laporan_pasien.setYKey(['jumlah']);
		laporan_pasien.setLabels(["Jumlah"]);
		

        laporan_pasien.getViewData=function(){
        	var view_data=this.getRegulerData();
        	view_data['command']="list";
        	view_data['kriteria']=$("#"+this.prefix+"_kriteria").val();
        	view_data['number']=$("#"+this.prefix+"_number").val();
        	view_data['max']=$("#"+this.prefix+"_max").val();
        	view_data['mode']=$("#"+this.prefix+"_mode_model").val();
           
        	for(var i=0;i<this.column.length;i++){
        		var name=this.column[i];
        		view_data[name]=$("#"+this.prefix+"_"+name).val();
        	}
        	if(view_data["provinsi"]=="" || view_data["provinsi"]==null){
        		view_data["provinsi"]="%";
            }
            if(view_data["kabupaten"]=="" || view_data["kabupaten"]==null){
            	view_data["kabupaten"]="%";
            }
            
            if(view_data["kecamatan"]=="" || view_data["kecamatan"]==null){
            	view_data["kecamatan"]="%";
            }
			
			if(view_data["kelurahan"]=="" || view_data["kelurahan"]==null){
            	view_data["kelurahan"]="%";
            }
            
        	return view_data;
        };

        laporan_pasien.propinsi=function(id_propinsi){
            var data=this.getRegulerData();
            data['super_command']="kabupaten";
            data['id_provinsi']=id_propinsi;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_pasien_kabupaten").html(json);
                $("#laporan_pasien_kabupaten").select2();
				 laporan_pasien.kabupaten('%');
            });
        };

        laporan_pasien.sourcePieAdapter=function(json){
        	var so=new Array();
    		$(json).each(function(idx, obj){
    			var a={label:obj.Lokasi,value:obj.jumlah};
    			so.push(a);
    		});
    		return so;
    	};
        
        laporan_pasien.kabupaten=function(id_kabupaten){
            var data=this.getRegulerData();
            data['super_command']="kecamatan";
            data['id_kabupaten']=id_kabupaten;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_pasien_kecamatan").html(json);
                $("#laporan_pasien_kecamatan").select2();
                 laporan_pasien.kecamatan('%');
            });
            
        };
        
        laporan_pasien.kecamatan=function(id_kecamatan){
            var data=this.getRegulerData();
            data['super_command']="kelurahan";
            data['id_kecamatan']=id_kecamatan;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_pasien_kelurahan").html(json);
                $("#laporan_pasien_kelurahan").select2();
               
            });
        };
    
        $("#laporan_pasien_provinsi").select2().on("change",function(e){
            laporan_pasien.propinsi(e.val);
        });
        
         $("#laporan_pasien_kabupaten").select2().on("change",function(e){
            laporan_pasien.kabupaten(e.val);
        });
        
        $("#laporan_pasien_kecamatan").select2().on("change",function(e){
            laporan_pasien.kecamatan(e.val);
        });
		
		 $("#laporan_pasien_kelurahan").select2();
       
        
        
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	laporan_pasien.initDiagram("");
	laporan_pasien.setTitle("Pendataan Pasien");
	
});