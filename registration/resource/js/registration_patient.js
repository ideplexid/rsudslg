var registration_patient;
var is_update;

var column_registration_patient = new Array('id','tanggal','ktp','suku',"bahasa",'sebutan',
										'nama','alamat','tempat_lahir','tgl_lahir','kelamin',
										'rt','rw','provinsi','kabupaten','kecamatan','kelurahan',
										'kedusunan','pekerjaan','pendidikan',"status",'telpon','agama',
										'umur','suami','istri','ayah','ibu','bbm','email','jenis','kartu',
										'document','keterangan',"nobpjs","profile_number",
										"alamat_keluarga","desa_keluarga","kabupaten_keluarga","kecamatan_keluarga","pekerjaan_keluarga",
										"umur_keluarga","telepon_keluarga");
registration_patient = new TableAction("registration_patient","registration","registration_patient",column_registration_patient);
registration_patient.setEnableAutofocus(true);
registration_patient.setNextEnter();
registration_patient.enabledOnNotEdit(new Array("id"));
registration_patient.getViewData 	= function(){
	var view_data                   = this.getRegulerData();
	view_data['command']            = "list";
	view_data['kriteria']           = $("#"+this.prefix+"_kriteria").val();
	view_data['number']             = $("#"+this.prefix+"_number").val();
	view_data['max']                = $("#"+this.prefix+"_max").val();
	view_data['search_id']          = $("#search_nrm").val();
	view_data['search_nama']        = $("#search_nama").val();
	view_data['search_ktp']         = $("#search_ktp").val();
	view_data['search_tanggal']     = $("#search_date").val();
	view_data['search_alamat']      = $("#search_alamat").val();
	view_data['search_kelamin']     = $("#search_kelamin").val();
	view_data['search_provinsi']    = $("#search_provinsi option:selected").text();
	view_data['search_kabupaten']   = $("#search_kabupaten option:selected").text();
	view_data['search_kecamatan']   = $("#search_kecamatan option:selected").text();
	view_data['search_kelurahan']   = $("#search_kelurahan option:selected").text();
	view_data['search_nobpjs']      = $("#search_nobpjs").val();
	view_data['search_profnum']     = $("#search_profnum").val();
	view_data['search_panggilan']   = $("#search_panggilan").val();	
	view_data['search_tgl_lhr']   	= $("#search_tgl_lhr").val();	
	var cek							= "";
	cek 						   += view_data['search_id'];
	cek 						   += view_data['search_nama'];
	cek 						   += view_data['search_ktp'];
	cek 						   += view_data['search_alamat'];
	cek 						   += view_data['search_nobpjs'];
	cek 						   += view_data['search_profnum'];
	if(	cek == "" &&  $("#reg_autoload_pasien").val()!="1" ){
		this.view_data_null_message = "Tidak Bisa Melakukan Pencarian Minimal Salah Satu dari NRM/NAMA/KTP/Alamat/NO BPJS/Profile Number Terisi";
		return null;
	}
	return view_data;
};


/**
 * off the unused patient
 * so the system will not
 * bribe with the junk
 * */
registration_patient.her_op = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = id;
	data['prop'] = "del";
	bootbox.confirm(
		"Yakin menontaktifkan rekam medis pasien ini (HER-OP)?",
		function(result) {
			if (result) {
				showLoading();
				$.post(	"",data,function(response) {
					var json = getContent(response);
					if (json == null) return;
					self.view();
					dismissLoading();
				});
			}
		}
	);
};



/**
 * this is for finding 
 * who doctor that duty on this
 * patient based on it's schedule
 * this is used setting
 *  - reg_activate_dokter_utama
 *  and would be response by snippet
 *    registration/snippet/get_dokter_utama.php
 * */
registration_patient.getDokterUtama=function(){
	if($("#reg_activate_dokter_utama").val()=="0"){
		return;
	}
	var data=this.getRegulerData();
	data['nrm']=$("#daftar_pasien_nrm").val();
	data['tanggal_masuk']=$("#daftar_pasien_tanggal").val();
	data['action']="get_dokter_utama";
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		$("#daftar_pasien_kode_du").val(json.kode_du);
		$("#daftar_pasien_suggest_du").val(json.suggest_du);
		$("#daftar_pasien_keterangan_du").val(json.keterangan);
		$("#daftar_pasien_editable_du").val(json.editable);
		if(json.editable=="1"){
			$("#daftar_pasien_kode_du").prop("disabled",false);
		}else{
			$("#daftar_pasien_kode_du").prop("disabled",true);
		}
		$(".daftar_pasien_kode_du, .daftar_pasien_keterangan_du").show();
		dismissLoading();
	});
};



/**
 * find patient type methode
 * this is used for finding patient type.
 * so which one need to be visible 
 * and which one need to be invisible
 * */
registration_patient.getJenisPasien=function(js){
    if( js==null || typeof js.carabayar === 'undefined'){
        return;
    }
	var ddlv = {
		page 				: "registration",
		action 				: "get_jenis_pasien",
		prototype_name		: "",
		prototype_slug		: "",
		prototype_implement	: "",				
		slug				: js.carabayar
	};
	showLoading();
	$.post("",ddlv,function(ddlv_res) {
			var js_ddlv = getContent(ddlv_res);
			$("#daftar_pasien_nobpjs").val(js.nobpjs);
			$("#daftar_pasien_nama_perusahaan").val(js.nama_perusahaan);
			$("#daftar_pasien_asuransi").val(js.asuransi);
			smis_edit("#daftar_pasien_tanggal",js.last_time);
			if (js_ddlv != null) {
				$("#daftar_pasien_asuransi").prop("disabled", js_ddlv.asuransi == "0");
				$("#daftar_pasien_nobpjs").prop("disabled", js_ddlv.nobpjs == "0");
				$("#daftar_pasien_nama_perusahaan").prop("disabled", js_ddlv.nama_perusahaan == "0");
                setMoney("#daftar_pasien_plafon_bpjs",Number(js_ddlv.plafon));
            } else {
				$("#daftar_pasien_asuransi").prop("disabled", false);
				$("#daftar_pasien_nobpjs").prop("disabled", false);
				$("#daftar_pasien_nama_perusahaan").prop("disabled", false);
                setMoney("#daftar_pasien_plafon_bpjs",Number(0));
			}
            registration_patient.getDokterUtama();
			dismissLoading();
		}
	);
};



/**
 * find the last visited data
 * this is used to find the 
 * - Penganggung Jawab
 * - Cara Bayar
 * - Phone Number
 * */
registration_patient.getLastVisitedData=function(id){
	var dlv = this.getRegulerData();
	dlv['super_command'] = "daftar_pasien";
	dlv['command'] = "get_last_visited_data";
	dlv['nrm'] = id;
	showLoading();
	$.post("",dlv,function(response) {
		var js = getContent(response);
		if (js == null) return;
		$("#daftar_pasien_namapenanggungjawab").val(js.namapenanggungjawab);
        $("#daftar_pasien_telponpenanggungjawab").val(js.telponpenanggungjawab);
        
        if(typeof js.carabayar!== 'undefined'){
            $("#daftar_pasien_carabayar").val(js.carabayar);
        }
		registration_patient.getJenisPasien(js);
		dismissLoading();
	});
};


registration_patient.register_ruangan=function(id,ruangan,carabayar){
	var self = this;
	var data=this.getRegulerData();
	data['command']="check_baru_lama";
	data['id']=id;
	data['super_command']="daftar_pasien";
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		daftar_pasien.show_add_form();
		$("#daftar_pasien_nrm").val(id);
		$("#"+this.prefix+"_id").prop('disabled', false);
		$("#daftar_pasien_barulama").val(json);
		$("#daftar_pasien_jenislayanan").val(ruangan);
		$("#daftar_pasien_carabayar").val(carabayar);
		registration_patient.getLastVisitedData(id);
		dismissLoading();
	});
};


/**
 * register patient, find the nrm and the status
 * is he new or old patient
 * */
registration_patient.register=function(id){
	var self = this;
	var data=this.getRegulerData();
	data['command']="check_baru_lama";
	data['id']=id;
	data['super_command']="daftar_pasien";
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		daftar_pasien.show_add_form();
		$("#daftar_pasien_nrm").val(id);
		$("#"+this.prefix+"_id").prop('disabled', false);
		$("#daftar_pasien_barulama").val(json);
		registration_patient.getLastVisitedData(id);
		dismissLoading();
	});
};

/**
 * this used to load fingerprint command to user
 * so the patient would be easy to find
 * */
registration_patient.fingerprint=function(id){
	var self = this;
	var data=this.getRegulerData();
	data['action']="fingerprint_loader";
	data['id']=id;
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		dismissLoading();
        setTimeout(function(){ 
            registration_patient.fingerprint_confirm_save(id);
        }, 20000);
	});
};

/**
 * this is used to verivication that the data fingerprint
 * is already set to patient. only confirmation that the data is set
 * */
 registration_patient.fingerprint_confirm_save=function(id){
	var self = this;
	var data=this.getRegulerData();
	data['action']="fingerprint_confirm_save";
	data['id']=id;
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
        $("div#warning > div.modal-header > h3").html(json.title);
		$("div#warning > div.modal-body").html(json.msg);
		dismissLoading();
	});
};


registration_patient.clear = function() {
	TableAction.prototype.clear.call(this);
	$(".error_field").removeClass("error_field");
	$("#modal_alert_registration_patient_add_form").html("");
};

registration_patient.show_add_form=function(){
    /* clear after save*/
    registration_patient.aftersave=function(){};
	is_update = false;
	this.clear();
	$("#"+this.prefix+"_add_form").smodal('show');
	registration_patient.bindChange("registration_patient");
	$("#"+this.prefix+"_id").prop('disabled', false);
	var self=this;
	registration_patient.unbindChange();	
	showLoading();
	var data=this.getRegulerData();
    data['super_command']="kabupaten";
    data['id_propinsi']=$("#registration_patient_provinsi").attr("dv");
	data['code']="registration_patient";
    $.post("",data,function(res){
        var json_kab=getContent(res);
        $("#registration_patient_kabupaten").html(json_kab);        
        data['super_command']="kecamatan";
        data['id_kabupaten']=$("#registration_patient_kabupaten").attr("dv");
        $.post("",data,function(res){
            var json_kec=getContent(res);
            $("#registration_patient_kecamatan").html(json_kec);
            $("#registration_patient_kabupaten").val(data['id_kabupaten']);
            data['super_command']="kelurahan";
            data['id_kecamatan']= $("#registration_patient_kecamatan").val();
            $.post("",data,function(res){
                var json_kel=getContent(res);
                $("#registration_patient_kelurahan").html(json_kel);
                registration_patient.bindChange("registration_patient");
                dismissLoading();	
            });
            
        });
    });
	
};

registration_patient.bindChange=function(name){	
	$("#"+name+"_provinsi").bind("change",function(){
    	registration_patient.propinsi(this.value,name);
    });    
    $("#"+name+"_kabupaten").bind("change",function(){
    	 registration_patient.kabupaten(this.value,name);
    });    
    $("#"+name+"_kecamatan").bind("change",function(){
    	registration_patient.kecamatan(this.value,name);
    });
    $("#"+name+"_kelurahan").bind("change",function(){
    	registration_patient.kelurahan(this.value,name);
    });
};

registration_patient.unbindChange=function(){
	$("#registration_patient_provinsi").unbind("change");	    
    $("#registration_patient_kabupaten").unbind("change");	    
    $("#registration_patient_kecamatan").unbind("change");
    $("#registration_patient_kelurahan").unbind("change");
};

registration_patient.getSaveData=function(){	
	var save_data=this.getRegulerData();
	save_data['command']="save";
	for(var i=0;i<this.column.length;i++){
		var name=this.column[i];
		save_data[name]=smis_get_data("#"+this.prefix+"_"+name);
	}	
	save_data['nama_provinsi']=$("#registration_patient_provinsi option:selected").text();
	save_data['nama_kabupaten']=$("#registration_patient_kabupaten option:selected").text();
	save_data['nama_kecamatan']=$("#registration_patient_kecamatan option:selected").text();
	save_data['nama_kelurahan']=$("#registration_patient_kelurahan option:selected").text();
	save_data['nama_kedusunan']=$("#registration_patient_kedusunan option:selected").text();	
	return save_data;
};

registration_patient.cekSave = function() {
	$(".error_field").removeClass("error_field");
	$("#modal_alert_registration_patient_add_form").html("");
	var result = TableAction.prototype.cekSave.call(this);
	if ($("#registration_patient_id").val().trim() !== "" && is_update == false) {
		var self = this;
		// checking exist nrm:
		var data_nrm = this.getRegulerData();
		data_nrm['id'] = $("#registration_patient_id").val();
		data_nrm['command'] = "check_exist_nrm";
		$.ajax({
			type: "POST",
			url: "",
			async: false,
			data: data_nrm,
			success: function(response_exist_nrm) {
				var json_exist_nrm = getContent(response_exist_nrm);
				if (json_exist_nrm === true) {
					//nrm exist:
					var error_statement = "<strong>No. RM</strong> Sudah Digunakan";
					if (result === false) {
						$("#modal_alert_registration_patient_add_form div").html(
							$("#modal_alert_registration_patient_add_form div").html() + "<br/>" + 
							error_statement
						);
					} else {
						$("#modal_alert_registration_patient_add_form").html(
							"<div class='alert alert-block alert-info'>" +
								"<h4>Pemberitahuan</h4> <br>" +
								error_statement +
							"</div>"
						);
					}
					$("#registration_patient_id").addClass("error_field");
					result = false;
				} else if($("#activate_reg_nrm").val()=="1"){
					data_nrm['id'] = $("#registration_patient_id").val();
					data_nrm['command'] = "check_max_nrm";
					$.ajax({
						type: "POST",
						url: "",
						data: data_nrm,
						async: false,
						success: function(response_max_nrm) {
							var json_max_nrm = getContent(response_max_nrm);
							if (json_max_nrm === true) {
								//nrm greater than last max id:
								var error_statement = "<strong>No. RM</strong> Melebihi Batas Akhir No. RM Terbaru";
								if (result === false)
									$("#modal_alert_registration_patient_add_form div").html(
										$("#modal_alert_registration_patient_add_form div").html() + "<br/>" + 
										error_statement
									);
								else {
									$("#modal_alert_registration_patient_add_form").html(
										"<div class='alert alert-block alert-info'>" +
											"<h4>Pemberitahuan</h4> <br>" +
											error_statement +
										"</div>"
									);
								}
								$("#registration_patient_id").addClass("error_field");
								result = false;
							}
						},
						fail: function() {
							result =  false;
						}
					});
				}
			},
			fail: function() {
				result = false;
			}
		});
	}
	if ($("#registration_patient_nama").val().trim() != "" && $("#registration_patient_alamat").val().trim() != "" && is_update == false) {
		//ehecking exist nama + alamat:
		var data_nama_alamat = this.getRegulerData();
		data_nama_alamat['nama'] = $("#registration_patient_nama").val();
		data_nama_alamat['alamat'] = $("#registration_patient_alamat").val();
		data_nama_alamat['command'] = "check_exist_name_address";
		$.ajax({
			type: "POST",
			url: "",
			data: data_nama_alamat,
			async: false,
			success: function(response_exist_nama_alamat) {
				var json_exist_nama_alamat = getContent(response_exist_nama_alamat);
				if (json_exist_nama_alamat === true) {
					//nama dan alamat sudah terentri:
					var error_statement = "<strong>Nama</strong> dan <strong>Alamat</strong> Sudah Ada";
					if (result === false)
						$("#modal_alert_registration_patient_add_form div").html(
							$("#modal_alert_registration_patient_add_form div").html() + "<br/>" + 
							error_statement
						);
					else {
						$("#modal_alert_registration_patient_add_form").html(
							"<div class='alert alert-block alert-info'>" +
								"<h4>Pemberitahuan</h4> <br>" +
								error_statement +
							"</div>"
						);
					}
					$("#registration_patient_nama").addClass("error_field");
					$("#registration_patient_alamat").addClass("error_field");
					result = false;
				}
			},
			fail: function() {
				result = false;
			}
		});
	}
	return result;
};

registration_patient.save=function (){
	var result = this.cekSave();
	if(result === false){
		return;
	}
	var self=this;
	var a=this.getSaveData();
	showLoading();
	$.ajax({url:"",data:a,type:'post',success:function(res){
		self.hide_add_form();
		var json=getContent(res);
		if(json==null) {
			return;
		}

		if($("#reg_autoload_pasien").val()=="1"){
			self.view();	
		}
		
		self.clear();
		if(REG==1){				
            /*memanggil fungsi registrasi*/
			registration_patient.register(json.id);
			REG=0;
		}else if(REG==2){
            /* memanggil fungsi untuk menambahkan data pasien */
            registration_patient.idcard(json.id);
            REG=0;
        }
        registration_patient.aftersave(json);
	   dismissLoading();
	}});                
};

registration_patient.aftersave=function(json){
    
}

registration_patient.idcard=function(id){
	$("#master_card_anchor").trigger("click");
    if(typeof nomor_asuransi_patient!=='undefined'){
        nomor_asuransi_patient.select_show_form(id);
    }
};


registration_patient.saveReg=function(){
	REG=1;
	this.save();
};


registration_patient.saveCard=function(){
	REG=2;
	this.save();
};

registration_patient.loadPatient=function(){
	var nrm=$("#registration_patient_id").val();
	if(nrm==""){
		showWarning("Error","Tidak Bisa Di Register dengan Cara ini Karena NRM Kosong !!!, Gunakan Save And Register !!!");
	}else{
		registration_patient.hide_add_form();
		registration_patient.register(nrm);
	}
	
};



registration_patient.propinsi=function(id_propinsi,code){
	var data=this.getRegulerData();
    data['super_command']="kabupaten";
    data['id_propinsi']=id_propinsi;
    data['code']=code;
    $.post("",data,function(res){
        var json=getContent(res);
        $("#"+code+"_kabupaten").html(json);
        registration_patient.kabupaten($("#"+code+"_kabupaten").val(),code);
    });
};



registration_patient.kabupaten=function(id_kabupaten,code){
	var data=this.getRegulerData();
    data['super_command']="kecamatan";
    data['id_kabupaten']=id_kabupaten;
    data['code']=code;
    $.post("",data,function(res){
        var json=getContent(res);
        $("#"+code+"_kecamatan").html(json);
        registration_patient.kecamatan($("#"+code+"_kecamatan").val(),code);
    });
    
};

registration_patient.kecamatan=function(id_kecamatan,code){
	var data=this.getRegulerData();
    data['super_command']="kelurahan";
    data['id_kecamatan']=id_kecamatan;
    data['code']=code;
    $.post("",data,function(res){
        var json=getContent(res);
		$("#"+code+"_kelurahan").html(json);
		registration_patient.kelurahan($("#"+code+"_kelurahan").val(),code);
    });
};

registration_patient.kelurahan=function(id_kelurahan,code){         
	 //$("#"+code+"_kelurahan").val(id_kelurahan);
	
	 var data=this.getRegulerData();
    data['super_command']="kedusunan";
    data['id_kelurahan']=id_kelurahan;
    data['code']=code;
    $.post("",data,function(res){
        var json=getContent(res);
        $("#"+code+"_kedusunan").html(json);
    });
};

registration_patient.kedusunan=function(id_kedusunan,code){         
	$("#"+code+"_kedusunan").val(id_kedusunan);
}

registration_patient.skb=function(id){
	var data=this.getRegulerData();
    data['super_command']="skb";
    data['id']=id;
    $.post("",data,function(res){
    	var json=getContent(res);
    	smis_print_force(json);
    });
};

registration_patient.kartu=function(id){
	var data=this.getRegulerData();
    data['super_command']="kartu";
    data['id']=id;
    $.post("",data,function(res){
    	var json=getContent(res);
    	try{
			$("#printing_area").html(json);
		}catch(err){
			smis_print(json);
		}
    });
};

/**
 * binding fast foward so 
 * the user could go to cashie as long as in one autonomous
 * @param id is the noreg_pasien / the registration number
 * 
 * */
registration_patient.fast_cashier=function(id){
    $("#warning").smodal("hide");
	LoadSmisPage({
        page:"kasir",
        action:"pembayaran_patient",
        prototype_name:"",
        prototype_slug:"",
        prototype_implement:"",
        list_registered_select:id
    });
};

registration_patient.kartu_bayi=function(id){
	var data=this.getRegulerData();
    data['super_command']="kartu_bayi";
    data['id']=id;
    $.post("",data,function(res){
    	var json=getContent(res);
    	try{
			$("#printing_area").html(json);
		}catch(err){
			smis_print(json);
		}
    });
};

registration_patient.tarif_kartu=function(id){
    $("#biaya_kartu_anchor").trigger("click");
	$("#biaya_kartu_add").trigger("click");
    if(biaya_kartu_pasien!=="undefined"){
        biaya_kartu_pasien.select(id);
    }
};

/**
 * sinkronisasi data pasien ketika ada perubahan mendadak.
 * 
 * */
registration_patient.sinkronisasi=function(id){
	var data=this.getRegulerData();
	data['command']="sinkronisasi";
    data['id']=id;
	showLoading();
	$.post("",data,function(res){
    	var json=getContent(res);
		showWarning("Peringatan !!!",json);
    	dismissLoading();
    });
};




registration_patient.edit=function (id){
	is_update = true;
	this.clear();
	var self=this;
	showLoading();	
	$("#"+this.prefix+"_id").prop('disabled', true);
	var edit_data					= this.getEditData(id);	
	$.post('',edit_data,function(res){		
		var json					= getContent(res);
		if(json==null) return;		
		registration_patient.unbindChange();
		var id_propinsi	 			= json['provinsi'];
		var id_kabupaten 			= json['kabupaten'];
		var id_kecamatan 			= json['kecamatan'];
		var id_kelurahan 			= json['kelurahan'];
		var id_kedusunan 			= json['kedusunan'];

		var data		 			= self.getRegulerData();
        data['super_command']		= "kabupaten";
        data['id_propinsi']			= id_propinsi;
		data['code']				= "registration_patient";
        $.post("",data,function(res){
            var json_kab			= getContent(res);
            $("#registration_patient_kabupaten").html(json_kab);
          
            data['super_command']	= "kecamatan";
            data['id_kabupaten']	= id_kabupaten;
            $.post("",data,function(res){
                var json_kec		= getContent(res);
                $("#registration_patient_kecamatan").html(json_kec);

                data['super_command']	= "kelurahan";
                data['id_kecamatan']	= id_kecamatan;
                $.post("",data,function(res){
                    var json_kel		= getContent(res);
                    $("#registration_patient_kelurahan").html(json_kel);
					
					data['super_command']	= "kedusunan";
					data['id_kelurahan']	= id_kelurahan;
					$.post("",data,function(res){
						var json_kel		= getContent(res);
						$("#registration_patient_kedusunan").html(json_kel);
						for(var i=0;i<self.column.length;i++){
        					var name		= self.column[i];
        					smis_edit("#"+self.prefix+"_"+name,json[""+name]);
        				}
						registration_patient.bindChange("registration_patient");
						dismissLoading();
					});
                });
                
            });
        });
        self.disabledOnEdit(self.column_disabled_on_edit);
        $("#"+self.prefix+"_add_form").smodal('show');
	});
};


registration_patient.sep_bpjs=function (id){
    $("#"+this.prefix+"_id").prop('disabled', true);
	var edit_data	= this.getEditData(id);
    showLoading();
	$.post('',edit_data,function(res){
        var json	= getContent(res);
		if(json==null) return;
        var ktp		= json.ktp;
        cek_bpjs.cek_nomor_rm(id,ktp);
        $("#cek_bpjs_anchor").trigger("click");
        dismissLoading();    
    });		
};

registration_patient.vclaim_bpjs=function (id){
    $("#"+this.prefix+"_id").prop('disabled', true);
	var edit_data	= this.getEditData(id);
    showLoading();
	$.post('',edit_data,function(res){
        var json	= getContent(res);
		if(json==null) return;
        var ktp		= json.ktp;
        var notelp	= json.telpon;
        cek_vklaim.cek_nomor_rm(id,ktp,notelp);
        $("#cek_vklaim_anchor").trigger("click");
        dismissLoading();    
    });		
};


var perujuk;
perujuk = new TableAction("perujuk","registration","registration_patient",new Array());
perujuk.setSuperCommand("perujuk");
perujuk.selected=function(json){
	$("#daftar_pasien_nama_rujukan").val(json.nama);
	$("#daftar_pasien_id_rujukan").val(json.id);	
};


var besarbonus;
besarbonus = new TableAction("besarbonus","registration","registration_patient",new Array());
besarbonus.setSuperCommand("besarbonus");
besarbonus.selected=function(json){
	setMoney("#daftar_pasien_besarbonus",json.nilai);
	$("#daftar_pasien_kodebonus").val(json.id);
	$("#daftar_pasien_namakodebonus").val(json.nama);
};

$(document).ready(function(){
	$("#search_provinsi").val("");
	$("#search_provinsi").attr("dv", "%");
	$("#table_registration_patient tfoot tr .input-append .btn-group").hide();
	if($("#reg_autoload_pasien").val()=="1"){
		registration_patient.view();
	}
	registration_patient.bindChange("search");
	is_update 	= false;
	if($("#reg_disabled_edit_name").val()=="1"){
		var a	= new Array();
		a.push("nama");
		registration_patient.setDisabledOnEdit(a);			
	}

	if($("#reg_activate_dokter_utama").val()=="1"){
		$("#daftar_pasien_tanggal").on("change",function(){
			registration_patient.getDokterUtama();
		});
	}

	if($("#reg_font_case").val()!="0"){
		var casef 	 = $("#reg_font_case").val();
		$("#registration_patient_nama, #registration_patient_alamat, #registration_patient_tempat_lahir, #registration_patient_istri, #registration_patient_suami, #registration_patient_ayah, #registration_patient_ibu, #registration_patient_desa_keluarga, #registration_patient_alamat_keluarga, #registration_patient_kecamatan_keluarga, #registration_patient_kabupaten_keluarga, #registration_patient_pekerjaan_keluarga, #registration_patient_umur_keluarga ").on("change",function(){
			var str  = $(this).val();
			var nstr = "";
			if(casef=="1"){
				nstr = str.toUpperCase();
			}else if(casef=="2"){
				nstr = str.replace(/\w\S*/g, function(txt){ 
					return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}
				);
			}
			$(this).val(nstr);
		});
	}
});
