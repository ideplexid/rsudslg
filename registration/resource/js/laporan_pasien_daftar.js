
var laporan_pasien_daftar;
$(document).ready(function(){
        var column            = new Array('provinsi','kabupaten','kecamatan','kelurahan','kedusunan',"baru","caradatang","rujukan","carabayar","uri","diagram","kelamin","pulang","origin_data", "f_no_bpjs");
		laporan_pasien_daftar = new ReportAction("laporan_pasien_daftar","registration","laporan_pasien_daftar",column);
		laporan_pasien_daftar.setDiagramHolder("laporan_pasien_daftar_mychart");
		laporan_pasien_daftar.setXKey('Lokasi');
		laporan_pasien_daftar.setYKey(['jumlah']);
		laporan_pasien_daftar.setLabels(["Jumlah"]);

        laporan_pasien_daftar.getViewData = function(){
        	var view_data               = this.getRegulerData();
        	view_data['command']        = "list";
        	view_data['kriteria']       = $("#"+this.prefix+"_kriteria").val();
        	view_data['number']         = $("#"+this.prefix+"_number").val();
        	view_data['max']            = $("#"+this.prefix+"_max").val();
        	view_data['mode']           = $("#"+this.prefix+"_mode_model").val();
            
            view_data['jenislayanan']   = $("#"+this.prefix+"_jenislayanan").val();
        	view_data['kamar_inap']     = $("#"+this.prefix+"_kamar_inap").val();
            view_data['kunjungan']      = $("#"+this.prefix+"_kunjungan").val();
            view_data['no_bpjs']        = $("#"+this.prefix+"_f_no_bpjs").val();
            
            if(view_data['no_bpjs']==""){
               view_data['no_bpjs']     = "%"; 
            }
            
        	for(var i=0;i<this.column.length;i++){
        		var name                = this.column[i];
        		view_data[name]         = $("#"+this.prefix+"_"+name).val();
        	}
        	if(view_data["provinsi"]=="" || view_data["provinsi"]==null){
        		view_data["provinsi"]   = "%";
            }
            if(view_data["kabupaten"]=="" || view_data["kabupaten"]==null){
            	view_data["kabupaten"]  = "%";
            }
            if(view_data["kecamatan"]=="" || view_data["kecamatan"]==null){
            	view_data["kecamatan"]  = "%";
            }            
			if(view_data["kelurahan"]=="" || view_data["kelurahan"]==null){
            	view_data["kelurahan"]  = "%";
            }
            if(view_data["kedusunan"]=="" || view_data["kedusunan"]==null){
            	view_data["kedusunan"]  = "%";
            }
        	return view_data;
        };
        
        laporan_pasien_daftar.setDefaultDate = function() {
            /* Mendapatkan range tanggal 7 hari dari sekarang */
            var date        = new Date();
            var dd_date     = date.getDate();
            var mm_date     = date.getMonth()+1; 
            var yyyy_date   = date.getFullYear();
            if(dd_date<10){
                dd_date     = '0'+dd_date;
            } 
            if(mm_date<10){
                mm_date     = '0'+mm_date;
            } 
            var to_date     = yyyy_date+'-'+mm_date+'-'+dd_date;

            var dateOffset  = (24*60*60*1000) * 7; //7 days
            var date1       = new Date();
            date1.setTime(date1.getTime() - dateOffset);
            var dd_date1    = date1.getDate();
            var mm_date1    = date1.getMonth()+1;
            var yyyy_date1  = date1.getFullYear();
            if(dd_date1<10){
                dd_date1    = '0'+dd_date1;
            } 
            if(mm_date1<10){
                mm_date1    = '0'+mm_date1;
            } 
            var from_date   = yyyy_date1+'-'+mm_date1+'-'+dd_date1;
            
            $("#"+this.prefix+"_from_date").val(from_date);
            $("#"+this.prefix+"_to_date").val(to_date);
        }

        laporan_pasien_daftar.propinsi = function(id_propinsi){
            var data                = this.getRegulerData();
            data['super_command']   = "kabupaten";
            data['id_provinsi']     = id_propinsi;
            $.post("",data,function(res){
                var json            = getContent(res);
                $("#laporan_pasien_daftar_kabupaten").html(json);
                $("#laporan_pasien_daftar_kabupaten").select2();
				laporan_pasien_daftar.kabupaten('%');
            });
        };

        laporan_pasien_daftar.sourcePieAdapter=function(json){
			var so      = new Array();
    		$(json).each(function(idx, obj){
    			var a   = {label:obj.Lokasi,value:obj.jumlah};
    			so.push(a);
    		});
    		return so;
    	};
        
        laporan_pasien_daftar.proceed_loop=function(total_data,current,data){
            if(!smis_loader.isShown() || current>=total_data){
                smis_loader.hideLoader();
                laporan_pasien_daftar.view();
                return;
            }   
            current++;
            $.post("",data,function(res){
                var json    = getContent(res);
                smis_loader.updateLoader('true',"Processing "+json['nama']+" - "+json['id']+"... [ "+current+" / "+total_data+" ] ",current*100/total_data);
                laporan_pasien_daftar.proceed_loop(total_data,current,data);
            });
        
        };
        
        laporan_pasien_daftar.backup = function(){
            var reg_data            = this.getRegulerData();
            reg_data['command']     = "get_total_id";
            smis_loader.updateLoader('true',"Processing... ",0);
            smis_loader.showLoader();
            $.post("",reg_data,function(res){
                var json            = getContent(res);
                reg_data['command'] = "backup_current_id";
                laporan_pasien_daftar.proceed_loop(json,0,reg_data);
            });
        };
        
        laporan_pasien_daftar.kabupaten = function(id_kabupaten){
            var data                = this.getRegulerData();
            data['super_command']   = "kecamatan";
            data['id_kabupaten']    = id_kabupaten;
            $.post("",data,function(res){
                var json            = getContent(res);
                $("#laporan_pasien_daftar_kecamatan").html(json);
                $("#laporan_pasien_daftar_kecamatan").select2();
                laporan_pasien_daftar.kecamatan('%');
            });
        };
        
        laporan_pasien_daftar.kecamatan = function(id_kecamatan){
            var data                = this.getRegulerData();
            data['super_command']   = "kelurahan";
            data['id_kecamatan']    = id_kecamatan;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_pasien_daftar_kelurahan").html(json);
                $("#laporan_pasien_daftar_kelurahan").select2();
                laporan_pasien_daftar.kelurahan('%');
            });
        };

        laporan_pasien_daftar.kelurahan = function(id_kelurahan){
            var data                = this.getRegulerData();
            data['super_command']   = "kedusunan";
            data['id_kelurahan']    = id_kelurahan;
            $.post("",data,function(res){
                var json=getContent(res);
                $("#laporan_pasien_daftar_kedusunan").html(json);
                $("#laporan_pasien_daftar_kedusunan").select2();
            });
        };
    
        $("#laporan_pasien_daftar_provinsi").select2().on("change",function(e){
            laporan_pasien_daftar.propinsi(e.val);
        });
        
         $("#laporan_pasien_daftar_kabupaten").select2().on("change",function(e){
            laporan_pasien_daftar.kabupaten(e.val);
        });
        
        $("#laporan_pasien_daftar_kecamatan").select2().on("change",function(e){
            laporan_pasien_daftar.kecamatan(e.val);
        });
		
		$("#laporan_pasien_daftar_kelurahan").select2().on("change",function(e){
            laporan_pasien_daftar.kelurahan(e.val);
        });
        
        $("#laporan_pasien_daftar_kedusunan").select2();
        
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	laporan_pasien_daftar.initDiagram("");
	laporan_pasien_daftar.setTitle("Pendaftaran Pasien");
	
	$("#laporan_pasien_daftar_caradatang").on("change",function(){
			$("#laporan_pasien_daftar_rujukan").val("%");
		});
        
    laporan_pasien_daftar.setDefaultDate();
	laporan_pasien_daftar.view();
});
