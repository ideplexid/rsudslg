$(document).ready(function(res){
	
	change_noreg_baru.selected=function(json){
		$("#change_noreg_nrm_pasien_baru").val(json.nrm);
		$("#change_noreg_nama_pasien_baru").val(json.nama_pasien);
		$("#change_noreg_noreg_pasien_baru").val(json.id);
	};
	
	change_noreg_layanan.selected=function(json){
		$("#change_noreg_nrm_pasien").val(json.nrm);
		$("#change_noreg_nama_pasien").val(json.nama_pasien);
		$("#change_noreg_noreg_pasien").val(json.id);
	};
	
	change_noreg.proceed=function(){
        var a=this.getRegulerData();
        a['command']            = "save";
        a['nrm_pasien']         = $("#change_noreg_nrm_pasien").val();
        a['nama_pasien']        = $("#change_noreg_nama_pasien").val();
        a['noreg_pasien']       = $("#change_noreg_noreg_pasien").val();
        a['nrm_pasien_baru']    = $("#change_noreg_nrm_pasien_baru").val();
        a['nama_pasien_baru']   = $("#change_noreg_nama_pasien_baru").val();
        a['noreg_pasien_baru']  = $("#change_noreg_noreg_pasien").val();
        
        if(a['noreg_pasien']=="" || a['noreg_pasien']=="[]" || a['nrm_pasien_baru']==""){
            bootbox.alert("Pastikan Nama, NRM, Noreg Pasien Baru dan Lama Terisi, Pastikan juga Nama dan NRM Sama ");
            return;
        }
        
        bootbox.confirm("Anda Yakin, Memproses Data Berikut ini.  "+
        "<table>"+
            "<tr><td>Nomor Reg	  Asal	</td> <td> : </td> <td><strong>"+a['noreg_pasien']+			"</strong></td></tr>"+
            "<tr><td>Nama  Pasien Asal  </td> <td> : </td> <td><strong>"+a['nrm_pasien']+			"</strong></td></tr>"+
            "<tr><td>NRM   Pasien Asal  </td> <td> : </td> <td><strong>"+a['nama_pasien']+			"</strong></td></tr>"+
            "<tr><td>Nomor Reg	  Baru	</td> <td> : </td> <td><strong>"+a['noreg_pasien_baru']+	"</strong></td></tr>"+
            "<tr><td>Nama  Pasien Baru  </td> <td> : </td> <td><strong>"+a['nrm_pasien_baru']+		"</strong></td></tr>"+
            "<tr><td>NRM   Pasien Baru  </td> <td> : </td> <td><strong>"+a['nama_pasien_baru']+		"</strong></td></tr>"+
        "</table>"
        ,function(result){
            if(result){
                showLoading();
                $.post("",a,function(res){
                    $("#change_noreg_nrm_pasien").val("");
                    $("#change_noreg_nama_pasien").val("");
                    $("#change_noreg_noreg_pasien").val("");
                    $("#change_noreg_noreg_pasien_baru").val("");
                    $("#change_noreg_nrm_pasien_baru").val("");
                    $("#change_noreg_nama_pasien_baru").val("");
                    change_noreg.view();
                    dismissLoading();
                });
            }
        });
	};
	
	change_noreg.view();
	
});