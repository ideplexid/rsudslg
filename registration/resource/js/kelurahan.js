var KEL_KODE_KABUPATEN=0;
var KEL_KODE_KECAMATAN=0;
$(document).ready(function(){
	$("#kelurahan_no_prop").on("change",function(){
		var data=kelurahan.getRegulerData();
		data['action']="selector_kabupaten";
		data['id_propinsi']=$("#kelurahan_no_prop").val();
		data['code']="view";
		$.post("",data,function(res){
			var json=getContent(res);
			$("#kelurahan_no_kab").html(json);
			$("#kelurahan_no_kab").val(KEL_KODE_KABUPATEN);
			$("#kelurahan_no_kab").trigger("change");			
		});
	});
	
	$("#kelurahan_no_kab").on("change",function(){
		var data=kelurahan.getRegulerData();
		data['action']="selector_kecamatan";
		data['id_kabupaten']=$("#kelurahan_no_kab").val();
		data['code']="view";
		$.post("",data,function(res){
			var json=getContent(res);
			$("#kelurahan_no_kec").html(json);
			$("#kelurahan_no_kec").val(KEL_KODE_KECAMATAN);
		});
	});
	
	
	kelurahan.edit=function (id){
		var self=this;
		showLoading();	
		var edit_data=this.getEditData(id);
		$.post('',edit_data,function(res){		
			var json=getContent(res);
			if(json==null) return;
			for(var i=0;i<self.column.length;i++){
				if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
					continue;
				}
					
				var name=self.column[i];
				var the_id="#"+self.prefix+"_"+name;
				smis_edit(the_id,json[""+name]);
			}
			dismissLoading();
			self.disabledOnEdit(self.column_disabled_on_edit);
			self.show_form();
			KEL_KODE_KABUPATEN=json.no_kab;
			KEL_KODE_KECAMATAN=json.no_kec;
			$("#kelurahan_no_prop").trigger("change");			
		});
	};
	
});