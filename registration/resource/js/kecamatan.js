var KEC_KODE_KABUPATEN=0;
$(document).ready(function(){
	$("#kecamatan_no_prop").on("change",function(){
		var data=kecamatan.getRegulerData();
		data['action']="selector_kabupaten";
		data['id_propinsi']=$("#kecamatan_no_prop").val();
		data['code']="view";
		$.post("",data,function(res){
			var json=getContent(res);
			$("#kecamatan_no_kab").html(json);
			$("#kecamatan_no_kab").val(KEC_KODE_KABUPATEN);
		});
	});
	
	
	kecamatan.edit=function (id){
		var self=this;
		showLoading();	
		var edit_data=this.getEditData(id);
		$.post('',edit_data,function(res){		
			var json=getContent(res);
			if(json==null) return;
			for(var i=0;i<self.column.length;i++){
				if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
					continue;
				}
					
				var name=self.column[i];
				var the_id="#"+self.prefix+"_"+name;
				smis_edit(the_id,json[""+name]);
			}
			dismissLoading();
			self.disabledOnEdit(self.column_disabled_on_edit);
			self.show_form();
			KEC_KODE_KABUPATEN=json.no_kab;
			$("#kecamatan_no_prop").trigger("change");			
		});
	};
	
});