/**
 * this page used to control and registration
 * of patient BPJS so usign VClaim system
 * 
 * @author      : Nurul Huda
 * @since       : 17 Feb 2018
 * @license     : LGPv2
 * @copyright   : goblooge@gmail.com
 * @version     : 1.0.1
 * @used        : registration/resource/php/registration_patient/cek_vklaim.php
 * 
 */
 var cek_vklaim;
 var cek_vklaim_icd;
 var cek_vklaim_dokter;
 var cek_vklaim_dpjp;
 var cek_vklaim_poli;
 var cek_vklaim_provinsi;
 var cek_vklaim_kabupaten;
 var cek_vklaim_kecamatan;
 var cek_vklaim_kecelakaan;
 var current_pasien;
 $(document).ready(function(){
     $(".mydatetime").datetimepicker({minuteStep:1});
     $("#cek_vklaim_noSurat").prop("disabled","disabled");
     $("#cek_vklaim_asalRujukan").on("click",function(){
         var vl = $(this).val();
         if(vl=="1" || vl=="2"){
             $("#cek_vklaim_diambil_dari option[value='Nomor Rujukan']").show();
             $("#cek_vklaim_diambil_dari option[value='Nomor BPJS']").show();
             $("#cek_vklaim_diambil_dari option[value='Nomor KTP']").hide();
             $("#cek_vklaim_jnsPelayanan").val(2);
             $("#cek_vklaim_jnsPelayanan").prop("disabled",true);
             //$(".fcontainer_cek_vklaim_namapoliTujuan").show();
             
             
             //$("#cek_vklaim_tglRujukan").prop("disabled",true);
             $("#cek_vklaim_namaRujukan").prop("disabled",true);
             $("#cek_vklaim_ppkRujukan").prop("disabled",true);
             $("#cek_vklaim_noRujukan").prop("disabled",true);
 
         }else{
             $("#cek_vklaim_diambil_dari option[value='Nomor Rujukan']").hide();
             $("#cek_vklaim_diambil_dari option[value='Nomor BPJS']").show();
             $("#cek_vklaim_diambil_dari option[value='Nomor KTP']").show();
             $("#cek_vklaim_jnsPelayanan").val("");
             $("#cek_vklaim_jnsPelayanan").prop("disabled",false);
             //$(".fcontainer_cek_vklaim_namapoliTujuan").show();
 
             
             //$("#cek_vklaim_tglRujukan").prop("disabled",false);
             $("#cek_vklaim_namaRujukan").prop("disabled",false);
             $("#cek_vklaim_ppkRujukan").prop("disabled",false);
             $("#cek_vklaim_noRujukan").prop("disabled",false);
         }
 
         if($("#cek_vklaim_asalRujukan").val()==""){
             $("#cek_vklaim_namaRujukan").val("Rumah Sakit");
             $("#cek_vklaim_ppkRujukan").val($("#cek_vklaim_ppkPelayanan").val());
         }else{
             $("#cek_vklaim_namaRujukan").val("");
             $("#cek_vklaim_ppkRujukan").val("");
         }
 
         $("#cek_vklaim_diambil_dari").val("");
         $(".ranap").hide();
         
     });
     $("#cek_vklaim_asalRujukan").trigger("click");
 
     /*$("#cek_vklaim_tujuanKunj").on("change",function(){
         var vl = $(this).val();
         if(vl=="0"){
             $("#cek_vklaim_flagProcedure").val("");
             $("#cek_vklaim_kdPenunjang").val("");
             $("#cek_vklaim_assesmentPel").val(4);
         }else if(vl=="2"){
             $("#cek_vklaim_assesmentPel").val(4);
         }
     });*/
 
     $("#cek_vklaim_naikKelas").on("change",function(){
         var vl = $(this).val();
         if(vl==""){
             $("#cek_vklaim_pembiayaan").val("");
         }
     });
     $("#cek_vklaim_asalRujukan").trigger("click");
     $("#cek_vklaim_no_bpjs, #cek_vklaim_no_ktp ").keyup(function(e){ 
         var code = e.which;
         if(code==13)e.preventDefault();
         if(code==32||code==13||code==188||code==186){
             cek_vklaim.fetch_server();
         }
     });
     
     cek_vklaim = new TableAction("cek_vklaim","registration","cek_vklaim");
     cek_vklaim.refresh = function(){
         var j = this.getRegulerData();
         showLoading();
         $.post("",j,function(res){
             $("#cek_vklaim").html(res);
             dismissLoading();
         });
     }
     cek_vklaim.fetch_server=function(){
         $("#cek_vklaim_noSEP").val("");
         data            = this.getRegulerData();
         data['no_bpjs'] = $("#cek_vklaim_no_bpjs").val();
         data['no_ktp']  = $("#cek_vklaim_no_ktp").val();
         data['command'] = "cek_vklaim";
         
         if(data['no_bpjs']=="" && (data['no_ktp']=="" || data['no_ktp']=="-") ){
             showWarning("Terjadi Kesalahan !!!","Masukan No. BPJS atau No. KTP, jika No. BPJS terisi No. KTP tidak dihiraukan");
         }
         
         showLoading();
         $.post("",data,function(res){
               var json  = getContent(res);
               $("#cek_vklaim_page").html(json.table);
               cek_vklaim.reset_part();
               if(json.raw != null && json.raw.status=="AKTIF"){
                   console.log("RAW CEK VKLAIM");
                   console.log(json);
                   $("#cek_vklaim_noKartu").val(json.raw.nobpjs);
                   $("#cek_vklaim_no_ktp").val(json.raw.nik);
                   $("#cek_vklaim_no_bpjs").val(json.raw.nobpjs);
                   $("#cek_vklaim_ppkRujukan").val(json.raw.kd_provider);
                   $("#cek_vklaim_namaRujukan").val(json.raw.nama_provider);
                   $("#cek_vklaim_poliPerujuk").val(json.raw.nama_poli);
                   
               }
               dismissLoading();
         });
     };
 
     cek_vklaim.daftar_pasien = function(){
         $("#warning").smodal("hide");
         registration_patient.show_add_form();
         setTimeout(function(){
             $("#registration_patient_save_reg").addClass("hide");
             $("#registration_patient_reg").addClass("hide");
             var tglhr = current_pasien.tgl_lahir.split("-");
             registration_patient.set("tgl_lahir",tglhr[2]+"-"+tglhr[1]+"-"+tglhr[0]);
             registration_patient.set("kelamin",current_pasien.jk=="L"?0:1);
             registration_patient.set("nama",current_pasien.nama);
             registration_patient.set("ktp",current_pasien.nik);
             registration_patient.set("umur",current_pasien.umur);
             registration_patient.set("nobpjs",current_pasien.nobpjs);
             registration_patient.aftersave=function(json){
                 $("#cek_vklaim_id_pasien").val(json.id);
                 $("#cek_vklaim_noMr").val(json.nrm);
                 $("#cek_vklaim_noTelp").val(json.telpon);
                 $("#cek_vklaim_status_nrm").val("Sudah Ada");
                 activeTab("#cek_vklaim");
             };
             activeTab("#registration_patient_tab");
         },1000);
     }
     
     cek_vklaim.cek_rujukan=function(){
         console.log("==cek_vklaim.cek_rujukan===");
         $("#cek_vklaim_noSEP").val("");
         data                = this.getRegulerData();
         data['noRujukan']   = $("#cek_vklaim_noRujukan").val();
         data['noBPJS']      = $("#cek_vklaim_no_bpjs").val();
         data['faskes']      = $("#cek_vklaim_asalRujukan").val();
         data['command']     = "cek_rujukan";
         
         if(data['noBPJS']=="" && data['noRujukan']=="" ){
             showWarning("Terjadi Kesalahan !!!","Silakan Isikan Nomor Rujukanya atau Nomor BPJS Terlebih Dahulu, kemudian tekan enter atau Klik Tombol Pencarian Rujukan");
         }
         
         showLoading();
         $.post("",data,function(res){
               var json      = getContent(res);
               $("#cek_vklaim_noRujukan").val(data['noRujukan']);
               current_pasien = null;
               if(json.message=="Data Ditemukan"){
                   console.log("ek_vklaim.cek_rujukan -> data ditermukan");
                   console.log(json);
                     current_pasien = json;
                   cek_vklaim.reset_part();
                   $("#cek_vklaim_no_bpjs").val(json.nobpjs);     
                   $("#cek_vklaim_status_nrm").val(json.status_nrm);
                   $("#cek_vklaim_noKartu").val(json.nobpjs);                  
                   $("#cek_vklaim_tglRujukan").val(json.tgl_kunjungan);
                   $("#cek_vklaim_ppkRujukan").val(json.kd_provider);
                   $("#cek_vklaim_namaRujukan").val(json.nama_provider);
                   $("#cek_vklaim_poliPerujuk").val(json.nama_poli);
                   $("#cek_vklaim_diagAwal").val(json.kode_diagnosa);
                   $("#cek_vklaim_poliTujuan").val(json.kd_poli);
                   $("#cek_vklaim_namapoliTujuan").val(json.nama_poli);
                   $("#cek_vklaim_catatan").val(json.catatan);
                   $("#cek_vklaim_noRujukan").val(json.no_kunjungan);
                   $("#cek_vklaim_catatan").val(json.catatan);
                   $("#cek_vklaim_no_ktp").val(json.nik);
                   $("#cek_vklaim_klsRawat").val(json.kelas);
                   $("#cek_vklaim_asalRujukan").val(json.asal_rujukan);  
                   $("#cek_vklaim_namapoliTujuan").trigger("change");  
                   //$("#cek_vklaim_noRujukan").val(json.no_rujukan);              
                   if(json.kd_poli!=null && json.kd_poli!=""){
                     $("#cek_vklaim_jnsPelayanan").val(2);
                   }else{
                     $("#cek_vklaim_jnsPelayanan").val(1); 
                   }
 
                   if(json.status_nrm=="Belum Ada"){
                     $("#cek_vklaim_id_pasien").val("");
                     $("#cek_vklaim_noMr").val("");
                     $(".df_pasien").show();
                     showWarning("Peringatan","Pasien ini Belum Punya no RM ! </br> <a href='#' class='btn btn-primary' onclick='cek_vklaim.daftar_pasien()'>Daftarkan Pasien Baru</a>");
                   }else{
                      $("#cek_vklaim_id_pasien").val(json.id_pasien);
                      $("#cek_vklaim_noMr").val(json.nrm_pasien);
                      $("#cek_vklaim_noMr").val(json.nrm_pasien);
                      $(".df_pasien").hide();                    
                   }
                   //$("#cek_vklaim_jnsPelayanan").trigger("change");
 
                   data            = cek_vklaim.getRegulerData();
                   //data['no_bpjs'] = $("#cek_vklaim_no_bpjs").val();
                   //data['no_ktp']  = $("#cek_vklaim_no_ktp").val();
                   data['command'] = "cek_vklaim";
                   console.log("cek_vklaim.cek_rujukan -> cek vklaim");
                   //$("#cek_vklaim_noRujukan").val("");
                   data['no_ktp']="";
                   data['no_bpjs']=json.nobpjs;
                   //alert($("#cek_vklaim_noRujukan").val()+"  "+json.no_kunjungan);  
                   showLoading();
                   $.post("",data,function(res){
                       var json  = getContent(res);
                       console.log("cek_vklaim.cek_rujukan -> cek vklaim -> response ");
                       console.log(json);
                       $("#cek_vklaim_page").html(json.table);
                       //cek_vklaim.reset_part();
                       if(json.raw != null && json.raw.status=="AKTIF"){
                           $("#cek_vklaim_noKartu").val(json.raw.nobpjs);
                           $("#cek_vklaim_no_ktp").val(json.raw.nik);
                           $("#cek_vklaim_no_bpjs").val(json.raw.nobpjs);
                           //$("#cek_vklaim_ppkRujukan").val(json.raw.kd_provider);
                           //$("#cek_vklaim_namaRujukan").val(json.raw.nama_provider);
                       }
                       if(json.raw!=null){
                           current_pasien = json.raw;
       
                           $("#cek_vklaim_status_nrm").val(json.raw.status_nrm);
                           if(json.raw.status_nrm=="Sudah Ada"){
                               //alert("BELUM ADA");
                               $("#cek_vklaim_id_pasien").val(json.raw.id_pasien);
                               $("#cek_vklaim_noMr").val(json.raw.nrm_pasien);
                               $("#cek_vklaim_noTelp").val(json.raw.telpon);
                               $("#df_pasien").addClass("hide");
                           }else{
                             //alert("BELUM ADA");
                             showWarning("Peringatan","<div style='text-align:center'>Pasien ini Belum Punya no RM ! </br> <a href='#' class='btn btn-primary' onclick='cek_vklaim.daftar_pasien()'>Daftarkan Pasien Baru</a></div>");
                             $("#cek_vklaim_id_pasien").val("");
                             $("#cek_vklaim_noMr").val("");
                             $("#df_pasien").removeClass("hide");
                           }                         
                       }
                       cek_vklaim.cek_nomor_rujukan_pernah_dipakai();
                       dismissLoading();
                   });
 
               }else{
                   showWarning("Terjadi Kesalahan !!!",json.message);
               }
               dismissLoading();
         });
     };
 
     cek_vklaim.chooser=function(modal,elm_id,param,action,modal_title){
         if(elm_id == "cek_vklaim_namapoliTujuan" && $("#"+elm_id).prop('disabled')){
             return;
         }
         var the_data=action.getChooserData();
         if(the_data==null){
            var data=this.getRegulerData();
            the_data=this.addChooserData(data);
         }
         var self=this;
         the_data['super_command']=param;
         $.post('',the_data,function(res){
             show_chooser(self,param,res,action.getShowParentModalInChooser(),modal_title);
             action.view();
             action.focusSearch();
             this.current_chooser=action;
             CURRENT_SMIS_CHOOSER=action;
         });
         return this;
     };
 
     cek_vklaim.cek_nomor_rujukan_pernah_dipakai = function(){
         //$("#cek_vklaim_namapoliTujuan").prop("disabled",false);
         if($("#cek_vklaim_diambil_dari").val()!="Nomor Rujukan"){
             return;
         }
         var dt = this.getRegulerData();
         dt['command'] = "cek_nomor_rujukan_pernah_dipakai";
         dt['no_rujukan'] = $("#cek_vklaim_inputNoPencarian").val();
         dt['no_kartu'] = $("#cek_vklaim_noKartu").val();
 
         if(dt['no_rujukan']=="" || dt['no_kartu']==""){
             return;
         }
 
         showLoading();
         $.post("",dt,function(res){
             var json = getContent(res);
             if(json==0){
                 //$("#cek_vklaim_namapoliTujuan").prop("disabled",true);
             }
             dismissLoading();
         });
         
 
     };
 
     cek_vklaim.cari = function(){
         console.log("cek_vklaim.cari");
         current_pasien = null;
         var nomor = $("#cek_vklaim_inputNoPencarian").val();
         if(nomor==""){
             smis_alert("Warning","Nomor Tidak Boleh Kosong","alert-danger");
             return;
         }
         var cari = $("#cek_vklaim_diambil_dari").val();
         var asal = $("#cek_vklaim_asalRujukan").val();
 
         cek_vklaim.set("jnsPelayanan","");
         cek_vklaim.set("tglSep","");
         cek_vklaim.set("eksekutif",0);
         cek_vklaim.set("namapoliTujuan","");
         cek_vklaim.set("dpjpLayanNama","");
         cek_vklaim.set("noMr","");
         
         cek_vklaim.set("diagAwal","");
         cek_vklaim.set("cob",0);
         cek_vklaim.set("katarak",0);
         cek_vklaim.set("catatan","");
         cek_vklaim.set("noTelp","");
         cek_vklaim.set("tujuanKunj","");
         cek_vklaim.set("flagProcedure","");
         cek_vklaim.set("kdPenunjang","");
         cek_vklaim.set("assesmentPel","");
         cek_vklaim.set("noSurat","");
         cek_vklaim.set("namaDPJP","");
         cek_vklaim.set("lakaLantas","");
         $("#cek_vklaim_noSEP").val("");
         $(".fnosep").hide();
         $(".cek_vklaim_namapoliTujuan").show();
         if($("#cek_vklaim_asalRujukan").val()!=""){
             cek_vklaim.set("namaRujukan","");
             cek_vklaim.set("ppkRujukan","");
             cek_vklaim.set("tglRujukan","");
             cek_vklaim.set("noRujukan","");    
         }
 
 
         if(asal==""){
             $("#cek_vklaim_noSEP").val("");
             data            = this.getRegulerData();
             //data['no_bpjs'] = $("#cek_vklaim_no_bpjs").val();
             //data['no_ktp']  = $("#cek_vklaim_no_ktp").val();
             data['command'] = "cek_vklaim";
             $("#cek_vklaim_noRujukan").val("");
             if(cari=="Nomor BPJS"){
                 data['no_ktp']="";
                 data['no_bpjs']=nomor;
             }else{
                 data['no_ktp']=nomor;
                 data['no_bpjs']="";
             }
                         
             showLoading();
             $.post("",data,function(res){
                 var json  = getContent(res);
                 console.log("CARI VCLAIM");
                 $("#cek_vklaim_page").html(json.table);
                 cek_vklaim.reset_part();
                 if(json.raw != null && json.raw.status=="AKTIF"){
                     console.log("NEXT RAW AKTIF");
                     console.log(json);
                     $("#cek_vklaim_noKartu").val(json.raw.nobpjs);
                     $("#cek_vklaim_no_ktp").val(json.raw.nik);
                     $("#cek_vklaim_no_bpjs").val(json.raw.nobpjs);
                     if($("#cek_vklaim_asalRujukan").val()!=""){
                         console.log("NGISI");
                         $("#cek_vklaim_ppkRujukan").val(json.raw.kd_provider);
                         $("#cek_vklaim_namaRujukan").val(json.raw.nama_provider);   
                         $("#cek_vklaim_poliPerujuk").val(json.raw.nama_poli); 
                     }
                 }
                 if(json.raw!=null){
                     current_pasien = json.raw;
                     $("#cek_vklaim_status_nrm").val(json.raw.status_nrm);
                     if(json.raw.status_nrm=="Sudah Ada"){
                         //alert("BELUM ADA");
                         $("#cek_vklaim_id_pasien").val(json.raw.id_pasien);
                         $("#cek_vklaim_noMr").val(json.raw.nrm_pasien);
                         $("#cek_vklaim_noTelp").val(json.raw.telpon);
                         $("#df_pasien").addClass("hide");
                     }else{
                       showWarning("Peringatan","<div style='text-align:center'>Pasien ini Belum Punya no RM ! </br> <a href='#' class='btn btn-primary' onclick='cek_vklaim.daftar_pasien()'>Daftarkan Pasien Baru</a></div>");
                       //alert("BELUM ADA");
                       $("#cek_vklaim_id_pasien").val("");
                       $("#cek_vklaim_noMr").val("");
                       $("#df_pasien").removeClass("hide");
                     }
                     $("#cek_vklaim_klsRawat").val(json.raw.kd_kelas);
                 }
                 dismissLoading();
             });
         }else{
             console.log("vklaim_cari CARI RUJUKAN");
             if(cari=="Nomor Rujukan"){
                 console.log("vklaim_cari Pakai NOmor Rujukan");
                 //var norujukan = $("#cek_vklaim_noRujukan").val();
                 $("#cek_vklaim_noRujukan").val(nomor);
                 $("#cek_vklaim_no_bpjs").val("");
                 $("#cek_vklaim_no_ktp").val("");            
                 cek_vklaim.cek_rujukan();
             }else{
                 console.log("vklaim_cari Pakai NOmor BPJS");
                 $("#cek_vklaim_noRujukan").val("");
                 $("#cek_vklaim_no_bpjs").val(nomor);
                 $("#cek_vklaim_no_ktp").val("");
                 cek_vklaim.cek_rujukan();
             }
         }
         
     }
     
     
     cek_vklaim.reset=function(){
         $("#cek_vklaim_noSEP").val("");
         $("#cek_vklaim_no_ktp").val(""); 
         $("#cek_vklaim_noMr").val("");
         $("#cek_vklaim_noTelp").val("");
         cek_vklaim.reset_part();
     };
     
     cek_vklaim.reset_part=function(){
         var currentdate = new Date(); 
         var month       = (currentdate.getMonth()+1);
         if(month<10){
                 month   = "0"+month;
         }        
         var dt          = currentdate.getDate();
         if(dt<10){
                 dt      = "0"+dt;
         }        
         var hour        = currentdate.getHours();
         if(hour<10){
                 hour    = "0"+hour;
         }        
         var minute      = currentdate.getMinutes();
         if(minute<10){
                 minute  = "0"+minute;
         }        
         var second      = currentdate.getSeconds();
         if(second<10){
                 second  = "0"+second;
         }        
         var datetime    = currentdate.getFullYear() + "-" + month + "-" + dt + " "+ hour + ":" + minute + ":"+ second;         
         $("#cek_vklaim_noKartu").val("");
         $("#cek_vklaim_no_bpjs").val("");
         $("#cek_vklaim_tglSep").val(datetime);
         if($("#cek_vklaim_asalRujukan").val()!=""){
             $("#cek_vklaim_tglRujukan").val(datetime);
             $("#cek_vklaim_noRujukan").val("");
             $("#cek_vklaim_ppkRujukan").val("");
             $("#cek_vklaim_namaRujukan").val("");    
         }
         $("#cek_vklaim_jnsPelayanan").val("");
         $("#cek_vklaim_diagAwal").val("");
         $("#cek_vklaim_poliTujuan").val("");
         $("#cek_vklaim_klsRawat").val("");
         $("#cek_vklaim_lakaLantas").val("");
         $("#cek_vklaim_lokasiLaka").val("");
         $("#cek_vklaim_catatan").val("");
         $("#cek_vklaim_noREG").val("");
         $("#cek_vklaim_penjamin").val("");
         $("#cek_vklaim_cob").val("");
         $("#cek_vklaim_eksekutif").val("");
         $("#cek_vklaim_asalRujukan").val("");        
     };
     
      cek_vklaim.print_sep=function(){
          var nosep      = $("#cek_vklaim_noSEP").val();
          if(nosep==""){
             showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
             return;
         }
          data           = this.getRegulerData();
          data['nosep']  = nosep;
          data['action'] = "print_vklaim";
          showLoading();
          $.post("",data,function(res){
              var json=getContent(res);
              //smis_print(json);
              
              var getUrl = window.location['pathname']+json;
              window.open(getUrl, 'pdf');
              dismissLoading();
          });
      };
      
      cek_vklaim.sep_sementara=function(){
          var self                       = this;
          bootbox.confirm("SEP Sementara tidak terhubung dengan SIM SEP BPJS, Hanya Dipakai ketika Jaringan BPJS Bermasalah dan harus diinput ulang oleh petugas maksimal 3 hari", function(result){ 
             if(result){
                 data                    = self.getRegulerData();
                 data['tanggal']         = $("#"+self.prefix+"_tglSep").val();
                 data['no_bpjs']         = $("#"+self.prefix+"_no_bpjs").val()==""?$("#"+self.prefix+"_no_bpjs").val():$("#"+self.prefix+"_noKartu").val();
                 data['no_rujukan']      = $("#"+self.prefix+"_noRujukan").val();
                 data['no_ktp']          = $("#"+self.prefix+"_no_ktp").val();
                 data['nrm']             = $("#"+self.prefix+"_noMr").val();
                 data['poli_tujuan']     = $("#"+self.prefix+"_poliTujuan option:selected").text();;
                 data['diagnosa_awal']   = $("#"+self.prefix+"_diagAwal").val();
                 data['catatan']         = $("#"+self.prefix+"_catatan").val();
                 data['jns_rawat']       = $("#"+self.prefix+"_jnsPelayanan option:selected").text();
                 data['kasus_laka']      = $("#"+self.prefix+"_lakaLantas option:selected").text();
                 data['lokasi_laka']     = $("#"+self.prefix+"_lokasiLaka").val();;
                 data['action']          = "print_sep_sementara";
                 showLoading();
                 $.post("",data,function(res){
                     var json            = getContent(res);
                     smis_print(json);
                     dismissLoading();
                 });
             }
         });
      };
      
     cek_vklaim.register_pasien_bpjs=function(){
         var nosep   = $("#"+this.prefix+"_noSEP").val();
         var noreg   = $("#"+this.prefix+"_noREG").val();
         var nrm     = $("#"+this.prefix+"_noMr").val();
         var nobpjs  = $("#"+this.prefix+"_noKartu").val();
         var kelas   = $("#"+this.prefix+"_klsRawat").val();
         if(nosep==""){
             showWarning("Peringatan !!!","Silakan Masukan Nomor BPJS Terlebih dahulu, pastikan pasien aktif, kemudian create SEP baru registrasi.");
             return;
         }        
         if(noreg=="0"){
             daftar_pasien.show_add_form_nosep(nrm,nosep,nobpjs,kelas);
         }else{
             daftar_pasien.edit_nosep(noreg,nosep,nobpjs,kelas);    
         }
     };
     
     cek_vklaim.cek_nomor_rm=function(id_pasien,nrm,ktp,telpon){
         cek_vklaim.reset();
         $("#cek_vklaim_id_pasien").val(id_pasien);
         $("#cek_vklaim_noMr").val(nrm);
         $("#cek_vklaim_no_ktp").val(ktp);
         $("#cek_vklaim_noTelp").val(telpon);
         $("#cek_vklaim_no_bpjs").val("");
         if(ktp=="" || ktp=="-"){
             var data        = this.getRegulerData();
             data['nrm']     = nrm;
             data['action']  = "search_no_bpjs";
             showLoading();
             $.post("",data,function(res){
                 dismissLoading();
                 var json=getContent(res);
                 if(json==""){
                     showWarning("No. BPJS/KTP/NIK Kosong","dikarenakan No. BPJS/KTP/NIK kosong, silakan isikan No. BPJS/KTP/NIK pasien yang bersangkutan");
                 }else{
                     $("#cek_vklaim_no_bpjs").val(json);
                     $("#cek_vklaim_button").trigger("click");
                 }
             });
         }else{
             $("#cek_vklaim_button").trigger("click");
         }
     };
 
     cek_vklaim.cari_laka = function(){
         cek_vklaim.chooser('cek_vklaim','cek_vklaim_no_suplesi','cek_vklaim_kecelakaan',cek_vklaim_kecelakaan,'Kecelakaan')
     }
     
 
     cek_vklaim.fix_register=function(){
         data            = this.getRegulerData();
         data['noSEP']   = $("#cek_vklaim_noSEP").val();
         data['noMr']    = $("#cek_vklaim_noMr").val();
         if(data['noSEP']=="" || data['noMr']==""){
             showWarning("Peringatan !!!","Silakan Bikin BPJS SEP terlebih dahulu, Jika No RM kosong, silakan buka Menu Registrasi Pasien, Pilih pasienya, kemudian Tekan Opsi SEP. Pastikan No. KTP/NIK terisi untuk kemudahan");
             return;
         }        
         data['command'] = "fix_register";
         showLoading();
         $.post("",data,function(res){
               var json  = getContent(res);
               $("#cek_vklaim_noREG").val(json.noreg);
               dismissLoading();
         });
     };
 
     
     cek_vklaim.reg_sep=function(){
         $("#cek_vklaim_noSEP").val("");
         data                 = this.getRegulerData();
         
         data['jnsPelayanan'] = cek_vklaim.get("jnsPelayanan");
         data['eksekutif']    = cek_vklaim.get("eksekutif");
         data['noKartu']      = cek_vklaim.get("noKartu");
         data['tglSep']       = cek_vklaim.get("tglSep");
         data['tglRujukan']   = cek_vklaim.get("tglRujukan");
         data['noRujukan']    = cek_vklaim.get("noRujukan");
         data['ppkRujukan']   = cek_vklaim.get("ppkRujukan");
         data['ppkPelayanan'] = cek_vklaim.get("ppkPelayanan");
         data['jnsPelayanan'] = cek_vklaim.get("jnsPelayanan");
         data['diagAwal']     = cek_vklaim.get("diagAwal");
         data['poliTujuan']   = cek_vklaim.get("poliTujuan");
         data['klsRawat']     = cek_vklaim.get("klsRawat");
 
         data['lakaLantas']   = cek_vklaim.get("lakaLantas");
         data['tgl_kejadian']   = cek_vklaim.get("tgl_kejadian");
         data['no_suplesi']   = cek_vklaim.get("no_suplesi");
         data['lokasiLaka']   = cek_vklaim.get("lokasiLaka");
         data['kdPropinsi']   = cek_vklaim.get("kdPropinsi");
         data['kdKabupaten']  = cek_vklaim.get("kdKabupaten");
         data['kdKecamatan']  = cek_vklaim.get("kdKecamatan");
         data['keterangan']   = cek_vklaim.get("keterangan");
 
         data['catatan']     = cek_vklaim.get("catatan");
         data['user']        = cek_vklaim.get("user");
         data['noMr']        = cek_vklaim.get("noMr");
         data['id_pasien']   = cek_vklaim.get("id_pasien");        
         data['penjamin']    = cek_vklaim.get("penjamin");
         data['cob']         = cek_vklaim.get("cob");
         data['tujuanKunj']  = cek_vklaim.get("tujuanKunj");
         data['flagProcedure']  = cek_vklaim.get("flagProcedure");
         data['kdPenunjang']  = cek_vklaim.get("kdPenunjang");
         data['assesmentPel']  = cek_vklaim.get("assesmentPel");
 
         data['naikKelas']  = cek_vklaim.get("naikKelas");
         data['pembiayaan']  = cek_vklaim.get("pembiayaan");
         data['penanggunjwb']  = cek_vklaim.get("penanggunjwb");
         
         data['noSurat']     = cek_vklaim.get("noSurat");
         data['kodeDPJP']    = cek_vklaim.get("kodeDPJP");
     
         data['asalRujukan'] = cek_vklaim.get("asalRujukan");
         data['tglRujukan'] = cek_vklaim.get("tglRujukan");
         data['ppkRujukan'] = cek_vklaim.get("ppkRujukan");
         data['noRujukan'] = cek_vklaim.get("noRujukan");
         
         data['noTelp']      = cek_vklaim.get("noTelp");
         data['dpjpLayan']      = cek_vklaim.get("dpjpLayan");
         data['command']     = "reg_sep";
 
         data['n_tujuanKunjungan'] = $("#cek_vklaim_tujuanKunj option:selected").text().trim();
         data['n_flagProcedure'] = $("#cek_vklaim_flagProcedure option:selected").text().trim();
         data['n_kdPenunjang'] = $("#cek_vklaim_kdPenunjang option:selected").text().trim();
         data['n_assesmentPel'] = $("#cek_vklaim_assesmentPel option:selected").text().trim();
         data['n_poliTujuan'] = $("#cek_vklaim_poliPerujuk").val().trim();
         data['n_namarujukan'] = cek_vklaim.get("namaRujukan");
         data['n_potensiprb'] = $("#vclaim_postensi_prb").html();
         vclaim_postensi_prb
 
         showLoading();
         $.post("",data,function(res){
               var json      = getContent(res);
               $("#cek_vklaim_noREG").val(json.noreg);
               $("#cek_vklaim_noSEP").val(json.noSEP);
               $(".fnosep").show();
               dismissLoading();
         });
     };
     
     $("#cek_vklaim_noRujukan").keyup(function(e){
         if (e.keyCode==13) {
             cek_vklaim.cek_rujukan();
         }
     });
     
     $("#cek_vklaim_lakaLantas").on("change",function(e){
 
         if($("#cek_vklaim_lakaLantas").val()=="0"){
             $(".kclk").hide();
         }else{
             $(".kclk").show();
         }
 
         if( $("#cek_vklaim_lakaLantas").val()=="1"){
             $("#cek_vklaim_lokasiLaka").prop("disabled",false);
         }else{
             $("#cek_vklaim_lokasiLaka").prop("disabled",true);
             $("#cek_vklaim_lokasiLaka").val("");
         }
     });
     
     
     $("#cek_vklaim_jnsPelayanan").on("change",function(e){
         
         /*if( $(this).val()=="2"){
             //rawat jalan
             $("#cek_vklaim_klsRawat").val("3");
             $("#cek_vklaim_klsRawat").attr('disabled','disabled');
             $("#cek_vklaim_poliTujuan").removeAttr('disabled');
         }else{
             //rawat inap
            $("#cek_vklaim_klsRawat").val("");
            $("#cek_vklaim_klsRawat").removeAttr('disabled');
            $("#cek_vklaim_poliTujuan").val("");
            $("#cek_vklaim_poliTujuan").attr('disabled','disabled');
         }*/
         if($(this).val()=="1"){
             $(".ranap").show();
             //$(".fcontainer_cek_vklaim_namapoliTujuan").hide();
             $("#cek_vklaim_namapoliTujuan").val("");
             $("#cek_vklaim_poliTujuan").val("");            
         }else{
             $(".ranap").hide();
             //$(".fcontainer_cek_vklaim_namapoliTujuan").show();
         }
 
         if($(this).val()=="1" && $("#cek_vklaim_asalRujukan").val()==""){
             $(".fcontainer_cek_vklaim_dpjpLayanNama").hide();
         }else{
             $(".fcontainer_cek_vklaim_dpjpLayanNama").show();
         }
 
         //if($(this).val()=="2" && $("#cek_vklaim_asalRujukan").val()==""){
         //    $(".fcontainer_cek_vklaim_noRujukan").hide();
         //}else{
         //    $(".fcontainer_cek_vklaim_noRujukan").show();
         //}
 
         
         
     });
     
     cek_vklaim_icd=new TableAction("cek_vklaim_icd","registration","cek_vklaim",new Array());
     cek_vklaim_icd.setSuperCommand("cek_vklaim_icd");
     cek_vklaim_icd.select=function(json){	
         $("#cek_vklaim_diagAwal").val(json);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     cek_vklaim_poli=new TableAction("cek_vklaim_poli","registration","cek_vklaim",new Array());
     cek_vklaim_poli.setSuperCommand("cek_vklaim_poli");
     cek_vklaim_poli.select=function(kode){
         var kd = kode.split("||");
         $("#cek_vklaim_poliTujuan").val(kd[0]);
         $("#cek_vklaim_namapoliTujuan").val(kd[1]);
         $("#cek_vklaim_namapoliTujuan").trigger("change");
         $("#smis-chooser-modal").smodal("hide");
     };
 
     cek_vklaim_provinsi=new TableAction("cek_vklaim_provinsi","registration","cek_vklaim",new Array());
     cek_vklaim_provinsi.setSuperCommand("cek_vklaim_provinsi");
     cek_vklaim_provinsi.select=function(kode){
         $("#cek_vklaim_kdPropinsi").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     cek_vklaim_kabupaten=new TableAction("cek_vklaim_kabupaten","registration","cek_vklaim",new Array());
     cek_vklaim_kabupaten.setSuperCommand("cek_vklaim_kabupaten");
     cek_vklaim_kabupaten.addViewData = function(x){
         x['provinsi'] = $("#cek_vklaim_kdPropinsi").val();
         return x;
     };
     cek_vklaim_kabupaten.select=function(kode){
         $("#cek_vklaim_kdKabupaten").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     cek_vklaim_kecamatan=new TableAction("cek_vklaim_kecamatan","registration","cek_vklaim",new Array());
     cek_vklaim_kecamatan.setSuperCommand("cek_vklaim_kecamatan");
     cek_vklaim_kecamatan.addViewData = function(x){
         x['kabupaten'] = $("#cek_vklaim_kdKabupaten").val();
         return x;
     };
     cek_vklaim_kecamatan.select=function(kode){
         $("#cek_vklaim_kdKecamatan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     cek_vklaim_kecelakaan=new TableAction("cek_vklaim_kecelakaan","registration","cek_vklaim",new Array());
     cek_vklaim_kecelakaan.setSuperCommand("cek_vklaim_kecelakaan");
     cek_vklaim_kecelakaan.addViewData = function(x){
         x['nokartu'] = $("#cek_vklaim_noKartu").val();
         return x;
     };
     cek_vklaim_kecelakaan.select=function(kode){
         $("#cek_vklaim_kdKecamatan").val(kode);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     
 
     cek_vklaim_dokter=new TableAction("cek_vklaim_dokter","registration","cek_vklaim",new Array());
     cek_vklaim_dokter.setSuperCommand("cek_vklaim_dokter");
     cek_vklaim_dokter.addViewData = function(x){
         x['jenis'] = $("#cek_vklaim_jnsPelayanan").val(); //rj = 2
         x['asal'] = $("#cek_vklaim_asalRujukan").val(); // mepty string
         x['poli'] = $("#cek_vklaim_poliTujuan").val();
         if(x['jenis']=="2" && x['asal']==""){
             //paksa ke rawat inap
             x['jenis']=1;
         }
         return x;
     }
     cek_vklaim_dokter.select = function(kode){
         var vl = kode.split("||");
         $("#cek_vklaim_dpjpLayanNama").val(vl[1]);
         $("#cek_vklaim_dpjpLayan").val(vl[0]);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     cek_vklaim_dpjp=new TableAction("cek_vklaim_dpjp","registration","cek_vklaim",new Array());
     cek_vklaim_dpjp.setSuperCommand("cek_vklaim_dpjp");
     cek_vklaim_dpjp.addViewData = function(x){
         x['jenis'] = $("#cek_vklaim_jnsPelayanan").val();
         x['poli'] = $("#cek_vklaim_poliTujuan").val();
         return x;
     }
     cek_vklaim_dpjp.select = function(kode){
         var kd = kode.split("||");
         $("#cek_vklaim_kodeDPJP").val(kd[0]);
         $("#cek_vklaim_namaDPJP").val(kd[1]);
         $("#smis-chooser-modal").smodal("hide");
     };
 
     $("#cek_vklaim_namapoliTujuan").on("change",function(){
         var k = $("#cek_vklaim_poliTujuan").val();
         if(k=="HDL" || k=="JAN" || k=="IRM" || k=="MAT"){
             $(".finger").show();
             $("#hasil-finger").html("");
         }else{
             $(".finger").hide();
             $("#hasil-finger").html("");
         }
     });
 
     cek_vklaim.fingerprint = function(){
         data                = this.getRegulerData();
         data['peserta']     = cek_vklaim.get("noKartu");
         data['tgl']         = cek_vklaim.get("tglSep");
         data['command']     = "fingerprint";
         showLoading();
         $.post("",data,function(res){
             var g = getContent(res);
             $("#hasil-finger").html(g);
             dismissLoading();
         });
     }

     cek_vklaim.more_option=function(x){
        kontrol.show_add_form();
     }
 
 });