var importer_pasien;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','tanggal',"max_memory","max_time","file");
    importer_pasien=new TableAction("importer_pasien","registration","importer_pasien",column);
    importer_pasien.view();
    importer_pasien.import_proceed=function(id){
        var data=this.getRegulerData();
        data['id']=id;
        data['command']="import_proceed";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            importer_pasien.view();
            dismissLoading();
        });
    };
    
    importer_pasien.download_template=function(){
        window.location = "./registration/resource/excel/TemplatePendaftaran.xls";
    };
});