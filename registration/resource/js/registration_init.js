var REG=0;
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
    
    /* BLOCK UNTUK REGISTRASI PASIEN */
    $("#daftar_pasien_add_form").appendTo("#destination");	
	$("#search_nrm, #search_nama, #search_alamat, #search_nobpjs, #search_profnum, #search_ktp").on("keyup",function(e){
		if(e.which==13){
			 registration_patient.view();
		}
	});
	if($("#registration_patient_tgl_lahir").hasClass("mydate")){
		$('#registration_patient_tgl_lahir').datepicker().on('changeDate', function (ev) {
		    var lahir	= smis_get_data('#registration_patient_tgl_lahir');
		    var y		= Number(lahir.substring(0,4));
		    var m		= Number(lahir.substring(5,7))-1;
		    var d		= Number(lahir.substring(8,10));
		    var ldate	= new Date(y,m,d);
		    var hasil	= getAge(ldate);
		    $("#registration_patient_umur").val(hasil);
		});
	};
	
	$('#registration_patient_tgl_lahir').on('change', function (ev) {
	    var lahir	= smis_get_data('#registration_patient_tgl_lahir');
	    var y		= Number(lahir.substring(0,4));
	    var m		= Number(lahir.substring(5,7))-1;
	    var d		= Number(lahir.substring(8,10));
	    var ldate	= new Date(y,m,d);
	    var hasil	= getAge(ldate);
	    $("#registration_patient_umur").val(hasil);
	});
	
	/* BLOCK UNTUK REGISTRASI LAYANAN PASIEN */
	$("#daftar_pasien_carabayar").change(function(){
		var data = {
				page				: "registration",
				action				: "get_jenis_pasien",
				prototype_name		: "",
				prototype_slug		: "",
				prototype_implement	: "",				
				slug				: $("#daftar_pasien_carabayar").val()
		};
		showLoading();
	    $.post("",data,function(res){
	        var json=getContent(res);
	        $("#daftar_pasien_asuransi").val("");
			$("#daftar_pasien_nobpjs").val("");
			$("#daftar_pasien_nama_perusahaan").val("");
			$("#daftar_pasien_asuransi").prop("disabled",json.asuransi=="0");
			$("#daftar_pasien_nobpjs").prop("disabled",json.nobpjs=="0");
			$("#daftar_pasien_kelas_bpjs").prop("disabled",json.nobpjs=="0");
			$("#daftar_pasien_nama_perusahaan").prop("disabled",json.nama_perusahaan=="0");			
            setMoney("#daftar_pasien_plafon_bpjs",Number(json.plafon));
            daftar_pasien.fix_asuransi(0,$("#daftar_pasien_carabayar").val());
            dismissLoading();
	    });	    
	});

	$("#daftar_pasien_caradatang").change(function(){
		if(this.value=="Rujukan"){
			$("#daftar_pasien_rujukan").prop("disabled",false);
			$("#daftar_pasien_nama_rujukan").prop("disabled",false);
			$("#daftar_pasien_keteranganbonus").prop("disabled",false);
			$("#daftar_pasien_besarbonus").prop("disabled",false);
			$("#daftar_pasien_ambilbonus").prop("disabled",false);
			$("#daftar_pasien_besarambil").prop("disabled",false);
			$("#daftar_pasien_namakodebonus").prop("disabled",false);
			$("#daftar_pasien_chooser_namakodebonus").prop("disabled",false);
			$("#daftar_pasien_rujukan").attr("empty","n");
			$("#daftar_pasien_nama_rujukan").attr("empty","n");
		}else{
			$("#daftar_pasien_rujukan").val("");
			$("#daftar_pasien_nama_rujukan").val("");
			$("#daftar_pasien_rujukan").attr("empty","y");
			$("#daftar_pasien_nama_rujukan").attr("empty","y");
			$("#daftar_pasien_keteranganbonus").val("");
			$("#daftar_pasien_namakodebonus").val("");
			setMoney("#daftar_pasien_besarambil", 0);
			setMoney("#daftar_pasien_besarbonus", 0);
			$("#daftar_pasien_ambilbonus").val("");
			$("#daftar_pasien_rujukan").prop("disabled",true);
			$("#daftar_pasien_nama_rujukan").prop("disabled",true);
			$("#daftar_pasien_keteranganbonus").prop("disabled",true);
			$("#daftar_pasien_besarbonus").prop("disabled",true);
			$("#daftar_pasien_ambilbonus").prop("disabled",true);
			$("#daftar_pasien_besarambil").prop("disabled",true);
			$("#daftar_pasien_namakodebonus").prop("disabled",true);
			$("#daftar_pasien_chooser_namakodebonus").prop("disabled",true);
		}
	});
    
    $("#daftar_pasien_jenislayanan, #daftar_pasien_barulama").click(function(){
        if ($("#daftar_pasien_jenislayanan").val() == null || $("#daftar_pasien_barulama").val() == null)
            return;
        daftar_pasien.karcis();
    });
    
    $("#daftar_pasien_jenislayanan, #daftar_pasien_barulama").on("change",function(){
        if ($("#daftar_pasien_jenislayanan").val() == null || $("#daftar_pasien_barulama").val() == null)
            return;
        daftar_pasien.karcis();
    });

    
    /* BLOCK UNTUK SHORCUT */

    /** menyimpan nomor-nomor peserta yang pernah atau 
     * sudah tersimpan di sistem */
    $("#daftar_pasien_asuransi").on("click",function(){
        var data=daftar_pasien.getRegulerData();
        data['nrm_pasien']=$("#daftar_pasien_nrm").val();
        data['id_asuransi']=$("#daftar_pasien_asuransi").val();
        data['action']="get_nomor_asuransi";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            if(json!=null){
                $("#daftar_pasien_nobpjs").val(json.nomor);
            }else{
                $("#daftar_pasien_nobpjs").val("");
            }
            dismissLoading();
        });
    });
    
    $("#daftar_pasien_nobpjs").focus(function(){
        var carabayar = $("#daftar_pasien_carabayar").val().trim();
        var nrm       = $("#daftar_pasien_nrm").val();
        var nobpjs    = $("#daftar_pasien_nobpjs").val().trim();
        if(nobpjs=="" && carabayar=="bpjs"){
            var data = registration_patient.getEditData(nrm);
            showLoading();
            $.post("",data,function(res){
                var json = getContent(res);
                $("#daftar_pasien_nobpjs").val(json.nobpjs);
                dismissLoading();
            });
        }
    });
	
	
    /** ini adalah block untuk menyimpan berbagai macam shorcut agar memudahkan
     * petugas dalam mendaftarkan pasien
     * */
	shortcut.add("ctrl+b", function() {
		$("#registration_patient_add").trigger("click");
    });
	shortcut.add("ctrl+s", function() {
		if($("#registration_patient_add_form").hasClass("in")){
			$("#registration_patient_save").trigger("click");
		}else if($("#daftar_pasien_add_form").hasClass("in")){
			$("#daftar_pasien_save").trigger("click");
		}
    });
	shortcut.add("ctrl+r", function() {
		if($("#registration_patient_add_form").hasClass("in")){
			$("#registration_patient_save_reg").trigger("click");
		}
    });
	shortcut.add("ctrl+c", function() {
		if(! $("#registration_patient_add_form").hasClass("in") && !$("#daftar_pasien_add_form").hasClass("in")){
			if( $("#registration_patient_tab_anchor").parent().hasClass("active")){
				$("#search_nrm").focus();
			}else if($("#daftar_patient_tab_anchor").parent().hasClass("active")){
				$("#daftar_pasien_kriteria").focus();
			}
		}
    });
	shortcut.add("ctrl+m", function() {
		if(! $("#registration_patient_add_form").hasClass("in") && !$("#daftar_pasien_add_form").hasClass("in")){
			if( $("#registration_patient_tab_anchor").parent().hasClass("active")){
				activeTab("#daftar_patient_tab");
			}else if($("#daftar_patient_tab_anchor").parent().hasClass("active")){
				activeTab("#registration_patient_tab");
			}
		}
    });	
	shortcut.add("ctrl+h", function() {
		helptable('registration','rawat_jalan','','','');
    });
});
