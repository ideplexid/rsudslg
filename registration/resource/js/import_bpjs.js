var import_bpjs;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','tanggal',"max_memory","max_time","file");
    import_bpjs=new TableAction("import_bpjs","registration","import_bpjs",column);
    import_bpjs.view();
    import_bpjs.import_proceed=function(id){
        var data=this.getRegulerData();
        data['id']=id;
        data['command']="import_proceed";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            import_bpjs.view();
            dismissLoading();
        });
    };
    
    import_bpjs.clearall=function(){
        var data=this.getRegulerData();
        data['command']="clearall";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };
});