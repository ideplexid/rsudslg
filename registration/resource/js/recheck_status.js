/**
 * dipakai untuk recheck status pasien
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: registration/resource/php/perubahan_pasien/recheck_status.php
 * 
 * */

var recheck_status;
var IS_recheck_status_RUNNING;
$(document).ready(function(){
	$('.mydate').datepicker();
	recheck_status=new TableAction("recheck_status","registration","recheck_status",new Array());
	recheck_status.addRegulerData=function(data){
		data['dari']=$("#recheck_status_dari").val();
		data['nama_pasien']=$("#recheck_status_nama_pasien").val();
		data['nrm']=$("#recheck_status_nrm").val();
		data['sampai']=$("#recheck_status_sampai").val();		
		data['carabayar']=$("#recheck_status_carabayar").val();							
		return data;
	};

	recheck_status.batal=function(){
		IS_recheck_status_RUNNING=false;
		$("#rekap_recheck_status_modal").modal("hide");
	};
	
	recheck_status.afterview=function(json){
		if(json!=null){
			$("#kode_table_recheck_status").html(json.nomor);
			$("#waktu_table_recheck_status").html(json.waktu);
			recheck_status_data=json;
		}
	};

	recheck_status.rekaptotal=function(){
		if(IS_recheck_status_RUNNING) return;
		$("#rekap_recheck_status_bar").sload("true","Fetching total data",0);
		$("#rekap_recheck_status_modal").modal("show");
		$("#recheck_status_list").html("");
		IS_recheck_status_RUNNING=true;
		var d=this.getRegulerData();
		d['super_command']="total";
		$.post("",d,function(res){
			var all=getContent(res);
			if(all!=null) {
				var total=Number(all);
				recheck_status.rekaploop(0,total);
			} else {
				$("#rekap_recheck_status_modal").modal("hide");
				IS_recheck_status_RUNNING=false;
			}
		});
	};

	recheck_status.excel=function(){
		var d=this.getRegulerData();
		d['command']="excel";
		download(d);
	};

	recheck_status.rekaploop=function(current,total){
		if(current>=total || !IS_recheck_status_RUNNING) {
			IS_recheck_status_RUNNING=false;
			$("#rekap_recheck_status_modal").modal("hide");
			return;
		}
		var d=this.getRegulerData();
		d['super_command']="limit";
		d['halaman']=current;
		$.post("",d,function(res){
			var ct=getContent(res);
			$("#recheck_status_list").append(ct.content);
			$("#rekap_recheck_status_bar").sload("true"," Processing ... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			if(ct.status=="1"){
				setTimeout(function(){recheck_status.rekaploop(current,--total)},300);
			}else{
				setTimeout(function(){recheck_status.rekaploop(++current,total)},300);
			}
			
		});
	};
	
	
	
	
});