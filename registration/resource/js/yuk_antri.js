var yuk_antri_cur_content = null;
$(document).ready(function(){
    yuk_antri.register = function(id){
        var data = this.getRegulerData();
        data['action'] = "yuk_antri_register";
        data['id'] = id;
        
        showLoading();
        $.post("",data,function(res){
            yuk_antri_cur_content = getContent(res);
            yuk_antri.view();
            dismissLoading();
        });
    };
    yuk_antri.register_nrm = function(id){
        $("#registration_patient_tab_anchor").trigger("click");
        registration_patient.show_add_form();
        $("#registration_patient_nama").val(yuk_antri_cur_content.yka_nama);
        $("#registration_patient_telpon").val(yuk_antri_cur_content.yka_telp);
        $("#registration_patient_nobpjs").val(yuk_antri_cur_content.yka_no_bpjs);
        $("#registration_patient_ktp").val(yuk_antri_cur_content.yka_ktp);
    };
    yuk_antri.register_noreg = function(id,ruangan,carabayar){
        $("#warning").smodal("hide");
        registration_patient.register_ruangan(id,ruangan,carabayar);

    };
    yuk_antri.register_bpjs = function(id){
        $("#warning").smodal("hide");
        registration_patient.vclaim_bpjs(id);
    };
    yuk_antri.crawler = function(){
        var data = this.getRegulerData();
        data['action'] = "yuk_antri_crawler";
        showLoading();
        $.post("",data,function(res){
            var json = getContent(res);
            yuk_antri.view();
            dismissLoading();
        });
    };
});