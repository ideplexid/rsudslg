var DUS_KODE_KABUPATEN=0;
var DUS_KODE_KECAMATAN=0;
var DUS_KODE_KELURAHAN=0;
$(document).ready(function(){
	$("#kedusunan_no_prop").on("change",function(){
		var data=kedusunan.getRegulerData();
		data['action']="selector_kabupaten";
		data['id_propinsi']=$("#kedusunan_no_prop").val();
		data['code']="view";
		$.post("",data,function(res){
			var json=getContent(res);
			$("#kedusunan_no_kab").html(json);
			$("#kedusunan_no_kab").val(DUS_KODE_KABUPATEN);
			$("#kedusunan_no_kab").trigger("change");			
		});
	});
	
	$("#kedusunan_no_kab").on("change",function(){
		var data=kedusunan.getRegulerData();
		data['action']="selector_kecamatan";
		data['id_kabupaten']=$("#kedusunan_no_kab").val();
		data['code']="view";
		$.post("",data,function(res){
			var json=getContent(res);
			$("#kedusunan_no_kec").html(json);
            $("#kedusunan_no_kec").val(DUS_KODE_KECAMATAN);
            $("#kedusunan_no_kec").trigger("change");
		});
    });
    
    $("#kedusunan_no_kec").on("change",function(){
		var data=kedusunan.getRegulerData();
		data['action']="selector_kelurahan";
		data['id_kecamatan']=$("#kedusunan_no_kec").val();
		data['code']="view";
		$.post("",data,function(res){
			var json=getContent(res);
			$("#kedusunan_no_kel").html(json);
			$("#kedusunan_no_kel").val(DUS_KODE_KELURAHAN);
		});
	});
	
	
	kedusunan.edit=function (id){
		var self=this;
		showLoading();	
		var edit_data=this.getEditData(id);
		$.post('',edit_data,function(res){		
			var json=getContent(res);
			if(json==null) return;
			for(var i=0;i<self.column.length;i++){
				if($.inArray(self.column[i],self.noclear)!=-1 && !self.edit_clear_for_no_clear){
					continue;
				}
					
				var name=self.column[i];
				var the_id="#"+self.prefix+"_"+name;
				smis_edit(the_id,json[""+name]);
			}
			dismissLoading();
			self.disabledOnEdit(self.column_disabled_on_edit);
			self.show_form();
			DUS_KODE_KABUPATEN=json.no_kab;
			DUS_KODE_KECAMATAN=json.no_kec;
			$("#kedusunan_no_prop").trigger("change");			
		});
	};
	
});