var koreksi_dokter;
var koreksi_dokter_karyawan;
var koreksi_dokter_data;
var IS_koreksi_dokter_RUNNING;
$(document).ready(function(){
    $('.mydate').datepicker();
    koreksi_dokter=new TableAction("koreksi_dokter","registration","koreksi_dokter",new Array());
    koreksi_dokter.addRegulerData=function(data){
        data['dari']=$("#koreksi_dokter_dari").val();
        data['sampai']=$("#koreksi_dokter_sampai").val();
        $("#dari_table_koreksi_dokter").html(getFormattedDate(data['dari']));
        $("#sampai_table_koreksi_dokter").html(getFormattedDate(data['sampai']));			
        return data;
    };

    koreksi_dokter.batal=function(){
        IS_koreksi_dokter_RUNNING=false;
        $("#koreksi_dokter_modal").smodal("hide");
    };
    
    koreksi_dokter.afterview=function(json){
        if(json!=null){
            $("#kode_table_koreksi_dokter").html(json.nomor);
            $("#waktu_table_koreksi_dokter").html(json.waktu);
            koreksi_dokter_data=json;
        }
    };

    koreksi_dokter.rekaptotal=function(){
        if(IS_koreksi_dokter_RUNNING) return;
        $("#koreksi_dokter_bar").sload("true","Fetching total data",0);
        $("#koreksi_dokter_modal").smodal("show");
        IS_koreksi_dokter_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var total=Number(getContent(res));
            if(total>0) {
                koreksi_dokter.rekaploop(0,total);
            } else {
                $("#koreksi_dokter_modal").smodal("hide");
                IS_koreksi_dokter_RUNNING=false;
            }
        });
    };

    koreksi_dokter.rekaploop=function(current,total){
        $("#koreksi_dokter_bar").sload("true","Processing... [ "+current+" / "+total+" ] ",(current*100/total));
        if(current>=total || !IS_koreksi_dokter_RUNNING) {
            $("#koreksi_dokter_modal").smodal("hide");
            IS_koreksi_dokter_RUNNING=false;
            return;
        }
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['limit_start']=current;			
        $.post("",d,function(res){
            var ct=getContent(res);
            $("#koreksi_dokter_list").append(ct);
            setTimeout(function(){koreksi_dokter.rekaploop(++current,total)},3000);
        });
    };
            
});