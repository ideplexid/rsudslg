$(document).ready(function(res){
	
	replace_pasien_patient.selected=function(json){
		$("#replace_pasien_nrm_pasien").val(json.id);
		$("#replace_pasien_nama_pasien").val(json.nama);
		$("#replace_pasien_noreg_pasien").val(json.noreg_pasien);
	}
	
	replace_pasien_layanan.selected=function(json){
		$("#replace_pasien_nrm_pasien").val(json.nrm);
		$("#replace_pasien_nama_pasien").val(json.nama_pasien);
		$("#replace_pasien_noreg_pasien").val("["+json.id+"]");
	}
	
	replace_pasien.proceed=function(){
			var a=this.getRegulerData();
			a['command']="save";
			a['nrm_pasien']=$("#replace_pasien_nrm_pasien").val();
			a['nama_pasien']=$("#replace_pasien_nama_pasien").val();
			a['noreg_pasien']=$("#replace_pasien_noreg_pasien").val();
			a['nrm_pasien_baru']=$("#replace_pasien_nrm_pasien_baru").val();
			a['nama_pasien_baru']=$("#replace_pasien_nama_pasien_baru").val();
			
			if(a['noreg_pasien']=="" || a['noreg_pasien']=="[]" || a['nrm_pasien_baru']==""){
				bootbox.alert("Pastikan Pasien Lama , Noreg dan Pasien Baru Terisi ");
				return;
			}
			
			bootbox.confirm("Anda Yakin, Memproses Data Berikut ini.  "+
			"<table>"+
				"<tr><td>Nomor Reg			</td> <td> : </td> <td><strong>"+a['noreg_pasien']+			"</strong></td></tr>"+
				"<tr><td>Nama  Pasien Asal  </td> <td> : </td> <td><strong>"+a['nrm_pasien']+			"</strong></td></tr>"+
				"<tr><td>NRM   Pasien Asal  </td> <td> : </td> <td><strong>"+a['nama_pasien']+			"</strong></td></tr>"+
				"<tr><td>Nama  Pasien Baru  </td> <td> : </td> <td><strong>"+a['nrm_pasien_baru']+		"</strong></td></tr>"+
				"<tr><td>NRM   Pasien Baru  </td> <td> : </td> <td><strong>"+a['nama_pasien_baru']+		"</strong></td></tr>"+
			"</table>"
			,function(result){
				if(result){
					showLoading();
					$.post("",a,function(res){
						$("#replace_pasien_nrm_pasien").val("");
						$("#replace_pasien_nama_pasien").val("");
						$("#replace_pasien_noreg_pasien").val("");
						$("#replace_pasien_nrm_pasien_baru").val("");
						$("#replace_pasien_nama_pasien_baru").val("");
						replace_pasien.view();
						dismissLoading();
					});
				}
			});
			
			
	} 
	
	replace_pasien.restore = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		showLoading();
		$.post(
			"", 
			data, 
			function(response) {
			var json = getContent(response);
			dismissLoading();
			bootbox.confirm(
				"Anda Yakin, Me-Restore Data Berikut ini.  " +
				"<table>" +
					"<tr><td>Nomor Reg			</td> <td> : </td> <td><strong>" + json.noreg_pasien + "</strong></td></tr>" +
					"<tr><td>Nama  Pasien Asal  </td> <td> : </td> <td><strong>" + json.nama_pasien_baru + "</strong></td></tr>" +
					"<tr><td>NRM   Pasien Asal  </td> <td> : </td> <td><strong>" + json.nrm_pasien_baru + "</strong></td></tr>" +
					"<tr><td>Nama  Pasien Baru  </td> <td> : </td> <td><strong>" + json.nama_pasien + "</strong></td></tr>" +
					"<tr><td>NRM   Pasien Baru  </td> <td> : </td> <td><strong>" + json.nrm_pasien + "</strong></td></tr>" +
				"</table>", 
				function(result) {
					if (result) {
						showLoading();
						var data_restore = self.getRegulerData();
						data_restore['command'] = "save";
						data_restore['noreg_pasien'] = json.noreg_pasien;
						data_restore['nama_pasien'] = json.nama_pasien_baru;
						data_restore['nrm_pasien'] = json.nrm_pasien_baru;
						data_restore['nama_pasien_baru'] = json.nama_pasien;
						data_restore['nrm_pasien_baru'] = json.nrm_pasien;
						$.post(
							"",
							data_restore,
							function(restore_response) {
								replace_pasien.view();
								dismissLoading();
							}
						);
					}
				}
			);
		});
	} 
	
	replace_pasien.view();
	
});