<?php 
global $wpdb;
$query="CREATE OR REPLACE VIEW smis_rl51 as  SELECT '' as id, YEAR(tanggal) as tahun,
	MONTH(tanggal) as bulan,
	sum(if(barulama=0,1,0)) as baru,
	sum(if(barulama=1,1,0)) as lama,
	count(*) as total,
	'' as prop
	FROM `smis_rg_layananpasien`
	WHERE prop!='del'
	GROUP BY YEAR(tanggal),MONTH(tanggal) ;";
	$wpdb->query ( $query );
?>