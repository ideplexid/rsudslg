<?php 
global $wpdb;
$query = "CREATE OR REPLACE VIEW smis_rgv_duplicate  AS  
        select a.*,
        b.id AS dnrm from 
        (smis_rg_patient a join smis_rg_patient b) 
        where ((a.tanggal = b.tanggal) 
        and (a.nama = b.nama) 
        and (a.alamat = b.alamat) 
        and (a.tempat_lahir = b.tempat_lahir) 
        and (a.tgl_lahir = b.tgl_lahir) 
        and (a.kelamin = b.kelamin) 
        and (a.rt = b.rt) 
        and (a.rw = b.rw) 
        and (a.provinsi = b.provinsi) 
        and (a.kabupaten = b.kabupaten) 
        and (a.kecamatan = b.kecamatan) 
        and (a.kelurahan = b.kelurahan) 
        and (a.suami = b.suami) 
        and (a.istri = b.istri) 
        and (a.ayah = b.ayah) 
        and (a.ibu = b.ibu) 
        and (b.id < a.id)) ;";
$wpdb->query($query);

?>