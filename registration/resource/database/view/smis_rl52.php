<?php 
global $wpdb;
$query="
    CREATE OR REPLACE VIEW smis_rl52  AS  
    select '' AS id,year(smis_rg_layananpasien.tanggal) AS tahun,
    month(smis_rg_layananpasien.tanggal) AS bulan,
    smis_rg_layananpasien.jenislayanan AS layanan,
    sum(if((smis_rg_layananpasien.barulama = 0),1,0)) AS baru,
    sum(if((smis_rg_layananpasien.barulama = 1),1,0)) AS lama,
    count(0) AS total,'' AS prop from smis_rg_layananpasien 
    where (smis_rg_layananpasien.prop != 'del') 
    group by year(smis_rg_layananpasien.tanggal),
    month(smis_rg_layananpasien.tanggal) ;
";
$wpdb->query ( $query );

?>