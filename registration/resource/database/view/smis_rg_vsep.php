<?php
global $wpdb;
$query = "create or replace view smis_rg_vsep as 
         SELECT smis_rg_sep.id, smis_rg_sep.prop, smis_rg_patient.nama, smis_rg_patient.kelamin, smis_rg_sep.noSEP,smis_rg_sep.noKartu, smis_rg_sep.tglSep  
         FROM smis_rg_sep LEFT JOIN smis_rg_patient ON smis_rg_sep.noMr=smis_rg_patient.id WHERE smis_rg_sep.tglPulang='0000-00-00' ORDER BY smis_rg_sep.id DESC;";
$wpdb->query ( $query );
?>