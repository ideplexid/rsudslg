<?php 
global $wpdb;
$query="create or replace view  smis_rgvj_layananpasien as 
    SELECT id,no_urut,no_kunjungan,
	barulama,tanggal,nrm,jenislayanan,
	karcis,lunas,carabayar,caradatang,
	if(gratis=1,'Tidak Bayar',carapulang) as carapulang,
	jenispasien, rujukan,nama_rujukan,id_rujukan,
	nama_perusahaan,asuransi,nobpjs,plafon_bpjs,
	diagnosa_bpjs,tindakan_bpjs,
	keterangan_bpjs,kunci_bpjs,keteranganbonus,
	besarbonus,ambilbonus,besarambil,kodebonus,
	namakodebonus,namapenanggungjawab,telponpenanggungjawab,
	selesai,umur,gol_umur,uri,id_dokter,
	nama_dokter,kamar_inap,administrasi_inap,tanggal_inap,
	tanggal_pulang,status_dokumen,prop
	FROM smis_rg_layananpasien ";
	$wpdb->query ( $query );
?>