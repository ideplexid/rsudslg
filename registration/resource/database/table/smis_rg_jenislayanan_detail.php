<?php 
global $wpdb;
require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_rg_jenislayanan_detail");
$dbcreator->addColumn("ruangan","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("waktu","timestamp",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_CURRENT_TIMESTAMP,false,DBCreator::$ON_UPDATE_CURRENT_TIMESTAMP,"");
$dbcreator->addColumn("id_rg_jenislayanan","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->initialize();
?>

