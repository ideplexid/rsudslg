<?php 
global $wpdb;
require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_rg_kec");
$dbcreator->addColumn("nama","varchar(50)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("no_kab","varchar(200)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("no_prop","varchar(10)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->initialize();
?>