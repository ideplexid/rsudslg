<?php 
global $wpdb;
require_once "smis-libs-class/DBCreator.php";
$dbcreator = new DBCreator($wpdb,"smis_rg_sep_rujukan");
$dbcreator->addColumn("nosep","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("nama_pasien","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("jk","tinyint(1)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("no_bpjs","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("no_rujukan","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("tanggal","date",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("ppk","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("jenis","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("tipe","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("poli","varchar(16)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("nama_poli","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("catatan","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("diagnosa","varchar(8)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("nama_diagnosa","varchar(128)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("nama_ppk","varchar(128)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("id_pasien","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->initialize();
?>