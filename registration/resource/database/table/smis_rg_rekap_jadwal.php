<?php 
global $wpdb;
require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smis_rg_rekap_jadwal");
$dbcreator->addColumn("tanggal","date",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("kelompok","varchar(1)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("id_dokter","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("jumlah","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("carabayar","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("hari","varchar(8)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->initialize();
?>