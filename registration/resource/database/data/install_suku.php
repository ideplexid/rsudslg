<?php 
global $wpdb;


$query="INSERT INTO `smis_rg_suku` (`id`, `nama`, `propinsi`, `prop`) VALUES
(1, 'Aceh', 'Indonesia - Aceh ', ''),
(2, 'Batak', 'Indonesia - Sumatra Utara', ''),
(3, 'Minangkabau', 'Indonesia - Sumatra Barat', ''),
(4, 'Akit', 'Indonesia - Riau', ''),
(5, 'Sakai', 'Indonesia - Kepulauan Riau', ''),
(6, 'Kerinci', 'Indonesia - Jambi', ''),
(7, 'Musi', 'Indonesia - Sumatra Selatan', ''),
(8, 'Melayu', 'Indonesia - Bangka Belitung', ''),
(9, 'Serawai', 'Indonesia - Bengkulu', ''),
(10, 'Lampung', 'Indonesia - Lampung', ''),
(11, 'Betawi', 'Indonesia - DKI Jakarta', ''),
(12, 'Sunda', 'Indonesia - Jawa Barat', ''),
(13, 'Baduy', 'Indonesia - Banten', ''),
(14, 'Samin', 'Indonesia - Jawa Tengah', ''),
(15, 'Jawa', 'Indonesia - Jawa', ''),
(16, 'Madura', 'Indonesia - Jawa Timur', ''),
(17, 'Bali Aga', 'Indonesia - Bali', ''),
(18, 'Sasak', 'Indonesia - Nusa Tenggara Barat', ''),
(19, 'Bima', 'Indonesia - Nusa Tenggara Timur', ''),
(20, 'Dayak', 'Indonesia - Kalimantan Barat', ''),
(21, 'Ot Danum', 'Indonesia - Kalimantan Tengah', ''),
(22, 'Banjar', 'Indonesia - Kalimantan Selatan', ''),
(23, 'Tidung', 'Indonesia - Kalimantan Timur', ''),
(24, 'Bulungan', 'Indonesia - Kalimantan Utara', ''),
(25, 'Minahasa', 'Indonesia - Sulawesi Utara', ''),
(26, 'Toraja', 'Indonesia - Sulawesi Barat', ''),
(27, 'Mori', 'Indonesia - Sulawesi Tengah', ''),
(28, 'Buton', 'Indonesia - Sulawesi Tenggara', ''),
(29, 'Bugis', 'Indonesia - Sulawesi Selatan', ''),
(30, 'Gorontalo', 'Indonesia - Gorontalo', ''),
(31, 'Buru', 'Indonesia - Maluku', ''),
(32, 'Togutil', 'Indonesia - Maluku Utara', ''),
(33, 'Dani', 'Indonesia - Papua Barat', ''),
(34, 'Asmat', 'Indonesia - Papua', ''),
(35, 'Mandarin', 'China - WNA', ''),
(36, 'Negro', 'WNA', '');";
$wpdb->query ( $query );

?>