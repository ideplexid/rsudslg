<?php
    global $db;
    $head=array ("No.",'Tanggal','Sheets','Max Memory','Max Time','File');
    $uitable = new Table ( $head, "Import BPJS", NULL, true );
    $uitable->setName ( "import_bpjs" );
    $uitable->setReloadButtonEnable(false);
    
    $btn=new Button("","","");
    $btn->setIsButton(Button::$ICONIC);
    $btn->setIcon("fa fa-file-excel-o");
    $btn->setClass(" btn btn-inverse");
    $uitable->addContentButton("import_proceed",$btn);
    
    $btn=new Button("","","");
    $btn->setIsButton(Button::$ICONIC);
    $btn->setIcon("fa fa-trash");
    $btn->setClass(" btn btn-danger");
    $btn->setAction(" import_bpjs.clearall()");
    $uitable->addHeaderButton($btn);
    if (isset ( $_POST ['command'] )) {
        require_once "smis-libs-class/ExcelImporter.php";
        require_once "registration/class/responder/ExcelImporterResponder.php";
        $adapter = new SimpleAdapter ();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add ( "File","file" );
        $adapter->add ( "Tanggal","tanggal","date d M Y H:i" );
        $adapter->add ( "Sheets","sheets" );
        $adapter->add ( "Max Memory","max_memory" );
        $adapter->add ( "Max Time","max_time" );
        
        $dbtable = new DBTable ( $db, "smis_rg_import_bpjs" );
        
        $dbres = new ExcelImporterResponder ( $dbtable, $uitable, $adapter );
        if($dbres->isSave() || $dbres->is("import_proceed")){
            $importer=new ExcelImporter($db,new DBTable($db,"smis_rg_excel_bpjs"),"");
            $dbres->setImporter($importer);
        }
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    }
    
    $uitable->addModal("id", "hidden", "", "");
    $uitable->addModal("tanggal", "datetime", "Tanggal", date("Y-m-d H:i:s"));
    $uitable->addModal("max_memory", "text", "Max Memory", "150M");
    $uitable->addModal("max_time", "text", "Max Time", "3600000");
    $uitable->addModal("file", "file-single-document", "File", "");
    echo $uitable->getHtml ();
    echo $uitable->getModal()->setTitle("Import BPJS")->getHtml();
    echo addJS ( "framework/smis/js/table_action.js" );
    echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
    echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    echo addJS ( "registration/resource/js/import_bpjs.js",false );
    
?>

