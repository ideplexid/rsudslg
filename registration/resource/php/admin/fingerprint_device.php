<?php 
require_once "smis-libs-class/MasterTemplate.php";
global $db;
$mas=new MasterTemplate($db,"smis_rg_fingerprint","registration","fingerprint_device");
$uitable=$mas->getUItable();
$uitable->addModal("id","hidden","","");
$uitable->addModal("device_name","text","Device Name","");
$uitable->addModal("serial_number","text","Serial Number","");
$uitable->addModal("verivication_code","text","Verivication Code","");
$uitable->addModal("activation_code","text","Activation Code","");
$uitable->addModal("vkey","text","VKey","");
$uitable->setHeader(array("No.","Nama","Serial Number","Verivication Code","Activation Code","VKey"));
$adapter=$mas->getAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Nama","device_name");
$adapter->add("Serial Number","serial_number");
$adapter->add("Verivication Code","verivication_code");
$adapter->add("Activation Code","activation_code");
$adapter->add("VKey","vkey");
$mas->setModalTitle("Fingerprint Device");
$mas->setModalComponentSize(Modal::$MEDIUM);
$mas->initialize();
?>