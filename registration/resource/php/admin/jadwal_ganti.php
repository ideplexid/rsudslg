<?php
global $db;

$dbtable=new DBTable($db,"smis_rg_jadwal");
$uitable=new Table(array("Kode","Dokter I","Dokter II"));
$uitable->setName("dokter_jadwal_ganti");
$uitable->setModel(Table::$SELECT);
$adapter=new SimpleAdapter();
$adapter->add("Kode","kode");
$adapter->add("Dokter I","nama_dokter_satu");
$adapter->add("Dokter II","nama_dokter_dua");
$dbres=new DBResponder($dbtable,$uitable,$adapter);
$super=new SuperCommand();
$super->addResponder("dokter_jadwal_ganti",$dbres);
$data=$super->initialize();
if($data!=null){
    echo $data;
    return;
}


$head=array ("No.",'Tanggal','Shift','Kode',"ID Dokter","Nama Dokter");
$uitable = new Table ( $head, "Jadwal Ganti", NULL, true );
$uitable->setName ( "jadwal_ganti" );

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "Nama", "nama_pasien" );
	$adapter->add ( "Tanggal", "tanggal","date d M Y" );
	$adapter->add ( "Shift", "shift");
	$adapter->add ( "Kode", "kode" );
    $adapter->add ( "ID Dokter", "id_dokter");
	$adapter->add ( "Nama Dokter", "nama_dokter");
	
	$dbtable = new DBTable ( $db, "smis_rg_dokter_ganti" );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$shift=new OptionBuilder();
$shift->add("Pagi","P");
$shift->add("Siang","S");
$shift->add("Malam","M");


$uitable->addModal("id", "hidden", "", "");
$uitable->addModal("tanggal", "date", "Tanggal", "");
$uitable->addModal("shift", "select", "Shift", $shift->getContent());
$uitable->addModal("kode", "chooser-jadwal_ganti-dokter_jadwal_ganti-Pilih Kode", "Kode","");
$uitable->addModal("id_dokter", "hidden", "","");
$uitable->addModal("nama_dokter", "text", "Dokter","","n",null,true);
echo $uitable->getModal()->setTitle("Jadwal Ganti")->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "registration/resource/js/jadwal_ganti.js",false );
?>
