<?php
/**
 * this used for viewing the history of the patient 
 * that come to the Hospital
 * 
 * @version     : 5.0.0
 * @since       : 14 May 2015
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv3
 * @database    : smis_rg_layananpasien
 *                smis_rgv_layananpasien
 * */
global $db;
require_once "registration/class/table/LapRegisterTable.php";
$head=array ('Masuk','Tgl Pulang','Inap','Nama','NRM',"No Reg",'Ruang',"Kamar Inap",'Jenis Pasien',"Asuransi",'Status','Cara Pulang','OBS','OPRJ',"OPRI","HBI","OPP");
$uitable = new LapRegisterTable($head, "", NULL, true);
$uitable->setName("lap_register");
$uitable->setAddButtonEnable(false);
$uitable->setDelButtonEnable(false);
$uitable->setPrintButtonEnable(false);

$button = new Button("","","List Inap");
$button ->setIcon("fa fa-bed");
$button ->setClass("btn-success");
$button ->setIsButton(Button::$ICONIC_TEXT);
$uitable ->addContentButton("print_preview",$button);
if (isset($_POST ['command'] )) {
    require_once "registration/class/responder/RiwayatPasienResponder.php";
	$adapter = new SimpleAdapter ();
	$adapter ->add("Nama", "nama_pasien")
			 ->add("No Reg", "id","digit10")
			 ->add("Masuk", "tanggal","date d M Y H:i:s")
			 ->add("Tgl Pulang", "tanggal_pulang","date d M Y H:i:s")
			 ->add("Inap", "tanggal_inap","date d M Y H:i:s")
			 ->add("NRM", "nrm","digit8")
			 ->add("Ruang", "jenislayanan","unslug")
			 ->add("Jenis Pasien", "carabayar","unslug")
			 ->add("Asuransi", "nama_asuransi")
			 ->add("Cara Pulang", "carapulang","unslug")
			 ->add("Kamar Inap", "kamar_inap","unslug")
			 ->add("OBS", "obs")
			 ->add("OPRJ", "oprj")
			 ->add("OPRI", "opri")
			 ->add("OPP", "opp")
			 ->add("HBI", "hbi")
			 ->add("Status", "selesai","trivial_0_Aktif_Non Aktif");
	
	$dbtable = new DBTable($db, "smis_rg_layananpasien");
	$dbtable->setPreferredView(true,"smis_rgv_layananpasien");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
    $dbres = new RiwayatPasienResponder($dbtable, $uitable, $adapter);
    if($dbres->isView()){
        if($_POST['nrm_pasien']!="") $dbtable->addCustomKriteria(" nrm ","='".$_POST['nrm_pasien']."'");
        if($_POST['nama_pasien']!="") $dbtable->addCustomKriteria(" nama_pasien "," LIKE '%".$_POST['nama_pasien']."%'");
        if($_POST['noreg_pasien']!="") $dbtable->addCustomKriteria(" id "," = '".$_POST['noreg_pasien']."'");
    }
	$data = $dbres->command($_POST ['command']);
	echo json_encode($data);
	return;
}
$button=new Button("","","Cari");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon(" icon-white icon-search");
$button->setAction("lap_register.view();");
$button->setClass(" btn-primary");

$uitable->addModal("nama_pasien","text","Nama Pasien","");
$uitable->addModal("nrm_pasien","text","NRM Pasien","");
$uitable->addModal("noreg_pasien","text","Noreg Pasien","");
$form=$uitable->getModal()->getForm();
$form->setTitle("Riwayat Pasien");
$form->addElement("",$button);
$uitable->clearContent();

$selesai = new OptionBuilder();
$selesai ->add("Aktif","0");
$selesai ->add("Non - Aktif","1");

loadLibrary("smis-libs-function-medical");
$uitable->addModal("id", "hidden", "", "");
$uitable->addModal("selesai", "select", "Status Aktif", $selesai->getContent());
$uitable->addModal("carapulang", "select", "Cara Pulang", medical_carapulang());
$uitable->addModal("uri", "checkbox", "Rawat Inap", "");
$uitable->addModal("tanggal", "datetime", "Tanggal", "");
$uitable->addModal("tanggal_inap", "datetime", "Tanggal Inap", "");
$uitable->addModal("tanggal_pulang", "datetime", "Tanggal Pulang", "");
$uitable->setHelpButtonEnabled(true,"registration","lap_register");
echo $uitable->getModal()->setTitle("Di Pulangkan")->getHtml();
echo $form->getHtml();
echo $uitable->getHtml ();
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS("registration/resource/js/lap_register.js",false);