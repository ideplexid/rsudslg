<?php 
global $db;
global $user;
loadLibrary("smis-libs-function-time");
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$dari     = $_POST['dari'];
    $sampai   = $_POST['sampai'];
    $hasil=createDateRangeArray($dari,$sampai);
    $result=array();
    $result["data"]=&$hasil;
    $result["total"]=count($hasil);
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    $pack->setContent($result);
    echo json_encode($pack->getPackage());
	return;
}

$header = array('No.',"Tanggal","Kelompok",'ID Dokter','Jumlah','Carabayar');
$uitable= new Table($header);
$uitable->setName("rekap_jadwal_dokter")
		->setActionEnable(false)
		->setFooterVisible(true);

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
    require_once "registration/function/get_jadwal.php";
    $tanggal=$_POST['tanggal'];
    $date=date_create($tanggal);
    $day=date_format($date, 'l');
    $dayname=dayEnId($day);
    
    $dbtable=new DBTable($db,"smis_rg_rekap_jadwal");
    $dbtable_jadwal=new DBTable($db,"smis_rg_jadwal");
    $dbtable_ganti=new DBTable($db,"smis_rg_dokter_ganti");
    
    proceedJadwal($db,$dbtable,$dbtable_jadwal,$dbtable_ganti,$dayname,$tanggal,"P");
    proceedJadwal($db,$dbtable,$dbtable_jadwal,$dbtable_ganti,$dayname,$tanggal,"S");
    proceedJadwal($db,$dbtable,$dbtable_jadwal,$dbtable_ganti,$dayname,$tanggal,"M");
    
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    $pack->setContent("");
    echo json_encode($pack->getPackage());
	return;
}

if(isset($_POST['command']) && $_POST['command']!=""){
    $adapter=new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Tanggal","tanggal","date d M Y");
    $adapter->add("Kelompok","kelompok");
    $adapter->add("ID Dokter","id_dokter");
    $adapter->add("Jumlah","jumlah");
    $adapter->add("Carabayar","carabayar","unslug");
    
    $dbtable=new DBTable($db,"smis_rg_rekap_jadwal");
    $responder=new DBResponder($dbtable,$uitable,$adapter);
    $data=$responder->command($_POST['command']);
    echo json_encode($data);
    return;
}

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("rekap_jadwal_dokter.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("registration")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("rekap_jadwal_dokter.batal()");

$load=new LoadingBar("rekap_jadwal_dokter_bar", "");
$modal=new Modal("rekap_jadwal_dokter_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_rekap_jadwal_dokter'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("registration/resource/js/rekap_jadwal_dokter.js",false);
?>