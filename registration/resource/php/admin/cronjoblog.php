<?php 
require_once "smis-libs-class/MasterTemplate.php";
global $db;
if ($_POST ['command'] == "clean") {
	$db->query ( "truncate smis_rg_cronjob;" );
	return "";
}
$mas=new MasterTemplate($db,"smis_rg_cronjob","registration","cronjoblog");
$uitable=$mas->getUItable();
$uitable->setDelButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDetailButtonEnable(true);

$button = new Button ( "", "", "" );
$button->setAction ( "cronjoblog.clean()" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon("fa fa-trash");
$button->setClass("btn-primary");
$uitable->addFooterButton ( $button );

$uitable->setHeader(array("No.","Waktu","Last Data","Run Time","Run","Next Run","Compare"));
$adapter=$mas->getAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Waktu","waktu");
$adapter->add("Last Data","tgl_last");
$adapter->add("Run Time","jam_last");
$adapter->add("Run Log","run");
$adapter->add("Compare","compare","trivial_1_<i class='fa fa-check'></i>_");
$adapter->add("Next Run","nextrun");
$mas->addResouce("js","registration/resource/js/cronjoblog.js");
$mas->initialize();
?>