<?php 
global $db;
global $user;
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$dbtable=new DBTable($db, "smis_rg_dokter_utama");
	$dbtable->addCustomKriteria(NULL, "mulai>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "mulai<'".$_POST['sampai']."'");
	$dbtable->addCustomKriteria(NULL, "kode='' ");
    $dbtable->addCustomKriteria(NULL, "nama_pasien='' ");
	$query=$dbtable->getQueryCount("");
	$content=$db->get_var($query);
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array('No.',"NRM","Nama",'Kode','Mulai','Selesai',"Petugas","Keterangan");
$uitable=new Table($header);
$uitable->setName("koreksi_dokter")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
    require_once "registration/function/get_jadwal_dokter.php";
	$dbtable=new DBTable($db, "smis_rg_dokter_utama");
	$dbtable->addCustomKriteria(NULL, "mulai>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "mulai<'".$_POST['sampai']."'");
	$dbtable->addCustomKriteria(NULL, "kode='' ");
    $dbtable->addCustomKriteria(NULL, "nama_pasien='' ");
	$dbtable->setMaximum(1);
	$limit_start=$_POST['limit_start'];
	$data=$dbtable->view("", "0");
	$jadwal=$data['data'][0];
	$tanggal=substr($jadwal->mulai,0,10);
    $tahun=substr($tanggal,0,4)*1+1;
    $next_year=$tahun.substr($tanggal,4,6);
    
    $pxtable=new DBTable($db,"smis_rg_patient");
    $px=$pxtable->select(array("id"=>$jadwal->nrm_pasien));
    
	$up['id']=$jadwal->id;
	$dt['selesai']=$next_year;
    $dt['nama_pasien']=$px->nama;
    $dt['kode']=getJadwalDokter($db,$jadwal->mulai);
	$dt['petugas']=$user->getNameOnly();
    $dt['keterangan']="Dikoreksi melalui menu koreksi dokter";
	$dbtable->update($dt, $up);	
    $dt['mulai']=$jadwal->mulai;
    $dt['nrm_pasien']=$jadwal->nrm_pasien;
    
	$simple=new SimpleAdapter();
    $simple->setUseNumber(true,"No.","back.");
    $simple->setNumber($limit_start);
	$simple->add("Kode","kode");
	$simple->add("Mulai","mulai","date d M Y H:i");
	$simple->add("Selesai","selesai","date d M Y");
	$simple->add("Petugas","petugas");
	$simple->add("Keterangan","keterangan");
    $simple->add("Nama","nama_pasien");
    $simple->add("NRM","nrm_pasien","only-digit8");
	$content=$simple->getContent(array($dt) );
	$uitable->setContent($content);
	$bdcontent=$uitable->getBodyContent();	
	

	
	$pack=new ResponsePackage();
	$pack->setContent($bdcontent);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
	
}

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("koreksi_dokter.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("registration")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("koreksi_dokter.batal()");

$load=new LoadingBar("koreksi_dokter_bar", "");
$modal=new Modal("koreksi_dokter_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_koreksi_dokter'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("registration/resource/js/koreksi_dokter.js",false);
?>