<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";
$master = new MasterTemplate($db,"smis_rg_patient","registration","duplicate_pasien");
$master ->getAdapter()
        ->add("Nama","nama")
        ->add("Alamat","alamat")
        ->add("Tempat Lahir","tempat_lahir")
        ->add("Tanggal Lahir","tgl_lahir","date d M Y")
        ->add("Kelamin","kelamin","trivial_0_L_P")
        ->add("RT","rt")
        ->add("RW","rw")
        ->add("Provinsi","provinsi")
        ->add("Kabupaten","kabupaten")
        ->add("Kecamatan","kecamatan")
        ->add("Kelurahan","kelurahan")
        ->add("Suami","suami")
        ->add("Istri","istri")
        ->add("Ayah","ayah")
        ->add("Ibu","ibu")
        ->add("NRM","id","only-digit8")
        ->add("NRMD","dnrm","only-digit8");
$master ->getDBtable()
        ->setPreferredView(true,"smis_rgv_duplicate",array())
        ->setRealDelete(true);
$master ->getUItable()->setHeader(array("NRM","NRMD","Nama","Alamat","Tempat Lahir","Tanggal Lahir","Kelamin","RT","RW","Provinsi","Kabupaten","Kecamatan","Kelurahan","Suami","Istri","Ayah","Ibu"));
$master ->getUItable()
        ->setAddButtonEnable(false)
        ->setReloadButtonEnable(false)
        ->setPrintButtonEnable(false)
        ->setEditButtonEnable(false);
$master ->initialize();
?>