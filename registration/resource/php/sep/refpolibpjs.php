<?php 
require_once "smis-libs-class/MasterTemplate.php";
require_once "registration/class/responder/BPJSCheckerResponder.php";
require_once "registration/class/responder/RefBPJSResponder.php";
global $db;

$mas=new MasterTemplate($db,"smis_rg_refpolibpjs","registration","refpolibpjs");
$uitable=$mas->getUItable();
$uitable->addModal("id","hidden","","");
$uitable->addModal("nama","text","Nama","");
$uitable->addModal("kode","text","Kode","");
$uitable->addModal("grup","text","Grup","");
$uitable->addModal("status","checkbox","Status","");
$uitable->setHeader(array("No.","Grup","Nama","Kode","Status"));
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);

if(isset($_POST['command']) && ($_POST['command']=="ref_kamar" || $_POST['command']=="ref_poli") ){
    $responder=new RefBPJSResponder($mas->getDBtable(),$mas->getUItable(),$mas->getAdapter());
    $mas->setDBresponder($responder);
}

$button=new Button("","","Poli");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon("fa fa-cloud-download");
$button->setAction("refpolibpjs.fetch_poli()");
$button->setClass("btn-primary");
$uitable->addHeaderButton($button);

$button=new Button("","","Kamar");
$button->setIsButton(Button::$ICONIC_TEXT);
$button->setIcon("fa fa-bed");
$button->setAction("refpolibpjs.fetch_kamar()");
$button->setClass("btn-primary");
$uitable->addHeaderButton($button);

$adapter=$mas->getAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Grup","grup");
$adapter->add("Nama","nama");
$adapter->add("Kode","kode");
$adapter->add("Status","status","trivial_1_Aktif_-");
$mas->setModalTitle("Map Referensi Poli BPJS");
$mas->addResouce("js","registration/resource/js/refpolibpjs.js","after");
$mas->initialize();

?>