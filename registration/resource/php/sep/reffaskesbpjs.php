<?php 
require_once "smis-libs-class/MasterTemplate.php";
require_once "registration/class/responder/BPJSCheckerResponder.php";
require_once "registration/class/responder/RefBPJSResponder.php";
global $db;

$mas     = new MasterTemplate($db,"smis_rg_refpolibpjs","registration","reffaskesbpjs");
$dbtable = $mas->getDBTable();
$dbtable ->addCustomKriteria(" grup","='faskes' ");
$uitable = $mas->getUItable();
$uitable->addModal("id","hidden","","");
$uitable->addModal("nama","text","Nama","");
$uitable->addModal("kode","text","Kode","");
$uitable->addModal("grup","hidden","","faskes");
$uitable->addModal("status","checkbox","Status","");
$uitable->setHeader(array("No.","Nama","Kode","Status"));
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
/*
if(isset($_POST['command']) && ($_POST['command']=="ref_kamar" || $_POST['command']=="ref_poli") ){
    $responder=new RefBPJSResponder($mas->getDBtable(),$mas->getUItable(),$mas->getAdapter());
    $mas->setDBresponder($responder);
}*/

$adapter=$mas->getAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Grup","grup");
$adapter->add("Nama","nama");
$adapter->add("Kode","kode");
$adapter->add("Status","status","trivial_1_Aktif_-");
$mas->setModalTitle("Map Referensi Poli BPJS");
$mas->addResouce("js","registration/resource/js/reffaskesbpjs.js","after");
$mas->initialize();

?>