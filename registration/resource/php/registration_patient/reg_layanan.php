<?php
global $db;
require_once "smis-base/smis-include-duplicate.php";
require_once 'registration/class/table/DaftarTable.php';
$duplicate  = getSettings($db,"reg-layanan-duplicate","0");
$edit_mode  = getSettings($db,"reg-edit-time-strict","-1")*1;
$header		= array('No.','No Reg',"No Urut","Baru Lama",'Tanggal','NRM',"Nama","Umur","Golongan",'Layanan','Pembayaran','Rawat');
if(getSettings($db,"reg-jalan-show-profile-number","0")=="1"){
    $header[] = "Profile Number";
}
$uitable_dp = new DaftarTable($header,"", NULL,true);
$uitable_dp ->setEditMode($edit_mode)
			->setName('daftar_pasien')
			->setDelButtonEnable(false)
			->setAddButtonEnable(false)
			->setPrintButtonEnable(false)
			->setSynchronizeButton(false)
			->setLoopDuplicateButtonEnable($duplicate!="0")
			->setDuplicateButton($duplicate!="0");

if(isset($_POST['super_command']) && ($_POST['super_command']=="daftar_pasien") ){
	$adapter = new SynchronousViewAdapter();
	$adapter ->setUseNumber(true, "No.","back.")
			 ->add("No Reg", "id")
			 ->add("No Urut","no_urut")
			 ->add("Tanggal", "tanggal","date d M Y")
			 ->add("tgl", "tanggal")
			 ->add("NRM", "nrm")
			 ->add("Umur", "umur")
			 ->add("Golongan","gol_umur")
			 ->add("Baru Lama","barulama","trivial_0_Baru_Lama")
			 ->add("Nama", "nama_pasien")
			 ->add("Layanan", "jenislayanan","unslug")
			 ->add("Pembayaran", "carabayar", "unslug")
			 ->add("Rawat", "uri", "trivial_0_Jalan_Inap")
			 ->add("Profile Number", "profile_number");	
	$dbtly   = new DBTable($db,'smis_rg_layananpasien');
	$dbtly   ->setPreferredView(true, "smis_rgv_layananpasien_aktif",array("nrm","jenislayanan","carabayar","nama_pasien","lunas"))
			 ->setUseWhereforView(true)
			 ->setViewForSelect(true)
			 ->setOrder(" id DESC ");
	if(isset($_POST['dari']) && $_POST['dari']!=""){
		$dbtly ->addCustomKriteria(" tanggal>= ","'".$_POST['dari']."'");
	}
	if(isset($_POST['sampai']) && $_POST['sampai']!=""){
		$dbtly ->addCustomKriteria(" tanggal< ","'".$_POST['sampai']."'");
	}
	require_once 'registration/class/responder/DaftarResponder.php';
	$dbrsp	 = new DaftarResponder($dbtly,$uitable_dp,$adapter);
	$dbrsp	 ->setDuplicate($duplicate=="1","duplicate_layanan")
			 ->setAutonomous(getSettings($db,"smis_autonomous_id",""))
			 ->addNotifyData("table","smis_rg_layananpasien");
    $dtpx	 = $dbrsp->command($_POST['command']);
	echo json_encode($dtpx);
	return;
}
$payoff		= getSettings($db, "registration-payoff-model", "show");
$IS_DTL_PJ  = getSettings($db,"reg-pasien-show-pj-data","0")=="1";

$uitable_dp  ->addModal("id", "hidden", "", "");
$uitable_dp  ->addModal("nrm", "hidden", "", "");
$disable_date = getSettings($db, "reg-auto-jalan", "0")=="1";
$uitable_dp	  ->addModal("tanggal", "datetime", "Tanggal", date("Y-m-d H:i:s"),"n",NULL,$disable_date,NULL,true,"barulama");

$uitable_dp->addModal("barulama", "select", "Baru/Lama", RegistrationResource::getBaruLama(),"n",NULL,false,NULL,false,"jenislayanan");
if(getSettings($db,"registration-multiple-reg-place","0")=="1"){
    $uitable_dp->addModal("jenislayanan", "multiple-select", "Ruangan", RegistrationResource::getRuanganDaftar(), "n",NULL,false,NULL,false,"karcis");
}else{
    $uitable_dp->addModal("jenislayanan", "select", "Ruangan", RegistrationResource::getRuanganDaftar(), "n",NULL,false,NULL,false,"karcis");
}

$uitable_dp->addModal("karcis", "money", "Biaya Registrasi", "0","y",NULL,(getSettings($db,"reg-lock-karcis-rj","0")=="1"),NULL,false,"namapenanggungjawab");
$uitable_dp->addModal("namapenanggungjawab","text", "Penanggung Jawab", "", "n",NULL,false,NULL,false,"telponpenanggungjawab");
$uitable_dp->addModal("telponpenanggungjawab", "text", "Telp. Penanggung Jawab", "","y",NULL,false,NULL,false,$IS_DTL_PJ?"alamat_pj":($payoff=="show"?"lunas":"carabayar"));

if($IS_DTL_PJ){
	$uitable_dp->addModal("alamat_pj", "text", "Alamat Penanggung Jawab", "","y",NULL,false,NULL,false,"desa_pj");
	$uitable_dp->addModal("desa_pj", "text", "Desa Penanggung Jawab", "","y",NULL,false,NULL,false,"kecamatan_pj");
	$uitable_dp->addModal("kecamatan_pj", "text", "Kecamatan Penanggung Jawab", "","y",NULL,false,NULL,false,"kabupaten_pj");
	$uitable_dp->addModal("kabupaten_pj", "text", "Kabupaten Penanggung Jawab", "","y",NULL,false,NULL,false,"pekerjaan_pj");
	$uitable_dp->addModal("pekerjaan_pj", "text", "Pekerjaan Penanggung Jawab", "","y",NULL,false,NULL,false,"umur_pj");
	$uitable_dp->addModal("umur_pj", "text", "Umur Penanggung Jawab", "","y",NULL,false,NULL,false,"carabayar");
	$uitable_dp->addModal("hubungan_pj", "text", "Hubungan PJ dengan Pasien", "","y",NULL,false,NULL,false,"carabayar");	
}

if($payoff=="show"){
	$uitable_dp->addModal("lunas", "checkbox", "Lunas", "0","y",NULL,false,NULL,false,"carabayar");
}else{
	$uitable_dp->addModal("lunas", "hidden", "", "0");
}

$uitable_dp->addModal("carabayar", "select", "Pembayaran", RegistrationResource::getJenisPembayaran(), "n",NULL,false,NULL,false,"nama_perusahaan");
$uitable_dp->addModal("nama_perusahaan", "select", "Perusahaan", RegistrationResource::getPerusahaan(),"y",NULL,false,NULL,false,"asuransi");
$uitable_dp->addModal("asuransi", "select", "Asuransi", RegistrationResource::getAsuransi(),"y",NULL,false,NULL,false,"nobpjs");

if(getSettings($db,"reg-pasien-bpjs-checker","0")=="1"){
    $option=new OptionBuilder();
    $option->add("Aktif",1,0);
    $option->add("Non Aktif",0,1);
    
    $uitable_dp->addModal("nobpjs", "text", "No BPJS / Asuransi", "","y",NULL,false,"daftar_pasien",false,"status_bpjs");
    $uitable_dp->addModal("status_bpjs", "select", "Status", $option->getContent(),"y",NULL,true,NULL,false,"periode_bpjs");
	$uitable_dp->addModal("periode_bpjs", "text", "Periode", "","y",NULL,true,NULL,false,"ppk_bpjs");
	$uitable_dp->addModal("ppk_bpjs", "text", "Nama PPK", "","y",NULL,true,NULL,false,"kelas_bpjs");    
}else{
    $uitable_dp->addModal("nobpjs", "text", "No BPJS / Asuransi", "","y",NULL,false,NULL,false,"kelas_bpjs");
}
$uitable_dp->addModal("kelas_bpjs", "select", "Kelas BPJS", RegistrationResource::getKelasBPJS(),"y",NULL,false,NULL,false,"no_sep_rj");

if(getSettings($db,"reg-bpjs-activate-bpjs-reg-urj","0")=="1"){
    $uitable_dp->addModal("no_sep_rj", "text", "No SEP RJ", "","y",NULL,false,NULL,false,"plafon_bpjs");    
}else{
    $uitable_dp->addModal("no_sep_rj", "hidden", "", "","y",NULL,false,NULL,false,"plafon_bpjs");
}

if(getSettings($db,"reg-show-plafon","0")=="1"){
    $uitable_dp->addModal("plafon_bpjs", "money", "Plafon BPJS", "","y",NULL,false,NULL,false,"caradatang");   
}else{
    $uitable_dp->addModal("plafon_bpjs", "hidden", "", "","y",NULL,false,NULL,false,"caradatang");
}

$uitable_dp->addModal("caradatang", "select", "Kedatangan",RegistrationResource::getJenisKedatangan() ,"n",NULL,false,NULL,false,"rujukan");
$uitable_dp->addModal("rujukan", "select", "Jenis Perujuk", RegistrationResource::getJenisPerujuk(),"y",NULL,false,NULL,false,"nama_rujukan");
$uitable_dp->addModal("nama_rujukan", "chooser-registration_patient-perujuk", "Nama Perujuk", "","y",NULL,false,NULL,false,"keteranganbonus");
$uitable_dp->addModal("id_rujukan", "hidden", "", "");

$sender = getSettings($db, "registration-sender-model", "show");
if($sender=="show"){
	$uitable_dp->addModal("keteranganbonus", "select", "Ket Bonus", RegistrationResource::getJenisBonus(),"y",NULL,false,NULL,false,"namakodebonus");
	$uitable_dp->addModal("namakodebonus", "chooser-registration_patient-besarbonus", "Jasa", "","y",NULL,false,NULL,false,"besarbonus");
	$uitable_dp->addModal("kodebonus", "hidden", "", "0");
	$uitable_dp->addModal("besarbonus", "money", "Besar Jasa", "50000","y",NULL,false,NULL,false,"besarambil");
	$uitable_dp->addModal("besarambil", "money", "Telah Diambil", "0","y",NULL,false,NULL,false,"ambilbonus");
	$uitable_dp->addModal("ambilbonus", "text", "Pengambil Fee", "","y",NULL,false,NULL,false,"save");
}else{
	$uitable_dp->addModal("keteranganbonus", "hidden", "", "");
	$uitable_dp->addModal("namakodebonus", "hidden", "", "");
	$uitable_dp->addModal("kodebonus", "hidden", "", "0");
	$uitable_dp->addModal("besarbonus", "hidden", "", "0");
	$uitable_dp->addModal("besarambil", "hidden", "", "0");
	$uitable_dp->addModal("ambilbonus", "hidden", "", "0");
}

$activated_dokter_utama=getSettings($db,"reg-activate-dokter-utama","0");
$hidden_activated_dokter_utama=new Hidden("reg_activate_dokter_utama", "", $activated_dokter_utama);
if($activated_dokter_utama=="1"){
    $query="SELECT DISTINCT kode, keterangan FROM smis_rg_jadwal WHERE prop!='del' ";
    $data=$db->get_result($query);
    $option=new OptionBuilder();
    $option->add("","","1");
    foreach($data as $x){
        $option->add($x->kode." - ".$x->keterangan,$x->kode);
    }
	$uitable_dp->addModal("kode_du", "select", "Kode Dokter", $option->getContent(),"n");
	$uitable_dp->addModal("keterangan_du", "textarea", "Keterangan", "","y",null,true);
	$uitable_dp->addModal("suggest_du", "hidden", "", "y");
	$uitable_dp->addModal("editable_du", "hidden", "", "1");
}else{
	$uitable_dp->addModal("kode_du", "hidden", "", "","y");
	$uitable_dp->addModal("keterangan_du", "hidden", "", "","y");
	$uitable_dp->addModal("suggest_du", "hidden", "", "y");
	$uitable_dp->addModal("editable_du", "hidden", "", "1");
}

$modal_daftar_pasien = $uitable_dp->getModal();
$modal_daftar_pasien ->setTitle("Daftar Pasien");
$modal_daftar_pasien ->setComponentSize(Modal::$MEDIUM);

$reg_hidden_printsetup = getSettings($db, "reg-print-data-social-jsetup", "0");
$reg_hidden_component  = new Hidden("reg_print_data_social_jsetup", "", $reg_hidden_printsetup);

$activated_reg_nrm = getSettings($db, "pendaftaran-activated-check-nrm", "1");
$activated_cek_rm  = new Hidden("activate_reg_nrm", "activate_reg_nrm", $activated_reg_nrm);

$activated_autoprint  = getSettings($db, "reg-autoprint-registration", "1");
$activated_autoprint  = new Hidden("autoprint_registration", "autoprint_registration", $activated_autoprint);

$activated_autoload   = getSettings($db, "reg-jalan-autoload-pasien", "1");
$activated_autoload   = new Hidden("autoload_pasien_aktif", "autoload_pasien_aktif", $activated_autoload);


$uitable_dp->clearContent();
$uitable_dp->addModal("dari", "date", "Dari", "");
$uitable_dp->addModal("sampai", "date", "Sampai", "");

$show = new Button("","","");
$show ->setClass("btn btn-primary")
	  ->setIsButton(Button::$ICONIC_TEXT)
	  ->setIcon("icon-white icon-search")
	  ->setAction("daftar_pasien.view()");

$form = $uitable_dp->getModal()->getForm();
$form ->setTitle("Pasien Aktif")
	  ->addElement("",$show);
echo $form->getHtml();

echo $modal_daftar_pasien			->getHtml();
echo $uitable_dp					->getHtml();
echo $reg_hidden_component			->getHtml();
echo $activated_cek_rm				->getHtml();
echo $activated_autoprint			->getHtml();
echo $hidden_activated_dokter_utama	->getHtml();
echo $activated_autoload			->getHtml();
?>