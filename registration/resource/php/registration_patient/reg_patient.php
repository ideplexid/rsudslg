<?php 
global $db;
require_once "smis-base/smis-include-duplicate.php";
$header     = array("No.",'NRM','Tanggal',"Panggilan",'Nama',"KTP","BPJS","No. Profile","Tanggal Lahir",'Alamat',"Provinsi","Kabupaten","Kecamatan","Kelurahan",'L/P');
$duplicate  = getSettings($db,"reg-patient-duplicate","0");
$input_bpjs = getSettings($db,"registration-show-input-bpjs","0")=="1";
$prof_num   = getSettings($db,"registration-show-profile-number","0")=="1";
$is_cvf_ktp = getSettings($db,"reg-cvf-ktp","1")=="1";
$is_cvf_tgl = getSettings($db,"reg-cvf-tgl","1")=="1";
$is_cvf_jk  = getSettings($db,"reg-cvf-jk","1")=="1";
$IS_DTL_KK  = getSettings($db,"registration-show-family-data","0")=="1";

$uitable    = new TableSynchronous($header,"Registrasi Awal : Pendataan Pasien", NULL,true);
$uitable    ->setName('registration_patient')
            ->setHelpButtonEnabled(true,"registration","rawat_jalan")
            ->setReloadButtonEnable(false)
            ->setPrintButtonEnable(false)
            ->setLoopDuplicateButtonEnable($duplicate!="0")
            ->setDuplicateButton($duplicate!="0")
            ->setSynchronizeButton(false);

$btn        = new Button("", "", "Register");
$btn        ->setIsButton(Button::$ICONIC)
            ->setIcon("fa fa-sign-in");
$uitable->addContentButton("register", $btn);
if (getSettings($db, "pendaftaran-show-her_op", "0")=="1") {
	$btn     = new Button("", "", "HER-OP");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setClass("btn-danger")
             ->setIcon("fa fa-trash");
	$uitable ->addContentButton("her_op", $btn);
}
if(getSettings($db, "reg-show-skb", "0")=="1"){
	$btn     = new Button("", "", "SKB");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-heartbeat")
             ->setClass(" btn-primary");
	$uitable ->addContentButton("skb", $btn);
}

if(getSettings($db, "reg-fingerprint", "0")=="1"){
	$btn     = new Button("", "", "Fingerprint");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-thumbs-up")
             ->setClass(" btn-success");
	$uitable ->addContentButton("fingerprint", $btn);
}

$IS_KARTU=getSettings($db,"pendaftaran-activated-kartu","0")=="1";
if($IS_KARTU){
	$btn     = new Button("", "", "KARTU DEWASA");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-credit-card")
             ->setClass(" btn-primary");
	$uitable ->addContentButton("kartu", $btn);
	
	$btn     = new Button("", "", "KARTU BAYI");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-child")
             ->setClass(" btn-primary");
	$uitable ->addContentButton("kartu_bayi", $btn);
    
    if(getSettings($db,"reg-kartu-activate-price","0")=="1"){
        $btn     = new Button("", "", " BAYAR KARTU");
        $btn     ->setIsButton(Button::$ICONIC)
                 ->setIcon("fa fa-credit-card-alt")
                 ->setClass(" btn-primary");
        $uitable ->addContentButton("tarif_kartu", $btn);
    }
}

if(getSettings($db, "reg-show-sinkronisasi", "0")=="1"){
	$btn     = new Button("", "", "Broadcast");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-volume-up")
             ->setClass(" btn-primary");
	$uitable ->addContentButton("sinkronisasi", $btn);
}

if(getSettings($db,"reg-bpjs-activate-bpjs-reg-urj","0")=="1"){
	$btn     = new Button("", "", "SEP");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-id-badge")
             ->setClass(" btn-primary");
	$uitable ->addContentButton("sep_bpjs", $btn);
}

if(getSettings($db,"reg-bpjs-activate-vclaim-reg-urj","0")=="1"){
	$btn     = new Button("", "", "VClaim");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-credit-card")
             ->setClass(" btn-primary");
	$uitable ->addContentButton("vclaim_bpjs", $btn);
}

$ID_CARD     = getSettings($db,"reg-activate-master-card","0")=="1";
if($ID_CARD){
    $btn     = new Button("", "", "ID Card");
	$btn     ->setIsButton(Button::$ICONIC)
             ->setIcon("fa fa-id-card")
             ->setClass(" btn-success");
	$uitable ->addContentButton("idcard", $btn);
}
$sbt    = RegistrationResource::getSebutan("");
$x      = array("name"=>"","value"=>"","default"=>"1");
array_unshift($sbt,$x);

$uitable->setDelButtonEnable(false);
$jenis_kelamin = new OptionBuilder();
$jenis_kelamin ->add("Semua","%","1")
               ->add("Laki-Laki","0")
               ->add("Perempuan","0");
$id            = new Text("search_nrm","search_nrm","");
$id            ->setClass("search");
$panggilan     = new Select("search_panggilan","search_panggilan","");
$panggilan     ->setValue($sbt);
$panggilan     ->setClass("search");
$nama          = new Text("search_nama","search_nama","");
$nama          ->setClass("search");
$alamat        = new Text("search_alamat","search_alamat","");
$alamat        ->setClass("search");
$nobpjs        = new Text("search_nobpjs","search_nobpjs","");
$nobpjs        ->setClass("nobpjs");
$profnum       = new Text("search_profnum","search_profnum","");
$profnum       ->setClass("profnum");
$kelamin       = new Select("search_kelamin","search_kelamin",$jenis_kelamin->getContent());
$kelamin       ->setClass("search");
$ktp           = new Text("search_ktp","search_ktp","");
$ktp           ->setClass("search");
$date          = new Text("search_date","search_date","");
$date          ->setModel(Text::$DATE);
$date_lhr      = new Text("search_tgl_lhr","search_tgl_lhr","");
$date_lhr      ->setModel(Text::$DATE);

$search        = new Button("","","");
$search        ->setClass(" btn-inverse ")
               ->setAction("registration_patient.view()")
               ->setIsButton(Button::$ICONIC)
               ->setIcon("icon-white icon-search");

$kosong        = array("name"=>"","value"=>"%");
$kabupaten     = RegistrationResource::getProvinsi();
array_unshift($kabupaten, $kosong);
$provinsi      = new Select("search_provinsi", "Provinsi", $kabupaten);
$kabupaten     = new Select("search_kabupaten", "Kabupaten","");
$kecamatan     = new Select("search_kecamatan", "Kecamatan", "");
$kelurahan     = new Select("search_kelurahan", "Kelurahan", "");

$header="<tr class='reghead'>
				<td></td>
				<td>".$id->getHtml()."</td>
                <td>".$date->getHtml()."</td>
                <td>".$panggilan->getHtml()."</td>
				<td>".$nama->getHtml()."</td>
                <td>".$ktp->getHtml()."</td>
                <td>".$nobpjs->getHtml()."</td>
                <td>".$profnum->getHtml()."</td>
				<td>".$date_lhr->getHtml()."</td>
				<td>".$alamat->getHtml()."</td>
                <td>".$provinsi->getHtml()."</td>
				<td>".$kabupaten->getHtml()."</td>
				<td>".$kecamatan->getHtml()."</td>
				<td>".$kelurahan->getHtml()."</td>		
				<td>".$kelamin->getHtml()."</td>
				<td>".$search->getHtml()."</td>
			</tr>";

$uitable->addHeader("after",$header);

if(isset($_POST['command'])){
	require_once 'registration/class/responder/PasienResponder.php';
	$adapter     = new SynchronousViewAdapter(true,"No.");
    $adapter     ->add("NRM", "id","digit6")
                 ->add("Panggilan", "sebutan")
                 ->add("Tanggal", "tanggal","date d M Y")
                 ->add("Nama", "nama")
                 ->add("Alamat", "alamat")
                 ->add("Provinsi", "nama_provinsi")
                 ->add("Kabupaten", "nama_kabupaten")
                 ->add("Kecamatan", "nama_kecamatan")
                 ->add("Kelurahan", "nama_kelurahan")
                 ->add("Tanggal Lahir", "tgl_lahir","date d M Y")
                 ->add("L/P","kelamin", "trivial_0_Laki-Laki_Perempuan")
                 ->add("BPJS", "nobpjs")
                 ->add("No. Profile", "profile_number")
                 ->add("KTP", "ktp");
	$dbtable     = new DBTable($db,"smis_rg_patient");
    
	$dbtable     ->setOrder(" id DESC ");
	$dbresponder = new PasienResponder($dbtable,$uitable,$adapter);
    $dbresponder ->setNoProfileUsability($prof_num)
                 ->setNoBPJSUsability($input_bpjs)
                 ->setDuplicate($duplicate=="1","duplicate_patient")
                 ->setAutonomous(getSettings($db,"smis_autonomous_id",""))
                 ->addNotifyData("table","smis_rg_patient");
	$data        = $dbresponder->command($_POST['command']);
	echo json_encode($data);
	return;
}

$dfkab          = getSettings($db,"registration-kabupaten-nama","");
$dfkabid        = getSettings($db,"registration-kabupaten-id","");
$jenis_tampil   = getSettings($db,"reg-allow-jenis-pasien","1")=="1";
$option         = new OptionBuilder();
$option         ->add($dfkab,$dfkabid,"1");
$uitable        ->addModal("id", "text", "No RM", "","y",NULL,false,NULL,true,$jenis_tampil?"jenis":"sebutan")
                ->addModal("label", "html", "&nbsp;", "<small>Kosongi No RM untuk pasien baru</small>")
                ->addModal("tanggal", "hidden", "", date("Y-m-d"));
if($jenis_tampil){
	$uitable    ->addModal("jenis", "select", "Jenis", RegistrationResource::getPatientJenis(),"n",NULL,false,NULL,false,"sebutan");
}
$uitable        ->addModal("sebutan", "select", "Sebutan", RegistrationResource::getSebutan(),"n",NULL,false,NULL,false,"nama")
                ->addModal("nama", "text", "Nama", "","n",NULL,false,NULL,false,"kelamin")
                ->addModal("kelamin", "select","Kelamin", RegistrationResource::getJenisKelamin(),"n",NULL,false,NULL,false,"ktp")
                ->addModal("ktp", "text", "KTP", "", "y",NULL,false,NULL,false,"alamat");
if($input_bpjs){
    $uitable ->addModal("nobpjs", "text", "No. BPJS", "","y","numeric",false,NULL,false,"profile_number");
}
                
$uitable        ->addModal("label", "html", "&nbsp;", "<small>Jika tidak memiliki KTP bisa diisikan '-'</small>")
                ->addModal("alamat", "text", "Alamat", "","n",NULL,false,NULL,false,"rt");
if(getSettings($db,"reg-show-rt","1")=="1"){
    $uitable    ->addModal("rt", "text", "RT", "","y","numeric",false,NULL,false,"rw");    
}
if(getSettings($db,"reg-show-rt","1")=="1"){
    $uitable    ->addModal("rw", "text", "RW", "","y","numeric",false,NULL,false,"provinsi");
}
$uitable     ->addModal("provinsi", "select", "Provinsi", RegistrationResource::getProvinsi(),"y",NULL,false,NULL,false,"kabupaten")
             ->addModal("kabupaten", "select", "Kabupaten", $option->getContent(),"y",NULL,false,NULL,false,"kecamatan")
             ->addModal("kecamatan", "select", "Kecamatan", "","y",NULL,false,NULL,false,"kelurahan")
             ->addModal("kelurahan", "select", "Kelurahan", "","y",NULL,false,NULL,false,"kedusunan")
             ->addModal("kedusunan", "select", "Dusun", "","y",NULL,false,NULL,false,"suku");

if(getSettings($db, "reg-show-suku", "0")=="1"){
	$uitable ->addModal("suku", "select", "Suku", RegistrationResource::getSukuBangsa(),"n",NULL,false,NULL,false,"bahasa");
}
if(getSettings($db, "reg-show-bahasa", "0")=="1"){
	$uitable ->addModal("bahasa", "select", "Bahasa", RegistrationResource::getBahasa(),"n",NULL,false,NULL,false,"tempat_lahir");
}
$uitable     ->addModal("tempat_lahir", "text", "Tempat Lahir", "","y",NULL,false,NULL,false,"tgl_lahir");

if(getSettings($db, "reg-tgl-lahir", "0")=="1"){
	$uitable ->addModal("tgl_lahir", "itextdate", "Tanggal Lahir", "","n",NULL,false,NULL,false,"umur");
}else{
	$uitable ->addModal("tgl_lahir", "idate", "Tanggal Lahir", "","n",NULL,false,NULL,false,"umur");
}
$uitable ->addModal("umur", "text", "Umur", "","y",NULL,true,NULL,false,"status")
         ->addModal("status", "select", "Status", RegistrationResource::getMarital(),"y",NULL,false,NULL,false,"pendidikan")
         ->addModal("pekerjaan", "select", "Pekerjaan", RegistrationResource::getJenisKerja(),"y",NULL,false,NULL,false,"pendidikan")
         ->addModal("pendidikan", "select", "Pendidikan", RegistrationResource::getJenisPendidikan(),"y",NULL,false,NULL,false,"agama")
         ->addModal("agama", "select", "Agama", RegistrationResource::getAgama(),"y",NULL,false,NULL,false,"telpon")
         ->addModal("telpon", "text", "Telepon/HP", "","y","numeric",false,NULL,false,"email");
 
if(getSettings($db,"reg-show-email","1")=="1"){
    $uitable ->addModal("email", "text", "Email", "","y",NULL,false,NULL,false,"bbm");
}
if(getSettings($db,"reg-show-bbm","1")=="1"){
    $uitable->addModal("bbm", "text", "BBM", "","y",NULL,false,NULL,false,"istri");
}
if(getSettings($db,"reg-show-istri","1")=="1"){
    $uitable->addModal("istri", "text", "Istri", "","y",NULL,false,NULL,false,"suami");
}
if(getSettings($db,"reg-show-suami","1")=="1"){
    $uitable->addModal("suami", "text", "Suami", "","y",NULL,false,NULL,false,"ayah");
}
if(getSettings($db,"reg-show-ayah","1")=="1"){
    $uitable->addModal("ayah", "text", "Ayah", "","y",NULL,false,NULL,false,"ibu");
}
if(getSettings($db,"reg-show-ibu","1")=="1"){
    $uitable->addModal("ibu", "text", "Ibu", "","y",NULL,false,NULL,false,$IS_DTL_KK?"alamat_keluarga":"kartu");
}

    

if($IS_DTL_KK){
    $uitable ->addModal("alamat_keluarga", "text", "Alamat Keluarga", "","y",NULL,false,NULL,false,"desa_keluarga")
             ->addModal("desa_keluarga", "text", "Desa Keluarga", "","y",NULL,false,NULL,false,"kecamatan_keluarga")
             ->addModal("kecamatan_keluarga", "text", "Kecamatan Keluarga", "","y",NULL,false,NULL,false,"pekerjaan_keluarga")
             ->addModal("kabupaten_keluarga", "text", "Kabupaten Keluarga", "","y",NULL,false,NULL,false,"pekerjaan_keluarga")
             ->addModal("pekerjaan_keluarga", "text", "Pekerjaan Keluarga", "","y",NULL,false,NULL,false,"umur_keluarga")
             ->addModal("umur_keluarga", "text", "Umur Keluarga", "","y",NULL,false,NULL,false,"telepon_keluarga")
             ->addModal("telepon_keluarga", "text", "Telepon Keluarga", "","y","numeric",false,NULL,false,"kartu");
}

if($IS_KARTU){
	$uitable ->addModal("kartu", "checkbox", "Sudah Dapat Kartu", "","0",NULL,false,NULL,false,"nobpjs");
}
if($prof_num){
    $uitable ->addModal("profile_number", "text", "No. Profile", "","0",NULL,false,NULL,false,"save");
}
if(getSettings( $db, "reg-document", "0")=="1"){
	$uitable ->addModal("document", "file-multiple-image", "Dokumen", "","y",NULL,false,NULL,false,"save");
}
if(getSettings( $db, "registration-keterangan", "0")=="1"){
	$uitable ->addModal("keterangan", "textarea", "Keterangan", "","y",NULL,false,NULL,false,"save");
}

$modal  = $uitable->getModal();
$modal  ->setTitle("Data Lengkap Pasien")
        ->setComponentSize(Modal::$MEDIUM);

/* menambahkan tombol simpan dan registrasi */
$button = new Button("registration_patient_save_reg","registration_patient_save_reg","Save & Register");
$button ->setClass("btn-warning")
        ->setAction("registration_patient.saveReg()")
        ->setIcon(" fa fa-save")
        ->setIsButton(Button::$ICONIC_TEXT);
$modal  ->addFooter($button);

/* menambahkan tombol save dan lengkapi id card */
if($ID_CARD){
    $button = new Button("registration_patient_save_card","registration_patient_save_card","Save & ID Card");
    $button ->setIcon("fa fa-id-card")
            ->setClass("btn-success")
            ->setIsButton(Button::$ICONIC_TEXT)
            ->setAction("registration_patient.saveCard()");
    $modal  ->addFooter($button);
}

/* menambahkan tombol register */
$button = new Button("registration_patient_reg","registration_patient_reg","Register");
$button ->setIcon("fa fa-sign-in")
        ->setClass("btn-danger")
        ->setIsButton(Button::$ICONIC_TEXT)
        ->setAction("registration_patient.loadPatient()");
$modal  ->addFooter($button);

$regnamaset  = getSettings($db, "reg-disabled-edit-name", "0");
$regnama     = new Hidden("reg_disabled_edit_name","",$regnamaset);
$fontcaseset = getSettings($db, "reg-font-case", "0");
$fontcaseset = new Hidden("reg_font_case","",$fontcaseset);
$autoloadset = getSettings($db, "reg-autoload-pasien", "0");
$autoloadset = new Hidden("reg_autoload_pasien","",$autoloadset);

if(!$input_bpjs){
    $uitable ->addClass("no_bpjs_column");
}
if(!$prof_num){
    $uitable ->addClass("no_profnum_column");
}
if(!$is_cvf_ktp){
    $uitable ->addClass("no_ktp_column");
}
if(!$is_cvf_tgl){
    $uitable ->addClass("no_tgl_column");
}
if(!$is_cvf_jk){
    $uitable ->addClass("no_jk_column");
}

echo $regnama     ->getHtml();
echo $fontcaseset ->getHtml();
echo $autoloadset ->getHtml();
echo $uitable     ->getHtml();
echo $modal       ->getHtml();
echo addJS("registration/resource/js/registration_patient.js",false);
echo addCSS("registration/resource/css/reg_patient.css",false);
?>
