<?php

/**
 * digunakan untuk manajemen data pasien 
 * yang memiliki kepesertaan dengan asuransi
 * tertentu, yang mana nomor-nomor asuransi ini 
 * akan disimpan untuk kemudian hari dipakai
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_rg_patient
 * 				  - smis_rg_nomor_asuransi
 * @since		: 16 Mei 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';


$header=array ("Nama","Alamat",'L/P','NRM' );
$dbtable= new DBTable($db, "smis_rg_patient");
$dktable = new Table ( $header );
$dktable->setName ( "nomor_asuransi_patient" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Alamat", "alamat" );
$dkadapter->add ( "L/P", "kelamin","trivial_0_L_P" );
$dkadapter->add ( "NRM", "id","only-digit8");
$nomor_asuransi_patient = new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ("Nama","Alamat",'Telpon' );
$dbtable= new DBTable($db, "smis_rg_asuransi");
$dktable = new Table ( $header );
$dktable->setName ( "asuransi_nomor_asuransi" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Alamat", "alamat" );
$dkadapter->add ( "Telpon", "telpon" );
$asuransi_nomor_asuransi = new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ('No.',"Asuransi","Nomor Peserta","Kartu");
$pemreg=new MasterSlaveTemplate($db, "smis_rg_nomor_asuransi", "registration", "nomor_asuransi");

$pemreg->setDateEnable(true);
$uitable=$pemreg->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setName("nomor_asuransi");
$pemreg->setUITable($uitable);
if($pemreg->getDBResponder()->isView() && $_POST['super_command']==""){
	$pemreg	->getDBtable()
			->addCustomKriteria("nrm_pasien", "='".$_POST['nrm_pasien']."'");
}

$btn=new Button("btn_nomor_asuransi_register","","Register");
$btn->setClass(" btn-danger");
$btn->setIcon(" fa fa-sign-in");
$btn->setAction("nomor_asuransi.regPatient()");
$btn->setIsButton(Button::$ICONIC_TEXT);

$pemreg->addFlag("id_pegawai", "Pilih Pasien Dahulu", "Silakan Pilih Pasien Dahulu")
		 ->addNoClear("nrm_pasien")
		 ->addNoClear("nama_pasien")
		 ->setDateTimeEnable(true)
		 ->getUItable()
		 ->addHeaderButton($btn)
         ->addModal("nrm_pasien", "chooser-nomor_asuransi-nomor_asuransi_patient-Nomor Peserta", "NRM", "","n",null,true)
		 ->addModal("nama_pasien", "text", "Nama", "","n",null,true);
$pemreg ->getForm();
$pemreg ->getUItable()
		 ->setHeader($header)
		 ->addModal ( "id", "hidden", "", "")
		 ->addModal ( "nama_asuransi", "chooser-nomor_asuransi-asuransi_nomor_asuransi-Asuransi", "Asuransi", "")
		 ->addModal ( "id_asuransi", "hidden", "", "")
		 ->addModal ( "nomor_asuransi", "text", "Nomor", "")
         ->addModal ( "kartu", "file-single-image", "Kartu", "");
$adapter=$pemreg->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Asuransi", "nama_asuransi")
		->add("Nomor Peserta", "nomor_asuransi")
		->add("Kartu", "kartu","file-image-show");
$pemreg ->getSuperCommand()
        ->addResponder("nomor_asuransi_patient", $nomor_asuransi_patient)
        ->addResponder("asuransi_nomor_asuransi", $asuransi_nomor_asuransi);
$pemreg ->addSuperCommand("asuransi_nomor_asuransi", array())
		->addSuperCommandArray("asuransi_nomor_asuransi", "id_asuransi", "id")
		->addSuperCommandArray("asuransi_nomor_asuransi", "nama_asuransi", "nama")
        ->addSuperCommand("nomor_asuransi_patient", array())
		->addSuperCommandArray("nomor_asuransi_patient", "nama_pasien", "nama")
		->addSuperCommandArray("nomor_asuransi_patient", "nrm_pasien", "id")
		->addSuperCommandArray("nomor_asuransi_patient", "smis_action_js", "nomor_asuransi.view();","smis_action_js")
		->addRegulerData("nrm_pasien", "nrm_pasien","id-value")
		->getModal()
		->setTitle("Nomor Asuransi Pasien");
$pemreg->addResouce("js","registration/resource/js/nomor_asuransi.js");
$pemreg->initialize();
?>