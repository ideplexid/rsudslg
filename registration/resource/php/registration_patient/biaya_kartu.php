<?php 

global $db;
require_once "smis-libs-class/MasterTemplate.php";

$pasien_karyawan=null;
if(isset($_POST['super_command']) && $_POST['super_command']=="biaya_kartu_pasien"){
    $dkadapter = new SimpleAdapter ();
    $dkadapter->add ( "NRM", "id","only-digit8" );
    $dkadapter->add ( "Nama", "nama" );
    $dkadapter->add ( "Alamat", "alamat" );
    $header=array ('NRM','Nama',"Alamat" );
    $dktable = new Table ( $header);
    $dktable->setName ( "biaya_kartu_pasien" );
    $dktable->setModel ( Table::$SELECT );
    $dbtable=new DBTable($db,"smis_rg_patient");
    $pasien_karyawan = new DBResponder ( $dbtable, $dktable, $dkadapter );    
}

$master=new MasterTemplate($db,"smis_rg_kartu","registration","biaya_kartu");
$master->setDateTimeEnable(true);
$uitable=$master->getUItable();
$uitable->addModal("id","hidden","","");
$uitable->addModal("nrm_pasien","chooser-biaya_kartu-biaya_kartu_pasien-Pilih Pasien","NRM","","n",null,true);
$uitable->addModal("nama_pasien","text","Nama","","n",null,true);
$uitable->addModal("tanggal","datetime","Waktu",date("Y-m-d H:i"));
$uitable->addModal("biaya","money","Biaya",getSettings($db,"reg-kartu-activate-price-value","0"));

$header=array("No.","Tanggal","Nama","NRM","Biaya");
$uitable->setHeader($header);
$adapter=$master->getAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Nama","nama_pasien");
$adapter->add("NRM","nrm_pasien");
$adapter->add("Tanggal","tanggal","date d M Y");
$adapter->add("Biaya","biaya","money Rp.");
$master->setModalTitle("Tarif Cetak Kartu");
$master->setModalComponentSize(Modal::$MEDIUM);

$master->getSuperCommand()->addResponder("biaya_kartu_pasien", $pasien_karyawan);
$master->addSuperCommand("biaya_kartu_pasien", array());
$master->addSuperCommandArray("biaya_kartu_pasien", "nrm_pasien", "id");
$master->addSuperCommandArray("biaya_kartu_pasien", "nama_pasien", "nama");
$alert=new HTML("","","<div class='left'>Pastikan Kartu Telah Tercetak, Jangan Sampai Sudah di Save ternyata kartu gagal cetak</div>");
$master->getModal()
        ->setModalSize(Modal::$HALF_MODEL)
        ->addFooter($alert);
$master->initialize();
?>