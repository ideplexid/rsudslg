<?php
    global $db;
    $head=array ("No.",'Tanggal','Sheets','Max Memory','Max Time','File');
    $uitable = new Table ( $head, "Import Pasien", NULL, true );
    $uitable->setName ( "importer_pasien" );
    
    $btn_excel=new Button("","","Template Excel");
    $btn_excel->setIsButton(Button::$ICONIC_TEXT);
    $btn_excel->setIcon("fa fa-download");
    $btn_excel->setAction("importer_pasien.download_template()");
    $btn_excel->setClass(" btn btn-inverse");
    $uitable->addFooterButton( $btn_excel);
    
    $btn=new Button("","","");
    $btn->setIsButton(Button::$ICONIC);
    $btn->setIcon("fa fa-file-excel-o");
    $btn->setClass(" btn btn-inverse");
    $uitable->addContentButton("import_proceed",$btn);
    if (isset ( $_POST ['command'] )) {
        require_once "registration/class/importer/PasienExcelImporter.php";
        require_once "registration/class/responder/PasienExcelImporterResponder.php";
        $adapter = new SimpleAdapter ();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add ( "File","file" );
        $adapter->add ( "Tanggal","tanggal","date d M Y H:i" );
        $adapter->add ( "Sheets","sheets" );
        $adapter->add ( "Max Memory","max_memory" );
        $adapter->add ( "Max Time","max_time" );
        $dbtable = new DBTable ( $db, "smis_rg_import_pasien" );
        
        $dbres = new PasienExcelImporterResponder ( $dbtable, $uitable, $adapter );
        if($dbres->isSave() || $dbres->is("import_proceed")){
            $importer=new PasienExcelImporter($db,new DBTable($db,"smis_rg_patient"),"");
            $dbres->setImporter($importer);
        }
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    }
    
    $uitable->addModal("id", "hidden", "", "");
    $uitable->addModal("tanggal", "datetime", "Tanggal", date("Y-m-d H:i:s"));
    $uitable->addModal("max_memory", "text", "Max Memory", "150M");
    $uitable->addModal("max_time", "text", "Max Time", "3600000");
    $uitable->addModal("file", "file-single-document", "File", "");
    echo $uitable->getHtml ();
    echo $uitable->getModal()->setTitle("Import Pasien")->getHtml();
    echo addJS ( "framework/smis/js/table_action.js" );
    echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
    echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    echo addJS ( "registration/resource/js/importer_pasien.js",false );
    
?>