<?php

/**
 * digunakan untuk manajemen data pasien 
 * yang memiliki kepesertaan dengan asuransi
 * tertentu, yang mana nomor-nomor asuransi ini 
 * akan disimpan untuk kemudian hari dipakai
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_rg_patient
 * 				  - smis_rg_nomor_asuransi
 * @since		: 16 Mei 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once "registration/class/table/ImportBPJSTable.php";
$btn=new Button("","","");
$btn->setClass(" btn-inverse");
$btn->setAction("cek_import_bpjs.view()");
$btn->setIcon(" icon-white icon-search");
$btn->setIsButton(Button::$ICONIC);

$header=array ('No.',"Nama Faskes","Nama PPK","No Peserta","Nama Pasien","NRM","JK","Tgl Lahir","Pisa","Alamat","Jenis","Periode","Updated");
$pemreg=new MasterSlaveTemplate($db, "smis_rg_excel_bpjs", "registration", "cek_import_bpjs");

$uitable=new ImportBPJSTable($header,"");
$pemreg->setDateEnable(true);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setAddButtonEnable(false);
$uitable->setEditButtonEnable(false);
$uitable->setDelButtonEnable(false);
$uitable->setAction(true);
$uitable->setName("cek_import_bpjs");
$pemreg->setUITable($uitable);

$pemreg->setUITable($uitable);
if( $pemreg ->getDBResponder() ->isView()){
	$pemreg	->getDBtable()
			->addCustomKriteria("nama_pasien", " LIKE '%".$_POST['nama']."%'")
            ->addCustomKriteria("no_pst", " LIKE '%".$_POST['no_pst']."%'");
}

$pemreg  ->addNoClear("nama")
		 ->addNoClear("no_pst")
		 ->getUItable()
         ->addModal("nama", "text", "Nama", "","y")
		 ->addModal("no_pst", "text", "No. Peserta", "","y");
$pemreg ->getForm()->addElement("",$btn);
$pemreg ->getUItable()
		 ->setHeader($header);
$adapter=$pemreg->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Nama Faskes", "nama_faskes")
		->add("Nama PPK", "nama_ppk")
        ->add("Nama Pasien", "nama_pasien")
		->add("No Peserta", "no_pst")
        ->add("JK", "jk","trivial_1_L_P")
        ->add("Tgl Lahir", "tgl_lahir","date d M Y")
        ->add("Updated", "last_update","date d M Y H:i")
        ->add("Pisa", "pisa")
        ->add("NRM", "nrm")
        ->add("Alamat", "alamat")
        ->add("Jenis", "jenis")
        ->add("Periode", "periode");
$pemreg->addViewData("nama","nama");
$pemreg->addViewData("no_pst","no_pst");
$pemreg->setAutoReload(true);
$pemreg->addResouce("js","registration/resource/js/cek_import_bpjs.js");
$pemreg->initialize();
?>