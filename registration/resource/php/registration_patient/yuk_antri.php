<?php 
global $db;
require_once "smis-libs-class/MasterSlaveTemplate.php";
$master  = new MasterSlaveTemplate($db,"smis_rg_yukantri","registration","yuk_antri");
$uitable = $master ->getUITable();
$uitable ->addHeaderElement("No.");
$uitable ->addHeaderElement("Tanggal");
$uitable ->addHeaderElement("Nama");
$uitable ->addHeaderElement("Nomor");
$uitable ->addHeaderElement("NRM");
$uitable ->addHeaderElement("KTP");
$uitable ->addHeaderElement("Telp");
$uitable ->addHeaderElement("No. BPJS");
$uitable ->addHeaderElement("Cara Bayar");
$uitable ->addHeaderElement("Poli");
$uitable ->addHeaderElement("Kode");
$uitable ->addHeaderElement("YKA ID");
$uitable ->setDelButtonEnable(false);
$uitable ->setAddButtonEnable(false);
$uitable ->setEditButtonEnable(false);
$uitable ->setPrintButtonEnable(false);
$uitable ->setReloadButtonEnable(false);

if($master->getDBResponder()->isView()){
    if($_POST['tanggal']!=""){
        $master->getDBTable()->addCustomKriteria(" yka_tanggal ","='".$_POST['tanggal']."'");
    }
}

$view = new Button("","","Tampilkan");
$view ->setClass("btn btn-primary");
$view ->setAction("yuk_antri.view()");
$view ->setIsButton(Button::$ICONIC_TEXT);
$view ->setIcon("fa fa-refresh");

$crawler = new Button("","","Tarik Dari Yuk Antri");
$crawler ->setClass("btn btn-danger");
$crawler ->setAction("yuk_antri.crawler()");
$crawler ->setIsButton(Button::$ICONIC_TEXT);
$crawler ->setIcon("fa fa-download");

$register = new Button("","","Register");
$register ->setClass("btn btn-success");
$register ->setIsButton(Button::$ICONIC_TEXT);
$register ->setIcon("fa fa-sign-in");

$uitable ->addModal("tanggal","date","Tanggal",date("Y-m-d"));
$form = $master ->getForm();
$form ->addElement("",$view);
$form ->addElement("",$crawler);

$uitable ->addContentButton("register",$register);
$adapter = $master ->getAdapter();
$adapter ->setUseNumber(true,"No.","back.");
$adapter ->add("Tanggal","yka_tanggal","date d M Y");
$adapter ->add("Nama","yka_nama");
$adapter ->add("Nomor","yka_number");
$adapter ->add("NRM","yka_nrm");
$adapter ->add("KTP","yka_ktp");
$adapter ->add("Telp","yka_telp");
$adapter ->add("No. BPJS","yka_no_bpjs");
$adapter ->add("Cara Bayar","yka_carabayar");
$adapter ->add("Poli","yka_poli");
$adapter ->add("Kode","yka_code");
$adapter ->add("YKA ID","yka_id");

$master ->setModalTitle("Yuk Antri");
$master ->addResouce("js","registration/resource/js/yuk_antri.js");
$master ->addRegulerData("tanggal","tanggal","id-value");
$master ->initialize();
