<?php

global $db;
require_once "registration/class/responder/BPJSRujukanResponder.php";
require_once "registration/class/table/RujukanVClaimTable.php";
$uitable = new RujukanVClaimTable(array("No.","No Rujukan","No SEP","Tanggal","PPK Rujukan","Jenis Pelayanan","Catatan","Diagnosa","Rujukan Ke","Tipe"));
$uitable ->setName("rujukan_bpjs");
$uitable ->setPrintElementButtonEnable(true);
if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    $super          = new SuperCommand ();
    if($_POST['super_command']=="rujukan_bpjs_icd"){
        require_once "smis-base/smis-include-service-consumer.php";
        $array          = array ("Kode",'DTD','Nama',"Sebab" );
        $muitable       = new Table($array);
        $muitable       ->setModel(Table::$SELECT)
                        ->setName("rujukan_bpjs_icd");
        $madapter       = new SimpleAdapter();
        $madapter       ->add("Nama","nama")
                        ->add("Kode","icd")
                        ->add("DTD","dtd")
                        ->add("Sebab","sebab");
        $mr_mresponder  = new ServiceResponder($db, $muitable, $madapter,"get_diagnosa_responder","medical_record");
        $super->addResponder ( "rujukan_bpjs_icd", $mr_mresponder );    
    }else if($_POST['super_command']=="rujukan_bpjs_ppk"){
        $array          = array ("Kode",'Nama' );
        $muitable       = new Table($array);
        $muitable       ->setModel(Table::$SELECT)
                        ->setName("rujukan_bpjs_ppk");
        $madapter       = new SimpleAdapter();
        $madapter       ->add("Nama","nama")
                        ->add("Kode","kode");

        $dntable       = new DBTable($db,"smis_rg_refpolibpjs");
        $dntable ->addCustomKriteria("grup","='faskes'");
        $mr_mresponder  = new DBResponder($dntable,$muitable, $madapter);
        $super->addResponder ( "rujukan_bpjs_ppk", $mr_mresponder );    
    }else if($_POST['super_command']=="rujukan_bpjs_vclaim"){
        $array          = array ("Nama",'No. BPJS',"No. SEP","Tanggal" );
        $muitable       = new Table($array);
        $muitable       ->setModel(Table::$SELECT)
                        ->setName("rujukan_bpjs_vclaim");
        $madapter       = new SimpleAdapter();
        $madapter       ->add("Nama","nama")
                        ->add("No. BPJS","noKartu")
                        ->add("No. SEP","noSEP")
                        ->add("Tanggal","tglSep","date d M Y");

        $dntable       = new DBTable($db,"smis_rg_vsep");
        $mr_mresponder  = new DBResponder($dntable,$muitable, $madapter);
        $super->addResponder ( "rujukan_bpjs_vclaim", $mr_mresponder );    
    }
    $init           = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

if(isset($_POST['command']) && $_POST['command']!=""){
    $adapter = new SimpleAdapter();
    $adapter ->setUseNumber(true,"No.","back.")
             ->add("No SEP","nosep")
             ->add("No Rujukan","no_rujukan")
             ->add("Tanggal","tanggal","date d M Y")
             ->add("PPK Rujukan","ppk")
             ->add("Jenis Pelayanan","jenis","trivial_1_Rawat Jalan_Rawat Inap")
             ->add("Catatan","catatan")
             ->add("Diagnosa","diagnosa")
             ->add("Rujukan Ke","nama_poli")
             ->add("Tipe","tipe","trivial_0_Penuh_1_Parsial_Rujuk Balik");
    $dbtable = new DBTable($db,"smis_rg_sep_rujukan");
    $dbres   = new BPJSRujukanResponder($dbtable,$uitable,$adapter);
    $data    = $dbres->command($_POST['command']);
    echo json_encode($data); 
    return;
}


$dbtable = new DBTable($db,"smis_rg_refpolibpjs");
$dbtable->addCustomKriteria(" grup","='poli' ");
$dbtable->setShowAll(true);
$data = $dbtable->view("","0");
$adapter = new SelectAdapter("nama","kode");
$result = $adapter ->getContent($data['data']);

$tipe    = new OptionBuilder();
$tipe    ->add("Penuh","0",1)
         ->add("Parsial","1",0)
         ->add("Rujuk Balik","2",0);

$jenis   = new OptionBuilder();
$jenis   ->add("Rawat Jalan","2",0)
         ->add("Rawat Inap","1",1);
$uitable ->addModal("id","hidden","","")
         ->addModal("nosep","chooser-rujukan_bpjs-rujukan_bpjs_vclaim-No SEP","No. SEP","")
         ->addModal("nama_pasien","text","Nama","","n",null,true)
         ->addModal("no_bpjs","text","No. BPJS","","n",null,true)
         ->addModal("jk","hidden","","","n",null,true)
         ->addModal("no_rujukan","text","No. Rujukan","","y",null,true)
         ->addModal("tanggal","date","Tanggal",date("Y-m-d"))
         ->addModal("nama_ppk","chooser-rujukan_bpjs-rujukan_bpjs_ppk-Faskes Rujukan","Nama Faskes","")
         ->addModal("ppk","text","Kode PPK","")
         ->addModal("jenis","select","Jenis",$jenis->getContent())
         ->addModal("diagnosa","chooser-rujukan_bpjs-rujukan_bpjs_icd-Kode ICD","ICD Diagnosa","")
         ->addModal("nama_diagnosa","text","Diagnosa","","n",null,true)
         ->addModal("tipe","select","Tipe Rujukan",$tipe->getContent())
         ->addModal("poli","select","Poli",$result)
         ->addModal("catatan","textarea","Catatan","");

$modal = $uitable->getModal();
$modal ->setTitle("Rujukan BPJS");
echo $uitable ->getHtml();
echo $modal ->getHtml();
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("registration/resource/js/rujukan_bpjs.js",false);
?>