<?php 
global $db;
$uitable=new Table("");
$uitable->setName("cek_bpjs");

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    require_once "smis-base/smis-include-service-consumer.php";
    $array=array ("Kode",'DTD','Nama',"Sebab" );
    $muitable = new Table ( $array);
    $muitable->setModel ( Table::$SELECT );
    $muitable->setName ( "cek_bpjs_icd" );
    $madapter = new SimpleAdapter ();
    $madapter->add ( "Nama", "nama" );
    $madapter->add ( "Kode", "icd" );
    $madapter->add ( "DTD", "dtd" );
    $madapter->add ( "Sebab", "sebab" );
    $mr_mresponder = new ServiceResponder ( $db, $muitable, $madapter,"get_diagnosa_responder","medical_record" );
    
    $super = new SuperCommand ();
    $super->addResponder ( "cek_bpjs_icd", $mr_mresponder );
    $init = $super->initialize ();
    if ($init != null) {
        echo $init;
        return;
    }
}

if(isset($_POST['command']) &&  $_POST['command']!=""){
    require_once "registration/class/responder/BPJSCheckerResponder.php";
    $dbtable=new DBTable($db,"smis_rg_sep");
    $adapter=new SimpleAdapter();
    $dbres=new BPJSCheckerResponder($dbtable,$uitable,$adapter);
    $data=$dbres->command($_POST['command']);
    echo json_encode($data);
    return;
}

$uitable->addModal("no_bpjs","text","No. BPJS","");
$uitable->addModal("no_ktp","text","No. KTP","");
$form=$uitable->getModal()->getForm();
$cek=new Button("cek_bpjs_button","cek_bpjs_button","Cek BPJS");
$cek->setAction("cek_bpjs.fetch_server()");
$cek->setIsButton(Button::$ICONIC_TEXT);
$cek->setIcon(" fa fa-id-card");
$cek->setClass(" btn btn-primary");

$rujukan=new Button("","","Cek Rujukan");
$rujukan->setAction("cek_bpjs.cek_rujukan()");
$rujukan->setIsButton(Button::$ICONIC_TEXT);
$rujukan->setIcon(" fa fa-arrows");
$rujukan->setClass(" btn btn-success");

$reset=new Button("","","Reset");
$reset->setAction("cek_bpjs.reset()");
$reset->setIsButton(Button::$ICONIC_TEXT);
$reset->setIcon(" fa fa-refresh");
$reset->setClass(" btn btn-danger");

$grup=new ButtonGroup("","","");
$grup->addButton($cek);
$grup->addButton($rujukan);
$grup->addButton($reset);

$form->addElement("",$grup);

$jenis=new OptionBuilder();
$jenis->add("","",1);
//$jenis->add("IGD","0",0);
$jenis->add("Rawat Inap","1",0);
$jenis->add("Rawat Jalan","2",0);

$polidb=new DBTable($db,"smis_rg_refpolibpjs");
$polidb->addCustomKriteria("status","='1'");
$polidb->addCustomKriteria("grup","='poli'");
$polidb->setOrder(" nama ASC ",true);
$polidb->setShowAll(true);
$d=$polidb->view("",0);
$list=$d['data'];
$adapter=new SelectAdapter("nama","kode");
$content=$adapter->getContent($list);
array_push($content,array("name"=>"","value"=>"","default"=>1));

$kelas=new OptionBuilder();
$kelas->add("","",1);
$kelas->add("Kelas I","1",0);
$kelas->add("Kelas II","2",0);
$kelas->add("Kelas III/Rawat Jalan","3",0);

$laka=new OptionBuilder();
$laka->add("Kasus Kecelakaan","1",0);
$laka->add("Bukan Kasus Kecelakaan","2",1);
$ppkPelayanan=getSettings($db,"reg-bpjs-sep-api-ppk-pelayanan","");
$bpjs_user=getSettings($db,"reg-bpjs-sep-api-user","smis");
$uitable->clearContent();
$uitable->addModal("noKartu","text","No BPJS","","n",null,false);
$uitable->addModal("tglSep","datetime","Tgl SEP",date("Y-m-d H:i:s"),"n",null,false);
$uitable->addModal("tglRujukan","datetime","Tgl Rujukan",date("Y-m-d H:i:s"),"n");
$uitable->addModal("noRujukan","text","No Rujukan","","y",null);
$uitable->addModal("ppkRujukan","text","PPK Rujukan","","y",null);
$uitable->addModal("ppkPelayanan","text","PPK Pelayanan",$ppkPelayanan,"n",null,true);
$uitable->addModal("jnsPelayanan","select","Jenis Layanan",$jenis->getContent(),"n",null);
$uitable->addModal("diagAwal","chooser-cek_bpjs-cek_bpjs_icd-ICD 10 Diagnosa","Kode Diagnosa","","n",null);
$uitable->addModal("poliTujuan","select","Poli Tujuan",$content,"n",null);
$uitable->addModal("klsRawat","select","Kelas",$kelas->getContent(),"n",null);
$uitable->addModal("lakaLantas","select","Kasus Laka",$laka->getContent(),"n",null);
$uitable->addModal("lokasiLaka","text","Lokasi Laka","","y",null,true);
$uitable->addModal("catatan","textarea","Catatan","","y",null);
$uitable->addModal("user","text","User SEP",$bpjs_user,"n",null,true);
$uitable->addModal("noMr","text","NRM Pasien","","n",null,true);

$formsep=$uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$cek=new Button("","","Register SEP");
$cek->setAction("cek_bpjs.reg_sep()");
$cek->setIsButton(Button::$ICONIC_TEXT);
$cek->setIcon(" fa fa-upload");
$cek->setClass(" btn btn-primary");
$formsep->addElement("",$cek);

$uitable->clearContent();
$uitable->addModal("noSEP","text","No SEP","","n",null,true);
$formnosep=$uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$print=new Button("","","Cetak SEP");
$print->setAction("cek_bpjs.print_sep()");
$print->setIsButton(Button::$ICONIC_TEXT);
$print->setIcon(" fa fa-print");
$print->setClass(" btn btn-primary");
$formnosep->addElement("",$print);

$uitable->clearContent();
$uitable->addModal("noREG","text","No REG","","n",null,true);
$formnorereg=$uitable->getModal()->setComponentSize(Modal::$MEDIUM)->getForm();
$reg=new Button("","","Register Pasien");
$reg->setAction("cek_bpjs.register_pasien_bpjs()");
$reg->setIsButton(Button::$ICONIC_TEXT);
$reg->setIcon(" fa fa-sign-in");
$reg->setClass(" btn btn-primary");

$fix=new Button("","","Fix Register");
$fix->setAction("cek_bpjs.fix_register()");
$fix->setIsButton(Button::$ICONIC_TEXT);
$fix->setIcon(" fa fa-eraser");
$fix->setClass(" btn btn-success");

$sep=new Button("","","SEP Sementara");
$sep->setAction("cek_bpjs.sep_sementara()");
$sep->setIsButton(Button::$ICONIC_TEXT);
$sep->setIcon(" fa fa-tag");
$sep->setClass(" btn btn-danger");

$btngroup=new ButtonGroup("");
$btngroup->addButton($reg);
$btngroup->addButton($fix);
$formnorereg->addElement("",$btngroup);
$formnorereg->addElement("",$sep);


$rows=new RowSpan();
$rows->addSpan("<h5>REGISTRASI BPJS/SEP</h5><div class='clear'></div><div>".$formsep->getHtml()." <div class='clear'></div>".$formnosep->getHtml()." <div class='clear'></div>".$formnorereg->getHtml()." </div>",7);
$rows->addSpan("<h5>DATA PESERTA BPJS</h5><div class='clear'></div><div id='cek_bpjs_page'>Silakan Masukan No. BPJS atau No. KTP dibagian Pencarian dan Hasilnya akan di tampilkan !!!</div>",5);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo $rows->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
echo addJS("framework/smis/js/table_action.js");
echo addJS("registration/resource/js/cek_bpjs.js",false);
echo addCSS("registration/resource/css/sep_table.css",false);
?>