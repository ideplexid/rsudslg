<?php 
	global $db;
	global $user;
	require_once "smis-base/smis-include-service-consumer.php";
	require_once "smis-libs-class/MasterTemplate.php";
	require_once "smis-libs-class/MasterSlaveTemplate.php";
	require_once "registration/class/responder/ReplacePasienResponder.php";
	require_once "registration/class/responder/ReplacePasienListResponder.php";
	
	
	$master=new MasterSlaveTemplate($db,"smis_rg_replaced","registration","replace_pasien");
	$responder=new ReplacePasienListResponder($master->getDBtable(),$master->getUItable(),$master->getAdapter());
	$master->setDBresponder($responder);
	$uitable=$master->getUItable();
	$uitable->addModal("nrm_pasien", "chooser-replace_pasien-replace_pasien_patient-Pasien Lama", "NRM Lama","","n",null,true);
	$uitable->addModal("nama_pasien", "text", "Nama Lama","","n",null,true);
	$uitable->addModal("noreg_pasien", "chooser-replace_pasien-replace_pasien_layanan-Registrasi Pasien", "No Reg","","n",null,true);
	$uitable->addModal("nrm_pasien_baru", "chooser-replace_pasien-replace_pasien_patient_baru-Pasien Baru", "NRM Baru","","n",null,true);
	$uitable->addModal("nama_pasien_baru", "text", "Nama Baru","","n",null,true);
	
	$button=new Button("","","Proses");
	$button->setAction("replace_pasien.proceed()");
	$button->setClass("btn btn-primary");
	$button->setIcon(" fa fa-send");
	$button->setIsButton(Button::$ICONIC_TEXT);
	
	$button_ref=new Button("","","");
	$button_ref->setAction("replace_pasien.restore()");
	$button_ref->setClass("btn btn-inverse");
	$button_ref->setIcon(" fa fa-chrome");
	$button_ref->setIsButton(Button::$ICONIC);
	
	$form=$master->getForm();
	$form->addElement("",$button);
	$uitable->addHeaderElement("No.");
	$uitable->addHeaderElement("Nama Lama");
	$uitable->addHeaderElement("NRM Lama");
	$uitable->addHeaderElement("Nama Baru");
	$uitable->addHeaderElement("NRM Baru");
	$uitable->addHeaderElement("No Reg");
	$uitable->addHeaderElement("User");
	$uitable->addHeaderElement("Waktu");
	$uitable->addContentButton("restore",$button_ref);
	$uitable->setDelButtonEnable(false);
	$uitable->setAddButtonEnable(false);
	$uitable->setEditButtonEnable(false);
	$uitable->setReloadButtonEnable(false);
	$adapter=$master->getAdapter();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add("Nama Lama","nama_pasien");
	$adapter->add("Nama Baru","nama_pasien_baru");
	$adapter->add("NRM Lama","nrm_pasien","only-digit8");
	$adapter->add("NRM Baru","nrm_pasien_baru","only-digit8");
	$adapter->add("No Reg","noreg_pasien");
	$adapter->add("User","user");
	$adapter->add("Waktu","waktu","date d M Y H:i");
	$master->getDBResponder()->addColumnFixValue("user",$user->getNameOnly());
	$master->getDBtable()->setOrder(" id DESC ");
	$master->addResouce("js","registration/resource/js/replace_pasien.js");
	
	if(isset($_POST['super_command']) && $_POST['super_command']=="replace_pasien_layanan" ){
		$p_dbtable=new DBTable($db,"smis_rgv_layananpasien");
		$p_dbtable->setOrder(" nrm ASC, no_kunjungan DESC ");
		$p_uitable=new Table(array("No. Kunj.","No. Reg","Nama","NRM","Alamat"));
		$p_uitable->setModel(Table::$SELECT);
		$p_uitable->setName("replace_pasien_layanan");
		$p_adapter=new SimpleAdapter();
		$p_adapter->add("No. Reg","id","only-digit8");
		$p_adapter->add("Nama","nama_pasien");
		$p_adapter->add("No. Kunj.","no_kunjungan");
		$p_adapter->add("NRM","nrm","only-digit8");
		$p_adapter->add("Alamat","alamat_pasien");
		$p_responder=new ReplacePasienResponder($p_dbtable,$p_uitable,$p_adapter);	
		$master->getSuperCommand()->addResponder("replace_pasien_layanan",$p_responder);		
	}
	$master->addSuperCommand("replace_pasien_layanan", array());
	
	if(isset($_POST['super_command']) && $_POST['super_command']=="replace_pasien_patient" ){
		$p_dbtable=new DBTable($db,"smis_rg_patient");
		$p_uitable=new Table(array("Nama","NRM","Alamat"));
		$p_uitable->setModel(Table::$SELECT);
		$p_uitable->setName("replace_pasien_patient");
		$p_adapter=new SimpleAdapter();
		$p_adapter->add("Nama","nama");
		$p_adapter->add("NRM","id","only-digit8");
		$p_adapter->add("Alamat","alamat");
		$p_responder=new ReplacePasienResponder($p_dbtable,$p_uitable,$p_adapter);	
		$master->getSuperCommand()->addResponder("replace_pasien_patient",$p_responder);		
	}
	$master->addSuperCommand("replace_pasien_patient", array());
	
	if(isset($_POST['super_command']) && $_POST['super_command']=="replace_pasien_patient_baru" ){	
		$p_dbtable=new DBTable($db,"smis_rg_patient");
		$p_uitable=new Table(array("Nama","NRM","Alamat"));
		$p_uitable->setModel(Table::$SELECT);
		$p_uitable->setName("replace_pasien_patient_baru");
		$p_adapter=new SimpleAdapter();
		$p_adapter->add("Nama","nama");
		$p_adapter->add("NRM","id","only-digit8");
		$p_adapter->add("Alamat","alamat");
		$p_responder=new DBResponder($p_dbtable,$p_uitable,$p_adapter);	
		$master->getSuperCommand()->addResponder("replace_pasien_patient_baru",$p_responder);
	}
	$master->addSuperCommand("replace_pasien_patient_baru", array());
	$master->addSuperCommandArray("replace_pasien_patient_baru","nrm_pasien_baru","id");
	$master->addSuperCommandArray("replace_pasien_patient_baru","nama_pasien_baru","nama");
	
	
	$master->initialize();
?>