<?php 
/**
 * dipakai untuk memulangkan pasien yang sudah pulang dari ruangan lain 
 * tetapi belum di pulangkan dari kasir
 * */
global $db;
require_once "smis-base/smis-include-service-consumer.php";
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_rg_layananpasien");
	$dbtable->addCustomKriteria(NULL," selesai ='0' ");
	if(isset($_POST['dari']) && $_POST['dari']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal >='". $_POST['dari']."' ");
	if(isset($_POST['sampai']) && $_POST['sampai']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal <'". $_POST['sampai']."' ");
	if(isset($_POST['carabayar']) && $_POST['carabayar']!="") 
		$dbtable->addCustomKriteria(NULL," carabayar ='". $_POST['carabayar']."' ");	
	$q=$dbtable->getQueryCount("");
	$total=$db->get_var($q);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($total);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();	
	$dbtable=new DBTable($db, "smis_rgv_layananpasien");
	$dbtable->addCustomKriteria(NULL," selesai ='0' ");	
	if(isset($_POST['dari']) && $_POST['dari']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal >='". $_POST['dari']."' ");
	if(isset($_POST['sampai']) && $_POST['sampai']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal <'". $_POST['sampai']."' ");
	if(isset($_POST['carabayar']) && $_POST['carabayar']!="") 
		$dbtable->addCustomKriteria(NULL," carabayar ='". $_POST['carabayar']."' ");	
	$dbtable->setMaximum(1);
	$data=$dbtable->view("",$_POST['halaman']);
	$datalist=$data['data'];
	$x=$datalist[0];

	$noreg=$x->id;
	$serv=new ServiceConsumer($db,"recheck_pasien");
	$serv->setMode(ServiceConsumer::$MULTIPLE_MODE);
	$serv->addData("noreg_pasien",$noreg);
	$serv->execute();
	$data=$serv->getContent();
	$ruangan_belum="";
	foreach($data as $one=>$k){
		foreach($k as $v=>$xy){
			if($xy==0 || $xy=="0"){
				$ruangan_belum.=ArrayAdapter::format("unslug",$v)." , ";
			}
		}
	}
	
	$content="";
	$status=0;
	if($ruangan_belum==""){
        
        global $user;
        $username=$user->getUsername();
        $opp="[4|".$username."]";
            
        
		$dbtable->setName("smis_rg_layananpasien");
		$up=array();
		$up['selesai']="1";
		$up['tanggal_pulang']=date("Y-m-d H:i:s");
		$up['carapulang']="Dipulangkan Hidup";
        $up['obs']='obs|4';
        $up['opp']="concat(opp,' ','".$opp."')";        
        $warp=array();
        $warp['obs']=1;
        $warp['opp']=2;
		$id=array();
		$id['id']=$x->id;
		$dbtable->update($up,$id,$warp);
		$content="<tr>
						<td>".$x->nama_pasien."</td>
						<td>".ArrayAdapter::format("digit8",$x->id)."</td>
						<td>".ArrayAdapter::format("digit8",$x->nrm)."</td>
						<td>".ArrayAdapter::format("date d-M-Y",$x->tanggal)."</td>
						<td>".$x->carabayar."</td>
						<td>Dipulangkan Hidup</td>
					</tr>";
		$status="1";
	}else{
		$content="<tr>
						<td>".$x->nama_pasien."</td>
						<td>".ArrayAdapter::format("digit8",$x->id)."</td>
						<td>".ArrayAdapter::format("digit8",$x->nrm)."</td>
						<td>".ArrayAdapter::format("date d-M-Y",$x->tanggal)."</td>
						<td>".$x->carabayar."</td>
						<td>".$ruangan_belum."</td>
					</tr>";
		$status="0";
	}
	$result=array("content"=>$content,"status"=>$status);	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($result);
	echo json_encode($res->getPackage());
	return;
}
$array=array("Nama","No Reg","NRM","Tanggal Masuk","Carabayar","Status");
$uitable=new Table($array);
$uitable->setName("recheck_pasien")
		->setActionEnable(false)
		->setFooterVisible(false);
		
require_once "registration/class/RegistrationResource.php";
$jenis=RegistrationResource::getJenisPembayaran();
$jenis[]=array("name"=>"","value"=>"","default"=>"1");

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("carabayar", "select", "Carabayar", $jenis);
		
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("recheck_pasien.rekaptotal()");

$btng=new ButtonGroup("");
$btng->addButton($action);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("recheck_pasien.batal()");

$load=new LoadingBar("rekap_recheck_pasien_bar", "");
$modal=new Modal("rekap_recheck_pasien_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_recheck_pasien'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");

?>

<script type="text/javascript">
	var recheck_pasien;
	var IS_recheck_pasien_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		recheck_pasien=new TableAction("recheck_pasien","registration","recheck_pasien",new Array());
		recheck_pasien.addRegulerData=function(data){
			data['dari']=$("#recheck_pasien_dari").val();
			data['sampai']=$("#recheck_pasien_sampai").val();		
			data['carabayar']=$("#recheck_pasien_carabayar").val();							
			return data;
		};

		recheck_pasien.batal=function(){
			IS_recheck_pasien_RUNNING=false;
			$("#rekap_recheck_pasien_modal").modal("hide");
		};
		
		recheck_pasien.afterview=function(json){
			if(json!=null){
				$("#kode_table_recheck_pasien").html(json.nomor);
				$("#waktu_table_recheck_pasien").html(json.waktu);
				recheck_pasien_data=json;
			}
		};

		recheck_pasien.rekaptotal=function(){
			if(IS_recheck_pasien_RUNNING) return;
			$("#rekap_recheck_pasien_bar").sload("true","Fetching total data",0);
			$("#rekap_recheck_pasien_modal").modal("show");
			$("#recheck_pasien_list").html("");
			IS_recheck_pasien_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					recheck_pasien.rekaploop(0,total);
				} else {
					$("#rekap_recheck_pasien_modal").modal("hide");
					IS_recheck_pasien_RUNNING=false;
				}
			});
		};

		recheck_pasien.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		recheck_pasien.rekaploop=function(current,total){
			if(current>=total || !IS_recheck_pasien_RUNNING) {
				IS_recheck_pasien_RUNNING=false;
				$("#rekap_recheck_pasien_modal").modal("hide");
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#recheck_pasien_list").append(ct.content);
				$("#rekap_recheck_pasien_bar").sload("true"," Processing ... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				if(ct.status=="1"){
					setTimeout(function(){recheck_pasien.rekaploop(current,--total)},300);
				}else{
					setTimeout(function(){recheck_pasien.rekaploop(++current,total)},300);
				}
				
			});
		};
		
		
		
		
	});
</script>