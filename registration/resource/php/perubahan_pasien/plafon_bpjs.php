<?php

global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-class/MasterSlaveTemplate.php';

$header=array ("No.","No Reg","NRM",'Nama','BPJS',"Plafon","Tanggal","Kamar","Status");
$plafon=new MasterSlaveTemplate($db, "smis_rg_layananpasien", "registration", "plafon_bpjs");
$plafon	->getDBtable()
		->setViewForSelect(true,false)
		->setPreferredView(true, "smis_rgv_uri")
		->addCustomKriteria("carabayar"," LIKE '%bpjs%'");

$plafon ->getAdapter()
        ->setUseNumber(true, "No.","back.")
		->add("No Reg", "id","only-digit9")
		->add("NRM", "nrm","only-digit8")
		->add("Nama", "nama_pasien")
		->add("BPJS","nobpjs")
		->add("Plafon","plafon_bpjs","money Rp.")
		->add("Status","kunci_bpjs","trivial_0_<i class='fa fa-unlock fa-2x' style=\"color: rgb(218, 79, 73);\"  ></i>_<i class='fa fa-lock fa-2x' style=\"color:rgb(71, 99, 158);\"></i>")
		->add("Tanggal", "tanggal","date d M Y H:i")
		->add("Kamar", "kamar_inap","unslug");
if($plafon->getDBResponder()->isPreload()){
    require_once "registration/class/RegistrationResource.php";
    $ctx=RegistrationResource::getURJIP($db);
    $ruangan=RegistrationResource::getURIFromContent($ctx);
    $ruangan[]=array("name"=>"","value"=>"","default"=>"1");
    $plafon ->getUItable()
            ->addModal("ruangan", "select", "Ruangan", $ruangan);
    $btn=new Button("","","");
    $btn->setIsButton(Button::$ICONIC);
    $btn->setIcon(" fa fa-refresh");
    $btn->setAction("plafon_bpjs.view()");
    $btn->setClass("btn-primary");
    $plafon->getForm()->addElement("", $btn);	
}

if($plafon->getDBResponder()->isView()){
    if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
        $plafon->getDBtable()->addCustomKriteria(" kamar_inap "," ='".$_POST['ruangan']."' ");        
    }
}
   
$plafon ->getUItable()
		->setHeader($header)
		->setDelButtonEnable(false)
		->setAddButtonEnable(false)
		->addModal("id", "hidden", "", "")
		->addModal("nama_pasien", "text", "Pasien", "","n",NULL,true)
		->addModal("nama_dokter", "text", "Dokter", "","y",NULL,true)
		->addModal("nrm", "text", "NRM", "","n",NULL,true)
		->addModal("nobpjs", "text", "No BPJS", "","y",NULL,true)
		->addModal("diagnosa_bpjs", "text", "Diagnosa", "","y",NULL,false,NULL,true)
		->addModal("tindakan_bpjs", "text", "Tindakan", "")
		->addModal("kunci_bpjs", "checkbox", "Kunci Rencana", "0")
		->addModal("plafon_bpjs", "money", "Plafon", "0")
		->addModal("keterangan_bpjs", "textarea", "Keterangan", "");
$plafon->addJSColumn("nama_pasien");
$plafon->addJSColumn("nama_dokter");
$plafon->addJSColumn("id");
$plafon->addJSColumn("nrm");
$plafon->addJSColumn("nobpjs");
$plafon->addJSColumn("plafon_bpjs");
$plafon->addJSColumn("diagnosa_bpjs");
$plafon->addJSColumn("tindakan_bpjs");
$plafon->addJSColumn("keterangan_bpjs");
$plafon->addJSColumn("kunci_bpjs");
$plafon->addViewData("ruangan", "ruangan");
$plafon->getModal()
		->setTitle("Plafon");
$plafon->setAutoReload(true);
$plafon->initialize();
		
?>

