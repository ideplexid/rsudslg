<?php 
	require_once 'smis-libs-class/MasterTemplate.php';
	require_once "registration/class/responder/PasienAktifResponder.php";
	global $db;
	
	$paktif=new MasterTemplate($db, "smis_rg_layananpasien", "registration", "pasien_aktif");
	
	$paktif->addResouce("js", "registration/resource/js/pasien_aktif.js");
	$column=array("id","status_dokumen");
	$dokumen=isset($_POST['dokumen'])?$_POST['dokumen']:"";
	if($dokumen=="Belum") $dokumen="";
	$paktif	->getDBtable()
			->setColumn($column)
			->addCustomKriteria(" selesai ", "LIKE '%".(isset($_POST['selesai'])?$_POST['selesai']:"")."%'")
			->addCustomKriteria(" uri ", "LIKE '%".(isset($_POST['uri'])?$_POST['uri']:"")."%'")
			->addCustomKriteria(" status_dokumen ", " LIKE '".$dokumen."'")
			->setPreferredView(true, "smis_rgv_layananpasien", $column);
			
	if(isset($_POST['nrm']) && $_POST['nrm']!=""){
		$paktif	->getDBtable()
				->addCustomKriteria(" nrm ", " = '".$_POST['nrm']."'");
	}
	
	if(isset($_POST['jenis']) && $_POST['jenis']!="%"){
		$paktif	->getDBtable()
				->addCustomKriteria(" jenis ", "LIKE '%".(isset($_POST['jenis'])?$_POST['jenis']:"")."%'");
	}
	
	if(isset($_POST['urutan']) && $_POST['urutan']=="terbaru"){
		$paktif	->getDBtable()->setOrder(" id DESC ");
	}else{
		$paktif	->getDBtable()->setOrder(" id ASC ");
	}
	
	$paktif	->getDBtable()->setOrder(" id DESC ");
	
	$paktif->setModalTitle("Pasien Aktif");
	$paktif->setFormVisibility(true);
	$uitable=$paktif->getUItable();
	$uitable->setAddButtonEnable(false);
	$uitable->setEditButtonEnable(false);
	$uitable->setDelButtonEnable(false);
	$uitable->setPrintButtonEnable(false);
	$uitable->setReloadButtonEnable(false);
	
	
	$sudah=new Button("", "", "");
	$sudah->setClass("btn btn-primary");
	$sudah->setIsButton(Button::$ICONIC);
	$sudah->setIcon("fa fa-check");
	
	$return=new Button("", "", "");
	$return->setClass("btn btn-primary");
	$return->setIsButton(Button::$ICONIC);
	$return->setIcon("fa fa-reply");
	
	
	$belum=new Button("", "", "");
	$belum->setClass("btn btn-primary");
	$belum->setIsButton(Button::$ICONIC);
	$belum->setIcon("fa fa-circle-o-notch");
	
	$track=new Button("", "", "");
	$track->setClass("btn-inverse");
	$track->setIsButton(Button::$ICONIC);
	$track->setIcon("fa fa-book");
	
	$uitable->addContentButton("sudah", $sudah);
	$uitable->addContentButton("belum", $belum);
	$uitable->addContentButton("kembali", $return);
	$uitable->addContentButton("track", $track);
	
	$header=array("Waktu","NRM","No Reg","Nama","Alamat","Layanan","Jenis Pasien","Pembayaran","Kedatangan","Kepulangan","URI/URJ","Dokumen");
	$uitable->setHeader($header);
	$jenis=new OptionBuilder();
	$jenis->add("Spesialis","spesialis");
	$jenis->add("Umum","umum");
	$jenis->add("","%","1");
	
	$uri=new OptionBuilder();
	$uri->add("Rawat Jalan","0");
	$uri->add("Rawat Inap","1");
	$uri->add("","%","1");
	
	$selesai=new OptionBuilder();
	$selesai->add("Berlangsung","0");
	$selesai->add("Selesai","1");
	$selesai->add("","%","1");
	
	
	$dokumen=new OptionBuilder();
	$dokumen->add("Terkirim","terkirim");
	$dokumen->add("Belum","");
	$dokumen->add("Kembali","kembali");
	$dokumen->add("","%","1");
	
	$urutan=new OptionBuilder();
	$urutan->add("Terbaru","terbaru");
	$urutan->add("Terlama","terlama");
	
	$uitable->addModal("nrm","text", "NRM", "");
	$uitable->addModal("jenis","select", "Jenis", $jenis->getContent());
	$uitable->addModal("uri","select", "Rawat", $uri->getContent());
	$uitable->addModal("dokumen","select", "Dokumen", $dokumen->getContent());
	$uitable->addModal("selesai","select", "Pelayanan", $selesai->getContent());
	$uitable->addModal("urutan","select", "Urutkan", $urutan->getContent());
	
	$adapter=$paktif->getAdapter();
	$adapter->add("Waktu", "tanggal","date d M Y H:i:s")
			->add("No Reg", "id","digit8")
			->add("NRM", "nrm","digit8")
			->add("Alamat", "alamat_pasien")
			->add("Nama","nama_pasien")
			->add("Pembayaran","carabayar")
			->add("Layanan","jenislayanan","unslug")
			->add("Kedatangan","caradatang")
			->add("Jenis Pasien","jenis","unslug")
			->add("Dokumen","status_dokumen","case|terkirim-Terkirim|kembali-Kembali|Belum")
			->add("Kepulangan","carapulang","exception__Berlangsung")
			->add("URI/URJ","uri","trivial_1_Rawat Inap_Rawat Jalan");
	$btn=new Button("", "", "");
	$btn->setClass("btn btn-primary");
	$btn->setIsButton(Button::$ICONIC);
	$btn->setIcon("fa fa-refresh");
	$btn->setAction("pasien_aktif.view()");
	$paktif->getModal()->clearFooter()->addFooter($btn);
	$paktif->addViewData("jenis", "jenis","id-value");
	$paktif->addViewData("uri", "uri","id-value");
	$paktif->addViewData("dokumen", "dokumen","id-value");
	$paktif->addViewData("selesai", "selesai","id-value");
	$paktif->addViewData("urutan", "urutan","id-value");
	$paktif->addViewData("nrm", "nrm","id-value");
	
	$pasien_aktif_responder=new PasienAktifResponder($paktif->getDBtable(),$paktif->getUItable(),$paktif->getAdapter());
	$paktif->setDBresponder($pasien_aktif_responder);
	$paktif->initialize();
?>