<?php
global $db;
$head=array ('Masuk','Tgl Pulang','Inap','Nama','NRM',"No Reg",'Ruang',"Kamar Inap",'Jenis Pasien','Status','Cara Pulang');
$uitable = new Table ( $head, "Registrasi Pasien", NULL, true );
$uitable->setName ( "lap_register" );
$uitable->setFixHeaderOnScroll(true);
$uitable->setAddButtonEnable(false);
$uitable->setDelButtonEnable(false);
$uitable->setPrintButtonEnable(false);
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama_pasien" );
	$adapter->add ( "No Reg", "id","digit10" );
	$adapter->add ( "Masuk", "tanggal","date d M Y H:i:s" );
	$adapter->add ( "Tgl Pulang", "tanggal_pulang","date d M Y H:i:s" );
	$adapter->add ( "Inap", "tanggal_inap","date d M Y H:i:s" );
	$adapter->add ( "NRM", "nrm","digit8" );
	$adapter->add ( "Ruang", "jenislayanan","unslug" );
	$adapter->add ( "Jenis Pasien", "carabayar","unslug" );
	$adapter->add ( "Cara Pulang", "carapulang","unslug" );
	$adapter->add ( "Kamar Inap", "kamar_inap","unslug" );
	$adapter->add ( "Status", "selesai","trivial_0_Aktif_Pulang" );
	
	$dbtable = new DBTable ( $db, "smis_rg_layananpasien" );
	$dbtable->setPreferredView(true,"smis_rgv_layananpasien");
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

loadLibrary("smis-libs-function-medical");
$uitable->addModal("id", "hidden", "", "");
$uitable->addModal("selesai", "checkbox", "Selesai", "");
$uitable->addModal("carapulang", "select", "Cara Pulang", medical_carapulang());
$uitable->addModal("uri", "checkbox", "Rawat Inap", "");
$uitable->addModal("tanggal", "datetime", "Tanggal", "");
$uitable->addModal("tanggal_inap", "datetime", "Tanggal Inap", "");
$uitable->addModal("tanggal_pulang", "datetime", "Tanggal Pulang", "");
$uitable->setFixHeaderOnScroll(true);
echo $uitable->getModal()->setTitle("Di Pulangkan")->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
?>
<script type="text/javascript">

var lap_register;
$(document).ready(function(){
	$(".mydatetime").datetimepicker({minuteStep:1});
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id',"selesai","uri","carapulang","tanggal_pulang","tanggal","tanggal_inap");
	lap_register=new TableAction("lap_register","registration","lap_register",column);
	lap_register.view();	
});
</script>



