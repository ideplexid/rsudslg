<?php
global $db;
require_once("smis-base/smis-include-service-consumer.php");
$head=array ('NRM','Nama','Alamat');
$uitable = new Table ( $head, "Bagian", NULL, true );
$uitable->setName ( "ubah_nama_pasien" );
$uitable->setAddButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setDelButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "NRM", "id","digit8" );
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Alamat", "alamat" );
	$dbtable = new DBTable ( $db, "smis_rg_patient" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isSave()){
		/*pencarian noreg yang aktif*/
		$query="SELECT id FROM smis_rg_layananpasien WHERE nrm='".$_POST['id']."' AND selesai=0 AND prop!='del' ORDER BY id DESC;";
		$noreg_pasien=$db->get_var($query);
		
		/*push_service_change_name*/
		$serv=new ServiceConsumer($db,"set_change_patient");
		$serv->addData("noreg_pasien",$noreg_pasien);
		$serv->addData("nama_pasien",$_POST['nama']);
		$serv->execute();
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Perubahan Nama Pasien" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var ubah_nama_pasien;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','alamat');
	ubah_nama_pasien=new TableAction("ubah_nama_pasien","registration","ubah_nama_pasien",column);
	ubah_nama_pasien.view();	
});
</script>
