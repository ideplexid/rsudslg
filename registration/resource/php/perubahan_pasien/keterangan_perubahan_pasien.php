<!-- 
@author 	: Ichwan Qodrian
@copyright	: ichwan.work@gmail.com
@used		: registration/modul/data_induk.php
@since		: 6 Sept 2016
@license	: LGPLv2
@version	: 1.0.0
-->


<h4>Deskripsi Data Induk</h4>
<ul>
	<li><strong>Cek Status</strong><p> Digunakan untuk melihat keberadaan pasien secara data pada SIMRS </p></li>
	<li><strong>Lock Status </strong><p>Digunakan untuk menghapus data pasien yang telah dipulangkan oleh Ruangan Rawat Inap pada SIMRS </p></li>
	<li><strong>Berkas</strong><p> </p>Digunakan untuk menggabungkan nomor Rekam Medis pada pasien yang memiliki nomor Rekam Medis lebih dari satu</li>
	<li><strong>Her-Op</strong><p></p>Digunakan untuk menghapus data Pasien beserta Nomor Rekam Medis-nya</li>
	<li><strong>Nama</strong><p> </p>Digunakan untuk mengubah nama pasien pada SIMRS</li>
	<li><strong>Cara Bayar</strong><p>Digunakan untuk mengubah cara bayar pasien</p></li>
	<li><strong>Status Pasien</strong><p>Digunakan untuk mendeteksi keberadaan Map Status Pasien secara Data pada SIMRS</p></li>
</ul>

