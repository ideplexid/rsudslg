<?php 
	global $db;
	global $user;
	require_once "smis-base/smis-include-service-consumer.php";
	require_once "smis-libs-class/MasterTemplate.php";
	require_once "smis-libs-class/MasterSlaveTemplate.php";
    require_once "registration/class/responder/ChangeNoregResponder.php";
    
	$master=new MasterSlaveTemplate($db,"smis_rg_changed","registration","change_noreg");
	$responder=new ChangeNoregResponder($master->getDBtable(),$master->getUItable(),$master->getAdapter());
	$master->setDBresponder($responder);
	$uitable=$master->getUItable();
	$uitable->addModal("noreg_pasien", "chooser-change_noreg-change_noreg_layanan-Registrasi Lama", "No. Reg Lama","","n",null,true);
	$uitable->addModal("nrm_pasien", "text", "NRM Lama","","n",null,true);
	$uitable->addModal("nama_pasien", "text", "Nama Lama","","n",null,true);
	$uitable->addModal("noreg_pasien_baru", "chooser-change_noreg-change_noreg_baru-Registrasi Baru", "No. Reg Baru","","n",null,true);
	$uitable->addModal("nrm_pasien_baru", "text", "NRM Baru","","n",null,true);
	$uitable->addModal("nama_pasien_baru", "text", "Nama Baru","","n",null,true);
    
	$button=new Button("","","Proses");
	$button->setAction("change_noreg.proceed()");
	$button->setClass("btn btn-primary");
	$button->setIcon(" fa fa-send");
	$button->setIsButton(Button::$ICONIC_TEXT);
	
	$form=$master->getForm();
	$form->addElement("",$button);
	$uitable->addHeaderElement("No.");
	$uitable->addHeaderElement("Nama Lama");
	$uitable->addHeaderElement("NRM Lama");
	$uitable->addHeaderElement("Nama Baru");
	$uitable->addHeaderElement("NRM Baru");
	$uitable->addHeaderElement("No Reg Lama");
	$uitable->addHeaderElement("No Reg Baru");
	$uitable->addHeaderElement("User");
	$uitable->addHeaderElement("Waktu");
	$uitable->setDelButtonEnable(false);
	$uitable->setAddButtonEnable(false);
	$uitable->setEditButtonEnable(false);
	$uitable->setReloadButtonEnable(false);
	$adapter=$master->getAdapter();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add("Nama Lama","nama_pasien");
	$adapter->add("Nama Baru","nama_pasien_baru");
	$adapter->add("NRM Lama","nrm_pasien","only-digit8");
	$adapter->add("NRM Baru","nrm_pasien_baru","only-digit8");
	$adapter->add("No Reg Lama","noreg_pasien");
    $adapter->add("No Reg Baru","noreg_pasien_baru");
	$adapter->add("User","user");
	$adapter->add("Waktu","waktu","date d M Y H:i");
	$master->getDBResponder()->addColumnFixValue("user",$user->getNameOnly());
	$master->getDBtable()->setOrder(" id DESC "); 
	$master->addResouce("js","registration/resource/js/change_noreg.js");
	
	if(isset($_POST['super_command']) && $_POST['super_command']=="change_noreg_layanan" ){
		$p_dbtable=new DBTable($db,"smis_rgv_layananpasien");
		$p_dbtable->setOrder(" nrm ASC, no_kunjungan DESC ");
		$p_uitable=new Table(array("No. Kunj.","No. Reg","Nama","NRM","Alamat"));
		$p_uitable->setModel(Table::$SELECT);
		$p_uitable->setName("change_noreg_layanan");
		$p_adapter=new SimpleAdapter();
		$p_adapter->add("No. Reg","id","only-digit8");
		$p_adapter->add("Nama","nama_pasien");
		$p_adapter->add("No. Kunj.","no_kunjungan");
		$p_adapter->add("NRM","nrm","only-digit8");
		$p_adapter->add("Alamat","alamat_pasien");
		$p_responder=new DBResponder($p_dbtable,$p_uitable,$p_adapter);	
		$master->getSuperCommand()->addResponder("change_noreg_layanan",$p_responder);		
	}
	$master->addSuperCommand("change_noreg_layanan", array());
    $master->addSuperCommandArray("change_noreg_layanan","noreg_pasien","id");
	$master->addSuperCommandArray("change_noreg_layanan","nrm_pasien","nrm");
	$master->addSuperCommandArray("change_noreg_layanan","nama_pasien","nama");
	
	if(isset($_POST['super_command']) && $_POST['super_command']=="change_noreg_baru" ){	
		$p_dbtable=new DBTable($db,"smis_rgv_layananpasien");
		$p_dbtable->setOrder(" nrm ASC, no_kunjungan DESC ");
		$p_uitable=new Table(array("No. Kunj.","No. Reg","Nama","NRM","Alamat"));
		$p_uitable->setModel(Table::$SELECT);
		$p_uitable->setName("change_noreg_baru");
		$p_adapter=new SimpleAdapter();
		$p_adapter->add("No. Reg","id","only-digit8");
		$p_adapter->add("Nama","nama_pasien");
		$p_adapter->add("No. Kunj.","no_kunjungan");
		$p_adapter->add("NRM","nrm","only-digit8");
		$p_adapter->add("Alamat","alamat_pasien");
		$p_responder=new DBResponder($p_dbtable,$p_uitable,$p_adapter);	
		$master->getSuperCommand()->addResponder("change_noreg_baru",$p_responder);
	}
	$master->addSuperCommand("change_noreg_baru", array());
    $master->addSuperCommandArray("change_noreg_baru","noreg_pasien_baru","id");
	$master->addSuperCommandArray("change_noreg_baru","nrm_pasien_baru","nrm");
	$master->addSuperCommandArray("change_noreg_baru","nama_pasien_baru","nama");
	$master->initialize();
?>