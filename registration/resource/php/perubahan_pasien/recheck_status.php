<?php 
/**
 * dipakai untuk mengecek status terakhir 
 * pasien , tetapi belum di pulangkan dari kasir
 * 
 * 
 * @author 		: Nurul Huda
 * @license 	: Apache v3
 * @version 	: 1.0.0
 * @since 		: 24 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @database	: - smis_rgv_layananpasien
 * 
 * */
global $db;
require_once "smis-base/smis-include-service-consumer.php";
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_rgv_layananpasien");
	$dbtable->addCustomKriteria(NULL," selesai ='0' ");
	if(isset($_POST['dari']) && $_POST['dari']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal >='". $_POST['dari']."' ");
	if(isset($_POST['sampai']) && $_POST['sampai']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal <'". $_POST['sampai']."' ");
	if(isset($_POST['carabayar']) && $_POST['carabayar']!="") 
		$dbtable->addCustomKriteria(NULL," carabayar ='". $_POST['carabayar']."' ");
	if(isset($_POST['nama_pasien']) && $_POST['nama_pasien']!="") 
		$dbtable->addCustomKriteria(NULL," nama_pasien LIKE '%". $_POST['nama_pasien']."%' ");
	if(isset($_POST['nrm']) && $_POST['nrm']!="") 
		$dbtable->addCustomKriteria(NULL," nrm ='". $_POST['nrm']."' ");	
	$q=$dbtable->getQueryCount("");
	$total=$db->get_var($q);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($total);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();	
	$dbtable=new DBTable($db, "smis_rgv_layananpasien");
	$dbtable->addCustomKriteria(NULL," selesai ='0' ");	
	if(isset($_POST['dari']) && $_POST['dari']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal >='". $_POST['dari']."' ");
	if(isset($_POST['sampai']) && $_POST['sampai']!="") 
		$dbtable->addCustomKriteria(NULL," tanggal <'". $_POST['sampai']."' ");
	if(isset($_POST['carabayar']) && $_POST['carabayar']!="") 
		$dbtable->addCustomKriteria(NULL," carabayar ='". $_POST['carabayar']."' ");
	if(isset($_POST['nama_pasien']) && $_POST['nama_pasien']!="") 
		$dbtable->addCustomKriteria(NULL," nama_pasien LIKE '%". $_POST['nama_pasien']."%' ");
	if(isset($_POST['nrm']) && $_POST['nrm']!="") 
		$dbtable->addCustomKriteria(NULL," nrm ='". $_POST['nrm']."' ");	
	$dbtable->setMaximum(1);
	$data=$dbtable->view("",$_POST['halaman']);
	$datalist=$data['data'];
	$x=$datalist[0];

	$noreg=$x->id;
	$serv=new ServiceConsumer($db,"recheck_status");
	$serv->setMode(ServiceConsumer::$MULTIPLE_MODE);
	$serv->addData("noreg_pasien",$noreg);
	$serv->execute();
	$data=$serv->getContent();
	$ruangan_belum="";
	
	$ruangan_terakhir="";
	$waktu_akhir="";
	$status_akhir=-1;
	
	foreach($data as $one=>$k){
		foreach($k as $v=>$xy){
			if($xy['posisi']=="0" || $xy['posisi']==0){
				continue;
			}
			
			/* pasien belum keluar
			 * pasien masuk mengalahkan pasien keluar*/
			if($xy['status']=="0" || $xy['status']==0){
				$ruangan_belum.=ArrayAdapter::format("unslug",$v)." , ";
				if($ruangan_terakhir=="" || $status_akhir==1){
					$ruangan_terakhir=ArrayAdapter::format("unslug",$v);
					$waktu_akhir=$xy['masuk'];
					$status_akhir=0;
				}else if($waktu_akhir<$xy['masuk'] || $status_akhir==1){
					$ruangan_terakhir=ArrayAdapter::format("unslug",$v);
					$waktu_akhir=$xy['masuk'];
					$status_akhir=0;
				}
			}else{
				/* pasien sudah keluar 
				 * pasien keluar kalah dengan pasien masuk*/
				if($ruangan_terakhir=="" && $status_akhir!=0){
					$ruangan_terakhir=ArrayAdapter::format("unslug",$v);
					$waktu_akhir=$xy['keluar'];
					$status_akhir=1;
				}else if($waktu_akhir<$xy['keluar'] && $status_akhir!=0){
					$ruangan_terakhir=ArrayAdapter::format("unslug",$v);
					$waktu_akhir=$xy['keluar'];
					$status_akhir=1;
				}
			}
		}
	}
	
	
	$content="<tr>
					<td>".$x->nama_pasien."</td>
					<td>".ArrayAdapter::format("digit8",$x->id)."</td>
					<td>".ArrayAdapter::format("digit8",$x->nrm)."</td>
					<td>".ArrayAdapter::format("date d-M-Y",$x->tanggal)."</td>
					<td>".$x->carabayar."</td>
					
					<td>".$ruangan_terakhir."</td>
				</tr>";//<td>".$ruangan_belum."</td>
	$status="0";
	
	$result=array("content"=>$content,"status"=>$status);	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($result);
	echo json_encode($res->getPackage());
	return;
}
$array=array("Nama","No Reg","NRM","Tanggal Masuk","Carabayar","Ruangan Akhir");
$uitable=new Table($array);
$uitable->setName("recheck_status")
		->setActionEnable(false)
		->setFooterVisible(false);
		
require_once "registration/class/RegistrationResource.php";
$jenis=RegistrationResource::getJenisPembayaran();
$jenis[]=array("name"=>"","value"=>"","default"=>"1");

$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("nama_pasien", "text", "Nama", "")
		->addModal("nrm", "text", "NRM", "")
		->addModal("carabayar", "select", "Carabayar", $jenis);
		
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("recheck_status.rekaptotal()");

$btng=new ButtonGroup("");
$btng->addButton($action);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("recheck_status.batal()");

$load=new LoadingBar("rekap_recheck_status_bar", "");
$modal=new Modal("rekap_recheck_status_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_recheck_status'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("registration/resource/js/recheck_status.js",false);
?>
