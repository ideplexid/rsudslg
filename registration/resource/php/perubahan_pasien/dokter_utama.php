<?php
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
global $db;
global $user;

if(isset($_POST['super_command']) && $_POST['super_command']=="autocorrect" ){
    /*call all problematic list*/
    $dbtable=new DBTable($db,"smis_rg_dokter_utama",array("id","prop"));
    if($_POST['command']=="list"){        
        $dbtable->addCustomKriteria(" kode ","=''");
        $dbtable->setShowAll(true);
        $data=$dbtable->view("",0);
        $list=$data['data'];
        
        $package=new ResponsePackage();
        $package->setContent($list);
        $package->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($package->getPackage());
    }else if($_POST['command']=="full_list"){    
        $dbtable->setShowAll(true);
        $data=$dbtable->view("",0);
        $list=$data['data'];
        
        $package=new ResponsePackage();
        $package->setContent($list);
        $package->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($package->getPackage());
    }else if($_POST['command']=="one"){
        /*initialize all data*/
        $id=$_POST['id'];
        $x=$dbtable->select($id);
        $mulai=$x->mulai;
        
        require_once "registration/function/get_jadwal_dokter.php";
        $kode=getJadwalDokter($db,$mulai);
        
        if($x->kode!=$kode){
            $one=array();
            $one['asal']=$x->kode;
            $one['ubah']=$kode;
            $one['waktu']=date("Y-m-d H:i:s");
            $one['user']=$user->getNameOnly();
            $array=json_decode($x->history,true);
            $array[]=$one;
            
            $update['history']=json_encode($array);
            $update['kode']=$kode;
            $idx['id']=$id;
            $dbtable->update($update,$idx);
        }
        
        $package=new ResponsePackage();
        $package->setContent(ArrayAdapter::dateFormat("date D, d M Y H:i",$mulai)." - ".$kode);
        $package->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($package->getPackage());
    }
    return;
}

/* PASIEN */
$ptable = new Table ( array ('Nama','NRM',"Alamat"), "", NULL, true );
$ptable->setName ( "pasien" );
$ptable->setModel ( Table::$SELECT );
$pbtable=new DBTable($db,"smis_rg_patient");
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama" );
$padapter->add ( "NRM", "id", "digit8" );
$padapter->add ( "Alamat", "alamat" );
$presponder = new DBResponder ( $pbtable, $ptable, $padapter );

$super = new SuperCommand ();
$super->addResponder ( "pasien", $presponder );
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}

$uitable = new Table ( array ('No.','Kode','Mulai','Selesai',"Petugas","Keterangan"), "&nbsp;", NULL, true );
$uitable->setName ( "dokter_utama" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	global $user;
	$adapter = new SimpleAdapter ();
	$adapter->setUseNumber(true,"No.","back.");
	$adapter->add ( "Mulai", "mulai", "date d M Y" );
	$adapter->add ( "Selesai", "selesai", "date d M Y" );
	$adapter->add ( "Kode", "kode" );
	$adapter->add ( "Petugas", "petugas" );
	$adapter->add ( "Keterangan", "keterangan" );
	$dbtable = new DBTable ( $db, "smis_rg_dokter_utama" );
	$dbtable->setOrder(" selesai ASC ",true);
	$dbtable->addCustomKriteria ( "nrm_pasien", " ='" . $_POST ['nrm_pasien'] . "'" );
    if(isset($_POST['filter_tanggal']) && $_POST['filter_tanggal']!="" ){
        $dbtable->addCustomKriteria ( "selesai", " >='" . $_POST ['filter_tanggal'] . "'" );
    }
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$dbres->addColumnFixValue("petugas",$user->getNameOnly());
	$dbres->addColumnFixValue("keterangan","Diubah Melalui Menu Dokter Utama");	
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$dbkode=new DBTable($db,"smis_rg_jadwal");
$qv="SELECT DISTINCT kode FROM smis_rg_jadwal ";
$qc="SELECT count(*) as total FROM smis_rg_jadwal";
$dbkode->setPreferredQuery(true,$qv,$qc);
$dbkode->setUseWhereforView(true);
$dbkode->setFetchMethode(DBTable::$ARRAY_FETCH);
$dbkode->setShowAll(true);
$data=$dbkode->view("","0");
$select_adapt=new SelectAdapter("kode","kode");
$kode=$select_adapt->getContent($data['data']);

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "mulai", "datetime", "Mulai", "" );
$uitable->addModal ( "selesai", "date", "Selesai", "" );
$uitable->addModal ( "kode", "select", "Kode", $kode );

$modal = $uitable->getModal ();
$modal->setTitle ( "Dokter Utama" );

$nrm = new Text ( "nrm_pasien", "nrm_pasien", "" );
$alamat = new Text ( "alamat_pasien", "alamat_pasien", "" );
$nama = new Text ( "nama_pasien", "nama_pasien", "" );
$filter = new Text ( "filter_tanggal", "filter_tanggal", "" );
$filter->setModel(Text::$DATE);

$alamat->setDisabled ( true );
$nama->setDisabled ( true );

$action = new Button ( "", "", "Select" );
$action->setClass("btn-primary");
$action->setIcon(" fa fa-check");
$action->setIsButton(Button::$ICONIC_TEXT);
$action->setAction ( "dokter_utama.chooser('dokter_utama','nama_pasien','pasien',pasien)" );

$autocorrect = new Button ( "", "", "Autocorrect" );
$autocorrect->setClass("btn-primary");
$autocorrect->setIcon(" fa fa-refresh");
$autocorrect->setIsButton(Button::$ICONIC_TEXT);
$autocorrect->setAction ( "dokter_utama.autocorrect()" );

$full_autocorrect = new Button ( "", "", "Full Autocorrect" );
$full_autocorrect->setClass("btn-primary");
$full_autocorrect->setIcon(" fa fa-refresh");
$full_autocorrect->setIsButton(Button::$ICONIC_TEXT);
$full_autocorrect->setAction ( "dokter_utama.full_autocorrect()" );


// form for proyek
$form = new Form ( "form_pasien", "", "Dokter Utama" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "Alamat", $alamat );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Tanggal", $filter );
$form->addElement ( "", $action );
$form->addElement ( "", $autocorrect );
$form->addElement ( "", $full_autocorrect );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addJS ( "base-js/smis-base-loading.js" );
echo addJS("registration/resource/js/dokter_utama.js",false);

?>