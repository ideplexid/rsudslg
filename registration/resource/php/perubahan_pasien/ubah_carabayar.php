<?php
global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once("registration/class/RegistrationResource.php");
$head=array ("No. Reg",'NRM','Nama','Alamat',"Tanggal","Ruangan","Kamar Inap","Cara Bayar");
$uitable = new Table ( $head, "Bagian", NULL, true );
$uitable->setName ( "ubah_carabayar" );
$uitable->setAddButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setDelButtonEnable(false);

if (isset ( $_POST ['command'] )) {
	
	if($_POST ['command']=="get_jenis_pasien"){
		require_once "registration/snippet/get_jenis_pasien.php";
		return;
	}
	
	$adapter = new SimpleAdapter ();
	$adapter->add ( "No. Reg", "id","digit8" );
	$adapter->add ( "NRM", "nrm","digit8" );
	$adapter->add ( "Nama", "nama_pasien" );
	$adapter->add ( "Alamat", "alamat_pasien" );
	$adapter->add ( "Cara Bayar", "carabayar" );
	$adapter->add ( "Tanggal", "tanggal","date d M Y" );
	$adapter->add ( "Ruangan", "jenislayanan","unslug" );
	$adapter->add ( "Kamar Inap", "kamar_inap","unslug" );
	$dbtable = new DBTable ( $db, "smis_rg_layananpasien" );
	$dbtable->setPreferredView(true,"smis_rgv_layananpasien_aktif");
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if($dbres->isSave()){
		/*push_service_change_name*/
		$serv=new ServiceConsumer($db,"set_change_carabayar");
		$serv->addData("noreg_pasien",$_POST['id']);
		$serv->addData("carabayar",$_POST['carabayar']);
		$serv->execute();
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nrm", "text", "NRM", "","n",NULL,true );
$uitable->addModal ( "carabayar", "select", "Carabayar", RegistrationResource::getJenisPembayaran() );
$uitable->addModal("nama_perusahaan", "select", "Perusahaan", RegistrationResource::getPerusahaan(),"y");
$uitable->addModal("asuransi", "select", "Asuransi", RegistrationResource::getAsuransi(),"y");
$uitable->addModal("nobpjs", "text", "No BPJS", "","y");

$modal = $uitable->getModal ();
$modal->setTitle ( "Perubahan Nama Pasien" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var ubah_carabayar;

function changePilihan(yes,json){
	if(yes){
		$("#ubah_carabayar_asuransi").val("");
		$("#ubah_carabayar_nobpjs").val("");
		$("#ubah_carabayar_nama_perusahaan").val("");
	}
	$("#ubah_carabayar_asuransi").prop("disabled",json.asuransi=="0");
	$("#ubah_carabayar_nobpjs").prop("disabled",json.nobpjs=="0");
	$("#ubah_carabayar_nama_perusahaan").prop("disabled",json.nama_perusahaan=="0");			
}

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','carabayar','nrm','nama_perusahaan',"asuransi","bpjs");
	ubah_carabayar=new TableAction("ubah_carabayar","registration","ubah_carabayar",column);
	ubah_carabayar.view();	
	
	ubah_carabayar.after_form_shown=function(){
		var data=this.getRegulerData();
		data['command']="get_jenis_pasien";
		data['slug']=$("#ubah_carabayar_carabayar").val();
		showLoading();
	    $.post("",data,function(res){
	        var json=getContent(res);
			changePilihan(false,json)		
	        dismissLoading();
	    });
	};
	
	$("#ubah_carabayar_carabayar").change(function(){
		var data=ubah_carabayar.getRegulerData();
		data['command']="get_jenis_pasien";
		data['slug']=$("#ubah_carabayar_carabayar").val();
		showLoading();
	    $.post("",data,function(res){
	        var json=getContent(res);
	        changePilihan(true,json)
			dismissLoading();
	    });	
	});
});
</script>
