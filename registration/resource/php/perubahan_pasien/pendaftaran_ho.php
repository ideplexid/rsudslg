<?php
	global $db;
	
	class PendaftaranHOTable extends Table {
		public function getContentButton($id) {
			$button_group = new ButtonGroup("noprint");
			$btn = new Button("cancel", "","Cancel");
			$btn->setAction($this->action.".cancel('".$id."')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Cancel' data-toggle='popover'");
			$btn->setIcon("fa fa-recycle");
			$btn->setIsButton(Button::$ICONIC);
			$button_group->addElement($btn);
			return $button_group;
		}
	}
	$table = new PendaftaranHOTable(
		array("No.", "NRM", "Nama", "Alamat", "Provinsi", "Kabupaten", "Kecamatan", "Kelurahan", "Jenis Kelamin"),
		"Pendaftaran : Pasien HER-OP",
		null,
		true
	);
	$table->setName("pasien_ho");
	$table->setAddButtonEnable(false);
	$table->setEditButtonEnable(false);
	$table->setDelButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("NRM", "id", "digit6");
		$adapter->add("Nama", "nama");
		$adapter->add("Alamat", "alamat");
		$adapter->add("Provinsi", "nama_provinsi");
		$adapter->add("Kabupaten", "nama_kabupaten");
		$adapter->add("Kecamatan", "nama_kecamatan");
		$adapter->add("Kelurahan", "nama_kelurahan");
		$adapter->add("Jenis Kelamin", "kelamin", "trivial_0_Laki-Laki_Perempuan");
		
		$columns = array("id", "prop");
		$dbtable = new DBTable($db, "smis_rg_patient", $columns);
		$dbtable->setDelete(true);
		$dbtable->addCustomKriteria(" prop ", " ='del' ");
		class PendaftaranHOResponder extends DBResponder {
			public function save() {
				$id['id'] = $_POST['id'];
				$data['prop'] = $_POST['prop'];
				$result = $this->dbtable->update($data, $id);
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if($result == false) $success['success'] = 0;
				return $success;
			}
		}
		$dbresponder = new PendaftaranHOResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}	
	
	echo $table->getHtml();	
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PasienHOAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PasienHOAction.prototype.constructor = PasienHOAction;
	PasienHOAction.prototype = new TableAction();
	PasienHOAction.prototype.cancel = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = id;
		data['prop'] = "";
		bootbox.confirm(
			"Yakin mengaktifkan rekam medis pasien ini?",
			function(result) {
				if (result) {
					showLoading();
					$.post(	
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	
	var pasien_ho;
	$(document).ready(function() {
		pasien_ho = new PasienHOAction(
			"pasien_ho",
			"registration",
			"pendaftaran_ho",
			new Array()
		);
		pasien_ho.view();
	});
</script>
