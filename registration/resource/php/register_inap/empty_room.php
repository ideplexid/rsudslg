<?php 

require_once("smis-base/smis-include-service-consumer.php");
require_once 'registration/class/service/BedService.php';

global $db;
$bed=new BedService($db);
$bed->execute();
$bedroom=$bed->getContent();
$node_empty=$bed->getEmptyRoom();
$node_used=$bed->getUsedRoom();

$rows=new RowSpan();
$rows->addSpan("<h5>Kamar Kosong</h5></br>".$node_empty->getHtml(),6);
$rows->addSpan("<h5>Pasien Rawat Inap</h5></br>".$node_used->getHtml(),6);

$p=$rows->getHtml();
$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setContent($p);
$response->setAlertVisible(false);
echo json_encode($response->getPackage());

?>