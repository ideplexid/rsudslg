<?php 
global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once "smis-libs-hrd/EmployeeResponder.php";
require_once "smis-base/smis-include-duplicate.php";
require_once "registration/class/table/DaftarTable.php";
require_once "registration/class/table/DaftarTableInap.php";

/** kamar chooser */
if(isset($_POST['super_command']) && $_POST['super_command']=="chooser_kamar" ){
    require_once "registration/snippet/chooser_kamar.php";
    return;
}

/**dokter */
$dkadapter = new SimpleAdapter();
$dkadapter ->add("Jabatan", "nama_jabatan")
           ->add("Nama", "nama")
           ->add("NIP", "nip");
$dktable   = new Table(array('Nama','Jabatan',"NIP"),"", NULL,true);
$dktable   ->setName("dokter")
           ->setModel(Table::$SELECT);
$dokter    = new EmployeeResponder($db, $dktable, $dkadapter, "dokter");

/**daftar pasien */
$edit_mode = getSettings($db,"reg-edit-time-strict","-1")*1;
$uitable   = new DaftarTableInap(array('Tanggal','NRM',"Nama","Pembayaran","Ruangan"),"", NULL,true);
$uitable   ->setEditMode($edit_mode);
$uitable   ->setName("urj")
           ->setModel(Table::$SELECT)
           ->setMode("inap");
$adapter   = new SimpleAdapter();
$adapter   ->add("Tanggal", "tanggal","date d M Y H:i")
           ->add("NRM", "nrm","digit6")
           ->add("Nama", "nama_pasien")
           ->add("Layanan", "jenislayanan","ucfirst")
           ->add("Pembayaran", "carabayar")
           ->add("Ibu", "ibu")
           ->add("Ayah", "ayah")
           ->add("Suami", "suami")
           ->add("Istri", "istri")
           ->add("Ruangan", "jenislayanan","unslug")
           ->add("tgl_inap", "tanggal_inap");

$column         = array('id','uri','id_dokter','tanggal_inap','nama_dokter','kamar_inap','administrasi_inap',"no_sep_ri","id_bed_kamar","bed_kamar");
$dbtablelayanan = new DBTable($db,'smis_rg_layananpasien',$column);
$dbtablelayanan ->setPreferredView(true,"smis_rgv_urj")
                ->setUseWhereforView(false)
                ->setViewForSelect(true);
$dbresponder    = new DBResponder($dbtablelayanan,$uitable,$adapter);
if(isset($_POST['kriteria'])){
	$dbtablelayanan->addCustomKriteria(NULL,"  ( 
		   smis_rgv_urj.nama_pasien LIKE '%".$_POST['kriteria']."%' 
		OR smis_rgv_urj.ibu         LIKE '%".$_POST['kriteria']."%'
		OR smis_rgv_urj.ayah        LIKE '%".$_POST['kriteria']."%'
		OR smis_rgv_urj.suami       LIKE '%".$_POST['kriteria']."%'
		OR smis_rgv_urj.istri       LIKE '%".$_POST['kriteria']."%'
		OR smis_rgv_urj.nrm         LIKE '%".$_POST['kriteria']."%'
	)");
}
$ruangan = getSettings($db,"reg-inap-only","");
if($ruangan!=""){
	$dbtablelayanan->addCustomKriteria(" smis_rgv_urj.jenislayanan ","='".$ruangan."'");	
}
$super   = new SuperCommand();
$super   ->addResponder("dokter", $dokter)
         ->addResponder("urj", $dbresponder);
$init    = $super->initialize();
if($init!=null){
	echo $init;
	return;
}

global $user;
$username       = $user->getUsername();
$header         = array("No.",'Tanggal','NRM',"No Reg","Nama","Pembayaran","Ruangan Pertama","Ibu","Ayah","Suami","Istri","Alamat","Ruangan Terakhir","Info Bed");
if(getSettings($db,"reg-inap-show-profile-number","0")=="1"){
    $header[]   = "Profile Number";
}
$uitable    ->setHeader($header)
            ->setName("register_menginap")
            ->setModel(Table::$EDIT)
            ->setDelButtonEnable(false);
$button     = new Button("", "", "Batal Rawat Inap");
$button     ->setIsButton(Button::$ICONIC)
            ->setIcon("fa fa-stop")
            ->setClass("btn-danger");
if(getSettings($db,"reg-show-cancel-ranap","1")=="1"){
    $uitable->addContentButton("cancel", $button);
}
$uitable    ->setReloadButtonEnable(false)
            ->setPrintButtonEnable(false)
            ->setPrintButtonEnable(false)
            ->setHelpButtonEnabled(true, "registration","rawat_inap");

if(isset($_POST['command'])){		
    $adapter ->setUseNumber(true,"No.","back.")
             ->add("No Reg", "id","digit6")
             ->add("Ruangan Pertama", "kamar_inap","unslug")
             ->add("Ibu", "ibu")
             ->add("Ayah", "ayah")
             ->add("Suami", "suami")
             ->add("Istri", "istri")
             ->add("Alamat", "alamat_pasien")
             ->add("Umur", "umur")
             ->add("Tanggal", "tanggal_inap","date d M Y H:i")
             ->add("Profile Number", "profile_number")
             ->add("Ruangan Terakhir", "last_ruangan","unslug")
             ->add("Info Bed", "last_bed");
    $dbtablelayanan = new DBTable($db,'smis_rg_layananpasien',$column);
    $dbtablelayanan ->setPreferredView(true,"smis_rgv_uri")
                    ->setUseWhereforView(true)
                    ->setViewForSelect(true);    
    if(isset($_POST['dari']) && $_POST['dari']!=""){
		$dbtablelayanan ->addCustomKriteria(" tanggal_inap>= ","'".$_POST['dari']."'");
	}
	if(isset($_POST['sampai']) && $_POST['sampai']!=""){
		$dbtablelayanan ->addCustomKriteria(" tanggal_inap< ","'".$_POST['sampai']."'");
	}
    $dbresponder=new DBResponder($dbtablelayanan,$uitable,$adapter);
    if($dbresponder->isSave() && getSettings($db, "reg-auto-inap", "0")=="1"){
        $dbresponder->addColumnFixValue("tanggal_inap", date("Y-m-d H:i:s"));
    }
    
    if($dbresponder->isSave() ){
        /* reset the sync status */
        $dbresponder->addColumnFixValue("synch", "0");        
        if(isset($_POST['uri']) && $_POST['uri']==1){
            $dbresponder->addColumnFixValue("opri", $username);
        }else{
            $dbresponder->addColumnFixValue("hbi", "concat(hbi,' ','[-|1|".$username."|".date("Y-m-d h:i")."]')");
            $dbresponder->addUpSaveWarp("hbi",DBController::$NO_STRIP_ESCAPE_WRAP);
        }
        if(isset($_POST['uri']) && $_POST['uri']==1 && getSettings($db,"reg-inap-auto-zero","0")=="1"){
            /**mereset biaya karcis rawat jalan jadi 0 */
            $dbresponder->addColumnFixValue("karcis", "0");
        }else if(isset($_POST['uri']) && $_POST['uri']==0 && getSettings($db,"reg-inap-auto-zero","0")=="1"){
            /**jika batal maka karcis rawat jalan kembali seperti semula */
            require_once "smis-base/smis-include-service-consumer.php";
	        require_once "registration/class/service/KarcisService.php";
            $one        = $dbtablelayanan->select($_POST['id']);
            $karcis     = new KarcisService($db,$one->jenislayanan,$one->barulama,"0");
            $karcis->execute();
            $content    = $karcis->getContent ();
            $nilai      = $content['content'];
            $dbresponder->addColumnFixValue("karcis", $nilai);
        }
    }
    
    $datapasien=$dbresponder->command($_POST['command']);    
    if($_POST['command']=='save'){
        //mengirimkan data kepada kasir
        require_once "registration/class/RegistrationResource.php";
        RegistrationResource::synchronizeToCashier($db,$_POST['id']);
        RegistrationResource::synchronizeToAccounting($db,$_POST['id']);
        /*Mengupdate MRS dari IGD, untuk Diagnosa menjadi Rawat Inap dan Kelanutan Laporan IGD menjadi Rawat Inap*/
        $service = new ServiceConsumer($db, "update_mrs_igd");
        $service ->addData("noreg_pasien", $datapasien['content']['id'])
                 ->addData("uri", $_POST['uri'])
                 ->execute();
    }
    
    if($_POST['command']=='save' && $_POST['uri']!='0'){
        require_once 'registration/class/service/AntrianService.php';
        $pasien_table               = new DBTable($db,'smis_rg_patient');
        $pasien                     = $pasien_table->select($_POST['nrm'],false);
        $reg_pasien                 = $dbtablelayanan->select($datapasien['content']['id'],false);
        $detail                     = array();
        $detail['alamat']           = $pasien->alamat;
        $detail['caradatang']       = $reg_pasien->caradatang;
        $detail['waktu_register']	= $reg_pasien->tanggal;
        $detail['tgl_lahir']        = $pasien->tgl_lahir;
        $detail['no_profile']       = $pasien->profile_number;
        $alamat                     = $pasien->alamat." - ".$pasien->nama_kelurahan." - ".$pasien->nama_kecamatan." - ".$pasien->nama_kabupaten." - ".$pasien->nama_provinsi;
    
        /*Memasukan Rawat Inap yang dituju Pasien*/
        $service  = new AntrianService($db,$_POST['tanggal_inap'],$pasien->nama,$_POST['nrm'],$_POST['carabayar'],$_POST['kamar_inap'],$datapasien['content']['id'],$reg_pasien->umur,$reg_pasien->gol_umur,$pasien->kelamin,$alamat,$detail);
        if(isset($_POST['id_bed_kamar']) && $_POST['id_bed_kamar']!="" ){
            /**jika ada kamar maka masukan sekalian bed kamarnya */
            $service ->setBedKamar($_POST['id_bed_kamar'],$_POST['bed_kamar']);
        }
        $service  ->execute();
        $response = $service->getContent();
        
        /* melakukan update kode sep jika diaktifkan */
        if(getSettings($db,"reg-bpjs-activate-bpjs-reg-uri","0")=="1"){
            $dbtable            = new DBTable($db,"smis_rg_sep");
            $id["noSEP"]        = $_POST['no_sep_ri'];
            $update["noReg"]    = $_POST['id'];
            $dbtable->update($update,$id);
        }
        
        $rp   = new ResponsePackage();
        $rp   ->setAlertVisible(false)
              ->setStatus(ResponsePackage::$STATUS_OK)
              ->setContent($datapasien['content']);
        $warn = "<strong >".$pasien->nama."</strong>
                        dengan NRM : <strong class='label label-info'>".ArrayAdapter::format("digit8",$_POST['nrm'])."</strong>
                        Berhasil di Register ke
                        <strong>".strtoupper($_POST['kamar_inap'])."</strong>
                        Pada Tanggal
                        <strong class='label label-info'>".ArrayAdapter::format("date d-M-Y",$_POST['tanggal_inap'])."</strong>";
        
        $rp->setWarning(true, "Berhasil", $warn);
        echo json_encode($rp->getPackage());
        return;
    }    
    echo json_encode($datapasien);
    return;
}

require_once "registration/class/RegistrationResource.php";
$uitable ->addModal("id", "hidden", "", "")
         ->addModal("id_dokter", "hidden", "", "")
         ->addModal("uri", "hidden", "", "1");
if(getSettings($db, "reg-tgl-lahir", "0")=="1"){
	$uitable->addModal("tanggal_inap", "idatetime", "Tanggal", date("Y-m-d H:i:s"),"n",NULL,getSettings($db, "reg-auto-inap", "0")=="1",NULL,true,"nama_pasien");
}else{
	$uitable->addModal("tanggal_inap", "datetime", "Tanggal", date("Y-m-d H:i:s"),"n",NULL,getSettings($db, "reg-auto-inap", "0")=="1",NULL,true,"nama_pasien");
}
$uitable ->addModal("nama_pasien", "chooser-register_menginap-urj","Pasien","","n",NULL,false,NULL,false)
         ->addModal("nrm", "text", "NRM","","n",NULL,true,NULL,false,"nama_dokter")
         ->addModal("carabayar", "text", "Pembayaran","","y",null,true,null,false,"nama_dokter")
         ->addModal("nama_dokter", "chooser-register_menginap-dokter", "Dokter", "","n",NULL,false,NULL,false)
         ->addModal("kamar_inap", "select", "Ruang", RegistrationResource::getURI($db),"n",NULL,false,NULL,false,"bed_kamar");

if(getSettings($db,"reg-inap-show-bed","0")=="1"){
    $uitable ->addModal("bed_kamar", "chooser-register_menginap-chooser_kamar-Pilih Bed", "Bed","","n",NULL,true,NULL,false,"id_bed_kamar")
             ->addModal("id_bed_kamar", "hidden", "","","y",NULL,false,NULL,false,"sisa_kamar");
}
         
$uitable ->addModal("sisa_kamar", "text", "Sisa", "0","y",NULL,true,NULL,false,"kelas_kamar")
         ->addModal("kelas_kamar", "text", "Kelas", "0","y",NULL,true,NULL,false,"administrasi_inap")
         ->addModal("administrasi_inap", "money", "Administrasi", "0","y",NULL,(getSettings($db,"reg-lock-karcis-ri","0")=="1"),NULL,false,"save");
if(getSettings($db,"reg-bpjs-activate-bpjs-reg-uri","0")=="1"){
    $uitable ->addModal("nobpjs", "text", "No BPJS", "","y",NULL,false,NULL,false,"")
             ->addModal("no_sep_ri", "text", "No SEP RI", "","y",NULL,false,NULL,false,"");
}else{
    $uitable->addModal("no_sep_ri", "hidden", "", "","y",NULL,false,NULL,false,"");
}

$modal = $uitable->getModal();
$modal ->setTitle("Registrasi Rawat Inap");
$REG_PRINT_JS_SOCIAL        = getSettings($db, "reg-print-data-social-jsetup", "0");
$REG_PRINT_JS_SOCIAL_HIDDEN = new Hidden("reg_print_data_social_jsetup", "", $REG_PRINT_JS_SOCIAL);

$uitable ->clearContent();
$uitable ->addModal("dari", "date", "Dari", "")
         ->addModal("sampai", "date", "Sampai", "");
$show    = new Button();
$show    ->setClass("btn btn-primary")
	     ->setIsButton(Button::$ICONIC_TEXT)
	     ->setIcon("icon-white icon-search")
	     ->setAction("register_menginap.view()");
$form    = $uitable->getModal()->getForm();
$form    ->setTitle("Pasien Rawat Inap")
         ->addElement("",$show);
      
echo $form                          ->getHtml();
echo $REG_PRINT_JS_SOCIAL_HIDDEN    ->getHtml();
echo $modal                         ->getHtml();
echo $uitable                       ->getHtml();
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addCSS ("registration/resource/css/registration.css",false);
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("smis-base-js/smis-base-shortcut.js",false);
echo addJS  ("registration/resource/js/daftar_patient.js",false);
echo addJS  ("framework/jquery/jquery-barcode.min.js");
echo addJS  ("base-js/smis-base-barcode.js");
echo addJS  ("registration/resource/js/register_menginap.js",false);
echo addJS  ("registration/resource/js/chooser_kamar.js",false);
?>