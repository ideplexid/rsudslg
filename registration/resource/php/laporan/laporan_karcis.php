<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'registration/class/RegistrationResource.php';
require_once 'registration/class/responder/KarcisResponder.php';
global $db;


$button = new Button("", "", "");
$button->setAction("laporan_karcis.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak = new Button("", "", "");
$cetak->setAction("laporan_karcis.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$excel = new Button("", "", "");
$excel->setAction("laporan_karcis.excel()");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");

$laporan_karcis	= new MasterSlaveTemplate($db, "smis_rgv_layananpasien", "registration", "laporan_karcis");
$responder = new KarcisResponder($laporan_karcis->getDBTable(),$laporan_karcis->getUItable(),$laporan_karcis->getAdapter());
$laporan_karcis ->setDBresponder($responder);
$laporan_karcis	->getDBtable()->setOrder(" tanggal ASC ");
$uitable		= $laporan_karcis->getUItable();
$uitable->setAction(false);
if(isset($_POST['dari']) && $_POST['dari']!="" && isset($_POST['sampai']) && $_POST['sampai']!=""){
	$laporan_karcis ->getDBtable()
					->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
					->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
			
	if($_POST['urji']!="") 				$laporan_karcis->getDBtable()->addCustomKriteria("uri","='".$_POST['urji']."'");
	if($_POST['layananpasien']!="") 	$laporan_karcis->getDBtable()->addCustomKriteria("jenislayanan","='".$_POST['layananpasien']."'");
	if($_POST['kamar_inap']!="") 		$laporan_karcis->getDBtable()->addCustomKriteria("kamar_inap","='".$_POST['kamar_inap']."'");
	if($_POST['carabayar']!="") 		$laporan_karcis->getDBtable()->addCustomKriteria("carabayar","='".$_POST['carabayar']."'");
}

$header  = array("No.","Tanggal","Nama","NRM","No Reg","URI/URJ","Ruangan","Kamar Inap","Jenis Pasien","Karcis","Administrasi");
$uitable->setHeader($header);
$adapter = new SummaryAdapter();
$adapter->addFixValue("Ruangan", "<strong>Total</strong>");
$adapter->addSummary("Biaya", "biaya","money Rp.");
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y H:i")
		->add("Nama", "nama_pasien")
		->add("NRM", "nrm","only-digit8")
		->add("No Reg", "id","only-digit8")
		->add("Ruangan", "jenislayanan","unslug")
		->add("Jenis Pasien", "carabayar","unslug")
		->add("Kamar Inap", "kamar_inap","unslug")
		->add("URI/URJ", "uri","trivial_0_URJ_URI")
		->add("Karcis", "karcis","money Rp.")
		->add("Administrasi", "administrasi_inap","money Rp.");
$adapter->addSummary("Karcis","karcis","money Rp.")
		->addSummary("Administrasi","administrasi_inap","money Rp.");
$laporan_karcis	->setAdapter($adapter);
$laporan_karcis ->addViewData("dari", "dari")
				->addViewData("sampai", "sampai")
				->addViewData("urji","urji")
				->addViewData("layananpasien","layananpasien")
				->addViewData("kamar_inap","kamar_inap")
				->addViewData("carabayar","carabayar");

if($laporan_karcis->getDBResponder()->isPreload()){

	$urji = new OptionBuilder();
	$urji->add("","","1");
	$urji->add("Rawat Inap","1","0");
	$urji->add("Rawat Jalan","0","0");
	
	$service = new ServiceConsumer ( $db, "get_jenis_patient",NULL,"registration" );
	$service->execute ();
	$jenis_pasien 	= $service->getContent ();
	$cbayar			= new OptionBuilder();
	$cbayar->add("","","1");
	foreach($jenis_pasien as $jp){
		$cbayar->add($jp['name'],$jp['value']);
	}
	
	$content	= RegistrationResource::getURJIP($db);
	$uri		= RegistrationResource::getURIFromContent($content);
	$urj		= RegistrationResource::getURJFromContent($content);
	$default	= array("name"=>"","value"=>"","default"=>"1");
	$uri[]		= $default;
	$urj[]		= $default;

	$laporan_karcis ->addFlag("dari", "Tanggal Awal", "Silakan Masukan Tanggal Mulai")
					->addFlag("sampai", "Tanggal Akhir", "Silakan Masukan Tanggal Akhir")
					->addNoClear("dari")
					->addNoClear("sampai")
					->addNoClear("urji")
					->addNoClear("layananpasien")
					->addNoClear("kamar_inap")
					->addNoClear("carabayar")
					->setDateEnable(true)
					->getUItable()
					->setActionEnable(false)
					->addModal("dari", "date", "Awal", "")
					->addModal("sampai", "date", "Akhir", "")
					->addModal("urji", "select", "URI/URJ", $urji->getContent())
					->addModal("layananpasien", "select", "Ruangan", $urj)
					->addModal("kamar_inap", "select", "Kamar Inap", $uri)
					->addModal("carabayar", "select", "Jenis Pasien", $cbayar->getContent());
	$laporan_karcis ->getForm()
					->addElement("", $button)
					->addElement("", $cetak)
					->addElement("", $excel);		
}
$laporan_karcis->initialize();