<?php 
global $db;
if( isset($_POST['super_command']) && $_POST['super_command']!=""){
	switch($_POST['super_command']){
		case "kabupaten" : require_once "registration/snippet/selector_kabupaten.php"; return;
		case "kecamatan" : require_once "registration/snippet/selector_kecamatan.php"; return; 
		case "kelurahan" : require_once "registration/snippet/selector_kelurahan.php"; return;
	}
}

$uitable=new Report(array("No.","Nama","L/P","NRM","Tanggal","Alamat","Provinsi","Kabupaten","Kecamatan",'Kelurahan',"Marital","Pendidikan","Pekerjaan","Agama","Suku","Bahasa"),"", NULL);
$uitable->setName("laporan_pasien");
$uitable->setDiagram(true);
$uitable->setModelLineUsability(false);
$uitable->setModelAreaUsability(false);
/*this is respond when system have to response*/
if(isset($_POST['command'])){
    require_once "registration/class/responder/PasienReport.php";
	$dbtable=new DBTable($db,'smis_rg_patient');
	//$dbtable->setShowAll(true);
	$adapter = new SimpleAdapter();
	$adapter ->setUseNumber(true, "No.","back.")
			 ->add("Nama", "nama")
			 ->add("Tanggal", "tanggal","date d-M-Y")
			 ->add("NRM", "id","digit8")
			 ->add("Alamat", "alamat")
			 ->add("Provinsi", "nama_provinsi")
			 ->add("Kabupaten", "nama_kabupaten")
			 ->add("Kecamatan", "nama_kecamatan")
			 ->add("Kelurahan", "nama_kelurahan")
			 ->add("Marital", "status")
			 ->add("Pendidikan", "pendidikan")
			 ->add("Suku", "suku")
			 ->add("Bahasa", "bahasa")
			 ->add("Agama", "agama")
			 ->add("Pekerjaan", "pekerjaan")
			 ->add("L/P", "kelamin","trivial_0_Laki-Laki_Perempuan");
	$dbres=new PasienReport($dbtable,$uitable,$adapter,'tanggal',DBReport::$DATE);
	$dbres->setTimeGroup(false);
	$group="";
	
	if($_POST['diagram']=="Lokasi"){
		if($_POST['provinsi']=="%"){
			$diagram_view=" count(*) as jumlah, nama_provinsi as nama";
			$group=" provinsi ASC ";
		}else if($_POST['kabupaten']=="%"){
			$diagram_view=" count(*) as jumlah, nama_kabupaten as nama";
			$group=" provinsi ASC, kabupaten ASC ";
		}else if($_POST['kecamatan']=="%"){
			$diagram_view=" count(*) as jumlah, nama_kecamatan as nama";
			$group=" provinsi ASC, kabupaten ASC, kecamatan ASC ";
		}else {
			$diagram_view=" count(*) as jumlah, nama_kelurahan as nama";
			$group=" provinsi ASC, kabupaten ASC, kecamatan ASC, kelurahan ASC  ";
		}
	}else if($_POST['diagram']=="Pendidikan"){
		$diagram_view=" count(*) as jumlah, pendidikan as nama";
		$group=" pendidikan ASC ";
	}else if($_POST['diagram']=="Agama"){
		$diagram_view=" count(*) as jumlah, agama as nama";
		$group=" agama ASC ";
	}else if($_POST['diagram']=="Suku"){
		$diagram_view=" count(*) as jumlah, suku as nama";
		$group=" suku ASC ";
	}else if($_POST['diagram']=="Bahasa"){
		$diagram_view=" count(*) as jumlah, bahasa as nama";
		$group=" bahasa ASC ";
	}else if($_POST['diagram']=="Marital"){
		$diagram_view=" count(*) as jumlah, status as nama";
		$group=" status ASC ";
	}else if($_POST['diagram']=="Pekerjaan"){
		$diagram_view=" count(*) as jumlah, pekerjaan as nama";
		$group=" pekerjaan ASC ";
	}else if($_POST['diagram']=="Jenis Kelamin"){
		$diagram_view=" count(*) as jumlah, if(kelamin=0,'Laki-Laki','Perempuan') as nama";
		$group=" kelamin ASC ";
	}
	
	$diagramadapter=new SimpleAdapter();
	$diagramadapter->add("Lokasi", "nama","emptyKOSONG");
	$diagramadapter->add("jumlah", "jumlah");
	$dbres->setDiagram(true, $diagram_view,$group, $diagramadapter);
	
	$data=$dbres->command($_POST['command']);
	echo json_encode($data,JSON_NUMERIC_CHECK);
	return;
}

$btn=new Button();
$btn->setClass("btn btn-primary");
$btn->setIcon(" fa fa-file-excel-o");
$btn->setIsButton(Button::$ICONIC);
$btn->setAction("laporan_pasien.excel()");
$uitable->addFooterButton($btn);

require_once 'registration/class/RegistrationResource.php';
$propinsi_dbtable = new DBTable($db,"smis_rg_propinsi");
$propinsi_dbtable->setShowAll(true);
$propinsi_dbtable->setOrder(" nama ASC ");
$data=$propinsi_dbtable->view("","0");
$prop_adapter=new SelectAdapter("nama", "id");
$propinsi=$prop_adapter->getContent($data['data']);
$propinsi[]=array("name"=>"SEMUA","value"=>"%");

$diagram=new OptionBuilder();
$diagram->add("Pekerjaan");
$diagram->add("Pendidikan");
$diagram->add("Agama");
$diagram->add("Suku");
$diagram->add("Bahasa");
$diagram->add("Marital");
$diagram->add("Jenis Kelamin");
$diagram->add("Lokasi");

$jk=new OptionBuilder();
$jk->add("","%","1");
$jk->add("Laki-Laki","0","0");
$jk->add("Perempuan","1","0");




/*This is Modal Form and used for add and edit the table*/
$uitable->addModal("provinsi", "select", "Provinsi", $propinsi);
$uitable->addModal("kabupaten", "select", "Kabupaten", "");
$uitable->addModal("kecamatan", "select", "Kecamatan", "");
$uitable->addModal("kelurahan", "select", "Kelurahan", "");
$uitable->addModal("status", "select", "Marital", RegistrationResource::getMarital(true));
$uitable->addModal("pendidikan", "select", "Pendidikan", RegistrationResource::getJenisPendidikan(true));
$uitable->addModal("pekerjaan", "select", "Pekerjaan", RegistrationResource::getJenisKerja(true));
$uitable->addModal("agama", "select", "Agama", RegistrationResource::getAgama(true));
$uitable->addModal("suku", "select", "Suku", RegistrationResource::getSukuBangsa(true));
$uitable->addModal("bahasa", "select", "Bahasa", RegistrationResource::getBahasa(true));
$uitable->addModal("diagram", "select", "Diagram", $diagram->getContent());
$uitable->addModal("kelamin", "select", "Jenis Kelamin", $jk->getContent());


$modal=$uitable->getAdvanceModal();
$modal->setTitle("Laporan Pendataan Pasien");
$form=$modal->getForm();

echo addCSS("framework/bootstrap/css/datepicker.css");
echo addCSS("framework/bootstrap/css/bootstrap-select.css");

echo addCSS("framework/bootstrap/css/morris.css");
echo addJS("framework/jquery/raphael-min.js");
echo addJS("framework/bootstrap/js/morris.min.js");

echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/smis/js/report_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addJS("registration/resource/js/laporan_pasien.js",false);
echo addCSS("registration/resource/css/laporan_pasien.css",false);

echo "<div>".$form->getHtml()."</div>";
echo "<div class='clear line'></div>";
echo "<div>".$uitable->getDiagram("laporan_pasien_mychart")."</div>";
echo "<div class='clear line'></div>";
echo "<div>".$uitable->getHtml()."</div>";
?>