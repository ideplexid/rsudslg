<?php
global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'registration/class/RegistrationResource.php';
require_once 'registration/class/adapter/LapURJAdapter.php';

$ruangan=RegistrationResource::getRuanganDaftar();

$lal_uri=new MasterSlaveTemplate($db, "smis_rgv_layananpasien", "registration", "lap_urj");
$lal_uri->setDateTimeEnable(true);
$header=array ("No.","NRM","No. Reg","Masuk","Keluar","Carabayar",'Nama',"Umur",'Desa','Kecamatan',"Kabupaten","L/P");
$summary=new LapURJAdapter(false,"",$ruangan);
$summary->setUseNumber(true, "No.","back.");
$lal_uri->setAdapter($summary)
		->getAdapter()
		->setUseNumber(true, "No.","back.")
		->add("NRM", "nrm","only-digit8")
		->add("No. Reg", "id","only-digit8")
		->add("Masuk", "tanggal","date d M Y H:i")
		->add("Keluar", "tanggal_pulang","date d M Y H:i")
		 ->add("Jam", "tanggal","date H:i")
		->add("Carabayar", "carabayar")
		->add("Nama", "nama_pasien")
		->add("Umur", "umur")
		->add("L/P", "kelamin","trivial_0_L_P")
		->add("Desa", "nama_kelurahan")
		->add("Kecamatan", "nama_kecamatan")
		->add("Kabupaten", "nama_kabupaten");
foreach($ruangan as $one){
	$header[]=$one['name'];
}

if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " uri='0'");
	$lal_uri->getDBtable()->setShowAll(true);
	$lal_uri->getDBtable()->setOrder(" tanggal ASC ");
}
$btn=$lal_uri->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->setExcelButtonEnable(true)
			  ->getHeaderButton();
$lal_uri->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "");
$lal_uri->getForm(true,"Laporan Rawat Jalan")
		 ->addElement("", $btn);
$lal_uri->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);


$lal_uri->addViewData("dari", "dari")
		 ->addViewData("sampai", "sampai");
$lal_uri->initialize();
?>