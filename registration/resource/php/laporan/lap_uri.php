<?php
global $db;
require_once 'registration/class/RegistrationResource.php';
require_once 'smis-libs-class/MasterSlaveTemplate.php';
$lal_uri=new MasterSlaveTemplate($db, "smis_rgv_layananpasien", "registration", "lap_uri");
$lal_uri->setDateTimeEnable(true);
$header=array ("No.","Masuk","Keluar","NRM","No. Reg",'Nama',"TGL Lahir","TMP Lahir","Umur",'Desa','Kecamatan',"Kabupaten","JK","Status","Pekerjaan","Penanggung Jawab","Kiriman","Pengirim","IGD","Carabayar","Ruangan","Bonus","Baru/Lama");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " uri='1'");
	$barulama=$_POST['barulama'];
	$carabayar=$_POST['carabayar'];
	$jk=$_POST['jenis_kelamin'];
	if($barulama!="") $lal_uri->getDBtable()->addCustomKriteria(" barulama ", " ='".$barulama."' ");
	if($carabayar!="") $lal_uri->getDBtable()->addCustomKriteria(" carabayar ", " LIKE '%".$carabayar."%' ");
	if($jk!="") $lal_uri->getDBtable()->addCustomKriteria(" kelamin ", " ='".$jk."' ");
	$lal_uri->getDBtable()->setShowAll(true);
	$lal_uri->getDBtable()->setOrder(" tanggal ASC ");
}

$barulama=new OptionBuilder();
$barulama->add("","","1");
$barulama->add("Baru","0","0");
$barulama->add("Lama","1","0");
$brlm=$barulama->getContent();
$jenis=RegistrationResource::getJenisPembayaran();
$jenis[]=array("name"=>"--Semua--","value"=>"%","default"=>"1");


$option=new OptionBuilder();
$option->add("","","1");
$option->add("Laki - Laki","0","0");
$option->add("Perempuan","1","0");
$btn=$lal_uri->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->addNoClear("jenis_kelamin")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->setExcelButtonEnable(true)
			  ->getHeaderButton();

$lap_print_excel=new Button("",""," Excel Print");
$lap_print_excel->setIcon(" fa fa-file-excel-o");
$lap_print_excel->setAction("lap_uri.download_report()");
$lap_print_excel->setClass(" btn-primary");
$lap_print_excel->setIsButton(Button::$ICONIC_TEXT);

$lal_uri->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("carabayar", "select", "Jenis Pasien", $jenis)
		 ->addModal("barulama", "select", "Baru Lama", $brlm)
		 ->addModal("jenis_kelamin", "select", "Jenis Kelamin", $option->getContent());
$lal_uri->getForm(true,"Laporan Rawat Inap")
		 ->addElement("", $btn)
		 ->addElement("", $lap_print_excel);
$lal_uri->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new SimpleAdapter();
$summary->setUseNumber(true, "No.","back.");

$lal_uri->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Masuk", "tanggal","date d M Y H:i")
		 ->add("Keluar", "tanggal_pulang","date d M Y H:i")
		 ->add("Nama", "nama_pasien")
		 ->add("NRM", "nrm","only-digit8")
		 ->add("No. Reg", "id","only-digit8")
		 ->add("TGL Lahir", "tgl_lahir","date d M Y")
		 ->add("TMP Lahir", "tempat_lahir")
		 ->add("Umur", "umur")
		 ->add("JK", "kelamin","trivial_0_Laki-Laki_Perempuan")
		 ->add("Desa", "nama_kelurahan")
		 ->add("Kecamatan", "nama_kecamatan")
		 ->add("Kabupaten", "nama_kabupaten")
		 ->add("Status", "status")
		//->add("Agama", "agama")
		//->add("Suku", "suku")
		//->add("Pendidikan", "pendidikan")
		->add("Ruangan", "kamar_inap","unslug")
		->add("Pekerjaan", "pekerjaan")
		->add("Penanggung Jawab", "namapenanggungjawab")
		->add("Kiriman", "caradatang")
		->add("IGD", "tanggal_inap","date d M Y H:i")
		->add("Carabayar", "carabayar")
		->add("Baru/Lama", "barulama","trivial_0_Baru_Lama")
		->add("Kiriman", "caradatang")
		->add("Pengirim", "nama_rujukan")
		->add("Bonus", "ambilbonus");
$lal_uri->addViewData("dari", "dari")
		->addViewData("sampai", "sampai")
		->addViewData("barulama", "barulama")
		->addViewData("carabayar", "carabayar")
		->addViewData("jenis_kelamin", "jenis_kelamin");
$lal_uri->addResouce("js","registration/resource/js/lap_uri.js");
$lal_uri->initialize();
?>