<?php 
ini_set('max_execution_time', 6000);
ini_set('memory_limit','512M');
$header  = array("Tanggal","Pasien","NRM","No Registrasi","URI/URJ");
$uitable = new Table($header,"",NULL,false);
$btngrup = $uitable ->setExcelButtonEnable(true)
				 	->setFooterVisible(true)
				 	->setAddButtonEnable(false)
				 	->setPrintButtonEnable(true)
				 	->setName("absensi_dokter")
				 	->getHeaderButton();
				 
if($_POST['command']){
	require_once 'registration/class/responder/AbsensiDokterResponder.php';
	$adapter = new SimpleAdapter();
	$adapter ->setUseNumber(true,"No.","back.")
			 ->add("Pasien", "nama_pasien")
			 ->add("NRM", "nrm","digit8")
			 ->add("No Registrasi", "id","digit8")
			 ->add("URI/URJ", "uri","trivial_0_<strong class='badge badge-important'>Rawat Jalan</strong>_<strong class='badge badge-success'>Rawat Inap</strong>")
			 ->add("Tanggal","tanggal", "date d M Y H:i");
	$dbtable = new DBTable($db, "smis_rgv_layananpasien");
	$dbtable ->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
			 ->addCustomKriteria(NULL, "tanggal<='".$_POST['sampai']."'")
			 ->setShowAll(false);
	$dbres	 = new AbsensiDokterResponder($dbtable, $uitable, $adapter);
	$dt		 = $dbres->command($_POST['command']);
	echo json_encode($dt);
	return;
}

$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("registration/resource/js/absensi_dokter.js",false);
echo $uitable->getModal()->getForm()->addElement("", $btngrup)->getHtml();
echo $uitable->getHtml();
?>