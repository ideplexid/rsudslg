<?php
global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'registration/class/RegistrationResource.php';
require_once 'registration/class/adapter/LapURJAdapter.php';
require_once 'registration/class/RegistrationResource.php';
$option=new OptionBuilder();
$option->add("","","1");
$option->add("Laki - Laki","0","0");
$option->add("Perempuan","1","0");

$barulama=new OptionBuilder();
$barulama->add("","","1");
$barulama->add("Baru","0","0");
$barulama->add("Lama","1","0");
$brlm=$barulama->getContent();
$jenis=RegistrationResource::getJenisPembayaran();
$jenis[]=array("name"=>"--Semua--","value"=>"%","default"=>"1");
$lal_uri=new MasterSlaveTemplate($db, "smis_rgv_layananpasien", "registration", "lap_urj_undetail");
$lal_uri->setDateTimeEnable(true);
$header=array ("No.","NRM","No. Reg","Tanggal","Jam","Carabayar",'Nama',"Umur",'Desa','Kecamatan',"Kabupaten","L/P","Ruangan","Baru/Lama");
$summary=new SimpleAdapter();
$summary->setUseNumber(true, "No.","back.");
$lal_uri->setAdapter($summary)
		->getAdapter()
		->setUseNumber(true, "No.","back.")
		->add("NRM", "nrm","only-digit8")
		->add("No. Reg", "id","only-digit8")
		->add("Jam", "tanggal","date H:i")
		->add("Tanggal", "tanggal","date d M Y")
		->add("Jam", "tanggal","date H:i")
		->add("Carabayar", "carabayar")
		->add("Ruangan", "jenislayanan","unslug")
		->add("Nama", "nama_pasien")
		->add("Umur", "umur")
		->add("L/P", "kelamin","trivial_0_Laki-Laki_Perempuan")
		->add("Desa", "nama_kelurahan")
		->add("Kecamatan", "nama_kecamatan")
		->add("Kabupaten", "nama_kabupaten")
		->add("Baru/Lama", "barulama","trivial_0_Baru_Lama");


if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$lal_uri->getDBtable()->addCustomKriteria(NULL, " uri='0'");
	$barulama=$_POST['barulama'];
	$carabayar=$_POST['carabayar'];
	$jk=$_POST['jenis_kelamin'];
	if($barulama!="") $lal_uri->getDBtable()->addCustomKriteria(" barulama ", " ='".$barulama."' ");
	if($carabayar!="") $lal_uri->getDBtable()->addCustomKriteria(" carabayar ", " LIKE '%".$carabayar."%' ");
	if($jk!="") $lal_uri->getDBtable()->addCustomKriteria(" kelamin ", " ='".$jk."' ");
	$lal_uri->getDBtable()->setShowAll(true);
	$lal_uri->getDBtable()->setOrder(" tanggal ASC ");
}
$btn=$lal_uri ->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->addNoClear("jenis_kelamin")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->setExcelButtonEnable(true)
			  ->getHeaderButton();
$lal_uri ->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "") 
		 ->addModal("carabayar", "select", "Jenis Pasien", $jenis)
		 ->addModal("barulama", "select", "Baru Lama", $brlm)
		 ->addModal("jenis_kelamin", "select", "Jenis Kelamin", $option->getContent());
$lal_uri ->getForm(true,"Laporan Rawat Jalan")
		 ->addElement("", $btn);
$lal_uri ->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);


$lal_uri ->addViewData("dari", "dari")
		 ->addViewData("sampai", "sampai")
		 ->addViewData("barulama", "barulama")
		 ->addViewData("carabayar", "carabayar")
		 ->addViewData("jenis_kelamin", "jenis_kelamin");
$lal_uri ->initialize();
?>