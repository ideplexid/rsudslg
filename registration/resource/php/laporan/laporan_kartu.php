<?php 
require_once 'smis-libs-class/MasterSlaveTemplate.php';
global $db;


$button=new Button("", "", "");
$button->setAction("laporan_kartu.view()");
$button->setClass("btn-primary");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-refresh");

$cetak=new Button("", "", "");
$cetak->setAction("laporan_kartu.print()");
$cetak->setClass("btn-primary");
$cetak->setIsButton(Button::$ICONIC);
$cetak->setIcon("fa fa-print");

$laporan_kartu=new MasterSlaveTemplate($db, "smis_rg_kartu", "registration", "laporan_kartu");
$laporan_kartu->getDBtable()->setOrder(" tanggal ASC ");
$uitable=$laporan_kartu->getUItable();
$uitable->setAction(false);
if(isset($_POST['dari']) && $_POST['dari']!="" && isset($_POST['sampai']) && $_POST['sampai']!=""){
	$laporan_kartu ->getDBtable()
					->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
					->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
}

$header=array("No.","Tanggal","Nama","NRM","Biaya");
$uitable->setHeader($header);
$adapter=new SummaryAdapter();
$adapter->addFixValue("Tanggal", "<strong>Total</strong>");
$adapter->addSummary("Biaya", "biaya","money Rp.");
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y H:i")
		->add("Nama", "nama_pasien")
		->add("NRM", "nrm_pasien","only-digit8")
		->add("Biaya", "biaya","money Rp.");
$adapter->addSummary("Biaya","biaya","money Rp.");
$laporan_kartu->setAdapter($adapter);
$laporan_kartu ->addViewData("dari", "dari")
				->addViewData("sampai", "sampai");

if($laporan_kartu->getDBResponder()->isPreload()){
	$laporan_kartu ->addFlag("dari", "Tanggal Awal", "Silakan Masukan Tanggal Mulai")
					->addFlag("sampai", "Tanggal Akhir", "Silakan Masukan Tanggal Akhir")
					->addNoClear("dari")
					->addNoClear("sampai")
					->setDateEnable(true)
					->getUItable()
					->setActionEnable(false)
					->addModal("dari", "date", "Awal", "")
					->addModal("sampai", "date", "Akhir", "");
	$laporan_kartu->getForm()->addElement("", $button)->addElement("", $cetak);		
}

$laporan_kartu->initialize();
?>