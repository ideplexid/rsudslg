<?php 
global $db;
if( isset($_POST['super_command']) && $_POST['super_command']!=""){
	switch($_POST['super_command']){
		case "kabupaten" : require_once "registration/snippet/selector_kabupaten.php"; return;
		case "kecamatan" : require_once "registration/snippet/selector_kecamatan.php"; return; 
		case "kelurahan" : require_once "registration/snippet/selector_kelurahan.php"; return;
		case "kedusunan" : require_once "registration/snippet/selector_kedusunan.php"; return;
	}
}

$uitable = new Report(array("No.","Nama","Kunjungan","NRM","No. BPJS","L/P","Umur","Tanggal","Alamat","Provinsi","Kabupaten","Kecamatan",'Kelurahan',"Dusun","Pasien","Datang","Rujukan","Perujuk","Instansi","Cara Bayar","Rawat","Rawat Jalan","Kamar Inap","Pulang","Perusahaan","Asuransi","Asal Data","Telpon"),"", NULL);
$uitable ->setName("laporan_pasien_daftar")
		 ->setDiagram(true)
		 ->setModelLineUsability(false)
		 ->setModelAreaUsability(false)
		 ->setAllowDateEmptyEnable(true);
$is_backup = getSettings($db,"smis-reg-duplicate-vregister","0")=="1";
$db_name   = $is_backup?"smis_rg_vregisterb":"smis_rg_vregister";
/*this is respond when system have to response*/
if(isset($_POST['command'])){	
	require_once "registration/class/responder/PasienDaftarReport.php";
    $dbtable = new DBTable($db,$db_name);    
    if(isset($_POST['pulang']) && $_POST['pulang']!=="-"){
        if($_POST['pulang']=="bukan_tidak_datang"){
            $dbtable->addCustomKriteria(" carapulang "," !='Tidak Datang' ");
        }else if($_POST['pulang']=="pulang_kecuali_tidak_datang"){
            $dbtable->addCustomKriteria(NULL," carapulang !='Tidak Datang' AND selesai=1 ");
        }else if($_POST['pulang']=="pulang_kecuali_tidak_datang"){
            $dbtable->addCustomKriteria(NULL,"  selesai=0 ");
        }else{
            $dbtable->addCustomKriteria(" carapulang "," ='".$_POST['pulang']."' ");
        }
        
    }
    $dbtable->addCustomKriteria(NULL," carapulang != 'Tidak Datang' ");
	    
	$tableadapter = new SimpleAdapter();
	$tableadapter ->setUseNumber(true,"No.","back.")
				  ->add("Nama", "nama")
				  ->add("Tanggal", "waktu","date d M Y")
				  ->add("NRM","nrm","digit8")
				  ->add("No. BPJS","no_bpjs")
				  ->add("Kunjungan","no_kunjungan")
				  ->add("Alamat", "alamat")
				  ->add("Pasien", "baru")
				  ->add("Datang", "caradatang")
				  ->add("Rujukan", "rujukan")
				  ->add("Perujuk", "nama_rujukan")
				  ->add("Instansi", "instansi_rujukan")
				  ->add("Cara Bayar", "carabayar")
				  ->add("Provinsi", "nama_provinsi")
				  ->add("Kabupaten", "nama_kabupaten")
				  ->add("Kecamatan", "nama_kecamatan")
				  ->add("Kelurahan", "nama_kelurahan")
				  ->add("Dusun", "nama_kedusunan")
				  ->add("Rawat", "uri","trivial_0_RJ_RI")
                  ->add("L/P", "kelamin")
                  ->add("Umur", "umur")
				  ->add("Kamar Inap", "kamar_inap","unslug")
				  ->add("Rawat Jalan", "jenislayanan","unslug")
				  ->add("Pulang", "carapulang","unslug")
				  ->add("Asal Data", "origin","unslug")
				  ->add("Perusahaan", "nama_perusahaan")
				  ->add("Asuransi", "nama_asuransi")
                  ->add("Telpon", "telpon");
	
	$diagramadapter = new SimpleAdapter();
	$diagramadapter ->setUseID(false);
	$diagramadapter ->add("Lokasi", "nama","empty KOSONG")
					->add("jumlah", "jumlah");
	
	$dbres = new PasienDaftarReport($dbtable,$uitable,$tableadapter,'waktu',DBReport::$DATE);
	$dbres ->setTimeGroup(false);
    
	$group			= "";
	$diagram_view 	= "";
	if(isset($_POST['diagram'])){
		if( $_POST['diagram']=="Lokasi"){
			if($_POST['provinsi']=="%"){
				$diagram_view	=" count(*) as jumlah, nama_provinsi as nama";
				$group			=" provinsi ASC ";
			}else if($_POST['kabupaten']=="%"){
				$diagram_view	=" count(*) as jumlah, nama_kabupaten as nama";
				$group			=" provinsi ASC, kabupaten ASC ";
			}else if($_POST['kecamatan']=="%"){
				$diagram_view	=" count(*) as jumlah, nama_kecamatan as nama";
				$group			=" provinsi ASC, kabupaten ASC, kecamatan ASC ";
			}else if($_POST['kelurahan']=="%"){
				$diagram_view	=" count(*) as jumlah, nama_kelurahan as nama";
				$group			=" provinsi ASC, kabupaten ASC, kecamatan ASC, kelurahan ASC  ";
			}else {
				$diagram_view	=" count(*) as jumlah, nama_kedusunan as nama";
				$group			=" provinsi ASC, kabupaten ASC, kecamatan ASC, kelurahan ASC , kedusunan ASC ";
			}
		}else if($_POST['diagram']=="Rujukan"){
			$diagram_view		=" count(*) as jumlah, rujukan as nama";
			$group				=" rujukan ASC ";
		}else if($_POST['diagram']=="Jenis Pasien"){
			$diagram_view		=" count(*) as jumlah, carabayar as nama";
			$group				=" carabayar ASC ";
		}else if($_POST['diagram']=="URJ/URI"){
			$diagram_view		=" count(*) as jumlah, if(uri=0,'Rawat Jalan','Rawat Inap') as nama ";
			$group				=" uri ASC ";
		}else if($_POST['diagram']=="Jenis Kelamin"){
			$diagram_view		=" count(*) as jumlah, kelamin as nama";
			$group				=" kelamin ASC ";
		}else if($_POST['diagram']=="Kamar Inap"){
			$diagram_view		=" count(*) as jumlah, kamar_inap as nama";
			$group				=" kamar_inap ASC ";
		}else if($_POST['diagram']=="Rawat Jalan"){
			$diagram_view		=" count(*) as jumlah, jenislayanan as nama";
			$group				=" jenislayanan ASC ";
		}
	}
    
    if(isset($_POST['jenislayanan']) && $_POST['jenislayanan']!=""){
       $dbtable->addCustomKriteria("jenislayanan","='".$_POST['jenislayanan']."' ");
    }    
    if(isset($_POST['kamar_inap']) && $_POST['kamar_inap']!=""){
       $dbtable->addCustomKriteria("kamar_inap","='".$_POST['kamar_inap']."' ");        
    }    
    if(isset($_POST['kunjungan']) && $_POST['kunjungan']!=""){
       $dbtable->addCustomKriteria(" no_kunjungan "," '".$_POST['kunjungan']."' ");        
    }    
    if(isset($_POST['f_no_bpjs']) && $_POST['f_no_bpjs']!=""){
       $dbtable->addCustomKriteria(" no_bpjs "," ='".$_POST['f_no_bpjs']."' ");        
    }    
    if(isset($_POST['origin_data']) && trim($_POST['origin_data'])!=""){
       $dbtable->addCustomKriteria(" origin "," ='".$_POST['origin_data']."' ");        
    }    
    if(isset($_POST['from_date']) && $_POST['from_date'] == "") {
        $dbres->setUseFromControl(false);
    }    
    if(isset($_POST['to_date']) && $_POST['to_date'] == "") {
        $dbres->setUseToControl(false);
    }
	
	$dbres->setDiagram(true, $diagram_view,$group, $diagramadapter);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data,JSON_NUMERIC_CHECK);
	return;
}

$excel   = new Button();
$excel   ->setClass("btn btn-primary")
		 ->setIcon(" fa fa-file-excel-o")
		 ->setIsButton(Button::$ICONIC)
		 ->setAction("laporan_pasien_daftar.excel()");
$uitable ->addFooterButton($excel);

$propinsi_dbtable = new DBTable($db,"smis_rg_propinsi");
$propinsi_dbtable ->setShowAll(true);
$propinsi_dbtable ->setOrder(" nama ASC ");
$data 			  = $propinsi_dbtable->view("","0");
$prop_adapter	  = new SelectAdapter("nama", "id");
$propinsi		  = $prop_adapter->getContent($data['data']);
$propinsi[]		  = array("name"=>"--- SEMUA ---","value"=>"%");

$jenis  			= new DBTable($db,"smis_rg_jenispasien");
$jenis  			->setShowAll(true);
$jenis  			->setOrder(" nama ASC ");
$data				= $jenis->view("","0");
$jenis_adapter		= new SelectAdapter("nama", "slug");
$jenis_pembayaran	= $jenis_adapter->getContent($data['data']);
$jenis_pembayaran[]	= array("name"=>"--- SEMUA ---","value"=>"%");

$query="SELECT DISTINCT origin FROM ".$db_name." WHERE prop!='del' ";
$origin=new OptionBuilder();
$origin->add(" - Semua - ","",1);
$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$jenis_pasien		= array(array("name"=>"Lama","value"=>"lama"),array("name"=>"Baru","value"=>"baru"),array("name"=>"--- SEMUA ---","value"=>"%"));
$jenis_kedatangan	= array(array("name"=>"Datang Sendiri","value"=>"Datang Sendiri"),array("name"=>"Rujukan","value"=>"Rujukan"),array("name"=>"--- SEMUA ---","value"=>"%"));
$jenis_perujuk		= array(array("name"=>"Dokter","value"=>"Dokter"),array("name"=>"Mantri","value"=>"Mantri"),array("name"=>"Bidan","value"=>"Bidan"),array("name"=>"Puskesmas","value"=>"Puskesmas"),array("name"=>"RS Lain","value"=>"RS Lain"),array("name"=>"Balai Pengobatan Lain","value"=>"Balai Pengobatan Lain"),array("name"=>"--- SEMUA ---","value"=>"%"));
$jenis_layanan		= array(array("name"=>"","value"=>"%","default"=>"1"),array("name"=>"Rawat Jalan","value"=>"0"),array("name"=>"Rawat Inap","value"=>"1"));
$jenis_kunjungan    = array(array("name"=>"","value"=>"","default"=>"1"),array("name"=>">5","value"=>">5"),  array("name"=>">6","value"=>">6"), array("name"=>">7","value"=>">7"), array("name"=>">8","value"=>">8"), array("name"=>">9","value"=>">9"), array("name"=>">10","value"=>">10") );

$diagram = new OptionBuilder();
$diagram ->add("Rujukan")
		 ->add("Jenis Pasien")
		 ->add("URJ/URI")
		 ->add("Lokasi")
		 ->add("Jenis Kelamin")
		 ->add("Rawat Jalan")
		 ->add("Kamar Inap");

$jk = new OptionBuilder();
$jk ->add("","%","1")
	->add("Laki-Laki","Laki-Laki","0")
	->add("Perempuan","Perempuan","0");

loadLibrary("smis-libs-function-medical");
$pulang 	= array();
$pulang[]	= array("name"=>"  ","value"=>"-","default"=>"1");
//$pulang[]	= array("name"=>" - Semua Kecuali Tidak Datang - ","value"=>"bukan_tidak_datang","default"=>"");
//$pulang[]	= array("name"=>" - Semua Yang Pulang Kecuali Tidak Datang - ","value"=>"pulang_kecuali_tidak_datang","default"=>"");
//$pulang[]	= array("name"=>" - Belum Pulang - ","value"=>"aktif","default"=>"");
$pulang2	= medical_carapulang();
foreach($pulang2 as $x){
    if($x['name']!="Tidak Datang"){
        $pulang[] = $x;
    }
}

require_once 'smis-base/smis-include-service-consumer.php';
require_once 'registration/class/RegistrationResource.php';
$content	= RegistrationResource::getURJIP($db);
$uri		= RegistrationResource::getURIFromContent($content);
$uri[]		= array("name"=>"","value"=>"%","default"=>"1");
$urj		= RegistrationResource::getURJFromContent($content);
$urj[]		= array("name"=>"","value"=>"%","default"=>"1");

/*This is Modal Form and used for add and edit the table*/
$mcolumn	= array("provinsi","kabupaten","kecamatan",'kelurahan','kedusunan',"baru","caradatang","rujukan","carabayar","uri","diagram","kelamin","kunjungan","pulang","origin_data","f_no_bpjs");
$mname		= array("Provinsi","Kabupaten","Kecamatan",'Kelurahan','Dusun',"Pasien","Datang","Rujukan","Cara Bayar","Layanan","Diagram","Jenis Kelamin","Kunjungan","Pulang","Asal Data", "No. BPJS");
$mtype		= array("select","select","select","select","select","select","select","select","select","select","select","select","select","select","select", "text");
$mvalue		= array($propinsi,"","","","",$jenis_pasien,$jenis_kedatangan,$jenis_perujuk,$jenis_pembayaran,$jenis_layanan,$diagram->getContent(),$jk->getContent(),$jenis_kunjungan,$pulang,$origin->getContent(), "");
$uitable	->setModal($mcolumn, $mtype, $mname, $mvalue)
			->addModal("jenislayanan", "select", "Rawat Jalan", $urj)
			->addModal("kamar_inap", "select", "Rawat Inap",  $uri);

$modal = $uitable->getAdvanceModal();
$modal ->setTitle("Laporan Pendaftaran Pasien");
$form  = $modal->getForm();

if($is_backup){
    $backup = new Button("","","Backup Data");
	$backup ->setAction("laporan_pasien_daftar.backup()")
			->setIsButton(Button::$ICONIC_TEXT)
			->setClass("btn-primary")
			->setIcon(" fa fa-download");
    $form	->addElement("",$backup);
}

echo "<div>".$form->getHtml()."</div>";
echo "<div class='clear line'></div>";
echo "<div>".$uitable->getDiagram("laporan_pasien_daftar_mychart")."</div>";
echo "<div class='clear line'></div>";
echo "<div>".$uitable->getHtml()."</div>";
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addCSS ("framework/bootstrap/css/bootstrap-select.css");
echo addCSS ("framework/bootstrap/css/morris.css");
echo addJS  ("base-js/smis-base-loading.js");
echo addJS  ("framework/jquery/raphael-min.js");
echo addJS  ("framework/bootstrap/js/morris.min.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/smis/js/report_action.js");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("framework/bootstrap/js/bootstrap-select.js");
echo addJS  ("registration/resource/js/laporan_pasien_daftar.js",false);
echo addCSS ("registration/resource/css/laporan_pasien_daftar.css",false);

?>
