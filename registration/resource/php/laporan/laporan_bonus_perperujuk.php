<?php 
	$phead=array("Nama","Alamat","Instansi");
	$ptable=new Table($phead);
	$ptable->setName("perujuk")
		   ->setModel(Table::$SELECT);
	$padapter=new SimpleAdapter();
	$padapter->add("Nama","nama")
			 ->add("Alamat", "alamat")
			 ->add("Instansi", "instansi");
	$pdbtable=new DBTable($db,"smis_rg_perujuk");
	$pdbres=new DBResponder($pdbtable, $ptable, $padapter);
	$super=new SuperCommand();
	$super->addResponder("perujuk", $pdbres);
	$data=$super->initialize();
	if($data!=null){
		echo $data;
		return;
	}

	$btn=new Button("", "", "");
	$btn->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setAction("laporan_bonus_perperujuk.cair()")
		->setIcon("fa fa-money");

	$print=new Button("", "", "");
	$print->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setAction("laporan_bonus_perperujuk.print()")
		->setIcon("fa fa-print");


	$header=array("Tanggal","Pasien","NRM","No Registrasi","Bonus","URI/URJ","Keterangan","Ambil","Nilai");
	$uitable=new Table($header,"",NULL,false);
	$btngrup=$uitable->setFooterVisible(false)
				 ->setAddButtonEnable(false)
				 ->setPrintButtonEnable(false)
				 ->setName("laporan_bonus_perperujuk")
				 ->addHeaderButton($print)
				 ->addHeaderButton($btn)
				 ->getHeaderButton();
				 
	if($_POST['command']){
		require_once 'registration/class/adapter/BonusAdapter.php';
		require_once 'registration/class/responder/BonusPerujukResponder.php';
		$adapter=new BonusAdapter();
		$adapter->add("Pasien", "nama_pasien")
				->add("NRM", "nrm","digit8")
				->add("No Registrasi", "id","digit8")
				->add("Tanggal","tanggal", "date d M Y")
				->add("Keterangan","keteranganbonus")
				->add("URI/URJ", "uri","trivial_0_<strong class='badge badge-important'>Rawat Jalan</strong>_<strong class='badge badge-success'>Rawat Inap</strong>")
				->add("Bonus", "besarbonus","money Rp.");
		$dbtable=new DBTable($db, "smis_rgv_layananpasien");
		$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'")
				->addCustomKriteria(NULL, "tanggal<='".$_POST['sampai']."'")
				->addCustomKriteria(NULL, "id_rujukan='".$_POST['id_perujuk']."'")
				->setShowAll(true);
		$dbres=new BonusPerujukResponder($dbtable, $uitable, $adapter);
		$dt=$dbres->command($_POST['command']);
		echo json_encode($dt);
		return;
	}

	$uitable->addModal("perujuk", "chooser-laporan_bonus_perperujuk-perujuk", "Perujuk", "");
	$uitable->addModal("alamat", "text", "Alamat", "","y",NULL,true);
	$uitable->addModal("instansi", "text", "Instansi", "","y",NULL,true);
	$uitable->addModal("id_perujuk", "hidden", "", "");
	$uitable->addModal("dari", "date", "Dari", "");
	$uitable->addModal("sampai", "date", "Sampai", "");

	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("registration/resource/js/laporan_bonus_perperujuk.js",false);
	echo $uitable->getModal()->getForm()->addElement("", $btngrup)->getHtml();
	echo $uitable->getHtml();
?>