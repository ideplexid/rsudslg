<?php

setChangeCookie ( false );
$header = array ();
$header [] = "no";
$header [] = "tanggal";
$header [] = "no_kartu_peserta";
$header [] = "nama_peserta";
$header [] = "diagnosa";
$header [] = "dirujuk";
$header [] = "spesialistik";
$header [] = "non_spesialistik";
$header [] = "alasan";

$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_rujukan" );
$uitable->addHeader ( "before", "<tr>
								<th rowspan='2'>NO</th>
								<th rowspan='2'>TANGGAL</th>
								<th rowspan='2'>NO. KARTU PESERTA</th>
								<th rowspan='2'>NAMA PESERTA</th>
								<th rowspan='2'>DIAGNOSA</th>
								<th rowspan='2'>DI RUJUK</th>
								<th colspan='3'>KETERANGAN</th>
                                </tr>" );
$uitable->addHeader ( "before", "<tr>
								<th>RUJUKAN SPESIALISTIK</th>
								<th>RUJUKAN NON SPESIALISTIK</th>
								<th>ALASAN RUJUKAN NON SPESIALISTIK</th>
                                </tr>" );
$uitable->setHeaderVisible ( false );
$uitable->setFooterVisible ( false );

if(isset($_POST['command'])) {
    require_once ("registration/class/adapter/LapRujukanAdapter.php");
    $adapter = new LapRujukanAdapter ();
	$dbtable = new DBTable ( $db, "smis_rgv_layananpasien" );
    
    if(isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
        $dbtable->addCustomKriteria ( "'" . $_POST ['dari'] . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $_POST ['sampai'] . "'", ">tanggal" );
		$dbtable->setShowAll ( true );
    }
    if(isset ( $_POST ['origin'] ) && $_POST ['origin'] != "") {
        $dbtable->addCustomKriteria ( " origin ", "='".$_POST['origin']."'" );
    }
    
    
    $dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$query="SELECT DISTINCT origin FROM smis_rgv_layananpasien WHERE prop!='del' ";
$option=new OptionBuilder();
$option->add(" - Semua - ","",1);

$res=$db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $option->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$uitable->addModal ( "dari", "datetime", "Dari", "" );
$uitable->addModal ( "sampai", "datetime", "Sampai", "" );
$uitable->addModal ( "origin", "select", "Asal", $option->getContent() );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh" );
$button->setClass("btn-primary");
$button->setAction ( "lap_rujukan.view()" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
echo addJS ( "registration/resource/js/lap_rujukan.js" );

?>