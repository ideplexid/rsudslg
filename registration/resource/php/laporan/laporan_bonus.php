<?php 
global $db;
if( isset($_POST['super_command']) && $_POST['super_command']!=""){
	switch($_POST['super_command']){
		case "kabupaten" : require_once "registration/snippet/selector_kabupaten.php"; return;
		case "kecamatan" : require_once "registration/snippet/selector_kecamatan.php"; return; 
		case "kelurahan" : require_once "registration/snippet/selector_kelurahan.php"; return;
	}
}

$uitable=new Report(array("No.","Nama","NRM","L/P","Tanggal","Alamat","Pasien","Datang","Rujukan","Cara Bayar","Rawat","Perujuk","Bonus","Ambil Bonus","Nilai Bonus"),"", NULL);
$uitable->setName("laporan_bonus");
$uitable->setDiagram(true);
$uitable->setModelLineUsability(false);
$uitable->setModelAreaUsability(false);
/*this is respond when system have to response*/
if(isset($_POST['command'])){
	$dbtable=new DBTable($db,' smis_rg_vregister');
	$tableadapter=new SimpleAdapter();
	$tableadapter->setUseNumber(true,"No.","back.");
	$tableadapter->add("Nama", "nama");
	$tableadapter->add("Tanggal", "waktu","date d M Y");
	$tableadapter->add("NRM","id","digit8");
	$tableadapter->add("Alamat", "alamat");
	$tableadapter->add("Pasien", "baru");
	$tableadapter->add("Datang", "caradatang");
	$tableadapter->add("Rujukan", "rujukan");
	$tableadapter->add("Cara Bayar", "carabayar");
	$tableadapter->add("Provinsi", "nama_provinsi");
	$tableadapter->add("Kabupaten", "nama_kabupaten");
	$tableadapter->add("Kecamatan", "nama_kecamatan");
	$tableadapter->add("Kelurahan", "nama_kelurahan");
	$tableadapter->add("Rawat", "uri","trivial_0_RJ_RI");
	$tableadapter->add("L/P", "kelamin","trivial_0_Laki-Laki_Perempuan");
	$tableadapter->add("Perujuk", "nama_rujukan");
	$tableadapter->add("Ambil Bonus", "ambilbonus");
	$tableadapter->add("Nilai Bonus", "besarbonus","money");
	$tableadapter->add("Bonus", "bonus");
	
	$diagramadapter=new SimpleAdapter();
	$diagramadapter->setUseID(false);
	$diagramadapter->add("Lokasi", "nama","empty KOSONG");
	$diagramadapter->add("jumlah", "jumlah");
	
	$dbres=new DBReport($dbtable,$uitable,$tableadapter,'waktu',DBReport::$DATE);
	$dbres->setTimeGroup(false);
	$group="";
	if($_POST['diagram']=="Lokasi"){
		if($_POST['provinsi']=="%"){
			$diagram_view=" count(*) as jumlah, nama_provinsi as nama";
			$group=" provinsi ASC ";
		}else if($_POST['kabupaten']=="%"){
			$diagram_view=" count(*) as jumlah, nama_kabupaten as nama";
			$group=" provinsi ASC, kabupaten ASC ";
		}else if($_POST['kecamatan']=="%"){
			$diagram_view=" count(*) as jumlah, nama_kecamatan as nama";
			$group=" provinsi ASC, kabupaten ASC, kecamatan ASC ";
		}else {
			$diagram_view=" count(*) as jumlah, nama_kelurahan as nama";
			$group=" provinsi ASC, kabupaten ASC, kecamatan ASC, kelurahan ASC  ";
		}
	}else if($_POST['diagram']=="Rujukan"){
		$diagram_view=" count(*) as jumlah, rujukan as nama";
		$group=" rujukan ASC ";
	}else if($_POST['diagram']=="Jenis Pasien"){
		$diagram_view=" count(*) as jumlah, carabayar as nama";
		$group=" carabayar ASC ";
	}else if($_POST['diagram']=="URJ/URI"){
		$diagram_view=" count(*) as jumlah, if(uri=0,'Rawat Jalan','Rawat Inap') as nama ";
		$group=" uri ASC ";
	}else if($_POST['diagram']=="Jenis Kelamin"){
		$diagram_view=" count(*) as jumlah, kelamin as nama";
		$group=" kelamin ASC ";
	}
	
	$dbres->setDiagram(true, $diagram_view,$group, $diagramadapter);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data,JSON_NUMERIC_CHECK);
	return;
}

$propinsi_dbtable = new DBTable($db,"smis_rg_propinsi");
$propinsi_dbtable->setShowAll(true);
$propinsi_dbtable->setOrder(" nama ASC ");
$data=$propinsi_dbtable->view("","0");
$prop_adapter=new SelectAdapter("nama", "id");
$propinsi=$prop_adapter->getContent($data['data']);
$propinsi[]=array("name"=>"--- SEMUA ---","value"=>"%");

$jenis = new DBTable($db,"smis_rg_jenispasien");
$jenis->setShowAll(true);
$jenis->setOrder(" nama ASC ");
$data=$jenis->view("","0");
$jenis_adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$jenis_adapter->getContent($data['data']);
$jenis_pembayaran[]=array("name"=>"--- SEMUA ---","value"=>"%");

$jenis_pasien		= array(array("name"=>"Lama","value"=>"lama"),array("name"=>"Baru","value"=>"baru"),array("name"=>"--- SEMUA ---","value"=>"%"));
$jenis_kedatangan	= array(array("name"=>"Datang Sendiri","value"=>"Datang Sendiri"),array("name"=>"Rujukan","value"=>"Rujukan"),array("name"=>"--- SEMUA ---","value"=>"%"));
$jenis_perujuk		= array(array("name"=>"Dokter","value"=>"Dokter"),array("name"=>"Mantri","value"=>"Mantri"),array("name"=>"Bidan","value"=>"Bidan"),array("name"=>"Puskesmas","value"=>"Puskesmas"),array("name"=>"RS Lain","value"=>"RS Lain"),array("name"=>"Balai Pengobatan Lain","value"=>"Balai Pengobatan Lain"),array("name"=>"--- SEMUA ---","value"=>"%"));
$jenis_layanan		= array(array("name"=>"","value"=>"%","default"=>"1"),array("name"=>"Rawat Jalan","value"=>"0"),array("name"=>"Rawat Inap","value"=>"1"));

$diagram=new OptionBuilder();
$diagram->add("Rujukan");
$diagram->add("Jenis Pasien");
$diagram->add("URJ/URI");
$diagram->add("Lokasi");
$diagram->add("Jenis Kelamin");


/*This is Modal Form and used for add and edit the table*/
$mcolumn=array("provinsi","kabupaten","kecamatan",'kelurahan',"baru","caradatang","rujukan","carabayar","uri","diagram");
$mname=array("Provinsi","Kabupaten","Kecamatan",'Kelurahan',"Pasien","Datang","Rujukan","Cara Bayar","Layanan","Diagram");
$mtype=array("select","select","select","select","select","select","select","select","select","select");
$mvalue=array($propinsi,"","","",$jenis_pasien,$jenis_kedatangan,$jenis_perujuk,$jenis_pembayaran,$jenis_layanan,$diagram->getContent());
$uitable->setModal($mcolumn, $mtype, $mname, $mvalue);
$modal=$uitable->getAdvanceModal();
$modal->setTitle("Laporan Pendaftaran Pasien");

echo addCSS("framework/bootstrap/css/datepicker.css");
echo addCSS("framework/bootstrap/css/bootstrap-select.css");
echo addCSS("framework/bootstrap/css/morris.css");
echo addCSS("registration/resource/css/laporan_bonus.css",false);
echo addJS("framework/jquery/raphael-min.js");
echo addJS("framework/bootstrap/js/morris.min.js");
echo addJS("framework/smis/js/table_action.js");
echo addJS("framework/smis/js/report_action.js");
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addJS("registration/resource/js/laporan_bonus.js",false);
echo "<div >".$modal->getForm()->getHtml()."</div>";
echo "<div class='clear line'></div>";
echo "<div >".$uitable->getDiagram("laporan_bonus_mychart")."</div>";
echo "<div class='clear line'></div>";
echo "<div>". $uitable->getHtml()."</div>";
?>