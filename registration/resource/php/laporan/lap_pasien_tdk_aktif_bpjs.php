<?php
global $db;
$header      = array("No.", "NRM", "No.KTP", "No.BPJS", "Nama Pasien", "No.Register", "Tanggal Berobat", "Aktif", "Periode", "Poli");
$uitable     = new Table($header);
$uitable     ->setAction(false);
$uitable     ->setName("lap_pasien_tdk_aktif_bpjs");

if(isset($_POST['command'])) {
    $adapter = new SimpleAdapter();
    $adapter ->setUseNumber(true, "No.","back.");
    $adapter ->add("NRM", "nrm")
             ->add("No.KTP", "ktp")
             ->add("No.BPJS", "nobpjs")
             ->add("Nama Pasien", "nama_pasien")
             ->add("No.Register", "id")
             ->add("Tanggal Berobat", "tanggal", "date d M Y")
             ->add("Aktif", "status_bpjs", "trivial_0_TIDAK_AKTIF")
             ->add("Periode", "tanggal", "date M Y")
             ->add("Poli", "jenislayanan", "unslug");            
    $dbtable = new DBTable($db, "smis_rgv_layananpasien");
    $dbtable ->setOrder("tanggal ASC")
             ->addCustomKriteria(NULL, "prop != 'del'")
             ->addCustomKriteria(NULL, "uri = '0'")
             ->addCustomKriteria(NULL, "status_bpjs = '0'");    
    require_once "registration/class/responder/LapPasienTdkAktifBPJSResponder.php";
    $dbres = new LapPasienTdkAktifBPJSResponder($dbtable, $uitable, $adapter);    
    if(isset($_POST['dari']) && isset($_POST['sampai'])) {
        $dbtable ->addCustomKriteria(NULL, "tanggal >='".$_POST['dari']."'")
                 ->addCustomKriteria(NULL, "tanggal <'".$_POST['sampai']."'");
    }
    if($_POST['asal'] != NULL && $_POST['asal'] != "") {
        $dbtable ->addCustomKriteria(NULL, "origin ='".$_POST['asal']."'");
    }    
    if($_POST['layanan'] != NULL && $_POST['layanan'] != "") {
        $dbtable ->addCustomKriteria(NULL, "jenislayanan ='".$_POST['layanan']."'");
    }    
    $data       = $dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$query  = "SELECT DISTINCT origin FROM smis_rgv_layananpasien WHERE prop!='del' ";
$origin = new OptionBuilder();
$origin ->add(" - Semua - ","",1);
$res    = $db->get_result($query);
foreach($res as $x){
    if($x->origin==""){
        continue;
    }
    $origin->add(ArrayAdapter::slugFormat("unslug",$x->origin),$x->origin);
}

$load   = new Button("", "", "");
$load   ->setClass("btn-primary")
        ->setIsButton(Button::$ICONIC)
        ->setIcon("fa fa-refresh")
        ->setAction("lap_pasien_tdk_aktif_bpjs.view()");

$print  = new Button("", "", "");
$print  ->setClass("btn-primary")
        ->setIsButton(Button::$ICONIC)
        ->setIcon("fa fa-print")
        ->setAction("lap_pasien_tdk_aktif_bpjs.print()");

$excel  = new Button("", "", "");
$excel  ->setClass("btn-primary")
        ->setIsButton(Button::$ICONIC)
        ->setIcon("fa fa-file-excel-o")
        ->setAction("lap_pasien_tdk_aktif_bpjs.excel()");

$btgrup = new ButtonGroup("");
$btgrup ->addButton($load)
	    ->addButton($print)
	    ->addButton($excel);

$query      = "SELECT DISTINCT jenislayanan FROM smis_rgv_layananpasien WHERE prop!='del' ";
$res        = $db->get_result($query);
$layanan    = array();
$i          = 0;
foreach($res as $x){
    if($x->jenislayanan=="" || $x->jenislayanan == NULL){
        continue;
    }
    $layanan[$i]['name']    = ArrayAdapter::slugFormat("unslug",$x->jenislayanan);
    $layanan[$i]['value']   = $x->jenislayanan;
    $i++;
}
$layanan[$i]['name']        = " - Semua - ";
$layanan[$i]['value']       = "";

$uitable ->addModal("asal","select","Asal Data",$origin->getContent())
         ->addModal("layanan","select","Poli",$layanan)
         ->addModal("dari","date","Dari","")
         ->addModal("sampai","date","Sampai","");
$form    = $uitable->getModal()->getForm();
$form    ->addElement("", $btgrup);

echo $form      ->getHtml();
echo $uitable   ->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("registration/resource/js/lap_pasien_tdk_aktif_bpjs.js",false);
?>