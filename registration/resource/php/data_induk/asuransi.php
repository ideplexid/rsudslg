<?php

	$header=array ('Nama','Alamat','Telepon',"Awal Kontrak","Akhir Kontrak","Jenis Pasien", "Pemerintah");
	$uitable = new Table ( $header , "Asuransi", NULL, true );
	$uitable->setName ( 'asuransi' );
	
	if (isset ( $_POST ['command'] )) {
		$adapter = new SimpleAdapter();
		$adapter ->add("Nama", "nama")
			->add("Alamat", "alamat")
			->add("Telepon", "telpon")
			->add("Awal Kontrak", "awal_kontrak","date d-M-Y")
			->add("Akhir Kontrak", "akhir_kontrak","date d-M-Y")
			->add("Jenis Pasien", "jenis_pasien","unslug")
			->add("Pemerintah", "pemerintah","trivial_1_&#10003_&#10005");
		$dbtable = new DBTable ( $db, 'smis_rg_asuransi');
		$dbresponder = new DBResponder ( $dbtable, $uitable, $adapter );
		$datapasien = $dbresponder->command ( $_POST ['command'] );
		echo json_encode ( $datapasien );
		return;
	}

	$dbtable = new DBTable($db,"smis_rg_jenispasien");
	$dbtable ->setShowAll(true);
	$data = $dbtable ->view("","0");
	$list = $data['data'];
	$selectadapter = new SelectAdapter("nama","slug");
	$ctx  = $selectadapter->getContent($list);
	$ctx[] = array("name"=>"","value"=>"","default"=>"1");

	$uitable->addModal("id", "hidden", "","");
	$uitable->addModal("nama", "text", "Nama", "");
	$uitable->addModal("alamat", "text", "Alamat", "");
	$uitable->addModal("telpon", "text", "Telepon", "");
	$uitable->addModal("awal_kontrak", "date", "Awal Kontrak", "");
	$uitable->addModal("akhir_kontrak", "date", "Akhir Kontrak", "");
	$uitable->addModal("jenis_pasien", "select", "Jenis Pasien", $ctx);
	$uitable->addModal("pemerintah", "checkbox", "Pemerintah", "");
	$modal = $uitable->getModal ();
	$modal->setTitle ( "Asuransi" );
	
	echo $uitable->getHtml ();
	echo $modal->getHtml ();
	echo addJS ( "framework/smis/js/table_action.js" );
	echo addCSS ( "framework/bootstrap/css/datepicker.css" );
	echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );

?>
<script type="text/javascript">
var asuransi;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id','nama','alamat','telpon','awal_kontrak','akhir_kontrak',"jenis_pasien","pemerintah");
	asuransi=new TableAction("asuransi","registration","asuransi",column);
	asuransi.view();
});
</script>
