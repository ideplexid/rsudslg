<?php 

require_once("smis-base/smis-include-service-consumer.php");
require_once 'registration/class/BedService.php';

global $db;
$bed=new BedService($db,"get_bed");
$bed->execute();
$bedroom=$bed->getContent();
$data=$bed->getList();
$resume=$bed->getResume();
$header=array();
$header[]="No.";
$header[]="Ruang";
$header[]="Bed";
$header[]="Nama";
$header[]="NRM";
$header[]="No. Reg";
$header[]="L/P";
$header[]="Ayah";
$header[]="Ibu";
$header[]="Alamat";
$header[]="RT";
$header[]="RW";
$header[]="Desa";
$header[]="Kecamatan";
$header[]="Kabupaten";
$header[]="Propinsi";
$header[]="Status";

$adapter=new SimpleAdapter(true,"No.");
$adapter->add("Nama","nama");
$adapter->add("NRM","nrm","digit8");
$adapter->add("Ruang","ruang","unslug");
$adapter->add("Bed","bed");
$adapter->add("Ayah","ayah");
$adapter->add("Ibu","ibu");
$adapter->add("Alamat","alamat");
$adapter->add("RT","rt");
$adapter->add("RW","rw");
$adapter->add("Desa","desa");
$adapter->add("Kecamatan","kecamatan");
$adapter->add("Kabupaten","kabupaten");
$adapter->add("Status","status");
$adapter->add("Propinsi","propinsi");
$adapter->add("L/P","jk","trivial_0_L_P");
$adapter->add("No. Reg","noreg","only-digit10");
$content=$adapter->getContent($data);

$tabs=new Tabulator("", "");
$input=new Text("search_box_ruang_pasien", "", "");
$header_0="<tr>
		<td>Pencarian</td>
		<td colspan='100'>".$input->getHtml()."</td>
		</tr>";
$table=new Table($header,"Pasien Rawat Inap",NULL,false);
$table->setContent($content);
$table->addHeader("before", $header_0);
$table->setFooterVisible(false);
$table->setName("ruang_pasien");
$tabs->add("detil", "Pasien", $table,Tabulator::$TYPE_COMPONENT);

$radapter=new SimpleAdapter();
$radapter->add("Ruangan", "ruangan","unslug");
$radapter->add("Laki-Laki", "L");
$radapter->add("Perempuan", "P");
$radapter->add("Total", "Total");
$content=$radapter->getContent($resume);
$resume=array();
$resume[]="Ruangan";
$resume[]="Laki-Laki";
$resume[]="Perempuan";
$resume[]="Total";
$table=new Table($resume,"Resume",NULL,false);
$table->setContent($content);
$table->setFooterVisible(false);
$tabs->add("resume", "Resume", $table,Tabulator::$TYPE_COMPONENT);
echo $tabs->getHtml();
?>


<script type="text/javascript">
$(document).ready(function(){
	$('#search_box_ruang_pasien').keyup(function() {
		var $rows = $('table#table_ruang_pasien tbody tr');
	    var val=$(this).val();
		$rows.show().filter(function() {
	        var this_text=$(this).text().toLowerCase();
	    	if(this_text.indexOf(val) === -1) 
		    	$(this).hide();
	        else 
		        $(this).show();                
	    });
	});
});
</script>