<?php
	$uitable=new Table(array('Nama','Alamat','Telepon'),"Master Perusahaan", NULL,true);
	$uitable->setName('masterperusahaan');
	if(isset($_POST['command'])){
			class myadapter extends ArrayAdapter{
				
				public function adapt($row){
					$array=array();
					$array['id']=$row->id;
					$array['Nama']=$row->nama;
					$array['Alamat']=$row->alamat;
					$array['Telepon']=$row->telpon;
				return $array;	
				}				
							
			}			
			
			$adapter=new myadapter();
			$column=array('id','nama','alamat','telpon');
			$dbtable=new DBTable($db,'smis_rg_perusahaan',$column);
			$dbresponder=new DBResponder($dbtable,$uitable,$adapter);
			
			$datapasien=$dbresponder->command($_POST['command']);
			echo json_encode($datapasien);
                        
			return;
	}		
	
	
	/*This is Modal Form and used for add and edit the table*/
	$mcolumn=array('id','nama','alamat','telpon');
	$mname=array("","Nama","Alamat","Telepon");
	$mtype=array("hidden","text","text","text");
	$value=array("","","","");
	$empty=array("","n","n","n","n","n");
	$typical=array("","","alpha","numeric","alpha","alpha");


	$uitable->setModal($mcolumn, $mtype, $mname,$value);
	$modal=$uitable->getModal();
	$modal->setTitle("Master Perusahaan");
	
	echo $uitable->getHtml();
	echo $modal->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
?>
<script type="text/javascript">
var masterperusahaan;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	$('.mydate').datepicker();
	var column=new Array('id','nama','alamat','telpon');
	masterperusahaan=new TableAction("masterperusahaan","registration","masterperusahaan",column);
	masterperusahaan.view();
});
</script>
