<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";
$master=new MasterTemplate($db,"smis_rg_propinsi","registration","provinsi");
$master->getUItable()->addModal("id","hidden","","");
$master->getUItable()->addModal("nama","text","Nama Propinsi","");
$master->getUItable()->addHeaderElement("No.");
$master->getUItable()->addHeaderElement("ID");
$master->getUItable()->addHeaderElement("Nama");
$master->getAdapter()->setUseNumber(true,"No.","back.");
$master->getAdapter()->add("ID","id");
$master->getAdapter()->add("Nama","nama");
$master->setModalTitle("Propinsi");
$master->initialize();
?>