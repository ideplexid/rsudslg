<?php 
	global $db;
	require_once 'smis-libs-class/MasterTemplate.php';
	$m=new MasterTemplate($db, "smis_rg_bahasa", "registration", "bahasa");
	$m->getDBtable()->setOrder("nama");
	$m->getUItable()->addModal("id", "hidden", "", "");
	$m->getUItable()->addModal("nama", "text", "Nama", "");
	$m->getUItable()->addModal("indoasing", "checkbox", "Asing", "");
	$m->getUItable()->addModal("keterangan", "textarea", "Keterangan", "");
	$m->getUItable()->setHeader(array("No.","Nama","Asal","Keterangan"));
	$m->getAdapter()->setUseNumber(true, "No.","back.");
	$m->getAdapter()->add("Asal", "indoasing","trivial_0_Indonesia_Asing");
	$m->getAdapter()->add("Nama", "nama");
	$m->getAdapter()->add("Keterangan", "keterangan");
	$m->initialize();
?>