<?php 
	global $db;
	require_once 'smis-libs-class/MasterTemplate.php';
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	
	$hari=new OptionBuilder();
	$hari->addSingle("Minggu");
	$hari->addSingle("Senin");
	$hari->addSingle("Selasa");
	$hari->addSingle("Rabu");
	$hari->addSingle("Kamis");
	$hari->addSingle("Jumat");
	$hari->addSingle("Sabtu");
	
    $kelompok=new OptionBuilder();
	$kelompok->add("","",1);
	$kelompok->add("Pagi","P");
	$kelompok->add("Siang","S");
	$kelompok->add("Malam","M");
	
	$m=new MasterTemplate($db, "smis_rg_jadwal", "registration", "jadwal_dokter");
	$m->getUItable()->addModal("id", "hidden", "", "");
	$m->getUItable()->addModal("kode", "text", "Kode", "");
	$m->getUItable()->addModal("hari", "select", "Hari", $hari->getContent());
	$m->getUItable()->addModal("bagdu", "checkbox", "Bagi Dua", "");
    $m->getUItable()->addModal("is_count", "checkbox", "Pasien Dihitung", "");
	$m->getUItable()->addModal("nama_dokter_satu", "chooser-jadwal_dokter-dokter_satu-Dokter I", "Nama Dokter I", "");
	$m->getUItable()->addModal("nama_dokter_dua", "chooser-jadwal_dokter-dokter_dua-Dokter II", "Nama Dokter II", "");
	$m->getUItable()->addModal("id_dokter_satu", "hidden", "", "");
	$m->getUItable()->addModal("id_dokter_dua", "hidden", "", "");
	$m->getUItable()->addModal("kelompok", "select", "Kelompok", $kelompok->getContent());
	$m->getUItable()->addModal("jam_mulai", "time", "Mulai", "");
	$m->getUItable()->addModal("jam_selesai", "time", "Selesai", "");
	$m->getUItable()->addModal("keterangan", "text", "Keterangan", "");
	$m->getUItable()->setHeader(array("No.","Hari","Kode","Dihitung","Bagi Dua","Dokter I","Dokter II","Kelompok","Mulai","Selesai","Keterangan"));
	
	$m->getAdapter()->setUseNumber(true, "No.","back.");
	$m->getAdapter()->add("Hari", "hari");
	$m->getAdapter()->add("Kode", "kode");
	$m->getAdapter()->add("Kelompok", "kelompok");
	$m->getAdapter()->add("Dokter I", "nama_dokter_satu");
	$m->getAdapter()->add("Dokter II", "nama_dokter_dua");
	$m->getAdapter()->add("Mulai", "jam_mulai");
	$m->getAdapter()->add("Selesai", "jam_selesai");
	$m->getAdapter()->add("Bagi Dua", "bagdu","trivial_1_Ya_Tidak");
    $m->getAdapter()->add("Dihitung", "is_count","trivial_1_Ya_Tidak");
    $m->getAdapter()->add("Keteragan", "keterangan");
    
	$m->setTimeEnable(true);
	$m->setModalTitle("Jadwal Dokter");	
	
	$dkadapter = new SimpleAdapter ();
	$dkadapter->add ( "Jabatan", "nama_jabatan" )
			  ->add ( "Nama", "nama" )
			  ->add ( "NIP", "nip" );
	$header=array ('Nama','Jabatan',"NIP" );
	
	$dktable = new Table ( $header);
	$dktable->setName ( "dokter_satu" )
			->setModel ( Table::$SELECT );
	$doktersatu = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );
	$m  ->getSuperCommand()
		->addResponder("dokter_satu", $doktersatu);
	
	$m	->addSuperCommand("dokter_satu", array())
		->addSuperCommandArray("dokter_satu", "id_dokter_satu", "id")
		->addSuperCommandArray("dokter_satu", "nama_dokter_satu", "nama");
	
	$dktable = new Table ( $header);
	$dktable->setName ( "dokter_dua" )
			->setModel ( Table::$SELECT );
	$dokterdua = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );
	$m	->getSuperCommand()
		->addResponder("dokter_dua", $dokterdua);
	$m	->addSuperCommand("dokter_dua", array())
		->addSuperCommandArray("dokter_dua", "id_dokter_dua", "id")
		->addSuperCommandArray("dokter_dua", "nama_dokter_dua", "nama");
	
	$m->initialize();
	
	
?>