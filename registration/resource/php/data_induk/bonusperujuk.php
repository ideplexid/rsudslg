<?php 
	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
	
	$perujuk=new MasterTemplate($db, "smis_rg_besarbonus", "registration", "bonusperujuk");
	$perujuk->setModalTitle("Bonus Perujuk");
	$uitable=$perujuk->getUItable();
	$header=array("Nama","Keterangan","Nilai");
	$uitable->setHeader($header);
	$uitable->addModal("id", "hidden", "", "")
			->addModal("nama", "text", "Nama", "")
			->addModal("keterangan", "textarea", "Keterangan", "")
			->addModal("nilai","money", "Nilai", "");
	$adapter=$perujuk->getAdapter();
	$adapter->add("Nama", "nama")
			->add("Keterangan", "keterangan")
			->add("Nilai","nilai","money Rp.");
	$perujuk->initialize();
?>