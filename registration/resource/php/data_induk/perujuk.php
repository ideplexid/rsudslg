<?php 
	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
	
	$perujuk=new MasterTemplate($db, "smis_rg_perujuk", "registration", "perujuk");
	$perujuk->setModalTitle("Perujuk");
	$uitable=$perujuk->getUItable();
	$header=array("Nama","Alamat","Instansi");
	$uitable->setHeader($header);
	$uitable->addModal("id", "hidden", "", "")
			->addModal("nama", "text", "Nama", "")
			->addModal("alamat", "text", "Alamat", "")
			->addModal("instansi","text", "Instansi", "");
	$adapter=$perujuk->getAdapter();
	$adapter->add("Nama", "nama")
			->add("Alamat", "alamat")
			->add("Instansi","instansi");
	$perujuk->initialize();
?>