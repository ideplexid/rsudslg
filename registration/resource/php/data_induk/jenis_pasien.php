<?php 
	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
	$option	= new OptionBuilder();
	$option ->add("Aktif","1")
			->add("Pasif","0");
	
	$perujuk = new MasterTemplate($db, "smis_rg_jenispasien", "registration", "jenis_pasien");
	$perujuk ->setModalTitle("Jenis Pasien");
	$uitable = $perujuk->getUItable();
	$header  = array("Nama","Slug","Keterangan","BPJS","Perusahaan","Asuransi","Plafon");
	$uitable ->setHeader($header);
	$uitable ->addModal("id", "hidden", "", "")
			 ->addModal("nama", "text", "Nama", "")
			 ->addModal("slug", "text", "Slug", "")
			 ->addModal("keterangan","textarea", "Keterangan", "")
			 ->addModal("nobpjs", "select", "BPJS", $option->getContent())
			 ->addModal("nama_perusahaan", "select", "Perusahaan", $option->getContent())
			 ->addModal("asuransi", "select", "Asuransi", $option->getContent())
             ->addModal("plafon","money", "Plafon", "");
	$adapter = $perujuk->getAdapter();
	$adapter ->add("Nama", "nama")
			 ->add("Slug", "slug")
			 ->add("BPJS", "nobpjs","trivial_1_X_-")
			 ->add("Perusahaan", "nama_perusahaan","trivial_1_X_-")
			 ->add("Asuransi", "asuransi","trivial_1_X_-")
             ->add("Plafon", "plafon","money Rp.")
			 ->add("Keterangan","keterangan");
	$perujuk ->initialize();
?>