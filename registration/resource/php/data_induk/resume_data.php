<?php 
/**
 * this page used to 
 * determine the result of system
 * to give an information to user
 * that there was several set 
 * of data that incomplete or damage
 * that can cause system will 
 * give wrong result of data 
 * 
 * @author : Nurul Huda
 * @since : 03 Mar 2017
 * @database : - smis_rg_patient
 *             - smis_rg_layananpatient
 * @license : LGPLv3
 * @copyright : goblooge@gmail.com
 * 
 * */

global $db;
$table=new TablePrint("");
$table->setDefaultBootrapClass(true);
$table->setMaxWidth(false);
$table->addColumn("INFORMASI DATABASE",1,3);
$table->commit("header");

/* validitas data pasien */
$dtable=new DBTable($db,"smis_rg_patient");
$total=$dtable->count("");
$dtable->addCustomKriteria("tgl_lahir","='0000-00-00'");
$tgl_lahir=$dtable->count("");
$dtable->setCustomKriteria(array());
$dtable->addCustomKriteria("alamat","=''");
$alamat=$dtable->count("");
$dtable->setCustomKriteria(array());
$dtable->addCustomKriteria("provinsi","='0'");
$provinsi=$dtable->count("");

$table  ->addColumn("<strong>NAMA DATABASE</strong>",1,1)
        ->addColumn("smis_rg_patient",2,1)
        ->commit("body");
$table  ->addColumn("Total Data",1,1)
        ->addColumn($total,2,1)
        ->commit("body");
$table  ->addColumn("Tanggal Lahir Kosong",1,1)
        ->addColumn($tgl_lahir,1,1)
        ->addColumn(round($tgl_lahir*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("",1,1)
        ->addColumn(" * Tanggal Lahir yang kosong akan mengakibatkan pengelompokan data berdasarkan umur menjadi tidak valid di rekam medis",2,1)
        ->commit("body");
$table  ->addColumn("Alamat Kosong",1,1)
        ->addColumn($alamat,1,1)
        ->addColumn(round($alamat*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("",1,1)
        ->addColumn(" * Alamat yang Kosong akan Mengganggu Kelengkapan data pasien di Pelayanan",2,1)
        ->commit("body");
$table  ->addColumn("Provinsi Kosong",1,1)
        ->addColumn($provinsi,1,1)
        ->addColumn(round($provinsi*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("",1,1)
        ->addColumn(" * data Provinsi, Kabupaten, Kecamatan dan Desa yang tidak lengkap akan mengakibatkan pengelompokan data berdasarkan wilayah menjadi tidak valid",2,1)
        ->commit("body");
$hitung=100-((($tgl_lahir+$alamat+$provinsi)*100)/(3*$total) );
$table  ->addColumn("Tingkat Kelengkapan Data Pasien",1,1)
        ->addColumn(round($hitung,2)." % ",1,1)
        ->addColumn("<i>untuk melengkapi data silakan dilengkapi pada menu Pendaftaran -> Rawat Jalan dan IGD -> Tab Registrasi Pasien </i>",1,1)
        ->commit("body");


/* validitas data layanan pasien */
$dtable=new DBTable($db,"smis_rg_layananpasien");
$total=$dtable->count("");
$dtable->addCustomKriteria("gol_umur","='TGL LAHIR TDK VALID'");
$umur=$dtable->count("");
$dtable->setCustomKriteria(array());
$dtable->addCustomKriteria("carabayar","=''");
$carabayar=$dtable->count("");
$dtable->setCustomKriteria(array());
$dtable->addCustomKriteria("selesai","='1'");
$dtable->addCustomKriteria("carapulang","=''");
$carapulang=$dtable->count("");
$dtable->setCustomKriteria(array());
$dtable->addCustomKriteria("uri","='1'");
$dtable->addCustomKriteria("nama_dokter","=''");
$dpjp=$dtable->count("");
$dtable->setCustomKriteria(array());
$dtable->addCustomKriteria("uri","='1'");
$dtable->addCustomKriteria("kamar_inap","=''");
$inap=$dtable->count("");
$dtable->setCustomKriteria(array());
$dtable->addCustomKriteria("caradatang","=''");
$caradata=$dtable->count("");

$table  ->addColumn("<strong>NAMA DATABASE</strong>",1,1)
        ->addColumn("smis_rg_layananpasien",2,1)
        ->commit("body");
$table  ->addColumn("Total Data",1,1)
        ->addColumn($total,2,1)
        ->commit("body");
$table  ->addColumn("Tanggal Lahir Tidak Valid",1,1)
        ->addColumn($umur,1,1)
        ->addColumn(round($umur*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("Carabayar Kosong",1,1)
        ->addColumn($carabayar,1,1)
        ->addColumn(round($carabayar*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("Caradatang Kosong",1,1)
        ->addColumn($caradata,1,1)
        ->addColumn(round($caradata*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("Cara Pulang Kosong",1,1)
        ->addColumn($carapulang,1,1)
        ->addColumn(round($carapulang*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("Dokter DPJP Kosong",1,1)
        ->addColumn($dpjp,1,1)
        ->addColumn(round($dpjp*100/$total,2)."%",1,1)
        ->commit("body");
$table  ->addColumn("Data Inap Kosong",1,1)
        ->addColumn($inap,1,1)
        ->addColumn(round($inap*100/$total,2)."%",1,1)
        ->commit("body");
$hitung=100-((($umur+$carabayar+$caradata+$carapulang+$dpjp+$inap)*100)/(6*$total) );
$table  ->addColumn("Tingkat Kelengkapan Data Pasien",1,1)
        ->addColumn(round($hitung,2)." % ",2,1)
        ->commit("body");
$table  ->addColumn("ketidaklengkapan data akan mengakibatkan ketidakvalidan data rekam medis. tanggal lahir yang kosong akan mengakibatkan pengelompokan data pasien berdasarkan umur menjadi tidak valid. alamat yang kurang lengkap akan menggangu data induk pasien, dan pengisian data provinsi, kabupaten, kecamatan dan desa yang tidak lengkap akan mengakibatkan pengelompokandata pasien berdasarkan lokasi menjadi tidak lengkap. Untuk Melengkapi Data Induk silakan buka Menu Pendaftaran -> Rawat Jalan dan IGD pada Tab Registrasi Pasien ",3,1)
        ->commit("body"); 

echo $table->getHtml();
?>