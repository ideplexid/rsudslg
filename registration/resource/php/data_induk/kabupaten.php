<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";

$master=new MasterTemplate($db,"smis_rg_kabupaten","registration","kabupaten");
$master->getUItable()->addHeaderElement("No.");
$master->getUItable()->addHeaderElement("ID");
$master->getUItable()->addHeaderElement("Nama");
$master->getUItable()->addHeaderElement("Propinsi");
$master->getAdapter()->setUseNumber(true,"No.","back.");
$master->getAdapter()->add("ID","id");
$master->getAdapter()->add("Nama","nama");
$master->getAdapter()->add("Propinsi","propinsi");
$master->setModalTitle("Kabupaten");
if($master->getDBResponder()->isPreload()){	
	$dbtable=new DBTable($db,"smis_rg_propinsi");
	$dbtable->setShowAll(true);
	$dbtable->setOrder(" nama ASC ",true);
	$list=$dbtable->view("","0");
	$adapter=new SelectAdapter("nama","id");	
	$list_prop=$adapter->getContent($list['data']);
	$master->getUItable()->addModal("id","hidden","","");
	$master->getUItable()->addModal("nama","text","Nama Kabupaten","");
	$master->getUItable()->addModal("no_prop","select","Propinsi",$list_prop);
}
if($master->getDBResponder()->isView()){	
	$dbtable=new DBTable($db,"smis_rg_propinsi");
	$qv="SELECT smis_rg_kabupaten.*, smis_rg_propinsi.nama as propinsi 
		 FROM smis_rg_kabupaten 
		 LEFT JOIN smis_rg_propinsi ON smis_rg_kabupaten.no_prop=smis_rg_propinsi.id ";
	$qc="SELECT count(*) as total
		 FROM smis_rg_kabupaten ";
	$master->getDBtable()->setPreferredQuery(true,$qv,$qc);
	$master->getDBtable()->setUseWhereForView(true);
}
$master->initialize();
?>