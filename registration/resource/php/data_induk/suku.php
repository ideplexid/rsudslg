<?php 
	global $db;
	require_once 'smis-libs-class/MasterTemplate.php';
	$m=new MasterTemplate($db, "smis_rg_suku", "registration", "suku");
	$m->getDBtable()->setOrder("nama");
	$m->getUItable()->addModal("id", "hidden", "", "");
	$m->getUItable()->addModal("nama", "text", "Nama", "");
	$m->getUItable()->addModal("propinsi", "text", "Propinsi", "");
	$m->getUItable()->setHeader(array("No.","Nama","Propinsi"));
	$m->getAdapter()->setUseNumber(true, "No.","back.");
	$m->getAdapter()->add("Propinsi", "propinsi");
	$m->getAdapter()->add("Nama", "nama");
	$m->getAdapter()->add("ID", "id","digit8");
	$m->initialize();
?>