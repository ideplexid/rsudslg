<?php 
global $db;
require_once "smis-libs-class/MasterTemplate.php";

$master=new MasterTemplate($db,"smis_rg_kelurahan","registration","kelurahan");
$master->getUItable()->addHeaderElement("No.");
$master->getUItable()->addHeaderElement("ID");
$master->getUItable()->addHeaderElement("Nama");
$master->getUItable()->addHeaderElement("Kecamatan");
$master->getUItable()->addHeaderElement("Kabupaten");
$master->getUItable()->addHeaderElement("Propinsi");
$master->getAdapter()->setUseNumber(true,"No.","back.");
$master->getAdapter()->add("ID","id");
$master->getAdapter()->add("Nama","nama");
$master->getAdapter()->add("Kabupaten","kabupaten");
$master->getAdapter()->add("Propinsi","propinsi");
$master->getAdapter()->add("Kecamatan","kecamatan");
$master->setModalTitle("Kelurahan");
if($master->getDBResponder()->isPreload()){	
	$dbtable=new DBTable($db,"smis_rg_propinsi");
	$dbtable->setShowAll(true);
	$dbtable->setOrder(" nama ASC ",true);
	$list=$dbtable->view("","0");
	$adapter=new SelectAdapter("nama","id");	
	$list_prop=$adapter->getContent($list['data']);
	$master->getUItable()->addModal("id","hidden","","");
	$master->getUItable()->addModal("no_prop","select","Propinsi",$list_prop);
	$master->getUItable()->addModal("no_kab","select","Kabupaten",array());
	$master->getUItable()->addModal("no_kec","select","Kecamatan",array());
	$master->getUItable()->addModal("nama","text","Nama Kelurahan","");
}
if($master->getDBResponder()->isView()){	
	$dbtable=new DBTable($db,"smis_rg_kelurahan");
	$qv="SELECT smis_rg_kelurahan.*, 
				smis_rg_kec.nama as kecamatan, 
				smis_rg_kabupaten.nama as kabupaten, 
				smis_rg_propinsi.nama as propinsi 
		 FROM smis_rg_kelurahan 
		 LEFT JOIN smis_rg_propinsi ON smis_rg_kelurahan.no_prop=smis_rg_propinsi.id
		 LEFT JOIN smis_rg_kabupaten ON smis_rg_kelurahan.no_kab=smis_rg_kabupaten.id 
		 LEFT JOIN smis_rg_kec ON smis_rg_kelurahan.no_kec=smis_rg_kec.id ";
	$qc="SELECT count(*) as total
		 FROM smis_rg_kelurahan ";
	
	
	$master->getDBtable()->setPreferredQuery(true,$qv,$qc);
	$master->getDBtable()->setUseWhereForView(true);
}
$master->addResouce("js","registration/resource/js/kelurahan.js");
$master->initialize();
?>