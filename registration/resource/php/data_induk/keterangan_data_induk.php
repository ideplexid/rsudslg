<!-- 
@author 	: Ichwan Qodrian
@copyright	: ichwan.work@gmail.com
@used		: registration/modul/data_induk.php
@since		: 6 Sept 2016
@license	: LGPLv2
@version	: 1.0.0
-->

<h4>Deskripsi Data Induk</h4>
<p>
Data Induk berisi tentang data - data penting yang difungsikan sebagai pelengkap biodata pasien baik secara internal maupun eksternal Rumah Sakit,
berikut ini merupakan bagian dari Data Induk yang terdapat pada Pendaftaran Rawat Jalan/IGD dan Rawat Inap :
</p>

<ul>
	<li><strong>Bahasa</strong><p>  dipakai pada Pendaftaran -> Rawat Jalan dan IGD pada Pendataan Pasien. Digunakan untuk menentukan cara berkomunikasi antar pasien dengan Perawat maupun Dokter</p></li>
	<li><strong>Suku </strong><p> dipakai pada Pendaftaran -> Rawat Jalan dan IGD pada Pendataan Pasien. Digunakan sebagai filter pada pencarian database pasien</p></li>
	<li><strong>Perusahaan</strong><p> dipakai pada Pendaftaran -> Rawat Jalan dan IGD pada Pendaftaran Pasien. Digunakan untuk pasien perusahaan yang bekerjasama dengan Rumah Sakit</p></li>
	<li><strong>Asuransi</strong><p> dipakai pada Pendaftaran -> Rawat Jalan dan IGD pada Pendaftaran Pasien. Digunakan untuk pasien yang menggunakan asuransi</p></li>
	<li><strong>Perujuk</strong><p> dipakai pada Pendaftaran -> Rawat Jalan dan IGD pada Pendaftaran Pasien. Digunakan untuk instansi yang merujuk pasien ke Rumah Sakit</p></li>
	<li><strong>Bonus Perujuk</strong><p> dipakai pada Pendaftaran -> Rawat Jalan dan IGD pada Pendaftaran Pasien. Digunakan sebagai bonus pada instansi yang merujuk pasien ke Rumah Sakit</p></li>
	<li><strong>Jenis Pasien / Pembayaran Pasien</strong><p> dipakai pada Pendaftaran -> Rawat Jalan dan IGD pada Pendaftaran Pasien. Digunakan untuk metode pembayaran pada pasien</p></li>
</ul>