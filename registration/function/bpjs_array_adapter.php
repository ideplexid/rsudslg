<?php 

function bpjs_array_adapter($data){
    $res=array();
    $res['Nama']=$data['response']['peserta']['nama'];
    $res['NIK/No.KTP']=$data['response']['peserta']['nik'];
    $res['No BPJS']=$data['response']['peserta']['noKartu'];
    $res['No Mr']=$data['response']['peserta']['noMr'];
    $res['L/P']=$data['response']['peserta']['sex'];
    $res['Kelas']=$data['response']['peserta']['kelasTanggungan']['nmKelas'];
    $res['Umur']=$data['response']['peserta']['umur']['umurSekarang'];
    $res['Jenis Peserta']=$data['response']['peserta']['jenisPeserta']['nmJenisPeserta'];
    $res['Jenis Pelayanan']=$data['response']['jnsPelayanan'];
    $res['PISA']=$data['response']['peserta']['pisa'];            
    $res['Diagnosa Awal']=$data['response']['diagAwal']['kdDiag']." - ".$data['response']['diagAwal']['nmDiag'];
    $res['Informasi Dinsos']=$data['response']['peserta']['informasi']['dinsos'];
    $res['Informasi Iuran']=$data['response']['peserta']['informasi']['iuran'];
    $res['Informasi SKTM']=$data['response']['peserta']['informasi']['noSKTM'];
    $res['Informasi PRB']=$data['response']['peserta']['informasi']['prolanisPRB'];
    $res['Tanggal Lahir']=$data['response']['peserta']['tglLahir'];
    $res['Tanggal Daftar']=$data['response']['peserta']['tglTMT'];
    $res['Tanggal TAT']=$data['response']['peserta']['tglTAT'];
    $res['Tanggal Cetak Kartu']=$data['response']['peserta']['tglCetakKartu'];      
    
    $res['Provider Umum']=$data['response']['peserta']['provUmum']['nmProvider']." - ".$data['response']['peserta']['provUmum']['kdProvider'];
    $res['Provider Rujukan']=$data['response']['provRujukan']['nmProvider']." - ".$data['response']['provRujukan']['kdProvider'];
    $res['Provider Pelayanan']=$data['response']['provPelayanan']['nmProvider']." - ".$data['response']['provPelayanan']['kdProvider'];
    $res['Poli Tujuan']=$data['response']['poliTujuan']['nmPoli']." - ".$data['response']['poliTujuan']['kdPoli'];
    $res['Status SEP']=$data['response']['statSep']['nmStatSep']." - ".$data['response']['statSep']['kdStatSep'];
    $res['Status COB']=$data['response']['statusCOB']['namaCOB']." - ".$data['response']['statusCOB']['kodeCOB'];
    
    $res['Tanggal Pulang']=$data['response']['tglPulang'];
    $res['Tanggal Rujukan']=$data['response']['tglRujukan'];
    $res['Tanggal SEP']=$data['response']['tglSep'];
    $res['Catatan']=$data['response']['catatan'];
    $res['Biaya']=ArrayAdapter::format("money Rp.",$data['response']['byTagihan']);
    
    return $res;
}

function bpjs_array_adapter_vclaim($data,$data_sep){
    $res=array();
    $res['Nama']             = $data['peserta']['nama'];
    $res['No BPJS']          = $data['peserta']['noKartu'];
    $res['No Mr']            = $data['peserta']['noMr'];
    $res['L/P']              = $data['peserta']['kelamin']=="L"?"Laki-Laki":"Perempuan";
    $res['Kelas']            = $data['peserta']['hakKelas'];
    $res['Jenis Peserta']    = $data['peserta']['jnsPeserta'];
    $res['Tanggal Lahir']    = $data['peserta']['tglLahir'];
    $res['Jenis Pelayanan']  = $data['jnsPelayanan'];
    $res['Diagnosa Awal']    = $data['diagnosa'];    
    $res['Poli Tujuan']      = $data['poli'];
    $res['Tanggal SEP']      = $data['tglSep'];
    $res['Catatan']          = $data['catatan'];
    $res['Status COB']       = $data['cob']=="1"?"Ya":"Tidak";
    $res['Provider Rujukan'] = $data['asalRujukan'];
    $res['No. Telp']         = $data_sep['noTelp'];    
    $res['Dokter']           = $data['dpjp']['nmDPJP'];    
    $res['KelasRawat']       = $data['klsRawat']['klsRawatHak'];    
    return $res;
}

?>