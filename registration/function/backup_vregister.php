<?php 
/**
 * @brief this function used to backup view smis_vregister to smis_vregisterb table
 *         the purpose is fr synchronize data, where data need to synchronize accross
 *         multiple autonomous but the nrm not synchronize and not unique
 * @param Database $db 
 * @param string $noreg 
 * @return  boolean success
 */
function backup_vregister(Database $db,$noreg){
    $is_backup = getSettings($db,"smis-reg-duplicate-vregister","0")=="1";
    if($is_backup){
        $dbtable = new DBTable($db,"smis_rg_vregister");
        $dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
        $x = $dbtable->select($noreg);
        $dbtable->setName("smis_rg_vregisterb");
        return $dbtable-> insertOrUpdate((array)$x,array("id"=>$noreg));
    }
    return false; 
}

?>