<?php 

/**
 * @brief get the schedule of doctor based on current data
 * @param DBTable $dbtable 
 * @param string $hari 
 * @param string $shift 
 * @return  return Object data
 */
function get_jadwal(DBTable $dbtable,$hari,$shift){
    $kriteria['is_count']=1;
    $kriteria['hari']=$hari;
    $kriteria['kelompok']=$shift;
    return $dbtable->select($kriteria);
}

/**
 * @brief get the data of total patient in the given range
 * @param Database $db 
 * @param date $tanggal 
 * @param string $waktu_dari 
 * @param string $waktu_sampai 
 * @return  Array 2 Dimension of the data
 */
function get_total_data($db,$tanggal,$waktu_dari,$waktu_sampai){
    $query="SELECT carabayar, count(*) as jumlah
            FROM smis_rg_layananpasien
            WHERE 
            tanggal >= '".$tanggal." ".$waktu_dari."'
            AND tanggal < '".$tanggal." ".$waktu_sampai."'
            AND prop!='del'
    ";
    return $db->get_result($query);
}

/**
 * @brief search replacement schedule of patient
 * @param \DBtable $dbtable 
 * @param string $tanggal 
 * @param string $shift 
 * @return  Object of the replacement
 */
function getJadwalGanti(DBtable $dbtable,$tanggal,$shift){
     $kriteria["tanggal"]=$tanggal;
     $kriteria["shift"]=$shift;
     $x=$dbtable->select($kriteria);
     return $x;
}

/**
 * @brief to prceed searching and inserting data in database
 *          of counting the patient data
 * @param Database $db 
 * @param DBTable $dbtable_rekap 
 * @param DBTable $dbtable_jadwal 
 * @param DBTable $dbtable_ganti 
 * @param string $dayname 
 * @param string $tanggal 
 * @param string $shift 
 * @return  null
 */
function proceedJadwal(Database &$db,DBTable &$dbtable_rekap,DBTable &$dbtable_jadwal,DBTable &$dbtable_ganti,$dayname, $tanggal,$shift){
    $one_shift=get_jadwal($dbtable_jadwal,$dayname,$shift);
    if($one_shift!=null){
        $id_dokter=$one_shift->id_dokter_satu;
        $data=get_total_data($db,$tanggal,$one_shift->jam_mulai,$one_shift->jam_selesai);
        if(count($data>0)){
            $ganti=getJadwalGanti($dbtable_ganti,$tanggal,$shift);
            if($ganti!=null){
                $id_dokter=$ganti->id_dokter;
            }
            foreach($data as $x){
                if($x->jumlah*1 <= 0)
                    continue;
                
                $one['tanggal']=$tanggal;
                $one['kelompok']=$shift;
                $one['carabayar']=$x->carabayar;
                $one['jumlah']=$x->jumlah;
                $one['id_dokter']=$id_dokter;
                $one['hari']=$dayname;
                
                $up['tanggal']=$tanggal;
                $up['kelompok']=$shift;
                $up['carabayar']=$x->carabayar;
                
                $dbtable_rekap->insertOrUpdate($one,$up);
            }
        }
    }
}

?>