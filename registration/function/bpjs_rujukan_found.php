<?php 

function bpjs_rujukan_found($response){
    $res                    = array();
    $res['message']         = "Data Ditemukan";
    $res['kode_diagnosa']   = $response["item"]["diagnosa"]["kdDiag"];
    $res['nama_diagnosa']   = $response["item"]["diagnosa"]["nmDiag"];
    $res['keluhan']         = $response["keluhan"];
    $res['nik']             = $response["item"]['peserta']["nik"];
    $res['nobpjs']          = $response["item"]['peserta']["noKartu"];
    $res['nama_provider']   = $response["item"]['provKunjungan']["nmProvider"];
    $res['kd_provider']     = $response["item"]['provKunjungan']["kdProvider"];
    $res['kd_poli']         = $response["item"]["poliRujukan"]["kdPoli"];
    $res['nama_poli']       = $response["item"]["poliRujukan"]["nmPoli"];
    $res['tgl_kunjungan']   = $response["item"]["tglKunjungan"];
    $res['no_kunjungan']    = $response["item"]["noKunjungan"];
    $res['catatan']         = $response["item"]["catatan"];
    return $res;
}

function bpjs_rujukan_found_vclaim($response,$key,$asal=""){
    $res=array();
    $res['message']="Data Ditemukan";

    require_once "registration/function/vclaim_decrypt.php";
    $rep = array();
    if(is_string($response)){
        $hasilcompress = stringDecrypt($key, $response);
        $hasil = decompress($hasilcompress);
        $rep = json_decode($hasil,true);
    }else{
        $rep = $data['response'];
    }


    global $querylog;
    $querylog->addMessage(json_encode($rep));

    $result                    = array();
    $result['message']         = "Data Ditemukan";
    $result['kode_diagnosa']   = $rep["rujukan"]["diagnosa"]["kode"];
    $result['nama_diagnosa']   = $rep["rujukan"]["diagnosa"]["nama"];
    $result['keluhan']         = $rep['rujukan']["keluhan"];
    $result['nik']             = $rep["rujukan"]['peserta']["nik"];
    $result['nobpjs']          = $rep["rujukan"]['peserta']["noKartu"];
    $result['nama_provider']   = $rep["rujukan"]['provPerujuk']["nama"];
    $result['kd_provider']     = $rep["rujukan"]['provPerujuk']["kode"];
    $result['kd_poli']         = $rep["rujukan"]['poliRujukan']['kode'];
    $result['nama_poli']       = $rep["rujukan"]['poliRujukan']['nama'];
    $result['tgl_kunjungan']   = $rep["rujukan"]["tglKunjungan"];
    $result['no_kunjungan']    = $rep["rujukan"]["noKunjungan"];
    $result['catatan']         = $rep["rujukan"]["catatan"];
    $result['kelas']           = $rep["rujukan"]['peserta']["hakKelas"]['kode'];
    $result['asal_rujukan']    = $asal==""?"1":"2";

    //echo json_encode($result);
    return $result;
}

?>