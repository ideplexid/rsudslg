<?php
 
/**
 * @brief this function used to 
 * get the current doctor, on given time
 * @param Database $db 
 * @param String $tanggal the time that given (2016-02-01 12:12:00) 
 * @author Nurul Huda
 * @copyright goblooge@gmail.com
 * @since 18 Jan 2017
 * @version 1.0.0
 * @return  the doctor code;
 */

function getJadwalDokter(Database $db,$tanggal){
    $jdw=getRowJadwalDokter($db,$tanggal);
    return $jdw['kode'];
}

/**
 * @brief this used to get all setup
 *          of each code, in given time
 * @param Database $db 
 * @param String $tanggal 
 * @return  Array contains the detail for each Sceduled Code
 */
function getRowJadwalDokter(Database $db,$tanggal){
    loadLibrary("smis-libs-function-time");
    $jam=substr($tanggal,11,5);
	
	$hari=date("N",strtotime($tanggal));
	$nama_hari=dayName($hari);
	$dbtable=new DBTable($db,"smis_rg_jadwal");
    $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$dbtable->addCustomKriteria("hari","='".$nama_hari."'");
	$dbtable->addCustomKriteria("jam_mulai","<='".$jam."'");
	$dbtable->setOrder("jam_mulai DESC ");
	$dbtable->setMaximum(1);
	
	$jadwal=$dbtable->view("","0");
	$fjadwal=$jadwal['data'];
    if(!isset($fjadwal[0])){
        $a=array();
        $a['id_dokter_satu']="-1";
        $a['nama_dokter_satu']="N/A";
        $a['id_dokter_dua']="-1";
        $a['nama_dokter_dua']="N/A";
        $a['id_dokter_satu']="-1";
        $a['kode']="N/A";
        $a['bagdu']=0;
        return $a;
    }
    return $fjadwal[0];
}

?>