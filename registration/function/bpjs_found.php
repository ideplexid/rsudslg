<?php 

function bpjs_found($data,$key){
    $res=array();
    $res['message']="Data Ditemukan";

    require_once "registration/function/vclaim_decrypt.php";
    $rep = array();
    if(is_string($data['response'])){
        $hasilcompress = stringDecrypt($key, $data['response']);
        $hasil = decompress($hasilcompress);
        $rep = json_decode($hasil,true);
    }else{
        $rep = $data['response'];
    }

    global $querylog;
    $querylog->addMessage("Hasil Asli BPJS");
    $querylog->addMessage(json_encode($rep));

    $res['info_dinson']      = $rep['peserta']['informasi']['dinsos'];
    $res['info_iuran']       = $rep['peserta']['informasi']['iuran'];
    $res['info_sktm']        = $rep['peserta']['informasi']['noSKTM'];
    $res['info_prb']         = $rep['peserta']['informasi']['prolanisPRB'];
    $res['nama']             = $rep['peserta']['nama'];
    $res['nik']              = $rep['peserta']['nik'];
    $res['nobpjs']           = $rep['peserta']['noKartu'];
    $res['jk']               = $rep['peserta']['sex'];
    $res['tgl_lahir']        = $rep['peserta']['tglLahir'];
    $res['tgl_daftar']       = $rep['peserta']['tglTMT'];
    $res['tgl_tat']          = $rep['peserta']['tglTAT'];
    $res['tgl_ckartu']       = $rep['peserta']['tglCetakKartu'];            
    $res['umur']             = $rep['peserta']['umur']['umurSekarang'];
    $res['status']           = $rep['peserta']['statusPeserta']['keterangan'];
    $res['kd_status']        = $rep['peserta']['statusPeserta']['kode'];
    $res['jenis_peserta']    = $rep['peserta']['jenisPeserta']['nmJenisPeserta'];
    $res['kd_jenis_peserta'] = $rep['peserta']['jenisPeserta']['kdJenisPeserta'];
    $res['kelas']            = $rep['peserta']['hakKelas']['keterangan'];
    $res['kd_kelas']         = $rep['peserta']['hakKelas']['kode'];
    $res['pisa']             = $rep['peserta']['pisa'];
    
    $res['nama_cabang']      = $rep['peserta']['provUmum']['nmProvider'];
    $res['kd_cabang']        = $rep['peserta']['provUmum']['kdProvider'];

    $res['nama_provider']    = $rep['peserta']['provPerujuk']['nmProvider'];
    $res['kd_provider']      = $rep['peserta']['provPerujuk']['kdProvider'];

    $res['dinsos']      = $rep['peserta']['informasi']['dinsos'];
    $res['sktm']      = $rep['peserta']['informasi']['noSKTM'];
    $res['prb']      = $rep['peserta']['informasi']['prolanisPRB'];

    $nik = $res['nik'];
    $bpjs = $res['nobpjs'];
    
    $query = "SELECT id,telpon FROM smis_rg_patient WHERE nobpjs='".$bpjs."' OR ktp='".$nik."' ";
    global $db;
    $pasien = $db->get_row($query);
    $res['status_nrm']  = "Belum Ada";
    if($pasien!=null){
        $res['nrm_pasien']  = $pasien->id;
        $res['id_pasien']   = $pasien->id;
        $res['telpon']      = $pasien->telpon;
        $res['status_nrm']  = "Sudah Ada";    
    }

    if($res['status']!="AKTIF"){
        $res['message'].=". <strong>Peringatan</strong>, Status Kartu ini <strong>tidak</strong> aktif, tidak dapat memakai BPJS !!!";
    }else{
        $res['message'].=". Pasien ini bisa menggunakan BPJS selama Hari ini tidak ada SEP Aktif";
    }
    return $res;
}

?>