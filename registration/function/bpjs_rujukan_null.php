<?php 

function bpjs_rujukan_null($msg){
    $res                    = array();
    $res['message']         = $msg;
    $res['kode_diagnosa']   = "";
    $res['nama_diagnosa']   = "";
    $res['keluhan']         = "";
    $res['catatan']         = "";
    $res['nik']             = "";
    $res['nobpjs']          = "";
    $res['nama_provider']   = "";
    $res['kd_provider']     = "";
    $res['nama_provider']     = "";
    $res['kd_poli']         = "";
    $res['nama_poli']       = "";
    $res['tgl_kunjungan']   = "";
    $res['kelas']           = "";
    return $res;
}

?>