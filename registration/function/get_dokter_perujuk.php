<?php 

/**
 * @brief this function used to 
 *          get the 'dokter perujuk' of given patient in given time
 * @param Database $db 
 * @param String $nrm the Number Medical Record 
 * @param String $id_dokter id current dokter operator 
 * @param String $nama_dokter name current dokter operator 
 * @param String $tanggal the date of current surgery , format yyyy-mm-dd hh:mm:ss
 * @return  array contains 'dokter perujuk'
 */
function getDokterPerujuk(Database $db,$nrm,$id_dokter,$nama_dokter,$tanggal){
    $kode=getKodeDokterPerujuk($db,$nrm,$tanggal);
    $one_jd=null;
    if($kode!="N/A"){
        $one_jd=getDetailJadwal($db,$kode);
    }
    $id_dokter_perujuk="0";
    $keterangan="";
    $nama_dokter_perujuk="";
    if($one_jd==null){
        $keterangan="Kode Perujuk Pasien Ini Belum di Set, Silakan Hubungi Bagian Pendaftaran ";
    }else if($one_jd['bagdu']*1==1){
        if($id_dokter==$one_jd['id_dokter_satu']){
            /*kondisi jika dokter satu kebetulan yang meng-operasi maka dokter dua yang jadi perujuk*/
            $keterangan=" [".$kode."] Kondisi Bagi 2, Karena Dokter Operator adalah ".$nama_dokter." yang Merupakan Dokter Satu maka Dokter Perujuk Adalah Dokter Dua yaitu ".$one_jd['nama_dokter_dua'];
            $id_dokter_perujuk=$one_jd['id_dokter_dua'];
            $nama_dokter_perujuk=$one_jd['nama_dokter_dua'];
        }else if($id_dokter==$one_jd['id_dokter_dua']){
            /*kondisi jika dokter dua kebetulan yang meng-operasi maka dokter satu yang jadi perujuk*/
            $keterangan=" [".$kode."] Kondisi Bagi 2, Karena Dokter Operator adalah ".$nama_dokter." yang Merupakan Dokter Dua maka Dokter Perujuk Adalah Dokter Satu yaitu ".$one_jd['nama_dokter_satu'];
            $id_dokter_perujuk=$one_jd['id_dokter_satu'];
            $nama_dokter_perujuk=$one_jd['nama_dokter_satu'];
        }else{
            /*kondisi dilarang, dokter satu dan dokter dua bukanah operator */
            $keterangan=" [".$kode."] Kondisi Bagi 2, Mohon di Cek Ulang Kondisi Seperti ini Tidak Diperbolehkan Karena 3 Dokter. Dokter Operator adalah ".$nama_dokter." Sedangkan Dokter Satu pada Kode [".$kode."] adalah ".$one_jd['nama_dokter_satu']." dan Dokter dua pada Kode [".$kode."] adalah ".$one_jd['nama_dokter_dua'];
            $id_dokter_perujuk="0";
            $nama_dokter_perujuk="KONDISI DILARANG";
        }
    }else{
        /*kondisi dokter bukan bagi dua, baik dokter maupun perujuk boleh satu orang yang sama*/
        $keterangan=" [".$kode."] Bukan Bagi Dua, Karena Dokter Operator adalah ".$nama_dokter." maka Dokter Perujuk Mengikuti Kode  [".$kode."]  yaitu ".$one_jd['nama_dokter_satu'];
        $id_dokter_perujuk=$one_jd['id_dokter_satu'];
        $nama_dokter_perujuk=$one_jd['nama_dokter_satu'];
    }

    $hasil=array();
    $hasil['id_dokter']=$id_dokter_perujuk;
    $hasil['nama_dokter']=$nama_dokter_perujuk;
    $hasil['keterangan']=$keterangan;
    $hasil['kode_perujuk']=$kode;
    return $hasil;
}

function getDetailJadwal(Database $db, $kode){   
    $jadwal=new DBTable($db,"smis_rg_jadwal",array("kode"));
    $jadwal->setFetchMethode(DBTable::$ARRAY_FETCH);
    $jadwal->addCustomKriteria(" kode "," ='".$kode."'");
    $jadwal->setMaximum(1);
    $jadwal->setDebuggable(true);
    $q=$jadwal->getQueryView("","0");
    $jd=$jadwal->view("",0);
    $djd=$jd['data'];
    $one_jd=isset($djd[0])?$djd[0]:null;
    return $one_jd;
}

/**
 * @brief get the current perujuk code
 * @param Database $db 
 * @param String $nrm of the patient 
 * @param String $tanggal of patient 
 * @return kode of patient
 */
function getKodeDokterPerujuk(Database $db,$nrm,$tanggal){
    $dbtable=new DBTable($db,"smis_rg_dokter_utama");
    $dbtable->addCustomKriteria("nrm_pasien","='".$nrm."'");
    $dbtable->addCustomKriteria("selesai",">='".$tanggal."'");
    $dbtable->addCustomKriteria(null," kode != '' ");
    $dbtable->addCustomKriteria(null," kode != 'N/A' ");
    $dbtable->setShowAll(true);    
    $dbtable->setMaximum(1);
    $dbtable->setOrder(" selesai ASC ");
    $du=$dbtable->view("","0");
    $ddu=$du['data'];
    if(!isset($ddu[0]))
        return "N/A";
    $one_du=$ddu[0];
    $kode=$one_du->kode;
    return $kode;
}

?>