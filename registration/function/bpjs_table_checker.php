<?php 

function bpjs_table_checker($res){
    $tp=new TablePrint("");
    $tp->setMaxWidth(false);
    $tp->setDefaultBootrapClass(true);
    $tp->addColumn($res['message'],10,1);
    $tp->commit("header");
    //$tp->addColumn("Kode Status",1,1)->addColumn($res['kd_status'],1,1,"body");
    $tp->addColumn("Nama",1,1)->addColumn($res['nama'],1,1,"body");
    $tp->addColumn("NIK",1,1)->addColumn($res['nik'],1,1,"body");
    $tp->addColumn("No. BPJS",1,1)->addColumn($res['nobpjs'],1,1,"body");
    $tp->addColumn("Jenis Kelamin",1,1)->addColumn($res['jk'],1,1,"body");
    //    $tp->addColumn("Tgl Lahir",1,1)->addColumn(ArrayAdapter::format("date d M Y",$res['tgl_lahir']),1,1,"body");
    //    $tp->addColumn("Tgl Daftar",1,1)->addColumn(ArrayAdapter::format("date d M Y",$res['tgl_daftar']),1,1,"body");
    //    $tp->addColumn("Tgl TAT",1,1)->addColumn(ArrayAdapter::format("date d M Y",$res['tgl_tat']),1,1,"body");
    //    $tp->addColumn("Tgl Cetak Kartu",1,1)->addColumn(ArrayAdapter::format("date d M Y",$res['tgl_ckartu']),1,1,"body");
    //    $tp->addColumn("Umur",1,1)->addColumn($res['umur'],1,1,"body");
    //    $tp->addColumn("Informasi Dinsos",1,1)->addColumn( $res['info_dinson'],1,1,"body");
    //    $tp->addColumn("Informasi Iuran",1,1)->addColumn($res['info_iuran'],1,1,"body");
    //    $tp->addColumn("Informasi SKTM",1,1)->addColumn($res['info_sktm'],1,1,"body");
    //    $tp->addColumn("Informasi prolanisPRB",1,1)->addColumn($res['info_prb'],1,1,"body");
    //    $tp->addColumn("Jenis Peserta",1,1)->addColumn($res['jenis_peserta'],1,1,"body");
    //    $tp->addColumn("Kode Jenis Peserta",1,1)->addColumn($res['kd_jenis_peserta'],1,1,"body");
    $tp->addColumn("Kelas",1,1)->addColumn($res['kelas'],1,1,"body");
    $tp->addColumn("Nama Provider",1,1)->addColumn($res['kd_cabang']." - ".$res['nama_cabang'],1,1,"body");

    $tp->addColumn("Informasi Dinsos",1,1)->addColumn($res['dinsos'],1,1,"body");
    $tp->addColumn("Informasi No. SKTM",1,1)->addColumn($res['sktm'],1,1,"body");
    $tp->addColumn("Informasi Prolanis PRB",1,1)->addColumn("<div id='vclaim_postensi_prb'>".$res['prb']."</div>",1,1,"body");

    $tp->addColumn("Status",1,1)->addColumn("<div class='badge badge-important'>".$res['status']."</badge>",1,1,"body");

    //TAMBAH RIWAYAT SEP DISINI
    $tp->addColumn("&nbsp;<br><br><br><br>",10,1,"body");
    $tp->addColumn("<strong>Riwayat SEP</strong>",10,1);
    $tp->commit("body");

    global $db;
    require_once "registration/class/responder/BPJSVClaimCheckerResponder.php";
    $chek = new BPJSVClaimCheckerResponder(new DBTable($db,"smis_rg_patient"),new Table(array()), new SimpleAdapter());
    $result = $chek->cari_history($res['nobpjs']);

    usort($result, function ($item1, $item2) {
        return $item1['tglSep'] < $item2['tglSep'];
    });
    /**
     *  {
                "diagnosa": "A00.1 - Cholera due to Vibrio cholerae 01, biovar eltor",
                "jnsPelayanan": "2",
                "kelasRawat": null,
                "namaPeserta": "STAMI",
                "noKartu": "0001160271256",
                "noSep": "0301R0110818V100085",
                "noRujukan": "0301U01108180201084",
                "poli": "",
                "ppkPelayanan": "RS YOS SUDARSO",
                "tglPlgSep": "2018-08-09",
                "tglSep": "2018-08-09"
             }
     * 
     */
    $max = 10;
    $num = 1;
    foreach($result as $r){
        if($num>=10){
            break;
        }
        
        $tp->addColumn($num.". Tanggal ".ArrayAdapter::format("date d M Y",$r['tglSep']),10,1,"body");
        $tp->addColumn("Jenis Pelayanan",1,1)->addColumn($r['jnsPelayanan']=="2"?"Rawat Jalan":"Rawat Inap",1,1,"body");
        $tp->addColumn("Poli",1,1)->addColumn($r['poli'],1,1,"body");
        $tp->addColumn("No. SEP",1,1)->addColumn($r['noSep'],1,1,"body");
        $tp->addColumn("No. Rujukan",1,1)->addColumn($r['noRujukan'],1,1,"body");
        $tp->addColumn("&nbsp;",10,1,"body");
        $num++;
    }

    return $tp->getHtml();
}

?>