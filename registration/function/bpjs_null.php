<?php 

function bpjs_null($msg=""){
        $res=array();
        $res['message']="Data Tidak Ditemukan Mohon Cek Nomor BPJS atau Nomor KTP, Jika Nomor KTP sudah benar, berarti pasien tidak terdaftar di BPJS";
        if($msg!=""){
            $res['message']=$msg;
        }
        $res['info_dinson']="";
        $res['info_iuran']="";
        $res['info_sktm']="";
        $res['info_prb']="";
        $res['nama']="";
        $res['nik']="";
        $res['nobpjs']="";
        $res['jk']="";
        $res['tgl_lahir']="";
        $res['tgl_daftar']="";
        $res['tgl_tat']="";
        $res['tgl_ckartu']="";            
        $res['umur']="";
        $res['status']="";
        $res['kd_status']="";
        $res['jenis_peserta']="";
        $res['kd_jenis_peserta']="";
        $res['kelas']="";
        $res['kd_kelas']="";
        $res['pisa']="";
        $res['nama_cabang']="";
        $res['nama_provider']="";
        $res['kd_cabang']="";
        $res['kd_provider']="";
        return $res;
    }

?>