<?php 

function bpjs_print_format(Database $db,$nosep,$res){

    $dtable = new DBTable($db,"smis_rg_sep");
    $sepdb = $dtable ->select(array("noSEP"=>$nosep));

    $tp=new TablePrint("sep_table");
    $tp->setMaxWidth(false);
    $tp->setDefaultBootrapClass(false);
    global $db;

    $logo_a = getSettings($db,"reg-bpjs-vclaim-logo",'');
    if($logo_a==""){
        $logo_a="registration/resource/image/bpjs-logo.png";
    }else{
        $logo_a="smis-upload/".$logo_a;
    }

    $logo_a = "<img src='".$logo_a."' id='logo_bpjs' style='width:150px; height:auto;'  />";


    //$tp->addColumn("<img src='registration/resource/image/bpjs-logo.png' id='logo_bpjs' style='width:50%;' />",1,2);
    $logo=getLogo();
    $tp->addColumn($logo_a,2,1);
    $tp->addColumn("<div id='title_bpjs' style='text-align:center;'> &nbsp;&nbsp;&nbsp;&nbsp;SURAT ELEGIBILITAS PESERTA</div><div style='text-align:center;' id='sub_title_bpjs'> &nbsp;&nbsp;&nbsp;&nbsp;".getSettings($db,"smis_autonomous_title","")."</div>",6,1);
    $tp->addColumn($sepdb->potensiprb,2,1);
    $tp->commit("header");
    
    $tp->addColumn("No.SEP",1,1)
       ->addColumn(":",1,1)
       ->addColumn("<strong><big>".$nosep."</big></strong>",8,1)
       ->commit("body");
       
    $tp->addColumn("Tgl SEP",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Tanggal SEP'],3,1)       
       ->addColumn("Peserta",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Jenis Peserta'],3,1)
       ->commit("body");
    
    $tp->addColumn("No Kartu",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['No BPJS']." (RM. ".$res['No Mr'].") ",3,1)
       ->addColumn("COB",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Status COB'],3,1)
       ->commit("body");


       
    $tp->addColumn("Nama Peserta",1,1)
       ->addColumn(":",1,1)
       ->addColumn(strtoupper($res['Nama']),3,1)
       ->addColumn("Jns Rawat",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Jenis Pelayanan'],3,1)
       ->commit("body");
       
    $tp->addColumn("Tgl Lahir",1,1)
       ->addColumn(":",1,1)
       ->addColumn(ArrayAdapter::format("date d M Y",$res['Tanggal Lahir']),1,1)
       ->addColumn("Jns Kelamin",1,1)
       ->addColumn(": ".$res['L/P'],1,1)
       ->addColumn("Jns Kunjungan",1,1)
       ->addColumn(":",1,1)
       ->addColumn($sepdb->TujuanKunjungan,3,1)
       ->commit("body");
    
   if($res['No. Telp']==null){
      $ptable = new DBTable($db,"smis_rg_patient");
      $px = $ptable->select(array("id"=>$res['No Mr']));
      $res['No. Telp'] = $px->telpon;
   }

    $tp->addColumn("No. Telp",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['No. Telp'],3,1)
       ->addColumn("",1,1)
       ->addColumn(":",1,1)
       ->addColumn($sepdb->AssesmentPelayanan,3,1)
       ->commit("body");
    
    $tp->addColumn("Sub/Spesialis",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Poli Tujuan'],3,1)
       ->addColumn("Poli Perujuk",1,1)
       ->addColumn(":",1,1)
       ->addColumn($sepdb->PoliPerujuk,3,1)
       ->commit("body");

   $tp->addColumn("Dokter",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Dokter'],3,1)
       ->addColumn("Kls Hak",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Kelas'],3,1)
       ->commit("body");
    
    $tp->addColumn("Faskes Perujuk",1,1)
       ->addColumn(":",1,1)
       ->addColumn($sepdb->namaRujukan,3,1)
       ->addColumn("Kls Rawat",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['KelasRawat'],3,1)
       ->commit("body");
    
    $tp->addColumn("Diagnosa Awal",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Diagnosa Awal'],3,1)
       ->addColumn("Penjamin",1,1)
       ->addColumn(":",1,1)
       ->addColumn($sepdb->penjamin,3,1)
       ->commit("body");
    
    $tp->addColumn("Catatan",1,1)
       ->addColumn(":",1,1)
       ->addColumn($res['Catatan'],3,1)
       ->addColumn("",3,1)
       ->addColumn("Pasien",2,1)               
       ->commit("body");
    
    $tp->addSpace(5,1)
       ->addColumn("",3,1)
       ->addColumn("Keluarga Pasien",2,1) //BPJS Kesehatan
       ->commit("body");
    
    $tp->addColumn("<small>*saya menyetujui bpjs kesehatan menggunakan informasi medis jika diperlukan</small>",5,1)
       ->addSpace(5,1)
       ->commit("body");
    
    $tp->addColumn("<small>*SEP bukan sebagai penjaminan peserta </small>",5,1)
       ->addSpace(5,1)
       ->commit("body");
       
    $tp->addColumn("<small>cetakan ke 1 - ".date("d-m-Y H:i:s")."</small>",5,1)
       ->addColumn("",3,1)
       ->addColumn("_______________",2,1)//_______________
       ->commit("body");
    
    $tp->addColumn("",10,1) //<!-- <strong><big>".$nosep."</big></strong> -->
       ->commit("body");
       
    $css=getSettings($db,"reg-bpjs-vclaim-print-css","");

    require_once 'smis-libs-out/mpdf-7.1.0/vendor/autoload.php';
   include 'smis-libs-out/mpdf-7.1.0/src/Mpdf.php';
   
   $pathfile = "smis-temp/SEP-".$nosep.".pdf";

   $mpdfConfig = array(
      'mode' => 'utf-8', 
      'format' => [215,140]  
   );
   //['mode' => 'utf-8', 'format' => 'A4-P']
   $mpdf = new \Mpdf\Mpdf($mpdfConfig);
   $print_data = "<html><body>".$tp->getHtml().$css."</body></html>";
   $mpdf->AddPageByArray([
      'margin-left' => 10,
      'margin-right' => 0,
      'margin-top' => 10,
      'margin-bottom' => 0,
  ]);

  
   
   $mpdf->WriteHTML($print_data,0);
   $mpdf->SetDisplayMode('fullpage');
   $mpdf->Output($pathfile, "F");
   return $pathfile;
}

?>