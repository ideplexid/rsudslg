<?php 

/**
 * this file is used by registration/class/responder/DaftarResponder.php
 * for creating a ring baby for patient.
 * */
	
global $db;
$id = $_POST['id'];
$dbtable = new DBTable($db, "smis_rgv_layananpasien");
$pasien = $dbtable->select($id);
$ukuran_font=getSettings($db, "reg-font-gelang-bayi", "10px");
$jumlah_font=getSettings($db, "reg-karakter-gelang-bayi", "20")*1;
$css=getSettings($db, "reg-css-gelang-bayi", "");
$barcode=getSettings($db, "reg-barcode-gelang-bayi", "0")=="1";
$boutput=getSettings($db, "reg-barcode-output-gelang-bayi", "css");
$bhri=getSettings($db, "reg-barcode-hri-gelang-bayi", "0");
$jsprint_setup=getSettings($db, "reg-show-gelang-bayi-js-setup", "");
$ibu=getSettings($db, "reg-ibu-gelang-bayi", "0")=="1"?$pasien->ibu:"";
$sebutan=getSettings($db, "reg-sebutan-gelang-bayi", "0")=="1"?$pasien->sebutan." ":"";

$nama_pasien = $sebutan.substr(strtoupper($pasien->nama_pasien), 0,$jumlah_font) ;
$ttl = strtoupper($pasien->tempat_lahir) . ", " . ArrayAdapter::format("date d M Y", $pasien->tgl_lahir);
$nrm_noreg = ArrayAdapter::format("only-digit6", $pasien->nrm) . " / " . ArrayAdapter::format("only-digit6", $pasien->id);

$table=new TablePrint("b_wbpatient_print",false);
$table->setUseFooter(false);
$table->setUseHeader(false);
$table->setMaxWidth(false);
$table->setStyle("border:0px;");
$table	->addColumn(substr($nama_pasien, 0, 20), 4,1)
		->commit("body");
$table	->addColumn($ttl, 4,1)
		->commit("body");
$table	->addColumn($nrm_noreg, 4,1)
		->commit("body");
if($ibu!=""){
	$table	->addColumn(substr($ibu, 0, 60), 4,1)
			->commit("body");
}
if($barcode){
	$bwidth=getSettings($db, "reg-barcode-width-gelang-bayi", "30");
	$bheight=getSettings($db, "reg-barcode-height-gelang-bayi", "2");
	$benc=getSettings($db, "reg-barcode-encrypt-gelang-bayi", "code128");
	$table	->addColumn("<div id='sbarcode' data-boutput='".$boutput."' data-bhri='".$bhri."' data-barcode='".$pasien->nrm."'  data-bwidth='".$bwidth."' data-bheight='".$bheight."' data-bcode='".$benc."' ></div>", 4,1)
			->commit("body");
}

$style = "
	<style type='text/css'>
		".$css."
		#b_wbpatient_print td, #b_wbpatient_print th {
			font-size	: ".$ukuran_font." !important;
		},
		#printing_area {
			border		: solid 0px !important;
		}
	</style>";
	/*	if($jsprint_setup==""){
	 $script="<script type='text/javascript'>window.print();</script>";
	}*/
echo  $table->getHtml() . $style.$jsprint_setup;

?>