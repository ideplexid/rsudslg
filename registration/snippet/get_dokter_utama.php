<?php 
global $db;
$nrm=$_POST['nrm'];
/*diambil YYYY-MM-DD HH:ii sedangkan second tidak dipakai */
$tanggal=substr($_POST['tanggal_masuk'],0,16);
$save_each=getSettings($db,"reg-activate-dokter-utama-each-time","0")=="1";
$dbtable=new DBTable($db,"smis_rg_dokter_utama");
$dbtable->addCustomKriteria("nrm_pasien","='".$nrm."'");
$tgl_selesai="";
if($save_each){
	/**
	 * khusus yang tersimpan setiap saat
	 * maka yang dicocokan adalah tanggal berakhirnya
	 * jika sudah ada yang tanggal berakhirnya sama maka tidak
	 * diubah.
	 * */
    loadLibrary("smis-libs-function-time");
	$tgl_selesai=addYear(substr($tanggal,0,10),1);
	$dbtable->addCustomKriteria("selesai","='".$tgl_selesai."'");
}else{
	/**khusus untuk mode yang mana
	 * selama ada yang aktif maka tidak dimasukan sebagai
	 * dokter utama baru
	 * */
	$dbtable->addCustomKriteria("selesai",">='".$tanggal."'");
}
$dbtable->setMaximum(1);
$dbtable->setOrder(" selesai DESC ");

$du=$dbtable->view("","0");
$ddu=$du['data'];
$result=array();
$keterangan="";
if(isset($ddu[0])){
	/**
	 * pasien memiliki data Dokter Utama
	 * sehingga data dokter utama yang lama yang dipakai
	 * */
	$one_du=$ddu[0];
	$kode=$one_du->kode;
	$mulai=$one_du->mulai;
	$akhir=$one_du->selesai;
	$id=$one_du->id;
	$petugas=$one_du->petugas;
	$keterangan="Pasien Ini Pernah Masuk Pada Tanggal [".ArrayAdapter::format("date d M Y H:i",$mulai)."] Sehingga Mendapatkan Kode [".$kode."] dan baru akan berakhir pada [".ArrayAdapter::format("date d M Y",$akhir)."] dengan ID ['".$id."'] dan dimasukan oleh [".$petugas."]";
	$result['kode_du']=$kode;
	$result['suggest_du']=$kode;
	$result['keterangan']=$keterangan;
	$result['editable']="0";
}else{	 
	$kriteria["nrm_pasien"]="='".$nrm."'";
	$dbtable->setCustomKriteria($kriteria);
	$du=$dbtable->view("","0");
	$ddu=$du['data'];
	if(isset($ddu[0]) && !$save_each){
		/**
		  * pasien tidak memiliki data tetapi sudah kedaluarsa. yang mana
		  * sudah habis masa pakainya. 
		  * jika mode penyimpanan setiap kunjungan diaktifkan
		  * maka user tidak akan pernah masuk kesini
		  * karena setiap kunjungan baru akan mendapatkan kode dokter 
		  * yang baru. bergeser stiap 1 tahun */
		$one_du=$ddu[0];
		$kode=$one_du->kode;
		$mulai=$one_du->mulai;
		$akhir=$one_du->mulai;
		$id=$one_du->id;
		$petugas=$one_du->petugas;
		$keterangan="Pasien Ini Pernah Masuk Pada Tanggal [".ArrayAdapter::format("date d M Y H:i",$mulai)."] dengan Kode [".$kode."] , dengan ID ['".$id."'] dan dimasukan oleh [".$petugas."] Tetapi Berakhir pada [".ArrayAdapter::format("date d M Y",$akhir)."] sehingga mendapatkan Kode Baru ";
	}else{
		/**
		  * pasien tidak memiliki data dokter
		  * utama sehingga akan dicari sesuai dengan jadwal dokter*/
		if($save_each){
			$keterangan="Model Per Kunjungan di Aktifkan Pasien ini akan mendapatkan Kode Dokter Baru Mulai Tanggal Hari ini sampai satu tahun ke depan [".ArrayAdapter::format("date d M Y",$tgl_selesai)."] ";	
		}else{
			$keterangan="Pasien Ini Belum Pernah Masuk ke System Dokter Utama Sehingga akan Mendapatkan Kode Baru ";			
		}	
	}
	
    require_once "registration/function/get_jadwal_dokter.php";
    $kode=getJadwalDokter($db,$tanggal);
	
	$result['kode_du']=$kode;
	$result['suggest_du']=$kode;
	$result['keterangan']=$keterangan." Sesuai Jadwal Dokter Saat ini [".ArrayAdapter::format("date d M Y H:i",$_POST['tanggal_masuk'])."] Yakni [".$kode."]";
	$result['editable']=getSettings($db,"reg-editable-dokter-utama","1");
}

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setContent($result);
echo json_encode($response->getPackage());

?>