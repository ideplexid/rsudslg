<?php 
global $db;
$id         = $_POST['id'];
$dbtable    = new DBTable($db,"smis_rg_yukantri");
$row        = $dbtable->select($_POST['id']);

$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$response ->setContent($row);

//pasien baru
$btn = new Button("","","Register Pasien Baru");
$btn ->setClass("btn btn-primary");
$btn ->setAction("yuk_antri.register_nrm()");
$btn ->setIsButton(Button::$ICONIC_TEXT);
$btn ->setIcon("fa fa-users");


if($row->yka_ktp=="" && $row->yka_nrm=="" && $row->yka_no_bpjs==""){    
    $result = "Data Pasien tidak Ditemukan di SIMRS, Apakah Anda ingin Mendata Pasien Baru ini ? </br>";
    $result .= $btn->getHtml();

    $response ->setWarning(true,"Data Pasien Tidak Ditemukan",$result);
}else{
    $dbtable  = new DBTable($db,"smis_rg_patient");
    //cek pakai nrm
    $px = $dbtable ->select($row->yka_nrm);
    if($px==NULL){
        //cek no bpjs
        $px = $dbtable ->select(array("nobpjs"=>$row->yka_no_bpjs));    
    }
    if($px==NULL){
        //cek ktp
        
        $px = $dbtable ->select(array("ktp"=>$row->yka_ktp));
    }
    if($px==NULL){
        $result = "Data Pasien tidak Ditemukan di SIMRS, Apakah Anda ingin Mendata Pasien Baru ini ? </br>";
        $result .= $btn->getHtml();    
        $response ->setWarning(true,"Data Pasien Tidak Ditemukan",$result);
    }else{
        $uptd = array();
        if($px->nobpjs=="" && $row->yka_no_bpjs!=""){
            $uptd['nobpjs'] = $row->yka_no_bpjs;
        }
        if($px->ktp=="" && $row->yka_ktp!=""){
            $uptd['ktp'] = $row->yka_ktp;
        }
        if(count($uptd)!=0){
            /**ambil KTP dan nobpjsnya */
            $dbtable->update($uptd,array("id"=>$px->id));
        }

        //echo json_encode($px);
        if($row->yka_carabayar=="bpjs"){
            $btn = new Button("","","Register BPJS");
            $btn ->setClass("btn btn-primary");
            $btn ->setAction(" yuk_antri.register_bpjs('".$px->id."')");
            $btn ->setIsButton(Button::$ICONIC_TEXT);
            $btn ->setIcon("fa fa-credit-card");
            $result = "Data Pasien Ditemukan di SIMRS, Apakah Anda ingin Mendaftarkan BPJS Pasien ini ? </br>";
            $result .= $btn->getHtml();
            $response ->setWarning(true,"Data Pasien Ditemukan",$result);
        }else{
            $slug = "";
            $list= getAllActivePrototypeOf($db,"rawat");
            foreach($list as $k=>$v){
                if($v==$row->yka_poli){
                    $slug = $k;
                }
            }

            $btn = new Button("","","Register Pasien Lama");
            $btn ->setClass("btn btn-primary");
            $btn ->setAction("yuk_antri.register_noreg('".$px->id."','".$slug."','".$row->yka_carabayar."')");
            $btn ->setIsButton(Button::$ICONIC_TEXT);
            $btn ->setIcon("fa fa-users");
            $result = "Data Pasien Ditemukan di SIMRS, Apakah Anda ingin Mendaftarkan Pasien ini ? </br>";
            $result .= $btn->getHtml();
            $response ->setWarning(true,"Data Pasien Ditemukan",$result);
        }
    }
}

echo json_encode($response->getPackage());