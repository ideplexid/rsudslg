<?php
/** 
 * this source used for searching patient bpjs number based on the data
 * that already inside the system, first from smis_rg_patient, if not found then
 * search in smis_rg_layananpasien
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 1 May 2018
 * @version     : 1.0.1
 * @license     : LGPLv1
 * @database    : - smis_rg_patient
 *                - smis_rg_layananpasien
 * */

global $db;
$nobpjs     = "";
$nrm        = $_POST['nrm'];
$dbtable    = new DBTable($db,"smis_rg_patient");
$px         = $dbtable->select($nrm);
if($px!=null && $px->nobpjs!=""){
    $nobpjs = trim($px->nobpjs);
}

if($nobpjs==""){
    $query  = "  SELECT  nobpjs FROM smis_rg_layananpasien 
                 WHERE   nrm = '".$nrm."' AND nobpjs!='' 
                         AND carabayar='bpjs' ORDER BY id DESC ";
    $nobpjs = $db->get_var($query);
}

$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
$response ->setContent($nobpjs);
echo json_encode($response->getPackage());
?>