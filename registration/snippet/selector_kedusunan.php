<?php 
/**
 * this snippet used for getting kelurahan list
 * and put it into selector so user can select the kelurahan 
 * list. 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @since 		: 6 Sept 2016
 * @database 	: smis_rg_kedusunan
 * @version		: 1.0.0
 * @used		: registration/modul/registration_patient.php 
 * 
 */
$kedusunan_dbtable 	 = new DBTable($db,"smis_rg_kedusunan");
$kedusunan_dbtable	 ->setShowAll(true)
					 ->addCustomKriteria(" no_kel "," ='".$_POST['id_kelurahan']."'")
					 ->setOrder(" nama ASC ");
$data				 = $kedusunan_dbtable->view("","0");
$kedusunan_adapter	 = new SelectAdapter("nama", "id");
$kedusunan			 = $kedusunan_adapter->getContent($data['data']);

if(  (isset($_POST['code']) && $_POST['code']!="search") ){
	$semua			 = array("name"=>"","value"=>"");
	$kedusunan[]	 = $semua;
}else if(!isset($_POST['code'])) {
	$kedusunan[]	 = array("name"=>'SEMUA',"value"=>"%");
}

$select		= new Select("","","");
$select		->setValue($kedusunan);
$response	= new ResponsePackage();
$response	->setStatus("ok")
			->setContent($select->getValue());
echo json_encode($response->getPackage());
?>