<?php 
global $db;
loadLibrary("smis-libs-function-barcode");
$dbtable	 	= new DBTable($db, "smis_rg_patient");
$p			 	= $dbtable->select($_POST['id']);
$NAMA_PASIEN 	= $p->nama;
$TGL_LAHIR	 	= ArrayAdapter::format("date d M Y",$p->tgl_lahir);    
$NRM		 	= $p->id;
$PROFILE	 	= $p->profile_number;
$ALAMAT		 	= "";
if(getSettings($db,"reg-barcode-kartu-alamat","0")=="1"){
	$MAX		= getSettings($db, "reg-barcode-kartu-max-alamat", "20")*1;
	$MAX_LURAH	= getSettings($db, "reg-kartu-max-kelurahan", "100")*1;
	$MAX_KECAM	= getSettings($db, "reg-kartu-max-kecamatan", "100")*1;
	$MAX_KABUP	= getSettings($db, "reg-kartu-max-kabupaten", "100")*1;	
	$_alamat	= $p->alamat;
	if(getSettings($db,"reg-barcode-kartu-alamat-replace","1")=="1"){
		$_alamat	= str_replace($p->nama_kelurahan," ",$p->alamat);
		$_alamat	= str_replace($p->nama_kecamatan," ",$_alamat);
		$_alamat	= str_replace($p->nama_kabupaten," ",$_alamat);	
	}
	$ALAMAT		= $_alamat." ".substr($p->nama_kelurahan,0,$MAX_LURAH)." ".substr($p->nama_kecamatan,0,$MAX_KECAM)." ".substr($p->nama_kabupaten,0,$MAX_KABUP);
	$ALAMAT		= substr($ALAMAT, 0,$MAX);
}

$NAME_TAG			= "";
$PROFILE_TAG		= "";
$NRM_TAG			= "";
$ALAMAT_TAG			= "";
$TITIK_DUA			= "";
$TGL_LAHIR_TAG		= "";
$title				= getSettings($db,"reg-barcode-show-title","0")=="1";
if($title){
	$NAME_TAG		= "<div id='nama_tag'>NAMA</div>";
	$NRM_TAG		= "<div id='nrm_tag'>NORM</div>";
	$PROFILE_TAG	= "<div id='profile_tag'>NOPROF</div>";
	$ALAMAT_TAG		= "<div id='alamat_tag'>ALAMAT</div>";
    $TGL_LAHIR_TAG	= "<div id='lahir_tag'>TG LHR</div>";
	$TITIK_DUA		= " : ";
}

$BARCODE	 	= "";
if(getSettings($db,"reg-barcode-kartu","0")=="1"){
	$bwidth		= getSettings($db,"reg-barcode-kartu-width","2");
	$bheight	= getSettings($db,"reg-barcode-kartu-height","30");
	$benc		= getSettings($db,"reg-barcode-kartu-encrypt","code128");
	$boutput	= getSettings($db,"reg-barcode-kartu-output","css");
	$bhri		= getSettings($db,"reg-barcode-kartu-hri","0");
	$BARCODE	=" 	<div id				= 'sbarcode' 
				   		data-boutput 	= '".$boutput."' 
				   		data-bhri	 	= '".$bhri."' 
						data-barcode	= '".$NRM."'  
						data-bwidth		= '".$bwidth."' 
						data-bheight	= '".$bheight."' 
						data-bcode		= '".$benc."' >
					</div>";
}

$R_ALAMAT  = getSettings($db,"reg-barcode-kartu-alamat","0")=="0"?"":$ALAMAT_TAG."  <div id='alamat_pasien'>".$TITIK_DUA.$ALAMAT."</div>";
$R_LAHIR   = getSettings($db,"reg-barcode-kartu-lahir","0")=="0"?"":$TGL_LAHIR_TAG." <div id='tgl_lahir'>".$TITIK_DUA.$TGL_LAHIR."</div>";
$R_NRM 	   = getSettings($db,"reg-barcode-kartu-nrm","0")=="0"?"":$NRM_TAG."  	 <div id='nrm_pasien'>".$TITIK_DUA.$NRM."</div>";
$R_PROFILE = getSettings($db,"reg-barcode-kartu-no-profile","0")=="0"?"":$PROFILE_TAG." <div id='profile_pasien'>".$TITIK_DUA.$PROFILE."</div>";
$JS		   = getSettings($db,"reg-kartu-js-bayi", "<script type='text/javascript'>window.print();</script>");


$result	= "<div id='kartu_pasien'>
				".$NAME_TAG."<div id='nama_pasien'>".$TITIK_DUA.$NAMA_PASIEN."</div>".
				$R_ALAMAT.
				$R_LAHIR.
				$R_NRM.
				$R_PROFILE.
				$BARCODE."
			</div>
			<style type='text/css'>".getSettings($db, "reg-kartu-css-bayi", "")."</style>".
			$JS;

$res = new ResponsePackage();
$res ->setStatus(ResponsePackage::$STATUS_OK);
$res ->setContent($result);
echo json_encode($res->getPackage());
?>