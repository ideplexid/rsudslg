<?php 
global $db;
$update['id']           = $_POST['id'];
$data['carapulang']     = "Tidak Datang";
$data['selesai']        = 1;
$data['tanggal_pulang'] = date("Y-m-d H:i:s");

$dbtable = new DBTable($db,"smis_rg_layananpasien");
$dbtable ->update($data,$update);

require_once("smis-base/smis-include-service-consumer.php");
$serv    = new ServiceConsumer($db,"pasien_batal");
$serv    ->addData("noreg_pasien",$_POST['id']);
$serv    ->execute();

$response = new ResponsePackage();
$response ->setStatus(ResponsePackage::$STATUS_OK);
echo json_encode($response->getPackage());
?>