<?php 
/**
 * this snippet used for getting kecamatan list
 * and put it into selector so user can select the kecamatan 
 * list. 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @since 		: 6 Sept 2016
 * @database 	: smis_rg_kecamatan
 * @version		: 1.0.0
 * @used		:   registration/resource/php/laporan/laporan_bonus.php 
 * 					registration/resource/php/laporan/laporan_pasien.php 
 * 					registration/resource/php/laporan/laporan_pasien_daftar.php 
 * 					registration/modul/registration_patient.php 
 * 
 */
 
$kecamatan_dbtable   = new DBTable($db,"smis_rg_kec");
$kecamatan_dbtable   ->setShowAll(true)
					 ->addCustomKriteria(" no_kab "," ='".$_POST['id_kabupaten']."'")
					 ->setOrder(" nama ASC ");
$data				 = $kecamatan_dbtable->view("","0");
$kecamatan_adapter	 = new SelectAdapter("nama", "id");
$kecamatan			 = $kecamatan_adapter->getContent($data['data']);

if(  (isset($_POST['code']) && $_POST['code']=="search") || getSettings($db, "reg-allow-empty-kecamatan", "0")=="1"){
	$semua			 = array("name"=>"","value"=>"");
	if($_POST['code']=="search") 
		array_unshift($kecamatan, $semua);
	else 
		$kecamatan[] = $semua;
}else if(!isset($_POST['code'])) {
	$kecamatan[]	 = array("name"=>'SEMUA',"value"=>"%");
}

$select		= new Select("","","");
$select 	->setValue($kecamatan);
$response	= new ResponsePackage();
$response	->setStatus("ok")
			->setContent($select->getValue());
echo json_encode($response->getPackage());
?>