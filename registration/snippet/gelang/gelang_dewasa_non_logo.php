<?php 

/**
 * this file is used by registration/class/responder/DaftarResponder.php
 * for creating a ring adult for patient.
 * */

/*GELANG UNTUK DEWASA*/
global $db;
$id = $_POST['id'];
$dbtable = new DBTable($db, "smis_rgv_layananpasien");
$pasien = $dbtable->select($id);
$ukuran_font=getSettings($db, "reg-font-gelang-dewasa", "13px");
$jumlah_font=getSettings($db, "reg-karakter-gelang-dewasa", "20")*1;
$css=getSettings($db, "reg-css-gelang-dewasa", "");
$barcode=getSettings($db, "reg-barcode-gelang-dewasa", "0")=="1";
$jsprint_setup=getSettings($db, "reg-show-gelang-dewasa-js-setup", "");
$nama_pasien = substr(strtoupper($pasien->nama_pasien), 0,$jumlah_font) ;
$ttl = strtoupper($pasien->tempat_lahir) . ", " . ArrayAdapter::format("date d M Y", $pasien->tgl_lahir);
$alamat = substr(strtoupper($pasien->alamat_pasien),0,$jumlah_font);
$nrm_noreg = ArrayAdapter::format("only-digit6", $pasien->nrm) . " / " . ArrayAdapter::format("only-digit6", $pasien->id);
$ibu=getSettings($db, "reg-ibu-gelang-dewasa", "0")=="1"?$pasien->ibu:"";
$sebutan=getSettings($db, "reg-sebutan-gelang-dewasa", "0")=="1"?$pasien->sebutan." ":"";

$table=new TablePrint("a_wbpatient_print",false);
$table->setUseFooter(false);
$table->setUseHeader(false);
$table->setMaxWidth(false);
$table->setStyle("border:0px;");

$table	->addColumn(substr($sebutan.$nama_pasien, 0, 60), 2,1)
		->addColumn(" &nbsp; ", 1,1)
		->addColumn($ttl, 2,1)
		->commit("body");
$table	->addColumn(substr($alamat, 0, 60), 2,1)
		->addColumn(" &nbsp; ", 1,1)
		->addColumn($nrm_noreg, 2,1)
		->commit("body");
if($ibu!=""){
	$table	->addColumn(substr($ibu, 0, 60), 5,1)
			->commit("body");
}
if($barcode){
	$bwidth=getSettings($db, "reg-barcode-width-gelang-dewasa", "2");
	$bheight=getSettings($db, "reg-barcode-height-gelang-dewasa", "30");
	$benc=getSettings($db, "reg-barcode-encrypt-gelang-dewasa", "code128");
	$boutput=getSettings($db, "reg-barcode-output-gelang-dewasa", "css");
	$bhri=getSettings($db, "reg-barcode-hri-gelang-dewasa", "0");				
	$BCD="<div id='sbarcode' data-boutput='".$boutput."' data-bhri='".$bhri."' data-barcode='".$pasien->nrm."'  data-bwidth='".$bwidth."' data-bheight='".$bheight."' data-bcode='".$benc."' ></div>";
	$table	->addColumn($BCD, 5, 1)
			->commit("body");
}


$style = "
	<style type='text/css'>
		".$css."
		#a_wbpatient_print td, #a_wbpatient_print th {
			font-size	: ".$ukuran_font." !important;
		},
		#printing_area {
			border		: solid 0px !important;
		}
	</style>";
echo  $table->getHtml() . $style.$jsprint_setup;

 ?>