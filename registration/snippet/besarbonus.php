<?php 

/**
 * this snippet used for searching and show the
 * patient Bonus Type. The user can search a bonus type 
 * in registration so it he/she could choose one of the type 
 * of patient referral bonus.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @since 		: 17 august 2015
 * @database 	: smis_rg_besarbonus
 * @version		: 1.0.0
 * @used		: registration/modul/registration_patient.php 
 * 
 */
global $db;
$header=array("Nama","Keterangan","Nilai");
$uitable=new Table($header);
$uitable->setName("besarbonus");
$uitable->setModel(Table::$SELECT);

if(isset($_POST['command'])){
	$adapter=new SimpleAdapter();
	$adapter->add("Nama", "nama")
			->add("Keterangan", "keterangan")
			->add("Nilai", "nilai","money Rp.");
	$dbtable=new DBTable($db, "smis_rg_besarbonus");
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}
echo $uitable->getHtml();

?>