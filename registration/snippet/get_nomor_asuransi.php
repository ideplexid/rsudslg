<?php 

/**
 * dipakai untuk mengambil data kepesertaan pasien
 * sehingga data nomor peserta dapat disajikan
 * berdasarkan asuransi yang dipilih
 * 
 * @author    : Nurul Huda
 * @since     : 17 Mei 2017
 * @database  : smis_rg_nomor_asuransi
 * @license   : LGPLv3
 * @copyright : goblooge@gmail.com
 * */

global $db;
$dbtable=new DBTable($db,"smis_rg_nomor_asuransi");
$kriteria['nrm_pasien']=$_POST['nrm_pasien'];
$kriteria['id_asuransi']=$_POST['id_asuransi'];
$row=$dbtable->select($kriteria);
$result=array();
$result['nomor']="";
if($row!=null){
    $result['nomor']=$row->nomor_asuransi;
}

$res=new ResponsePackage();
$res->setContent($result);
$res->setStatus(ResponsePackage::$STATUS_OK);
echo json_encode($res->getPackage());

?>