<?php 

/**
 * this function used to print data of patient
 * this separated because it's higly changeable and
 * make the source code too long.
 * 
 * @author 		: Nurul Huda
 * @license		: LGPLv2
 * @version 	: 1.0.0
 * @since 		: 7 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: registration/class/table/DaftarTable.php
 * @database 	: 	smis_rg_layananpasien
 * 					smis_rg_patient
 * 
 *
 * @param Object $pr 	: Pasien Raw, data asli pasien
 * @param Array $pf 	: Pasien Formatted , data pasien yang terformat
 * @param Object $rr 	: Register Raw , data register asli
 * @param Array $rf 	: Register Formatted, data register yang terformat
 */

function print_data_social($pr,$rr,$rf,$_mode){
	global $db;
    require_once("smis-base/smis-include-service-consumer.php");
    
    $serv 			= new ServiceConsumer($db,"get_kelas_sisa_kamar",NULL,$rr->kamar_inap);
    $serv 			->execute();
    $ctx 			= $serv ->getContent();
    $kelas_kamar    = $ctx['kelas'];
    
	$tp=new TablePrint("data_social_kunjugan_pasien");
	$tp->setDefaultBootrapClass(false);
	$tp->setMaxWidth(true);
    //$tp->setStyle("table.data_social_kunjugan_pasien { border-spacing:0; border-collapse:separate; }");
    $tp->setStyle("width: 100%;");
	
	$title=getSettings($db, "smis_autonomous_title", "");
	$subtitle=getSettings($db, "smis_autonomous_sub_title", "");
	$address=getSettings($db, "smis_autonomous_address", "");
	
    $tp->addColumn("<p align='right' style='font-size:10px; margin-bottom:0;'>FORM RM.1</p>",9,1,"title");
	$tp->addColumn("<h4 style='margin-bottom:0;margin-top:0;'>".$title."</h4>",9,1,"title");
	$tp->addColumn("<h3 style='margin-top:0;margin-bottom:0'>REKAM MEDIK RAWAT INAP</h3>",9,1,"title");
    $tp->addColumn("<h4 style='margin-top:0;'>U.P.F. _____________________</h4>",9,1,"title");
    
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>No.RM</b></p>", 1, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>: ".ArrayAdapter::format("only-digit8", $pr->id)."</b></p>", 1, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>No.Reg</p>", 1, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>: ".ArrayAdapter::format("only-digit8", $rr->id)."</b></p>", 1, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>No.Profile</b></p>", 1, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>: ".$pr->profile_number."</b></p>", 2, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0;'><b>A.</b></p>", 1, 1, null, null, "left_dash top_dash bottom_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0;'><b>IDENTITAS (Administrasi)</b></p>", 4, 1, null, null, "top_dash bottom_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0;'><b>B.</b></p>", 1, 1, null, null, "left_dash top_dash bottom_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0;'><b>MEDIS ADMINISTRASI (Ruangan)</b></p>", 3, 1, null, null, "top_dash right_dash bottom_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>1)</b></p>", 1, 1, null, null, "left_dash top_dash ")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>IDENTITAS PENDERITA</b></p>", 4, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>1)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>CARA DATANG RUMAH SAKIT</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->nama.", ".$pr->sebutan."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Sex</p>", 1, 1, null, null, "")
        ->addColumn($pr->kelamin==0?"<p style='font-size:11px; margin-bottom:0'>: L</p>":"<p style='font-size:11px; margin-bottom:0'>: P</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Pengirim</p>", 1, 1, null, null, "")
        ->addColumn($rr->rujukan != NULL ? "<p style='font-size:11px; margin-bottom:0'>: ".$rr->rujukan."</p>" : "<p style='font-size:11px; margin-bottom:0'>: Datang Sendiri</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
    
    $umur=str_replace(" Tahun ","T-",$rf['Umur']);
    $umur=str_replace(" Bulan ","B-",$umur);
    $umur=str_replace(" Hari","H",$umur);
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Tanggal Lahir / Umur</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".ArrayAdapter::format("date d M Y", $pr->tgl_lahir)." / ".$umur."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Agama</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->agama."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dengan / Tanpa Surat Rujukan*)</p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Alamat</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->alamat."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>RT / RW</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->rt." / ".$pr->rw."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->nama_rujukan."</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kel / Desa</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->nama_kelurahan."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kec</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->nama_kecamatan."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Alamat</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kota / Kab</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->nama_kabupaten."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Telp</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->telpon."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Diag. Masuk</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Pendidikan</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->pendidikan."</p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "left_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Pekerjaan</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->pekerjaan."</p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "left_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Status Perkawinan</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->status."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Suku</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->suku."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "left_dash right_dash")
        ->commit("body");
        
     $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>2)</b></p>", 1, 1, null, null, "left_dash top_dash ")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>IDENTITAS KELUARGA AYAH / IBU / SUAMI / ISTRI</b></p>", 4, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>2)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>PROSEDUR MRS : UGD / POLI*)</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    if($pr->ayah != NULL || $pr->ayah != "") {
        $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->ayah."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Umur</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->umur_keluarga."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Pertama:</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->nama_dokter."</p>", 2, 1, null, null, "right_dash")
            ->commit("body");
    } else if($pr->ibu != NULL || $pr->ibu != "") {
        $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->ibu."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Umur</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->umur_keluarga."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Pertama:</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->nama_dokter."</p>", 2, 1, null, null, "right_dash")
            ->commit("body");
    } else if($pr->suami != NULL || $pr->suami != "") {
        $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->suami."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Umur</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->umur_keluarga."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Pertama:</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->nama_dokter."</p>", 2, 1, null, null, "right_dash")
            ->commit("body");
    } else if($pr->istri != NULL || $pr->istri != "") {
        $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->istri."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Umur</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->umur_keluarga."</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Pertama:</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->nama_dokter."</p>", 2, 1, null, null, "right_dash")
            ->commit("body");
    } else {
        $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Umur</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>:</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Pertama:</p>", 1, 1, null, null, "")
            ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->nama_dokter."</p>", 2, 1, null, null, "right_dash")
            ->commit("body");
    }
    
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Alamat</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->alamat_keluarga."</p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Diag. Masuk</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kel / Desa</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->desa_keluarga."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kec</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->kecamatan_keluarga."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>3)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>KETERANGAN WAKTU MASUK RS</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kota / Kab</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->kabupaten_keluarga."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Telp</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->telepon_keluarga."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Tanggal</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".ArrayAdapter::format("date d M Y", $rr->tanggal_inap)."</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Pekerjaan</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$pr->pekerjaan_keluarga."</p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Jam</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".ArrayAdapter::format("date H:i:s", $rr->tanggal_inap)."</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>3)</b></p>", 1, 1, null, null, "left_dash top_dash ")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>PENANGGUNG BIAYA PERAWATAN</b></p>", 4, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>4)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>KET. KELAS PERAWATAN</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->namapenanggungjawab."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Umur</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->umur_pj."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kelas</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$kelas_kamar."</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Alamat</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->alamat_pj."</p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Biaya Harian<p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: Rp.<p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kel / Desa</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->desa_pj."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kec</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->kecamatan_pj."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>5)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>KET. PINDAH KELAS</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kab / Kota</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->kabupaten_pj."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Telp</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->telponpenanggungjawab."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Tanggal:</p>", 2, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Jam:</p>", 1, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Pekerjaan</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->pekerjaan_pj."</p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kelas</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>:</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Hubungan dengan penderita</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: ".$rr->hubungan_pj."</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>ttd</p>", 2, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Biaya Harian<p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: Rp.<p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 5, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Tanggal:</p>", 2, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Jam:</p>", 1, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 3, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>".$rr->namapenanggungjawab."</p>", 2, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kelas</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>:</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>4)</b></p>", 1, 1, null, null, "left_dash top_dash ")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>DIAGNOSA KELUAR (diisi oleh dokter)</b></p>", 4, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Biaya Harian<p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: Rp.<p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Nama Dokter</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Tanggal:</p>", 2, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Jam:</p>", 1, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Diagnosis</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>No.ICD</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Kelas</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>:</p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>No.ICD</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Biaya Harian<p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: Rp.<p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>No.ICD</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>6)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>KETERANGAN WAKTU KELUAR</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>No.ICD</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Tanggal: <p>", 2, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Jam:<p>", 1, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Komplikasi</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Cara Keluar: PP / Dipulangkan / Dirujuk ke*)<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Jenis Operasi</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>_____________________________<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Indek Operasi</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Keadaan keluar: sembuh / belum sembuh / mati<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Otopsi</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Lama Perawatan<p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>:", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>hr/bl<p>", 1, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Sebab Kematian</p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: </p>", 3, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Jumlah Biaya<p>", 1, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>: Rp<p>", 2, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>5)</b></p>", 1, 1, null, null, "left_dash top_dash ")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>TINDAKAN</b></p>", 4, 1, null, null, "top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>7)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>DOKTER PENANGGUNG JAWAB</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>1.</p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Jaga:<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>2.</p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Konsulen:<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>3.</p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Operator:<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>4.</p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Anestesi:<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>Dokter Ruangan:<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p align='center' style='font-size:11px; margin-bottom:0'><b>Dokter yang memulangkan</b></p>", 5, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>8)</b></p>", 1, 1, null, null, "left_dash top_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'><b>IMUNISASI YANG PERNAH DIDAPAT</b></p>", 3, 1, null, null, "top_dash right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'>1.BCG 2.DPT 3.Polio 4.Campak 5.Hepatitis 6._____<p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p align='center' style='font-size:11px; margin-bottom:0'><b>Petugas Ruangan</b><p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p align='center' style='font-size:11px; margin-bottom:0'><p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p align='center' style='font-size:11px; margin-bottom:0'><b>Penanggung Jawab Rekam Medik</b><p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 4, 1, null, null, "")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash")
        ->addColumn("<p align='center' style='font-size:11px; margin-bottom:0'><p>", 3, 1, null, null, "right_dash")
        ->commit("body");
        
    $tp ->addColumn("<p align='center' style='font-size:11px; margin-bottom:0'>________________________</p>", 5, 1, null, null, "left_dash bottom_dash")
        ->addColumn("<p style='font-size:11px; margin-bottom:0'></p>", 1, 1, null, null, "left_dash bottom_dash")
        ->addColumn("<p align='center' style='font-size:11px; margin-bottom:0'><p>", 3, 1, null, null, "right_dash bottom_dash")
        ->commit("body");
    
	$CSS=getSettings($db, "reg-print-data-social-css", "");
	$JS="";
	if(getSettings($db, "reg-print-data-social-jsetup", "0")=="1"){
		$JS=getSettings($db, "reg-print-data-social-js", "");
	}
	return $tp->getHtml().$CSS.$JS.addCSS("registration/resource/css/print_data_social_rm01.css", false);
	//return $tp->getHtml().$CSS.$JS;
}

?>