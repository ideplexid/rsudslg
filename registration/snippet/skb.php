<?php 

global $db;
loadLibrary("smis-libs-function-math");
loadLibrary("smis-libs-function-medical");
$dbtable=new DBTable($db, "smis_rg_patient");
$p=$dbtable->select($_POST['id']);

$tb=new TablePrint("skb");
$tb->setDefaultBootrapClass(false);
$tb->setTableClass("smis-print-table table-hover table-condensed");
$tb->setMaxWidth(true);
$title=getSettings($db, "smis_autonomous_title", "");
$sub=getSettings($db, "smis_autonomous_sub_title", "");
$addr=getSettings($db, "smis_autonomous_address", "");
$month=date("m");
$month=romanNumerals($month);
$year=date("Y");
$nomor=$p->id." / ".rand(1,1000)." / ".$month." / ".$year;

$nm=$p->nama." / ".medical_umur($p->tgl_lahir, date("Y-m-d"))." / ".($p->kelamin=="0"?"L":"P"); 

$dg="Dengan ini kami Karyawan / Keluarga / Pensiunan Karyawan PTPN XII (Persero) mohon mendapatkan Pemeriksaan / Pengobatan / Perawatan atas beban Perusahaan / Pribadi:";

$head="<strong>".$sub."</strong></br> <h4>".$title."</h4> </br> $addr";

$tb	->addColumn("<div id='skb_print_tbl'>".$head."<div>", 2,1,"title","","left");
$tb	->addColumn("<u><h4>SURAT KETERANGAN BEROBAT</h4></u>", 2,1,"title","","center");
$tb	->addColumn("No. ".$nomor, 2,1,"title","","center");
$tb	->addSpace(2, 1,"title");
$tb	->addColumn($dg, 2,1,"title","","left");
$tb	->addColumn("1. Nama / Umur / Jenis Kelamin", 1,1,NULL,"","left")
	->addColumn(": ".$nm, 1, 1,"title","","left");
$tb	->addColumn("2. Hubungan Keluarga", 1,1,NULL,"","left")
	->addColumn(": ", 1, 1,"title","","left");
$tb	->addColumn("3. Jabatan Golongan", 1,1,NULL,"","left")
	->addColumn(": ", 1, 1,"title","","left");
$tb	->addColumn("4. Kebun / Bagian", 1,1,NULL,"","left")
	->addColumn(": ", 1, 1,"title","","left");
$tb	->addColumn("5. Keluhan", 1,1,NULL,"","left")
	->addColumn(": ", 1, 1,"title","","left");
$tb	->addColumn("NB:", 2,1,"title","","left");
$tb	->addColumn("Bila dirawat di RS berhak kelas : ", 1,1,NULL,"","left")
	->addColumn("Jember, ".ArrayAdapter::format("date d M Y", date("Y-m-d")), 1, 1,"title","","center");
$tb	->addColumn(". . . . . . . . . . . . . . . . . . . . . . . . . . . . ", 1,1,NULL,"","left")
	->addSpace(1, 1,"title");
$tb	->addSpace(2, 1,"title");
$tb	->addSpace(2, 1,"title");
$tb	->addSpace(1, 1)
	->addColumn("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", 1, 1,"title","","center");
$tb	->addColumn("*) Coret yg Tidak Perlu : ", 1,1,NULL,"","left")
	->addColumn(ArrayAdapter::format("unslug", $p->nama), 1, 1,"title","","center");



$res=new ResponsePackage();
$res->setStatus(ResponsePackage::$STATUS_OK);
$res->setContent($tb->getHtml());
echo json_encode($res->getPackage());



?>