<?php 
/** this used to get url of fingerprint
 *  machine so the machine would understand 
 *  where the url that need to be visited
 *  to store and confirm the verivication code
 * 
 * @author      : Nurul Huda
 * @since       : 25 May 2017
 * @license     : LGPLv3
 * @version     : 1.0.0
 * @copyright   : goblooge@gmail.com
 * 
 * */
global $db;
if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
	require_once "smis-libs-class/DirectAccessManager.php";
    $base_url=curPageURL();
    $base_url=getFolderUrl($base_url)."/";
    
    $dma=DirectAccessManager::getIntance($db,null);
    $time_limit_reg=getSettings($db,"reg-fingerprint-time-limit-reg",15);
    $bridge_url=getSettings($db,"reg-fingerprint-direct-bridge");
    $dma->getDirectAccessSettings($bridge_url);
    $urlsave=$base_url."?smis_direct=".$dma->getCurrentService()."&&smis_direct_password=".$dma->getCurrentPassword()."&&smis_direct_autonomous=".$dma->getCurrentAutonomous()."&&bridge=store_finger_data";
    $urldevice=$base_url."?smis_direct=".$dma->getCurrentService()."&&smis_direct_password=".$dma->getCurrentPassword()."&&smis_direct_autonomous=".$dma->getCurrentAutonomous()."&&bridge=get_activation_code";
    $user_id=$_GET['user_id'];
    $hasil=$user_id.";SecurityKey;".$time_limit_reg.";".$urlsave.";".$urldevice;
    echo $hasil;
	
}

?>