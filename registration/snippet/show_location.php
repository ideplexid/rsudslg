<?php
    require_once "smis-base/smis-include-service-consumer.php";
    global $db;
    $serv   = new ServiceConsumer($db,"cek_location");
    $serv->addData("noreg_pasien",$_POST['id']);
    $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $serv->execute();
    $result = $serv->getContent();

    $printable = new TablePrint("");
    $printable ->setDefaultBootrapClass(true)
               ->setMaxWidth(false);
    $printable ->addColumn("Ruangan",1,1)
               ->addColumn("Status",1,1)
               ->addColumn("Waktu Keluar",1,1)
               ->addColumn("Cara Keluar",1,1)
               ->addColumn("Action",1,1)
               ->commit("title");           
    foreach($result as $x){
        $printable      ->addColumn($x['ruangan'],1,1)
                        ->addColumn($x['status']=="0"?"Berlangsung":"Selesai",1,1)
                        ->addColumn($x['status']=="0"?"-":ArrayAdapter::format("date d M Y H:i",$x['waktu_keluar']),1,1)
                        ->addColumn($x['status']=="0"?"-":$x['keluar'],1,1);
        if($x['status']=="0"){
            $printable  ->addColumn("-",1,1);
        }else{
            $kembali    = new Button("","","Kembalikan");
            $kembali    ->setIsButton(Button::$ICONIC)
                        ->setClass("btn btn-primary")
                        ->setIcon("fa fa-sign-in")
                        ->setAction("daftar_pasien.kembalikan('".$x['entity']."','".$_POST['id']."')");
            $printable  ->addColumn($kembali->getHtml(),1,1);
        }
        $printable  ->commit("body");
    }
    $res = new ResponsePackage();
    $res ->setStatus(ResponsePackage::$STATUS_OK);
    $res ->setWarning(true,"Lokasi Pasien",$printable->getHtml());
    echo json_encode($res->getPackage());
?>