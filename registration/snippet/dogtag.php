<?php 
/**
 * this file is used by registration/class/responder/DaftarResponder.php
 * for creating a dog tag for patient.
 * */


global $db;
$id=$_POST['id'];
$dbtable=new DBTable($db, "smis_rgv_layananpasien");
$px=$dbtable->select($id);
$jk=$px->kelamin=="1"?"<i class='fa fa-venus fa-2x'></i>":"<i class='fa fa-mars fa-2x'></i>";
$ttl=ArrayAdapter::format("date d-m-Y", $px->tgl_lahir)." , ".$px->tempat_lahir;
$max=getSettings($db, "reg-tag-max-char", "15")*1;

$RESULT="<div id='dogtag'>";


if(getSettings($db,"reg-tag-noreg","1")=="1"){
	$RESULT.="<div id='dogtag_noreg'>
				<div class='dogtag_title'>NORG</div>
				<div class='dogtag_colon'>:</div>
				<div class='dogtag_value'>".ArrayAdapter::format("only-digit10", $px->id)."</div>
			</div>";
}

if(getSettings($db,"reg-tag-nrm","1")=="1"){
	$RESULT.="<div id='dogtag_nrm'>
				<div class='dogtag_title'>NRM</div>
				<div class='dogtag_colon'>:</div>
				<div class='dogtag_value'>".ArrayAdapter::format("only-digit10", $px->nrm)."</div>
			</div>";
}

if(getSettings($db,"reg-tag-nama","1")=="1"){
	$RESULT.="<div id='dogtag_nama'>
				<div class='dogtag_title'>NAMA</div>
				<div class='dogtag_colon'>:</div>
				<div class='dogtag_value'>".substr(strtoupper($px->nama_pasien), 0,$max)."</div>
			</div>";
}

if(getSettings($db,"reg-tag-ttl","1")=="1"){
	$RESULT.="<div id='dogtag_ttl'>
				<div class='dogtag_title'>TTL</div>
				<div class='dogtag_colon'>:</div>
				<div class='dogtag_value'>".substr($ttl, 0,$max)."</div>
			</div>";
}

if(getSettings($db,"reg-tag-ibu","1")=="1"){
	$RESULT.="<div id='dogtag_ibu'>
				<div class='dogtag_title'>IBU</div>
				<div class='dogtag_colon'>:</div>
				<div class='dogtag_value'>".substr(strtoupper($px->ibu), 0,$max)."</div>
			</div>";
}

if(getSettings($db,"reg-tag-alamat","1")=="1"){
	$RESULT.="<div id='dogtag_alamat'>
				<div class='dogtag_title'>ALMT</div>
				<div class='dogtag_colon'>:</div>
				<div class='dogtag_value'>".substr($px->alamat_pasien." ".$px->nama_kelurahan." ".$px->nama_kecamatan." ".$px->nama_kabupaten,0, $max)."</div>
			</div>";
}

if(getSettings($db,"reg-barcode-dogtag","1")=="1"){
	$bwidth=getSettings($db, "reg-barcode-dogtag-width", "2");
	$bheight=getSettings($db, "reg-barcode-dogtag-height", "30");
	$benc=getSettings($db, "reg-barcode-dogtag-encrypt", "code128");
	$boutput=getSettings($db, "reg-barcode-dogtag-output", "css");
	$bhri=getSettings($db, "reg-barcode-dogtag-hri", "0");
	$BARCODE="<div id='sbarcode'
				data-boutput='".$boutput."'
				data-bhri='".$bhri."'
				data-barcode='".$px->nrm."'
				data-bwidth='".$bwidth."'
				data-bheight='".$bheight."'
				data-bcode='".$benc."' ></div>";
	$RESULT.=$BARCODE;
}

$RESULT.="</div>";

$CSS="<style type='text/css'>".getSettings($db, "reg-tag-css", "")."</style>";
$JS="";
if(getSettings($db, "reg-tag-jsprintsetup", "0")=="1"){
	$JS=getSettings($db, "reg-tag-js", "");
}
echo $RESULT.$CSS.$JS;

?>