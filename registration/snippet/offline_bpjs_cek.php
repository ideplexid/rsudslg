<?php 
global $db;
$nobpjs['no_pst']   = $_POST['no_bpjs'];
$dbtable            = new DBTable($db,"smis_rg_excel_bpjs");
$x                  = $dbtable->select($nobpjs);

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$content=array();
if($x==null){
    $response->setAlertVisible(true);
    $response->setAlertContent("Data Tidak DItemukan !!", "System tidak menemukan data ".$_POST['no_bpjs']." dalam database",Alert::$DANGER);
    $content['periode']  = "";
    $content['nama_ppk'] = "";
    $content['status']   = "0";
}else{
    $table = new TablePrint();
    $table ->setDefaultBootrapClass(false);
    $table ->setMaxWidth(false);
    $table ->addColumn("Data Nomor Peserta ".$_POST['no_bpjs'],3,1);
    $table ->commit("header");
    
    $table  ->addColumn("Faskes",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->nama_faskes,1,1)
            ->commit("body");
    $table  ->addColumn("PPK",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->nama_ppk,1,1)
            ->commit("body");
    $table  ->addColumn("Nama",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->nama_pasien,1,1)
            ->commit("body");
    $table  ->addColumn("Jenis Kelamin",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->jk,1,1)
            ->commit("body");
    $table  ->addColumn("Tanggal Lahir",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn(ArrayAdapter::format("date d M Y",$x->tgl_lahir),1,1)
            ->commit("body");
    $table  ->addColumn("PISA",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->pisa,1,1)
            ->commit("body");
    $table  ->addColumn("Alamat",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->alamat,1,1)
            ->commit("body");
    $table  ->addColumn("Jenis",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->jenis,1,1)
            ->commit("body");    
    $table  ->addColumn("Kode Asuransi",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->kode_asuransi,1,1)
            ->commit("body");
    $table  ->addColumn("NRM",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn($x->nrm,1,1)
            ->commit("body");
    $table  ->addColumn("Last Updated",1,1)
            ->addColumn(" : ",1,1)
            ->addColumn(ArrayAdapter::format("date d M Y",$x->last_update),1,1)
            ->commit("body");
            
    $response->setAlertVisible(true);
    $response->setAlertContent(" Data Ditemukan ",$table->getHtml());
    $content['periode']  = $x->periode;
    $content['nama_ppk'] = $x->nama_ppk;
    $content['status']   = "1";
}
$response->setContent($content);
echo json_encode($response->getPackage())
    
?>