<?php 
global $db;
require_once "smis-libs-class/DirectAccessManager.php";
$nrm=$_POST['id'];
$base_url=curPageURL();
$base_url=getFolderUrl($base_url)."/";
$dma=DirectAccessManager::getIntance($db,null);
$setup_url=getSettings($db,"reg-fingerprint-direct-bridge","");
$dma->getDirectAccessSettings($setup_url);
$urlsetup=$base_url."?smis_direct=".$dma->getCurrentService()."&&smis_direct_password=".$dma->getCurrentPassword()."&&smis_direct_autonomous=".$dma->getCurrentAutonomous()."&&bridge=get_reg_patient_fingerprint_url&&user_id=".$nrm;
$url_register = base64_encode($urlsetup);

$btn = new Button("fingerprint_loader", "fingerprint_loader", "Click Here to Initialize Registration Process");
$btn->setHref("finspot:FingerspotReg;".$url_register);
$btn->setClass("btn-success");
$btn->setAtribute("data-content='Register Finger Print' data-toggle='popover'");
$btn->setIcon("fa fa-hand-paper-o");
$btn->setIsButton(Button::$ICONIC_TEXT);
$content=$btn->getHtml();


$urlver=$base_url."?smis_direct=".$dma->getCurrentService()."&&smis_direct_password=".$dma->getCurrentPassword()."&&smis_direct_autonomous=".$dma->getCurrentAutonomous()."&&bridge=get_ver_patient_fingerprint_url&&user_id=".$nrm;
$url_register = base64_encode($urlsetup);

$btn = new Button("fingerprint_verify", "fingerprint_verify", "Click Here to Initialize Verivication Process");
$btn->setHref("finspot:FingerspotVer;".$url_register);
$btn->setClass("btn-primary");
$btn->setAtribute("data-content='Verify Finger Print' data-toggle='popover'");
$btn->setIcon("fa fa-recycle");
$btn->setIsButton(Button::$ICONIC_TEXT);
$content.="</br></br>".$btn->getHtml();

$pack=new ResponsePackage();
$pack->setAlertContent("Loading","Fingerprint is Loaded");
$pack->setWarning(true,"Initialize Device",$content);
$pack->setStatus(ResponsePackage::$STATUS_OK);
$pack->setContent("");

echo json_encode($pack->getPackage());

?>

