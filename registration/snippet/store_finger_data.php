<?php 
/** this used to store fingerprint data
 *  into database. of identified user
 * 
 * @author      : Nurul Huda
 * @since       : 25 May 2017
 * @database    : smis_rg_patient
 * @license     : LGPLv3
 * @version     : 1.0.0
 * @copyright   : goblooge@gmail.com
 * 
 * */
global $db;
if (isset($_POST['RegTemp']) && !empty($_POST['RegTemp'])) {    
    $data 		= explode(";",$_POST['RegTemp']);
    $vStamp 	= $data[0];
    $sn 		= $data[1];
    $user_id	= $data[2];
    $finger 	= $data[3];
    $dbtable=new DBTable($db,"smis_rg_patient");
    $up['fingerprint']=$finger;
    $up['fingerprint_proses']="reg_ok";
    $id['id']=$user_id;
    $dbtable->update($up,$id);
}

?>