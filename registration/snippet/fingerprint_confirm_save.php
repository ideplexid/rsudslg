<?php 
global $db;
$nrm=$_POST['id'];
$dbtable=new DBTable($db,"smis_rg_patient");
$patient=$dbtable->select($nrm);
$pack=new ResponsePackage();
$pack->setStatus(ResponsePackage::$STATUS_OK);
$msg="Finger Print Proses in Patient <strong>".$patient->nama."</strong> Has Been Failed, Close this box and try again";
$title="Finger Print Process Fail!!";
if($patient->fingerprint_proses=="reg_ok" && $patient->fingerprint!="" ){
    $msg="Finger Print Capture in Patient <strong>".$patient->nama."</strong> Successfull, you can Close this box. <pre>".$patient->fingerprint."</pre>";
    $title="Finger Print Capture Successfull !!";        
}else if($patient->fingerprint_proses=="ver_ok" && $patient->fingerprint){
    $msg="Finger Print Verivication in Patient <strong>".$patient->nama."</strong> Successfull, you can Close this box. <pre>".$patient->fingerprint."</pre>";
    $title="Finger Print Verivication Successfull !!";
}else if($patient->fingerprint_proses=="ver_fail" && $patient->fingerprint){
    $msg="Finger Print Verivication in Patient <strong>".$patient->nama."</strong> Failed, This is not the <strong>".$patient->nama."</strong> Finger, Close this box, and try again with other patient";
    $title="Finger Print Verivication Failed !!";
}
$content=array();
$content['msg']=$msg;
$content['title']=$title;
$pack->setContent($content);

echo json_encode($pack->getPackage());

?>

