<?php
    require_once "smis-base/smis-include-service-consumer.php";
    global $db;
    $array                  = array();
    $array['noreg_pasien']  = $_POST['noreg_pasien'];
    $serv                   = new ServiceConsumer( $db, "reactived_antrian", $array, $_POST['entity'] );
    $serv                   ->execute();

    $res = new ResponsePackage();
    $res ->setStatus(ResponsePackage::$STATUS_OK);
    $res ->setAlertVisible(true);
    $res ->setAlertContent("Berhasil","Pasien telah dikembalikan ke ruangan");
    echo json_encode($res->getPackage());

?>