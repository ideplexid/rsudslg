<?php
/** this used to get the verivication code
 *  of given serial number in finger print
 * 
 * @author      : Nurul Huda
 * @since       : 25 May 2017
 * @license     : LGPLv3
 * @version     : 1.0.0
 * @copyright   : goblooge@gmail.com
 * 
 * */
 
global $db;
if (isset($_GET['vc']) && !empty($_GET['vc'])) {	
    require_once "registration/function/get_device_ac_sn.php";
    $dbtable=new DBTable($db,"smis_rg_fingerprint");
    $data = getDeviceAcSn($dbtable,$_GET['vc']);
    echo $data[0]['ac'].$data[0]['sn'];
}
?>