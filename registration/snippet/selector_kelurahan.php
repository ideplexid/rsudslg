<?php 
/**
 * this snippet used for getting kelurahan list
 * and put it into selector so user can select the kelurahan 
 * list. 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @since 		: 6 Sept 2016
 * @database 	: smis_rg_kelurahan
 * @version		: 1.0.0
 * @used		:   registration/resource/php/laporan/laporan_bonus.php 
 * 					registration/resource/php/laporan/laporan_pasien.php 
 * 					registration/resource/php/laporan/laporan_pasien_daftar.php 
 * 					registration/modul/registration_patient.php 
 * 
 */
$kelurahan_dbtable 	 = new DBTable($db,"smis_rg_kelurahan");
$kelurahan_dbtable	 ->setShowAll(true)
					 ->addCustomKriteria(" no_kec "," ='".$_POST['id_kecamatan']."'")
					 ->setOrder(" nama ASC ");
$data				 = $kelurahan_dbtable->view("","0");
$kelurahan_adapter	 = new SelectAdapter("nama", "id");
$kelurahan			 = $kelurahan_adapter->getContent($data['data']);

if(  (isset($_POST['code']) && $_POST['code']=="search") || getSettings($db, "reg-allow-empty-kecamatan", "0")=="1"){
	$semua			 = array("name"=>"","value"=>"");
	if($_POST['code']=="search") 
		array_unshift($kelurahan, $semua);
	else 
		$kelurahan[] = $semua;
}else if(!isset($_POST['code'])) {
	$kelurahan[]	 = array("name"=>'SEMUA',"value"=>"%");
}

$select		= new Select("","","");
$select		->setValue($kelurahan);
$response	= new ResponsePackage();
$response	->setStatus("ok")
			->setContent($select->getValue());
echo json_encode($response->getPackage());
?>