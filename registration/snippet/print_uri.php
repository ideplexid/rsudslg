<?php 

/**
 * this snippet used for create excel file
 * for user that ready for printing.
 * because it's simplicity of the file
 * but taking too much resource. 
 * i decide to create in another place.
 * 
 * @used 		: /registration/resource/php/laporan/lap_uri.php
 * @database 	: smis_rgv_layananpasien
 * @author 		: Nurul Huda
 * @since 		: 26 Oct 2016
 * @version 	: 1.0.0
 * @license 	: Apache License v2
 * @copyright 	: goblooge@gmail.com
 * 
 * */

global $db;
$dbtable=new DBTable($db,"smis_rgv_layananpasien",array("id","tanggal","nama_pasien","kamar_inap","nrm","tanggal_pulang"));
$dbtable->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
$dbtable->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
$dbtable->addCustomKriteria(NULL, " uri='1'");
$barulama=$_POST['barulama'];
$carabayar=$_POST['carabayar'];
$jk=$_POST['jenis_kelamin'];
if($barulama!="") $dbtable->addCustomKriteria(" barulama ", " ='".$barulama."' ");
if($carabayar!="") $dbtable->addCustomKriteria(" carabayar ", " LIKE '%".$carabayar."%' ");
if($jk!="") $dbtable->addCustomKriteria(" kelamin ", " ='".$jk."' ");
$dbtable->setShowAll(true);
$dbtable->setOrder(" tanggal ASC ");
$patient=$dbtable->view("","0");
$list=$patient['data'];

date_default_timezone_set ( "Asia/Jakarta" );
require_once ("smis-libs-out/php-excel/PHPExcel.php"); 
$file = new PHPExcel ();
$file->getProperties ()->setCreator ( "" );
$file->getProperties ()->setLastModifiedBy ( "" );
$file->getProperties ()->setTitle ( "Data Pasien Rawat Inap" );
$file->getProperties ()->setSubject ( "Data Pasien Rawat Inap" );
$file->getProperties ()->setDescription ( "Data Pasien Rawat Inap" );
$file->getProperties ()->setKeywords ( "Data Pasien Rawat Inap " );
$file->getProperties ()->setCategory ( "Data Pasien Rawat Inap" );

$autname=getSettings($db,"smis_autonomous_name");

$sheet = $file->getActiveSheet ( 0 );
$sheet	->setTitle ( "Data Pasien Rawat Inap" );
$sheet	->mergeCells ( 'A1:G1' )
		->setCellValue ( 'A1',$autname );
$sheet	->mergeCells ( 'A2:G2' )
		->setCellValue ( 'A2',"Laporan Data Pasien Rawat Inap" );
$sheet	->setCellValue ( 'A3',"Dari" );
$sheet	->mergeCells ( 'B3:D3' )
		->setCellValue ( 'B3',ArrayAdapter::format("date d M Y H:i",$_POST['dari']) );
$sheet	->setCellValue ( 'E3',"Sampai" );
$sheet	->mergeCells ( 'F3:G3' )
		->setCellValue ( 'F3',ArrayAdapter::format("date d M Y H:i",$_POST['sampai']) );

$sheet ->getColumnDimension ( "A" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "B" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "C" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "D" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "E" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "F" )->setAutoSize ( true );
$sheet ->getColumnDimension ( "G" )->setAutoSize ( true );

$sheet->setCellValue ( 'A5',"No." );
$sheet->setCellValue ( 'B5',"No Reg" );
$sheet->setCellValue ( 'C5',"NRM" );
$sheet->setCellValue ( 'D5',"Nama" );
$sheet->setCellValue ( 'E5',"Ruangan" );
$sheet->setCellValue ( 'F5',"Masuk" );
$sheet->setCellValue ( 'G5',"Keluar" );
$sheet->getStyle ( "A1:G5" )->getFont ()->setBold ( true );

$nomor=0;
$column=5;
foreach($list as $x){
	$nomor++;
	$column++;
	$sheet->setCellValue ( 'A'.$column,$nomor );
	$sheet->setCellValue ( 'B'.$column,"'".ArrayAdapter::format("only-digit6",$x->id) );
	$sheet->setCellValue ( 'C'.$column,"'".ArrayAdapter::format("only-digit6",$x->nrm) );
	$sheet->setCellValue ( 'D'.$column,$x->nama_pasien );
	$sheet->setCellValue ( 'E'.$column,ArrayAdapter::format("unslug",$x->kamar_inap) );
	$sheet->setCellValue ( 'F'.$column,ArrayAdapter::format("date d M Y H:i",$x->tanggal) );
	$sheet->setCellValue ( 'G'.$column,ArrayAdapter::format("date d M Y H:i",$x->tanggal_pulang) );
}
$border = array ();
$border['borders']=array();
$border['borders']['allborders']=array();
$border['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
$sheet->getStyle ( "A5:G" .$column )->applyFromArray ( $border );

$center = array();
$center ['alignment']=array();
$center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
$sheet->getStyle ( 'A1:G2' )->applyFromArray ($center);

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="Data-Pasien.xls"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );
?>