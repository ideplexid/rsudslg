<?php 
global $db;

$sub_id  = getSettings($db,"reg-yuk-antri-sid","20");
$api_key = getSettings($db,"reg-yuk-antri-api","CNUGOnvjibsi6oMEiYLeZUg65U9ffE9a");

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "http://rest.yukngantri.com/v1/queues?sub_id=".$sub_id,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic Og==",
    "x-api-key: ".$api_key
  ),
));
$pkg    = new ResponsePackage();
$pkg    ->setStatus(ResponsePackage::$STATUS_OK);

$dbtable = new DBTable($db,"smis_rg_yukantri");

$response   = curl_exec($curl);
$err        = curl_error($curl);
curl_close($curl);
$pkg->setAlertVisible(true);
if ($err) {
  $pkg->setAlertContent("Error On Loading Sub ID",$err);
} else {
    
    $queue = json_decode($response,true);
    $rsp   = "";
    foreach($queue as $q){
        $cur_id   = $q['id'];
        $cur_date = substr($q['date'],0,10);

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://rest.yukngantri.com/v1/tickets?queue_id=".$cur_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "authorization: Basic Og==",
            "x-api-key: ".$api_key
        ),
        ));
        $response   = curl_exec($curl);
        $err        = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $rsp .= " Queue ID Error ".$err." </br>";
        } else {
            $tickets = json_decode($response,true);
            $rsp .= " Queue ID ".$cur_id." Success ".count($tickets)." Ticket </br>";

            foreach($tickets as $t){
                $one = array();
                $one['yka_id']          = $t['id'];
                $one['yka_number']      = $t['number'];
                $one['yka_code']        = $t['code'];
                $one['yka_poli']        = $t['user_data']['Poli'];
                $one['yka_carabayar']   = strtolower($t['user_data']['Cara Bayar']);
                $one['yka_no_bpjs']     = $t['user_data']['Nomor BPJS'];
                $one['yka_nama']        = $t['user_data']['Nama Pasien'];
                $one['yka_telp']        = $t['user_data']['Nomor Telepon'];
                $one['yka_nrm']         = $t['user_data']['Nomor Kartu Pasien'];
                $one['yka_ktp']         = $t['user_data']['Nomor KTP'];
                $one['yka_tanggal']     = $cur_date;
                $one['yka_org']         = $t['org_id'];
                $one['yka_queue']       = $t['queue_id'];
                $one['yka_userid']      = $t['user_id'];
                $dbtable->insertOrUpdate($one,array("yka_id"=>$one['yka_id']));
            }
        }
    }
    $pkg->setAlertContent("Result",$rsp);
}

echo json_encode($pkg->getPackage());