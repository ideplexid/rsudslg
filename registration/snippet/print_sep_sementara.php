<?php 

$tanggal         = $_POST['tanggal'];
$no_bpjs         = $_POST['no_bpjs'];
$no_rujukan      = $_POST['no_rujukan'];
$no_ktp          = $_POST['no_ktp'];
$nrm             = $_POST['nrm'];
$poli_tujuan     = $_POST['poli_tujuan'];
$diagnosa_awal   = $_POST['diagnosa_awal'];
$catatan         = $_POST['catatan'];
$jns_rawat       = $_POST['jns_rawat'];
$kasus_laka      = $_POST['kasus_laka'];
$lokasi_laka     = $_POST['lokasi_laka'];
$dbtable         = new DBTable($db,"smis_rg_patient");
$pasien          = $dbtable->select(array("id"=>$nrm));

$table=new TablePrint("print_sep_sementara");
$table->setDefaultBootrapClass(false);
$table->setMaxWidth(false);
$table->addColumn("SURAT KETERANGAN PESERTA BPJS",7,1);
$table->commit("title");
$table->addColumn(getSettings($db,"smis_autonomous_title"),7,1);
$table->commit("title");

$table->addColumn("Tgl Pemeriksaan",1,1);
$table->addColumn(":",1,1);
$table->addColumn(ArrayAdapter::dateFormat("date d M Y H:i",$tanggal),1,1);
$table->addColumn(" &nbsp; &nbsp; &nbsp;",1,1);
$table->addColumn("Jenis Rawat",1,1);
$table->addColumn(":",1,1);
$table->addColumn($jns_rawat,1,1);
$table->commit("body");

$table->addColumn("No. Kartu",1,1);
$table->addColumn(":",1,1);
$table->addColumn($no_bpjs,1,1);
$table->addColumn("",1,1);
$table->addColumn("Kasus Laka",1,1);
$table->addColumn(":",1,1);
$table->addColumn($kasus_laka,1,1);
$table->commit("body");

$table->addColumn("No. Rujukan",1,1);
$table->addColumn(":",1,1);
$table->addColumn($no_rujukan,1,1);
$table->addColumn("",1,1);
$table->addColumn("Lokasi Laka",1,1);
$table->addColumn(":",1,1);
$table->addColumn($lokasi_laka,1,1);
$table->commit("body");

$table->addColumn("No. KTP",1,1);
$table->addColumn(":",1,1);
$table->addColumn($no_ktp,1,1);
$table->addColumn("",4,1);
$table->commit("body");

$table->addColumn("Nama Pasien",1,1);
$table->addColumn(":",1,1);
$table->addColumn($pasien->nama,1,1);
$table->addColumn("",4,1);
$table->commit("body");

$table->addColumn("No. RM",1,1);
$table->addColumn(":",1,1);
$table->addColumn($pasien->id,1,1);
$table->addColumn("",1,1);
$table->addColumn("Pasien",1,1);
$table->addColumn("",1,1);
$table->addColumn("Petugas");
$table->commit("body");

$table->addColumn("Tgl Lahir",1,1);
$table->addColumn(":",1,1);
$table->addColumn(ArrayAdapter::dateFormat("date d M Y H:i",$pasien->tgl_lahir),1,1);
$table->addColumn("",1,1);
$table->addColumn("Keluarga Pasien",1,1);
$table->addColumn("",1,1);
$table->addColumn("Rumah Sakit");
$table->commit("body");

$table->addColumn("Jenis Kelamin",1,1);
$table->addColumn(":",1,1);
$table->addColumn($pasien->kelamin=="0"?"Laki-Laki":"Perempuan",1,1);
$table->addColumn("",4,1);
$table->commit("body");

$table->addColumn("Poli Tujuan",1,1);
$table->addColumn(":",1,1);
$table->addColumn($poli_tujuan,1,1);
$table->addColumn("",4,1);
$table->commit("body");

$table->addColumn("Diagnosa Awal",1,1);
$table->addColumn(":",1,1);
$table->addColumn($diagnosa_awal,1,1);
$table->addColumn("",4,1);
$table->commit("body");

$table->addColumn("Catatan",1,1);
$table->addColumn(":",1,1);
$table->addColumn($catatan,1,1);
$table->addColumn("",1,1);
$table->addColumn(" - - - - - - ",1,1);
$table->addColumn("",1,1);
$table->addColumn(" - - - - - - ");
$table->commit("body");

$css=addCSS("registration/resource/css/print_sep_sementara.css",false);

$pack=new ResponsePackage();
$pack->setStatus(ResponsePackage::$STATUS_OK);
$pack->setContent($table->getHtml().$css);
echo json_encode($pack->getPackage());
?>