<?php
global $db;
require_once "registration/class/MapRuangan.php";
$dbtable = new DBTable($db,"smis_rgv_layananpasien");
$px      = $dbtable ->select($_POST['id']);
$table   = new TablePrint("cetak_antrian");
$table   ->addColumn("NOMOR ANTRIAN ".$px->no_urut,3,1,NULL,"","b_title")
         ->commit("title");
if(getSettings($db,"reg-antrian-noreg","1")=="1"){
         $table   ->addColumn("No Reg",1,1,NULL,"","b_subtitle b_row")
                  ->addColumn(" : ",1,1,NULL,"","b_semicolon  b_row")
                  ->addColumn($px->id,1,1,"body","","b_content  b_row");
}
if(getSettings($db,"reg-antrian-tanggal","1")=="1"){
         $table   ->addColumn("Tanggal",1,1,NULL,"","b_subtitle b_row")
                  ->addColumn(" : ",1,1,NULL,"","b_semicolon  b_row")
                  ->addColumn(ArrayAdapter::format("date d M Y",$px->tanggal),1,1,"body","","b_content  b_row");
}
if(getSettings($db,"reg-antrian-nrm","1")=="1"){
         $table   ->addColumn("NRM",1,1,NULL,"","b_subtitle b_row")
                  ->addColumn(" : ",1,1,NULL,"","b_semicolon  b_row")
                  ->addColumn($px->nrm,1,1,"body","","b_content  b_row");
}
if(getSettings($db,"reg-antrian-nama","1")=="1"){
         $table   ->addColumn("Nama",1,1,NULL,"","b_subtitle b_row")
                  ->addColumn(" : ",1,1,NULL,"","b_semicolon  b_row")
                  ->addColumn($px->nama_pasien,1,1,"body","","b_content  b_row");
}
if(getSettings($db,"reg-antrian-layanan","1")=="1"){
         $table   ->addColumn("Layanan",1,1,NULL,"","b_subtitle b_row")
                  ->addColumn(" : ",1,1,NULL,"","b_semicolon  b_row")
                  ->addColumn(MapRuangan::getRealName($px->jenislayanan),1,1,"body","","b_content  b_row");
}
if(getSettings($db,"reg-antrian-dokter","1")=="1"){
         $table   ->addColumn("Dokter",1,1,NULL,"","b_subtitle b_row")
                  ->addColumn(" : ",1,1,NULL,"","b_semicolon  b_row")
                  ->addColumn($px->nama_dokter,1,1,"body","","b_content  b_row");
}

$table   ->addColumn("Karcis",1,1,NULL,"","b_subtitle b_row")
         ->addColumn(" : ",1,1,NULL,"","b_semicolon  b_row")
         ->addColumn(ArrayAdapter::format("money Rp.",$px->karcis),1,1,"body","","b_content  b_row");


$css    = getSettings($db,"reg-antrian-css","");
$js     = getSettings($db,"reg-antrian-js","");
$result = $table->getHtml().$css.$js;

$response = new ResponsePackage();
$response ->setContent($result);
$response ->setStatus(ResponsePackage::$STATUS_OK);
$ctx      = $response ->getPackage();
echo json_encode($ctx);
?>