<?php
global $db;
require_once "registration/class/MapRuangan.php";
$dbtable = new DBTable($db,"smis_rgv_layananpasien");
$px      = $dbtable ->select($_POST['id']);
$table   = new TablePrint("cetak_antrian");
$table   ->addColumn(getSettings($db,"smis_autonomous_name"),1,1,"title","","second-jumbo center");
if(getSettings($db,"reg-antrian-tanggal","1")=="1"){
    $table   ->addColumn(ArrayAdapter::format("date d M Y",$px->tanggal),1,1,"body","","second-jumbo center");
}

if(getSettings($db,"reg-antrian-layanan","1")=="1"){
    $table   ->addColumn(MapRuangan::getRealName($px->jenislayanan),1,1,"body","","second-jumbo center");
}

$table   ->addColumn(($px->no_urut),1,1,"body","","jumbo center");
if(getSettings($db,"reg-antrian-noreg","1")=="1"){
    $table   ->addColumn(ArrayAdapter::format("only-digit6",$px->id),1,1,"body","","second-jumbo center");
}

if(getSettings($db,"reg-antrian-nrm","1")=="1"){
    $table   ->addColumn(ArrayAdapter::format("only-digit6",$px->nrm),1,1,"body","","second-jumbo center");
}
if(getSettings($db,"reg-antrian-nama","1")=="1"){
    $table   ->addColumn(ArrayAdapter::format("unslug",$px->nama_pasien),1,1,"body","","second-jumbo center");
}

$css    = getSettings($db,"reg-antrian-css","");
$js     = getSettings($db,"reg-antrian-js","");
$result = $table->getHtml().$css.$js;

$response = new ResponsePackage();
$response ->setContent($result);
$response ->setStatus(ResponsePackage::$STATUS_OK);
$ctx      = $response ->getPackage();
echo json_encode($ctx);
?>