<?php 
/**
 * this snippet used for getting kabupaten list
 * and put it into selector so user can select the kabupaten 
 * list. 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @since 		: 6 Sept 2016
 * @database 	: smis_rg_kabupaten
 * @version		: 1.0.0
 * @used		:   registration/resource/php/laporan/laporan_bonus.php 
 * 					registration/resource/php/laporan/laporan_pasien.php 
 * 					registration/resource/php/laporan/laporan_pasien_daftar.php 
 * 					registration/modul/registration_patient.php  
 * 
 */

global $db;
$id_propinsi		 = isset($_POST['id_provinsi'])?$_POST['id_provinsi']:$_POST['id_propinsi'];
$kabupaten_dbtable 	 = new DBTable($db,"smis_rg_kabupaten");
$kabupaten_dbtable	 ->setShowAll(true)
					 ->addCustomKriteria(" no_prop "," ='".$id_propinsi."'")
					 ->setOrder(" nama ASC ");
$data				 = $kabupaten_dbtable->view("","0");
$kabupaten_adapter	 = new SelectAdapter("nama", "id");
$kabupaten			 = $kabupaten_adapter->getContent($data['data']);

if(  (isset($_POST['code']) && $_POST['code']=="search") || getSettings($db, "reg-allow-empty-kabupaten", "0")=="1"){
	$semua			 = array("name"=>"","value"=>"");
	if($_POST['code']=="search") 
		array_unshift($kabupaten, $semua);
	else 
		$kabupaten[] = $semua;
}else if(!isset($_POST['code'])) {
	$kabupaten[]=array("name"=>'SEMUA',"value"=>"%");
}

$select		= new Select("","","");
$select 	->setValue($kabupaten);
$response	= new ResponsePackage();
$response	->setStatus("ok")
			->setContent($select->getValue());
echo json_encode($response->getPackage());
?>