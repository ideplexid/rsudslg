<?php 
global $db;
if (isset($_POST['VerPas']) && !empty($_POST['VerPas'])) {
    $data 		= explode(";",$_POST['VerPas']);
	$user_id	= $data[0];
	$vStamp 	= $data[1];
	$time 		= $data[2];
	$sn 		= $data[3];
    
    $dbtable_px = new DBTable($db,"smis_rg_patient");
    $pasien     = $dbtable_px->select($user_id);
	$fingerData	= $pasien->fingerprint; 
    
    require_once "registration/function/get_device_by_sn.php";
    $dbtable    = new DBTable($db,"smis_rg_fingerprint");
	$device 	= getDeviceBySn($dbtable,$sn);
    
    $salt = md5($sn.$fingerData.$device[0]['vc'].$time.$user_id.$device[0]['vkey']);
	if (strtoupper($vStamp) == strtoupper($salt)) {
        $up['fingerprint_proses']="ver_ok";
        $id['id']=$user_id;
        $dbtable_px->update($up,$id);
    }else{
        $up['fingerprint_proses']="ver_fail";
        $id['id']=$user_id;
        $dbtable_px->update($up,$id);
    }
}


?>