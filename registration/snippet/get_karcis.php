<?php
	require_once "smis-base/smis-include-service-consumer.php";
	require_once 'registration/class/service/KarcisService.php';
	global $db;

	$barulama	= isset($_POST['barulama'])?$_POST['barulama']:"baru";
	$urji		= isset($_POST['urji'])?$_POST['urji']:"urj";
	$polislug 	= $_POST ['polislug'];

	$karcis = new KarcisService ( $db, $polislug ,$barulama,$urji);
	$karcis->execute ();
	$content = $karcis->getContent ();
	$result['administrasi'] = $content['content'];
	$result['sisa']  = "";
	$result['kelas'] = "";
	/* menampilkan jumlah dan kelas kamar */

	$serv 			 = new ServiceConsumer($db,"get_kelas_sisa_kamar",NULL,$polislug);
	$serv 			 ->execute();
	$ctx 			 = $serv ->getContent();
	if($ctx!=null){
		$result['sisa']  = $ctx['sisa'];
		$result['kelas'] = $ctx['kelas'];	
	}
	
	$pack = new ResponsePackage();
	$pack ->setStatus(ResponsePackage::$STATUS_OK);
	$pack ->setContent($result);
	echo json_encode($pack->getPackage());
?>