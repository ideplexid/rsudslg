<?php 

global $db;
$header=array("Nama","Alamat","Instansi");
$uitable=new Table($header);
$uitable->setName("perujuk");
$uitable->setModel(Table::$SELECT);

if(isset($_POST['command'])){
	$adapter=new SimpleAdapter();
	$adapter->add("Nama", "nama")
			->add("Alamat", "alamat")
			->add("Instansi", "instansi");
	$dbtable=new DBTable($db, "smis_rg_perujuk");
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}
echo $uitable->getHtml();

?>