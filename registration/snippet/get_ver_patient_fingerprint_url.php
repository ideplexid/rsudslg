<?php 
/** this used to get url of fingerprint
 *  machine so the machine would understand 
 *  where the url that need to be visited
 *  to to confirm finger the verivication provess
 * 
 * @author      : Nurul Huda
 * @since       : 25 May 2017
 * @license     : LGPLv3
 * @version     : 1.0.0
 * @database    : smis_rg_patient
 * @copyright   : goblooge@gmail.com
 * 
 * */
global $db;
if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
    
    $user_id 	= $_GET['user_id'];
    $dbtable    = new DBTable($db,"smis_rg_patient");
    $pasien     = $dbtable->select($user_id);
	$finger		= $pasien->fingerprint; 
    
	require_once "smis-libs-class/DirectAccessManager.php";
    $base_url=curPageURL();
    $base_url=getFolderUrl($base_url)."/";
    
    $dma=DirectAccessManager::getIntance($db,null);
    $time_limit_ver=getSettings($db,"reg-fingerprint-time-limit-ver",10);
    $bridge_url=getSettings($db,"reg-fingerprint-direct-bridge");
    $dma->getDirectAccessSettings($bridge_url);
    $urlver=$base_url."?smis_direct=".$dma->getCurrentService()."&&smis_direct_password=".$dma->getCurrentPassword()."&&smis_direct_autonomous=".$dma->getCurrentAutonomous()."&&bridge=verify_finger_data";
    $urldevice=$base_url."?smis_direct=".$dma->getCurrentService()."&&smis_direct_password=".$dma->getCurrentPassword()."&&smis_direct_autonomous=".$dma->getCurrentAutonomous()."&&bridge=get_activation_code";
    
    
    //echo "$user_id;".$finger[0]['finger_data'].";SecurityKey;".$time_limit_ver.";".$base_path."process_verification.php;".$base_path."getac.php".";extraParams";
	
    
    $user_id=$_GET['user_id'];
    $hasil=$user_id.";".$finger.";SecurityKey;".$time_limit_reg.";".$urlver.";".$urldevice.";extraParams";
    echo $hasil;
}

?>