<?php 
/**
 * 
 * 
 * DEFAULT RECOMMENDED JS
 * <script type='text/javascript'>
 * jsPrintSetup.clearSilentPrint();
 * jsPrintSetup.setOption('printSilent', 1);
 * jsPrintSetup.setOption('orientation', jsPrintSetup.kLandscapeOrientation);
 * jsPrintSetup.definePaperSize(102, 102, "DOGTAG_SMIS_PRINT", "CUSTOM DOGTAG", "DOGTAG PAPER", 19, 291,jsPrintSetup.kPaperSizeMillimeters);
 * jsPrintSetup.setPaperSizeData(102);
 * jsPrintSetup.setPrinter('102');
 * bootbox_choose_print();
 * </script>
 * 
 * DEFAULT RECOMMENDED CSS
 * .label_container{width:155mm; height:500; font-size:13px;}
 * div.label-element {float:left; margin-left:1mm; margin-top:1mm; border:solid 1px #000; width:75mm; height:23mm;}
 * div.element-row {clear:both; padding-left:4px;}
 * div.element-title {float:left; width:50px;}
 * div.element-colon {float:left;margin-left:3px;margin-right:2px; }
 * div.element-content {float:left; width:auto;}
 * 
 * */

global $db;
$_id			= $_POST['id'];
$dbtable		= new DBTable($db,"smis_rgv_layananpasien");
$_person		= $dbtable->select($_id);

$_total			= getSettings($db,"reg-label-inap-repeat","1")*1;
$_max_nama		= getSettings($db,"reg-label-inap-nama","20")*1;
$_max_alamat	= getSettings($db,"reg-label-inap-alamat","20")*1;
$_css			= getSettings($db,"reg-label-inap-css","");
$_js			= getSettings($db,"reg-label-inap-js","");
$s_sebutan		= getSettings($db, "reg-label-inap-sebutan", "0");
$sebutan		= $s_sebutan!="0"?$_person->sebutan." ":"";
$kabupaten		= getSettings($db, "reg-label-inap-kabupaten", "1")=="1"?"</br>".$_person->nama_kabupaten:"";
$profile 		= getSettings($db, "reg-label-show-inap-profile-number", "0")=="1"?$_person->profile_number:""; 


$pumur			= str_replace(" Tahun ","T-",$_person->umur);
$pumur			= str_replace(" Bulan ","B-",$pumur);
$pumur			= str_replace(" Hari","H",$pumur);
$kelamin		= $_person->kelamin==0?"(L)":"(P)";
$nama			= substr($_person->nama_pasien,0,$_max_nama)." ".$kelamin;
if($s_sebutan=="1"){
	$nama			= substr($sebutan.$_person->nama_pasien,0,$_max_nama)." ".$kelamin;
}else if($s_sebutan=="2"){
	$nama			= substr($_person->nama_pasien.",".$sebutan,0,$_max_nama)." ".$kelamin;
}

$umur=ArrayAdapter::format("date d M Y",$_person->tgl_lahir);
if(getSettings($db,"reg-label-inap-umur","0")=="1"){
    $umur		= ArrayAdapter::format("mini-date d M y",$_person->tgl_lahir).", ".$pumur;
}
$nrm			= ArrayAdapter::format("only-digit6",$_person->nrm);
$alamat			= substr($_person->alamat_pasien,0,$_max_alamat)." </br> ".$_person->nama_kelurahan." - ".$_person->nama_kecamatan.$kabupaten;

$element="<div class='label-element'>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>Nama</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$nama."</div>";
	$element.="</div>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>TL/UMR</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$umur."</div>";
	$element.="</div>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>No. RM</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$nrm."</div>";
	$element.="</div>";
	$element.="<div class='element-row'>";
		$element.="<div class='element-title'>Alamat</div>";
		$element.="<div class='element-colon'>:</div>";
		$element.="<div class='element-content'>".$alamat."</div>";
	$element.="</div>";
	if($profile!=""){
		$element.="<div class='element-row'>";
			$element.="<div class='element-title'>No. Profil</div>";
			$element.="<div class='element-colon'>:</div>";
			$element.="<div class='element-content'>".$profile."</div>";
		$element.="</div>";
    }
    if(getSettings($db,"reg-barcode-label-ri","0")=="1" || true){
        $bwidth=getSettings($db, "reg-barcode-label-ri-width", "2");
        $bheight=getSettings($db, "reg-barcode-label-ri-height", "30");
        $benc=getSettings($db, "reg-barcode-label-ri-encrypt", "code128");
        $boutput=getSettings($db, "reg-barcode-label-ri-output", "css");
        $bhri=getSettings($db, "reg-barcode-label-ri-hri", "0");				
        $BCD="<div id='sbarcode' class='sbarcode' data-boutput='".$boutput."' data-bhri='".$bhri."' data-barcode='".$_person->nrm."'  data-bwidth='".$bwidth."' data-bheight='".$bheight."' data-bcode='".$benc."' ></div>";
        $element.=$BCD;
    }
$element.="</div>";

$result.="<div class='label_container'>";
for($i=0;$i<$_total;$i++){
	$result.=$element;
}
$result.="</div>";
echo $result.$_css.$_js;
?>