<?php 

/**
 * this function used to print data of patient
 * this separated because it's higly changeable and
 * make the source code too long.
 * 
 * @author 		: Nurul Huda
 * @license		: LGPLv2
 * @version 	: 1.0.0
 * @since 		: 7 Sept 2016
 * @copyright 	: goblooge@gmail.com
 * @used		: registration/class/table/DaftarTable.php
 * @database 	: 	smis_rg_layananpasien
 * 					smis_rg_patient
 * 
 *
 * @param Object $pr 	: Pasien Raw, data asli pasien
 * @param Array $pf 	: Pasien Formatted , data pasien yang terformat
 * @param Object $rr 	: Register Raw , data register asli
 * @param Array $rf 	: Register Formatted, data register yang terformat
 */

function print_data_social($pr,$rr,$rf,$_mode){
	global $db;
	$tp=new TablePrint("data_social_kunjugan_pasien");
	$tp->setDefaultBootrapClass(false);
	$tp->setMaxWidth(true);
	
	$title=getSettings($db, "smis_autonomous_title", "");
	$subtitle=getSettings($db, "smis_autonomous_sub_title", "");
	$address=getSettings($db, "smis_autonomous_address", "");
	
	if($subtitle!="") $tp->addColumn($subtitle,9,1,"title");
	$tp->addColumn("<h5>".$title."</h5>",9,1,"title");
	$tp->addColumn("<h5>".$address."</h5>",9,1,"title");
	
	$tp	->addColumn("No. RM / No. Reg",1,1,null,null,"bold top_dash")
		->addColumn(":",1,1,null,null,"top_dash")
		->addColumn(ArrayAdapter::format("only-digit8", $pr->id)." / ".ArrayAdapter::format("only-digit8", $rr->id),1,1,null,null,"top_dash")
		->addColumn("Tanggal",1,1,null,null,"bold left_dash top_dash")->addColumn(":",1,1,null,null,"top_dash")->addColumn($rf['Tanggal'],1,1,null,null,"top_dash")
		->addColumn("Jam",1,1,null,null,"bold left_dash top_dash")
		->addColumn(":",1,1,null,null,"top_dash")
		->addColumn(date("H:i"),1,1,null,null,"top_dash")
		->commit("body");
	
	$tp	->addColumn("Nama Pasien",1,1,null,null,"bold")
		->addColumn(":",1,1)->addColumn($pr->nama,1,1)
		->addColumn("Jenis Kelamin",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)->addColumn($pr->kelamin==0?"Laki-Laki":"Perempuan",1,1)
		->addColumn("TTL",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn(ucwords($pr->tempat_lahir).", ".ArrayAdapter::format("date d M Y", $pr->tgl_lahir),1,1)
		->commit("body");
	
	$tp	->addColumn("Umur",1,1,null,null,"bold")
		->addColumn(":",1,1)
		->addColumn($rf["Umur"],1,1)
		->addColumn("Status",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->status,1,1)
		->addColumn("Agama",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->agama,1,1)
		->commit("body");
	
	$tp	->addColumn("Suku",1,1,null,null,"bold")
		->addColumn(":",1,1)
		->addColumn($pr->suku,1,1)
		->addColumn("Bahasa",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->bahasa,1,1)
		->addColumn("",1,1,null,null,"bold left_dash")
		->addColumn("",1,1)
		->addColumn("",1,1)
		->commit("body");
	
	$tp	->addColumn("Pendidikan",1,1,null,null,"bold")
		->addColumn(":",1,1)
		->addColumn($pr->pendidikan,1,1)
		->addColumn("Pekerjaan",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->pekerjaan,1,1)
		->addColumn("Alamat",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->alamat,1,1)
		->commit("body");
			
	$tp	->addColumn("No. Telp",1,1,null,null,"bold")
		->addColumn(":",1,1)
		->addColumn($pr->telpon,1,1)
		->addColumn("RT/RW",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->rt." / ".$pr->rw,1,1)
		->addColumn("Kelurahan",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->nama_kelurahan,1,1)
		->commit("body");
	
	$tp	->addColumn("Kecamatan",1,1,null,null,"bold")
		->addColumn(":",1,1)
		->addColumn($pr->nama_kecamatan,1,1)
		->addColumn("Kabupaten",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->nama_kabupaten,1,1)
		->addColumn("Propinsi",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->nama_provinsi,1,1)
		->commit("body");
	
	$tp	->addColumn("Ayah",1,1,null,null,"bold")
		->addColumn(":",1,1)
		->addColumn($pr->ayah,1,1)
		->addColumn("Ibu",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($pr->ibu,1,1)
		->addColumn("P. Jawab",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($rr->namapenanggungjawab,1,1)
		->commit("body");
	
	$tp	->addColumn("Telp. PJ",1,1,null,null,"bold")
		->addColumn(":",1,1)
		->addColumn($rr->telponpenanggungjawab,1,1)
		->addColumn("Jenis Pasien",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1)
		->addColumn($rr->no_kunjungan==1?"Baru":"Lama",1,1)
		->addColumn("Tujuan",1,1,null,null,"bold left_dash")
		->addColumn(":",1,1);	
    
    $_no_sep="";
	if($_mode=="inap"){
		$tp->addColumn($rf['Menginap']." - (RI) ",1,1);
        $_no_sep="(".$rr->no_sep_ri.")";
	}else{
        $ruangan=str_replace('","',", ",$rr->jenislayanan);
        $ruangan=str_replace('["',"",$ruangan);
        $ruangan=str_replace('"]',"",$ruangan);
        $tp->addColumn(ArrayAdapter::slugFormat("unslug",$ruangan)." - (RJ) ",1,1);
        $_no_sep="(".$rr->no_sep_rj.")";
	}
	$tp->commit("body");
	
	$tp	->addColumn("Cara Datang",1,1,null,null,"bold bottom_dash")
		->addColumn(":",1,1,null,null,"bottom_dash")
		->addColumn($rr->caradatang,1,1,null,null,"bottom_dash")
		->addColumn("Perujuk",1,1,null,null,"bold left_dash bottom_dash")
		->addColumn(":",1,1,null,null,"bottom_dash")
		->addColumn($rr->nama_rujukan,1,1,null,null,"bottom_dash")
		->addColumn("Cara Bayar",1,1,null,null,"bold left_dash bottom_dash")
		->addColumn(":",1,1,null,null,"bottom_dash")
		->addColumn($rr->carabayar." ".$rr->nama_asuransi." ".$_no_sep,1,1,null,null,"bottom_dash")
		->commit("body");
	
	$tp	->addColumn("Penanggung Jawab</br></br></br>",4,1)
		->addColumn("Petugas</br></br></br>",5,1,"footer");
	
			global $user;
	if($rr->namapenanggungjawab!=""){
		$pj="<i style='text-decoration: overline;'>".$rr->namapenanggungjawab."</i>";
	}else{
		$pj="<i style='text-decoration: overline;'>Tidak Ada</i>";
	}
	$tp	->addColumn($pj,4,1)
		->addColumn("<i style='text-decoration: overline;'>".$user->getNameOnly()."</i>",5,1,"footer");
	
	$CSS=getSettings($db, "reg-print-data-social-css", "");
	$JS="";
	if(getSettings($db, "reg-print-data-social-jsetup", "0")=="1"){
		$JS=getSettings($db, "reg-print-data-social-js", "");
	}
	return $tp->getHtml().$CSS.$JS;
}

?>