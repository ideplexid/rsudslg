<?php 

/**
 * khusus untuk pasien yang sudah pulang saja. */

global $db;
if(isset($_POST['command'])){
	$dbtablelayanan=new DBTable($db,'smis_rgv_layananpasien');
	$dbtablelayanan->setViewForSelect(true);
	$dbtablelayanan->setOrder(" smis_rgv_layananpasien.id DESC ");
	
	if(isset($_POST['nrm'])){
		$dbtablelayanan->addCustomKriteria("nrm", "='".$_POST['nrm']."'");
	}
	
	if(isset($_POST['nama_pasien']) && $_POST['nama_pasien']!=""){
		$nama=$dbtablelayanan->escaped_string($_POST['nama_pasien']);
		$dbtablelayanan->addCustomKriteria("nama_pasien", " LIKE '%".$nama."%'");
	}
	
	if(isset($_POST['nrm_pasien']) && $_POST['nrm_pasien']!=""){
		$nrm_pasien = $_POST['nrm_pasien'] * 1;
		$dbtablelayanan->addCustomKriteria("nrm", " LIKE '%".$nrm_pasien."%'");
	}

	if(isset($_POST['umur']) && $_POST['umur']!=""){
		$dbtablelayanan->addCustomKriteria("umur", " LIKE '%".$_POST['umur']."%'");
	}	
	
	if(isset($_POST['alamat_pasien']) && $_POST['alamat_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("alamat_pasien", " LIKE '%".$_POST['alamat_pasien']."%'");
	}
	
	if(isset($_POST['jenislayanan'])  && $_POST['jenislayanan']!=""){
		$dbtable->addCustomKriteria("jenislayanan","='".$_POST['jenislayanan']."'");
	}

	if(isset($_POST['nama_kecamatan']) && $_POST['nama_kecamatan']!=""){
		$dbtablelayanan->addCustomKriteria("nama_kecamatan", " LIKE '%".$_POST['nama_kecamatan']."%'");
	}	
	
	if(isset($_POST['jenis_pasien']) && $_POST['jenis_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("carabayar", " LIKE '%".$_POST['jenis_pasien']."%'");
	}
	
	if(isset($_POST['perusahaan_pasien']) && $_POST['perusahaan_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("n_perusahaan", " LIKE '%".$_POST['perusahaan_pasien']."%'");
	}
	
	if(isset($_POST['asuransi_pasien']) && $_POST['asuransi_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("nama_asuransi", " LIKE '%".$_POST['asuransi_pasien']."%'");
	}
	
	
	if(isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!=""){
		$noreg=$_POST['noreg_pasien']*1;
		$dbtablelayanan->addCustomKriteria("id", " LIKE '%".$noreg."%'");
	}
	
	if($_POST['command']!="edit")
		$dbtablelayanan->addCustomKriteria(" selesai "," ='0' ");
	
	if(isset($_POST['uri']) && $_POST['uri']!=""){
		$dbtablelayanan->addCustomKriteria(" uri "," LIKE '".$_POST['uri']."' ");
	}
	
	if(isset($_POST['dari'])){
		$dbtablelayanan->addCustomKriteria("tanggal>=","'".$_POST['dari']."'");
	}

	if(isset($_POST['sampai'])){
		$dbtablelayanan->addCustomKriteria("tanggal<","'".$_POST['sampai']."'");
	}

	if (isset($_POST['last_ruangan'])) {
		$dbtablelayanan->addCustomKriteria(" last_ruangan ", " LIKE '%" . ArrayAdapter::format("slug", strtolower($_POST['last_ruangan'])) . "%'");
	}

	if (isset($_POST['last_nama_ruangan'])) {
		$dbtablelayanan->addCustomKriteria(" last_nama_ruangan ", " LIKE '%" . $_POST['last_nama_ruangan'] . "%'");
	}
	
	$responder=new ServiceProvider($dbtablelayanan);
    
	$data=$responder->command($_POST['command']);
	echo json_encode($data);
	
}




?>