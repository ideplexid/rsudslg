<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$administrasi = 0;
		$billing_rawat_inap = 0;
		$billing_rawat_jalan = 0;
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		
		$row = $dbtable->get_row("
			SELECT jenislayanan AS 'unit_tujuan', karcis
			FROM smis_rg_layananpasien
			WHERE id = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND uri = '0'
		");
		if ($row != null) {
			if ($row->unit_tujuan == "instalasi_gawat_darurat") { // jika igd
				if ($row->karcis <= 15000) {
					$administrasi += 0;
					$billing_rawat_jalan += $row->karcis;
				} else {
					$billing_rawat_jalan += 15000;
					$administrasi += $row->karcis - 15000;
				}
			} else {
				if ($row->karcis >= 1000) {
					$billing_rawat_jalan += 1000;
					$administrasi += $row->karcis - 1000;
				} else {
					$billing_rawat_jalan += $row->karcis;
					$administrasi += 0;
				}
			}
		}

		$row = $dbtable->get_row("
			SELECT jenislayanan AS 'unit_tujuan', karcis
			FROM smis_rg_layananpasien
			WHERE id = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND uri = '1'
		");
		if ($row != null) {
			if ($row->unit_tujuan == "instalasi_gawat_darurat") { // jika igd
				if ($row->karcis <= 15000) {
					$administrasi += 0;
					$billing_rawat_inap += $row->karcis;
				} else {
					$billing_rawat_inap += 15000;
					$administrasi += $row->karcis - 15000;
				}
			} else {
				if ($row->karcis >= 1000) {
					$billing_rawat_inap += 1000;
					$administrasi += $row->karcis - 1000;
				} else {
					$billing_rawat_inap += $row->karcis;
					$administrasi += 0;
				}
			}
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"				=> "registration",
			"billing_rawat_jalan" 	=> $billing_rawat_jalan,
			"billing_rawat_inap" 	=> $billing_rawat_inap,
			"administrasi"			=> $administrasi
		);
		echo json_encode($data);
	}
?>