<?php 

/**
 * this used for update total package of patient that need to be 
 * payed. it set by cashier but save by registration.
 * also it set the inacbg code and description
 * 
 * @author 		: Nurul Huda
 * @since 		: 20 Des 2016
 * @version 	: 1.0.0
 * @license 	: Apache 2.0
 * @copyright 	: goblooge@gmail.com
 * @database 	: smis_rg_layananpasien
 * */
 
 global $db;
 $noreg=$_POST['noreg_pasien'];
 $bayar=$_POST['total_paket'];
 $code=$_POST['kode_inacbg'];
 $desc=$_POST['desc_inacbg'];
 
 $id=array("id"=>$noreg);
 $up=array();
 $up['total_paket']=$bayar;
 $up['inacbg_bpjs']=$code;
 $up['deskripsi_bpjs']=$desc;
 $dbtable=new DBTable($db, "smis_rg_layananpasien");
 $dbtable->update($up,$id);
 
?>