<?php 
global $db;
require_once "registration/function/get_jadwal_dokter.php";
$nrm=$_POST['nrm']*1;
$tanggal=substr($_POST['tanggal'],0,16);
$_kode_operator=getRowJadwalDokter($db,$tanggal);

$table=new TablePrint("");
$table->setDefaultBootrapClass(true);
$table->setMaxWidth(false);
$table->addColumn("DETAIL NRM ".$nrm,5,1);
$table->commit("header");

$table->addColumn("Tanggal",2,1);
$table->addColumn(":",1,1);
$table->addColumn(ArrayAdapter::format("date D, d M Y H:i",$tanggal),3,1);
$table->commit("body");

$table->addColumn("Kode Operator",2,1);
$table->addColumn(":",1,1);
$table->addColumn($_kode_operator['kode'],3,1);
$table->commit("body");

if($_kode_operator['nama_dokter_satu']!=""){
    $table->addColumn("Kode Dokter I",2,1);
    $table->addColumn(":",1,1);
    $table->addColumn($_kode_operator['nama_dokter_satu'],3,1);
    $table->commit("body");
}

if($_kode_operator['nama_dokter_dua']!=""){
    $table->addColumn("Kode Dokter II",2,1);
    $table->addColumn(":",1,1);
    $table->addColumn($_kode_operator['nama_dokter_dua'],3,1);
    $table->commit("body");
}

//kode perujuk
$dbtable=new DBTable($db,"smis_rg_dokter_utama");
$dbtable->addCustomKriteria("nrm_pasien","='".$nrm."'");
$dbtable->addCustomKriteria("selesai",">='".$tanggal."'");
$dbtable->addCustomKriteria(null," kode != '' ");
$dbtable->addCustomKriteria(null," kode != 'N/A' ");
$dbtable->setShowAll(true);    
$dbtable->setMaximum(10);
$dbtable->setOrder(" selesai ASC ");
$du=$dbtable->view("","0");
$list=$du['data'];
if(count($list)>0){
    $table->addSpace(5,1);
    $table->commit("body");
    $table->addColumn("<strong>History Perujuk</strong>",5,1);
    $table->commit("body");
    
    $table->addColumn("<strong>Kode</strong>",1,1);
    $table->addColumn("<strong>Mulai</strong>",1,1);
    $table->addColumn("<strong>Selesai</strong>",1,1);
    $table->addColumn("<strong>Petugas</strong>",1,1);
    $table->addColumn("<strong>Keterangan</strong>",1,1);
    $table->commit("body");
    
    foreach($list as $x){
        $table->addColumn($x->kode,1,1);
        $table->addColumn(ArrayAdapter::format("date D, d M Y H:i",$x->mulai),1,1);
        $table->addColumn(ArrayAdapter::format("date D, d M Y",$x->selesai),1,1);
        $table->addColumn($x->petugas,1,1);
        $table->addColumn($x->keterangan,1,1);
        $table->commit("body");
    }
}

echo json_encode($table->getHtml());

?>
