<?php
    /**
     * this function used to update the total tagihan 
     * of patient when patient after crawler
     * not only total tagihan but also 
     * total pembayaran 
     * 
     * @author      : goblooge@gmail.com
     * @since       : 16 Feb 2017
     * @license     : LGPLv3
     * @database    : smis_rg_layananpasien
     * 
     * */
     
	global $db;
	$noreg = $_POST['noreg_pasien'];
	$total = $_POST['total_tagihan'];
	$administrasi = $_POST['administrasi'];

    $dbtable = new DBTable($db,"smis_rg_layananpasien");
	$px = $dbtable->select($noreg);
    $up = array();
    if($px->uri=="1"){
        if($px->carabayar=="jasa_raharja"){
            $up["administrasi_inap"] = $administrasi;
        }
    }else{
        $up["administrasi_inap"] = 0;
    }
	$up["total_tagihan"]=$total;
	$up["tb_cash"]=$_POST['cash'];
	$up["tb_bank"]=$_POST['bank'];
	$up["tb_asuransi"]=$_POST['asuransi'];
    $up["tb_diskon"]=$_POST['diskon'];
    $up["diskon_keterangan"]=$_POST['diskon_keterangan'];
	$id = array("id"=>$noreg);
	$dbtable->update($up,$id);

    $cur = $dbtable->select($id);
    
    echo json_encode($cur);

?>