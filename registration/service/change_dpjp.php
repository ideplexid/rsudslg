<?php 

/**
 * service ini berfungsi untuk melakukan update
 * history dokter DPJP jika terjadi perubahan dokter DPJP  
 * 
 * */

$_NOREG=$_POST['noreg_pasien'];
$dbtable = new DBTable($db, "smis_rg_layananpasien");
$one=$dbtable->select($_NOREG);
$_history_dpjp=json_decode($one->history_dokter,true);

/* jika dokter dpjp sudah ada isinya tapi history adalah null ,
 * berarti di backup dulu yang lama
 * - ke history dan yang baru nanti di backup juga ke history
 * - bedanya yang lama sesuai dengan waktu di $dbtable->tanggal_inap
 * - yang baru sesuai waktu saat ini.
 * */
if($one->nama_dokter!="" && $_history_dpjp==NULL){
	$new_dpjp=array();
	$new_dpjp['id_dokter']=$one->id_dokter;
	$new_dpjp['nama_dokter']=$one->nama_dokter;
	$new_dpjp['waktu']=$one->tanggal_inap;
	$_history_dpjp=array();
	$_history_dpjp[]=$new_dpjp;
}

if($one->id_dokter!=$_POST['id_dokter'] && $_POST['id_dokter']!="" &&  $_POST['id_dokter']!=0 &&  $_POST['id_dokter']!="0"){
	$unew_dpjp=array();
	$unew_dpjp['id_dokter']=$_POST['id_dokter'];
	$unew_dpjp['nama_dokter']=$_POST['nama_dokter'];
	$unew_dpjp['waktu']=date("Y-m-d H:i:s");
	$_history_dpjp[]=$unew_dpjp;

	$update=array();
	$update['id_dokter']=$_POST['id_dokter'];
	$update['nama_dokter']=$_POST['nama_dokter'];
	$update['history_dokter']=json_encode($_history_dpjp);
	$dbtable->update($update,array("id"=>$_NOREG));	
}

?>