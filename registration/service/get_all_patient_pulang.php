<?php 
	global $db;
	$dbtable=new DBTable($db, "smis_rgv_layananpasien");
	$dbtable->addCustomKriteria("selesai", "='1'");
	$dbtable->addCustomKriteria("tanggal_pulang >=", "'".$_POST['dari']."'");
	$dbtable->addCustomKriteria("tanggal_pulang <", "'".$_POST['sampai']."'");
	$column=array("id","nama_pasien","nrm","uri","carabayar","carapulang","tanggal_pulang","n_perusahaan","nama_asuransi","nobpjs");
	$dbtable->setView($column);
	$dbtable->setShowAll(true);
	$data=$dbtable->view("", "0");
	$list=$data['data'];
	echo json_encode($list);
?>