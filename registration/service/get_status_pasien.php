<?php
	global $db;

	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		$row = $dbtable->select($noreg_pasien);
		$is_active = "1";
		if ($row->selesai == "1")
			$is_active = "0";
		$data = array(
			"is_active"	=> $is_active
		);
		echo json_encode($data);
	}
?>