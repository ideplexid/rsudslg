<?php 
/**
 * this function used to count how many doctor presentio
 * based on the scedule that he have in database
 * so that we could count the salary of the doctor
 * 
 * @author : Nurul Huda
 * @since : 06-May-2017
 * @version : 1.0.0
 * @database :  smis_rg_rekap_jadwal
 * @copyright  : goblooge@gmail.com
 * @license : LGPLv3
 * 
 * 
 * */
 
global $db;
$id_dokter=$_POST['id_dokter'];
$dari=$_POST['dari'];
$sampai=$_POST['sampai'];
$query="SELECT carabayar, SUM(jumlah) as total 
        FROM smis_rg_rekap_jadwal 
        WHERE prop!='del' 
        AND tanggal>='".$dari."' 
        AND tanggal <='".$sampai."' 
        AND id_dokter='".$id_dokter."' 
        GROUP BY carabayar";
$result=$db->get_result($query);
echo json_encode($result);

?>
