<?php 
global $db;

if(isset($_POST['command']) && $_POST['command']!=""){

	require_once 'registration/class/RegistrationResource.php';
	$ruangan=RegistrationResource::getRuanganDaftar();
	$dari=$_POST['dari'];
	$sampai=$_POST['sampai'];
	
	$qv="SELECT karcis,
	YEAR(tanggal) as tahun,
	MONTH(tanggal) as bt,
	DATE_FORMAT(tanggal,'%M') as bulan
	";
	foreach($ruangan as $one){
		$jl=$one['value'];
		$qv.=" ,SUM(IF(jenislayanan='".$jl."',karcis,0)) as '".$jl."' ";
	}
	$qv.=" ,SUM(karcis) as total FROM smis_rg_layananpasien ";
	$qc="SELECT * FROM smis_rg_layananpasien";
	$dbtable=new DBTable($db, "smis_rg_layananpasien");
	$dbtable->setPreferredQuery(true, $qv, $qc);
	$dbtable->setUseWhereforView(true);
	$dbtable->setGroupBy(true, " tahun, bt, karcis ");
	$dbtable->setOrder(" tahun ASC, bt ASC ");
	$dbtable->setShowAll(true);
	$s=$dbtable->getQueryView("", "0");
	$serv=new ServiceProvider($dbtable);
	$res=$serv->command($_POST['command']);
	echo json_encode($res);
	return;
}


?>