<?php
show_error();
global $db;
require_once "registration/class/template/DashboardRegistration.php";
$page           = $_POST['page'];
$protoslug      = $_POST['prototype_slug'];
$protoimplement = $_POST['prototype_implement'];
$protoname      = $_POST['prototype_name'];
$action         = $_POST['action'];
$from           = $_POST['dari'];
$to             = $_POST['sampai'];
$rentang        = $_POST['rentang'];

ob_start();
$dashreg        = new DashboardRegistration($db,$page,$action,$protoslug,$protoname,$protoimplement,$from,$to,$rentang);
$dashreg        ->initialize();
$result         = ob_get_clean ();
echo json_encode($result);
?>