<?php 
global $user;
global $db;
$id['id']               		= $_POST['noreg_pasien'];
$update['last_ruangan'] 		= $_POST['last_ruangan'];
$update['last_bed']     		= $_POST['last_bed'];
$update['last_kelas']    		= $_POST['last_kelas'];
$update['last_nama_ruangan']    = $_POST['last_nama_ruangan'];

$dbtable = new DBTable($db,"smis_rg_layananpasien");
$dbtable ->update($update,$id);

$update['c_time'] = date("Y-m-d H:i:s");
$update['c_user'] = $user->getNameOnly();
$update['noreg_pasien'] = $_POST['noreg_pasien'];
$dbtable = new DBTable($db,"smis_rg_bed_pasien");
$dbtable ->insert($update);

echo json_encode($_POST);