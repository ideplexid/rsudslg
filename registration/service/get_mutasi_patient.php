<?php 
	/*	service ini dipakai untuk mencari pasien yang baik masuk maupun keluar 
	 *  dalam peride tertentu, tetapi khusus untuk pasien rawat inap saja*/
	global $db;
	if(isset($_POST['command'])){
		$dbtablelayanan=new DBTable($db,'smis_rgv_layananpasien');
		$dbtablelayanan->setOrder(" smis_rgv_layananpasien.id DESC ");
		$dbtablelayanan->setCustomKriteria(" uri = 1 AND
		( 	tanggal_pulang>='".$_POST['dari']."' 
			AND tanggal_pulang<'".$_POST['sampai']."' OR 
			tanggal_inap>='".$_POST['dari']."' 
			AND tanggal_inap<'".$_POST['sampai']."'
			) ");
		$dbtablelayanan->setMaximum(1);	
		if($_POST['command']=="count"){
			$query=$dbtablelayanan->getQueryCount("");
			$total=$db->get_var($query);
			echo json_encode($total);
			return;
		}
		$data=$dbtablelayanan->view("", $_POST['halaman']);
		echo json_encode($data['data']);
	}
?>