<?php 
global $db;
$dbtable=new DBTable($db, "smis_rl51");
$dbtable->setOrder("tahun ASC, bulan ASC ");
$dbtable->setShowAll(true);
if(isset($_POST['origin']) && $_POST['origin']!=""){
    $qv="SELECT '' as id, YEAR(tanggal) as tahun,
        MONTH(tanggal) as bulan,
        sum(if(barulama=0,1,0)) as baru,
        sum(if(barulama=1,1,0)) as lama,
        count(*) as total,
        '' as prop
        FROM `smis_rg_layananpasien`
        WHERE prop!='del'
        origin='".$_POST['origin']."'
        GROUP BY YEAR(tanggal),MONTH(tanggal);";
    $qc="SELECT count(*) FROM smis_rg_layananpasien";
    $dbtable->setPreferredQuery(true,$qv,$qc);
}
$service=new ServiceProvider($dbtable);
$pack=$service->command($_POST['command']);
echo json_encode($pack);
?>