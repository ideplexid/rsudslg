<?php 

global $db;
$dokter =$_POST['id_dokter'];
$dari   =strtotime($_POST['dari']);
$sampai =strtotime($_POST['sampai']);

$hasil_kelompok=array();
$hasil_hari=array();
$hasil_kelompok_hari=array();

$pengganti_kelompok=array();
$pengganti_hari=array();
$pengganti_kelompok_hari=array();


loadLibrary("smis-libs-function-time");
$dbtable=new DBTable($db,"smis_rg_jadwal");
$dbtable->addCustomKriteria(NULL," (id_dokter_satu='".$dokter."' OR id_dokter_dua='".$dokter."') ");
$dbtable->setShowAll(true);

$pengganti=new DBTable($db,"smis_rg_dokter_ganti");
$pengganti->addCustomKriteria(" tanggal >= ","'".$_POST['dari']."'");
$pengganti->addCustomKriteria(" tanggal < ","'".$_POST['sampai']."'");


$data=$dbtable->view("","0");
$list=$data['data'];
foreach($list as $x){    
    $hari=strtolower($x->hari);
    $hexaday=day_id_to_hexa($hari);
    $inday=day_id_to_int($hari);
    
    //pencarian jadwal berdasarkan hari
    $total=number_of_days($hexaday,$dari,$sampai);
    if(!isset($hasil_kelompok[$x->kelompok])){
        $hasil_kelompok[$x->kelompok]=0;
    }
    if(!isset($hasil_hari[$x->hari])){
        $hasil_hari[$x->hari]=0;
    }
    if(!isset($hasil_kelompok_hari[$x->hari." - ".$x->kelompok])){
        $hasil_kelompok_hari[$x->hari." - ".$x->kelompok]=0;
    }
    $hasil_kelompok_hari[$x->hari." - ".$x->kelompok]+=$total;
    $hasil_kelompok[$x->kelompok]+=$total;
    $hasil_hari[$x->hari]+=$total;
    
    //pengurangan karena jadwal ganti
    if(!isset($pengganti_kelompok[$x->kelompok])){
        $pengganti_kelompok[$x->kelompok]=0;
    }
    if(!isset($pengganti_hari[$x->hari])){
        $pengganti_hari[$x->hari]=0;
    }
    if(!isset($pengganti_kelompok_hari[$x->hari." - ".$x->kelompok])){
        $pengganti_kelompok_hari[$x->hari." - ".$x->kelompok]=0;
    }
    $pengganti->addCustomKriteria(" dayofweek(tanggal) ","='".$inday."'");
    $pengganti->addCustomKriteria(" shift ","='".$x->kelompok."'");
    $total_ganti=$pengganti->count("");
    
    
    if($total_ganti>0){
        $pengganti_kelompok_hari[$x->hari." - ".$x->kelompok]+=$total_ganti;
        $pengganti_kelompok[$x->kelompok]+=$total_ganti;
        $pengganti_hari[$x->hari]+=$total_ganti;        
    }
}


$hasil=array();
$hasil['hari']=$hasil_hari;
$hasil['kelompok']=$hasil_kelompok;
$hasil['hari-kelompok']=$hasil_kelompok_hari;
$hasil['ganti-hari']=$pengganti_hari;
$hasil['ganti-kelompok']=$pengganti_kelompok;
$hasil['ganti-hari-kelompok']=$pengganti_kelompok_hari;
echo json_encode($hasil);

?>