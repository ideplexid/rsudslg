<?php 
/**
 * this service used for comapring and correcting
 * docter operator and docter requester
 * of operation.
 * 
 * @author Nurul Huda
 * @since 19 Jan 2017
 * @copyright goblooge@gmail.com
 * @version 1.1.1
 * 
 * */
 
global $db;
$nrm=$_POST['nrm_pasien'];
$id_operator=$_POST['id_dokter_operator'];
$nama_operator=$_POST['nama_dokter_operator'];
$_result=array();
$_result['success_operator']="0";
$_result['id_operator']="0";
$_result['nama_operator']="N/A";
$_result['success_perujuk']="0";
$_result['id_perujuk']="0";
$_result['nama_perujuk']="N/A";
$_result['keterangan']="";


/* MENGAMBIL DATA DOKTER OPERATOR */
require_once "registration/function/get_jadwal_dokter.php";
$tanggal=substr($_POST['tanggal'],0,16);
$_kode_operator=getRowJadwalDokter($db,$tanggal);
if($_kode_operator['kode']!="N/A"){
    if($_kode_operator['bagdu']=="1"){
        /* kode dokter operator ada tetapi kondisi bagi dua, sehingga tidak dapat ditentukan secara otomatis 
         * kondisi ini akan benar jika petugas melakukan set kode, akan mengikuti kode dokter yang sudah ada */
         if($id_operator=="0" || $nama_operator==""){
             /* kondisi dokter operator bagi dua, dan tidak diisi oleh petugas, maka tidak bisa dipakai */
             $_result['keterangan'].="Kode Dokter Operator Pada Tanggal ".
                                    ArrayAdapter::format("date d M Y H:i",$tanggal).
                                    " adalah ".$_kode_operator['kode'].", Karena tidak di isi dan kondisi Bagi Dua, Sehingga tidak bisa ditentukan otomatis. antara dokter ".
                                    $_kode_operator['nama_dokter_satu']." dan ".$_kode_operator['nama_dokter_dua'].". ";
         }else if($id_operator!=$_kode_operator['id_dokter_satu'] && $id_operator!=$_kode_operator['id_dokter_dua']){
             /* kondisi dokter operator di isi tetapi bagi dua, dan dokter yang dipilih ternyata bukanlah dokter operator yang ada pada kondisi bagi dua */
             $_result['keterangan'].="Kode Dokter Operator Pada Tanggal ".
                                    ArrayAdapter::format("date d M Y H:i",$tanggal).
                                    " adalah ".$_kode_operator['kode'].", kondisi Bagi Dua, antara dokter ".
                                    $_kode_operator['nama_dokter_satu']." dan ".$_kode_operator['nama_dokter_dua']." Tetapi yang dipilih 
                                    adalah ".$nama_operator." Kondisi tidak diperbolehkan. ";
         }else{
             /* kondisi valid, dokter operator sesuai dengan system pada kondisi bagi dua*/
             $_result['keterangan'].="Kode Dokter Operator Pada Tanggal ".
                                    ArrayAdapter::format("date d M Y H:i",$tanggal).
                                    " adalah ".$_kode_operator['kode'].", kondisi Bagi Dua, antara dokter ".
                                    $_kode_operator['nama_dokter_satu']." dan ".$_kode_operator['nama_dokter_dua']." dan yang dipilih adalah 
                                    ".$nama_operator." kondisi valid. ";
            $_result['success_operator']="1";
            $_result['id_operator']=$id_operator;
            $_result['nama_operator']=$nama_operator;
         }
    }else{
        /* kondisi valid */
        if($id_operator=="0" || $nama_operator==""){
            /* karena tidak di isi maka akan langsung diset sesuai dengan data */
            $_result['keterangan'].="Kode Dokter Operator Pada Tanggal ".
                                    ArrayAdapter::format("date d M Y H:i",$tanggal).
                                    " adalah ".$_kode_operator['kode'].", Karena Bukan Bagi dua dan  
                                    Data Asalnya kosong maka 
                                    dokter Operatornya di set adalah ".
                                    $_kode_operator['nama_dokter_satu'].". ";
        }else if($id_operator!=$_kode_operator['id_dokter_satu']){
            /* kondisi valid, tetapi operator asal adalah salah, sehingga dipaksa untuk jadi data operator yang benar */
            $_result['keterangan'].="Kode Dokter Operator Pada Tanggal ".
                                    ArrayAdapter::format("date d M Y H:i",$tanggal).
                                    " adalah ".$_kode_operator['kode'].", Kondisi bukan bagi dua untuk dokter ".$_kode_operator['nama_dokter_satu']." , tetapi data yang diset 
                                    petugas adalah ".$nama_operator." sehingga disesuaikan jadwal yakni ".
                                    $_kode_operator['nama_dokter_satu'].". ";
        }else{
            /* kode dokter operator ada dan bukan kondisi bagi dua, maka ini [valid] dan sesuai 
             * pada kondisi ini tidak peduli apa yang dipilih user, langsung di set sesuai jadwal */
            $_result['keterangan'].="Kode Dokter Operator Pada Tanggal ".
                                    ArrayAdapter::format("date d M Y H:i",$tanggal).
                                    " adalah ".$_kode_operator['kode'].", Bukan Bagi dua dan sudah diisikan sesuai oleh Operator ".
                                    $_kode_operator['nama_dokter_satu'].". ";
        }
        $_result['success_operator']="1";
        $_result['id_operator']=$id_operator['id_dokter_satu'];
        $_result['nama_operator']=$_kode_operator['nama_dokter_satu'];
    }
}else{
    /* kode untuk dokter operator adalah tidak ada , tidak peduli diisi atau tidak maka akan di set kosong */
    $_result['keterangan'].="Kode Dokter Operator Pada Tanggal ".ArrayAdapter::format("date d M Y H:i",$tanggal)." Tidak ada di [Master Jadwal], Tidak Bisa Ditentukan Otomatis. ";
}
/* END - MENGAMBIL DATA DOKTER OPERATOR */


/* MENGAMBIL DATA DOKTER PERUJUK */
require_once "registration/function/get_dokter_perujuk.php";
if($_result['success_operator']=="0"){
    /* kondisi tidak ada dokter operator */
    $kode=getKodeDokterPerujuk($db,$nrm,$tanggal);
    $_kode_perujuk=null;
    if($kode!="N/A"){
        $jadwal_perujuk=getDetailJadwal($db,$kode);
        if($jadwal_perujuk==null){
            /* dokter perujuk tidak ada di jadwal */
            $_result['keterangan'].="Kode Dokter Perujuk Pasien NRM [".$nrm."] Pada Tanggal ".
                                    ArrayAdapter::format("date d M Y H:i",$tanggal)." adalah [".$kode."]  Tetapi di [Master Jadwal]
                                    ini Tidak ada, Tidak Bisa Ditentukan Otomatis, Karena Jadwal Tidak Ada";
        }else{
            if($jadwal_perujuk['bagdu']*1=="1"){
                /* kondisi bagdu bisa dibantu dengan langsung menginputkan baik dokter operator maupun dokter perujuk */
                $_result['keterangan'].="Kode Perujuk Pasien NRM [".$nrm."] Pada Tanggal ".
                                        ArrayAdapter::format("date d M Y H:i",$tanggal)." adalah [".$kode."], Kondisi Bagi Dua , 
                                        Karena Operator tidak di set, Maka Dokter Satu yakni ".
                                        $jadwal_perujuk['nama_dokter_satu']." Sebagai Dokter Operator dan
                                        Dokter dua yakni ".$jadwal_perujuk['nama_dokter_dua']." Sebagai Dokter Perujuk";
                 $_result['success_operator']="1";
                 $_result['id_operator']=$_kode_operator['id_dokter_satu'];
                 $_result['nama_operator']=$_kode_operator['nama_dokter_satu'];
                 $_result['success_perujuk']="1";
                 $_result['id_perujuk']=$_kode_operator['id_dokter_dua'];
                 $_result['nama_perujuk']=$_kode_operator['nama_dokter_dua'];
            }else{
                /* kondisi bukan bagdu maka dokter perujuknya otomatis akan di set sebagai dokter pertama */
                $_result['keterangan'].="Kode Perujuk Pasien NRM [".$nrm."] Pada Tanggal ".
                                        ArrayAdapter::format("date d M Y H:i",$tanggal)." adalah [".$kode."], Kondisi Bukan Bagi Dua , 
                                        Karena Operator tidak di set, Maka Dokter Perujuknya yakni ".
                                        $jadwal_perujuk['nama_dokter_satu'];
                 $_result['success_perujuk']="1";
                 $_result['id_perujuk']=$_kode_operator['id_dokter_dua'];
                 $_result['nama_perujuk']=$_kode_operator['nama_dokter_dua'];
            }
        }
    }else{
        /* dokter perujuk tidak ada di jadwal */
        $_result['keterangan'].="Kode Dokter Perujuk Pasien NRM [".$nrm."] Pada Tanggal ".ArrayAdapter::format("date d M Y H:i",$tanggal)." Tidak ada, Mungkin Belum di isi di [Data Perujuk], Tidak Bisa Ditentukan Otomatis, karena data belum di set";
    }
}else{
    /* kondisi ada dokter operator */
    $id_dokter=$_result['id_operator'];
    $nama_dokter=$_result['nama_operator'];
    $perujuk=getDokterPerujuk($db,$nrm,$id_dokter,$nama_dokter,$tanggal);
   
    if($perujuk['id_dokter']=="0"){
        /* kode untuk dokter perujuk adalah tidak ada */
        $_result['keterangan'].="Kode Dokter Perujuk Pasien NRM [".$nrm."] Pada Tanggal ".ArrayAdapter::format("date d M Y H:i",$tanggal)." Tidak ada, Mungkin Belum di isi di [Data Perujuk], Tidak Bisa Ditentukan Otomatis. karena ID Operatornya 0";
    }else{
        $_result['success_perujuk']="1";
        $_result['id_perujuk']=$perujuk['id_dokter'];
        $_result['nama_perujuk']=$perujuk['nama_dokter'];
        $_result['keterangan'].=$perujuk['keterangan'];
    }
}
/* END - MENGAMBIL DATA DOKTER PERUJUK */
 echo json_encode($_result);


?>