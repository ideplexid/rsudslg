<?php 
	global $db;
	if(isset($_POST['command'])){
		$dbtablelayanan=new DBTable($db,'smis_rgv_layananpasien');
		$dbtablelayanan->setOrder(" smis_rgv_layananpasien.id DESC ");
		$dbtablelayanan->addCustomKriteria("selesai", "='1'");
		if(isset($_POST['uri']) && $_POST['uri']!=""){
            $dbtablelayanan->addCustomKriteria("uri", "='".$_POST['uri']."'");
        }else{
            $dbtablelayanan->addCustomKriteria("uri", "='1'");
        }
        $dbtablelayanan->addCustomKriteria(NULL, "tanggal_pulang>='".$_POST['dari']."'");
		$dbtablelayanan->addCustomKriteria(NULL, "tanggal_pulang<'".$_POST['sampai']."'");
		$dbtablelayanan->setMaximum(1);	
		if($_POST['command']=="count"){
			$query=$dbtablelayanan->getQueryCount("");
			$total=$db->get_var($query);
			echo json_encode($total);
			return;
		}
		$data=$dbtablelayanan->view("", $_POST['halaman']);
		echo json_encode($data['data']);
	}
?>