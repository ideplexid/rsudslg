<?php 

/**
 * service ini berfungsi untuk melakukan update
 * history dokter DPJP jika terjadi perubahan dokter DPJP  
 * 
 * */

$_NOREG=$_POST['noreg_pasien'];
$dbtable = new DBTable($db, "smis_rg_layananpasien");
$one=$dbtable->select($_NOREG);
$_history_dpjp=json_decode($one->history_dokter,true);

/* jika dokter dpjp sudah ada isinya tapi history adalah null ,
 * berarti di backup dulu yang lama
 * - ke history dan yang baru nanti di backup juga ke history
 * - bedanya yang lama sesuai dengan waktu di $dbtable->tanggal_inap
 * - yang baru sesuai waktu saat ini.
 * */
if($one->nama_dokter!="" && $_history_dpjp==NULL){
	$new_dpjp=array();
	$new_dpjp['id_dokter']=$one->id_dokter;
	$new_dpjp['nama_dokter']=$one->nama_dokter;
	$new_dpjp['waktu']=$one->tanggal_inap;
	$_history_dpjp=array();
	array_push($_history_dpjp,$new_dpjp);
}

if($_POST['id_dokter']!="" &&  $_POST['id_dokter']!=0 &&  $_POST['id_dokter']!="0"){
	
	if($_POST[id]*1>=0){
		$id=$_POST['id']*1;
		unset($_history_dpjp[$id]);
	}
	
	$unew_dpjp=array();
	$unew_dpjp['id_dokter']=$_POST['id_dokter'];
	$unew_dpjp['nama_dokter']=$_POST['nama_dokter'];
	$unew_dpjp['waktu']=$_POST['waktu'];
	$unew_dpjp['waktu_selesai']=$_POST['waktu_selesai'];
	$_history_dpjp[]=$unew_dpjp;
	
	function sort_by_waktu_selesai($a, $b) {
		return $a['waktu_selesai'] < $b['waktu_selesai'];
	}
	usort($_history_dpjp, 'sort_by_waktu_selesai');
	$update=array();
	$update['id_dokter']=$_history_dpjp[0]['id_dokter'];
	$update['nama_dokter']=$_history_dpjp[0]['nama_dokter'];
	$update['history_dokter']=json_encode($_history_dpjp);
	$dbtable->update($update,array("id"=>$_NOREG));
}

?>