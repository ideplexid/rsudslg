<?php 
    global $db;
    
    $qv="SELECT smis_rg_kec.* , 
        smis_rg_kabupaten.nama as kabupaten, 
        smis_rg_propinsi.nama as propinsi  
        FROM smis_rg_kec 
             LEFT JOIN smis_rg_propinsi ON smis_rg_kec.no_prop= smis_rg_propinsi.id
             LEFT JOIN smis_rg_kabupaten ON smis_rg_kec.no_kab= smis_rg_kabupaten.id";
    $qc="SELECT count(*) as total 
        FROM smis_rg_kec 
             LEFT JOIN smis_rg_propinsi ON smis_rg_kec.no_prop= smis_rg_propinsi.id
             LEFT JOIN smis_rg_kabupaten ON smis_rg_kec.no_kab= smis_rg_kabupaten.id";
    
    $dbtable=new DBTable($db,"smis_rg_kec");
    $dbtable->setPreferredQuery(true,$qv,$qc);
    $dbtable->setUseWhereforView(true);
    $dbtable->setViewForSelect(true);
    if( isset($_POST['propinsi']) && $_POST['propinsi']!=""  ){
        $dbtable->addCustomKriteria(" smis_rg_propinsi.nama "," LIKE '%".$_POST['propinsi']."' ");
    }
    if( isset($_POST['kabupaten']) && $_POST['kabupaten']!=""  ){
        $dbtable->addCustomKriteria(" smis_rg_kabupaten.nama "," LIKE '%".$_POST['kabupaten']."' ");
    }
    $service_provider = new ServiceProvider($dbtable);
    $data = $service_provider->command($_POST['command']);
    echo json_encode($data);
?>