<?php 
global $db;
$tahun=$_POST['tahun'];
$query  =   "SELECT'' AS id,
            YEAR(r.tanggal) AS tahun,
            r.jenislayanan AS ruangan,
            s.status AS status,
            SUM(if((MONTH(r.tanggal) = 1),1,0)) AS jan,
            SUM(if((MONTH(r.tanggal) = 2),1,0)) AS feb,
            SUM(if((MONTH(r.tanggal) = 3),1,0)) AS mar,
            SUM(if((MONTH(r.tanggal) = 4),1,0)) AS apr,
            SUM(if((MONTH(r.tanggal) = 5),1,0)) AS mei,
            SUM(if((MONTH(r.tanggal) = 6),1,0)) AS jun,
            SUM(if((MONTH(r.tanggal) = 7),1,0)) AS jul,
            SUM(if((MONTH(r.tanggal) = 8),1,0)) AS ags,
            SUM(if((MONTH(r.tanggal) = 9),1,0)) AS sep,
            SUM(if((MONTH(r.tanggal) = 10),1,0)) AS okt,
            SUM(if((MONTH(r.tanggal) = 11),1,0)) AS nov,
            SUM(if((MONTH(r.tanggal) = 12),1,0)) AS des,
            SUM(if((YEAR(r.tanggal) = '".$tahun."'),1,0)) AS total,
            '' AS prop
            FROM smis_rg_layananpasien AS r 
            LEFT JOIN smis_adm_prototype AS s ON r.jenislayanan = s.slug
            WHERE(r.prop != 'del')
            AND (r.jenislayanan != '')
            AND (YEAR(r.tanggal) = '".$tahun."')
            AND (r.jenislayanan NOT LIKE '[%')
            AND (r.jenislayanan NOT LIKE 'General%')
            GROUP BY YEAR(r.tanggal), r.jenislayanan
            ";
$result=$db->get_result($query);

echo json_encode($result);
?>