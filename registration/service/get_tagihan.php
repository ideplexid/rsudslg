<?php 
global $db;
if(isset($_POST['noreg_pasien'])){
    $noreg                      = $_POST['noreg_pasien'];
    $dbtable                    = new DBTable($db, "smis_rg_layananpasien");
    $data=$dbtable->select(array("id"=>$noreg));
    $response['selesai']        = "1";
    $response['exist']          = "1";
    $response['reverse']        = "0";
    $response['cara_keluar']    = "Selesai";
    
    $onedata['waktu']           = ArrayAdapter::format("date d M Y", $data->tanggal);
    $onedata['nama']            = "Karcis Pendaftaran Pasien (".strtoupper($data->jenislayanan).")";
    $onedata['biaya']           = $data->lunas=="1"?"0":$data->karcis;
    $onedata['jumlah']          = 1;
    $onedata['id']              = $data->id;
    $onedata['start']           = $data->tanggal;
    $onedata['end']             = $data->tanggal;
    $onedata['dokter']          = $data->nama_dokter;
    $onedata['debet']           = getSettings($db,"reg-map-karcis-debit-".$data->carabayar,"");;
    $onedata['kredit']          = getSettings($db,"reg-map-karcis-kredit-".$data->carabayar,"");;    
    $onedata['keterangan']      = "Karcis ".$data->jenislayanan." Senilai ".ArrayAdapter::format("only-money Rp.", $data->karcis)." ".($data->lunas=="1"?"Telah Lunas":"Belum dibayar");
    $result                     = array();
    $result[]                   = $onedata;
    
    $result2                    = array();
    if($data->uri=="1"){
        $second['waktu']        = ArrayAdapter::format("date d M Y", $data->tanggal_inap);
        $second['nama']         = "Administrasi Rawat Inap ".ArrayAdapter::format("unslug",$data->kamar_inap);
        $second['biaya']        = $data->administrasi_inap;
        $second['jumlah']       = 1;
        $second['id']           = $data->id;
        $second['start']        = $data->tanggal_inap;
        $second['end']          = $data->tanggal_pulang;
        $second['debet']        = getSettings($db,"reg-map-administrasi-debit-".$data->carabayar,"");
        $second['kredit']       = getSettings($db,"reg-map-administrasi-kredit-".$data->carabayar,"");
        $second['keterangan']   = "Administrasi Rawat Inap ".ArrayAdapter::format("unslug",$data->kamar_inap)." Senilai ".ArrayAdapter::format("only-money Rp.", $data->administrasi_inap);
        $result2[]=$second;
    }
    
    
    $unit_data                  = array("result"=>$result,"jasa_pelayanan"=>"0");
    $unit_data2                 = array("result"=>$result2,"jasa_pelayanan"=>"0");
    $ldata['registration']      = $unit_data;
    $ldata['administrasi']      = $unit_data2;
    $response['data']           = $ldata;
    echo json_encode($response);
}

?>