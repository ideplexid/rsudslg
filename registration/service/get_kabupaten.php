<?php 
    global $db;
    
    $qv="SELECT smis_rg_kabupaten.*, smis_rg_propinsi.nama as propinsi  FROM smis_rg_kabupaten 
        LEFT JOIN smis_rg_propinsi ON smis_rg_kabupaten.no_prop= smis_rg_propinsi.id";
    $qc="SELECT count(*) as total  FROM smis_rg_kabupaten 
        LEFT JOIN smis_rg_propinsi ON smis_rg_kabupaten.no_prop= smis_rg_propinsi.id";
    
    $dbtable=new DBTable($db,"smis_rg_kabupaten");
    $dbtable->setPreferredQuery(true,$qv,$qc);
    $dbtable->setUseWhereforView(true);
    $dbtable->setViewForSelect(true);
    if( isset($_POST['propinsi']) && $_POST['propinsi']!=""  ){
        $dbtable->addCustomKriteria(" smis_rg_propinsi.nama "," LIKE '%".$_POST['propinsi']."' ");
    }
    $service_provider = new ServiceProvider($dbtable);
    $data = $service_provider->command($_POST['command']);
    echo json_encode($data);
?>