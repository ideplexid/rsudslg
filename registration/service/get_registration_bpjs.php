<?php 
global $db;
require_once 'smis-libs-manajemen/ProvitSharingService.php';
$dbtable=new DBTable($db, "smis_rg_layananpasien");
$dbtable->setViewForSelect(true,false)
		->setPreferredView(true, "smis_rgv_layananpasien")
		->addCustomKriteria("carabayar"," LIKE '%bpjs%'");

if(isset($_POST['dari'])){
	$dbtable->addCustomKriteria("tanggal>=","'".$_POST['dari']."'");
}
if(isset($_POST['sampai'])){
	$dbtable->addCustomKriteria("tanggal<","'".$_POST['sampai']."'");
}
if(isset($_POST['uri']) && $_POST['uri']!=""){
	$dbtable->addCustomKriteria("uri"," ='".$_POST['uri']."'");
}
if(isset($_POST['selesai'])  && $_POST['selesai']!=""){
	$dbtable->addCustomKriteria("selesai","='".$_POST['selesai']."'");
}
if(isset($_POST['jenislayanan'])  && $_POST['jenislayanan']!=""){
	$dbtable->addCustomKriteria("jenislayanan","='".$_POST['jenislayanan']."'");
}
$service=new ServiceProvider($dbtable);
$data=$service->command($_POST['command']);
echo json_encode($data);


?>