<?php 
global $db;
$dbtable=new DBTable($db, "smis_rg_layananpasien");
//$dbtable->setOrder("tahun ASC, bulan ASC, layanan ASC ");
$dbtable->setOrder("layanan ASC ");
$dbtable->setShowAll(true);

if(isset($_POST['tahun']) && $_POST['tahun']!=""){
    if(isset($_POST['layanan']) && $_POST['layanan']!=""){
        $qv="select '' AS id,
        year(tanggal) AS tahun,
        jenislayanan AS layanan,
        sum(if((month(tanggal) = 1),1,0)) AS jan,
        sum(if((month(tanggal) = 2),1,0)) AS feb,
        sum(if((month(tanggal) = 3),1,0)) AS mar,
        sum(if((month(tanggal) = 4),1,0)) AS apr,
        sum(if((month(tanggal) = 5),1,0)) AS mei,
        sum(if((month(tanggal) = 6),1,0)) AS jun,
        sum(if((month(tanggal) = 7),1,0)) AS jul,
        sum(if((month(tanggal) = 8),1,0)) AS ags,
        sum(if((month(tanggal) = 9),1,0)) AS sep,
        sum(if((month(tanggal) = 10),1,0)) AS okt,
        sum(if((month(tanggal) = 11),1,0)) AS nov,
        sum(if((month(tanggal) = 12),1,0)) AS des,
        sum(if((year(tanggal) = '".$_POST['tahun']."'),1,0)) AS total,
        '' AS prop from smis_rg_layananpasien 
        where (prop != 'del')
        AND (jenislayanan = '".$_POST['layanan']."') 
        AND (year(tanggal) = '".$_POST['tahun']."') 
        AND (jenislayanan NOT LIKE '[%') 
        AND (jenislayanan NOT LIKE 'General%')
        group by year(tanggal), jenislayanan
        ";
        $qc="SELECT count(*) FROM smis_rg_layananpasien";
        $dbtable->setPreferredQuery(true,$qv,$qc);
    } else{
        $qv="SELECT'' AS id,
        YEAR(r.tanggal) AS tahun,
        r.jenislayanan AS layanan,
        s.status AS status,
        SUM(if((MONTH(r.tanggal) = 1),1,0)) AS jan,
        SUM(if((MONTH(r.tanggal) = 2),1,0)) AS feb,
        SUM(if((MONTH(r.tanggal) = 3),1,0)) AS mar,
        SUM(if((MONTH(r.tanggal) = 4),1,0)) AS apr,
        SUM(if((MONTH(r.tanggal) = 5),1,0)) AS mei,
        SUM(if((MONTH(r.tanggal) = 6),1,0)) AS jun,
        SUM(if((MONTH(r.tanggal) = 7),1,0)) AS jul,
        SUM(if((MONTH(r.tanggal) = 8),1,0)) AS ags,
        SUM(if((MONTH(r.tanggal) = 9),1,0)) AS sep,
        SUM(if((MONTH(r.tanggal) = 10),1,0)) AS okt,
        SUM(if((MONTH(r.tanggal) = 11),1,0)) AS nov,
        SUM(if((MONTH(r.tanggal) = 12),1,0)) AS des,
        SUM(if((YEAR(r.tanggal) = '".$_POST['tahun']."'),1,0)) AS total,
        '' AS prop
        FROM smis_rg_layananpasien AS r 
        LEFT JOIN smis_adm_prototype AS s ON r.jenislayanan = s.slug
        WHERE(r.prop != 'del')
        AND (r.jenislayanan != '')
        AND (YEAR(r.tanggal) = '".$_POST['tahun']."')
        AND (r.jenislayanan NOT LIKE '[%')
        AND (r.jenislayanan NOT LIKE 'General%')
        AND (s.status != 'deactived')
        GROUP BY YEAR(r.tanggal), r.jenislayanan
        ";
        $qc="SELECT count(*) FROM smis_rg_layananpasien";
        $dbtable->setPreferredQuery(true,$qv,$qc);
        $dbtable->addCustomKriteria(NULL, "status !='deactived'");
    }
}



$service=new ServiceProvider($dbtable);
$pack=$service->command($_POST['command']);
echo json_encode($pack);
?>