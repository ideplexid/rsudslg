<?php 
global $db;
require_once 'smis-libs-manajemen/ProvitSharingService.php';
$dbtable=new DBTable($db, "smis_rg_layananpasien");
$dbtable->setViewForSelect(true,false)
		->setPreferredView(true, "smis_rgv_layananpasien");

if(isset($_POST['nama']) && $_POST['nama']!=""){
	$dbtable->addCustomKriteria(" nama_pasien "," LIKE '%".$_POST['nama']."%'");
}

if(isset($_POST['nrm']) && $_POST['nrm']!=""){
	$dbtable->addCustomKriteria(" nrm "," = '".$_POST['nrm']."'");
}

if(isset($_POST['noreg']) && $_POST['noreg']!=""){
	$dbtable->addCustomKriteria(" id "," = '".$_POST['noreg']."'");
}

if(isset($_POST['dari']) && $_POST['dari']!=""){
	$dbtable->addCustomKriteria("tanggal>=","'".$_POST['dari']."'");
}

if(isset($_POST['sampai']) && $_POST['sampai']!=""){
	$dbtable->addCustomKriteria("tanggal<","'".$_POST['sampai']."'");
}
if(isset($_POST['uri']) && $_POST['uri']!=""){
	$dbtable->addCustomKriteria("uri"," ='".$_POST['uri']."'");
}
if(isset($_POST['selesai'])  && $_POST['selesai']!=""){
	$dbtable->addCustomKriteria("selesai","='".$_POST['selesai']."'");
}
if(isset($_POST['jenislayanan'])  && $_POST['jenislayanan']!=""){
	$dbtable->addCustomKriteria("jenislayanan","='".$_POST['jenislayanan']."'");
}

if(isset($_POST['carabayar'])  && $_POST['carabayar']!=""){
	$dbtable->addCustomKriteria("carabayar","='".$_POST['carabayar']."'");
}

if(isset($_POST['kamar_inap'])  && $_POST['kamar_inap']!=""){
	$dbtable->addCustomKriteria("kamar_inap","='".$_POST['kamar_inap']."'");
}


$service=new ServiceProvider($dbtable);
$data=$service->command($_POST['command']);
echo json_encode($data);


?>