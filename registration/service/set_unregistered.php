<?php 

global $db;
if(isset($_POST['id'])){
    $noreg['id']            = $_POST['id'];
    $data['carapulang']     = $_POST['carapulang'];
    $data['selesai']        = $_POST['selesai'];
    $data['tanggal_pulang'] = $_POST['waktu_pulang']; 
    $data['alasan_gratis']  = $_POST['alasan_gratis']; 
    $data['opsi_gratis']  = $_POST['opsi_gratis']; 
        
    if(isset($_POST['gratis']) && $_POST['gratis']!="0" && $_POST['gratis']!=0){
        $data['gratis']=$_POST['gratis'];    
    }
    
    $username=$_POST['username'];
    $opp="[1|".$username."]";
    
    //$data['tanggal_pulang']=$_POST['tanggal_pulang'];
    $data['obs']="obs|1";//OBS PASIEN PULANG DENGAN CARA BIASA
    $data['opp']="concat(opp,' ','".$opp."')";
    
    $warp=array();
    $warp['obs']=DBController::$NO_STRIP_WRAP;
    $warp['opp']=DBController::$NO_STRIP_ESCAPE_WRAP;
    $dbtable=new DBTable($db, "smis_rg_layananpasien");

    /// Kalkulasi Lama Dirawat
    $row = $dbtable->select($noreg);
    if ($row != null) {
        if ($row->uri == 0) {
            /// Kalkulasi Lama Dirawat Pasien Rawat Jalan :
            $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $row->tanggal));
            $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $data['tanggal_pulang']));
            $datediff = $tanggal_selesai - $tanggal_mulai;
            $los = ceil($datediff / (60 * 60 * 24));
            if ($los < 1)
                    $los = 1;
            $data['lama_dirawat'] = $los;
        } else {
            /// Kalkulasi Lama Dirawat Pasien Rawat Inap :
            $tanggal_mulai = strtotime(ArrayAdapter::format("date Y-m-d", $row->tanggal_inap));
            $tanggal_selesai = strtotime(ArrayAdapter::format("date Y-m-d", $data['tanggal_pulang']));
            $datediff = $tanggal_selesai - $tanggal_mulai;
            $los = ceil($datediff / (60 * 60 * 24));
            if ($los < 1)
                    $los = 1;
            $data['lama_dirawat'] = $los;
        }
    } else {
        $data['lama_dirawat'] = 0;
    }
    //. Akhir Kalkulasi Lama Dirawat
    
    $dbtable->update($data, $noreg,$warp);
    
    /* discard notify from update_total_tagihan */
    ob_start();
    require_once "registration/service/update_total_tagihan.php";
    if(getSettings($db,"reg-auto-sync-accounting","0")=="3"){
        require_once "smis-base/smis-include-service-consumer.php";
        require_once "registration/class/RegistrationResource.php";
        RegistrationResource::synchronizeToAccountingCashBase($db,$_POST['id'],"");
    }
    $result=ob_get_clean();
    
    $response=new ResponsePackage();
    $response->setStatus(ResponsePackage::$STATUS_OK);
    $response->setAlertVisible(true);
    $response->setAlertContent("Berhasil", "Pasien Berhasil Dipulangkan", ResponsePackage::$TIPE_INFO);
    $response->setContent("ok");
    echo json_encode($response->getPackage());
    return;
}

?>