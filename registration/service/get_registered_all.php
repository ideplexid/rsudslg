<?php 

/**
 * untuk semua pasien yang baik sudah pulang atau pun yang belum. */
 
global $db;
show_error();
if(isset($_POST['command'])){
	$dbtablelayanan=new DBTable($db,'smis_rgv_layananpasien');
	$dbtablelayanan->setViewForSelect(true);
	$dbtablelayanan->setOrder(" smis_rgv_layananpasien.id DESC ");
	
	if(isset($_POST['order_by'])){
		$dbtablelayanan->setOrder($_POST['order_by']);
	}
	
	if(isset($_POST['nrm'])){
		$dbtablelayanan->addCustomKriteria("nrm", "='".$_POST['nrm']."'");
	}
	
	if(isset($_POST['nama_pasien']) && $_POST['nama_pasien']!=""){
		$nama=$dbtablelayanan->escaped_string($_POST['nama_pasien']);
		$dbtablelayanan->addCustomKriteria("nama_pasien", " LIKE '%".$nama."%'");
	}
	
	if(isset($_POST['nrm_pasien']) && $_POST['nrm_pasien']!=""){
		$nrm_pasien = $_POST['nrm_pasien'] * 1;
		$dbtablelayanan->addCustomKriteria("nrm", " LIKE '%".$nrm_pasien."%'");
	}

	if(isset($_POST['umur']) && $_POST['umur']!=""){
		$dbtablelayanan->addCustomKriteria("umur", " LIKE '%".$_POST['umur']."%'");
	}	
	
	if(isset($_POST['alamat_pasien']) && $_POST['alamat_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("alamat_pasien", " LIKE '%".$_POST['alamat_pasien']."%'");
	}

	if(isset($_POST['nama_kecamatan']) && $_POST['nama_kecamatan']!=""){
		$dbtablelayanan->addCustomKriteria("nama_kecamatan", " LIKE '%".$_POST['nama_kecamatan']."%'");
	}	
	
	if(isset($_POST['jenis_pasien']) && $_POST['jenis_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("carabayar", " LIKE '%".$_POST['jenis_pasien']."%'");
	}
	
	if(isset($_POST['perusahaan_pasien']) && $_POST['perusahaan_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("n_perusahaan", " LIKE '%".$_POST['perusahaan_pasien']."%'");
	}
	
	if(isset($_POST['asuransi_pasien']) && $_POST['asuransi_pasien']!=""){
		$dbtablelayanan->addCustomKriteria("nama_asuransi", " LIKE '%".$_POST['asuransi_pasien']."%'");
	}
	
	
	if(isset($_POST['noreg_pasien']) && $_POST['noreg_pasien']!=""){
		$noreg=$_POST['noreg_pasien']*1;
		$dbtablelayanan->addCustomKriteria("id", " LIKE '%".$noreg."%'");
	}
	
	if(isset($_POST['uri']) && $_POST['uri']!=""){
		$dbtablelayanan->addCustomKriteria(" uri "," LIKE '".$_POST['uri']."' ");
	}
	
	if(isset($_POST['urji']) && $_POST['urji']!=""){
		$dbtablelayanan->addCustomKriteria(" uri "," = '".$_POST['urji']."' ");
	}
	
	if(isset($_POST['dari']) && $_POST['dari']!=""){
		$dbtablelayanan->addCustomKriteria("tanggal>=","'".$_POST['dari']."'");
	}

	if(isset($_POST['sampai']) && $_POST['sampai']!=""){
		$dbtablelayanan->addCustomKriteria("tanggal<","'".$_POST['sampai']."'");
	}
    
    if(isset($_POST['dari_pulang']) && $_POST['dari_pulang']!=""){
		$dbtablelayanan->addCustomKriteria("tanggal_pulang>=","'".$_POST['dari_pulang']."'");
	}

	if(isset($_POST['sampai_pulang']) && $_POST['sampai_pulang']!=""){
		$dbtablelayanan->addCustomKriteria("tanggal_pulang<","'".$_POST['sampai_pulang']."'");
	}
	
	if(isset($_POST['selesai'])  && $_POST['selesai']!=""){
		$dbtablelayanan->addCustomKriteria("selesai","='".$_POST['selesai']."'");
	}
	
	if(isset($_POST['jenislayanan'])  && $_POST['jenislayanan']!=""){
		$dbtablelayanan->addCustomKriteria("jenislayanan","='".$_POST['jenislayanan']."'");
	}
	
	if(isset($_POST['kamar_inap'])  && $_POST['kamar_inap']!=""){
		$dbtablelayanan->addCustomKriteria("kamar_inap","='".$_POST['kamar_inap']."'");
	}
	
	
	if(isset($_POST['carabayar'])  && $_POST['carabayar']!=""){
		$dbtablelayanan->addCustomKriteria("carabayar","='".$_POST['carabayar']."'");
	}
	

	if (isset($_POST['tanggal']) && $_POST['tanggal'] != "") {
		$dbtablelayanan->addCustomKriteria("tanggal"," LIKE '%".$_POST['tanggal']."%' ");	
	}
    
    if (isset($_POST['tanpa_tindakan']) && $_POST['tanpa_tindakan'] == "Tanpa Tindakan") {
		$dbtablelayanan->addCustomKriteria("total_tagihan"," = 'karcis' ");	
	}
    
    if (isset($_POST['filter_uang']) && $_POST['filter_uang'] != "") {
        /*dipakai untuk laporan uang masuk di kasir*/
        $dbtablelayanan->addCustomKriteria($_POST['filter_uang']," > 0 ");	
	}
    
    
	if($_POST['command']=="count"){
		$query=$dbtablelayanan->getQueryCount("");
		$total=$db->get_var($query);
		echo json_encode($total);
		return;
	}
    
    if(isset($_POST['setshowall']) && $_POST['setshowall']=="1"){
        /*untuk menampilkan seluruh data berdasarkan filter*/
        $dbtablelayanan->setShowAll(true);
    }
	
	$responder=new ServiceProvider($dbtablelayanan);
	$data=$responder->command($_POST['command']);
	echo json_encode($data);
}


?>