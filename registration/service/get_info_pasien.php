<?php
	global $db;
	
	if (isset($_POST['from']) &&
		isset($_POST['to']) &&
		isset($_POST['jenis_pasien']) &&
		isset($_POST['num']) &&
		isset($_POST['uri'])) {
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		$row = $dbtable->get_row("
			SELECT a.akunting_notify_date AS 'tanggal', a.id AS 'noreg_pasien', a.nrm AS 'nrm_pasien', b.nama AS 'nama_pasien', a.carabayar AS 'jenis_pasien', c.id AS 'id_perusahaan', c.nama AS 'perusahaan', d.id AS 'id_asuransi', d.nama AS 'asuransi'
			FROM ((smis_rg_layananpasien a LEFT JOIN smis_rg_patient b ON a.nrm = b.id) LEFT JOIN smis_rg_perusahaan c ON a.nama_perusahaan = c.id) LEFT JOIN smis_rg_asuransi d ON a.asuransi = d.id
			WHERE a.akunting_notify_date >= '" . $_POST['from'] . "' AND a.akunting_notify_date <= '" . $_POST['to'] . "' AND a.uri LIKE '" . $_POST['uri'] . "' AND a.carabayar LIKE '" . $_POST['jenis_pasien'] . "'
			LIMIT " . $_POST['num'] . ", 1
		");
		$data = array();
		$data['noreg_pasien'] = $row->noreg_pasien;
		$data['nrm_pasien'] = $row->nrm_pasien;
		$data['nama_pasien'] = $row->nama_pasien;
		$data['tanggal'] = $row->tanggal;
		$data['jenis_pasien'] = $row->jenis_pasien;
		$data['perusahaan'] = $row->perusahaan;
		$data['asuransi'] = $row->asuransi;
		$data['id_perusahaan'] = $row->id_perusahaan;
		$data['id_asuransi'] = $row->id_asuransi;
		echo json_encode($data);
	}
?>