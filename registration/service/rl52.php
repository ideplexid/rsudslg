<?php 
global $db;
$dbtable=new DBTable($db, "smis_rl52");
$dbtable->setOrder("tahun ASC, bulan ASC, layanan ASC ");
$dbtable->setShowAll(true);
if(isset($_POST['origin']) && $_POST['origin']!=""){
    $qv="select '' AS id,year(smis_rg_layananpasien.tanggal) AS tahun,
    month(smis_rg_layananpasien.tanggal) AS bulan,
    smis_rg_layananpasien.jenislayanan AS layanan,
    sum(if((smis_rg_layananpasien.barulama = 0),1,0)) AS baru,
    sum(if((smis_rg_layananpasien.barulama = 1),1,0)) AS lama,
    count(0) AS total,'' AS prop from smis_rg_layananpasien 
    where (smis_rg_layananpasien.prop != 'del') 
    AND origin ='".$_POST['origin']."'
    group by year(smis_rg_layananpasien.tanggal),
    month(smis_rg_layananpasien.tanggal)
    ";
    $qc="SELECT count(*) FROM smis_rg_layananpasien";
    $dbtable->setPreferredQuery(true,$qv,$qc);
}

$service=new ServiceProvider($dbtable);
$pack=$service->command($_POST['command']);
echo json_encode($pack);
?>