<?php
	/**
	 * service ini berfungsi untuk mengubah status rawat jalan menjadi 
	 * rawat inap sekaligus mengubah nama dokter yang merawat inapkan 
	 * adalah dokter rujukan yang bersangkutan, jika
	 * fitur tersebut di aktifkan, dan nama dokter DPJP 
	 * belum diset.
	 * pertama cek status pasien apakah URI atau URJ
	 * - kalau URI berarti sudah di set dari pendaftaran, tidak perlu dilakukan
	 * - kalau mari URJ ubah menjadi URI.
	 * 		- jika nama dokter dikirimkan juga dalam proses ini maka
	 * 			- jika dalam db nama dokter belum diset 
	 * 				-maka set dengan nama baru
	 * 			- jika sudah di set maka selesai
	 * 		- jika tidak selesai
	 * */
	global $db;	
	if (isset($_POST['noreg_pasien']) && isset($_POST['urji']) && ($_POST['urji']=="1" || $_POST['urji']=="0") ) {
		$id['id']       = $_POST['noreg_pasien']*1;
		$data['uri']    = $_POST['urji'];
		$data['hbi']    = "concat(hbi,' ','[".($_POST['urji']=="1"?"+":"-")."|2|".$_POST['tujuan']."|".date("Y-m-d h:i")."]')";
		$warp['hbi']    = DBController::$NO_STRIP_ESCAPE_WRAP;
        
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		$dbtable->update($data, $id,$warp);
		echo json_encode($data);
		/* mengecek apakah ada nama dokter dan id dokter yang dikirimkan */
		if(isset($_POST['nama_dokter']) && isset($_POST['id_dokter'])){
			$row				= $dbtable->select($id);			
			$id['id']			= $_POST['noreg_pasien']*1;
			$id['nama_dokter']	= "";	//memastikan hanya id dan nama dokter yang belum diset
			$id['id_dokter']	= "";	//memastikan hanya id dan nama dokter yang belum diset
			$datax				= array();
			if($row->kamar_inap==""){
				require_once 'smis-base/smis-include-service-consumer.php';
				require_once 'registration/class/service/KarcisService.php';
				
				/* jika memang rawat inap ada karcinsnya ubah sekalian harga karcisnya */
				$kar = new KarcisService($db, $_POST['tujuan'], $row->barulama,"uri");
				$kar->execute();
				$ct  = $kar->getContent();	

				$datax['tanggal_inap']		= $_POST['tanggal'];
				$datax['administrasi_inap'] = $ct['content'];
				$datax['kamar_inap']		= $_POST['tujuan'];
			}
			
			
			/*data history dokter DPJP*/
			$x   			  = array();
			$x['id_dokter']	  = $_POST['id_dokter'];
			$x['nama_dokter'] = $_POST['nama_dokter'];
			$x['waktu']		  = date("Y-m-d H:i:s");
            $history	      = array();
            $history[]		  = $x;
            
			/*data yang ingin di update*/
			$datax['nama_dokter']		= $_POST['nama_dokter'];
			$datax['id_dokter']			= $_POST['id_dokter'];
			$datax['history_dokter']	= json_encode($history);
			$datax['synch']				= '0';
            
			$dbtable->update($datax, $id);
            require_once "registration/class/RegistrationResource.php";
            RegistrationResource::synchronizeToCashier($db,$id['id']);
            RegistrationResource::synchronizeToAccounting($db,$id['id']);
		}
	}
?>