<?php 
global $db;
$id=$_POST['data'];

$dbtable= new DBTable($db,"smis_rg_vregister");
$x      = $dbtable->selectEventDel($id);

/*transaksi untuk pasien karcis*/
    //content untuk debet
    $debet=array();
    $debet['akun']    = getSettings($db,"reg-map-karcis-debit-".$x->carabayar,"");
    $debet['debet']   = $x->karcis;
    $debet['kredit']  = 0;
    $debet['ket']     = "Pendapatan Karcis ".$x->carabayar." pasien ".$x->nama." dengan No.Reg ".$x->id;
    $debet['code']    = "debet-karcis-".$x->id;

    //content untuk kredit
    $kredit=array();
    $kredit['akun']    = getSettings($db,"reg-map-karcis-kredit-".$x->carabayar,"");
    $kredit['debet']   = 0;
    $kredit['kredit']  = $x->karcis;
    $kredit['ket']     = "Piutang Karcis ".$x->carabayar." pasien ".$x->nama." dengan No.Reg ".$x->id;
    $kredit['code']     = "kredit-karcis-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Transaksi Pendaftaran Pasien ".$x->nama." dengan No.Reg ".$x->id;
    $header['code']         = "karcis-".$x->id;
    $header['nomor']        = "KRC-".$x->id;
    $header['debet']        = $x->karcis;
    $header['kredit']       = $x->karcis;
    $header['io']           = "1";
    
    $transaction_karcis=array();
    $transaction_karcis['header']=$header;
    $transaction_karcis['content']=$list;

/*transaksi untuk administrasi*/
    //content untuk debet
    $debet=array();
    $debet['akun']    = getSettings($db,"reg-map-administrasi-debit-".$x->carabayar,"");
    $debet['debet']   = $x->administrasi_inap;
    $debet['kredit']  = 0;
    $debet['ket']     = "Pendapatan Administrasi ".$x->carabayar." pasien ".$x->nama." dengan No.Reg ".$x->id;
    $debet['code']    = "debet-administrasi-".$x->id;

    //content untuk kredit
    $kredit=array();
    $kredit['akun']    = getSettings($db,"reg-map-administrasi-kredit-".$x->carabayar,"");
    $kredit['debet']   = 0;
    $kredit['kredit']  = $x->administrasi_inap;
    $kredit['ket']     = "Piutang Administrasi ".$x->carabayar." pasien ".$x->nama." dengan No.Reg ".$x->id;
    $kredit['code']    = "kredit-administrasi-".$x->id;
    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->tanggal_inap;
    $header['keterangan']   = "Transaksi Menginap Pasien ".$x->nama." dengan No.Reg ".$x->id;
    $header['code']         = "administrasi-".$x->id;
    $header['nomor']        = "ADM-".$x->id;
    $header['debet']        = $x->administrasi_inap;
    $header['kredit']       = $x->administrasi_inap;
    $header['io']           = "1";

    $transaction_administrasi=array();
    $transaction_administrasi['header']=$header;
    $transaction_administrasi['content']=$list;

/*transaksi keseluruhan*/
$transaction=array();
$transaction[]=$transaction_karcis;
if($x->uri==1){
    $transaction[]=$transaction_administrasi;
}

echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting']=1;
$id['id']=$x->id;
$dbtable->setName("smis_rg_layananpasien");
$dbtable->update($update,$id);

?>