<?php 
    global $db;
    
    $qv="SELECT smis_rg_kelurahan.* ,
        smis_rg_kec.nama as kecamatan, 
        smis_rg_kabupaten.nama as kabupaten, 
        smis_rg_propinsi.nama as propinsi  
        FROM smis_rg_kelurahan 
             LEFT JOIN smis_rg_propinsi ON smis_rg_kelurahan.no_prop= smis_rg_propinsi.id
             LEFT JOIN smis_rg_kabupaten ON smis_rg_kelurahan.no_kab= smis_rg_kabupaten.id
             LEFT JOIN smis_rg_kec ON smis_rg_kelurahan.no_kec= smis_rg_kec.id";
    $qc="SELECT count(*) as total
        FROM smis_rg_kelurahan 
             LEFT JOIN smis_rg_propinsi ON smis_rg_kelurahan.no_prop= smis_rg_propinsi.id
             LEFT JOIN smis_rg_kabupaten ON smis_rg_kelurahan.no_kab= smis_rg_kabupaten.id
             LEFT JOIN smis_rg_kec ON smis_rg_kelurahan.no_kec= smis_rg_kec.id";
    
    $dbtable=new DBTable($db,"smis_rg_kelurahan");
    $dbtable->setPreferredQuery(true,$qv,$qc);
    $dbtable->setUseWhereforView(true);
    $dbtable->setViewForSelect(true);
    if( isset($_POST['propinsi']) && $_POST['propinsi']!=""  ){
        $dbtable->addCustomKriteria(" smis_rg_propinsi.nama "," LIKE '%".$_POST['propinsi']."' ");
    }
    
    if( isset($_POST['kabupaten']) && $_POST['kabupaten']!=""  ){
        $dbtable->addCustomKriteria(" smis_rg_kabupaten.nama "," LIKE '%".$_POST['kabupaten']."' ");
    }
    
    if( isset($_POST['kecamatan']) && $_POST['kecamatan']!=""  ){
        $dbtable->addCustomKriteria(" smis_rg_kec.nama "," LIKE '%".$_POST['kecamatan']."' ");
    }
    $service_provider = new ServiceProvider($dbtable);
    $data = $service_provider->command($_POST['command']);
    echo json_encode($data);
?>