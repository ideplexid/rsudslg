<?php
	global $db;
	
	if (isset($_POST['from']) &&
		isset($_POST['to']) &&
		isset($_POST['jenis_pasien']) &&
		isset($_POST['uri'])) {
		$dbtable = new DBTable($db, "smis_rg_layananpasien");
		$row = $dbtable->get_row("
			SELECT COUNT(*) AS 'jumlah'
			FROM (
				SELECT *
				FROM smis_rg_layananpasien
				WHERE akunting_notify_date >= '" . $_POST['from'] . "' AND akunting_notify_date <= '" . $_POST['to'] . "' AND uri LIKE '" . $_POST['uri'] . "' AND carabayar LIKE '" . $_POST['jenis_pasien'] . "'
			) v
		");
		$data = array();
		$data['jumlah'] = $row->jumlah;
		echo json_encode($data);
	}
?>