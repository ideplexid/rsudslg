<?php 
	require_once 'smis-libs-inventory/cronjob/StockBarangED.php';
	require_once 'smis-libs-inventory/cronjob/StockObatED.php';
	global $db;
	global $notification;
    
    $barang=new CronjobStockBarangED($db, $notification, "smis_rg_stok_barang", "registration");
	$barang->initialize();
	$obat=new CronjobStockObatED($db, $notification, "smis_rg_stok_obat", "registration");
	$obat->initialize();
	
    $_LOGSAVE=array();
    $_LOGSAVE['waktu']=date("Y-m-d H:i:s");
	$AUTO_PASIEN_LEWAT_TANGGAL=getSettings($db,"reg-cronjob-activated-auto-out","0")=="1";
	if($AUTO_PASIEN_LEWAT_TANGGAL){
		$_run_time=false;
        $JAM_LAST= getSettings($db,"reg-cronjob-activated-time", "06:00:00");
        $_LOGSAVE['tgl_last']=getSettings($db,"reg-cronjob-activated-last-date", "");
		$_LOGSAVE['jam_last']=$JAM_LAST;
        $_LOGSAVE['run']="";
        $NEXT_RUN="";
        $TGL_LAST=substr($_LOGSAVE['tgl_last'],0,10);
        
        if($TGL_LAST==""){
            $_LOGSAVE['run'].=" init";
            $_run_time=true;
        }else{
            loadLibrary("smis-libs-function-time");
            /*kalau format jamnya bukan HH:ii:ss maka harus di setting jam 06:00:00*/
            if(strlen($JAM_LAST)!=8){
                $_LOGSAVE['run'].=" autoset";
                $JAM_LAST="06:00:00";
            }
            
            $NEXT_RUN=add_date($TGL_LAST,1)." ".$JAM_LAST;
            /*apakah hari ini lebih besar dari next run*/
            $_LOGSAVE['nextrun']=$NEXT_RUN;
            $result=datetime_compare($_LOGSAVE['waktu'],$NEXT_RUN);
            $_LOGSAVE['compare']=$result;
            if($result==1){
                $_LOGSAVE['run'].=" compare";
                /*jika tanggal dan waktu saat ini lebih besar dari NEXT_RUN maka lakukan cronjob */
                $_run_time=true;
            }
        }
		if($_run_time){
			$dbtablelayanan=new DBTable($db,'smis_rgv_layananpasien');
			$dbtablelayanan->setViewForSelect(true);
			$dbtablelayanan->setOrder(" smis_rgv_layananpasien.id DESC ");
			$dbtablelayanan->setShowAll(true);
			$dbtablelayanan->addCustomKriteria("uri", "='0'");
			$dbtablelayanan->addCustomKriteria("selesai", "='0'");
			$dbtablelayanan->addCustomKriteria("tanggal", "<'".date("Y-m-d")."'");
			
            global $user;
            $username=$user->getUsername();
            $opp="[16|".$username."]";
            
             /*algoritma penginapan pasien otomatis*/
            $ruangan=getSettings($db,"reg-cronjob-activated-inap-otomatis","");
            if($ruangan!=""){
                $query="UPDATE smis_rg_layananpasien 
                        set uri='1' , 
                            opri='cronjob',
                            tanggal_inap='".$_LOGSAVE['waktu']."'
                        WHERE tanggal<'".date("Y-m-d")."' 
                                AND prop!='del' 
                                AND selesai=0 
                                AND uri=0
                                AND jenislayanan='".$ruangan."' ";
                $_LOGSAVE['query']=$query;
                $db->query($query);                
            }
                
            /*algoritma pemulangan pasien otomatis*/
            if(getSettings($db,"reg-cronjob-activated-pulang-otomatis","0")=="1"){
                $cara="Dipulangkan Hidup";
                $query="UPDATE smis_rg_layananpasien 
                        set selesai='1' , 
                            obs=obs|16 ,
                            carapulang='".$cara."',
                            opp=concat(opp,' ','".$opp."'),
                            tanggal_pulang='".$_LOGSAVE['waktu']."'
                        WHERE tanggal<'".date("Y-m-d")."' 
                                AND prop!='del' 
                                AND selesai=0 
                                AND uri=0 ";
                $_LOGSAVE['query']=$query;
                $db->query($query);
            }
            
            
			setSettings($db,"reg-cronjob-activated-last-date",$_LOGSAVE['waktu']);
		}
	}
	
	if(getSettings($db,"reg-cronjob-activated-log-out","0")=="1"){
        $dbtable=new DBTable($db,"smis_rg_cronjob");
        $dbtable->insert($_LOGSAVE);
    }
	
?>