<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("registration", $user,"modul/");
	
	$policy	= new Policy("registration", $user);
	$policy	->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
    
	/*Rawat Jalan dan IGD*/
	$policy	->addPolicy("registration_patient", "registration_patient", Policy::$DEFAULT_COOKIE_CHANGE,"modul/registration_patient")
			->addPolicy("get_jenis_pasien", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_jenis_pasien")
			->addPolicy("karcis", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_karcis")
			->addPolicy("reaktivasi", "registration_patient", Policy::$DEFAULT_COOKIE_CHANGE,"snippet/reaktivasi")
			->addPolicy("help/user/rawat_jalan", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"help/user/rawat_jalan")
			->addPolicy("get_dokter_utama", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_dokter_utama")
			->addPolicy("cek_bpjs", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/cek_bpjs")
			->addPolicy("cek_vklaim", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/cek_vklaim")
			->addPolicy("rujukan_bpjs", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/rujukan_bpjs")
			->addPolicy("biaya_kartu", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/biaya_kartu")
			->addPolicy("nomor_asuransi", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/nomor_asuransi")
			->addPolicy("get_nomor_asuransi", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_nomor_asuransi")
			->addPolicy("print_sep", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_sep")
			->addPolicy("print_vklaim", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_vklaim")
			->addPolicy("fingerprint_loader", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/fingerprint_loader")
			->addPolicy("fingerprint_confirm_save", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/fingerprint_confirm_save")
			->addPolicy("cek_import_bpjs", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/cek_import_bpjs")
			->addPolicy("importer_pasien", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/importer_pasien")
			->addPolicy("print_sep_sementara", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_sep_sementara")
			->addPolicy("offline_bpjs_cek", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/offline_bpjs_cek")
			->addPolicy("batal_pasien", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/batal_pasien")
			->addPolicy("show_location", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/show_location")
			->addPolicy("reactived_antrian", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/reactived_antrian")
            ->addPolicy("search_no_bpjs", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/search_no_bpjs")
            ->addPolicy("fix_asuransi", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/fix_asuransi")
            ->addPolicy("yuk_antri", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/registration_patient/yuk_antri")
    		->addPolicy("yuk_antri_crawler", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/yuk_antri_crawler")
    		->addPolicy("yuk_antri_register", "registration_patient", Policy::$DEFAULT_COOKIE_KEEP,"snippet/yuk_antri_register");
			
	/*Rawat Inap*/
	$policy	->addPolicy("register_inap", "register_inap", Policy::$DEFAULT_COOKIE_CHANGE,"modul/register_inap")
			->addPolicy("register_menginap", "register_inap", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/register_inap/register_menginap")
			->addPolicy("empty_room", "register_inap", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/register_inap/empty_room")
			->addPolicy("help/user/rawat_inap", "register_inap", Policy::$DEFAULT_COOKIE_KEEP,"help/user/rawat_inap")
			->addPolicy("cek_bpjs_inap", "register_inap", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/register_inap/cek_bpjs_inap");
	
	/*Perubahan Pasien*/
	$policy	->addPolicy("perubahan_pasien", "perubahan_pasien", Policy::$DEFAULT_COOKIE_CHANGE,"modul/perubahan_pasien")
			->addPolicy("keterangan_perubahan_pasien", "perubahan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/keterangan_perubahan_pasien")
			->addPolicy("recheck_pasien","perubahan_pasien",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/recheck_pasien")
			->addPolicy("recheck_status","perubahan_pasien",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/recheck_status")
			->addPolicy("replace_pasien","perubahan_pasien",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/replace_pasien")
			->addPolicy("change_noreg","perubahan_pasien",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/change_noreg")
			->addPolicy("pendaftaran_ho", "perubahan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/pendaftaran_ho")
			->addPolicy("ubah_carabayar", "perubahan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/ubah_carabayar")
			->addPolicy("ubah_nama_pasien", "perubahan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/ubah_nama_pasien")
			->addPolicy("pasien_aktif", "perubahan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/pasien_aktif")
			->addPolicy("plafon_bpjs", "perubahan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/plafon_bpjs")
			->addPolicy("dokter_utama", "perubahan_pasien", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/perubahan_pasien/dokter_utama");
	
	/*Data Dasar*/
	$policy	->addPolicy("data_induk", "data_induk", Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk")
			->addPolicy("keterangan_data_induk", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/keterangan_data_induk")
			->addPolicy("bahasa", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/bahasa")
			->addPolicy("suku", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/suku")
			->addPolicy("ruang_pasien", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/ruang_pasien")
			->addPolicy("ruang_pasien_inap", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/ruang_pasien_inap")
			->addPolicy("asuransi", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/asuransi")
			->addPolicy("perujuk", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/perujuk")
			->addPolicy("jenis_pasien", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/jenis_pasien")
			->addPolicy("bonusperujuk", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/bonusperujuk")
			->addPolicy("masterperusahaan", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/masterperusahaan")
			->addPolicy("provinsi", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/provinsi")
			->addPolicy("kabupaten", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kabupaten")
			->addPolicy("kecamatan", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kecamatan")
			->addPolicy("kelurahan", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kelurahan")
			->addPolicy("kedusunan", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kedusunan")
			->addPolicy("jadwal_dokter", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/jadwal_dokter")
			->addPolicy("get_jadwal_dokter", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_jadwal_dokter")
			->addPolicy("resume_data", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/resume_data");
	
	$policy	->addPolicy("sep", "sep", Policy::$DEFAULT_COOKIE_CHANGE,"modul/sep")
			->addPolicy("refpolibpjs", "sep", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/sep/refpolibpjs")
			->addPolicy("reffaskesbpjs", "sep", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/sep/reffaskesbpjs")
			->addPolicy("history_sep", "sep", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/sep/history_sep");
			
	/*Laporan*/
	$policy	->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan")
			->addPolicy("keterangan_laporan", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/keterangan_laporan")
			->addPolicy("laporan_bonus_perperujuk", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_bonus_perperujuk")
			->addPolicy("laporan_bonus", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_bonus")
			->addPolicy("lap_uri", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_uri")
			->addPolicy("print_uri", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"snippet/print_uri")
			->addPolicy("lap_urj", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_urj")
			->addPolicy("lap_urj_undetail", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_urj_undetail")
			->addPolicy("laporan_pasien", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_pasien")
			->addPolicy("laporan_pasien_daftar", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_pasien_daftar")
			->addPolicy("ruang_pasien_inap", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/ruang_pasien_inap")
			->addPolicy("ruang_pasien", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/ruang_pasien")
			->addPolicy("laporan_karcis", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_karcis")
			->addPolicy("laporan_kartu", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_kartu")
			->addPolicy("kamar_kosong", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/kamar_kosong")
			->addPolicy("absensi_dokter", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/absensi_dokter")
			->addPolicy("lap_rujukan", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_rujukan")
			->addPolicy("lap_pasien_tdk_aktif_bpjs", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/lap_pasien_tdk_aktif_bpjs");
	
	/*admin*/
	$policy	->addPolicy("admin", "admin", Policy::$DEFAULT_COOKIE_CHANGE,"modul/admin")
			->addPolicy("lap_register", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/lap_register")
			->addPolicy("jadwal_ganti", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/jadwal_ganti")
			->addPolicy("rekap_jadwal_dokter", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/rekap_jadwal_dokter")
			->addPolicy("koreksi_dokter", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/koreksi_dokter")
			->addPolicy("cronjoblog", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/cronjoblog")
			->addPolicy("fingerprint_device", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/fingerprint_device")
			->addPolicy("help/user/lap_register", "admin", Policy::$DEFAULT_COOKIE_KEEP,"help/user/lap_register")
			->addPolicy("import_bpjs", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/import_bpjs")
			->addPolicy("duplicate_pasien", "admin", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/admin/duplicate_pasien");
	
	$policy	->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings")
			->addPolicy("selector_kabupaten","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"snippet/selector_kabupaten")
			->addPolicy("selector_kecamatan","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"snippet/selector_kecamatan")
			->addPolicy("selector_kelurahan","data_induk",Policy::$DEFAULT_COOKIE_KEEP,"snippet/selector_kelurahan")
			->addPolicy("help/user/increment_nrm","settings",Policy::$DEFAULT_COOKIE_KEEP,"help/user/increment_nrm")			
			->addPolicy("help/user/generate_token","settings",Policy::$DEFAULT_COOKIE_KEEP,"help/user/generate_token");

	$policy	->addPolicy("mapping", "mapping", Policy::$DEFAULT_COOKIE_CHANGE,"modul/mapping")
			->addPolicy("pasien_ranap", "pasien_ranap", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/ruang_pasien_inap");
	
	$policy	->combinePolicy($inventory_policy);
	$policy	->initialize();
?>
