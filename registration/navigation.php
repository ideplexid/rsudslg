<?php 
	global $NAVIGATOR;
	
	//Administrator Top Menu
	$mr = new Menu("fa fa-address-book-o");
	$mr->addProperty('title','Data Awal Pasien dan Layanan');
	$mr->addProperty('name','Pendaftaran');
	$mr->addSubMenu("Rawat Jalan dan IGD","registration","registration_patient","Pendaftaran Pasien Rawat Jalan dan IGD","fa fa-wheelchair");
	$mr->addSubMenu("Rawat Inap","registration","register_inap","Pendaftaran Pasien Rawat Inap","fa fa-h-square");
	$mr->addSubMenu("Perubahan Pasien","registration","perubahan_pasien","Berisi data-data Perubahan Pasien","fa fa-recycle");
	$mr->addSubMenu("Data Induk","registration","data_induk","Berisi Data - Data Induk Modul","fa fa-database");
	$mr->addSubMenu("Laporan","registration","laporan","Laporan","fa fa-list-alt");
	$mr->addSubMenu("Admin","registration","admin","Berisi Tool-Tool untuk admin","fa fa-user-secret");
	$mr->addSubMenu("SEP","registration","sep","Tool - Tool Khusus untuk Manajemen SEP BPJS","fa fa-user-md");
	$mr->addSubMenu("Settings","registration","settings","Settings"," fa fa-gear");
	$mr->addSubMenu("Mapping Akuntansi","registration","mapping","mapping"," fa fa-map-signs");
	$mr->addSubMenu("Pasien Rawat Inap","registration","pasien_ranap","fa fa-bed");
    require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Pendaftaran", "registration");
	$mr = $inventory_navigator->extendMenu($mr);	
	$NAVIGATOR->addMenu($mr,'registration');
?>
