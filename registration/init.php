<?php
	global $PLUGINS;
	$init ['name']              = 'registration';
	$init ['path']              = SMIS_DIR . "registration/";
	$init ['description']       = "Registration For Patient";
	$init ['require']           = "administrator";
	$init ['service']           = "get_patient, get_patients, ";
	$init ['version']           = "4.0.3";
	$init ['number']            = "103";
	$init ['type']              = "";    
	$myplugins                  = new Plugin ( $init );
	$PLUGINS [$init ['name']]   = $myplugins;