<?php 
    $tabs=new Tabulator("data_induk","data_induk",Tabulator::$LANDSCAPE_RIGHT);
    $tabs->add("keterangan_data_induk","Deskripsi","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
    $tabs->add("resume_data","Validitas Data","",Tabulator::$TYPE_HTML,"fa fa-database");
    $tabs->add("jadwal_dokter","Jadwal Dokter","",Tabulator::$TYPE_HTML,"fa fa-calendar");
    $tabs->add("bahasa","Bahasa","",Tabulator::$TYPE_HTML,"fa fa-flag");
    $tabs->add("suku","Suku","",Tabulator::$TYPE_HTML,"fa fa-users");
    $tabs->add("masterperusahaan","Perusahaan","",Tabulator::$TYPE_HTML,"fa fa-building");
    $tabs->add("asuransi","Asuransi","",Tabulator::$TYPE_HTML,"fa fa-cc-amex");
    $tabs->add("perujuk","Perujuk","",Tabulator::$TYPE_HTML,"fa fa-user-secret");
    $tabs->add("bonusperujuk","Bonus Perujuk","",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("jenis_pasien","Jenis Pasien","",Tabulator::$TYPE_HTML,"fa fa-user-plus");
    $tabs->add("provinsi","Provinsi","",Tabulator::$TYPE_HTML,"fa fa-map-o");
    $tabs->add("kabupaten","Kabupaten","",Tabulator::$TYPE_HTML,"fa fa-map-marker");
    $tabs->add("kecamatan","Kecamatan","",Tabulator::$TYPE_HTML,"fa fa-map-pin");
    $tabs->add("kelurahan","Kelurahan","",Tabulator::$TYPE_HTML,"fa fa-map-signs");
    $tabs->add("kedusunan","Kedusunan","",Tabulator::$TYPE_HTML,"fa fa-bed");

    $tabs->setPartialLoad(true, "registration", "data_induk", "data_induk",true);
    echo $tabs->getHtml();
?>