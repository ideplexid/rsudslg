<?php 
$tabs=new Tabulator("perubahan_pasien","perubahan_pasien",Tabulator::$LANDSCAPE_RIGHT);
$tabs->add("keterangan_perubahan_pasien","Deskripsi","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
$tabs->add("recheck_status","Cek Status","",Tabulator::$TYPE_HTML,"fa fa-check");
$tabs->add("recheck_pasien","Lock Status","",Tabulator::$TYPE_HTML,"fa fa-lock");
$tabs->add("replace_pasien","Pindah Berkas","",Tabulator::$TYPE_HTML,"fa fa-user-md");
$tabs->add("change_noreg","Pindah Register","",Tabulator::$TYPE_HTML,"fa fa-user-md");
$tabs->add("pendaftaran_ho","Her-OP","",Tabulator::$TYPE_HTML,"fa fa-recycle");
$tabs->add("ubah_nama_pasien","Nama","",Tabulator::$TYPE_HTML,"fa fa-file-o");
$tabs->add("ubah_carabayar","Cara Bayar","",Tabulator::$TYPE_HTML,"fa fa-credit-card");
$tabs->add("pasien_aktif","Status Pasien","",Tabulator::$TYPE_HTML,"fa fa-book");
$tabs->add("plafon_bpjs","Plafon BPJS","",Tabulator::$TYPE_HTML,"fa fa-money");
$tabs->add("dokter_utama","Dokter Utama","",Tabulator::$TYPE_HTML,"fa fa-user-md");

$tabs->setPartialLoad(true, "registration", "perubahan_pasien", "perubahan_pasien",true);
echo $tabs->getHtml();
?>