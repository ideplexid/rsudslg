<?php 
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'registration/class/RegistrationResource.php';

$smis = new SettingsBuilder ( $db, "mapping", "registration", "mapping","Mapping Akuntansi" );
$smis->setShowDescription ( true );
$smis->setTabulatorMode ( Tabulator::$LANDSCAPE_RIGHT );

$smis->addTabs ( "karcis", "Map Karcis" ,"fa fa-ticket");
if($smis->isGroup("karcis")){    
    $jenis=RegistrationResource::getJenisPembayaran();
    foreach($jenis as $x){
        $smis->addItem ( "karcis", new SettingsItem ( $db, "reg-map-karcis-debit-".$x['value'], "Kode Akun Debit untuk karcis pasien ".$x['name'],"", "text", "Kode Akun Pasien ".$x['name'] ) );
        $smis->addItem ( "karcis", new SettingsItem ( $db, "reg-map-karcis-kredit-".$x['value'], "Kode Akun Kredit untuk karcis pasien ".$x['name'],"", "text", "Kode Akun Pasien ".$x['name'] ) );
    }
}

$smis->addTabs ( "administrasi", "Map Administrasi" ,"fa fa-money");
if($smis->isGroup("administrasi")){    
    $jenis=RegistrationResource::getJenisPembayaran();
    foreach($jenis as $x){
        $smis->addItem ( "administrasi", new SettingsItem ( $db, "reg-map-administrasi-debit-".$x['value'], "Kode Akun Debit untuk biaya administrasi inap pasien ".$x['name'],"", "text", "Kode Akun Pasien ".$x['name'] ) );
        $smis->addItem ( "administrasi", new SettingsItem ( $db, "reg-map-administrasi-kredit-".$x['value'], "Kode Akun Kredit untuk biaya administrasi inap pasien ".$x['name'],"", "text", "Kode Akun Pasien ".$x['name'] ) );    
    }
}

$smis->setPartialLoad(true);
$response = $smis->init ();

?>