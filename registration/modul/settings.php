<?php

/**
 * this class used for Setting in Registration
 * @database	: - smis_rg_propinsi
 * 				  - smis_rg_kabupaten
 * 				  - smis_rg_kecamatan
 * 				  - smis_rg_kelurahan	
 * @author 		: Nurul Huda
 * @since		: 7 Sept 2016
 * @version		: 2.0.1
 * @licence		: LGPLv2
 * @copyright	: goblooge@gmail.com
 * */

global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-framework/smis/api/SettingsBuilder.php';
require_once 'registration/class/settings/RegistrasiSettingBuilder.php';
require_once "registration/class/RegistrationResource.php";

$smis = new RegistrasiSettingBuilder($db,"reg_settings","registration","settings","Settings Pendaftaran" );
$smis ->setShowDescription(true );
$smis ->setTabulatorMode(Tabulator::$LANDSCAPE_RIGHT );

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    if($_POST['super_command']=="kabupaten_settings_registration"){
        $dkadapter = new SimpleAdapter ();
        $dkadapter ->add("Kabupaten","nama" )
                   ->add("Kode","id" )
                   ->add("Propinsi","nama_propinsi" );
        $head      = array ('Kabupaten','Kode',"Propinsi" );
        $dktable   = new Table($head,"",NULL,true );
        $dktable   ->setName("kabupaten_settings_registration" )
                   ->setModel(Table::$SELECT );
        $dbtable    = new DBTable($db,"smis_rg_kabupaten");
        $qv = " SELECT smis_rg_kabupaten.* ,smis_rg_propinsi.nama as nama_propinsi 
                FROM smis_rg_kabupaten LEFT JOIN smis_rg_propinsi ON smis_rg_kabupaten.no_prop=smis_rg_propinsi.id ";
        $qc = " SELECT count(*) as total
                FROM smis_rg_kabupaten LEFT JOIN smis_rg_propinsi ON smis_rg_kabupaten.no_prop=smis_rg_propinsi.id ";
        $dbtable ->setPreferredQuery(true,$qv,$qc)
                 ->setUseWhereforView(true);
        $dokter  = new DBResponder($dbtable,$dktable,$dkadapter);
        $smis->addSuperCommandResponder("kabupaten_settings_registration",$dokter );
    }
}

if($smis->isGroupAdded("pendataan","Pendataan Pasien"," fa fa-file-o" )){
    $smis   ->setShowHelpButton(true);       
    $int    = new OptionBuilder();
    $int    ->addSingle("Stand Alone")
            ->addSingle("Integrated");
    $nrm    = new OptionBuilder();
    $nrm    ->add("Auto Increment ","0","1")
            ->add("Auto Date (YYYY-MM-NRM) ","1","0")
            ->add("Auto Partial ","2","0");    
    $case   = new OptionBuilder();
    $case   ->add("Upper Case","1","0")
            ->add("Word Case","2","0")
            ->add("","0","1");    
    $option = new OptionBuilder();
    $option ->addSingle("show")
            ->addSingle("hide");    
    $smis   ->addCurrent("registration-sistem-model","System Model ",$int->getContent(),"select","System Model Registrasi untuk persiapan keseluruhan,ketika Stand Alone berarti sendiri - sendiri,ketika Integrated berarti terintegrasi")
            ->addCurrent("reg-disabled-edit-name","Buat Nama Supaya Tidak Bisa di Edit","0","checkbox","Buat Nama Supaya Tidak Bisa di Edit")
            ->addCurrent("reg-autoload-pasien","Mengaktifkan Autoload Data Pasien","1","checkbox","Jika dicentang maka autoload akan diaktifkan ketika user membuka data pasien")
            ->addSectionCurrent("Content Input")
            ->addCurrent("reg-font-case","Default Case Penulisan",$case->getContent(),"select","Font Case dari Penulisan Nama")
            ->addCurrent("reg-show-suku","Menampilkan Suku Bangsa","0","checkbox","Menampilkan Suku Bangsa")
            ->addCurrent("reg-show-bahasa","Menampilkan Bahasa","0","checkbox","Menampilkan Pilihan Bahasa")
            ->addCurrent("reg-show-rt","Menampilkan RT","1","checkbox","Menampilkan RT")
            ->addCurrent("reg-show-rw","Menampilkan RW","1","checkbox","Menampilkan RW")
            ->addCurrent("reg-allow-empty-kelurahan","Mengizinkan Kelurahan Kosong ","0","checkbox","Dengan mencentangi ini Kelurahan Boleh Di Kosongkan")
            ->addCurrent("reg-allow-empty-kecamatan","Mengizinkan Kecamatan Kosong ","0","checkbox","Dengan mencentangi ini Kecamatan Boleh Di Kosongkan")
            ->addCurrent("reg-allow-empty-kabupaten","Mengizinkan Kabupaten Kosong ","0","checkbox","Dengan mencentangi ini Kabupaten Boleh Di Kosongkan")
            ->addCurrent("registration-kabupaten-nama","Kabupaten","","chooser-settings-kabupaten_settings_registration-Pilih Kabupaten","Pilih Kabupaten Basis")
            ->addCurrent("registration-kabupaten-id","ID Kabupaten","","text","AKan Otomatis Terisi Sendiri (Jangan Edit Manual)")
            ->addCurrent("registration-provinsi-id","ID Provinsi","","text","AKan Otomatis Terisi Sendiri (Jangan Edit Manual)")
            ->addCurrent("registration-keterangan","Tampilkan Keterangan","0","checkbox","Akan Menampilkan Keterangan Pasien")
            ->addCurrent("reg-show-email","Menampilkan E-mail","1","checkbox","Menampilkan E-Mail")
            ->addCurrent("reg-show-bbm","Menampilkan BBM","1","checkbox","Menampilkan BBM")
            ->addCurrent("reg-show-istri","Menampilkan Istri","1","checkbox","Menampilkan Istri")
            ->addCurrent("reg-show-suami","Menampilkan Suami","1","checkbox","Menampilkan Suami")
            ->addCurrent("reg-show-ayah","Menampilkan Ayah","1","checkbox","Menampilkan Ayah")
            ->addCurrent("reg-show-ibu","Menampilkan Ibu","1","checkbox","Menampilkan Ibu")
            ->addCurrent("registration-sender-model","Menampilkan Pengirim Pasien",$option->getContent(),"select","Menampilkan Sender Registrasi untuk memastikan menampilkan Pengirim atau Tidak")
            ->addCurrent("registration-payoff-model","Registration Pay Off Model",$option->getContent(),"select","Menampilkan atau Menyembunyikan Opsi Lunas")
            ->addCurrent("pendaftaran-activated-check-nrm","Aktifkan Pengecekan NRM yang lebih Tinggi","0","checkbox","Pengaturan ini akan mengaktifkan pengecekan NRM yang lebih tinggi,jika ternyata Lebih tinggi maka ditolak. jika di non aktifkan maka system tidka peduli dgn nomor RM yang dimasukan")
            ->addCurrent("registration-multiple-reg-place","Mengaktifkan Pendaftaran Multiple Ruangan","0","checkbox","Pendaftaran Bisa Multiple Secara Sekaligus")
            ->addCurrent("pendaftaran-show-her_op","Menambahkan Tombol Herr Off","0","checkbox","Pengaturan aktivasi tombol HER-OP pasien - yang berarti menghapus data pasien tersebut")
            ->addCurrent("registration-show-cashier-button","Menampilkan Link Kasir di Registrasi","0","checkbox","Setelah Registrasi Berhasil akan ditampilkan Link ke Kasir untuk Pembayaran dan Penginputan Tindakan")
            ->addCurrent("registration-show-input-bpjs","Input No BPJS di Data Induk Pasien","0","checkbox","jika fitur ini aktif maka nomor BPJS akan diinputkan di master pasien")
            ->addCurrent("registration-show-profile-number","Menampilkan Profile Number","0","checkbox","Jika Fitur ini diaktifkan akan menampilkan nomor profile")
            ->addCurrent("registration-show-family-data","Menampilkan Detail Data Keluarga","0","checkbox","Jika dipilih maka detail data keluarga akan muncul")
            ->addSectionCurrent("Fingerprint")
            ->addCurrent("reg-fingerprint","Tampilkan Penggunaan Fingerprint","0","checkbox","Aktifkan Penggunaan Fingerprint")
            ->addCurrent("reg-fingerprint-time-limit-reg","Limit Waktu Deteksi Registrasi","0","text","default 15") 
            ->addCurrent("reg-fingerprint-time-limit-ver","Limit Waktu Verivikasi","0","text","default 10") 
            ->addCurrent("reg-fingerprint-direct-bridge","Direct Access Fingerprint","","text","<i>service name direct access activation code</i> untuk <i>device name</i>,harus refer pada 'registration/direct/fingerprint_bridge.php'")
            ->addSectionCurrent("Content View & Filter")
            ->addCurrent("reg-cvf-tgl","Menampilkan Filter Tanggal","1","checkbox","menampilkan filter Tanggal ")           
            ->addCurrent("reg-cvf-jk","Menampilkan Filter Jenis Kelamin","1","checkbox","menampilkan filter Jenis Kelamin ")           
            ->addCurrent("reg-cvf-ktp","Menampilkan Filter KTP","1","checkbox","menampilkan filter KTP ")           
            ->addSectionCurrent("Aturan NRM , Replikasi dan Duplikasi Data")
            ->addCurrent("reg-format-rm","Model Incrementasi NRM",$nrm->getContent(),"select","Model Incrementasi NRM",false,"","increment_nrm")
            ->addCurrent("reg-last-number","Nomor RM Terakhir Yang Belum","0","text","Nomor Rekam Medis Trakhir Yang Belum Ter Register saat Migrasi Data")
            ->addCurrent("reg-patient-duplicate","Aktifkan Duplikasi Pasien","0","text","0. Tidak Aktif,1. Auto Duplicate,2. Manual Duplicate")
            ->addCurrent("smis-reg-nrm-partial-last","NRM Partial Terakhir","0","text","NRM Partial Terakhir untuk Synch data")
            ->addCurrent("reg-layanan-duplicate","Aktifkan Duplikasi Pasien","0","text","0. Tidak Aktif,1. Auto Duplicate,2. Manual Duplicate")
            ->addCurrent("smis-reg-noreg-partial-last","No Reg Partial Terakhir","0","text","NRM Partial Terakhir untuk Synch data")
            ->addCurrent("smis-reg-duplicate-vregister","Use Diplicate VRegister Table","0","checkbox","Penggunaan Duplicate Table untuk Laporan yang Beda Autonomous,tetapi NRM tidak dibedakan");
}

if($smis->isGroupAdded("pendaftaran","Pendaftaran Pasien","fa fa-registered")){
    $ruangan    = RegistrationResource::getURI($db);
    $ruangan[]  = array("name"=>"- - - Semua - - -","value"=>"","default"=>"1");
    $sync_akun  = new OptionBuilder();
    $sync_akun  ->add("Non Aktif","0","1")
                ->add("Aktif - Acruel Base","1")
                ->add("Aktif - Cash Base","2")
                ->add("Aktif - Cash Base End Process","3")
                ->add("Manual - Cash Base","4");

    $rmds   = new OptionBuilder();
    $rmds   ->add("Default","default","1")
            ->add("RM. 01","rm01","1");    

    $smis   ->addSectionCurrent("Rawat Jalan")
            ->addCurrent("reg-auto-jalan","Tanggal Jalan Otomatis","0","checkbox","Tanggal Inap Otomatis")
            ->addCurrent("reg-auto-sync-tarif","Otomatis Sinkronisasi Tarif","0","checkbox","Otomatis Sinkronisasi Tarif ke Kasir")
            ->addCurrent("reg-auto-sync-accounting","Otomatis Notifikasi Ke Akunting",$sync_akun->getContent(),"select","Otomatis Notifikasi ke Akunting")
            ->addCurrent("reg-edit-time-strict","Aturan Perubahan Pasien Aktif","-1","text","(x)=-1 Bebas Edit,(x)=-2 Tidak Edit,(x)>=0 tidak bisa edit jika sudah lebih (x) Menit ")
            ->addCurrent("reg-importer-pasien","Aktifkan Import Pasien Menggunakan Excel","0","checkbox","Menu Import Pasien Menggunakan Excel")
            ->addCurrent("reg-document","Tampilkan Document di Registrasi","0","checkbox","Menampilkan Dokumen Opsi Upload Document di Pendaftaran sehingga data-data seperti Surat Rujukan bisa di Scan dan di Upload")
            ->addCurrent("reg-show-plafon","Tampilkan Default Plafon","0","checkbox","jika dicentang maka default plafon akan ditampilkan sehingga bisa langsung diisi")
            ->addCurrent("reg-pasien-show-pj-data","Menampilkan Detail Data Penanggung Jawab","0","checkbox","Jika dicentang maka detail data Penanggung Jawab akan muncul")
            ->addCurrent("reg-pasien-bpjs-checker","Tampilkan Tombol Cek BPJS Offline di No. BPJS","0","checkbox","Akan Menampilkan Tombol Checking BPJS Offline jika diaktifkan ")
            ->addCurrent("reg-activate-master-card","Aktifkan Penggunaan Nomor Peserta Asuransi","0","checkbox","Mengaktifkan Penggunaan Nomor Peserta Asuransi")
            ->addCurrent("reg-lock-karcis-rj","Kunci Tarif Karcis Rawat Jalan / IGD","0","checkbox","Tarif Karcis IGD dan Rawat Jalan tidak bisa di ubah lagi")
            ->addCurrent("reg-show-skb","Menampilkan Tombol Surat Keterangan Berobat","0","checkbox","Menampilkan SKB di Bagian Registration")
            ->addCurrent("reg-show-location","Menampilkan Tombol Cek Lokasi Pasien","0","checkbox","Menampilkan Tombol untuk pengecekan Lokasi Pasien")
            ->addCurrent("reg-show-sinkronisasi","Menampilkan Tombol Sinkronisasi Data","0","checkbox","menampilkan tombol sinkronisasi data untuk perubahan pasien yang mana sudah terlanjur masuk ke ruangan")
            ->addCurrent("reg-show-cancel","Menampilkan Tombol Pembatalan Pembatalam Pelayanan","0","checkbox","jika dicentang maka pada pasien aktif dan ranap akan muncul Pembatalan Pasien yang akan Mengeluarkan Pasien dari ruangan juga")
            ->addCurrent("reg-jalan-show-profile-number","Tampilkan Nomor Profile","0","checkbox","jika di centang nomor profile di pasien aktif akan muncul")
            ->addCurrent("reg-jalan-autoload-pasien","Mengaktifkan Autoload Data Pasien Aktif","1","checkbox","Jika dicentang maka autoload akan diaktifkan ketika user membuka data pasien aktif")
            ->addSectionCurrent("Data Sosial Pasien")
            ->addCurrent("reg-autoprint-registration","Auto Print Bukti Pendaftaran","1","checkbox","Jika di centang maka bukti pendaftaran akan dicetak")
            ->addCurrent("reg-print-data-social-jsetup","Aktifkan JS Print Setup",0,"checkbox","mengaktifkan JS Print Setup pada Data Sosial")
            ->addCurrent("reg-print-data-social-css","CSS Data Sosial","","textarea","CSS Print Setup")
            ->addCurrent("reg-print-data-social-js","JS Data Sosial","","textarea","JS Print Setup")
            ->addCurrent("reg-print-data-social-format","Pilih Format Print Data Sosial",$rmds->getContent(),"select","Membuat Format Print Data Sosial")
            ->addSectionCurrent("Rawat Inap")
            ->addCurrent("reg-show-cancel-ranap","Menampilkan Tombol Pembatalan Pasien Rawat Inap tapi tidak synch ke seluruh ruangan","0","checkbox","jika dicentang maka pada pasien rawat inap akan muncul batal tetapi tiak synch ke seluruh ruangan")
            ->addCurrent("reg-auto-inap","Tanggal Inap Otomatis","0","checkbox","jika dicentang maka tanggal inap otomatis teris dan dibuat lock")
            ->addCurrent("reg-lock-karcis-ri","Kunci Tarif Karcis Rawat Inap","0","checkbox","Tarif Karcis Rawat Inap tidak bisa di ubah lagi")
            ->addCurrent("reg-inap-only","Pasien yang akan Rawat Inap Hanya Bisa Dipilih jika masuk ",$ruangan,"select","Jika dipilih selain Semua, maka pasien tersebut akan di filter yang masuk ke ruang tertentu")
            ->addCurrent("reg-inap-auto-zero","Reset Karcis Rawat Jalan","0","checkbox","jika di pilih maka karcis rawat jalan jadi 0 ketika pasien diinapkan")
            ->addCurrent("reg-inap-show-profile-number","Tampilkan Nomor Profile","0","checkbox","jika di centang nomor profile di Pasien Rawat Inap akan muncul")
            ->addCurrent("reg-inap-show-bed","Tampilkan Pilihan Bed","0","checkbox","jika di centang pilihan nomor Bed akan muncul")
            ->addSectionCurrent("Yuk Antri")
            ->addCurrent("reg-yuk-antri","Tampilkan Menu Yuk Antri","1","checkbox","Menampilkan Page Yuk Antri")
            ->addCurrent("reg-yuk-antri-sid","Sub ID","20","text","Sub ID Yuk Antri")
            ->addCurrent("reg-yuk-antri-api","Api Key","CNUGOnvjibsi6oMEiYLeZUg65U9ffE9a$13813","text","Api Key Yuk Antri")
            ->addSectionCurrent("RSU Kaliwates")
            ->addCurrent("reg-allow-jenis-pasien","Tampilakan Pilihan Jenis Pasien","1","checkbox","Menampilkan Jenis Pasien")
            ->addSectionCurrent("KMU")
            ->addCurrent("reg-activate-dokter-utama","Aktifkan Penggunaan Dokter Utama","0","checkbox","Tampilkan Dokter Utama Sesuai Jadwal")
            ->addCurrent("reg-activate-dokter-utama-each-time","Dokter Utama per Kunjungan","0","checkbox","Setiap kali kedatangan maka dokter utama baru akan disimpan")
            ->addCurrent("reg-editable-dokter-utama","Buat Editable Dokter Utama","1","checkbox","Buat supaya Dokter Utama Bisa Editable");
}

if($smis->isGroup("print_tag") || $smis->isGroup("print_gelang") || $smis->isGroup("print_kartu") || $smis->isGroup("print_label")){
    $barcode = new OptionBuilder();
    $barcode ->add("Ean 8","ean8")
             ->add("Ean 13","ean13")
             ->add("Code 11","code11")
             ->add("Code 39","code39")
             ->add("Code 93","code93")
             ->add("Code 128","code128")
             ->add("standard 2 of 5 - industrial 2 of 5","std25")
             ->add("interleaved 2 of 5","int25")
             ->add("MSI","msi")
             ->add("ASCII + extended","datamatrix");
    $output  = new OptionBuilder();
    $output  ->add("CSS","css")
             ->add("SVG","svg")
             ->add("BMP","bmp")
             ->add("Canvas","canvas");
}

if($smis->isGroupAdded("print_tag","Tag" ,"fa fa-tags")){    
    $smis   ->addCurrent("reg-show-tag","Menampilkan Tag","0","checkbox","Menampilkan Tombol untuk cetak Tag")
            ->addSectionCurrent("Content")
            ->addCurrent("reg-tag-max-char","Maksimal Karakter dari Tag","15","text","Maksimal Karakter dalam pencetakan Tag")
            ->addCurrent("reg-tag-noreg","Tampilkan No Reg","0","checkbox")
            ->addCurrent("reg-tag-nrm","Tampilkan NRM","0","checkbox")
            ->addCurrent("reg-tag-nama","Tampilkan Nama","0","checkbox")
            ->addCurrent("reg-tag-ttl","Tampilkan TTL","0","checkbox")
            ->addCurrent("reg-tag-ibu","Tampilkan Ibu","0","checkbox")
            ->addCurrent("reg-tag-alamat","Tampilkan Alamat","0","checkbox")
            ->addSectionCurrent("Barcode")
            ->addCurrent("reg-barcode-dogtag","Tampilkan Barcode Tag","0","checkbox","Barcode Tag")
            ->addCurrent("reg-barcode-dogtag-width","Barcode Width Tag","0","text","Barcode Width Tag")
            ->addCurrent("reg-barcode-dogtag-height","Barcode Height Tag","0","text","Barcode Height Tag")
            ->addCurrent("reg-barcode-dogtag-encrypt","Barcode Encrypt Tag",$barcode->getContent(),"select","Barcode Encrypt Tag")
            ->addCurrent("reg-barcode-dogtag-output","Barcode Output Tag",$output->getContent(),"select","Barcode Output Tag")
            ->addCurrent("reg-barcode-dogtag-hri","Barcode HRI Tag","0","checkbox","Barcode HRI Tag")
            ->addSectionCurrent("Setup Print")
            ->addCurrent("reg-tag-jsprintsetup","Gunakan JS Print Setup Untuk Cetak Tag","0","checkbox","Pastikan JS Print Setup telah Terinstall")
            ->addCurrent("reg-tag-css","CSS Tag","","textarea","CSS Tag")
            ->addCurrent("reg-tag-js","JS Tag","","textarea","JS Tag");
}


if($smis->isGroupAdded("no_antrian","Nomor Antrian","fa fa-list")){    
        $jenis   = RegistrationResource::getJenisPembayaran();
        $model   = new OptionBuilder();
        $model   ->add("Tanpa Harga","1","0");
        $model   ->add("Dengan Harga","0","1");

        $sebutan = new OptionBuilder();
        $smis    ->addSectionCurrent("Rawat Jalan") 
                 ->addCurrent("reg-antrian-active","Menampilkan Pilihan Cetak Nomor Antrian","0","checkbox","Menampilkan Cetak Nomor Antrian")
                 ->addCurrent("reg-antrian-model","Menampilkan Model Cetak Antrian",$model->getContent(),"select","Menampilkan Mode Cetak Antrian")
                 ->addCurrent("reg-antrian-noreg","Tampilkan No. Reg","1","checkbox","")
                 ->addCurrent("reg-antrian-tanggal","Tampilkan Tanggal","1","checkbox","")
                 ->addCurrent("reg-antrian-nrm","Tampilkan NRM","1","checkbox","")
                 ->addCurrent("reg-antrian-nama","Tampilkan Nama","1","checkbox","")
                 ->addCurrent("reg-antrian-layanan","Tampilkan Layanan","1","checkbox","")
                 ->addCurrent("reg-antrian-dokter","Tampilkan Dokter","1","checkbox","")
                 ->addCurrent("reg-antrian-css","CSS Cetak Antrian","","textarea","")
                 ->addCurrent("reg-antrian-js","JS Cetak Antrian","","textarea","");
}

if($smis->isGroupAdded("print_gelang","Gelang","fa fa-life-ring")){
    $gelang     = new OptionBuilder();
    $file       = getAllFileInDir("registration/snippet/gelang/",false,true,false);
    $gelang     ->addSingle("",1);
    foreach($file as $name){
        $gelang ->addSingle($name,0);
    }
    $smis   ->addSectionCurrent("Gelang Bayi")
            ->addCurrent("reg-show-gelang-bayi","Menampilkan Gelang Pasien Bayi","0","checkbox","Menampilkan Gelang Pasien Bayi")
            ->addCurrent("reg-show-gelang-bayi-js-setup","Gunakan JS Print Setup","","textarea","Script JS Print Setup")
            ->addCurrent("reg-font-gelang-bayi","Font Gelang Bayi","10px","text","Ukuran Font Gelang Bayi")
            ->addCurrent("reg-karakter-gelang-bayi","Jumlah Karakter Nama Gelang Bayi","20","text","Jumlah Karaketer Nama Gelang Bayi")
            ->addCurrent("reg-css-gelang-bayi","CSS Gelang Bayi","","textarea","Gunakan #b_wbpatient_print")
            ->addCurrent("reg-barcode-gelang-bayi","Barcode Gelang Bayi","0","checkbox","Barcode Gelang Bayi")
            ->addCurrent("reg-barcode-width-gelang-bayi","Barcode Width Gelang Bayi","0","text","Barcode Width Gelang Bayi")
            ->addCurrent("reg-barcode-height-gelang-bayi","Barcode Height Gelang Bayi","0","text","Barcode Height Gelang Bayi")
            ->addCurrent("reg-barcode-encrypt-gelang-bayi","Barcode Encrypt Gelang Bayi",$barcode->getContent(),"select","Barcode Encrypt Gelang Bayi")
            ->addCurrent("reg-barcode-output-gelang-bayi","Barcode Output Gelang Bayi",$output->getContent(),"select","Barcode Output Gelang Bayi")
            ->addCurrent("reg-barcode-hri-gelang-bayi","Barcode HRI Gelang Bayi","0","checkbox","Barcode HRI Gelang Bayi")
            ->addCurrent("reg-ibu-gelang-bayi","Tampilkan Ibu pada Gelang Bayi","0","checkbox","Tampilkan Ibu pada Gelang Bayi")
            ->addCurrent("reg-sebutan-gelang-bayi","Tampilkan Sebutan (Ny. / Tn. / An. / By. / Sdr.) pada Gelang Bayi","0","checkbox","Tampilkan Sebutan (Ny. / Tn. / An. / By. / Sdr.) pada Gelang Bayi")
            ->addSectionCurrent("Gelang Dewasa")
            ->addCurrent("reg-show-gelang-dewasa","Menampilkan Gelang Pasien Dewasa","0","checkbox","Menampilkan Gelang Pasien Dewasa")
            ->addCurrent("reg-show-gelang-dewasa-js-setup","Gunakan JS Print Setup","","textarea","Script JS Print Setup")
            ->addCurrent("reg-font-gelang-dewasa","Font Gelang Dewasa","13px","text","Ukuran Font Gelang Dewasa")
            ->addCurrent("reg-karakter-gelang-dewasa","Karakter Gelang Dewasa","","text","Jumlah Karaketer Nama Gelang Dewasa")
            ->addCurrent("reg-css-gelang-dewasa","CSS Gelang Dewasa","","textarea","Gunakan #a_wbpatient_print")
            ->addCurrent("reg-barcode-gelang-dewasa","Barcode Gelang Dewasa","0","checkbox","Barcode Gelang Dewasa")
            ->addCurrent("reg-barcode-width-gelang-dewasa","Barcode Width Gelang Dewasa","0","text","Barcode Width Gelang Dewasa")
            ->addCurrent("reg-barcode-height-gelang-dewasa","Barcode Height Gelang Dewasa","0","text","Barcode Height Gelang Dewasa")
            ->addCurrent("reg-barcode-encrypt-gelang-dewasa","Barcode Encrypt Gelang Dewasa",$barcode->getContent(),"select","Barcode Encrypt Gelang Dewasa")
            ->addCurrent("reg-barcode-output-gelang-dewasa","Barcode Output Gelang Dewasa",$output->getContent(),"select","Barcode Output Gelang Dewasa")
            ->addCurrent("reg-barcode-hri-gelang-dewasa","Barcode HRI Gelang Dewasa","0","checkbox","Barcode HRI Gelang Dewasa")
            ->addCurrent("reg-ibu-gelang-dewasa","Tampilkan Ibu pada Gelang Dewasa","0","checkbox","Tampilkan Ibu pada Gelang Bayi")
            ->addCurrent("reg-sebutan-gelang-dewasa","Tampilkan Sebutan (Ny. / Tn. / An. / By. / Sdr.) pada Gelang Dewasa","0","checkbox","Tampilkan Sebutan (Ny. / Tn. / An. / By. / Sdr.) pada Gelang Dewasa")    
            ->addCurrent("reg-gelang-dewasa-format","Format Gelang Dewasa",$gelang->getContent(),"select","Format Gelang Dewasa");
}

if($smis->isGroupAdded("print_kartu","Kartu"," fa fa-credit-card")){
    $smis   ->addCurrent("pendaftaran-activated-kartu","Aktifkan Kartu",0,"checkbox","mengaktifkan penggunaan kartu")
            ->addSectionCurrent("Pengaturan Barcode")            
            ->addCurrent("reg-barcode-kartu","Barcode Kartu","0","checkbox","Barcode Kartu")
            ->addCurrent("reg-barcode-kartu-width","Barcode Width Kartu","0","text","Barcode Width Kartu")
            ->addCurrent("reg-barcode-kartu-height","Barcode Height Kartu","0","text","Barcode Height Kartu")
            ->addCurrent("reg-barcode-kartu-encrypt","Barcode Encrypt Kartu",$barcode->getContent(),"select","Barcode Encrypt Kartu")
            ->addCurrent("reg-barcode-kartu-output","Barcode Output Kartu",$output->getContent(),"select","Barcode Output Kartu")
            ->addCurrent("reg-barcode-kartu-hri","Barcode HRI Kartu","0","checkbox","Barcode HRI Kartu")
            ->addSectionCurrent("Pengaturan Data")            
            ->addCurrent("reg-barcode-kartu-nrm","Tampilkan NRM","0","checkbox","Tampilkan NRM ")
            ->addCurrent("reg-barcode-kartu-no-profile","Tampilkan No. Profile","0","checkbox","Tampilkan Nomor Profile ")
            ->addCurrent("reg-barcode-kartu-alamat","Tampilkan Alamat","0","checkbox","Tampilkan Alamat ")
            ->addCurrent("reg-barcode-kartu-alamat-replace","Aktifkan Replace Alamat yang ditulis ganda dengan desa, kecamatan, kabupaten","1","checkbox","terkadang user memasukan desa, kecamatan, kabupaten dalam kolom alamat padahal mestinya cukup jl saja, jika aktifkan nama desa, kecamatan dan kabupaten akan dihilangkan dalam alamat supaya tidak ganda")
            ->addCurrent("reg-barcode-kartu-lahir","Tampilkan Tanggal Lahir","0","checkbox","Tampilkan Tanggal Lahir ")
            ->addCurrent("reg-barcode-kartu-max-alamat","Maksimum Jumlah Karakter Alamat","20","text","Maksimum Jumlah Karakter Alamat")
            ->addCurrent("reg-kartu-max-kelurahan","Maksimum Text Kelurahan","100","text","Maksimum Jumlah Karakter Untuk Kelurahan")
            ->addCurrent("reg-kartu-max-kecamatan","Maksimum Text Kecamatan","100","text","Maksimum Jumlah Karakter Untuk Kecamatan")
            ->addCurrent("reg-kartu-max-kabupaten","Maksimum Text Kabupaten","100","text","Maksimum Jumlah Karakter Untuk Kabupaten")
            ->addSectionCurrent("Pengaturan Harga")            
            ->addCurrent("reg-kartu-activate-price","Aktifkan Pencatatan Biaya Kartu",0,"checkbox","Mengaktifkan Pencatatan Biaya Kartu")
            ->addCurrent("reg-kartu-activate-price-value","Biaya Cetak Kartu",0,"money","Default Biaya Cetak Kartu")
            ->addSectionCurrent("Setup Kartu Dewasa")
            ->addCurrent("reg-kartu-css-dewasa","CSS Kartu Dewasa","","textarea","CSS Kartu Dewasa,gunakan id,kartu_pasien,nrm_pasien,nama_pasien,alamat_pasien,tgl_lahir,profile_pasien")
            ->addCurrent("reg-kartu-js-dewasa","JS Kartu Dewasa","","textarea","JS Kartu Dewasa,gunakan id,kartu_pasien,nrm_pasien,nama_pasien")
            ->addSectionCurrent("Setup Kartu Bayi")
            ->addCurrent("reg-barcode-show-title","Tampilkan Tulisan Judul - pada Kartu Bayi","0","checkbox","Tampilkan Tulisan Judul - pada Kartu Bayi")
            ->addCurrent("reg-kartu-css-bayi","CSS Kartu Bayi","","textarea","CSS Kartu Bayi,gunakan id,kartu_pasien,nrm_pasien,nama_pasien,alamat_pasien,tgl_lahir,profile_pasien")
            ->addCurrent("reg-kartu-js-bayi","JS Kartu Bayi","","textarea","JS Kartu Bayi,gunakan id,kartu_pasien,nrm_pasien,nama_pasien");
}

if($smis->isGroupAdded("print_label","Label","fa fa-bookmark")){    
    $sebutan = new OptionBuilder();
    $sebutan ->add("Tidak Tampil","0","1");
    $sebutan ->add("Tampilkan Di Depan Nama","1","0");
    $sebutan ->add("Tampilkan Di Belakang Nama","2","0");
    $smis    ->addSectionCurrent("Rawat Jalan") 
             ->addCurrent("reg-show-label","Menampilkan Label","0","checkbox","Menampilkan Print Label")
             ->addCurrent("reg-label-nama","Max Karakter Nama","20","text","Nama")
             ->addCurrent("reg-label-alamat","Max Karakter Alamat","20","text","Alamat")
             ->addCurrent("reg-label-sebutan","Tampilkan Sebutan (Tn. / Ny. / By. / Sdr. / An.) Pada Label",$sebutan->getContent(),"select","Tampilkan (Tn. / Ny. / By. / Sdr. / An.) Pada Label")
             ->addCurrent("reg-label-umur","Tampilkan Umur Pada Label Rawat Jalan","0","checkbox","Tampilkan Umur Pada")
             ->addCurrent("reg-label-kabupaten","Tampilkan Kabupaten","1","checkbox","Menampilkan Kabupaten di Label")
             ->addCurrent("reg-label-show-profile-number","Tampilkan Nomor Profile","0","checkbox","Jika Opsi ini dipilih akan muncul nomor profile")
             ->addCurrent("reg-label-repeat","Jumlah Label di Cetak","1","text","Berapa Kali Jumlah Label di Cetak ,default 1 ")
             ->addCurrent("reg-label-css","CSS Label","","textarea","CSS Label")
             ->addCurrent("reg-label-js","JS Label","","textarea","JS Label")
             ->addSectionCurrent("Barcode Rawat Jalan")
             ->addCurrent("reg-barcode-label-rj","Tampilkan Barcode","0","checkbox","Barcode Tag")
             ->addCurrent("reg-barcode-label-rj-width","Barcode Width","0","text","Barcode Width Tag")
             ->addCurrent("reg-barcode-label-rj-height","Barcode Height","0","text","Barcode Height Tag")
             ->addCurrent("reg-barcode-label-rj-encrypt","Barcode Encrypt",$barcode->getContent(),"select","Barcode Encrypt Tag")
             ->addCurrent("reg-barcode-label-rj-output","Barcode Output",$output->getContent(),"select","Barcode Output Tag")
             ->addCurrent("reg-barcode-label-rj-hri","Barcode HRI Tag","0","checkbox","Barcode HRI Tag");
    $smis    ->addSectionCurrent("Rawat Inap") 
             ->addCurrent("reg-show-inap-label","Menampilkan Label","0","checkbox","Menampilkan Print Label")
             ->addCurrent("reg-label-inap-nama","Max Karakter Nama","20","text","Nama")
             ->addCurrent("reg-label-inap-alamat","Max Karakter Alamat","20","text","Alamat")
             ->addCurrent("reg-label-inap-umur","Tampilkan Umur Pada Label","0","checkbox","Tampilkan Umur Pada Label Umur Jalan")
             ->addCurrent("reg-label-inap-sebutan","Tampilkan Sebutan (Tn. / Ny. / By. / Sdr. / An.) Pada Label Rawat",$sebutan->getContent(),"select","Tampilkan (Tn. / Ny. / By. / Sdr. / An.) Pada Label")
             ->addCurrent("reg-label-inap-kabupaten","Tampilkan Kabupaten","1","checkbox","Menampilkan Kabupaten di Label Rawat Inap")
             ->addCurrent("reg-label-show-inap-profile-number","Tampilkan Nomor Profile","0","checkbox","Jika Opsi ini dipilih akan muncul nomor profile")
             ->addCurrent("reg-label-inap-repeat","Jumlah Label di Cetak","1","text","Berapa Kali Jumlah Label di Cetak ,default 1 ")
             ->addCurrent("reg-label-inap-css","CSS Label","","textarea","CSS Label")
             ->addCurrent("reg-label-inap-js","JS Label","","textarea","JS Label")
             ->addSectionCurrent("Barcode Rawat Inap")
             ->addCurrent("reg-barcode-label-ri","Tampilkan Barcode","0","checkbox","Barcode Tag")
             ->addCurrent("reg-barcode-label-ri-width","Barcode Width","0","text","Barcode Width Tag")
             ->addCurrent("reg-barcode-label-ri-height","Barcode Height","0","text","Barcode Height Tag")
             ->addCurrent("reg-barcode-label-ri-encrypt","Barcode Encrypt",$barcode->getContent(),"select","Barcode Encrypt Tag")
             ->addCurrent("reg-barcode-label-ri-output","Barcode Output",$output->getContent(),"select","Barcode Output Tag")
             ->addCurrent("reg-barcode-label-ri-hri","Barcode HRI Tag","0","checkbox","Barcode HRI Tag");
}

/*Special Case for Cronjob*/
if($smis->isGroupAdded("cronjob","Cronjob"," fa fa-cogs")){    
    $ruangan    = RegistrationResource::getURJ($db);
    $ruangan[]  = array("name"=>" - - - - Non Aktif - - - - - ","value"=>"","default"=>"1");
    
    $smis   ->addCurrent("reg-cronjob-activated-auto-out","Aktivasi Keluar Otomatis","0","checkbox","memulangkan Pasien Lewat Tanggal Secara Otomatis") 
            ->addCurrent("reg-cronjob-activated-time","Waktu Pasien Pulang","06:00:00","text","Waktu Cronjob Dilakukan,Format <strong>HH:ii:ss</strong> example <strong>06:00:00</strong>")
            ->addCurrent("reg-cronjob-activated-last-date","Tanggal Aktif Terakhir","","date","Tanggal Aktif Terakhir")
            ->addCurrent("reg-cronjob-activated-log-out","Aktifkan Loging Cronjob Pasien Pulang","0","checkbox","Jika di Centang maka Log cronjob untuk pasien pulang akan disimpan")
            ->addCurrent("reg-cronjob-activated-inap-otomatis","Aktifkan Inap Otomatis Untuk Pasien Ruangan Tertentu",$ruangan,"select","isi dengan slug ruangan maka otomatis akan mengikut slug,jika kosong maka tidak aktif")
            ->addCurrent("reg-cronjob-activated-pulang-otomatis","Aktifkan Pulang Otomatis Untuk Pasien Rawat Jalan","0","checkbox","Pengaktifan Otomatis untuk pasien rawat jalan");
}

/*Special Case for BPJS*/
/*Special Case for BPJS*/
if($smis->isGroupAdded("bpjs","Bridging BPJS"," fa fa-credit-card")){
        $smis   ->setShowHelpButton(true);
    $smis   //->addSectionCurrent("Bridging SEP") 
            //->addCurrent("reg-activate-import-bpjs","Mengaktifkan Melihat Data Import Excel BPJS","0","checkbox","Jika di centang maka data hasil import excel BPJS akan ditampilkan") 
            //->addCurrent("reg-bpjs-activate-bpjs-reg-urj","Mengaktifkan Pendaftaran BPJS di Pendaftaran Rawat Jalan","0","checkbox","Jika di aktifkan maka pendaftaran Rawat Jalan dan iGD akan ada menu BPJS") 
            //->addCurrent("reg-bpjs-activate-bpjs-reg-uri","Mengaktifkan Pendaftaran BPJS di Pendaftaran Rawat Inap","0","checkbox","Jika di aktifkan maka pendaftaran Rawat Inap akan ada menu BPJS") 
            //->addCurrent("reg-bpjs-sep-api-cons-id","Consumer ID RS dari BPJS","24619","text","consumer ID adalah kode yang didapat sebuah rumah sakit dari BPJS dengan mengajukan permohonan,ID percobaan untuk latihan adalah <strong>24619</strong> ") 
            //->addCurrent("reg-bpjs-sep-api-cons-secret","Consumer Secret RS dari BPJS","6vOEA10FF6","text","consumer Secret adalah kode/password yang didapat sebuah rumah sakit dari BPJS dengan mengajukan permohonan,Kode percobaan untuk latihan adalah <strong>6vOEA10FF6</strong> ") 
            //->addCurrent("reg-bpjs-sep-api-ppk-pelayanan","Kode PPK Pelayanan Rumah Sakit di BPJS","1334R001","text","kode PPK Pelayanan adalah kode yang dimiliki RS dari BBPJ yang menunjukan nomor PPK Pelayanan dia. misalnya <strong>1334R001</strong> ") 
            //->addCurrent("reg-bpjs-sep-api-url-base","URL Base Service SEP API","http://dvlp.bpjs-kesehatan.go.id:8081/devwslokalrest/","text","URL Base adalah alamat IP dari Server BPJS yang dipasang di Lokal Rumah Sakit oleh Petugas BPJS,untuk latihan bisa menggunakan <strong>http://dvlp.bpjs-kesehatan.go.id:8081/devwslokalrest/</strong>") 
            //->addCurrent("reg-bpjs-sep-api-port","Port Number URL Base Service API ","8081","text","nomor port service BPJS api yang ada pada SEP ,untuk latihan biasanya adalah <strong>8081</strong>") 
            //->addCurrent("reg-bpjs-sep-api-user","Default User SEP yang dipakai ","smis","text","nama user default yang dipakai untuk mengakses API BPJS ,default adalah <strong></strong>") 
            //->addCurrent("reg-bpjs-print-logo","Logo System","0","checkbox","Tampilkan Logo System dalam cetak BPJS") 
            //->addCurrent("reg-bpjs-print-css","CSS","<style type='text/css'></style>","textarea","gunakan #logo_bpjs,untuk logo,#logo_system untuk logo system,#title_bpjs untuk title,dan #sub_title_bpjs untuk sub titlenya ")
            ->addSectionCurrent("Bridging VClaim") 
            ->addCurrent("reg-bpjs-activate-vclaim-reg-urj","Mengaktifkan Pendaftaran BPJS VClaim di Pendaftaran Rawat Jalan","0","checkbox","Jika di aktifkan maka pendaftaran Rawat Jalan dan iGD akan ada menu BPJS") 
            //->addCurrent("reg-bpjs-activate-vclaim-reg-uri","Mengaktifkan Pendaftaran BPJS VClaim di Pendaftaran Rawat Inap","0","checkbox","Jika di aktifkan maka pendaftaran Rawat Inap akan ada menu BPJS") 
            //->addCurrent("reg-bpjs-activate-vclaim-reg-rujukan","Mengaktifkan Pendaftaran Rujukan BPJS VClaim di Pendaftaran","0","checkbox","Jika di aktifkan maka pendaftaran Rujukan BPJS akan ditampilkan ") 
            ->addCurrent("reg-bpjs-vclaim-api-cons-id","Consumer ID RS dari BPJS VClaim","24619","text","consumer ID adalah kode yang didapat sebuah rumah sakit dari BPJS dengan mengajukan permohonan,ID percobaan untuk latihan adalah <strong>24508</strong> ") 
            ->addCurrent("reg-bpjs-vclaim-api-cons-secret","Consumer Secret RS dari BPJS VClaim","6vOEA10FF6","text","consumer Secret adalah kode/password yang didapat sebuah rumah sakit dari BPJS dengan mengajukan permohonan,Kode percobaan untuk latihan adalah <strong>8nR649539E</strong> ") 
            ->addCurrent("reg-bpjs-vclaim-user-key","User Key","","text","User Key",false,"","generate_token") 
            ->addCurrent("reg-bpjs-vclaim-api-ppk-pelayanan","Kode PPK Pelayanan Rumah Sakit di BPJS VClaim","1334R001","text","kode PPK Pelayanan adalah kode yang dimiliki RS dari BBPJ yang menunjukan nomor PPK Pelayanan dia. misalnya <strong>0205S100</strong> ") 
            ->addCurrent("reg-bpjs-vclaim-api-url-base","URL Base Service SEP API VClaim","http://dvlp.bpjs-kesehatan.go.id:8081/vclaim-rest/","text","URL Base adalah alamat IP dari Server BPJS yang dipasang di Lokal Rumah Sakit oleh Petugas BPJS,untuk latihan bisa menggunakan <strong>http://dvlp.bpjs-kesehatan.go.id:8081/vclaim-rest/</strong>") 
            ->addCurrent("reg-bpjs-vclaim-api-port","Port Number URL Base Service API VClaim","8081","text","nomor port service BPJS api yang ada pada SEP ,untuk latihan biasanya adalah <strong>8081</strong>") 
            ->addCurrent("reg-bpjs-vclaim-api-user","Default User SEP yang dipakai VClaim","smis","text","nama user default yang dipakai untuk mengakses API BPJS ,default adalah <strong>0205S100</strong>")
            ->addCurrent("reg-bpjs-vclaim-logo","Logo BPJS","smis","file-single-image","Lebar 900px dan Tinggi 223px")
            
            ; 
            //->addCurrent("reg-bpjs-vclaim-print-logo","Logo System VClaim","0","checkbox","Tampilkan Logo System dalam cetak BPJS")
            //->addCurrent("reg-bpjs-vclaim-print-css","CSS VClaim","<style type='text/css'></style>","textarea","gunakan #logo_bpjs,untuk logo,#logo_system untuk logo system,#title_bpjs untuk title,dan #sub_title_bpjs untuk sub titlenya ")
            //->addCurrent("reg-bpjs-vclaim-rujukan-print-css","CSS Cetak Rujukan","<style type='text/css'></style>","textarea","gunakan #rujukan_vclaim_table ");

}

$smis->setPartialLoad(true);
$response = $smis->init ();
?>
