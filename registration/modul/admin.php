<?php 
$tabs  = new Tabulator("admin","admin",Tabulator::$LANDSCAPE_RIGHT);
$tabs ->add("lap_register","Riwayat","",Tabulator::$TYPE_HTML,"fa fa-reorder")
      ->add("jadwal_ganti","Jadwal Ganti","",Tabulator::$TYPE_HTML,"fa fa-calendar")
      ->add("rekap_jadwal_dokter","Rekap Jadwal Dokter","",Tabulator::$TYPE_HTML,"fa fa-calendar-check-o")
      ->add("koreksi_dokter","Koreksi Dokter","",Tabulator::$TYPE_HTML,"fa fa-refresh")
      ->add("cronjoblog","Log Cronjob Pasien","",Tabulator::$TYPE_HTML,"fa fa-steam-square")
      ->add("import_bpjs","Import BPJS","",Tabulator::$TYPE_HTML,"fa fa-file-excel-o")
      ->add("fingerprint_device","Device Fingerprint","",Tabulator::$TYPE_HTML,"fa fa-thumbs-up")
      ->add("duplicate_pasien","Duplicate Pasien","",Tabulator::$TYPE_HTML,"fa fa-copy");

$tabs->setPartialLoad(true, "registration", "admin", "admin",true);
echo $tabs->getHtml();
?>