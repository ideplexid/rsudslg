<?php
	require_once 'registration/class/RegistrationResource.php';
	require_once("smis-base/smis-include-service-consumer.php");
	loadLibrary("smis-libs-function-medical");	
	if(isset($_POST['super_command']) && $_POST['super_command']!=""){
		$command=$_POST['super_command'];
		if($command=="kabupaten") {
			require_once 'registration/snippet/selector_kabupaten.php';
		}else if($command=="kecamatan") {
			require_once 'registration/snippet/selector_kecamatan.php';
		}else if($command=="kelurahan") {
			require_once 'registration/snippet/selector_kelurahan.php';
		}else if($command=="kedusunan") {
			require_once 'registration/snippet/selector_kedusunan.php';
		}else if($command=="daftar_pasien"){
			require_once 'registration/resource/php/registration_patient/reg_layanan.php';
		}else if($command=="perujuk"){
			require_once 'registration/snippet/perujuk.php';
		}else if($command=="besarbonus"){
			require_once 'registration/snippet/besarbonus.php';
		}else if($command=="skb"){
			require_once 'registration/snippet/skb.php';
		}else if($command=="kartu"){
			require_once 'registration/snippet/kartu.php';
		}else if($command=="kartu_bayi"){
			require_once 'registration/snippet/kartu_bayi.php';
		}else if($command=="cetak_antrian"){
			require_once 'registration/snippet/cetak_antrian.php';
		}
		return;
	}
	if(isset($_POST['command']) && $_POST['command']!=""){
		require_once 'registration/resource/php/registration_patient/reg_patient.php';
		return;
	}
	$tabs = new Tabulator ('mytabs',"tabs");
	$tabs->add("registration_patient_tab","Registrasi Pasien","registration/resource/php/registration_patient/reg_patient.php",Tabulator::$TYPE_INCLUDE,"fa fa-ticket");
	$tabs->add("daftar_patient_tab","Pasien Aktif","registration/resource/php/registration_patient/reg_layanan.php",Tabulator::$TYPE_INCLUDE,"fa fa-file-o");
	if(getSettings($db,"reg-activate-import-bpjs","0")=="1"){
        $tabs->add("cek_import_bpjs","Cek Import BPJS","registration/resource/php/registration_patient/cek_import_bpjs.php",Tabulator::$TYPE_INCLUDE,"fa fa-file-excel-o");
	}
    if(getSettings($db,"reg-activate-master-card","0")=="1"){
        $tabs->add("master_card","Nomor Peserta Asuransi","registration/resource/php/registration_patient/nomor_asuransi.php",Tabulator::$TYPE_INCLUDE,"fa fa-id-card");
    }
	if(getSettings($db,"reg-bpjs-activate-bpjs-reg-urj","0")=="1"){
        $tabs->add("cek_bpjs","BPJS SEP","registration/resource/php/registration_patient/cek_bpjs.php",Tabulator::$TYPE_INCLUDE,"fa fa-credit-card");
    }
    if(getSettings($db,"reg-bpjs-activate-vclaim-reg-urj","0")=="1"){
        $tabs->add("cek_vklaim","BPJS VClaim","registration/resource/php/registration_patient/cek_vklaim.php",Tabulator::$TYPE_INCLUDE,"fa fa-credit-card");
	}
	if(getSettings($db,"reg-bpjs-activate-vclaim-reg-rujukan","0")=="1"){
        $tabs->add("rujukan_bpjs","Rujukan VClaim","registration/resource/php/registration_patient/rujukan_bpjs.php",Tabulator::$TYPE_INCLUDE,"fa fa-forward");
    }
	if(getSettings($db,"reg-kartu-activate-price","0")=="1"){
        $tabs->add("biaya_kartu","Biaya Kartu","registration/resource/php/registration_patient/biaya_kartu.php",Tabulator::$TYPE_INCLUDE,"fa fa-credit-card-alt");
    }
    if(getSettings($db,"reg-importer-pasien","0")=="1"){
        $tabs->add("importer_pasien","Importer Pasien","registration/resource/php/registration_patient/importer_pasien.php",Tabulator::$TYPE_INCLUDE,"fa fa-file-excel-o");
    }

    if(getSettings($db,"reg-yuk-antri","0")=="1"){
        $tabs->add("yuk_antri","Yuk Antri","registration/resource/php/registration_patient/yuk_antri.php",Tabulator::$TYPE_INCLUDE,"fa fa-users");
    }
	
	echo $tabs->getHtml();	
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addCSS("framework/bootstrap/css/bootstrap-select.css");
	echo addCSS("registration/resource/css/registration.css",false);	
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/jquery/jquery-barcode.min.js");
	echo addJS("base-js/smis-base-barcode.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("framework/bootstrap/js/bootstrap-select.js");
	echo addJS("registration/resource/js/registration_init.js",false);
	echo addJS("registration/resource/js/daftar_patient.js",false);	
	echo addCSS("registration/resource/css/cetak_antrian.css",false);
	echo addJS("smis-base-js/smis-base-shortcut.js",false);
	echo "<div id='destination'></div>";
