<?php 
    $tabs=new Tabulator("laporan","laporan",Tabulator::$LANDSCAPE_RIGHT);
    $tabs->add("keterangan_laporan","Deskripsi","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
    $tabs->add("laporan_bonus","Bonus Pasien","",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("laporan_bonus_perperujuk","Bonus Perujuk","",Tabulator::$TYPE_HTML,"fa fa-user-secret");
    $tabs->add("kamar_kosong","Kamar Kosong","",Tabulator::$TYPE_HTML,"fa fa-bed");
    $tabs->add("lap_uri","R.Inap","",Tabulator::$TYPE_HTML,"fa fa-bed");
    $tabs->add("lap_urj","R.Jalan","",Tabulator::$TYPE_HTML,"fa fa-heart-o");
    $tabs->add("lap_urj_undetail","R.Jalan Sederhana","",Tabulator::$TYPE_HTML,"fa fa-heart-o");
    $tabs->add("ruang_pasien_inap","Posisi Bed Pasien ","",Tabulator::$TYPE_HTML,"fa fa-bed");
    $tabs->add("ruang_pasien","Posisi Pasien","",Tabulator::$TYPE_HTML,"fa fa-male");
    $tabs->add("laporan_pasien","Data Pasien","",Tabulator::$TYPE_HTML,"fa fa-users");
    $tabs->add("laporan_pasien_daftar","Kunjungan Pasien","",Tabulator::$TYPE_HTML,"fa fa-medkit");
    $tabs->add("laporan_karcis","Karcis","",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("laporan_kartu","Kartu","",Tabulator::$TYPE_HTML,"fa fa-credit-card-alt");
    $tabs->add("absensi_dokter","Absensi Dokter","",Tabulator::$TYPE_HTML,"fa fa-user-md");
    $tabs->add("lap_rujukan","Laporan Rujukan","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
    $tabs->add("lap_pasien_tdk_aktif_bpjs","Laporan Pasien Tidak Aktif BPJS","",Tabulator::$TYPE_HTML,"fa fa-file-text-o");
    $tabs->setPartialLoad(true, "registration", "laporan", "laporan",true);
    echo $tabs->getHtml();
?>