<?php 
    $tabs   = new Tabulator("sep","sep",Tabulator::$LANDSCAPE_RIGHT);
    $tabs   ->add("refpolibpjs","Map Poli BPJS","",Tabulator::$TYPE_HTML,"fa fa-list")
            ->add("reffaskesbpjs","Map Faskes BPJS","",Tabulator::$TYPE_HTML,"fa fa-list")
            ->add("history_sep","History SEP","",Tabulator::$TYPE_HTML,"fa fa-id-badge")
            ->add("monitoring_sep","Monitoring SEP","",Tabulator::$TYPE_HTML,"fa fa-id-desktop")
            ->setPartialLoad(true, "registration", "sep", "sep",true);
    echo $tabs->getHtml();
?>