<?php 

$tab=new Tabulator("register_inap", "register_inap");
$tab->add("register_menginap", "Register Rawat Inap", "", Tabulator::$TYPE_HTML,"fa fa-list-alt","load_register_menginap()");
if(getSettings($db,"reg-bpjs-activate-bpjs-reg-uri","0")=="1"){
    $tab->add("cek_bpjs_inap", "Cek BPJS Inap", "", Tabulator::$TYPE_HTML,"fa fa-credit-card","cek_bpjs_inap_load_only()");
}
$tab->add("empty_room", "Kamar Kosong", "", Tabulator::$TYPE_HTML,"fa fa-bed","load_empty_room()");
echo $tab->getHtml();
echo addJS("registration/resource/js/register_inap.js",false);
?>


