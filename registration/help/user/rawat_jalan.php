<div class="alert alert-block alert-inverse">
	<h4>Tips</h4>
	<ul>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>B</kbd> : Menampilkan Formulir Pasien Baru</li>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>S</kbd> : Menyimpan Pasien Baru Saja Atau Menyimpan Registrasi Pasien (Tergantung Konteks) </li>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>R</kbd> : Menyimpan Pasien Baru Saja dan Sekaligus Menampilkan Formulir Layanan</li>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>C</kbd> : Meletakan Focus pada Tab Pencarian Baik Pada Daftar Pasien maupun Pasien Aktif (Tergantung Konteks) </li>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>M</kbd> : Berpindah Pada Tab Registrasi Pasien atau Pasien Aktif (Tegantung Konteks)</li>
	</ul>
</div>