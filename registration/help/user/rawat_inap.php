<div class="alert alert-block alert-inverse">
	<h4>Tips</h4>
	<ul>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>B</kbd> : Menampilkan Formulir Pendaftaran Rawat Inap Baru</li>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>S</kbd> : Menyimpan Pendaftaran Rawat Inap Baru </li>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>C</kbd> : Meletakan Focus pada Tab Pencarian Pasien Rawat Inap </li>
	<li>Kombinasi Tombol <kbd>Ctrl</kbd> + <kbd>M</kbd> : Berpindah Pada Tab Registrasi Kamar Inap Atau Kamar Kosong (Tegantung Konteks)</li>
	</ul>
</div>