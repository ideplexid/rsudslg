<?php 
 $consmer_id = getSettings($db,"reg-bpjs-vclaim-api-cons-id","");
 $consumer_secret = getSettings($db,"reg-bpjs-vclaim-api-cons-secret","");
 $user_key = getSettings($db,"reg-bpjs-vclaim-user-key","");

$cur_zone = date_default_timezone_get();
date_default_timezone_set('UTC');
$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
$signature = hash_hmac('sha256', $consmer_id."&".$tStamp, $consumer_secret, true);
$encodedSignature = base64_encode($signature);
date_default_timezone_set($cur_zone);

echo "<pre>";
        echo "accept: application/json\n";
        echo "X-cons-id: ". $consmer_id."\n";
        echo "X-signature: ".$encodedSignature."\n";
        echo "X-timestamp: ".$tStamp."\n";
        echo "user_key: ".$user_key;
echo "</pre>";
