ada 3 jenis aturan dalam incremental NRM :
<ul>
<li><strong>Auto Increment </strong>: auto increment NRM pasien bisa yang mana setiap pasien baru nomornya selalu urutan dari nomor pasien yang terakhir sebelumnya</li>
<li><strong>Auto Date      </strong>: merupakan aturan pemberian Nomor Rekam Medis yang didasarkan pada tanggal yakni (YYYY-MM-DD-NRM) sehingga setiap bulan akan reset ke 1</li>
<li><strong>Auto Partial   </strong>: Merupakan Aturan Pemberian Nomor Rekam Medis yang mana di desain untuk mengikuti pembagian NRM tertentu, setiap pasien yang baru yang tidak punya nomor RM maka autoincrement mengikuti nomor terakhir NRM yang diinputkan di TextBox (NRM Partial Terakhir)</li> 
</ul>
