<ul>
<li><strong>OPRJ</strong> adalah Operator yang merawat jalankan pasien melalui menu <i>Pendaftaran -> Rawat Jalan dan IGD</i></li>
<li><strong>OPRI</strong> adalah Operator yang merawat inapkan pasien melalui menu <i>Pendaftaran -> Rawat Inap</i></li>
<li><strong>OPPP</strong> adalah Operator yang memulangkan melalui system </li>
<li><strong>OBS</strong> adalah Adalah Kode dari Cara Pasien Pulang (Otomasi Bentuk Selesai) </li>
<li><strong>OBS 1 </strong> Berarti pasien dipulangkan melalui <i>Kasir -> Pembayaran Pasien</i> (Kondisi Sempurna) </li>
<li><strong>OBS 2 </strong> Berarti pasien dipulangkan melalui <i>Kasir -> Data Induk -> Pasien Lewat Tanggal</i> (Kondisi User yang memulangkan) hanya berdampak pada pasien rawat jalan yang sudah 24 jam tidak dikeluarkan</li>
<li><strong>OBS 4 </strong> Berarti pasien dipulangkan melalui Otomasi Pasien Pulang Massal pada menu <i>Pendaftaran -> Perubahan Pasien -> Lock Status</i> (Kondisi User yang memulangkan) Berdampak pada semua pasien yang rawat inap yang sudah di keluarkan oleh ruangan tapi masih aktif di pendaftaran</li>
<li><strong>OBS 8 </strong> Berarti pasien dipulangkan melalui oleh Admin melalui Menu <i>Pendaftaran -> Admin -> Riwayat</i> (Kondisi Admin yang mengeluarkan dengan menekan tombol selesai) </li>
<li><strong>OBS 16 </strong> Berarti pasien dipulangkan Otomatis melalui Cronjob oleh system (Kondisi System Pulang Otomatis) dilakukan oleh system melalui cronjob jika di aktifkan, hanya berpengaruh pada pasien rawat jalan yang sudah 24 jam tidak dikeluarkan oleh kasir</li>
<li><strong>OBS</strong> menggunakan mode penjumlahan untuk mencatat bagaimana pasien dipulangkan misal pasien pernah dipulangkan dengan kode 1 dan dan 2 akan menghasilkan (1+2) = 3 pada OBS </li>
<li><strong>HBI</strong> HBI adalah History Berubah Inap, adalah history perubahan pasien dari rawat jalan ke rawat inap </li>
<li><strong>HBI + </strong> adalah hisotry pasien di rawat inapkan </li>
<li><strong>HBI - </strong> adalah hisotry pasien di rawat jalankan </li>
<li><strong>HBI 1 </strong> adalah hisotry pasien yang diubah melalui menu Pendaftaran Rawat Inap yang mana kemudian dibatalkan </li>
<li><strong>HBI 2 </strong> adalah hisotry pasien yang diubah melalui menu Push Antrian di ruangan, akibat trigger perubahan rawat pasien di aktifkan </li>
<li><strong>HBI 2 </strong> adalah hisotry pasien yang diubah melalui menu Riwayat pasien oleh petugas yang memiliki hak pendaftaran -> admin </li>
</ul>