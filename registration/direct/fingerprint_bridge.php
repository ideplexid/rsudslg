<?php 

if(isset($_GET['bridge']) && $_GET['bridge']!=""){
    $bridge=$_GET['bridge'];
    switch($bridge){
        case "get_activation_code"              : require_once "registration/snippet/get_activation_code.php";              break;
        case "get_reg_patient_fingerprint_url"  : require_once "registration/snippet/get_reg_patient_fingerprint_url.php";  break;
        case "get_ver_patient_fingerprint_url"  : require_once "registration/snippet/get_ver_patient_fingerprint_url.php";  break;
        case "store_finger_data"                : require_once "registration/snippet/store_finger_data.php";                break;
        case "verify_finger_data"               : require_once "registration/snippet/verify_finger_data.php";               break;
    }
}
?>