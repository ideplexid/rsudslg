<?php
	global $wpdb;
	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->setUsing(true, true, true);
	$install->extendInstall("rg");
	$install->install();
    require_once "smis-libs-class/DBCreator.php";
	require_once "registration/resource/database/table/smis_rg_asuransi.php";
	require_once "registration/resource/database/table/smis_rg_bahasa.php";
	require_once "registration/resource/database/table/smis_rg_besarbonus.php";
	require_once "registration/resource/database/table/smis_rg_cronjob.php";
	require_once "registration/resource/database/table/smis_rg_dokter_ganti.php";
	require_once "registration/resource/database/table/smis_rg_dokter_utama.php";
	require_once "registration/resource/database/table/smis_rg_jadwal.php";
	require_once "registration/resource/database/table/smis_rg_jenislayanan_detail.php";
	require_once "registration/resource/database/table/smis_rg_jenispasien.php";
	require_once "registration/resource/database/table/smis_rg_kabupaten.php";
	require_once "registration/resource/database/table/smis_rg_kartu.php";
	require_once "registration/resource/database/table/smis_rg_kec.php";
	require_once "registration/resource/database/table/smis_rg_kelurahan.php";
	require_once "registration/resource/database/table/smis_rg_layananpasien.php";
	require_once "registration/resource/database/table/smis_rg_nomor_asuransi.php";
	require_once "registration/resource/database/table/smis_rg_patient.php";
	require_once "registration/resource/database/table/smis_rg_perujuk.php";
	require_once "registration/resource/database/table/smis_rg_perusahaan.php";
	require_once "registration/resource/database/table/smis_rg_propinsi.php";
	require_once "registration/resource/database/table/smis_rg_refpolibpjs.php";
	require_once "registration/resource/database/table/smis_rg_replaced.php";
    require_once "registration/resource/database/table/smis_rg_changed.php";
	require_once "registration/resource/database/table/smis_rg_sep.php";
	require_once "registration/resource/database/table/smis_rg_sep_rujukan.php";
	require_once "registration/resource/database/table/smis_rg_suku.php";
    require_once "registration/resource/database/table/smis_rg_excel_bpjs.php";
    require_once "registration/resource/database/table/smis_rg_import_bpjs.php";
    require_once "registration/resource/database/table/smis_rg_import_pasien.php";
    require_once "registration/resource/database/table/smis_rg_fingerprint.php";
	require_once "registration/resource/database/table/smis_rg_rekap_jadwal.php";
	require_once "registration/resource/database/table/smis_rg_kedusunan.php";
	require_once "registration/resource/database/table/smis_rg_bed_pasien.php";
    require_once "registration/resource/database/table/smis_rg_yukantri.php";
    
    require_once "registration/resource/database/view/smis_rg_vregister.php";
    require_once "registration/resource/database/view/smis_rgv_layananpasien.php";
    require_once "registration/resource/database/view/smis_rgv_layananpasien_aktif.php";
    require_once "registration/resource/database/view/smis_rgv_uri.php";
    require_once "registration/resource/database/view/smis_rgv_urj.php";
    require_once "registration/resource/database/view/smis_rgvj_layananpasien.php";
    require_once "registration/resource/database/view/smis_rl51.php";
    require_once "registration/resource/database/view/smis_rl52.php";
    require_once "registration/resource/database/view/smis_rgv_duplicate.php";
    require_once "registration/resource/database/view/smis_rg_vsep.php";
    
    require_once "registration/resource/database/table/smis_rg_vregisterb.php";

?>