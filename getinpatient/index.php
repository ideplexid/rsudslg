<?php 
require_once "../smis-base/smis-config.php";
$nrm = $_GET['nrm'];
$result = array();
$result['result']="Success";
if($nrm!=""){
    $link = mysqli_connect(SMIS_SERVER,SMIS_USERNAME,SMIS_PASSWORD,SMIS_DATABASE);
    if(!$link || mysqli_connect_errno($link)){
        $result['result']	= "MySQL error ".mysqli_errno($link).": ".mysqli_error($link);
    }

    $res = mysqli_query($link,"SELECT * FROM smis_rg_patient WHERE id='".$nrm."' LIMIT 0,1");
    $pasien = mysqli_fetch_array($res);
    

    $res = mysqli_query($link,"SELECT * FROM smis_rg_layananpasien WHERE nrm='".$nrm."' AND selesai=0 ORDER BY id DESC LIMIT 0,1");
    $row = mysqli_fetch_array($res);
    if($row!=null){
        $result['item']['nrm']              = $nrm;
        $result['item']['nama']             = $pasien['nama'];
        $result['item']['jenis kelamin']    = $pasien['kelamin']=="0"?"Laki-Laki":"Perempuan";
        $tg = explode("-",$pasien['tgl_lahir']);
        $result['item']['tanggal lahir']    = $tg[2]."-".$tg['1']."-".$tg[0];
        $result['item']['umur']             = $row['umur'];
        $result['item']['ruangan']          = strtoupper(str_replace("_", " ", $row['last_ruangan']));
        $result['item']['bed']              = $row['last_bed'];    
    }
}
header('Content-Type: application/json');
echo json_encode($result);