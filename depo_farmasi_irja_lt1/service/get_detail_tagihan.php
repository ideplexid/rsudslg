<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");
	global $db;

	$_DB_RESEP = InventoryLibrary::$_TBL_PENJUALAN_RESEP;
	$_DB_OBAT_JADI = InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI;
	$_DB_OBAT_RACIKAN = InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN;
	$_DB_OBAT_BAHAN_RACIKAN = InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN;
	$_DB_RETUR = InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP;
	$_DB_DRETUR = InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP;
	$_DB_STOK = InventoryLibrary::$_TBL_STOK_OBAT;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$dbtable = new DBTable($db, "$_DB_RESEP");
		
		$resep_rows = $dbtable->get_result("
			SELECT *
			FROM $_DB_RESEP
			WHERE prop NOT LIKE 'del' AND noreg_pasien = '" . $noreg_pasien . "' AND tipe = 'resep' AND dibatalkan = '0'
		");

		$penjualan_result = array();
		$diskon_result = array();
		if ($resep_rows != null) {
			foreach($resep_rows as $rr) {
				$obat_jadi_rows = $dbtable->get_result("
					SELECT *
					FROM $_DB_OBAT_JADI
					WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $rr->id . "'
				");
				$total = 0;
				if ($obat_jadi_rows != null) {
					foreach ($obat_jadi_rows as $ojr) {
						$info = array(
							'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
							'nama'				=> "Resep " . $rr->id . " - " . $ojr->nama_obat,
							'jumlah'			=> $ojr->jumlah,
							'biaya'				=> $ojr->harga * $ojr->jumlah,
							'keterangan'		=> $ojr->nama_obat . " : " . $ojr->jumlah . " x " . ArrayAdapter::format("only-money", $ojr->harga),
							'id'				=> $ojr->id,
							'start'				=> $rr->tanggal,
							'end'				=> $rr->tanggal,
							'nama_dokter' 		=> $rr->nama_dokter,
							'id_dokter'			=> $rr->id_dokter,
							'jaspel_dokter'		=> 0,
							'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
							'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
						);
						$penjualan_result[] = $info;
						$total += ($ojr->harga * $ojr->jumlah);

						if($ojr->embalase!=0){
							$info = array(
								'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
								'nama'				=> "Resep " . $rr->id . " - Embalase - " . $ojr->nama_obat,
								'jumlah'			=> 1,
								'biaya'				=> $ojr->embalase,
								'keterangan'		=> "Embalase " . $ojr->nama_obat . " : 1 x " . ArrayAdapter::format("only-money", $ojr->embalase),
								'id'				=> "embalase_".$ojr->id,
								'start'				=> $rr->tanggal,
								'end'				=> $rr->tanggal,
								'nama_dokter' 		=> $rr->nama_dokter,
								'id_dokter'			=> $rr->id_dokter,
								'jaspel_dokter'		=> 0,
								'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
								'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
							);
							$penjualan_result[] = $info;
							$total += ($ojr->embalase);
						}
						
						if($ojr->tusla!=0){
							$info = array(
								'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
								'nama'				=> "Resep " . $rr->id . " - Tusla - " . $ojr->nama_obat,
								'jumlah'			=> 1,
								'biaya'				=> $ojr->tusla,
								'keterangan'		=> "Tuslah " . $ojr->nama_obat . " : 1 x " . ArrayAdapter::format("only-money", $ojr->tusla),
								'id'				=> "tusla_".$ojr->id,
								'start'				=> $rr->tanggal,
								'end'				=> $rr->tanggal,
								'nama_dokter' 		=> $rr->nama_dokter,
								'id_dokter'			=> $rr->id_dokter,
								'jaspel_dokter'		=> 0,
								'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
								'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
							);
							$penjualan_result[] = $info;
							$total += ($ojr->tusla);
						}
						
						/*jasa racik disini akan selalu 0*/
						if(0!=0){
							$info = array(
								'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
								'nama'				=> "Resep " . $rr->id . " - Jasa Racik - " . $ojr->nama_obat,
								'jumlah'			=> 1,
								'biaya'				=> 0,
								'keterangan'		=> "Jasa Racik " . $ojr->nama_obat . " : 1 x 0,00",
								'id'				=> "jasa_racik_".$ojr->id,
								'start'				=> $rr->tanggal,
								'end'				=> $rr->tanggal,
								'nama_dokter' 		=> $rr->nama_dokter,
								'id_dokter'			=> $rr->id_dokter,
								'jaspel_dokter'		=> 0,
								'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
								'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
							);
							$penjualan_result[] = $info;
						}
						
					}
				}

				$obat_racikan_rows = $dbtable->get_result("
					SELECT *
					FROM $_DB_OBAT_RACIKAN
					WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $rr->id . "'
				");
				if ($obat_racikan_rows != null) {
					foreach ($obat_racikan_rows as $orr) {
						$bahan_racikan_rows = $dbtable->get_result("
							SELECT *
							FROM $_DB_OBAT_BAHAN_RACIKAN
							WHERE id_penjualan_obat_racikan = '" . $orr->id . "'
						");
						if ($bahan_racikan_rows != null) {
							$jasa_racik = 0;
							if ($bahan_racikan_rows != null)
								$jasa_racik = $orr->jasa_racik / count($bahan_racikan_rows);
							foreach ($bahan_racikan_rows as $brr) {
								$info = array(
									'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
									'nama'				=> "Resep " . $rr->id . " - [R] " . $orr->nama . " - " . $brr->nama_obat,
									'jumlah'			=> $brr->jumlah,
									'biaya'				=> $brr->harga * $brr->jumlah,
									'keterangan'		=> $brr->nama_obat . " : " . $brr->jumlah . " x " . ArrayAdapter::format("only-money", $brr->harga),
									'id'				=> "racikan_".$brr->id,
									'start'				=> $rr->tanggal,
									'end'				=> $rr->tanggal,
									'nama_dokter' 		=> $rr->nama_dokter,
									'id_dokter'			=> $rr->id_dokter,
									'jaspel_dokter'		=> 0,
									'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
									'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
								);
								$penjualan_result[] = $info;
								$total += ($brr->harga * $brr->jumlah);
								
								if($brr->embalase!=0){
									$info = array(
										'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
										'nama'				=> "Resep " . $rr->id . " - Embalase [R] " . $orr->nama . " - " . $brr->nama_obat,
										'jumlah'			=> 1,
										'biaya'				=> $brr->embalase,
										'keterangan'		=> "Embalase " . $brr->nama_obat . " : 1 x " . ArrayAdapter::format("only-money", $brr->embalase),
										'id'				=> "embalase_racikan_".$brr->id,
										'start'				=> $rr->tanggal,
										'end'				=> $rr->tanggal,
										'nama_dokter' 		=> $rr->nama_dokter,
										'id_dokter'			=> $rr->id_dokter,
										'jaspel_dokter'		=> 0,
										'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
										'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
									);
									$penjualan_result[] = $info;
									$total += ($brr->embalase);
								}
								
								if($brr->tusla!=0){
									$info = array(
										'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
										'nama'				=> "Resep " . $rr->id . " - Tuslah [R] " . $orr->nama . " - " . $brr->nama_obat,
										'jumlah'			=> 1,
										'biaya'				=> $brr->tusla,
										'keterangan'		=> "Tuslah " . $brr->nama_obat . " : 1 x " . ArrayAdapter::format("only-money", $brr->tusla),
										'id'				=> "tusla_racikan_".$brr->id,
										'start'				=> $rr->tanggal,
										'end'				=> $rr->tanggal,
										'nama_dokter' 		=> $rr->nama_dokter,
										'id_dokter'			=> $rr->id_dokter,
										'jaspel_dokter'		=> 0,
										'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
										'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
									);
									$penjualan_result[] = $info;
									$total += ($brr->tusla);
								}
								if($jasa_racik!=0){
									$info = array(
										'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
										'nama'				=> "Resep " . $rr->id . " - Jasa Racik [R] " . $orr->nama . " - " . $brr->nama_obat,
										'jumlah'			=> 1,
										'biaya'				=> $jasa_racik,
										'keterangan'		=> "Jasa Racik " . $brr->nama_obat . " : 1 x " . ArrayAdapter::format("only-money", $jasa_racik),
										'id'				=> "jasa_racik_racikan_".$brr->id,
										'start'				=> $rr->tanggal,
										'end'				=> $rr->tanggal,
										'nama_dokter' 		=> $rr->nama_dokter,
										'id_dokter'			=> $rr->id_dokter,
										'jaspel_dokter'		=> 0,
										'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
										'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
									);
									$penjualan_result[] = $info;
									$total += ($jasa_racik);
								}
								
							}
						}
					}
				}

				if ($rr->diskon > 0) {
					$diskon = 0;
					if ($rr->t_diskon == "nominal")
						$diskon = $rr->diskon;
					else
						$diskon = $total * $rr->diskon / 100;
					$info = array(
						'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
						'nama'				=> "POTONGAN TAGIHAN RESEP " . $rr->id,
						'jumlah'			=> 1,
						'biaya'				=> -1 * $diskon,
						'keterangan'		=> "POTONGAN TAGIHAN RESEP " . $rr->id . " : " . ArrayAdapter::format("only-money", -1 * $diskon),
						'id'				=> "diskon_resep_" . $rr->id,
						'start'				=> $rr->tanggal,
						'end'				=> $rr->tanggal,
						'nama_dokter' 		=> $rr->nama_dokter,
						'id_dokter'			=> $rr->id_dokter,
						'jaspel_dokter'		=> 0,
						'urjigd'			=> $rr->uri == 1 ? "URI" : "URJ",
						'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rr->tanggal)
					);
					$diskon_result[] = $info;
				}
			}
		}

		$return_resep_rows = $dbtable->get_result("
			SELECT b.id, a.tanggal, b.id_penjualan_resep, c.nama_obat, b.jumlah, a.persentase_retur, b.harga, d.id_dokter, d.nama_dokter, d.uri
			FROM (($_DB_RETUR a 
					LEFT JOIN 
							$_DB_DRETUR b ON a.id = b.id_retur_penjualan_resep) 
							LEFT JOIN $_DB_STOK c ON b.id_stok_obat = c.id) 
							LEFT JOIN $_DB_RESEP d ON b.id_penjualan_resep = d.id
			WHERE a.prop NOT LIKE 'del' AND a.dibatalkan = '0' AND b.prop NOT LIKE 'del' AND d.noreg_pasien = '" . $noreg_pasien . "' 
		");
		$return_result = array();
		if ($return_resep_rows != null) {
			foreach ($return_resep_rows as $rrr) {
				$info = array(
					'id'				=> "detail_retur_resep_" . $rrr->id,
					'waktu' 			=> ArrayAdapter::format("date d M Y", $rrr->tanggal),
					'nama'				=> "Return Resep " . $rrr->id_penjualan_resep . " - " . $rrr->nama_obat,
					'jumlah'			=> $rrr->jumlah,
					'biaya'				=> -1 * $rrr->jumlah * $rrr->harga * $rrr->persentase_retur / 100,
					'keterangan'		=> $rrr->nama_obat . " : " . $rrr->jumlah . " x " . ArrayAdapter::format("only-money", (-1 * $rrr->harga * $rrr->persentase_retur / 100)),
					'nama_dokter' 		=> $rrr->nama_dokter,
					'id_dokter'			=> $rrr->id_dokter,
					'jaspel_dokter'		=> 0,
					'urjigd'			=> $rrr->uri == 1 ? "URI" : "URJ",
					'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rrr->tanggal)
				);
				$return_result[] = $info;
			}
		}

		$asuhan_farmasi_rows = $dbtable->get_result("
			SELECT a.*, b.uri
			FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . " a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id
			WHERE a.noreg_pasien = '" . $noreg_pasien . "' AND a.prop = ''
		");
		$asuhan_farmasi_result = array();
		if ($asuhan_farmasi_rows != null) {
			foreach ($asuhan_farmasi_rows as $afr) {
				$nama = $afr->nama_tindakan == "" ? "Asuhan Farmasi" : $afr->nama_tindakan;
				$info = array(
					'id'				=> $afr->id,
					'waktu'				=> ArrayAdapter::format("date d M Y", $afr->tanggal),
					'nama'				=> $nama . " No. " . $afr->id . " - " . ArrayAdapter::format("unslug", $afr->ruangan),
					'jumlah'			=> 1,
					'biaya'				=> $afr->biaya,
					'keterangan'		=> $nama . " No. " . $afr->id . " - " . ArrayAdapter::format("unslug", $afr->ruangan) . " : 1 x " . ArrayAdapter::format("only-money", $afr->biaya),
					'nama_dokter' 		=> "",
					'id_dokter'			=> 0,
					'jaspel_dokter'		=> 0,
					'urjigd'			=> $afr->uri == 1 ? "URI" : "URJ",
					'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $rrr->tanggal)
				);
				$asuhan_farmasi_result[] = $info;
			}
		}
		
		$cara_keluar = "Selesai";
		$selesai = "1";
		$params = array(
			'id' => $noreg
		);
		$service_consumer = new ServiceConsumer(
			$db,
			"get_one_register",
			$params,
			"registration"
		);
		$content = $service_consumer->execute()->getContent();
		if (count($content) == 1) {
			if ($content[0]['uri'] == "1") {
				$selesai = $db->get_var("
					SELECT 
						COUNT(*) jumlah 
					FROM 
						" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
					WHERE
						prop = ''
							AND dibatalkan = 0
							AND selesai = 0
							AND noreg_pasien = '" . $_POST['noreg_pasien'] . "'
				") > 0 ? "0" : "1";
				if ($selesai == "0") {
					$cara_keluar = "Belum Selesai";
				}
			}
		}
		
		echo json_encode(array(
			'selesai' => $selesai,
			'exist' => "1",
			'reverse' => "0",
			'cara_keluar' => $cara_keluar,
			'data' => array(
				'penjualan_resep'	=> array(
					'jasa_pelayanan'	=> 0,
					'result'			=> $penjualan_result
				),
				'return_resep'		=> array(
					'jasa_pelayanan'	=> 0,
					'result'			=> $return_result
				),
				'diskon_resep'		=> array(
					'jasa_pelayanan'	=> 0,
					'result'			=> $diskon_result
				),
				'asuhan_farmasi'	=> array(
					'jasa_pelayanan'	=> 0,
					'result'			=> $asuhan_farmasi_result	
				)
			),
		));
	}
?>
