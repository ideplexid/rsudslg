<?php
	global $PLUGINS;
	
	$init['name'] = "depo_farmasi_irja_lt1";
	$init['path'] = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Depo Farmasi IRJA Lt. 1";
	$init['require'] = "administrator";
	$init['service'] = "";
	$init['version'] = "1.0.1";
	$init['number'] = "2";
	$init['type'] = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>