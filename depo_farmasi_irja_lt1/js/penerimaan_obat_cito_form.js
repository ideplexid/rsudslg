var penerimaan_obat_cito;
var dpenerimaan_obat_cito;
var vendor;
var obat;
var row_num;
var previous_use_ppn;

$(document).ready(function() {
	$(".mydate").datepicker();
	obat = new ObatAction(
		"obat",
		"depo_farmasi_irja_lt1",
		"penerimaan_obat_cito_form",
		new Array()
	);
	obat.setSuperCommand("obat");
	vendor = new VendorAction(
		"vendor",
		"depo_farmasi_irja_lt1",
		"penerimaan_obat_cito_form",
		new Array()
	);
	vendor.setSuperCommand("vendor");
	dpenerimaan_obat_cito = new DPenerimaanObatCitoAction(
		"dpenerimaan_obat_cito",
		"depo_farmasi_irja_lt1",
		"penerimaan_obat_cito_form",
		new Array("hpp", "hna", "diskon")
	);
	penerimaan_obat_cito = new PenerimaanObatCitoAction(
		"penerimaan_obat_cito",
		"depo_farmasi_irja_lt1",
		"penerimaan_obat_cito_form",
		new Array("diskon", "materai")
	);

	var id = $("#penerimaan_obat_cito_id").val();
	row_num = 0;
	penerimaan_obat_cito.get_footer();
	penerimaan_obat_cito.show_detail_form(id);
	dpenerimaan_obat_cito.setEditMode("true");
	$("#penerimaan_obat_cito_no_opl").focus();

	$("#dpenerimaan_obat_cito_hpp").on("change", function() {
		var use_ppn = $("#penerimaan_obat_cito_use_ppn").val();
		if ($("#dpenerimaan_obat_cito_hpp").val() == "") {
			$("#dpenerimaan_obat_cito_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_cito_hna").val("Rp. 0,00");
		} else {
			var hna = parseFloat($("#dpenerimaan_obat_cito_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hpp = hna * 1.1;
			if (use_ppn == false)
				hpp = hna;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_cito_hna").val(f_hpp);
		}
	});
	$("#dpenerimaan_obat_cito_hna").on("change", function() {
		var use_ppn = $("#penerimaan_obat_cito_use_ppn").val();
		if ($("#dpenerimaan_obat_cito_hna").val() == "") {
			$("#dpenerimaan_obat_cito_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_cito_hna").val("Rp. 0,00");
		} else {
			var hpp = parseFloat($("#dpenerimaan_obat_cito_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hna = hpp / 1.1;
			if (use_ppn == false)
				hna = hpp;
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_cito_hpp").val(f_hna);
		}
	});

	var obat_data = obat.getViewData();
	$("#dpenerimaan_obat_cito_nama_obat").typeahead({
		minLength	: 3,
		source		: function (query, process) {
			var $items = new Array;
			$items = [""];                
			obat_data['kriteria'] = $('#dpenerimaan_obat_cito_nama_obat').val();
			$.ajax({
				url		: "",
				type	: "POST",
				data	: obat_data,
				success	: function(response) {
					var json = getContent(response);
					var t_data = json.d.data;
					$items = [""];      				
					$.map(t_data, function(data) {
						var group;
						group = {
							id		: data.id,
							name	: data.nama, 
							kode	: data.kode, 
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = "";
								value +=  this.kode + " - " + this.name;
								if(typeof(this.level) != "undefined") {
									value += " <span class='pull-right muted'>";
									value += this.level;
									value += "</span>";
								}
								return String.prototype.replace.apply("<div class='typeaheadiv'>" + value + "</div>", arguments);
							}
						};
						$items.push(group);
					});
					process($items);
				}
			});
		},
		updater		: function (item) {
			var item = JSON.parse(item);  
			obat.select(item.id);  
			$("#dpenerimaan_obat_cito_nama_obat").focus();       
			return item.name;
		}
	});
	$("#dpenerimaan_obat_cito_satuan").on("change", function() {
		var part = $("#dpenerimaan_obat_cito_satuan").val().split("_");
		$("#dpenerimaan_obat_cito_konversi").val(part[0]);
		$("#dpenerimaan_obat_cito_satuan_konversi").val(part[1]);
	});
});