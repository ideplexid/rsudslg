var drpr;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	drpr = new DRPRAction(
		"drpr",
		"depo_farmasi_irja_lt1",
		"laporan_rincian_retur_penjualan_resep",
		new Array()
	);
	$("#drpr_list").append(
		"<tr id='temp'>" +
			"<td colspan='18'><strong><small><center>DATA RETUR PENJUALAN RESEP BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});