var retur;
var penjualan_bebas;
var dretur;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$("#smis-chooser-modal").on("show", function() {
		$("#smis-chooser-modal").removeClass("full_model");
		$("#smis-chooser-modal").addClass("full_model");
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("full_model");
	});
	penjualan_bebas = new PenjualanBebasAction(
		"penjualan_bebas",
		"depo_farmasi_irja_lt1",
		"retur_penjualan_bebas",
		new Array()
	);
	penjualan_bebas.setSuperCommand("penjualan_bebas");
	var dretur_columns = new Array("id", "id_retur_penjualan_resep", "id_stok_obat", "jumlah", "harga", "subtotal");
	dretur = new DReturAction(
		"dretur",
		"depo_farmasi_irja_lt1",
		"retur_penjualan_bebas",
		dretur_columns
	);
	var retur_columns = new Array("id", "id_penjualan_resep", "tanggal", "persentase_retur");
	retur = new ReturAction(
		"retur",
		"depo_farmasi_irja_lt1",
		"retur_penjualan_bebas",
		retur_columns
	);
	retur.setSuperCommand("retur");
	retur.view();
});