function PerbaikanHargaAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PerbaikanHargaAction.prototype.constructor = PerbaikanHargaAction;
PerbaikanHargaAction.prototype = new TableAction();
PerbaikanHargaAction.prototype.edit = function(id) {
	var data = this.getEditData(id);
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#perbaikan_harga_id").val(json.id);
			$("#perbaikan_harga_id_dobat_masuk").val(json.id_dobat_masuk);
			$("#perbaikan_harga_kode").val(json.kode_obat);
			$("#perbaikan_harga_nama").val(json.nama_obat);
			$("#perbaikan_harga_jenis").val(json.nama_jenis_obat);
			$("#perbaikan_harga_hpp_lama").val("Rp. " + parseFloat(json.hna).formatMoney("2", ".", ","));
			$("#perbaikan_harga_hpp_baru").val("Rp. " + parseFloat(json.hna).formatMoney("2", ".", ","));
			$("#perbaikan_harga_add_form").smodal("show");
		}
	);
};
PerbaikanHargaAction.prototype.save = function() {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#perbaikan_harga_id").val();
	data['id_dobat_masuk'] = $("#perbaikan_harga_id_dobat_masuk").val();
	data['hna'] = $("#perbaikan_harga_hpp_baru").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	$.post(
		"",
		data,
		function() {
			self.view();
			$("#perbaikan_harga_add_form").smodal("hide");
		}
	);
};