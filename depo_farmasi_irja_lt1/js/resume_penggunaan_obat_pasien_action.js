function RPOPAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RPOPAction.prototype.constructor = RPOPAction;
RPOPAction.prototype = new TableAction();
RPOPAction.prototype.view = function() {
	if ($("#rpop_nrm_pasien").val() == "")
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_resume";
	data['nrm_pasien'] = $("#rpop_nrm_pasien").val();
	showLoading();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) {
				dismissLoading();
				return;
			}
			$("#resume_list").html(json.html);
			dismissLoading();
		}
	);
};
RPOPAction.prototype.detail = function(r_num) {
	var data = this.getRegulerData();
	data['action'] = "detail_resume_penggunaan_obat_pasien";
	data['nrm_pasien'] = parseFloat($("#rpop_nrm_pasien").val());
	data['nama_pasien'] = $("#rpop_nama_pasien").val();
	data['alamat_pasien'] = $("#rpop_alamat_pasien").val();
	data['noreg_pasien'] = $("tbody#resume_list tr:eq(" + r_num + ") td:eq(1)").text();
	data['tanggal_daftar'] = $("tbody#resume_list tr:eq(" + r_num + ") td:eq(3)").text();
	data['uri'] = $("tbody#resume_list tr:eq(" + r_num + ") td:eq(4)").text();
	data['umur'] = $("tbody#resume_list tr:eq(" + r_num + ") td:eq(5)").text();
	data['unit'] = $("tbody#resume_list tr:eq(" + r_num + ") td:eq(6)").text();
	data['pasien_aktif'] = $("tbody#resume_list tr:eq(" + r_num + ") td:eq(7)").text();
	LoadSmisPage(data);
};
RPOPAction.prototype.back = function(nrm_pasien, nama_pasien, alamat_pasien) {
	var data = this.getRegulerData();
	data['action'] = "resume_penggunaan_obat_pasien";
	data['nrm_pasien'] = nrm_pasien;
	data['nama_pasien'] = nama_pasien;
	data['alamat_pasien'] = alamat_pasien;
	LoadSmisPage(data);
};
RPOPAction.prototype.refresh = function(nrm_pasien, nama_pasien, alamat_pasien, noreg_pasien, tanggal_daftar, uri, umur, unit, pasien_aktif) {
	var data = this.getRegulerData();
	data['action'] = "detail_resume_penggunaan_obat_pasien";
	data['nrm_pasien'] = nrm_pasien;
	data['nama_pasien'] = nama_pasien;
	data['alamat_pasien'] = alamat_pasien;
	data['noreg_pasien'] = noreg_pasien;
	data['tanggal_daftar'] = tanggal_daftar;
	data['uri'] = uri;
	data['umur'] = umur;
	data['unit'] = unit;
	data['pasien_aktif'] = pasien_aktif;
	LoadSmisPage(data);
};
RPOPAction.prototype.print_detail = function() {
	smis_print($("#resume").html());
};