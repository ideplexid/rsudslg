function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	$("#pengaturan_margin_obat_jenis_pasien_id_obat").val(json.id);
	$("#pengaturan_margin_obat_jenis_pasien_kode_obat").val(json.kode);
	$("#pengaturan_margin_obat_jenis_pasien_nama_obat").val(json.nama);
	$("#pengaturan_margin_obat_jenis_pasien_nama_jenis_obat").val(json.nama_jenis_barang);
};