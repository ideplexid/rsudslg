function PasienAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PasienAction.prototype.constructor = PasienAction;
PasienAction.prototype = new TableAction();
PasienAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
PasienAction.prototype.selected = function(json) {
	$("#resep_noreg_pasien").val(json.id);
	$("#resep_nrm_pasien").val(json.nrm);
	$("#resep_nama_pasien").val(json.sebutan + " " + json.nama_pasien);
	$("#resep_alamat_pasien").val(json.alamat_pasien);
	$("#resep_uri").val(json.uri);
	$("#resep_jenis").val(json.carabayar);
	$("#resep_perusahaan").val(json.n_perusahaan);
	$("#resep_asuransi").val(json.nama_asuransi);
	$("#resep_nama_dokter").focus();
	need_margin_penjualan_request = 1;
	resep.refreshHargaAndSubtotal();
};
PasienAction.prototype.addViewData = function(d) {
	d['tanggal'] = $("#search_tanggal").val();
	d['noreg_pasien'] = $("#search_noreg").val();
	d['nrm_pasien'] = $("#search_nrm").val();
	d['nama_pasien'] = $("#search_nama").val();
	d['alamat_pasien'] = $("#search_alamat").val();
	d['last_nama_ruangan'] = $("#search_last_ruangan").val();
	d['jenis_pasien'] = $("#search_carabayar").val();
	d['perusahaan_pasien'] = $("#search_perusahaan").val();
	d['asuransi_pasien'] = $("#search_asuransi").val();
	return  d;
};