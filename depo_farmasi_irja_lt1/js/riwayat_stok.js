var riwayat_stok;
var stok_obat;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$("#smis-chooser-modal").on("show", function() {
		$("#smis-chooser-modal").removeClass("full_model");
		$("#smis-chooser-modal").addClass("full_model");
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("full_model");
	});
	stok_obat = new StokObatAction(
		"stok_obat",
		"depo_farmasi_irja_lt1",
		"riwayat_stok_obat",
		new Array()
	);
	stok_obat.setSuperCommand("stok_obat");
	riwayat_stok = new RiwayatStokAction(
		"riwayat_stok",
		"depo_farmasi_irja_lt1",
		"riwayat_stok_obat",
		new Array()
	);
	riwayat_stok.setSuperCommand("riwayat_stok");
	riwayat_stok.view();
});