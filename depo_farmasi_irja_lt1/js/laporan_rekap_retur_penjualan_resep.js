var rrpr;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	rrpr = new RRPRAction(
		"rrpr",
		"depo_farmasi_irja_lt1",
		"laporan_rekap_retur_penjualan_resep",
		new Array()
	);
	$("#rrpr_list").append(
		"<tr id='temp'>" +
			"<td colspan='13'><strong><small><center>DATA RETUR PENJUALAN RESEP BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});