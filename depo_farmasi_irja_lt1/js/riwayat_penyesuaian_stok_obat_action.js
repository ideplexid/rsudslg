function RiwayatPenyesuaianStokAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RiwayatPenyesuaianStokAction.prototype.constructor = RiwayatPenyesuaianStokAction;
RiwayatPenyesuaianStokAction.prototype = new TableAction();
RiwayatPenyesuaianStokAction.prototype.getViewData = function() {
	var data = TableAction.prototype.getViewData.call(this);
	data['tanggal_from'] = $("#riwayat_penyesuaian_stok_tanggal_from").val();
	data['tanggal_to'] = $("#riwayat_penyesuaian_stok_tanggal_to").val();
	return data;
};