<?php
class DObatRacikanTable extends Table {
	public function getHeaderButton() {
		$btn_group = new ButtonGroup("noprint");
		$btn_add_bahan = new Button("", "", "Bahan");
		$btn_add_bahan->setAction("dresep.show_add_bahan_form()");
		$btn_add_bahan->setAtribute("id='bahan_add'");
		$btn_add_bahan->setClass("btn-primary");
		$btn_add_bahan->setIcon("icon-plus icon-white");
		$btn_add_bahan->setIsButton(Button::$ICONIC_TEXT);
		$btn_group->addElement($btn_add_bahan);
		global $db;
		if (getSettings($db, "depo_farmasi6-paket_obat_racikan", 1) == 1) {
			$btn_add_paket_obat_racikan = new Button("", "", "Paket Obat");
			$btn_add_paket_obat_racikan->setAction("paket_obat_racikan.chooser('paket_obat_racikan', 'paket_obat_racikan_button', 'paket_obat_racikan', paket_obat_racikan, 'Paket Obat Racikan')");
			$btn_add_paket_obat_racikan->setAtribute("id='paket_obat_racikan_add'");
			$btn_add_paket_obat_racikan->setClass("btn-inverse");
			$btn_add_paket_obat_racikan->setIcon("icon-plus icon-white");
			$btn_add_paket_obat_racikan->setIsButton(Button::$ICONIC_TEXT);
			$btn_group->addElement($btn_add_paket_obat_racikan);
		}
		return $btn_group->getHtml();
	}
}
?>