<?php
class BahanRacikanTable extends Table {
	public function getHtml(){
		$table_content = "";
		$table_content .= "<div class='".$this->class."' style='display: none;'>";
		$table_content .= "<table class='table table-bordered table-hover table-striped table-condensed ' id='table_".$this->name."' >";
		$table_content .= "<caption>".$this->caption."</caption>";
		$table_content .= $this->getHeader();
		$table_content .= $this->getBody();
		$table_content .= "</table></div>";
		$content = $table_content;
		if($this->model==static::$EDIT){
			$content = "<div class='row'>";
			$content .= "<div class='span12 table-responsive'>";
			$content .= $table_content;
			$content .= "</div>";
			$content .= "</div>";
		}
		return $content;
	}
}?>