<?php
	require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");

	class PenyesuaianStokDBResponder extends DBResponder {
		public function save() {
			$data = $this->postToArray();
			$result = $this->dbtable->insert($data);
			$success['type'] = "insert";
			$success['id'] = $this->dbtable->get_inserted_id();
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;

			$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_OBAT);
			$stok_data = array();
			$stok_data['sisa'] = $_POST['jumlah_baru'];
			$stok_id['id'] = $_POST['id_stok_obat'];
			$stok_obat_dbtable->update($stok_data, $stok_id);
			//logging riwayat stok obat:
			$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
			$data_riwayat = array();
			$data_riwayat['tanggal'] = date("Y-m-d");
			$data_riwayat['id_stok_obat'] = $_POST['id_stok_obat'];
			$data_riwayat['jumlah_masuk'] = 0;
			$data_riwayat['jumlah_keluar'] = 0;
			if ($_POST['jumlah_baru'] > $_POST['jumlah_lama'])
				$data_riwayat['jumlah_masuk'] = $_POST['jumlah_baru'] - $_POST['jumlah_lama'];
			else
				$data_riwayat['jumlah_keluar'] = $_POST['jumlah_lama'] - $_POST['jumlah_baru'];
			$data_riwayat['sisa'] = $_POST['jumlah_baru'];
			$data_riwayat['keterangan'] = "Penyesuaian Stok";
			global $user;
			$data_riwayat['nama_user'] = $user->getName();
			$riwayat_dbtable->insert($data_riwayat);
			//logging kartu stok obat:
			$obat_row = $stok_obat_dbtable->get_row("
				SELECT a.id_obat, a.kode_obat, a.nama_obat, a.nama_jenis_obat
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a
				WHERE a.id = '" . $_POST['id_stok_obat'] . "'
			");
			$stok_row = $stok_obat_dbtable->get_row("
				SELECT SUM(b.sisa) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $obat_row->id_obat . "'
			");
			$kartu_stok_data = array(
				"f_id"				=> $success['id'],
				"unit"				=> "Rekonsiliasi Stok : " . $data_riwayat['keterangan'],
				"no_bon"			=> "-",
				"id_obat"			=> $obat_row->id_obat,
				"nama_obat"			=> $obat_row->nama_obat,
				"nama_jenis_obat"	=> $obat_row->nama_jenis_obat,
				"tanggal"			=> date("Y-m-d"),
				"masuk"				=> $data_riwayat['jumlah_masuk'],
				"keluar"			=> $data_riwayat['jumlah_keluar'],
				"sisa"				=> $stok_row->jumlah
			);
			$ks_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
			$ks_dbtable->insert($kartu_stok_data);
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$data = $this->dbtable->get_row("
				SELECT " . InventoryLibrary::$_TBL_STOK_OBAT . ".*
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . "				
				WHERE " . InventoryLibrary::$_TBL_STOK_OBAT . ".id = '" . $id . "'
			");
			return $data;
		}
	}
?>