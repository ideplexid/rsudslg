<?php
	require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");
	
	class PasienDBResponder extends DBResponder {
		public function edit() {
			$nrm_pasien = $_POST['id'];
			$data = $this->dbtable->get_db()->get_row("
				SELECT nrm_pasien, nama_pasien, alamat_pasien
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE prop NOT LIKE 'del' AND dibatalkan = '0' AND nrm_pasien = '" . $nrm_pasien . "' AND noreg_pasien > 0
				ORDER BY id DESC
				LIMIT 0, 1
			");
			return $data;
		}
	}
?>