<?php
	require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");
	
	class BahanDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT DISTINCT id_obat, kode_obat, nama_obat, nama_jenis_obat
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id . "'
			");
			$satuan_rows = $this->dbtable->get_result("
				SELECT satuan, konversi, satuan_konversi 
				FROM (
					SELECT a.satuan, a.konversi, a.satuan_konversi, SUM(a.sisa) AS 't_jumlah'
					FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $id . "'
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				) v
				ORDER BY t_jumlah DESC
			");
			$satuan_option = "";
			foreach($satuan_rows as $sr) {
				if ($sr->konversi == 1) {
					$satuan_option .= "
							<option value='" . $sr->konversi . "_" . $sr->satuan_konversi . "'>" . $sr->satuan . "</option>
						";
				}
			}
			$data['satuan_option'] = $satuan_option;
			return $data;
		}
	}
?>