<?php
global $db;
require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");
require_once("smis-base/smis-include-service-consumer.php");
require_once("depo_farmasi_irja_lt1/responder/AsuhanFarmasiDBResponder.php");
require_once("depo_farmasi_irja_lt1/table/PasienTable.php");
require_once("depo_farmasi_irja_lt1/adapter/PasienAdapter.php");

//tindakan farmasi consumer:
$tindakan_farmasi_table = new Table(
	array("Tindakan", "Biaya"),
	"",
	null,
	true
);
$tindakan_farmasi_table->setName("tindakan_farmasi");
$tindakan_farmasi_table->setModel(Table::$SELECT);
$tindakan_farmasi_adapter = new SimpleAdapter();
$tindakan_farmasi_adapter->add("Tindakan", "nama_tindakan");
$tindakan_farmasi_adapter->add("Biaya", "tarif");
$tindakan_farmasi_service_responder = new ServiceResponder(
	$db,
	$tindakan_farmasi_table,
	$tindakan_farmasi_adapter,
	"get_tindakan_farmasi"
);

$table = new Table(
	array("No.", "Tanggal", "No. Reg.", "NRM", "Nama Pasien", "Tindakan", "Total Biaya", "Ruangan", "Petugas"),
	"Depo Farmasi IRJA Lt. 1 : Asuhan Farmasi",
	null,
	true
);
$table->setName("asuhan_farmasi");

//patient service consumer:
$pasien_table = new PasienTable(
	array("Tgl. Daftar", "No. Reg.", "NRM", "Nama", "Alamat", "Ruangan Terakhir", "Jns. Pasien"),
	"",
	null,
	true
);
$pasien_table->setName("pasien");
$pasien_table->setModel(Table::$SELECT);
$tanggal_daftar_text = new Text("search_tanggal", "search_tanggal", "");
$tanggal_daftar_text->setClass("search search-header-tiny search-text");
$noreg_s_text = new Text("search_noreg", "search_noreg", "");
$noreg_s_text->setClass("search search-header-tiny search-text");
$nrm_s_text = new Text("search_nrm", "search_nrm", "");
$nrm_s_text->addAtribute("autofocus");
$nrm_s_text->setClass("search search-header-tiny search-text");
$nama_s_text = new Text("search_nama", "search_nama", "");
$nama_s_text->setClass("search search-header-med search-text ");
$alamat_s_text = new Text("search_alamat", "search_alamat", "");
$alamat_s_text->setClass("search search-header-big search-text");
$last_ruangan_s_text = new Text("search_last_ruangan", "search_last_ruangan", "");
$last_ruangan_s_text->setClass("search search-header-med search-text");
$carabayar_s_text = new Text("search_carabayar", "search_carabayar", "");
$carabayar_s_text->setClass("search search-header-tiny search-text");
$header = 	"<tr class = 'header_pasien'>" .
	"<td>" . $tanggal_daftar_text->getHtml() . "</td>" .
	"<td>" . $noreg_s_text->getHtml() . "</td>" .
	"<td>" . $nrm_s_text->getHtml() . "</td>" .
	"<td>" . $nama_s_text->getHtml() . "</td>" .
	"<td>" . $alamat_s_text->getHtml() . "</td>" .
	"<td>" . $last_ruangan_s_text->getHtml() . "</td>" .
	"<td>" . $carabayar_s_text->getHtml() . "</td>" .
	"</tr>";
$pasien_table->addHeader("after", $header);
$pasien_adapter = new PasienAdapter();
$pasien_service_responder = new ServiceResponder(
	$db,
	$pasien_table,
	$pasien_adapter,
	"get_registered"
);

//petugas service consumer:
$petugas_table = new Table(
	array("Nama", "Jabatan"),
	"",
	null,
	true
);
$petugas_table->setName("petugas");
$petugas_table->setModel(Table::$SELECT);
$petugas_adapter = new SimpleAdapter();
$petugas_adapter->add("Nama", "nama");
$petugas_adapter->add("Jabatan", "nama_jabatan");
$petugas_service_responder = new ServiceResponder(
	$db,
	$petugas_table,
	$petugas_adapter,
	"employee"
);

$super_command = new SuperCommand();
$super_command->addResponder("tindakan_farmasi", $tindakan_farmasi_service_responder);
$super_command->addResponder("pasien", $pasien_service_responder);
$super_command->addResponder("petugas", $petugas_service_responder);
$init = $super_command->initialize();
if ($init != null) {
	echo $init;
	return;
}

if (isset($_POST['command'])) {
	$adapter = new SimpleAdapter(true, "No.");
	$adapter->add("Tanggal", "tanggal", "date d-m-Y, H:i");
	$adapter->add("No. Reg.", "noreg_pasien", "digit6");
	$adapter->add("NRM", "nrm_pasien", "digit6");
	$adapter->add("Nama Pasien", "nama_pasien");
	$adapter->add("Tindakan", "nama_tindakan");
	$adapter->add("Total Biaya", "biaya", "money");
	$adapter->add("Ruangan", "ruangan", "unslug");
	$adapter->add("Petugas", "nama_petugas");
	$dbtable = new DBTable($db, InventoryLibrary::$_TBL_ASUHAN_FARMASI);
	$dbtable->setOrder(" id DESC ");
	$dbresponder = new AsuhanFarmasiDBResponder(
		$dbtable,
		$table,
		$adapter
	);
	if ($_POST['command'] == "save" && $_POST['id'] == "")
		$_POST['tanggal'] = date("Y-m-d H:i:s");
	$data = $dbresponder->command($_POST['command']);

	if ($_POST['command'] == "save" || $_POST['command'] == "del") {
		/// Push Data Tagihan ke Kasir Ketika Penambahan Data Baru atau Pembatalan Data Lama :
		if (isset($data['content']['success']) && $data['content']['success'] == 1 && getSettings($db, "cashier-real-time-tagihan", "0") != 0) {
			$mode_data_tagihan = getSettings($db, "depo_farmasi6-service-get_tagihan", "get_simple_tagihan.php") == "get_simple_tagihan.php" ? "simple" : "detail";
			if ($data['content']['type'] == "insert" || $data['content']['type'] == "update") {
				$id_asuhan_farmasi = $data['content']['id'];
				$data_tagihan = array(
					'grup_name'		=> "asuhan_farmasi",
					'entity'		=> "depo_farmasi_irja_lt1"
				);
				$list = array();

				$asuhan_farmasi_row = $db->get_row("
						SELECT a.*, b.uri
						FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . " a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id
						WHERE a.id = '" . $id_asuhan_farmasi . "'
					");
				if ($asuhan_farmasi_row != null) {
					$data_tagihan['nama_pasien'] = $asuhan_farmasi_row->nama_pasien;
					$data_tagihan['nrm_pasien'] = $asuhan_farmasi_row->nrm_pasien;
					$data_tagihan['noreg_pasien'] = $asuhan_farmasi_row->noreg_pasien;

					$date_part = explode("-", explode(" ", $asuhan_farmasi_row->tanggal)[0]);
					$waktu = $date_part[2] . " " . ArrayAdapter::format("month-id", $date_part[1]) . " " . $date_part[0];

					$nama = $asuhan_farmasi_row->nama_tindakan == "" ? "Asuhan Farmasi" : $asuhan_farmasi_row->nama_tindakan;

					$list[] = array(
						'id' 				=> $id_asuhan_farmasi,
						'waktu'				=> $waktu,
						'nama'				=> $nama . " No. " . $asuhan_farmasi_row->id,
						'start' 			=> $asuhan_farmasi_row->tanggal,
						'end' 				=> $asuhan_farmasi_row->tanggal,
						'jumlah' 			=> 1,
						'biaya' 			=> $asuhan_farmasi_row->biaya,
						'dokter'			=> $asuhan_farmasi_row->nama_petugas,
						'prop'				=> "",
						'grup_name'			=> "asuhan_farmasi",
						'keterangan'		=> $nama . " di Ruangan " . ArrayAdapter::format("unslug", $asuhan_farmasi_row->ruangan),
						'nama_dokter' 		=> "",
						'id_dokter'			=> 0,
						'jaspel_dokter'		=> 0,
						'urjigd'			=> $asuhan_farmasi_row->uri == 1 ? "URI" : "URJ",
						'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $asuhan_farmasi_row->tanggal)
					);
				}
				$data_tagihan['list'] = $list;
				$consumer_service = new ServiceConsumer(
					$db,
					"proceed_receivable",
					$data_tagihan,
					"kasir"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$consumer_service->execute();
			} else if ($_POST['command'] == "del" && isset($_POST['id'])) {
				$id_asuhan_farmasi = $_POST['id'];
				$data_tagihan = array(
					'grup_name'		=> "asuhan_farmasi",
					'entity'		=> "depo_farmasi_irja_lt1"
				);
				$list = array();

				$asuhan_farmasi_row = $db->get_row("
					SELECT a.*, b.uri
					FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . " a LEFT JOIN smis_rg_layananpasien b ON a.noreg_pasien = b.id
					WHERE a.id = '" . $id_asuhan_farmasi . "'
				");
				if ($asuhan_farmasi_row != null) {
					$data_tagihan['nama_pasien'] = $asuhan_farmasi_row->nama_pasien;
					$data_tagihan['nrm_pasien'] = $asuhan_farmasi_row->nrm_pasien;
					$data_tagihan['noreg_pasien'] = $asuhan_farmasi_row->noreg_pasien;

					$date_part = explode("-", explode(" ", $asuhan_farmasi_row->tanggal)[0]);
					$waktu = $date_part[2] . " " . ArrayAdapter::format("month-id", $date_part[1]) . " " . $date_part[0];

					$nama = $asuhan_farmasi_row->nama_tindakan == "" ? "Asuhan Farmasi" : $asuhan_farmasi_row->nama_tindakan;

					$list[] = array(
						'id' 				=> $id_asuhan_farmasi,
						'waktu'				=> $waktu,
						'nama'				=> $nama . " No. " . $asuhan_farmasi_row->id,
						'start' 			=> $asuhan_farmasi_row->tanggal,
						'end' 				=> $asuhan_farmasi_row->tanggal,
						'jumlah' 			=> 1,
						'biaya' 			=> $asuhan_farmasi_row->biaya,
						'dokter'			=> $asuhan_farmasi_row->nama_petugas,
						'prop'				=> "del",
						'grup_name'			=> "asuhan_farmasi",
						'keterangan'		=> $nama . " di Ruangan " . ArrayAdapter::format("unslug", $asuhan_farmasi_row->ruangan),
						'nama_dokter' 		=> "",
						'id_dokter'			=> 0,
						'jaspel_dokter'		=> 0,
						'urjigd'			=> $asuhan_farmasi_row->uri == 1 ? "URI" : "URJ",
						'tanggal_tagihan'	=> ArrayAdapter::format("date Y-m-d", $asuhan_farmasi_row->tanggal)
					);
					$data_tagihan['list'] = $list;
				}
				$consumer_service = new ServiceConsumer(
					$db,
					"proceed_receivable",
					$data_tagihan,
					"kasir"
				);
				$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
				$consumer_service->execute();
			}
		}
	}

	echo json_encode($data);
	return;
}

$modal = new Modal("asuhan_farmasi_add_form", "smis_form_container", "asuhan_farmasi");
$modal->setTitle("Data Asuhan Farmasi");
$id_hidden = new Hidden("asuhan_farmasi_id", "asuhan_farmasi_id", "");
$modal->addElement("", $id_hidden);
$tanggal_text = new Text("asuhan_farmasi_tanggal", "asuhan_farmasi_tanggal", "");
$tanggal_text->setAtribute("disabled='disabled'");
$modal->addElement("Tanggal", $tanggal_text);
$noreg_pasien_text = new Text("asuhan_farmasi_noreg_pasien", "asuhan_farmasi_noreg_pasien", "");
$noreg_pasien_text->setAtribute("disabled='disabled'");
$noreg_pasien_text->setClass("smis-one-option-input");
$pasien_button = new Button("", "", "Pilih");
$pasien_button->setAtribute("id='pasien_btn'");
$pasien_button->setClass("btn-info");
$pasien_button->setIsButton(Button::$ICONIC);
$pasien_button->setIcon("icon-white " . Button::$icon_list_alt);
$pasien_button->setAction("pasien.chooser('pasien', 'pasien_button', 'pasien', pasien, 'Pasien')");
$pasien_input_group = new InputGroup("");
$pasien_input_group->addComponent($noreg_pasien_text);
$pasien_input_group->addComponent($pasien_button);
$modal->addElement("No. Reg.", $pasien_input_group);
$nrm_pasien_text = new Text("asuhan_farmasi_nrm_pasien", "asuhan_farmasi_nrm_pasien", "");
$nrm_pasien_text->setAtribute("disabled='disabled'");
$modal->addElement("No. RM", $nrm_pasien_text);
$nama_pasien_text = new Text("asuhan_farmasi_nama_pasien", "asuhan_farmasi_nama_pasien", "");
$nama_pasien_text->setAtribute("disabled='disabled'");
$modal->addElement("Nama Pasien", $nama_pasien_text);

$tindakan_text = new Text("asuhan_farmasi_nama_tindakan", "asuhan_farmasi_nama_tindakan", "");
$tindakan_text->setAtribute("disabled='disabled'");
$tindakan_text->setClass("smis-one-option-input");
$tindakan_button = new Button("", "", "Pilih");
$tindakan_button->setAtribute("id='tindakan_btn'");
$tindakan_button->setClass("btn-info");
$tindakan_button->setIsButton(Button::$ICONIC);
$tindakan_button->setIcon("icon-white " . Button::$icon_list_alt);
$tindakan_button->setAction("tindakan_farmasi.chooser('tindakan_farmasi', 'tindakan_farmasi_button', 'tindakan_farmasi', tindakan_farmasi, 'Tindakan Farmasi')");
$tindakan_input_group = new InputGroup("");
$tindakan_input_group->addComponent($tindakan_text);
$tindakan_input_group->addComponent($tindakan_button);
$modal->addElement("Tindakan", $tindakan_input_group);

$biaya_text = new Text("asuhan_farmasi_biaya", "asuhan_farmasi_biaya", "");
$biaya_text->setAtribute("disabled='disabled'");
$modal->addElement("Biaya", $biaya_text);
$ruangan_service_consumer = new ServiceConsumer(
	$db,
	"get_urjip",
	array()
);
$ruangan_service_consumer->setMode(ServiceConsumer::$MULTIPLE_MODE);
$content = $ruangan_service_consumer->execute()->getContent();
$ruangan_option = new OptionBuilder();
foreach ($content as $autonomous => $ruang) {
	foreach ($ruang as $nama_ruang => $jip) {
		if ($jip[$nama_ruang] == "URJ" || $jip[$nama_ruang] == "UP" || $jip[$nama_ruang] == "URJI" || $jip[$nama_ruang] == "URI") {
			$ruangan_option->add(ArrayAdapter::format("unslug", $nama_ruang), $nama_ruang);
			$num++;
		}
	}
}
$ruangan_option->add("-", "", "1");
$ruangan_select = new Select("asuhan_farmasi_ruangan", "asuhan_farmasi_ruangan", $ruangan_option->getContent());
$modal->addElement("Ruangan", $ruangan_select);
$nama_petugas_text = new Text("asuhan_farmasi_nama_petugas", "asuhan_farmasi_nama_petugas", "");
$nama_petugas_text->setAtribute("disabled='disabled'");
$nama_petugas_text->setClass("smis-one-option-input");
$petugas_button = new Button("", "", "Pilih");
$petugas_button->setAtribute("id='petugas_btn'");
$petugas_button->setClass("btn-info");
$petugas_button->setIsButton(Button::$ICONIC);
$petugas_button->setIcon("icon-white " . Button::$icon_list_alt);
$petugas_button->setAction("petugas.chooser('petugas', 'petugas_button', 'petugas', petugas, 'Petugas')");
$petugas_input_group = new InputGroup("");
$petugas_input_group->addComponent($nama_petugas_text);
$petugas_input_group->addComponent($petugas_button);
$modal->addElement("Petugas", $petugas_input_group);
$id_petugas_hidden = new Hidden("asuhan_farmasi_id_petugas", "asuhan_farmasi_id_petugas", "");
$modal->addElement("", $id_petugas_hidden);
$keterangan_textarea = new TextArea("asuhan_farmasi_keterangan", "asuhan_farmasi_keterangan", "");
$keterangan_textarea->setLine("2");
$modal->addElement("Keterangan", $keterangan_textarea);
global $user;
$operator_hidden = new Hidden("asuhan_farmasi_operator", "asuhan_farmasi_operator", $user->getNameOnly());
$modal->addElement("", $operator_hidden);

$save_button = new Button("", "", "Simpan");
$save_button->setClass("btn-success");
$save_button->setAction("asuhan_farmasi.save()");
$save_button->setIcon("fa fa-floppy-o");
$save_button->setIsButton(Button::$ICONIC);
$modal->addFooter($save_button);

echo $modal->getHtml();
echo $table->getHtml();
echo addCSS("depo_farmasi_irja_lt1/css/penjualan_resep.css", false);
echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PasienAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PasienAction.prototype.constructor = PasienAction;
	PasienAction.prototype = new TableAction();
	PasienAction.prototype.chooser = function(modal, elm_id, param, action, modal_title) {
		if ($(".btn").attr("disabled") == "disabled")
			return;
		$(".btn").removeAttr("disabled");
		$(".btn").attr("disabled", "disabled");
		var data = this.getRegulerData();
		var dt = this.addChooserData(data);
		var self = this;
		data['super_command'] = param;
		$.post(
			'',
			dt,
			function(res) {
				show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
				action.view();
				action.focusSearch();
				this.current_chooser = action;
				CURRENT_SMIS_CHOOSER = action;
				$(".btn").removeAttr("disabled");
			}
		);
	};
	PasienAction.prototype.selected = function(json) {
		$("#asuhan_farmasi_noreg_pasien").val(json.id);
		$("#asuhan_farmasi_nrm_pasien").val(json.nrm);
		$("#asuhan_farmasi_nama_pasien").val(json.sebutan + " " + json.nama_pasien);
	};
	PasienAction.prototype.addViewData = function(d) {
		d['tanggal'] = $("#search_tanggal").val();
		d['noreg_pasien'] = $("#search_noreg").val();
		d['nrm_pasien'] = $("#search_nrm").val();
		d['nama_pasien'] = $("#search_nama").val();
		d['alamat_pasien'] = $("#search_alamat").val();
		d['last_nama_ruangan'] = $("#search_last_ruangan").val();
		d['jenis_pasien'] = $("#search_carabayar").val();
		d['perusahaan_pasien'] = $("#search_perusahaan").val();
		d['asuransi_pasien'] = $("#search_asuransi").val();
		return d;
	};

	function PetugasAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PetugasAction.prototype.constructor = PetugasAction;
	PetugasAction.prototype = new TableAction();
	PetugasAction.prototype.selected = function(json) {
		$("#asuhan_farmasi_id_petugas").val(json.id);
		$("#asuhan_farmasi_nama_petugas").val(json.nama);
	};

	function TindakanFarmasiAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	TindakanFarmasiAction.prototype.constructor = TindakanFarmasiAction;
	TindakanFarmasiAction.prototype = new TableAction();
	TindakanFarmasiAction.prototype.selected = function(json) {
		$("#asuhan_farmasi_nama_tindakan").val(json.nama_tindakan);
		$("#asuhan_farmasi_biaya").val(json.tarif);
	};

	var asuhan_farmasi;
	var pasien;
	var petugas;
	var tindakan_farmasi;

	$(document).ready(function() {
		$("*").dblclick(function(e) {
			e.preventDefault();
		});
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "PETUGAS" ||
				$("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("full_model");
				if ($("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
					$("table#table_pasien tfoot tr").eq(0).hide();
				} else {
					$("table#table_pasien tfoot tr").eq(0).show();
				}
			} else {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
			}
		});
		pasien = new PasienAction(
			"pasien",
			"depo_farmasi_irja_lt1",
			"asuhan_farmasi",
			new Array()
		);
		pasien.setSuperCommand("pasien");

		petugas = new PetugasAction(
			"petugas",
			"depo_farmasi_irja_lt1",
			"asuhan_farmasi",
			new Array()
		);
		petugas.setSuperCommand("petugas");

		tindakan_farmasi = new TindakanFarmasiAction(
			"tindakan_farmasi",
			"depo_farmasi_irja_lt1",
			"asuhan_farmasi",
			new Array()
		);
		tindakan_farmasi.setSuperCommand("tindakan_farmasi");

		asuhan_farmasi = new TableAction(
			"asuhan_farmasi",
			"depo_farmasi_irja_lt1",
			"asuhan_farmasi",
			new Array("id", "tanggal", "noreg_pasien", "nrm_pasien", "nama_pasien", "nama_tindakan", "biaya", "ruangan", "keterangan", "id_petugas", "nama_petugas", "operator")
		);
		asuhan_farmasi.checkClosingBill = function() {
			var data = {
				page: this.page,
				action: this.action,
				super_command: this.super_command,
				prototype_name: this.prototype_name,
				prototype_slug: this.prototype_slug,
				prototype_implement: this.prototype_implement
			};
			var noreg_pasien = $("#asuhan_farmasi_noreg_pasien").val();
			if (noreg_pasien == "") {
				smis_alert("Peringatan", "No. Reg. Pasien harus dipilih.", "alert-danger");
				return;
			}
			data['command'] = "check_closing_bill";
			data['noreg_pasien'] = noreg_pasien;

			var response = $.ajax({
				'type': "POST",
				'url': "",
				'data': data,
				'async': false
			}).responseText;

			var json = getContent(response);
			if (json == "1")
				return false;
			else
				return true;
		};
		asuhan_farmasi.edit = function(id) {
			var self = this;
			showLoading();
			var data_t = self.getRegulerData();
			data_t['command'] = "edit";
			data_t['id'] = id;
			$.post(
				"",
				data_t,
				function(response_t) {
					var json_t = getContent(response_t);
					if (json_t == null) return;
					var cb_data = self.getRegulerData();
					cb_data['super_command'] = "retur";
					cb_data['command'] = "check_closing_bill";
					cb_data['noreg_pasien'] = json_t.noreg_pasien;
					$.post(
						"",
						cb_data,
						function(response_cb) {
							var json_cb = getContent(response_cb);
							if (json_cb == null) {
								dismissLoading();
								return;
							}
							if (json_cb == "0") {
								TableAction.prototype.edit.call(self, id);
								dismissLoading();
							} else {
								bootbox.alert("Tagihan pasien tidak dapat diubah karena tagihan pasien sudah ditutup. Silakan menghubungi pihak Kasir.");
								dismissLoading();
							}
						}
					);
				}
			);
		};
		asuhan_farmasi.del = function(id) {
			var self = this;
			bootbox.confirm(
				"Yakin membatalkan Asuhan Farmasi ini?",
				function(result) {
					if (result) {
						showLoading();
						var data_t = self.getRegulerData();
						data_t['command'] = "edit";
						data_t['id'] = id;
						$.post(
							"",
							data_t,
							function(response_t) {
								var json_t = getContent(response_t);
								if (json_t == null) return;
								var cb_data = self.getRegulerData();
								cb_data['super_command'] = "retur";
								cb_data['command'] = "check_closing_bill";
								cb_data['noreg_pasien'] = json_t.noreg_pasien;
								$.post(
									"",
									cb_data,
									function(response_cb) {
										var json_cb = getContent(response_cb);
										if (json_cb == null) {
											dismissLoading();
											return;
										}
										if (json_cb == "0") {
											TableAction.prototype.del.call(self, id);
											dismissLoading();
										} else {
											bootbox.alert("Tagihan pasien tidak dapat diubah karena tagihan pasien sudah ditutup. Silakan menghubungi pihak Kasir.");
											dismissLoading();
										}
									}
								);
							}
						);
					}
				}
			);
		};
		asuhan_farmasi.save = function() {
			showLoading();
			var valid = this.checkClosingBill();
			if (valid)
				TableAction.prototype.save.call(this);
			dismissLoading();
		};
		asuhan_farmasi.view();
	});
</script>