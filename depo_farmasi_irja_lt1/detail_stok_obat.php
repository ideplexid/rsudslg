<?php 
	require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");
	require_once("depo_farmasi_irja_lt1/table/PenyesuaianStokTable.php");
	require_once("depo_farmasi_irja_lt1/adapter/PenyesuaianStokAdapter.php");
	require_once("depo_farmasi_irja_lt1/responder/PenyesuaianStokDBResponder.php");

	$penyesuaian_stok_table = new PenyesuaianStokTable(
		array("Kode Obat", "Nama Obat", "Jenis Obat", "Jenis Stok", "Stok", "Produsen", "Tgl. Exp.", "No. BBM", "Tgl. Masuk"),
		"",
		null,
		true
	);
	$penyesuaian_stok_table->setName("penyesuaian_stok");
	$penyesuaian_stok_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$penyesuaian_stok_adapter = new PenyesuaianStokAdapter();
		$columns = array("id", "id_stok_obat", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan", "nama_user");
		$penyesuaian_stok_dbtable = new DBTable(
			$db,
			InventoryLibrary::$_TBL_PENYESUAIAN_STOK_OBAT,
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (a.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT a.*
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' " . $filter . "
			ORDER BY a.nama_obat, a.id, a.tanggal_exp ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$penyesuaian_stok_dbresponder = new PenyesuaianStokDBResponder(
			$penyesuaian_stok_dbtable,
			$penyesuaian_stok_table,
			$penyesuaian_stok_adapter
		);
		if ($penyesuaian_stok_dbresponder->is("save")) {
			global $user;
			$penyesuaian_stok_dbresponder->addColumnFixValue("nama_user", $user->getName());
		}
		$data = $penyesuaian_stok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$penyesuaian_stok_modal = new Modal("penyesuaian_stok_add_form", "smis_form_container", "penyesuaian_stok");
	$penyesuaian_stok_modal->setTitle("Penyesuaian Stok Obat");
	$id_stok_obat_hidden = new Hidden("penyesuaian_stok_id_stok_obat", "penyesuaian_stok_id_stok_obat", "");
	$penyesuaian_stok_modal->addElement("", $id_stok_obat_hidden);
	$tanggal_text = new Text("penyesuaian_stok_tanggal", "penyesuaian_stok_tanggal", date("Y-m-d"));
	$tanggal_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Tanggal", $tanggal_text);
	$nama_obat_text = new Text("penyesuaian_stok_nama_obat", "penyesuaian_stok_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Nama Obat", $nama_obat_text);
	$nama_jenis_text = new Text("penyesuaian_stok_nama_jenis", "penyesuaian_stok_nama_jenis", "");
	$nama_jenis_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jenis Obat", $nama_jenis_text);
	$jenis_stok_text = new Text("penyesuaian_stok_jenis_stok", "penyesuaian_stok_jenis_stok", "");
	$jenis_stok_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jenis Stok", $jenis_stok_text);
	$produsen_text = new Text("penyesuaian_stok_produsen", "penyesuaian_stok_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Produsen", $produsen_text);
	$no_bbm_text = new Text("penyesuaian_stok_no_bbm", "penyesuaian_stok_no_bbm", "");
	$no_bbm_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("No. BBM", $no_bbm_text);
	$tanggal_datang_text = new Text("penyesuaian_stok_tanggal_datang", "penyesuaian_stok_tanggal_datang", "");
	$tanggal_datang_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Tgl. Masuk", $tanggal_datang_text);
	$f_jumlah_lama_text = new Text("penyesuaian_stok_f_jumlah_lama", "penyesuaian_stok_f_jumlah_lama", "");
	$f_jumlah_lama_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jml. Tercatat", $f_jumlah_lama_text);
	$jumlah_lama_hidden = new Hidden("penyesuaian_stok_jumlah_lama", "penyesuaian_stok_jumlah_lama", "");
	$penyesuaian_stok_modal->addElement("", $jumlah_lama_hidden);
	$jumlah_baru_text = new Text("penyesuaian_stok_jumlah_baru", "penyesuaian_stok_jumlah_baru", "");
	$penyesuaian_stok_modal->addElement("Jml. Aktual", $jumlah_baru_text);
	$satuan_text = new text("penyesuaian_stok_satuan", "penyesuaian_stok_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("penyesuaian_stok_keterangan", "penyesuaian_stok_keterangan", "");
	$penyesuaian_stok_modal->addElement("Keterangan", $keterangan_textarea);
	$penyesuaian_stok_button = new Button("", "", "Simpan");
	$penyesuaian_stok_button->setClass("btn-success");
	$penyesuaian_stok_button->setIcon("fa fa-floppy-o");
	$penyesuaian_stok_button->setIsButton(Button::$ICONIC);
	$penyesuaian_stok_button->setAction("penyesuaian_stok.save()");
	$penyesuaian_stok_modal->addFooter($penyesuaian_stok_button);
	
	echo $penyesuaian_stok_modal->getHtml();
	echo $penyesuaian_stok_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("depo_farmasi_irja_lt1/js/penyesuaian_stok_action.js", false);
	echo addJS("depo_farmasi_irja_lt1/js/penyesuaian_stok.js", false);
?>