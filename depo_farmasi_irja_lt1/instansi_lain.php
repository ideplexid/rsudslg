<?php 
	require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");
	require_once("depo_farmasi_irja_lt1/adapter/InstansiLainAdapter.php");
	global $db;

	$instansi_l_table = new Table(
		array("Nomor", "Nama", "Alamat", "No. Telp.", "Margin Penjualan"),
		"Depo Farmasi IRJA Lt. 1 : Daftar Instansi Lain",
		null,
		true
	);
	$instansi_l_table->setName("instansi_l");
	if (isset($_POST['command'])) {
		$instansi_l_adapter = new InstansiLainAdapter();
		$columns = array("id", "nama", "alamat", "telpon", "margin_jual");
		$instansi_l_dbtable = new DBTable(
			$db,
			InventoryLibrary::$_TBL_INSTANSI_LAIN,
			$columns
		);
		$instansi_l_dbtable->setOrder(" id DESC ");
		$instansi_l_dbresponder = new DBResponder(
			$instansi_l_dbtable,
			$instansi_l_table,
			$instansi_l_adapter
		);
		$data = $instansi_l_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	$instansi_l_modal_columns = array("id", "nama", "alamat", "telpon", "margin_jual");
	$instansi_l_modal_names = array("", "Nama", "Alamat", "No. Telp.", "Margin Jual");
	$instansi_l_modal_values = array("", "", "", "", "10");
	$instansi_l_modal_types = array("hidden", "text", "textarea", "text", "text");
	$instansi_l_modal_emptiness = array("y", "n", "n", "n", "n");
	$instansi_l_table->setModal(
		$instansi_l_modal_columns,
		$instansi_l_modal_types,
		$instansi_l_modal_names,
		$instansi_l_modal_values,
		$instansi_l_modal_emptiness
	);
	$instansi_l_modal = $instansi_l_table->getModal();
	$instansi_l_modal->setTitle("Instansi Lain");
	
	echo $instansi_l_modal->getHtml();
	echo $instansi_l_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("depo_farmasi_irja_lt1/js/instansi_lain_action.js", false);
	echo addJS("depo_farmasi_irja_lt1/js/instansi_lain.js", false);
?>