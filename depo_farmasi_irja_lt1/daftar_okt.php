<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
		"Depo Depo Farmasi IRJA Lt. 1 : Daftar OKT",
		true,
		null
	);
	$table->setName("daftar_okt");
	$table->setAction(false);
	$adapter = new SimpleAdapter(true, "No.");
	$adapter->add("ID Obat", "id");
	$adapter->add("Kode Obat", "kode");
	$adapter->add("Nama Obat", "nama");
	$adapter->add("Jenis Obat", "nama_jenis_barang");
	$service_responder = new ServiceResponder(
		$db,
		$table,
		$adapter,
		"daftar_okt"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("daftar_okt", $service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");	
?>
<script type="text/javascript">
	var daftar_okt;
	$(document).ready(function() {
		daftar_okt = new TableAction(
			"daftar_okt",
			"depo_farmasi_irja_lt1",
			"daftar_okt",
			new Array()
		);
		daftar_okt.setSuperCommand("daftar_okt");
		daftar_okt.view();
	});
</script>