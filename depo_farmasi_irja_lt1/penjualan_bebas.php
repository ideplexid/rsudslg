<?php
	global $db;
	require_once("depo_farmasi_irja_lt1/library/InventoryLibrary.php");
	require_once("depo_farmasi_irja_lt1/table/PenjualanBebasTable.php");
	require_once("depo_farmasi_irja_lt1/table/DPenjualanBebasTable.php");
	require_once("depo_farmasi_irja_lt1/adapter/PenjualanBebasAdapter.php");
	require_once("depo_farmasi_irja_lt1/adapter/ObatAdapter.php");
	require_once("depo_farmasi_irja_lt1/responder/PenjualanBebasDBResponder.php");
	require_once("depo_farmasi_irja_lt1/responder/ObatDBResponder.php");
	require_once("depo_farmasi_irja_lt1/responder/SisaDBResponder.php");
	
	$columns = array("No. Penjualan", "No. Resep", "Tanggal/Jam");
	if (getSettings($db, "depo_farmasi6-penjualan_bebas-show_nama", 0) == 1)
		$columns = array("No. Penjualan", "No. Resep", "Nama", "Tanggal/Jam");
	$penjualan_bebas_table = new PenjualanBebasTable(
		$columns,
		"Depo Farmasi IRJA Lt. 1 : Penjualan Umum Bebas"
	);
	$penjualan_bebas_table->setName("penjualan_bebas");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "penjualan_bebas") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "get_margin_penjualan") {
				/// mode margin :
				/// 0	=> paten
				/// 2 	=> per obat
				$mode_margin = $_POST['mode_margin'];
				$margin_penjualan = 0;
				if ($mode_margin == 0)
					$margin_penjualan = getSettings($db, "depo_farmasi6-penjualan_bebas-margin_penjualan", 0) / 100;
				$data = array(
					"margin_penjualan" => $margin_penjualan
				);
				echo json_encode($data);
				return;
			}
			$penjualan_bebas_adapter = new PenjualanBebasAdapter();
			$columns = array("id", "tanggal", "nama_pasien", "nomor_resep", "markup", "total", "dibatalkan", "diskon", "t_diskon", "keterangan_batal");
			$penjualan_bebas_dbtable = new DBTable(
				$db,
				InventoryLibrary::$_TBL_PENJUALAN_RESEP,
				$columns
			);
			$penjualan_bebas_dbtable->addCustomKriteria(" tipe ", " ='bebas' ");
			$penjualan_bebas_dbtable->setOrder(" id DESC ");	
			$penjualan_bebas_dbresponder = new PenjualanBebasDBResponder(
				$penjualan_bebas_dbtable,
				$penjualan_bebas_table,
				$penjualan_bebas_adapter
			);
			if ($penjualan_bebas_dbresponder->isSave()) {
				global $user;
				$penjualan_bebas_dbresponder->addColumnFixValue("operator", $user->getNameOnly());
			}
			$data = $penjualan_bebas_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	//get obat chooser:
	$obat_table = new Table(array("Kode", "Obat", "Jenis", "Stok"));
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new ObatAdapter();
	$obat_dbtable = new DBTable($db, InventoryLibrary::$_TBL_STOK_OBAT);
	$obat_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT v_stok.*, v_harga.hna, v_harga.markup
		FROM (
			SELECT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok', satuan, konversi, satuan_konversi
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, CASE label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.sisa > 0 " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi, label
			) v_obat
			GROUP BY id_obat
		) v_stok LEFT JOIN (
			SELECT id_obat, nama_obat, nama_jenis_obat,  MAX(hna) AS 'hna', 0 AS 'markup', satuan, konversi, satuan_konversi
			FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' " . $filter . "
			GROUP BY id_obat, nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi
		) v_harga ON v_stok.id = v_harga.id_obat AND v_stok.satuan = v_harga.satuan AND v_stok.konversi = v_harga.konversi AND v_stok.satuan_konversi = v_harga.satuan_konversi
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_obat
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	//get sisa, hna, dan markup by id obat, satuan, konversi, satuan_konversi:
	$sisa_table = new Table(array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi"));
	$sisa_table->setName("sisa");
	$sisa_adapter = new SimpleAdapter();
	$sisa_adapter->add("id_obat", "id_obat");
	$sisa_adapter->add("sisa", "sisa");
	$sisa_adapter->add("satuan", "satuan");
	$sisa_adapter->add("konversi", "konversi");
	$sisa_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
	$sisa_dbtable = new DBTable(
		$db,
		InventoryLibrary::$_TBL_STOK_OBAT,
		$columns
	);
	$sisa_dbresponder = new SisaDBResponder(
		$sisa_dbtable,
		$sisa_table,
		$sisa_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_dbresponder);
	$super_command->addResponder("sisa", $sisa_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$penjualan_bebas_modal = new Modal("penjualan_bebas_add_form", "smis_form_container", "penjualan_bebas");
	$penjualan_bebas_modal->setTitle("Data Penjualan Umum Bebas");
	$penjualan_bebas_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("penjualan_bebas_id", "penjualan_bebas_id", "");
	$penjualan_bebas_modal->addElement("", $id_hidden);
	$ppn_hidden = new Hidden("penjualan_bebas_ppn", "penjualan_bebas_ppn", getSettings($db, "depo_farmasi6-penjualan_bebas-ppn_jual", 11));
	$penjualan_bebas_modal->addElement("", $ppn_hidden);
	$nomor_text = new Text("penjualan_bebas_nomor", "penjualan_bebas_nomor", "");
	$nomor_text->addAtribute("autofocus");
	$penjualan_bebas_modal->addElement("Nomor", $nomor_text);
	if (getSettings($db, "depo_farmasi6-penjualan_bebas-show_nama", 0) == 1) {
		$nama_text = new Text("penjualan_bebas_nama", "penjualan_bebas_nama", "");
		$penjualan_bebas_modal->addElement("Nama", $nama_text);
	} else {
		$nama_hidden = new Hidden("penjualan_bebas_nama", "penjualan_bebas_nama", "");
		$penjualan_bebas_modal->addElement("", $nama_hidden);
	}
	$markup_hidden = new Hidden("penjualan_bebas_markup", "penjualan_bebas_markup", "0");
	$penjualan_bebas_modal->addElement("", $markup_hidden);
	$diskon_text = new Text("penjualan_bebas_diskon", "penjualan_bebas_diskon", "0,00");
	$diskon_text->setTypical("money");
	$diskon_text->setAtribute(" data-thousands=\".\" data-decimal=\",\"  data-precision=\"2\" " );
	$penjualan_bebas_modal->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	$t_diskon_option->add("Persen (%)", "persen", "1");
	$t_diskon_option->add("Nominal (Rp)", "nominal");
	$t_diskon_option->add("Gratis (100 %)", "gratis");
	$t_diskon_select = new Select("penjualan_bebas_t_diskon", "penjualan_bebas_t_diskon", $t_diskon_option->getContent());
	$penjualan_bebas_modal->addElement("Tipe Diskon", $t_diskon_select);
	$total_text = new Text("penjualan_bebas_total", "penjualan_bebas_total", "");
	$total_text->setTypical("money");
	$total_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$penjualan_bebas_modal->addElement("Total", $total_text);
	$dpenjualan_bebas_table = new DPenjualanBebasTable(array("No.", "Nama", "Jumlah", "Harga", "Embalase", "Tuslah", "Subtotal", "Apoteker"));
	$dpenjualan_bebas_table->setName("dpenjualan_bebas");
	$dpenjualan_bebas_table->setFooterVisible(false);
	$penjualan_bebas_modal->addBody("dpenjualan_bebas_table", $dpenjualan_bebas_table);
	$penjualan_bebas_button = new Button("", "", "Simpan");
	$penjualan_bebas_button->setClass("btn-success");
	$penjualan_bebas_button->setIcon("fa fa-floppy-o");
	$penjualan_bebas_button->setIsButton(Button::$ICONIC);
	$penjualan_bebas_button->setAtribute("id='penjualan_bebas_save'");
	$penjualan_bebas_modal->addFooter($penjualan_bebas_button);
	$penjualan_bebas_button = new Button("", "", "OK");
	$penjualan_bebas_button->setClass("btn-success");
	$penjualan_bebas_button->setAtribute("id='penjualan_bebas_ok'");
	$penjualan_bebas_button->setAction("$($(this).data('target')).smodal('hide')");
	$penjualan_bebas_modal->addFooter($penjualan_bebas_button);
	$tombol='<a href="#" class="input btn btn-info" ><i class="icon-white icon-list-alt"></i></a>';
	$penjualan_bebas_modal->addHTML("
		<div class='alert alert-block alert-inverse' id='help_bebas'>
			<h4>Tips</h4>
			<ul>
				<li>Tombol <kbd>Tab</kbd> : Berpindah Cepat ke Isian Berikutnya (dari Kiri ke Kanan)</li>
				<li>Tombol <kbd>&uarr;</kbd> / <kbd>&darr;</kbd> : Memilih <strong>Persen (%) / Nominal (Rp) / Gratis (100 %)</strong> pada Isian <strong>Tipe Diskon</strong></li>
				<li>Tombol <kbd>F3</kbd> : Tombol Cepat Menambahkan Obat Jadi Baru</li>
				<li>Tombol <kbd>F6</kbd> : Tombol Cepat Menyimpan Data Penjualan Bebas, Pastikan Semua Obat/Racikan dan Informasi Kepala Transaksi Sudah Lengkap.</li>
			<ul>
		</div>
	", "after");
	
	//obat jadi modal:
	$obat_jadi_modal = new Modal("obat_jadi_add_form", "smis_form_container", "obat_jadi");
	$obat_jadi_modal->setTitle("Data Obat Jadi");
	$id_hidden = new Hidden("obat_jadi_id", "obat_jadi_id", "");
	$obat_jadi_modal->addElement("", $id_hidden);
	$id_obat_hidden = new Hidden("obat_jadi_id_obat", "obat_jadi_id_obat", "");
	$obat_jadi_modal->addElement("", $id_obat_hidden);
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$obat_button->setIcon("icon-white icon-list-alt");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setAtribute("id='obat_jadi_browse'");
	$nama_obat_text = new Text("obat_jadi_nama_obat", "obat_jadi_nama_obat", "");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_text->addAtribute("autofocus");
	$nama_obat_input_group = new InputGroup("");
	$nama_obat_input_group->addComponent($nama_obat_text);
	$nama_obat_input_group->addComponent($obat_button);
	$obat_jadi_modal->addElement("Obat", $nama_obat_input_group);
	$kode_obat_hidden = new Hidden("obat_jadi_kode_obat", "obat_jadi_kode_obat", "");
	$obat_jadi_modal->addElement("", $kode_obat_hidden);
	$name_obat_hidden = new Hidden("obat_jadi_name_obat", "obat_jadi_name_obat", "");
	$obat_jadi_modal->addElement("", $name_obat_hidden);
	$nama_jenis_obat_hidden = new Hidden("obat_jadi_nama_jenis_obat", "obat_jadi_nama_jenis_obat", "");
	$obat_jadi_modal->addElement("", $nama_jenis_obat_hidden);
	$satuan_select = new Select("obat_jadi_satuan", "obat_jadi_satuan", "");
	$obat_jadi_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("obat_jadi_konversi", "obat_jadi_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$obat_jadi_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("obat_jadi_satuan_konversi", "obat_jadi_satuan_konversi", "");
	$obat_jadi_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("obat_jadi_stok", "obat_jadi_stok", "");
	$obat_jadi_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("obat_jadi_f_stok", "obat_jadi_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$obat_jadi_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("obat_jadi_jumlah_lama", "obat_jadi_jumlah_lama", "");
	$obat_jadi_modal->addElement("", $jumlah_lama_hidden);
	$hna_text = new Text("obat_jadi_hna", "obat_jadi_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$obat_jadi_modal->addElement("HJA", $hna_text);
	if (getSettings($db, "depo_farmasi6-penjualan_bebas-embalase_show-obat_jadi", 0) == 1) {
		$embalase_text = new Text("obat_jadi_embalase", "obat_jadi_embalase", "");
		$embalase_text->setTypical("money");
		$embalase_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$obat_jadi_modal->addElement("Embalase", $embalase_text);
	} else {
		$embalase_hidden = new Hidden("obat_jadi_embalase", "obat_jadi_embalase", "");
		$obat_jadi_modal->addElement("", $embalase_hidden);
	}
	if (getSettings($db, "depo_farmasi6-penjualan_bebas-tuslah_show-obat_jadi", 0) == 1) {
		$tuslah_text = new Text("obat_jadi_tuslah", "obat_jadi_tuslah", "");
		$tuslah_text->setTypical("money");
		$tuslah_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"" );
		$obat_jadi_modal->addElement("Tusla", $tuslah_text);
	} else {
		$tuslah_hidden = new Hidden("obat_jadi_tuslah", "obat_jadi_tuslah", "");
		$obat_jadi_modal->addElement("", $tuslah_hidden);
	}
	$markup_hidden = new Hidden("obat_jadi_markup", "obat_jadi_markup", "");
	$obat_jadi_modal->addElement("", $markup_hidden);
	$jumlah_text = new Text("obat_jadi_jumlah", "obat_jadi_jumlah", "");
	$obat_jadi_modal->addElement("Jumlah", $jumlah_text);
	$obat_jadi_button = new Button("", "", "Simpan");
	$obat_jadi_button->setClass("btn-success");
	$obat_jadi_button->setAtribute("id='obat_jadi_save'");
	$obat_jadi_button->setIcon("fa fa-floppy-o");
	$obat_jadi_button->setIsButton(Button::$ICONIC);
	$obat_jadi_modal->addFooter($obat_jadi_button);
	$obat_jadi_modal->addHTML("
		<div class='alert alert-block alert-inverse'>
			<h4>Tips</h4>
			<ul>
				<li><small>Ketik Min. <strong>3 Karakter</strong> / Tombol " . $tombol . " / Tombol <kbd>F2</kbd> : Menentukan Nama Obat</small></li>
				<li>Tombol <kbd>Esc</kbd> : Tombol Cepat Kembali ke Formulir Penjualan Bebas</li>
			</ul>
		</div>
	", "before");
	
	echo "<div class='alert alert-block alert-inverse'>" .
			 "<h4>Tips</h4>" .
			 "Kombinasi Tombol <kbd>F2</kbd> : Menampilkan Formulir Penjualan Bebas Baru" .
		 "</div>";
	echo $obat_jadi_modal->getHtml();
	echo $penjualan_bebas_modal->getHtml();
	echo $penjualan_bebas_table->getHtml();
	
	echo addCSS("depo_farmasi_irja_lt1/css/penjualan_bebas.css",false);
	echo addJS("depo_farmasi_irja_lt1/js/penjualan_bebas_action.js",false);
	echo addJS("depo_farmasi_irja_lt1/js/dpenjualan_bebas_action.js",false);
	echo addJS("depo_farmasi_irja_lt1/js/obat_action.js",false);
	echo addJS("smis-base-js/smis-base-shortcut.js",false);	
	echo addJS("smis-libs-out/webprint/webprint.js", false);
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var embalase_obat_jadi = <?php echo getSettings($db, "depo_farmasi6-penjualan_bebas-embalase-obat_jadi", 0); ?>;
	var tuslah_obat_jadi = <?php echo getSettings($db, "depo_farmasi6-penjualan_bebas-tuslah-obat_jadi", 0); ?>;

	/// mode margin :
	/// 0	=> paten
	/// 2 	=> per obat
	var mode_margin = <?php echo getSettings($db, "depo_farmasi6-penjualan_bebas-mode_margin_penjualan", 0); ?>;
	var need_margin_penjualan_request = 1;

	var dpenjualan_bebas_num;
	var penjualan_bebas;
	var obat;
	$(document).ready(function() {
		$('.modal').on('shown.bs.modal', function() {
			$(this).find('[autofocus]').focus();
		});
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("*").dblclick(function(e) {
			e.preventDefault();
		})
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "OBAT" || 
				$("#smis-chooser-modal .modal-header h3").text() == "BAHAN" ||
				$("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").addClass("half_model");
			} else {
				$("#smis-chooser-modal").removeClass("half_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
		});
		$("#penjualan_bebas_t_diskon").on("change", function() {
			var diskon = $("#penjualan_bebas_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#penjualan_bebas_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_penjualan_bebas_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			}
			$("#modal_alert_penjualan_bebas_add_form").html("");
			if ($("#penjualan_bebas_t_diskon").val() == "gratis") {
				$("#penjualan_bebas_diskon").val("100,00");
				$("#penjualan_bebas_diskon").removeAttr("disabled");
				$("#penjualan_bebas_diskon").attr("disabled", "disabled");
			} else {
				$("#penjualan_bebas_diskon").val("0,00");
				$("#penjualan_bebas_diskon").removeAttr("disabled");
			}
			penjualan_bebas.refreshBiayaTotal();
		});
		$("#penjualan_bebas_diskon").on("keyup", function() {
			var diskon = $("#penjualan_bebas_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var t_diskon = $("#penjualan_bebas_t_diskon").val();
			if (diskon > 100 && t_diskon == "persen") {
				$("#modal_alert_penjualan_bebas_add_form").html(
					"<div class='alert alert-block alert-danger'>" +
						"<h4>Peringatan</h4>" +
						"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
					"</div>"
				);
				return;
			} 
			$("#modal_alert_penjualan_bebas_add_form").html("");
			penjualan_bebas.refreshBiayaTotal();
		});
		$("#penjualan_bebas_diskon").on("change", function() {
			var diskon = $("#penjualan_bebas_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			if (diskon == "") {
				$("#penjualan_bebas_diskon").val("0,00");
			}
		});
		obat = new ObatAction(
			"obat",
			"depo_farmasi_irja_lt1",
			"penjualan_bebas",
			new Array()
		);
		obat.setSuperCommand("obat");
		obat.setDetailInfo = function() {
			var part = $("#obat_jadi_satuan").val().split("_");
			$("#obat_jadi_konversi").val(part[0]);
			$("#obat_jadi_satuan_konversi").val(part[1]);
			var data = this.getRegulerData();
			data['super_command'] = "sisa";
			data['command'] = "edit";
			data['id_obat'] = $("#obat_jadi_id_obat").val();
			data['satuan'] = $("#obat_jadi_satuan").find(":selected").text();
			data['konversi'] = $("#obat_jadi_konversi").val();
			data['satuan_konversi'] = $("#obat_jadi_satuan_konversi").val();
			data['ppn'] = $("#penjualan_bebas_ppn").val();
			$.post(
				"",
				data,
				function(response) {
					var json = getContent(response);
					if (json == null) return;
					$("#obat_jadi_stok").val(json.sisa);
					$("#obat_jadi_f_stok").val(json.sisa + " " + json.satuan);
					var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
					hna = "Rp. " + (parseFloat(hna)).formatMoney("2", ".", ",");
					$("#obat_jadi_hna").val(hna);
					$("#obat_jadi_markup").val(json.markup);
					$("#obat_jadi_warning_system").val(json.warning_system);
					$("#obat_jadi_strict_mode").val(json.strict_mode);
					$("#obat_jadi_jumlah").focus();
				}
			);
		};
		var dpenjualan_bebas_columns = new Array("id", "id_penjualan_bebas", "nama_pasien", "hja", "subtotal");
		dpenjualan_bebas = new DPenjualanBebasAction(
			"dpenjualan_bebas",
			"depo_farmasi_irja_lt1",
			"penjualan_bebas",
			dpenjualan_bebas_columns
		);
		var penjualan_bebas_columns = new Array("id", "tanggal", "markup", "total", "dibatalkan", "diskon", "t_diskon");
		penjualan_bebas = new PenjualanBebasAction(
			"penjualan_bebas",
			"depo_farmasi_irja_lt1",
			"penjualan_bebas",
			penjualan_bebas_columns
		);
		penjualan_bebas.setSuperCommand("penjualan_bebas");
		penjualan_bebas.view();
		var obat_jadi = new TableAction(
			"obat_jadi",
			"depo_farmasi_irja_lt1",
			"penjualan_bebas",
			new Array("tuslah", "embalase")
		);
		//typeahead handlers section:
		var obat_jadi_nama = obat.getViewData();
		$('#obat_jadi_nama_obat').typeahead({
			minLength:3,
			source: function (query, process) {
				var $items = new Array;
				$items = [""];                
				obat_jadi_nama['kriteria']=$('#obat_jadi_nama_obat').val();
				$.ajax({
					url: '',
					type: 'POST',
					data: obat_jadi_nama,
					success: function(res) {
						var json=getContent(res);
						var the_data_proses=json.dbtable.data;
						$items = [""];      				
						$.map(the_data_proses, function(data){
							var group;
							group = {
								id: data.id,
								name: data.nama_obat,                            
								kode: data.kode_obat,
								toString: function () {
									return JSON.stringify(this);
								},
								toLowerCase: function () {
									return this.name.toLowerCase();
								},
								indexOf: function (string) {
									return String.prototype.indexOf.apply(this.name, arguments);
								},
								replace: function (string) {
									var value = '';
									value +=  this.kode + " - " + this.name;
									if(typeof(this.level) != 'undefined') {
										value += ' <span class="pull-right muted">';
										value += this.level;
										value += '</span>';
									}
									return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
								}
							};
							$items.push(group);
						});
						process($items);
					}
				});
			},
			updater: function (item) {
				var item = JSON.parse(item);  
				obat.select(item.id);  
				$("#obat_jadi_jumlah").focus();       
				return item.name;
			}
		});
		$("#penjualan_bebas_nomor").keypress(function(e) {
			if(e.which == 13) {
				$('#penjualan_bebas_diskon').focus();
			}
		});
		$("#penjualan_bebas_diskon").keypress(function(e) {
			if(e.which == 13) {
				$('#penjualan_bebas_t_diskon').focus();
			}
		});
		$("#obat_jadi_nama_obat").keypress(function(e) {
			if(e.which == 13) {
				$('#obat_jadi_satuan').focus();
			}
		});
		$("#obat_jadi_satuan").keypress(function(e) {
			e.preventDefault();
			if(e.which == 13) {
				$('#obat_jadi_jumlah').focus();
			}
		});
		$("#obat_racikan_nama").keypress(function(e) {
			if(e.which == 13) {
				$('#obat_racikan_nama_apoteker').focus();
			}
		});
		$("#obat_jadi_jumlah").keypress(function(e) {
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			if(e.which == 13) {
				$('#obat_jadi_save').trigger('click');
			}
		});
		shortcut.add("F2", function() {
			if(!$('#penjualan_bebas_add_form').hasClass('in') && !$('#obat_jadi_add_form').hasClass('in') && !$("#smis-chooser-modal").hasClass('in')) {
				$("#penjualan_bebas_add").trigger("click");
			} else if ($('#obat_jadi_add_form').hasClass('in')) {
				$("#obat_jadi_browse").trigger('click');
			}
		});
		shortcut.add("F3", function() {
			if($('#penjualan_bebas_add_form').hasClass('in')) {
				$("#obat_jadi_add").trigger('click');
			}
		});
		shortcut.add("F6", function() {
			if($('#penjualan_bebas_add_form').hasClass('in')) {
				$("#penjualan_bebas_save").trigger('click');
			} else if($("#obat_jadi_add_form").hasClass('in')) {
				$("#obat_jadi_save").trigger('click');
			}
		});
		penjualan_bebas.refreshHargaAndSubtotal();
	});
</script>