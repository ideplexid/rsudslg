<?php
    require_once("smis-base/smis-include-service-consumer.php");
    require_once("depo_farmasi_ok/library/InventoryLibrary.php");

    class ReturObatPerPasienDBResponder extends DBResponder {
        public function command($command) {
            if ($command != "print_retur" && $command != "check_closing_bill") {
                return parent::command($command);
            }
            $pack = null;
            if ($command == "print_retur") {
                $id['id'] = $_POST['id'];
                $data['tercetak'] = 1;
                $this->dbtable->update($data, $id);
                $pack = new ResponsePackage();
                $content = $this->print_retur();
                $pack->setContent($content);
                $pack->setStatus(ResponsePackage::$STATUS_OK);
            } else if ($command == "check_closing_bill") {
                $pack = new ResponsePackage();
                $result = $this->checkClosingBill();
                if ($result == "1") {
                    $pack->setAlertVisible(true);
                    $pack->setAlertContent("Peringatan", "<strong>Tagihan Pasien Sudah Ditutup</strong>, </br>Tidak dapat melakukan perubahan tagihan pada pasien. Silakan menghubungi pihak Kasir.",Alert::$DANGER);
                }
                $pack->setContent($result);
                $pack->setStatus(ResponsePackage::$STATUS_OK);
            }
            return $pack->getPackage();
        }

        private function print_retur() {
            $id = $_POST['id'];
            $data = $this->dbtable->get_row("
                SELECT
                    id, tanggal, persentase_retur
                FROM
                    " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . "
                WHERE
                    id = '" . $id . "'
            ");
            $dokter_arr = array();
            $nomor_resep_arr = array();
            $noreg_pasien = "";
            $nrm_pasien = "";
            $nama_pasien = "";
            $alamat_pasien = "";
            $nomor_resep = "";
            $obat_jadi_sql = "
                SELECT 
                    b.nama_obat AS 'nama', a.jumlah, b.satuan, a.harga, a.subtotal, c.nomor_resep, c.nama_dokter, c.nama_pasien, c.alamat_pasien
                FROM 
                    " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a
                        LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
                        LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON a.id_penjualan_resep = c.id
                WHERE 
                    a.prop NOT LIKE 'del' 
                        AND a.id_retur_penjualan_resep = '" . $id . "'
            ";
            $obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
            if ($obat_jadi_data != null) {
                foreach ($obat_jadi_data as $ojd) {
                    if ($noreg_pasien == "")
                        $noreg_pasien = $ojd->noreg_pasien;
                    if ($nrm_pasien == "")
                        $nrm_pasien = $ojd->nrm_pasien;
                    if ($nama_pasien == "")
                        $nama_pasien = $ojd->nama_pasien;
                    if ($alamat_pasien == "")
                        $alamat_pasien = $ojd->alamat_pasien;
                    $dokter_arr[$ojd->nama_dokter] = $ojd->nama_dokter;
                    $nomor_resep_arr[$ojd->nomor_resep] = $ojd->nomor_resep;
                }
                if (count($nomor_resep_arr) > 0) {
                    $i = 1;
                    foreach ($nomor_resep_arr as $nr) {
                        $nomor_resep .= $nr;
                        if ($i < count($nomor_resep_arr))
                            $nomor_resep .= ", ";
                        $i++;
                    }
                }
            }
            $content = "<table border='0' width='100%' cellpadding='10' class='etiket_header'>";
                $content .= "<tr>";
                    $content .= "<td>";
                        $content .= "<div align='center'><table border='0' class='etiket_content' >";
                            $content .= "<tr>";
                                $content .= "<td colspan='5' align='center' style='font-size: 13px !important;'><b><u>DEPO FARMASI " . ucwords(getSettings($this->dbtable->get_db(), "smis_autonomous_title", "")) . " - RETUR RESEP PASIEN</u></b></td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td colspan='5'></td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td colspan='1'>No. Retur</td>";
                                $content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $data->id) . "</td>";
                                $content .= "<td colspan='1' class='tpadding'>No. Resep</td>";
                                $content .= "<td colspan='2'>: " .  $nomor_resep . "</td>";
                            $content .= "</tr>";
                            if (count($dokter_arr) > 0) {
                                $first_row = true;
                                foreach ($dokter_arr as $da) {
                                    if ($first_row) {
                                        $content .= "<tr>";
                                            $content .= "<td colspan='1'>Tgl. Retur</td>"; 
                                            $content .= "<td colspan='1'>: " . ArrayAdapter::format("date d-m-Y H:i", $data->tanggal) . "</td>"; 
                                            $content .= "<td colspan='1' class='tpadding'>Nama Dokter</td>";
                                            $content .= "<td colspan='2'>: " . substr(strtoupper($da), 0, 30) . "</td>";
                                        $content .= "</tr>";
                                        $first_row = false;
                                    } else {
                                        $content .= "<tr>";
                                            $content .= "<td colspan='1'></td>"; 
                                            $content .= "<td colspan='1'></td>"; 
                                            $content .= "<td colspan='1' class='tpadding'></td>";
                                            $content .= "<td colspan='2'>: " . substr(strtoupper($da), 0, 30) . "</td>";
                                        $content .= "</tr>";
                                    }
                                }
                            }
                            if ($noreg_pasien != "" && $nrm_pasien != "") {
                                $content .= "<tr>";
                                    $content .= "<td colspan='1'>Nama Pasien</td>";
                                    $content .= "<td colspan='1'>: " . substr(strtoupper($nama_pasien), 0, 30) . "</td>";
                                    $content .= "<td colspan='1' class='tpadding'>No. Registrasi</td>";
                                    $content .= "<td colspan='2'>: " . ArrayAdapter::format("only-digit8", $noreg_pasien) . "</td>";
                                $content .= "</tr>";
                                $content .= "<tr>";
                                    $content .= "<td colspan='1'>NRM</td>";
                                    $content .= "<td colspan='1'>: " . ArrayAdapter::format("only-digit8", $nrm_pasien) . "</td>";
                                    $content .= "<td colspan='1' class='tpadding'>Alamat Pasien</td>";
                                    $content .= "<td colspan='2'>: " . substr(strtoupper($alamat_pasien), 0, 30) . "</td>";
                                $content .= "</tr>";
                            } else {
                                $content .= "<tr>";
                                    $content .= "<td colspan='1'>Nama Pasien</td>";
                                    $content .= "<td colspan='1'>: " . substr(strtoupper($nama_pasien), 0, 30) . "</td>";
                                    $content .= "<td colspan='1' class='tpadding'>Alamat Pasien</td>";
                                    $content .= "<td colspan='2'>: " . substr(strtoupper($alamat_pasien), 0, 30) . "</td>";
                                $content .= "</tr>";
                            }
                        $content .= "</table></div>";
                    $content .= "</td>";
                $content .= "</tr>";
                $content .= "<tr>";
                    $content .= "<td>";
                        $content .= "<div align='center'><table border='1' class='etiket_content'>";
                        $content .= "<tr align='center'>";
                            $content .= "<td colspan='1'>No.</td>";
                            $content .= "<td colspan='1'>Obat</td>";
                            $content .= "<td colspan='1'>Jumlah</td>";
                            $content .= "<td colspan='1'>Harga Satuan</td>";
                            $content .= "<td colspan='1'>Subtotal</td>";
                        $content .= "</tr>";
                        $no_barang = 1;
                        $total = 0;
                        foreach($obat_jadi_data as $ojd) {
                            $content .= "<tr>";
                                $content .= "<td colspan='1'>" . $no_barang++ . "</td>";
                                $content .= "<td colspan='1'>" . $ojd->nama . "</td>";
                                $content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
                                $content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
                                $content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
                            $content .= "</tr>";
                            $total += $ojd->subtotal;
                        }
                        $content .= "<tr>";
                            $content .= "<td colspan='4' align='right'>Total</td>";
                            $content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
                        $content .= "</tr>";
                        $content .= "<tr>";
                            $content .= "<td colspan='4' align='right'>Diskon</td>";
                            $v_diskon = 0;
                            if ($data->t_diskon == "persen" || $data->t_diskon == "gratis") {
                                $v_diskon = ($total * $data->diskon) / 100;
                                $content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
                            } else {
                                $v_diskon = ($total * $data->diskon) / $data->total;
                                $content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
                            }
                        $content .= "</tr>";
                        $content .= "<tr>";
                            $content .= "<td colspan='4' align='right'>Total Setelah Diskon</td>";
                            $total = $total - $v_diskon;
                            $content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
                        $content .= "</tr>";
                        $content .= "<tr>";
                            $potongan = 100 - $data->persentase_retur;
                            $v_potongan = ($total * $potongan) / 100;
                            $content .= "<td colspan='4' align='right'>Potongan Retur (" . $potongan . " %)</td>";
                            $content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $v_potongan) . "</td>";
                        $content .= "</tr>";
                        $content .= "<tr>";
                            $potongan = 100 - $data->persentase_retur;
                            $content .= "<td colspan='4' align='right'>Uang Kembali</td>";
                            $content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total - $v_potongan) . "</td>";
                        $content .= "</tr>";
                        $content .= "</table></div>";
                    $content .= "</td>";
                $content .= "</tr>";
                $content .= "<tr>";
                    $content .= "<td colspan='5'>";
                        $content .= "<table border='0' align='center' class='etiket_content'>";
                            $content .= "<tr>";
                                $content .= "<td align='center'>PENERIMA,</td>";
                                $content .= "<td>&Tab;</td>";
                                $content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td></td>";
                                $content .= "<td>&Tab;</td>";
                                $content .= "<td></td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td></td>";
                                $content .= "<td>&Tab;</td>";
                                $content .= "<td></td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td></td>";
                                $content .= "<td>&Tab;</td>";
                                $content .= "<td></td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td></td>";
                                $content .= "<td>&Tab;</td>";
                                $content .= "<td></td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td align='center'>(_____________________)</td>";
                                $content .= "<td>&Tab;</td>";
                                global $user;
                                $content .= "<td align='center'>" . $user->getNameOnly() . "</td>";
                            $content .= "</tr>";
                            $content .= "<tr>";
                                $content .= "<td></td>";
                            $content .= "</tr>";
                        $content .= "</table>";
                    $content .= "</td>";
                $content .= "</tr>";
            $content .= "</table>";
            return $content;
        }
        
        private function checkClosingBill() {
            if (getSettings($this->dbtable->get_db(), "depo_farmasi3-lock_tagihan", "0") == "0") {
                return "0";
            }
            $service = new ServiceConsumer($this->getDBTable()->get_db(), "cek_tutup_tagihan", null, "registration");
            $service->addData("noreg_pasien", $_POST['noreg_pasien']);
            $service->execute();
            $result = $service->getContent();
            return $result;
        }
        
        public function save() {
            $header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert header here:
				$header_data['persentase_retur'] = getSettings($this->dbtable->get_db(), "depo_farmasi3-retur_penjualan-persentase_retur", 100);
                $header_data['tanggal'] = date("Y-m-d H:i:s");
                $header_data['id_penjualan_resep'] = 0;
				$result = $this->dbtable->insert($header_data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail'])) {
					//do insert detail here:
					$dretur_penjualan_resep_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP);
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_OBAT);
					$detail = $_POST['detail'];
					foreach($detail as $d) {
                        if ($d['id_penjualan_resep'] > 0) {
                            $penjualan_resep_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_PENJUALAN_RESEP);
                            $resep_data = array();
                            $resep_data['diretur'] = "1";
                            $resep_id['id'] = $d['id_penjualan_resep'];
                            $penjualan_resep_dbtable->update($resep_data, $resep_id);
                        }

						$dretur_data = array();
                        $dretur_data['id_retur_penjualan_resep'] = $id['id'];
                        $dretur_data['id_penjualan_resep'] = $d['id_penjualan_resep'];
                        $dretur_data['id_stok_obat'] = $d['id_stok_obat'];
                        $dretur_data['id_stok_pakai_obat_jadi'] = $d['id_stok_pakai_obat_jadi'];
						$dretur_data['jumlah'] = $d['jumlah_retur'];
						$dretur_data['harga'] = $d['harga_jual'];
						$dretur_data['subtotal'] = $d['jumlah_retur'] * $d['harga_jual'];
						$dretur_penjualan_resep_dbtable->insert($dretur_data);
						$stok_row = $stok_obat_dbtable->get_row("
							SELECT id, sisa
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . "
							WHERE id = '" . $d['id_stok_obat'] . "'
						");
						$stok_data = array();
						$stok_data['sisa'] = $stok_row->sisa + $d['jumlah_retur'];
						$stok_id['id'] = $d['id_stok_obat'];
						$stok_obat_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $d['id_stok_obat'];
						$data_riwayat['jumlah_masuk'] = $d['jumlah_retur'];
						$data_riwayat['sisa'] = $stok_row->sisa + $d['jumlah_retur'];
						$data_riwayat['keterangan'] = "Stok Retur Obat Per Pasien dari Resep (" . ArrayAdapter::format("only-digit8", $d['id_penjualan_resep']) . ")";
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
					}
				}

				$dretur_rows = $this->dbtable->get_result("
					SELECT b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', b.satuan
					FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
					WHERE a.prop NOT LIKE 'del' AND a.id_retur_penjualan_resep = '" . $id['id'] . "'
					GROUP BY b.id_obat, b.satuan
				");
				//logging kartu stok:
				$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
				$keterangan = "No. Reg. Pasien : " . $_POST['noreg_pasien'];
				foreach ($dretur_rows as $drr) {
					$sisa_row = $this->dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
						WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $drr->id_obat . "' AND a.satuan = '" . $drr->satuan . "' AND a.konversi = '1'
					");
					$kartu_stok_data = array(
						"f_id"				=> $id['id'],
						"no_bon"			=> $id['id'],
						"unit"				=> "Retur Obat Per Pasien : " . $keterangan,
						"id_obat"			=> $drr->id_obat,
						"kode_obat"			=> $drr->kode_obat,
						"nama_obat"			=> $drr->nama_obat,
						"nama_jenis_obat"	=> $drr->nama_jenis_obat,
						"tanggal"			=> date("Y-m-d"),
						"masuk"				=> $drr->jumlah,
						"keluar"			=> 0,
						"sisa"				=> $sisa_row->sisa
					);
					$result = $kartu_stok_dbtable->insert($kartu_stok_data);
				}
			} else {
				//do update header here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
					$header_row = $this->dbtable->get_row("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . "
						WHERE id = '" . $id['id'] . "'
					");
					$detail_rows = $this->dbtable->get_result("
						SELECT *
						FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . "
						WHERE id_retur_penjualan_resep = '" . $id['id'] . "' AND prop NOT LIKE 'del'
					");
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_STOK_OBAT);
					foreach($detail_rows as $dr) {
                        if ($dr->id_penjualan_resep > 0) {
                            $penjualan_resep_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_PENJUALAN_RESEP);
                            $resep_data = array();
                            $resep_data['diretur'] = "0";
                            $resep_id['id'] = $dr->id_penjualan_resep;
                            $penjualan_resep_dbtable->update($resep_data, $resep_id);
                        }

						$stok_row = $stok_obat_dbtable->get_row("
							SELECT id, sisa
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . "
							WHERE id = '" . $dr->id_stok_obat . "'
						");
						$stok_data = array();
						$stok_data['sisa'] = $stok_row->sisa - $dr->jumlah;
						$stok_id['id'] = $dr->id_stok_obat;
						$stok_obat_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_RIWAYAT_STOK_OBAT);
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $dr->id_stok_obat;
						$data_riwayat['jumlah_keluar'] = $dr->jumlah;
						$data_riwayat['sisa'] = $stok_row->sisa - $dr->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Retur Obat Per Pasien (" . ArrayAdapter::format("only-digit8", $id['id']) . ")";
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
					}
					$dretur_rows = $this->dbtable->get_result("
						SELECT b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', b.satuan
						FROM " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
						WHERE a.prop NOT LIKE 'del' AND a.id_retur_penjualan_resep = '" . $id['id'] . "'
						GROUP BY b.id_obat, b.satuan
					");
					//logging kartu stok:
					$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), InventoryLibrary::$_TBL_KARTU_STOK_OBAT);
					foreach ($dretur_rows as $drr) {
						$sisa_row = $this->dbtable->get_row("
							SELECT SUM(a.sisa) AS 'sisa'
							FROM " . InventoryLibrary::$_TBL_STOK_OBAT . " a LEFT JOIN " . InventoryLibrary::$_TBL_OBAT_MASUK . " b ON a.id_obat_masuk = b.id
							WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status = 'sudah' AND a.id_obat = '" . $drr->id_obat . "' AND a.satuan = '" . $drr->satuan . "' AND a.konversi = '1'
						");
						$kartu_stok_data = array(
							"f_id"				=> $id['id'],
							"no_bon"			=> $id['id'],
							"unit"				=> "Retur Obat Per Pasien : Pembatalan",
							"id_obat"			=> $drr->id_obat,
							"kode_obat"			=> $drr->kode_obat,
							"nama_obat"			=> $drr->nama_obat,
							"nama_jenis_obat"	=> $drr->nama_jenis_obat,
							"tanggal"			=> date("Y-m-d"),
							"masuk"				=> 0,
							"keluar"			=> $drr->jumlah,
							"sisa"				=> $sisa_row->sisa
						);
						$result = $kartu_stok_dbtable->insert($kartu_stok_data);
					}
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
        }

        public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
                SELECT 
                    *
                FROM 
                    " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . "
                WHERE 
                    id = '" . $id . "'
            ");
            
			$detail_rows = $this->dbtable->get_result("
                SELECT 
                    a.*,
                    b.nama_obat, 
                    b.satuan,
                    b.tanggal_exp,
                    c.nama_dokter,
                    c.nomor_resep,
                    d.persentase_retur
                FROM 
                    " . InventoryLibrary::$_TBL_DRETUR_PENJUALAN_RESEP . " a
                        LEFT JOIN " . InventoryLibrary::$_TBL_STOK_OBAT . " b ON a.id_stok_obat = b.id
                        LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " c ON a.id_penjualan_resep = c.id
                        LEFT JOIN " . InventoryLibrary::$_TBL_RETUR_PENJUALAN_RESEP . " d ON a.id_retur_penjualan_resep = d.id
                WHERE 
                    a.id_retur_penjualan_resep = '" . $id . "' 
                        AND a.prop = ''
            ");
            
			$detail_list = "";
            $arr_dokter = array();
			foreach($detail_rows as $dr) {
				if ($dr->jumlah > 0) {
                    $arr_dokter[$dr->nama_dokter] = $dr->nama_dokter;
                    $harga = $dr->harga * $dr->persentase_retur / 100;
					$subtotal = $dr->jumlah * $harga;
                    $f_subtotal = $dr->jumlah . " x " . ArrayAdapter::format("only-money", $harga) . " = " . ArrayAdapter::format("only-money", $subtotal);
                    $f_jumlah_retur = $dr->jumlah . " " . $dr->satuan;
                    $tanggal_exp = $dr->tanggal_exp == "0000-00-00" ? "-" : ArrayAdapter::format("date d-m-Y", $dr->tanggal_exp);
                    $detail_list .= "<tr>";
                        $detail_list .= "<td id='id_penjualan_resep'>" . $dr->id_penjualan_resep . "</td>";
                        $detail_list .= "<td id='nomor_resep'>" . $dr->nomor_resep . "</td>";
                        $detail_list .= "<td id='id_stok_obat' style='display: none;'>" . $dr->id_stok_obat . "</td>";
                        $detail_list .= "<td id='id_stok_pakai_obat_jadi' style='display: none;'>" . $dr->id_stok_pakai_obat_jadi . "</td>";
                        $detail_list .= "<td id='nama_obat'>" . $dr->nama_obat . "</td>";
                        $detail_list .= "<td id='tanggal_exp'>" . $tanggal_exp . "</td>";
                        $detail_list .= "<td id='f_subtotal'>" . $f_subtotal . "</td>";
                        $detail_list .= "<td id='f_jumlah_retur'>" . $f_jumlah_retur . "</td>";
                    $detail_list .= "</tr>";
				}
            }
            $daftar_dokter = "";
            if (count($arr_dokter) > 0) {
                foreach ($arr_dokter as $dokter)
                    $daftar_dokter .= $dokter . "\r\n";
            }
            $data['header']->daftar_dokter = $daftar_dokter;
            $data['header']->tanggal = ArrayAdapter::format("date d-m-Y, H:i", $data['header']->tanggal);
			$data['detail_list'] = $detail_list;
			return $data;
		}
    }
?>