<?php
    require_once("smis-libs-out/fpdf/fpdf.php");
    class PDF extends FPDF
    {
        function MultiCell($w, $h, $txt, $border = 0, $align = 'J', $fill = false, $maxline = 0)
        {
            //Output text with automatic or explicit line breaks, at most $maxline lines
            $cw = &$this->CurrentFont['cw'];
            if ($w == 0)
                $w = $this->w - $this->rMargin - $this->x;
            $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
            $s = str_replace("\r", '', $txt);
            $nb = strlen($s);
            if ($nb > 0 && $s[$nb - 1] == "\n")
                $nb--;
            $b = 0;
            if ($border) {
                if ($border == 1) {
                    $border = 'LTRB';
                    $b = 'LRT';
                    $b2 = 'LR';
                } else {
                    $b2 = '';
                    if (is_int(strpos($border, 'L')))
                        $b2 .= 'L';
                    if (is_int(strpos($border, 'R')))
                        $b2 .= 'R';
                    $b = is_int(strpos($border, 'T')) ? $b2 . 'T' : $b2;
                }
            }
            $sep = -1;
            $i = 0;
            $j = 0;
            $l = 0;
            $ns = 0;
            $nl = 1;
            while ($i < $nb) {
                //Get next character
                $c = $s[$i];
                if ($c == "\n") {
                    //Explicit line break
                    if ($this->ws > 0) {
                        $this->ws = 0;
                        $this->_out('0 Tw');
                    }
                    $this->Cell($w, $h, substr($s, $j, $i - $j), $b, 2, $align, $fill);
                    $i++;
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                    if ($border && $nl == 2)
                        $b = $b2;
                    if ($maxline && $nl > $maxline)
                        return substr($s, $i);
                    continue;
                }
                if ($c == ' ') {
                    $sep = $i;
                    $ls = $l;
                    $ns++;
                }
                $l += $cw[$c];
                if ($l > $wmax) {
                    //Automatic line break
                    if ($sep == -1) {
                        if ($i == $j)
                            $i++;
                        if ($this->ws > 0) {
                            $this->ws = 0;
                            $this->_out('0 Tw');
                        }
                        $this->Cell($w, $h, substr($s, $j, $i - $j), $b, 2, $align, $fill);
                    } else {
                        if ($align == 'J') {
                            $this->ws = ($ns > 1) ? ($wmax - $ls) / 1000 * $this->FontSize / ($ns - 1) : 0;
                            $this->_out(sprintf('%.3F Tw', $this->ws * $this->k));
                        }
                        $this->Cell($w, $h, substr($s, $j, $sep - $j), $b, 2, $align, $fill);
                        $i = $sep + 1;
                    }
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                    if ($border && $nl == 2)
                        $b = $b2;
                    if ($maxline && $nl > $maxline) {
                        if ($this->ws > 0) {
                            $this->ws = 0;
                            $this->_out('0 Tw');
                        }
                        return substr($s, $i);
                    }
                } else
                    $i++;
            }
            //Last chunk
            if ($this->ws > 0) {
                $this->ws = 0;
                $this->_out('0 Tw');
            }
            if ($border && is_int(strpos($border, 'B')))
                $b .= 'B';
            $this->Cell($w, $h, substr($s, $j, $i - $j), $b, 2, $align, $fill);
            $this->x = $this->lMargin;
            return '';
        }

        function Circle($x, $y, $r, $style = 'D')
        {
            $this->Ellipse($x, $y, $r, $r, $style);
        }

        function Ellipse($x, $y, $rx, $ry, $style = 'D')
        {
            if ($style == 'F')
            $op = 'f';
            elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
            else
                $op = 'S';
            $lx = 4 / 3 * (M_SQRT2 - 1) * $rx;
            $ly = 4 / 3 * (M_SQRT2 - 1) * $ry;
            $k = $this->k;
            $h = $this->h;
            $this->_out(sprintf(
                '%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
                ($x + $rx) * $k,
                ($h - $y) * $k,
                ($x + $rx) * $k,
                ($h - ($y - $ly)) * $k,
                ($x + $lx) * $k,
                ($h - ($y - $ry)) * $k,
                $x * $k,
                ($h - ($y - $ry)) * $k
            ));
            $this->_out(sprintf(
                '%.2F %.2F %.2F %.2F %.2F %.2F c',
                ($x - $lx) * $k,
                ($h - ($y - $ry)) * $k,
                ($x - $rx) * $k,
                ($h - ($y - $ly)) * $k,
                ($x - $rx) * $k,
                ($h - $y) * $k
            ));
            $this->_out(sprintf(
                '%.2F %.2F %.2F %.2F %.2F %.2F c',
                ($x - $rx) * $k,
                ($h - ($y + $ly)) * $k,
                ($x - $lx) * $k,
                ($h - ($y + $ry)) * $k,
                $x * $k,
                ($h - ($y + $ry)) * $k
            ));
            $this->_out(sprintf(
                '%.2F %.2F %.2F %.2F %.2F %.2F c %s',
                ($x + $lx) * $k,
                ($h - ($y + $ry)) * $k,
                ($x + $rx) * $k,
                ($h - ($y + $ly)) * $k,
                ($x + $rx) * $k,
                ($h - $y) * $k,
                $op
            ));
        }
    }
?>