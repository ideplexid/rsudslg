<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("depo_farmasi_ok/library/InventoryLibrary.php");
	require_once("depo_farmasi_ok/table/RPRPasienTable.php");
	require_once("depo_farmasi_ok/adapter/PasienAdapter.php");
	global $db;

	//patient service consumer:
	$pasien_table = new PasienTable(
		array("Tgl. Daftar", "No. Reg.", "NRM", "Nama", "Alamat", "Rawat Inap/Jalan", "Jns. Pasien"),
		"",
		null,
		true
	);
	$pasien_table->setName("rpr_pasien");
	$pasien_table->setModel(Table::$SELECT);
	$tanggal_daftar_text = new Text("search_tanggal", "search_tanggal", "");
	$tanggal_daftar_text->setClass("search search-header-tiny search-text");
	$noreg_s_text = new Text("search_noreg", "search_noreg", "");
	$noreg_s_text->setClass("search search-header-tiny search-text");
	$nrm_s_text = new Text("search_nrm", "search_nrm", "");
	$nrm_s_text->addAtribute("autofocus");
	$nrm_s_text->setClass("search search-header-tiny search-text");
	$nama_s_text = new Text("search_nama", "search_nama", "");
	$nama_s_text->setClass("search search-header-med search-text ");
	$alamat_s_text = new Text("search_alamat", "search_alamat", "");
	$alamat_s_text->setClass("search search-header-big search-text");
	$uri_option = new OptionBuilder();
	$uri_option->add("SEMUA", "%", "1");
	$uri_option->add("RAWAT JALAN", "0");
	$uri_option->add("RAWAT INAP", "1");
	$uri_s_select = new Select("search_uri", "search_uri", $uri_option->getContent());
	$uri_s_select->setClass("search search-header-med search-combo");
	$carabayar_s_text = new Text("search_carabayar", "search_carabayar", "");
	$carabayar_s_text->setClass("search search-header-tiny search-text");
	$header = "<tr class = 'header_pasien'>" .
					"<td>" . $tanggal_daftar_text->getHtml() . "</td>" .
					"<td>" . $noreg_s_text->getHtml() . "</td>" .
					"<td>" . $nrm_s_text->getHtml() . "</td>" .
					"<td>" . $nama_s_text->getHtml() . "</td>" .
					"<td>" . $alamat_s_text->getHtml() . "</td>" .
					"<td>" . $uri_s_select->getHtml() . "</td>" .
					"<td>" . $carabayar_s_text->getHtml() . "</td>" .
			  "</tr>";
	$pasien_table->addHeader("after", $header);
	$pasien_adapter = new PasienAdapter();
	$pasien_service_responder = new ServiceResponder(
		$db,
		$pasien_table,
		$pasien_adapter,
		"get_registered_all"
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("rpr_pasien", $pasien_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	$form = new Form("", "", "Depo Farmasi OK : Laporan Rekap Penjualan Resep");
	$tanggal_from_text = new Text("rpr_tanggal_from", "rpr_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("rpr_tanggal_to", "rpr_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$filter_option = new OptionBuilder();
	$filter_option->add("JENIS PASIEN", "jenis_pasien", "1");
	$filter_option->add("NO. REGISTRASI PASIEN", "noreg_pasien");
	$filter_select = new Select("rpr_filter", "rpr_filter", $filter_option->getContent());
	$form->addElement("Filter", $filter_select);
	$consumer_service = new ServiceConsumer(
		$db, 
		"get_jenis_patient",
		null,
		"registration" 
	);
	$content = $consumer_service->execute()->getContent();
	$jenis_pasien_option = new OptionBuilder();
	$jenis_pasien_option->add("SEMUA", "%%", 1);
	foreach($content as $c){
		$jenis_pasien_option->add($c['name'], $c['value']);
	}
	$jenis_pasien_select = new Select("rpr_jenis_pasien", "rpr_jenis_pasien", $jenis_pasien_option->getContent());
	$form->addElement("Jenis Pasien", $jenis_pasien_select);
	$uri_option = new OptionBuilder();
	$uri_option->add("SEMUA", "%%", "1");
	$uri_option->add("RAWAT JALAN", "0");
	$uri_option->add("RAWAT INAP", "1");
	$uri_select = new Select("rpr_uri", "rpr_uri", $uri_option->getContent());
	$form->addElement("IRJA/IRNA", $uri_select);
	$pasien_button = new Button("", "", "Pilih");
	$pasien_button->setClass("btn-info");
	$pasien_button->setIsButton(Button::$ICONIC);
	$pasien_button->setIcon("icon-white ".Button::$icon_list_alt);
	$pasien_button->setAction("rpr_pasien.chooser('rpr_pasien', 'rpr_pasien_button', 'rpr_pasien', rpr_pasien, 'PASIEN')");
	$pasien_button->setAtribute("id='rpr_pasien_browse'");
	$pasien_text = new Text("rpr_nama_pasien", "rpr_nama_pasien", "");
	$pasien_text->setAtribute("disabled='disabled'");
	$pasien_text->setClass("smis-one-option-input");
	$pasien_input_group = new InputGroup("");
	$pasien_input_group->addComponent($pasien_text);
	$pasien_input_group->addComponent($pasien_button);
	$form->addElement("Pasien", $pasien_input_group);
	$noreg_text = new Text("rpr_noreg_pasien", "rpr_noreg_pasien", "");
	$noreg_text->setAtribute("disabled='disabled'");
	$form->addElement("No. Registrasi", $noreg_text);
	$nrm_text = new Text("rpr_nrm_pasien", "rpr_nrm_pasien", "");
	$nrm_text->setAtribute("disabled='disabled'");
	$form->addElement("NRM Pasien", $nrm_text);
	$kategori_option = new OptionBuilder();
	$kategori_option->add("SEMUA", "%%", "1");
	$kategori_option->add("UMUM", "umum");
	$kategori_option->add("INA CBGS", "ina_cbgs");
	$kategori_option->add("INA CBGS NON TAGIHAN", "ina_cbgs_non_tagihan");
	$kategori_option->add("KRONIS", "kronis");
	$kategori_option->add("KEMOTERAPI", "kemoterapi");
	$kategori_select = new Select("rpr_kategori", "rpr_kategori", $kategori_option->getContent());
	$form->addElement("Kategori", $kategori_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("rpr.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='rpr_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "No. Trans.", "No. Resep", "Kategori", "Tanggal/Jam", "Dokter", "No. Reg.", "No. RM", "Pasien", "Alamat", "Jenis", "IRNA/IRJA", "Unit", "Total (Rp.)"),
		"",
		null,
		true
	);
	$table->setName("rpr");
	$table->setAction(false);
	$table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$uri = $_POST['uri'];
			$nama_pasien = $_POST['nama_pasien'];
			$nrm_pasien = $_POST['nrm_pasien'];
			$noreg_pasien = $_POST['noreg_pasien'];
			$kategori = $_POST['kategori'];
			$kategori_label = $_POST['kategori_label'];

			$filter = "";
			if ($jenis_filter == "jenis_pasien")
				$filter = " AND jenis LIKE '" . $jenis_pasien . "' AND uri LIKE '" . $uri . "' ";
			else if ($jenis_filter == "noreg_pasien")
				$filter = " AND noreg_pasien = '" . $noreg_pasien . "' ";

			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar') AND kategori LIKE '" . $kategori . "' " . $filter . "
			");
			$data = array(
				"jumlah"	=> $row->jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$uri = $_POST['uri'];
			$nama_pasien = $_POST['nama_pasien'];
			$nrm_pasien = $_POST['nrm_pasien'];
			$noreg_pasien = $_POST['noreg_pasien'];
			$kategori = $_POST['kategori'];
			$kategori_label = $_POST['kategori_label'];
			$num = $_POST['num'];

			$filter = "";
			if ($jenis_filter == "jenis_pasien")
				$filter = " AND jenis LIKE '" . $jenis_pasien . "' AND uri LIKE '" . $uri . "' ";
			else if ($jenis_filter == "noreg_pasien")
				$filter = " AND noreg_pasien = '" . $noreg_pasien . "' ";

			$row = $dbtable->get_row("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . "
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar') AND kategori LIKE '" . $kategori . "' " . $filter . "
				LIMIT " . $num . ", 1
			");

			$html = "";
			if ($row != null) {
				$uri = $row->uri == 1 ? "RAWAT INAP" : "RAWAT JALAN";
				$html = "
					<tr>
						<td id='rpr_nomor'></td>
						<td id='rpr_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
						<td id='rpr_no_resep'><small>" . $row->nomor_resep . "</small></td>
						<td id='rpr_no_resep'><small>" . ArrayAdapter::format("unslug", $row->kategori) . "</small></td>
						<td id='rpr_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
						<td id='rpr_nama_dokter'><small>" . $row->nama_dokter . "</small></td>
						<td id='rpr_noreg_pasien'><small>" . ArrayAdapter::format("only-digit8", $row->noreg_pasien) . "</small></td>
						<td id='rpr_nrm_pasien'><small>" . ArrayAdapter::format("only-digit6", $row->nrm_pasien) . "</small></td>
						<td id='rpr_nama_pasien'><small>" . $row->nama_pasien . "</small></td>
						<td id='rpr_alamat_pasien'><small>" . $row->alamat_pasien . "</small></td>
						<td id='rpr_jenis'><small>" . ArrayAdapter::format("unslug", $row->jenis) . "</small></td>
						<td id='rpr_uri'><small>" . $uri . "</small></td>
						<td id='rpr_unit'><small>" . ArrayAdapter::format("unslug", $row->ruangan) . "</small></td>
						<td id='rpr_total' style='display: none;'>" . $row->total . "</td>
						<td id='rpr_f_total'><small><div align='right'>" . ArrayAdapter::format("only-money", $row->total) . "</div></small></td>
					</tr>
				";
			}
			$data = array(
				"nomor_transaksi"	=> ArrayAdapter::format("only-digit8", $row->id),
				"nomor_resep"		=> $row->nomor_resep,
				"nama_pasien"		=> $row->nama_pasien,
				"nama_dokter"		=> $row->nama_dokter,
				"html"				=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "256M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$uri = $_POST['uri'];
			$nama_pasien = $_POST['nama_pasien'];
			$nrm_pasien = $_POST['nrm_pasien'];
			$noreg_pasien = $_POST['noreg_pasien'];
			$kategori = $_POST['kategori'];
			$kategori_label = $_POST['kategori_label'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_ok/templates/template_rekap_penjualan_resep.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP PENJUALAN RESEP");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B2", "DEPO FARMASI IGD");
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$label_filter = "SEMUA DATA";
			if ($jenis_filter == "jenis_pasien" && ($jenis_pasien != "%%" || $uri != "%%")) {
				$label_filter = $jenis_pasien == "%%" ? "SEMUA JENIS PASIEN" : ArrayAdapter::format("unslug", $jenis_pasien) . " - ";
				$label_filter .= $uri == "%%" ? "SEMUA RAWAT JALAN/INAP" : ($uri == 1 ? "RAWAT INAP" : "RAWAT JALAN");
			} else if ($jenis_filter == "noreg_pasien" && $noreg_pasien != "")
				$label_filter = "No. Reg. " . $noreg_pasien . " - No. RM. " . $nrm_pasien . " - " . $nama_pasien;
			$objWorksheet->setCellValue("B4", "FILTER : " . $label_filter . " | KATEGORI : " . $kategori_label);
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("N10")->getNumberFormat()->setFormatCode("#,##0.00");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(9, $_POST['num_rows'] - 2);
			$start_row_num = 8;
			$end_row_num = 8;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor_transaksi);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor_resep);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kategori);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_dokter);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->noreg_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nrm_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->alamat_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->uri);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->unit);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total);
				$objWorksheet->getStyle("N" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=A1_REKAP_PENJUALAN_RESEP_" . $label_filter . "_" . $kategori_label . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("rpr_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("rpr.cancel()");
	$loading_modal = new Modal("rpr_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='rpr_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("depo_farmasi_ok/js/rpr_pasien_action.js", false);
	echo addJS("depo_farmasi_ok/js/laporan_rekap_penjualan_resep_action.js", false);
	echo addJS("depo_farmasi_ok/js/laporan_rekap_penjualan_resep.js", false);
?>
<style type="text/css">
	.search-header-tiny {
		width: 100px !important;
	}
	.search-header-med {
		width: 150px !important;
	}
	.search-header-big {
		width: 200px !important;
	}
</style>