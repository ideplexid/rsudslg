<?php
	$daftar_obat_lanjutan_tabulator = new Tabulator("daftar_obat_lanjutan", "", Tabulator::$LANDSCAPE);
	$daftar_obat_lanjutan_tabulator->add("daftar_okt", "Daftar OKT", "depo_farmasi_ok/daftar_okt.php", Tabulator::$TYPE_INCLUDE);
	$daftar_obat_lanjutan_tabulator->add("daftar_narkotika", "Daftar Narkotika", "depo_farmasi_ok/daftar_narkotika.php", Tabulator::$TYPE_INCLUDE);
	$daftar_obat_lanjutan_tabulator->add("daftar_prekursor_farmasi", "Daftar Prekursor Farmasi", "depo_farmasi_ok/daftar_prekursor_farmasi.php", Tabulator::$TYPE_INCLUDE);
	echo $daftar_obat_lanjutan_tabulator->getHtml();
?>