<?php
	require_once("depo_farmasi_ok/library/InventoryLibrary.php");
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$form = new Form("", "", "Depo Farmasi OK : Laporan Asuhan Farmasi");
	$tanggal_from_text = new Text("laf_tanggal_from", "laf_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("laf_tanggal_to", "laf_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$ruangan_option = new OptionBuilder();
	$ruangan_option->add("Semua", "%%", "1");
	$ruangan_rows = $db->get_result("
		SELECT DISTINCT ruangan
		FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . "
		WHERE prop = '' AND ruangan NOT LIKE '' AND ruangan IS NOT NULL
		ORDER BY ruangan ASC
	");
	if ($ruangan_rows != null) {
		foreach ($ruangan_rows as $ruangan_row)
			$ruangan_option->add(ArrayAdapter::format("unslug", $ruangan_row->ruangan), $ruangan_row->ruangan);
	}
	$ruangan_select = new Select("laf_ruangan", "laf_ruangan", $ruangan_option->getContent());
	$form->addElement("Ruangan", $ruangan_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("laf.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAction("laf.export_xls()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "Tanggal", "No. Reg.", "NRM", "Nama Pasien", "Biaya Asuhan Farmasi", "Ruangan", "Petugas"),
		"",
		null,
		true
	);
	$table->setName("laf");
	$table->setAction(false);

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "1024M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$ruangan = $_POST['ruangan'];
			$ruangan_label = $_POST['ruangan_label'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("depo_farmasi_ok/templates/template_laporan_asuhan_farmasi.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("ASUHAN FARMASI");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("A10", "Laporan Asuhan Farmasi Depo Farmasi OK");
			$objWorksheet->setCellValue("A11", "Periode : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to) . " | Ruangan : " . $_POST['ruangan_label']);
			$data = $db->get_result("
				SELECT *
				FROM " . InventoryLibrary::$_TBL_ASUHAN_FARMASI . "
				WHERE prop = '' AND tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "' AND ruangan LIKE '" . $_POST['ruangan'] . "'
			");
			if (count($data) - 2 > 0)
				$objWorksheet->insertNewRowBefore(15, count($data) - 2);
			$start_row_num = 14;
			$end_row_num = 14;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 0;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("date d-m-Y, H:i", $d->tanggal));
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nrm_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->noreg_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->biaya);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $d->ruangan));
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_petugas);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->keterangan);
				$objWorksheet->getStyle("E" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				
				$row_num++;
				$end_row_num++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=L_ASUHAN_FARMASI_DEPO_FARMASI_OK_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y, H:i");
		$adapter->add("No. Reg.", "noreg_pasien", "digit6");
		$adapter->add("NRM", "nrm_pasien", "digit6");
		$adapter->add("Nama Pasien", "nama_pasien");
		$adapter->add("Biaya Asuhan Farmasi", "biaya", "money");
		$adapter->add("Ruangan", "ruangan", "unslug");
		$adapter->add("Petugas", "nama_petugas");
		$columns = array("nrm_pasien");
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_ASUHAN_FARMASI, $columns);
		$dbtable->addCustomKriteria(" tanggal ", " >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "'");
		$dbtable->addCustomKriteria(" ruangan ", " LIKE '" . $_POST['ruangan'] . "'");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='laf_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("depo_farmasi_ok/js/laporan_asuhan_farmasi.js", false);
?>