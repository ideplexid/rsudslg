<?php
	require_once("depo_farmasi_ok/library/InventoryLibrary.php");
	global $db;

	$bulan = $_POST['bulan'];
	$tahun = $_POST['tahun'];
	$id_obat_csv = $_POST['id_obat_csv'];
	$id_obat_arr = explode(",", $id_obat_csv);

	$jumlah = 0;

	if (count($id_obat_arr) > 0) {
		foreach ($id_obat_arr as $id_obat) {
			if ($bulan == "%%")
				$bulan = 12;
			$tanggal = $tahun . "-" . $bulan . "-" . cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$saldo_sekarang = InventoryLibrary::getCurrentStock($db, $id_obat, "", 1, "");
			$jumlah_masuk = InventoryLibrary::getStockIn($db, $id_obat, "", 1, "", date("Y-m-d", strtotime($tanggal . " +1 day")), date("Y-m-d")) + InventoryLibrary::getPenyesuaianStokPositif($db, $id_obat, "", 1, "", date("Y-m-d", strtotime($tanggal . "+1 day")), date("Y-m-d"));
			$jumlah_keluar = InventoryLibrary::getStockOut($db, $id_obat, "", 1, "",date("Y-m-d", strtotime($tanggal . " +1 day")), date("Y-m-d")) + InventoryLibrary::getPenyesuaianStokNegatif($db, $id_obat, "", 1, "", date("Y-m-d", strtotime($tanggal . "+1 day")), date("Y-m-d"));
			$saldo_akhir = $saldo_sekarang - $jumlah_masuk + $jumlah_keluar;
			$jumlah += $saldo_akhir;
		}
	}
	
	$data = array(
		'jumlah' => $jumlah
	);
	echo json_encode($data);
?>