<?php
	require_once("depo_farmasi_ok/library/InventoryLibrary.php");
	global $db;
	
	if (isset($_POST['nama_obat']) && 
		isset($_POST['nama_jenis_obat']) &&
		isset($_POST['tanggal_from']) &&
		isset($_POST['tanggal_to'])) {
		$nama_obat = $_POST['nama_obat'];
		$nama_jenis_obat = $_POST['nama_jenis_obat'];
		$tanggal_from = $_POST['tanggal_from'];
		$tanggal_to = $_POST['tanggal_to'];
		$dbtable = new DBTable($db, InventoryLibrary::$_TBL_PENJUALAN_RESEP);
		//obat jadi:
		$row = $dbtable->get_row("
			SELECT b.nama_obat, b.nama_jenis_obat, SUM(b.jumlah) AS 'jumlah'
			FROM " . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_JADI . " b ON a.id = b.id_penjualan_resep
			WHERE a.prop NOT LIKE 'del' AND a.dibatalkan = '0' AND a.tanggal >= '" . $tanggal_from . " 00:00' AND a.tanggal <= '" . $tanggal_to . " 23:59' AND b.nama_obat = '" . $nama_obat . "' AND b.nama_jenis_obat = '" . $nama_jenis_obat . "'
			GROUP BY b.nama_obat, b.nama_jenis_obat
		");
		$jumlah_obat_jadi = 0;
		if ($row != null)
			$jumlah_obat_jadi = $row->jumlah;
		//bahan racikan:
		$row = $dbtable->get_row("
			SELECT c.nama_obat, c.nama_jenis_obat, SUM(c.jumlah) AS 'jumlah'
			FROM (" . InventoryLibrary::$_TBL_PENJUALAN_RESEP . " a LEFT JOIN " . InventoryLibrary::$_TBL_PENJUALAN_OBAT_RACIKAN . " b ON a.id = b.id_penjualan_resep) LEFT JOIN " . InventoryLibrary::$_TBL_BAHAN_PAKAI_OBAT_RACIKAN . " c ON b.id = c.id_penjualan_obat_racikan
			WHERE a.prop NOT LIKE 'del' AND a.dibatalkan = '0' AND a.tanggal >= '" . $tanggal_from . " 00:00' AND a.tanggal <= '" . $tanggal_to . " 23:59' AND c.nama_obat = '" . $nama_obat . "' AND c.nama_jenis_obat = '" . $nama_jenis_obat . "'
			GROUP BY c.nama_obat, c.nama_jenis_obat
		");
		$jumlah_bahan = 0;
		if ($row != null)
			$jumlah_bahan = $row->jumlah;
		$data = array();
		$data['jumlah'] = $jumlah_obat_jadi + $jumlah_bahan;
		$data['unit'] = "depo_farmasi_ok";
		echo json_encode($data);
	}
?>