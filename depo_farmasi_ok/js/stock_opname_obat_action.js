function SOObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
SOObatAction.prototype.constructor = SOObatAction;
SOObatAction.prototype = new TableAction();
SOObatAction.prototype.selected = function(json) {
	$("#so_id_obat").val(json.id);
	$("#so_nama_obat").val(json.nama);
};