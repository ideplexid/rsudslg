var dpu;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	dpu = new DPUAction(
		"dpu",
		"depo_farmasi_ok",
		"laporan_rincian_penjualan_up",
		new Array()
	);
	$("#dpu_list").append(
		"<tr id='temp'>" +
			"<td colspan='14'><strong><small><center>DATA PENJUALAN UP BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});