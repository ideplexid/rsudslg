function LPOObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LPOObatAction.prototype.constructor = LPOObatAction;
LPOObatAction.prototype = new TableAction();
LPOObatAction.prototype.selected = function(json) {
	$("#lpo_id_obat").val(json.id);
	$("#lpo_nama_obat").val(json.nama);
};