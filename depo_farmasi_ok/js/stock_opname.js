var so;
var so_obat;
var so_jenis_obat;
var FINISHED;
$(document).ready(function() {
	$(".mydate").datepicker();
	so_obat = new SOObatAction(
		"so_obat",
		"depo_farmasi_ok",
		"stock_opname",
		new Array()
	);
	so_obat.setSuperCommand("so_obat");
	so_jenis_obat = new SOJenisObatAction(
		"so_jenis_obat",
		"depo_farmasi_ok",
		"stock_opname",
		new Array()
	);
	so_jenis_obat.setSuperCommand("so_jenis_obat");
	so = new SOAction(
		"so",
		"depo_farmasi_ok",
		"stock_opname",
		new Array()
	);
	$("#smis_container > div.form-container > form > div:nth-child(7)").hide();
	$("#smis_container > div.form-container > form > div:nth-child(8)").hide();
	$("#so_jenis_filter").on("change", function() {
		var jenis_filter = $("#so_jenis_filter").val();
		$("#so_id_obat").val("");
		$("#so_nama_obat").val("");
		$("#so_kode_jenis_obat").val("");
		$("#so_jenis_obat").val("");
		if (jenis_filter == "semua") {
			$("#smis_container > div.form-container > form > div:nth-child(7)").hide();
			$("#smis_container > div.form-container > form > div:nth-child(8)").hide();
		} else if (jenis_filter == "per_obat") {
			$("#smis_container > div.form-container > form > div:nth-child(7)").show();
			$("#smis_container > div.form-container > form > div:nth-child(8)").hide();
		} else if (jenis_filter == "per_jenis") {
			$("#smis_container > div.form-container > form > div:nth-child(7)").hide();
			$("#smis_container > div.form-container > form > div:nth-child(8)").show();
		}
	});
	$("#loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#loading_modal").on("hide", function() {
		$("a.close").show();
	});
	$("tbody#so_list").append(
		"<tr>" +
			"<td colspan='6'><strong><center><small>DATA PERSEDIAAN OBAT BELUM DIPROSES</small></center></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});