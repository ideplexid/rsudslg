function RRPBAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RRPBAction.prototype.constructor = RRPBAction;
RRPBAction.prototype = new TableAction();
RRPBAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#rrpb_tanggal_from").val();
	data['tanggal_to'] = $("#rrpb_tanggal_to").val();
	return data;
};
RRPBAction.prototype.view = function() {
	if ($("#rrpb_tanggal_from").val() == "" || $("#rrpb_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#rrpb_info").empty();
	$("#table_rrpb tfoot").remove();
	$("#rrpb_list").empty();
	$("#rrpb_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#rrpb_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
RRPBAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#rrpb_loading_modal").smodal("hide");
			$("#rrpb_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#rrpb_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#rrpb_list").append(
				json.html
			);
			$("#rrpb_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
RRPBAction.prototype.finalize = function() {
	var num_rows = $("tbody#rrpb_list tr").length;
	var t_total = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_nomor").html("<small>" + (i + 1) + "</small>");
		var total = parseFloat($("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_total").text());
		t_total += total;
	}
	$("#table_rrpb").append(
		"<tfoot>" +
			"<tr>" +
				"<td colspan='5'><small><strong><center>T O T A L</center></strong></small></td>" +
				"<td><small><div align='right'><strong>" + t_total.formatMoney("2", ".", ",") + "</strong></div></small></td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#rrpb_loading_modal").smodal("hide");
	$("#rrpb_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#rrpb_export_button").removeAttr("onclick");
	$("#rrpb_export_button").attr("onclick", "rrpb.export_xls()");
};
RRPBAction.prototype.cancel = function() {
	FINISHED = true;
};
RRPBAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#rrpb_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_nomor").text();
		var nomor_transaksi = $("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_id").text();
		var nomor_resep = $("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_no_resep").text();
		var tanggal = $("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_tanggal").text();
		var nama_pasien = $("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_nama_pasien").text();
		var total = $("tbody#rrpb_list tr:eq(" + i + ") td#rrpb_total").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"nomor_transaksi"	: nomor_transaksi,
			"nomor_resep"		: nomor_resep,
			"tanggal"			: tanggal,
			"nama_pasien"		: nama_pasien,
			"total"				: total
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#rrpb_tanggal_from").val();
	data['tanggal_to'] = $("#rrpb_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};