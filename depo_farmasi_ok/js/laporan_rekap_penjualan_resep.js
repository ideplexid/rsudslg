var rpr;
var rpr_pasien;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	rpr_pasien = new PasienAction(
		"rpr_pasien",
		"depo_farmasi_ok",
		"laporan_rekap_penjualan_resep",
		new Array()
	);
	rpr_pasien.setSuperCommand("rpr_pasien");
	rpr = new RPRAction(
		"rpr",
		"depo_farmasi_ok",
		"laporan_rekap_penjualan_resep",
		new Array()
	);
	$("#rpr_list").append(
		"<tr id='temp'>" +
			"<td colspan='14'><strong><small><center>DATA PENJUALAN RESEP BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
	$("#rpr_nama_pasien").parent().parent().hide();
	$(".rpr_noreg_pasien").hide();
	$(".rpr_nrm_pasien").hide();
	$("#rpr_filter").on("change", function() {
		if ($(this).val() == "jenis_pasien") {
			$(".rpr_jenis_pasien").show();
			$(".rpr_uri").show();
			$("#rpr_nama_pasien").parent().parent().hide();
			$(".rpr_noreg_pasien").hide();
			$(".rpr_nrm_pasien").hide();
		} else {
			$(".rpr_jenis_pasien").hide();
			$(".rpr_uri").hide();
			$("#rpr_nama_pasien").parent().parent().show();
			$(".rpr_noreg_pasien").show();
			$(".rpr_nrm_pasien").show();
		}
	});
	$("#smis-chooser-modal").on("show", function() {
		if ($("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("full_model");
			if ($("#smis-chooser-modal .modal-header h3").text() == "PASIEN") {
				$("table#table_rpr_pasien tfoot tr").eq(0).hide();
			} else {
				$("table#table_rpr_pasien tfoot tr").eq(0).show();
			}
		} else {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").removeClass("full_model");
		}
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("half_model");
		$("#smis-chooser-modal").removeClass("full_model");
	});
});