function LPOAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LPOAction.prototype.constructor = LPOAction;
LPOAction.prototype = new TableAction();
LPOAction.prototype.view = function() {
	if ($("#lpo_tanggal_from").val() == "" || $("#lpo_tanggal_to").val() == "")
		return;
	var self = this;
	$("#info").empty();
	$("#loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#loading_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "get_jumlah_obat";
	data['jenis_filter'] = $("#lpo_jenis_filter").val();
	data['id_obat'] = $("#lpo_id_obat").val();
	data['nama_obat'] = $("#lpo_nama_obat").val();
	data['nama_jenis_obat'] = $("#lpo_nama_jenis_obat").val();
	data['kode_jenis_obat'] = $("#lpo_kode_jenis_obat").val();
	data['urutan'] = $("#lpo_urutan").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#lpo_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
};
LPOAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#loading_modal").smodal("hide");
			$("#info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info_obat";
	data['num'] = num;
	data['tanggal_from'] = $("#lpo_tanggal_from").val();
	data['tanggal_to'] = $("#lpo_tanggal_to").val();
	data['jenis_filter'] = $("#lpo_jenis_filter").val();
	data['id_obat'] = $("#lpo_id_obat").val();
	data['nama_obat'] = $("#lpo_nama_obat").val();
	data['nama_jenis_obat'] = $("#lpo_nama_jenis_obat").val();
	data['kode_jenis_obat'] = $("#lpo_kode_jenis_obat").val();
	data['urutan'] = $("#lpo_urutan").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#lpo_list").append(
				json.html
			);
			$("#so_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
LPOAction.prototype.finalize = function() {
	var num_rows = $("tbody#lpo_list tr").length;
	for (var i = 0; i < num_rows; i++)
		$("tbody#lpo_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
	$("#loading_modal").smodal("hide");
	$("#info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#export_button").removeAttr("onclick");
	$("#export_button").attr("onclick", "lpo.export_xls()");
};
LPOAction.prototype.cancel = function() {
	FINISHED = true;
};
LPOAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#lpo_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#lpo_list tr:eq(" + i + ") td#nomor").text();
		var id_obat = $("tbody#lpo_list tr:eq(" + i + ") td#id_obat").text();
		var kode_obat = $("tbody#lpo_list tr:eq(" + i + ") td#kode_obat").text();
		var nama_obat = $("tbody#lpo_list tr:eq(" + i + ") td#nama_obat").text();
		var saldo_awal = $("tbody#lpo_list tr:eq(" + i + ") td#saldo_awal").text();
		var penyesuaian_stok = $("tbody#lpo_list tr:eq(" + i + ") td#penyesuaian_stok").text();
		var jumlah_masuk = $("tbody#lpo_list tr:eq(" + i + ") td#jumlah_masuk").text();
		var jumlah_keluar = $("tbody#lpo_list tr:eq(" + i + ") td#jumlah_keluar").text();
		var saldo_akhir = $("tbody#lpo_list tr:eq(" + i + ") td#saldo_akhir").text();
		var harga_pokok = $("tbody#lpo_list tr:eq(" + i + ") td#harga_pokok").text();
		var nilai_pemakaian = $("tbody#lpo_list tr:eq(" + i + ") td#nilai_pemakaian").text();
		var nilai_akhir = $("tbody#lpo_list tr:eq(" + i + ") td#nilai_akhir").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"id_obat" 			: id_obat,
			"kode_obat" 		: kode_obat,
			"nama_obat" 		: nama_obat,
			"saldo_awal" 		: saldo_awal,
			"penyesuaian_stok"	: penyesuaian_stok,
			"jumlah_masuk" 		: jumlah_masuk,
			"jumlah_keluar" 	: jumlah_keluar,
			"saldo_akhir" 		: saldo_akhir,
			"harga_pokok"		: harga_pokok,
			"nilai_pemakaian"	: nilai_pemakaian,
			"nilai_akhir"		: nilai_akhir
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#lpo_tanggal_from").val();
	data['tanggal_to'] = $("#lpo_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};