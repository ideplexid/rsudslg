var lso;
var FINISHED;
$(document).ready(function() {
	$(".mydate").datepicker();
	lso = new LSOAction(
		"lso",
		"depo_farmasi_ok",
		"laporan_stock_opname",
		new Array()
	);
	$("#loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#loading_modal").on("hide", function() {
		$("a.close").show();
	});
	$("tbody#lso_list").append(
		"<tr>" +
			"<td colspan='8'><strong><center><small>DATA PERSEDIAAN OBAT BELUM DIPROSES</small></center></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});