function InstansiLainAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
InstansiLainAction.prototype.constructor = InstansiLainAction;
InstansiLainAction.prototype = new TableAction();
InstansiLainAction.prototype.cekSave = function() {
	$(".error_field").removeClass("error_field");
	$("#modal_alert_instansi_l_add_form").html("");
	var result = TableAction.prototype.cekSave.call(this);
	var margin_jual = $("#instansi_l_margin_jual").val();
	if (!is_numeric(margin_jual)) {
		var error_statement = "<strong>Margin Jual</strong> seharunsya numerik (0-9)";
		if (result === false)
			$("#modal_alert_instansi_l_add_form div").html(
				$("#modal_alert_instansi_lain_l_add_form div").html() + "<br/>" + 
				error_statement
			);
		else {
			$("#modal_alert_instansi_l_add_form").html(
				"<div class='alert alert-block alert-info'>" +
					"<h4>Pemberitahuan</h4> <br>" +
					error_statement +
				"</div>"
			);
		}
		$("#instansi_l_margin_jual").addClass("error_field");
		result = false;
	}
	return result;
};
InstansiLainAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
InstansiLainAction.prototype.selected = function(json) {
	$("#penjualan_instansi_lain_id_instansi_lain").val(json.id);
	$("#penjualan_instansi_lain_nama_instansi_lain").val(json.nama);
	$("#penjualan_instansi_lain_alamat_instansi_lain").val(json.alamat);
	$("#penjualan_instansi_lain_telpon_instansi_lain").val(json.telpon);
	$("#penjualan_instansi_lain_markup").val(json.margin_jual);
	need_margin_penjualan_request = 1;
	penjualan_instansi_lain.refreshHargaAndSubtotal();
};