function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	showLoading();
	$("#pengaturan_margin_id_obat").val(json.id);
	$("#pengaturan_margin_kode_obat").val(json.kode);
	$("#pengaturan_margin_nama_obat").val(json.nama);
	$("#pengaturan_margin_nama_jenis_obat").val(json.nama_jenis_barang);
	var data = this.getRegulerData();
	data['command'] = "get_max_hpp";
	data['super_command'] = "";
	data['id_obat'] = json.id;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) {
				dismissLoading();
				return;
			}
			$("#pengaturan_margin_hpp_max").val(json.hpp);

			var v_hpp = json.hpp.replace(/[^0-9-,]/g, '').replace(",", ".");
			var v_hja = $("#pengaturan_margin_hja").val().replace(/[^0-9-,]/g, '').replace(",", ".");
			var v_margin = 0;
			if (v_hpp > 0)
				v_margin = (v_hja - v_hpp) / v_hpp * 100;
			$("#pengaturan_margin_margin_jual").val(v_margin);
			dismissLoading();
		}
	);
};