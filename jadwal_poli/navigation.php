<?php
	global $NAVIGATOR;

	$mr = new Menu ("fa fa-calendar");
	$mr->addProperty ( 'title', 'Jadwal Poli' );
	$mr->addProperty ( 'name', 'Jadwal Poli' );

	$mr->addLaravel("Referensi Poli","jadwal_poli","referensi_poli","Referensi Poli","fa fa-list");
	$mr->addLaravel("Referensi Dokter","jadwal_poli","referensi_dokter","Referensi Dokter","fa fa-list");
	$mr->addLaravel("Jadwal Poli","jadwal_poli","local","Jadwal Poli","fa fa-calendar");
	$mr->addLaravel("Jadwal Poli BPJS","jadwal_poli","bpjs","Jadwal Poli BPJS","fa fa-calendar");
	$mr->addLaravel("RS Credentials","jadwal_poli","credentials","Rs Credentials","fa fa-list");
	// $mr->addLaravel("Display","jadwal_poli","display","Display","fa fa-television");
	$NAVIGATOR->addMenu ( $mr, 'jadwal_poli' );
?>
