<?php
	global $PLUGINS;
	
	$init['name']        = "jadwal_poli";
	$init['path']        = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Jadwal Poli";
	$init['require']     = "Aplikasi Laravel (Eksternal)";
	$init['service']     = "";
	$init['version']     = "1.0.0";
	$init['number']      = "1";
	$init['type']        = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>