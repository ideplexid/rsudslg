<?php 
	require_once("smis-libs-class/DBCreator.php");
	global $wpdb;

	/// DDL Penerimaan Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_obat_masuk", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_po", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_opl", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_bbm", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_faktur", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_datang", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_tempo", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diskon", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("t_diskon", "enum('persen','nominal')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tipe", "enum('farmasi','sito','konsinyasi')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("use_ppn", "TINYINT(1)", DBCreator::$SIGN_NONE, true, 1, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("materai", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ekspedisi", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("total_tagihan", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("total_dibayar", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Detail Penerimaan Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_dobat_masuk", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_obat_masuk", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_dpo", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("inventaris", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("formularium", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("berlogo", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("generik", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("label", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("stok_entri", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_tercatat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hna", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("selisih", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diskon", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("t_diskon", "enum('persen','nominal')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("produsen", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();	

	/// DDL Stok Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_stok_obat", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_dobat_masuk", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("formularium", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("berlogo", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("generik", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("label", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("retur", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hna", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("produsen", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_exp", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_batch", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("turunan", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Mutasi Obat ke Unit :
	$dbcreator = new DBCreator($wpdb, "smis_frm_obat_keluar", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nomor", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("status", "enum('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Detail Mutasi Obat ke Unit :
	$dbcreator = new DBCreator($wpdb, "smis_frm_dobat_keluar", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_obat_keluar", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("stok_entri", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga_ma", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_diminta", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Stok Obat Digunakan Mutasi Obat ke Unit :
	$dbcreator = new DBCreator($wpdb, "smis_frm_stok_obat_keluar", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_dobat_keluar", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Retur Obat dari Unit :
	$dbcreator = new DBCreator($wpdb, "smis_frm_retur_obat_unit", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("f_id", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("lf_id", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("label", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hna", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("produsen", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_exp", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_batch", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("turunan", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("status", "enum('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("restok", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Riwayat Rekonsiliasi Stok Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_penyesuaian_stok", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_lama", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_baru", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_user", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Permintaan Obat dari Unit :
	$dbcreator = new DBCreator($wpdb, "smis_frm_permintaan_obat_unit", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("f_id", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("status", "enum('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Detail Permintaan Obat dari unit :	
	$dbcreator = new DBCreator($wpdb, "smis_frm_dpermintaan_obat_unit", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("f_id", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_permintaan_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_permintaan", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_permintaan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_dipenuhi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_dipenuhi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Retur Obat ke Vendor :
	$dbcreator = new DBCreator($wpdb, "smis_frm_retur_obat", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_obat_masuk", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_faktur", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_faktur", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("dibatalkan", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->setDuplicate(false);
	$dbcreator->initialize();
	
	/// DDL Detail Retur Obat ke Vendor :
	$dbcreator = new DBCreator($wpdb, "smis_frm_dretur_obat", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_retur_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->setDuplicate(false);
	$dbcreator->initialize();
	
	/// DDL Kartu Stok Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_kartu_stok_obat", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("f_id", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_bon", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("masuk", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keluar", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Riwayat Stok Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_riwayat_stok_obat", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_masuk", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_keluar", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_user", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Penjualan Resep :
	$dbcreator = new DBCreator($wpdb, "smis_frm_penjualan_resep", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("nomor_resep", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_dokter", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_dokter", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("noreg_pasien", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nrm_pasien", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_pasien", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("alamat_pasien", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_telpon", "varchar(16)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jenis", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("perusahaan", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("asuransi", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ruangan", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("markup", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("embalase", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tusla", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("biaya_racik", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("uri", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("total", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("edukasi", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("dibatalkan", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diretur", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tipe", "enum('resep','instansi_lain','bebas','resep_luar')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diskon", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("t_diskon", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan_batal", "text", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kategori", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Detail Penjualan Resep - Obat Jadi :
	$dbcreator = new DBCreator($wpdb, "smis_frm_penjualan_obat_jadi", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_penjualan_resep", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("embalase", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tusla", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("subtotal", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("aturan_pakai", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("pos", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("markup", "double", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();	

	/// DDL Stok Obat Digunakan Obat Jadi :
	$dbcreator = new DBCreator($wpdb, "smis_frm_stok_pakai_obat_jadi", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_penjualan_obat_jadi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Detail Penjualan Resep - Obat Racikan :	
	$dbcreator = new DBCreator($wpdb, "smis_frm_penjualan_obat_racikan", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_penjualan_resep", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_luaran", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_luaran", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("embalase", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tusla", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("biaya_racik", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("subtotal", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("aturan_pakai", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_apoteker", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_apoteker", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("pos", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Detail Obat Racikan - Bahan :
	$dbcreator = new DBCreator($wpdb, "smis_frm_bahan_pakai_obat_racikan", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_penjualan_obat_racikan", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("embalase", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tusla", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("markup", "double", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Stok Digunakan Bahan :
	$dbcreator = new DBCreator($wpdb, "smis_frm_stok_pakai_bahan", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_bahan", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Retur Penjualan Resep :	
	$dbcreator = new DBCreator($wpdb, "smis_frm_retur_penjualan_resep", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_penjualan_resep", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("persentase_retur", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("dibatalkan", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tercetak", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Detail Retur Penjualan Resep :
	$dbcreator = new DBCreator($wpdb, "smis_frm_dretur_penjualan_resep", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_retur_penjualan_resep", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("subtotal", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Unit Lain :
	$dbcreator = new DBCreator($wpdb, "smis_frm_unit_lain", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("kode", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Obat Keluar Unit Lain :
	$dbcreator = new DBCreator($wpdb, "smis_frm_obat_keluar_unit_lain", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_unit", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_unit", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_unit", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	/// DDL Detail Obat Keluar Unit Lain :
	$dbcreator = new DBCreator($wpdb, "smis_frm_dobat_keluar_unit_lain", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_obat_keluar_unit_lain", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_diminta", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("stok_entri", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga_ma", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Stok Obat Keluar Unit Lain :
	$dbcreator = new DBCreator($wpdb, "smis_frm_stok_obat_keluar_unit_lain", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_dobat_keluar_unit_lain", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Instansi Lain - UP :
	$dbcreator = new DBCreator($wpdb, "smis_frm_instansi_lain", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("nama", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("alamat", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("telpon", "varchar(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("margin_jual", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Pengaturan Margin Penjualan Per Jenis Pasien :
	$dbcreator = new DBCreator($wpdb, "smis_frm_margin_jual_jenis_pasien", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("slug", "varchar(512)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jenis_pasien", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("margin_jual", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();	

	/// DDL Pengaturan Margin Penjualan Per Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_margin_jual_obat", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("margin_jual", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Pengaturan Margin Penjualan Per Jenis Pasien Per Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_margin_jual_obat_jenis_pasien", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("slug", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jenis_pasien", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("margin_jual", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	/// DDL Histori HPP Obat :
	$dbcreator = new DBCreator($wpdb, "smis_frm_histori_hpp", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("tanggal", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_transaksi", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hpp", "float", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("in_out", "tinyint(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "text", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
?>