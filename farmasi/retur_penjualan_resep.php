<?php 
	require_once("farmasi/table/ReturPenjualanResepTable.php");
	require_once("farmasi/adapter/ReturPenjualanResepAdapter.php");
	require_once("farmasi/responder/ReturPenjualanResepDBResponder.php");
	require_once("farmasi/responder/ReturPenjualanResep_ResepDBResponder.php");
	
	$retur_penjualan_resep_table = new ReturPenjualanResepTable(
		array("Nomor", "Tanggal/Jam", "No. Resep", "NRM", "No. Reg.", "Pasien", "Dokter", "Status"),
		"Farmasi : Retur Penjualan Resep",
		null,
		true
	);
	$retur_penjualan_resep_table->setName("retur");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "retur") {
		if (isset($_POST['command'])) {
			$retur_penjualan_resep_adapter = new ReturPenjualanResepAdapter();
			$columns = array("id", "tanggal", "id_penjualan_resep", "persentase_retur", "dibatalkan", "tercetak");
			$retur_penjualan_resep_dbtable = new DBTable(
				$db,
				"smis_frm_retur_penjualan_resep",
				$columns
			);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (smis_frm_penjualan_resep.nama_dokter LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_penjualan_resep.nama_pasien LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_penjualan_resep.nrm_pasien LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_penjualan_resep.noreg_pasien LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_penjualan_resep.nomor_resep LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_frm_retur_penjualan_resep.*, smis_frm_penjualan_resep.nama_dokter, smis_frm_penjualan_resep.nama_pasien, smis_frm_penjualan_resep.nomor_resep, smis_frm_penjualan_resep.nrm_pasien, smis_frm_penjualan_resep.noreg_pasien
					FROM smis_frm_retur_penjualan_resep LEFT JOIN smis_frm_penjualan_resep ON smis_frm_retur_penjualan_resep.id_penjualan_resep = smis_frm_penjualan_resep.id
					WHERE smis_frm_retur_penjualan_resep.prop NOT LIKE 'del' AND (smis_frm_penjualan_resep.tipe = 'resep' OR smis_frm_penjualan_resep.tipe = 'resep_luar') " . $filter . "
					ORDER BY smis_frm_retur_penjualan_resep.id DESC
				) v_retur
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_frm_retur_penjualan_resep.*, smis_frm_penjualan_resep.nama_dokter, smis_frm_penjualan_resep.nama_pasien, smis_frm_penjualan_resep.nomor_resep, smis_frm_penjualan_resep.nrm_pasien, smis_frm_penjualan_resep.noreg_pasien
					FROM smis_frm_retur_penjualan_resep LEFT JOIN smis_frm_penjualan_resep ON smis_frm_retur_penjualan_resep.id_penjualan_resep = smis_frm_penjualan_resep.id
					WHERE smis_frm_retur_penjualan_resep.prop NOT LIKE 'del'  AND (smis_frm_penjualan_resep.tipe = 'resep' OR smis_frm_penjualan_resep.tipe = 'resep_luar') " . $filter . "
					ORDER BY smis_frm_retur_penjualan_resep.id DESC
				) v_retur
			";
			$retur_penjualan_resep_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$retur_penjualan_resep_dbresponder = new ReturPenjualanResepDBResponder(
				$retur_penjualan_resep_dbtable,
				$retur_penjualan_resep_table,
				$retur_penjualan_resep_adapter
			);
			$data = $retur_penjualan_resep_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	//resep chooser:
	$resep_table = new Table(
		array("No. Penjualan", "No. Resep", "Tanggal/Jam", "Dokter", "NRM", "No. Reg.", "Pasien", "Alamat"),
		"",
		null,
		true
	);
	$resep_table->setName("resep");
	$resep_table->setModel(Table::$SELECT);
	$resep_adapter = new SimpleAdapter();
	$resep_adapter->add("No. Penjualan", "id", "digit8");
	$resep_adapter->add("No. Resep", "nomor_resep");
	$resep_adapter->add("Tanggal/Jam", "tanggal", "date d-m-Y H:i");
	$resep_adapter->add("Dokter", "nama_dokter");
	$resep_adapter->add("NRM", "nrm_pasien");
	$resep_adapter->add("No. Reg.", "noreg_pasien");
	$resep_adapter->add("Pasien", "nama_pasien");
	$resep_adapter->add("Alamat", "alamat_pasien");
	$resep_dbtable = new DBTable($db, "smis_frm_penjualan_resep");
	$resep_dbtable->addCustomKriteria(" dibatalkan ", " = 0 ");
	$resep_dbtable->addCustomKriteria(" diretur ", " = 0 ");
	$resep_dbtable->addCustomKriteria("", " tipe='resep' OR tipe='resep_luar' ");
	$resep_dbreseponder = new ResepDBResponder(
		$resep_dbtable,
		$resep_table,
		$resep_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("resep", $resep_dbreseponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$retur_modal = new Modal("retur_add_form", "smis_form_container", "retur");
	$retur_modal->setTitle("Data Retur Penjualan Resep");
	$retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("retur_id", "retur_id", "");
	$retur_modal->addElement("", $id_hidden);
	$resep_button = new Button("", "", "Pilih");
	$resep_button->setClass("btn-info");
	$resep_button->setIsButton(Button::$ICONIC);
	$resep_button->setIcon("icon-white ".Button::$icon_list_alt);
	$resep_button->setAction("resep.chooser('resep', 'resep_button', 'resep', resep)");
	$resep_button->setAtribute("id='resep_browse'");
	$resep_text = new Text("retur_id_resep", "retur_id_resep", "");
	$resep_text->setAtribute("disabled='disabled'");
	$resep_text->setClass("smis-one-option-input");
	$resep_input_group = new InputGroup("");
	$resep_input_group->addComponent($resep_text);
	$resep_input_group->addComponent($resep_button);
	$retur_modal->addElement("No. Penjualan", $resep_input_group);
	$no_resep_text = new Text("retur_no_resep", "retur_no_resep", "");
	$no_resep_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("No.Resep", $no_resep_text);
	$dokter_text = new Text("retur_dokter", "retur_dokter", "");
	$dokter_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Dokter", $dokter_text);
	$noreg_text = new Text("retur_noreg", "retur_noreg", "");
	$noreg_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("No. Registrasi", $noreg_text);
	$nrm_text = new Text("retur_nrm", "retur_nrm", "");
	$nrm_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("NRM", $nrm_text);
	$pasien_text = new Text("retur_pasien", "retur_pasien", "");
	$pasien_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Pasien", $pasien_text);
	$alamat_text = new Text("retur_alamat", "retur_alamat", "");
	$alamat_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Alamat", $alamat_text);
	$dretur_table = new Table(
		array("Obat", "Tgl. Exp.", "Subtotal", "Jumlah Beli", "Jumlah Retur"),
		"",
		null,
		true
	);
	$dretur_table->setName("dretur");
	$dretur_table->setFooterVisible(false);
	$dretur_table->setAddButtonEnable(false);
	$dretur_table->setReloadButtonEnable(false);
	$dretur_table->setPrintButtonEnable(false);
	$retur_modal->addBody("dretur_table", $dretur_table);
	$retur_button = new Button("", "", "Simpan");
	$retur_button->setClass("btn-success");
	$retur_button->setIcon("fa fa-floppy-o");
	$retur_button->setIsButton(Button::$ICONIC);
	$retur_button->setAtribute("id='retur_save'");
	$retur_button->setAction("retur.save()");
	$retur_modal->addFooter($retur_button);
	
	$dretur_modal = new Modal("dretur_add_form", "smis_form_container", "dretur");
	$dretur_modal->setTitle("Data Detail Retur Penjualan Resep");
	$id_hidden = new Hidden("dretur_id", "dretur_id", "");
	$dretur_modal->addElement("", $id_hidden);
	$nama_obat_text = new Text("dretur_nama_obat", "dretur_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Obat", $nama_obat_text);
	$jumlah_hidden = new Hidden("dretur_jumlah", "dretur_jumlah", "");
	$dretur_modal->addElement("", $jumlah_hidden);
	$f_jumlah_text = new Text("dretur_f_jumlah", "dretur_f_jumlah", "");
	$f_jumlah_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Jml. Awal", $f_jumlah_text);
	$jumlah_retur_text = new Text("dretur_jumlah_retur", "dretur_jumlah_retur", "");
	$dretur_modal->addElement("Jml. Retur", $jumlah_retur_text);
	$satuan_text = new Text("dretur_satuan", "dretur_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Satuan", $satuan_text);
	$dretur_button = new Button("", "", "Simpan");
	$dretur_button->setClass("btn-success");
	$dretur_button->setIcon("fa fa-floppy-o");
	$dretur_button->setIsButton(Button::$ICONIC);
	$dretur_button->setAtribute("id='dretur_save'");
	$dretur_modal->addFooter($dretur_button);
	
	$v_retur_modal = new Modal("v_retur_add_form", "smis_form_container", "v_retur");
	$v_retur_modal->setTitle("Data Retur Penjualan Resep");
	$v_retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("v_retur_id", "v_retur_id", "");
	$v_retur_modal->addElement("", $id_hidden);
	$resep_text = new Text("v_retur_id_resep", "v_retur_id_resep", "");
	$resep_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Penjualan", $resep_text);
	$no_resep_text = new Text("v_retur_no_resep", "v_retur_no_resep", "");
	$no_resep_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Resep", $no_resep_text);
	$dokter_text = new Text("v_retur_dokter", "v_retur_dokter", "");
	$dokter_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Dokter", $dokter_text);
	$noreg_text = new Text("v_retur_noreg", "v_retur_noreg", "");
	$noreg_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Registrasi", $noreg_text);
	$nrm_text = new Text("v_retur_nrm", "v_retur_nrm", "");
	$nrm_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("NRM", $nrm_text);
	$pasien_text = new Text("v_retur_pasien", "v_retur_pasien", "");
	$pasien_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Pasien", $pasien_text);
	$alamat_text = new Text("v_retur_alamat", "v_retur_alamat", "");
	$alamat_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Alamat", $alamat_text);
	$v_dretur_table = new Table(
		array("Obat", "Harga Satuan", "Jumlah Retur"),
		"",
		null,
		true
	);
	$v_dretur_table->setName("v_dretur");
	$v_dretur_table->setFooterVisible(false);
	$v_dretur_table->setAddButtonEnable(false);
	$v_dretur_table->setReloadButtonEnable(false);
	$v_dretur_table->setPrintButtonEnable(false);
	$v_retur_modal->addBody("v_dretur_table", $v_dretur_table);
	$v_retur_button = new Button("", "", "OK");
	$v_retur_button->setClass("btn-success");
	$v_retur_button->setAction("$($(this).data('target')).smodal('hide')");
	$v_retur_modal->addFooter($v_retur_button);
	
	echo $v_retur_modal->getHtml();
	echo $dretur_modal->getHtml();
	echo $retur_modal->getHtml();
	echo $retur_penjualan_resep_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/retur_penjualan_resep_action.js", false);
	echo addJS("farmasi/js/dretur_penjualan_resep_action.js", false);
	echo addJS("farmasi/js/retur_penjualan_resep_resep_action.js", false);
	echo addJS("farmasi/js/retur_penjualan_resep.js", false);
	echo addCSS("farmasi/css/retur_penjualan_resep.css", false);
?>