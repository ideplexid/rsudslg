<?php
	require_once("farmasi/responder/ObatKSDBResponder.php");
	global $db;
	
	$form = new Form("rhpp_form", "", "Farmasi : Riwayat HPP Obat");
	$id_obat_hidden = new Hidden("rhpp_id_obat", "rhpp_id_obat", "");
	$form->addElement("", $id_obat_hidden);
	$kode_obat_text = new Text("rhpp_kode_obat", "rhpp_kode_obat", "");
	$kode_obat_text->setClass("smis-one-option-input");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setAction("obat_rhpp.chooser('obat_rhpp', 'obat_rhpp_button', 'obat_rhpp', obat_rhpp)");
	$obat_button->setIcon("icon-white icon-list-alt");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setAtribute("id='obat_rhpp_browse'");
	$obat_input_group = new InputGroup("");
	$obat_input_group->addComponent($kode_obat_text);
	$obat_input_group->addComponent($obat_button);
	$form->addElement("Kode Obat", $obat_input_group);
	$obat_text = new Text("rhpp_nama_obat", "rhpp_nama_obat", "");
	$obat_text->setAtribute("disabled='disabled'");
	$form->addElement("Nama Obat", $obat_text);
	$nama_jenis_obat_text = new Text("rhpp_nama_jenis_obat", "rhpp_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$form->addElement("Jenis Obat", $nama_jenis_obat_text);
	$tanggal_from_text = new Text("rhpp_tanggal_from", "rhpp_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("rhpp_tanggal_to", "rhpp_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-success");
	$show_button->setAction("riwayat_hpp.view()");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAtribute("id='show_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($show_button);
	$form->addElement("", $button_group);
		
	$table = new Table(
		array("No.", "Tgl. Dibuat", "Tgl. Transaksi", "HPP", "Jenis Transaksi", "Keterangan"),
		"",
		null,
		true
	);
	$table->setName("riwayat_hpp");
	$table->setAction(false);
	
	$obat_rhpp_table = new Table(
		array("ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat"),
		"",
		null, 
		true
	);
	$obat_rhpp_table->setName("obat_rhpp");
	$obat_rhpp_table->setModel(Table::$SELECT);
	$obat_rhpp_adapter = new SimpleAdapter();
	$obat_rhpp_adapter->add("ID Obat", "id");
	$obat_rhpp_adapter->add("Kode Obat", "kode_obat");
	$obat_rhpp_adapter->add("Nama Obat", "nama_obat");
	$obat_rhpp_adapter->add("Jenis Obat", "nama_jenis_obat");
	$obat_rhpp_dbtable = new DBTable($db, "smis_frm_dobat_masuk");
	$obat_rhpp_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%') ";
	}
	$query_value = "
		SELECT DISTINCT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat
		FROM smis_frm_dobat_masuk
		WHERE " . $filter . "
		ORDER BY kode_obat, nama_jenis_obat, nama_obat ASC
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat
			FROM smis_frm_dobat_masuk
			WHERE " . $filter . "
			ORDER BY kode_obat, nama_jenis_obat, nama_obat ASC
		) v
	";
	$obat_rhpp_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_rhpp_dbresponder = new ObatKSDBResponder(
		$obat_rhpp_dbtable,
		$obat_rhpp_table,
		$obat_rhpp_adapter
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("obat_rhpp", $obat_rhpp_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Tgl. Dibuat", "tanggal", "date d-m-Y, H:i:s");
		$adapter->add("Tgl. Transaksi", "tanggal_transaksi", "date d-m-Y");
		$adapter->add("HPP", "hpp", "money");
		$adapter->add("Jenis Transaksi", "in_out", "trivial_1_Masuk_Keluar");
		$adapter->add("Keterangan", "keterangan");
		$dbtable = new DBTable($db, "smis_frm_histori_hpp");
		$dbtable->addCustomKriteria(" tanggal ", " >= '" . $_POST['tanggal_from'] . " 00:00:00'");
		$dbtable->addCustomKriteria(" tanggal ", " <= '" . $_POST['tanggal_to'] . " 23:59:59'");
		$dbtable->addCustomKriteria(" id_obat ", " = '" . $_POST['id_obat'] . "'");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo "<div class='alert alert-block alert-info'>" .
			 "<h4>Catatan</h4>" .
			 "HPP merupakan hasil perhitungan dari Total Nilai HPP (Sisa Stok x HPP) dibagi dengan Total Sisa Stok (Metode : Average)." .
		 "</div>";
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function ObatRHPPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ObatRHPPAction.prototype.constructor = ObatRHPPAction;
	ObatRHPPAction.prototype = new TableAction();
	ObatRHPPAction.prototype.selected = function(json) {
		$("#rhpp_id_obat").val(json.id);
		$("#rhpp_kode_obat").val(json.kode_obat);
		$("#rhpp_nama_obat").val(json.nama_obat);
		$("#rhpp_nama_jenis_obat").val(json.nama_jenis_obat);
	};

	function RiwayatHPPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	RiwayatHPPAction.prototype.constructor = RiwayatHPPAction;
	RiwayatHPPAction.prototype = new TableAction();
	RiwayatHPPAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['id_obat'] = $("#rhpp_id_obat").val();
		data['kode_obat'] = $("#rhpp_kode_obat").val();
		data['nama_obat'] = $("#rhpp_nama_obat").val();
		data['nama_jenis_obat'] = $("#rhpp_nama_jenis_obat").val();
		data['tanggal_from'] = $("#rhpp_tanggal_from").val();
		data['tanggal_to'] = $("#rhpp_tanggal_to").val();
		return data;
	};

	var obat_rhpp;
	var riwayat_hpp;
	$(document).ready(function() {
		$(".mydate").datepicker();
		obat_rhpp = new ObatRHPPAction(
			"obat_rhpp",
			"farmasi",
			"riwayat_hpp",
			new Array()
		);
		obat_rhpp.setSuperCommand("obat_rhpp");
		riwayat_hpp = new RiwayatHPPAction(
			"riwayat_hpp",
			"farmasi",
			"riwayat_hpp",
			new Array()
		);
		riwayat_hpp.view();
	});
</script>