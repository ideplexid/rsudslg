<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	if (isset($_POST['data'])) {
		$id = $_POST['data'];
		$dbtable = new DBTable($db, "smis_frm_retur_obat");
		$header_row = $dbtable->get_row("
			SELECT *
			FROM smis_frm_retur_obat
			WHERE id = '" . $id . "'
		");

		$header = array();
		$list_debit_kredit = array();

		if ($header_row != null) {
			$kode_hutang = "";
			$kode_ppn = "";
			$kode_diskon_pembelian = "";
			$kode_pembelian = "";

			$data['id'] = $header_row->id_vendor;
			$service_consumer = new ServiceConsumer(
				$db,
				"get_kode_acc_vendor",
				$data,
				"pembelian"
			);
			$content = $service_consumer->execute()->getContent();
			if ($content != null) {
				$kode_hutang = $content['kode_hutang'];
				$kode_ppn = $content['kode_ppn'];
				$kode_diskon_pembelian = $content['kode_diskon_pembelian'];
				$kode_pembelian = $content['kode_pembelian'];
			}

			$total_hutang = 0;
			$total_diskon = 0;
			$total_pembelian = 0;
			$drows = $dbtable->get_result("
				SELECT a.*, b.id_obat, b.nama_obat, b.hna
				FROM smis_frm_dretur_obat a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id
				WHERE a.id_retur_obat = '" . $id . "' AND a.prop NOT LIKE 'del'
			");
			if ($drows != null) {
				foreach ($drows as $dr) {
					$subtotal = $dr->jumlah * ($dr->hna / 1.1);
					$diskon = 0;
					$total_diskon += $diskon;
					$subtotal = round($subtotal - $diskon);
					$total_hutang += $subtotal;
					$total_pembelian += $subtotal;
				}
			}
			$global_diskon = 0;
			$global_diskon = floor($global_diskon);
			$total_diskon += $global_diskon;
			$total_hutang = $total_hutang - $global_diskon;
			$total_pembelian = $total_pembelian - $global_diskon;
			$ppn = floor($total_hutang * 0.1);
			$total_pembelian = $total_pembelian - $global_diskon + $ppn;

			$total_debit = 0;
			$total_kredit = 0;
			$data_debit_kredit = array(
				"akun"		=> $kode_hutang,
				"debet"		=> $total_hutang,
				"kredit"	=> 0,
				"ket"		=> "Hutang Retur Pembelian ID Transaksi : " . $id,
				"code"		=> "debit-farmasi-hutang_retur_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_ppn,
				"debet"		=> $ppn,
				"kredit"	=> 0,
				"ket"		=> "PPn Retur Pembelian ID Transaksi : " . $id,
				"code"		=> "debit-farmasi-ppn_retur_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_diskon_pembelian,
				"debet"		=> 0,
				"kredit"	=> $total_diskon,
				"ket"		=> "Diskon Pembelian Retur Pembelian ID Transaksi : " . $id,
				"code"		=> "kredit-farmasi-diskon_retur_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$data_debit_kredit = array(
				"akun"		=> $kode_pembelian,
				"debet"		=> 0,
				"kredit"	=> $total_pembelian,
				"ket"		=> "Persediaan Retur Pembelian ID Transaksi : " . $id,
				"code"		=> "kredit-farmasi-pembelian_retur_bbm-" . $id
			);
			$list_debit_kredit[] = $data_debit_kredit;
			$total_debit += $data_debit_kredit['debet'];
			$total_kredit += $data_debit_kredit['kredit'];

			$header = array(
				"tanggal"		=> $header_row->tanggal,
				"keterangan"	=> "Retur Pembelian ID Transaksi " . $id,
				"code"			=> "retur_bbm-farmasi-" . $id,
				"nomor"			=> $header_row->no_faktur,
				"debet"			=> $total_debit,
				"kredit"		=> $total_kredit,
				"io"			=> 0
			);
		}

		$transaction = array(
			"header"	=> $header,
			"content"	=> $list_debit_kredit
		);
		$data_final = array();
		$data_final[] = $transaction;
		echo json_encode($data_final);
	}
?>