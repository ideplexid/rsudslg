<?php 
	if(isset($_POST['command'])) {
		class ResepResponder extends ServiceProvider {
			public function edit() {
				$id = $_POST['id'];
				//header:
				$header_row = $this->dbtable->get_row("
					SELECT *
					FROM smis_frm_penjualan_resep
					WHERE id = '" . $id . "'
				");
				$header = array();
				$header['id'] = $id;
				$header['tanggal'] = $header_row->tanggal;
				$header['nrm_pasien'] = $header_row->nrm_pasien;
				$header['noreg_pasien'] = $header_row->noreg_pasien;
				$header['nama_pasien'] = $header_row->nama_pasien;
				$header['jenis_pasien'] = $header_row->jenis_pasien;
				$header['nama_dokter'] = $header_row->nama_dokter;
				$header['total'] = $header_row->total;
				$header['diskon'] = $header_row->diskon;
				$header['t_diskon'] = $header_row->t_diskon;
				if ($header_row->tipe == "resep")
					$header['jenis'] = "Penjualan Resep";
				else if ($header_row->tipe == "instansi_lain")
					$header['jenis'] = "Penjualan ke Instansi Lain";
				else if ($header_row->tipe == "bebas")
					$header['jenis'] = "Penjualan Bebas";
				else if ($header_row->tipe == "resep_luar")
					$header['jenis'] = "Penjualan Resep Luar";
				$header['embalase'] = $header_row->embalase;
				$header['tuslah'] = $header_row->tusla;
				$header['biaya_racik'] = $header_row->biaya_racik;
				$data['header'] = $header;
				//detail obat jadi:
				$detail_jadi_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_penjualan_obat_jadi
					WHERE id_penjualan_resep = '" . $id . "'
				");
				$detail_jadi = array();
				foreach($detail_jadi_rows as $djr) {
					$detail = array();
					$detail['nama'] = $djr->nama_obat;
					$detail['jumlah'] = $djr->jumlah;
					$detail['satuan'] = $djr->satuan;
					$detail['harga_satuan'] = $djr->harga;
					$detail['harga_total'] = $djr->jumlah * $djr->harga;
					$detail_jadi[] = $detail;
				}
				$data['detail_jadi'] = $detail_jadi;
				//detail obat racikan:
				$detail_racikan_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_penjualan_obat_racikan
					WHERE id_penjualan_resep = '" . $id . "'
				");
				$detail_racikan = array();
				foreach($detail_racikan_rows as $drr) {
					$detail = array();
					$detail['nama'] = $drr->nama;
					$detail['jumlah'] = $drr->jumlah;
					$detail['satuan'] = $djr->satuan;
					$detail['harga_satuan'] = $drr->harga;
					$detail['harga_total'] = $drr->jumlah * $drr->harga;
					$detail_racikan[] = $detail;
				}
				$data['detail_racikan'] = $detail_racikan;
				//detail retur penjualan resep:
				$retur_penjualan_resep_rows = $this->dbtable->get_result("
					SELECT id, tanggal, persentase_retur
					FROM smis_frm_retur_penjualan_resep
					WHERE id_penjualan_resep = '" . $id . "'
				");
				$detail_retur = array();
				foreach($retur_penjualan_resep_rows as $rprr) {
					$detail = array();
					$detail['tanggal'] = $rprr->tanggal;
					$nominal_retur = 0;
					$detail_retur_rows = $this->dbtable->get_result("
						SELECT *
						FROM smis_frm_dretur_penjualan_resep
						WHERE id_retur_penjualan_resep = '" . $rprr->id . "'
					");
					foreach($detail_retur_rows as $drr) {
						$nominal_retur = $nominal_retur + $drr->subtotal;
					}
					$detail['nominal_retur'] = ($rprr->persentase_retur * $nominal_retur) / 100;
					$detail_retur[] = $detail;
				}
				$data['detail_retur'] = $detail_retur;
				return $data;
			}
		}
		$dbtable=new DBTable($db, "smis_frm_penjualan_resep");
		$dbtable->addCustomKriteria(" dibatalkan ", " = '0' ");
		$dbtable->setUseWhereforView(true);
		$service=new ResepResponder($dbtable);
		$pack=$service->command($_POST['command']);
		echo json_encode($pack);
	}
?>