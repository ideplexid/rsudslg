<?php
	global $db;
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$total = 0;
		$dbtable = new DBTable($db, "smis_frm_penjualan_resep");
		$row = $dbtable->get_row("
			SELECT SUM(total) AS 'total'
			FROM smis_frm_penjualan_resep
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del' AND dibatalkan = '0'
		");
		if ($row != null)
			$total = $row->total;

		$retur_rows = $dbtable->get_result("
			SELECT a.*
			FROM smis_frm_retur_penjualan_resep a LEFT JOIN smis_frm_penjualan_resep b ON a.id_penjualan_resep = b.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.dibatalkan = '0' AND b.noreg_pasien = '" . $noreg_pasien . "'
		");
		if ($retur_rows != null) {
			foreach ($retur_rows as $rr) {
				$dretur_rows = $dbtable->get_result("
					SELECT *
					FROM smis_frm_dretur_penjualan_resep
					WHERE id_retur_penjualan_resep = '" . $rr->id . "' AND prop NOT LIKE 'del'
				");
				if ($dretur_rows != null) {
					foreach ($dretur_rows as $dr)
						$total -= $dr->jumlah * $dr->harga * $rr->persentase_retur / 100;
				}
			}
		}
		echo json_encode($total);
	}
?>