<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	$_DB_RESEP="smis_frm_penjualan_resep";
	$_DB_OBAT_JADI="smis_frm_penjualan_obat_jadi";
	$_DB_OBAT_RACIKAN="smis_frm_penjualan_obat_racikan";
	$_DB_OBAT_BAHAN_RACIKAN="smis_frm_bahan_pakai_obat_racikan";
	$_DB_RETUR="smis_frm_retur_penjualan_resep";
	$_DB_DRETUR="smis_frm_dretur_penjualan_resep";
	$_DB_STOK="smis_frm_stok_obat";
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		$dbtable = new DBTable($db, "$_DB_RESEP");
		
		$resep_rows = $dbtable->get_result("
			SELECT *
			FROM $_DB_RESEP
			WHERE prop NOT LIKE 'del' AND noreg_pasien = '" . $noreg_pasien . "' AND tipe = 'resep' AND dibatalkan = '0'
		");

		$penjualan_result = array();
		if ($resep_rows != null) {
			foreach($resep_rows as $rr) {
				$total_tagihan_tercatat = $rr->total;
				$diskon_tercatat = $rr->diskon;
				if ($rr->t_diskon == "persen")
					$diskon_tercatat = ($total_tagihan_tercatat / (100 - $diskon_tercatat) * 100) * $diskon_tercatat / 100;
				$obat_jadi_rows = $dbtable->get_result("
					SELECT *
					FROM $_DB_OBAT_JADI
					WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $rr->id . "'
				");
				$total = 0;
				if ($obat_jadi_rows != null) {
					foreach ($obat_jadi_rows as $ojr) {
						$kode_pendapatan = "";
						$kode_diskon_penjualan = "";
						$kode_ppn_keluar = "";
						$data['id'] = $ojr->id_obat;
						$service_consumer = new ServiceConsumer(
							$db,
							"get_kode_acc_barang",
							$data,
							"perencanaan"
						);
						$content = $service_consumer->execute()->getContent();
						if ($content != null) {
							$kode_pendapatan = $content['kode_pendapatan'];
							$kode_diskon_penjualan = $content['kode_diskon_penjualan'];
							$kode_ppn_keluar = $content['kode_ppn_keluar'];
						}

						$diskon_penjualan = 0;
						if ($total_tagihan_tercatat > 0 && $diskon_tercatat > 0)
							$diskon_penjualan = $ojr->subtotal / $total_tagihan_tercatat * $diskon_tercatat;
						$nilai_pendapatan = $ojr->subtotal - $diskon_penjualan;
						$ppn_keluar = $nilai_pendapatan * 0.1;
						$nilai_pendapatan = $nilai_pendapatan - $ppn_keluar;

						$info = array(
							'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
							'nama'				=> "Diskon Jual " . $ojr->nama_obat,
							'jumlah'			=> 1,
							'biaya'				=> 0,
							'keterangan'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - Diskon Jual " . $ojr->nama_obat . " : " . ArrayAdapter::format("only-money", $diskon_penjualan),
							'id'				=> "J" . $rr->id . "-OJ" . $ojr->id . "-DISKON_JUAL",
							'start'				=> $rr->tanggal,
							'end'				=> $rr->tanggal,
							'debet'				=> $kode_diskon_penjualan,
							'kredit'			=> "",
							'akunting_nilai'	=> $diskon_penjualan,
							'akunting_only'		=> "1",
							'akunting_name'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - Diskon Jual " . $ojr->nama_obat
						);
						if ($diskon_penjualan > 0)
							$penjualan_result[] = $info;

						$info = array(
							'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
							'nama'				=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - PPn Jual " . $ojr->nama_obat,
							'jumlah'			=> 1,
							'biaya'				=> 0,
							'keterangan'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - PPn Jual " . $ojr->nama_obat . " : " . ArrayAdapter::format("only-money", $ppn_keluar),
							'id'				=> "J" . $rr->id . "-OJ" . $ojr->id . "-PPN_JUAL",
							'start'				=> $rr->tanggal,
							'end'				=> $rr->tanggal,
							'debet'				=> "",
							'kredit'			=> $kode_ppn_keluar,
							'akunting_nilai'	=> $ppn_keluar,
							'akunting_only'		=> "1",
							'akunting_name'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - PPn Jual " . $ojr->nama_obat
						);
						$penjualan_result[] = $info;
						
						$info = array(
							'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
							'nama'				=> "Penjualan " . $ojr->nama_obat,
							'jumlah'			=> $ojr->jumlah,
							'biaya'				=> $nilai_pendapatan + $ppn_keluar - $diskon_penjualan,
							'keterangan'		=> "Penjualan " . $ojr->nama_obat . " : " . $ojr->jumlah . " x " . ArrayAdapter::format("only-money", ($nilai_pendapatan + $ppn_keluar - $diskon_penjualan) / $ojr->jumlah),
							'id'				=> "J" . $rr->id . "-OJ" . $ojr->id . "-PENDAPATAN",
							'start'				=> $rr->tanggal,
							'end'				=> $rr->tanggal,
							'debet'				=> "",
							'kredit'			=> $kode_pendapatan,
							'akunting_nilai'	=> $nilai_pendapatan,
							'akunting_only'		=> "0",
							'akunting_name'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - Pendapatan Jual " . $ojr->nama_obat
						);
						$penjualan_result[] = $info;
					}
				}

				$obat_racikan_rows = $dbtable->get_result("
					SELECT *
					FROM $_DB_OBAT_RACIKAN
					WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $rr->id . "'
				");
				if ($obat_racikan_rows != null) {
					foreach ($obat_racikan_rows as $orr) {
						$bahan_racikan_rows = $dbtable->get_result("
							SELECT *
							FROM $_DB_OBAT_BAHAN_RACIKAN
							WHERE id_penjualan_obat_racikan = '" . $orr->id . "'
						");
						if ($bahan_racikan_rows != null) {
							$jasa_racik = 0;
							if ($bahan_racikan_rows != null)
								$jasa_racik = $orr->biaya_racik / count($bahan_racikan_rows);
							foreach ($bahan_racikan_rows as $brr) {
								$embalase = $brr->embalase;
								$tusla = $brr->tusla;
								$subtotal = $brr->jumlah * $brr->harga + $embalase + $tusla + $jasa_racik;

								$kode_pendapatan = "";
								$kode_diskon_penjualan = "";
								$kode_ppn_keluar = "";
								
								$data['id'] = $brr->id_obat;
								$service_consumer = new ServiceConsumer(
									$db,
									"get_kode_acc_barang",
									$data,
									"perencanaan"
								);
								$content = $service_consumer->execute()->getContent();
								if ($content != null) {
									$kode_pendapatan = $content['kode_pendapatan'];
									$kode_diskon_penjualan = $content['kode_diskon_penjualan'];
									$kode_ppn_keluar = $content['kode_ppn_keluar'];
								}

								$diskon_penjualan = 0;
								if ($total_tagihan_tercatat > 0 && $diskon_tercatat > 0)
									$diskon_penjualan = $subtotal / $total_tagihan_tercatat * $diskon_tercatat;
								$nilai_pendapatan = $subtotal - $diskon_penjualan;
								$ppn_keluar = $nilai_pendapatan * 0.1;
								$nilai_pendapatan = $nilai_pendapatan - $ppn_keluar;

								$info = array(
									'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
									'nama'				=> "Diskon Jual Racikan " . $orr->nama . " Bahan " . $brr->nama_obat,
									'jumlah'			=> 1,
									'biaya'				=> 0,
									'keterangan'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - Diskon Jual Racikan " . $orr->nama . " Bahan " . $brr->nama_obat . " : " . ArrayAdapter::format("only-money", $diskon_penjualan),
									'id'				=> "J" . $rr->id . "-OR" . $orr->id . "." . $brr->id . "-DISKON_JUAL",
									'start'				=> $rr->tanggal,
									'end'				=> $rr->tanggal,
									'debet'				=> $kode_diskon_penjualan,
									'kredit'			=> "",
									'akunting_nilai'	=> $diskon_penjualan,
									'akunting_only'		=> "1",
									'akunting_name'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - Diskon Jual Racikan " . $orr->nama . " Bahan " . $brr->nama_obat

								);
								if ($diskon_penjualan > 0)
									$penjualan_result[] = $info;

								$info = array(
									'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
									'nama'				=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - PPn Jual Racikan " . $orr->nama . " Bahan " . $brr->nama_obat,
									'jumlah'			=> 1,
									'biaya'				=> 0,
									'keterangan'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - PPn Jual Racikan " . $orr->nama . " Bahan " . $brr->nama_obat . " : " . ArrayAdapter::format("only-money", $ppn_keluar),
									'id'				=> "J" . $rr->id . "-OR" . $orr->id . "." . $brr->id . "-PPN_JUAL",
									'start'				=> $rr->tanggal,
									'end'				=> $rr->tanggal,
									'debet'				=> "",
									'kredit'			=> $kode_ppn_keluar,
									'akunting_nilai'	=> $ppn_keluar,
									'akunting_only'		=> "1",
									'akunting_name'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - PPn Jual Racikan " . $orr->nama . " Bahan " . $brr->nama_obat
								);
								$penjualan_result[] = $info;
								
								$info = array(
									'waktu'				=> ArrayAdapter::format("date d M Y", $rr->tanggal),
									'nama'				=> "Penjualan Racikan " . $orr->nama . " Bahan " . $brr->nama_obat,
									'jumlah'			=> $brr->jumlah,
									'biaya'				=> $nilai_pendapatan + $ppn_keluar - $diskon_penjualan,
									'keterangan'		=> "Penjualan Racikan " . $orr->nama . " Bahan " . $brr->nama_obat . " : " . $brr->jumlah . " x " . ArrayAdapter::format("only-money", ($nilai_pendapatan + $ppn_keluar - $diskon_penjualan) / $brr->jumlah),
									'id'				=> "J" . $rr->id . "-OR" . $orr->id . "." . $brr->id . "-PENDAPATAN",
									'start'				=> $rr->tanggal,
									'end'				=> $rr->tanggal,
									'debet'				=> "",
									'kredit'			=> $kode_pendapatan,
									'akunting_nilai'	=> $nilai_pendapatan,
									'akunting_only'		=> "0",
									'akunting_name'		=> "Resep No. " . $rr->nomor_resep . " ID. " . $rr->id . " - Pendapatan Jual Racikan " . $orr->nama . " Bahan " . $brr->nama_obat
								);
								$penjualan_result[] = $info;
							}
						}
					}
				}
			}
		}

		$return_resep_rows = $dbtable->get_result("
			SELECT a.id, b.id AS 'id_dretur', a.tanggal, a.id_penjualan_resep, c.id_obat, c.nama_obat, b.jumlah, a.persentase_retur, b.harga, d.nomor_resep
			FROM (($_DB_RETUR a 
					LEFT JOIN 
							$_DB_DRETUR b ON a.id = b.id_retur_penjualan_resep) 
							LEFT JOIN $_DB_STOK c ON b.id_stok_obat = c.id) 
							LEFT JOIN $_DB_RESEP d ON a.id_penjualan_resep = d.id
			WHERE a.prop NOT LIKE 'del' AND a.dibatalkan = '0' AND b.prop NOT LIKE 'del' AND d.noreg_pasien = '" . $noreg_pasien . "' 
		");
		$return_result = array();
		if ($return_resep_rows != null) {
			foreach ($return_resep_rows as $rrr) {
				$kode_pendapatan = "";

				$data['id'] = $rrr->id_obat;
				$service_consumer = new ServiceConsumer(
					$db,
					"get_kode_acc_barang",
					$data,
					"perencanaan"
				);
				$content = $service_consumer->execute()->getContent();
				if ($content != null) {
					$kode_pendapatan = $content['kode_pendapatan'];
				}

				$nilai_pendapatan = -1 * $rrr->jumlah * $rrr->harga * $rrr->persentase_retur / 100;

				$info = array(
					'waktu'				=> ArrayAdapter::format("date d M Y", $rrr->tanggal),
					'nama'				=> "Return Resep No. " . $rrr->nomor_resep . " ID. " . $rrr->id_penjualan_resep . " - " . $rrr->nama_obat,
					'jumlah'			=> $rrr->jumlah,
					'biaya'				=> $nilai_pendapatan,
					'keterangan'		=> "Retur Obat " . $rrr->nama_obat . " : " . $rrr->jumlah . " x " . ArrayAdapter::format("only-money", $nilai_pendapatan / $rrr->jumlah),
					'id'				=> "R" . $rrr->id . "." . $rrr->id_dretur . "-PENDAPATAN",
					'start'				=> $rrr->tanggal,
					'end'				=> $rrr->tanggal,
					'debet'				=> "",
					'kredit'			=> $kode_pendapatan,
					'akunting_nilai'	=> $nilai_pendapatan,
					'akunting_only'		=> "0",
					'akunting_name'		=> "Return Resep No. " . $rrr->nomor_resep . " ID. " . $rrr->id_penjualan_resep . " - Pendapatan " . $rrr->nama_obat
				);
				$return_result[] = $info;
			}
		}
		
		echo json_encode(array(
			'selesai' => "1",
			'exist' => "1",
			'reverse' => "0",
			'cara_keluar' => "Selesai",
			'data' => array(
				'penjualan_resep'	=> array(
					'jasa_pelayanan'	=> 0,
					'result'			=> $penjualan_result
				),
				'return_resep'		=> array(
					'jasa_pelayanan'	=> 0,
					'result'			=> $return_result
				)
			),
		));
	}
?>