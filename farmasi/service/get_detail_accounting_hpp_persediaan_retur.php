<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	if (isset($_POST['data'])) {
		$id = $_POST['data'];
		$dbtable = new DBTable($db, "smis_frm_retur_penjualan_resep");
		$header_row = $dbtable->get_row("
			SELECT a.*, b.noreg_pasien
			FROM smis_frm_retur_penjualan_resep a LEFT JOIN smis_frm_penjualan_resep b ON a.id_penjualan_resep = b.id
			WHERE a.id = '" . $id . "'
		");

		$header = array();
		$list_debit_kredit = array();

		if ($header_row != null) {
			$total_debit = 0;
			$total_kredit = 0;

			$detail_rows = $dbtable->get_result("
				SELECT a.*, b.id_obat, b.nama_obat
				FROM smis_frm_dretur_penjualan_resep a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id
				WHERE a.id_retur_penjualan_resep = '" . $id . "' AND a.prop NOT LIKE 'del'
			");
			if ($detail_rows != null) {
				foreach ($detail_rows as $dr) {
					$kode_hpp = "";
					$kode_nilai_persediaan = "";

					$data['id'] = $dr->id_obat;
					$service_consumer = new ServiceConsumer(
						$db,
						"get_kode_acc_barang",
						$data,
						"perencanaan"
					);
					$content = $service_consumer->execute()->getContent();
					if ($content != null) {
						$kode_hpp = $content['kode_hpp'];
						$kode_nilai_persediaan = $content['kode_nilai_persediaan'];
					}

					$hpp = 0;
					$hpp_row = $dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $dr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $header_row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null)
						$hpp = $hpp_row->hpp;

					$nilai_hpp = $dr->jumlah * $hpp;
					$nilai_persediaan = $nilai_hpp;

					$data_debit_kredit = array(
						"akun"		=> $kode_hpp,
						"debet"		=> 0,
						"kredit"	=> $hpp,
						"ket"		=> "HPP Obat " . $dr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "kredit-farmasi-hpp-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];

					$data_debit_kredit = array(
						"akun"		=> $kode_nilai_persediaan,
						"debet"		=> $nilai_persediaan,
						"kredit"	=> 0,
						"ket"		=> "Nilai Persediaan Obat " . $dr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "kredit-farmasi-nilai_persediaan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];
				}
			}
			$header = array(
				"tanggal"		=> $header_row->tanggal,
				"keterangan"	=> "Persediaan dari Retur Penjualan ID Transaksi " . $id . " No. Reg. : " . $header_row->noreg_pasien,
				"code"			=> "persediaan-retur-farmasi-" . $id,
				"nomor"			=> $id,
				"debet"			=> $total_debit,
				"kredit"		=> $total_kredit,
				"io"			=> 0
			);
		}

		$transaction = array(
			"header"	=> $header,
			"content"	=> $list_debit_kredit
		);
		$data_final = array();
		$data_final[] = $transaction;
		echo json_encode($data_final);
	}
?>