<?php 
require_once 'smis-base/smis-include-service-consumer.php';

if (isset($_POST['command'])) {
	$dbtable = new DBTable($db, "smis_frm_obat_masuk");
	$dbtable->addCustomKriteria(" id ", " !='0' ");
	$dbtable->addCustomKriteria(" total_tagihan ", " > total_dibayar ");
	if (isset($_POST['id_vendor']))
		$dbtable->addCustomKriteria(" id_vendor ", " = '" . $_POST['id_vendor'] . "' ");
	$service = new ServiceProvider($dbtable);
	$pack = $service->command($_POST['command']);	
	if ($_POST['command']=="edit") {
		class DFakturAdapter extends ArrayAdapter{
			private $total = 0;
			public function adapt($row) {
				$data = array();
				$data['nama_barang'] = $row->nama_obat;
				$data['id_barang'] = $row->id_obat;
				$data['nama_jenis_barang'] = $row->nama_jenis_obat;
				$data['jumlah'] = $row->jumlah;
				$data['satuan'] = $row->satuan;
				$data['konversi'] = $row->konversi;
				$data['satuan_konversi'] = $row->satuan_konversi;
				$data['hna'] = $row->hna;
				$data['harga_sebelum_diskon'] = ($row->hna + $row->selisih) * $row->jumlah;
				$data['diskon'] = $row->diskon;
				$data['t_diskon'] = $row->t_diskon;
				if ($data['t_diskon'] == "nominal") {
					$data['val_diskon'] = ArrayAdapter::format("money Rp.", $row->diskon);
					$data['n_diskon'] = $row->diskon * 1;
					$data['harga_setelah_diskon'] = $data['harga_sebelum_diskon'] - $data['n_diskon'];
				} else {
					$data['val_diskon'] = $row->diskon . "%";
					$data['n_diskon'] = $data['harga_sebelum_diskon'] * ($row->diskon * 1) / 100;
					$data['harga_setelah_diskon'] = $data['harga_sebelum_diskon'] - $data['n_diskon'];
				}
				$data['hbayar'] = $data['harga_setelah_diskon'];
				$this->total += $data['hbayar'];
				return $data;
			}
			public function getTotalBayar() {
				return $this->total;
			}
		}
		
		$pack = (array) $pack;
		$id_po = $pack['id_po'];
		$id_faktur = $pack['id'];
		$diskon = $pack['diskon'];
		$t_diskon = $pack['t_diskon'];		
		
		$responder = new ServiceResponder($db, $table, $adapter, "get_daftar_po");
		$responder->addData("id", $id_po);
		$pack_po = $responder->edit();
		$pack_po = (array) $pack_po;
		if ($pack_po ==NULL || !isset($pack_po['nama_vendor']) || $pack_po['nama_vendor'] == "" || $pack_po['nama_vendor'] == null) {
			$pack_po['nama_vendor'] = $pack['nama_vendor'];
			$pack_po['keterangan'] = "TIDAK ADA SP/PO";
		}
		$pack['po'] = $pack_po;
		
		$dfaktur = new DBTable($db, "smis_frm_dobat_masuk");
		$dfaktur->setShowAll(true);
		$dfaktur->addCustomKriteria("id_obat_masuk", "='" . $id_faktur . "'");
		$detail = $dfaktur->view("", "0");
		$adapter = new DFakturAdapter();
		$detail = $adapter->getContent($detail['data']);
		$pack['detail'] = $detail;		
		$pack['harga_sebelum_diskon_global'] = $adapter->getTotalBayar();
		
		$val_diskon = 0;
		if ($t_diskon == "nominal") {
			$val_diskon=$pack['diskon'];
			$pack['diskon_global']=ArrayAdapter::format("money Rp.", $val_diskon);
		} else {
			$val_diskon = $pack['harga_sebelum_diskon_global'] * $diskon / 100;
			$pack['diskon_global'] = $diskon . "% ( Rp. " . ArrayAdapter::format("only-money", $val_diskon) . " )";			
		}
		$pack['total_bayar'] = $pack['harga_sebelum_diskon_global'] - $val_diskon;
		$pack['biaya_lain'] = array(
			"materai"	=> $pack['materai'],
			"ekspedisi"	=> $pack['ekspedisi']
		);
		$pack['total_bayar'] += $pack['biaya_lain']['materai'] + $pack['biaya_lain']['ekspedisi'];
	}
	echo json_encode($pack);
}

?>