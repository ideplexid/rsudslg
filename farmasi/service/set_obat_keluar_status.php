<?php 
	global $db;
	
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_update") {
			$id['id'] = $_POST['id'];
			$data['status'] = $_POST['status'];
			$data['keterangan'] = $_POST['keterangan'];
			$obat_keluar_dbtable = new DBTable($db, "smis_frm_obat_keluar");
			$obat_keluar_dbtable->update($data, $id);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
			$row = $obat_keluar_dbtable->get_row("
				SELECT *
				FROM smis_frm_obat_keluar
				WHERE id = '" . $id['id'] . "'
			");
			if ($data['status'] == "dikembalikan") {
				// restock:
				$dobat_keluar_rows = $obat_keluar_dbtable->get_result("
					SELECT *
					FROM smis_frm_dobat_keluar
					WHERE id_obat_keluar = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				foreach ($dobat_keluar_rows as $dokr) {
					$stok_obat_keluar_dbtable = new DBTable($db, "smis_frm_stok_obat_keluar");
					$stok_keluar_query = "
						SELECT *
						FROM smis_frm_stok_obat_keluar
						WHERE id_dobat_keluar = '" . $dokr->id . "'
						ORDER BY id DESC
					";
					$stok_keluar = $stok_obat_keluar_dbtable->get_result($stok_keluar_query);
					foreach($stok_keluar as $sk) {
						$stok_keluar_data = array();
						$stok_keluar_data['jumlah'] = 0;
						$stok_keluar_data['prop'] = "del";
						$stok_keluar_id['id'] = $sk->id;
						$stok_obat_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
						$stok_obat_dbtable = new DBTable($db, "smis_frm_stok_obat");
						$stok_query = "
							SELECT *
							FROM smis_frm_stok_obat
							WHERE id = '" . $sk->id_stok_obat . "'
						";
						$stok_obat_row = $stok_obat_dbtable->get_row($stok_query);
						$stok_data = array();
						$stok_data['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
						$stok_id['id'] = $sk->id_stok_obat;
						$stok_dbtable = new DBTable($db, "smis_frm_stok_obat");
						$stok_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok obat:
						$riwayat_dbtable = new DBTable($db, "smis_frm_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
						$data_riwayat['jumlah_masuk'] = $sk->jumlah;
						$data_riwayat['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Stok Keluar ke " . ArrayAdapter::format("unslug", $row->unit);
						global $user;
						$data_riwayat['nama_user'] = $_POST['nama_user'];
						$riwayat_dbtable->insert($data_riwayat);
					}
					//kartu gudang induk:
					$dok_rows = $this->dbtable->get_result("
						SELECT *
						FROM smis_frm_dobat_keluar
						WHERE id_obat_keluar = '" . $id['id'] . "' AND prop NOT LIKE 'del'
					");
					if ($dok_rows != null) {
						$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
						$header_row = $this->dbtable->get_row("
							SELECT *
							FROM smis_frm_obat_keluar
							WHERE id = '" . $id['id'] . "'
						");
						foreach ($dok_rows as $dr) {	
							$ks_data = array();
							$ks_data['f_id'] = $id['id'];
							$ks_data['unit'] = "Pembatalan Mutasi ke " . ArrayAdapter::format("unslug", $header_row->unit);
							$ks_data['no_bon'] = $id['id'];
							$ks_data['id_obat'] = $dr->id_obat;
							$ks_data['kode_obat'] = $dr->kode_obat;
							$ks_data['nama_obat'] = $dr->nama_obat;
							$ks_data['nama_jenis_obat'] = $dr->nama_jenis_obat;
							$ks_data['tanggal'] = date("Y-m-d");
							$ks_data['masuk'] = $dr->jumlah;
							$ks_data['keluar'] = 0;	
							$ks_row = $ks_dbtable->get_row("
								SELECT SUM(sisa) AS 'jumlah'
								FROM smis_frm_stok_obat b ON a.id = b.id_dobat_masuk
								WHERE prop NOT LIKE 'del' AND id_obat = '" . $dr->id_obat . "'
							");
							$ks_data['sisa'] = $ks_row->jumlah + $dr->jumlah;
							$ks_dbtable->insert($ks_data);
						}
					}
				}
			}
			//notify:
			global $notification;
			$key=md5($row->unit." ".$row->id);
			$status = ArrayAdapter::format("unslug", $_POST['status']);
			if ($status == "SUDAH")
				$status .= " DITERIMA";
			$msg="Obat Keluar ke <strong>".ArrayAdapter::format("unslug", $row->unit)."</strong> sudah dikonfirmasi (<strong>" . $status . "</strong>)";
			$notification->addNotification("Konfirmasi Obat Keluar", $key, $msg, "gudang_farmasi","obat_keluar");
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());	
?>