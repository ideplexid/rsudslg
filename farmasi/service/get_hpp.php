<?php
	global $db;

	if (isset($_POST['id_obat'])) {
		$id_obat = $_POST['id_obat'];
		$tanggal = date("Y-m-d");
		if (isset($_POST['tanggal']))
			$tanggal = $_POST['tanggal'];
		$dbtable = new DBTable($db, "smis_frm_histori_hpp");
		$row = $dbtable->get_row("
			SELECT *
			FROM smis_frm_histori_hpp
			WHERE id_obat = '" . $id_obat . "' AND DATE(tanggal) <= '" . $tanggal . "'
			ORDER BY id DESC
			LIMIT 0, 1
		");
		$hpp = 0;
		if ($row != null)
			$hpp = $row->hpp;
		echo json_encode($hpp);
	}
?>