<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	if (isset($_POST['data'])) {
		$id = $_POST['data'];
		$dbtable = new DBTable($db, "smis_frm_penjualan_resep");
		$header_row = $dbtable->get_row("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");

		$header = array();
		$list_debit_kredit = array();

		if ($header_row != null) {
			$total_tagihan_tercatat = $header_row->total;
			$diskon_tercatat = $header_row->diskon;
			if ($header_row->t_diskon == "persen")
				$diskon_tercatat = ($total_tagihan_tercatat / (100 - $diskon_tercatat) * 100) * $diskon_tercatat / 100;
			$nama_pasien = $header_row->nama_pasien;
			$tanggal = $header_row->tanggal;
			$total_debit = 0;
			$total_kredit = 0;

			$oj_rows = $dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_jadi
				WHERE id_penjualan_resep = '" . $id . "' AND prop NOT LIKE 'del'
			");
			if ($oj_rows != null) {
				foreach ($oj_rows as $ojr) {
					$kode_hpp = "";
					$kode_nilai_persediaan = "";

					$data['id'] = $ojr->id_obat;
					$service_consumer = new ServiceConsumer(
						$db,
						"get_kode_acc_barang",
						$data,
						"perencanaan"
					);
					$content = $service_consumer->execute()->getContent();
					if ($content != null) {
						$kode_hpp = $content['kode_hpp'];
						$kode_nilai_persediaan = $content['kode_nilai_persediaan'];
					}
					$hpp = 0;
					$hpp_row = $dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $ojr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $header_row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null)
						$hpp = $hpp_row->hpp;
					$nilai_persediaan = $ojr->jumlah * $hpp;
					$hpp = $ojr->jumlah * $hpp;

					$data_debit_kredit = array(
						"akun"		=> $kode_hpp,
						"debet"		=> $hpp,
						"kredit"	=> 0,
						"ket"		=> "HPP Obat " . $ojr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "debit-farmasi-hpp-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];

					$data_debit_kredit = array(
						"akun"		=> $kode_nilai_persediaan,
						"debet"		=> 0,
						"kredit"	=> $nilai_persediaan,
						"ket"		=> "Nilai Persediaan Obat " . $ojr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "kredit-farmasi-nilai_persediaan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];
				}
			}
			
			$or_rows = $dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_racikan
				WHERE id_penjualan_resep = '" . $id . "' AND prop NOT LIKE 'del'
			");
			if ($or_rows != null) {
				foreach($or_rows as $orr) {
					$b_rows = $dbtable->get_result("
						SELECT *
						FROM smis_frm_bahan_pakai_obat_racikan
						WHERE id_penjualan_obat_racikan = '" . $orr->id . "' AND prop NOT LIKE 'del'
					");
					if ($b_rows != null) {
						foreach ($b_rows as $br) {
							$kode_hpp = "";
							$kode_nilai_persediaan = "";
							
							$data['id'] = $br->id_obat;
							$service_consumer = new ServiceConsumer(
								$db,
								"get_kode_acc_barang",
								$data,
								"perencanaan"
							);
							$content = $service_consumer->execute()->getContent();
							if ($content != null) {
								$kode_hpp = $content['kode_hpp'];
								$kode_nilai_persediaan = $content['kode_nilai_persediaan'];
							}

							$hpp = 0;
							$hpp_row = $dbtable->get_row("
								SELECT *
								FROM smis_frm_histori_hpp
								WHERE id_obat = '" . $br->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $header_row->tanggal . "'
								ORDER BY id DESC
								LIMIT 0, 1
							");
							if ($hpp_row != null)
								$hpp = $hpp_row->hpp;
							$nilai_persediaan = $br->jumlah * $hpp;
							$hpp = $br->jumlah * $hpp;

							$data_debit_kredit = array(
								"akun"		=> $kode_hpp,
								"debet"		=> $hpp,
								"kredit"	=> 0,
								"ket"		=> "HPP Obat " . $br->nama_obat . " (Racikan " . $orr->nama . ") dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
								"code"		=> "debit-farmasi-hpp-" . $id
							);
							$list_debit_kredit[] = $data_debit_kredit;
							$total_debit += $data_debit_kredit['debet'];
							$total_kredit += $data_debit_kredit['kredit'];

							$data_debit_kredit = array(
								"akun"		=> $kode_nilai_persediaan,
								"debet"		=> 0,
								"kredit"	=> $nilai_persediaan,
								"ket"		=> "Nilai Persediaan Obat " . $br->nama_obat . " (Racikan " . $orr->nama . ") dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
								"code"		=> "kredit-farmasi-nilai_persediaan-" . $id
							);
							$list_debit_kredit[] = $data_debit_kredit;
							$total_debit += $data_debit_kredit['debet'];
							$total_kredit += $data_debit_kredit['kredit'];
						}
					}
				}
			}

			$header = array(
				"tanggal"		=> $header_row->tanggal,
				"keterangan"	=> "Persediaan dari No. Penjualan " . $header_row->nomor_resep . " - ID Transaksi " . $id . " No. Reg. : " . $header_row->noreg_pasien,
				"code"			=> "persediaan-penjualan-farmasi-" . $id,
				"nomor"			=> $id,
				"debet"			=> $total_debit,
				"kredit"		=> $total_kredit,
				"io"			=> 1
			);
		}

		$transaction = array(
			"header"	=> $header,
			"content"	=> $list_debit_kredit
		);
		$data_final = array();
		$data_final[] = $transaction;
		echo json_encode($data_final);
	}
?>