<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	if (isset($_POST['data'])) {
		$id = $_POST['data'];
		$dbtable = new DBTable($db, "smis_frm_obat_keluar_unit_lain");
		$header_row = $dbtable->get_row("
			SELECT *
			FROM smis_frm_obat_keluar_unit_lain
			WHERE id = '" . $id . "'
		");

		$header = array();
		$list_debit_kredit = array();

		if ($header_row != null) {
			$total_tagihan_tercatat = $header_row->total;
			$diskon_tercatat = $header_row->diskon;
			if ($header_row->t_diskon == "persen")
				$diskon_tercatat = ($total_tagihan_tercatat / (100 - $diskon_tercatat) * 100) * $diskon_tercatat / 100;
			$nama_pasien = $header_row->nama_pasien;
			$tanggal = $header_row->tanggal;
			$total_debit = 0;
			$total_kredit = 0;

			$dok_rows = $dbtable->get_result("
				SELECT *
				FROM smis_frm_dobat_keluar_unit_lain
				WHERE id_obat_keluar_unit_lain = '" . $id . "' AND prop NOT LIKE 'del'
			");
			if ($dok_rows != null) {
				foreach ($dok_rows as $dok) {
					$kode_hpp = "";
					$kode_nilai_persediaan = "";

					$data['id'] = $dok->id_obat;
					$service_consumer = new ServiceConsumer(
						$db,
						"get_kode_acc_barang",
						$data,
						"perencanaan"
					);
					$content = $service_consumer->execute()->getContent();
					if ($content != null) {
						$kode_hpp = $content['kode_hpp'];
						$kode_nilai_persediaan = $content['kode_nilai_persediaan'];
					}
					$hpp = 0;
					$hpp_row = $dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $dok->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $header_row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null)
						$hpp = $hpp_row->hpp;
					$nilai_persediaan = $dok->jumlah * $hpp;
					$hpp = $dok->jumlah * $hpp;

					$data_debit_kredit = array(
						"akun"		=> $kode_hpp,
						"debet"		=> $hpp,
						"kredit"	=> 0,
						"ket"		=> "HPP Obat " . $dok->nama_obat . " dari Obat Keluar Unit Lain : " . $id,
						"code"		=> "debit-farmasi-hpp-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];

					$data_debit_kredit = array(
						"akun"		=> $kode_nilai_persediaan,
						"debet"		=> 0,
						"kredit"	=> $nilai_persediaan,
						"ket"		=> "Nilai Persediaan Obat " . $dok->nama_obat . " dari Obat Keluar Unit Lain ID Transaksi : " . $id,
						"code"		=> "kredit-farmasi-nilai_persediaan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];
				}
			}

			$header = array(
				"tanggal"		=> $header_row->tanggal,
				"keterangan"	=> "Persediaan dari Obat Keluar Unit Lain ID Transaksi " . $id . " Unit Lain : " . $header_row->nama_unit,
				"code"			=> "persediaan-obat_keluar_ext-farmasi-" . $id,
				"nomor"			=> $id,
				"debet"			=> $total_debit,
				"kredit"		=> $total_kredit,
				"io"			=> 1
			);
		}

		$transaction = array(
			"header"	=> $header,
			"content"	=> $list_debit_kredit
		);
		$data_final = array();
		$data_final[] = $transaction;
		echo json_encode($data_final);
	}
?>