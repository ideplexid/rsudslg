<?php 
	global $db;
	if (isset($_POST['noreg_pasien'])) {
		$noreg = $_POST['noreg_pasien'];
		$penjualan_resep_dbtable = new DBTable($db, "smis_frm_penjualan_resep");
		$resep_rows = $penjualan_resep_dbtable->get_result("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE prop NOT LIKE 'del' AND noreg_pasien = '$noreg' AND tipe = 'resep' AND dibatalkan = '0'
		");
		
		$result = array();
		foreach($resep_rows as $rr) {
			$resep = array(
				'id' => $rr->id,
				'nama' => "Resep No. $rr->nomor_resep",
				'waktu' => ArrayAdapter::format("date d M Y", $rr->tanggal),
				'start' => $rr->tanggal,
				'end' => $rr->tanggal,
				'jumlah' => 1,
				'biaya' => $rr->total,
			);
			
			$ket = array(
				'dokter' => $rr->nama_dokter,
			);
			
			$diskon = -$rr->total;
			
			$obat_jadi_rows = $penjualan_resep_dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_jadi
				WHERE id_penjualan_resep = '$rr->id'");
			
			foreach($obat_jadi_rows as $o) {
				$jaspel = $o->embalase + $o->tusla;
				$total = $o->harga * $o->jumlah + $jaspel;
				$diskon += $total;
				$ket['obat'][] = array(
					'nama_obat' => $o->nama_obat,		
					'jumlah' => $o->jumlah,
					'harga' => $o->harga,
					'jaspel' => $jaspel,
					'total' => $total,
				);
			}
			
			$obat_racikan_rows = $penjualan_resep_dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_racikan
				WHERE id_penjualan_resep = '$rr->id'");
			
			foreach($obat_racikan_rows as $o) {
				$jaspel = $o->embalase + $o->tusla + $o->biaya_racik;
				$total = $o->harga * $o->jumlah + $jaspel;
				$diskon += $total;
				$ket['obat'][] = array(
					'nama_obat' => $o->nama,
					'jumlah' => $o->jumlah,
					'harga' => $o->harga,
					'jaspel' => $jaspel,
					'total' => $total,
				);
			}
			
			$retur_resep_rows = $penjualan_resep_dbtable->get_result("
				SELECT *
				FROM smis_frm_retur_penjualan_resep
				WHERE id_penjualan_resep = '$rr->id' AND dibatalkan = 0");
			
			foreach($retur_resep_rows as $rrr) {
				$detail_retur_rows = $penjualan_resep_dbtable->get_result("
					SELECT smis_frm_dretur_penjualan_resep.*, smis_frm_stok_obat.nama_obat
					FROM smis_frm_dretur_penjualan_resep LEFT JOIN smis_frm_stok_obat ON smis_frm_dretur_penjualan_resep.id_stok_obat = smis_frm_stok_obat.id
					WHERE id_retur_penjualan_resep = '$rrr->id'");
				
				foreach($detail_retur_rows as $drr) {
					$ket['retur'][] = array(
						'nama_obat' => $drr->nama_obat,
						'jumlah' => $drr->jumlah,
						'harga' => $drr->harga,
						'persentase' => $rrr->persentase_retur, 
					);					
					$resep['biaya'] -= $drr->harga * $rrr->persentase_retur / 100 * $drr->jumlah;
					$resep['start'] = $rrr->tanggal;
					$resep['end'] = $rrr->tanggal;			
				}
			}

			if ($diskon > 1)
				$ket['diskon'] = $diskon;
						
			$resep['keterangan'] = $ket;
			$result[] = $resep;
		}
		
		echo json_encode(array(
			'selesai' => "1",
			'exist' => "1",
			'reverse' => "0",
			'cara_keluar' => "Selesai",
			'jasa_pelayanan' => "0",
			'data' => array(
				'penjualan_resep' => array(
					'result'			=> $result,
					'jasa_pelayanan' 	=> "0"
				)
			),
		));
	}
?>
