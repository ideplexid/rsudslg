<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	if (isset($_POST['data'])) {
		$id = $_POST['data'];
		$dbtable = new DBTable($db, "smis_frm_obat_keluar_unit_lain");
		$header_row = $dbtable->get_row("
			SELECT *
			FROM smis_frm_obat_keluar_unit_lain
			WHERE id = '" . $id . "'
		");

		$header = array();
		$list_debit_kredit = array();

		if ($header_row != null) {
			$total_debit = 0;
			$total_kredit = 0;

			$dok_rows = $dbtable->get_result("
				SELECT *
				FROM smis_frm_dobat_keluar_unit_lain
				WHERE id_obat_keluar_unit_lain = '" . $id . "' AND prop NOT LIKE 'del'
			");
			if ($dok_rows != null) {
				foreach ($dok_rows as $dok) {
					$kode_nilai_persediaan = "";
					$kode_penggunaan_bhp = "";

					$data['id'] = $dok->id_obat;
					$service_consumer = new ServiceConsumer(
						$db,
						"get_kode_acc_barang",
						$data,
						"perencanaan"
					);
					$content = $service_consumer->execute()->getContent();
					if ($content != null) {
						$kode_nilai_persediaan = $content['kode_nilai_persediaan'];
						$kode_penggunaan_bhp = $content['kode_penggunaan_bhp'];
					}
					$hpp = 0;
					$hpp_row = $dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $dok->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $header_row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null)
						$hpp = $hpp_row->hpp;
					$nilai_persediaan = $dok->jumlah * $hpp;
					$nilai_penggunaan_bhp = $dok->jumlah * $hpp;

					$data_debit_kredit = array(
						"akun"		=> $kode_nilai_persediaan,
						"debet"		=> 0,
						"kredit"	=> $nilai_persediaan,
						"ket"		=> "Nilai Persediaan Obat " . $dok->nama_obat . " dari Transaksi Obat Keluar Unit Lain : " . $id,
						"code"		=> "kredit-farmasi-nilai_persediaan_penggunaan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];

					$data_debit_kredit = array(
						"akun"		=> $kode_penggunaan_bhp,
						"debet"		=> $nilai_penggunaan_bhp,
						"kredit"	=> 0,
						"ket"		=> "Penggunaan Obat " . $dok->nama_obat . " dari Transaksi Obat Keluar Unit Lain : " . $id,
						"code"		=> "debit-farmasi-penggunaan_bhp-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];
				}
			}

			$header = array(
				"tanggal"		=> $header_row->tanggal,
				"keterangan"	=> "Penggunaan dari Obat Keluar Unit Lain ID Transaksi " . $id . " Unit Lain : " . $header_row->nama_unit,
				"code"			=> "penggunaan-obat_keluar_ext-farmasi-" . $id,
				"nomor"			=> $id,
				"debet"			=> $total_debit,
				"kredit"		=> $total_kredit,
				"io"			=> 1
			);
		}

		$transaction = array(
			"header"	=> $header,
			"content"	=> $list_debit_kredit
		);
		$data_final = array();
		$data_final[] = $transaction;
		echo json_encode($data_final);
	}
?>