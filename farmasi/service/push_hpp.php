<?php
	require_once("farmasi/library/InventoryLibrary.php");
	global $db;

	if (isset($_POST['id_obat']) && isset($_POST['tanggal_transaksi']) && isset($_POST['in_out']) && isset($_POST['keterangan'])) {
		$id_obat = $_POST['id_obat'];
		$tanggal_transaksi = $_POST['tanggal_transaksi'];
		$in_out = $_POST['in_out'];
		$keterangan = $_POST['keterangan'];
		InventoryLibrary::saveHistoryHPP($db, $id_obat, $tanggal_transaksi, $in_out, $keterangan);
	}
?>