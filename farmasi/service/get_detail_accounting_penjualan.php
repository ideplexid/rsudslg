<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	if (isset($_POST['data'])) {
		$id = $_POST['data'];
		$dbtable = new DBTable($db, "smis_frm_penjualan_resep");
		$header_row = $dbtable->get_row("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");

		$header = array();
		$list_debit_kredit = array();

		if ($header_row != null) {
			$total_tagihan_tercatat = $header_row->total;
			$diskon_tercatat = $header_row->diskon;
			if ($header_row->t_diskon == "persen")
				$diskon_tercatat = ($total_tagihan_tercatat / (100 - $diskon_tercatat) * 100) * $diskon_tercatat / 100;
			$nama_pasien = $header_row->nama_pasien;
			$tanggal = $header_row->tanggal;
			$total_debit = 0;
			$total_kredit = 0;

			$oj_rows = $dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_jadi
				WHERE id_penjualan_resep = '" . $id . "' AND prop NOT LIKE 'del'
			");
			if ($oj_rows != null) {
				foreach ($oj_rows as $ojr) {
					$kode_pendapatan = "";
					$kode_diskon_penjualan = "";
					$kode_piutang = "";
					$kode_ppn_keluar = "";

					$data['id'] = $ojr->id_obat;
					$service_consumer = new ServiceConsumer(
						$db,
						"get_kode_acc_barang",
						$data,
						"perencanaan"
					);
					$content = $service_consumer->execute()->getContent();
					if ($content != null) {
						$kode_pendapatan = $content['kode_pendapatan'];
						$kode_diskon_penjualan = $content['kode_diskon_penjualan'];
						$kode_piutang = $content['kode_piutang'];
						$kode_ppn_keluar = $content['kode_ppn_keluar'];
					}
					$diskon_penjualan = $ojr->subtotal / $total_tagihan_tercatat * $diskon_tercatat;
					$nilai_pendapatan = $ojr->subtotal - $diskon_penjualan;
					$ppn_keluar = $nilai_pendapatan * 0.1;
					$nilai_pendapatan = $nilai_pendapatan - $ppn_keluar;
					$nilai_piutang = $ojr->subtotal;


					$data_debit_kredit = array(
						"akun"		=> $kode_pendapatan,
						"debet"		=> 0,
						"kredit"	=> $nilai_pendapatan,
						"ket"		=> "Pendapatan Penjualan Obat " . $ojr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "kredit-farmasi-pendapatan_penjualan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];

					$data_debit_kredit = array(
						"akun"		=> $kode_ppn_keluar,
						"debet"		=> 0,
						"kredit"	=> $ppn_keluar,
						"ket"		=> "PPn Penjualan Obat " . $ojr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "kredit-farmasi-ppn_keluar_penjualan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];

					$data_debit_kredit = array(
						"akun"		=> $kode_diskon_penjualan,
						"debet"		=> $diskon_penjualan,
						"kredit"	=> 0,
						"ket"		=> "Diskon Penjualan Obat " . $ojr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "debit-farmasi-diskon_penjualan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];

					$data_debit_kredit = array(
						"akun"		=> $kode_piutang,
						"debet"		=> $nilai_piutang,
						"kredit"	=> 0,
						"ket"		=> "Piutang Penjualan Obat " . $ojr->nama_obat . " dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
						"code"		=> "debit-farmasi-piutang_penjualan-" . $id
					);
					$list_debit_kredit[] = $data_debit_kredit;
					$total_debit += $data_debit_kredit['debet'];
					$total_kredit += $data_debit_kredit['kredit'];
				}
			}
			
			$or_rows = $dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_racikan
				WHERE id_penjualan_resep = '" . $id . "' AND prop NOT LIKE 'del'
			");
			if ($or_rows != null) {
				foreach($or_rows as $orr) {
					$jasa_racik_all = $orr->biaya_racik;
					$b_rows = $dbtable->get_result("
						SELECT *
						FROM smis_frm_bahan_pakai_obat_racikan
						WHERE id_penjualan_obat_racikan = '" . $orr->id . "' AND prop NOT LIKE 'del'
					");
					if ($b_rows != null) {
						$jumlah_bahan = count($b_rows);
						foreach ($b_rows as $br) {
							$jasa_racik = $jasa_racik_all / $jumlah_bahan;
							$embalase = $br->embalase;
							$tusla = $br->tusla;
							$subtotal = $br->jumlah * $br->harga + $embalase + $tusla + $jasa_racik;

							$kode_pendapatan = "";
							$kode_diskon_penjualan = "";
							$kode_piutang = "";
							$kode_ppn_keluar = "";
							
							$data['id'] = $br->id_obat;
							$service_consumer = new ServiceConsumer(
								$db,
								"get_kode_acc_barang",
								$data,
								"perencanaan"
							);
							$content = $service_consumer->execute()->getContent();
							if ($content != null) {
								$kode_pendapatan = $content['kode_pendapatan'];
								$kode_diskon_penjualan = $content['kode_diskon_penjualan'];
								$kode_piutang = $content['kode_piutang'];
								$kode_ppn_keluar = $content['kode_ppn_keluar'];
							}
							$diskon_penjualan = $subtotal / $total_tagihan_tercatat * $diskon_tercatat;
							$nilai_pendapatan = $subtotal - $diskon_penjualan;
							$ppn_keluar = $nilai_pendapatan * 0.1;
							$nilai_pendapatan = $nilai_pendapatan - $ppn_keluar;
							$nilai_piutang = $subtotal;

							$data_debit_kredit = array(
								"akun"		=> $kode_pendapatan,
								"debet"		=> 0,
								"kredit"	=> $nilai_pendapatan,
								"ket"		=> "Pendapatan Penjualan Obat " . $br->nama_obat . " (Racikan " . $orr->nama . ") dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
								"code"		=> "kredit-farmasi-pendapatan_penjualan-" . $id
							);
							$list_debit_kredit[] = $data_debit_kredit;
							$total_debit += $data_debit_kredit['debet'];
							$total_kredit += $data_debit_kredit['kredit'];

							$data_debit_kredit = array(
								"akun"		=> $kode_ppn_keluar,
								"debet"		=> 0,
								"kredit"	=> $ppn_keluar,
								"ket"		=> "PPn Penjualan Obat " . $br->nama_obat . " (Racikan " . $orr->nama . ") dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
								"code"		=> "kredit-farmasi-ppn_keluar_penjualan-" . $id
							);
							$list_debit_kredit[] = $data_debit_kredit;
							$total_debit += $data_debit_kredit['debet'];
							$total_kredit += $data_debit_kredit['kredit'];

							$data_debit_kredit = array(
								"akun"		=> $kode_diskon_penjualan,
								"debet"		=> 0,
								"kredit"	=> $diskon_penjualan,
								"ket"		=> "Diskon Penjualan Obat " . $br->nama_obat . " (Racikan " . $orr->nama . ") dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
								"code"		=> "kredit-farmasi-diskon_penjualan-" . $id
							);
							$list_debit_kredit[] = $data_debit_kredit;
							$total_debit += $data_debit_kredit['debet'];
							$total_kredit += $data_debit_kredit['kredit'];

							$data_debit_kredit = array(
								"akun"		=> $kode_piutang,
								"debet"		=> $nilai_piutang,
								"kredit"	=> 0,
								"ket"		=> "Piutang Penjualan Obat " . $br->nama_obat . " (Racikan " . $orr->nama . ") dari Penjualan ID Transaksi : " . $id . " No. Reg. : " . $header_row->noreg_pasien,
								"code"		=> "debit-farmasi-piutang_penjualan-" . $id
							);
							$list_debit_kredit[] = $data_debit_kredit;
							$total_debit += $data_debit_kredit['debet'];
							$total_kredit += $data_debit_kredit['kredit'];
						}
					}
				}
			}

			$header = array(
				"tanggal"		=> $header_row->tanggal,
				"keterangan"	=> "No. Penjualan " . $header_row->nomor_resep . " - ID Transaksi " . $id . " No. Reg. : " . $header_row->noreg_pasien,
				"code"			=> "penjualan-farmasi-" . $id,
				"nomor"			=> $id,
				"debet"			=> $total_debit,
				"kredit"		=> $total_kredit,
				"io"			=> 1
			);
		}

		$transaction = array(
			"header"	=> $header,
			"content"	=> $list_debit_kredit
		);
		$data_final = array();
		$data_final[] = $transaction;
		echo json_encode($data_final);
	}
?>