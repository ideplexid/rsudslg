<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("farmasi/service_consumer/UnitServiceConsumer.php");
	global $db;

	$laporan_form = new Form("", "", "Farmasi : Laporan Mutasi Obat");
	$tanggal_from_text = new Text("lmo_tanggal_from", "lmo_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lmo_tanggal_to", "lmo_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$jenis_filter_option = new OptionBuilder();
	$jenis_filter_option->add("SEMUA", "semua", "1");
	$jenis_filter_option->add("PER OBAT", "per_obat");
	$jenis_filter_option->add("PER JENIS", "per_jenis");
	$jenis_filter_option->add("PER UNIT", "per_unit");
	$jenis_filter_select = new Select("lmo_jenis_filter", "lmo_jenis_filter", $jenis_filter_option->getContent());
	$laporan_form->addElement("Jenis Filter", $jenis_filter_select);
	$kode_jenis_obat_hidden = new Hidden("lmo_kode_jenis_obat", "lmo_kode_jenis_obat", "");
	$laporan_form->addElement("", $kode_jenis_obat_hidden);
	$id_obat_hidden = new Hidden("lmo_id_obat", "lmo_id_obat", "");
	$laporan_form->addElement("", $id_obat_hidden);
	$nama_obat_text = new Text("lmo_nama_obat", "lmo_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("lmo_obat.chooser('lmo_obat', 'lmo_obat_button', 'lmo_obat', lmo_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Obat", $input_group);
	$nama_jenis_obat_text = new Text("lmo_nama_jenis_obat", "lmo_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$nama_jenis_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("lmo_jenis_obat.chooser('lmo_jenis_obat', 'lmo_jenis_obat_button', 'lmo_jenis_obat', lmo_jenis_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_jenis_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Jenis Obat", $input_group);
	$unit_service_consumer = new UnitServiceConsumer($db);
	$unit_service_consumer->execute();
	$unit_option = $unit_service_consumer->getContent();
	$unit_select = new Select("lmo_unit", "lmo_unit", $unit_option);
	$laporan_form->addElement("Unit", $unit_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lmo.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='lmo_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$lmo_table = new Table(
		array("No.", "Tanggal", "Unit", "ID Obat", "Kode Obat", "Nama Obat", "Jenis Obat", "Jumlah", "Satuan"),
		"",
		null,
		true
	);
	$lmo_table->setName("lmo");
	$lmo_table->setAction(false);
	$lmo_table->setFooterVisible(false);

	//chooser nama obat:
	$obat_table = new Table(
		array("ID", "Kode", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("lmo_obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("ID", "id");
	$obat_adapter->add("Kode", "kode");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_dbtable = new DBTable($db, "smis_pr_barang");
	$obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	//chooser jenis obat:
	$jenis_obat_table = new Table(
		array("No.", "Kode", "Jenis Obat"),
		"",
		null,
		true
	);
	$jenis_obat_table->setName("lmo_jenis_obat");
	$jenis_obat_table->setModel(Table::$SELECT);
	$jenis_obat_adapter = new SimpleAdapter(true, "No.");
	$jenis_obat_adapter->add("Kode", "kode");
	$jenis_obat_adapter->add("Jenis Obat", "nama");
	$jenis_obat_dbtable = new DBTable($db, "smis_pr_jenis_barang");
	$jenis_obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$jenis_obat_dbresponder = new DBResponder(
		$jenis_obat_dbtable,
		$jenis_obat_table,
		$jenis_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("lmo_obat", $obat_dbresponder);
	$super_command->addResponder("lmo_jenis_obat", $jenis_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$id_obat = $_POST['id_obat'];
			$nama_obat = $_POST['nama_obat'];
			$nama_jenis_obat = $_POST['nama_jenis_obat'];
			$unit = $_POST['unit'];
			$filter = "";
			if ($jenis_filter == "per_obat")
				$filter = " AND b.id_obat = '" . $id_obat . "' ";
			else if ($jenis_filter == "per_jenis")
				$filter = " AND b.nama_jenis_obat = '" . $nama_jenis_obat . "' ";
			else if ($jenis_filter == "per_unit")
				$filter = " AND a.unit = '" . $unit . "' ";
			$dbtable = new DBTable($db, "smis_frm_obat_keluar");
			$row = $dbtable->get_row("
				SELECT COUNT(b.id) AS 'jumlah'
				FROM smis_frm_obat_keluar a LEFT JOIN smis_frm_dobat_keluar b ON a.id = b.id_obat_keluar
				WHERE a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' " . $filter . "
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			$data = array(
				"jumlah"	=> $jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$id_obat = $_POST['id_obat'];
			$nama_obat = $_POST['nama_obat'];
			$nama_jenis_obat = $_POST['nama_jenis_obat'];
			$unit = $_POST['unit'];
			$num = $_POST['num'];
			$filter = "";
			if ($jenis_filter == "per_obat")
				$filter = " AND b.id_obat = '" . $id_obat . "' ";
			else if ($jenis_filter == "per_jenis")
				$filter = " AND b.nama_jenis_obat = '" . $nama_jenis_obat . "' ";
			else if ($jenis_filter == "per_unit")
				$filter = " AND a.unit = '" . $unit . "' ";
			$dbtable = new DBTable($db, "smis_frm_obat_keluar");
			$row = $dbtable->get_row("
				SELECT b.*, a.unit, a.tanggal
				FROM smis_frm_obat_keluar a LEFT JOIN smis_frm_dobat_keluar b ON a.id = b.id_obat_keluar
				WHERE a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' " . $filter . "
				LIMIT " . $num . ", 1
			");
			$tanggal = "N/A";
			$html = "";
			if ($row != null) {
				$tanggal = ArrayAdapter::format("date d-m-Y", $row->tanggal);
				$unit = ArrayAdapter::format("unslug", $row->unit);
				$id_obat = $row->id_obat;
				$nama_obat = $row->nama_obat;
				$nama_jenis_obat = $row->nama_jenis_obat;
				$html = "
					<tr>
						<td id='nomor'></td>
						<td id='tanggal'>" . $tanggal . "</td>
						<td id='unit'>" . $unit . "</td>
						<td id='id_obat'>" . $row->id_obat . "</td>
						<td id='kode_obat'>" . $row->kode_obat . "</td>
						<td id='nama_obat'>" . $row->nama_obat . "</td>
						<td id='nama_jenis_obat'>" . $row->nama_jenis_obat . "</td>
						<td id='jumlah' style='display: none;'>" . $row->jumlah . "</td>
						<td id='f_jumlah'>" . ArrayAdapter::format("number", $row->jumlah) . "</td>
						<td id='satuan'>" . $row->satuan . "</td>
					</tr>
				";
			}

			$data = array();
			$data['tanggal'] = $tanggal;
			$data['unit'] = ArrayAdapter::format("unslug", $unit);
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['nama_obat'] = $nama_obat;
			$data['nama_jenis_obat'] = $nama_jenis_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$id_obat = $_POST['id_obat'];
			$nama_obat = $_POST['nama_obat'];
			$nama_jenis_obat = $_POST['nama_jenis_obat'];
			$unit = $_POST['unit'];
			$filter = ArrayAdapter::format("unslug", $jenis_filter);
			if ($jenis_filter != "semua") {
				$filter = ArrayAdapter::format("unslug", $jenis_filter) . " - ";
				if ($jenis_filter == "per_obat")
					$filter .= $nama_obat . " - " . $id_obat;
				else if ($jenis_filter == "per_jenis")
					$filter .= $nama_jenis_obat;
				else if ($jenis_filter == "per_unit")
					$filter .= ArrayAdapter::format("unslug", $unit);
			}
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("farmasi/templates/template_mutasi_obat.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("MUTASI OBAT");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$objWorksheet->setCellValue("B4", "FILTER DATA : " . $filter);
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(9, $_POST['num_rows'] - 2);
			$start_row_num = 8;
			$end_row_num = 8;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_jenis_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=F_MUTASI_OBAT_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . "_" . ArrayAdapter::format("unslug", $filter) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("lmo_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lmo.cancel()");
	$loading_modal = new Modal("lmo_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lmo_table->getHtml();
	echo "</div>";
	echo "<div id='lmo_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("farmasi/js/laporan_mutasi_obat_action.js", false);
	echo addJS("farmasi/js/laporan_mutasi_obat_obat_action.js", false);
	echo addJS("farmasi/js/laporan_mutasi_obat_jenis_obat_action.js", false);
	echo addJS("farmasi/js/laporan_mutasi_obat.js", false);
?>