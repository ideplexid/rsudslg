<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$table = new Table(
		array("No.", "No. E-Resep", "Waktu", "Unit Layanan", "Dokter", "No. Reg.", "No. RM", "Pasien", "Jenis", "Perusahaan", "Asuransi", "Operator Unit"),
		"Farmasi : E-Resep",
		null,
		true
	);
	$table->setName("e_resep");
	$table->setAddButtonEnable(false);
	$table->setPrintButtonEnable(false);
	$table->setReloadButtonEnable(false);
	$table->setEditButtonEnable(false);
	$table->setDelButtonEnable(false);
	$button = new Button("preview_button", "", "Pratinjau");
	$button->setClass("btn-inverse");
	$button->setIsButton(Button::$ICONIC);
	$button->setIcon("fa fa-file");
	$table->addContentButton("preview", $button);
	$button = new Button("proceed_button", "", "Buka Kunci");
	$button->setClass("btn-danger");
	$button->setIsButton(Button::$ICONIC);
	$button->setIcon("fa fa-unlock-alt");
	$table->addContentButton("unlock", $button);
	$button = new Button("proceed_button", "", "Proses");
	$button->setClass("btn-primary");
	$button->setIsButton(Button::$ICONIC);
	$button->setIcon("fa fa-share");
	$table->addContentButton("proceed", $button);

	if (isset($_POST['super_command']) && $_POST['super_command'] != "") {
		if ($_POST['super_command'] == "e_resep" && isset($_POST['command'])) {
			if ($_POST['command'] == "edit") {
				$id = $_POST['id'];
				$params = array(
					"id"		=> $id,
					"command"	=> $_POST['command']
				);
				$service_consumer = new ServiceConsumer(
					$db,
					"get_eresep_response_commands",
					$params,
					"e_resep"
				);
				$service_consumer->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $service_consumer->execute()->getContent();
				$data = array(
					"header" => $content[0],
					"details" => $content[1]
				);
				echo json_encode($data);
			} else if ($_POST['command'] == "unlock") {
				$id = $_POST['id'];
				$params = array(
					"id"		=> $id,
					"command"	=> $_POST['command']
				);
				$service_consumer = new ServiceConsumer(
					$db,
					"get_eresep_response_commands",
					$params,
					"e_resep"
				);
				$service_consumer->setMode(ServiceConsumer::$CLEAN_BOTH);
				$service_consumer->execute();
			} else if ($_POST['command'] == "preview") {
				$id = $_POST['id'];
				$params = array(
					"id"		=> $id,
					"command"	=> $_POST['command']
				);
				$service_consumer = new ServiceConsumer(
					$db,
					"get_eresep_response_commands",
					$params,
					"e_resep"
				);
				$service_consumer->setMode(ServiceConsumer::$CLEAN_BOTH);
				$content = $service_consumer->execute()->getContent();
				$data = array(
					"html" => $content[0]
				);
				echo json_encode($data);
			}
			return;
		}
		return;
	}

	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("No. E-Resep", "id", "digit6");
		$adapter->add("Waktu", "tanggal", "date d-m-Y, H:i");
		$adapter->add("Unit Layanan", "ruangan", "unslug");
		$adapter->add("Dokter", "nama_dokter");
		$adapter->add("No. Reg.", "noreg_pasien", "digit6");
		$adapter->add("No. RM", "nrm_pasien", "digit6");
		$adapter->add("Pasien", "nama_pasien");
		$adapter->add("Jenis", "jenis", "unslug");
		$adapter->add("Perusahaan", "perusahaan");
		$adapter->add("Asuransi", "asuransi");
		$adapter->add("Operator Unit", "nama_operator");
		$service_responder = new ServiceResponder(
			$db,
			$table,
			$adapter,
			"get_eresep_list"
		);
		$data = $service_responder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	$preview_modal = new Modal("preview_modal", "smis_form_container", "preview");
	$preview_modal->setTitle("Pratinjau E-Resep");

	echo $preview_modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function EResepAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	EResepAction.prototype.constructor = EResepAction;
	EResepAction.prototype = new TableAction();
	EResepAction.prototype.getRegulerData = function() {
		var data = TableAction.prototype.getRegulerData.call(this);
		data['depo'] = "farmasi";
		return data;
	};
	EResepAction.prototype.preview = function(id) {
		var data = this.getRegulerData();
		data['super_command'] = "e_resep";
		data['command'] = "preview";
		data['id'] = id;
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) {
					dismissLoading();
					return;
				}
				$("#preview_modal .modal-body").html(json.html);
				$("#preview_modal").smodal("show");
				dismissLoading();
			}			
		);
	};
	EResepAction.prototype.unlock = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "e_resep";
		data['command'] = "unlock";
		data['id'] = id;
		bootbox.confirm(
			"Yakin membuka kunci E-Resep ini?",
			function(result) {
				if (result) {
					showLoading();
					$.post(
						"",
						data,
						function() {
							self.view();
							dismissLoading();
						}			
					);
				}
			}
		);
	};
	EResepAction.prototype.proceed = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "e_resep";
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				data = self.getRegulerData();
				data['action'] = "e_resep_form";
				data['super_command'] = "";
				data['header'] = json.header;
				data['details'] = json.details;
				data['editable'] = 1;
				LoadSmisPage(data);
			}			
		);
	};

	var e_resep;
	$(document).ready(function() {
		e_resep = new EResepAction(
			"e_resep",
			"farmasi",
			"e_resep",
			new Array()
		);
		e_resep.view();
	});
</script>