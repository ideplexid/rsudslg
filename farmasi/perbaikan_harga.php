<?php
	require_once("farmasi/responder/PerbaikanHargaDBResponder.php");
	global $db;
	
	$table = new Table(
		array("No.", "Kode", "Nama", "Jenis", "HPP", "No. BBM", "Tanggal Masuk"),
		"Farmasi : Koreksi Harga",
		null,
		true
	);
	$table->setName("perbaikan_harga");
	$table->setDelButtonEnable(false);
	$table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Kode", "kode_obat");
		$adapter->add("Nama", "nama_obat");
		$adapter->add("Jenis", "nama_jenis_obat");
		$adapter->add("HPP", "hna", "money Rp. ");
		$adapter->add("No. BBM", "no_bbm");
		$adapter->add("Tanggal Masuk", "tanggal_datang", "date d-m-Y");
		$dbtable = new DBTable($db, "smis_frm_stok_obat");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (a.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR c.no_bbm LIKE '%" . $_POST['kriteria'] . "%' OR c.tanggal_datang LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT a.*, c.no_bbm, c.tanggal_datang
			FROM (smis_frm_stok_obat a LEFT JOIN smis_frm_dobat_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_frm_obat_masuk c ON b.id_obat_masuk = c.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' " . $filter . "
			ORDER BY a.id DESC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new PerbaikanHargaDBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("perbaikan_harga_add_form", "smis_form_container", "perbaikan_harga");
	$id_hidden = new Hidden("perbaikan_harga_id", "perbaikan_harga_id", "");
	$modal->addElement("", $id_hidden);
	$id_dobat_masuk_hidden = new Hidden("perbaikan_harga_id_dobat_masuk", "perbaikan_harga_id_dobat_masuk", "");
	$modal->addElement("", $id_dobat_masuk_hidden);
	$kode_text = new Text("perbaikan_harga_kode", "perbaikan_harga_kode", "");
	$kode_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode", $kode_text);
	$nama_text = new Text("perbaikan_harga_nama", "perbaikan_harga_nama", "");
	$nama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Nama", $nama_text);
	$jenis_text = new Text("perbaikan_harga_jenis", "perbaikan_harga_jenis", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis", $jenis_text);
	$hpp_lama_text = new Text("perbaikan_harga_hpp_lama", "perbaikan_harga_hpp_lama", "");
	$hpp_lama_text->setTypical("money");
	$hpp_lama_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$modal->addElement("HPP Sekarang", $hpp_lama_text);
	$hpp_baru_text = new Text("perbaikan_harga_hpp_baru", "perbaikan_harga_hpp_baru", "");
	$hpp_baru_text->setTypical("money");
	$hpp_baru_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" " );
	$modal->addElement("HPP Baru", $hpp_baru_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("perbaikan_harga.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/perbaikan_harga_action.js", false);
	echo addJS("farmasi/js/perbaikan_harga.js", false);
?>