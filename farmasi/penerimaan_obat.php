<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("farmasi/table/ObatMasukTable.php");
	global $db;

	$table = new ObatMasukTable(
		array("No.", "No. BBM", "No. OPL", "No. Faktur", "Tgl. Faktur", "Rekanan", "Tgl. Diterima", "Jatuh Tempo"),
		"Farmasi : Penerimaan Obat (BBM - OPL)",
		null,
		true
	);
	$table->setName("penerimaan_obat");

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$id = $_POST['id'];
			$objPHPExcel = PHPExcel_IOFactory::load("farmasi/templates/template_bbm.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("BBM");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$dbtable = new DBTable($db, "smis_frm_obat_masuk");
			$header_info = $dbtable->get_row("
				SELECT *
				FROM smis_frm_obat_masuk
				WHERE id = '" . $id . "'
			");
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			$objWorksheet->setCellValue("A2", ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("A3", ArrayAdapter::format("unslug", $alamat_entitas));
			$objWorksheet->setCellValue("F4", ": " . $header_info->nama_vendor);
			$objWorksheet->setCellValue("N2", ": " . $header_info->no_opl);
			$objWorksheet->setCellValue("N3", ": " . $header_info->no_faktur);
			$objWorksheet->setCellValue("N4", ": " . $header_info->no_bbm);
			$objWorksheet->setCellValue("N5", ": " . ArrayAdapter::format("date d-m-Y", $header_info->tanggal));
			$detail_info = $dbtable->get_result("
				SELECT a.*, b.no_batch, b.tanggal_exp
				FROM smis_frm_dobat_masuk a LEFT JOIN smis_frm_stok_obat b ON a.id = b.id_dobat_masuk
				WHERE a.id_obat_masuk = '" . $id . "' AND a.prop NOT LIKE 'del'
			");
			
			$jumlah_item = count($detail_info);
			$jumlah_item_per_halaman = 10;
			$jumlah_halaman = ceil($jumlah_item / $jumlah_item_per_halaman);
			$start_row_index = 1;
			$end_row_index = 29;
			$value_index_incr = 29;

			$cur_item_index = 0;
			$print_area_str = "";
			$total = 0;
			for ($cur_page = 1; $cur_page <= $jumlah_halaman; $cur_page++) {
				$cur_row_index = ($start_row_index + 8) + ($cur_page - 1) * $value_index_incr;
				$start_print_area = $start_row_index + $value_index_incr * ($cur_page - 1);
				$end_print_area = $end_row_index + $value_index_incr * ($cur_page - 1);
				$print_area_str .= "A" . $start_print_area . ":N" . $end_print_area . ",";
				for ($cur_item_num = 1; $cur_item_num <= $jumlah_item_per_halaman && $cur_item_index < $jumlah_item; $cur_item_num++) {
					$objWorksheet->setCellValue("A" . $cur_row_index, $detail_info[$cur_item_index]->kode_obat);
					$objWorksheet->setCellValue("B" . $cur_row_index, $detail_info[$cur_item_index]->nama_obat);
					$objWorksheet->setCellValue("D" . $cur_row_index, $detail_info[$cur_item_index]->satuan);
					$objWorksheet->setCellValue("E" . $cur_row_index, $detail_info[$cur_item_index]->jumlah_tercatat);
					$objWorksheet->getStyle("E" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("F" . $cur_row_index, $detail_info[$cur_item_index]->jumlah);
					$objWorksheet->getStyle("F" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("G" . $cur_row_index, $detail_info[$cur_item_index]->jumlah - $detail_info[$cur_item_index]->jumlah_tercatat);
					$objWorksheet->getStyle("G" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("H" . $cur_row_index, $detail_info[$cur_item_index]->stok_entri + ($detail_info[$cur_item_index]->jumlah * $detail_info[$cur_item_index]->konversi));
					$objWorksheet->getStyle("H" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("I" . $cur_row_index, $detail_info[$cur_item_index]->no_batch);
					$tanggal_exp = "-";
					if ($detail_info[$cur_item_index]->tanggal_exp != "0000-00-00")
						$tanggal_exp = ArrayAdapter::format("date d-m-Y", $detail_info[$cur_item_index]->tanggal_exp);
					$objWorksheet->setCellValue("J" . $cur_row_index, $tanggal_exp);
					$hna_barang = ($detail_info[$cur_item_index]->hna / 1.1) * $detail_info[$cur_item_index]->jumlah;
					if ($header_info->use_ppn == 0)
						$hna_barang = $detail_info[$cur_item_index]->hna * $detail_info[$cur_item_index]->jumlah;
					$diskon = $detail_info[$cur_item_index]->diskon;
					if ($detail_info[$cur_item_index]->t_diskon == "persen")
						$diskon = $hna_barang * $detail_info[$cur_item_index]->diskon / 100;
					if ($header_info->use_ppn == 1)
						$objWorksheet->setCellValue("K" . $cur_row_index, ($detail_info[$cur_item_index]->hna / 1.1));
					else
						$objWorksheet->setCellValue("K" . $cur_row_index, $detail_info[$cur_item_index]->hna);
					$objWorksheet->getStyle("K" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
					$objWorksheet->setCellValue("L" . $cur_row_index, $diskon);
					$objWorksheet->getStyle("L" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
					$objWorksheet->setCellValue("M" . $cur_row_index, "=ROUND(" . $hna_barang . "-" . $diskon . ", 0)");
					$objWorksheet->getStyle("M" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
					$total += ($hna_barang - $diskon);
					$cur_item_index++;
					$cur_row_index++;
				}
			}
			$objWorksheet->setCellValue("M19", $total);
			if ($header_info->use_ppn == 0)
				$objWorksheet->setCellValue("M20", 0);
			else
				$objWorksheet->setCellValue("M20", floor($total * 0.1));
			$objWorksheet->setCellValue("M21", $header_info->materai);
			$objWorksheet->setCellValue("M22", $header_info->ekspedisi);
			// $objWorksheet->getPageSetup()->setPrintArea(rtrim($print_area_str, ","));
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename='BBM-OPL_" . $header_info->no_opl . "-BBM_" . $header_info->no_bbm . ".xlsx'");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("No. BBM", "no_bbm");
		$adapter->add("No. OPL", "no_opl");
		$adapter->add("No. Faktur", "no_faktur");
		$adapter->add("Tgl. Faktur", "tanggal", "date d-m-Y");
		$adapter->add("Rekanan", "nama_vendor");
		$adapter->add("Tgl. Diterima", "tanggal_datang", "date d-m-Y");
		$adapter->add("Jatuh Tempo", "tanggal_tempo", "date d-m-Y");
		$dbtable = new DBTable($db, "smis_frm_obat_masuk");
		$dbtable->addCustomKriteria(" id ", " <> 0 ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/penerimaan_obat_action.js", false);
	echo addJS("farmasi/js/penerimaan_obat.js", false);
?>