<?php
class ObatKeluarAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row->id;
		$array['nomor'] = $row->nomor;
		$array['status'] = $row->status;
		$array['Nomor'] = $row->nomor;
		$array['Tanggal Keluar'] = self::format("date d M Y", $row->tanggal);
		$array['Unit'] = self::format("unslug", $row->unit);
		if ($row->status == "sudah")
			$array['Status'] = "Sudah Diterima";
		else if ($row->status == "dikembalikan")
			$array['Status'] = "Dikembalikan";
		else
			$array['Status'] = "Belum Diterima";
		return $array;
	}
}
?>