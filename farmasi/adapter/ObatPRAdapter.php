<?php
class ObatPRAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row['id'];
		if (substr($row['kode'], 0, 4) == "JKN.")
			$array['Kode'] = "319." . substr($row['kode'], 4);
		else if (substr($row['kode'], 0, 4) == "REG.")
			$array['Kode'] = "309." . substr($row['kode'], 4);
		else
			$array['Kode'] = $row['kode'];
		$array['Obat'] = $row['nama'];
		$array['Jenis'] = $row['nama_jenis_barang'];
		return $array;
	}
}
?>