<?php
	class ReturObatAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['dibatalkan'] = $row->dibatalkan;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
			$array['Jenis Stok'] = self::format("unslug", $row->tipe);
			$array['Vendor'] = $row->nama_vendor;
			$array['No. Faktur'] = $row->no_faktur;
			$array['Tgl. Faktur'] = self::format("date d M Y", $row->tanggal_faktur);
			if ($row->dibatalkan)
				$array['Status'] = "Dibatalkan";
			else
				$array['Status'] = "-";
			return $array;
		}
	}
?>