<?php
	class ReturObatAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['status'] = $row->status;
			$array['restok'] = $row->restok;
			$array['Unit'] = self::format("unslug", $row->unit);
			$array['Nomor'] = self::format("digit8", $row->f_id);
			$array['Tanggal Retur'] = self::format("date d M Y", $row->tanggal);
			$array['Nama Obat'] = $row->nama_obat;
			$array['Jenis Obat'] = $row->nama_jenis_obat;
			$array['Produsen'] = $row->produsen;
			$array['Vendor'] = $row->nama_vendor;
			$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
			if ($row->status == "belum") {
				$array['Status'] = "Belum Diterima";
			} else if ($row->status == "sudah") {
				$array['Status'] = "Sudah Diterima";
				if ($row->restok)
					$array['Status'] = "Restok";
			} else if ($row->status == "dikembalikan") {
				$array['Status'] = "Dikembalikan";
			}
			return $array;
		}
	}
?>