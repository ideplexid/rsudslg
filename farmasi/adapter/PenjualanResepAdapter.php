<?php
class PenjualanResepAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row->id;
		$array['edukasi'] = $row->edukasi;
		$array['dibatalkan'] = $row->dibatalkan;
		$array['tanggal'] = $row->tanggal;
		$array['No. Penjualan'] = self::format("digit8", $row->id);
		$array['No. Resep'] = $row->nomor_resep;
		$array['Tanggal/Jam'] = self::format("date d-m-Y H:i", $row->tanggal);
		$array['Dokter'] = $row->nama_dokter;
		$array['No. Registrasi'] = self::format("digit8", $row->noreg_pasien);
		$array['NRM'] = self::format("digit6", $row->nrm_pasien);
		$array['Pasien'] = $row->nama_pasien;
		$array['Alamat'] = $row->alamat_pasien;
		$array['Kat. Pasien'] = ArrayAdapter::format("trivial_0_Rawat Jalan_Rawat Inap", $row->uri);
		$array['Jns. Pasien'] = ArrayAdapter::format("unslug", $row->jenis);
		$array['Perusahaan'] = $row->perusahaan == "" ? "-" : $row->perusahaan;
		$array['Asuransi'] = $row->asuransi == "" ? "-" : $row->asuransi;
		if ($row->edukasi)
			$array['Edukasi'] = "&#10003;";
		else
			$array['Edukasi'] = "&#10005;";
		return $array;
	}
}
?>