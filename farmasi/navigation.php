<?php 
	global $NAVIGATOR;
	$menu = new Menu("fa fa-medkit");
	$menu->addProperty('title', "Farmasi");
	$menu->addProperty('name', "Farmasi");
	$menu->addSubMenu("Penerimaan Obat (BBM)", "farmasi", "penerimaan_obat", "Data Penerimaan Obat", "fa fa-plus");
	$menu->addSubMenu("Retur Obat - Unit", "farmasi", "retur_obat_unit", "Data Retur Obat Unit","fa fa-plus");
	$menu->addSubMenu("Retur Penjualan Resep", "farmasi", "retur_penjualan_resep", "Data Retur Penjualan Resep", "fa fa-plus");
	$menu->addSubMenu("Retur Penjualan Bebas", "farmasi", "retur_penjualan_bebas", "Data Retur Penjualan Bebas", "fa fa-plus");
	$menu->addSeparator();
	$menu->addSubMenu("E-Resep", "farmasi", "e_resep", "Data E-Resep", "fa fa-inbox");
	$menu->addSeparator();
	$menu->addSubMenu("Penjualan Resep (KIUP)", "farmasi", "penjualan_resep", "Data Penjualan Obat - KIUP", "fa fa-minus");
	$menu->addSubMenu("Penjualan Resep (Non-KIUP)", "farmasi", "penjualan_resep_luar", "Data Penjualan Obat - Non-KIUP", "fa fa-minus");
	$menu->addSubMenu("Penjualan Bebas", "farmasi", "penjualan_bebas", "Data Penjualan Bebas", "fa fa-minus");
	$menu->addSubMenu("Penjualan UP", "farmasi", "penjualan_instansi_lain", "Data Penjualan UP", "fa fa-minus");
	$menu->addSubMenu("Mutasi Obat - Unit (int.)", "farmasi", "obat_keluar", "Data Obat Keluar Unit","fa fa-minus");
	$menu->addSubMenu("Mutasi Obat - Unit (ext.)", "farmasi", "obat_keluar_unit_lain_main", "Data Obat Keluar Unit Lain","fa fa-minus");
	$menu->addSubMenu("Retur Obat - Vendor", "farmasi", "retur_keluar", "Data Retur Pembelian Obat","fa fa-minus");
	$menu->addSeparator();
	$menu->addSubMenu("Kartu Stok Obat", "farmasi", "kartu_stok", "Data Kartu Stok Obat","fa fa-calendar");
	$menu->addSubMenu("Riwayat Stok Obat", "farmasi", "riwayat_stok_obat", "Data Riwayat Stok Obat","fa fa-calendar");
	$menu->addSubMenu("Riwayat HPP", "farmasi", "riwayat_hpp", "Data Riwayat HPP Obat","fa fa-calendar");
	$menu->addSeparator();
	$menu->addSubMenu("Penyesuaian Stok Obat", "farmasi", "penyesuaian_stok_obat", "Penyesuaian Stok Obat","fa fa-check");
	$menu->addSubMenu("Perbaikan Harga", "farmasi", "perbaikan_harga", "Perbaikan Harga" ,"fa fa-check");
	$menu->addSubMenu("Perbaikan Info. Obat", "farmasi", "perbaikan_detail_info", "Perbaikan Info. Obat" ,"fa fa-check");
	$menu->addSeparator();
	$menu->addSubMenu("Laporan", "farmasi", "laporan", "Laporan" ,"fa fa-file");
	$menu->addSeparator();
	$menu->addSubMenu("Margin Jual Per Obat", "farmasi", "pengaturan_margin", "Margin Jual Per Obat" ,"fa fa-gear");
	$menu->addSubMenu("Margin Jual Per Jenis Pasien", "farmasi", "pengaturan_margin_jenis_pasien", "Margin Jual Per Jenis Pasien" ,"fa fa-gear");
	$menu->addSubMenu("Margin Jual Per Obat - Jenis Pasien", "farmasi", "pengaturan_margin_obat_jenis_pasien", "Margin Jual Per Obat Per Jenis Pasien" ,"fa fa-gear");
	$menu->addSubMenu("Pengaturan Umum", "farmasi", "settings", "Pengaturan Umum" ,"fa fa-gear");
	$NAVIGATOR->addMenu($menu, "farmasi");
?>