<?php
class ObatDBResponder extends DBResponder {
	public function command($command) {
		if ($command != "rounded_value")
			return parent::command($command);
		if ($command == "rounded_value") {
			$pack = new ResponsePackage();
			$content = $_POST['value'];
			if (getSettings($this->dbtable->get_db(), "farmasi-rounded-transaksi", 0) == 1) {
				loadLibrary("smis-libs-function-math");
			    $round = getSettings($this->dbtable->get_db(),"smis-simple-rounding-uang","-1");
			    $model = getSettings($this->dbtable->get_db(),"smis-simple-rounding-model","round");
			    $content  = smis_money_round($content, $round, $model);
			}
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}
	}
	public function edit() {
		$id = $_POST['id'];
		$data['header'] = $this->dbtable->get_row("
			SELECT DISTINCT v_dobat_masuk.id_obat, smis_frm_stok_obat.kode_obat, smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.nama_jenis_obat
			FROM smis_frm_stok_obat LEFT JOIN (
				SELECT label, id, id_obat, nama_obat
				FROM smis_frm_dobat_masuk
			) v_dobat_masuk ON smis_frm_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_frm_stok_obat.label = v_dobat_masuk.label
			WHERE smis_frm_stok_obat.prop NOT LIKE 'del' AND v_dobat_masuk.id_obat = '" . $id . "'
		");
		$satuan_rows = $this->dbtable->get_result("
			SELECT DISTINCT smis_frm_stok_obat.satuan, smis_frm_stok_obat.konversi, smis_frm_stok_obat.satuan_konversi	
			FROM smis_frm_stok_obat LEFT JOIN (
				SELECT label, id, id_obat, nama_obat, nama_jenis_obat
				FROM smis_frm_dobat_masuk
			) v_dobat_masuk ON smis_frm_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_frm_stok_obat.label = v_dobat_masuk.label
			WHERE smis_frm_stok_obat.prop NOT LIKE 'del' AND v_dobat_masuk.id_obat = '" . $id . "'
		");
		$satuan_option = "";
		foreach($satuan_rows as $sr) {
			if ($sr->konversi == 1) {
				$satuan_option .= "
					<option value='" . $sr->konversi . "_" . $sr->satuan_konversi . "'>" . $sr->satuan . "</option>
				";
			}
		}
		$data['satuan_option'] = $satuan_option;
		return $data;
	}
}
?>