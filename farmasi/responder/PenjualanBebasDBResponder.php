<?php
require_once("smis-base/smis-include-service-consumer.php");
require_once("farmasi/library/InventoryLibrary.php");
require_once ("farmasi/responder/PenjualanResepDBResponder.php");

class PenjualanBebasDBResponder extends PenjualanResepDBResponder {
	public function print_prescription_escp() {
		$id = $_POST['id'];
		$data = $this->dbtable->get_row("
			SELECT *
		    FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");
		$obat_jadi_data = $this->dbtable->get_result("
			SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
		");
		
		$no_barang = 1;
		$total = 0;
		foreach($obat_jadi_data as $ojd) {
			$items[] = array(
				"no" => $no_barang++,
				"name" => $ojd->nama,
				"qty" => $ojd->jumlah . " " . $ojd->satuan,
				"prc" => $this->formatMoneyForPrint($ojd->harga),
				"jaspel" => $this->formatMoneyForPrint($ojd->embalase + $ojd->tusla),
				"sbt" => $this->formatMoneyForPrint($ojd->subtotal)
			);
			$total += $ojd->subtotal;
		}
		
		$diskonLabel = 'DISKON';
		$diskonValue = $data->diskon;		
		if ($data->diskon > 0 && ($data->t_diskon == "persen" || $data->t_diskon == "gratis")) {
			$diskonLabel = "DISKON (" . $data->diskon . " %)";
			$diskonValue =  $total * $data->diskon / 100;
		}
		
		global $user;
		loadLibrary("smis-libs-function-math");
						
		$template = file_get_contents("farmasi/webprint/penjualan_obat.json");
			
		return array(
			'template' => json_decode($template, true),
			'data' => array(
				'kode_resep' => "RJ",
				'no_penjualan' => ArrayAdapter::format('only-digit8', $data->id),
				'no_resep' => $data->nomor_resep,
				'tgl_resep' => ArrayAdapter::format('date d-m-Y H:i', $data->tanggal),
				'nama_dokter' => "-",
				'nama_pasien' => "-",
				'no_registrasi' => "-",
				'nrm_pasien' => "-",
				'alamat_pasien' => "-",
				'jenis_pasien' => "-",
				'total' => $this->formatMoneyForPrint($total),
				'diskon_label' => $diskonLabel,
				'diskon' => $this->formatMoneyForPrint($diskonValue),
				'tagihan' => $this->formatMoneyForPrint($data->total),
				'terbilang' => numbertell($data->total),
				'ppn_info' => $data->uri ? '' : '* Sudah termasuk PPn (10 %)',
				'petugas' => $user->getNameOnly(),
				'items' => $items
			)
		);
	}
	public function print_claim_prescription_escp() {
		$id = $_POST['id'];
		$header_data = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");
		$nomor_resep = $header_data->nomor_resep;
		$resep_result = $this->dbtable->get_result("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE nomor_resep = '" . $nomor_resep . "' AND tipe = 'bebas' AND prop NOT LIKE 'del' AND dibatalkan = 0
			ORDER BY id ASC
		");
		
		$filter_id = "id_penjualan_resep = '" . $id . "'";
		if ($resep_result != null) {
			$filter_id = "(";
			$cur = 1;
			$count = count($resep_result);
			foreach($resep_result as $rr) {
				$filter_id .= "id_penjualan_resep = '" . $rr->id . "'";
				if ($cur < $count) {
					$filter_id .= " OR ";
					$cur++;
				}
			}
			$filter_id .= ")";
		}
		
		$obat_jadi_data = $this->dbtable->get_result("
			SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND " . $filter_id . "
		");
		
		$no_barang = 1;
		$total = 0;
		foreach($obat_jadi_data as $ojd) {
			$items[] = array(
				"no" => $no_barang++,
				"name" => $ojd->nama,
				"qty" => $ojd->jumlah . " " . $ojd->satuan,
				"prc" => $this->formatMoneyForPrint($ojd->harga),
				"jaspel" => $this->formatMoneyForPrint($ojd->embalase + $ojd->tusla),
				"sbt" => $this->formatMoneyForPrint($ojd->subtotal)
			);
			$total += $ojd->subtotal;
		}
		
		$diskonLabel = 'DISKON';
		$diskonValue = $resep_result[0]->diskon;		
		if ($resep_result[0]->diskon > 0 && ($resep_result[0]->t_diskon == "persen" || $resep_result[0]->t_diskon == "gratis")) {
			$diskonLabel = "DISKON (" . $resep_result[0]->diskon . " %)";
			$diskonValue =  $total * $resep_result[0]->diskon / 100;
		}
		
		global $user;
		loadLibrary("smis-libs-function-math");
						
		$template = file_get_contents("farmasi/webprint/penjualan_obat.json");
			
		return array(
			'template' => json_decode($template, true),
			'data' => array(
				'kode_resep' => "RJ",
				'no_penjualan' => ArrayAdapter::format('only-digit8', $resep_result[0]->id),
				'no_resep' => $resep_result[0]->nomor_resep,
				'tgl_resep' => ArrayAdapter::format('date d-m-Y H:i', $resep_result[0]->tanggal),
				'nama_dokter' => "-",
				'nama_pasien' => "-",
				'no_registrasi' => "-",
				'nrm_pasien' => "-",
				'alamat_pasien' => "-",
				'jenis_pasien' => "-",
				'total' => $this->formatMoneyForPrint($total),
				'diskon_label' => $diskonLabel,
				'diskon' => $this->formatMoneyForPrint($diskonValue),
				'tagihan' => $this->formatMoneyForPrint($total),
				'terbilang' => numbertell($resep_result[0]->total),
				'ppn_info' => $resep_result[0]->uri ? '' : '* Sudah termasuk PPn (10 %)',
				'petugas' => $user->getNameOnly(),
				'items' => $items
			)
		);
	}
	public function print_prescription() {
		$id = $_POST['id'];
		$data = $this->dbtable->get_row("
			SELECT *
		    FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");
		$obat_jadi_sql = "
			SELECT nama_obat AS 'nama', jumlah, satuan, harga, embalase, tusla, subtotal, 'obat_jadi' AS 'label'
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
		";
		$obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
		$obat_racik_sql = "
			SELECT id, nama AS 'nama', jumlah, 'Racikan' AS 'satuan', harga, embalase, tusla, biaya_racik, subtotal, 'obat_racikan' AS 'label'
			FROM smis_frm_penjualan_obat_racikan
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
		";
		$obat_racik_data = $this->dbtable->get_result($obat_racik_sql);
		$content = "<table border='0' width='100%' cellpadding='3' class='etiket_header'>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='0' class='etiket_content' >";
						$content .= "<tr>";
							$content .= "<td colspan='5' align='center' style='font-size: 14px !important;'><b>DEPO FARMASI " . ucwords(getSettings($this->dbtable->get_db(), "smis_autonomous_title", "")) . " - PENJUALAN UMUM BEBAS</b></td>";
						$content .= "</tr>";
					$content .= "</div></table>";
					$content .= "<div align='center'><table border='0' class='etiket_content' >";
						$content .= "<tr>";
							$content .= "<td colspan='1'>No. Penjualan / No. Resep</td>";
							$content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $data->id) . " / " . $data->nomor_resep . "</td>";
							$content .= "<td colspan='1'>Tgl. Penjualan</td>"; 
							$content .= "<td colspan='2'>: " . ArrayAdapter::format("date d-m-Y H:i", $data->tanggal) . "</td>"; 
						$content .= "</tr>";
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "</tr>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='1' class='etiket_content'>";
					$content .= "<tr align='center'>";
						$content .= "<td colspan='1'>No.</td>";
						$content .= "<td colspan='1'>Obat</td>";
						$content .= "<td colspan='1'>Jumlah</td>";
						$content .= "<td colspan='1'>Harga Satuan</td>";
						$content .= "<td colspan='1'>Jasa Pelayanan</td>";
						$content .= "<td colspan='1'>Subtotal</td>";
					$content .= "</tr>";
					$no_barang = 1;
					$total = 0;
					foreach($obat_jadi_data as $ojd) {
						$content .= "<tr>";
							$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
							$content .= "<td colspan='1'>" . $ojd->nama . "</td>";
							$content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->embalase + $ojd->tusla) . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
						$content .= "</tr>";
						$total += $ojd->subtotal;
					}
					$content .= "<tr>";
						$content .= "<td colspan='5' align='right'>Total</td>";
						$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					if ($data->diskon > 0) {
						if ($data->t_diskon == "persen" || $data->t_diskon == "gratis") {
							$v_diskon = ($total * $data->diskon) / 100;
							$content .= "<td colspan='5' align='right'>Diskon (" . $data->diskon . " %)</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
						} else {
							$content .= "<td colspan='5' align='right'>Diskon</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $data->diskon) . "</td>";
						}
					}
					$content .= "</tr>";
					$content .= "<tr>";
						$content .= "<td colspan='5' align='right'>Tagihan</td>";
						$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $data->total) . "</td>";
					$content .= "</tr>";
					loadLibrary("smis-libs-function-math");
					$content .= "<tr>";
						$content .= "<td colspan='1' align='right'>Terbilang</td>";
						$content .= "<td colspan='11'>" . numbertell($data->total) . " Rupiah </td>";
					$content .= "</tr>";
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "</tr>";
			$content .= "<tr>";
			$content .= "</tr>";
				$content .= "<td colspan='6'><i>* Sudah termasuk PPn (10 %)</i></td>";
			$content .= "<tr>";
				$content .= "<td colspan='6'>";
					$content .= "<table border='0' align='center' class='etiket_content'>";
						$content .= "<tr>";
							$content .= "<td align='center'>PENERIMA,</td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td align='center'>(_____________________)</td>";
							$content .= "<td>&Tab;</td>";
							global $user;
							$content .= "<td align='center'>" . $user->getNameOnly() . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
						$content .= "</tr>";
					$content .= "</table>";
				$content .= "</td>";
			$content .= "</tr>";
		$content .= "</table>";
		return $content;
	}
	public function print_claim_prescription() {
		$id = $_POST['id'];
		$header_data = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");
		$nomor_resep = $header_data->nomor_resep;
		$resep_result = $this->dbtable->get_result("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE nomor_resep = '" . $nomor_resep . "' AND tipe = 'bebas' AND prop NOT LIKE 'del' AND dibatalkan = 0
			ORDER BY id ASC
		");
		$content = "<table border='0' width='100%' cellpadding='3' class='etiket_header'>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='0' class='etiket_content' >";
						$content .= "<tr>";
							$content .= "<td colspan='5' align='center' style='font-size: 14px !important;'><b>DEPO FARMASI " . ucwords(getSettings($this->dbtable->get_db(), "smis_autonomous_title", "")) . " - PENJUALAN UMUM BEBAS</b></td>";
						$content .= "</tr>";
					$content .= "</div></table>";
					$content .= "<div align='center'><table border='0' class='etiket_content' >";
						$content .= "<tr>";
							$content .= "<td colspan='1'>No. Penjualan / No. Resep</td>";
							$content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $resep_result[0]->id) . "/" . $resep_result[0]->nomor_resep . "</td>";
							$content .= "<td colspan='1'>Tgl. Penjualan</td>"; 
							$content .= "<td colspan='2'>: " . ArrayAdapter::format("date d-m-Y H:i", $resep_result[0]->tanggal) . "</td>"; 
						$content .= "</tr>";
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "</tr>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='1' class='etiket_content'>";
					$content .= "<tr align='center'>";
						$content .= "<td colspan='1'>No.</td>";
						$content .= "<td colspan='1'>Obat</td>";
						$content .= "<td colspan='1'>Jumlah</td>";
						$content .= "<td colspan='1'>Harga Satuan</td>";
						$content .= "<td colspan='1'>Jasa Pelyanan</td>";
						$content .= "<td colspan='1'>Subtotal</td>";
					$content .= "</tr>";
					$no_barang = 1;
					$total = 0;
					foreach($resep_result as $row_resep_result) {
						$obat_jadi_sql = "
							SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
							FROM smis_frm_penjualan_obat_jadi
							WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $row_resep_result->id . "'
						";
						$obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
						foreach($obat_jadi_data as $ojd) {
							$content .= "<tr>";
								$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
								$content .= "<td colspan='1'>" . $ojd->nama . "</td>";
								$content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->embalase + $ojd->tusla) . "</td>";
								$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
							$content .= "</tr>";
							$total += $ojd->subtotal;
						}
					}
					$content .= "<tr>";
						$content .= "<td colspan='5' align='right'>Total</td>";
						$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$v_diskon = 0;
					if ($resep_result[0]->diskon > 0) {
						if ($resep_result[0]->t_diskon == "persen" || $resep_result[0]->t_diskon == "gratis") {
							$v_diskon = ($total * $resep_result[0]->diskon) / 100;
							$content .= "<td colspan='5' align='right'>Diskon (" . $resep_result[0]->diskon . " %)</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
						} else {
							$v_diskon = $resep_result[0]->diskon;
							$content .= "<td colspan='5' align='right'>Diskon</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
						}
					}
					$total_min_diskon = $total - $v_diskon;
					$content .= "</tr>";
					$content .= "<tr>";
						$content .= "<td colspan='5' align='right'>Tagihan</td>";
						$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
					$content .= "</tr>";
					loadLibrary("smis-libs-function-math");
					$content .= "<tr>";
						$content .= "<td colspan='1' align='right'>Terbilang</td>";
						$content .= "<td colspan='11'>" . numbertell($total) . " Rupiah </td>";
					$content .= "</tr>";
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<table border='0' align='center' class='etiket_content'>";
						$content .= "<tr>";
							$content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td>&Tab;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td>&Tab;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							global $user;
							$content .= "<td align='center'>" . $user->getNameOnly() . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
						$content .= "</tr>";
					$content .= "</table>";
				$content .= "</td>";
			$content .= "</tr>";
		$content .= "</table>";
		return $content;
	}
	public function batalDetailResep($id){
		$header = $this->dbtable->select($id);
		$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
		$stok_pakai_obat_jadi_rows = $this->dbtable->get_result("
			SELECT smis_frm_stok_pakai_obat_jadi.*
			FROM smis_frm_stok_pakai_obat_jadi LEFT JOIN smis_frm_penjualan_obat_jadi ON smis_frm_stok_pakai_obat_jadi.id_penjualan_obat_jadi = smis_frm_penjualan_obat_jadi.id
			WHERE smis_frm_stok_pakai_obat_jadi.prop NOT LIKE 'del' AND smis_frm_penjualan_obat_jadi.id_penjualan_resep = '" . $id['id'] . "'
		");
		$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Bebas; No. Resep : " . $header->nomor_resep;
		foreach($stok_pakai_obat_jadi_rows as $spojr) {
			$stok_obat_row = $stok_obat_dbtable->get_row("
									SELECT *
									FROM smis_frm_stok_obat
									WHERE id = '" . $spojr->id_stok_obat . "'
								");
			$stok_obat_data = array();
			$stok_obat_data['sisa'] = $stok_obat_row->sisa + $spojr->jumlah;
			$stok_obat_id['id'] = $stok_obat_row->id;
			$result=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
			if($result<1){
				$this->addMessage(" Obat <strong>".$stok_obat_row->nama_obat." </strong> Gagal Dibatalkan");
				return false;
			}
			
			//logging riwayat stok:
			$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
			$data_riwayat = array();
			$data_riwayat['tanggal'] = date("Y-m-d");
			$data_riwayat['id_stok_obat'] = $stok_obat_row->id;
			$data_riwayat['jumlah_masuk'] = $spojr->jumlah;
			$data_riwayat['sisa'] = $stok_obat_row->sisa + $spojr->jumlah;
			$data_riwayat['keterangan'] = "Stok Batal Digunakan Penjualan Bebas: " . $keterangan;
			global $user;
			$data_riwayat['nama_user'] = $user->getName();
			$result=$riwayat_dbtable->insert($data_riwayat);
			if($result===false){
				$this->addMessage(" Obat <strong>".$stok_obat_row->nama_obat." </strong> Gagal Gagal Dicatat di Riwayat");
				return false;
			}
		}

		$obat_jadi_rows = $this->dbtable->get_result("
			SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id['id'] . "'
			GROUP BY id_obat, satuan
		");
		//logging kartu stok dan logging hpp:
		$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
		foreach ($obat_jadi_rows as $ojr) {
			$sisa_row = $this->dbtable->get_row("
				SELECT SUM(a.sisa) AS 'sisa'
				FROM smis_frm_stok_obat a
				WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $ojr->id_obat . "' AND a.satuan = '" . $ojr->satuan . "' AND a.konversi = '1'
			");
			$kartu_stok_data = array(
				"f_id"				=> $id['id'],
				"no_bon"			=> $id['id'],
				"unit"				=> "Penjualan Bebas : Pembatalan, " . $keterangan,
				"id_obat"			=> $ojr->id_obat,
				"kode_obat"			=> $ojr->kode_obat,
				"nama_obat"			=> $ojr->nama_obat,
				"nama_jenis_obat"	=> $ojr->nama_jenis_obat,
				"tanggal"			=> date("Y-m-d"),
				"masuk"				=> $ojr->jumlah,
				"keluar"			=> 0,
				"sisa"				=> $sisa_row->sisa
			);
			$result = $kartu_stok_dbtable->insert($kartu_stok_data);
			if($result===false){
				$this->addMessage(" Obat <strong>".$ojr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
				return false;
			}
			InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $ojr->id_obat, date("Y-m-d H:i:s"), 1, "Pembatalan Penjualan Bebas ID : , " . $id['id']);
		}

		return true;
	}
	public function saveDetailResep($id){
		$success=true;
		$header = $this->dbtable->select($id);
		//do insert detail resep here:
		$penjualan_obat_jadi_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_penjualan_obat_jadi");
		$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
		$stok_pakai_obat_jadi_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_pakai_obat_jadi");
		$detail_penjualan_bebas = $_POST['detail_penjualan_bebas'];
		foreach($detail_penjualan_bebas as $dpenjualan_bebas) {
			//do insert obat jadi here:
			$obat_jadi_data = array();
			$obat_jadi_data['id_penjualan_resep'] = $id['id'];
			$obat_jadi_data['id_obat'] = $dpenjualan_bebas['id_obat'];
			$obat_jadi_data['kode_obat'] = $dpenjualan_bebas['kode_obat'];
			$obat_jadi_data['nama_obat'] = $dpenjualan_bebas['nama_obat'];
			$obat_jadi_data['nama_jenis_obat'] = $dpenjualan_bebas['nama_jenis_obat'];
			$obat_jadi_data['jumlah'] = $dpenjualan_bebas['jumlah'];
			$obat_jadi_data['satuan'] = $dpenjualan_bebas['satuan'];
			$obat_jadi_data['konversi'] = $dpenjualan_bebas['konversi'];
			$obat_jadi_data['satuan_konversi'] = $dpenjualan_bebas['satuan_konversi'];
			$obat_jadi_data['harga'] = $dpenjualan_bebas['harga'];
			$obat_jadi_data['embalase'] = $dpenjualan_bebas['embalase'];
			$obat_jadi_data['tusla'] = $dpenjualan_bebas['tusla'];
			$obat_jadi_data['subtotal'] = $dpenjualan_bebas['subtotal'];
			$obat_jadi_data['pos'] = $dpenjualan_bebas['pos'];
			$obat_jadi_data['markup'] = $dpenjualan_bebas['markup'];
			$result=$penjualan_obat_jadi_dbtable->insert($obat_jadi_data);
			
			if($result===false){
				$success=false;
				$this->addMessage(" Obat <strong>".$dpenjualan_bebas['nama_obat']." </strong> Gagal Masuk Penjualan Bebas");
				continue;
			}
			
			$id_penjualan_obat_jadi = $penjualan_obat_jadi_dbtable->get_inserted_id();
			//do insert stok pakai obat jadi and update stok obat here:
			$stok_obat_query = "
				SELECT smis_frm_stok_obat.id, smis_frm_stok_obat.sisa
				FROM smis_frm_stok_obat
				WHERE smis_frm_stok_obat.id_obat = '" . $dpenjualan_bebas['id_obat'] . "' 
				  AND smis_frm_stok_obat.satuan = '" . $dpenjualan_bebas['satuan'] . "' 
				  AND smis_frm_stok_obat.konversi = '" . $dpenjualan_bebas['konversi'] . "' 
				  AND smis_frm_stok_obat.satuan_konversi = '" . $dpenjualan_bebas['satuan_konversi'] . "'
				  AND smis_frm_stok_obat.sisa > 0
				ORDER BY smis_frm_stok_obat.tanggal_exp ASC
			";
			$stok_obat_rows = $stok_obat_dbtable->get_result($stok_obat_query);
			$jumlah = $dpenjualan_bebas['jumlah'] - $dpenjualan_bebas['jumlah_lama'];//kalau-kalau update
			foreach($stok_obat_rows as $sor) {
				$sisa_stok = 0;
				$jumlah_stok_pakai = 0;
				if ($sor->sisa >= $jumlah) {
					$sisa_stok = $sor->sisa - $jumlah;
					$jumlah_stok_pakai = $jumlah;
					$jumlah = 0;
				} else {
					$sisa_stok = 0;
					$jumlah_stok_pakai = $sor->sisa;
					$jumlah = $jumlah - $sor->sisa;
				}
				//do update stok obat here:
				$stok_obat_data = array();
				$stok_obat_data['sisa'] = $sisa_stok;
				$stok_obat_id['id'] = $sor->id;
				$keberhasilan_mengambil_stok=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
				if($keberhasilan_mengambil_stok==0){
					$success=false;
					$this->addMessage(" Stok <strong>".$dpenjualan_bebas['nama_obat']." </strong> Gagal diambil ");
					continue;
				}
				
				//logging riwayat stok:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $sor->id;
				$data_riwayat['jumlah_keluar'] = $jumlah_stok_pakai;
				$data_riwayat['sisa'] = $sisa_stok;
				$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Bebas; No. Resep : " . $header->nomor_resep;
				$data_riwayat['keterangan'] = "Stok Digunakan Penjualan Bebas: " . $keterangan;
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$result=$riwayat_dbtable->insert($data_riwayat);
				if($result===false){
					$success=false;
					$this->addMessage(" Riwayat <strong>".$dpenjualan_bebas['nama_obat']." </strong> Gagal dicatat");
					continue;
				}
				
				//do insert stok pakai obat jadi here:
				$stok_pakai_obat_jadi_data = array();
				$stok_pakai_obat_jadi_data['id_penjualan_obat_jadi'] = $id_penjualan_obat_jadi;
				$stok_pakai_obat_jadi_data['id_stok_obat'] = $sor->id;
				$stok_pakai_obat_jadi_data['jumlah'] = $jumlah_stok_pakai;
				$result=$stok_pakai_obat_jadi_dbtable->insert($stok_pakai_obat_jadi_data);
				if($result===false){
					$success=false;
					$this->addMessage(" Obat <strong>".$dpenjualan_bebas['nama_obat']." </strong> Gagal Disimpan di Riwayat Pemakaian Obat Jadi","Pakai Obat Jadi");
					continue;
				}
				if ($jumlah == 0) break;
			}
			if($jumlah>0) {
				$success=false;
				$this->addMessage(" Stok untuk Obat Jadi pada <strong>".$dpenjualan_bebas['nama_obat']." </strong> Kurang ".$jumlah." ".$dpenjualan_bebas['satuan']);
			}
		}

		$obat_jadi_rows = $this->dbtable->get_result("
			SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id['id'] . "'
			GROUP BY id_obat, satuan
		");
		//logging kartu stok dan logging hpp:
		$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
		$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Bebas; No. Resep : " . $header->nomor_resep;
		foreach ($obat_jadi_rows as $ojr) {
			$sisa_row = $this->dbtable->get_row("
				SELECT SUM(a.sisa) AS 'sisa'
				FROM smis_frm_stok_obat a
				WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $ojr->id_obat . "' AND a.satuan = '" . $ojr->satuan . "' AND a.konversi = '1'
			");
			$kartu_stok_data = array(
				"f_id"				=> $id['id'],
				"no_bon"			=> $id['id'],
				"unit"				=> "Penjualan Bebas : " . $keterangan,
				"id_obat"			=> $ojr->id_obat,
				"kode_obat"			=> $ojr->kode_obat,
				"nama_obat"			=> $ojr->nama_obat,
				"nama_jenis_obat"	=> $ojr->nama_jenis_obat,
				"tanggal"			=> date("Y-m-d"),
				"masuk"				=> 0,
				"keluar"			=> $ojr->jumlah,
				"sisa"				=> $sisa_row->sisa
			);
			$result = $kartu_stok_dbtable->insert($kartu_stok_data);
			if($result===false){
				$this->addMessage(" Obat <strong>".$ojr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
				return false;
			}
			InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $ojr->id_obat, date("Y-m-d H:i:s"), 0, "Penjualan Bebas ID : " . $id['id']);
		}

		return $success;
	}
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		$berhasil=false;
		$db=$this->dbtable->get_db();
		$db->set_autocommit(false);
		$db->begin_transaction();
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			$header_data['tipe'] = "bebas";
			$header_data['tanggal'] = date("Y-m-d H:i:s");
			$result = $this->dbtable->insert($header_data);	
			if ($result===false) {
				$berhasil = false;
				$this->addMessage(" Penjualan Bebas Gagal Disimpan");
			} else {
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail_penjualan_bebas'])) {
					$berhasil=$this->saveDetailResep($id);
				}
			}
			/// notify pendapatan penjualan :
			$row = $this->dbtable->get_row("
				SELECT *
				FROM smis_frm_penjualan_resep
				WHERE id = '" . $id['id'] . "'
			");
			$nama = "";
			$tanggal = "";
			$total = 0;
			if ($row != null) {
				$nama = $row->nama_pasien;
				$tanggal = $row->tanggal;
				$total = $row->total;
			}

			$data = array(
				"jenis_akun"	=> "transaction",
				"jenis_data"	=> "penjualan",
				"id_data"		=> $id['id'],
				"entity"		=> "farmasi",
				"service"		=> "get_detail_accounting_penjualan",
				"data"			=> $id['id'],
				"code"			=> "penjualan-farmasi-" . $id['id'],
				"operation"		=> "",
				"tanggal"		=> $tanggal,
				"uraian"		=> "No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
				"nilai"			=> $total
			);
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_notify_accounting",
				$data,
				"accounting"
			);
			$service_consumer->execute();

			$hpp_total = 0;
			$obat_jadi_rows = $this->dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_jadi
				WHERE id_penjualan_resep = '" . $row->id . "' AND prop NOT LIKE 'del'
			");
			if ($obat_jadi_rows != null) {
				foreach ($obat_jadi_rows as $ojr) {
					$hpp_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $ojr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null && $hpp_row->hpp != null)
						$hpp_total += ($hpp_row->hpp * $ojr->jumlah);
				}
			}
			$bahan_obat_racikan_rows = $this->dbtable->get_result("
				SELECT a.*
				FROM smis_frm_bahan_pakai_obat_racikan a LEFT JOIN smis_frm_penjualan_obat_racikan b ON a.id_penjualan_obat_racikan = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.id_penjualan_resep = '" . $row->id . "'
			");
			if ($bahan_obat_racikan_rows != null) {
				foreach ($bahan_obat_racikan_rows as $brr) {
					$hpp_row = $dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $brr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null && $hpp_row->hpp != null)
						$hpp_total += ($hpp_row->hpp * $brr->jumlah);
				}
			}

			// notify hpp dan persediaan :
			$data = array(
				"jenis_akun"	=> "transaction",
				"jenis_data"	=> "persediaan",
				"id_data"		=> $id['id'],
				"entity"		=> "farmasi",
				"service"		=> "get_detail_accounting_hpp_persediaan_penjualan",
				"data"			=> $id['id'],
				"code"			=> "persediaan-farmasi-" . $id['id'],
				"operation"		=> "",
				"tanggal"		=> $tanggal,
				"uraian"		=> "Persediaan dari No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
				"nilai"			=> $hpp_total
			);
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_notify_accounting",
				$data,
				"accounting"
			);
			$service_consumer->execute();
		} else {
			//do update header here:
			$result = $this->dbtable->update($header_data, $id);
			if ($result===false){
				$berhasil=false;
				$this->addMessage(" Pembatalan Penjualan Bebas Gagal");
			} else {
				$success['type'] = "update";
			}
			if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
				$berhasil=$this->batalDetailResep($id);
				/// notify accounting :
				$row = $this->dbtable->get_row("
					SELECT *
					FROM smis_frm_penjualan_resep
					WHERE id = '" . $id['id'] . "'
				");
				$nama = "";
				$tanggal = "";
				$total = 0;
				if ($row != null) {
					$nama = $row->nama_pasien;
					$tanggal = $row->tanggal;
					$total = $row->total;
				}

				// notify pendapatan penjualan :
				$data = array(
					"jenis_akun"	=> "transaction",
					"jenis_data"	=> "penjualan",
					"id_data"		=> $id['id'],
					"entity"		=> "farmasi",
					"service"		=> "get_detail_accounting_penjualan",
					"data"			=> $id['id'],
					"code"			=> "penjualan-farmasi-" . $id['id'],
					"operation"		=> "del",
					"tanggal"		=> $tanggal,
					"uraian"		=> "No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
					"nilai"			=> $total
				);
				$service_consumer = new ServiceConsumer(
					$this->dbtable->get_db(),
					"push_notify_accounting",
					$data,
					"accounting"
				);
				$service_consumer->execute();

				$hpp_total = 0;
				$obat_jadi_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_penjualan_obat_jadi
					WHERE id_penjualan_resep = '" . $row->id . "' AND prop NOT LIKE 'del'
				");
				if ($obat_jadi_rows != null) {
					foreach ($obat_jadi_rows as $ojr) {
						$hpp_row = $this->dbtable->get_row("
							SELECT *
							FROM smis_frm_histori_hpp
							WHERE id_obat = '" . $ojr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
							ORDER BY id DESC
							LIMIT 0, 1
						");
						if ($hpp_row != null && $hpp_row->hpp != null)
							$hpp_total += ($hpp_row->hpp * $ojr->jumlah);
					}
				}
				$bahan_obat_racikan_rows = $this->dbtable->get_result("
					SELECT a.*
					FROM smis_frm_bahan_pakai_obat_racikan a LEFT JOIN smis_frm_penjualan_obat_racikan b ON a.id_penjualan_obat_racikan = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.id_penjualan_resep = '" . $row->id . "'
				");
				if ($bahan_obat_racikan_rows != null) {
					foreach ($bahan_obat_racikan_rows as $brr) {
						$hpp_row = $dbtable->get_row("
							SELECT *
							FROM smis_frm_histori_hpp
							WHERE id_obat = '" . $brr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
							ORDER BY id DESC
							LIMIT 0, 1
						");
						if ($hpp_row != null && $hpp_row->hpp != null)
							$hpp_total += ($hpp_row->hpp * $brr->jumlah);
					}
				}

				// notify hpp dan persediaan :
				$data = array(
					"jenis_akun"	=> "transaction",
					"jenis_data"	=> "persediaan",
					"id_data"		=> $id['id'],
					"entity"		=> "farmasi",
					"service"		=> "get_detail_accounting_hpp_persediaan_penjualan",
					"data"			=> $id['id'],
					"code"			=> "persediaan-farmasi-" . $id['id'],
					"operation"		=> "del",
					"tanggal"		=> $tanggal,
					"uraian"		=> "Persediaan dari No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
					"nilai"			=> $hpp_total
				);
				$service_consumer = new ServiceConsumer(
					$this->dbtable->get_db(),
					"push_notify_accounting",
					$data,
					"accounting"
				);
				$service_consumer->execute();
			}
		}
		if ($berhasil) $db->commit();
		else $db->rollback();
		$db->set_autocommit(true);
		$success['id'] = $id['id'];
		$success['success'] = 1;
		$success['message'] = $this->getMessage();
		if ($berhasil === false) $success['success'] = 0;
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$data['header'] = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");
		$dpenjualan_bebas_num = 0;
		$dpenjualan_bebas_list = "";
		$bahan_racikan_list = "";
		$obat_jadi_rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_frm_penjualan_obat_jadi
			WHERE id_penjualan_resep = '" .  $id . "' AND prop NOT LIKE 'del'
		");
		$no = 1;
		
		foreach($obat_jadi_rows as $obat_jadi) {
			$dpenjualan_bebas_action = "";
			if (!$_POST['readonly']) {
				$dpenjualan_bebas_action = "<div class='btn-group noprint'>" .
												"<a href='#' onclick='dpenjualan_bebas.edit_obat_jadi(" . $dpenjualan_bebas_num . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" .
													"<i class='icon-edit icon-white'></i>" .
												"</a>" .
												"<a href='#' onclick='dpenjualan_bebas.delete(" . $dpenjualan_bebas_num . ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" .
													"<i class='icon-remove icon-white'></i>" .
												"</a>" .
											"</div>";
			}
			$dpenjualan_bebas_list .=   "<tr id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "'>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_id' style='display: none;'>" . $obat_jadi->id . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_tipe' style='display: none;'>obat_jadi</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_id_obat' style='display: none;'>" . $obat_jadi->id_obat . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_kode_obat' style='display: none;'>" . $obat_jadi->kode_obat . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_nama_jenis_obat' style='display: none;'>" . $obat_jadi->nama_jenis_obat . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_jumlah' style='display: none;'>" . $obat_jadi->jumlah . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_jumlah_lama' style='display: none;'>" . $obat_jadi->jumlah . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_satuan' style='display: none;'>" . $obat_jadi->satuan . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_konversi' style='display: none;'>" . $obat_jadi->konversi . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_satuan_konversi' style='display: none;'>" . $obat_jadi->satuan_konversi . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_nomor'>" . $no++ . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_nama_obat'>" . $obat_jadi->nama_obat . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_f_jumlah'>" . $obat_jadi->jumlah . " " . $obat_jadi->satuan . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_harga'>" . "Rp. " . number_format($obat_jadi->harga, 2, ",", ".") . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_embalase'>" . "Rp. " . number_format($obat_jadi->embalase, 2, ",", ".") . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_tusla'>" . "Rp. " . number_format($obat_jadi->tusla, 2, ",", ".") . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_subtotal'>" . "Rp. " . number_format($obat_jadi->subtotal, 2, ",", ".") . "</td>" .
											"<td id='dpenjualan_bebas_" . $dpenjualan_bebas_num . "_apoteker'>-</td>" .
											"<td>" .
												$dpenjualan_bebas_action .
											"</td>" .
										"</tr>";
			$dpenjualan_bebas_num++;
		}
		$data['dpenjualan_bebas_num'] = $dpenjualan_bebas_num;
		$data['dpenjualan_bebas_list'] = $dpenjualan_bebas_list;
		return $data;
	}
}
?>