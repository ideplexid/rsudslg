<?php
	require_once("farmasi/library/InventoryLibrary.php");
	
	class ReturObatDBResponder extends DBResponder {
		public function save() {
			$data = $this->postToArray();
			$id['id'] = $_POST['id'];
			$result = $this->dbtable->update($data, $id);
			$success['type'] = "update";
			if (isset($_POST['restok'])) {
				if ($_POST['restok'] == true) {
					//menambah stok:
					$retur_data = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_retur_obat_unit
						WHERE id = '" . $id['id'] . "'
					");
					$stok_keluar_data = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_stok_obat_keluar
						WHERE id = '" . $retur_data->lf_id . "'
					");
					if ($stok_keluar_data != null) {
						$stok_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_frm_stok_obat
							WHERE id = '" . $stok_keluar_data->id_stok_obat . "'
						");
						$new_stok_data = array();
						$new_stok_data['sisa'] = $stok_data->sisa + $retur_data->jumlah;
						$new_stok_id = array();
						$new_stok_id['id'] = $stok_keluar_data->id_stok_obat;
						$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
						$stok_dbtable->update($new_stok_data, $new_stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $stok_keluar_data->id_stok_obat;
						$data_riwayat['jumlah_masuk'] = $retur_data->jumlah;
						$data_riwayat['sisa'] = $stok_data->sisa + $retur_data->jumlah;
						$data_riwayat['keterangan'] = "Restok Retur Obat dari Unit " . ArrayAdapter::format("unslug", $retur_data->unit);
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
						//logging kartu stok dan hpp:
						$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
						$sisa_row = $this->dbtable->get_row("
							SELECT SUM(a.sisa) AS 'sisa'
							FROM smis_frm_stok_obat a
							WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $stok_data->id_obat . "' AND a.satuan = '" . $stok_data->satuan . "' AND a.konversi = '1'
						");
						$kartu_stok_data = array(
							"f_id"				=> $id['id'],
							"no_bon"			=> $id['id'],
							"unit"				=> "Retur Obat Unit " . ArrayAdapter::format("unslug", $_POST['unit']) . " ID : " . $id['id'],
							"id_obat"			=> $stok_data->id_obat,
							"kode_obat"			=> $stok_data->kode_obat,
							"nama_obat"			=> $stok_data->nama_obat,
							"nama_jenis_obat"	=> $stok_data->nama_jenis_obat,
							"tanggal"			=> date("Y-m-d"),
							"masuk"				=> $retur_data->jumlah,
							"keluar"			=> 0,
							"sisa"				=> $sisa_row->sisa
						);
						$kartu_stok_dbtable->insert($kartu_stok_data);
						InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $stok_data->id_obat, date("Y-m-d H:i:s"), 1, "Restok Retur Obat Unit " . ArrayAdapter::format("unslug", $_POST['unit']) . " ID : " . $id['id']);
					} else {
						// insert dobat masuk:
						$dobat_masuk_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_masuk");
						$data_dobat_masuk = array();
						$data_dobat_masuk['id_obat_masuk'] = "0";
						$data_dobat_masuk['id_dpo'] = "-1";
						$data_dobat_masuk['id_obat'] = $retur_data->id_obat;
						$data_dobat_masuk['nama_obat'] = $retur_data->nama_obat;
						$data_dobat_masuk['nama_jenis_obat'] = $retur_data->nama_jenis_obat;
						$data_dobat_masuk['formularium'] = "0";
						$data_dobat_masuk['generik'] = "0";
						$data_dobat_masuk['berlogo'] = "0";
						$data_dobat_masuk['medis'] = "1";
						$data_dobat_masuk['label'] = $retur_data->label;
						$data_dobat_masuk['jumlah'] = $retur_data->jumlah;
						$data_dobat_masuk['sisa'] = 0;
						$data_dobat_masuk['satuan'] = $retur_data->satuan;
						$data_dobat_masuk['konversi'] = $retur_data->konversi;
						$data_dobat_masuk['satuan_konversi'] = $retur_data->satuan_konversi;
						$data_dobat_masuk['hna'] = $retur_data->hna;
						$data_dobat_masuk['selisih'] = 0;
						$data_dobat_masuk['diskon'] = 0;
						$data_dobat_masuk['t_diskon'] = 'persen';
						$dobat_masuk_dbtable->insert($data_dobat_masuk);
						$id_dobat_masuk = $dobat_masuk_dbtable->get_inserted_id();
						// insert stok obat:
						$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
						$data_stok = array();
						$data_stok['id_dobat_masuk'] = $id_dobat_masuk;
						$data_stok['id_obat'] = $retur_data->nama_obat;
						$data_stok['nama_obat'] = $retur_data->nama_obat;
						$data_stok['nama_jenis_obat'] = $retur_data->nama_jenis_obat;
						$data_stok['formularium'] = "0";
						$data_stok['berlogo'] = "0";
						$data_stok['generik'] = "0";
						$data_stok['label'] = $retur_data->label;
						$data_stok['id_vendor'] = $retur_data->id_vendor;
						$data_stok['nama_vendor'] = $retur_data->nama_vendor;
						$data_stok['jumlah'] = $retur_data->jumlah;
						$data_stok['sisa'] = $retur_data->jumlah;
						$data_stok['retur'] = 0;
						$data_stok['satuan'] = $retur_data->satuan;
						$data_stok['konversi'] = $retur_data->konversi;
						$data_stok['satuan_konversi'] = $retur_data->satuan_konversi;
						$data_stok['hna'] = $retur_data->hna;
						$data_stok['produsen'] = $retur_data->produsen;
						$data_stok['tanggal_exp'] = $retur_data->tanggal_exp;
						$data_stok['no_batch'] = $retur_data->no_batch;
						$data_stok['turunan'] = 0;
						$stok_dbtable->insert($data_stok);
						$id_stok_obat = $stok_dbtable->get_inserted_id();
						// logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $id_stok_obat;
						$data_riwayat['jumlah_masuk'] = $retur_data->jumlah;
						$data_riwayat['sisa'] = $retur_data->jumlah;
						$data_riwayat['keterangan'] = "Restok Retur Obat dari Unit " . ArrayAdapter::format("unslug", $retur_data->unit);
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
						//logging kartu stok dan hpp:
						$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
						$sisa_row = $this->dbtable->get_row("
							SELECT SUM(a.sisa) AS 'sisa'
							FROM smis_frm_stok_obat a
							WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $retur_data->id_obat . "' AND a.satuan = '" . $retur_data->satuan . "' AND a.konversi = '1'
						");
						$kartu_stok_data = array(
							"f_id"				=> $id['id'],
							"no_bon"			=> $id['id'],
							"unit"				=> "Retur Obat Unit " . ArrayAdapter::format("unslug", $_POST['unit']) . " ID : " . $id['id'],
							"id_obat"			=> $retur_data->id_obat,
							"kode_obat"			=> $retur_data->kode_obat,
							"nama_obat"			=> $retur_data->nama_obat,
							"nama_jenis_obat"	=> $retur_data->nama_jenis_obat,
							"tanggal"			=> date("Y-m-d"),
							"masuk"				=> $retur_data->jumlah,
							"keluar"			=> 0,
							"sisa"				=> $sisa_row->sisa
						);
						$kartu_stok_dbtable->insert($kartu_stok_data);
						InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $retur_data->id_obat, date("Y-m-d H:i:s"), 1, "Restok Retur Obat Unit " . ArrayAdapter::format("unslug", $_POST['unit']) . " ID : " . $id['id']);
					}
				} else {
					//mengurangi stok:
					$retur_data = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_retur_obat_unit
						WHERE id = '" . $id['id'] . "'
					");
					$stok_keluar_data = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_stok_obat_keluar
						WHERE id = '" . $retur_data->lf_id . "'
					");
					$stok_data = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_stok_obat
						WHERE id = '" . $stok_keluar_data->id_stok_obat . "'
					");
					$new_stok_data = array();
					$new_stok_data['sisa'] = $stok_data->sisa - $retur_data->jumlah;
					$new_stok_id = array();
					$new_stok_id['id'] = $stok_keluar_data->id_stok_obat;
					$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
					$stok_dbtable->update($new_stok_data, $new_stok_id);
					//logging riwayat stok:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $stok_keluar_data->id_stok_obat;
					$data_riwayat['jumlah_keluar'] = $retur_data->jumlah;
					$data_riwayat['sisa'] = $stok_data->sisa - $retur_data->jumlah;
					$data_riwayat['keterangan'] = "Pembatalan Restok Retur Obat Unit " . ArrayAdapter::format("unslug", $retur_data->unit) . " ID : " . $retur_data->id;
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
					//logging kartu stok dan hpp:
					$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
					$sisa_row = $this->dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM smis_frm_stok_obat a
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $retur_data->id_obat . "' AND a.satuan = '" . $retur_data->satuan . "' AND a.konversi = '1'
					");
					$kartu_stok_data = array(
						"f_id"				=> $id['id'],
						"no_bon"			=> $id['id'],
						"unit"				=> "Retur Obat Unit " . ArrayAdapter::format("unslug", $_POST['unit']) . " ID : " . $id['id'],
						"id_obat"			=> $retur_data->id_obat,
						"kode_obat"			=> $retur_data->kode_obat,
						"nama_obat"			=> $retur_data->nama_obat,
						"nama_jenis_obat"	=> $retur_data->nama_jenis_obat,
						"tanggal"			=> date("Y-m-d"),
						"masuk"				=> 0,
						"keluar"			=> $retur_data->jumlah,
						"sisa"				=> $sisa_row->sisa
					);
					$kartu_stok_dbtable->insert($kartu_stok_data);
					InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $retur_data->id_obat, date("Y-m-d H:i:s"), 0, "Pembatalan Retur Obat Unit " . ArrayAdapter::format("unslug", $retur_data->unit) . " ID : " . $retur_data->id);
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
	}
?>