<?php
require_once("farmasi/library/InventoryLibrary.php");

class ReturPenjualanResepDBResponder extends DBResponder {
	public function command($command) {
		if ($command != "print_retur") {
			return parent::command($command);
		}
		$pack = null;
		if ($command == "print_retur") {
			$id['id'] = $_POST['id'];
			$data['tercetak'] = 1;
			$this->dbtable->update($data, $id);
			$pack = new ResponsePackage();
			$content = $this->print_retur();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}
		return $pack->getPackage();
	}
	public function print_retur() {
		$id = $_POST['id'];
		$data = $this->dbtable->get_row("SELECT smis_frm_retur_penjualan_resep.id, smis_frm_penjualan_resep.nomor_resep, smis_frm_retur_penjualan_resep.tanggal, smis_frm_penjualan_resep.total, smis_frm_penjualan_resep.nama_dokter, smis_frm_penjualan_resep.noreg_pasien, smis_frm_penjualan_resep.nrm_pasien, smis_frm_penjualan_resep.uri, smis_frm_penjualan_resep.nama_pasien, smis_frm_penjualan_resep.alamat_pasien, smis_frm_penjualan_resep.diskon, smis_frm_penjualan_resep.t_diskon, smis_frm_retur_penjualan_resep.persentase_retur
										 FROM smis_frm_retur_penjualan_resep LEFT JOIN smis_frm_penjualan_resep ON smis_frm_retur_penjualan_resep.id_penjualan_resep = smis_frm_penjualan_resep.id
										 WHERE smis_frm_retur_penjualan_resep.id = '" . $id . "'");
		$obat_jadi_sql = "
			SELECT smis_frm_stok_obat.nama_obat AS 'nama', smis_frm_dretur_penjualan_resep.jumlah, smis_frm_stok_obat.satuan, smis_frm_dretur_penjualan_resep.harga, smis_frm_dretur_penjualan_resep.subtotal
			FROM smis_frm_dretur_penjualan_resep LEFT JOIN smis_frm_stok_obat ON smis_frm_dretur_penjualan_resep.id_stok_obat = smis_frm_stok_obat.id
			WHERE smis_frm_dretur_penjualan_resep.prop NOT LIKE 'del' AND id_retur_penjualan_resep = '" . $id . "'
		";
		$obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
		$content = "<table border='0' width='100%' cellpadding='10' class='etiket_header'>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='0' class='etiket_content' >";
						$content .= "<tr>";
							$content .= "<td colspan='5' align='center' style='font-size: 13px !important;'><b><u>DEPO FARMASI " . ucwords(getSettings($this->dbtable->get_db(), "smis_autonomous_title", "")) . " - RETUR RESEP PASIEN</u></b></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='5'></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='1'>No. Retur</td>";
							$content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $data->id) . "</td>";
							$content .= "<td colspan='1' class='tpadding'>No. Resep</td>";
							$content .= "<td colspan='2'>: " .  $data->nomor_resep . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='1'>Tgl. Retur</td>"; 
							$content .= "<td colspan='1'>: " . ArrayAdapter::format("date d-m-Y H:i", $data->tanggal) . "</td>"; 
							$content .= "<td colspan='1' class='tpadding'>Nama Dokter</td>";
							$content .= "<td colspan='2'>: " . substr(strtoupper($data->nama_dokter), 0, 30) . "</td>";
						$content .= "</tr>";
						if ($data->noreg_pasien != 0 && $data->nrm_pasien != 0) {
							$content .= "<tr>";
								$content .= "<td colspan='1'>Nama Pasien</td>";
								$content .= "<td colspan='1'>: " . substr(strtoupper($data->nama_pasien), 0, 30) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>No. Registrasi</td>";
								$content .= "<td colspan='2'>: " . ArrayAdapter::format("only-digit8", $data->noreg_pasien) . "</td>";
							$content .= "</tr>";
							$content .= "<tr>";
								$content .= "<td colspan='1'>NRM</td>";
								$content .= "<td colspan='1'>: " . ArrayAdapter::format("only-digit8", $data->nrm_pasien) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>Alamat Pasien</td>";
								$content .= "<td colspan='2'>: " . substr(strtoupper($data->alamat_pasien), 0, 30) . "</td>";
							$content .= "</tr>";
						} else {
							$content .= "<tr>";
								$content .= "<td colspan='1'>Nama Pasien</td>";
								$content .= "<td colspan='1'>: " . substr(strtoupper($data->nama_pasien), 0, 30) . "</td>";
								$content .= "<td colspan='1' class='tpadding'>Alamat Pasien</td>";
								$content .= "<td colspan='2'>: " . substr(strtoupper($data->alamat_pasien), 0, 30) . "</td>";
							$content .= "</tr>";
						}
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "</tr>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='1' class='etiket_content'>";
					$content .= "<tr align='center'>";
						$content .= "<td colspan='1'>No.</td>";
						$content .= "<td colspan='1'>Obat</td>";
						$content .= "<td colspan='1'>Jumlah</td>";
						$content .= "<td colspan='1'>Harga Satuan</td>";
						$content .= "<td colspan='1'>Subtotal</td>";
					$content .= "</tr>";
					$no_barang = 1;
					$total = 0;
					foreach($obat_jadi_data as $ojd) {
						$content .= "<tr>";
							$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
							$content .= "<td colspan='1'>" . $ojd->nama_obat . "</td>";
							$content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
						$content .= "</tr>";
						$total += $ojd->subtotal;
					}
					$content .= "<tr>";
						$content .= "<td colspan='4' align='right'>Total</td>";
						$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
						$content .= "<td colspan='4' align='right'>Diskon</td>";
						$v_diskon = 0;
						if ($data->t_diskon == "persen" || $data->t_diskon == "gratis") {
							$v_diskon = ($total * $data->diskon) / 100;
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
						} else {
							$v_diskon = ($total * $data->diskon) / $data->total;
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
						}
					$content .= "</tr>";
					$content .= "<tr>";
						$content .= "<td colspan='4' align='right'>Total Setelah Diskon</td>";
						$total = $total - $v_diskon;
						$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
						$potongan = 100 - $data->persentase_retur;
						$v_potongan = ($total * $potongan) / 100;
						$content .= "<td colspan='4' align='right'>Potongan Retur (" . $potongan . " %)</td>";
						$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $v_potongan) . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
						$potongan = 100 - $data->persentase_retur;
						$content .= "<td colspan='4' align='right'>Uang Kembali</td>";
						$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $total - $v_potongan) . "</td>";
					$content .= "</tr>";
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "</tr>";
			$content .= "<tr>";
				$content .= "<td colspan='5'>";
					$content .= "<table border='0' align='center' class='etiket_content'>";
						$content .= "<tr>";
							$content .= "<td align='center'>PENERIMA,</td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td align='center'>(_____________________)</td>";
							$content .= "<td>&Tab;</td>";
							global $user;
							$content .= "<td align='center'>" . $user->getNameOnly() . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
						$content .= "</tr>";
					$content .= "</table>";
				$content .= "</td>";
			$content .= "</tr>";
		$content .= "</table>";
		return $content;
	}
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			$header_data['persentase_retur'] = "90";
			$header_data['tanggal'] = date("Y-m-d H:i:s");
			$result = $this->dbtable->insert($header_data);
			if (isset($_POST['id_penjualan_resep'])) {
				$penjualan_resep_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_penjualan_resep");
				$resep_data = array();
				$resep_data['diretur'] = "1";
				$resep_id['id'] = $_POST['id_penjualan_resep'];
				$penjualan_resep_dbtable->update($resep_data, $resep_id);
			}
			$id['id'] = $this->dbtable->get_inserted_id();
			$success['type'] = "insert";
			if (isset($_POST['detail'])) {
				//do insert detail here:
				$dretur_penjualan_resep_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dretur_penjualan_resep");
				$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					$dretur_data = array();
					$dretur_data['id_retur_penjualan_resep'] = $id['id'];
					$dretur_data['id_stok_obat'] = $d['id_stok_obat'];
					$dretur_data['jumlah'] = $d['jumlah'];
					$dretur_data['harga'] = $d['harga'];
					$dretur_data['subtotal'] = $d['jumlah'] * $d['harga'];
					$dretur_penjualan_resep_dbtable->insert($dretur_data);
					$stok_row = $stok_obat_dbtable->get_row("
						SELECT id, sisa
						FROM smis_frm_stok_obat
						WHERE id = '" . $d['id_stok_obat'] . "'
					");
					$stok_data = array();
					$stok_data['sisa'] = $stok_row->sisa + $d['jumlah'];
					$stok_id['id'] = $d['id_stok_obat'];
					$stok_obat_dbtable->update($stok_data, $stok_id);
					//logging riwayat stok:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $d['id_stok_obat'];
					$data_riwayat['jumlah_masuk'] = $d['jumlah'];
					$data_riwayat['sisa'] = $stok_row->sisa + $d['jumlah'];
					$data_riwayat['keterangan'] = "Stok Retur Penjualan Resep (" . ArrayAdapter::format("only-digit8", $_POST['id_penjualan_resep']) . ")";
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
				}
			}

			$dretur_rows = $this->dbtable->get_result("
				SELECT b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', b.satuan
				FROM smis_frm_dretur_penjualan_resep a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id
				WHERE a.prop NOT LIKE 'del' AND a.id_retur_penjualan_resep = '" . $id['id'] . "'
				GROUP BY b.id_obat, b.satuan
			");
			//logging kartu stok:
			$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
			$keterangan = "Resep (" . ArrayAdapter::format("only-digit8", $_POST['id_penjualan_resep']) . ")";
			foreach ($dretur_rows as $drr) {
				$sisa_row = $this->dbtable->get_row("
					SELECT SUM(a.sisa) AS 'sisa'
					FROM smis_frm_stok_obat a
					WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $drr->id_obat . "' AND a.satuan = '" . $drr->satuan . "' AND a.konversi = '1'
				");
				$kartu_stok_data = array(
					"f_id"				=> $id['id'],
					"no_bon"			=> $id['id'],
					"unit"				=> "Retur Penjualan Resep : " . $keterangan,
					"id_obat"			=> $drr->id_obat,
					"kode_obat"			=> $drr->kode_obat,
					"nama_obat"			=> $drr->nama_obat,
					"nama_jenis_obat"	=> $drr->nama_jenis_obat,
					"tanggal"			=> date("Y-m-d"),
					"masuk"				=> $drr->jumlah,
					"keluar"			=> 0,
					"sisa"				=> $sisa_row->sisa
				);
				$result = $kartu_stok_dbtable->insert($kartu_stok_data);
				if($result===false){
					$this->addMessage(" Obat <strong>".$drr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
					return false;
				}
				InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $drr->id_obat, date("Y-m-d H:i:s"), 1, "Retur Penjualan Resep ID : " . $id['id']);
			}
			$data_row = $this->dbtable->get_row("
				SELECT a.*, b.nama_pasien, b.tipe
				FROM smis_frm_retur_penjualan_resep a LEFT JOIN smis_frm_penjualan_resep b ON a.id_penjualan_resep = b.id
				WHERE a.id = '" . $id['id'] . "'
			");
			// notify accounting :
			$nama = "";
			$tanggal = "";
			$total = 0;
			if ($data_row != null) {
				$nama = $data_row->nama_pasien;
				$tanggal = $data_row->tanggal;
				$persentase_retur = $data_row->persentase_retur;
				$data_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_dretur_penjualan_resep
					WHERE id_retur_penjualan_resep = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				if ($data_rows != null)
					foreach ($data_rows as $dr)
						$total += ($dr->subtotal * $persentase_retur / 100);
			}
			$data = array(
				"jenis_akun"	=> "transaction",
				"jenis_data"	=> "retur_penjualan",
				"id_data"		=> $id['id'],
				"entity"		=> "farmasi",
				"service"		=> "get_detail_accounting_retur",
				"data"			=> $id['id'],
				"code"			=> "retur_penjualan-farmasi-" . $id['id'],
				"operation"		=> "",
				"tanggal"		=> $tanggal,
				"uraian"		=> "Retur Penjualan ID Transaksi : " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
				"nilai"			=> $total
			);
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_notify_accounting",
				$data,
				"accounting"
			);
			$service_consumer->execute();
		} else {
			//do update header here:
			$result = $this->dbtable->update($header_data, $id);
			$success['type'] = "update";
			if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
				$header_row = $this->dbtable->get_row("
					SELECT *
					FROM smis_frm_retur_penjualan_resep
					WHERE id = '" . $id['id'] . "'
				");
				$penjualan_resep_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_penjualan_resep");
				$resep_data = array();
				$resep_data['diretur'] = "0";
				$resep_id['id'] = $header_row->id_penjualan_resep;
				$penjualan_resep_dbtable->update($resep_data, $resep_id);
				$detail_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_dretur_penjualan_resep
					WHERE id_retur_penjualan_resep = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
				foreach($detail_rows as $dr) {
					$stok_row = $stok_obat_dbtable->get_row("
						SELECT id, sisa
						FROM smis_frm_stok_obat
						WHERE id = '" . $dr->id_stok_obat . "'
					");
					$stok_data = array();
					$stok_data['sisa'] = $stok_row->sisa - $dr->jumlah;
					$stok_id['id'] = $dr->id_stok_obat;
					$stok_obat_dbtable->update($stok_data, $stok_id);
					//logging riwayat stok:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $dr->id_stok_obat;
					$data_riwayat['jumlah_keluar'] = $dr->jumlah;
					$data_riwayat['sisa'] = $stok_row->sisa - $dr->jumlah;
					$data_riwayat['keterangan'] = "Pembatalan Retur Penjualan Resep (" . ArrayAdapter::format("only-digit8", $id['id']) . ")";
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
				}
				$dretur_rows = $this->dbtable->get_result("
					SELECT b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat, SUM(a.jumlah) AS 'jumlah', b.satuan
					FROM smis_frm_dretur_penjualan_resep a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id
					WHERE a.prop NOT LIKE 'del' AND a.id_retur_penjualan_resep = '" . $id['id'] . "'
					GROUP BY b.id_obat, b.satuan
				");
				//logging kartu stok:
				$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
				foreach ($dretur_rows as $drr) {
					$sisa_row = $this->dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM smis_frm_stok_obat a
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $drr->id_obat . "' AND a.satuan = '" . $drr->satuan . "' AND a.konversi = '1'
					");
					$kartu_stok_data = array(
						"f_id"				=> $id['id'],
						"no_bon"			=> $id['id'],
						"unit"				=> "Retur Penjualan Resep : Pembatalan",
						"id_obat"			=> $drr->id_obat,
						"kode_obat"			=> $drr->kode_obat,
						"nama_obat"			=> $drr->nama_obat,
						"nama_jenis_obat"	=> $drr->nama_jenis_obat,
						"tanggal"			=> date("Y-m-d"),
						"masuk"				=> 0,
						"keluar"			=> $drr->jumlah,
						"sisa"				=> $sisa_row->sisa
					);
					$result = $kartu_stok_dbtable->insert($kartu_stok_data);
					if($result===false){
						$this->addMessage(" Obat <strong>".$drr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
						return false;
					}
					InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $drr->id_obat, date("Y-m-d H:i:s"), 0, "Pembatalan Retur Penjualan Bebas ID : " . $id['id']);
				}
			}
			$data_row = $this->dbtable->get_row("
				SELECT a.*, b.nama_pasien
				FROM smis_frm_retur_penjualan_resep a LEFT JOIN smis_frm_penjualan_resep b ON a.id_penjualan_resep = b.id
				WHERE a.id = '" . $id['id'] . "'
			");
			// notify accounting :
			$nama = "";
			$tanggal = "";
			$total = 0;
			if ($data_row != null) {
				$nama = $data_row->nama_pasien;
				$tanggal = $data_row->tanggal;
				$persentase_retur = $data_row->persentase_retur;
				$data_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_dretur_penjualan_resep
					WHERE id_retur_penjualan_resep = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				if ($data_rows != null)
					foreach ($data_rows as $dr)
						$total += ($dr->subtotal * $persentase_retur / 100);
			}
			$data = array(
				"jenis_akun"	=> "transaction",
				"jenis_data"	=> "retur_penjualan",
				"id_data"		=> $id['id'],
				"entity"		=> "farmasi",
				"service"		=> "get_detail_accounting_retur",
				"data"			=> $id['id'],
				"code"			=> "retur_penjualan-farmasi-" . $id['id'],
				"operation"		=> "del",
				"tanggal"		=> $tanggal,
				"uraian"		=> "Retur Penjualan ID Transaksi : " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
				"nilai"			=> $total
			);
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_notify_accounting",
				$data,
				"accounting"
			);
			$service_consumer->execute();
		}
		$success['id'] = $id['id'];
		$success['success'] = 1;
		if ($result === false) $success['success'] = 0;
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$data['header'] = $this->dbtable->get_row("
			SELECT smis_frm_retur_penjualan_resep.*, smis_frm_penjualan_resep.nomor_resep, smis_frm_penjualan_resep.nama_dokter, smis_frm_penjualan_resep.nama_pasien, smis_frm_penjualan_resep.noreg_pasien, smis_frm_penjualan_resep.nrm_pasien, smis_frm_penjualan_resep.alamat_pasien
			FROM smis_frm_retur_penjualan_resep LEFT JOIN smis_frm_penjualan_resep ON smis_frm_retur_penjualan_resep.id_penjualan_resep = smis_frm_penjualan_resep.id
			WHERE smis_frm_retur_penjualan_resep.id = '" . $id . "'
		");
		$detail_rows = $this->dbtable->get_result("
			SELECT smis_frm_dretur_penjualan_resep.*, smis_frm_stok_obat.id_obat, smis_frm_stok_obat.nama_jenis_obat, smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.satuan, smis_frm_stok_obat.konversi, smis_frm_stok_obat.satuan_konversi
			FROM smis_frm_dretur_penjualan_resep LEFT JOIN smis_frm_stok_obat ON smis_frm_dretur_penjualan_resep.id_stok_obat = smis_frm_stok_obat.id
			WHERE smis_frm_dretur_penjualan_resep.id_retur_penjualan_resep = '" . $id . "' AND smis_frm_dretur_penjualan_resep.prop NOT LIKE 'del'
		");
		$detail_list = "";
		$row_id = 0;
		foreach($detail_rows as $dr) {
			if ($dr->jumlah > 0) {
				$detail_list .= "<tr id='detail_" . $row_id . "'>" .
									"<td id='detail_" . $row_id . "_id' style='display: none;'>" . $dr->id . "</td>" .
									"<td id='detail_" . $row_id . "_id_stok_obat' style='display: none;'>" . $dr->id_stok_obat . "</td>" .
									"<td id='detail_" . $row_id . "_id_obat' style='display: none;'>" . $dr->id_obat . "</td>" .
									"<td id='detail_" . $row_id . "_nama_jenis_obat' style='display: none;'>" . $dr->nama_jenis_obat . "</td>" .
									"<td id='detail_" . $row_id . "_harga' style='display: none;'>" . $dr->harga . "</td>" .
									"<td id='detail_" . $row_id . "_jumlah_retur' style='display: none;'>" . $dr->jumlah . "</td>" .
									"<td id='detail_" . $row_id . "_satuan' style='display: none;'>" . $dr->satuan . "</td>" .
									"<td id='detail_" . $row_id . "_konversi' style='display: none;'>" . $dr->konversi . "</td>" .
									"<td id='detail_" . $row_id . "_satuan_konversi' style='display: none;'>" . $dr->satuan_konversi . "</td>" .
									"<td id='detail_" . $row_id . "_nama_obat'>" . $dr->nama_obat . "</td>" .
									"<td id='detail_" . $row_id . "_f_harga'>" . "Rp. " . number_format($dr->harga, 2, ",", ".") . "</td>" .
									"<td id='detail_" . $row_id . "_f_jumlah'>" . $dr->jumlah . " " . $dr->satuan . "</td>" .
									"<td></td>" .
								"</tr>";
				$row_id++;
			}
		}
		$data['detail_list'] = $detail_list;
		return $data;
	}
}
?>