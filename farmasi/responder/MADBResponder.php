<?php
class MADBResponder extends DBResponder {
	public function edit() {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$data = $this->dbtable->get_row("
			SELECT id_obat, '" . $satuan . "' AS satuan, '" . $konversi . "' AS konversi, '" . $satuan_konversi . "' AS satuan_konversi, 1 AS 'jumlah', hpp AS 'harga_ma'
			FROM smis_frm_histori_hpp
			WHERE id_obat = '" . $id_obat . "' AND prop NOT LIKE 'del'
			ORDER BY id DESC
			LIMIT 0, 1
		");
		return $data;
	}
}
?>