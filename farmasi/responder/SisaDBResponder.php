<?php
class SisaDBResponder extends DBResponder {
	public function edit() {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$default_margin = 0;
		$value_margin = 0;
		// margin per obat - jenis pasien :
		if (getSettings($this->dbtable->get_db(), "farmasi-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 3) {
			$jenis_pasien = "";
			if (isset($_POST['jenis_pasien']))
				$jenis_pasien = $_POST['jenis_pasien'];
			$value_margin = "b.margin_jual";
			$sub_query = "
				SELECT id_obat, MAX(hna) AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
				FROM (		
					SELECT a.id_obat, a.sisa, a.satuan, a.konversi, a.satuan_konversi, a.hna
					FROM smis_frm_stok_obat a
					WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
				) v_stok_obat
				GROUP BY id_obat, satuan, konversi, satuan_konversi
			";
			if (getSettings($this->dbtable->get_db(), "farmasi-hpp-patokan_hpp", 0) == 1) {
				$sub_query = "
					SELECT id_obat, MAX(hna) AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
					FROM (		
						SELECT a.id_obat, a.sisa, a.satuan, a.konversi, a.satuan_konversi, a.hna
						FROM smis_frm_stok_obat a
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "' AND a.sisa > 0
					) v_stok_obat
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				";	
			}
			$data = $this->dbtable->get_row("
				SELECT a.*, CASE WHEN b.margin_jual IS NULL THEN " . $default_margin . " ELSE " . $value_margin . " END AS 'markup'
				FROM
				(
					" . $sub_query . "
				) a LEFT JOIN (
					SELECT id_obat, margin_jual
					FROM smis_frm_margin_jual_obat_jenis_pasien
					WHERE id_obat = " . $id_obat . " AND prop NOT LIKE 'del' AND slug = '" . $jenis_pasien . "'
					ORDER BY id DESC
					LIMIT 0, 1
				) b ON a.id_obat = b.id_obat
			");
			return $data;
		}
		// margin per obat :
		if (getSettings($this->dbtable->get_db(), "farmasi-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 2) {
			$value_margin = "b.margin_jual";
			$sub_query = "
				SELECT id_obat, MAX(hna) AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
				FROM (		
					SELECT a.id_obat, a.sisa, a.satuan, a.konversi, a.satuan_konversi, a.hna
					FROM smis_frm_stok_obat a
					WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "'
				) v_stok_obat
				GROUP BY id_obat, satuan, konversi, satuan_konversi
			";
			if (getSettings($this->dbtable->get_db(), "farmasi-hpp-patokan_hpp", 0) == 1) {
				$sub_query = "
					SELECT id_obat, MAX(hna) AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
					FROM (		
						SELECT a.id_obat, a.sisa, a.satuan, a.konversi, a.satuan_konversi, a.hna
						FROM smis_frm_stok_obat a
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "' AND a.sisa > 0
					) v_stok_obat
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				";	
			}
			$data = $this->dbtable->get_row("
				SELECT a.*, CASE WHEN b.margin_jual IS NULL THEN " . $default_margin . " ELSE " . $value_margin . " END AS 'markup'
				FROM
				(
					" . $sub_query . "
				) a LEFT JOIN (
					SELECT id_obat, margin_jual
					FROM smis_frm_margin_jual_obat
					WHERE id_obat = " . $id_obat . " AND prop NOT LIKE 'del'
					ORDER BY id DESC
					LIMIT 0, 1
				) b ON a.id_obat = b.id_obat
			");
			return $data;
		}
		// margin per jenis pasien :
		if (getSettings($this->dbtable->get_db(), "farmasi-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 1) {
			$jenis_pasien = $_POST['jenis_pasien'];
			$data = $this->dbtable->get_row("
				SELECT *
				FROM smis_frm_margin_jual_jenis_pasien
				WHERE prop NOT LIKE 'del' AND slug = '" . $jenis_pasien . "'
				LIMIT 0, 1
			");
			if ($data != null)
				$value_margin = $data->margin_jual;
			$filter = "";
			if (getSettings($this->dbtable->get_db(), "farmasi-hpp-patokan_hpp", 0) == 1) {
				$filter = "  AND sisa > 0 ";
			}
			$data = $this->dbtable->get_row("
				SELECT id_obat, MAX(hna) AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, " . $value_margin . " AS markup
				FROM smis_frm_stok_obat
				WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '" . $konversi . "' AND satuan_konversi = '" . $satuan_konversi . "' " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi
			");
			return $data;
		}
		// margin per jenis transaksi:
		if (getSettings($this->dbtable->get_db(), "farmasi-" . $_POST['action'] . "-mode_margin_penjualan", 0) == 0) {
			$value_margin = getSettings($this->dbtable->get_db(), "farmasi-" . $_POST['action'] . "-margin_penjualan", 0);
			$filter = "";
			if (getSettings($this->dbtable->get_db(), "farmasi-hpp-patokan_hpp", 0) == 1) {
				$filter = "  AND sisa > 0 ";
			}
			$data = $this->dbtable->get_row("
				SELECT id_obat, MAX(hna) AS 'hna', SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, " . $value_margin . " AS markup
				FROM smis_frm_stok_obat
				WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '" . $konversi . "' AND satuan_konversi = '" . $satuan_konversi . "' " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi
			");
			return $data;
		}
		return null;
	}
}
?>