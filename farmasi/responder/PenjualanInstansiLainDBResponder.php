<?php
require_once("smis-base/smis-include-service-consumer.php");
require_once("farmasi/library/InventoryLibrary.php");
require_once("farmasi/responder/PenjualanResepDBResponder.php");

class PenjualanInstansiLainDBResponder extends PenjualanResepDBResponder {
	public function print_prescription_escp() {
		$id = $_POST['id'];
		$data = $this->dbtable->get_row("
			SELECT *
		    FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");
		$obat_jadi_data = $this->dbtable->get_result("
			SELECT nama_obat AS 'nama', jumlah, satuan, harga, subtotal, embalase, tusla, 0 AS 'biaya_racik', 'obat_jadi' AS 'label'
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
		");
		
		$no_barang = 1;
		$total = 0;
		foreach($obat_jadi_data as $ojd) {
			$items[] = array(
				"no" => $no_barang++,
				"name" => $ojd->nama,
				"qty" => $ojd->jumlah . " " . $ojd->satuan,
				"prc" => $this->formatMoneyForPrint($ojd->harga),
				"jaspel" => $this->formatMoneyForPrint($ojd->embalase + $ojd->tusla),
				"sbt" => $this->formatMoneyForPrint($ojd->subtotal)
			);
			$total += $ojd->subtotal;
		}
		
		$diskonLabel = 'DISKON';
		$diskonValue = $data->diskon;		
		if ($data->diskon > 0 && ($data->t_diskon == "persen" || $data->t_diskon == "gratis")) {
			$diskonLabel = "DISKON (" . $data->diskon . " %)";
			$diskonValue =  $total * $data->diskon / 100;
		}
		
		global $user;
		loadLibrary("smis-libs-function-math");
						
		$template = file_get_contents("depo_farmasi/webprint/penjualan_obat.json");
			
		return array(
			'template' => json_decode($template, true),
			'data' => array(
				'kode_resep' => "RJ",
				'no_penjualan' => ArrayAdapter::format('only-digit8', $data->id),
				'no_resep' => $data->nomor_resep,
				'tgl_resep' => ArrayAdapter::format('date d-m-Y H:i', $data->tanggal),
				'nama_dokter' => "-",
				'nama_pasien' => substr(strtoupper($data->nama_pasien), 0, 30),
				'no_registrasi' => "-",
				'nrm_pasien' => "-",
				'alamat_pasien' => substr(strtoupper($data->alamat_pasien), 0, 30),
				'jenis_pasien' => "-",
				'total' => $this->formatMoneyForPrint($total),
				'diskon_label' => $diskonLabel,
				'diskon' => $this->formatMoneyForPrint($diskonValue),
				'tagihan' => $this->formatMoneyForPrint($data->total),
				'terbilang' => numbertell($data->total),
				'ppn_info' => $data->uri ? '' : '* Sudah termasuk PPn (10 %)',
				'petugas' => $user->getNameOnly(),
				'items' => $items
			)
		);
	}
	public function print_prescription() {
		$id = $_POST['id'];
		$data = $this->dbtable->get_row("SELECT *
										 FROM smis_frm_penjualan_resep
										 WHERE id = '" . $id . "'");
		$obat_jadi_sql = "
			SELECT nama_obat AS 'nama', jumlah, satuan, harga, embalase, tusla, subtotal, 'obat_jadi' AS 'label'
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id . "'
		";
		$obat_jadi_data = $this->dbtable->get_result($obat_jadi_sql);
		$content = "<table border='0' width='100%' cellpadding='3' class='etiket_header'>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='0' class='etiket_content' >";
						$content .= "<tr>";
							$content .= "<td colspan='5' align='center' style='font-size: 14px !important;'><b><u>DEPO FARMASI - PENJUALAN UP</u></b></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='1'>No. Penjualan / No. Resep</td>";
							$content .= "<td colspan='1'>: " .  ArrayAdapter::format("only-digit8", $data->id) . " / " . $data->nomor_resep . "</td>";
							$content .= "<td colspan='1'>Tgl. Penjualan</td>"; 
							$content .= "<td colspan='2'>: " . ArrayAdapter::format("date d-m-Y H:i", $data->tanggal) . "</td>"; 
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='1'>Instansi</td>";
							$content .= "<td colspan='1'>: " . substr(strtoupper($data->nama_pasien), 0, 30) . "</td>";
							$content .= "<td colspan='1'>No. Telpon</td>";
							$content .= "<td colspan='2'>: " . substr(strtoupper($data->no_telpon), 0, 30) . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td colspan='1'>Alamat</td>";
							$content .= "<td colspan='4'>: " . substr(strtoupper($data->alamat_pasien), 0, 60) . "</td>";
						$content .= "</tr>";
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "</tr>";
			$content .= "<tr>";
				$content .= "<td>";
					$content .= "<div align='center'><table border='1' class='etiket_content'>";
					$content .= "<tr align='center'>";
						$content .= "<td colspan='1'>No.</td>";
						$content .= "<td colspan='1'>Obat</td>";
						$content .= "<td colspan='1'>Jumlah</td>";
						$content .= "<td colspan='1'>Harga Satuan</td>";
						$content .= "<td colspan='1'>Jasa Pelayanan</td>";
						$content .= "<td colspan='1'>Subtotal</td>";
					$content .= "</tr>";
					$no_barang = 1;
					$total = 0;
					foreach($obat_jadi_data as $ojd) {
						$content .= "<tr>";
							$content .= "<td colspan='1'>" . $no_barang++ . "</td>";
							$content .= "<td colspan='1'>" . $ojd->nama . "</td>";
							$content .= "<td colspan='1'>" . $ojd->jumlah . " " . $ojd->satuan . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->harga) . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->embalase + $ojd->tusla) . "</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $ojd->subtotal) . "</td>";
						$content .= "</tr>";
						$total += $ojd->subtotal;
					}
					$content .= "<tr>";
						$content .= "<td colspan='5' align='right'>Total</td>";
						$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $total) . "</td>";
					$content .= "</tr>";
					if ($data->diskon > 0) {
						if ($data->t_diskon == "persen") {
							$v_diskon = ($total * $data->diskon) / 100;
							$content .= "<td colspan='5' align='right'>Diskon (" . $data->diskon . " %)</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $v_diskon) . "</td>";
						} else {
							$content .= "<td colspan='5' align='right'>Diskon</td>";
							$content .= "<td colspan='1' align='right'>" . ArrayAdapter::format("money Rp.", $data->diskon) . "</td>";
						}
					}
					$content .= "<tr>";
						$content .= "<td colspan='5' align='right'>Tagihan</td>";
						$content .= "<td colspan='1'>" . ArrayAdapter::format("money Rp.", $data->total) . "</td>";
					$content .= "</tr>";
					loadLibrary("smis-libs-function-math");
					$content .= "<tr>";
						$content .= "<td colspan='1' align='right'>Terbilang</td>";
						$content .= "<td colspan='11'>" . numbertell($data->total) . " Rupiah </td>";
					$content .= "</tr>";
					$content .= "</table></div>";
				$content .= "</td>";
			$content .= "</tr>";
			$content .= "</tr>";
				$content .= "<td colspan='5'><i>* Sudah termasuk PPn (10 %)</i></td>";
			$content .= "<tr>";
			$content .= "<tr>";
				$content .= "<td colspan='5'>";
					$content .= "<table border='0' align='center' class='etiket_content'>";
						$content .= "<tr>";
							$content .= "<td align='center'>PENERIMA,</td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td align='center'>PETUGAS DEPO FARMASI,</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
							$content .= "<td>&Tab;</td>";
							$content .= "<td></td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td align='center'>(_____________________)</td>";
							$content .= "<td>&Tab;</td>";
							global $user;
							$content .= "<td align='center'>" . $user->getNameOnly() . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
							$content .= "<td></td>";
						$content .= "</tr>";
					$content .= "</table>";
				$content .= "</td>";
			$content .= "</tr>";
		$content .= "</table>";
		return $content;
	}
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		$berhasil=false;
		$db=$this->dbtable->get_db();
		$db->set_autocommit(false);
		$db->begin_transaction();
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			$header_data['tipe'] = "instansi_lain";
			$header_data['tanggal'] = date("Y-m-d H:i:s");
			$result = $this->dbtable->insert($header_data);	
			if($result===false){
				$berhasil = false;
				$this->addMessage(" Penjualan UP Gagal Disimpan");
			}else{
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail_penjualan_instansi_lain'])) {
					$berhasil=$this->saveDetailResep($id);
				}
			}		
			/// notify pendapatan penjualan :
			$row = $this->dbtable->get_row("
				SELECT *
				FROM smis_frm_penjualan_resep
				WHERE id = '" . $id['id'] . "'
			");
			$nama = "";
			$tanggal = "";
			$total = 0;
			if ($row != null) {
				$nama = $row->nama_pasien;
				$tanggal = $row->tanggal;
				$total = $row->total;
			}

			$data = array(
				"jenis_akun"	=> "transaction",
				"jenis_data"	=> "penjualan",
				"id_data"		=> $id['id'],
				"entity"		=> "farmasi",
				"service"		=> "get_detail_accounting_penjualan",
				"data"			=> $id['id'],
				"code"			=> "penjualan-farmasi-" . $id['id'],
				"operation"		=> "",
				"tanggal"		=> $tanggal,
				"uraian"		=> "No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
				"nilai"			=> $total
			);
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_notify_accounting",
				$data,
				"accounting"
			);
			$service_consumer->execute();

			$hpp_total = 0;
			$obat_jadi_rows = $this->dbtable->get_result("
				SELECT *
				FROM smis_frm_penjualan_obat_jadi
				WHERE id_penjualan_resep = '" . $row->id . "' AND prop NOT LIKE 'del'
			");
			if ($obat_jadi_rows != null) {
				foreach ($obat_jadi_rows as $ojr) {
					$hpp_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $ojr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null && $hpp_row->hpp != null)
						$hpp_total += ($hpp_row->hpp * $ojr->jumlah);
				}
			}
			$bahan_obat_racikan_rows = $this->dbtable->get_result("
				SELECT a.*
				FROM smis_frm_bahan_pakai_obat_racikan a LEFT JOIN smis_frm_penjualan_obat_racikan b ON a.id_penjualan_obat_racikan = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.id_penjualan_resep = '" . $row->id . "'
			");
			if ($bahan_obat_racikan_rows != null) {
				foreach ($bahan_obat_racikan_rows as $brr) {
					$hpp_row = $dbtable->get_row("
						SELECT *
						FROM smis_frm_histori_hpp
						WHERE id_obat = '" . $brr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
						ORDER BY id DESC
						LIMIT 0, 1
					");
					if ($hpp_row != null && $hpp_row->hpp != null)
						$hpp_total += ($hpp_row->hpp * $brr->jumlah);
				}
			}

			// notify hpp dan persediaan :
			$data = array(
				"jenis_akun"	=> "transaction",
				"jenis_data"	=> "persediaan",
				"id_data"		=> $id['id'],
				"entity"		=> "farmasi",
				"service"		=> "get_detail_accounting_hpp_persediaan_penjualan",
				"data"			=> $id['id'],
				"code"			=> "persediaan-farmasi-" . $id['id'],
				"operation"		=> "",
				"tanggal"		=> $tanggal,
				"uraian"		=> "Persediaan dari No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
				"nilai"			=> $hpp_total
			);
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_notify_accounting",
				$data,
				"accounting"
			);
			$service_consumer->execute();
		} else {
			//do update header here:
			$result = $this->dbtable->update($header_data, $id);
			if($result===false){
				$berhasil=false;
				$this->addMessage(" Pembatalan Penjualan UP Gagal");
			}else{
				$success['type'] = "update";
			}
			
			if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
				$berhasil=$this->batalDetailResep($id);
				// notify pendapatan penjualan :
				$row = $this->dbtable->get_row("
					SELECT *
					FROM smis_frm_penjualan_resep
					WHERE id = '" . $id['id'] . "'
				");
				$nama = "";
				$tanggal = "";
				$total = 0;
				if ($row != null) {
					$nama = $row->nama_pasien;
					$tanggal = $row->tanggal;
					$total = $row->total;
				}

				$data = array(
					"jenis_akun"	=> "penjualan",
					"jenis_data"	=> "penjualan_obat",
					"id_data"		=> $id['id'],
					"entity"		=> "farmasi",
					"service"		=> "get_detail_accounting_penjualan",
					"data"			=> $id['id'],
					"code"			=> "penjualan-farmasi-" . $id['id'],
					"operation"		=> "del",
					"tanggal"		=> $tanggal,
					"uraian"		=> "No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
					"nilai"			=> $total
				);
				$service_consumer = new ServiceConsumer(
					$this->dbtable->get_db(),
					"push_notify_accounting",
					$data,
					"accounting"
				);
				$service_consumer->execute();

				$hpp_total = 0;
				$obat_jadi_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_penjualan_obat_jadi
					WHERE id_penjualan_resep = '" . $row->id . "' AND prop NOT LIKE 'del'
				");
				if ($obat_jadi_rows != null) {
					foreach ($obat_jadi_rows as $ojr) {
						$hpp_row = $this->dbtable->get_row("
							SELECT *
							FROM smis_frm_histori_hpp
							WHERE id_obat = '" . $ojr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
							ORDER BY id DESC
							LIMIT 0, 1
						");
						if ($hpp_row != null && $hpp_row->hpp != null)
							$hpp_total += ($hpp_row->hpp * $ojr->jumlah);
					}
				}
				$bahan_obat_racikan_rows = $this->dbtable->get_result("
					SELECT a.*
					FROM smis_frm_bahan_pakai_obat_racikan a LEFT JOIN smis_frm_penjualan_obat_racikan b ON a.id_penjualan_obat_racikan = b.id
					WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.id_penjualan_resep = '" . $row->id . "'
				");
				if ($bahan_obat_racikan_rows != null) {
					foreach ($bahan_obat_racikan_rows as $brr) {
						$hpp_row = $dbtable->get_row("
							SELECT *
							FROM smis_frm_histori_hpp
							WHERE id_obat = '" . $brr->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
							ORDER BY id DESC
							LIMIT 0, 1
						");
						if ($hpp_row != null && $hpp_row->hpp != null)
							$hpp_total += ($hpp_row->hpp * $brr->jumlah);
					}
				}

				// notify hpp dan persediaan :
				$data = array(
					"jenis_akun"	=> "transaction",
					"jenis_data"	=> "persediaan",
					"id_data"		=> $id['id'],
					"entity"		=> "farmasi",
					"service"		=> "get_detail_accounting_hpp_persediaan_penjualan",
					"data"			=> $id['id'],
					"code"			=> "persediaan-farmasi-" . $id['id'],
					"operation"		=> "del",
					"tanggal"		=> $tanggal,
					"uraian"		=> "Persediaan dari No. Penjualan " . $id['id'] . " a.n. " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
					"nilai"			=> $hpp_total
				);
				$service_consumer = new ServiceConsumer(
					$this->dbtable->get_db(),
					"push_notify_accounting",
					$data,
					"accounting"
				);
				$service_consumer->execute();
			}
		}
		if($berhasil) $db->commit();
		else $db->rollback();
		$db->set_autocommit(true);
		$success['id'] = $id['id'];
		$success['success'] = 1;
		$success['message']=$this->getMessage();
		if ($berhasil === false) $success['success'] = 0;
		return $success;
	}
	public function batalDetailResep($id){
		$header = $this->dbtable->select($id);
		$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
		$stok_pakai_obat_jadi_rows = $this->dbtable->get_result("
			SELECT smis_frm_stok_pakai_obat_jadi.*
			FROM smis_frm_stok_pakai_obat_jadi LEFT JOIN smis_frm_penjualan_obat_jadi ON smis_frm_stok_pakai_obat_jadi.id_penjualan_obat_jadi = smis_frm_penjualan_obat_jadi.id
			WHERE smis_frm_stok_pakai_obat_jadi.prop NOT LIKE 'del' AND smis_frm_penjualan_obat_jadi.id_penjualan_resep = '" . $id['id'] . "'
		");
		$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Jadi; Pasien: " . $header->nama_pasien . "; Alamat: " . $header->alamat_pasien . "; No. Resep : " . $header->nomor_resep;
		foreach($stok_pakai_obat_jadi_rows as $spojr) {
			$stok_obat_row = $stok_obat_dbtable->get_row("
				SELECT *
				FROM smis_frm_stok_obat
				WHERE id = '" . $spojr->id_stok_obat . "'
			");
			$stok_obat_data = array();
			$stok_obat_data['sisa'] = $stok_obat_row->sisa + $spojr->jumlah;
			$stok_obat_id['id'] = $stok_obat_row->id;
			$result=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
			if($result<1){
				$this->addMessage(" Obat <strong>".$stok_obat_row->nama_obat." </strong> Gagal Dibatalkan");
				return false;
			}
			
			//logging riwayat stok:
			$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
			$data_riwayat = array();
			$data_riwayat['tanggal'] = date("Y-m-d");
			$data_riwayat['id_stok_obat'] = $stok_obat_row->id;
			$data_riwayat['jumlah_masuk'] = $spojr->jumlah;
			$data_riwayat['sisa'] = $stok_obat_row->sisa + $spojr->jumlah;
			$data_riwayat['keterangan'] = "Stok Batal Digunakan Penjualan Instansi Lain: " . $keterangan;
			global $user;
			$data_riwayat['nama_user'] = $user->getName();
			$result=$riwayat_dbtable->insert($data_riwayat);
			if($result===false){
				$this->addMessage(" Obat <strong>".$stok_obat_row->nama_obat." </strong> Gagal Dicatat di Riwayat");
				return false;
			}
		}

		$obat_jadi_rows = $this->dbtable->get_result("
			SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id['id'] . "'
			GROUP BY id_obat, satuan
		");
		//logging kartu stok dan logging hpp:
		$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
		foreach ($obat_jadi_rows as $ojr) {
			$sisa_row = $this->dbtable->get_row("
				SELECT SUM(a.sisa) AS 'sisa'
				FROM smis_frm_stok_obat a
				WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $ojr->id_obat . "' AND a.satuan = '" . $ojr->satuan . "' AND a.konversi = '1'
			");
			$kartu_stok_data = array(
				"f_id"				=> $id['id'],
				"no_bon"			=> $id['id'],
				"unit"				=> "Penjualan Instansi Lain : Pembatalan, " . $keterangan,
				"id_obat"			=> $ojr->id_obat,
				"kode_obat"			=> $ojr->kode_obat,
				"nama_obat"			=> $ojr->nama_obat,
				"nama_jenis_obat"	=> $ojr->nama_jenis_obat,
				"tanggal"			=> date("Y-m-d"),
				"masuk"				=> $ojr->jumlah,
				"keluar"			=> 0,
				"sisa"				=> $sisa_row->sisa
			);
			$result = $kartu_stok_dbtable->insert($kartu_stok_data);
			if($result===false){
				$this->addMessage(" Obat <strong>".$ojr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
				return false;
			}
			InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $ojr->id_obat, date("Y-m-d H:i:s"), 1, "Pembatalan Penjualan Unit Lain ID : , " . $id['id']);
		}
		
		return true;
	}
	public function saveDetailResep($id){
		$success=true;
		$header = $this->dbtable->select($id);
		//do insert detail resep here:
		$penjualan_obat_jadi_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_penjualan_obat_jadi");
		$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
		$stok_pakai_obat_jadi_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_pakai_obat_jadi");
		$detail_resep = $_POST['detail_penjualan_instansi_lain'];
		foreach($detail_resep as $dresep) {						
			//do insert obat jadi here:
			$obat_jadi_data = array();
			$obat_jadi_data['id_penjualan_resep'] = $id['id'];
			$obat_jadi_data['id_obat'] = $dresep['id_obat'];
			$obat_jadi_data['kode_obat'] = $dresep['kode_obat'];
			$obat_jadi_data['nama_obat'] = $dresep['nama_obat'];
			$obat_jadi_data['nama_jenis_obat'] = $dresep['nama_jenis_obat'];
			$obat_jadi_data['jumlah'] = $dresep['jumlah'];
			$obat_jadi_data['satuan'] = $dresep['satuan'];
			$obat_jadi_data['konversi'] = $dresep['konversi'];
			$obat_jadi_data['satuan_konversi'] = $dresep['satuan_konversi'];
			$obat_jadi_data['harga'] = $dresep['harga'];
			$obat_jadi_data['embalase'] = $dresep['embalase'];
			$obat_jadi_data['tusla'] = $dresep['tusla'];
			$obat_jadi_data['subtotal'] = $dresep['subtotal'];
			$obat_jadi_data['pos'] = $dresep['pos'];
			$obat_jadi_data['markup'] = $dresep['markup'];
			$result=$penjualan_obat_jadi_dbtable->insert($obat_jadi_data);
			
			if($result===false){
				$success=false;
				$this->addMessage(" Obat <strong>".$dresep['nama_obat']." </strong> Gagal Masuk Penjualan Instansi Lain");
				continue;
			}
			
			$id_penjualan_obat_jadi = $penjualan_obat_jadi_dbtable->get_inserted_id();
			//do insert stok pakai obat jadi and update stok obat here:
			$stok_obat_query = "
				SELECT smis_frm_stok_obat.id, smis_frm_stok_obat.sisa
				FROM smis_frm_stok_obat
				WHERE smis_frm_stok_obat.id_obat = '" . $dresep['id_obat'] . "' 
				  AND smis_frm_stok_obat.satuan = '" . $dresep['satuan'] . "' 
				  AND smis_frm_stok_obat.konversi = '" . $dresep['konversi'] . "' 
				  AND smis_frm_stok_obat.satuan_konversi = '" . $dresep['satuan_konversi'] . "'
				  AND smis_frm_stok_obat.sisa > 0
				ORDER BY smis_frm_stok_obat.tanggal_exp ASC
			";
			$stok_obat_rows = $stok_obat_dbtable->get_result($stok_obat_query);
			$jumlah = $dresep['jumlah'] - $dresep['jumlah_lama'];//kalau-kalau update
			foreach($stok_obat_rows as $sor) {
				$sisa_stok = 0;
				$jumlah_stok_pakai = 0;
				if ($sor->sisa >= $jumlah) {
					$sisa_stok = $sor->sisa - $jumlah;
					$jumlah_stok_pakai = $jumlah;
					$jumlah = 0;
				} else {
					$sisa_stok = 0;
					$jumlah_stok_pakai = $sor->sisa;
					$jumlah = $jumlah - $sor->sisa;
				}
				//do update stok obat here:
				$stok_obat_data = array();
				$stok_obat_data['sisa'] = $sisa_stok;
				$stok_obat_id['id'] = $sor->id;
				$keberhasilan_mengambil_stok=$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
				if($keberhasilan_mengambil_stok==0){
					$success=false;
					$this->addMessage(" Stok <strong>".$dresep['nama_obat']."</strong> Gagal diambil ");
					continue;
				}
				
				//logging riwayat stok:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $sor->id;
				$data_riwayat['jumlah_keluar'] = $jumlah_stok_pakai;
				$data_riwayat['sisa'] = $sisa_stok;
				$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Jadi; Apotek: " . $_POST['nama_pasien'] . "; Alamat: " . $_POST['alamat_pasien'] . "; No. Resep : " . $header->nomor_resep;
				$data_riwayat['keterangan'] = "Stok Digunakan Penjualan Instansi Lain: " . $keterangan;
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$result=$riwayat_dbtable->insert($data_riwayat);
				if($result===false){
					$success=false;
					$this->addMessage(" Riwayat <strong>".$dresep['nama_obat']." </strong> Gagal dicatat");
					continue;
				}
				
				//do insert stok pakai obat jadi here:
				$stok_pakai_obat_jadi_data = array();
				$stok_pakai_obat_jadi_data['id_penjualan_obat_jadi'] = $id_penjualan_obat_jadi;
				$stok_pakai_obat_jadi_data['id_stok_obat'] = $sor->id;
				$stok_pakai_obat_jadi_data['jumlah'] = $jumlah_stok_pakai;
				$result=$stok_pakai_obat_jadi_dbtable->insert($stok_pakai_obat_jadi_data);
				if($result===false){
					$success=false;
					$this->addMessage(" Obat <strong>".$dresep['nama_obat']." </strong> Gagal Disimpan di Riwayat Pemakaian Obat Jadi","Pakai Obat Jadi");
					continue;
				}
				if ($jumlah == 0) break;
			}
			if($jumlah>0) {
				$success=false;
				$this->addMessage(" Stok untuk Obat Jadi pada <strong>".$dresep['nama_obat']." </strong> Kurang ".$jumlah." ".$dresep['satuan']);
			}
		}
		
		$obat_jadi_rows = $this->dbtable->get_result("
			SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(jumlah) AS 'jumlah', satuan
			FROM smis_frm_penjualan_obat_jadi
			WHERE prop NOT LIKE 'del' AND id_penjualan_resep = '" . $id['id'] . "'
			GROUP BY id_obat, satuan
		");
		//logging kartu stok dan hpp:
		$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
		$keterangan = "Nomor: " . ArrayAdapter::format("only-digit8", $id['id']) . "; Jenis: Obat Jadi; Pasien: " . $_POST['nama_pasien'] . "; Alamat: " . $_POST['alamat_pasien'] . "; No. Resep : " . $header->nomor_resep;
		foreach ($obat_jadi_rows as $ojr) {
			$sisa_row = $this->dbtable->get_row("
				SELECT SUM(a.sisa) AS 'sisa'
				FROM smis_frm_stok_obat a
				WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $ojr->id_obat . "' AND a.satuan = '" . $ojr->satuan . "' AND a.konversi = '1'
			");
			$kartu_stok_data = array(
				"f_id"				=> $id['id'],
				"no_bon"			=> $id['id'],
				"unit"				=> "Penjualan Instansi Lain : " . $keterangan,
				"id_obat"			=> $ojr->id_obat,
				"kode_obat"			=> $ojr->kode_obat,
				"nama_obat"			=> $ojr->nama_obat,
				"nama_jenis_obat"	=> $ojr->nama_jenis_obat,
				"tanggal"			=> date("Y-m-d"),
				"masuk"				=> 0,
				"keluar"			=> $ojr->jumlah,
				"sisa"				=> $sisa_row->sisa
			);
			$result = $kartu_stok_dbtable->insert($kartu_stok_data);
			if($result===false){
				$this->addMessage(" Obat <strong>".$ojr->nama_obat." </strong> Gagal Dicatat di Kartu Stok");
				return false;
			}
			InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $ojr->id_obat, date("Y-m-d H:i:s"), 0, "Penjualan Unit Lain ID : , " . $id['id']);
		}

		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$data['header'] = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_penjualan_resep
			WHERE id = '" . $id . "'
		");
		$dpal_num = 0;
		$dpal_list = "";
		$dpal_rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_frm_penjualan_obat_jadi
			WHERE id_penjualan_resep = '" .  $id . "' AND prop NOT LIKE 'del'
		");
		$no = 1;
		foreach($dpal_rows as $dpal) {
			$dpal_action = "";
			if (!$_POST['readonly']) {
				$dpal_action = "<div class='btn-group noprint'>" .
									 "<a href='#' onclick='dpenjualan_instansi_lain.edit(" . $dpal_num . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" . 
										 "<i class='icon-edit icon-white'></i>" .
									 "</a>" .
									 "<a href='#' onclick='dpenjualan_instansi_lain.delete(" . $dpal_num . ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" . 
										 "<i class='icon-remove icon-white'></i>" . 
									 "</a>" .
								 "</div>";
			}
			$dpal_list .= "<tr id='dpal_" . $dpal_num . "'>" .
								"<td id='dpal_" . $dpal_num . "_id' style='display: none;'>" . $dpal->id . "</td>" .
								"<td id='dpal_" . $dpal_num . "_id_obat' style='display: none;'>" . $dpal->id_obat . "</td>" .
								"<td id='dpal_" . $dpal_num . "_kode_obat' style='display: none;'>" . $dpal->kode_obat . "</td>" .
								"<td id='dpal_" . $dpal_num . "_nama_jenis_obat' style='display: none;'>" . $dpal->nama_jenis_obat . "</td>" .
								"<td id='dpal_" . $dpal_num . "_jumlah' style='display: none;'>" . $dpal->jumlah . "</td>" .
								"<td id='dpal_" . $dpal_num . "_jumlah_lama' style='display: none;'>" . $dpal->jumlah . "</td>" .
								"<td id='dpal_" . $dpal_num . "_satuan' style='display: none;'>" . $dpal->satuan . "</td>" .
								"<td id='dpal_" . $dpal_num . "_konversi' style='display: none;'>" . $dpal->konversi . "</td>" .
								"<td id='dpal_" . $dpal_num . "_satuan_konversi' style='display: none;'>" . $dpal->satuan_konversi . "</td>" .
								"<td id='dpal_" . $dpal_num . "_nomor'>" . $no++ . "</td>" .
								"<td id='dpal_" . $dpal_num . "_nama_obat'>" . $dpal->nama_obat . "</td>" .
								"<td id='dpal_" . $dpal_num . "_f_jumlah'>" . $dpal->jumlah . " " . $dpal->satuan . "</td>" .
								"<td id='dpal_" . $dpal_num . "_harga'>" . "Rp. " . number_format($dpal->harga, 2, ",", ".") . "</td>" .
								"<td id='dpal_" . $dpal_num . "_embalase'>" . "Rp. " . number_format($dpal->embalase, 2, ",", ".") . "</td>" .
								"<td id='dpal_" . $dpal_num . "_tuslah'>" . "Rp. " . number_format($dpal->tusla, 2, ",", ".") . "</td>" .
								"<td id='dpal_" . $dpal_num . "_subtotal'>" . "Rp. " . number_format($dpal->subtotal, 2, ",", ".") . "</td>" .
								"<td>" .
									$dpal_action .
								"</td>" .
							"</tr>";
			$dpal_num++;
		}
		$data['dpal_num'] = $dpal_num;
		$data['dpal_list'] = $dpal_list;
		return $data;
	}
}
?>