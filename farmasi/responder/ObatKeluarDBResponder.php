<?php
require_once("farmasi/library/InventoryLibrary.php");

class ObatKeluarDBResponder extends DBResponder {
	public function command($command) {
		if ($command != "print_mutasi")
			return parent::command($command);
		$pack = new ResponsePackage();
		if ($command == "print_mutasi") {
			$content = $this->print_mutasi();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}
		return $pack->getPackage();
	}
	public function print_mutasi() {
		$id = $_POST['id'];
		$status = $_POST['status'];
		$row_header = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_obat_keluar
			WHERE id = '" . $id . "'
		");
		$rows_detail = $this->dbtable->get_result("
			SELECT smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.kode_obat, smis_frm_stok_obat.satuan, smis_frm_stok_obat.tanggal_exp, smis_frm_stok_obat_keluar.jumlah, smis_frm_stok_obat.hna
			FROM (smis_frm_stok_obat_keluar LEFT JOIN smis_frm_dobat_keluar ON smis_frm_stok_obat_keluar.id_dobat_keluar = smis_frm_dobat_keluar.id) LEFT JOIN smis_frm_stok_obat ON smis_frm_stok_obat_keluar.id_stok_obat = smis_frm_stok_obat.id
			WHERE smis_frm_dobat_keluar.id_obat_keluar = '" . $id . "'
		");
		$print_data .= "<table width='100%' border='1'>
							<tr>
								<th width='35%' colspan='2'>
									<center>
										PT. ROLAS NUSANTARA MEDIKA</br>
										RSU \"KALIWATES\" JEMBER</br>
										GUDANG OBAT
									</center>
								</th>
								<th width='35%' colspan='2'>
									<center>
										BUKTI KELUAR</br>
										(Surat Pengantar)</br>
										Ke: " . ArrayAdapter::format("unslug", $row_header->unit) . "
									</center>
								</th>
								<th width='30%' colspan='2'>
									<table border='0'>
										<tr>
											<td>Bon</td>
											<td>:</td>
											<td>" . ArrayAdapter::format("digit8",  $row_header->id) . "</td>	
										</tr>
										<tr>
											<td>Tgl</td>
											<td>:</td>
											<td>" . ArrayAdapter::format("date d/m/Y", $row_header->tanggal) . "</td>
										</tr>
										<tr>
											<td colspan='3'>&nbsp;</td>
										</tr>
									</table>
								</th>
							</tr>
							<tr>
								<th>NAMA OBAT</th>
								<th>KODE OBAT</th>
								<th>SAT</th>
								<th>TGL. EXPIRED</th>
								<th>JUMLAH</th>
								<th>HARGA</th>
							</tr>";
		if (count($rows_detail) > 0) {
			foreach($rows_detail as $d) {
				$prefix_kode_obat = substr($d->kode_obat, 0, 3);
				$kode_obat = substr($d->kode_obat, 4);
				if ($prefix_kode_obat == "REG")
					$kode_obat = "309." . $kode_obat;
				else if ($prefix_kode_obat == "JKN")
					$kode_obat = "319." . $kode_obat;
				$print_data .= "<tr>
									<td>" . $d->nama_obat . "</td>
									<td>" . $kode_obat . "</td>
									<td>" . $d->satuan . "</td>
									<td>" . ArrayAdapter::format("date d/m/Y", $d->tanggal_exp) . "</td>
									<td>" . ArrayAdapter::format("number", $d->jumlah) . "</td>
									<td>" . ArrayAdapter::format("money Rp. ", $d->hna) . "</td>
								</tr>";
			}
		} else {
			$print_data .= "<tr>
								<td colspan='6' align='center'><i>Tidak terdapat data detail mutasi obat</i></td>
							</tr>";
		}
		$print_data .= "</table>";
		$print_data .= "<br/>";
		$print_data .= "<table border='0' class='united' width='100%'>
							<tr>
								<td align='center'>Gudang Obat</td>
								<td>&Tab;</td>
								<td align='center'>Penerima</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align='center'><b>(___________________)</b></td>
								<td>&Tab;</td>
								<td align='center'><b>(___________________)</b></td>
							</tr>
						</table>";
		return $print_data;
	}
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			$header_data['tanggal'] = date("Y-m-d");
			$result = $this->dbtable->insert($header_data);
			$id['id'] = $this->dbtable->get_inserted_id();
			$success['type'] = "insert";
			if (isset($_POST['detail'])) {
				//do insert detail here:
				$dobat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_keluar");
				$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					$detail_data = array();
					$detail_data['id_obat_keluar'] = $id['id'];
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['stok_entri'] = $d['stok_entri'];
					$detail_data['harga_ma'] = $d['harga_ma'];
					$detail_data['jumlah_diminta'] = $d['jumlah_diminta'];
					$detail_data['jumlah'] = $d['jumlah'];
					$detail_data['satuan'] = $d['satuan'];
					$detail_data['konversi'] = $d['konversi'];
					$detail_data['satuan_konversi'] = $d['satuan_konversi'];
					$dobat_keluar_dbtable->insert($detail_data);
					
					//kartu gudang induk:
					$ks_data = array();
					$ks_data['f_id'] = $dobat_keluar_dbtable->get_inserted_id();
					$ks_data['unit'] = ArrayAdapter::format("unslug", $header_data['unit']);
					$ks_data['no_bon'] = ArrayAdapter::format("unslug", $id['id']);
					$ks_data['id_obat'] = $d['id_obat'];
					$ks_data['kode_obat'] = $d['kode_obat'];
					$ks_data['nama_obat'] = $d['nama_obat'];
					$ks_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$ks_data['tanggal'] = $header_data['tanggal'];
					$ks_data['masuk'] = 0;
					$ks_data['keluar'] = $d['jumlah'];
					$ks_row = $ks_dbtable->get_row("
						SELECT SUM(b.sisa) AS 'jumlah'
						FROM smis_frm_dobat_masuk a LEFT JOIN smis_frm_stok_obat b ON a.id = b.id_dobat_masuk
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $d['id_obat'] . "'
					");
					$ks_data['sisa'] = $ks_row->jumlah - $d['jumlah'];
					$ks_dbtable->insert($ks_data);
					
					$id_detail = $dobat_keluar_dbtable->get_inserted_id();
					$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
					$stok_query = "
						SELECT smis_frm_stok_obat.id, v_dobat_masuk.id_obat, smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.nama_jenis_obat, smis_frm_stok_obat.sisa, smis_frm_stok_obat.satuan, smis_frm_stok_obat.konversi, smis_frm_stok_obat.satuan_konversi, smis_frm_stok_obat.label
						FROM smis_frm_stok_obat LEFT JOIN (
							SELECT label, id, id_obat
							FROM smis_frm_dobat_masuk
						) v_dobat_masuk ON smis_frm_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_frm_stok_obat.label = v_dobat_masuk.label
						WHERE smis_frm_stok_obat.prop NOT LIKE 'del' AND v_dobat_masuk.id_obat = '" . $d['id_obat'] . "' AND smis_frm_stok_obat.satuan = '" . $d['satuan'] . "' AND smis_frm_stok_obat.konversi = '" . $d['konversi'] . "' AND smis_frm_stok_obat.satuan_konversi = '" . $d['satuan_konversi'] . "' AND smis_frm_stok_obat.sisa > 0
						ORDER BY FIELD(smis_frm_stok_obat.label, 'reguler', 'sito'), smis_frm_stok_obat.tanggal_exp ASC
					";
					$stok_rows = $stok_dbtable->get_result($stok_query);
					$jumlah = $detail_data['jumlah'];
					foreach($stok_rows as $sr) {
						$sisa_stok = 0;
						$jumlah_stok_keluar = 0;
						if ($sr->sisa >= $jumlah) {
							$sisa_stok = $sr->sisa - $jumlah;
							$jumlah_stok_keluar = $jumlah;
							$jumlah = 0;
						} else {
							$sisa_stok = 0;
							$jumlah_stok_keluar = $sr->sisa;
							$jumlah = $jumlah - $sr->sisa;
						}
						//do update stok obat here:
						$stok_data = array();
						$stok_data['sisa'] = $sisa_stok;
						$stok_id['id'] = $sr->id;
						$stok_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok obat:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $sr->id;
						$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
						$data_riwayat['sisa'] = $sisa_stok;
						$data_riwayat['keterangan'] = "Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
						//do insert stok obat keluar here:
						$stok_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar");
						$stok_keluar_data = array();
						$stok_keluar_data['id_dobat_keluar'] = $id_detail;
						$stok_keluar_data['id_stok_obat'] = $sr->id;
						$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
						$stok_keluar_dbtable->insert($stok_keluar_data);
						if ($jumlah == 0) break;
					}
					/// logging hpp :
					InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $d['id_obat'], $header_data['tanggal'], 0, "Obat Keluar Unit ID : " . $id['id']);
				}
			}
		} else {
			//do update header here:
			$header_data = array();
			$header_data['status'] = $_POST['status'];
			$header_data['keterangan'] = $_POST['keterangan'];
			$result = $this->dbtable->update($header_data, $id);
			$success['type'] = "update";
			if (isset($_POST['detail'])) {
				//do update detail here:
				$dobat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_keluar");
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					if ($d['cmd'] == "insert") {
						$detail_data = array();
						$detail_data['id_obat_keluar'] = $id['id'];
						$detail_data['id_obat'] = $d['id_obat'];
						$detail_data['kode_obat'] = $d['kode_obat'];
						$detail_data['nama_obat'] = $d['nama_obat'];
						$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
						$detail_data['stok_entri'] = $d['stok_entri'];
						$detail_data['harga_ma'] = $d['harga_ma'];
						$detail_data['jumlah_diminta'] = $d['jumlah_diminta'];
						$detail_data['jumlah'] = $d['jumlah'];
						$detail_data['satuan'] = $d['satuan'];
						$detail_data['konversi'] = $d['konversi'];
						$detail_data['satuan_konversi'] = $d['satuan_konversi'];
						$dobat_keluar_dbtable->insert($detail_data);
						$id_detail = $dobat_keluar_dbtable->get_inserted_id();
						$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
						$stok_query = "
							SELECT smis_frm_stok_obat.id, v_dobat_masuk.id_obat, smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.nama_jenis_obat, smis_frm_stok_obat.sisa, smis_frm_stok_obat.satuan, smis_frm_stok_obat.konversi, smis_frm_stok_obat.satuan_konversi, smis_frm_stok_obat.label
							FROM smis_frm_stok_obat LEFT JOIN (
								SELECT label, id, id_obat
								FROM smis_frm_dobat_masuk
							) v_dobat_masuk ON smis_frm_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_frm_stok_obat.label = v_dobat_masuk.label
							WHERE smis_frm_stok_obat.prop NOT LIKE 'del' AND v_dobat_masuk.id_obat = '" . $d['id_obat'] . "' AND smis_frm_stok_obat.satuan = '" . $d['satuan'] . "' AND smis_frm_stok_obat.konversi = '" . $d['konversi'] . "' AND smis_frm_stok_obat.satuan_konversi = '" . $d['satuan_konversi'] . "' AND smis_frm_stok_obat.sisa > 0
							ORDER BY FIELD(smis_frm_stok_obat.label, 'reguler', 'sito'), smis_frm_stok_obat.tanggal_exp ASC
						";
						$stok_rows = $stok_dbtable->get_result($stok_query);
						$jumlah = $detail_data['jumlah'];
						foreach($stok_rows as $sr) {
							$sisa_stok = 0;
							$jumlah_stok_keluar = 0;
							if ($sr->sisa >= $jumlah) {
								$sisa_stok = $sr->sisa - $jumlah;
								$jumlah_stok_keluar = $jumlah;
								$jumlah = 0;
							} else {
								$sisa_stok = 0;
								$jumlah_stok_keluar = $sr->sisa;
								$jumlah = $jumlah - $sr->sisa;
							}
							//do update stok obat here:
							$stok_data = array();
							$stok_data['sisa'] = $sisa_stok;
							$stok_id['id'] = $sr->id;
							$stok_dbtable->update($stok_data, $stok_id);
							//logging riwayat stok obat:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $sr->id;
							$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
							$data_riwayat['sisa'] = $sisa_stok;
							$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
							//do insert stok obat keluar here:
							$stok_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar");
							$stok_keluar_data = array();
							$stok_keluar_data['id_dobat_keluar'] = $id_detail;
							$stok_keluar_data['id_stok_obat'] = $sr->id;
							$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
							$stok_keluar_dbtable->insert($stok_keluar_data);
							if ($jumlah == 0) break;
						}
					} else if ($d['cmd'] == "update") {
						$detail_data = array();
						$detail_data['jumlah'] = $d['jumlah'];
						$detail_id['id'] = $d['id'];
						$dobat_keluar_dbtable->update($detail_data, $detail_id);
						if ($d['jumlah'] > $d['jumlah_lama']) {
							//jumlah baru > jumlah lama:
							$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
							$stok_query = "
								SELECT smis_frm_stok_obat.id, v_dobat_masuk.id_obat, smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.nama_jenis_obat, smis_frm_stok_obat.sisa, smis_frm_stok_obat.satuan, smis_frm_stok_obat.konversi, smis_frm_stok_obat.satuan_konversi, smis_frm_stok_obat.label
								FROM smis_frm_stok_obat LEFT JOIN (
									SELECT label, id, id_obat
									FROM smis_frm_dobat_masuk
								) v_dobat_masuk ON smis_frm_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_frm_stok_obat.label = v_dobat_masuk.label
								WHERE smis_frm_stok_obat.prop NOT LIKE 'del' AND v_dobat_masuk.id_obat = '" . $d['id_obat'] . "' AND smis_frm_stok_obat.satuan = '" . $d['satuan'] . "' AND smis_frm_stok_obat.konversi = '" . $d['konversi'] . "' AND smis_frm_stok_obat.satuan_konversi = '" . $d['satuan_konversi'] . "' AND smis_frm_stok_obat.sisa > 0
								ORDER BY FIELD(smis_frm_stok_obat.label, 'reguler', 'sito'), smis_frm_stok_obat.tanggal_exp ASC
							";
							$stok_rows = $stok_dbtable->get_result($stok_query);
							$jumlah = $d['jumlah'] - $d['jumlah_lama'];
							foreach($stok_rows as $sr) {
								$sisa_stok = 0;
								$jumlah_stok_keluar = 0;
								if ($sr->sisa >= $jumlah) {
									$sisa_stok = $sr->sisa - $jumlah;
									$jumlah_stok_keluar = $jumlah;
									$jumlah = 0;
								} else {
									$sisa_stok = 0;
									$jumlah_stok_keluar = $sr->sisa;
									$jumlah = $jumlah - $sr->sisa;
								}
								//do update stok obat here:
								$stok_data = array();
								$stok_data['sisa'] = $sisa_stok;
								$stok_id['id'] = $sr->id;
								$stok_dbtable->update($stok_data, $stok_id);
								//logging riwayat stok obat:
								$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
								$data_riwayat = array();
								$data_riwayat['tanggal'] = date("Y-m-d");
								$data_riwayat['id_stok_obat'] = $sr->id;
								$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
								$data_riwayat['sisa'] = $sisa_stok;
								$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
								global $user;
								$data_riwayat['nama_user'] = $user->getName();
								$riwayat_dbtable->insert($data_riwayat);
								//do insert stok obat keluar here:
								$stok_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar");
								$stok_keluar_data = array();
								$stok_keluar_data['id_dobat_keluar'] = $d['id'];
								$stok_keluar_data['id_stok_obat'] = $sr->id;
								$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
								$stok_keluar_dbtable->insert($stok_keluar_data);
								if ($jumlah == 0) break;
							}
						} else if ($d['jumlah'] < $d['jumlah_lama']) {
							//jumlah baru < jumlah lama:
							$stok_obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar");
							$stok_keluar_query = "
								SELECT *
								FROM smis_frm_stok_obat_keluar
								WHERE id_dobat_keluar = '" . $d['id'] . "'
								ORDER BY id DESC
							";
							$stok_keluar = $stok_obat_keluar_dbtable->get_result($stok_keluar_query);
							$jumlah = $d['jumlah_lama'] - $d['jumlah'];
							foreach($stok_keluar as $sk) {
								$simpan_stok_keluar = 0;
								$simpan_stok = 0;
								if ($jumlah >= $sk->jumlah) {
									$jumlah = $jumlah - $sk->jumlah;
									$simpan_stok = $sk->jumlah;
									$simpan_stok_keluar = 0;
								} else {
									$simpan_stok_keluar = $sk->jumlah - $jumlah;
									$simpan_stok = $jumlah;
									$jumlah = 0;
								}
								$stok_keluar_data = array();
								$stok_keluar_data['jumlah'] = $simpan_stok_keluar;
								if ($simpan_stok_keluar == 0)
									$stok_keluar_data['prop'] = "del";
								$stok_keluar_id['id'] = $sk->id;
								$stok_obat_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
								$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
								$stok_query = "
									SELECT *
									FROM smis_frm_stok_obat
									WHERE id = '" . $sk->id_stok_obat . "'
								";
								$stok_obat_row = $stok_obat_dbtable->get_row($stok_query);
								$stok_data = array();
								$stok_data['sisa'] = $stok_obat_row->sisa + $simpan_stok;
								$stok_id['id'] = $sk->id_stok_obat;
								$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
								$stok_dbtable->update($stok_data, $stok_id);
								//logging riwayat stok obat:
								$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
								$data_riwayat = array();
								$data_riwayat['tanggal'] = date("Y-m-d");
								$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
								$data_riwayat['jumlah_masuk'] = $simpan_stok;
								$data_riwayat['sisa'] = $stok_obat_row->sisa + $simpan_stok;
								$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
								global $user;
								$data_riwayat['nama_user'] = $user->getName();
								$riwayat_dbtable->insert($data_riwayat);
								if ($jumlah == 0) break;
							}
						} //end of update
					} else if($d['cmd'] == "delete") {
						$detail_data = array();
						$detail_data['prop'] = "del";
						$detail_id['id'] = $d['id'];
						$dobat_keluar_dbtable->update($detail_data, $detail_id);
						$stok_obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar");
						$stok_keluar_query = "
							SELECT *
							FROM smis_frm_stok_obat_keluar
							WHERE id_dobat_keluar = '" . $d['id'] . "' AND smis_frm_stok_obat_keluar.prop NOT LIKE 'del'
							ORDER BY id DESC
						";
						$stok_keluar = $stok_obat_keluar_dbtable->get_result($stok_keluar_query);
						foreach($stok_keluar as $sk) {
							$stok_keluar_data = array();
							$stok_keluar_data['jumlah'] = 0;
							$stok_keluar_data['prop'] = "del";
							$stok_keluar_id['id'] = $sk->id;
							$stok_obat_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
							$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
							$stok_query = "
								SELECT *
								FROM smis_frm_stok_obat
								WHERE id = '" . $sk->id_stok_obat . "'
							";
							$stok_obat_row = $stok_obat_dbtable->get_row($stok_query);
							$stok_data = array();
							$stok_data['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
							$stok_id['id'] = $sk->id_stok_obat;
							$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
							$stok_dbtable->update($stok_data, $stok_id);
							//logging riwayat stok obat:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
							$data_riwayat['jumlah_masuk'] = $sk->jumlah;
							$data_riwayat['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
							$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . ArrayAdapter::format("unslug", $_POST['unit']);
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
						}
					}
				}
			}
		}
		$success['id'] = $id['id'];
		$success['success'] = 1;
		if ($result === false) $success['success'] = 0;
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$header_row = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_obat_keluar
			WHERE id = '" . $id . "'
		");
		$data['header'] = $header_row;
		$dobat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_keluar");
		$data['detail'] = $dobat_keluar_dbtable->get_result("
			SELECT *
			FROM smis_frm_dobat_keluar
			WHERE id_obat_keluar = '" . $id . "' AND prop NOT LIKE 'del'
		");
		return $data;
	}
	public function delete() {
		$id['id'] = $_POST['id'];
		if ($this->dbtable->isRealDelete()) {
			$result = $this->dbtable->delete(null,$id);
		} else {
			$data['prop'] = "del";
			$result = $this->dbtable->update($data, $id);
		}
		$stok_obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar");
		$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
		$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
		$stok_keluar_query = "
			SELECT smis_frm_stok_obat_keluar.*, smis_frm_obat_keluar.unit
			FROM (smis_frm_stok_obat_keluar LEFT JOIN smis_frm_dobat_keluar ON smis_frm_stok_obat_keluar.id_dobat_keluar = smis_frm_dobat_keluar.id) LEFT JOIN smis_frm_obat_keluar ON smis_frm_dobat_keluar.id_obat_keluar = smis_frm_obat_keluar.id
			WHERE smis_frm_obat_keluar.id = '" . $id['id'] . "' AND smis_frm_stok_obat_keluar.prop NOT LIKE 'del'
			ORDER BY smis_frm_stok_obat_keluar.id DESC
		";
		$stok_keluar = $this->dbtable->get_result($stok_keluar_query);
		foreach($stok_keluar as $sk) {
			$stok_keluar_data = array();
			$stok_keluar_data['prop'] = "del";
			$stok_keluar_id['id'] = $sk->id;
			$stok_obat_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
			$stok_query = "
				SELECT *
				FROM smis_frm_stok_obat
				WHERE id = '" . $sk->id_stok_obat . "'
			";
			$stok_obat_row =  $this->dbtable->get_row($stok_query);
			$stok_data = array();
			$stok_data['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
			$stok_id['id'] = $sk->id_stok_obat;
			$stok_obat_dbtable->update($stok_data, $stok_id);
			//logging riwayat stok obat:
			$data_riwayat = array();
			$data_riwayat['tanggal'] = date("Y-m-d");
			$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
			$data_riwayat['jumlah_masuk'] = $sk->jumlah;
			$data_riwayat['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
			$data_riwayat['keterangan'] = "Pembatalan Stok Keluar ke " . ArrayAdapter::format("unslug", $sk->unit);
			global $user;
			$data_riwayat['nama_user'] = $user->getName();
			$riwayat_dbtable->insert($data_riwayat);
		}

		//kartu gudang induk:
		$dok_rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_frm_dobat_keluar
			WHERE id_obat_keluar = '" . $id['id'] . "' AND prop NOT LIKE 'del'
		");
		if ($dok_rows != null) {
			$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
			$header_row = $this->dbtable->get_row("
				SELECT *
				FROM smis_frm_obat_keluar
				WHERE id = '" . $id['id'] . "'
			");
			foreach ($dok_rows as $dr) {	
				$ks_data = array();
				$ks_data['f_id'] = $id['id'];
				$ks_data['unit'] = "Pembatalan Mutasi ke " . ArrayAdapter::format("unslug", $header_row->unit);
				$ks_data['no_bon'] = $id['id'];
				$ks_data['id_obat'] = $dr->id_obat;
				$ks_data['kode_obat'] = $dr->kode_obat;
				$ks_data['nama_obat'] = $dr->nama_obat;
				$ks_data['nama_jenis_obat'] = $dr->nama_jenis_obat;
				$ks_data['tanggal'] = date("Y-m-d");
				$ks_data['masuk'] = $dr->jumlah;
				$ks_data['keluar'] = 0;	
				$ks_row = $ks_dbtable->get_row("
					SELECT SUM(sisa) AS 'jumlah'
					FROM smis_frm_stok_obat
					WHERE prop NOT LIKE 'del' AND id_obat = '" . $dr->id_obat . "'
				");
				$ks_data['sisa'] = $ks_row->jumlah + $dr->jumlah;
				$ks_dbtable->insert($ks_data);

				/// logging hpp :
				InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $dr->id_obat, $header_row->tanggal, 1, "Pembatalan Obat Keluar Unit ID : " . $id['id']);
			}
		}

		$success['success'] = 1;
		$success['id'] = $_POST['id'];
		if ($result === 'false') $success['success'] = 0;
		return $success;
	}
}
?>