<?php
	class PerbaikanHargaDBResponder extends DBResponder {
		public function save() {
			$data = $this->postToArray();
			$id['id'] = $_POST['id'];
			$result = $this->dbtable->update($data, $id);
			
			$dobat_masuk_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_masuk");
			$dobat_data = array();
			$dobat_id['id'] = $_POST['id_dobat_masuk'];
			$dobat_data = array();
			$dobat_data['hna'] = $_POST['hna'];
			$dobat_masuk_dbtable->update($dobat_data, $dobat_id);
			
			$success['type'] = 'update';
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
	}
?>