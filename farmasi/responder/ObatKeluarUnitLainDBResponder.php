<?php
require_once("farmasi/library/InventoryLibrary.php");

class ObatKeluarUnitLainDBResponder extends DBResponder {
	public function command($command) {
		if ($command != "print_mutasi")
			return parent::command($command);
		$pack = new ResponsePackage();
		if ($command == "print_mutasi") {
			$content = $this->print_mutasi();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
		}
		return $pack->getPackage();
	}
	public function print_mutasi() {
		$id = $_POST['id'];
		$row_header = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_obat_keluar_unit_lain
			WHERE id = '" . $id . "'
		");
		$rows_detail = $this->dbtable->get_result("
			SELECT *
			FROM smis_frm_dobat_keluar_unit_lain
			WHERE id_obat_keluar_unit_lain = '" . $id . "'
		");
		$print_data = "";
		$print_data .= "<center><strong>NOTA MUTASI OBAT KE KEBUN NO. " . ArrayAdapter::format("only-digit8", $id) . "</strong></center><br/>";
		$print_data .= "<table border='0'>
							<tr>
								<td>Unit Pengirim</td>
								<td>:</td>
								<td>" . ArrayAdapter::format("unslug", "gudang_farmasi") . "</td>
							</tr>
							<tr>
								<td>Kebun Penerima</td>
								<td>:</td>
								<td>" . $row_header->nama_unit . "</td>
							</tr>
							<tr>
								<td>Tanggal</td>
								<td>:</td>
								<td>" . ArrayAdapter::format("date d M Y", $row_header->tanggal) . "</td>
							</tr>
							<tr>
								<td colspan='3'>&Tab;</td>
							</tr>
						</table>";
		$print_data .= "<table border='1'>
							<tr>
								<th>No.</th>
								<th>Nama Obat</th>
								<th>Jenis Obat</th>
								<th>Jumlah</th>
							</tr>";
		$no = 1;
		if (count($rows_detail) > 0) {
			foreach($rows_detail as $d) {
				$print_data .= "<tr>
									<td>" . $no++ . "</td>
									<td>" . $d->nama_obat . "</td>
									<td>" . $d->nama_jenis_obat . "</td>
									<td>" . $d->jumlah . " " . $d->satuan . "</td>
								</tr>";
				$detail_no = 'a';
				$rows_stok_keluar = $this->dbtable->get_result("
					SELECT smis_frm_stok_obat_keluar_unit_lain.id, smis_frm_stok_obat.tanggal_exp, smis_frm_stok_obat.hna, smis_frm_stok_obat_keluar_unit_lain.jumlah, smis_frm_stok_obat.satuan
					FROM smis_frm_stok_obat_keluar_unit_lain LEFT JOIN smis_frm_stok_obat ON smis_frm_stok_obat_keluar_unit_lain.id_stok_obat = smis_frm_stok_obat.id
					WHERE smis_frm_stok_obat_keluar_unit_lain.id_dobat_keluar_unit_lain = '" . $d->id . "'
				");
				foreach($rows_stok_keluar as $dsk) {
					$print_data .= "<tr>
										<td></td>
										<td colspan='2'>" . $detail_no++ . ". ED. " . ArrayAdapter::format("date d/m/Y", $dsk->tanggal_exp) . " - " . ArrayAdapter::format("only-money Rp. ", $dsk->hna) . " : " . $dsk->jumlah . " " . $dsk->satuan . "</td>
										<td></td>
									</tr>";
				}
			}
		} else {
			$print_data .= "<tr>
								<td colspan='4' align='center'><i>Tidak terdapat data detail mutasi obat</i></td>
							</tr>";
		}
		$print_data .= "</table><br/>";
		global $user;
		$print_data .= "<table border='0' align='center'>
							<tr>
								<td align='center'>Jember, " . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align='center'>PETUGAS GUDANG FARMASI</td>
								<td>&Tab;</td>
								<td align='center'>PETUGAS " . $row_header->nama_unit . "</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&Tab;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align='center'><b>" . $user->getNameOnly() . "</b></td>
								<td>&Tab;</td>
								<td align='center'>(_____________________)</td>
							</tr>
						</table>";
		return $print_data;
	}
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			// $header_data['tanggal'] = date("Y-m-d h:i:s");
			$result = $this->dbtable->insert($header_data);
			$id['id'] = $this->dbtable->get_inserted_id();
			$success['type'] = "insert";
			if (isset($_POST['detail'])) {
				//do insert detail here:
				$dobat_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_keluar_unit_lain");
				$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					$detail_data = array();
					$detail_data['id_obat_keluar_unit_lain'] = $id['id'];
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['stok_entri'] = $d['stok_entri'];
					$detail_data['jumlah_diminta'] = $d['jumlah_diminta'];
					$detail_data['jumlah'] = $d['jumlah'];
					$detail_data['satuan'] = $d['satuan'];
					$detail_data['konversi'] = $d['konversi'];
					$detail_data['satuan_konversi'] = $d['satuan_konversi'];
					$detail_data['harga_ma'] = $d['harga_ma'];
					$dobat_keluar_unit_lain_dbtable->insert($detail_data);

					//kartu gudang induk:
					$ks_data = array();
					$ks_data['f_id'] = $dobat_keluar_unit_lain_dbtable->get_inserted_id();
					$ks_data['unit'] = ArrayAdapter::format("unslug", $header_data['nama_unit']);
					$ks_data['no_bon'] = ArrayAdapter::format("unslug", $id['id']);
					$ks_data['id_obat'] = $d['id_obat'];
					$ks_data['kode_obat'] = $d['kode_obat'];
					$ks_data['nama_obat'] = $d['nama_obat'];
					$ks_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$ks_data['tanggal'] = $header_data['tanggal'];
					$ks_data['masuk'] = 0;
					$ks_data['keluar'] = $d['jumlah'];
					$ks_row = $ks_dbtable->get_row("
						SELECT SUM(b.sisa) AS 'jumlah'
						FROM smis_frm_dobat_masuk a LEFT JOIN smis_frm_stok_obat b ON a.id = b.id_dobat_masuk
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $d['id_obat'] . "'
					");
					$ks_data['sisa'] = $ks_row->jumlah - $d['jumlah'];
					$ks_dbtable->insert($ks_data);

					$id_detail = $dobat_keluar_unit_lain_dbtable->get_inserted_id();
					$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
					$stok_query = "
						SELECT id, id_obat, nama_obat, nama_jenis_obat, sisa, satuan, konversi, satuan_konversi, label
						FROM smis_frm_stok_obat
						WHERE prop NOT LIKE 'del' AND id_obat = '" . $d['id_obat'] . "' AND satuan = '" . $d['satuan'] . "' AND konversi = '" . $d['konversi'] . "' AND satuan_konversi = '" . $d['satuan_konversi'] . "' AND sisa > 0
						ORDER BY FIELD(label, 'reguler', 'sito'), tanggal_exp ASC
					";
					$stok_rows = $stok_dbtable->get_result($stok_query);
					$jumlah = $detail_data['jumlah'];
					foreach($stok_rows as $sr) {
						$sisa_stok = 0;
						$jumlah_stok_keluar = 0;
						if ($sr->sisa >= $jumlah) {
							$sisa_stok = $sr->sisa - $jumlah;
							$jumlah_stok_keluar = $jumlah;
							$jumlah = 0;
						} else {
							$sisa_stok = 0;
							$jumlah_stok_keluar = $sr->sisa;
							$jumlah = $jumlah - $sr->sisa;
						}
						//do update stok obat here:
						$stok_data = array();
						$stok_data['sisa'] = $sisa_stok;
						$stok_id['id'] = $sr->id;
						$stok_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok obat:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $sr->id;
						$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
						$data_riwayat['sisa'] = $sisa_stok;
						$data_riwayat['keterangan'] = "Stok Keluar ke " . $_POST['nama_unit'];
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
						//do insert stok obat keluar here:
						$stok_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar_unit_lain");
						$stok_keluar_data = array();
						$stok_keluar_data['id_dobat_keluar_unit_lain'] = $id_detail;
						$stok_keluar_data['id_stok_obat'] = $sr->id;
						$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
						$stok_keluar_dbtable->insert($stok_keluar_data);
						if ($jumlah == 0) break;
					}
					/// logging hpp :
					InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $d['id_obat'], $header_data['tanggal'], 0, "Obat Keluar Unit Lain ID : " . $id['id']);
				}
			}
		} else {
			//do update header here:
			$header_data = array();
			$header_data['status'] = $_POST['status'];
			$header_data['keterangan'] = $_POST['keterangan'];
			$result = $this->dbtable->update($header_data, $id);
			$success['type'] = "update";
			if (isset($_POST['detail'])) {
				//do update detail here:
				$dobat_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_keluar_unit_lain");
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					if ($d['cmd'] == "insert") {
						$detail_data = array();
						$detail_data['id_obat_keluar_unit_lain'] = $id['id'];
						$detail_data['id_obat'] = $d['id_obat'];
						$detail_data['kode_obat'] = $d['kode_obat'];
						$detail_data['nama_obat'] = $d['nama_obat'];
						$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
						$detail_data['stok_entri'] = $d['stok_entri'];
						$detail_data['jumlah_diminta'] = $d['jumlah_diminta'];
						$detail_data['jumlah'] = $d['jumlah'];
						$detail_data['satuan'] = $d['satuan'];
						$detail_data['konversi'] = $d['konversi'];
						$detail_data['satuan_konversi'] = $d['satuan_konversi'];
						$detail_data['harga_ma'] = $d['harga_ma'];
						$dobat_keluar_unit_lain_dbtable->insert($detail_data);
						$id_detail = $dobat_keluar_unit_lain_dbtable->get_inserted_id();
						$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
						$stok_query = "
							SELECT id, id_obat, nama_obat, nama_jenis_obat, sisa, satuan, konversi, satuan_konversi, label
							FROM smis_frm_stok_obat
							WHERE prop NOT LIKE 'del' AND id_obat = '" . $d['id_obat'] . "' AND satuan = '" . $d['satuan'] . "' AND konversi = '" . $d['konversi'] . "' AND satuan_konversi = '" . $d['satuan_konversi'] . "' AND sisa > 0
							ORDER BY FIELD(label, 'reguler', 'sito'), tanggal_exp ASC
						";
						$stok_rows = $stok_dbtable->get_result($stok_query);
						$jumlah = $detail_data['jumlah'];
						foreach($stok_rows as $sr) {
							$sisa_stok = 0;
							$jumlah_stok_keluar = 0;
							if ($sr->sisa >= $jumlah) {
								$sisa_stok = $sr->sisa - $jumlah;
								$jumlah_stok_keluar = $jumlah;
								$jumlah = 0;
							} else {
								$sisa_stok = 0;
								$jumlah_stok_keluar = $sr->sisa;
								$jumlah = $jumlah - $sr->sisa;
							}
							//do update stok obat here:
							$stok_data = array();
							$stok_data['sisa'] = $sisa_stok;
							$stok_id['id'] = $sr->id;
							$stok_dbtable->update($stok_data, $stok_id);
							//logging riwayat stok obat:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $sr->id;
							$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
							$data_riwayat['sisa'] = $sisa_stok;
							$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . $_POST['nama_unit'];
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
							//do insert stok obat keluar here:
							$stok_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar_unit_lain");
							$stok_keluar_data = array();
							$stok_keluar_data['id_dobat_keluar_unit_lain'] = $id_detail;
							$stok_keluar_data['id_stok_obat'] = $sr->id;
							$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
							$stok_keluar_unit_lain_dbtable->insert($stok_keluar_data);
							if ($jumlah == 0) break;
						}
					} else if ($d['cmd'] == "update") {
						$detail_data = array();
						$detail_data['jumlah'] = $d['jumlah'];
						$detail_id['id'] = $d['id'];
						$dobat_keluar_unit_lain_dbtable->update($detail_data, $detail_id);
						if ($d['jumlah'] > $d['jumlah_lama']) {
							//jumlah baru > jumlah lama:
							$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
							$stok_query = "
								SELECT id, id_obat, nama_obat, nama_jenis_obat, sisa, satuan, konversi, satuan_konversi, label
								FROM smis_frm_stok_obat
								WHERE prop NOT LIKE 'del' AND id_obat = '" . $d['id_obat'] . "' AND satuan = '" . $d['satuan'] . "' AND konversi = '" . $d['konversi'] . "' AND satuan_konversi = '" . $d['satuan_konversi'] . "' AND sisa > 0
								ORDER BY FIELD(label, 'reguler', 'sito'), tanggal_exp ASC
							";
							$stok_rows = $stok_dbtable->get_result($stok_query);
							$jumlah = $d['jumlah'] - $d['jumlah_lama'];
							foreach($stok_rows as $sr) {
								$sisa_stok = 0;
								$jumlah_stok_keluar = 0;
								if ($sr->sisa >= $jumlah) {
									$sisa_stok = $sr->sisa - $jumlah;
									$jumlah_stok_keluar = $jumlah;
									$jumlah = 0;
								} else {
									$sisa_stok = 0;
									$jumlah_stok_keluar = $sr->sisa;
									$jumlah = $jumlah - $sr->sisa;
								}
								//do update stok obat here:
								$stok_data = array();
								$stok_data['sisa'] = $sisa_stok;
								$stok_id['id'] = $sr->id;
								$stok_dbtable->update($stok_data, $stok_id);
								//logging riwayat stok obat:
								$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
								$data_riwayat = array();
								$data_riwayat['tanggal'] = date("Y-m-d");
								$data_riwayat['id_stok_obat'] = $sr->id;
								$data_riwayat['jumlah_keluar'] = $jumlah_stok_keluar;
								$data_riwayat['sisa'] = $sisa_stok;
								$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . $_POST['nama_unit'];
								global $user;
								$data_riwayat['nama_user'] = $user->getName();
								$riwayat_dbtable->insert($data_riwayat);
								//do insert stok obat keluar here:
								$stok_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar_unit_lain");
								$stok_keluar_data = array();
								$stok_keluar_data['id_dobat_keluar_unit_lain'] = $d['id'];
								$stok_keluar_data['id_stok_obat'] = $sr->id;
								$stok_keluar_data['jumlah'] = $jumlah_stok_keluar;
								$stok_keluar_unit_lain_dbtable->insert($stok_keluar_data);
								if ($jumlah == 0) break;
							}
						} else if ($d['jumlah'] < $d['jumlah_lama']) {
							//jumlah baru < jumlah lama:
							$stok_obat_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar_unit_lain");
							$stok_keluar_query = "
								SELECT *
								FROM smis_frm_stok_obat_keluar_unit_lain
								WHERE id_dobat_keluar_unit_lain = '" . $d['id'] . "'
								ORDER BY id DESC
							";
							$stok_keluar = $stok_obat_keluar_unit_lain_dbtable->get_result($stok_keluar_query);
							$jumlah = $d['jumlah_lama'] - $d['jumlah'];
							foreach($stok_keluar as $sk) {
								$simpan_stok_keluar = 0;
								$simpan_stok = 0;
								if ($jumlah >= $sk->jumlah) {
									$jumlah = $jumlah - $sk->jumlah;
									$simpan_stok = $sk->jumlah;
									$simpan_stok_keluar = 0;
								} else {
									$simpan_stok_keluar = $sk->jumlah - $jumlah;
									$simpan_stok = $jumlah;
									$jumlah = 0;
								}
								$stok_keluar_data = array();
								$stok_keluar_data['jumlah'] = $simpan_stok_keluar;
								if ($simpan_stok_keluar == 0)
									$stok_keluar_data['prop'] = "del";
								$stok_keluar_id['id'] = $sk->id;
								$stok_obat_keluar_unit_lain_dbtable->update($stok_keluar_data, $stok_keluar_id);
								$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
								$stok_query = "
									SELECT *
									FROM smis_frm_stok_obat
									WHERE id = '" . $sk->id_stok_obat . "'
								";
								$stok_obat_row = $stok_obat_dbtable->get_row($stok_query);
								$stok_data = array();
								$stok_data['sisa'] = $stok_obat_row->sisa + $simpan_stok;
								$stok_id['id'] = $sk->id_stok_obat;
								$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
								$stok_dbtable->update($stok_data, $stok_id);
								//logging riwayat stok obat:
								$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
								$data_riwayat = array();
								$data_riwayat['tanggal'] = date("Y-m-d");
								$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
								$data_riwayat['jumlah_masuk'] = $simpan_stok;
								$data_riwayat['sisa'] = $stok_obat_row->sisa + $simpan_stok;
								$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . $_POST['nama_unit'];
								global $user;
								$data_riwayat['nama_user'] = $user->getName();
								$riwayat_dbtable->insert($data_riwayat);
								if ($jumlah == 0) break;
							}
						} //end of update
					} else if($d['cmd'] == "delete") {
						$detail_data = array();
						$detail_data['prop'] = "del";
						$detail_id['id'] = $d['id'];
						$dobat_keluar_unit_lain_dbtable->update($detail_data, $detail_id);
						$stok_obat_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar_unit_lain");
						$stok_keluar_query = "
							SELECT *
							FROM smis_frm_stok_obat_keluar_unit_lain
							WHERE id_dobat_keluar_unit_lain = '" . $d['id'] . "' AND smis_frm_stok_obat_keluar_unit_lain.prop NOT LIKE 'del'
							ORDER BY id DESC
						";
						$stok_keluar = $stok_obat_keluar_unit_lain_dbtable->get_result($stok_keluar_query);
						foreach($stok_keluar as $sk) {
							$stok_keluar_data = array();
							$stok_keluar_data['prop'] = "del";
							$stok_keluar_id['id'] = $sk->id;
							$stok_obat_keluar_unit_lain_dbtable->update($stok_keluar_data, $stok_keluar_id);
							$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
							$stok_query = "
								SELECT *
								FROM smis_frm_stok_obat
								WHERE id = '" . $sk->id_stok_obat . "'
							";
							$stok_obat_row = $stok_obat_dbtable->get_row($stok_query);
							$stok_data = array();
							$stok_data['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
							$stok_id['id'] = $sk->id_stok_obat;
							$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
							$stok_dbtable->update($stok_data, $stok_id);
							//logging riwayat stok obat:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
							$data_riwayat['jumlah_masuk'] = $sk->jumlah;
							$data_riwayat['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
							$data_riwayat['keterangan'] = "Perubahan Stok Keluar ke " . $_POST['nama_unit'];
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
						}
					}
				}
			}
		}
		$success['id'] = $id['id'];
		$success['success'] = 1;
		if ($result === false) $success['success'] = 0;
		else {
			if ($success['type'] == "insert") {
				$row = $this->dbtable->select($id['id']);
				$hpp_total = 0;
				$dobat_keluar_rows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_dobat_keluar_unit_lain
					WHERE id_obat_keluar_unit_lain = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				if ($dobat_keluar_rows != null) {
					foreach ($dobat_keluar_rows as $dok) {
						$hpp_row = $this->dbtable->get_row("
							SELECT *
							FROM smis_frm_histori_hpp
							WHERE id_obat = '" . $dok->id_obat . "' AND prop NOT LIKE 'del' AND tanggal_transaksi <= '" . $row->tanggal . "'
							ORDER BY id DESC
							LIMIT 0, 1
						");
						if ($hpp_row != null && $hpp_row->hpp != null)
							$hpp_total += ($hpp_row->hpp * $dok->jumlah);
					}
				}

				// notify hpp dan persediaan :
				$data = array(
					"jenis_akun"	=> "transaction",
					"jenis_data"	=> "persediaan",
					"id_data"		=> $id['id'],
					"entity"		=> "farmasi",
					"service"		=> "get_detail_accounting_hpp_persediaan_obat_keluar_ext",
					"data"			=> $id['id'],
					"code"			=> "persediaan-farmasi-" . $id['id'],
					"operation"		=> "",
					"tanggal"		=> $row->tanggal,
					"uraian"		=> "Persediaan dari Obat Keluar Ext. " . $id['id'] . " ke " . $row->nama_unit . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $row->tanggal),
					"nilai"			=> $hpp_total
				);
				$service_consumer = new ServiceConsumer(
					$this->dbtable->get_db(),
					"push_notify_accounting",
					$data,
					"accounting"
				);
				$service_consumer->execute();
			}
		}
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$header_row = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_obat_keluar_unit_lain
			WHERE id = '" . $id . "'
		");
		$data['header'] = $header_row;
		$dobat_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_keluar_unit_lain");
		$data['detail'] = $dobat_keluar_unit_lain_dbtable->get_result("
			SELECT *
			FROM smis_frm_dobat_keluar_unit_lain
			WHERE id_obat_keluar_unit_lain = '" . $id . "' AND prop NOT LIKE 'del'
		");
		return $data;
	}
	public function delete() {
		$id['id'] = $_POST['id'];
		if ($this->dbtable->isRealDelete()) {
			$result = $this->dbtable->delete(null,$id);
		} else {
			$data['prop'] = "del";
			$result = $this->dbtable->update($data, $id);
		}
		$stok_obat_keluar_unit_lain_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat_keluar_unit_lain");
		$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
		$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
		$stok_keluar_query = "
			SELECT smis_frm_stok_obat_keluar_unit_lain.*, smis_frm_obat_keluar_unit_lain.nama_unit
			FROM (smis_frm_stok_obat_keluar_unit_lain LEFT JOIN smis_frm_dobat_keluar_unit_lain ON smis_frm_stok_obat_keluar_unit_lain.id_dobat_keluar_unit_lain = smis_frm_dobat_keluar_unit_lain.id) LEFT JOIN smis_frm_obat_keluar_unit_lain ON smis_frm_dobat_keluar_unit_lain.id_obat_keluar_unit_lain = smis_frm_obat_keluar_unit_lain.id
			WHERE smis_frm_obat_keluar_unit_lain.id = '" . $id['id'] . "' AND smis_frm_stok_obat_keluar_unit_lain.prop NOT LIKE 'del'
			ORDER BY smis_frm_stok_obat_keluar_unit_lain.id DESC
		";
		$stok_keluar = $this->dbtable->get_result($stok_keluar_query);
		foreach($stok_keluar as $sk) {
			$stok_keluar_data = array();
			$stok_keluar_data['prop'] = "del";
			$stok_keluar_id['id'] = $sk->id;
			$stok_obat_keluar_unit_lain_dbtable->update($stok_keluar_data, $stok_keluar_id);
			$stok_query = "
				SELECT *
				FROM smis_frm_stok_obat
				WHERE id = '" . $sk->id_stok_obat . "'
			";
			$stok_obat_row =  $this->dbtable->get_row($stok_query);
			$stok_data = array();
			$stok_data['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
			$stok_id['id'] = $sk->id_stok_obat;
			$stok_obat_dbtable->update($stok_data, $stok_id);
			//logging riwayat stok obat:
			$data_riwayat = array();
			$data_riwayat['tanggal'] = date("Y-m-d");
			$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
			$data_riwayat['jumlah_masuk'] = $sk->jumlah;
			$data_riwayat['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
			$data_riwayat['keterangan'] = "Pembatalan Stok Keluar ke " . $sk->nama_unit;
			global $user;
			$data_riwayat['nama_user'] = $user->getName();
			$riwayat_dbtable->insert($data_riwayat);
		}

		//kartu gudang induk dan logging hpp:
		$dok_rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_frm_dobat_keluar_unit_lain
			WHERE id_obat_keluar = '" . $id['id'] . "' AND prop NOT LIKE 'del'
		");
		if ($dok_rows != null) {
			$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
			$header_row = $this->dbtable->get_row("
				SELECT *
				FROM smis_frm_obat_keluar_unit_lain
				WHERE id = '" . $id['id'] . "'
			");
			foreach ($dok_rows as $dr) {	
				$ks_data = array();
				$ks_data['f_id'] = $id['id'];
				$ks_data['unit'] = "Pembatalan Mutasi ke " . ArrayAdapter::format("unslug", $header_row->nama_unit);
				$ks_data['no_bon'] = $id['id'];
				$ks_data['id_obat'] = $dr->id_obat;
				$ks_data['kode_obat'] = $dr->kode_obat;
				$ks_data['nama_obat'] = $dr->nama_obat;
				$ks_data['nama_jenis_obat'] = $dr->nama_jenis_obat;
				$ks_data['tanggal'] = date("Y-m-d");
				$ks_data['masuk'] = $dr->jumlah;
				$ks_data['keluar'] = 0;	
				$ks_row = $ks_dbtable->get_row("
					SELECT SUM(sisa) AS 'jumlah'
					FROM smis_frm_stok_obat
					WHERE prop NOT LIKE 'del' AND id_obat = '" . $dr->id_obat . "'
				");
				$ks_data['sisa'] = $ks_row->jumlah + $dr->jumlah;
				$ks_dbtable->insert($ks_data);
				/// logging hpp :
				InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $dr->id_obat, $header_row->tanggal, 1, "Pembatalan Obat Keluar Unit Lain ID : " . $id['id']);
			}
		}

		$success['success'] = 1;
		$success['id'] = $_POST['id'];
		if ($result === 'false') $success['success'] = 0;
		return $success;
	}
}
?>