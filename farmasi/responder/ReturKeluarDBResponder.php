<?php
	require_once("farmasi/library/InventoryLibrary.php");

	class ReturObatDBResponder extends DBResponder {
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];
			if ($id['id'] == 0 || $id['id'] == "") {
				//do insert header here:
				$header_data['tanggal'] = date("Y-m-d");
				$result = $this->dbtable->insert($header_data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail'])) {
					//do insert detail here:
					$dretur_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dretur_obat");
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
					$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$dretur_data = array();
						$dretur_data['id_retur_obat'] = $id['id'];
						$dretur_data['id_stok_obat'] = $d['id_stok_obat'];
						$dretur_data['jumlah'] = $d['jumlah'];
						$dretur_data['keterangan'] = $d['keterangan'];
						$dretur_obat_dbtable->insert($dretur_data);
						$stok_row = $stok_obat_dbtable->get_row("
							SELECT id, id_obat, kode_obat, nama_obat, nama_jenis_obat, satuan, sisa, retur
							FROM smis_frm_stok_obat
							WHERE id = '" . $d['id_stok_obat'] . "'
						");
						$stok_data = array();
						$stok_data['sisa'] = $stok_row->sisa - $d['jumlah'];
						$stok_data['retur'] = $stok_row->retur + $d['jumlah'];
						$stok_id['id'] = $d['id_stok_obat'];
						$stok_obat_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok obat:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $d['id_stok_obat'];
						$data_riwayat['jumlah_keluar'] = $d['jumlah'];
						$data_riwayat['sisa'] = $stok_row->sisa - $d['jumlah'];
						$data_riwayat['keterangan'] = "Retur Stok ke Vendor";
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
						//logging kartu stok dan logging hpp:
						$sisa_row = $this->dbtable->get_row("
							SELECT SUM(a.sisa) AS 'sisa'
							FROM smis_frm_stok_obat a
							WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $stok_row->id_obat . "' AND a.satuan = '" . $stok_row->satuan . "' AND a.konversi = '1'
						");
						$kartu_stok_data = array(
							"f_id"				=> $id['id'],
							"no_bon"			=> $id['id'],
							"unit"				=> "Retur Obat Keluar : ID : " . $id['id'],
							"id_obat"			=> $stok_row->id_obat,
							"kode_obat"			=> $stok_row->kode_obat,
							"nama_obat"			=> $stok_row->nama_obat,
							"nama_jenis_obat"	=> $stok_row->nama_jenis_obat,
							"tanggal"			=> date("Y-m-d"),
							"masuk"				=> 0,
							"keluar"			=> $d['jumlah'],
							"sisa"				=> $sisa_row->sisa
						);
						$kartu_stok_dbtable->insert($kartu_stok_data);
						InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $stok_row->id_obat, date("Y-m-d H:i:s"), 0, "Retur Obat Keluar ID : , " . $id['id']);
					}
				}
				// notify accounting:
				$total = 0;
				$nama = "";
				$tanggal = "";
				$data_row = $this->dbtable->get_row("
					SELECT *
					FROM smis_frm_retur_obat
					WHERE id = '" . $id['id'] . "'
				");
				if ($data_row != null) {
					$nama = $data_row->nama_vendor;
					$tanggal = $data_row->tanggal;
					$data_rows = $this->dbtable->get_result("
						SELECT a.*, b.id_obat, b.nama_obat, b.hna
						FROM smis_frm_dretur_obat a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id
						WHERE a.id_retur_obat = '" . $id['id'] . "' AND a.prop NOT LIKE 'del'
					");
					if ($data_rows != null)
						foreach ($data_rows as $dr)
							$total += $dr->jumlah * $dr->hna;
				}
				$data = array(
					"jenis_akun"	=> "transaction",
					"jenis_data"	=> "retur_pembelian",
					"id_data"		=> $id['id'],
					"entity"		=> "farmasi",
					"service"		=> "get_detail_accounting_retur_pembelian",
					"data"			=> $id['id'],
					"code"			=> "retur_pembelian-farmasi-" . $id['id'],
					"operation"		=> "",
					"tanggal"		=> $tanggal,
					"uraian"		=> "Retur Pembelian ID Transaksi : " . $id['id'] . " dari " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
					"nilai"			=> $total
				);
				$service_consumer = new ServiceConsumer(
					$this->dbtable->get_db(),
					"push_notify_accounting",
					$data,
					"accounting"
				);
				$service_consumer->execute();
			} else {
				//do update header here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
					$detail_rows = $this->dbtable->get_result("
						SELECT *
						FROM smis_frm_dretur_obat
						WHERE id_retur_obat = '" . $id['id'] . "' AND prop NOT LIKE 'del'
					");
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
					$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
					foreach($detail_rows as $dr) {
						$stok_row = $stok_obat_dbtable->get_row("
							SELECT id, id_obat, kode_obat, nama_obat, nama_jenis_obat, satuan, sisa, retur
							FROM smis_frm_stok_obat
							WHERE id = '" . $dr->id_stok_obat . "'
						");
						$stok_data = array();
						$stok_data['sisa'] = $stok_row->sisa + $dr->jumlah;
						$stok_data['retur'] = $stok_row->retur - $dr->jumlah;
						$stok_id['id'] = $dr->id_stok_obat;
						$stok_obat_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok obat:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $dr->id_stok_obat;
						$data_riwayat['jumlah_keluar'] = $dr->jumlah;
						$data_riwayat['sisa'] = $stok_row->sisa - $dr->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Retur Stok ke Vendor";
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
						//logging kartu stok dan logging hpp:
						$sisa_row = $this->dbtable->get_row("
							SELECT SUM(a.sisa) AS 'sisa'
							FROM smis_frm_stok_obat a
							WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $stok_row->id_obat . "' AND a.satuan = '" . $stok_row->satuan . "' AND a.konversi = '1'
						");
						$kartu_stok_data = array(
							"f_id"				=> $id['id'],
							"no_bon"			=> $id['id'],
							"unit"				=> "Pembatalan Retur Obat Keluar : ID : " . $id['id'],
							"id_obat"			=> $stok_row->id_obat,
							"kode_obat"			=> $stok_row->kode_obat,
							"nama_obat"			=> $stok_row->nama_obat,
							"nama_jenis_obat"	=> $stok_row->nama_jenis_obat,
							"tanggal"			=> date("Y-m-d"),
							"masuk"				=> 0,
							"keluar"			=> $d['jumlah'],
							"sisa"				=> $sisa_row->sisa
						);
						$kartu_stok_dbtable->insert($kartu_stok_data);
						InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $stok_row->id_obat, date("Y-m-d H:i:s"), 0, "Pembatalan Retur Obat Keluar ID : , " . $id['id']);
					}
					// notify accounting:
					$total = 0;
					$nama = "";
					$tanggal = "";
					$data_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_frm_retur_obat
						WHERE id = '" . $id['id'] . "'
					");
					if ($data_row != null) {
						$nama = $data_row->nama_vendor;
						$tanggal = $data_row->tanggal;
						$data_rows = $this->dbtable->get_result("
							SELECT a.*, b.id_obat, b.nama_obat, b.hna
							FROM smis_frm_dretur_obat a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id
							WHERE a.id_retur_obat = '" . $id['id'] . "' AND a.prop NOT LIKE 'del'
						");
						if ($data_rows != null)
							foreach ($data_rows as $dr)
								$total += $dr->jumlah * $dr->hna;
					}
					$data = array(
						"jenis_akun"	=> "transaction",
						"jenis_data"	=> "retur_pembelian",
						"id_data"		=> $id['id'],
						"entity"		=> "farmasi",
						"service"		=> "get_detail_accounting_retur_pembelian",
						"data"			=> $id['id'],
						"code"			=> "retur_pembelian-farmasi-" . $id['id'],
						"operation"		=> "del",
						"tanggal"		=> $tanggal,
						"uraian"		=> "Retur Pembelian ID Transaksi : " . $id['id'] . " dari " . $nama . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $tanggal),
						"nilai"			=> $total
					);
					$service_consumer = new ServiceConsumer(
						$this->dbtable->get_db(),
						"push_notify_accounting",
						$data,
						"accounting"
					);
					$service_consumer->execute();
				}
			}
			$success['id'] = $id['id'];
			$success['success'] = 1;
			if ($result === false) $success['success'] = 0;
			return $success;
		}
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT smis_frm_retur_obat.*, smis_frm_obat_masuk.tipe
				FROM smis_frm_retur_obat LEFT JOIN smis_frm_obat_masuk ON smis_frm_retur_obat.id_obat_masuk = smis_frm_obat_masuk.id
				WHERE smis_frm_retur_obat.id = '" . $id . "'
			");
			$detail_rows = $this->dbtable->get_result("
				SELECT smis_frm_dretur_obat.*, smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.nama_jenis_obat, smis_frm_stok_obat.satuan, smis_frm_stok_obat.konversi, smis_frm_stok_obat.satuan_konversi, smis_frm_stok_obat.tanggal_exp, smis_frm_stok_obat.produsen
				FROM smis_frm_dretur_obat LEFT JOIN smis_frm_stok_obat ON smis_frm_dretur_obat.id_stok_obat = smis_frm_stok_obat.id
				WHERE smis_frm_dretur_obat.prop NOT LIKE 'del' AND smis_frm_dretur_obat.id_retur_obat = '" . $id . "'
			");
			$detail_list = "";
			$row_id = 0;
			foreach($detail_rows as $dr) {
				$f_tanggal_exp = "";
				if ($dr->tanggal_exp == "0000-00-00")
					$f_tanggal_exp = "-";
				else
					$f_tanggal_exp = ArrayAdapter::format("date d M Y", $dr->tanggal_exp);
				$detail_list .= "<tr id='detail_" . $row_id . "'>" .
									"<td id='detail_" . $row_id . "_id' style='display: none;'>" . $dr->id . "</td>" .
									"<td id='detail_" . $row_id . "_id_stok_obat' style='display: none;'>" . $dr->id_stok_obat . "</td>" .
									"<td id='detail_" . $row_id . "_jumlah_retur' style='display: none;'>" . $dr->jumlah . "</td>" .
									"<td id='detail_" . $row_id . "_satuan' style='display: none;'>" . $dr->satuan . "</td>" .
									"<td id='detail_" . $row_id . "_konversi' style='display: none;'>" . $dr->konversi . "</td>" .
									"<td id='detail_" . $row_id . "_satuan_konversi' style='display: none;'>" . $dr->satuan_konversi . "</td>" .
									"<td id='detail_" . $row_id . "_tanggal_exp' style='display: none;'>" . $dr->tanggal_exp . "</td>" .
									"<td id='detail_" . $row_id . "_nama_obat'>" . $dr->nama_obat . "</td>" .
									"<td id='detail_" . $row_id . "_nama_jenis_obat'>" . $dr->nama_jenis_obat . "</td>" .
									"<td id='detail_" . $row_id . "_produsen'>" . $dr->produsen . "</td>" .
									"<td id='detail_" . $row_id . "_f_tanggal_exp'>" . $f_tanggal_exp . "</td>" .
									"<td id='detail_" . $row_id . "_f_jumlah'>" . $dr->jumlah . " " . $dr->satuan . "</td>" .
									"<td id='detail_" . $row_id . "_keterangan'>" . $dr->keterangan . "</td>" .
									"<td></td>" .
								"</tr>";
				$row_id++;
			}
			$data['detail_list'] = $detail_list;
			return $data;
		}
	}
?>