<?php
require_once("smis-base/smis-include-service-consumer.php");
require_once("farmasi/library/InventoryLibrary.php");

class ObatMasukDBResponder extends DBResponder {
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			$result = $this->dbtable->insert($header_data);
			$id['id'] = $this->dbtable->get_inserted_id();
			$success['type'] = "insert";
			$subtotal = 0;
			if (isset($_POST['detail'])) {
				//do insert detail here:
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_masuk");
				$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_stok_obat");
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_riwayat_stok_obat");
				$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_kartu_stok_obat");
				$detail = $_POST['detail'];
				$tipe = $header_data['tipe'];
				if ($header_data['tipe'] == "farmasi")
					$tipe = "reguler";
				foreach($detail as $d) {
					//detail:
					$detail_data = array();
					$detail_data['id_obat_masuk'] = $id['id'];
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['medis'] = $d['medis'];
					$detail_data['inventaris'] = $d['inventaris'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['label'] = $tipe;
					$detail_data['id_dpo'] = $d['id_dpo'];
					$detail_row = $detail_dbtable->get_row("
						SELECT SUM(b.sisa) AS 'jumlah'
						FROM smis_frm_dobat_masuk a LEFT JOIN smis_frm_stok_obat b ON a.id = b.id_dobat_masuk
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $d['id_obat'] . "' AND b.prop NOT LIKE 'del'
					");
					$d['stok_entri'] = $detail_row->jumlah;
					$detail_data['stok_entri'] = $d['stok_entri'];
					$detail_data['jumlah_tercatat'] = $d['jumlah_tercatat'];
					$detail_data['jumlah'] = $d['jumlah'];
					$detail_data['sisa'] = 0;
					$detail_data['satuan'] = $d['satuan'];
					$detail_data['konversi'] = $d['konversi'];
					$detail_data['satuan_konversi'] = $d['satuan_konversi'];
					$detail_data['hna'] = $d['hna'];
					$detail_data['selisih'] = $d['selisih'];
					$detail_data['produsen'] = $d['produsen'];
					$detail_data['diskon'] = $d['diskon'];
					$detail_data['t_diskon'] = $d['t_diskon'];
					$detail_dbtable->insert($detail_data);
					$id_dobat_masuk = $detail_dbtable->get_inserted_id();
					$subtotal = $subtotal + ($d['hna'] * $d['jumlah']);
					
					//stok:
					$stok_data = array();
					$stok_data['id_dobat_masuk'] = $id_dobat_masuk;
					$stok_data['id_obat'] = $d['id_obat'];
					$stok_data['kode_obat'] = $d['kode_obat'];
					$stok_data['nama_obat'] = $d['nama_obat'];
					$stok_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$stok_data['label'] = $tipe;
					$stok_data['id_vendor'] = $header_data['id_vendor'];
					$stok_data['nama_vendor'] = $header_data['nama_vendor'];
					$stok_data['jumlah'] = $d['jumlah'];
					$stok_data['sisa'] = $d['sisa'];
					$stok_data['satuan'] = $d['satuan'];
					$stok_data['konversi'] = $d['konversi'];
					$stok_data['satuan_konversi'] = $d['satuan_konversi'];
					$stok_data['hna'] = $d['hna'] + $d['selisih'];
					$stok_data['produsen'] = $d['produsen'];
					$stok_data['tanggal_exp'] = $d['ed'];
					$stok_data['no_batch'] = $d['nobatch'];
					$stok_dbtable->insert($stok_data);
					$id_stok_obat = $stok_dbtable->get_inserted_id();
					
					//riwayat stok obat:
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $id_stok_obat;
					$data_riwayat['jumlah_masuk'] = $d['jumlah'];
					$data_riwayat['sisa'] = $d['sisa'];
					$data_riwayat['keterangan'] = "Stok Masuk dari " . $header_data['nama_vendor'] . ", No. Faktur: " . $header_data['no_faktur'];
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
					
					//kartu gudang induk dan hpp:
					$ks_data = array();
					$ks_data['f_id'] = $id_dobat_masuk;
					$ks_data['unit'] = $header_data['nama_vendor'];
					$ks_data['no_bon'] = $header_data['no_bbm'];
					$ks_data['id_obat'] = $d['id_obat'];
					$ks_data['kode_obat'] = $d['kode_obat'];
					$ks_data['nama_obat'] = $d['nama_obat'];
					$ks_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$ks_data['tanggal'] = $header_data['tanggal_datang'];
					$ks_data['masuk'] = $d['jumlah'];
					$ks_data['keluar'] = 0;
					$ks_data['sisa'] = $d['stok_entri'] + $d['jumlah'];
					$ks_dbtable->insert($ks_data);
					InventoryLibrary::saveHistoryHPP($this->dbtable->get_db(), $d['id_obat'], $header_data['tanggal_datang'], 1, "Stok Masuk dari " . $header_data['nama_vendor'] . " ID : " . $id['id']);
				}
			}
		} else {
			$this->dbtable->update($header_data, $id);
			$success['type'] = "update";
			if (isset($_POST['detail'])) {
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_masuk");
				$detail = $_POST['detail'];
				foreach($detail as $d) {
					//detail:
					$d_detail = array(
						"diskon"	=> $d['diskon'],
						"t_diskon"	=> $d['t_diskon']
					);
					$d_id['id'] = $d['id'];
					$detail_dbtable->update($d_detail, $d_id);
				}
				$result = true;
			}
		}
		$success['id'] = $id['id'];
		$success['success'] = 1;
		if ($result === false) $success['success'] = 0;
		else {
			if ($success['type'] == "insert") {
				/// notify accounting :
				$h_row = $this->dbtable->get_row("
					SELECT *
					FROM smis_frm_obat_masuk
					WHERE id = '" . $id['id'] . "'
				");
				$total = 0;
				$drows = $this->dbtable->get_result("
					SELECT *
					FROM smis_frm_dobat_masuk
					WHERE id_obat_masuk = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				if ($drows != null) {
					foreach ($drows as $dr) {
						$subtotal = $dr->jumlah * ($dr->hna / 1.1);
						$diskon = $dr->diskon;
						if ($dr->t_diskon == "persen")
							$diskon = $subtotal * $dr->diskon / 100;
						$subtotal = round($subtotal - $diskon);
						$total += $subtotal;
					}
				}
				$global_diskon = $h_row->diskon;
				if ($h_row->t_diskon == "persen")
					$global_diskon = $global_diskon * $total / 100;
				$global_diskon = floor($global_diskon);
				$total = $total - $global_diskon;
				$ppn = floor($total * 0.1);
				$total = $total + $ppn;

				$data = array(
					"jenis_akun"	=> "transaction",
					"jenis_data"	=> "pembelian",
					"id_data"		=> $id['id'],
					"entity"		=> "farmasi",
					"service"		=> "get_detail_accounting_bbm",
					"data"			=> $id['id'],
					"code"			=> "bbm-farmasi-" . $id['id'],
					"operation"		=> "",
					"tanggal"		=> $h_row->tanggal_datang,
					"uraian"		=> "BBM No. " . $h_row->no_bbm . " -  ID Transaksi " . $id['id'] . " dari " . $h_row->nama_vendor . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $h_row->tanggal_datang),
					"nilai"			=> $total
				);
				$service_consumer = new ServiceConsumer(
					$this->dbtable->get_db(),
					"push_notify_accounting",
					$data,
					"accounting"
				);
				$service_consumer->execute();
			}
		}
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$header_row = $this->dbtable->get_row("
			SELECT *
			FROM smis_frm_obat_masuk
			WHERE id = '" . $id . "'
		");
		$data['header'] = $header_row;
		$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_frm_dobat_masuk");
		$data['detail'] = $detail_dbtable->get_result("
			SELECT a.*, CASE WHEN b.tanggal_exp IS NULL THEN '-' ELSE b.tanggal_exp END AS 'tanggal_exp', CASE WHEN b.no_batch IS NULL THEN '-' ELSE b.no_batch END AS 'no_batch'
			FROM smis_frm_stok_obat b ON a.id = b.id_dobat_masuk
			WHERE a.id_obat_masuk = '" . $id . "' AND a.prop NOT LIKE 'del'
		");
		return $data;
	}
	public function delete() {
		$id['id'] = $_POST['id'];
		if ($this->dbtable->isRealDelete()) {
			$result = $this->dbtable->delete(null,$id);
		} else {
			/// notify accounting :
			$h_row = $this->dbtable->get_row("
				SELECT *
				FROM smis_frm_obat_masuk
				WHERE id = '" . $id['id'] . "'
			");
			$total = 0;
			$drows = $this->dbtable->get_result("
				SELECT *
				FROM smis_frm_dobat_masuk
				WHERE id_obat_masuk = '" . $id['id'] . "' AND prop NOT LIKE 'del'
			");
			if ($drows != null) {
				foreach ($drows as $dr) {
					$subtotal = $dr->jumlah * ($dr->hna / 1.1);
					$diskon = $dr->diskon;
					if ($dr->t_diskon == "persen")
						$diskon = $subtotal * $dr->diskon / 100;
					$subtotal = round($subtotal - $diskon);
					$total += $subtotal;
				}
			}
			$global_diskon = $h_row->diskon;
			if ($h_row->t_diskon == "persen")
				$global_diskon = $global_diskon * $total / 100;
			$global_diskon = floor($global_diskon);
			$total = $total - $global_diskon;
			$ppn = floor($total * 0.1);
			$total = $total + $ppn;

			$data = array(
				"jenis_akun"	=> "transaction",
				"jenis_data"	=> "pembelian",
				"id_data"		=> $id['id'],
				"entity"		=> "farmasi",
				"service"		=> "get_detail_accounting_bbm",
				"data"			=> $id['id'],
				"code"			=> "bbm-farmasi-" . $id['id'],
				"operation"		=> "del",
				"tanggal"		=> $h_row->tanggal_datang,
				"uraian"		=> "BBM No. " . $h_row->no_bbm . " -  ID Transaksi " . $id['id'] . " dari " . $h_row->nama_vendor . " pada tanggal " . ArrayAdapter::format("date d-m-Y", $h_row->tanggal_datang),
				"nilai"			=> $total
			);
			$service_consumer = new ServiceConsumer(
				$this->dbtable->get_db(),
				"push_notify_accounting",
				$data,
				"accounting"
			);
			$service_consumer->execute();
			$data = array();
			$data['prop'] = "del";
			$result = $this->dbtable->update($data, $id);
		}
		$success['success'] = 1;
		$success['id'] = $_POST['id'];
		if ($result === 'false') $success['success'] = 0;
		return $success;
	}
}
?>