<?php
	class ReturKeluarObatMasukDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT *
				FROM smis_frm_obat_masuk 
				WHERE id = '" . $id . "'
			");
			$detail_list = "";
			$dobat_masuk_rows = $this->dbtable->get_result("
				SELECT smis_frm_stok_obat.*
				FROM smis_frm_stok_obat LEFT JOIN smis_frm_dobat_masuk ON smis_frm_stok_obat.id_dobat_masuk = smis_frm_dobat_masuk.id
				WHERE smis_frm_stok_obat.prop NOT LIKE 'del' AND smis_frm_stok_obat.turunan = '0' AND smis_frm_stok_obat.label = 'reguler' AND smis_frm_dobat_masuk.id_obat_masuk = '" . $id . "'
			");
			$row_id = 0;
			foreach($dobat_masuk_rows as $dom) {
				$f_tanggal_exp = "";
				if ($dom->tanggal_exp == "0000-00-00")
					$f_tanggal_exp = "-";
				else
					$f_tanggal_exp = ArrayAdapter::format("date d M Y", $dom->tanggal_exp);
				$detail_list .= "<tr id='obat_" . $row_id . "'>" .
									"<td id='obat_" . $row_id . "_id' style='display: none;'></td>" .
									"<td id='obat_" . $row_id . "_id_stok_obat' style='display: none;'>" . $dom->id . "</td>" .
									"<td id='obat_" . $row_id . "_sisa' style='display: none;'>" . $dom->sisa . "</td>" .
									"<td id='obat_" . $row_id . "_jumlah_retur' style='display: none;'>0</td>" .
									"<td id='obat_" . $row_id . "_satuan' style='display: none;'>" . $dom->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_konversi' style='display: none;'>" . $dom->konversi . "</td>" .
									"<td id='obat_" . $row_id . "_satuan_konversi' style='display: none;'>" . $dom->satuan_konversi . "</td>" .
									"<td id='obat_" . $row_id . "_tanggal_exp' style='display: none;'>" . $dom->tanggal_exp . "</td>" .
									"<td id='obat_" . $row_id . "_nama_obat'>" . $dom->nama_obat . "</td>" .
									"<td id='obat_" . $row_id . "_nama_jenis_obat'>" . $dom->nama_jenis_obat . "</td>" .
									"<td id='obat_" . $row_id . "_produsen'>" . $dom->produsen . "</td>" .
									"<td id='obat_" . $row_id . "_f_tanggal_exp'>" . $f_tanggal_exp . "</td>" .
									"<td id='obat_" . $row_id . "_f_sisa'>" . $dom->sisa . " " . $dom->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_f_retur'>0 " . $dom->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_keterangan'>-</td>" .
									"<td>" .
										"<div class='btn-group noprint'>" .
											"<a href='#' onclick='dretur.edit(" . $row_id . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" . 
												"<i class='icon-edit icon-white'></i>" .
											"</a>" .
										"</div>" .
									"</td>" .
								"</tr>";
				$row_id++;
			}
			$data['detail_list'] = $detail_list;
			return $data;
		}
	}
?>