<?php
class BahanDBResponder extends DBResponder {
	public function command($command) {
		if ($command != "rounded_value")
			return parent::command($command);
		if ($command == "rounded_value") {
			$pack = new ResponsePackage();
			$content = $_POST['value'];
			if (getSettings($this->dbtable->get_db(), "farmasi-rounded-transaksi", 0) == 1) {
				loadLibrary("smis-libs-function-math");
			    $round = getSettings($this->dbtable->get_db(),"smis-simple-rounding-uang","-1");
			    $model = getSettings($this->dbtable->get_db(),"smis-simple-rounding-model","round");
			    $content  = smis_money_round($content, $round, $model);
			}
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}
	}
	public function edit() {
		$id = $_POST['id'];
		$data['header'] = $this->dbtable->get_row("
				SELECT DISTINCT id_obat, kode_obat, nama_obat, nama_jenis_obat
				FROM smis_frm_stok_obat
				WHERE prop NOT LIKE 'del' AND id_obat = '" . $id . "'
			");
		$satuan_rows = $this->dbtable->get_result("
				SELECT satuan, konversi, satuan_konversi 
				FROM (
					SELECT satuan, konversi, satuan_konversi, SUM(sisa) AS 't_jumlah'
					FROM smis_frm_stok_obat
					WHERE prop NOT LIKE 'del' AND id_obat = '" . $id . "'
					GROUP BY id_obat, satuan, konversi, satuan_konversi
				) v
				ORDER BY t_jumlah DESC
			");
		$satuan_option = "";
		foreach($satuan_rows as $sr) {
			if ($sr->konversi == 1) {
				$satuan_option .= "
						<option value='" . $sr->konversi . "_" . $sr->satuan_konversi . "'>" . $sr->satuan . "</option>
					";
			}
		}
		$data['satuan_option'] = $satuan_option;
		return $data;
	}
}
?>