<?php
class StokDBResponder extends DBResponder {
	public function edit() {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$data = $this->dbtable->get_row("
			SELECT id_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
			FROM smis_frm_stok_obat
			WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '" . $konversi . "' AND satuan_konversi = '" . $satuan_konversi . "'
			GROUP BY id_obat, satuan, konversi, satuan_konversi
		");
		return $data;
	}
}
?>