<?php
	global $db;
	require_once("farmasi/table/ObatKeluarUnitLainTable.php");
	require_once("farmasi/responder/ObatKeluarUnitLainDBResponder.php");
	require_once("farmasi/responder/ObatDBResponder.php");
	require_once("farmasi/responder/SisaDBResponder.php");
	require_once("farmasi/responder/MADBResponder.php");
	require_once("farmasi/table/DObatKeluarUnitLainTable.php");
	require_once("farmasi/adapter/ObatAdapter.php");
	
	$obat_keluar_unit_lain_table = new ObatKeluarUnitLainTable(
		array("Nomor", "Tanggal Keluar", "Kode Unit", "Nama Unit"),
		"Farmasi : Obat Keluar - Unit Eksternal",
		null,
		true
	);
	$obat_keluar_unit_lain_table->setName("obat_keluar_unit_lain");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "obat_keluar_unit_lain") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "export_au58") {
				require_once("smis-libs-out/php-excel/PHPExcel.php");
				$id = $_POST['id'];
				
				$objPHPExcel = PHPExcel_IOFactory::load("farmasi/templates/template_bukti_mutasi.xlsx");
				$objPHPExcel->setActiveSheetIndexByName("MUTASI OBAT");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				
				$dbtable = new DBTable($db, "smis_frm_obat_keluar_unit_lain");
				$header_info = $dbtable->get_row("
					SELECT *
					FROM smis_frm_obat_keluar_unit_lain
					WHERE id = '" . $id . "'
				");
				$objWorksheet->setCellValue("H4", ArrayAdapter::format("unslug", $header_info->nama_unit_lain));
				$objWorksheet->setCellValue("L2", ": K-" . ArrayAdapter::format("only-digit6", $header_info->id));
				$objWorksheet->setCellValue("L3", ": " . ArrayAdapter::format("date d-m-Y", $header_info->tanggal));
				$objWorksheet->setCellValue("L4", ": " . $header_info->kode_unit_lain);
				$detail_info = $dbtable->get_result("
					SELECT *
					FROM smis_frm_dobat_keluar_unit_lain
					WHERE id_obat_keluar_unit_lain = '" . $id . "'
				");
				$jumlah_item = count($detail_info);
				$jumlah_item_per_halaman = 15;
				$jumlah_halaman = ceil($jumlah_item / $jumlah_item_per_halaman);
				$start_row_index = 1;
				$end_row_index = 29;
				$value_index_incr = 29;

				$cur_item_index = 0;
				$print_area_str = "";
				for ($cur_page = 1; $cur_page <= $jumlah_halaman; $cur_page++) {
					$cur_row_index = ($start_row_index + 8) + ($cur_page - 1) * $value_index_incr;
					$start_print_area = $start_row_index + $value_index_incr * ($cur_page - 1);
					$end_print_area = $end_row_index + $value_index_incr * ($cur_page - 1);
					$print_area_str .= "A" . $start_print_area . ":L" . $end_print_area . ",";
					for ($cur_item_num = 1; $cur_item_num <= $jumlah_item_per_halaman && $cur_item_index < $jumlah_item; $cur_item_num++) {						
						$objWorksheet->setCellValue("A" . $cur_row_index, $detail_info[$cur_item_index]->kode_obat);
						$objWorksheet->setCellValue("B" . $cur_row_index, $detail_info[$cur_item_index]->nama_obat);
						$objWorksheet->setCellValue("D" . $cur_row_index, $detail_info[$cur_item_index]->satuan);
						$objWorksheet->setCellValue("E" . $cur_row_index, $detail_info[$cur_item_index]->stok_entri);
						$objWorksheet->setCellValue("F" . $cur_row_index, $detail_info[$cur_item_index]->jumlah_diminta);
						$objWorksheet->setCellValue("I" . $cur_row_index, $detail_info[$cur_item_index]->harga_ma);
						$objWorksheet->getStyle("I" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
						$objWorksheet->setCellValue("K" . $cur_row_index, $header_info->kode_unit_lain);
						$cur_item_index++;
						$cur_row_index++;
					}
				}
				$objWorksheet->getPageSetup()->setPrintArea(rtrim($print_area_str, ","));
				
				header("Content-type: application/vnd.ms-excel");	
				header("Content-Disposition: attachment; filename=OBAT_KELUAR_EXT-" . ArrayAdapter::format("unslug", $header_info->nama_unit) . "_" . ArrayAdapter::format("only-digit6", $header_info->id) . "_" . ArrayAdapter::format("date Ymd", $header_info->tanggal) . ".xlsx");
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				$objWriter->save("php://output");
				return;
			}
			$obat_keluar_unit_lain_adapter = new SimpleAdapter();
			$obat_keluar_unit_lain_adapter->add("id", "id");
			$obat_keluar_unit_lain_adapter->add("Nomor", "id", "digit8");
			$obat_keluar_unit_lain_adapter->add("Tanggal Keluar", "tanggal", "date d M Y");
			$obat_keluar_unit_lain_adapter->add("Kode Unit", "kode_unit");
			$obat_keluar_unit_lain_adapter->add("Nama Unit", "nama_unit");
			$columns = array("id", "tanggal", "id_unit", "kode_unit", "nama_unit", "keterangan");
			$obat_keluar_unit_lain_dbtable = new DBTable(
				$db,
				"smis_frm_obat_keluar_unit_lain",
				$columns
			);
			$obat_keluar_unit_lain_dbtable->setOrder(" id DESC ");
			$obat_keluar_unit_lain_dbresponder = new ObatKeluarUnitLainDBResponder(
				$obat_keluar_unit_lain_dbtable,
				$obat_keluar_unit_lain_table,
				$obat_keluar_unit_lain_adapter
			);
			$data = $obat_keluar_unit_lain_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}

	//get unit lain chooser:
	$unit_lain_table = new Table(
		array("Nomor", "Kode", "Nama"),
		"",
		null,
		true
	);
	$unit_lain_table->setName("unit_lain");
	$unit_lain_table->setModel(Table::$SELECT);
	$unit_lain_adapter = new SimpleAdapter();
	$unit_lain_adapter->add("Nomor", "id", "digit8");
	$unit_lain_adapter->add("Kode", "kode");
	$unit_lain_adapter->add("Nama", "nama");
	$unit_lain_dbtable = new DBTable($db, "smis_frm_unit_lain");
	$unit_lain_dbresponder = new DBResponder(
		$unit_lain_dbtable,
		$unit_lain_table,
		$unit_lain_adapter
	);
	
	//get obat chooser:
	$obat_table = new Table(
		array("Kode", "Obat", "Jenis", "Stok"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new ObatAdapter();
	$obat_dbtable = new DBTable($db, "smis_frm_stok_obat");
	$obat_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT *
		FROM (
			SELECT id_obat AS id, kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok'
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi, CASE label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM smis_frm_stok_obat
				WHERE prop NOT LIKE 'del' AND konversi = '1' " . $filter . "
				GROUP BY id_obat, satuan, konversi, satuan_konversi, label
			) v_obat
			GROUP BY id_obat
		) v_stok
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			" . $query_value . "
		) v_stok
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	//get sisa by id_obat, satuan, konversi, dan satuan_konversi:
	$sisa_table = new Table(
		array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi"),
		"",
		null,
		true
	);
	$sisa_table->setName("sisa");
	$sisa_adapter = new SimpleAdapter();
	$sisa_adapter->add("id_obat", "id_obat");
	$sisa_adapter->add("sisa", "sisa");
	$sisa_adapter->add("satuan", "satuan");
	$sisa_adapter->add("konversi", "konversi");
	$sisa_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
	$sisa_dbtable = new DBTable(
		$db,
		"smis_frm_stok_obat",
		$columns
	);
	$sisa_dbresponder = new SisaDBResponder(
		$sisa_dbtable,
		$sisa_table,
		$sisa_adapter
	);

	//get harga ma:
	$harga_ma_table = new Table(
		array("id_obat", "harga_ma", "jumlah", "satuan", "konversi", "satuan_konversi"),
		"",
		null,
		true
	);
	$harga_ma_table->setName("harga_ma");
	$harga_ma_adapter = new SimpleAdapter();
	$harga_ma_adapter->add("id_obat", "id_obat");
	$harga_ma_adapter->add("harga_ma", "harga_ma");
	$harga_ma_adapter->add("satuan", "satuan");
	$harga_ma_adapter->add("konversi", "konversi");
	$harga_ma_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "harga_ma", "jumlah", "satuan", "konversi", "satuan_konversi");
	$harga_ma_dbtable = new DBTable(
		$db,
		"smis_frm_stok_obat",
		$columns
	);
	$harga_ma_dbresponder = new MADBResponder(
		$harga_ma_dbtable,
		$harga_ma_table,
		$harga_ma_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("unit_lain", $unit_lain_dbresponder);
	$super_command->addResponder("obat", $obat_dbresponder);
	$super_command->addResponder("sisa", $sisa_dbresponder);
	$super_command->addResponder("harga_ma", $harga_ma_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$obat_keluar_unit_lain_modal = new Modal("obat_keluar_unit_lain_add_form", "smis_form_container", "obat_keluar_unit_lain");
	$obat_keluar_unit_lain_modal->setTitle("Obat Keluar");
	$obat_keluar_unit_lain_modal->setClass(Modal::$FULL_MODEL);
	$id_text = new Text("obat_keluar_unit_lain_id", "obat_keluar_unit_lain_id", "");
	$id_text->setAtribute("disabled='disabled'");
	$obat_keluar_unit_lain_modal->addElement("No.", $id_text);
	$tanggal_text = new Text("obat_keluar_unit_lain_tanggal", "obat_keluar_unit_lain_tanggal", date("Y-m-d"));
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$obat_keluar_unit_lain_modal->addElement("Tanggal", $tanggal_text);
	$id_unit_hidden = new Hidden("obat_keluar_unit_lain_id_unit", "obat_keluar_unit_lain_id_unit", "");
	$obat_keluar_unit_lain_modal->addElement("", $id_unit_hidden);
	$kode_unit_hidden = new Hidden("obat_keluar_unit_lain_kode_unit", "obat_keluar_unit_lain_kode_unit", "");
	$obat_keluar_unit_lain_modal->addElement("", $kode_unit_lain_hidden);
	$unit_lain_button = new Button("", "", "Pilih");
	$unit_lain_button->setClass("btn-info");
	$unit_lain_button->setIsButton(Button::$ICONIC);
	$unit_lain_button->setIcon("icon-white ".Button::$icon_list_alt);
	$unit_lain_button->setAction("unit_lain.chooser('unit_lain', 'unit_lain_button', 'unit_lain', unit_lain)");
	$unit_lain_button->setAtribute("id='unit_lain_browse'");
	$unit_lain_text = new Text("obat_keluar_unit_lain_nama_unit", "obat_keluar_unit_lain_nama_unit", "");
	$unit_lain_text->setAtribute("disabled='disabled' autofocus");
	$unit_lain_text->setClass("smis-one-option-input");
	$unit_lain_input_group = new InputGroup("");
	$unit_lain_input_group->addComponent($unit_lain_text);
	$unit_lain_input_group->addComponent($unit_lain_button);
	$obat_keluar_unit_lain_modal->addElement("Unit Lain", $unit_lain_input_group);
	$total_text = new Text("obat_keluar_unit_lain_total", "obat_keluar_unit_lain_total", "0,00");
	$total_text->setAtribute("disabled='disabled'");
	$obat_keluar_unit_lain_modal->addElement("Total", $total_text);
	$dobat_keluar_unit_lain_table = new DObatKeluarUnitLainTable(
		array("No.", "Nama Obat", "Jenis Obat", "Jumlah", "Nilai Sat.", "Nilai Tot.", "Keterangan"),
		"",
		null,
		true
	);
	$dobat_keluar_unit_lain_table->setName("dobat_keluar_unit_lain");
	$dobat_keluar_unit_lain_table->setFooterVisible(false);
	$obat_keluar_unit_lain_modal->addBody("dobat_keluar_unit_lain_table", $dobat_keluar_unit_lain_table);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAtribute("id='obat_keluar_unit_lain_save'");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$obat_keluar_unit_lain_modal->addFooter($save_button);
	$ok_button = new Button("", "", "OK");
	$ok_button->setClass("btn-success");
	$ok_button->setAtribute("id='obat_keluar_unit_lain_ok'");
	$ok_button->setAction("$($(this).data('target')).smodal('hide')");
	$obat_keluar_unit_lain_modal->addFooter($ok_button);
	
	$dobat_keluar_unit_lain_modal = new Modal("dobat_keluar_unit_lain_add_form", "smis_form_container", "dobat_keluar_unit_lain");
	$dobat_keluar_unit_lain_modal->setTitle("Detail Obat Keluar");
	$id_hidden = new Hidden("dobat_keluar_unit_lain_id", "dobat_keluar_unit_lain_id", "");
	$dobat_keluar_unit_lain_modal->addElement("", $id_hidden);
	$id_obat_hidden = new Hidden("dobat_keluar_unit_lain_id_obat", "dobat_keluar_unit_lain_id_obat", "");
	$dobat_keluar_unit_lain_modal->addElement("", $id_obat_hidden);
	$nama_obat_button = new Button("", "", "Pilih");
	$nama_obat_button->setClass("btn-info");
	$nama_obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$nama_obat_button->setIcon("icon-white icon-list-alt");
	$nama_obat_button->setAtribute("id='obat_browse'");
	$nama_obat_button->setIsButton(Button::$ICONIC);
	$nama_obat_text = new Text("dobat_keluar_unit_lain_nama_obat", "dobat_keluar_unit_lain_nama_obat", "");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_input_group = new InputGroup("");
	$nama_obat_input_group->addComponent($nama_obat_text);
	$nama_obat_input_group->addComponent($nama_obat_button);
	$dobat_keluar_unit_lain_modal->addElement("Nama Obat", $nama_obat_input_group);
	$nama_jenis_obat_text = new Text("dobat_keluar_unit_lain_nama_jenis_obat", "dobat_keluar_unit_lain_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$dobat_keluar_unit_lain_modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	$kode_obat_text = new Text("dobat_keluar_unit_lain_kode_obat", "dobat_keluar_unit_lain_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$dobat_keluar_unit_lain_modal->addElement("Kode Obat", $kode_obat_text);
	$satuan_select = new Select("dobat_keluar_unit_lain_satuan", "dobat_keluar_unit_lain_satuan", "");
	$dobat_keluar_unit_lain_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("dobat_keluar_unit_lain_konversi", "dobat_keluar_unit_lain_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$dobat_keluar_unit_lain_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("dobat_keluar_unit_lain_satuan_konversi", "dobat_keluar_unit_lain_satuan_konversi", "");
	$dobat_keluar_unit_lain_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("dobat_keluar_unit_lain_stok", "dobat_keluar_unit_lain_stok", "");
	$dobat_keluar_unit_lain_modal->addElement("", $stok_hidden);
	$f_stok_text = new Text("dobat_keluar_unit_lain_f_stok", "dobat_keluar_unit_lain_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$dobat_keluar_unit_lain_modal->addElement("Stok", $f_stok_text);
	$jumlah_lama_hidden = new Hidden("dobat_keluar_unit_lain_jumlah_lama", "dobat_keluar_unit_lain_jumlah_lama", "");
	$dobat_keluar_unit_lain_modal->addElement("", $jumlah_lama_hidden);
	$jumlah_diminta_text = new Text("dobat_keluar_unit_lain_jumlah_diminta", "dobat_keluar_unit_lain_jumlah_diminta", "");
	$dobat_keluar_unit_lain_modal->addElement("Jml. Diminta", $jumlah_diminta_text);
	$jumlah_text = new Text("dobat_keluar_unit_lain_jumlah", "dobat_keluar_unit_lain_jumlah", "");
	$dobat_keluar_unit_lain_modal->addElement("Jml. Dipenuhi", $jumlah_text);
	$harga_ma_text = new Text("dobat_keluar_unit_lain_harga_ma", "dobat_keluar_unit_lain_harga_ma", "");
	$harga_ma_text->setAtribute("disabled='disabled'");
	$dobat_keluar_unit_lain_modal->addElement("Nilai Sat.", $harga_ma_text);
	$dobat_keluar_unit_lain_button = new Button("", "", "Simpan");
	$dobat_keluar_unit_lain_button->setClass("btn-success");
	$dobat_keluar_unit_lain_button->setAtribute("id='dobat_keluar_unit_lain_save'");
	$dobat_keluar_unit_lain_button->setIcon("fa fa-floppy-o");
	$dobat_keluar_unit_lain_button->setIsButton(Button::$ICONIC);
	$dobat_keluar_unit_lain_modal->addFooter($dobat_keluar_unit_lain_button);
	
	echo $dobat_keluar_unit_lain_modal->getHtml();
	echo $obat_keluar_unit_lain_modal->getHtml();
	echo $obat_keluar_unit_lain_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/obat_keluar_unit_lain_action.js", false);
	echo addJS("farmasi/js/dobat_keluar_unit_lain_action.js", false);
	echo addJS("farmasi/js/obat_keluar_unit_lain_obat_action.js", false);
	echo addJS("farmasi/js/obat_keluar_unit_lain_unit_lain_action.js", false);
	echo addJS("farmasi/js/obat_keluar_unit_lain.js", false);
?>