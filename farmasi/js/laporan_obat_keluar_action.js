function LOBKAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LOBKAction.prototype.constructor = LOBKAction;
LOBKAction.prototype = new TableAction();
LOBKAction.prototype.view = function() {
	if ($("#lobk_tanggal_from").val() == "" || $("#lobk_tanggal_to").val() == "")
		return;
	var self = this;
	$("#lobk_info").empty();
	$("#lobk_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#lobk_loading_modal").smodal("show");
	FINISHED = false;
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	data['tanggal_from'] = $("#lobk_tanggal_from").val();
	data['tanggal_to'] = $("#lobk_tanggal_to").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#lobk_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
};
LOBKAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#lobk_loading_modal").smodal("hide");
			$("#lobk_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#lobk_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	data['tanggal_from'] = $("#lobk_tanggal_from").val();
	data['tanggal_to'] = $("#lobk_tanggal_to").val();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#lobk_list").append(
				json.html
			);
			$("#lobk_loading_bar").sload("true", json.tanggal + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
LOBKAction.prototype.finalize = function() {
	var num_rows = $("tbody#lobk_list tr").length;
	for (var i = 0; i < num_rows; i++)
		$("tbody#lobk_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
	$("#lobk_loading_modal").smodal("hide");
	$("#lobk_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#lobk_export_button").removeAttr("onclick");
	$("#lobk_export_button").attr("onclick", "lobk.export_xls()");
};
LOBKAction.prototype.cancel = function() {
	FINISHED = true;
};
LOBKAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#lobk_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#lobk_list tr:eq(" + i + ") td#nomor").text();
		var tanggal = $("tbody#lobk_list tr:eq(" + i + ") td#tanggal").text();
		var id_obat = $("tbody#lobk_list tr:eq(" + i + ") td#id_obat").text();
		var kode_obat = $("tbody#lobk_list tr:eq(" + i + ") td#kode_obat").text();
		var nama_obat = $("tbody#lobk_list tr:eq(" + i + ") td#nama_obat").text();
		var jumlah = $("tbody#lobk_list tr:eq(" + i + ") td#jumlah").text();
		d_data[i] = {
			"nomor" 	: nomor,
			"tanggal"	: tanggal,
			"id_obat" 	: id_obat,
			"kode_obat" : kode_obat,
			"nama_obat" : nama_obat,
			"jumlah" 	: jumlah
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#lobk_tanggal_from").val();
	data['tanggal_to'] = $("#lobk_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};