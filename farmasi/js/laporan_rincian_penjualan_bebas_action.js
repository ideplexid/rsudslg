function DPBAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPBAction.prototype.constructor = DPBAction;
DPBAction.prototype = new TableAction();
DPBAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#dpb_tanggal_from").val();
	data['tanggal_to'] = $("#dpb_tanggal_to").val();
	return data;
};
DPBAction.prototype.view = function() {
	if ($("#dpb_tanggal_from").val() == "" || $("#dpb_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#dpb_info").empty();
	$("#table_dpr tfoot").remove();
	$("#dpb_list").empty();
	$("#dpb_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#dpb_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
DPBAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#dpb_loading_modal").smodal("hide");
			$("#dpb_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#dpb_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#dpb_list").append(
				json.html
			);
			$("#dpb_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
DPBAction.prototype.finalize = function() {
	$("#dpb_loading_modal").smodal("hide");
	$("#dpb_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#dpb_export_button").removeAttr("onclick");
	$("#dpb_export_button").attr("onclick", "dpb.export_xls()");
};
DPBAction.prototype.cancel = function() {
	FINISHED = true;
};
DPBAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#dpb_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var label = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_label").text();
		if (label == "data") {
			var nomor = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_nomor").text();
			var tanggal = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_tanggal").text();
			var id = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_id").text();
			var lokasi = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_lokasi").text();
			var nama_pasien = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_nama_pasien").text();
			var no_resep = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_no_resep").text();
			var nama_obat = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_nama_obat").text();
			var harga = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_harga").text();
			var jumlah = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_jumlah").text();
			var subtotal = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_subtotal").text();
			d_data[i] = {
				"label" 		: label,
				"nomor" 		: nomor,
				"tanggal" 		: tanggal,
				"id" 			: id,
				"lokasi" 		: lokasi,
				"nama_pasien"	: nama_pasien,
				"no_resep"		: no_resep,
				"nama_obat"		: nama_obat,
				"harga"			: harga,
				"jumlah"		: jumlah,
				"subtotal"		: subtotal
			};
		} else {
			var total = $("tbody#dpb_list tr:eq(" + i + ") td#dpb_total").text();
			d_data[i] = {
				"label"			: "total",
				"total"			: total
			};
		}
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#dpb_tanggal_from").val();
	data['tanggal_to'] = $("#dpb_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};