function PenerimaanObatOPLAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenerimaanObatOPLAction.prototype.constructor = PenerimaanObatOPLAction;
PenerimaanObatOPLAction.prototype = new TableAction();
PenerimaanObatOPLAction.prototype.selected = function(json) {
	$("#penerimaan_obat_id_po").val(json.id);
	$("#penerimaan_obat_no_opl").val(json.nomor);
	$("#penerimaan_obat_id_vendor").val(json.id_vendor);
	$("#penerimaan_obat_nama_vendor").val(json.nama_vendor);
	$("#penerimaan_obat_no_bbm").focus();
	penerimaan_obat.show_detail_opl(json.id);
};