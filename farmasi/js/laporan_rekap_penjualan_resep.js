var rpr;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	rpr = new RPRAction(
		"rpr",
		"farmasi",
		"laporan_rekap_penjualan_resep",
		new Array()
	);
	$("#rpr_list").append(
		"<tr id='temp'>" +
			"<td colspan='13'><strong><small><center>DATA PENJUALAN RESEP BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});