function RPRAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RPRAction.prototype.constructor = RPRAction;
RPRAction.prototype = new TableAction();
RPRAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#rpr_tanggal_from").val();
	data['tanggal_to'] = $("#rpr_tanggal_to").val();
	data['jenis_pasien'] = $("#rpr_jenis_pasien").val();
	return data;
};
RPRAction.prototype.view = function() {
	if ($("#rpr_tanggal_from").val() == "" || $("#rpr_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#rpr_info").empty();
	$("#table_rpr tfoot").remove();
	$("#rpr_list").empty();
	$("#rpr_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#rpr_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
RPRAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#rpr_loading_modal").smodal("hide");
			$("#rpr_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#rpr_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#rpr_list").append(
				json.html
			);
			$("#rpr_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_dokter + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
RPRAction.prototype.finalize = function() {
	var num_rows = $("tbody#rpr_list tr").length;
	var t_total = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#rpr_list tr:eq(" + i + ") td#rpr_nomor").html("<small>" + (i + 1) + "</small>");
		var total = parseFloat($("tbody#rpr_list tr:eq(" + i + ") td#rpr_total").text());
		t_total += total;
	}
	$("#table_rpr").append(
		"<tfoot>" +
			"<tr>" +
				"<td colspan='12'><small><strong><center>T O T A L</center></strong></small></td>" +
				"<td><small><div align='right'><strong>" + t_total.formatMoney("2", ".", ",") + "</strong></div></small></td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#rpr_loading_modal").smodal("hide");
	$("#rpr_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#rpr_export_button").removeAttr("onclick");
	$("#rpr_export_button").attr("onclick", "rpr.export_xls()");
};
RPRAction.prototype.cancel = function() {
	FINISHED = true;
};
RPRAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#rpr_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_nomor").text();
		var nomor_transaksi = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_id").text();
		var nomor_resep = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_no_resep").text();
		var tanggal = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_tanggal").text();
		var nama_dokter = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_nama_dokter").text();
		var noreg_pasien = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_noreg_pasien").text();
		var nrm_pasien = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_nrm_pasien").text();
		var nama_pasien = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_nama_pasien").text();
		var alamat_pasien = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_alamat_pasien").text();
		var jenis = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_jenis").text();
		var uri = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_uri").text();
		var unit = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_unit").text();
		var total = $("tbody#rpr_list tr:eq(" + i + ") td#rpr_total").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"nomor_transaksi"	: nomor_transaksi,
			"nomor_resep"		: nomor_resep,
			"tanggal"			: tanggal,
			"nama_dokter"		: nama_dokter,
			"noreg_pasien"		: noreg_pasien,
			"nrm_pasien"		: nrm_pasien,
			"nama_pasien"		: nama_pasien,
			"alamat_pasien"		: alamat_pasien,
			"jenis"				: jenis,
			"uri"				: uri,
			"unit"				: unit,
			"total"				: total
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#rpr_tanggal_from").val();
	data['tanggal_to'] = $("#rpr_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};