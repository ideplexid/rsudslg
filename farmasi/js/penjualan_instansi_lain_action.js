function PenjualanInstansiLainAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenjualanInstansiLainAction.prototype.constructor = PenjualanInstansiLainAction;
PenjualanInstansiLainAction.prototype = new TableAction();
PenjualanInstansiLainAction.prototype.refresh_no_dpenjualan_instansi_lain = function() {
	var no = 1;
	var nor_dpenjualan_bebas = $("tbody#dpenjualan_instansi_lain_list").children("tr").length;
	for(var i = 0; i < nor_dpenjualan_bebas; i++) {
		var dr_prefix = $("tbody#dpenjualan_instansi_lain_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html(no++);
	}
};
PenjualanInstansiLainAction.prototype.show_add_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#help_instansi_lain").show();
	$("#penjualan_instansi_lain_nomor").val("");
	$("#penjualan_instansi_lain_nomor").removeAttr("disabled");
	$("#penjualan_instansi_lain_id").val("");
	$("#penjualan_instansi_lain_id_instansi_lain").val("");
	$("#penjualan_instansi_lain_nama_instansi_lain").val("");
	$("#penjualan_instansi_lain_nama_instansi_lain").removeAttr("disabled");
	$("#instansi_lain_browse").removeAttr("onclick");
	$("#instansi_lain_browse").attr("onclick", "instansi_lain.chooser('instansi_lain', 'instansi_lain_button', 'instansi_lain', instansi_lain)");
	$("#instansi_lain_browse").removeClass("btn-info");
	$("#instansi_lain_browse").removeClass("btn-inverse");
	$("#instansi_lain_browse").addClass("btn-info");
	$("#penjualan_instansi_lain_alamat_instansi_lain").val("");
	$("#penjualan_instansi_lain_telpon_instansi_lain").val("");
	$("#penjualan_instansi_lain_diskon").val("0,00");
	$("#penjualan_instansi_lain_diskon").removeAttr("disabled");
	$("#penjualan_instansi_lain_t_diskon").val("persen");
	$("#penjualan_instansi_lain_t_diskon").removeAttr("disabled");
	$("#penjualan_instansi_lain_markup").val("0");
	$("#dpenjualan_instansi_lain_add").show();
	dpal_num = 0;
	$("tbody#dpenjualan_instansi_lain_list").children().remove();
	$("#modal_alert_penjualan_instansi_lain_add_form").html("");
	$(".error_field").removeClass("error_field");
	this.refreshBiayaTotal();
	$("#penjualan_instansi_lain_save").show();
	$("#penjualan_instansi_lain_save").removeAttr("onclick");
	$("#penjualan_instansi_lain_save").attr("onclick", "penjualan_instansi_lain.save()");
	$("#penjualan_instansi_lain_save_print").show();
	$("#penjualan_instansi_lain_save_print").removeAttr("onclick");
	$("#penjualan_instansi_lain_save_print").attr("onclick", "penjualan_instansi_lain.save_print()");
	$("#penjualan_instansi_lain_ok").hide();
	$("#penjualan_instansi_lain_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
PenjualanInstansiLainAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nomor = $("#penjualan_instansi_lain_nomor").val();
	var nama_instansi_lain = $("#penjualan_instansi_lain_nama_instansi_lain").val();
	var diskon = $("#penjualan_instansi_lain_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var t_diskon = $("#penjualan_instansi_lain_t_diskon").val();
	var nord = $("tbody#dpenjualan_instansi_lain_list").children("tr").length;
	$(".error_field").removeClass("error_field");
	if (nomor == "") {
		valid = false;
		invalid_msg += "</br><strong>Nomor</strong> tidak boleh kosong";
		$("#penjualan_instansi_lain_nomor").addClass("error_field");
	} else if (!is_numeric(nomor)) {
		valid = false;
		invalid_msg += "</br><strong>Nomor</strong> seharusnya numerik (0-9)";
		$("#penjualan_instansi_lain_nomor").addClass("error_field");
	}
	if (nama_instansi_lain == "") {
		valid = false;
		invalid_msg += "</br><strong>Instansi Lain</strong> tidak boleh kosong";
		$("#penjualan_instansi_lain_nama_instansi_lain").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#penjualan_instansi_lain_diskon").addClass("error_field");
	}
	if (t_diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
		$("#penjualan_instansi_lain_t_diskon").addClass("error_field");
	} else if (t_diskon == "persen") {
		if (diskon > 100) {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> tidak boleh lebih dari 100%";
			$("#penjualan_instansi_lain_diskon").addClass("error_field");
		}
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "</br><strong>Detail Penjualan UP</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_penjualan_instansi_lain_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
PenjualanInstansiLainAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['super_command'] == "penjualan_instansi_lain";
	data['command'] = "save";
	data['id'] = $("#penjualan_instansi_lain_id").val();
	data['nomor_resep'] = $("#penjualan_instansi_lain_nomor").val();
	data['markup'] = parseFloat($("#penjualan_instansi_lain_markup").val()) / 100;
	data['nrm_pasien'] = $("#penjualan_instansi_lain_id_instansi_lain").val();
	data['nama_pasien'] = $("#penjualan_instansi_lain_nama_instansi_lain").val();
	data['alamat_pasien'] = $("#penjualan_instansi_lain_alamat_instansi_lain").val();
	data['no_telpon'] = $("#penjualan_instansi_lain_telpon_instansi_lain").val();
	data['diskon'] = $("#penjualan_instansi_lain_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	data['t_diskon'] = $("#penjualan_instansi_lain_t_diskon").val();
	var v_total = parseFloat($("#penjualan_instansi_lain_total").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['total'] = v_total;
	var detail_penjualan_instansi_lain = {};
	var nor_dpal =  $("tbody#dpenjualan_instansi_lain_list").children("tr").length;
	var pos = 1;
	for(var i = 0; i < nor_dpal; i++) {
		var dr_prefix = $("tbody#dpenjualan_instansi_lain_list").children("tr").eq(i).prop("id");
		var id_penjualan_obat_jadi = $("#" + dr_prefix + "_id").text();
		var id_obat = $("#" + dr_prefix + "_id_obat").text();
		var kode_obat = $("#" + dr_prefix + "_kode_obat").text();
		var nama_obat = $("#" + dr_prefix + "_nama_obat").text();
		var nama_jenis_obat = $("#" + dr_prefix + "_nama_jenis_obat").text();
		var jumlah = $("#" + dr_prefix + "_jumlah").text();
		var jumlah_lama = $("#" + dr_prefix + "_jumlah_lama").text();
		var satuan = $("#" + dr_prefix + "_satuan").text();
		var konversi = $("#" + dr_prefix + "_konversi").text();
		var satuan_konversi = $("#" + dr_prefix + "_satuan_konversi").text();
		var v_harga = parseFloat($("#" + dr_prefix + "_harga").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_embalase = parseFloat($("#" + dr_prefix + "_embalase").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_tusla = parseFloat($("#" + dr_prefix + "_tuslah").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_subtotal = parseFloat($("#" + dr_prefix + "_subtotal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		var d_data = {};
		d_data['id_penjualan_obat_jadi'] = id_penjualan_obat_jadi;
		d_data['id_obat'] = id_obat;
		d_data['nama_obat'] = nama_obat;
		d_data['kode_obat'] = kode_obat;
		d_data['nama_jenis_obat'] = nama_jenis_obat;
		d_data['jumlah'] = jumlah;
		d_data['jumlah_lama'] = jumlah_lama;
		d_data['satuan'] = satuan;
		d_data['konversi'] = konversi;
		d_data['satuan_konversi'] = satuan_konversi;
		d_data['harga'] = v_harga;
		d_data['embalase'] = v_embalase;
		d_data['tusla'] = v_tusla;
		d_data['subtotal'] = v_subtotal;
		d_data['pos'] = pos++;
		detail_penjualan_instansi_lain[i] = d_data;
	}
	data['detail_penjualan_instansi_lain'] = detail_penjualan_instansi_lain;
	return data;
};
PenjualanInstansiLainAction.prototype.save = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	showLoading();
	var self = this;
	var data = this.getSaveData();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);				
			if (json == null || json.success==0) {
				$("#modal_alert_penjualan_instansi_lain_add_form").html(
					"<div class='alert alert-block alert-info'>" +
						"<h4>Peringatan</h4>" +
						json.message +
					"</div>"
				);
			} else {
				$("#penjualan_instansi_lain_add_form").smodal("hide");
				self.print_prescription(json.id);
				self.view();
			}
			dismissLoading();
			$(".btn").removeAttr("disabled");
		}
	);
};
PenjualanInstansiLainAction.prototype.detail = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "penjualan_instansi_lain";
	data['command'] = "edit";
	data['id'] = id;
	data['readonly'] = true;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#help_instansi_lain").hide();
			$("#penjualan_instansi_lain_nomor").val(json.header.nomor_resep);
			$("#penjualan_instansi_lain_nomor").removeAttr("disabled");
			$("#penjualan_instansi_lain_nomor").attr("disabled", "disabled");
			$("#penjualan_instansi_lain_id_instansi_lain").val(json.header.nrm_pasien);
			$("#penjualan_instansi_lain_nama_instansi_lain").val(json.header.nama_pasien);
			$("#penjualan_instansi_lain_nama_instansi_lain").removeAttr("disabled");
			$("#penjualan_instansi_lain_nama_instansi_lain").attr("disabled", "disabled");
			$("#penjualan_instansi_lain_alamat_instansi_lain").val(json.header.alamat_pasien);
			$("#penjualan_instansi_lain_telpon_instansi_lain").val(json.header.no_telpon);
			$("#penjualan_instansi_lain_diskon").val((parseFloat(json.header.diskon)).formatMoney("2", ".", ","));
			$("#penjualan_instansi_lain_diskon").removeAttr("disabled");
			$("#penjualan_instansi_lain_diskon").attr("disabled", "disabled");
			$("#penjualan_instansi_lain_t_diskon").val(json.header.t_diskon);
			$("#penjualan_instansi_lain_t_diskon").removeAttr("disabled");
			$("#penjualan_instansi_lain_t_diskon").attr("disabled", "disabled");
			$("#instansi_lain_browse").removeAttr("onclick");
			$("#instansi_lain_browse").removeClass("btn-info");
			$("#instansi_lain_browse").removeClass("btn-inverse");
			$("#instansi_lain_browse").addClass("btn-inverse");
			$("#dpenjualan_instansi_lain_add").hide();
			$("tbody#dpenjualan_instansi_lain_list").html(json.dpal_list);
			dpal_num = json.dpal_num;
			$("#modal_alert_penjualan_instansi_lain_add_form").html("");
			$(".error_field").removeClass("error_field");
			self.refreshBiayaTotal();
			$("#penjualan_instansi_lain_save").removeAttr("onclick");
			$("#penjualan_instansi_lain_save").hide();
			$("#penjualan_instansi_lain_save_print").removeAttr("onclick");
			$("#penjualan_instansi_lain_save_print").hide();
			$("#penjualan_instansi_lain_ok").show();
			$("#penjualan_instansi_lain_add_form").smodal("show");
			$(".btn").removeAttr("disabled");
		}
	);
};
PenjualanInstansiLainAction.prototype.cancel = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "penjualan_instansi_lain";
	data['command'] = "save";
	data['id'] = id;
	data['dibatalkan'] = 1;
	bootbox.confirm(
		"Yakin membatalkan data Penjualan UP ini?",
		function(result) {
			if (result) {
				showLoading();
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
PenjualanInstansiLainAction.prototype.refreshBiayaTotal = function() {
	var biaya_total = 0;
	var nord = $("tbody#dpenjualan_instansi_lain_list").children("tr").length;
	for(var i = 0; i < nord; i++) {
		var prefix = $("tbody#dpenjualan_instansi_lain_list").children("tr").eq(i).prop("id");
		var v_subtotal = parseFloat($("#" + prefix + "_subtotal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		biaya_total += v_subtotal;
	}
	var diskon = $("#penjualan_instansi_lain_diskon").val();
	diskon = parseFloat(diskon.replace(/[^0-9-,]/g, '').replace(",", "."));
	var t_diskon = $("#penjualan_instansi_lain_t_diskon").val();
	if (t_diskon == "persen") {
		diskon = (diskon * biaya_total) / 100;
	}
	biaya_total = biaya_total - diskon;
	biaya_total = "Rp. " + parseFloat(biaya_total).formatMoney("2", ".", ",");
	$("#penjualan_instansi_lain_total").val(biaya_total);
};
PenjualanInstansiLainAction.prototype.refreshHargaAndSubtotal = function() {
	var self = this;
	if (need_margin_penjualan_request == 1 && (mode_margin == 0 || mode_margin == 1)) {
		need_margin_penjualan_request = 0;
		var data = this.getRegulerData();
		data['super_command'] = "penjualan_instansi_lain";
		data['command'] = "get_margin_penjualan";
		data['mode_margin'] = mode_margin;
		data['id_rekanan'] = $("#penjualan_instansi_lain_id_instansi_lain").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				self.refreshDetailInfo(json.margin_penjualan);
			}
		);
	} else {
		var markup = $("#penjualan_instansi_lain_markup").val();
		this.refreshDetailInfo(markup);
	}
};
PenjualanInstansiLainAction.prototype.refreshDetailInfo = function(markup) {
	$("#penjualan_instansi_lain_markup").val(markup);
	var nord = $("tbody#dpenjualan_instansi_lain_list").children("tr").length;
	for(var i = 0; i < nord; i++) {
		var prefix = $("tbody#dpenjualan_instansi_lain_list").children("tr").eq(i).prop("id");
		var hna = $("#" + prefix + "_hna").text();
		var embalase = $("#" + prefix + "_embalase").text();
		var tuslah = $("#" + prefix + "_tuslah").text();
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_embalase = parseFloat(embalase.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_tuslah = parseFloat(tuslah.replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_hja = v_hna + (markup * v_hna);
		var hja = "Rp. " + (parseFloat(v_hja)).formatMoney("2", ".", ",");
		$("#" + prefix + "_harga").text(hja);
		var v_jumlah = $("#" + prefix + "_jumlah").text();
		var v_subtotal = parseFloat(v_jumlah) * parseFloat(v_hja) + parseFloat(v_embalase) + parseFloat(v_tuslah);
		var subtotal = "Rp. " + (parseFloat(v_subtotal)).formatMoney("2", ".", ",");
		$("#" + prefix + "_subtotal").text(subtotal);
	}
	penjualan_instansi_lain.refreshBiayaTotal();
};
PenjualanInstansiLainAction.prototype.print_prescription = function(id) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "penjualan_instansi_lain";
	data['command'] = "print_prescription";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			smis_print(json);
			// webprint(json);
			$(".btn").removeAttr("disabled");
		}
	);
};