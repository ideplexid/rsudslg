function LRPPDAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LRPPDAction.prototype.constructor = LRPPDAction;
LRPPDAction.prototype = new TableAction();
LRPPDAction.prototype.view = function() {
	var self = this;
	var data = this.getRegulerData();
	data['tanggal_from'] = $("#lrppd_tanggal_from").val();
	data['tanggal_to'] = $("#lrppd_tanggal_to").val();
	data['id_vendor'] = $("#lrppd_id_vendor").val();
	data['command'] = "get_item_number";
	$("#lrppd_download").removeAttr("onclick");
	FINISHED = false;
	$("#lrppd_list").empty();
	$("#table_lrppd tfoot").remove();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#lrppd_loading_bar").sload("true", "Harap ditunggu...", 0);
			$("#lrppd_modal").smodal("show");
			self.fill_html(0, json.jumlah);
		}
	);
};
LRPPDAction.prototype.fill_html = function(num, limit) {
	if (num >= limit || FINISHED) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#loading_modal").smodal("hide");
			$("#lrppd_download").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['num'] = num;
	data['tanggal_from'] = $("#lrppd_tanggal_from").val();
	data['tanggal_to'] = $("#lrppd_tanggal_to").val();
	data['id_vendor'] = $("#lrppd_id_vendor").val();
	data['command'] = "get_item_info";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#lrppd_list").append(json.html);
			$("#lrppd_loading_bar").sload("true", json.no_faktur + " - " + json.nama_vendor + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fill_html(num + 1, limit);
		}
	);
};
LRPPDAction.prototype.finalize = function() {
	$("#lrppd_loading_bar").sload("true", "Finalisasi..", 100);
	var num_rows = $("tbody#lrppd_list tr").length;
	var t_subtotal = 0;
	var t_potongan = 0;
	var t_ppn = 0;
	var t_materai = 0;
	var t_tagihan = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#lrppd_list tr:eq(" + i + ") td#lrppd_nomor").html("<small>" + (i + 1) + "</small>");
		t_subtotal += parseFloat($("tbody#lrppd_list tr:eq(" + i + ") td#lrppd_t_subtotal").text());
		t_potongan += parseFloat($("tbody#lrppd_list tr:eq(" + i + ") td#lrppd_t_potongan").text());
		t_ppn += parseFloat($("tbody#lrppd_list tr:eq(" + i + ") td#lrppd_t_ppn").text());
		t_materai += parseFloat($("tbody#lrppd_list tr:eq(" + i + ") td#lrppd_t_materai").text());
		t_tagihan += parseFloat($("tbody#lrppd_list tr:eq(" + i + ") td#lrppd_t_tagihan").text());
	}
	var f_t_subtotal = t_subtotal.formatMoney("2", ".", ",");
	var f_t_potongan = t_potongan.formatMoney("2", ".", ",");
	var f_t_ppn = t_ppn.formatMoney("2", ".", ",");
	var f_t_materai = t_materai.formatMoney("2", ".", ",");
	var f_t_tagihan = t_tagihan.formatMoney("2", ".", ",");
	$("#table_lrppd").append(
		"<tfoot>" + 
			"<tr>" + 
				"<td colspan='7'><small><center><strong>T O T A L</strong></center></small></td>" +
				"<td><small><div align='right'><strong>" + f_t_subtotal + "</strong></div></small></td>" +
				"<td><small><div align='right'><strong>" + f_t_potongan + "</strong></div></small></td>" +
				"<td><small><div align='right'><strong>" + f_t_ppn + "</strong></div></small></td>" +
				"<td><small><div align='right'><strong>" + f_t_materai + "</strong></div></small></td>" +
				"<td><small><div align='right'><strong>" + f_t_tagihan + "</strong></div></small></td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#lrppd_modal").smodal("hide");
	$("#lrppd_loading_bar").sload("true", "", 0);
	$("#lrppd_download").attr("onclick", "lrppd.download()");
};
LRPPDAction.prototype.cancel = function() {
	FINISHED = true;
};
LRPPDAction.prototype.download = function() {
	var nor = $("#lrppd_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < nor; i++) {
		var dd_data = {};
		var nomor = $("#lrppd_list tr:eq(" + i + ") td#lrppd_nomor").text();
		var tanggal_datang = $("#lrppd_list tr:eq(" + i + ") td#lrppd_tanggal_datang").text();
		var tanggal = $("#lrppd_list tr:eq(" + i + ") td#lrppd_tanggal").text();
		var no_opl = $("#lrppd_list tr:eq(" + i + ") td#lrppd_no_opl").text();
		var no_bbm = $("#lrppd_list tr:eq(" + i + ") td#lrppd_no_bbm").text();
		var no_faktur = $("#lrppd_list tr:eq(" + i + ") td#lrppd_no_faktur").text();
		var nama_vendor = $("#lrppd_list tr:eq(" + i + ") td#lrppd_nama_vendor").text();
		var t_subtotal = $("#lrppd_list tr:eq(" + i + ") td#lrppd_t_subtotal").text();
		var t_potongan = $("#lrppd_list tr:eq(" + i + ") td#lrppd_t_potongan").text();
		var t_ppn = $("#lrppd_list tr:eq(" + i + ") td#lrppd_t_ppn").text();
		var t_materai = $("#lrppd_list tr:eq(" + i + ") td#lrppd_t_materai").text();
		var t_tagihan = $("#lrppd_list tr:eq(" + i + ") td#lrppd_t_tagihan").text();
		dd_data['nomor'] = nomor;
		dd_data['tanggal_datang'] = tanggal_datang;
		dd_data['tanggal'] = tanggal;
		dd_data['no_opl'] = no_opl;
		dd_data['no_bbm'] = no_bbm;
		dd_data['no_faktur'] = no_faktur;
		dd_data['nama_vendor'] = nama_vendor;
		dd_data['t_subtotal'] = t_subtotal;
		dd_data['t_potongan'] = t_potongan;
		dd_data['t_ppn'] = t_ppn;
		dd_data['t_materai'] = t_materai;
		dd_data['t_tagihan'] = t_tagihan;
		d_data[i] = dd_data;
	}
	var data = this.getRegulerData();
	data['command'] = "download";
	data['d_data'] = JSON.stringify(d_data);
	data['tanggal_from'] = $("#lrppd_tanggal_from").val();
	data['tanggal_to'] = $("#lrppd_tanggal_to").val();
	postForm(data);
};