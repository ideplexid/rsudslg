function LBRAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LBRAction.prototype.constructor = LBRAction;
LBRAction.prototype = new TableAction();
LBRAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#lbr_tanggal_from").val();
	data['tanggal_to'] = $("#lbr_tanggal_to").val();
	return data;
};
LBRAction.prototype.view = function() {
	if ($("#lbr_tanggal_from").val() == "" || $("#lbr_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#lbr_info").empty();
	$("#table_lbr tfoot").remove();
	$("#lbr_list").empty();
	$("#lbr_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#lbr_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
LBRAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#lbr_loading_modal").smodal("hide");
			$("#lbr_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#lbr_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#lbr_list").append(
				json.html
			);
			$("#lbr_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_dokter + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
LBRAction.prototype.finalize = function() {
	var num_rows = $("tbody#lbr_list tr").length;
	var t_total = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#lbr_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
		var total = parseFloat($("tbody#lbr_list tr:eq(" + i + ") td#biaya_racik").text());
		t_total += total;
	}
	$("#table_lbr").append(
		"<tfoot>" +
			"<tr>" +
				"<td colspan='8'><small><strong><center>T O T A L</center></strong></small></td>" +
				"<td><small><div align='right'><strong>" + t_total.formatMoney("2", ".", ",") + "</strong></div></small></td>" +
				"<td>&nbsp;</td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#lbr_loading_modal").smodal("hide");
	$("#lbr_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#lbr_export_button").removeAttr("onclick");
	$("#lbr_export_button").attr("onclick", "lbr.export_xls()");
};
LBRAction.prototype.cancel = function() {
	FINISHED = true;
};
LBRAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#lbr_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#lbr_list tr:eq(" + i + ") td#nomor").text();
		var id_resep = $("tbody#lbr_list tr:eq(" + i + ") td#id_resep").text();
		var noreg_pasien = $("tbody#lbr_list tr:eq(" + i + ") td#noreg_pasien").text();
		var nrm_pasien = $("tbody#lbr_list tr:eq(" + i + ") td#nrm_pasien").text();
		var tanggal = $("tbody#lbr_list tr:eq(" + i + ") td#tanggal").text();
		var nama_pasien = $("tbody#lbr_list tr:eq(" + i + ") td#nama_pasien").text();
		var nama_racikan = $("tbody#lbr_list tr:eq(" + i + ") td#nama_racikan").text();
		var jumlah_racikan = $("tbody#lbr_list tr:eq(" + i + ") td#jumlah_racikan").text();
		var biaya_racik = $("tbody#lbr_list tr:eq(" + i + ") td#biaya_racik").text();
		var jenis_pasien = $("tbody#lbr_list tr:eq(" + i + ") td#jenis_pasien").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"id_resep"			: id_resep,
			"noreg_pasien"		: noreg_pasien,
			"nrm_pasien"		: nrm_pasien,
			"tanggal"			: tanggal,
			"nama_pasien"		: nama_pasien,
			"nama_racikan"		: nama_racikan,
			"jumlah_racikan"	: jumlah_racikan,
			"biaya_racik"		: biaya_racik,
			"jenis_pasien"		: jenis_pasien
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#lbr_tanggal_from").val();
	data['tanggal_to'] = $("#lbr_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};