function LRPAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LRPAction.prototype.constructor = LRPAction;
LRPAction.prototype = new TableAction();
LRPAction.prototype.view = function() {
	var self = this;
	var data = this.getRegulerData();
	data['tanggal_from'] = $("#lrp_tanggal_from").val();
	data['tanggal_to'] = $("#lrp_tanggal_to").val();
	data['id_vendor'] = $("#lrp_id_vendor").val();
	data['command'] = "get_item_number";
	$("#lrp_download").removeAttr("onclick");
	FINISHED = false;
	$("#lrp_list").empty();
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#lrp_loading_bar").sload("true", "Harap ditunggu...", 0);
			$("#lrp_modal").smodal("show");
			self.fill_html(0, json.jumlah);
		}
	);
};
LRPAction.prototype.fill_html = function(num, limit) {
	if (num >= limit || FINISHED) {
		this.finalize();
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['num'] = num;
	data['tanggal_from'] = $("#lrp_tanggal_from").val();
	data['tanggal_to'] = $("#lrp_tanggal_to").val();
	data['id_vendor'] = $("#lrp_id_vendor").val();
	data['command'] = "get_item_info";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#lrp_list").append(json.html);
			$("#lrp_loading_bar").sload("true", json.no_faktur + " - " + json.nama_vendor + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fill_html(num + 1, limit);
		}
	);
};
LRPAction.prototype.finalize = function() {
	$("#lrp_loading_bar").sload("true", "Finalisasi..", 100);
	$("#lrp_modal").smodal("hide");
	$("#lrp_loading_bar").sload("true", "", 0);
	$("#lrp_download").attr("onclick", "lrp.download()");
};
LRPAction.prototype.cancel = function() {
	FINISHED = true;
};
LRPAction.prototype.download = function() {
	var nor = $("#lrp_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < nor; i++) {
		var dd_data = {};
		var first_col_val = $("#lrp_list tr:eq(" + i + ") td:eq(0)").text();
		if (first_col_val == "SUBTOTAL") {
			var label = "SUBTOTAL";
			var t_subtotal = $("#lrp_list tr:eq(" + i + ") td#lrp_t_subtotal").text();
			var t_potongan = $("#lrp_list tr:eq(" + i + ") td#lrp_t_potongan").text();
			var t_ppn = $("#lrp_list tr:eq(" + i + ") td#lrp_t_ppn").text();
			var t_materai = $("#lrp_list tr:eq(" + i + ") td#lrp_t_materai").text();
			var t_tagihan = $("#lrp_list tr:eq(" + i + ") td#lrp_t_tagihan").text();
			dd_data['t_subtotal'] = t_subtotal;
			dd_data['t_potongan'] = t_potongan;
			dd_data['t_ppn'] = t_ppn;
			dd_data['t_materai'] = t_materai;
			dd_data['t_tagihan'] = t_tagihan;
			dd_data['label'] = "subtotal";
		} else {
			var nomor = $("#lrp_list tr:eq(" + i + ") td#lrp_nomor").text();
			var tanggal_datang = $("#lrp_list tr:eq(" + i + ") td#lrp_tanggal_datang").text();
			var tanggal = $("#lrp_list tr:eq(" + i + ") td#lrp_tanggal").text();
			var no_opl = $("#lrp_list tr:eq(" + i + ") td#lrp_no_opl").text();
			var no_bbm = $("#lrp_list tr:eq(" + i + ") td#lrp_no_bbm").text();
			var no_faktur = $("#lrp_list tr:eq(" + i + ") td#lrp_no_faktur").text();
			var nama_vendor = $("#lrp_list tr:eq(" + i + ") td#lrp_nama_vendor").text();
			var id_obat = $("#lrp_list tr:eq(" + i + ") td#lrp_id_obat").text();
			var kode_obat = $("#lrp_list tr:eq(" + i + ") td#lrp_kode_obat").text();
			var nama_obat = $("#lrp_list tr:eq(" + i + ") td#lrp_nama_obat").text();
			var hna = $("#lrp_list tr:eq(" + i + ") td#lrp_hna").text().replace(/[^0-9-,]/g, '').replace(",", ".");
			var jumlah = $("#lrp_list tr:eq(" + i + ") td#lrp_jumlah").text().replace(/[^0-9-,]/g, '').replace(",", ".");
			var subtotal = $("#lrp_list tr:eq(" + i + ") td#lrp_total").text();
			var diskon = $("#lrp_list tr:eq(" + i + ") td#lrp_diskon").text();
			dd_data['nomor'] = nomor;
			dd_data['tanggal_datang'] = tanggal_datang;
			dd_data['tanggal'] = tanggal;
			dd_data['no_opl'] = no_opl;
			dd_data['no_bbm'] = no_bbm;
			dd_data['no_faktur'] = no_faktur;
			dd_data['nama_vendor'] = nama_vendor;
			dd_data['id_obat'] = id_obat;
			dd_data['kode_obat'] = kode_obat;
			dd_data['nama_obat'] = nama_obat;
			dd_data['hna'] = hna;
			dd_data['jumlah'] = jumlah;
			dd_data['subtotal'] = subtotal;
			dd_data['diskon'] = diskon;
			dd_data['label'] = "detail";
		}
		d_data[i] = dd_data;
	}
	var data = this.getRegulerData();
	data['command'] = "download";
	data['d_data'] = JSON.stringify(d_data);
	data['tanggal_from'] = $("#lrp_tanggal_from").val();
	data['tanggal_to'] = $("#lrp_tanggal_to").val();
	postForm(data);
};