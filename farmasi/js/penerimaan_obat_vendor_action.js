function VendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
VendorAction.prototype.constructor = VendorAction;
VendorAction.prototype = new TableAction();
VendorAction.prototype.selected = function(json) {
	$("#penerimaan_obat_id_vendor").val(json.id);
	$("#penerimaan_obat_nama_vendor").val(json.nama);
};