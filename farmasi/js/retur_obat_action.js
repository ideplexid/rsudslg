function ReturObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ReturObatAction.prototype.constructor = ReturObatAction;
ReturObatAction.prototype = new TableAction();
ReturObatAction.prototype.edit = function(id) {
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#retur_obat_id").val(json.id);
			$("#retur_obat_tanggal").val(json.tanggal);
			$("#retur_obat_nama_obat").val(json.nama_obat);
			$("#retur_obat_jenis").val(json.nama_jenis_obat);
			$("#retur_obat_produsen").val(json.produsen);
			$("#retur_obat_vendor").val(json.nama_vendor);
			$("#retur_obat_f_jumlah").val(json.jumlah + " " + json.satuan);
			$("#retur_obat_keterangan").val(json.keterangan);
			$("#retur_obat_accept").show();
			$("#retur_obat_return").show();
			$("#retur_obat_ok").hide();
			$("#retur_obat_add_form").smodal("show");
		}
	);
};
ReturObatAction.prototype.accept = function() {
	$("#retur_obat_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['push_command'] = "save";
	data['id'] = $("#retur_obat_id").val();
	data['status'] = "sudah";
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				$("#retur_obat_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ReturObatAction.prototype.return = function() {
	$("#retur_obat_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['push_command'] = "save";
	data['id'] = $("#retur_obat_id").val();
	data['status'] = "dikembalikan";
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				$("#retur_obat_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ReturObatAction.prototype.detail = function(id) {
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#retur_obat_id").val(json.id);
			$("#retur_obat_tanggal").val(json.tanggal);
			$("#retur_obat_nama_obat").val(json.nama_obat);
			$("#retur_obat_jenis").val(json.nama_jenis_obat);
			$("#retur_obat_produsen").val(json.produsen);
			$("#retur_obat_vendor").val(json.nama_vendor);
			$("#retur_obat_f_jumlah").val(json.jumlah + " " + json.satuan);
			$("#retur_obat_keterangan").val(json.keterangan);
			$("#retur_obat_accept").hide();
			$("#retur_obat_return").hide();
			$("#retur_obat_ok").show();
			$("#retur_obat_add_form").smodal("show");
		}
	);
};
ReturObatAction.prototype.restock = function(id) {
	var self = this;
	bootbox.confirm(
		"Yakin memasukkan Stok Retur ini ke Perbekalan?",
		function(result) {
			if (result) {
				var data = self.getRegulerData();
				data['command'] = "save";
				data['id'] = id;
				data['restok'] = "1";
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
					}
				);
			}
		}
	);
};
ReturObatAction.prototype.cancel_restock = function(id) {
	var self = this;
	bootbox.confirm(
		"Yakin membatalkan Restok Retur ini ke Perbekalan?",
		function(result) {
			if (result) {
				var data = self.getRegulerData();
				data['command'] = "save";
				data['id'] = id;
				data['restok'] = "0";
				$.post(
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
					}
				);
			}
		}
	);
};