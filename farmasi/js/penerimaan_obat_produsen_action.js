function ProdusenAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ProdusenAction.prototype.constructor = ProdusenAction;
ProdusenAction.prototype = new TableAction();
ProdusenAction.prototype.selected = function(json) {
	$("#dpenerimaan_obat_produsen").val(json.produsen);
	$("#dpenerimaan_obat_tanggal_exp").focus();
};