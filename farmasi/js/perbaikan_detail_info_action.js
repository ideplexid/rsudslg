function PerbaikanDetailInfoAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PerbaikanDetailInfoAction.prototype.constructor = PerbaikanDetailInfoAction;
PerbaikanDetailInfoAction.prototype = new TableAction();
PerbaikanDetailInfoAction.prototype.edit = function(id) {
	var data = this.getEditData(id);
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#perbaikan_detail_info_id").val(json.id);
			$("#perbaikan_detail_info_id_dobat_masuk").val(json.id_dobat_masuk);
			$("#perbaikan_detail_info_kode").val(json.kode_obat);
			$("#perbaikan_detail_info_nama").val(json.nama_obat);
			$("#perbaikan_detail_info_jenis").val(json.nama_jenis_obat);
			$("#perbaikan_detail_info_produsen_lama").val(json.produsen);
			$("#perbaikan_detail_info_produsen_baru").val(json.produsen);
			$("#perbaikan_detail_info_ed_lama").val(json.tanggal_exp);
			$("#perbaikan_detail_info_ed_baru").val(json.tanggal_exp);
			$("#perbaikan_detail_info_no_batch_lama").val(json.no_batch);
			$("#perbaikan_detail_info_no_batch_baru").val(json.no_batch);
			$("#perbaikan_detail_info_add_form").smodal("show");
		}
	);
};
PerbaikanDetailInfoAction.prototype.save = function() {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#perbaikan_detail_info_id").val();
	data['id_dobat_masuk'] = $("#perbaikan_detail_info_id_dobat_masuk").val();
	data['produsen'] = $("#perbaikan_detail_info_produsen_baru").val();
	data['tanggal_exp'] = $("#perbaikan_detail_info_ed_baru").val();
	data['no_batch'] = $("#perbaikan_detail_info_no_batch_baru").val();
	$.post(
		"",
		data,
		function() {
			self.view();
			$("#perbaikan_detail_info_add_form").smodal("hide");
		}
	);
};