function BahanAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
BahanAction.prototype.constructor = BahanAction;
BahanAction.prototype = new TableAction();
BahanAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
BahanAction.prototype.selected = function(json) {
	$("#bahan_id_bahan").val(json.header.id_obat);
	$("#bahan_kode_bahan").val(json.header.kode_obat);
	$("#bahan_name_bahan").val(json.header.nama_obat);
	$("#bahan_nama_bahan").val(json.header.nama_obat);
	$("#bahan_nama_jenis_bahan").val(json.header.nama_jenis_obat);
	$("#bahan_satuan").html(json.satuan_option);
	this.setDetailInfo();
};
BahanAction.prototype.setDetailInfo = function() {
	var part = $("#bahan_satuan").val().split("_");
	$("#bahan_konversi").val(part[0]);
	$("#bahan_satuan_konversi").val(part[1]);
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "sisa";
	data['command'] = "edit";
	data['id_obat'] = $("#bahan_id_bahan").val();
	data['satuan'] = $("#bahan_satuan").find(":selected").text();
	data['konversi'] = $("#bahan_konversi").val();
	data['satuan_konversi'] = $("#bahan_satuan_konversi").val();
	if ($("#resep_jenis").length)
		data['jenis_pasien'] = $("#resep_jenis").val();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#bahan_stok").val(json.sisa);
			$("#bahan_f_stok").val(json.sisa + " " + json.satuan);
			var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
			var data_rounded = self.getRegulerData();
			data['super_command'] = "bahan";
			data['command'] = "rounded_value";
			data['value'] = hna;
			$.post(
				"",
				data,
				function(rspns) {
					var jsn = getContent(rspns);
					if (jsn == null) return;
					hna = "Rp. " + (parseFloat(jsn)).formatMoney("2", ".", ",");
					$("#bahan_hna").val(hna);
					$("#bahan_markup").val(json.markup);
					$("#bahan_jumlah").focus();
				}
			);
		}
	);
};