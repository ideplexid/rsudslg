var lobk;
var FINISHED;
$(document).ready(function() {
	lobk = new LOBKAction(
		"lobk",
		"farmasi",
		"laporan_obat_keluar",
		new Array()
	);
	$("#lobk_loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#lobk_loading_modal").on("hide", function() {
		$("a.close").show();
	});
	$("tbody#lobk_list").append(
		"<tr>" +
			"<td colspan='6'><strong><center><small>DATA PENGELUARAN OBAT BELUM DIPROSES</small></center></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	})
});