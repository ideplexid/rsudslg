var retur;
var resep;
var dretur;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$("#smis-chooser-modal").on("show", function() {
		$("#smis-chooser-modal").removeClass("full_model");
		$("#smis-chooser-modal").addClass("full_model");
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("full_model");
	});
	resep = new ResepAction(
		"resep",
		"farmasi",
		"retur_penjualan_resep",
		new Array()
	);
	resep.setSuperCommand("resep");
	var dretur_columns = new Array("id", "id_retur_penjualan_resep", "id_stok_obat", "jumlah", "harga", "subtotal");
	dretur = new DReturPenjualanResepAction(
		"dretur",
		"farmasi",
		"retur_penjualan_resep",
		dretur_columns
	);
	var retur_columns = new Array("id", "id_penjualan_resep", "tanggal", "persentase_retur");
	retur = new ReturPenjualanResepAction(
		"retur",
		"farmasi",
		"retur_penjualan_resep",
		retur_columns
	);
	retur.setSuperCommand("retur");
	retur.view();
});