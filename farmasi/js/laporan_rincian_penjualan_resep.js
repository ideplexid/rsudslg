var dpr;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	dpr = new DPRAction(
		"dpr",
		"farmasi",
		"laporan_rincian_penjualan_resep",
		new Array()
	);
	$("#dpr_list").append(
		"<tr id='temp'>" +
			"<td colspan='18'><strong><small><center>DATA PENJUALAN RESEP BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});