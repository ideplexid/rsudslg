var pengaturan_margin_obat_jenis_pasien;
var obat;
$(document).ready(function() {
	obat = new ObatAction(
		"obat",
		"farmasi",
		"pengaturan_margin_obat_jenis_pasien",
		new Array()
	);
	obat.setSuperCommand("obat");
	var columns = new Array("id", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "slug", "jenis_pasien", "margin_jual");
	pengaturan_margin_obat_jenis_pasien = new TableAction(
		"pengaturan_margin_obat_jenis_pasien",
		"farmasi",
		"pengaturan_margin_obat_jenis_pasien",
		columns
	);
	pengaturan_margin_obat_jenis_pasien.view();
});