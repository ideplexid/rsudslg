function PenerimaanObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenerimaanObatAction.prototype.constructor = PenerimaanObatAction;
PenerimaanObatAction.prototype = new TableAction();
PenerimaanObatAction.prototype.show_add_form = function() {
	var data = this.getRegulerData();
	data['action'] = "penerimaan_obat_form";
	data['super_command'] = "";
	data['editable'] = "true";
	LoadSmisPage(data);
};
PenerimaanObatAction.prototype.show_add_non_opl_form = function() {
	var data = this.getRegulerData();
	data['action'] = "penerimaan_obat_non_opl_form";
	data['super_command'] = "";
	data['editable'] = "true";
	LoadSmisPage(data);
};
PenerimaanObatAction.prototype.show_detail_form = function(id) {
	if (id == 0 || id == "")
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_detail";
	data['limited'] = $("#penerimaan_obat_limited").val();
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#dpenerimaan_obat_list").html(json.html);
			self.update_total();
			row_num = json.row_num;
		}
	);
};
PenerimaanObatAction.prototype.show_detail_opl = function(id_opl) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_detail_opl";
	data['row_num'] = row_num;
	data['id_opl'] = id_opl;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#dpenerimaan_obat_list").html(json.html);
			self.update_total();
			row_num = json.row_num;
		}
	);
};
PenerimaanObatAction.prototype.get_footer = function() {
	if ($("#total").length != 0)
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_footer";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#dpenerimaan_obat_list").after(json.html);
		}
	);
};
PenerimaanObatAction.prototype.update_total = function() {
	var num_rows = $("tbody#dpenerimaan_obat_list").children("tr").length;
	var nomor = 1;
	var total = 0;
	for (var i = 0; i < num_rows; i++) {
		var deleted = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ")").attr("class") == "deleted" ? true : false;
		if (!deleted) {
			var subtotal = parseFloat($("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#subtotal").text());
			total += subtotal;
			$("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td:eq(0)").html("<small>" + nomor + "</small>");
			nomor++;
		}
	}
	var global_diskon = parseFloat($("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var global_t_diskon = $("#penerimaan_obat_t_diskon").val();
	if (global_t_diskon == "persen")
		global_diskon = global_diskon * total / 100;
	else
		global_diskon = global_diskon;
	global_diskon = Math.floor(global_diskon);
	var total_1 = total - global_diskon;
	var use_ppn = $("#penerimaan_obat_use_ppn").val();
	var ppn = Math.floor(total_1 * 0.1);
	if (use_ppn == 0)
		ppn = 0;
	var materai = parseFloat($("#penerimaan_obat_materai").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var ekspedisi = parseFloat($("#penerimaan_obat_ekspedisi").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var total_2 = total_1 + ppn + materai + ekspedisi;
	$("#total").html("<small><strong><div align='right'>" + total.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#diskon_global").html("<small><strong><div align='right'>" + global_diskon.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#total_2").html("<small><strong><div align='right'>" + total_1.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#ppn").html("<small><strong><div align='right'>" + ppn.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#materai").html("<small><strong><div align='right'>" + materai.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#ekspedisi").html("<small><strong><div align='right'>" + ekspedisi.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#tagihan").html("<small><strong><div align='right'>" + total_2.formatMoney("2", ".", ",") + "</div></strong></small>");
};
PenerimaanObatAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#penerimaan_obat_id").val();
	data['tipe'] = $("#penerimaan_obat_label").val();
	data['id_po'] = $("#penerimaan_obat_id_po").val();
	data['no_opl'] = $("#penerimaan_obat_no_opl").val();
	data['no_bbm'] = $("#penerimaan_obat_no_bbm").val();
	data['id_vendor'] = $("#penerimaan_obat_id_vendor").val();
	data['nama_vendor'] = $("#penerimaan_obat_nama_vendor").val();
	data['no_faktur'] = $("#penerimaan_obat_no_faktur").val();
	data['tanggal'] = $("#penerimaan_obat_tanggal").val();
	data['tanggal_tempo'] = $("#penerimaan_obat_tanggal_tempo").val();
	data['tanggal_datang'] = $("#penerimaan_obat_tanggal_datang").val();
	data['diskon'] = parseFloat($("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['t_diskon'] = $("#penerimaan_obat_t_diskon").val();
	data['keterangan'] = $("#penerimaan_obat_keterangan").val();
	data['materai'] = parseFloat($("#penerimaan_obat_materai").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['ekspedisi'] = parseFloat($("#penerimaan_obat_ekspedisi").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['total_tagihan'] = parseFloat($("#tagihan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['use_ppn'] = $("#penerimaan_obat_use_ppn").val();
	var detail = {};
	var nor = $("tbody#dpenerimaan_obat_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		d_data['id'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#id").text();
		d_data['id_dpo'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#id_dpo").text();
		d_data['id_obat'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#id_obat").text();
		d_data['kode_obat'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#kode_obat").text();
		d_data['nama_obat'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#nama_obat").text();
		d_data['medis'] = 1;
		d_data['inventaris'] = 0;
		d_data['nama_jenis_obat'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#nama_jenis_obat").text();
		d_data['stok_entri'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#stok_entri").text();
		d_data['jumlah_tercatat'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#jumlah_tercatat").text();
		d_data['jumlah'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#jumlah").text();
		d_data['sisa'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#jumlah").text();
		d_data['satuan'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#satuan").text();
		d_data['konversi'] = 1;
		d_data['satuan_konversi'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#satuan").text();
		d_data['hna'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#hna").text();
		d_data['selisih'] = 0;
		d_data['produsen'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#produsen").text();
		d_data['diskon'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#diskon").text();
		d_data['t_diskon'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#t_diskon").text();
		d_data['ed'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#tanggal_exp").text();
		d_data['nobatch'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#no_batch").text();
		detail[i] = d_data;
	}
	data['detail'] = detail;
	return data;
};
PenerimaanObatAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var no_opl = $("#penerimaan_obat_no_opl").val();
	var id_vendor = $("#penerimaan_obat_no_opl").val();
	var no_bbm = $("#penerimaan_obat_no_bbm").val();
	var no_faktur = $("#penerimaan_obat_no_faktur").val();
	var tanggal_faktur = $("#penerimaan_obat_tanggal").val();
	var tanggal_diterima = $("#penerimaan_obat_tanggal_datang").val();
	var tanggal_tempo = $("#penerimaan_obat_tanggal_tempo").val();
	var diskon = $("#penerimaan_obat_diskon").val();
	var nord = $("tbody#dpenerimaan_obat_list").children().length;
	var total_cr = 0;
	var total_tagihan = $("#tagihan").text().replace(/[^0-9-,]/g, '').replace(",", ".");
	for (var i = 0; i < nord; i++) {
		var jumlah_tercatat = parseFloat($("#dpenerimaan_obat_list tr:eq(" + i + ") td#jumlah_tercatat").text());
		var jumlah_diterima = parseFloat($("#dpenerimaan_obat_list tr:eq(" + i + ") td#jumlah").text());
		if (jumlah_tercatat > 0 && jumlah_diterima > 0)
			total_cr++;
	}
	$(".error_field").removeClass("error_field");
	if (no_opl == "") {
		valid = false;
		invalid_msg += "</br><strong>No. OPL</strong> tidak boleh kosong";
		$("#penerimaan_obat_no_opl").addClass("error_field");
	}
	if (id_vendor == "") {
		valid = false;
		invalid_msg += "</br><strong>Rekanan</strong> tidak boleh kosong";
		$("#penerimaan_obat_nama_vendor").addClass("error_field");
	}
	if (no_bbm == "") {
		valid = false;
		invalid_msg += "</br><strong>No. BBM</strong> tidak boleh kosong";
		$("#penerimaan_obat_no_bbm").addClass("error_field");
	}
	if (no_faktur == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Faktur</strong> tidak boleh kosong";
		$("#penerimaan_obat_no_faktur").addClass("error_field");
	}
	if (tanggal_faktur == "") {
		valid = false;
		invalid_msg += "</br><strong>Tgl. Faktur</strong> tidak boleh kosong";
		$("#penerimaan_Obat_tanggal").addClass("error_field");
	}
	if (tanggal_diterima == "") {
		valid = false;
		invalid_msg += "</br><strong>Tgl. Diterima</strong> tidak boleh kosong";
		$("#penerimaan_obat_tanggal_masuk").addClass("error_field");
	}
	if (tanggal_tempo == "") {
		valid = false;
		invalid_msg += "</br><strong>Jatuh Tempo</strong> tidak boleh kosong";
		$("#penerimaan_obat_tanggal_tempo").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#penerimaan_obat_diskon").addClass("error_field");
	}
	if (total_cr == 0) {
		valid = false;
		invalid_msg += "</br>Tidak terdapat <strong>detil penerimaan obat</strong> dengan info jumlah diterima > 0";
	}
	if (total_tagihan == "") {
		valid = false;
		invalid_msg += "</br>Gagal mendapatakan info <strong>Total Tagihan</strong>";	
	}
	if (!valid)
		bootbox.alert(invalid_msg);
	return valid;
};
PenerimaanObatAction.prototype.getUpdateInfoBBMData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#penerimaan_obat_id").val();
	data['tipe'] = $("#penerimaan_obat_label").val();
	data['id_po'] = $("#penerimaan_obat_id_po").val();
	data['no_opl'] = $("#penerimaan_obat_no_opl").val();
	data['no_bbm'] = $("#penerimaan_obat_no_bbm").val();
	data['id_vendor'] = $("#penerimaan_obat_id_vendor").val();
	data['nama_vendor'] = $("#penerimaan_obat_nama_vendor").val();
	data['no_faktur'] = $("#penerimaan_obat_no_faktur").val();
	data['tanggal'] = $("#penerimaan_obat_tanggal").val();
	data['tanggal_tempo'] = $("#penerimaan_obat_tanggal_tempo").val();
	data['tanggal_datang'] = $("#penerimaan_obat_tanggal_datang").val();
	data['diskon'] = parseFloat($("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['t_diskon'] = $("#penerimaan_obat_t_diskon").val();
	data['keterangan'] = $("#penerimaan_obat_keterangan").val();
	data['materai'] = parseFloat($("#penerimaan_obat_materai").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['ekspedisi'] = parseFloat($("#penerimaan_obat_ekspedisi").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['total_tagihan'] = parseFloat($("#tagihan").text().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['use_ppn'] = $("#penerimaan_obat_use_ppn").val();
	var detail = {};
	var nor = $("tbody#dpenerimaan_obat_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		d_data['id'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#id").text();
		d_data['diskon'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#diskon").text();
		d_data['t_diskon'] = $("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#t_diskon").text();
		detail[i] = d_data;
	}
	data['detail'] = detail;
	return data;
};
PenerimaanObatAction.prototype.update_info_bbm = function() {
	var self = this;
	var data = this.getUpdateInfoBBMData();
	bootbox.dialog({
		message : "Yakin melakukan memperbarui info BBM ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
PenerimaanObatAction.prototype.save = function() {
	if (!this.validate())
		return;
	var self = this;
	var data = this.getSaveData();
	bootbox.dialog({
		message : "Yakin melakukan penyimpanan BBM ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
PenerimaanObatAction.prototype.edit = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			if (json.id_po > 0)
				data['action'] = "penerimaan_obat_form";
			else
				data['action'] = "penerimaan_obat_non_opl_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['editable'] = "true";
			LoadSmisPage(data);
		}
	);
};
PenerimaanObatAction.prototype.edit_info = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			data['action'] = "penerimaan_obat_non_opl_form";
			data['super_command'] = "";
			data['editable'] = "true";
			data['limited'] = "true";
			data['id'] = json.id;
			data['id_po'] = json.id_po;
			data['no_opl'] = json.no_opl;
			data['no_bbm'] = json.no_bbm;
			data['id_vendor'] = json.id_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['no_faktur'] = json.no_faktur;
			data['tanggal'] = json.tanggal;
			data['tanggal_datang'] = json.tanggal_datang;
			data['tanggal_tempo'] = json.tanggal_tempo;
			data['diskon'] = json.diskon;
			data['t_diskon'] = json.t_diskon;
			data['materai'] = json.materai;
			data['ekspedisi'] = json.ekspedisi;
			data['keterangan'] = json.keterangan;
			data['use_ppn'] = json.use_ppn;
			LoadSmisPage(data);
		}
	);
};
PenerimaanObatAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			if (json.id_po > 0)
				data['action'] = "penerimaan_obat_form";
			else
				data['action'] = "penerimaan_obat_non_opl_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['id_po'] = json.id_po;
			data['no_opl'] = json.no_opl;
			data['no_bbm'] = json.no_bbm;
			data['id_vendor'] = json.id_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['no_faktur'] = json.no_faktur;
			data['tanggal'] = json.tanggal;
			data['tanggal_datang'] = json.tanggal_datang;
			data['tanggal_tempo'] = json.tanggal_tempo;
			data['diskon'] = json.diskon;
			data['t_diskon'] = json.t_diskon;
			data['materai'] = json.materai;
			data['ekspedisi'] = json.ekspedisi;
			data['keterangan'] = json.keterangan;
			data['use_ppn'] = json.use_ppn;
			data['editable'] = "false";
			LoadSmisPage(data);
		}
	);
};
PenerimaanObatAction.prototype.download_au53 = function(id) {
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['id'] = id;
	postForm(data);
};
PenerimaanObatAction.prototype.back = function() {
	var data = this.getRegulerData();
	data['action'] = "penerimaan_obat";
	data['super_command'] = "";
	data['kriteria'] = "";
	data['max'] = 5;
	data['number'] = 0;
	LoadSmisPage(data);
};