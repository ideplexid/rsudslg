var rpb;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	rpb = new RPBAction(
		"rpb",
		"farmasi",
		"laporan_rekap_penjualan_bebas",
		new Array()
	);
	$("#rpb_list").append(
		"<tr id='temp'>" +
			"<td colspan='6'><strong><small><center>DATA PENJUALAN BEBAS BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});