function ObatInstansiLainAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatInstansiLainAction.prototype.constructor = ObatInstansiLainAction;
ObatInstansiLainAction.prototype = new TableAction();
ObatInstansiLainAction.prototype.chooser = function(modal, elm_id, param,action, modal_title) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var data = this.getRegulerData();
	var dt = this.addChooserData(data);
	var self = this;
	data['super_command'] = param;
	$.post(
		'',
		dt,
		function(res) {
			show_chooser(self, param, res, action.getShowParentModalInChooser(), modal_title);
			action.view();
			action.focusSearch();
			this.current_chooser=action;
			CURRENT_SMIS_CHOOSER=action;
			$(".btn").removeAttr("disabled");
		}
	);
};
ObatInstansiLainAction.prototype.selected = function(json) {
	$("#dpenjualan_instansi_lain_id_obat").val(json.header.id_obat);
	$("#dpenjualan_instansi_lain_kode_obat").val(json.header.kode_obat);
	$("#dpenjualan_instansi_lain_name_obat").val(json.header.nama_obat);
	$("#dpenjualan_instansi_lain_nama_obat").val(json.header.nama_obat);
	$("#dpenjualan_instansi_lain_nama_jenis_obat").val(json.header.nama_jenis_obat);
	$("#dpenjualan_instansi_lain_satuan").html(json.satuan_option);
	this.setDetailInfo();
};
ObatInstansiLainAction.prototype.setDetailInfo = function() {
	var part = $("#dpenjualan_instansi_lain_satuan").val().split("_");
	$("#dpenjualan_instansi_lain_konversi").val(part[0]);
	$("#dpenjualan_instansi_lain_satuan_konversi").val(part[1]);
	var data = this.getRegulerData();
	data['super_command'] = "sisa";
	data['command'] = "edit";
	data['id_obat'] = $("#dpenjualan_instansi_lain_id_obat").val();
	data['satuan'] = $("#dpenjualan_instansi_lain_satuan").find(":selected").text();
	data['konversi'] = $("#dpenjualan_instansi_lain_konversi").val();
	data['satuan_konversi'] = $("#dpenjualan_instansi_lain_satuan_konversi").val();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#dpenjualan_instansi_lain_stok").val(json.sisa);
			$("#dpenjualan_instansi_lain_f_stok").val(json.sisa + " " + json.satuan);
			var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
			hna = "Rp. " + (parseFloat(hna)).formatMoney("2", ".", ",");
			$("#dpenjualan_instansi_lain_hna").val(hna);
			$("#dpenjualan_instansi_lain_markup").val(json.markup);
		}
	);
};