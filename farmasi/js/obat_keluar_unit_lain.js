var obat_keluar_unit_lain;
var dobat_keluar_unit_lain;
var obat;
var unit_lain;
var row_id;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	unit_lain = new UnitLainAction(
		"unit_lain",
		"farmasi",
		"obat_keluar_unit_lain",
		new Array()
	);
	unit_lain.setSuperCommand("unit_lain");
	obat = new ObatAction(
		"obat",
		"farmasi",
		"obat_keluar_unit_lain",
		new Array()
	);
	obat.setSuperCommand("obat");
	var dobat_keluar_unit_lain_columns = new Array("id", "id_obat_keluar_unit_lain", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan", "konversi", "satuan_konversi");
	dobat_keluar_unit_lain = new DObatKeluarUnitLainAction(
		"dobat_keluar_unit_lain",
		"farmasi",
		"obat_keluar_unit_lain",
		dobat_keluar_unit_lain_columns
	);
	var obat_keluar_unit_lain_columns = new Array("id", "tanggal", "id_unit", "kode_unit", "nama_unit", "keterangan");
	obat_keluar_unit_lain = new ObatKeluarUnitLainAction(
		"obat_keluar_unit_lain",
		"farmasi",
		"obat_keluar_unit_lain",
		obat_keluar_unit_lain_columns
	);
	obat_keluar_unit_lain.setSuperCommand("obat_keluar_unit_lain");
	obat_keluar_unit_lain.view();
});