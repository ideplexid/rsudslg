function RPUAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RPUAction.prototype.constructor = RPUAction;
RPUAction.prototype = new TableAction();
RPUAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#rpu_tanggal_from").val();
	data['tanggal_to'] = $("#rpu_tanggal_to").val();
	return data;
};
RPUAction.prototype.view = function() {
	if ($("#rpu_tanggal_from").val() == "" || $("#rpu_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#rpu_info").empty();
	$("#table_rpu tfoot").remove();
	$("#rpu_list").empty();
	$("#rpu_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#rpu_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
RPUAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#rpu_loading_modal").smodal("hide");
			$("#rpu_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#rpu_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#rpu_list").append(
				json.html
			);
			$("#rpu_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " - " + json.nama_pasien + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
RPUAction.prototype.finalize = function() {
	var num_rows = $("tbody#rpu_list tr").length;
	var t_total = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#rpu_list tr:eq(" + i + ") td#rpu_nomor").html("<small>" + (i + 1) + "</small>");
		var total = parseFloat($("tbody#rpu_list tr:eq(" + i + ") td#rpu_total").text());
		t_total += total;
	}
	$("#table_rpu").append(
		"<tfoot>" +
			"<tr>" +
				"<td colspan='5'><small><strong><center>T O T A L</center></strong></small></td>" +
				"<td><small><div align='right'><strong>" + t_total.formatMoney("2", ".", ",") + "</strong></div></small></td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#rpu_loading_modal").smodal("hide");
	$("#rpu_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#rpu_export_button").removeAttr("onclick");
	$("#rpu_export_button").attr("onclick", "rpu.export_xls()");
};
RPUAction.prototype.cancel = function() {
	FINISHED = true;
};
RPUAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#rpu_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#rpu_list tr:eq(" + i + ") td#rpu_nomor").text();
		var nomor_transaksi = $("tbody#rpu_list tr:eq(" + i + ") td#rpu_id").text();
		var nomor_resep = $("tbody#rpu_list tr:eq(" + i + ") td#rpu_no_resep").text();
		var tanggal = $("tbody#rpu_list tr:eq(" + i + ") td#rpu_tanggal").text();
		var nama_pasien = $("tbody#rpu_list tr:eq(" + i + ") td#rpu_nama_pasien").text();
		var total = $("tbody#rpu_list tr:eq(" + i + ") td#rpu_total").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"nomor_transaksi"	: nomor_transaksi,
			"nomor_resep"		: nomor_resep,
			"tanggal"			: tanggal,
			"nama_pasien"		: nama_pasien,
			"total"				: total
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#rpu_tanggal_from").val();
	data['tanggal_to'] = $("#rpu_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};