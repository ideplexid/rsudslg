function DPenerimaanObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPenerimaanObatAction.prototype.constructor = DPenerimaanObatAction;
DPenerimaanObatAction.prototype = new TableAction();
DPenerimaanObatAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#dpenerimaan_obat_name_obat").val();
	var jumlah_tercatat = $("#dpenerimaan_obat_jumlah_tercatat").val();
	var jumlah = $("#dpenerimaan_obat_jumlah").val();
	var hna = $("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var diskon = $("#dpenerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var produsen = $("#dpenerimaan_obat_produsen").val();
	var tanggal_exp = $("#dpenerimaan_obat_tanggal_exp").val();
	var no_batch = $("#dpenerimaan_obat_no_batch").val();
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_nama_obat").addClass("error_field");
	}
	if (jumlah_tercatat == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_jumlah_tercatat").addClass("error_field");
	} else if (!is_numeric(jumlah_tercatat)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> seharusnya numerik (0-9)";
		$("#dpenerimaan_obat_jumlah_tercatat").addClass("error_field");
	} else if (jumlah_tercatat <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> seharusnya > 0";
		$("#dpenerimaan_obat_jumlah_tercatat").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> seharusnya numerik (0-9)";
		$("#dpenerimaan_obat_jumlah").addClass("error_field");
	} else if (jumlah <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> seharusnya > 0";
		$("#dpenerimaan_obat_jumlah").addClass("error_field");
	}
	if (hna == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_hna").addClass("error_field");
	} else if (jumlah <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Harga</strong> tidak boleh 0";
		$("#dpenerimaan_obat_hna").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_diskon").addClass("error_field");
	}
	if (produsen == "") {
		valid = false;
		invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_produsen").addClass("error_field");
	}
	if (tanggal_exp == "") {
		valid = false;
		invalid_msg += "</br><strong>Tanggal Exp.</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_tanggal_exp").addClass("error_field");
	}
	if (no_batch == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Batch</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_no_batch").addClass("error_field");
	}
	if (!valid)
		bootbox.alert(invalid_msg);
	return valid;
};
DPenerimaanObatAction.prototype.clear = function() {
	$("#dpenerimaan_obat_row_num").val("");
	$("#dpenerimaan_obat_id_dpo").val("");
	$("#dpenerimaan_obat_id_obat").val("");
	$("#dpenerimaan_obat_kode_obat").val("");
	$("#dpenerimaan_obat_name_obat").val("");
	$("#dpenerimaan_obat_nama_obat").val("");
	$("#dpenerimaan_obat_nama_jenis_obat").val("");
	$("#dpenerimaan_obat_jumlah_tercatat").val("");
	$("#dpenerimaan_obat_jumlah").val("");
	$("#dpenerimaan_obat_satuan").val("");
	$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
	$("#dpenerimaan_obat_hna").val("Rp. 0,00");
	$("#dpenerimaan_obat_diskon").val("0,00");
	$("#dpenerimaan_obat_t_diskon").val("persen");
	$("#dpenerimaan_obat_produsen").val("");
	$("#dpenerimaan_obat_tanggal_exp").val("");
	$("#dpenerimaan_obat_no_batch").val("");
};
DPenerimaanObatAction.prototype.save = function() {
	if (!this.validate())
		return;
	var r_num = $("#dpenerimaan_obat_row_num").val();
	var id_dpo = $("#dpenerimaan_obat_id_dpo").val();
	var id_obat = $("#dpenerimaan_obat_id_obat").val();
	var kode_obat = $("#dpenerimaan_obat_kode_obat").val();
	var nama_obat = $("#dpenerimaan_obat_name_obat").val();
	var nama_jenis_obat = $("#dpenerimaan_obat_nama_jenis_obat").val();
	var stok_entri = $("#dpenerimaan_obat_stok_entri").val();
	var f_stok_entri = parseFloat(stok_entri).formatMoney("0", ".", ",");
	var jumlah_tercatat = $("#dpenerimaan_obat_jumlah_tercatat").val();
	var f_jumlah_tercatat = parseFloat(jumlah_tercatat).formatMoney("0", ".", ",");
	var jumlah = $("#dpenerimaan_obat_jumlah").val();
	var f_jumlah = parseFloat(jumlah).formatMoney("0", ".", ",");
	var satuan = $("#dpenerimaan_obat_satuan").val();
	var konversi = $("#dpenerimaan_obat_konversi").val();
	var satuan_konversi = $("#dpenerimaan_obat_satuan_konversi").val();
	var hna = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var f_hna = (hna / 1.1).formatMoney("2", ".", ",");
	var use_ppn = $("#penerimaan_obat_use_ppn").val();
	if (use_ppn == 0)
		f_hna = hna.formatMoney("2", ".", ",");
	var diskon = parseFloat($("#dpenerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var t_diskon = $("#dpenerimaan_obat_t_diskon").val();
	var f_diskon = diskon + " %";
	if (t_diskon == "nominal")
		f_diskon = diskon.formatMoney("2", ".", ",");
	var produsen = $("#dpenerimaan_obat_produsen").val();
	var tanggal_exp = $("#dpenerimaan_obat_tanggal_exp").val();
	var no_batch = $("#dpenerimaan_obat_no_batch").val();
	var subtotal = jumlah * (hna / 1.1);
	if (use_ppn == 0)
		subtotal = jumlah * hna;
	if (t_diskon == "persen")
		subtotal = subtotal - (diskon * subtotal / 100);
	else
		subtotal = subtotal - diskon;
	subtotal = Math.round(subtotal);
	var f_subtotal = subtotal.formatMoney("2", ".", ",");
	
	if (r_num != "") {
		$("tr#data_" + r_num + " td#jumlah_tercatat").html(jumlah_tercatat);
		$("tr#data_" + r_num + " td#f_jumlah_tercatat").html("<small><div align='right'>" + f_jumlah_tercatat + "</div></small>");
		$("tr#data_" + r_num + " td#jumlah").html(jumlah);
		$("tr#data_" + r_num + " td#f_jumlah").html("<small><div align='right'>" + f_jumlah + "</div></small>");
		$("tr#data_" + r_num + " td#hna").html(hna);
		$("tr#data_" + r_num + " td#f_hna").html("<small><div align='right'>" + f_hna + "</div></small>");
		$("tr#data_" + r_num + " td#diskon").html(diskon);
		$("tr#data_" + r_num + " td#t_diskon").html(t_diskon);
		$("tr#data_" + r_num + " td#f_diskon").html("<small><div align='right'>" + f_diskon + "</div></small>");
		$("tr#data_" + r_num + " td#produsen").html(produsen);
		$("tr#data_" + r_num + " td#tanggal_exp").html(tanggal_exp);
		$("tr#data_" + r_num + " td#no_batch").html(no_batch);
		$("tr#data_" + r_num + " td#subtotal").html(subtotal);
		$("tr#data_" + r_num + " td#f_subtotal").html("<small><div align='right'>" + f_subtotal + "</div></small>");
	} else {
		var html_edit_action = "<a href='#' onclick='dpenerimaan_obat.edit(" + row_num + ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a>";
		var html_del_action = "<a href='#' onclick='dpenerimaan_obat.del(" + row_num + ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a>";
		$("tbody#dpenerimaan_obat_list").append(
			"<tr id='data_" + row_num + "'>" +
				"<td id='nomor'></td>" +
				"<td id='id' style='display: none;'></td>" +
				"<td id='id_dpo' style='display: none;'>" + id_dpo + "</td>" +
				"<td id='id_obat' style='display: none;'>" + id_obat + "</td>" +
				"<td id='kode_obat'><small>" + kode_obat + "</small></td>" +
				"<td id='nama_obat'><small>" + nama_obat + "</small></td>" +
				"<td id='nama_jenis_obat'><small>" + nama_jenis_obat + "</small></td>" +
				"<td id='stok_entri' style='display: none;'>" + stok_entri + "</td>" +
				"<td id='f_stok_entri'><small><div align='right'>" + f_stok_entri + "</div></small></td>" +
				"<td id='jumlah_tercatat' style='display: none;'>" + jumlah_tercatat + "</td>" +
				"<td id='f_jumlah_tercatat'><small><div align='right'>" + f_jumlah_tercatat + "</div></small></td>" +
				"<td id='jumlah' style='display: none;'>" + jumlah + "</td>" +
				"<td id='f_jumlah'><small><div align='right'>" + f_jumlah + "</div></small></td>" +
				"<td id='satuan'><small>" + satuan + "</small></td>" +
				"<td id='hna' style='display: none;'>" + hna + "</td>" +
				"<td id='f_hna'><small>" + f_hna + "</small></td>" +
				"<td id='diskon' style='display: none;'>" + diskon + "</td>" +
				"<td id='t_diskon' style='display: none;'>" + t_diskon + "</td>" +
				"<td id='f_diskon'><small><div align='right'>" + f_diskon + "</div></small></td>" +
				"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
				"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
				"<td id='produsen' style='display: none;'>" + produsen + "</td>" +
				"<td id='no_batch' style='display: none;'>" + no_batch + "</td>" +
				"<td id='tanggal_exp' style='display: none;'>" + tanggal_exp + "</td>" +
				"<td>" + html_edit_action + "</td>" +
				"<td>" + html_del_action + "</td>" +
			"</tr>"
		);
		row_num++;
	}
	this.clear();
	penerimaan_obat.update_total();
	this.setEditMode("false");
};
DPenerimaanObatAction.prototype.setEditMode = function(enable) {
	if (enable == "true") {
		$("#dpenerimaan_obat_nama_obat").removeClass("smis-one-option-input");
		$("#dpenerimaan_obat_nama_obat").removeAttr("disabled");
		$("#dpenerimaan_obat_nama_obat").attr("disabled", "disabled");
		$("#dopl_browse").hide();
		$("#penerimaan_obat_form_save").hide();
		$("#penerimaan_obat_form_update").show();
		$("#penerimaan_obat_form_cancel").show();
	} else {
		$("#dpenerimaan_obat_nama_obat").removeClass("smis-one-option-input");
		$("#dpenerimaan_obat_nama_obat").addClass("smis-one-option-input");
		$("#dpenerimaan_obat_nama_obat").removeAttr("disabled");
		$("#dopl_browse").show();
		$("#penerimaan_obat_form_save").show();
		$("#penerimaan_obat_form_update").hide();
		$("#penerimaan_obat_form_cancel").hide();
	}
};
DPenerimaanObatAction.prototype.edit = function(r_num) {
	var kode_obat = $("tr#data_" + r_num + " td#kode_obat").text();
	var nama_obat = $("tr#data_" + r_num + " td#nama_obat").text();
	var nama_jenis_obat = $("tr#data_" + r_num + " td#nama_jenis_obat").text();
	var jumlah_tercatat = parseFloat($("tr#data_" + r_num + " td#jumlah_tercatat").text());
	var jumlah = parseFloat($("tr#data_" + r_num + " td#jumlah").text());
	var satuan = $("tr#data_" + r_num + " td#satuan").text();
	var hna = "Rp. " + parseFloat($("tr#data_" + r_num + " td#hna").text()).formatMoney("2", ".", ",");
	var hpp = "Rp. " + (parseFloat($("tr#data_" + r_num + " td#hna").text()) / 1.1).formatMoney("2", ".", ",");
	var use_ppn = $("#penerimaan_obat_use_ppn").val();
	if (use_ppn == 0)
		hna = hpp;
	var diskon = $("tr#data_" + r_num + " td#diskon").text();
	var t_diskon = $("tr#data_" + r_num + " td#t_diskon").text();
	var produsen = $("tr#data_" + r_num + " td#produsen").text();
	var tanggal_exp = $("tr#data_" + r_num + " td#tanggal_exp").text();
	var no_batch = $("tr#data_" + r_num + " td#no_batch").text();
	this.setEditMode("true");
	$("#dpenerimaan_obat_row_num").val(r_num);
	$("#dpenerimaan_obat_kode_obat").val(kode_obat);
	$("#dpenerimaan_obat_name_obat").val(nama_obat);
	$("#dpenerimaan_obat_nama_obat").val(nama_obat);
	$("#dpenerimaan_obat_nama_jenis_obat").val(nama_jenis_obat);
	$("#dpenerimaan_obat_jumlah_tercatat").val(jumlah_tercatat);
	$("#dpenerimaan_obat_jumlah").val(jumlah);
	$("#dpenerimaan_obat_satuan").val(satuan);
	$("#dpenerimaan_obat_hpp").val(hpp);
	$("#dpenerimaan_obat_hna").val(hna);
	$("#dpenerimaan_obat_diskon").val(diskon);
	$("#dpenerimaan_obat_t_diskon").val(t_diskon);
	$("#dpenerimaan_obat_produsen").val(produsen);
	$("#dpenerimaan_obat_tanggal_exp").val(tanggal_exp);
	$("#dpenerimaan_obat_no_batch").val(no_batch);
};
DPenerimaanObatAction.prototype.empty = function(r_num) {
	$("tr#data_" + r_num + " td#jumlah").html(0);
	$("tr#data_" + r_num + " td#f_jumlah").html("<small><div align='right'>0</div></small>");
	$("tr#data_" + r_num + " td#jumlah_tercatat").html(0);
	$("tr#data_" + r_num + " td#f_jumlah_tercatat").html("<small><div align='right'>0</div></small>");
	$("tr#data_" + r_num + " td#subtotal").html(0);
	$("tr#data_" + r_num + " td#f_subtotal").html("<small><div align='right'>0,00</div></small>");
	penerimaan_obat.update_total();
};
DPenerimaanObatAction.prototype.del = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	penerimaan_obat.update_total();
};
DPenerimaanObatAction.prototype.cancel = function() {
	this.clear();
	this.setEditMode("false");
};