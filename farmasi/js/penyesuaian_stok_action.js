function PenyesuaianStokAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenyesuaianStokAction.prototype.constructor = PenyesuaianStokAction;
PenyesuaianStokAction.prototype = new TableAction();
PenyesuaianStokAction.prototype.edit = function(id) {
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#penyesuaian_stok_id_stok_obat").val(id);
			$("#penyesuaian_stok_nama_obat").val(json.nama_obat);
			$("#penyesuaian_stok_nama_jenis").val(json.nama_jenis_obat);
			$("#penyesuaian_stok_jenis_stok").val(json.label.replace("_", " ").toUpperCase());
			$("#penyesuaian_stok_produsen").val(json.produsen);
			$("#penyesuaian_stok_f_jumlah_lama").val(json.sisa + " " + json.satuan);
			$("#penyesuaian_stok_jumlah_lama").val(json.sisa);
			$("#penyesuaian_stok_jumlah_baru").val(json.sisa);
			$("#penyesuaian_stok_satuan").val(json.satuan);
			$("#penyesuaian_stok_keterangan").val("");
			$("#penyesuaian_stok_no_bbm").val(json.no_bbm);
			$("#penyesuaian_stok_tanggal_datang").val(json.tanggal_datang);
			$("#modal_alert_penyesuaian_stok_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#penyesuaian_stok_add_form").smodal("show");
		}
	);
};
PenyesuaianStokAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var tanggal = $("#penyesuaian_stok_tanggal").val();
	var jumlah_aktual = $("#penyesuaian_stok_jumlah_baru").val();
	var jumlah_tercatat = $("#penyesuaian_stok_jumlah_tercatat").val();
	var keterangan = $("#penyesuaian_stok_keterangan").val();
	$(".error_field").removeClass("error_field");
	if (jumlah_aktual == "") {
		valid = false;
		invalid_msg += "</br><strong>Jml. Aktual</strong> tidak boleh kosong";
		$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
	} else if (!is_numeric(jumlah_aktual)) {
		valid = false;
		invalid_msg += "</br><strong>Jml. Aktual</strong> seharusnya numerik (0-9)";
		$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
	} else if (parseFloat(jumlah_aktual) == parseFloat(jumlah_tercatat)) {
		valid = false;
		invalid_msg += "</br><strong>Jml. Aktual</strong> dan <strong>Jml. Tercatat</strong> tidak terdapat selisih";
		$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
	}
	if (tanggal == "") {
		valid = false;
		invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
		$("#penyesuaian_stok_tanggal").addClass("error_field");
	}
	if (keterangan == "") {
		valid = false;
		invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
		$("#penyesuaian_stok_keterangan").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_penyesuaian_stok_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
PenyesuaianStokAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	$("#penyesuaian_stok_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = "";
	data['id_stok_obat'] = $("#penyesuaian_stok_id_stok_obat").val();
	data['tanggal'] = $("#penyesuaian_stok_tanggal").val();
	data['jumlah_baru'] = $("#penyesuaian_stok_jumlah_baru").val();
	data['jumlah_lama'] = $("#penyesuaian_stok_jumlah_lama").val();
	data['keterangan'] = $("#penyesuaian_stok_keterangan").val();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				$("#penyesuaian_stok_add_form").smodal("show");
			} else {
				self.view();
				riwayat_penyesuaian_stok.view();
			}
			dismissLoading();
		}
	);
};