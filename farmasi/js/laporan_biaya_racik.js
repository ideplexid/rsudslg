var lbr;
var FINISHED;
$(document).ready(function() {
	$('.mydatetime').datetimepicker({ 
		minuteStep: 1
	});
	lbr = new LBRAction(
		"lbr",
		"farmasi",
		"laporan_biaya_racik",
		new Array()
	);
	$("#lbr_list").append(
		"<tr id='lbr_temp'>" +
			"<td colspan='10'><strong><small><center>DATA BIAYA RACIK BELUM DIPROSES</center></small></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	});
});