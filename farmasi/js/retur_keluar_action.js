function ReturAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ReturAction.prototype.constructor = ReturAction;
ReturAction.prototype = new TableAction();
ReturAction.prototype.show_add_form = function() {
	var today = new Date();
	$("#retur_id").val("");
	$("#retur_no_faktur").val("");
	$("#retur_id_obat_masuk").val("");
	$("#retur_id_vendor").val("");
	$("#retur_nama_vendor").val("");
	$("#retur_tanggal_faktur").val("");
	$("tbody#dretur_list").children("tr").remove();
	$("#modal_alert_retur_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#retur_add_form").smodal("show");
};
ReturAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var id_obat_masuk = $("#retur_id_obat_masuk").val();
	var retur_exist = false;
	var nor = $("tbody#dretur_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var prefix = $("tbody#dretur_list").children("tr").eq(i).prop("id");
		var jumlah_retur = parseFloat($("#" + prefix + "_jumlah_retur").text());
		if (jumlah_retur > 0) {
			retur_exist = true;
			break;
		}
	}
	$(".error_field").removeClass("error_field");
	if (id_obat_masuk == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Faktur</strong> tidak boleh kosong";
		$("#retur_no_faktur").addClass("error_field");
	}
	if (!retur_exist) {
		valid = false;
		invalid_msg += "</br>Tidak ada obat yang diretur";
	}
	if (!valid) {
		$("#modal_alert_retur_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ReturAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	showLoading();
	$("#retur_add_form").smodal("hide");
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "retur";
	data['command'] = "save";
	data['id'] = $("#retur_id").val();
	data['id_obat_masuk'] = $("#retur_id_obat_masuk").val();
	data['id_vendor'] = $("#retur_id_vendor").val();
	data['nama_vendor'] = $("#retur_nama_vendor").val();
	data['no_faktur'] = $("#retur_no_faktur").val();
	data['tanggal_faktur'] = $("#retur_tanggal_faktur").val();
	var detail = {};
	var nor = $("tbody#dretur_list").children("tr").length;
	var j = 0;
	for(var i = 0; i < nor; i++) {
		var prefix = $("tbody#dretur_list").children("tr").eq(i).prop("id");
		var jumlah_retur = parseFloat($("#" + prefix + "_jumlah_retur").text());
		if (jumlah_retur > 0) {
			var id_stok_obat = $("#" + prefix + "_id_stok_obat").text();
			var keterangan = $("#" + prefix + "_keterangan").text();
			var d_data = {};
			d_data['id_stok_obat'] = id_stok_obat;
			d_data['jumlah'] = jumlah_retur;
			d_data['keterangan'] = keterangan;
			detail[j] = d_data;
			j++;
		}
	}
	data['detail'] = detail;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				$("#retur_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ReturAction.prototype.detail = function(id) {
	var data = this.getRegulerData();
	data['super_command'] = "retur";
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#v_retur_id").val(json.header.id);
			$("#v_retur_tanggal").val(json.header.tanggal);
			$("#v_retur_no_faktur").val(json.header.no_faktur);
			$("#v_retur_id_obat_masuk").val(json.header.id_obat_masuk);
			$("#v_retur_id_vendor").val(json.header.id_vendor);
			$("#v_retur_nama_vendor").val(json.header.nama_vendor);
			$("#v_retur_tanggal_faktur").val(json.header.tanggal_faktur);
			$("tbody#v_dretur_list").html(json.detail_list);
			$("#v_retur_add_form").smodal("show");
		}
	);
};
ReturAction.prototype.cancel = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "retur";
	data['command'] = "save";
	data['id'] = id;
	data['dibatalkan'] = "1";
	bootbox.confirm(
		"Yakin membatalkan Retur Penjualan Resep ini?",
		function(result) {
			if (result) {
				showLoading();
				$.post(	
					"",
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
ReturAction.prototype.export_xls = function(id) {
	var data = this.getRegulerData();
	data['id'] = id;
	data['super_command'] = "retur";
	data['command'] = "export_xls";
	postForm(data);
};