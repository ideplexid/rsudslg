var retur;
var obat_masuk;
var dretur;
$(document).ready(function() {
	$("#smis-chooser-modal").addClass("full_model");
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$(".mydate").datepicker();
	obat_masuk = new ObatMasukAction(
		"obat_masuk",
		"farmasi",
		"retur_keluar",
		new Array()
	);
	obat_masuk.setSuperCommand("obat_masuk");
	var dretur_columns = new Array("id", "id_retur_obat", "id_stok_obat", "jumlah", "keterangan");
	dretur = new DReturAction(
		"dretur",
		"farmasi",
		"retur_keluar",
		dretur_columns
	);
	var retur_columns = new Array("id", "id_obat_masuk", "tanggal", "id_vendor", "nama_vendor", "no_faktur", "tanggal_faktur");
	retur = new ReturAction(
		"retur",
		"farmasi",
		"retur_keluar",
		retur_columns
	);
	retur.setSuperCommand("retur");
	retur.view();
});