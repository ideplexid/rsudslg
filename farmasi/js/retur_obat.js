var retur_obat;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$("#smis-chooser-modal").on("show", function() {
		$("#smis-chooser-modal").removeClass("full_model");
		$("#smis-chooser-modal").addClass("full_model");
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("full_model");
	});
	var retur_obat_columns = new Array("id", "f_id", "unit", "tanggal", "id_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "turunan", "keterangan", "status", "id_vendor", "nama_vendor", "produsen", "restok");
	retur_obat = new ReturObatAction(
		"retur_obat",
		"farmasi",
		"retur_obat_unit",
		retur_obat_columns
	);
	retur_obat.view();
});