function LMOAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LMOAction.prototype.constructor = LMOAction;
LMOAction.prototype = new TableAction();
LMOAction.prototype.getRegulerData =  function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#lmo_tanggal_from").val();
	data['tanggal_to'] = $("#lmo_tanggal_to").val();
	data['jenis_filter'] = $("#lmo_jenis_filter").val();
	data['id_obat'] = $("#lmo_id_obat").val();
	data['nama_obat'] = $("#lmo_nama_obat").val();
	data['nama_jenis_obat'] = $("#lmo_nama_jenis_obat").val();
	data['unit'] = $("#lmo_unit").val();
	return data;
};
LMOAction.prototype.view = function() {
	if ($("#lmo_tanggal_from").val() == "" || $("#lmo_tanggal_to").val() == "")
		return;
	$("#lmo_info").empty();
	$("#lmo_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#lmo_loading_modal").smodal("show");
	FINISHED = false;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null || json.jumlah == 0) {
				$("#lmo_loading_modal").smodal("hide");
				$("tbody#lmo_list").html(
					"<tr>" +
						"<td colspan='9'><strong><center><small>TIDAK TERDAPAT DATA MUTASI OBAT</small></center></strong></td>" +
					"</tr>"
				);
				return;
			}
			$("#lmo_list").empty();
			self.fillHtml(0, json.jumlah);
		}
	);
};
LMOAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#lmo_loading_modal").smodal("hide");
			$("#lmo_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info_obat";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#lmo_list").append(
				json.html
			);
			$("#lmo_loading_bar").sload("true", json.tanggal + " - " + json.unit + " - " + json.nama_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
LMOAction.prototype.finalize = function() {
	var num_rows = $("tbody#lmo_list tr").length;
	for (var i = 0; i < num_rows; i++)
		$("tbody#lmo_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
	$("#lmo_loading_modal").smodal("hide");
	$("#lmo_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#lmo_export_button").removeAttr("onclick");
	$("#lmo_export_button").attr("onclick", "lmo.export_xls()");
};
LMOAction.prototype.cancel = function() {
	FINISHED = true;
};
LMOAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#lmo_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#lmo_list tr:eq(" + i + ") td#nomor").text();
		var unit = $("tbody#lmo_list tr:eq(" + i + ") td#unit").text();
		var id_obat = $("tbody#lmo_list tr:eq(" + i + ") td#id_obat").text();
		var kode_obat = $("tbody#lmo_list tr:eq(" + i + ") td#kode_obat").text();
		var nama_obat = $("tbody#lmo_list tr:eq(" + i + ") td#nama_obat").text();
		var nama_jenis_obat = $("tbody#lmo_list tr:eq(" + i + ") td#nama_jenis_obat").text();
		var jumlah = $("tbody#lmo_list tr:eq(" + i + ") td#jumlah").text();
		var satuan = $("tbody#lmo_list tr:eq(" + i + ") td#satuan").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"unit" 				: unit,
			"id_obat" 			: id_obat,
			"kode_obat" 		: kode_obat,
			"nama_obat" 		: nama_obat,
			"nama_jenis_obat"	: nama_jenis_obat,
			"jumlah"			: jumlah,
			"satuan" 		: satuan
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};