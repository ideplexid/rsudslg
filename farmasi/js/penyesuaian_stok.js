var penyesuaian_stok;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$(".mydate").datepicker();
	var penyesuaian_stok_columns = new Array("id", "id_stok_obat", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan");
	penyesuaian_stok = new PenyesuaianStokAction(
		"penyesuaian_stok",
		"farmasi",
		"detail_stok_obat",
		penyesuaian_stok_columns
	);
	penyesuaian_stok.view();
});