var lpo;
var lpo_obat;
var lpo_jenis_obat;
var FINISHED;
$(document).ready(function() {
	lpo_obat = new LPOObatAction(
		"lpo_obat",
		"farmasi",
		"laporan_persediaan_obat",
		new Array()
	);
	lpo_obat.setSuperCommand("lpo_obat");
	lpo_jenis_obat = new LPOJenisObatAction(
		"lpo_jenis_obat",
		"farmasi",
		"laporan_persediaan_obat",
		new Array()
	);
	lpo_jenis_obat.setSuperCommand("lpo_jenis_obat");
	lpo = new LPOAction(
		"lpo",
		"farmasi",
		"laporan_persediaan_obat",
		new Array()
	);
	$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").hide();
	$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").hide();
	$("#lpo_jenis_filter").on("change", function() {
		var jenis_filter = $("#lpo_jenis_filter").val();
		$("#lpo_id_obat").val("");
		$("#lpo_nama_obat").val("");
		$("#lpo_kode_jenis_obat").val("");
		$("#lpo_jenis_obat").val("");
		if (jenis_filter == "semua") {
			$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").hide();
		} else if (jenis_filter == "per_obat") {
			$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").show();
			$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").hide();
		} else if (jenis_filter == "per_jenis") {
			$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").show();
		}
	});
	$("#loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#loading_modal").on("hide", function() {
		$("a.close").show();
	});
	$("tbody#lpo_list").append(
		"<tr>" +
			"<td colspan='11'><strong><center><small>DATA PERSEDIAAN OBAT BELUM DIPROSES</small></center></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	})
});