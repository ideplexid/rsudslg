function LRPPDVendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LRPPDVendorAction.prototype.constructor = LRPPDVendorAction;
LRPPDVendorAction.prototype = new TableAction();
LRPPDVendorAction.prototype.selected = function(json) {
	$("#lrppd_id_vendor").val(json.id);
	$("#lrppd_nama_vendor").val(json.nama);
};
LRPPDVendorAction.prototype.clear = function() {
	$("#lrppd_id_vendor").val("%%");
	$("#lrppd_nama_vendor").val("SEMUA");
};