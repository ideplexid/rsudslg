function LMOObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LMOObatAction.prototype.constructor = LMOObatAction;
LMOObatAction.prototype = new TableAction();
LMOObatAction.prototype.selected = function(json) {
	$("#lmo_id_obat").val(json.id);
	$("#lmo_nama_obat").val(json.nama);
};