function LMOULObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LMOULObatAction.prototype.constructor = LMOULObatAction;
LMOULObatAction.prototype = new TableAction();
LMOULObatAction.prototype.selected = function(json) {
	$("#lmoul_id_obat").val(json.id);
	$("#lmoul_nama_obat").val(json.nama);
};