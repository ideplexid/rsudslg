var penerimaan_obat;
var dpenerimaan_obat;
var vendor;
var obat;
var produsen;
var row_num;

$(document).ready(function() {
	$(".mydate").datepicker();
	produsen = new ProdusenAction(
		"produsen",
		"farmasi",
		"penerimaan_obat_form",
		new Array()
	);
	produsen.setSuperCommand("produsen");
	obat = new ObatAction(
		"obat",
		"farmasi",
		"penerimaan_obat_non_opl_form",
		new Array()
	);
	obat.setSuperCommand("obat");
	vendor = new VendorAction(
		"vendor",
		"farmasi",
		"penerimaan_obat_non_opl_form",
		new Array()
	);
	vendor.setSuperCommand("vendor");
	dpenerimaan_obat = new DPenerimaanObatAction(
		"dpenerimaan_obat",
		"farmasi",
		"penerimaan_obat_non_opl_form",
		new Array("hpp", "hna", "diskon")
	);
	penerimaan_obat = new PenerimaanObatAction(
		"penerimaan_obat",
		"farmasi",
		"penerimaan_obat_non_opl_form",
		new Array("diskon", "materai", "ekspedisi")
	);

	var id = $("#penerimaan_obat_id").val();
	row_num = 0;
	penerimaan_obat.get_footer();
	penerimaan_obat.show_detail_form(id);
	dpenerimaan_obat.setEditMode("true");
	$("#penerimaan_obat_no_opl").focus();

	$("#penerimaan_obat_diskon").on("change", function() {
		var diskon = $("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#penerimaan_obat_diskon").val("0,00");
		}
	});
	
	$("#penerimaan_obat_t_diskon").on("change", function() {
		var diskon = $("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#penerimaan_obat_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			bootbox.alert("<b>Diskon</b> tidak boleh melebihi 100%");
			return;
		}
		penerimaan_obat.update_total();
	});
	
	$("#penerimaan_obat_materai").on("change", function() {
		var diskon = $("#penerimaan_obat_materai").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#penerimaan_obat_materai").val("0,00");
		}
	});

	$("#penerimaan_obat_ekspedisi").on("change", function() {
		var diskon = $("#penerimaan_obat_ekspedisi").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#penerimaan_obat_ekspedisi").val("0,00");
		}
	});
	
	$("#penerimaan_obat_diskon").on("keyup", function() {
		var diskon = $("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#penerimaan_obat_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			bootbox.alert("<b>Diskon</b> tidak boleh melebihi 100%");
			return;
		}
		penerimaan_obat.update_total();
	});

	$("#penerimaan_obat_materai").on("keyup", function() {
		penerimaan_obat.update_total();
	});

	$("#penerimaan_obat_ekspedisi").on("keyup", function() {
		penerimaan_obat.update_total();
	});

	$("#dpenerimaan_obat_diskon").on("change", function() {
		var diskon = $("#dpenerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#dpenerimaan_obat_diskon").val("0,00");
		}
	});

	$("#penerimaan_obat_no_opl").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_no_bbm").focus();
		}
	});
	$("#penerimaan_obat_no_bbm").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_no_faktur");
		}
	});
	$("#penerimaan_obat_no_faktur").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_tanggal").focus();
		}
	});
	$("#penerimaan_obat_tanggal").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_tanggal_datang").focus();
		}
	});
	$("#penerimaan_obat_tanggal_datang").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_tanggal_tempo").focus();
		}
	});
	$("#penerimaan_obat_tanggal_tempo").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_diskon").focus();
			$("div.datepicker").hide();
		}
	});
	$("#penerimaan_obat_tanggal_diskon").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_materai").focus();
		}
	});
	$("#penerimaan_obat_tanggal_materai").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_ekspedisi").focus();
		}
	});
	$("#penerimaan_obat_use_ppn").change(function() {
		var hna = $("#dpenerimaan_obat_hpp").val();
		var hpp = $("#dpenerimaan_obat_hna").val();
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		console.log(hna + " | " + hpp + " | " + use_ppn);
		if (hna != "") {
			hna = parseFloat($("#dpenerimaan_obat_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			hpp = hna * 1.1;
			if (use_ppn == 0)
				hpp = hna;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hna").val(f_hpp);
		} else if (hpp != "") {
			hpp = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			hna = hpp / 1.1;
			if (use_ppn == 0)
				hna = hpp;
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hpp").val(f_hna);
		}
		penerimaan_obat.update_total();
	});

	$("#dpenerimaan_obat_nama_obat").keypress(function(e) {
		if (e.which == 13) {		
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			$("#dpenerimaan_obat_jumlah_tercatat").focus();
		}
	});
	$("#dpenerimaan_obat_jumlah_tercatat").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_jumlah").focus();
		}
	});
	$("#dpenerimaan_obat_jumlah").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_hpp").focus();
		}
	});
	$("#dpenerimaan_obat_hpp").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_hna").focus();
		}
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hpp").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hna = parseFloat($("#dpenerimaan_obat_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hpp = hna * 1.1;
			if (use_ppn == false)
				hpp = hna;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hna").val(f_hpp);
		}
	});
	$("#dpenerimaan_obat_hna").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_diskon").focus();
		}
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hna").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hpp = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hna = hpp / 1.1;
			if (use_ppn == false)
				hna = hpp;
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hpp").val(f_hna);
		}
	});
	$("#dpenerimaan_obat_hpp").on("change", function() {
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hpp").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hna = parseFloat($("#dpenerimaan_obat_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hpp = hna * 1.1;
			if (use_ppn == false)
				hpp = hna;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hna").val(f_hpp);
		}
	});
	$("#dpenerimaan_obat_hna").on("change", function() {
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hna").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hpp = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hna = hpp / 1.1;
			if (use_ppn == false)
				hna = hpp;
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hpp").val(f_hna);
		}
	});
	$("#dpenerimaan_obat_diskon").keydown(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_t_diskon").focus();
		}
	});
	$("#dpenerimaan_obat_t_diskon").keypress(function(e) {
		e.preventDefault();
		if (e.which == 13) {
			$("#dpenerimaan_obat_produsen").focus();
		}
	});
	$("#dpenerimaan_obat_produsen").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_tanggal_exp").focus();
		}
	});
	$("#dpenerimaan_obat_tanggal_exp").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_no_batch").focus();
			$("div.datepicker").hide();
		}
	});
	$("#dpenerimaan_obat_no_batch").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_form_save").trigger("click");
			$("#dpenerimaan_obat_nama_obat").focus();
		}
	});

	var obat_data = obat.getViewData();
	$("#dpenerimaan_obat_nama_obat").typeahead({
		minLength	: 3,
		source		: function (query, process) {
			var $items = new Array;
			$items = [""];                
			obat_data['kriteria'] = $('#dpenerimaan_obat_nama_obat').val();
			$.ajax({
				url		: "",
				type	: "POST",
				data	: obat_data,
				success	: function(response) {
					var json = getContent(response);
					var t_data = json.d.data;
					$items = [""];      				
					$.map(t_data, function(data) {
						var group;
						group = {
							id		: data.id,
							name	: data.nama, 
							kode	: data.kode, 
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = "";
								value +=  this.kode + " - " + this.name;
								if(typeof(this.level) != "undefined") {
									value += " <span class='pull-right muted'>";
									value += this.level;
									value += "</span>";
								}
								return String.prototype.replace.apply("<div class='typeaheadiv'>" + value + "</div>", arguments);
							}
						};
						$items.push(group);
					});
					process($items);
				}
			});
		},
		updater		: function (item) {
			var item = JSON.parse(item);  
			obat.select(item.id);  
			$("#dpenerimaan_obat_nama_obat").focus();       
			return item.name;
		}
	});
	var produsen_data = produsen.getViewData();
	$("#dpenerimaan_obat_produsen").typeahead({
		minLength	: 3,
		source		: function (query, process) {
			var $items = new Array;
			$items = [""];                
			produsen_data['kriteria'] = $('#dpenerimaan_obat_produsen').val();
			$.ajax({
				url		: "",
				type	: "POST",
				data	: produsen_data,
				success	: function(response) {
					var json = getContent(response);
					var t_data = json.dbtable.data;
					$items = [""];      				
					$.map(t_data, function(data) {
						var group;
						group = {
							id		: data.produsen,
							name	: data.produsen,
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = "";
								value +=  this.name;
								if(typeof(this.level) != "undefined") {
									value += " <span class='pull-right muted'>";
									value += this.level;
									value += "</span>";
								}
								return String.prototype.replace.apply("<div class='typeaheadiv'>" + value + "</div>", arguments);
							}
						};
						$items.push(group);
					});
					process($items);
				}
			});
		},
		updater		: function (item) {
			var item = JSON.parse(item);  
			$("#dpenerimaan_obat_produsen").val(item.id);
			$("#dpenerimaan_obat_tanggal_exp").focus();   
			return item.name;
		}
	});
});