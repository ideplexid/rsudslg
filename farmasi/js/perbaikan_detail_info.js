var perbaikan_detail_info;
$(document).ready(function() {
	$(".mydate").datepicker();
	var perbaikan_detail_info_columns = new Array("id", "id_dobat_masuk", "kode_obat", "nama_obat", "nama_jenis_obat", "produsen_lama", "produsen_baru", "ed_lama", "ed_baru", "no_batch_lama", "no_batch_baru");
	perbaikan_detail_info = new PerbaikanDetailInfoAction(
		"perbaikan_detail_info",
		"farmasi",
		"perbaikan_detail_info",
		perbaikan_detail_info_columns
	);
	perbaikan_detail_info.view();
});