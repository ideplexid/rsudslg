function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	$("#dpenerimaan_obat_id_obat").val(json.id);
	$("#dpenerimaan_obat_kode_obat").val(json.kode);
	$("#dpenerimaan_obat_nama_obat").val(json.nama);
	$("#dpenerimaan_obat_name_obat").val(json.nama);
	$("#dpenerimaan_obat_medis").val(json.medis);
	$("#dpenerimaan_obat_inventaris").val(json.inventaris);
	$("#dpenerimaan_obat_nama_jenis_obat").val(json.nama_jenis_barang);
	$("#dpenerimaan_obat_jumlah").val("");
	$("#dpenerimaan_obat_satuan").val(json.satuan);
	$("#dpenerimaan_obat_konversi").val(json.konversi);
	$("#dpenerimaan_obat_satuan_konversi").val(json.satuan_konversi);
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_last_item";
	data['super_command'] = "";
	data['id_obat'] = json.id;
	$.post(
		"",
		data,
		function(last_item_response) {
			var json_last_item = JSON.parse(last_item_response);
			if (json_last_item == null) {
				$("#dpenerimaan_obat_hna").val("Rp. 0,00");
				$("#dpenerimaan_obat_harga").val("Rp. 0,00");
				$("#dpenerimaan_obat_total_hna").val("Rp. 0,00");
				$("#dpenerimaan_obat_stok_terkini").val("0");
			} else {
				var hpp = parseFloat(json_last_item.data.hna);
				var hna = hpp / 1.1;
				var satuan = json_last_item.data.satuan;
				var konversi = json_last_item.data.konversi;
				var satuan_konversi = json_last_item.data.satuan_konversi;
				var produsen = json_last_item.data.produsen;
				hpp = "Rp. " + parseFloat(hpp).formatMoney("2", ".", ",");
				hna = "Rp. " + parseFloat(hna).formatMoney("2", ".", ",");
				$("#dpenerimaan_obat_satuan").val(satuan);
				$("#dpenerimaan_obat_konversi").val(konversi);
				$("#dpenerimaan_obat_satuan_konversi").val(satuan_konversi);
				$("#dpenerimaan_obat_produsen").val(produsen);
				$("#dpenerimaan_obat_hpp").val(hna);
				$("#dpenerimaan_obat_hna").val(hpp);
				var data_stok = self.getRegulerData();
				data_stok['super_command'] = "stok";
				data_stok['command'] = "edit";
				data_stok['id_obat'] = json.id;
				data_stok['satuan'] = json_last_item.data.satuan;
				data_stok['konversi'] = json_last_item.data.konversi;
				data_stok['satuan_konversi'] = json_last_item.data.satuan_konversi;
				$.post(
					"",
					data_stok,
					function(sisa_response) {
						var json_stok = getContent(sisa_response);
						if (json_stok == null) return;
						$("#dpenerimaan_obat_stok_entri").val(json_stok.sisa);
					}
				);
			}
			$("#dpenerimaan_obat_jumlah_tercatat").focus();
		}
	);
};