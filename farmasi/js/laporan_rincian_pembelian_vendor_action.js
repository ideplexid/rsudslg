function LRPVendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LRPVendorAction.prototype.constructor = LRPVendorAction;
LRPVendorAction.prototype = new TableAction();
LRPVendorAction.prototype.selected = function(json) {
	$("#lrp_id_vendor").val(json.id);
	$("#lrp_nama_vendor").val(json.nama);
};
LRPVendorAction.prototype.clear = function() {
	$("#lrp_id_vendor").val("%%");
	$("#lrp_nama_vendor").val("SEMUA");
};