function RPBAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RPBAction.prototype.constructor = RPBAction;
RPBAction.prototype = new TableAction();
RPBAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['tanggal_from'] = $("#rpb_tanggal_from").val();
	data['tanggal_to'] = $("#rpb_tanggal_to").val();
	return data;
};
RPBAction.prototype.view = function() {
	if ($("#rpb_tanggal_from").val() == "" || $("#rpb_tanggal_to").val() == "")
		return;
	var self = this;
	FINISHED = false;
	$("#rpb_info").empty();
	$("#table_rpb tfoot").remove();
	$("#rpb_list").empty();
	$("#rpb_loading_bar").sload("true", "Harap ditunggu...", 0);
	$("#rpb_loading_modal").smodal("show");
	var data = this.getRegulerData();
	data['command'] = "get_jumlah";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			self.fillHtml(0, json.jumlah);
		}
	);
};
RPBAction.prototype.fillHtml = function(num, limit) {
	if (FINISHED || num == limit) {
		if (FINISHED == false && num == limit) {
			this.finalize();
		} else {
			$("#rpb_loading_modal").smodal("hide");
			$("#rpb_info").html(
				"<div class='alert alert-block alert-inverse'>" +
					 "<center><strong>PROSES DIBATALKAN</strong></center>" +
				 "</div>"
			);
			$("#rpb_export_button").removeAttr("onclick");
		}
		return;
	}
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info";
	data['num'] = num;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("tbody#rpb_list").append(
				json.html
			);
			$("#rpb_loading_bar").sload("true", json.nomor_transaksi + " - " + json.nomor_resep + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
			self.fillHtml(num + 1, limit);
		}
	);
};
RPBAction.prototype.finalize = function() {
	var num_rows = $("tbody#rpb_list tr").length;
	var t_total = 0;
	for (var i = 0; i < num_rows; i++) {
		$("tbody#rpb_list tr:eq(" + i + ") td#rpb_nomor").html("<small>" + (i + 1) + "</small>");
		var total = parseFloat($("tbody#rpb_list tr:eq(" + i + ") td#rpb_total").text());
		t_total += total;
	}
	$("#table_rpb").append(
		"<tfoot>" +
			"<tr>" +
				"<td colspan='5'><small><strong><center>T O T A L</center></strong></small></td>" +
				"<td><small><div align='right'><strong>" + t_total.formatMoney("2", ".", ",") + "</strong></div></small></td>" +
			"</tr>" +
		"</tfoot>"
	);
	$("#rpb_loading_modal").smodal("hide");
	$("#rpb_info").html(
		"<div class='alert alert-block alert-info'>" +
			 "<center><strong>PROSES SELESAI</strong></center>" +
		 "</div>"
	);
	$("#rpb_export_button").removeAttr("onclick");
	$("#rpb_export_button").attr("onclick", "rpb.export_xls()");
};
RPBAction.prototype.cancel = function() {
	FINISHED = true;
};
RPBAction.prototype.export_xls = function() {
	showLoading();
	var num_rows = $("#rpb_list").children("tr").length;
	var d_data = {};
	for (var i = 0; i < num_rows; i++) {
		var nomor = $("tbody#rpb_list tr:eq(" + i + ") td#rpb_nomor").text();
		var nomor_transaksi = $("tbody#rpb_list tr:eq(" + i + ") td#rpb_id").text();
		var nomor_resep = $("tbody#rpb_list tr:eq(" + i + ") td#rpb_no_resep").text();
		var tanggal = $("tbody#rpb_list tr:eq(" + i + ") td#rpb_tanggal").text();
		var nama_pasien = $("tbody#rpb_list tr:eq(" + i + ") td#rpb_nama_pasien").text();
		var total = $("tbody#rpb_list tr:eq(" + i + ") td#rpb_total").text();
		d_data[i] = {
			"nomor" 			: nomor,
			"nomor_transaksi"	: nomor_transaksi,
			"nomor_resep"		: nomor_resep,
			"tanggal"			: tanggal,
			"nama_pasien"		: nama_pasien,
			"total"				: total
		};
	}
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['tanggal_from'] = $("#rpb_tanggal_from").val();
	data['tanggal_to'] = $("#rpb_tanggal_to").val();
	data['d_data'] = JSON.stringify(d_data);
	data['num_rows'] = num_rows;
	postForm(data);
	dismissLoading();
};