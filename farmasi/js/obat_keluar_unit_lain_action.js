function ObatKeluarUnitLainAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatKeluarUnitLainAction.prototype.constructor = ObatKeluarUnitLainAction;
ObatKeluarUnitLainAction.prototype = new TableAction();
ObatKeluarUnitLainAction.prototype.refreshNumber = function() {
	var no = 1;
	var nor_dobat_keluar_unit_lain = $("tbody#dobat_keluar_unit_lain_list").children("tr").length;
	for(var i = 0; i < nor_dobat_keluar_unit_lain; i++) {
		var dr_prefix = $("tbody#dobat_keluar_unit_lain_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html("<div align='right'>" + no + ".</div>");
		no++;
	}
};
ObatKeluarUnitLainAction.prototype.update_total = function() {
	var row_nums = $("#dobat_keluar_unit_lain_list tr").length;
	var total = 0;
	for (var i = 0; i < row_nums; i++) {
		var harga = $("#dobat_keluar_unit_lain_list tr:eq(" + i + ") td:eq(10)").text();
		var jumlah = $("#dobat_keluar_unit_lain_list tr:eq(" + i + ") td:eq(5)").text();
		total += parseFloat(harga) * parseFloat(jumlah);
	}
	total = parseFloat(total).formatMoney("2", ".", ",");
	$("#obat_keluar_unit_lain_total").val(total);
};
ObatKeluarUnitLainAction.prototype.show_add_form = function() {
	row_id = 0;
	var today = new Date();
	$("#obat_keluar_unit_lain_id").val("");
	$("#obat_keluar_unit_lain_tanggal").removeAttr("disabled");
	$("#obat_keluar_unit_lain_id_unit").val("");
	$("#obat_keluar_unit_lain_kode_unit").val("");
	$("#obat_keluar_unit_lain_nama_unit").val("");
	$("#obat_keluar_unit_lain_total").val("0,00");
	$("#unit_lain_browse").removeAttr("onclick");
	$("#unit_lain_browse").attr("onclick", "unit_lain.chooser('unit_lain', 'unit_lain_button', 'unit_lain', unit_lain)");
	$("#unit_lain_browse").removeClass("btn-info");
	$("#unit_lain_browse").removeClass("btn-inverse");
	$("#unit_lain_browse").addClass("btn-info");
	$("#dobat_keluar_unit_lain_list").children().remove();
	$("#dobat_keluar_kebun_add").show();
	$("#modal_alert_obat_keluar_unit_lain_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_keluar_unit_lain_save").removeAttr("onclick");
	$("#obat_keluar_unit_lain_save").attr("onclick", "obat_keluar_unit_lain.save()");
	$("#obat_keluar_unit_lain_save").show();
	$("#obat_keluar_unit_lain_ok").hide();
	$("#obat_keluar_unit_lain_add_form").smodal("show");
};
ObatKeluarUnitLainAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_unit_lain = $("#obat_keluar_unit_lain_nama_unit").val();
	var nord = $("#dobat_keluar_unit_lain_list").children().length;
	$(".error_field").removeClass("error_field");
	if (nama_unit_lain == "") {
		valid = false;
		invalid_msg += "<br/><strong>Kebun</strong> tidak boleh kosong";
		$("#obat_keluar_unit_lain_nama_unit_lain").addClass("error_field");
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "<br/><strong>Detail</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_obat_keluar_unit_lain_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ObatKeluarUnitLainAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	$("#obat_keluar_unit_lain_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_unit_lain";
	data['command'] = "save";
	data['id'] = "";
	data['tanggal'] = $("#obat_keluar_unit_lain_tanggal").val();
	data['id_unit'] = $("#obat_keluar_unit_lain_id_unit").val();
	data['kode_unit'] = $("#obat_keluar_unit_lain_kode_unit").val();
	data['nama_unit'] = $("#obat_keluar_unit_lain_nama_unit").val();
	data['keterangan'] = "";
	var detail = {};
	var nor = $("tbody#dobat_keluar_unit_lain_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		var prefix = $("tbody#dobat_keluar_unit_lain_list").children('tr').eq(i).prop("id");
		d_data['id'] = $("#" + prefix + "_id").text();
		d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
		d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
		d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
		d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
		d_data['stok_entri'] = $("#" + prefix + "_stok").text();
		d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
		d_data['jumlah_diminta'] = $("#" + prefix + "_jumlah_diminta").text();
		d_data['harga_ma'] = $("#" + prefix + "_harga_ma").text();
		d_data['satuan'] = $("#" + prefix + "_satuan").text();
		d_data['konversi'] = $("#" + prefix + "_konversi").text();
		d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
		d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
		detail[i] = d_data;
	}
	data['detail'] = detail;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) { 
				$("#obat_keluar_unit_lain_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ObatKeluarUnitLainAction.prototype.edit = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_unit_lain";
	data['command'] = "edit";
	data['id'] = id;
	$.post(	
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_keluar_unit_lain_id").val(id);
			$("#obat_keluar_unit_lain_id_unit").val(json.header.id_unit_lain);
			$("#obat_keluar_unit_lain_kode_unit").val(json.header.kode_unit_lain);
			$("#obat_keluar_unit_lain_nama_unit").val(json.header.nama_unit_lain);
			$("#unit_lain_browse").removeAttr("onclick");
			$("#unit_lain_browse").attr("onclick", "unit_lain.chooser('unit_lain', 'unit_lain_button', 'unit_lain', unit_lain)");
			$("#unit_lain_browse").removeClass("btn-info");
			$("#unit_lain_browse").removeClass("btn-inverse");
			$("#unit_lain_browse").addClass("btn-info");
			$("#dobat_keluar_kebun_add").show();
			row_id = 0;
			$("#dobat_keluar_unit_lain_list").children("tr").remove();
			for(var i = 0; i < json.detail.length; i++) {
				var dobat_keluar_unit_lain_id = json.detail[i].id;
				var dobat_keluar_unit_lain_id_obat_keluar_unit_lain = json.detail[i].id_obat_keluar_unit_lain;
				var dobat_keluar_unit_lain_kode_obat = json.detail[i].kode_obat;
				var dobat_keluar_unit_lain_id_obat = json.detail[i].id_obat;
				var dobat_keluar_unit_lain_nama_obat = json.detail[i].nama_obat;
				var dobat_keluar_unit_lain_nama_jenis_obat = json.detail[i].nama_jenis_obat;
				var dobat_keluar_unit_lain_stok = json.detail[i].stok_entri;
				var dobat_keluar_unit_lain_jumlah_diminta = json.detail[i].jumlah_diminta;
				var dobat_keluar_unit_lain_jumlah = json.detail[i].jumlah;
				var dobat_keluar_unit_lain_satuan = json.detail[i].satuan;
				var dobat_keluar_unit_lain_konversi = json.detail[i].konversi;
				var dobat_keluar_unit_lain_satuan_konversi = json.detail[i].satuan_konversi;
				var dobat_keluar_unit_lain_harga_ma = json.detail[i].harga_ma;
				$("tbody#dobat_keluar_unit_lain_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td style='display: none;' id='data_" + row_id + "_id'>" + dobat_keluar_unit_lain_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_id_obat'>" + dobat_keluar_unit_lain_id_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_kode_obat'>" + dobat_keluar_unit_lain_kode_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_stok'>" + dobat_keluar_unit_lain_stok + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_diminta'>" + dobat_keluar_unit_lain_jumlah_diminta + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dobat_keluar_unit_lain_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dobat_keluar_unit_lain_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dobat_keluar_unit_lain_satuan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dobat_keluar_unit_lain_konversi + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dobat_keluar_unit_lain_satuan_konversi + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_harga_ma'>" + dobat_keluar_unit_lain_harga_ma + "</td>" +
						"<td id='data_" + row_id + "_nomor'></td>" +
						"<td id='data_" + row_id + "_nama_obat'>" + dobat_keluar_unit_lain_nama_obat + "</td>" +
						"<td id='data_" + row_id + "_nama_jenis_obat'>" + dobat_keluar_unit_lain_nama_jenis_obat + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + dobat_keluar_unit_lain_jumlah + " " + dobat_keluar_unit_lain_satuan + "</td>" +
						"<td id='data_" + row_id + "_keterangan'>1 " + dobat_keluar_unit_lain_satuan + " = " + dobat_keluar_unit_lain_konversi + " " + dobat_keluar_unit_lain_satuan_konversi + "</td>" +
						"<td>" + 
							"<div class='btn-group noprint'>" +
								"<a href='#' onclick='dobat_keluar_unit_lain.edit_jumlah(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
									"<i class='icon-edit icon-white'></i>" +
								"</a>" +
								"<a href='#' onclick='dobat_keluar_unit_lain.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
									"<i class='icon-remove icon-white'></i>" + 
								"</a>" +
							 "</div>" +
						"</td>" +
					"</tr>"
				);
				row_id++;
			}
			obat_keluar_unit_lain.refreshNumber();
			$("#modal_alert_obat_keluar_unit_lain_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_keluar_unit_lain_save").removeAttr("onclick");
			$("#obat_keluar_unit_lain_save").attr("onclick", "obat_keluar_unit_lain.update(" + id + ")");
			$("#obat_keluar_unit_lain_save").show();
			$("#obat_keluar_unit_lain_ok").hide();
			$("#obat_keluar_unit_lain_add_form").smodal("show");
		}
	);
};
ObatKeluarUnitLainAction.prototype.update = function(id) {
	if (!this.validate()) {
		return;
	}
	$("#obat_keluar_unit_lain_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_unit_lain";
	data['command'] = "save";
	data['id'] = id;
	data['id_unit'] = $("#obat_keluar_unit_lain_id_unit").val();
	data['kode_unit'] = $("#obat_keluar_unit_lain_kode_unit").val();
	data['nama_unit'] = $("#obat_keluar_unit_lain_nama_unit").val();
	data['keterangan'] = "";
	var detail = {};
	var nor = $("tbody#dobat_keluar_unit_lain_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		var prefix = $("tbody#dobat_keluar_unit_lain_list").children('tr').eq(i).prop("id");
		d_data['id'] = $("#" + prefix + "_id").text();
		d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
		d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
		d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
		d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
		d_data['stok_entri'] = $("#" + prefix + "_stok").text();
		d_data['jumlah_diminta'] = $("#" + prefix + "_jumlah_diminta").text();
		d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
		d_data['jumlah_lama'] = $("#" + prefix + "_jumlah_lama").text();
		d_data['satuan'] = $("#" + prefix + "_satuan").text();
		d_data['konversi'] = $("#" + prefix + "_konversi").text();
		d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
		d_data['harga_ma'] = $("#" + prefix + "_harga_ma").text();
		if ($("#" + prefix).attr("class") == "deleted") {
			d_data['cmd'] = "delete";
		} else if ($("#" + prefix + "_id").text() == "") {
			d_data['cmd'] = "insert";
		} else {
			d_data['cmd'] = "update";
		}
		detail[i] = d_data;
	}
	data['detail'] = detail;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) { 
				$("#obat_keluar_unit_lain_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ObatKeluarUnitLainAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_unit_lain";
	data['command'] = "edit";
	data['id'] = id;
	$.post(	
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_keluar_unit_lain_id").val(id);
			$("#obat_keluar_unit_lain_tanggal").val(json.header.tanggal);
			$("#obat_keluar_unit_lain_tanggal").removeAttr("disabled");
			$("#obat_keluar_unit_lain_tanggal").attr("disabled", "disabled");
			$("#obat_keluar_unit_lain_id_unit").val(json.header.id_unit);
			$("#obat_keluar_unit_lain_kode_unit").val(json.header.kode_unit);
			$("#obat_keluar_unit_lain_nama_unit").val(json.header.nama_unit);
			$("#unit_lain_browse").removeAttr("onclick");
			$("#unit_lain_browse").removeClass("btn-info");
			$("#unit_lain_browse").removeClass("btn-inverse");
			$("#unit_lain_browse").addClass("btn-inverse");
			$("#dobat_keluar_kebun_add").hide();
			row_id = 0;
			$("#dobat_keluar_unit_lain_list").children("tr").remove();
			for(var i = 0; i < json.detail.length; i++) {
				var dobat_keluar_unit_lain_id = json.detail[i].id;
				var dobat_keluar_unit_lain_id_obat_keluar_unit_lain = json.detail[i].id_obat_keluar_unit_lain;
				var dobat_keluar_unit_lain_id_obat = json.detail[i].id_obat;
				var dobat_keluar_unit_lain_kode_obat = json.detail[i].kode_obat;
				var dobat_keluar_unit_lain_nama_obat = json.detail[i].nama_obat;
				var dobat_keluar_unit_lain_nama_jenis_obat = json.detail[i].nama_jenis_obat;
				var dobat_keluar_unit_lain_stok = json.detail[i].stok_entri;
				var dobat_keluar_unit_lain_jumlah_diminta = json.detail[i].jumlah_diminta;
				var dobat_keluar_unit_lain_jumlah = json.detail[i].jumlah;
				var dobat_keluar_unit_lain_satuan = json.detail[i].satuan;
				var dobat_keluar_unit_lain_konversi = json.detail[i].konversi;
				var dobat_keluar_unit_lain_satuan_konversi = json.detail[i].satuan_konversi;
				var dobat_keluar_unit_lain_harga_ma = json.detail[i].harga_ma;
				$("tbody#dobat_keluar_unit_lain_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td style='display: none;' id='data_" + row_id + "_id'>" + dobat_keluar_unit_lain_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_kode_obat'>" + dobat_keluar_unit_lain_kode_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_id_obat'>" + dobat_keluar_unit_lain_id_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_stok'>" + dobat_keluar_unit_lain_stok + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_diminta'>" + dobat_keluar_unit_lain_jumlah_diminta + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dobat_keluar_unit_lain_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dobat_keluar_unit_lain_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dobat_keluar_unit_lain_satuan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dobat_keluar_unit_lain_konversi + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dobat_keluar_unit_lain_satuan_konversi + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_harga_ma'>" + dobat_keluar_unit_lain_harga_ma + "</td>" +
						"<td id='data_" + row_id + "_nomor'></td>" +
						"<td id='data_" + row_id + "_nama_obat'>" + dobat_keluar_unit_lain_nama_obat + "</td>" +
						"<td id='data_" + row_id + "_nama_jenis_obat'>" + dobat_keluar_unit_lain_nama_jenis_obat + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + dobat_keluar_unit_lain_jumlah + " " + dobat_keluar_unit_lain_satuan + "</td>" +
						"<td id='data_" + row_id + "_f_harga_ma'>" + parseFloat(dobat_keluar_unit_lain_harga_ma).formatMoney("2", ".", ",") + "</td>" +
						"<td id='data_" + row_id + "_f_total_harga_ma'>" + parseFloat(dobat_keluar_unit_lain_harga_ma * dobat_keluar_unit_lain_jumlah).formatMoney("2", ".", ",") + "</td>" +
						"<td id='data_" + row_id + "_keterangan'>1 " + dobat_keluar_unit_lain_satuan + " = " + dobat_keluar_unit_lain_konversi + " " + dobat_keluar_unit_lain_satuan_konversi + "</td>" +
						"<td></td>" +
					"</tr>"
				);
				row_id++;
			}
			obat_keluar_unit_lain.refreshNumber();
			obat_keluar_unit_lain.update_total();
			$("#modal_alert_obat_keluar_unit_lain_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_keluar_unit_lain_save").removeAttr("onclick");
			$("#obat_keluar_unit_lain_save").hide();
			$("#obat_keluar_unit_lain_ok").show();
			$("#obat_keluar_unit_lain_add_form").smodal("show");
		}
	);
};
ObatKeluarUnitLainAction.prototype.del = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_unit_lain";
	data['command'] = "del";
	data['id'] = id;
	data['push_command'] = "delete";
	bootbox.confirm(
		"Anda yakin menghapus data ini?", 
		function(result) {
			if(result) {
				showLoading();
				$.post(
					'',
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
ObatKeluarUnitLainAction.prototype.download_au58 = function(id) {
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_unit_lain";
	data['command'] = "export_au58";
	data['id'] = id;
	postForm(data);
};