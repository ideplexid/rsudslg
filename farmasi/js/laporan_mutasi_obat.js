var lmo;
var lmo_obat;
var lmo_jenis_obat;
var FINISHED;
$(document).ready(function() {
	lmo_obat = new LMOObatAction(
		"lmo_obat",
		"farmasi",
		"laporan_mutasi_obat",
		new Array()
	);
	lmo_obat.setSuperCommand("lmo_obat");
	lmo_jenis_obat = new LMOJenisObatAction(
		"lmo_jenis_obat",
		"farmasi",
		"laporan_mutasi_obat",
		new Array()
	);
	lmo_jenis_obat.setSuperCommand("lmo_jenis_obat");
	lmo = new LMOAction(
		"lmo",
		"farmasi",
		"laporan_mutasi_obat",
		new Array()
	);
	$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(7)").hide();
	$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(8)").hide();
	$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(9)").hide();
	$("#lmo_jenis_filter").on("change", function() {
		var jenis_filter = $("#lmo_jenis_filter").val();
		$("#lmo_id_obat").val("");
		$("#lmo_nama_obat").val("");
		$("#lmo_kode_jenis_obat").val("");
		$("#lmo_jenis_obat").val("");
		$("#lmo_unit").val("");
		if (jenis_filter == "semua") {
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(8)").hide();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(9)").hide();
			$("#lmo_unit").val("");
			$("#lmo_id_obat").val("");
			$("#lmo_nama_obat").val("");
			$("#lmo_nama_jenis_obat").val("");
		} else if (jenis_filter == "per_obat") {
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(7)").show();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(8)").hide();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(9)").hide();
			$("#lmo_unit").val("");
			$("#lmo_id_obat").val("");
			$("#lmo_nama_obat").val("");
			$("#lmo_nama_jenis_obat").val("");
		} else if (jenis_filter == "per_jenis") {
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(8)").show();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(9)").hide();
			$("#lmo_unit").val("");
			$("#lmo_id_obat").val("");
			$("#lmo_nama_obat").val("");
			$("#lmo_nama_jenis_obat").val("");
		} else if (jenis_filter == "per_unit") {
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(8)").hide();
			$("#laporan_mutasi_obat > div.form-container > form > div:nth-child(9)").show();
			$("#lmo_unit").val("");
			$("#lmo_id_obat").val("");
			$("#lmo_nama_obat").val("");
			$("#lmo_nama_jenis_obat").val("");
		}
	});
	$("#loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#loading_modal").on("hide", function() {
		$("a.close").show();
	});
	$("tbody#lmo_list").append(
		"<tr>" +
			"<td colspan='9'><strong><center><small>DATA MUTASI OBAT BELUM DIPROSES</small></center></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	})
});