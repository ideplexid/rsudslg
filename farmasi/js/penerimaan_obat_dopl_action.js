function DOPLAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DOPLAction.prototype.constructor = DOPLAction;
DOPLAction.prototype = new TableAction();
DOPLAction.prototype.getRegulerData = function() {
	var data = TableAction.prototype.getRegulerData.call(this);
	data['id_opl'] = $("#penerimaan_obat_id_po").val();
	var num_rows = $("#dpenerimaan_obat_list tr").length;
	// var id_dpo_csv = "";
	// for (var i = 0; i < num_rows; i++) {
	// 	var id_dpo = $("#dpenerimaan_obat_list tr:eq(" + i + ") td#id_dpo").text();
	// 	id_dpo_csv += id_dpo + ",";
	// }
	// id_dpo_csv = id_dpo_csv.replace(/,+$/, '');
	// data['not_in'] = id_dpo_csv;
	return data;
};
DOPLAction.prototype.selected = function(json) {
	$("#dpenerimaan_obat_id_dpo").val(json.id);
	$("#dpenerimaan_obat_id_obat").val(json.id_barang);
	$("#dpenerimaan_obat_kode_obat").val(json.kode_barang);
	$("#dpenerimaan_obat_nama_obat").val(json.nama_barang);
	$("#dpenerimaan_obat_name_obat").val(json.nama_barang);
	$("#dpenerimaan_obat_medis").val(json.medis);
	$("#dpenerimaan_obat_inventaris").val(json.inventaris);
	$("#dpenerimaan_obat_nama_jenis_obat").val(json.nama_jenis_barang);
	$("#dpenerimaan_obat_jumlah_tercatat").val(json.jumlah_dipesan);
	$("#dpenerimaan_obat_jumlah").val(json.jumlah_dipesan);
	$("#dpenerimaan_obat_satuan").val(json.satuan);
	$("#dpenerimaan_obat_konversi").val(json.konversi);
	$("#dpenerimaan_obat_satuan_konversi").val(json.satuan_konversi);
	var hpp = parseFloat(json.hpp);
	var hna = hpp / 1.1;
	hpp = "Rp. " + parseFloat(hpp).formatMoney("2", ".", ",");
	hna = "Rp. " + parseFloat(hna).formatMoney("2", ".", ",");
	$("#dpenerimaan_obat_hpp").val(hna);
	$("#dpenerimaan_obat_hna").val(hpp);

	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_last_item";
	data['super_command'] = "";
	data['id_obat'] = json.id_barang;
	$.post(
		"",
		data,
		function(last_item_response) {
			var json_last_item = JSON.parse(last_item_response);
			if (json_last_item == null) {
				$("#dpenerimaan_obat_stok_terkini").val("0");
			} else {
				var produsen = json_last_item.data.produsen;
				$("#dpenerimaan_obat_produsen").val(produsen);
				var data_stok = self.getRegulerData();
				data_stok['super_command'] = "stok";
				data_stok['command'] = "edit";
				data_stok['id_obat'] = json.id_barang;
				data_stok['satuan'] = json.satuan;
				data_stok['konversi'] = json.konversi;
				data_stok['satuan_konversi'] = json.satuan_konversi;
				$.post(
					"",
					data_stok,
					function(sisa_response) {
						var json_stok = getContent(sisa_response);
						if (json_stok == null) return;
						$("#dpenerimaan_obat_stok_entri").val(json_stok.sisa);
					}
				);
			}
			$("#dpenerimaan_obat_jumlah_tercatat").focus();
		}
	);
};