function ObatMasukAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatMasukAction.prototype.constructor = ObatMasukAction;
ObatMasukAction.prototype = new TableAction();
ObatMasukAction.prototype.selected = function(json) {
	$("#retur_id_obat_masuk").val(json.header.id);
	$("#retur_no_faktur").val(json.header.no_faktur);
	$("#retur_tanggal_faktur").val(json.header.tanggal);
	$("#retur_id_vendor").val(json.header.id_vendor);
	$("#retur_nama_vendor").val(json.header.nama_vendor);
	$("tbody#dretur_list").html(json.detail_list);
};