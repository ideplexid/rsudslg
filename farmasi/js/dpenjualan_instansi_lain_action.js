function DPenjualanInstansiLainAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPenjualanInstansiLainAction.prototype.constructor = DPenjualanInstansiLainAction;
DPenjualanInstansiLainAction.prototype = new TableAction();
DPenjualanInstansiLainAction.prototype.show_add_form = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	$("#dpenjualan_instansi_lain_id").val("");
	$("#dpenjualan_instansi_lain_id_obat").val("");
	$("#dpenjualan_instansi_lain_kode_obat").val("");
	$("#dpenjualan_instansi_lain_name_obat").val("");
	$("#dpenjualan_instansi_lain_nama_obat").val("");
	$("#dpenjualan_instansi_lain_nama_jenis_obat").val("");
	$("#dpenjualan_instansi_lain_satuan").removeAttr("onchange");
	$("#dpenjualan_instansi_lain_satuan").attr("onchange", "obat.setDetailInfo()");
	$("#dpenjualan_instansi_lain_satuan").html("");
	$("#dpenjualan_instansi_lain_stok").val("");
	$("#dpenjualan_instansi_lain_f_stok").val("");
	$("#dpenjualan_instansi_lain_jumlah_lama").val(0);
	$("#dpenjualan_instansi_lain_hna").val("");
	$("#dpenjualan_instansi_lain_markup").val(0);
	$("#dpenjualan_instansi_lain_embalase").val("Rp. " + parseFloat(embalase_obat_jadi).formatMoney("2", ".", ","));
	$("#dpenjualan_instansi_lain_tuslah").val("Rp. " + parseFloat(tuslah_obat_jadi).formatMoney("2", ".", ","));
	$("#dpenjualan_instansi_lain_jumlah").val("");
	$("#dpenjualan_instansi_lain_save").removeAttr("onclick");
	$("#dpenjualan_instansi_lain_save").attr("onclick", "dpenjualan_instansi_lain.save()");
	$("#modal_alert_dpenjualan_instansi_lain_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dpenjualan_instansi_lain_add_form").smodal("show");
	$(".btn").removeAttr("disabled");
};
DPenjualanInstansiLainAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#dpenjualan_instansi_lain_name_obat").val();
	var satuan = $("#dpenjualan_instansi_lain_satuan").val();
	var stok = $("#dpenjualan_instansi_lain_stok").val();
	var jumlah_lama = $("#dpenjualan_instansi_lain_jumlah_lama").val();
	var jumlah = $("#dpenjualan_instansi_lain_jumlah").val();
	var harga = $("#dpenjualan_instansi_lain_hna").val();
	$(".error_field").removeClass("error_field");
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#dpenjualan_instansi_lain_nama_obat").addClass("error_field");
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#dpenjualan_instansi_lain_satuan").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#dpenjualan_instansi_lain_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#dpenjualan_instansi_lain_jumlah").addClass("error_field");
	} else if (stok != "" && is_numeric(stok) && (parseFloat(jumlah) - parseFloat(jumlah_lama)) > parseFloat(stok)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi stok";
		$("#dpenjualan_instansi_lain_jumlah").addClass("error_field");
	}
	if (harga == 0 || harga == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga Netto</strong> tidak boleh kosong";
		$("#dpenjualan_instansi_lain_hna").addClass("error_field");
		$("#dpenjualan_instansi_lain_jumlah").focus();
	}
	if (!valid) {
		$("#modal_alert_dpenjualan_instansi_lain_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DPenjualanInstansiLainAction.prototype.save = function() {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var dpal_id = $("#dpenjualan_instansi_lain_id").val();
	var dpal_id_obat = $("#dpenjualan_instansi_lain_id_obat").val();
	var dpal_kode_obat = $("#dpenjualan_instansi_lain_kode_obat").val();
	var dpal_nama_obat = $("#dpenjualan_instansi_lain_name_obat").val();
	var dpal_nama_jenis_obat = $("#dpenjualan_instansi_lain_nama_jenis_obat").val();
	var dpal_jumlah_lama = $("#dpenjualan_instansi_lain_jumlah_lama").val();
	var dpal_jumlah = $("#dpenjualan_instansi_lain_jumlah").val();
	var dpal_satuan = $("#dpenjualan_instansi_lain_satuan").find(":selected").text();
	var dpal_konversi = $("#dpenjualan_instansi_lain_konversi").val();
	var dpal_satuan_konversi = $("#dpenjualan_instansi_lain_satuan_konversi").val();
	var dpal_hna = $("#dpenjualan_instansi_lain_hna").val();
	var dpal_embalase = $("#dpenjualan_instansi_lain_embalase").val();
	var dpal_tuslah = $("#dpenjualan_instansi_lain_tuslah").val();
	var dpal_markup = $("#dpenjualan_instansi_lain_markup").val();
	$("tbody#dpenjualan_instansi_lain_list").append(
		"<tr id='dpal_" + dpal_num + "'>" +
			"<td id='dpal_" + dpal_num + "_id' style='display: none;'>" + dpal_id + "</td>" +
			"<td id='dpal_" + dpal_num + "_id_obat' style='display: none;'>" + dpal_id_obat + "</td>" +
			"<td id='dpal_" + dpal_num + "_kode_obat' style='display: none;'>" + dpal_kode_obat + "</td>" +
			"<td id='dpal_" + dpal_num + "_nama_jenis_obat' style='display: none;'>" + dpal_nama_jenis_obat + "</td>" +
			"<td id='dpal_" + dpal_num + "_jumlah' style='display: none;'>" + dpal_jumlah + "</td>" +
			"<td id='dpal_" + dpal_num + "_jumlah_lama' style='display: none;'>" + dpal_jumlah_lama + "</td>" +
			"<td id='dpal_" + dpal_num + "_satuan' style='display: none;'>" + dpal_satuan + "</td>" +
			"<td id='dpal_" + dpal_num + "_konversi' style='display: none;'>" + dpal_konversi + "</td>" +
			"<td id='dpal_" + dpal_num + "_satuan_konversi' style='display: none;'>" + dpal_satuan_konversi + "</td>" +
			"<td id='dpal_" + dpal_num + "_markup' style='display: none;'>" + dpal_markup + "</td>" +
			"<td id='dpal_" + dpal_num + "_hna' style='display: none;'>" + dpal_hna + "</td>" +
			"<td id='dpal_" + dpal_num + "_nomor'></td>" +
			"<td id='dpal_" + dpal_num + "_nama_obat'>" + dpal_nama_obat + "</td>" +
			"<td id='dpal_" + dpal_num + "_f_jumlah'>" + dpal_jumlah + " " + dpal_satuan + "</td>" +
			"<td id='dpal_" + dpal_num + "_harga'></td>" +
			"<td id='dpal_" + dpal_num + "_embalase'>" + dpal_embalase + "</td>" +
			"<td id='dpal_" + dpal_num + "_tuslah'>" + dpal_tuslah + "</td>" +
			"<td id='dpal_" + dpal_num + "_subtotal'></td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dpenjualan_instansi_lain.edit(" + dpal_num + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dpenjualan_instansi_lain.delete(" + dpal_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	dpal_num++;
	$("#dpenjualan_instansi_lain_id").val("");
	$("#dpenjualan_instansi_lain_id_obat").val("");
	$("#dpenjualan_instansi_lain_kode_obat").val("");
	$("#dpenjualan_instansi_lain_name_obat").val("");
	$("#dpenjualan_instansi_lain_nama_obat").val("");
	$("#dpenjualan_instansi_lain_nama_jenis_obat").val("");
	$("#dpenjualan_instansi_lain_satuan").removeAttr("onchange");
	$("#dpenjualan_instansi_lain_satuan").attr("onchange", "obat.setDetailInfo()");
	$("#dpenjualan_instansi_lain_satuan").html("");
	$("#dpenjualan_instansi_lain_stok").val("");
	$("#dpenjualan_instansi_lain_f_stok").val("");
	$("#dpenjualan_instansi_lain_jumlah_lama").val(0);
	$("#dpenjualan_instansi_lain_hna").val("");
	$("#dpenjualan_instansi_lain_markup").val(0);
	$("#dpenjualan_instansi_lain_jumlah").val("");
	$("#dpenjualan_instansi_lain_nama_obat").focus();
	$("#dpenjualan_instansi_lain_save").removeAttr("onclick");
	$("#dpenjualan_instansi_lain_save").attr("onclick", "dpenjualan_instansi_lain.save()");
	$("#modal_alert_dpenjualan_instansi_lain_add_form").html("");
	$(".error_field").removeClass("error_field");
	penjualan_instansi_lain.refreshHargaAndSubtotal();
	penjualan_instansi_lain.refresh_no_dpenjualan_instansi_lain();
	$(".btn").removeAttr("disabled");
};
DPenjualanInstansiLainAction.prototype.edit = function(r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	var dpal_id = $("#dpal_" + r_num + "_id").text();
	var dpal_id_obat = $("#dpal_" + r_num + "_id_obat").text();
	var dpal_kode_obat = $("#dpal_" + r_num + "_kode_obat").text();
	var dpal_nama_obat = $("#dpal_" + r_num + "_nama_obat").text();
	var dpal_nama_jenis_obat = $("#dpal_" + r_num + "_nama_jenis_obat").text();
	var dpal_jumlah_lama = $("#dpal_" + r_num + "_jumlah_lama").text();
	var dpal_jumlah = $("#dpal_" + r_num + "_jumlah").text();
	var dpal_satuan = $("#dpal_" + r_num + "_satuan").text();
	var dpal_konversi = $("#dpal_" + r_num + "_konversi").text();
	var dpal_satuan_konversi = $("#dpal_" + r_num + "_satuan_konversi").text();
	var dpal_hna = $("#dpal_" + r_num + "_harga").text();
	var dpal_embalase = $("#dpal_" + r_num + "_embalase").text();
	var dpal_tuslah = $("#dpal_" + r_num + "_tuslah").text();
	var dpal_markup = $("#dpal_" + r_num + "_markup").text();
	$("#dpenjualan_instansi_lain_id").val(dpal_id);
	$("#dpenjualan_instansi_lain_jumlah_lama").val(dpal_jumlah_lama);
	$("#dpenjualan_instansi_lain_jumlah").val(dpal_jumlah);
	$("#dpenjualan_instansi_lain_markup").val(dpal_markup);
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_instansi_lain";
	data['command'] = "edit";
	data['id'] = dpal_id_obat;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#dpenjualan_instansi_lain_id_obat").val(json.header.id_obat);
			$("#dpenjualan_instansi_lain_kode_obat").val(json.header.kode_obat);
			$("#dpenjualan_instansi_lain_name_obat").val(json.header.nama_obat);
			$("#dpenjualan_instansi_lain_nama_obat").val(json.header.nama_obat);
			$("#dpenjualan_instansi_lain_nama_jenis_obat").val(json.header.nama_jenis_obat);
			$("#dpenjualan_instansi_lain_satuan").html(json.satuan_option);
			$("#dpenjualan_instansi_lain_satuan").val(dpal_konversi + "_" + dpal_satuan_konversi);
			$("#dpenjualan_instansi_lain_nama_obat").val(dpal_hna);
			var part = $("#dpenjualan_instansi_lain_satuan").val().split("_");
			$("#dpenjualan_instansi_lain_konversi").val(part[0]);
			$("#dpenjualan_instansi_lain_satuan_konversi").val(part[1]);
			data = self.getRegulerData();
			data['super_command'] = "sisa";
			data['command'] = "edit";
			data['id_obat'] = $("#dpenjualan_instansi_lain_id_obat").val();
			data['satuan'] = $("#dpenjualan_instansi_lain_satuan").find(":selected").text();
			data['konversi'] = $("#dpenjualan_instansi_lain_konversi").val();
			data['satuan_konversi'] = $("#dpenjualan_instansi_lain_satuan_konversi").val();
			$.post(
				"",
				data,
				function(response) {
					var json = getContent(response);
					if (json == null) return;
					$("#dpenjualan_instansi_lain_stok").val(json.sisa);
					$("#dpenjualan_instansi_lain_f_stok").val(json.sisa + " " + json.satuan);						
					var hna = parseFloat(json.hna) * (1 + parseFloat(json.markup) / 100);
					var data_rounded = self.getRegulerData();
					data['super_command'] = "obat_instansi_lain";
					data['command'] = "rounded_value";
					data['value'] = hna;
					$.post(
						"",
						data,
						function(rspns) {
							var jsn = getContent(rspns);
							if (jsn == null) return;
							hna = "Rp. " + (parseFloat(jsn)).formatMoney("2", ".", ",");
							$("#dpenjualan_instansi_lain_hna").val(hna);
							$("#dpenjualan_instansi_lain_markup").val(json.markup);
							$("#dpenjualan_instansi_lain_satuan").removeAttr("onchange");
							$("#dpenjualan_instansi_lain_satuan").attr("onchange", "obat.setDetailInfo()");
							$("#modal_alert_dpenjualan_instansi_lain_add_form").html("");
							$(".error_field").removeClass("error_field");
							$("#dpenjualan_instansi_lain_save").removeAttr("onclick");
							$("#dpenjualan_instansi_lain_add_form").smodal("show");
							$(".btn").removeAttr("disabled");
						}
					);
				}
			);
		}
	);
};
DPenjualanInstansiLainAction.prototype.update = function(r_num) {
	if ($(".btn").attr("disabled") == "disabled")
		return;
	$(".btn").removeAttr("disabled");
	$(".btn").attr("disabled", "disabled");
	if (!this.validate()) {
		$(".btn").removeAttr("disabled");
		return;
	}
	var dpal_id_obat = $("#dpenjualan_instansi_lain_id_obat").val();
	var dpal_kode_obat = $("#dpenjualan_instansi_lain_kode_obat").val();
	var dpal_nama_obat = $("#dpenjualan_instansi_lain_name_obat").val();
	var dpal_nama_jenis_obat = $("#dpenjualan_instansi_lain_nama_jenis_obat").val();
	var dpal_jumlah = $("#dpenjualan_instansi_lain_jumlah").val();
	var dpal_satuan = $("#dpenjualan_instansi_lain_satuan").find(":selected").text();
	var dpal_konversi = $("#dpenjualan_instansi_lain_konversi").val();
	var dpal_satuan_konversi = $("#dpenjualan_instansi_lain_satuan_konversi").val();
	var dpal_hna = $("#dpenjualan_instansi_lain_hna").val();
	var dpal_embalase = $("#dpenjualan_instansi_lain_embalase").val();
	var dpal_tuslah = $("#dpenjualan_instansi_lain_tuslah").val();
	var dpal_markup = $("#dpenjualan_instansi_lain_markup").val();
	$("#dpal_" + r_num + "_id_obat").text(dpal_id_obat);
	$("#dpal_" + r_num + "_kode_obat").text(dpal_kode_obat);
	$("#dpal_" + r_num + "_nama_obat").text(dpal_nama_obat);
	$("#dpal_" + r_num + "_nama_jenis_obat").text(dpal_nama_jenis_obat);
	$("#dpal_" + r_num + "_jumlah").text(dpal_jumlah);
	$("#dpal_" + r_num + "_satuan").text(dpal_satuan);
	$("#dpal_" + r_num + "_konversi").text(dpal_konversi);
	$("#dpal_" + r_num + "_satuan_konversi").text(dpal_satuan_konversi);
	$("#dpal_" + r_num + "_harga").text(dpal_hna);
	$("#dpal_" + r_num + "_embalase").text(dpal_embalase);
	$("#dpal_" + r_num + "_tuslah").text(dpal_tuslah);
	$("#dpal_" + r_num + "_markup").text(dpal_markup);
	$("#dpal_" + r_num + "_f_jumlah").text(dpal_jumlah + " " + dpal_satuan);
	penjualan_instansi_lain.refreshHargaAndSubtotal();
	$("#dpenjualan_instansi_lain_add_form").smodal("hide");
	$(".btn").removeAttr("disabled");
};
DPenjualanInstansiLainAction.prototype.delete = function(r_num) {
	var id = $("#dpal_" + r_num + "_id").text();
	if (id.length == 0) {
		$("#dpal_" + r_num).remove();
	} else {
		$("#dpal_" + r_num).attr("style", "display: none;");
		$("#dpal_" + r_num).attr("class", "deleted");
	}
	penjualan_instansi_lain.refreshBiayaTotal();
	penjualan_instansi_lain.refresh_no_dpenjualan_bebas();
};