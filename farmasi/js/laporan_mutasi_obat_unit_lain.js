var lmoul;
var lmoul_obat;
var lmoul_jenis_obat;
var FINISHED;
$(document).ready(function() {
	lmoul_obat = new LMOULObatAction(
		"lmoul_obat",
		"farmasi",
		"laporan_mutasi_obat_unit_lain",
		new Array()
	);
	lmoul_obat.setSuperCommand("lmoul_obat");
	lmoul_jenis_obat = new LMOULJenisObatAction(
		"lmoul_jenis_obat",
		"farmasi",
		"laporan_mutasi_obat_unit_lain",
		new Array()
	);
	lmoul_jenis_obat.setSuperCommand("lmoul_jenis_obat");
	lmoul = new LMOULAction(
		"lmoul",
		"farmasi",
		"laporan_mutasi_obat_unit_lain",
		new Array()
	);
	$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(7)").hide();
	$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(8)").hide();
	$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(9)").hide();
	$("#lmoul_jenis_filter").on("change", function() {
		var jenis_filter = $("#lmoul_jenis_filter").val();
		$("#lmoul_id_obat").val("");
		$("#lmoul_nama_obat").val("");
		$("#lmoul_kode_jenis_obat").val("");
		$("#lmoul_jenis_obat").val("");
		$("#lmoul_unit").val("");
		if (jenis_filter == "semua") {
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(8)").hide();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(9)").hide();
			$("#lmoul_unit").val("");
			$("#lmoul_id_obat").val("");
			$("#lmoul_nama_obat").val("");
			$("#lmoul_nama_jenis_obat").val("");
		} else if (jenis_filter == "per_obat") {
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(7)").show();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(8)").hide();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(9)").hide();
			$("#lmoul_unit").val("");
			$("#lmoul_id_obat").val("");
			$("#lmoul_nama_obat").val("");
			$("#lmoul_nama_jenis_obat").val("");
		} else if (jenis_filter == "per_jenis") {
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(8)").show();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(9)").hide();
			$("#lmoul_unit").val("");
			$("#lmoul_id_obat").val("");
			$("#lmoul_nama_obat").val("");
			$("#lmoul_nama_jenis_obat").val("");
		} else if (jenis_filter == "per_unit") {
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(7)").hide();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(8)").hide();
			$("#laporan_mutasi_obat_unit_lain > div.form-container > form > div:nth-child(9)").show();
			$("#lmoul_unit").val("");
			$("#lmoul_id_obat").val("");
			$("#lmoul_nama_obat").val("");
			$("#lmoul_nama_jenis_obat").val("");
		}
	});
	$("#loading_modal").on("show", function() {
		$("a.close").hide();
	});
	$("#loading_modal").on("hide", function() {
		$("a.close").show();
	});
	$("tbody#lmoul_list").append(
		"<tr>" +
			"<td colspan='9'><strong><center><small>DATA MUTASI OBAT BELUM DIPROSES</small></center></strong></td>" +
		"</tr>"
	);
	$(document).keyup(function(e) {
		if (e.which == 27) {
			FINISHED = true;
		}
	})
});