function LMOJenisObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
LMOJenisObatAction.prototype.constructor = LMOJenisObatAction;
LMOJenisObatAction.prototype = new TableAction();
LMOJenisObatAction.prototype.selected = function(json) {
	$("#lmo_nama_jenis_obat").val(json.nama);
};