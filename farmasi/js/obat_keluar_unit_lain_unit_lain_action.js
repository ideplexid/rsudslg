function UnitLainAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
UnitLainAction.prototype.constructor = UnitLainAction;
UnitLainAction.prototype = new TableAction();
UnitLainAction.prototype.selected = function(json) {
	$("#obat_keluar_unit_lain_id_unit").val(json.id);
	$("#obat_keluar_unit_lain_kode_unit").val(json.kode);
	$("#obat_keluar_unit_lain_nama_unit").val(json.nama);
};