<?php
	require_once("smis-base/smis-include-service-consumer.php");

	class InventoryLibrary {
		public static function saveHistoryHPP($db, $id_obat, $tanggal_transaksi, $in_out, $keterangan) {
			$hpp = 0;
			$total_hpp = 0;
			$total_stok = 0;
			
			// get hpp info farmasi:
			$dbtable = new DBTable($db, "smis_frm_histori_hpp");
			$row = $dbtable->get_row("
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, SUM(sisa * hna) AS 'total_hpp', SUM(sisa) AS 'total_stok' 
				FROM smis_frm_stok_obat 
				WHERE sisa > 0 AND id_obat = '" . $id_obat . "'
				LIMIT 0, 1
			");
			if ($row != null) {
				$total_hpp += $row->total_hpp;
				$total_stok += $row->total_stok;
			}

			// get hpp info poli mata :
			$params = array(
		         "id_obat" 	=> $id_obat,
		         "ruangan"	=> "poli_mata"
		    );
			$service_consumer = new ServiceConsumer($db, "get_total_hpp", $params, "poli_mata");
			$content = $service_consumer->execute()->getContent();
			if ($content != null)
				$total_hpp += $content['total_hpp'];
			$service_consumer = new ServiceConsumer($db, "get_item_stok", $params, "poli_mata");
			$content = $service_consumer->execute()->getContent();
			if ($content != null)
				$total_stok += $content['jumlah'];

			// get hpp info kamar_operasi_mata :
			$params = array(
		         "id_obat" 	=> $id_obat,
		         "ruangan"	=> "kamar_operasi_mata"
		    );
			$service_consumer = new ServiceConsumer($db, "get_total_hpp", $params, "kamar_operasi_mata");
			$content = $service_consumer->execute()->getContent();
			if ($content != null)
				$total_hpp += $content['total_hpp'];
			$service_consumer = new ServiceConsumer($db, "get_item_stok", $params, "kamar_operasi_mata");
			$content = $service_consumer->execute()->getContent();
			if ($content != null)
				$total_stok += $content['jumlah'];

			if ($total_stok != 0)
				$hpp = $total_hpp / $total_stok;
			$data = array(
				"tanggal"			=> date("Y-m-d H:i:s"),
				"tanggal_transaksi"	=> $tanggal_transaksi,
				"id_obat"			=> $id_obat,
				"kode_obat"			=> $row->kode_obat,
				"nama_obat"			=> $row->nama_obat,
				"nama_jenis_obat"	=> $row->nama_jenis_obat,
				"hpp"				=> $hpp,
				"in_out"			=> $in_out,
				"keterangan"		=> $keterangan
			);
			$result = $dbtable->insert($data);
			return $result;
		}		

		public static function getCurrentStock($db, $id_obat, $satuan, $konversi, $satuan_konversi) {
			$dbtable = new DBTable($db, "smis_frm_stok_obat");
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(sisa) AS 'jumlah'
				FROM smis_frm_stok_obat
				WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan_konversi . "' AND konversi = '1' AND satuan_konversi = '" . $satuan_konversi . "'
			");
			$jumlah += $row->jumlah;
			$row = $dbtable->get_row("
				SELECT SUM(sisa * konversi) AS 'jumlah'
				FROM smis_frm_stok_obat
				WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND konversi > 1 AND satuan_konversi = '" . $satuan_konversi . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getLastHPP($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_frm_stok_obat");
			$row = $dbtable->get_row("
				SELECT b.jumlah, b.hna, b.diskon AS 'diskon_detail', b.t_diskon AS 't_diskon_detail', c.diskon As 'diskon_global', c.t_diskon AS 't_diskon_global', c.id AS 'id_obat_masuk'
				FROM (smis_frm_stok_obat a LEFT JOIN smis_frm_dobat_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_frm_obat_masuk c ON b.id_obat_masuk = c.id
				WHERE a.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "' AND c.tanggal_datang <= '" . $tanggal_to . "'
				ORDER BY c.id DESC
				LIMIT 0, 1
			");
			$hpp = 0;
			if ($row != null) {
				$hpp = $row->hna;
				$diskon = $row->diskon_detail / $row->jumlah;
				if ($row->t_diskon_detail == "persen")
					$diskon = $row->diskon_detail * $hpp / 100;
				$hpp -= $diskon;
				$diskon = $row->diskon_global * $hpp / 100;
				if ($row->t_diskon_global == "nominal") {
					$rows = $dbtable->get_result("
						SELECT * 
						FROM smis_frm_dobat_masuk
						WHERE id_obat_masuk = '" . $row->id_obat_masuk . "'
					");
					$total = 0;
					foreach ($rows as $r) {
						$total += $r->jumlah * $r->hna;
					}
					$diskon = ($row->hna * $row->jumlah) / $total * $row->diskon_global;
				}
				$hpp -= $diskon;
			}
			return $hpp;
		}

		public static function getStockIn($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_frm_obat_masuk");
			$jumlah = 0;
			// retur penjualan resep:
			$row = $dbtable->get_row("
				SELECT SUM(b.jumlah) AS 'jumlah'
				FROM (smis_frm_retur_penjualan_resep a LEFT JOIN smis_frm_dretur_penjualan_resep b ON a.id = b.id_retur_penjualan_resep) LEFT JOIN smis_frm_stok_obat c ON b.id_stok_obat = c.id
				WHERE a.prop NOT LIKE 'del' AND a.dibatalkan = '0' AND c.id_obat = '" . $id_obat . "' AND c.satuan = '" . $satuan . "' AND c.konversi = '" . $konversi . "' AND c.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . " 00:00' AND a.tanggal <= '" . $tanggal_to . " 23:59'
			");
			$jumlah += $row->jumlah;
			// penerimaan faktur:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM (smis_frm_stok_obat a LEFT JOIN smis_frm_dobat_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_frm_obat_masuk c ON b.id_obat_masuk = c.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "' AND c.tanggal_datang >= '" . $tanggal_from . "' AND c.tanggal_datang <= '" . $tanggal_to . "' AND c.id >= 0 AND b.id_dpo != -1
			");
			$jumlah += $row->jumlah;
			// penerimaan retur unit:
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM smis_frm_retur_obat_unit
				WHERE prop NOT LIKE 'del' AND status = 'sudah' AND restok = '1' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '" . $konversi . "' AND satuan_konversi = '" . $satuan_konversi . "' AND tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getStockOut($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_frm_obat_masuk");
			$jumlah = 0;
			// penjualan obat jadi:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM smis_frm_penjualan_obat_jadi a LEFT JOIN smis_frm_penjualan_resep b ON a.id_penjualan_resep = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "' AND b.tanggal >= '" . $tanggal_from . " 00:00' AND b.tanggal <= '" . $tanggal_to . " 23:59'
			");
			$jumlah += $row->jumlah;
			// penjualan racikan:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM (smis_frm_bahan_pakai_obat_racikan a LEFT JOIN smis_frm_penjualan_obat_racikan b ON a.id_penjualan_obat_racikan = b.id) LEFT JOIN smis_frm_penjualan_resep c ON b.id_penjualan_resep = c.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del' AND c.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "' AND c.tanggal >= '" . $tanggal_from . " 00:00' AND c.tanggal <= '" . $tanggal_to . " 23:59'
			");
			$jumlah += $row->jumlah;
			// mutasi obat:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM smis_frm_dobat_keluar a LEFT JOIN smis_frm_obat_keluar b ON a.id_obat_keluar = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status NOT LIKE 'dikembalikan' AND a.id_obat = '" . $id_obat . "' AND a.satuan = '" . $satuan . "' AND a.konversi = '" . $konversi . "' AND a.satuan_konversi = '" . $satuan_konversi . "' AND b.tanggal >= '" . $tanggal_from . "' AND b.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// retur vendor:
			$row = $dbtable->get_row("
				SELECT SUM(c.jumlah) AS 'jumlah'
				FROM ((smis_frm_dobat_masuk a LEFT JOIN smis_frm_stok_obat b ON a.id = b.id_dobat_masuk) LEFT JOIN smis_frm_dretur_obat c ON b.id = c.id_stok_obat) LEFT JOIN smis_frm_retur_obat d ON c.id_retur_obat = d.id
				WHERE c.prop NOT LIKE 'del' AND d.prop NOT LIKE 'del' AND d.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND d.tanggal >= '" . $tanggal_from . "' AND d.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getPenyesuaianStokPositif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_frm_igd_penyesuaian_stok_obat");
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
			 	FROM (smis_frm_penyesuaian_stok a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id) LEFT JOIN smis_frm_dobat_masuk c ON b.id_dobat_masuk = c.id
			 	WHERE a.prop NOT LIKE 'del' AND c.id_obat = '" . $id_obat . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru >= a.jumlah_lama
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			return $jumlah;
		}

		public static function getPenyesuaianStokNegatif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_frm_igd_penyesuaian_stok_obat");
			$row = $dbtable->get_row("
			 	SELECT SUM(a.jumlah_lama - a.jumlah_baru) AS 'jumlah'
			 	FROM (smis_frm_penyesuaian_stok a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id) LEFT JOIN smis_frm_dobat_masuk c ON b.id_dobat_masuk = c.id
			 	WHERE a.prop NOT LIKE 'del' AND c.id_obat = '" . $id_obat . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru < a.jumlah_lama
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			return $jumlah;
		}
	}
?>