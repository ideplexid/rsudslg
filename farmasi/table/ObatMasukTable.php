<?php
class ObatMasukTable extends Table {
	public function getContentButton($id) {
		$btn_group = new ButtonGroup("noprint");
		$btn = new Button("", "", "View");
		$btn->setAction($this->action . ".detail('" . $id . "')");
		$btn->setClass("btn-success");
		$btn->setAtribute("data-content='View' data-toggle='popover'");
		$btn->setIcon("icon-eye-open icon-white");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		$btn = new Button("", "", "Unduh XLS");
		$btn->setAction($this->action . ".download_au53('" . $id . "')");
		$btn->setClass("btn-inverse");
		$btn->setAtribute("data-content='Unduh XLS' data-toggle='popover'");
		$btn->setIcon("fa fa-download");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		$btn = new Button("", "", "Hapus");
		$btn->setAction($this->action . ".del('" . $id . "')");
		$btn->setClass("btn-danger");
		$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
		$btn->setIcon("icon-remove icon-white");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		$btn = new Button("", "", "Ubah Info. BBM");
		$btn->setAction($this->action . ".edit_info('" . $id . "')");
		$btn->setClass("btn-warning");
		$btn->setAtribute("data-content='Ubah Informasi BBM' data-toggle='popover'");
		$btn->setIcon("fa fa-pencil");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		return $btn_group;
	}
}
?>