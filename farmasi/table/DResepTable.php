<?php
class DResepTable extends Table {
	public function getHeaderButton() {
		$btn_group = new ButtonGroup("noprint");
		$btn_add_obat = new Button("", "", "Obat Jadi");
		$btn_add_obat->setAtribute("id='obat_jadi_add'");
		$btn_add_obat->setAction($this->action . ".show_add_obat_jadi_form()");
		$btn_add_obat->setClass("btn-primary");
		$btn_add_obat->setIcon("icon-plus icon-white");
		$btn_add_obat->setIsButton(Button::$ICONIC_TEXT);
		$btn_group->addElement($btn_add_obat);
		$btn_add_racikan = new Button("", "", "Obat Racikan");
		$btn_add_racikan->setAction($this->action . ".show_add_obat_racikan_form()");
		$btn_add_racikan->setAtribute("id='obat_racikan_add'");
		$btn_add_racikan->setClass("btn-info");
		$btn_add_racikan->setIcon("icon-plus icon-white");
		$btn_add_racikan->setIsButton(Button::$ICONIC_TEXT);
		$btn_group->addElement($btn_add_racikan);
		return $btn_group->getHtml();
	}
}
?>