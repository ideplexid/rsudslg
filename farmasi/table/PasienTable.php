<?php
class PasienTable extends Table {
	public function getHtml() {
		$html = parent::getHtml();
		$html .= addJS("farmasi/js/pasien_search.js", false);
		return $html;
	}
}
?>