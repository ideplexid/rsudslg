<?php
class DObatRacikanTable extends Table {
	public function getHeaderButton() {
		$btn_group = new ButtonGroup("noprint");
		$btn_add_bahan = new Button("", "", "Bahan");
		$btn_add_bahan->setAction("dresep.show_add_bahan_form()");
		$btn_add_bahan->setAtribute("id='bahan_add'");
		$btn_add_bahan->setClass("btn-primary");
		$btn_add_bahan->setIcon("icon-plus icon-white");
		$btn_add_bahan->setIsButton(Button::$ICONIC_TEXT);
		$btn_group->addElement($btn_add_bahan);
		return $btn_group->getHtml();
	}
}
?>