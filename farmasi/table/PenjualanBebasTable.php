<?php
class PenjualanBebasTable extends Table {
	public function getBodyContent() {
		$content = "";
		if ($this->content!=NULL) {
			foreach ($this->content as $d) {
				$content .= "<tr>";
				foreach ($this->header as $h) {
					$content .= "<td>" . $d[$h] . "</td>";
				}
				if ($this->is_action) {
					$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['dibatalkan'], $d['tanggal'])->getHtml() . "</td>";
				}
				$content .= "</tr>";
			}
		}
		return $content;
	}
	public function getFilteredContentButton($id, $dibatalkan, $tanggal) {
		$btn_group = new ButtonGroup("noprint");
		if ($dibatalkan) {
			$btn = new Button("", "", "Lihat Penjualan");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Lihat Alasan Batal");
			$btn->setAction($this->action . ".view_cancel_info('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Lihat Alasan Batal' data-toggle='popover'");
			$btn->setIcon("fa fa-comment-o");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		} else {
			$btn = new Button("", "", "Cetak Penjualan");
			$btn->setAction($this->action . ".print_prescription('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak Penjualan Bebas' data-toggle='popover'");
			$btn->setIcon("icon-print icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Lihat");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			global $db;
			$lock_transact_date = getSettings($db, "farmasi-penjualan-lock_transact_date", "") == "" ? date("Y-m-d H:i") : getSettings($db, "farmasi-penjualan-lock_transact_date", "") . " 23:59";
			if (strtotime($tanggal) > strtotime($lock_transact_date)) {
				$btn = new Button("", "", "Penjualan Susulan");
				$btn->setAction($this->action . ".copy_header('" . $id . "')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Penjualan Susulan' data-toggle='popover'");
				$btn->setIcon("fa fa-files-o");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			$btn = new Button("", "", "Cetak Rekap Penjualan");
			$btn->setAction($this->action . ".print_all_prescription('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak Rekap Penjualan' data-toggle='popover'");
			$btn->setIcon("fa fa-print");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			if (strtotime($tanggal) > strtotime($lock_transact_date)) {
				$btn = new Button("", "", "Batal");
				$btn->setAction($this->action . ".cancel('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Batal' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
		}
		return $btn_group;
	}
}
?>