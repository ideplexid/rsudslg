<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("farmasi/library/InventoryLibrary.php");
	global $db;

	$laporan_form = new Form("", "", "Farmasi : Pengeluaran Obat");
	$tanggal_from_text = new Text("lobk_tanggal_from", "lobk_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lobk_tanggal_to", "lobk_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lobk.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='lobk_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$lobk_table = new Table(
		array("No.", "Tanggal", "ID Obat", "Kode Obat", "Nama Obat", "Jumlah"),
		"",
		null,
		true
	);
	$lobk_table->setName("lobk");
	$lobk_table->setAction(false);
	$lobk_table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$str_tanggal_from = strtotime($tanggal_from);
			$str_tanggal_to = strtotime($tanggal_to);
			$date_diff = $str_tanggal_to - $str_tanggal_from;
			$data = array();
			$data['jumlah'] = $date_diff / (60 * 60 * 24) + 1;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$num = $_POST['num'];

			$tanggal = date("Y-m-d", strtotime("+" . $num . " day", strtotime($tanggal_from)));			
			$html = "";
			$rows = $db->get_result("
				SELECT tanggal, id_obat, kode_obat, nama_obat, SUM(jumlah) AS 'jumlah'
				FROM (
					SELECT DATE(a.tanggal) AS 'tanggal', b.id_obat, b.kode_obat, b.nama_obat, b.jumlah
					FROM smis_frm_penjualan_resep a INNER JOIN smis_frm_penjualan_obat_jadi b ON a.id = b.id_penjualan_resep
					WHERE a.tanggal >= '" . $tanggal . " 00:00' AND a.tanggal <= '" . $tanggal . " 23:59' AND a.prop NOT LIKE 'del' AND a.dibatalkan = 0 AND b.prop NOT LIKE 'del'
					UNION ALL
					SELECT DATE(a.tanggal) AS 'tanggal', c.id_obat, c.kode_obat, c.nama_obat, c.jumlah
					FROM (smis_frm_penjualan_resep a INNER JOIN smis_frm_penjualan_obat_racikan b ON a.id = b.id_penjualan_resep) INNER JOIN smis_frm_bahan_pakai_obat_racikan c ON b.id = c.id_penjualan_obat_racikan
					WHERE a.tanggal >= '" . $tanggal . " 00:00' AND a.tanggal <= '" . $tanggal . " 23:59' AND a.prop NOT LIKE 'del' AND a.dibatalkan = 0 AND b.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del'
					UNION ALL
					SELECT a.tanggal, b.id_obat, b.kode_obat, b.nama_obat, b.jumlah
					FROM smis_frm_obat_keluar a INNER JOIN smis_frm_dobat_keluar b ON a.id = b.id_obat_keluar
					WHERE a.tanggal = '" . $tanggal . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
				) v
				GROUP BY tanggal, id_obat, kode_obat, nama_obat
				ORDER BY tanggal ASC, id_obat ASC
			");
			if ($rows != null) {
				$number = 1;
				foreach ($rows as $row) {
					if ($number == 1) {
						$html .= "
							<tr>
								<td id='nomor'></td>
								<td id='tanggal'><small>" . ArrayAdapter::format("date d-m-Y", $tanggal) . "</small></td>
								<td id='id_obat'><small>" . $row->id_obat . "</small></td>
								<td id='kode_obat'><small>" . $row->kode_obat . "</small></td>
								<td id='nama_obat'><small>" . $row->nama_obat . "</small></td>
								<td id='jumlah' style='display: none'>" . $row->jumlah . "</td>
								<td id='f_jumlah'><small>" . ArrayAdapter::format("number", $row->jumlah) . "</small></td>
							</tr>
						";
					} else {
						$html .= "
							<tr>
								<td id='nomor'></td>
								<td id='tanggal'></td>
								<td id='id_obat'><small>" . $row->id_obat . "</small></td>
								<td id='kode_obat'><small>" . $row->kode_obat . "</small></td>
								<td id='nama_obat'><small>" . $row->nama_obat . "</small></td>
								<td id='jumlah' style='display: none'>" . $row->jumlah . "</td>
								<td id='f_jumlah'><small>" . ArrayAdapter::format("number", $row->jumlah) . "</small></td>
							</tr>
						";
					}
					$number++;
				}
			}
			
			$data = array();
			$data['tanggal'] = $tanggal;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("farmasi/templates/template_obat_keluar.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("OBAT KELUAR");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=H_PENGELUARAN_OBAT_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("lobk_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lobk.cancel()");
	$loading_modal = new Modal("lobk_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lobk_table->getHtml();
	echo "</div>";
	echo "<div id='lobk_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("farmasi/js/laporan_obat_keluar_action.js", false);
	echo addJS("farmasi/js/laporan_obat_keluar.js", false);
?>