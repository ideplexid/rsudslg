<?php
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");

	$form = new Form("", "", "Farmasi : Laporan Rincian Penjualan Resep");
	$tanggal_from_text = new Text("dpr_tanggal_from", "dpr_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("dpr_tanggal_to", "dpr_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$consumer_service = new ServiceConsumer(
		$db, 
		"get_jenis_patient",
		null,
		"registration" 
	);
	$content = $consumer_service->execute()->getContent();
	$jenis_pasien_option = new OptionBuilder();
	$jenis_pasien_option->add("Semua", "%%", 1);
	foreach($content as $c){
		$jenis_pasien_option->add($c['name'], $c['value']);
	}
	$jenis_pasien_select = new Select("dpr_jenis_pasien", "dpr_jenis_pasien", $jenis_pasien_option->getContent());
	$form->addElement("Jenis Pasien", $jenis_pasien_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("dpr.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='dpr_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "Tanggal", "ID Transaksi", "Lokasi", "Jenis", "Perusahaan", "Asuransi", "No. Reg.", "No. RM", "Pasien", "Alamat", "No. Resep", "Dokter", "Ruangan", "Nama Obat", "Harga", "Jumlah", "Total"),
		"",
		null,
		true
	);
	$table->setName("dpr");
	$table->setAction(false);
	$table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, "smis_frm_penjualan_resep");
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM smis_frm_penjualan_resep
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar') AND jenis LIKE '" . $jenis_pasien . "'
			");
			$data = array(
				"jumlah"	=> $row->jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_pasien = $_POST['jenis_pasien'];
			$num = $_POST['num'];
			$is_first_row = true;
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_frm_penjualan_resep
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar') AND jenis LIKE '" . $jenis_pasien . "'
				LIMIT " . $num . ", 1
			");
			$html = "";
			$total = 0;
			if ($row != null) {
				// obat jadi :
				$obat_jadi_rows = $dbtable->get_result("
					SELECT *
					FROM smis_frm_penjualan_obat_jadi
					WHERE id_penjualan_resep = '" . $row->id . "' AND prop NOT LIKE 'del'
				");
				if ($obat_jadi_rows != null) {
					foreach ($obat_jadi_rows as $obat_jadi_row) {
						$html .= "<tr>";
						if ($is_first_row) {
							$html .= "<td id='dpr_nomor'><small>" . ($num + 1) . "</small></td>";
							$is_first_row = false;
						} else {
							$html .= "<td></td>";
						}
						$subtotal = $obat_jadi_row->harga * $obat_jadi_row->jumlah;
						$total += $subtotal;
						$html .= "
								<td id='dpr_label' style='display: none;'>data</td>
								<td id='dpr_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
								<td id='dpr_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
								<td id='dpr_lokasi'><small>FARMASI</small></td>
								<td id='dpr_jenis'><small>" . ArrayAdapter::format("unslug", $row->jenis) . "</small></td>
								<td id='dpr_perusahaan'><small>" . ArrayAdapter::format("unslug", $row->perusahaan) . "</small></td>
								<td id='dpr_asuransi'><small>" . ArrayAdapter::format("unslug", $row->asuransi) . "</small></td>
								<td id='dpr_noreg_pasien'><small>" . ArrayAdapter::format("only-digit8", $row->noreg_pasien) . "</small></td>
								<td id='dpr_nrm_pasien'><small>" . ArrayAdapter::format("only-digit6", $row->nrm_pasien) . "</small></td>
								<td id='dpr_nama_pasien'><small>" . strtoupper($row->nama_pasien) . "</small></td>
								<td id='dpr_alamat_pasien'><small>" . strtoupper($row->alamat_pasien) . "</small></td>
								<td id='dpr_no_resep'><small>" . $row->nomor_resep . "</small></td>
								<td id='dpr_nama_dokter'><small>" . strtoupper($row->nama_dokter) . "</small></td>
								<td id='dpr_unit'><small>" . ArrayAdapter::format("unslug", $row->ruangan) . "</small></td>
								<td id='dpr_nama_obat'><small>" . $obat_jadi_row->nama_obat . "</small></td>
								<td id='dpr_harga' style='display: none;'>" . $obat_jadi_row->harga . "</td>
								<td id='dpr_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $obat_jadi_row->harga) . "</div></small></td>
								<td id='dpr_jumlah' style='display: none;'>" . $obat_jadi_row->jumlah . "</td>
								<td id='dpr_f_jumlah'><small>" . ArrayAdapter::format("number", $obat_jadi_row->jumlah) . "</small></td>
								<td id='dpr_subtotal' style='display: none;'>" . $subtotal . "</td>
								<td id='dpr_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
							</tr>
						";
					}
				}
				// bahan racikan :
				$bahan_racikan_rows = $dbtable->get_result("
					SELECT b.*
					FROM smis_frm_penjualan_obat_racikan a LEFT JOIN smis_frm_bahan_pakai_obat_racikan b ON a.id = b.id_penjualan_obat_racikan
					WHERE a.id_penjualan_resep = '" . $row->id . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
				");
				if ($bahan_racikan_rows != null) {
					foreach ($bahan_racikan_rows as $bahan_racikan_row) {
						$html .= "<tr>";
						if ($is_first_row) {
							$html .= "<td id='dpr_nomor'><small>" . ($num + 1) . "</small></td>";
							$is_first_row = false;
						} else {
							$html .= "<td></td>";
						}
						$subtotal = $bahan_racikan_row->harga * $bahan_racikan_row->jumlah;
						$total += $subtotal;
						$html .= "
								<td id='dpr_label' style='display: none;'>data</td>
								<td id='dpr_tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i:s", $row->tanggal) . "</small></td>
								<td id='dpr_id'><small>" . ArrayAdapter::format("only-digit8", $row->id) . "</small></td>
								<td id='dpr_lokasi'><small>DEPO FARMASI IGD</small></td>
								<td id='dpr_jenis'><small>" . ArrayAdapter::format("unslug", $row->jenis) . "</small></td>
								<td id='dpr_perusahaan'><small>" . ArrayAdapter::format("unslug", $row->perusahaan) . "</small></td>
								<td id='dpr_asuransi'><small>" . ArrayAdapter::format("unslug", $row->asuransi) . "</small></td>
								<td id='dpr_noreg_pasien'><small>" . ArrayAdapter::format("only-digit8", $row->noreg_pasien) . "</small></td>
								<td id='dpr_nrm_pasien'><small>" . ArrayAdapter::format("only-digit6", $row->nrm_pasien) . "</small></td>
								<td id='dpr_nama_pasien'><small>" . strtoupper($row->nama_pasien) . "</small></td>
								<td id='dpr_alamat_pasien'><small>" . strtoupper($row->alamat_pasien) . "</small></td>
								<td id='dpr_no_resep'><small>" . $row->nomor_resep . "</small></td>
								<td id='dpr_nama_dokter'><small>" . strtoupper($row->nama_dokter) . "</small></td>
								<td id='dpr_unit'><small>" . ArrayAdapter::format("unslug", $row->ruangan) . "</small></td>
								<td id='dpr_nama_obat'><small>" . $bahan_racikan_row->nama_obat . "</small></td>
								<td id='dpr_harga' style='display: none;'>" . $bahan_racikan_row->harga . "</td>
								<td id='dpr_f_harga'><small><div align='right'>" . ArrayAdapter::format("only-money", $bahan_racikan_row->harga) . "</div></small></td>
								<td id='dpr_jumlah' style='display: none;'>" . $bahan_racikan_row->jumlah . "</td>
								<td id='dpr_f_jumlah'><small>" . ArrayAdapter::format("number", $bahan_racikan_row->jumlah) . "</small></td>
								<td id='dpr_subtotal' style='display: none;'>" . $subtotal . "</td>
								<td id='dpr_f_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $subtotal) . "</div></small></td>
							</tr>
						";
					}
				}
				$html .= "
					<tr>
						<td id='dpr_label' colspan='17'><center><small><strong>TOTAL</strong></small></center></td>
						<td id='dpr_total' style='display: none;'>" . $total . "</td>
						<td id='dpr_f_total'><strong><small><div align='right'>" . ArrayAdapter::format("only-money", $total) . "</div></small></strong></td>
					</tr>
				";
			}
			$data = array(
				"nomor_transaksi"	=> ArrayAdapter::format("only-digit8", $row->id),
				"nomor_resep"		=> $row->nomor_resep,
				"nama_pasien"		=> strtoupper($row->nama_pasien),
				"nama_dokter"		=> strtoupper($row->nama_dokter),
				"html"				=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "1024M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("farmasi/templates/template_rincian_penjualan_resep.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("RINCIAN PENJUALAN RESEP");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				if ($d->label == "data") {
					$col_num = 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->lokasi);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->perusahaan);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->asuransi);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->noreg_pasien);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nrm_pasien);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->alamat_pasien);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no_resep);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_dokter);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->unit);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->subtotal);
					$objWorksheet->getStyle("Q" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$objWorksheet->getStyle("R" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->getStyle("S" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				} else {
					$style = array(
						"font"		=> array(
							"bold"			=> true
						)
					);
					$objWorksheet->getStyle("R" . $row_num . ":S" . $row_num)->applyFromArray($style);
					$col_num = 17;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "TOTAL");
					$col_num += 1;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total);
					$objWorksheet->getStyle("S" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				}
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=B1_RINCIAN_PENJUALAN_RESEP_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("dpr_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("dpr.cancel()");
	$loading_modal = new Modal("dpr_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='dpr_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("farmasi/js/laporan_rincian_penjualan_resep_action.js", false);
	echo addJS("farmasi/js/laporan_rincian_penjualan_resep.js", false);
?>