<?php
	global $db;
	
	$table = new Table(
		array("No.", "Kode", "Nama", "Jenis", "Produsen", "Tanggal Exp.", "No. Batch", "No. BBM", "Tanggal Masuk"),
		"Farmasi : Koreksi Info. Obat",
		null,
		true
	);
	$table->setName("perbaikan_detail_info");
	$table->setDelButtonEnable(false);
	$table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Kode", "kode_obat");
		$adapter->add("Nama", "nama_obat");
		$adapter->add("Jenis", "nama_jenis_obat");
		$adapter->add("Produsen", "produsen");
		$adapter->add("Tanggal Exp.", "tanggal_exp", "date d-m-Y");
		$adapter->add("No. Batch", "no_batch");
		$adapter->add("No. BBM", "no_bbm");
		$adapter->add("Tanggal Masuk", "tanggal_datang", "date d-m-Y");
		$dbtable = new DBTable($db, "smis_frm_stok_obat");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (a.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR c.no_bbm LIKE '%" . $_POST['kriteria'] . "%' OR c.tanggal_datang LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT a.*, c.no_bbm, c.tanggal_datang
			FROM (smis_frm_stok_obat a LEFT JOIN smis_frm_dobat_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_frm_obat_masuk c ON b.id_obat_masuk = c.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' " . $filter . "
			ORDER BY a.id DESC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("perbaikan_detail_info_add_form", "smis_form_container", "perbaikan_detail_info");
	$id_hidden = new Hidden("perbaikan_detail_info_id", "perbaikan_detail_info_id", "");
	$modal->addElement("", $id_hidden);
	$id_dobat_masuk_hidden = new Hidden("perbaikan_detail_info_id_dobat_masuk", "perbaikan_detail_info_id_dobat_masuk", "");
	$modal->addElement("", $id_dobat_masuk_hidden);
	$kode_text = new Text("perbaikan_detail_info_kode", "perbaikan_detail_info_kode", "");
	$kode_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode", $kode_text);
	$nama_text = new Text("perbaikan_detail_info_nama", "perbaikan_detail_info_nama", "");
	$nama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Nama", $nama_text);
	$jenis_text = new Text("perbaikan_detail_info_jenis", "perbaikan_detail_info_jenis", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis", $jenis_text);
	$produsen_lama_text = new Text("perbaikan_detail_info_produsen_lama", "perbaikan_detail_info_produsen_lama", "");
	$produsen_lama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Produsen Sekarang", $produsen_lama_text);
	$produsen_baru_text = new Text("perbaikan_detail_info_produsen_baru", "perbaikan_detail_info_produsen_baru", "");
	$modal->addElement("Produsen Baru", $produsen_baru_text);
	$ed_lama_text = new Text("perbaikan_detail_info_ed_lama", "perbaikan_detail_info_ed_lama", "");
	$ed_lama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Tgl. Exp. Sekarang", $ed_lama_text);
	$ed_baru_text = new Text("perbaikan_detail_info_ed_baru", "perbaikan_detail_info_ed_baru", "");
	$ed_baru_text->setClass("mydate");
	$ed_baru_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$modal->addElement("Tgl. Exp. Baru", $ed_baru_text);
	$no_batch_lama_text = new Text("perbaikan_detail_info_no_batch_lama", "perbaikan_detail_info_no_batch_lama", "");
	$no_batch_lama_text->setAtribute("disabled='disabled'");
	$modal->addElement("No. Batch Sekarang", $no_batch_lama_text);
	$no_batch_baru_text = new Text("perbaikan_detail_info_no_batch_baru", "perbaikan_detail_info_no_batch_baru", "");
	$modal->addElement("No. Bacth Baru", $no_batch_baru_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("perbaikan_detail_info.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("farmasi/js/perbaikan_detail_info_action.js", false);
	echo addJS("farmasi/js/perbaikan_detail_info.js", false);
?>