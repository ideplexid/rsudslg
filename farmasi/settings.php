<?php
	require_once("smis-framework/smis/api/SettingsBuilder.php");
	global $db;
	
	$settings_builder = new SettingsBuilder($db, "farmasi_settings", "farmasi", "settings");
	$settings_builder->setShowDescription(true);
	$settings_builder->setTabulatorMode(Tabulator::$LANDSCAPE);
	
	/// Penjualan Resep KIUP :
	$settings_builder->addTabs("farmasi_resep_settings", "Penjualan Resep (KIUP)");
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-resep-show_kategori", "Merubah Kategori Transaksi", $option->getContent(), "select", "Ya&Tab;&Tab;: Memperbolehkan Merubah Kategori Transaksi (Umum, INA CBG's, Kronis)<br/>Tidak&Tab;: Tidak Memperbolehkan Merubah Kategori Transaksi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-resep-change_jenis_pasien", "Merubah Jenis Pasien", $option->getContent(), "select", "Ya&Tab;&Tab;: Memperbolehkan Merubah Jenis Pasien<br/>Tidak&Tab;: Tidak Memperbolehkan Merubah Jenis Pasien<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-resep-show_uri", "Merubah Rawat Inap/Jalan", $option->getContent(), "select", "Ya&Tab;&Tab;: Memperbolehkan Merubah Rawat Inap/Jalan<br/>Tidak&Tab;: Tidak Memperbolehkan Merubah Rawat Inap/Jalan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-use_ruangan", "Tampilan Info. Ruangan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Ruangan<br/>Tidak&Tab;: Tidak Menampilkan Info. Ruangan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Jenis Transaksi", "0", "1");
	$option->add("Jenis Pasien", "1");
	$option->add("Obat", "2");
	$option->add("Obat dan Jenis Pasien", "3");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-mode_margin_penjualan", "Mode Margin Jual", $option->getContent(), "select", "Jenis Transaksi&Tab;: Margin Jual Sesuai Pengaturan Nilai Margin Konstan<br/>Jenis Pasien&Tab;&Tab;: Margin Jual Tergantung Jenis Pasien dan Pengaturan Margin Jual - Jenis Pasien<br/>Obat&Tab;&Tab;&Tab;: Margin Jual Sesuai Pengaturan Margin Jual - Obat<br/>Obat&Tab;&Tab;&Tab;: Margin Jual Sesuai Pengaturan Margin Jual - Obat & Jenis Pasien<br/>Default&Tab;&Tab;&Tab;: Jenis Transaksi");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-margin_penjualan", "Margin Jual Konstan", 0, "text", "Nilai Margin Jual Konstan - Mode Margin Jual Jenis Transaksi (%)");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-embalase_show-obat_jadi", "Tampilkan Info. Embalase Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Embalase Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Embalase Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-embalase-obat_jadi", "Embalase Obat Jadi", 0, "text", "Nilai Embalase Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-tuslah_show-obat_jadi", "Tampilkan Info. Tuslah Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Tuslah Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Tuslah Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-tuslah-obat_jadi", "Tuslah Obat Jadi", 0, "text", "Nilai Tuslah Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-embalase_show-bahan_racikan", "Tampilkan Info. Embalase Bahan Racikan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Embalase Bahan Racikan<br/>Tidak&Tab;: Tidak Menampilkan Info. Embalase Bahan Racikan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-embalase-bahan_racikan", "Embalase Bahan Racikan", 0, "text", "Nilai Embalase Bahan Racikan (Rp)");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-tuslah_show-bahan_racikan", "Tampilkan Info. Tuslah Bahan Racikan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Tuslah Bahan Racikan<br/>Tidak&Tab;: Tidak Menampilkan Info. Tuslah Bahan Racikan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-tuslah-bahan_racikan", "Tuslah Bahan Racikan", 0, "text", "Nilai Tuslah Bahan Racikan (Rp)");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-jasa_racik_edit", "Editable Info. Jasa Racik", $option->getContent(), "select", "Ya&Tab;&Tab;: Memperbolehkan Mengubah Info. Jasa Racik<br/>Tidak&Tab;: Tidak Memperbolehkan Mengubah Info. Jasa Racik<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-jasa_racik", "Jasa Racik", 0, "text", "Nilai Jasa Racik (Rp)");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep-enable_jumlah_luaran_racikan", "Tampilkan Info. Jml. Luaran Racikan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Jml. Luaran Racikan<br/>Tidak&Tab;: Tidak Menampilkan Info. Jml. Luaran Racikan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_settings", $settings_item);

	/// Penjualan Resep Non-KIUP :
	$settings_builder->addTabs("farmasi_resep_luar_settings", "Penjualan Resep (Non-KIUP)");
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-resep_luar-show_kategori", "Merubah Kategori Transaksi", $option->getContent(), "select", "Ya&Tab;&Tab;: Memperbolehkan Merubah Kategori Transaksi (Umum, INA CBG's, Kronis)<br/>Tidak&Tab;: Tidak Memperbolehkan Merubah Kategori Transaksi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Jenis Transaksi", "0", "1");
	$option->add("Jenis Pasien", "1");
	$option->add("Obat", "2");
	$option->add("Obat dan Jenis Pasien", "3");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-mode_margin_penjualan", "Mode Margin Jual", $option->getContent(), "select", "Jenis Transaksi&Tab;: Margin Jual Sesuai Pengaturan Nilai Margin Konstan<br/>Jenis Pasien&Tab;&Tab;: Margin Jual Tergantung Jenis Pasien dan Pengaturan Margin Jual - Jenis Pasien<br/>Obat&Tab;&Tab;&Tab;: Margin Jual Sesuai Pengaturan Margin Jual - Obat<br/>Default&Tab;&Tab;&Tab;: Jenis Transaksi");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-margin_penjualan", "Margin Jual Konstan", 0, "text", "Nilai Margin Jual Konstan - Mode Margin Jual Jenis Transaksi (%)");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-embalase_show-obat_jadi", "Tampilkan Info. Embalase Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Embalase Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Embalase Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-embalase-obat_jadi", "Embalase Obat Jadi", 0, "text", "Nilai Embalase Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-tuslah_show-obat_jadi", "Tampilkan Info. Tuslah Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Tuslah Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Tuslah Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-tuslah-obat_jadi", "Tuslah Obat Jadi", 0, "text", "Nilai Tuslah Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-embalase_show-bahan_racikan", "Tampilan Info. Embalase Bahan Racikan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Embalase Bahan Racikan<br/>Tidak&Tab;: Tidak Menampilkan Info. Embalase Bahan Racikan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-embalase-bahan_racikan", "Embalase Bahan Racikan", 0, "text", "Nilai Embalase Bahan Racikan (Rp)");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-tuslah_show-bahan_racikan", "Tampilkan Info. Tuslah Bahan Racikan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Tuslah Bahan Racikan<br/>Tidak&Tab;: Tidak Menampilkan Info. Tuslah Bahan Racikan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-tuslah-bahan_racikan", "Tuslah Bahan Racikan", 0, "text", "Nilai Tuslah Bahan Racikan (Rp)");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-jasa_racik_edit", "Editable Info. Jasa Racik", $option->getContent(), "select", "Ya&Tab;&Tab;: Memperbolehkan Mengubah Info. Jasa Racik<br/>Tidak&Tab;: Tidak Memperbolehkan Mengubah Info. Jasa Racik<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-jasa_racik", "Jasa Racik", 0, "text", "Nilai Jasa Racik (Rp)");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_resep_luar-enable_jumlah_luaran_racikan", "Tampilkan Info. Jml. Luaran Racikan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Jml. Luaran Racikan<br/>Tidak&Tab;: Tidak Menampilkan Info. Jml. Luaran Racikan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_resep_luar_settings", $settings_item);

	/// Penjualan Bebas :
	$settings_builder->addTabs("farmasi_bebas_settings", "Penjualan Bebas");
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_bebas-show_nama", "Menampilkan Isian Nama", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Isian Nama<br/>Tidak&Tab;: Tidak Menampilkan Isian Nama<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_bebas_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Jenis Transaksi", "0", "1");
	$option->add("Obat", "2");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_bebas-mode_margin_penjualan", "Mode Margin Jual", $option->getContent(), "select", "Jenis Transaksi&Tab;: Margin Jual Sesuai Pengaturan Nilai Margin Konstan<br/>Obat&Tab;&Tab;&Tab;: Margin Jual Sesuai Pengaturan Margin Jual - Obat<br/>Default&Tab;&Tab;&Tab;: Jenis Transaksi");
	$settings_builder->addItem("farmasi_bebas_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_bebas-margin_penjualan", "Margin Jual Konstan", 0, "text", "Nilai Margin Jual Konstan - Mode Margin Jual Jenis Transaksi (%)");
	$settings_builder->addItem("farmasi_bebas_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_bebas-embalase_show-obat_jadi", "Tampilan Info. Embalase Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Embalase Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Embalase Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_bebas_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_bebas-embalase-obat_jadi", "Embalase Obat Jadi", 0, "text", "Nilai Embalase Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_bebas_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_bebas-tuslah_show-obat_jadi", "Tampilan Info. Tuslah Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Tuslah Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Tuslah Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_bebas_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_bebas-tuslah-obat_jadi", "Tuslah Obat Jadi", 0, "text", "Nilai Tuslah Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_bebas_settings", $settings_item);

	/// Penjualan Resep UP :
	$settings_builder->addTabs("farmasi_instansi_lain_settings", "Penjualan UP");
	$option = new OptionBuilder();
	$option->add("Jenis Transaksi", "0", "1");
	$option->add("Rekanan", "1");
	$option->add("Obat", "2");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_instansi_lain-mode_margin_penjualan", "Mode Margin Jual", $option->getContent(), "select", "Jenis Transaksi&Tab;: Margin Jual Sesuai Pengaturan Nilai Margin Konstan<br/>Rekanan&Tab;&Tab;: Margin Jual Tergantung Pengaturan Margin Jual - Instansi Lain<br/>Obat&Tab;&Tab;&Tab;: Margin Jual Sesuai Pengaturan Margin Jual - Obat<br/>Default&Tab;&Tab;&Tab;: Jenis Transaksi");
	$settings_builder->addItem("farmasi_instansi_lain_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_instansi_lain-margin_penjualan", "Margin Jual Konstan", 0, "text", "Nilai Margin Jual Konstan - Mode Margin Jual Jenis Transaksi (%)");
	$settings_builder->addItem("farmasi_instansi_lain_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-penjualan_instansi_lain-embalase_show-obat_jadi", "Tampilan Info. Embalase Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Embalase Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Embalase Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_instansi_lain_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_instansi_lain-embalase-obat_jadi", "Embalase Obat Jadi", 0, "text", "Nilai Embalase Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_instansi_lain_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_instansi_lain-tuslah_show-obat_jadi", "Tampilan Info. Tuslah Obat Jadi", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Info. Tuslah Obat Jadi<br/>Tidak&Tab;: Tidak Menampilkan Info. Tuslah Obat Jadi<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_instansi_lain_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-penjualan_instansi_lain-tuslah-obat_jadi", "Tuslah Obat Jadi", 0, "text", "Nilai Tuslah Obat Jadi (Rp)");
	$settings_builder->addItem("farmasi_instansi_lain_settings", $settings_item);

	/// HPP :
	$settings_builder->addTabs("farmasi_hpp_settings", "HPP");
	$option = new OptionBuilder();
	$option->add("Maks. Nilai Pembelian", "0", "1");
	$option->add("Maks. Nilai Stok Tersisa", "1");
	$settings_item = new SettingsItem($db, "farmasi-hpp-patokan_hpp", "Perhitungan HPP", $option->getContent(), "select", "Maks. Nilai Pembelian&Tab;: HPP Berdasarkan Nilai Tertinggi dari Stok yang Pernah Dibeli<br/>Maks. Nilai Stok Tersisa&Tab;: HPP Berdasarkan Nilai Tertinggi dari Stok yang Tersedia<br/>Default&Tab;&Tab;&Tab;&Tab;: Maks. Nilai Pembelian");
	$settings_builder->addItem("farmasi_hpp_settings", $settings_item);

	/// Retur Penjualan :
	$settings_builder->addTabs("retur_penjualan_resep", "Retur Penjualan");
	$option = new OptionBuilder();
	$option->add("Cetak Sekali", "1", "1");
	$option->add("Cetak Berulang Kali", "0");
	$settings_item = new SettingsItem($db, "depo_farmasi-retur_penjualan-print_once", "Pilihan Mode Cetak Retur Penjualan Resep/Bebas", $option->getContent(), "select", "Cetak Sekali&Tab;&Tab;&Tab;: Hanya Diperbolehkan Sekali Cetak Nota<br/>Cetak Berulang Kali&Tab;: Diperbolehkan Cetak Lebih dari Sekali<br>Default&Tab;&Tab;&Tab;&Tab;: Cetak Sekali");
	$settings_builder->addItem("retur_penjualan_resep", $settings_item);

	/// Lock Transaksi :
	$settings_builder->addTabs("lock_transact", "Kunci Transaksi");
	$settings_item = new SettingsItem($db, "farmasi-penjualan-lock_transact_date", "Tgl. Kunci Transaksi Penjualan", "", "date", "Tanggal Penguncian Transaksi Penjualan (YYYY-mm-dd)");
	$settings_builder->addItem("lock_transact", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-retur_penjualan-lock_transact_date", "Tgl. Kunci Transaksi Retur Penjualan", "", "date", "Tanggal Penguncian Transaksi Retur Penjualan (YYYY-mm-dd)");
	$settings_builder->addItem("lock_transact", $settings_item);

	/// Laporan :
	$settings_builder->addTabs("farmasi_laporan_settings", "Laporan");
	$option = new OptionBuilder();
	$option->add("Ya", "1", "1");
	$option->add("Tidak", "0");
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_rekap_penjualan", "Tampilkan Lap. Rekap Penjualan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Rekap Penjualan<br/>Tidak&Tab;: Tidak Menampilkan Lap. Rekap Penjualan<br/>Default&Tab;: Ya");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_detail_penjualan", "Tampilkan Lap. Detail Penjualan", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Detail Penjualan<br/>Tidak&Tab;: Tidak Menampilkan Lap. Detail Penjualan<br/>Default&Tab;: Ya");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_rekap_pembelian", "Tampilkan Lap. Rekap Pembelian", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Rekap Pembelian<br/>Tidak&Tab;: Tidak Menampilkan Lap. Rekap Pembelian<br/>Default&Tab;: Ya");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_detail_pembelian", "Tampilkan Lap. Detail Pembelian", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Detail Pembelian<br/>Tidak&Tab;: Tidak Menampilkan Lap. Detail Pembelian<br/>Default&Tab;: Ya");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_so", "Tampilkan Lap. Stok Opname", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Stok Opname<br/>Tidak&Tab;: Tidak Menampilkan Lap. Stok opname<br/>Default&Tab;: Ya");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_mutasi_obat", "Tampilkan Lap. Mutasi Obat", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Mutasi Obat<br/>Tidak&Tab;: Tidak Menampilkan Lap. Mutasi Obat<br/>Default&Tab;: Ya");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_mutasi_obat_unit_lain", "Tampilkan Lap. Mutasi Obat Ext.", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Mutasi Obat Ext.<br/>Tidak&Tab;: Tidak Menampilkan Lap. Mutasi Obat Ext.<br/>Default&Tab;: Ya");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_biaya_racik", "Tampilkan Lap. Biaya Racik", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Biaya Racik<br/>Tidak&Tab;: Tidak Menampilkan Lap. Biaya Racik<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	$settings_item = new SettingsItem($db, "farmasi-laporan-show_lap_jaspel_skw", "Tampilkan Lap. Jasa Pelayanan (SKW)", $option->getContent(), "select", "Ya&Tab;&Tab;: Menampilkan Lap. Jasa Pelayanan (SKW)<br/>Tidak&Tab;: Tidak Menampilkan Lap. Jasa Pelayanan<br/>Default&Tab;: Tidak");
	$settings_builder->addItem("farmasi_laporan_settings", $settings_item);
	
	/// Layanan Data :
	$settings_builder->addTabs("farmasi_service_settings", "Layanan Data");
	$option = new OptionBuilder();
	$option->add("Detail", "get_detail_tagihan.php");
	$option->add("Rekap", "get_simple_tagihan.php", "1");
	$settings_item = new SettingsItem($db, "farmasi-service-get_tagihan", "Layanan Data Tagihan", $option->getContent(), "select", "Detail&Tab;: Menampilkan Data Tagihan Obat Secara Detail Per Obat<br/>Rekap&Tab;: Menampilkan Data Tagihan Terekap Tunggal<br/>Default&Tab;: Rekap");
	$settings_builder->addItem("farmasi_service_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Ya", "1");
	$option->add("Tidak", "0", "1");
	$settings_item = new SettingsItem($db, "farmasi-resep-jenis_pasien_registration", "Layanan Data Jenis Pasien", $option->getContent(), "select", "Ya&Tab;&Tab;: Terintegrasi dengan Data Jenis Pasien di Pendaftaran<br/>Tidak&Tab;: Tidak Terintegrasi dengan Data Jenis Pasien di Pendaftaran<br/>Default&Tab;: Tidak");	
	$settings_builder->addItem("farmasi_service_settings", $settings_item);
	$option = new OptionBuilder();
	$option->add("Accrual Basis", "accrual", "1");
	$option->add("Cash Basis", "cash");
	$settings_item = new SettingsItem($db, "farmasi-resep-notify_penjualan_accounting", "Layanan Data Mode Notify Acc. Trans. Penjualan", $option->getContent(), "select", "Accrual Basis&Tab;: Penjualan KIUP menotifikasi modul Akuntansi terkait transaksi penjualan berdasarkan Accrual Basis<br/>Cash Basis&Tab;: Penjualan KIUP menotifikasi modul Akuntansi terkait transaksi penjualan berdasarkan Cash Basis<br/>Default&Tab;&Tab;: Accrual Basis");	
	$settings_builder->addItem("farmasi_service_settings", $settings_item);

	/// Locker :
	$settings_builder->addTabs("farmasi_locker_settings", "Trans. Locker");
	$option = new OptionBuilder();
	$option->add("Aktif", "1", "1");
	$option->add("Non-Aktif", "0");
	$settings_item = new SettingsItem($db, "farmasi-locker-penjualan_resep", "L. Penjualan", $option->getContent(), "select", "Aktif&Tab;&Tab;: Locker Penjualan Diaktifkan<br/>Non-Aktif&Tab;: Locker Penjualan Dinonaktifkan<br/>Default&Tab;: Aktif");
	$settings_builder->addItem("farmasi_locker_settings", $settings_item);
	
	$response = $settings_builder->init();
?>