<?php
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");

	$form = new Form("", "", "Farmasi : Laporan Biaya Racik");
	$tanggal_from_text = new Text("lbr_tanggal_from", "lbr_tanggal_from", date("Y-m-d 00:00"));
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lbr_tanggal_to", "lbr_tanggal_to", date("Y-m-d H:i"));
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lbr.view()");
	$download_button = new Button("", "", "Eksport XLS");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='lbr_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "ID. Trans. Resep", "No. RM", "No. Reg.", "Tanggal", "Nama Pasien", "Nama Racikan", "Jumlah Racikan", "Biaya Racik", "Jenis Pasien"),
		"",
		null,
		true
	);
	$table->setName("lbr");
	$table->setAction(false);
	$table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, "smis_frm_penjualan_resep");
		if ($_POST['command'] == "get_jumlah") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM smis_frm_penjualan_resep
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar')
			");
			$data = array(
				"jumlah"	=> $row->jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$num = $_POST['num'];
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_frm_penjualan_resep
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND dibatalkan = '0' AND prop NOT LIKE 'del' AND dibatalkan = '0' AND (tipe LIKE 'resep' OR tipe LIKE 'resep_luar')
				LIMIT " . $num . ", 1
			");
			$html = "";
			if ($row != null) {
				$obat_racikan_rows = $dbtable->get_result("
					SELECT a.id_penjualan_resep, a.id, a.nama, SUM(b.tusla) + a.biaya_racik AS 'jasa_racik', a.jumlah_luaran, a.satuan_luaran
					FROM smis_frm_penjualan_obat_racikan a LEFT JOIN smis_frm_bahan_pakai_obat_racikan b ON a.id = b.id_penjualan_obat_racikan
					WHERE a.id_penjualan_resep = '" . $row->id . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
					GROUP BY a.id
				");
				if ($obat_racikan_rows != null) {
					foreach ($obat_racikan_rows as $orr) {
						$html .= "
							<tr id='data_" . $num . "'>
								<td id='nomor'></td>
								<td id='id_resep'><small>" . $row->id . "</small></td>
								<td id='nrm_pasien'><small>" . $row->nrm_pasien . "</small></td>
								<td id='noreg_pasien'><small>" . $row->noreg_pasien . "</small></td>
								<td id='tanggal'><small>" . ArrayAdapter::format("date d-m-Y, H:i", $row->tanggal) . "</small></td>
								<td id='nama_pasien'><small>" . $row->nama_pasien . "</small></td>
								<td id='nama_racikan'><small>" . $orr->nama . "</small></td>
								<td id='jumlah_racikan'><small>" . $orr->jumlah_luaran . " " . $orr->satuan_luaran . "</small></td>
								<td id='biaya_racik' style='display: none;'>" . $orr->jasa_racik . "</td>
								<td id='f_biaya_racik'><small>" . ArrayAdapter::format("money", $orr->jasa_racik) . "</small></td>
								<td id='jenis_pasien'><small>" . ArrayAdapter::format("unslug", $row->jenis) . "</small></td>
							</tr>
						";
					}
				}
			}
			$data = array(
				"nomor_transaksi"	=> ArrayAdapter::format("only-digit8", $row->id),
				"nomor_resep"		=> $row->nomor_resep,
				"nama_pasien"		=> $row->nama_pasien,
				"nama_dokter"		=> $row->nama_dokter,
				"html"				=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			ini_set("memory_limit", "256M");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("farmasi/templates/template_biaya_racik.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("BIAYA RACIK");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("N9")->getNumberFormat()->setFormatCode("#,##0.00");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_resep);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nrm_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->noreg_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_pasien);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_racikan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_racikan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->biaya_racik);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jenis_pasien);
				$objWorksheet->getStyle("N" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=SW_BIAYA_RACIK_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd_Hi", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("lbr_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lbr.cancel()");
	$loading_modal = new Modal("lbr_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='lbr_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
	echo addJS("farmasi/js/laporan_biaya_racik_action.js", false);
	echo addJS("farmasi/js/laporan_biaya_racik.js", false);
?>