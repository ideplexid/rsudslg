<?php 	
	require_once("farmasi/table/ReturKeluarTable.php");
	require_once("farmasi/adapter/ReturKeluarAdapter.php");
	require_once("farmasi/responder/ReturKeluarDBResponder.php");
	require_once("farmasi/responder/ReturKeluarObatMasukDBResponder.php");

	$retur_keluar_table = new ReturKeluarTable(
		array("Nomor", "Tanggal", "Jenis Stok", "Vendor", "No. Faktur", "Tgl. Faktur", "Status"),
		"Farmasi : Retur Pembelian Obat",
		null,
		true
	);
	$retur_keluar_table->setName("retur");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "retur") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "export_xls") {
				require_once("smis-libs-out/php-excel/PHPExcel.php");
				$id = $_POST['id'];
				
				$objPHPExcel = PHPExcel_IOFactory::load("farmasi/templates/template_retur_pembelian.xlsx");
				$objPHPExcel->setActiveSheetIndexByName("RETUR PEMBELIAN");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				
				$dbtable = new DBTable($db, "smis_frm_retur_obat");
				$header_info = $dbtable->get_row("
					SELECT *
					FROM smis_frm_retur_obat
					WHERE id = '" . $id . "'
				");
				$objWorksheet->setCellValue("B3", "REKANAN : " . ArrayAdapter::format("unslug", $header_info->nama_vendor));
				$objWorksheet->setCellValue("B4", "TANGGAL : " . ArrayAdapter::format("date d-m-Y", $header_info->tanggal));
				$detail_info = $dbtable->get_result("
					SELECT a.*, b.kode_obat, b.nama_obat, b.tanggal_exp, b.satuan
					FROM smis_frm_dretur_obat a LEFT JOIN smis_frm_stok_obat b ON a.id_stok_obat = b.id
					WHERE id_retur_obat = '" . $id . "'
				");
				if (count($detail_info) - 2 > 0)
					$objWorksheet->insertNewRowBefore(9, count($detail_info) - 2);

				$start_row_num = 8;
				$end_row_num = 9;
				$row_num = $start_row_num;
				$number = 1;
				foreach ($detail_info as $di) {
					$kode_obat = "";
					if (substr($di->kode_obat, 0, 3) === "REG")
						$kode_obat = "309" . substr($di->kode_obat, 3);
					else if (substr($di->kode_obat, 0, 3) === "JKN")
						$kode_obat = "319" . substr($di->kode_obat, 3);
					$objWorksheet->setCellValue("B" . $row_num, $number++);
					$objWorksheet->setCellValue("C" . $row_num, $kode_obat);
					$objWorksheet->setCellValue("D" . $row_num, $di->nama_obat);
					$objWorksheet->setCellValue("E" . $row_num, $di->jumlah);
					$objWorksheet->setCellValue("F" . $row_num, $di->satuan);
					$objWorksheet->setCellValue("G" . $row_num, ArrayAdapter::format("date d-m-Y", $di->tanggal_exp));
					$objWorksheet->setCellValue("H" . $row_num, $di->keterangan);
					$objWorksheet->getStyle("E" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$row_num++;
				}
				
				header("Content-type: application/vnd.ms-excel");	
				header("Content-Disposition: attachment; filename=RETUR_PEMBELIAN-" . ArrayAdapter::format("only-digit6", $header_info->id) . "_" . ArrayAdapter::format("date Ymd", $header_info->tanggal) . ".xlsx");
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				$objWriter->save("php://output");
				return;
			}
			$retur_obat_adapter = new ReturObatAdapter();
			$columns = array("id", "id_obat_masuk", "id_vendor", "nama_vendor", "no_faktur", "tanggal_faktur", "tanggal", "dibatalkan");
			$retur_obat_dbtable = new DBTable(
				$db,
				"smis_frm_retur_obat",
				$columns
			);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (smis_frm_retur_obat.no_faktur LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_retur_obat.nama_vendor LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT smis_frm_retur_obat.*, smis_frm_obat_masuk.tipe
				FROM smis_frm_retur_obat LEFT JOIN smis_frm_obat_masuk ON smis_frm_retur_obat.id_obat_masuk = smis_frm_obat_masuk.id
				WHERE smis_frm_retur_obat.prop NOT LIKE 'del' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_frm_retur_obat.*, smis_frm_obat_masuk.tipe
					FROM smis_frm_retur_obat LEFT JOIN smis_frm_obat_masuk ON smis_frm_retur_obat.id_obat_masuk = smis_frm_obat_masuk.id
					WHERE smis_frm_retur_obat.prop NOT LIKE 'del' " . $filter . "
				) v_retur
			";
			$retur_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			$retur_obat_dbresponder = new ReturObatDBResponder(
				$retur_obat_dbtable,
				$retur_keluar_table,
				$retur_obat_adapter
			);
			$data = $retur_obat_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	//obat masuk chooser:
	$obat_masuk_table = new Table(
		array("Nomor", "No. SP", "Vendor", "No. Faktur", "Tgl. Faktur", "Tgl. Datang", "Jatuh Tempo"),
		"",
		null,
		true
	);
	$obat_masuk_table->setName("obat_masuk");
	$obat_masuk_table->setModel(Table::$SELECT);
	$obat_masuk_adapter = new SimpleAdapter();
	$obat_masuk_adapter->add("Nomor", "id", "digit8");
	$obat_masuk_adapter->add("No. SP", "id_po", "digit8");
	$obat_masuk_adapter->add("Vendor", "nama_vendor");
	$obat_masuk_adapter->add("No. Faktur", "no_faktur");
	$obat_masuk_adapter->add("Tgl. Faktur", "tanggal", "date d M Y");
	$obat_masuk_adapter->add("Tgl. Datang", "tanggal_datang", "date d M Y");
	$obat_masuk_adapter->add("Jatuh Tempo", "tanggal_tempo", "date d M Y");
	$obat_masuk_dbtable = new DBTable($db, "smis_frm_obat_masuk");
	$obat_masuk_dbtable->addCustomKriteria(" tipe ", " = 'farmasi' ");
	$obat_masuk_dbtable->addCustomKriteria(" no_faktur ", " != '-' ");
	$obat_masuk_dbtable->addCustomKriteria(" tanggal ", " != '0000-00-00' ");
	$obat_masuk_dbresponder = new ReturKeluarObatMasukDBResponder(
		$obat_masuk_dbtable,
		$obat_masuk_table,
		$obat_masuk_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat_masuk", $obat_masuk_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$retur_modal = new Modal("retur_add_form", "smis_form_container", "retur");
	$retur_modal->setTitle("Data Retur Obat");
	$retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("retur_id", "retur_id", "");
	$retur_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("retur_tanggal", "retur_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d' disabled='disabled'");
	$retur_modal->addElement("Tanggal", $tanggal_text);
	$obat_masuk_button = new Button("", "", "Pilih");
	$obat_masuk_button->setClass("btn-info");
	$obat_masuk_button->setIsButton(Button::$ICONIC);
	$obat_masuk_button->setIcon("icon-white ".Button::$icon_list_alt);
	$obat_masuk_button->setAction("obat_masuk.chooser('obat_masuk', 'obat_masuk_button', 'obat_masuk', obat_masuk)");
	$obat_masuk_button->setAtribute("id='obat_masuk_browse'");
	$obat_masuk_text = new Text("retur_no_faktur", "retur_no_faktur", "");
	$obat_masuk_text->setAtribute("disabled='disabled'");
	$obat_masuk_text->setClass("smis-one-option-input");
	$obat_masuk_input_group = new InputGroup("");
	$obat_masuk_input_group->addComponent($obat_masuk_text);
	$obat_masuk_input_group->addComponent($obat_masuk_button);
	$retur_modal->addElement("No. Faktur", $obat_masuk_input_group);
	$id_obat_masuk_hidden = new Hidden("retur_id_obat_masuk", "retur_id_obat_masuk", "");
	$retur_modal->addElement("", $id_obat_masuk_hidden);
	$id_vendor_hidden = new Hidden("retur_id_vendor", "retur_id_vendor", "");
	$retur_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("retur_nama_vendor", "retur_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Vendor", $nama_vendor_text);
	$tanggal_faktur_text = new Text("retur_tanggal_faktur", "retur_tanggal_faktur", "");
	$tanggal_faktur_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Tgl. Faktur", $tanggal_faktur_text);
	$dretur_table = new Table(
		array("Nama Obat", "Jenis Obat", "Produsen", "Tgl. Exp.", "Sisa", "Jml. Retur", "Keterangan"),
		"",
		null,
		true
	);
	$dretur_table->setName("dretur");
	$dretur_table->setFooterVisible(false);
	$dretur_table->setAddButtonEnable(false);
	$dretur_table->setReloadButtonEnable(false);
	$dretur_table->setPrintButtonEnable(false);
	$retur_modal->addBody("dretur_table", $dretur_table);
	$retur_button = new Button("", "", "Simpan");
	$retur_button->setClass("btn-success");
	$retur_button->setIcon("fa fa-floppy-o");
	$retur_button->setIsButton(Button::$ICONIC);
	$retur_button->setAtribute("id='retur_save'");
	$retur_button->setAction("retur.save()");
	$retur_modal->addFooter($retur_button);
	
	$dretur_modal = new Modal("dretur_add_form", "smis_form_container", "dretur");
	$dretur_modal->setTitle("Data Detail Retur Obat");
	$id_hidden = new Hidden("dretur_id", "dretur_id", "");
	$dretur_modal->addElement("", $id_hidden);
	$nama_obat_text = new Text("dretur_nama_obat", "dretur_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Nama Obat", $nama_obat_text);
	$jenis_obat_text = new Text("dretur_jenis_obat", "dretur_jenis_obat", "");
	$jenis_obat_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Jenis Obat", $jenis_obat_text);
	$produsen_text = new Text("dretur_produsen", "dretur_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Produsen", $produsen_text);
	$tanggal_exp_text = new Text("dretur_tanggal_exp", "dretur_tanggal_exp", "");
	$tanggal_exp_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Tgl. Exp.", $tanggal_exp_text);
	$sisa_hidden = new Hidden("dretur_sisa", "dretur_sisa", "");
	$dretur_modal->addElement("", $sisa_hidden);
	$f_sisa_text = new Text("dretur_f_sisa", "dretur_f_sisa", "");
	$f_sisa_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Sisa", $f_sisa_text);
	$jumlah_retur_text = new Text("dretur_jumlah_retur", "dretur_jumlah_retur", "");
	$dretur_modal->addElement("Jml. Retur", $jumlah_retur_text);
	$satuan_text = new Text("dretur_satuan", "dretur_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("dretur_keterangan", "dretur_keterangan", "");
	$dretur_modal->addElement("Keterangan", $keterangan_textarea);
	$dretur_button = new Button("", "", "Simpan");
	$dretur_button->setClass("btn-success");
	$dretur_button->setIcon("fa fa-floppy-o");
	$dretur_button->setIsButton(Button::$ICONIC);
	$dretur_button->setAtribute("id='dretur_save'");
	$dretur_modal->addFooter($dretur_button);
	
	$v_retur_modal = new Modal("v_retur_add_form", "smis_form_container", "v_retur");
	$v_retur_modal->setTitle("Data Retur Obat");
	$v_retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("v_retur_id", "v_retur_id", "");
	$v_retur_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("v_retur_tanggal", "v_retur_tanggal", "");
	$tanggal_text->setAtribute("disabled");
	$v_retur_modal->addElement("Tanggal", $tanggal_text);
	$no_faktur_text = new Text("v_retur_no_faktur", "v_retur_no_faktur", "");
	$no_faktur_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Faktur", $no_faktur_text);
	$id_obat_masuk_hidden = new Hidden("v_retur_id_obat_masuk", "v_retur_id_obat_masuk", "");
	$v_retur_modal->addElement("", $id_obat_masuk_hidden);
	$id_vendor_hidden = new Hidden("v_retur_id_vendor", "v_retur_id_vendor", "");
	$v_retur_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("v_retur_nama_vendor", "v_retur_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Vendor", $nama_vendor_text);
	$tanggal_faktur_text = new Text("v_retur_tanggal_faktur", "v_retur_tanggal_faktur", "");
	$tanggal_faktur_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Tgl. Faktur", $tanggal_faktur_text);
	$v_dretur_table = new Table(
		array("Nama Obat", "Jenis Obat", "Produsen", "Tgl. Exp.", "Jml. Retur", "Keterangan"),
		"",
		null,
		true
	);
	$v_dretur_table->setName("v_dretur");
	$v_dretur_table->setFooterVisible(false);
	$v_dretur_table->setAddButtonEnable(false);
	$v_dretur_table->setReloadButtonEnable(false);
	$v_dretur_table->setPrintButtonEnable(false);
	$v_retur_modal->addBody("v_dretur_table", $v_dretur_table);
	$v_retur_button = new Button("", "", "OK");
	$v_retur_button->setClass("btn-success");
	$v_retur_button->setAction("$($(this).data('target')).smodal('hide')");
	$v_retur_modal->addFooter($v_retur_button);
	
	echo $v_retur_modal->getHtml();
	echo $dretur_modal->getHtml();
	echo $retur_modal->getHtml();
	echo $retur_keluar_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("farmasi/js/retur_keluar_action.js", false);
	echo addJS("farmasi/js/dretur_keluar_action.js", false);
	echo addJS("farmasi/js/retur_keluar_obat_masuk_action.js", false);
	echo addJS("farmasi/js/retur_keluar.js", false);
?>