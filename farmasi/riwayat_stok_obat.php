<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("farmasi/adapter/StokObatAdapter.php");
	
	$riwayat_stok_form = new Form("riwayat_stok_form", "", "Farmasi : Riwayat Stok Obat");
	$id_stok_obat_text = new Text("riwayat_stok_id_stok_obat", "riwayat_stok_id_stok_obat", "");
	$id_stok_obat_text->setClass("smis-one-option-input");
	$id_stok_obat_text->setAtribute("disabled='disabled'");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$browse_button->setAction("stok_obat.chooser('stok_obat', 'stok_obat_button', 'stok_obat', stok_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($id_stok_obat_text);
	$input_group->addComponent($browse_button);
	$riwayat_stok_form->addElement("No. Stok", $input_group);
	$nama_obat_text = new Text("riwayat_stok_nama_obat", "riwayat_stok_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$riwayat_stok_form->addElement("Nama Obat", $nama_obat_text);
	$jenis_obat_text = new Text("riwayat_stok_jenis_obat", "riwayat_stok_jenis_obat", "");
	$jenis_obat_text->setAtribute("disabled='disabled'");
	$riwayat_stok_form->addElement("Jenis Obat", $jenis_obat_text);
	$jenis_stok_text = new Text("riwayat_stok_jenis_stok", "riwayat_stok_jenis_stok", "");
	$jenis_stok_text->setAtribute("disabled='disabled'");
	$riwayat_stok_form->addElement("Jenis Stok", $jenis_stok_text);
	$produsen_text = new Text("riwayat_stok_produsen", "riwayat_stok_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$riwayat_stok_form->addElement("Produsen", $produsen_text);
	$vendor_text = new Text("riwayat_stok_vendor", "riwayat_stok_vendor", "");
	$vendor_text->setAtribute("disabled='disabled'");
	$riwayat_stok_form->addElement("Vendor", $vendor_text);
	$satuan_text = new Text("riwayat_stok_satuan", "riwayat_stok_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$riwayat_stok_form->addElement("Satuan", $satuan_text);
	$ed_text = new Text("riwayat_stok_ed", "riwayat_stok_ed", "");
	$ed_text->setAtribute("disabled='disabled'");
	$riwayat_stok_form->addElement("Tgl. ED", $ed_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("riwayat_stok.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("riwayat_stok.print()");
	$button_group = new ButtonGroup("noprint");
	$button_group->addElement($show_button);
	$button_group->addElement($print_button);
	$riwayat_stok_form->addElement("", $button_group);
	$riwayat_stok_table = new Table(
		array("Nomor", "Tanggal", "Masuk", "Keluar", "Sisa", "Keterangan", "Petugas Entri"),
		"",
		null,
		true
	);
	$riwayat_stok_table->setName("riwayat_stok");
	$riwayat_stok_table->setAction(false);
	$riwayat_stok_adapter = new SimpleAdapter();
	$riwayat_stok_adapter->add("Nomor", "id", "digit8");
	$riwayat_stok_adapter->add("Tanggal", "tanggal", "date d M Y");
	$riwayat_stok_adapter->add("Masuk", "jumlah_masuk", "front +");
	$riwayat_stok_adapter->add("Keluar", "jumlah_keluar", "front -");
	$riwayat_stok_adapter->add("Sisa", "sisa");
	$riwayat_stok_adapter->add("Keterangan", "keterangan");
	$riwayat_stok_adapter->add("Petugas Entri", "nama_user");
	$riwayat_stok_dbtable = new DBTable($db, "smis_frm_riwayat_stok_obat");
	if (isset($_POST['super_command']) && $_POST['super_command'] == "riwayat_stok") {
		if (isset($_POST['command'])) {
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND  (keterangan LIKE '%" . $_POST['kriteria'] . "%' OR nama_user LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM smis_frm_riwayat_stok_obat
				WHERE id_stok_obat = '" . $_POST['id_stok_obat'] . "' " . $filter . " AND prop NOT LIKE 'del'
			";
			$query_count = "
				SELECT COUNT(*)
				FROM smis_frm_riwayat_stok_obat
				WHERE id_stok_obat = '" . $_POST['id_stok_obat'] . "' " . $filter . " AND prop NOT LIKE 'del'
			";
			$riwayat_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
	}
	$riwayat_stok_dbresponder = new DBResponder(
		$riwayat_stok_dbtable,
		$riwayat_stok_table,
		$riwayat_stok_adapter
	);
	
	//get stok obat chooser:
	$stok_obat_table = new Table(
		array("No. Stok", "Nama Obat", "Jenis Obat", "Jenis Stok", "Produsen", "Vendor", "Satuan", "Tgl. ED"),
		"",
		null,
		true
	);
	$stok_obat_table->setName("stok_obat");
	$stok_obat_table->setModel(Table::$SELECT);
	$stok_obat_adapter = new StokObatAdapter();
	$columns = array("id", "nama_obat", "nama_jenis_obat", "label", "produsen", "nama_vendor", "satuan", "tanggal_exp");
	$stok_obat_dbtable = new DBTable(
		$db, 
		"smis_frm_stok_obat", 
		$columns
	);
	$stok_obat_dbresponder = new DBResponder(
		$stok_obat_dbtable,
		$stok_obat_table,
		$stok_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("riwayat_stok", $riwayat_stok_dbresponder);
	$super_command->addResponder("stok_obat", $stok_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	echo $riwayat_stok_form->getHtml();
	echo "<div id='table_content'>";
	echo $riwayat_stok_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/riwayat_stok_action.js", false);
	echo addJS("farmasi/js/riwayat_stok_stok_obat_action.js", false);
	echo addJS("farmasi/js/riwayat_stok.js", false);
?>