<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("farmasi/table/ReturObatTable.php");
	require_once("farmasi/adapter/ReturObatAdapter.php");
	require_once("farmasi/responder/ReturObatDBResponder.php");
	require_once("farmasi/service_consumer/SetReturObatStatusServiceConsumer.php");
	
	$retur_obat_table = new ReturObatTable(
		array("Unit", "Nomor", "Tanggal Retur", "Nama Obat", "Jenis Obat", "Produsen", "Vendor", "Jumlah", "Status"),
		"Farmasi : Retur Obat Unit",
		null,
		true
	);
	$retur_obat_table->setName("retur_obat");
	$retur_obat_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		$retur_obat_adapter = new ReturObatAdapter();
		$columns = array("id", "f_id", "unit", "tanggal", "id_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "no_batch", "turunan", "keterangan", "status", "id_vendor", "nama_vendor", "produsen", "restok");
		$retur_obat_dbtable = new DBTable(
			$db,
			"smis_frm_retur_obat_unit",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%' OR nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR produsen LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM smis_frm_retur_obat_unit
			WHERE prop NOT LIKE 'del' AND status != 'dikembalikan' " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_frm_retur_obat_unit
			WHERE prop NOT LIKE 'del' AND status != 'dikembalikan' " . $filter . "
		";
		$retur_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$retur_obat_dbresponder = new ReturObatDBResponder(
			$retur_obat_dbtable,
			$retur_obat_table,
			$retur_obat_adapter
		);
		$data = $retur_obat_dbresponder->command($_POST['command']);
		//set_retur_obat_status:
		if (isset($_POST['push_command'])) {
			$retur_obat_dbtable = new DBTable($db, "smis_frm_retur_obat_unit");
			$retur_obat_row = $retur_obat_dbtable->get_row("
				SELECT f_id, unit, status
				FROM smis_frm_retur_obat_unit
				WHERE id = '" . $data['content']['id'] . "'
			");
			$command = "push_" . $_POST['push_command'];
			$set_retur_obat_status_service_consumer = new SetReturObatStatusServiceConsumer($db, $retur_obat_row->f_id, $retur_obat_row->status, $retur_obat_row->unit, $command);
			$set_retur_obat_status_service_consumer->execute();
		}
		echo json_encode($data);
		return;
	}
	
	$retur_obat_modal = new Modal("retur_obat_add_form", "smis_form_container", "retur_obat");
	$retur_obat_modal->setTitle("Retur Obat");
	$id_hidden = new Hidden("retur_obat_id", "retur_obat_id", "");
	$retur_obat_modal->addElement("", $id_hidden);
	$id_stok_obat_hidden = new Hidden("retur_obat_id_stok_obat", "retur_obat_id_stok_obat", "");
	$retur_obat_modal->addElement("", $id_stok_obat_hidden);
	$tanggal_text = new Text("retur_obat_tanggal", "retur_obat_tanggal", "");
	$tanggal_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Tanggal", $tanggal_text);
	$nama_obat_text = new Text("retur_obat_nama_obat", "retur_obat_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Nama Obat", $nama_obat_text);
	$nama_jenis_text = new Text("retur_obat_jenis", "retur_obat_jenis", "");
	$nama_jenis_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Jenis Obat", $nama_jenis_text);
	$produsen_text = new Text("retur_obat_produsen", "retur_obat_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Produsen", $produsen_text);
	$vendor_text = new Text("retur_obat_vendor", "retur_obat_vendor", "");
	$vendor_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Vendor", $vendor_text);
	$f_jumlah_text = new Text("retur_obat_f_jumlah", "retur_obat_f_jumlah", "");
	$f_jumlah_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Jumlah", $f_jumlah_text);
	$keterangan_textarea = new TextArea("retur_obat_keterangan", "retur_obat_keterangan", "");
	$keterangan_textarea->setLine(2);
	$keterangan_textarea->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Keterangan", $keterangan_textarea);
	$retur_obat_button = new Button("", "", "Terima");
	$retur_obat_button->setClass("btn-success");
	$retur_obat_button->setAtribute("id='retur_obat_accept'");
	$retur_obat_button->setAction("retur_obat.accept()");
	$retur_obat_button->setIcon("fa fa-check");
	$retur_obat_button->setIsButton(Button::$ICONIC);
	$retur_obat_modal->addFooter($retur_obat_button);
	$retur_obat_button = new Button("", "", "Kembalikan");
	$retur_obat_button->setClass("btn-danger");
	$retur_obat_button->setAtribute("id='retur_obat_return'");
	$retur_obat_button->setAction("retur_obat.return()");
	$retur_obat_button->setIcon("fa fa-times");
	$retur_obat_button->setIsButton(Button::$ICONIC);
	$retur_obat_modal->addFooter($retur_obat_button);
	$retur_obat_button = new Button("", "", "OK");
	$retur_obat_button->setClass("btn-success");
	$retur_obat_button->setAtribute("id='retur_obat_ok'");
	$retur_obat_button->setAction("$($(this).data('target')).smodal('hide')");
	$retur_obat_modal->addFooter($retur_obat_button);
	
	echo $retur_obat_modal->getHtml();
	echo $retur_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/retur_obat_action.js", false);
	echo addJS("farmasi/js/retur_obat.js", false);
?>