<?php 
	require_once("farmasi/adapter/RiwayatPenyesuaianStokAdapter.php");

	$riwayat_penyesuaian_stok_form = new Form("form_riwayat_penyesuaian_stok", "", "");
	$tanggal_from_text = new Text("riwayat_penyesuaian_stok_tanggal_from", "riwayat_penyesuaian_stok_tanggal_from", date('Y-m-') . "01");
	$tanggal_from_text->setClass("mydate smis-one-option-input");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-m-d'");
	$riwayat_penyesuaian_stok_form->addElement("Tanggal Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("riwayat_penyesuaian_stok_tanggal_to", "riwayat_penyesuaian_stok_tanggal_to", date('Y-m-d'));
	$tanggal_to_text->setClass("mydate smis-one-option-input");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-m-d'");
	$riwayat_penyesuaian_stok_form->addElement("Tanggal Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setisButton(Button::$ICONIC);
	$show_button->setAction("riwayat_penyesuaian_stok.view()");
	$riwayat_penyesuaian_stok_form->addElement("", $show_button);
	
	$riwayat_penyesuaian_stok_table = new Table(
		array("Tanggal", "Nama Obat", "Jenis Obat", "Jenis Stok", "Produsen", "Vendor", "Jml. Awal", "Jml. Aktual", "Selisih", "Keterangan", "Petugas Entri"),
		"",
		null,
		true
	);
	$riwayat_penyesuaian_stok_table->setName("riwayat_penyesuaian_stok");
	$riwayat_penyesuaian_stok_table->setAddButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setReloadButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setPrintButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setAction(false);
	
	if (isset($_POST['command'])) {
		$riwayat_penyesuaian_stok_adapter = new RiwayatPenyesuaianStokAdapter();
		$riwayat_penyesuaian_stok_dbtable = new DBTable(
			$db,
			"smis_frm_penyesuaian_stok"
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_frm_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_stok_obat.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_penyesuaian_stok.nama_user LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_stok_obat.produsen LIKE '%" . $_POST['kriteria'] . "%' OR smis_frm_stok_obat.nama_vendor LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM  (
				SELECT smis_frm_penyesuaian_stok.*, smis_frm_stok_obat.nama_obat, smis_frm_stok_obat.nama_jenis_obat, smis_frm_stok_obat.label, smis_frm_stok_obat.produsen, smis_frm_stok_obat.nama_vendor, smis_frm_stok_obat.satuan
				FROM smis_frm_penyesuaian_stok LEFT JOIN smis_frm_stok_obat ON smis_frm_penyesuaian_stok.id_stok_obat = smis_frm_stok_obat.id
				WHERE smis_frm_penyesuaian_stok.prop NOT LIKE 'del' AND smis_frm_penyesuaian_stok.tanggal >= '" . $_POST['tanggal_from'] . "' AND smis_frm_penyesuaian_stok.tanggal <= '" . $_POST['tanggal_to'] . "' " . $filter . "
			) v_riwayat_penyesuaian_stok
 		";
		$query_count = "
			SELECT COUNT(*)
			FROM  (
				" . $query_value . "
			) v_riwayat_penyesuaian_stok
		";
		$riwayat_penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$riwayat_penyesuaian_stok_dbresponder = new DBResponder(
			$riwayat_penyesuaian_stok_dbtable,
			$riwayat_penyesuaian_stok_table,
			$riwayat_penyesuaian_stok_adapter
		);
		$data = $riwayat_penyesuaian_stok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $riwayat_penyesuaian_stok_form->getHtml();
	echo "<div id'table_content'>";
	echo $riwayat_penyesuaian_stok_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/riwayat_penyesuaian_stok_obat_action.js", false);
	echo addJS("farmasi/js/riwayat_penyesuaian_stok_obat.js", false);
?>