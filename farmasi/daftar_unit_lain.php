<?php
	global $db;
	$kebun_table = new Table(
		array("Kode", "Nama"),
		"Farmasi : Unit Lain",
		null,
		true
	);
	$kebun_table->setName("daftar_unit_lain");
	
	if (isset($_POST['command'])) {
		$kebun_adapter = new SimpleAdapter();
		$kebun_adapter->add("id", "id");
		$kebun_adapter->add("Kode", "kode");
		$kebun_adapter->add("Nama", "nama");
		$kebun_dbtable = new DBTable($db, "smis_frm_unit_lain");
		$kebun_dbresponder = new DBResponder(
			$kebun_dbtable,
			$kebun_table,
			$kebun_adapter
		);
		$data = $kebun_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$kebun_table->addModal("id", "hidden", "", "");
	$kebun_table->addModal("kode", "text", "Kode", "");
	$kebun_table->addModal("nama", "text", "Nama", "");
	$kebun_modal = $kebun_table->getModal();
	$kebun_modal->setTitle("Data Unit Lain");
	
	echo $kebun_modal->getHtml();
	echo $kebun_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("farmasi/js/daftar_unit_lain.js", false);
?>
