<?php
	class SetReturObatStatusServiceConsumer extends ServiceConsumer {
		public function __construct($db, $id, $status, $unit, $command) {
			$array = array();
			$array['id'] = $id;
			$array['status'] = $status;
			$array['command'] = $command;
			parent::__construct($db, "set_retur_obat_status", $array, $unit);
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>