<?php 
	global $db;

	$laporan_tabulator = new Tabulator("laporan", "", Tabulator::$LANDSCAPE);
	if (getSettings($db, "farmasi-laporan-show_lap_rekap_penjualan", 1) == 1)
		$laporan_tabulator->add("laporan_rekap_penjualan", "Lap. Rekap Penjualan", "farmasi/laporan_rekap_penjualan.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_detail_penjualan", 1) == 1)
		$laporan_tabulator->add("laporan_rincian_penjualan", "Lap. Rincian Penjualan", "farmasi/laporan_rincian_penjualan.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_rekap_pembelian", 1) == 1)
		$laporan_tabulator->add("laporan_rekap_pembelian_per_pbf", "Lap. Rekap Pembelian", "farmasi/laporan_rekap_pembelian_per_pbf.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_detail_pembelian", 1) == 1)
		$laporan_tabulator->add("laporan_rekap_pembelian", "Lap. Rincian Pembelian", "farmasi/laporan_rekap_pembelian.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_so", 1) == 1)
		$laporan_tabulator->add("laporan_persediaan_obat", "Lap. Stok Opname", "farmasi/laporan_persediaan_obat.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_mutasi_obat", 1) == 1)
		$laporan_tabulator->add("laporan_mutasi_obat", "Lap. Mutasi Obat", "farmasi/laporan_mutasi_obat.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_mutasi_obat_unit_lain", 1) == 1)
		$laporan_tabulator->add("laporan_mutasi_obat_unit_lain", "Lap. Mutasi Obat Ext.", "farmasi/laporan_mutasi_obat_unit_lain.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_obat_keluar", 0) == 1)
		$laporan_tabulator->add("laporan_obat_keluar", "Lap. Pengeluaran Obat", "farmasi/laporan_obat_keluar.php", Tabulator::$TYPE_INCLUDE);
	if (getSettings($db, "farmasi-laporan-show_lap_biaya_racik", 0) == 1)
		$laporan_tabulator->add("laporan_biaya_racik", "Lap. Biaya Racik", "farmasi/laporan_biaya_racik.php", Tabulator::$TYPE_INCLUDE);
	echo $laporan_tabulator->getHtml();
?>