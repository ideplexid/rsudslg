<?php
class PenjualanResepLocker extends Locker{
	public function proceed($variable){
		return $variable->save();
	}
	
	public function execute(){
		global $db;
		if (getSettings($db, "farmasi-locker-penjualan_resep", 1) == 1)
			return parent::execute();
		$result = null;
		try{
			$result=$this->succeed();
		}catch(Exception $e){
			$result=$this->proceedError($e->getMessage());
		}
		$this->releaseLock();
		return $result;
	}

	public function alreadyUsed(){
		$result=array();
		$result['success']=0;
		$result['message']="Terjadi Tabrakan Data Harap satu-satu saat memasukan Data, Silakan Ulangi Lagi";
		return $result;
	}
	
	public function succeed(){
		return $this->proceed($this->variable);
	}	
}
?>