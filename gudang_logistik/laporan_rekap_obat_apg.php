<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$laporan_form = new Form("", "", "Gudang Logistik : Laporan Rekap Obat APG");
	$tanggal_from_text = new Text("lrapg_tanggal_from", "lrapg_tanggal_from", "");
	$tanggal_from_text->setClass("mydatetime");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lrapg_tanggal_to", "lrapg_tanggal_to", "");
	$tanggal_to_text->setClass("mydatetime");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd hh:ii'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lrapg.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='lrapg_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "Unit", "Jml. A Form.", "Jml. A Non-Form.", "Jml. P Form.", "Jml. P Non-Form.", "Jml. G Form.", "Jml. G Non-Form.", "Jml. Resep", "Jml. Pasien"),
		"",
		null,
		true
	);
	$table->setName("lrapg");
	$table->setAction(false);
	$table->setFooterVisible(false);
	$table->setHeaderVisible(false);
	$table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Unit</small></center>
			</th>
			<th colspan='3' style='vertical-align: middle !important;'>
				<center><small>Alat Kesehatan (A)</small></center>
			</th>
			<th colspan='3' style='vertical-align: middle !important;'>
				<center><small>Paten (P)</small></center>
			</th>
			<th colspan='3' style='vertical-align: middle !important;'>
				<center><small>Generik (G)</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Jml. Resep</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Jml. Pasien</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Non-Formularium</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Formularium</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Non-Formularium</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Formularium</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Non-Formularium</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Formularium</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
		</tr>
	");

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_apg_and_entity") {
			$params = array();
			$consumer_service = new ServiceConsumer(
				$db,
				"get_apg_csv",
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$data = array(
				"alkes_formularium_csv"			=> $content[0]['alkes_formularium_csv'],
				"alkes_non_formularium_csv"		=> $content[0]['alkes_non_formularium_csv'],
				"paten_formularium_csv"			=> $content[0]['paten_formularium_csv'],
				"paten_non_formularium_csv"		=> $content[0]['paten_non_formularium_csv'],
				"generik_formularium_csv"		=> $content[0]['generik_formularium_csv'],
				"generik_non_formularium_csv"	=> $content[0]['generik_non_formularium_csv']
			);
			class APGUnitServiceConsumer extends ServiceConsumer {
				public function __construct($db) {
					parent::__construct ($db, "get_entity", "get_jumlah_apg");
				}
				public function proceedResult() {
					$content = array ();
					$result = json_decode ( $this->result, true );
					foreach ( $result as $autonomous ) {
						foreach ( $autonomous as $entity ) {
							$option = array ();
							$option ['value'] = $entity;
							$option ['name'] = ArrayAdapter::format("unslug", $entity ) ;
							$number = count ( $content );
							$content [$number] = $option;
						}
					}
					return $content;
				}
			}
			$apg_unit_service_consumer = new APGUnitServiceConsumer($db);
			$apg_unit_service_consumer->execute();
			$data['apg_unit'] = $apg_unit_service_consumer->getContent();
			$data['n_apg_unit'] = count($data['apg_unit']);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info") {
			$jumlah_alkes_formularium = 0;
			$jumlah_alkes_non_formularium = 0;
			$jumlah_paten_formularium = 0;
			$jumlah_paten_non_formularium = 0;
			$jumlah_generik_formularium = 0;
			$jumlah_generik_non_formularium = 0;
			$jumlah_resep = 0;
			$jumlah_pasien = 0;

			/// info apg:
			$params = array(
				"tanggal_from"					=> $_POST['tanggal_from'],
				"tanggal_to"					=> $_POST['tanggal_to'],
				"alkes_formularium_csv"			=> $_POST['alkes_formularium_csv'],
				"alkes_non_formularium_csv"		=> $_POST['alkes_non_formularium_csv'],
				"paten_formularium_csv"			=> $_POST['paten_formularium_csv'],
				"paten_non_formularium_csv"		=> $_POST['paten_non_formularium_csv'],
				"generik_formularium_csv"		=> $_POST['generik_formularium_csv'],
				"generik_non_formularium_csv"	=> $_POST['generik_non_formularium_csv']
			);
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jumlah_apg",
				$params,
				$_POST['slug_unit']
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_alkes_formularium += $content[0]['alkes_formularium'];
			$jumlah_alkes_non_formularium += $content[0]['alkes_non_formularium'];
			$jumlah_alkes = $jumlah_alkes_non_formularium + $jumlah_alkes_formularium;
			$jumlah_paten_formularium += $content[0]['paten_formularium'];
			$jumlah_paten_non_formularium += $content[0]['paten_non_formularium'];
			$jumlah_paten = $jumlah_paten_non_formularium + $jumlah_paten_formularium;
			$jumlah_generik_formularium += $content[0]['generik_formularium'];
			$jumlah_generik_non_formularium += $content[0]['generik_non_formularium'];
			$jumlah_generik = $jumlah_generik_non_formularium + $jumlah_generik_formularium;

			/// info jumlah resep:
			$params = array(
				"tanggal_from"	=> $_POST['tanggal_from'],
				"tanggal_to"	=> $_POST['tanggal_to']
			);
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jumlah_resep",
				$params,
				$_POST['slug_unit']
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_resep += $content[0]['jumlah'];

			/// info jumlah pasien:
			$params = array(
				"tanggal_from"	=> $_POST['tanggal_from'],
				"tanggal_to"	=> $_POST['tanggal_to']
			);
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jumlah_pasien",
				$params,
				$_POST['slug_unit']
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_pasien += $content[0]['jumlah'];

			$num = $_POST['num'];
			$html = "
				<tr id='data_" . $num . "'>
					<td id='data_" . $num . "_nomor'><small>" . ($num + 1) . "</small></td>
					<td id='data_" . $num . "_unit'><small>" . $_POST['nama_unit'] . "</small></td>
					<td id='data_" . $num . "_alkes_non_formularium' style='display: none;'>" . $jumlah_alkes_non_formularium . "</td>
					<td id='data_" . $num . "_f_alkes_non_formularium'><small>" . ArrayAdapter::format("number", $jumlah_alkes_non_formularium) . "</small></td>
					<td id='data_" . $num . "_alkes_formularium' style='display: none;'>" . $jumlah_alkes_formularium . "</td>
					<td id='data_" . $num . "_f_alkes_formularium'><small>" . ArrayAdapter::format("number", $jumlah_alkes_formularium) . "</small></td>
					<td id='data_" . $num . "_alkes' style='display: none;'>" . $jumlah_alkes . "</td>
					<td id='data_" . $num . "_f_alkes'><small>" . ArrayAdapter::format("number", $jumlah_alkes) . "</small></td>
					<td id='data_" . $num . "_paten_non_formularium' style='display: none;'>" . $jumlah_paten_non_formularium . "</td>
					<td id='data_" . $num . "_f_paten_non_formularium'><small>" . ArrayAdapter::format("number", $jumlah_paten_non_formularium) . "</small></td>
					<td id='data_" . $num . "_paten_formularium' style='display: none;'>" . $jumlah_paten_formularium . "</td>
					<td id='data_" . $num . "_f_paten_formularium'><small>" . ArrayAdapter::format("number", $jumlah_paten_formularium) . "</small></td>
					<td id='data_" . $num . "_paten' style='display: none;'>" . $jumlah_paten . "</td>
					<td id='data_" . $num . "_f_paten'><small>" . ArrayAdapter::format("number", $jumlah_paten) . "</small></td>
					<td id='data_" . $num . "_generik_non_formularium' style='display: none;'>" . $jumlah_generik_non_formularium . "</td>
					<td id='data_" . $num . "_f_generik_non_formularium'><small>" . ArrayAdapter::format("number", $jumlah_generik_non_formularium) . "</small></td>
					<td id='data_" . $num . "_generik_formularium' style='display: none;'>" . $jumlah_generik_formularium . "</td>
					<td id='data_" . $num . "_f_generik_formularium'><small>" . ArrayAdapter::format("number", $jumlah_generik_formularium) . "</small></td>
					<td id='data_" . $num . "_generik' style='display: none;'>" . $jumlah_generik . "</td>
					<td id='data_" . $num . "_f_generik'><small>" . ArrayAdapter::format("number", $jumlah_generik) . "</small></td>
					<td id='data_" . $num . "_resep' style='display: none;'>" . $jumlah_resep . "</td>
					<td id='data_" . $num . "_f_resep'><small>" . ArrayAdapter::format("number", $jumlah_resep) . "</small></td>
					<td id='data_" . $num . "_pasien' style='display: none;'>" . $jumlah_pasien . "</td>
					<td id='data_" . $num . "_f_pasien'><small>" . ArrayAdapter::format("number", $jumlah_pasien) . "</small></td>
				</tr>
			";

			$data = array(
				"nama_unit"	=> $_POST['nama_unit'],
				"html"		=> $html
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_logistik/templates/template_rekap_obat_apg.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP OBAT APG");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B2", ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y, H:i", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("F9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("I9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("L9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("M9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("N9")->getNumberFormat()->setFormatCode("#,##0");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->unit);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->alkes_non_formularium);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->alkes_formularium);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->alkes);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->paten_non_formularium);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->paten_formularium);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->paten);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->generik_non_formularium);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->generik_formularium);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->generik);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->resep);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->pasien);

				$objWorksheet->getStyle("D" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("E" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("L" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("M" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("N" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=REKAP_OBAT_APG_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}

	$loading_bar = new LoadingBar("lrapg_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lrapg.cancel()");
	$loading_modal = new Modal("lrapg_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo $table->getHtml();
	echo "<div id='lrapg_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datetimepicker.js");
	echo addCSS("framework/bootstrap/css/bootstrap-datetimepicker.css");
?>
<script type="text/javascript">
	function LRAPGAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRAPGAction.prototype.constructor = LRAPGAction;
	LRAPGAction.prototype = new TableAction();
	LRAPGAction.prototype.view = function() {
		if ($("#lrapg_tanggal_from").val() == "" || $("#lrapg_tanggal_to").val() == "")
			return;
		var self = this;
		$("#lrapg_list").empty();
		$("#lrapg_info").empty();
		$("#lrapg_loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#lrapg_loading_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_apg_and_entity";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#lrapg_list").empty();
				self.fillHtml(0, json);
			}
		);
	};
	LRAPGAction.prototype.fillHtml = function(num, json) {
		if (FINISHED || num == json.n_apg_unit) {
			if (FINISHED == false && num == json.n_apg_unit) {
				this.finalize();
			} else {
				$("#lrapg_loading_modal").smodal("hide");
				$("#lrapg_info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#lrapg_export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info";
		data['num'] = num;
		data['slug_unit'] = json.apg_unit[num]['value'];
		data['nama_unit'] = json.apg_unit[num]['name'];
		data['tanggal_from'] = $("#lrapg_tanggal_from").val();
		data['tanggal_to'] = $("#lrapg_tanggal_to").val();
		data['alkes_formularium_csv'] = json.alkes_formularium_csv;
		data['alkes_non_formularium_csv'] = json.alkes_non_formularium_csv;
		data['paten_formularium_csv'] = json.paten_formularium_csv;
		data['paten_non_formularium_csv'] = json.paten_non_formularium_csv;
		data['generik_formularium_csv'] = json.generik_formularium_csv;
		data['generik_non_formularium_csv'] = json.generik_non_formularium_csv;
		$.post(
			"",
			data,
			function(response) {
				var r_json = JSON.parse(response);
				if (r_json == null) return;
				$("tbody#lrapg_list").append(
					r_json.html
				);
				$("#lrapg_loading_bar").sload("true", r_json.nama_unit + " (" + (num + 1) + " / " + json.n_apg_unit + ")", (num + 1) * 100 / json.n_apg_unit - 1);
				self.fillHtml(num + 1, json);
			}
		);
	};
	LRAPGAction.prototype.finalize = function() {
		var num_rows = $("tbody#lrapg_list tr").length;
		var a_total = 0;
		var p_total = 0;
		var g_total = 0;
		var resep_total = 0;
		var pasien_total = 0;
		for (var i = 0; i < num_rows; i++) {
			var a = parseFloat($("#data_" + i + "_alkes").html());
			var p = parseFloat($("#data_" + i + "_paten").html());
			var g = parseFloat($("#data_" + i + "_generik").html());
			var resep = parseFloat($("#data_" + i + "_resep").html());
			var pasien = parseFloat($("#data_" + i + "_pasien").html());
			a_total += a;
			p_total += p;
			g_total += g;
			resep_total += resep;
			pasien_total += pasien;
		}
		$("tbody#lrapg_list").append(
			"<tr>" +
				"<td colspan='4'><small><strong><center>T O T A L</center></strong></small></td>" +
				"<td><small><strong><div align='right'>" + a_total.formatMoney("0", ".", ",") + "</div></strong></small></td>" +
				"<td colspan='2'></td>" +
				"<td><small><strong><div align='right'>" + p_total.formatMoney("0", ".", ",") + "</div></strong></small></td>" +
				"<td colspan='2'></td>" +
				"<td><small><strong><div align='right'>" + g_total.formatMoney("0", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div align='right'>" + resep_total.formatMoney("0", ".", ",") + "</div></strong></small></td>" +
				"<td><small><strong><div align='right'>" + pasien_total.formatMoney("0", ".", ",") + "</div></strong></small></td>" +
			"</tr>"
		);
		$("#lrapg_info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#lrapg_loading_modal").smodal("hide");
		$("#lrapg_export_button").removeAttr("onclick");
		$("#lrapg_export_button").attr("onclick", "lrapg.export_xls()");
	};
	LRAPGAction.prototype.cancel = function() {
		FINISHED = true;
	};
	LRAPGAction.prototype.export_xls = function() {
		showLoading();
		var num_rows = $("#lrapg_list").children("tr").length - 1;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var nomor = $("#data_" + i + "_nomor").text();
			var unit = $("#data_" + i + "_unit").text();
			var alkes_non_formularium = $("#data_" + i + "_alkes_non_formularium").text();
			var alkes_formularium = $("#data_" + i + "_alkes_formularium").text();
			var alkes = $("#data_" + i + "_alkes").text();
			var paten_non_formularium = $("#data_" + i + "_paten_non_formularium").text();
			var paten_formularium = $("#data_" + i + "_paten_formularium").text();
			var paten = $("#data_" + i + "_paten").text();
			var generik_non_formularium = $("#data_" + i + "_generik_non_formularium").text();
			var generik_formularium = $("#data_" + i + "_generik_formularium").text();
			var generik = $("#data_" + i + "_generik").text();
			var resep = $("#data_" + i + "_resep").text();
			var pasien = $("#data_" + i + "_pasien").text();
			d_data[i] = {
				"nomor" 					: nomor,
				"unit" 						: unit,
				"alkes_non_formularium"		: alkes_non_formularium,
				"alkes_formularium"			: alkes_formularium,
				"alkes"						: alkes,
				"paten_non_formularium"		: paten_non_formularium,
				"paten_formularium"			: paten_formularium,
				"paten"						: paten,
				"generik_non_formularium"	: generik_non_formularium,
				"generik_formularium"		: generik_formularium,
				"generik"					: generik,
				"resep"						: resep,
				"pasien"					: pasien
			};
		}
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tanggal_from'] = $("#lrapg_tanggal_from").val();
		data['tanggal_to'] = $("#lrapg_tanggal_to").val();
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		postForm(data);
		dismissLoading();
	};

	var lrapg;
	var FINISHED;
	$(document).ready(function() {
		$('.mydatetime').datetimepicker({ 
			minuteStep: 1
		});
		lrapg = new LRAPGAction(
			"lrapg",
			"gudang_logistik",
			"laporan_rekap_obat_apg",
			new Array()
		);
		$("#lrapg_loading_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#lrapg_loading_modal").on("hide", function() {
			$("a.close").show();
		});
		$("tbody#lrapg_list").append(
			"<tr>" +
				"<td colspan='13'><strong><center><small>DATA REKAP OBAT APG BELUM DIPROSES</small></center></strong></td>" +
			"</tr>"
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		})
	});
</script>