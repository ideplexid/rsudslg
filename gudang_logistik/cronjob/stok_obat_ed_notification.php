<?php 
	global $notification;
	global $db;
	
	$stok_obat_dbtable = new DBTable($db, "smis_lgs_stok_obat");
	$stok_obat_dbtable->addCustomKriteria(" DATEDIFF(tanggal_exp, CURDATE()) ", " <= 30 ");
	$stok_obat_dbtable->setOrder(" tanggal_exp ");
	$stok_obat_dbtable->setMaximum(3);
	$stok_obat_pack = $stok_obat_dbtable->view("", 0);
	$stok_obat_data = $stok_obat_pack['data'];
	$message = "";
	$i = 0;
	foreach($stok_obat_data as $stok) {
		$message .= $stok->nama_obat;
		if ($i > 0)
			$message .= ",";
		$message .= " ";
		$i++;
	}
	$message .= "mendekati ED dalam waktu <= 30 hari.";
	$type = "Stok Obat ED";
	$code = "gudang_logistik-obat-ed-" . date("Y-m-d") . $message;
	if ($stok_obat_data != null && count($stok_obat_data) > 0)
		$notification->addNotification($type, md5($code), $message, "gudang_logistik", "pengecekan_stok_ed");
?>