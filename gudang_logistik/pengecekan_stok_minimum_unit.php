<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-libs-class/ServiceProviderList.php");
	global $db;
	
	$pengecekan_stok_min_table = new Table(
		array("Nama Obat", "Jenis Obat", "Batas Min.", "Stok Aktual", "Satuan"),
		"",
		null,
		true
	);
	$pengecekan_stok_min_table->setName("pengecekan_stok_min");
	$pengecekan_stok_min_table->setAction(false);
	$pengecekan_stok_min_adapter = new SimpleAdapter();
	$pengecekan_stok_min_adapter->add("Nama Obat", "nama_barang");
	$pengecekan_stok_min_adapter->add("Jenis Obat", "nama_jenis_barang");
	$pengecekan_stok_min_adapter->add("Batas Min.", "jumlah_min");
	$pengecekan_stok_min_adapter->add("Stok Aktual", "sisa");
	$pengecekan_stok_min_adapter->add("Satuan", "satuan");
	$unit = isset($_POST['unit'])? $_POST['unit'] : "";
	$pengecekan_stok_min_responder = new ServiceResponder(
		$db,
		$pengecekan_stok_min_table,
		$pengecekan_stok_min_adapter,
		"get_minimum_stok_obat",
		$unit
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("pengecekan_stok_min", $pengecekan_stok_min_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$pengecekan_stok_min_form = new Form("pengecekan_stok_min_form", "", "Pengecekan Stok Minimum Obat Unit");
	$service_list = new ServiceProviderList($db, "get_minimum_stok_obat");
	$service_list->execute();
	$unit_select = new Select("pengecekan_stok_min_unit", "pengecekan_stok_min", $service_list->getContent());
	$pengecekan_stok_min_form->addElement("Unit", $unit_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("pengecekan_stok_min.view()");
	$pengecekan_stok_min_form->addElement("", $show_button);
	
	echo $pengecekan_stok_min_form->getHtml();
	echo "<div id='table_content'>";
	echo $pengecekan_stok_min_table->getHtml();
	echo "</div'>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PengecekanStokMinAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PengecekanStokMinAction.prototype.constructor = PengecekanStokMinAction;
	PengecekanStokMinAction.prototype = new TableAction();
	PengecekanStokMinAction.prototype.getViewData = function() {
		var view_data = this.getRegulerData();
		view_data['super_command'] = "pengecekan_stok_min";
		view_data['command'] = "list";
		view_data['kriteria'] = $("#"+this.prefix+"_kriteria").val();
		view_data['number'] = $("#"+this.prefix+"_number").val();
		view_data['max'] = $("#"+this.prefix+"_max").val();
		view_data['unit'] = $("#pengecekan_stok_min_unit").val();
		return view_data;
	};
	
	var pengecekan_stok_min;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		pengecekan_stok_min = new PengecekanStokMinAction(
			"pengecekan_stok_min",
			"gudang_logistik",
			"pengecekan_stok_minimum_unit",
			new Array()
		);
		pengecekan_stok_min.view();
	});
</script>