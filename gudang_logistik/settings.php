<?php
	require_once("smis-framework/smis/api/SettingsBuilder.php");
	global $db;
	
	$settings_builder = new SettingsBuilder($db, "gudang_logistik_settings", "gudang_logistik", "settings");
	$settings_builder->setShowDescription(true);
	$settings_builder->setTabulatorMode(Tabulator::$POTRAIT);
	
	$settings_builder->addTabs("penerimaan_obat", "Penerimaan Obat (BBM - OPL)");
	$settings_item = new SettingsItem($db, "no_bbm", "No. BBM ", 0, "text", "Nomor Bukti Barang Masuk (BBM)");
	$settings_builder->addItem("penerimaan_obat", $settings_item);

	$settings_builder->addTabs("obat_keluar", "Obat Keluar");
	$settings_item = new SettingsItem($db, "no_mutasi", "No. Mutasi ", 0, "text", "Nomor Mutasi");
	$settings_builder->addItem("obat_keluar", $settings_item);
	
	$response = $settings_builder->init();
?>