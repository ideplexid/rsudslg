<?php

global $db;

//Chooser
$barang_table = new Table(array("Barang","Jenis"));
$barang_table->setName("barang");
$barang_table->setModel(Table::$SELECT);
$barang_dbtable = new DBTable($db,"smis_lgs_stok_obat");
$barang_adapter = new SimpleAdapter();
$barang_adapter->add("Barang","nama_obat");
$barang_adapter->add("Jenis","nama_jenis_obat");
$barang_dbresponder=new DBResponder($barang_dbtable,$barang_table,$barang_adapter);
$super=new SuperCommand();
$super->addResponder("barang",$barang_dbresponder);
$hasil=$super->initialize();
if($hasil!=NULL){
	echo $hasil;
	return;
}

$header = array("No.","Nama","Jenis","Minimum","Satuan");
$uitable = new Table($header,"pengaturan stok minimum",NULL,true);
$uitable->setName("pengaturan_stok_minimum");
$uitable->setExcelButtonEnable(true);
if(isset($_POST['command']))
{
    $adapter = new SimpleAdapter(true,"No.");
	$adapter->add("Nama","nama_barang");
    $adapter->add("Jenis","nama_jenis_barang");
    $adapter->add("Minimum","jumlah_min");
    $adapter->add("Satuan","satuan");
    
    $dbtable = new DBTable($db,"smis_lgs_stok_barang_minimum_maksimum");
    
    $dbresponder = new DBResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->addModal("id","hidden","","");
$uitable->addModal("id_barang","hidden","","");
$uitable->addModal("nama_barang","chooser-pengaturan_stok_minimum-barang-Pilih Barang","Nama Barang","");
//$uitable->addModal("nama_barang","text","Nama","");
$uitable->addModal("nama_jenis_barang","text", "Jenis", "","y",null,true);
$uitable->addModal("medis","hidden","","");
$uitable->addModal("jumlah_min","text","Minimum", "","n","numeric");
$uitable->addModal("jumlah_max","hidden","","");
$uitable->addModal("satuan","text","Satuan", "", "n", "alpha");

$modal = $uitable->getModal();
$modal->setTitle("pengaturan_stok_minimum");
echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );

?>

<script type="text/javascript">
/*
var PENGATURAN_STOK_MINIMUM_ENTITY="<?php echo $this->page; ?>";
var pengaturan_stok_minimum;
var pengaturan_stok_minimum_barang;
var PENGATURAN_STOK_MINIMUM_PNAME="<?php echo $this->proto_name; ?>";
var PENGATURAN_STOK_MINIMUM_PSLUG="<?php echo $this->proto_slug; ?>";
var PENGATURAN_STOK_MINIMUM_PIMPL="<?php echo $this->proto_implement; ?>";

function PengaturanStokMinimumAction(name, page, action, column) 
{
    this.initialize(name, page, action, column);
}

PengaturanStokMinimumAction.prototype.constructor = PengaturanStokMinimumAction;
PengaturanStokMinimumAction.prototype = new TableAction();
PengaturanStokMinimumAction.prototype.show_add_form = function() 
{
    $("#pengaturan_stok_minimum_id").val("");
    $("#pengaturan_stok_minimum_id_barang").val("");
    $("#pengaturan_stok_minimum_nama_barang").val("");
    $("#pengaturan_stok_minimum_nama_jenis_barang").val("");
    $("#pengaturan_stok_minimum_medis").val("");
    $("#pengaturan_stok_minimum_jumlah_min").val("");
    //$("#pengaturan_stok_minimum_jumlah_max").val("");
    $("#pengaturan_stok_minimum_satuan").val("");
    $("#modal_alert_pengaturan_stok_minimum_add_form").html();
    $(".error_field").removeClass("error_field");
    $("#pengaturan_stok_minmaks_add_form").smodal("show");
};
PengaturanStokMinimumAction.prototype.validate = function() 
{
    var valid = true;
    var invalid_msg = "";
    var nama_barang = $("#pengaturan_stok_minimum_nama_barang").val();
    var jumlah_min = $("#pengaturan_stok_minimum_jumlah_min").val();
    var satuan = $("#pengaturan_stok_minimum_satuan").val();
    $(".error_field").removeClass("error_field");
    if (nama_barang == "") {
        valid = false;
        invalid_msg += "<br/><strong>Barang</strong> tidak boleh kosong";
        $("#pengaturan_stok_minimum_nama_barang").addClass("error_field");
    }
    if (jumlah_min == "") {
        valid = false;
        invalid_msg += "<br/><strong>Jumlah Min.</strong> tidak boleh kosong";
        $("#pengaturan_stok_minimum_jumlah_min").addClass("error_field");
    } else if (!is_numeric(jumlah_min)) {
        valid = false;
        invalid_msg += "<br/><strong>Jumlah Min.</strong> seharusnya numerik (0-9)";
        $("#pengaturan_stok_minimum_jumlah_min").addClass("error_field");
    }
    if (satuan == "") {
        valid = false;
        invalid_msg += "<br/><strong>Satuan</strong> tidak boleh kosong";
        $("#pengaturan_stok_minimum_satuan").addClass("error_field");
    }
    if (!valid) {
        $("#modal_alert_pengaturan_stok_minimum_add_form").html(
            "<div class='alert alert-block alert-danger'>" +
                "<h4>Peringatan</h4>" +
                invalid_msg +
            "</div>"
        );
    }
    return valid;
};
PengaturanStokMinimumAction.prototype.save = function() 
{
    if (!this.validate()) {
        return;
    }
    showLoading();
    $("#pengaturan_stok_minimum_add_form").smodal("hide");
    var self = this;
    var data = this.getRegulerData();
    data['command'] = "save";
    data['id'] = $("#pengaturan_stok_minimum_id").val();
    data['id_barang'] = $("#pengaturan_stok_minimum_id_barang").val();
    data['nama_barang'] = $("#pengaturan_stok_minimum_nama_barang").val();
    data['nama_jenis_barang'] = $("#pengaturan_stok_minimum_nama_jenis_barang").val();
    data['medis'] = $("#pengaturan_stok_minimum_medis").val();
    data['jumlah_min'] = $("#pengaturan_stok_minimum_jumlah_min").val();
    //data['jumlah_max'] = $("#pengaturan_stok_minimum_jumlah_max").val();
    data['satuan'] = $("#pengaturan_stok_minimum_satuan").val();
    $.post(
        "",
        data,
        function(response) {
            var json = getContent(response);
            if (json == null) {
                $("#pengaturan_stok_minimum_add_form").smodal("show");
            } else {
                self.view();
            }
            dismissLoading();
        }
    );
};
$(document).ready(function() 
{
    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        'placement': 'top'
    });
    var pengaturan_stok_minimum_columns = new Array("id", "id_barang", "nama_barang", "nama_jenis_barang", "medis", "jumlah_min", "satuan");
    pengaturan_stok_minimum = new PengaturanStokMinimumAction("pengaturan_stok_minimum",PENGATURAN_STOK_MINIMUM_ENTITY,"pengaturan_stok_minimum",pengaturan_stok_minimum_columns);
    pengaturan_stok_minimum.setPrototipe(PENGATURAN_STOK_MINIMUM_PNAME,PENGATURAN_STOK_MINIMUM_PSLUG,PENGATURAN_STOK_MINIMUM_PIMPL);
    pengaturan_stok_minimum.view();
});
*/

var pengaturan_stok_minimum;
var barang;
$(document).ready(function(){
    barang = new TableAction("barang","gudang_logistik","pengaturan_stok_minimum",new Array(""));
	barang.setSuperCommand("barang");
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement':'bottom'});
	var column = new Array('id','id_barang','nama_barang','nama_jenis_barang','medis','jumlah_min','jumlah_max','satuan');
	pengaturan_stok_minimum = new TableAction("pengaturan_stok_minimum","gudang_logistik","pengaturan_stok_minimum",column);
	pengaturan_stok_minimum.view();
    
    //CHOOSER AUTOCOMPLETE
	stypeahead("#pengaturan_stok_minimum_nama_barang",3,barang,"nama_obat",function(json){
		$("#pengaturan_stok_minimum_id_barang").val(json.id);
		$("#pengaturan_stok_minimum_nama_barang").val(json.nama_obat);
        $("#pengaturan_stok_minimum_nama_jenis_barang").val(json.nama_jenis_obat);
	});
	/*
	catat.setMultipleInput(true);
	catat.setNextEnter(true);
	catat.setEnableAutofocus();
	catat.setFocusOnMultiple("jumlah");
	catat.addNoClear("nama_linen");
	catat.addNoClear("id_linen");
	catat.setDisabledOnEdit(new Array("jumlah","tanggal"));
    */
	barang.selected=function(json){
		$("#pengaturan_stok_minimum_id_barang").val(json.id);
		$("#pengaturan_stok_minimum_nama_barang").val(json.nama_obat);
        $("#pengaturan_stok_minimum_nama_jenis_barang").val(json.nama_jenis_obat);
	};
    
});


</script>