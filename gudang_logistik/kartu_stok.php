<?php
	require_once("gudang_logistik/responder/ObatKSDBResponder.php");
	global $db;
	
	$form = new Form("ks_form", "", "Gudang Logistik : Kartu Gudang Induk");
	$id_obat_hidden = new Hidden("ks_id_obat", "ks_id_obat", "");
	$form->addElement("", $id_obat_hidden);
	$kode_obat_text = new Text("ks_kode_obat", "ks_kode_obat", "");
	$kode_obat_text->setClass("smis-one-option-input");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setAction("obat_ks.chooser('obat_ks', 'obat_ks_button', 'obat_ks', obat_ks)");
	$obat_button->setIcon("icon-white icon-list-alt");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setAtribute("id='obat_ks_browse'");
	$obat_input_group = new InputGroup("");
	$obat_input_group->addComponent($kode_obat_text);
	$obat_input_group->addComponent($obat_button);
	$form->addElement("Kode Obat", $obat_input_group);
	$obat_text = new Text("ks_nama_obat", "ks_nama_obat", "");
	$obat_text->setAtribute("disabled='disabled'");
	$form->addElement("Nama Obat", $obat_text);
	$nama_jenis_obat_text = new Text("ks_nama_jenis_obat", "ks_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$form->addElement("Jenis Obat", $nama_jenis_obat_text);
	$tanggal_from_text = new Text("ks_tanggal_from", "ks_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("ks_tanggal_to", "ks_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Tanggal Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-success");
	$show_button->setAction("kartu_stok.view()");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAtribute("id='show_button'");
	$download_button = new Button("", "", "AU-54");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='download_button'");
	$button_group = new ButtonGroup("");
	$button_group->addButton($show_button);
	$button_group->addButton($download_button);
	$form->addElement("", $button_group);
		
	$table = new Table(
		array("No.", "Tanggal", "No. Bon", "Dipakai untuk / Diterima dari", "Masuk", "Keluar", "Sisa"),
		"",
		null,
		true
	);
	$table->setName("kartu_stok");
	$table->setAction(false);
	$table->setFooterVisible(false);
	
	$obat_ks_table = new Table(
		array("Kode Obat", "Nama Obat", "Jenis Obat"),
		"",
		null, 
		true
	);
	$obat_ks_table->setName("obat_ks");
	$obat_ks_table->setModel(Table::$SELECT);
	$obat_ks_adapter = new SimpleAdapter();
	$obat_ks_adapter->add("Kode Obat", "kode_obat");
	$obat_ks_adapter->add("Nama Obat", "nama_obat");
	$obat_ks_adapter->add("Jenis Obat", "nama_jenis_obat");
	$obat_ks_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
	$obat_ks_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%') ";
	}
	$query_value = "
		SELECT DISTINCT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat
		FROM smis_lgs_dobat_f_masuk
		WHERE " . $filter . "
		ORDER BY kode_obat, nama_jenis_obat, nama_obat ASC
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat
			FROM smis_lgs_dobat_f_masuk
			WHERE " . $filter . "
			ORDER BY kode_obat, nama_jenis_obat, nama_obat ASC
		) v
	";
	$obat_ks_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_ks_dbresponder = new ObatKSDBResponder(
		$obat_ks_dbtable,
		$obat_ks_table,
		$obat_ks_adapter
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("obat_ks", $obat_ks_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah_sisa") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$id_obat = $_POST['id_obat'] = $_POST['id_obat'];
			$dbtable = new DBTable($db, "smis_lgs_kartu_stok");
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM smis_lgs_kartu_stok
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . date('Y-m-d') . "' AND id_obat = '" . $id_obat . "' AND prop NOT LIKE 'del'
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			$row = $dbtable->get_row("
				SELECT SUM(a.sisa) AS 'sisa'
				FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE b.id_obat = '" . $id_obat . "'
			");
			$sisa = 0;
			if ($row != null)
				$sisa = $row->sisa;
			$data = array(
				"jumlah"	=> $jumlah,
				"sisa"		=> $sisa
			);
			echo json_encode($data);
			return;
		} else if ($_POST['command'] == "get_info") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$id_obat = $_POST['id_obat'] = $_POST['id_obat'];
			$num = $_POST['num'];
			$sisa = $_POST['sisa'];
			$dbtable = new DBTable($db, "smis_lgs_kartu_stok");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_lgs_kartu_stok
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . date('Y-m-d') . "' AND id_obat = '" . $id_obat . "' AND prop NOT LIKE 'del'
				ORDER BY tanggal DESC, id DESC
				LIMIT " . $num . ", 1
			");
			$html = "";
			$nama_obat = "";
			$tanggal = "";
			$no_bon = "";
			if ($row != null) {
				if (strtotime($row->tanggal) >= strtotime($tanggal_from) && strtotime($row->tanggal) <= strtotime($tanggal_to)) {
					$html = "
						<tr>
							<td id='nomor'></td>
							<td id='tanggal'>" . ArrayAdapter::format("date d-m-Y", $row->tanggal) . "</td>
							<td id='no_bon'>" . $row->no_bon . "</td>
							<td id='unit'>" . $row->unit . "</td>
							<td id='f_masuk'>" . ArrayAdapter::format("number", $row->masuk) . "</td>
							<td id='masuk' style='display: none;'>" . $row->masuk . "</td>
							<td id='f_keluar'>" . ArrayAdapter::format("number", $row->keluar) . "</td>
							<td id='keluar' style='display: none;'>" . $row->keluar . "</td>
							<td id='f_sisa'>" . ArrayAdapter::format("number", $sisa) . "</td>
							<td id='sisa' style='display: none;'>" . $sisa . "</td>
						</tr>
					";
				}
				$masuk = $row->masuk;
				$keluar = $row->keluar;
				$no_bon = $row->no_bon;
				$tanggal = $row->tanggal;
				$nama_obat = $row->nama_obat;
			}
			$data = array(
				"html" 		=> $html,
				"id_obat"	=> ArrayAdapter::format("only-digit6", $id_obat),
				"nama_obat"	=> $nama_obat,
				"tanggal"	=> ArrayAdapter::format("date d-m-Y", $tanggal),
				"no_bon"	=> $no_bon,
				"sisa"		=> $sisa - $masuk + $keluar
			);
			echo json_encode($data);
			return;
		} else if ($_POST['command'] == "export_excel") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_logistik/templates/template_au-54.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("AU-54.K");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			
			$id_obat = $_POST['id_obat'];
			$kode_obat = $_POST['kode_obat'];
			if (substr($kode_obat, 0, 3) == "REG")
				$kode_obat = "309" . substr($kode_obat, 3);
			else if (substr($kode_obat, 0, 3) == "JKN")
				$kode_obat = "319" . substr($kode_obat, 3);
			$nama_obat = $_POST['nama_obat'];
			$nama_jenis_obat = $_POST['nama_jenis_obat'];
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$objWorksheet->setCellValue("E5", ": " . $kode_obat);
			$objWorksheet->setCellValue("H3", ": " . $kode_obat);
			$objWorksheet->setCellValue("H4", ": " . $nama_obat);
			$objWorksheet->setCellValue("H5", ": 0");
			
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(10, $_POST['num_rows'] - 2);
			$start_row_num = 10;
			$end_row_num = 10;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->tanggal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->unit);
				$col_num+=2;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->masuk);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->keluar);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->sisa);
				$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
			}
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=AU_54-" . $kode_obat . "_" . ArrayAdapter::format("only-digit6", $id_obat) . "_" . date("Ymd_his") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");			
			return;
		}
		return;
	}
	
	$loading_bar = new LoadingBar("loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lpo.cancel()");
	$loading_modal = new Modal("loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("gudang_logistik/js/obat_ks_action.js", false);
	echo addJS("gudang_logistik/js/kartu_stok_action.js", false);
	echo addJS("gudang_logistik/js/kartu_stok.js", false);
?>