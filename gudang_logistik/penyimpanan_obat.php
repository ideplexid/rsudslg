<?php 
	$penyimpanan_obat_tabulator = new Tabulator("penyimpanan_obat", "", Tabulator::$POTRAIT);
	$penyimpanan_obat_tabulator->add("stok_obat_t", "Detail Stok Obat", "gudang_logistik/daftar_stok_obat_tersimpan.php", Tabulator::$TYPE_INCLUDE);
	$penyimpanan_obat_tabulator->add("stok_obat_b", "Detail Stok Obat Dapat Dikonversi", "gudang_logistik/daftar_stok_obat_dapat_dibongkar.php", Tabulator::$TYPE_INCLUDE);
	
	echo $penyimpanan_obat_tabulator->getHtml();
?>