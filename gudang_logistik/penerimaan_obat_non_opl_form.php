<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("gudang_logistik/adapter/ObatPRAdapter.php");
	require_once("gudang_logistik/responder/ObatMasukDBResponder.php");
	require_once("gudang_logistik/responder/StokDBResponder.php");
	global $db;

	$id = isset($_POST['id']) ? $_POST['id'] : "";
	$id_po = isset($_POST['id_po']) ? $_POST['id_po'] : "0";
	$no_opl = isset($_POST['no_opl']) ? $_POST['no_opl'] : "";
	$no_bbm = isset($_POST['no_bbm']) ? $_POST['no_bbm'] : "BBM-" . ArrayAdapter::format("only-digit8", getSettings($db, "no_bbm", 0) + 1);
	$tipe = isset($_POST['tipe']) ? $_POST['tipe'] : "";
	$id_vendor = isset($_POST['id_vendor']) ? $_POST['id_vendor'] : "";
	$nama_vendor = isset($_POST['nama_vendor']) ? $_POST['nama_vendor'] : "";
	$no_faktur = isset($_POST['no_faktur']) ? $_POST['no_faktur'] : "";
	$tanggal = isset($_POST['tanggal']) ? $_POST['tanggal'] : date("Y-m-d");
	$tanggal_datang = isset($_POST['tanggal_datang']) ? $_POST['tanggal_datang'] : date("Y-m-d");
	$tanggal_tempo = isset($_POST['tanggal_tempo']) ? $_POST['tanggal_tempo'] : date('Y-m-d', strtotime("+30 days"));
	$diskon = isset($_POST['diskon']) ? $_POST['diskon'] : "0,00";
	$t_diskon = isset($_POST['t_diskon']) ? $_POST['t_diskon'] : "persen";
	$materai = isset($_POST['materai']) ? $_POST['materai'] : "0,00";
	$keterangan = isset($_POST['keterangan']) ? $_POST['keterangan'] : "";
	$use_ppn = isset($_POST['use_ppn']) ? $_POST['use_ppn'] : "";
	$editable_header = isset($_POST['editable_header']) ? $_POST['editable_header'] : "false";
	$editable_detail = isset($_POST['editable_detail']) ? $_POST['editable_detail'] : "false";
	$limited = isset($_POST['limited']) ? $_POST['limited'] : "false";

	$header_form = new Form("", "", "Gudang Logistik : Penerimaan Obat (BBM - Non-OPL)");
	$id_hidden = new Hidden("penerimaan_obat_id", "penerimaan_obat_id", $id);
	$header_form->addElement("", $id_hidden);
	$limited_hidden = new Hidden("penerimaan_obat_limited", "penerimaan_obat_limited", $limited);
	$header_form->addElement("", $limited_hidden);
	$id_po_hidden = new Hidden("penerimaan_obat_id_po", "penerimaan_obat_id_po", $id_po);
	$header_form->addElement("", $id_po_hidden);
	$no_opl_text = new Text("penerimaan_obat_no_opl", "penerimaan_obat_no_opl", $no_opl);
	if ($editable_header == "false")
		$no_opl_text->setAtribute("disabled='disabled'");
	$header_form->addElement("No. OPL", $no_opl_text);
	$no_bbm_text = new Text("penerimaan_obat_no_bbm", "penerimaan_obat_no_bbm", $no_bbm);
	$no_bbm_text->setAtribute("disabled='disabled'");
	$header_form->addElement("No. BBM", $no_bbm_text);
	$no_faktur_text = new Text("penerimaan_obat_no_faktur", "penerimaan_obat_no_faktur", $no_faktur);
	if ($editable_header == "false")
		$no_faktur_text->setAtribute("disabled='disabled'");
	$header_form->addElement("No. Faktur", $no_faktur_text);
	$id_vendor_hidden = new Hidden("penerimaan_obat_id_vendor", "penerimaan_obat_id_vendor", $id_vendor);
	$header_form->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("penerimaan_obat_nama_vendor", "penerimaan_obat_nama_vendor", $nama_vendor);
	$nama_vendor_text->setAtribute("disabled='disabled'");
	if ($editable_header == "true" && ($id_po == 0 || $id_po == "")) {
		$nama_vendor_text->setClass("smis-one-option-input");
		$vendor_browse_button = new Button("", "", "Pilih");
		$vendor_browse_button->setClass("btn-info");
		$vendor_browse_button->setIsButton(Button::$ICONIC);
		$vendor_browse_button->setIcon("fa fa-list");
		$vendor_browse_button->setAction("vendor.chooser('vendor', 'vendor', 'vendor', vendor)");
		$vendor_browse_button->setAtribute("id='vendor_browse'");
		$vendor_input_group = new InputGroup("");
		$vendor_input_group->addComponent($nama_vendor_text);
		$vendor_input_group->addComponent($vendor_browse_button);
		$header_form->addElement("Rekanan", $vendor_input_group);
	} else
		$header_form->addElement("Rekanan", $nama_vendor_text);
	$tanggal_text = new Text("penerimaan_obat_tanggal", "penerimaan_obat_tanggal", $tanggal);
	if ($editable_header == "true") {
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$tanggal_text->setClass("mydate");
	} else {
		$tanggal_text->setAtribute("disabled='disabled'");
	}
	$header_form->addElement("Tgl. Faktur", $tanggal_text);
	$tanggal_datang_text = new Text("penerimaan_obat_tanggal_datang", "penerimaan_obat_tanggal_datang", $tanggal_datang);
	if ($editable_header == "true") {
		$tanggal_datang_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$tanggal_datang_text->setClass("mydate");
	} else{
		$tanggal_datang_text->setAtribute("disabled='disabled'");
	}
	$header_form->addElement("Tgl. Diterima", $tanggal_datang_text);
	$tanggal_tempo_text = new Text("penerimaan_obat_tanggal_tempo", "penerimaan_obat_tanggal_tempo", $tanggal_tempo);
	if ($editable_header == "true") {
		$tanggal_tempo_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$tanggal_tempo_text->setClass("mydate");
	} else {
		$tanggal_tempo_text->setAtribute("disabled='disabled'");
	}
	$header_form->addElement("Jatuh Tempo", $tanggal_tempo_text);
	$diskon_text = new Text("penerimaan_obat_diskon", "penerimaan_obat_diskon", $diskon);
	if ($editable_header == "true") {
		$diskon_text->setTypical("money");
		$diskon_text->setAtribute("data-thousands='.' data-decimal=',' data-precision='2'");
	} else {
		$diskon_text->setAtribute("disabled='disabled'");
	}
	$header_form->addElement("Diskon", $diskon_text);
	$t_diskon_option = new OptionBuilder();
	if ($t_diskon != "") {
		$t_diskon_option->add("Persen (%)", "persen", "1");
		$t_diskon_option->add("Nominal (Rp.)", "nominal");
	} else {
		$t_diskon_option->add("Persen (%)", "persen", $t_diskon == "persen" ? "1" : "0");
		$t_diskon_option->add("Nominal (Rp.)", "nominal", $t_diskon == "nominal" ? "1" : "0");
	}
	$t_diskon_select = new Select("penerimaan_obat_t_diskon", "penerimaan_obat_t_diskon", $t_diskon_option->getContent());
	if ($editable_header == "false")
		$t_diskon_select->setAtribute("disabled='disabled'");
	$header_form->addElement("T. Diskon", $t_diskon_select);
	$materai_text = new Text("penerimaan_obat_materai", "penerimaan_obat_materai", $materai);
	if ($editable_header == "true") {
		$materai_text->setTypical("money");
		$materai_text->setAtribute("data-thousands='.' data-decimal=',' data-precision='2'");
	} else {
		$materai_text->setAtribute("disabled='disabled'");
	}
	$header_form->addElement("Materai", $materai_text);
	$keterangan_hidden = new Hidden("penerimaan_obat_keterangan", "penerimaan_obat_keterangan", $keterangan);
	$header_form->addElement("", $keterangan_hidden);
	$tipe_option = new OptionBuilder();
	if ($tipe == "") {
		$tipe_option->add("Reguler", "logistik", "1");
		$tipe_option->add("Cito", "sito");
		$tipe_option->add("Konsinyasi", "konsinyasi");
		$tipe_option->add("E-Katalog", "e_katalog");
		$tipe_option->add("Non E-Katalog", "non_e_katalog");
	} else {
		if ($tipe == "logistik")
			$tipe_option->add("Reguler", "logistik", "1");
		else
			$tipe_option->add("Reguler", "logistik");
		if ($tipe == "sito")
			$tipe_option->add("Cito", "sito", "1");
		else
			$tipe_option->add("Cito", "sito");
		if ($tipe == "konsinyasi")
			$tipe_option->add("Konsinyasi", "konsinyasi", "1");
		else
			$tipe_option->add("Konsinyasi", "konsinyasi");
		if ($tipe == "e_katalog")
			$tipe_option->add("E-Katalog", "e_katalog", "1");
		else
			$tipe_option->add("E-Katalog", "e_katalog");
		if ($tipe == "non_e_katalog")
			$tipe_option->add("Non E-Katalog", "non_e_katalog", "1");
		else
			$tipe_option->add("Non E-Katalog", "non_e_katalog");
	}
	$tipe_select = new Select("penerimaan_obat_tipe", "penerimaan_obat_tipe", $tipe_option->getContent());
	if ($editable_header == "false")
		$tipe_select->setAtribute("disabled='disabled'");
	$header_form->addElement("Tipe", $tipe_select);
	$use_ppn_option = new OptionBuilder();
	if ($use_ppn == "") {
		$use_ppn_option->add("Ya", 1, 1);
		$use_ppn_option->add("Tidak", 0);
	} else {
		if ($use_ppn == 1) {
			$use_ppn_option->add("Ya", 1, 1);
			$use_ppn_option->add("Tidak", 0);
		} else {
			$use_ppn_option->add("Ya", 1);
			$use_ppn_option->add("Tidak", 0, 1);
		}
	}
	$use_ppn_select = new Select("penerimaan_obat_use_ppn", "penerimaan_obat_use_ppn", $use_ppn_option->getContent());
	if ($editable_detail == "false")
		$use_ppn_select->setAtribute("disabled='disabled'");
	$header_form->addElement("Inc. PPn", $use_ppn_select);

	$detail_form = new Form("", "", "");
	$row_num_hidden = new Hidden("dpenerimaan_obat_row_num", "dpenerimaan_obat_row_num", "");
	$detail_form->addElement("", $row_num_hidden);
	$id_obat_hidden = new Hidden("dpenerimaan_obat_id_obat", "dpenerimaan_obat_id_obat", "");
	$detail_form->addElement("", $id_obat_hidden);
	$name_obat_hidden = new Hidden("dpenerimaan_obat_name_obat", "dpenerimaan_obat_name_obat", "");
	$detail_form->addElement("", $name_obat_hidden);
	$medis_hidden = new Hidden("dpenerimaan_obat_medis", "dpenerimaan_obat_medis", "");
	$detail_form->addElement("", $medis_hidden);	
	$inventaris_hidden = new Hidden("dpenerimaan_obat_inventaris", "dpenerimaan_obat_inventaris", "");
	$detail_form->addElement("", $inventaris_hidden);
	$formularium_hidden = new Hidden("dpenerimaan_obat_formularium", "dpenerimaan_obat_formularium", "");
	$detail_form->addElement("", $formularium_hidden);
	$generik_hidden = new Hidden("dpenerimaan_obat_generik", "dpenerimaan_obat_generik", "");
	$detail_form->addElement("", $generik_hidden);
	$berlogo_hidden = new Hidden("dpenerimaan_obat_berlogo", "dpenerimaan_obat_berlogo", "");
	$detail_form->addElement("", $berlogo_hidden);
	$label_hidden = new Hidden("dpenerimaan_obat_label", "dpenerimaan_obat_label", "");
	$detail_form->addElement("", $label_hidden);
	$stok_entri_hidden = new Hidden("dpenerimaan_obat_stok_entri", "dpenerimaan_obat_stok_entri", "");
	$detail_form->addElement("", $stok_entri_hidden);
	$sisa_hidden = new Hidden("dpenerimaan_obat_sisa", "dpenerimaan_obat_sisa", "");
	$detail_form->addElement("", $sisa_hidden);
	$nama_obat_text = new Text("dpenerimaan_obat_nama_obat", "dpenerimaan_obat_nama_obat", "");
	if ($limited == "true") {
		$nama_obat_text->setAtribute("disabled='disabled'");
		$detail_form->addElement("Nama Obat", $nama_obat_text);
	} else {
		$nama_obat_text->setClass("smis-one-option-input");
		$obat_browse_button = new Button("", "", "BBM - Non-OPL");
		$obat_browse_button->setClass("btn-info");
		$obat_browse_button->setIsButton(Button::$ICONIC);
		$obat_browse_button->setIcon("fa fa-list");
		$obat_browse_button->setAction("obat.chooser('obat', 'obat', 'obat', obat)");
		$obat_browse_button->setAtribute("id='obat_browse'");
		$obat_input_group = new InputGroup("");
		$obat_input_group->addComponent($nama_obat_text);
		$obat_input_group->addComponent($obat_browse_button);
		$detail_form->addElement("Nama Obat", $obat_input_group);
	}
	$kode_obat_text = new Text("dpenerimaan_obat_kode_obat", "dpenerimaan_obat_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Kode Obat", $kode_obat_text);
	$nama_jenis_obat_text = new Text("dpenerimaan_obat_nama_jenis_obat", "dpenerimaan_obat_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Jenis Obat", $nama_jenis_obat_text);
	$jumlah_tercatat_text = new Text("dpenerimaan_obat_jumlah_tercatat", "dpenerimaan_obat_jumlah_tercatat", "");
	if ($limited == "true")
		$jumlah_tercatat_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Jml. Tercatat", $jumlah_tercatat_text);
	$jumlah_text = new Text("dpenerimaan_obat_jumlah", "dpenerimaan_obat_jumlah", "");
	if ($limited == "true")
		$jumlah_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Jml. Diterima", $jumlah_text);
	$satuan_text = new Text("dpenerimaan_obat_satuan", "dpenerimaan_obat_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Satuan", $satuan_text);
	$konversi_text = new Text("dpenerimaan_obat_konversi", "dpenerimaan_obat_konversi", "");
	$detail_form->addElement("Konversi", $konversi_text);
	$satuan_konversi_text = new Text("dpenerimaan_obat_satuan_konversi", "dpenerimaan_obat_satuan_konversi", "");
	$satuan_konversi_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Sat. Konversi", $satuan_konversi_text);
	$hpp_text = new Text("dpenerimaan_obat_hpp", "dpenerimaan_obat_hpp", "");
	$hpp_text->setTypical("money");
	if ($limited == "true")
		$hpp_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2' disabled='disabled'");
	else
		$hpp_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$detail_form->addElement("HNA", $hpp_text);
	$hna_text = new Text("dpenerimaan_obat_hna", "dpenerimaan_obat_hna", "");
	$hna_text->setTypical("money");
	if ($limited == "true")
		$hna_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2' disabled='disabled'");
	else
		$hna_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$detail_form->addElement("HPP", $hna_text);
	$diskon_text = new Text("dpenerimaan_obat_diskon", "dpenerimaan_obat_diskon", "0,00");
	$diskon_text->setTypical("money");
	$diskon_text->setAtribute("data-thousands='.' data-decimal=',' data-precision='2'");
	$detail_form->addElement("Diskon", $diskon_text);
	$t_diskon_select = new Select("dpenerimaan_obat_t_diskon", "dpenerimaan_obat_t_diskon", $t_diskon_option->getContent());
	$detail_form->addElement("T. Diskon", $t_diskon_select);
	$produsen_text = new Text("dpenerimaan_obat_produsen", "dpenerimaan_obat_produsen", "");
	if ($limited == "true")
		$produsen_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Produsen", $produsen_text);
	$tanggal_exp_text = new Text("dpenerimaan_obat_tanggal_exp", "dpenerimaan_obat_tanggal_exp", "");
	if ($limited == "true")
		$tanggal_exp_text->setAtribute("data-date-format='yyyy-mm-dd' disabled='disabled'");
	else
		$tanggal_exp_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$tanggal_exp_text->setClass("mydate");
	$detail_form->addElement("Tgl. Exp.", $tanggal_exp_text);
	$no_batch_text = new Text("dpenerimaan_obat_no_batch", "dpenerimaan_obat_no_batch", "");
	if ($limited == "true")
		$no_batch_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("No. Batch", $no_batch_text);

	$save_button = new Button("", "", "Tambahkan");
	$save_button->setClass("btn-info");
	$save_button->setIsButton(Button::$ICONIC_TEXT);
	$save_button->setIcon("fa fa-chevron-right");
	$save_button->setAction("dpenerimaan_obat.save()");
	$save_button->setAtribute("id='penerimaan_obat_form_save'");

	$update_button = new Button("", "", "Perbarui");
	$update_button->setClass("btn-info");
	$update_button->setIsButton(Button::$ICONIC_TEXT);
	$update_button->setIcon("fa fa-chevron-right");
	$update_button->setAction("dpenerimaan_obat.save()");
	$update_button->setAtribute("id='penerimaan_obat_form_update'");

	$cancel_button = new Button("", "", "Batal");
	$cancel_button->setClass("btn-danger");
	$cancel_button->setIsButton(Button::$ICONIC_TEXT);
	$cancel_button->setIcon("icon-white icon-remove");
	$cancel_button->setAction("dpenerimaan_obat.cancel()");
	$cancel_button->setAtribute("id='penerimaan_obat_form_cancel'");	

	$button_group = new ButtonGroup("");
	$button_group->addButton($update_button);
	$button_group->addButton($cancel_button);

	if ($limited == "false")
		$detail_form->addElement("", $save_button);
	$detail_form->addElement("", $button_group);

	$table = new Table(
		array("No.", "Kode", "Obat", "Jenis", "Stok<sup>*</sup>", "Tercatat", "Diterima", "Satuan", "HNA", "Diskon", "Subtotal", "Ubah", "Hapus"),
		"",
		null,
		true
	);
	$table->setName("dpenerimaan_obat");
	$table->setFooterVisible(false);
	$table->setAction(false);
	
	$button_group = new ButtonGroup("");
	$back_button = new Button("", "", "Kembali");
	$back_button->setClass("btn-inverse");
	$back_button->setIsButton(Button::$ICONIC_TEXT);
	$back_button->setIcon("fa fa-chevron-left");
	$back_button->setAction("penerimaan_obat.back()");
	$back_button->setAtribute("id='penerimaan_obat_back'");
	$button_group->addButton($back_button);
	if ($editable_header == "true") {
		if ($limited == "false") {
			$save_button = new Button("", "", "Simpan Draft");
			$save_button->setClass("btn-success");
			$save_button->setIsButton(Button::$ICONIC_TEXT);
			$save_button->setIcon("fa fa-floppy-o");
			$save_button->setAction("penerimaan_obat.save()");
			$save_button->setAtribute("id='penerimaan_obat_save'");
			$button_group->addButton($save_button);
		} else {
			$save_button = new Button("", "", "Update Info. BBM");
			$save_button->setClass("btn-success");
			$save_button->setIsButton(Button::$ICONIC_TEXT);
			$save_button->setIcon("fa fa-floppy-o");
			$save_button->setAction("penerimaan_obat.update_info_bbm()");
			$save_button->setAtribute("id='penerimaan_obat_update'");
			$button_group->addButton($save_button);
		}
	}

	//get_daftar_vendor_consumer:
	$vendor_table = new Table(
		array("Nama", "NPWP", "Alamat", "No. Telp."),
		"",
		null,
		true
	);
	$vendor_table->setName("vendor");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter();
	$vendor_adapter->add("Nama", "nama");
	$vendor_adapter->add("NPWP", "npwp");
	$vendor_adapter->add("Alamat", "alamat");
	$vendor_adapter->add("No. Telp.", "telpon");
	$vendor_service_responder = new ServiceResponder(
		$db,
		$vendor_table,
		$vendor_adapter,
		"get_daftar_vendor"
	);
	
	//get_daftar_barang_reguler_consumer:
	$obat_table = new Table(
		array("ID", "Obat", "Jenis", "Satuan", "Konversi", "Sat. Konversi"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("ID", "id");
	$obat_adapter->add("Obat", "nama");
	$obat_adapter->add("Jenis", "nama_jenis_barang");
	$obat_adapter->add("Satuan", "satuan");
	$obat_adapter->add("Konversi", "konversi", "number");
	$obat_adapter->add("Sat. Konversi", "satuan_konversi");
	$obat_service_responder = new ServiceResponder(
		$db,
		$obat_table,
		$obat_adapter,
		"get_daftar_barang_m"
	);	
	
	//get stok:
	$stok_table = new Table(array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi"));
	$stok_table->setName("sisa");
	$stok_adapter = new SimpleAdapter();
	$stok_adapter->add("id_obat", "id_obat");
	$stok_adapter->add("sisa", "sisa");
	$stok_adapter->add("satuan", "satuan");
	$stok_adapter->add("konversi", "konversi");
	$stok_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
	$stok_dbtable = new DBTable(
		$db,
		"smis_lgs_stok_obat",
		$columns
	);
	$stok_dbresponder = new StokDBResponder(
		$stok_dbtable,
		$stok_table,
		$stok_adapter
	);
	
	//get satuan typeahead items:
	$satuan_table = new Table(
		array("Satuan"),
		"",
		null,
		true
	);
	$satuan_table->setName("satuan");
	$satuan_adapter = new SimpleAdapter();
	$satuan_adapter->add("Satuan", "satuan");
	$satuan_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND satuan LIKE '%" . $_POST['kriteria'] . "%' ";
	}
	$query_value = "
		SELECT satuan AS 'id', satuan
		FROM (
			SELECT DISTINCT satuan
			FROM smis_lgs_dobat_f_masuk
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_satuan
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT satuan
			FROM smis_lgs_dobat_f_masuk
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_satuan
	";
	$satuan_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$satuan_dbresponder = new DBResponder(
		$satuan_dbtable,
		$satuan_table,
		$satuan_adapter
	);
	
	//get produsen typeahead items:
	$produsen_table = new Table(
		array("Produsen"),
		"",
		null,
		true
	);
	$produsen_table->setName("produsen");
	$produsen_adapter = new SimpleAdapter();
	$produsen_adapter->add("Produsen", "produsen");
	$produsen_dbtable = new DBTable($db, "smis_lgs_stok_obat");
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND produsen LIKE '%" . $_POST['kriteria'] . "%' ";
	}
	$query_value = "
		SELECT produsen AS 'id', produsen
		FROM (
			SELECT DISTINCT produsen
			FROM smis_lgs_stok_obat
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_produsen
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT produsen
			FROM smis_lgs_stok_obat
			WHERE prop NOT LIKE 'del' " . $filter . "
		) v_produsen
	";
	$produsen_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$produsen_dbresponder = new DBResponder(
		$produsen_dbtable,
		$produsen_table,
		$produsen_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_service_responder);
	$super_command->addResponder("vendor", $vendor_service_responder);
	$super_command->addResponder("stok", $stok_dbresponder);
	$super_command->addResponder("satuan", $satuan_dbresponder);
	$super_command->addResponder("produsen", $produsen_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "show_detail") {
			$id = $_POST['id'];
			$limited = isset($_POST['limited']) ? $_POST['limited'] : "false";
			$dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
			$header_row = $dbtable->select($id);
			$detail_rows = $dbtable->get_result("
				SELECT a.*, b.produsen, b.tanggal_exp, b.no_batch
				FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_stok_obat b ON a.id = b.id_dobat_masuk
				WHERE a.id_obat_f_masuk = '" . $id . "' AND a.prop NOT LIKE 'del'
			");
			$locked = isset($_POST['locked']) ? $_POST['locked'] : false;
			$html = "";
			$num = 0;
			if ($detail_rows != null) {
				foreach ($detail_rows as $row) {
					$html_edit_action = "";
					if ($limited == "true")
						$html_edit_action = "<a href='#' onclick='dpenerimaan_obat.edit(" . $num . ")' class='input btn btn-warning'><i class='fa fa-pencil'></i></a>";
					$html_del_action = "";
					$subtotal = $row->hna * $row->jumlah;
					$hpp = $row->hna;
					if ($header_row->use_ppn == 1) {
						$subtotal = $row->hna / 1.1 * $row->jumlah;
						$hpp = $row->hna / 1.1;
					}
					$f_diskon = $row->diskon . " %";
					if ($row->t_diskon == "nominal") {
						$f_diskon = ArrayAdapter::format("only-money", $row->diskon);
						$subtotal = $subtotal - $row->diskon;
					} else {
						$subtotal = $subtotal - ($row->diskon * $subtotal) / 100;
					}
					$subtotal = round($subtotal);
					$f_subtotal = ArrayAdapter::format("money", $subtotal);
					$html .= "
						<tr id='data_" . $num . "'>
							<td id='nomor'></td>
							<td id='id' style='display: none;'>" . $row->id . "</td>
							<td id='id_dpo' style='display: none;'>" . $row->id_dpo . "</td>
							<td id='id_obat' style='display: none;'>" . $row->id_obat . "</td>
							<td id='kode_obat'><small>" . $row->kode_obat . "</small></td>
							<td id='nama_obat'><small>" . $row->nama_obat . "</small></td>
							<td id='nama_jenis_obat'><small>" . $row->nama_jenis_obat . "</small></td>
							<td id='stok_entri' style='display: none;'>" . $row->stok_entri . "</td>
							<td id='f_stok_entri'><small>" . ArrayAdapter::format("number", $row->stok_entri) . "</small></td>
							<td id='jumlah_tercatat' style='display: none;'>" . $row->jumlah_tercatat . "</td>
							<td id='f_jumlah_tercatat'><small>" . ArrayAdapter::format("number", $row->jumlah_tercatat) . "</small></td>
							<td id='jumlah' style='display: none;'>" . $row->jumlah . "</td>
							<td id='f_jumlah'><small>" . ArrayAdapter::format("number", $row->jumlah) . "</small></td>
							<td id='satuan'><small>" . $row->satuan . "</small></td>
							<td id='hna' style='display: none;'>" . $row->hna . "</td>
							<td id='hpp' style='display: none;'>" . $hpp . "</td>
							<td id='f_hna'><small>" . ArrayAdapter::format("money", $hpp) . "</small></td>
							<td id='diskon' style='display: none;'>" . $row->diskon . "</td>
							<td id='t_diskon' style='display: none;'>" . $row->t_diskon . "</td>
							<td id='f_diskon'><small><div align='right'>" . $f_diskon . "</div></small></td>
							<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='f_subtotal'><small>" . $f_subtotal . "</small></td>
							<td id='produsen' style='display: none;'>" . $row->produsen . "</td>
							<td id='no_batch' style='display: none;'>" . $row->no_batch . "</td>
							<td id='tanggal_exp' style='display: none;'>" . $row->tanggal_exp . "</td>
							<td>" . $html_edit_action . "</td>
							<td>" . $html_del_action . "</td>
						</tr>";
					$num++;
				}
			}
			$data = array();
			$data['html'] = $html;
			$data['row_num'] = $num;
			echo json_encode($data);
		} else if ($_POST['command'] == "show_footer") {
			$html = "<tfoot>
						<tr>
							<td colspan='10'><div align='right'><small><strong>Total I</strong></small></div></td>
							<td id='total'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='10'><div align='right'><small><strong>Diskon</strong></small></div></td>
							<td id='diskon_global'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='10'><div align='right'><small><strong>Total II</strong></small></div></td>
							<td id='total_2'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='10'><div align='right'><small><strong>PPn</strong></small></div></td>
							<td id='ppn'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='10'><div align='right'><small><strong>Jumlah Tagihan</strong></small></div></td>
							<td id='tagihan'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
					</tfoot>
			";
			$data = array();
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_last_item") {
			$id_obat = $_POST['id_obat'];
			$dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
			$row = $dbtable->get_row("
				SELECT *
				FROM smis_lgs_dobat_f_masuk
				WHERE id_obat = '" . $id_obat . "'
				ORDER BY id DESC
				LIMIT 0, 1
			");
			$data = array(
				"data"	=> $row
			);
			echo json_encode($data);
		} else {
			$adapter = new SimpleAdapter();
			$dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
			$dbresponder = new ObatMasukDBResponder(
				$dbtable,
				$table,
				$adapter
			);
			if ($dbresponder->isSave() && ($_POST['id'] == 0 || $_POST['id'] == "")) {
				$no_bbm = getSettings($db, "no_bbm", 0) + 1;
				setSettings($db, "no_bbm", $no_bbm);
			}
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
		}
		return;
	}

	echo $header_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>";
	if ($editable_detail == "true") {
		echo	 "<div class='span4'>" .
					 $detail_form->getHtml() .
				 "</div>" .
				 "<div class='span8'>" .
					 $table->getHtml() .
					 "<div align='right'>" . $button_group->getHtml() . "</div>" .
				 "</div>";
	} else {
		echo	 "<div class='span12'>" .
					 $table->getHtml() .
					 "<div align='right'>" . $button_group->getHtml() . "</div>" .
				 "</div>";
	}
	echo 	 "</div>" .
		 "</div>";
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("gudang_logistik/js/penerimaan_obat_vendor_action.js", false);
	echo addJS("gudang_logistik/js/penerimaan_obat_obat_action.js", false);
	echo addJS("gudang_logistik/js/dpenerimaan_obat_non_opl_action.js", false);
	echo addJS("gudang_logistik/js/penerimaan_obat_action.js", false);
	echo addJS("gudang_logistik/js/penerimaan_obat_non_opl_form.js", false);
?>