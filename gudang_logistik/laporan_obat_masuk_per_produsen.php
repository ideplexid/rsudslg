<?php 
	global $db;
	
	$laporan_form = new Form("lompp_form", "", "Gudang Logistik : Laporan Obat Masuk Per Produsen");
	$tipe_option = new OptionBuilder();
	$tipe_option->add("SEMUA", "%%", "1");
	$tipe_option->add("REGULER", "logistik");
	$tipe_option->add("SITO", "sito");
	$tipe_option->add("KONSINYASI", "konsinyasi");
	$tipe_select = new Select("lompp_tipe", "lompp_tipe", $tipe_option->getContent());
	$laporan_form->addElement("Tipe", $tipe_select);
	$produsen_button = new Button("", "", "Pilih");
	$produsen_button->setClass("btn-info");
	$produsen_button->setAction("produsen.chooser('produsen', 'produsen_button', 'produsen', produsen)");
	$produsen_button->setIcon("icon-white icon-list-alt");
	$produsen_button->setIsButton(Button::$ICONIC);
	$produsen_button->setAtribute("id='produsen_browse'");
	$produsen_text = new Text("lompp_produsen", "lompp_produsen", "");
	$produsen_text->setClass("smis-one-option-input");
	$produsen_text->setAtribute("disabled='disabled'");
	$produsen_input_group = new InputGroup("");
	$produsen_input_group->addComponent($produsen_text);
	$produsen_input_group->addComponent($produsen_button);
	$laporan_form->addElement("Produsen", $produsen_input_group);
	$tanggal_from_text = new Text("lompp_tanggal_from", "lompp_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lompp_tanggal_to", "lompp_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$filter_option = new OptionBuilder();
	$filter_option->add("MELIPUTI", "IN");
	$filter_option->add("SELAIN", "NOT IN", "1");
	$filter_obat_select = new Select("lompp_filter_obat", "lompp_filter_obat", $filter_option->getContent());
	$laporan_form->addElement("Fil. Obat", $filter_obat_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lompp.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lompp.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	
	$lompp_table = new Table(
		array("Nama Obat", "Jumlah", "Satuan", "Harga Satuan", "Total H. Satuan"),
		"",
		null,
		true
	);
	$lompp_table->setName("lompp");
	$lompp_table->setAction(false);
	
	class ObatTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup('noprint');
			$btn = new Button($this->name . "_add", "", "Add");
			$btn->setAction("obat_mp.chooser('obat_mp', 'obat_mp_button', 'obat_mp', obat_mp)");
			$btn->setClass("btn-primary");
			$btn->setIcon("fa fa-plus");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button($this->name . "_clear", "", "Clear");
			$btn->setAction("obat_mp.clear()");
			$btn->setClass("btn-danger");
			$btn->setIcon("fa fa-trash");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$obat_list_table = new ObatTable(
		array("Nama Obat", "Jenis Obat", "Jns. Pembelian"),
		"",
		null,
		true
	);
	$obat_list_table->setName("obat_lmp");
	$obat_list_table->setFooterVisible(false);
	
	//chooser produsen:
	$produsen_table = new Table(
		array("Produsen"),
		"",
		null,
		true
	);
	$produsen_table->setName("produsen");
	$produsen_table->setModel(Table::$SELECT);
	$produsen_adapter = new SimpleAdapter();
	$produsen_adapter->add("id", "produsen");
	$produsen_adapter->add("Produsen", "produsen");
	$produsen_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
	$produsen_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (produsen LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT produsen AS 'id', produsen
		FROM (
			SELECT DISTINCT smis_lgs_dobat_f_masuk.produsen
			FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
			WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
		) v_produsen
		WHERE " . $filter . "
		ORDER BY produsen ASC
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT smis_lgs_dobat_f_masuk.produsen
			FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
			WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
		) v_produsen
		WHERE " . $filter . "
		ORDER BY produsen ASC
	";
	$produsen_dbtable->setPreferredQuery(true, $query_value, $query_count);
	class ProdusenDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['id'] = $id;
			$data['produsen'] = $id;
			return $data;
		}
	}
	$produsen_dbresponder = new ProdusenDBResponder(
		$produsen_dbtable,
		$produsen_table,
		$produsen_adapter
	);
	
	//chooser obat:
	$obat_table = new Table(
		array("Nomor", "Nama Obat", "Jenis Obat", "Jns. Pembelian"),
		"",
		null,
		true
	);
	$obat_table->setName("obat_mp");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("id", "id");
	$obat_adapter->add("Nomor", "id", "digit8");
	$obat_adapter->add("Nama Obat", "nama_obat");
	$obat_adapter->add("Jenis Obat", "nama_jenis_obat");
	$obat_adapter->add("Jns. Pembelian", "label", "unslug");
	$obat_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
	$obat_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT id_obat AS 'id', nama_obat, nama_jenis_obat, label
		FROM (
			SELECT DISTINCT smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.nama_obat, smis_lgs_dobat_f_masuk.nama_jenis_obat, smis_lgs_dobat_f_masuk.label
			FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
			WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
		) v_produsen
		WHERE " . $filter . "
		ORDER BY nama_obat, nama_jenis_obat, label ASC
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.nama_obat, smis_lgs_dobat_f_masuk.nama_jenis_obat, smis_lgs_dobat_f_masuk.label
			FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
			WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
		) v_produsen
		WHERE " . $filter . "
		ORDER BY nama_obat, nama_jenis_obat, label ASC
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	class ObatMPDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$row = $this->dbtable->get_row("
				SELECT id_obat, nama_obat, nama_jenis_obat, label
				FROM (
					SELECT DISTINCT smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.nama_obat, smis_lgs_dobat_f_masuk.nama_jenis_obat, smis_lgs_dobat_f_masuk.label
					FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
					WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
				) v_produsen
				WHERE id_obat = '" . $id . "'
				ORDER BY nama_obat, nama_jenis_obat, label ASC
			");
			$data['id_obat'] = $id;
			$data['nama_obat'] = $row->nama_obat;
			$data['nama_jenis_obat'] = $row->nama_jenis_obat;
			$data['label'] = $row->label;
			$data['f_label'] = ArrayAdapter::format("unslug", $row->label);
			return $data;
		}
	}
	$obat_dbresponder = new ObatMPDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("produsen", $produsen_dbresponder);
	$super_command->addResponder("obat_mp", $obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$harga_satuan = 100 * $row->hna / 110;
				$array['Harga Satuan'] = self::format("money Rp. ", $harga_satuan);
				$array['Total H. Satuan'] = self::format("money Rp. ", $row->jumlah * $harga_satuan);
				return $array;
			}
		}
		$lompp_adapter = new LLSOAdapter();		
		$lompp_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
		$filter_obat = " NOT IN ('') ";
		if (isset($_POST['obat'])) {
			$obats = $_POST['obat'];
			if (count($obats) > 0) {
				$filter_obat = " " . $_POST['filter_obat'] . " ('" . implode("','", $obats) . "') ";
			}
		}
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT smis_lgs_dobat_f_masuk.id, smis_lgs_dobat_f_masuk.nama_obat, SUM(smis_lgs_dobat_f_masuk.jumlah) AS 'jumlah', smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
				FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
				WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.produsen = '" . $_POST['produsen'] . "' AND smis_lgs_obat_f_masuk.tanggal >= '" . $_POST['tanggal_from'] . "' AND smis_lgs_obat_f_masuk.tanggal <= '" . $_POST['tanggal_to'] . "' AND smis_lgs_obat_f_masuk.tipe LIKE '" . $_POST['tipe'] . "' AND smis_lgs_dobat_f_masuk.id_obat " . $filter_obat . "
				GROUP BY smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
			) v_lompp
			WHERE " . $filter . "
			ORDER BY nama_obat ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT smis_lgs_dobat_f_masuk.id, smis_lgs_dobat_f_masuk.nama_obat, SUM(smis_lgs_dobat_f_masuk.jumlah) AS 'jumlah', smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
				FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
				WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.produsen = '" . $_POST['produsen'] . "' AND smis_lgs_obat_f_masuk.tanggal >= '" . $_POST['tanggal_from'] . "' AND smis_lgs_obat_f_masuk.tanggal <= '" . $_POST['tanggal_to'] . "' AND smis_lgs_obat_f_masuk.tipe LIKE '" . $_POST['tipe'] . "' AND smis_lgs_dobat_f_masuk.id_obat " . $filter_obat . "
				GROUP BY smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
			) v_lompp
			WHERE " . $filter . "
			ORDER BY nama_obat ASC
		";
		$lompp_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['tanggal_from'];
				$to = $_POST['tanggal_to'];
				$produsen = $_POST['produsen'];
				$tipe = $_POST['tipe'];
				$filter_obat = " NOT IN ('') ";
				if (isset($_POST['obat'])) {
					$obats = $_POST['obat'];
					if (count($obats) > 0) {
						$filter_obat = " " . $_POST['filter_obat'] . " ('" . implode("','", $obats) . "') ";
					}
				}
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_lgs_dobat_f_masuk.id, smis_lgs_dobat_f_masuk.nama_obat, SUM(smis_lgs_dobat_f_masuk.jumlah) AS 'jumlah', smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
						FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
						WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.produsen = '" . $produsen . "' AND smis_lgs_obat_f_masuk.tanggal >= '" . $from . "' AND smis_lgs_obat_f_masuk.tanggal <= '" . $to . "' AND smis_lgs_obat_f_masuk.tipe LIKE '" . $tipe . "' AND smis_lgs_dobat_f_masuk.id_obat " . $filter_obat . "
						GROUP BY smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
					) v_lompp
					ORDER BY nama_obat ASC
				");
				if ($tipe == "logistik") {
					$tipe = "reguler";
				} else if ($tipe == "%%") {
					$tipe = "semua";
				}
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN OBAT MASUK PER PRODUSEN</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Produsen</td>
										<td>:</td>
										<td>" . $produsen . "</td>
									</tr>
									<tr>
										<td>Jns. Penerimaan</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $tipe) . "</td>
									</tr>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>";
				if (isset($_POST['label_obat'])) {
					$label_obats = $_POST['label_obat'];
					if (count($label_obats) > 0) {
						if ($filter_obat == "IN") {
							$print_data .= "<tr>
												<td>Obat</td>
												<td>:</td>
												<td>" . implode(', ', $label_obats) . "</td>
											</tr>";
						} else {
							$print_data .= "<tr>
												<td>Obat</td>
												<td>:</td>
												<td><strong>SELAIN</strong> " . implode(', ', $label_obats) . "</td>
											</tr>";
						}
					}
				}
				$print_data .= "</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>Nama Obat</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>Harga Satuan</th>
										<th>Total H. Satuan</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$harga_satuan = 100 * $d->hna / 110;
						$print_data .= "<tr>
											<td>" . $d->nama_obat . "</td>
											<td>" . $d->jumlah . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $harga_satuan) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->jumlah * $harga_satuan) . "</td>
										</tr>";
						$total += ($d->jumlah * $harga_satuan);
					}
				} else {
					$print_data .= "<tr>
										<td colspan='5' align='center'><i>Tidak terdapat data obat masuk</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='4' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lompp_dbresponder = new LLSODBResponder(
			$lompp_dbtable,
			$lompp_table,
			$lompp_adapter
		);
		$data = $lompp_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>" .
				 $obat_list_table->getHtml() .
			 "</div>" .
			 "<div class='row-fluid'>" .
				 $btn_group->getHtml() .
			 "</div>" .
		 "</div>";
	echo $lompp_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOMPPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOMPPAction.prototype.constructor = LOMPPAction;
	LOMPPAction.prototype = new TableAction();
	LOMPPAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['produsen'] = $("#lompp_produsen").val();
		data['tanggal_from'] = $("#lompp_tanggal_from").val();
		data['tanggal_to'] = $("#lompp_tanggal_to").val();
		data['tipe'] = $("#lompp_tipe").val();
		data['filter_obat'] = $("#lompp_filter_obat").val();
		var obat_mp_list = new Array();
		var nor_obat_mp =  $("tbody#obat_lmp_list").children("tr").length;
		for (var i = 0; i < nor_obat_mp; i++) {
			var obat_mp_prefix = $("tbody#obat_lmp_list").children("tr").eq(i).prop("id");
			var id_obat = $("#" + obat_mp_prefix + "_id_obat").text();
			obat_mp_list.push(id_obat);
		}
		data['obat'] = obat_mp_list;
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOMPPAction.prototype.print = function() {
		if ($("#lompp_produsen").val() == "" || $("#lompp_tanggal_from").val() == "" || $("#lompp_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['produsen'] = $("#lompp_produsen").val();
		data['tanggal_from'] = $("#lompp_tanggal_from").val();
		data['tanggal_to'] = $("#lompp_tanggal_to").val();
		data['tipe'] = $("#lompp_tipe").val();
		data['filter_obat'] = $("#lompp_filter_obat").val();
		var obat_mp_list = new Array();
		var label_obat_mp_list = new Array();
		var nor_obat_mp =  $("tbody#obat_lmp_list").children("tr").length;
		for (var i = 0; i < nor_obat_mp; i++) {
			var obat_mp_prefix = $("tbody#obat_lmp_list").children("tr").eq(i).prop("id");
			var id_obat = $("#" + obat_mp_prefix + "_id_obat").text();
			var label = $("#" + obat_mp_prefix + "_nama_obat").text();
			obat_mp_list.push(id_obat);
			label_obat_mp_list.push(label);
		}
		data['obat'] = obat_mp_list;
		data['label_obat'] = label_obat_mp_list;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	function ProdusenAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ProdusenAction.prototype.constructor = ProdusenAction;
	ProdusenAction.prototype = new TableAction();
	ProdusenAction.prototype.selected = function(json) {
		$("#lompp_produsen").val(json.produsen);
	};
	
	function ObatMPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ObatMPAction.prototype.constructor = ObatMPAction;
	ObatMPAction.prototype = new TableAction();
	ObatMPAction.prototype.selected = function(json) {
		$("tbody#obat_lmp_list").append(
			"<tr id='obat_mp_" + obat_mp_num + "'>" +
				"<td id='obat_mp_" + obat_mp_num + "_id_obat' style='display: none;'>" + json.id_obat + "</td>" +
				"<td id='obat_mp_" + obat_mp_num + "_label' style='display: none;'>" + json.label + "</td>" +
				"<td id='obat_mp_" + obat_mp_num + "_nama_obat'>" + json.nama_obat + "</td>" +
				"<td id='obat_mp_" + obat_mp_num + "_nama_jenis_obat'>" + json.nama_jenis_obat + "</td>" +
				"<td id='obat_mp_" + obat_mp_num + "_f_label'>" + json.f_label + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='produsen_m.delete(" + obat_mp_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		obat_mp_num++;
	};
	ObatMPAction.prototype.clear = function() {
		$("#obat_lmp_list").empty();
	};
	ObatMPAction.prototype.delete = function(r_num) {
		$("#obat_mp_" + r_num).remove();
	};
	
	var lompp;
	var produsen;
	var obat_mp;
	var obat_mp_num;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		produsen = new ProdusenAction(
			"produsen",
			"gudang_logistik",
			"laporan_obat_masuk_per_produsen",
			new Array()
		);
		produsen.setSuperCommand("produsen");
		obat_mp = new ObatMPAction(
			"obat_mp",
			"gudang_logistik",
			"laporan_obat_masuk_per_produsen",
			new Array()
		);
		obat_mp.setSuperCommand("obat_mp");
		obat_mp_num = 0;
		lompp = new LOMPPAction(
			"lompp",
			"gudang_logistik",
			"laporan_obat_masuk_per_produsen",
			new Array()
		)
	});
</script>