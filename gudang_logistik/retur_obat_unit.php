<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	
	class ReturObatTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'], $d['restok'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $status, $restok) {
			$btn_group = new ButtonGroup("noprint");
			if ($status == "sudah") {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				if (!$restok) {
					$btn->setClass("btn-success");
				} else {
					$btn->setClass("btn-inverse");
				}
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				if (!$restok) {
					$btn = new Button("", "", "Restok");
					$btn->setAction($this->action . ".restock('" . $id . "')");
					$btn->setAtribute("data-content='Restok' data-toggle='popover'");
					$btn->setIcon("fa fa-refresh");
					$btn->setIsButton(Button::$ICONIC);
					$btn_group->addElement($btn);
				} else {
					// $btn = new Button("", "", "Batal Restok");
					// $btn->setAction($this->action . ".cancel_restock('" . $id . "')");
					// $btn->setClass("btn-danger");
					// $btn->setAtribute("data-content='Batal Restok' data-toggle='popover'");
					// $btn->setIcon("icon-remove icon-white");
					// $btn->setIsButton(Button::$ICONIC);
					// $btn_group->addElement($btn);
				}
			} else {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
	$retur_obat_table = new ReturObatTable(
		array("Unit", "Nomor", "Tanggal Retur", "Nama Obat", "Jenis Obat", "Produsen", "Vendor", "Jumlah", "Status"),
		"Gudang Logistik : Retur Obat Unit",
		null,
		true
	);
	$retur_obat_table->setName("retur_obat");
	$retur_obat_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class ReturObatAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['status'] = $row->status;
				$array['restok'] = $row->restok;
				$array['Unit'] = self::format("unslug", $row->unit);
				$array['Nomor'] = self::format("digit8", $row->f_id);
				$array['Tanggal Retur'] = self::format("date d M Y", $row->tanggal);
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
				if ($row->status == "belum") {
					$array['Status'] = "Belum Diterima";
				} else if ($row->status == "sudah") {
					$array['Status'] = "Sudah Diterima";
					if ($row->restok)
						$array['Status'] = "Restok";
				} else if ($row->status == "dikembalikan") {
					$array['Status'] = "Dikembalikan";
				}
				return $array;
			}
		}
		$retur_obat_adapter = new ReturObatAdapter();
		$columns = array("id", "f_id", "unit", "tanggal", "id_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "no_batch", "turunan", "keterangan", "status", "id_vendor", "nama_vendor", "produsen", "restok");
		$retur_obat_dbtable = new DBTable(
			$db,
			"smis_lgs_retur_obat_unit",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%' OR nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR produsen LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM smis_lgs_retur_obat_unit
			WHERE prop NOT LIKE 'del' AND status != 'dikembalikan' " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_lgs_retur_obat_unit
			WHERE prop NOT LIKE 'del' AND status != 'dikembalikan' " . $filter . "
		";
		$retur_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class ReturObatDBResponder extends DBResponder {
			public function save() {
				$data = $this->postToArray();
				$id['id'] = $_POST['id'];
				$result = $this->dbtable->update($data, $id);
				$success['type'] = "update";
				if (isset($_POST['restok'])) {
					if ($_POST['restok'] == true) {
						//menambah stok:
						$retur_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_lgs_retur_obat_unit
							WHERE id = '" . $id['id'] . "'
						");
						$stok_keluar_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_lgs_stok_obat_keluar
							WHERE id = '" . $retur_data->lf_id . "'
						");
						if ($stok_keluar_data != null) {
							$stok_data = $this->dbtable->get_row("
								SELECT a.*, b.id_obat
								FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
								WHERE a.id = '" . $stok_keluar_data->id_stok_obat . "'
							");
							$new_stok_data = array();
							$new_stok_data['sisa'] = $stok_data->sisa + $retur_data->jumlah;
							$new_stok_id = array();
							$new_stok_id['id'] = $stok_keluar_data->id_stok_obat;
							$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
							$stok_dbtable->update($new_stok_data, $new_stok_id);

							//kartu gudang induk:
							$ks_data = array();
							$ks_data['f_id'] = $retur_data->id;
							$ks_data['unit'] = ArrayAdapter::format("unslug", $retur_data->unit);
							$ks_data['no_bon'] = $retur_data->id;
							$ks_data['id_obat'] = $stok_data->id_obat;
							$ks_data['kode_obat'] = $stok_data->kode_obat;
							$ks_data['nama_obat'] = $stok_data->nama_obat;
							$ks_data['nama_jenis_obat'] = $stok_data->nama_jenis_obat;
							$ks_data['tanggal'] = date("Y-m-d");
							$ks_data['masuk'] = $retur_data->jumlah;
							$ks_data['keluar'] = 0;
							$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_kartu_stok");
							$ks_row = $ks_dbtable->get_row("
								SELECT SUM(b.sisa) AS 'jumlah'
								FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_stok_obat b ON a.id = b.id_dobat_masuk
								WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $stok_data->id_obat . "'
							");
							$ks_data['sisa'] = $ks_row->jumlah;
							$ks_dbtable->insert($ks_data);

							//logging riwayat stok:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $stok_keluar_data->id_stok_obat;
							$data_riwayat['jumlah_masuk'] = $retur_data->jumlah;
							$data_riwayat['sisa'] = $stok_data->sisa + $retur_data->jumlah;
							$data_riwayat['keterangan'] = "Restok Retur Obat dari Unit " . ArrayAdapter::format("unslug", $retur_data->unit);
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
						} else {
							// insert dobat masuk:
							$dobat_f_masuk_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_dobat_f_masuk");
							$data_dobat_f_masuk = array();
							$data_dobat_f_masuk['id_obat_f_masuk'] = "0";
							$data_dobat_f_masuk['id_dpo'] = "-1";
							$data_dobat_f_masuk['id_obat'] = $retur_data->id_obat;
							$data_dobat_f_masuk['nama_obat'] = $retur_data->nama_obat;
							$data_dobat_f_masuk['nama_jenis_obat'] = $retur_data->nama_jenis_obat;
							$data_dobat_f_masuk['formularium'] = "0";
							$data_dobat_f_masuk['generik'] = "0";
							$data_dobat_f_masuk['berlogo'] = "0";
							$data_dobat_f_masuk['medis'] = "1";
							$data_dobat_f_masuk['label'] = $retur_data->label;
							$data_dobat_f_masuk['jumlah'] = $retur_data->jumlah;
							$data_dobat_f_masuk['sisa'] = 0;
							$data_dobat_f_masuk['satuan'] = $retur_data->satuan;
							$data_dobat_f_masuk['konversi'] = $retur_data->konversi;
							$data_dobat_f_masuk['satuan_konversi'] = $retur_data->satuan_konversi;
							$data_dobat_f_masuk['hna'] = $retur_data->hna;
							$data_dobat_f_masuk['selisih'] = 0;
							$data_dobat_f_masuk['diskon'] = 0;
							$data_dobat_f_masuk['t_diskon'] = 'persen';
							$dobat_f_masuk_dbtable->insert($data_dobat_f_masuk);
							$id_dobat_f_masuk = $dobat_f_masuk_dbtable->get_inserted_id();
							// insert stok obat:
							$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
							$data_stok = array();
							$data_stok['id_dobat_masuk'] = $id_dobat_f_masuk;
							$data_stok['nama_obat'] = $retur_data->nama_obat;
							$data_stok['nama_jenis_obat'] = $retur_data->nama_jenis_obat;
							$data_stok['formularium'] = "0";
							$data_stok['berlogo'] = "0";
							$data_stok['generik'] = "0";
							$data_stok['label'] = $retur_data->label;
							$data_stok['id_vendor'] = $retur_data->id_vendor;
							$data_stok['nama_vendor'] = $retur_data->nama_vendor;
							$data_stok['jumlah'] = $retur_data->jumlah;
							$data_stok['sisa'] = $retur_data->jumlah;
							$data_stok['retur'] = 0;
							$data_stok['satuan'] = $retur_data->satuan;
							$data_stok['konversi'] = $retur_data->konversi;
							$data_stok['satuan_konversi'] = $retur_data->satuan_konversi;
							$data_stok['hna'] = $retur_data->hna;
							$data_stok['produsen'] = $retur_data->produsen;
							$data_stok['tanggal_exp'] = $retur_data->tanggal_exp;
							$data_stok['no_batch'] = $retur_data->no_batch;
							$data_stok['turunan'] = 0;
							$stok_dbtable->insert($data_stok);
							$id_stok_obat = $stok_dbtable->get_inserted_id();
							// logging riwayat stok:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $id_stok_obat;
							$data_riwayat['jumlah_masuk'] = $retur_data->jumlah;
							$data_riwayat['sisa'] = $retur_data->jumlah;
							$data_riwayat['keterangan'] = "Restok Retur Obat dari Unit " . ArrayAdapter::format("unslug", $retur_data->unit);
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
							//kartu gudang induk:
							$ks_data = array();
							$ks_data['f_id'] = $retur_data->id;
							$ks_data['unit'] = ArrayAdapter::format("unslug", $retur_data->unit);
							$ks_data['no_bon'] = $retur_data->id;
							$ks_data['id_obat'] = $retur_data->id_obat;
							$ks_data['kode_obat'] = $retur_data->kode_obat;
							$ks_data['nama_obat'] = $retur_data->nama_obat;
							$ks_data['nama_jenis_obat'] = $retur_data->nama_jenis_obat;
							$ks_data['tanggal'] = date("Y-m-d");
							$ks_data['masuk'] = $retur_data->jumlah;
							$ks_data['keluar'] = 0;
							$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_kartu_stok");
							$ks_row = $ks_dbtable->get_row("
								SELECT SUM(b.sisa) AS 'jumlah'
								FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_stok_obat b ON a.id = b.id_dobat_masuk
								WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $retur_data->id_obat . "'
							");
							$ks_data['sisa'] = $ks_row->jumlah;
							$ks_dbtable->insert($ks_data);
						}
					} else {
						//mengurangi stok:
						$retur_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_lgs_retur_obat_unit
							WHERE id = '" . $id['id'] . "'
						");
						$stok_keluar_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_lgs_stok_obat_keluar
							WHERE id = '" . $retur_data->lf_id . "'
						");
						$stok_data = $this->dbtable->get_row("
							SELECT *
							FROM smis_lgs_stok_obat
							WHERE id = '" . $stok_keluar_data->id_stok_obat . "'
						");
						$new_stok_data = array();
						$new_stok_data['sisa'] = $stok_data->sisa - $retur_data->jumlah;
						$new_stok_id = array();
						$new_stok_id['id'] = $stok_keluar_data->id_stok_obat;
						$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
						$stok_dbtable->update($new_stok_data, $new_stok_id);
						//logging riwayat stok:
						$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $stok_keluar_data->id_stok_obat;
						$data_riwayat['jumlah_keluar'] = $retur_data->jumlah;
						$data_riwayat['sisa'] = $stok_data->sisa - $retur_data->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Restok Retur Obat dari Unit " . ArrayAdapter::format("unslug", $retur_data->unit);
						global $user;
						$data_riwayat['nama_user'] = $user->getName();
						$riwayat_dbtable->insert($data_riwayat);
						//kartu gudang induk:
						$ks_data = array();
						$ks_data['f_id'] = $retur_data->id;
						$ks_data['unit'] = ArrayAdapter::format("unslug", $retur_data->unit);
						$ks_data['no_bon'] = $retur_data->id;
						$ks_data['id_obat'] = $retur_data->id_obat;
						$ks_data['kode_obat'] = $retur_data->kode_obat;
						$ks_data['nama_obat'] = $retur_data->nama_obat;
						$ks_data['nama_jenis_obat'] = $retur_data->nama_jenis_obat;
						$ks_data['tanggal'] = date("Y-m-d");
						$ks_data['masuk'] = 0;
						$ks_data['keluar'] = $retur_data->jumlah;
						$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_kartu_stok");
						$ks_row = $ks_dbtable->get_row("
							SELECT SUM(b.sisa) AS 'jumlah'
							FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_stok_obat b ON a.id = b.id_dobat_masuk
							WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $retur_data->id_obat . "'
						");
						$ks_data['sisa'] = $ks_row->jumlah;
						$ks_dbtable->insert($ks_data);
					}
				}
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
		}
		$retur_obat_dbresponder = new ReturObatDBResponder(
			$retur_obat_dbtable,
			$retur_obat_table,
			$retur_obat_adapter
		);
		$data = $retur_obat_dbresponder->command($_POST['command']);
		//set_retur_obat_status:
		if (isset($_POST['push_command'])) {
			class SetReturObatStatusServiceConsumer extends ServiceConsumer {
				public function __construct($db, $id, $status, $unit, $command) {
					$array = array();
					$array['id'] = $id;
					$array['status'] = $status;
					$array['command'] = $command;
					parent::__construct($db, "set_retur_obat_status", $array, $unit);
				}
				public function proceedResult() {
					$content = array();
					$result = json_decode($this->result,true);
					foreach ($result as $autonomous) {
						foreach ($autonomous as $entity) {
							if ($entity != null || $entity != "")
								return $entity;
						}
					}
					return $content;
				}
			}
			$retur_obat_dbtable = new DBTable($db, "smis_lgs_retur_obat_unit");
			$retur_obat_row = $retur_obat_dbtable->get_row("
				SELECT f_id, unit, status
				FROM smis_lgs_retur_obat_unit
				WHERE id = '" . $data['content']['id'] . "'
			");
			$command = "push_" . $_POST['push_command'];
			$set_retur_obat_status_service_consumer = new SetReturObatStatusServiceConsumer($db, $retur_obat_row->f_id, $retur_obat_row->status, $retur_obat_row->unit, $command);
			$set_retur_obat_status_service_consumer->execute();
		}
		echo json_encode($data);
		return;
	}
	
	$retur_obat_modal = new Modal("retur_obat_add_form", "smis_form_container", "retur_obat");
	$retur_obat_modal->setTitle("Retur Obat");
	$id_hidden = new Hidden("retur_obat_id", "retur_obat_id", "");
	$retur_obat_modal->addElement("", $id_hidden);
	$id_stok_obat_hidden = new Hidden("retur_obat_id_stok_obat", "retur_obat_id_stok_obat", "");
	$retur_obat_modal->addElement("", $id_stok_obat_hidden);
	$tanggal_text = new Text("retur_obat_tanggal", "retur_obat_tanggal", "");
	$tanggal_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Tanggal", $tanggal_text);
	$nama_obat_text = new Text("retur_obat_nama_obat", "retur_obat_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Nama Obat", $nama_obat_text);
	$nama_jenis_text = new Text("retur_obat_jenis", "retur_obat_jenis", "");
	$nama_jenis_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Jenis Obat", $nama_jenis_text);
	$produsen_text = new Text("retur_obat_produsen", "retur_obat_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Produsen", $produsen_text);
	$vendor_text = new Text("retur_obat_vendor", "retur_obat_vendor", "");
	$vendor_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Vendor", $vendor_text);
	$f_jumlah_text = new Text("retur_obat_f_jumlah", "retur_obat_f_jumlah", "");
	$f_jumlah_text->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Jumlah", $f_jumlah_text);
	$keterangan_textarea = new TextArea("retur_obat_keterangan", "retur_obat_keterangan", "");
	$keterangan_textarea->setLine(2);
	$keterangan_textarea->setAtribute("disabled='disabled'");
	$retur_obat_modal->addElement("Keterangan", $keterangan_textarea);
	$retur_obat_button = new Button("", "", "Terima");
	$retur_obat_button->setClass("btn-success");
	$retur_obat_button->setAtribute("id='retur_obat_accept'");
	$retur_obat_button->setAction("retur_obat.accept()");
	$retur_obat_button->setIcon("fa fa-check");
	$retur_obat_button->setIsButton(Button::$ICONIC);
	$retur_obat_modal->addFooter($retur_obat_button);
	$retur_obat_button = new Button("", "", "Kembalikan");
	$retur_obat_button->setClass("btn-danger");
	$retur_obat_button->setAtribute("id='retur_obat_return'");
	$retur_obat_button->setAction("retur_obat.return()");
	$retur_obat_button->setIcon("fa fa-times");
	$retur_obat_button->setIsButton(Button::$ICONIC);
	$retur_obat_modal->addFooter($retur_obat_button);
	$retur_obat_button = new Button("", "", "OK");
	$retur_obat_button->setClass("btn-success");
	$retur_obat_button->setAtribute("id='retur_obat_ok'");
	$retur_obat_button->setAction("$($(this).data('target')).smodal('hide')");
	$retur_obat_modal->addFooter($retur_obat_button);
	
	echo $retur_obat_modal->getHtml();
	echo $retur_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function ReturObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ReturObatAction.prototype.constructor = ReturObatAction;
	ReturObatAction.prototype = new TableAction();
	ReturObatAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#retur_obat_id").val(json.id);
				$("#retur_obat_tanggal").val(json.tanggal);
				$("#retur_obat_nama_obat").val(json.nama_obat);
				$("#retur_obat_jenis").val(json.nama_jenis_obat);
				$("#retur_obat_produsen").val(json.produsen);
				$("#retur_obat_vendor").val(json.nama_vendor);
				$("#retur_obat_f_jumlah").val(json.jumlah + " " + json.satuan);
				$("#retur_obat_keterangan").val(json.keterangan);
				$("#retur_obat_accept").show();
				$("#retur_obat_return").show();
				$("#retur_obat_ok").hide();
				$("#retur_obat_add_form").smodal("show");
			}
		);
	};
	ReturObatAction.prototype.accept = function() {
		$("#retur_obat_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "save";
		data['id'] = $("#retur_obat_id").val();
		data['status'] = "sudah";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#retur_obat_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	ReturObatAction.prototype.return = function() {
		$("#retur_obat_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "save";
		data['id'] = $("#retur_obat_id").val();
		data['status'] = "dikembalikan";
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#retur_obat_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	ReturObatAction.prototype.detail = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#retur_obat_id").val(json.id);
				$("#retur_obat_tanggal").val(json.tanggal);
				$("#retur_obat_nama_obat").val(json.nama_obat);
				$("#retur_obat_jenis").val(json.nama_jenis_obat);
				$("#retur_obat_produsen").val(json.produsen);
				$("#retur_obat_vendor").val(json.nama_vendor);
				$("#retur_obat_f_jumlah").val(json.jumlah + " " + json.satuan);
				$("#retur_obat_keterangan").val(json.keterangan);
				$("#retur_obat_accept").hide();
				$("#retur_obat_return").hide();
				$("#retur_obat_ok").show();
				$("#retur_obat_add_form").smodal("show");
			}
		);
	};
	ReturObatAction.prototype.restock = function(id) {
		var self = this;
		bootbox.confirm(
			"Yakin memasukkan Stok Retur ini ke Perbekalan?",
			function(result) {
				if (result) {
					var data = self.getRegulerData();
					data['command'] = "save";
					data['id'] = id;
					data['restok'] = "1";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
						}
					);
				}
			}
		);
	};
	ReturObatAction.prototype.cancel_restock = function(id) {
		var self = this;
		bootbox.confirm(
			"Yakin membatalkan Restok Retur ini ke Perbekalan?",
			function(result) {
				if (result) {
					var data = self.getRegulerData();
					data['command'] = "save";
					data['id'] = id;
					data['restok'] = "0";
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
						}
					);
				}
			}
		);
	};
	
	var retur_obat;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#smis-chooser-modal").on("show", function() {
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("full_model");
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("full_model");
		});
		var retur_obat_columns = new Array("id", "f_id", "unit", "tanggal", "id_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "turunan", "keterangan", "status", "id_vendor", "nama_vendor", "produsen", "restok");
		retur_obat = new ReturObatAction(
			"retur_obat",
			"gudang_logistik",
			"retur_obat_unit",
			retur_obat_columns
		);
		retur_obat.view();
	});
</script>