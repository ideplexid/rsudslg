<?php 
	$laporan_tabulator = new Tabulator("laporan", "", Tabulator::$LANDSCAPE);
	$laporan_tabulator->add("laporan_rekap_pembelian", "Rekap Pembelian", "gudang_logistik/laporan_rekap_pembelian.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_pembelian_per_pbf", "Rekap Pembelian Per Distributor", "gudang_logistik/laporan_rekap_pembelian_per_pbf.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_persediaan_obat", "Stok Opname", "gudang_logistik/laporan_persediaan_obat.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_obat_rumah_sakit", "Rekap Obat RS - Form. A", "gudang_logistik/laporan_rekap_obat_rumah_sakit.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_obat_bpk", "Rekap Obat RS - Form. B", "gudang_logistik/laporan_rekap_obat_bpk.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_obat_apg", "Rekap Obat APG", "gudang_logistik/laporan_rekap_obat_apg.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_obat_npp", "Rekap Obat NPP", "gudang_logistik/laporan_rekap_obat_npp.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_obat_keluar_non_apotek", "Rekap Mutasi Obat", "gudang_logistik/laporan_obat_keluar_non_apotek.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_obat_keluar_per_ruangan", "Mutasi Obat Per Ruangan", "gudang_logistik/laporan_obat_keluar_per_ruangan.php", Tabulator::$TYPE_INCLUDE);
	
	echo $laporan_tabulator->getHtml();
?>
<style type="text/css">
	@page {
		margin-left: 50px;
		margin-right: 50px;
		margin-top: 50px;
		margin-bottom: 50px;
	}
	@media print{
		#signature {
			page-break-inside: avoid; 
		}
	}
</style>