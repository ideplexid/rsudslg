<?php 
global $db;
$_ID_OBAT=$_POST['id_obat'];
$_DARI=$_POST['dari'];
$_SAMPAI=$_POST['sampai'];

/* OBAT MASUK 
 * Menampilkan data Penjualan Obat Masuk sehingga 
 * yang ditampilkan berdasarkan ID, Nama, Kode, Jenis, Harga, Jumlah
 * sehingga dikumpulkan berdasarkan obat dan harga masuk-nya */
$query="SELECT 
			smis_lgs_dobat_f_masuk.id_obat,
			smis_lgs_dobat_f_masuk.nama_obat,
			smis_lgs_dobat_f_masuk.kode_obat,
			smis_lgs_dobat_f_masuk.nama_jenis_obat,
			smis_lgs_dobat_f_masuk.hna as harga,
			sum(smis_lgs_dobat_f_masuk.jumlah*smis_lgs_dobat_f_masuk.konversi) as jumlah
		FROM 
			smis_lgs_obat_f_masuk, 
			smis_lgs_dobat_f_masuk 
		WHERE 
			smis_lgs_dobat_f_masuk.id_obat_f_masuk=smis_lgs_obat_f_masuk.id
			AND smis_lgs_obat_f_masuk.tanggal<='".$_DARI."' 
			AND smis_lgs_obat_f_masuk.tanggal>'".$_SAMPAI."' 
			AND smis_lgs_dobat_f_masuk.id_obat='".$_ID_OBAT."'
			AND smis_lgs_obat_f_masuk.prop!='del' 
			AND smis_lgs_dobat_f_masuk.prop!='del'
		GROUP BY 	smis_lgs_dobat_f_masuk.id_obat, 
					smis_lgs_dobat_f_masuk.hna;";		
$masuk=$db->get_result($query);

/* OBAT Keluar 
 * Menampilkan data Penjualan Obat Keluar sehingga 
 * yang ditampilkan berdasarkan ID, Nama, Kode, Jenis, Harga, Jumlah
 * sehingga dikumpulkan berdasarkan obat dan harga masuk-nya */
$query="SELECT 
			smis_lgs_dobat_keluar.id_obat,
			smis_lgs_dobat_keluar.nama_obat,
			smis_lgs_dobat_keluar.kode_obat,
			smis_lgs_dobat_keluar.nama_jenis_obat,
			smis_lgs_dobat_keluar.harga_ma as harga,
			sum(smis_lgs_dobat_keluar.jumlah*smis_lgs_dobat_keluar.konversi) as jumlah
		FROM 
			smis_lgs_obat_keluar, 
			smis_lgs_dobat_keluar 
		WHERE 
			smis_lgs_obat_keluar.id = smis_lgs_dobat_keluar.id_obat_keluar
			AND smis_lgs_obat_keluar.tanggal<='".$_DARI."' 
			AND smis_lgs_obat_keluar.tanggal>'".$_SAMPAI."' 
			AND smis_lgs_dobat_keluar.id_obat='".$_ID_OBAT."'
			AND smis_lgs_obat_keluar.prop!='del' 
			AND smis_lgs_dobat_keluar.prop!='del'
		GROUP BY 	smis_lgs_dobat_keluar.id_obat, 
					smis_lgs_dobat_keluar.harga_ma;";	
$keluar=$db->get_result($query);

$result=array();
$result['keluar']=$keluar;
$result['masuk']=$masuk;
echo json_encode($result);
?>