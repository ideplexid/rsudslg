<?php 
if(isset($_POST['command'])){
	$dbtable=new DBTable($db, "smis_lgs_obat_f_masuk");
	$dbtable->addCustomKriteria(" id ", " !='0' ");
	$service=new ServiceProvider($dbtable);
	$pack=$service->command($_POST['command']);	
	if($_POST['command']=="edit"){
		require_once 'smis-base/smis-include-service-consumer.php';
		require_once 'gudang_logistik/kelas/DfakturAdapter.php';
		
		$pack=(array) $pack;
		$id_po=$pack['id_po'];
		$id_faktur=$pack['id'];
		$diskon=$pack['diskon'];
		$t_diskon=$pack['t_diskon'];		
		
		$responder=new ServiceResponder($db,$table,$adapter,"get_daftar_po");
		$responder->addData("id", $id_po);
		$pack_po=$responder->edit();
		$pack_po=(array) $pack_po;
		if($pack_po==NULL || !isset($pack_po['nama_vendor']) || $pack_po['nama_vendor']=="" || $pack_po['nama_vendor']==null){
			$pack_po['nama_vendor']=$pack['nama_vendor'];
			$pack_po['keterangan']="TIDAK ADA SP/PO - PEMBELIAN SITO";
		}
		$pack['po']=$pack_po;
		
		$dfaktur=new DBTable($db, "smis_lgs_dobat_f_masuk");
		$dfaktur->setShowAll(true);
		$dfaktur->addCustomKriteria("id_obat_f_masuk", "='".$id_faktur."'");
		$detail=$dfaktur->view("", "0");
		$adapter=new DFakturAdapter();
		$detail=$adapter->getContent($detail['data']);
		$pack['detail']=$detail;		
		$pack['harga_sebelum_diskon_global']=$adapter->getTotalBayar();
		
		$val_diskon=0;
		if($t_diskon=="nominal"){
			$val_diskon=$pack['diskon'];
			$pack['diskon_global']=ArrayAdapter::format("money Rp.", $val_diskon);
		}else{
			$val_diskon=$pack['harga_sebelum_diskon_global']*$diskon/100;
			$pack['diskon_global']=$diskon."% ( Rp. ".ArrayAdapter::format("only-money", $val_diskon)." )";			
		}
		$pack['total_bayar']=$pack['harga_sebelum_diskon_global']-$val_diskon;		
	}
	echo json_encode($pack);
}

?>