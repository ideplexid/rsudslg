<?php 
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_save") {
			$retur_obat_unit_dbtable = new DBTable($db, "smis_lgs_retur_obat_unit");
			$retur_obat_unit_row = $retur_obat_unit_dbtable->get_row("
				SELECT id
				FROM smis_lgs_retur_obat_unit
				WHERE f_id = '" . $_POST['id_retur_obat'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($retur_obat_unit_row == null) {
				//do insert:
				$retur_data = array();
				$retur_data['f_id'] = $_POST['id_retur_obat'];
				$retur_data['lf_id'] = $_POST['f_id'];
				$retur_data['unit'] = $_POST['unit'];
				$retur_data['tanggal'] = $_POST['tanggal'];
				$retur_data['id_obat'] = $_POST['id_obat'];
				$retur_data['kode_obat'] = $_POST['kode_obat'];
				$retur_data['nama_obat'] = $_POST['nama_obat'];
				$retur_data['nama_jenis_obat'] = $_POST['nama_jenis_obat'];
				$retur_data['label'] = $_POST['label'];
				$retur_data['id_vendor'] = $_POST['id_vendor'];
				$retur_data['nama_vendor'] = $_POST['nama_vendor'];
				$retur_data['jumlah'] = $_POST['jumlah'];
				$retur_data['satuan'] = $_POST['satuan'];
				$retur_data['konversi'] = $_POST['konversi'];
				$retur_data['satuan_konversi'] = $_POST['satuan_konversi'];
				$retur_data['hna'] = $_POST['hna'];
				$retur_data['produsen'] = $_POST['produsen'];
				$retur_data['tanggal_exp'] = $_POST['tanggal_exp'];
				$retur_data['no_batch'] = $_POST['no_batch'];
				$retur_data['turunan'] = $_POST['turunan'];
				$retur_data['keterangan'] = $_POST['keterangan'];
				$retur_data['status'] = $_POST['status'];
				$retur_data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
		        $retur_data['duplicate'] = 0;
		        $retur_data['time_updated'] = date("Y-m-d H:i:s");
		        $retur_data['origin_updated'] = getSettings($db, "smis_autonomous_id", "");
				$retur_obat_unit_dbtable->insert($retur_data);
				$retur_obat_unit_id = $retur_obat_unit_dbtable->get_inserted_id();
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$retur_obat_unit_id);
				$msg="Retur Obat dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong>";
				$notification->addNotification("Retur Obat Unit", $key, $msg, "gudang_logistik","retur_obat_unit");
			} else {
				//do update:
				$retur_data = array();
				$retur_data['f_id'] = $_POST['id_retur_obat'];
				$retur_data['lf_id'] = $_POST['f_id'];
				$retur_data['unit'] = $_POST['unit'];
				$retur_data['tanggal'] = $_POST['tanggal'];
				$retur_data['id_obat'] = $_POST['id_obat'];
				$retur_data['kode_obat'] = $_POST['kode_obat'];
				$retur_data['nama_obat'] = $_POST['nama_obat'];
				$retur_data['nama_jenis_obat'] = $_POST['nama_jenis_obat'];
				$retur_data['label'] = $_POST['label'];
				$retur_data['id_vendor'] = $_POST['id_vendor'];
				$retur_data['nama_vendor'] = $_POST['nama_vendor'];
				$retur_data['jumlah'] = $_POST['jumlah'];
				$retur_data['satuan'] = $_POST['satuan'];
				$retur_data['konversi'] = $_POST['konversi'];
				$retur_data['satuan_konversi'] = $_POST['satuan_konversi'];
				$retur_data['hna'] = $_POST['hna'];
				$retur_data['produsen'] = $_POST['produsen'];
				$retur_data['tanggal_exp'] = $_POST['tanggal_exp'];
				$retur_data['no_batch'] = $_POST['no_batch'];
				$retur_data['turunan'] = $_POST['turunan'];
				$retur_data['keterangan'] = $_POST['keterangan'];
				$retur_data['status'] = $_POST['status'];
				$retur_data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
		        $retur_data['duplicate'] = 0;
		        $retur_data['time_updated'] = date("Y-m-d H:i:s");
				$retur_id['id'] = $retur_obat_unit_row->id;
				$retur_obat_unit_dbtable->update($retur_data, $retur_id);
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else if ($_POST['command'] == "push_delete") {
			$retur_obat_unit_dbtable = new DBTable($db, "smis_ap_retur_obat_unit");
			$retur_obat_unit_row = $retur_obat_unit_dbtable->get_row("
				SELECT id
				FROM smis_lgs_retur_obat_unit
				WHERE f_id = '" . $_POST['id_retur_obat'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($retur_obat_unit_row != null) {
				$retur_obat_unit_id['id'] = $retur_obat_unit_row->id;
				$retur_obat_unit_data['prop'] = "del";
				$retur_obat_unit_data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
		        $retur_obat_unit_data['duplicate'] = 0;
		        $retur_obat_unit_data['time_updated'] = date("Y-m-d H:i:s");
				$retur_obat_unit_dbtable->update($retur_obat_unit_data, $retur_obat_unit_id);
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());
?>