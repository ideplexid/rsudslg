<?php
	global $db;
	
	if (isset($_POST['id_obat'])) {
		$id_obat = $_POST['id_obat'];
		$dbtable = new DBTable($db, "smis_lgs_stok_obat");
		$jumlah = 0;
		$row = $dbtable->get_row("
			SELECT SUM(a.sisa) AS 'jumlah'
			FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND a.konversi = 1 AND b.id_obat = '" . $id_obat . "'
		");
		if ($row != null)
			$jumlah += $row->jumlah;
		$row = $dbtable->get_row("
			SELECT SUM(a.sisa * a.konversi) AS 'jumlah'
			FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND a.konversi > 1 AND b.id_obat = '" . $id_obat . "'
		");
		if ($row != null)
			$jumlah += $row->jumlah;
		$data = array();
		$data['data'] = array(
			"ruangan"	=> "gudang_logistik",
			"jumlah"	=> $jumlah
		);
		echo json_encode($data);
	}
?>