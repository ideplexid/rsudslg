<?php
	global $db;

	if (isset($_POST['id_obat']) && isset($_POST['tanggal_to'])) {
		$row = $db->get_row("
			SELECT a.id_obat_f_masuk AS 'id', (a.hna / a.konversi) AS 'hpp', a.diskon AS 'diskon_detail', a.t_diskon AS 't_diskon_detail', b.diskon AS 'diskon_global', b.t_diskon AS 't_diskon_global', a.jumlah
			FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_obat_f_masuk b ON a.id_obat_f_masuk = b.id
			WHERE b.tanggal_datang <= '" . $_POST['tanggal_to'] . "' AND a.id_obat = '" . $_POST['id_obat'] . "' AND b.prop NOT LIKE 'del' AND a.prop NOT LIKE 'del' AND a.jumlah > 0
			ORDER BY b.tanggal_datang DESC, a.id DESC
			LIMIT 0, 1
		");
		$hpp_terakhir = 0;
		if ($row != null) {
			$hpp_terakhir = $row->hpp;
			if ($row->t_diskon_detail == "persen") {
			 	$diskon = $hpp_terakhir * $row->diskon_detail / 100;
			 	$hpp_terakhir -= $diskon;
			} else if ($row->t_diskon_detail == "nominal") {
			 	$diskon = $row->diskon_detail / $row->jumlah;
			 	$hpp_terakhir -= $diskon;
			}
			if ($row->t_diskon_global == "persen") {
			 	$diskon = $hpp_terakhir * $row->diskon_global / 100;
			 	$hpp_terakhir -= $diskon;
			} else if ($row->t_diskon_global == "nominal") {
			 	$total_row = $dbtable->get_row("
			 		SELECT SUM(jumlah * hna) AS 'total'
			 		FROM smis_lgs_dobat_f_masuk
			 		WHERE id_obat_f_masuk = '" . $row->id . "' AND prop NOT LIKE 'del'
			 	");
			 	$diskon = $row->diskon_global * $row->hpp / $total_row->total;
			 	$hpp_terakhir -= $diskon;
			}
		}
		$data = array(
			"hpp"		=> $hpp_terakhir
		);
		echo json_encode($data);
	}
?>