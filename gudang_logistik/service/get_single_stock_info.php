<?php
	if (isset($_POST['id_obat']) 
	 && isset($_POST['satuan']) 
     && isset($_POST['konversi']) 
	 && isset($_POST['satuan_konversi'])) {
		$dbtable = new DBTable($db, "smis_lgs_stok_obat");
		$row = $dbtable->get_row("
			SELECT SUM(a.sisa) AS 'jumlah'
			FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND b.id_obat = '" . $_POST['id_obat'] . "' AND a.satuan = '" . $_POST['satuan'] . "' AND a.konversi = '" . $_POST['konversi'] . "' AND a.satuan_konversi = '" . $_POST['satuan_konversi'] . "'
		");
		$data = array();
		$data['jumlah'] = $row->jumlah;
		if ($row->jumlah == null)
			$data['jumlah'] = 0;
		echo json_encode($data);		
	}
?>