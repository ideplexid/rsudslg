<?php
	require_once("gudang_farmasi/kelas/GudangLogistikInventory.php");
	global $db;

	if (isset($_POST['id_obat']) && isset($_POST['satuan']) && isset($_POST['konversi']) && isset($_POST['satuan_konversi']) && isset($_POST['tanggal_from']) && isset($_POST['tanggal_to'])) {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$tanggal_from = $_POST['tanggal_from'];
		$tanggal_to = $_POST['tanggal_to'];

		$data = array(
			"stok" => GudangLogistikInventory::getCurrentStock($db, $id_obat, $satuan, 1, $satuan_konversi)
		);
		echo json_encode($data);
	}
?>