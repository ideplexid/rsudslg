<?php
	global $db;

	if (isset($_POST['id_obat']) && isset($_POST['satuan'])) {
		$dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
		$max_row = $dbtable->get_row("
			SELECT MAX(id) AS 'id'
			FROM smis_lgs_dobat_f_masuk
			WHERE id_obat = '" . $_POST['id_obat'] . "' AND satuan = '" . $_POST['satuan'] . "' AND konversi = '" . $_POST['konversi'] . "' AND satuan_konversi = '" . $_POST['satuan_konversi'] . "'
		");
		$last_hpp = 0;
		if ($max_row != null) {
			$row = $dbtable->select($max_row->id);
			$last_hpp = $row->hna * $row->konversi;
		}
		$data['data'] = array(
			"hpp"	=> $last_hpp
		);
		echo json_encode($data);
	}
?>