<?php
	global $db;

	if (isset($_POST['id_obat']) && isset($_POST['satuan']) && isset($_POST['konversi']) && isset($_POST['satuan_konversi']) && isset($_POST['tanggal_from']) && isset($_POST['tanggal_to'])) {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$tanggal_from = $_POST['tanggal_from'];
		$tanggal_to = $_POST['tanggal_to'];

		// Mendapatkan Nominal Mutasi:
		$dbtable = new DBTable($db, "smis_lgs_obat_keluar");
		$data = $dbtable->get_result("
			SELECT e.id_obat, d.kode_obat, d.nama_obat, c.jumlah, d.hna AS 'hpp'
			FROM (((smis_lgs_obat_keluar a LEFT JOIN smis_lgs_dobat_keluar b ON a.id = b.id_obat_keluar) LEFT JOIN smis_lgs_stok_obat_keluar c ON b.id = c.id_dobat_keluar) LEFT JOIN smis_lgs_stok_obat d ON c.id_stok_obat = d.id) LEFT JOIN smis_lgs_dobat_f_masuk e ON d.id_dobat_masuk = e.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND e.id_obat = '" . $id_obat . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "'
		");
		$nominal_jual = 0;
		if ($data != null) {
			foreach ($data as $d) {
				$subtotal = $d->jumlah * $d->hpp;
				$nominal_jual += $subtotal;
			}
		}
		$data = array(
			"nominal"	=> $nominal_jual
		);
		echo json_encode($data);
	}
?>