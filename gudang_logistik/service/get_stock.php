<?php
	if (isset($_POST['command']) && $_POST['command'] == "list") {
		$stock_dbtable = new DBTable($db, "smis_lgs_stok_obat");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT '' AS 'id', nama_obat, nama_jenis_obat, produsen, SUM(sisa) AS 'sisa', satuan, CASE WHEN konversi = 1 THEN '-' ELSE CONCAT('1 ', satuan, ' terdiri dari ', konversi, ' ', satuan_konversi) END AS 'ket_satuan', hna
				FROM (
					SELECT *
					FROM smis_lgs_stok_obat
					WHERE prop NOT LIKE 'del' AND sisa > 0 " . $filter . "
				) v_stock
				GROUP BY nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi, hna
			) v_rekap_stock
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT '' AS 'id', nama_obat, nama_jenis_obat, produsen, SUM(sisa) AS 'sisa', satuan, CASE WHEN konversi = 1 THEN '-' ELSE CONCAT('1 ', satuan, ' terdiri dari ', konversi, ' ', satuan_konversi) END AS 'ket_satuan', hna
				FROM (
					SELECT *
					FROM smis_lgs_stok_obat
					WHERE prop NOT LIKE 'del' AND sisa > 0 " . $filter . "
				) v_stock
				GROUP BY nama_obat, nama_jenis_obat, satuan, konversi, satuan_konversi, hna
			) v_rekap_stock
		";
		$stock_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$service_provider = new ServiceProvider($stock_dbtable);
		$data = $service_provider->command($_POST['command']);
		echo json_encode($data);
	}
?>