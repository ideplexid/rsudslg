<?php
class UpdateSisaDPOServiceConsumer extends ServiceConsumer {
	public function __construct($db, $detail, $command) {
		$array['detail'] = json_encode($detail);
		$array['command'] = $command;
		parent::__construct($db, "update_sisa_dpo", $array, "pembelian");
	}
	public function proceedResult() {
		$content = array();
		$result = json_decode($this->result,true);
		foreach ($result as $autonomous) {
			foreach ($autonomous as $entity) {
				if ($entity != null || $entity != "")
					return $entity;
			}
		}
		return $content;
	}
}
?>