<?php
class UnitServiceConsumer extends ServiceConsumer {
	public function __construct($db) {
		parent::__construct($db, "is_inventory");
	}		
	public function proceedResult() {
		$content = array();
		$result = json_decode($this->result, true);
		foreach($result as $autonomous) {
			foreach($autonomous as $entity=>$res) {
				if($res['value_obat']=="1"){
					$option = array();
					$option['value'] = $entity;
					$option['name'] = ArrayAdapter::format("unslug", $res['name']);
					$content[] = $option;
				}
			}
		}
		return $content;
	}
}
?>