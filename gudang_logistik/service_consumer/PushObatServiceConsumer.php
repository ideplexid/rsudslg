<?php
class PushObatServiceConsumer extends ServiceConsumer {
	public function __construct($db, $id_obat_keluar, $tanggal, $unit, $status, $keterangan, $detail, $command) {
		$array['id_obat_keluar'] = $id_obat_keluar;
		$array['tanggal'] = $tanggal;
		$array['unit'] = "gudang_logistik";
		$array['status'] = $status;
		$array['keterangan'] = $keterangan;
		$array['detail'] = json_encode($detail);
		$array['command'] = $command;
		parent::__construct($db, "push_obat", $array, $unit);
	}
	public function proceedResult() {
		$content = array();
		$result = json_decode($this->result,true);
		foreach ($result as $autonomous) {
			foreach ($autonomous as $entity) {
				if ($entity != null || $entity != "")
					return $entity;
			}
		}
		return $content;
	}
}
?>