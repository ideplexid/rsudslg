<?php
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");
	
	class PemusnahanObatTable extends Table {
		public function getBodyContent() {
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['dibatalkan'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $dibatalkan) {
			$btn_group = new ButtonGroup("noprint");
			if ($dibatalkan) {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Batal");
				$btn->setAction($this->action . ".cancel('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Batal' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
	$pemusnahan_obat_table = new PemusnahanObatTable(
		array("Nomor", "Tanggal", "Petugas", "Keterangan"),
		"Gudang Logistik : Pemusnahan Obat",
		null,
		true
	);
	$pemusnahan_obat_table->setName("pemusnahan_obat");
	
	//chooser karyawan:
	$karyawan_table = new Table(
		array("NIP", "Nama", "Jabatan"),
		"",
		null,
		true
	);
	$karyawan_table->setName("karyawan");
	$karyawan_table->setModel(Table::$SELECT);
	$karyawan_adapter = new SimpleAdapter();
	$karyawan_adapter->add("NIP", "nip");
	$karyawan_adapter->add("Nama", "nama");
	$karyawan_adapter->add("Jabatan", "nama_jabatan");
	$karyawan_service_responder = new ServiceResponder(
		$db,
		$karyawan_table,
		$karyawan_adapter,
		"employee"
	);
	
	//chooser obat:
	$obat_table = new Table(
		array("No. Stok", "Nama Obat", "Jenis Obat", "Produsen", "Sisa", "Satuan", "Tgl. Exp."),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("No. Stok", "id", "digit8");
	$obat_adapter->add("Nama Obat", "nama_obat");
	$obat_adapter->add("Jenis Obat", "nama_jenis_obat");
	$obat_adapter->add("Produsen", "produsen");
	$obat_adapter->add("Sisa", "sisa");
	$obat_adapter->add("Satuan", "satuan");
	$obat_adapter->add("Tgl. Exp.", "tanggal_exp", "date d M Y");
	$obat_dbtable = new DBTable($db, "smis_lgs_stok_obat");
	$obat_dbtable->addCustomKriteria(" sisa ", " > 0 ");
	$obat_dbtable->setOrder(" nama_obat, nama_jenis_obat ASC ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("karyawan", $karyawan_service_responder);
	$super_command->addResponder("obat", $obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		class PemusnahanObatAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['dibatalkan'] = $row->dibatalkan;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
				$array['Petugas'] = $row->nama_karyawan;
				$array['Keterangan'] = $row->keterangan;
				return $array;
			}
		}
		$pemusnahan_obat_adapter = new PemusnahanObatAdapter();
		$pemusnahan_obat_dbtable = new DBTable(
			$db,
			"smis_lgs_pemusnahan_obat"
		);
		class PemusnahanObatDBResponder extends DBResponder {
			public function save() {
				$header_data = $this->postToArray();
				$id['id'] = $_POST['id'];
				if ($id['id'] == 0 || $id['id'] == "") {
					//do insert header here:
					$result = $this->dbtable->insert($header_data);
					$id['id'] = $this->dbtable->get_inserted_id();
					$success['type'] = "insert";
					if (isset($_POST['detail'])) {
						//do insert detail here:
						$dpemusnahan_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_dpemusnahan_obat");
						$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
						$detail = $_POST['detail'];
						foreach($detail as $d) {
							$dpemusnahan_data = array();
							$dpemusnahan_data['id_pemusnahan_obat'] = $id['id'];
							$dpemusnahan_data['id_stok_obat'] = $d['id_stok_obat'];
							$dpemusnahan_data['jumlah'] = $d['jumlah'];
							$dpemusnahan_data['keterangan'] = $d['keterangan'];
							$dpemusnahan_obat_dbtable->insert($dpemusnahan_data);
							$stok_row = $stok_obat_dbtable->get_row("
								SELECT id, sisa
								FROM smis_lgs_stok_obat
								WHERE id = '" . $d['id_stok_obat'] . "'
							");
							$stok_data = array();
							$stok_data['sisa'] = $stok_row->sisa - $d['jumlah'];
							$stok_id['id'] = $d['id_stok_obat'];
							$stok_obat_dbtable->update($stok_data, $stok_id);
							//logging riwayat stok obat:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $d['id_stok_obat'];
							$data_riwayat['jumlah_keluar'] = $d['jumlah'];
							$data_riwayat['sisa'] = $stok_row->sisa - $d['jumlah'];
							$data_riwayat['keterangan'] = "Dimusnahkan";
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
						}
					}
				} else {
					//do update header here:
					$result = $this->dbtable->update($header_data, $id);
					$success['type'] = "update";
					if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
						$detail_rows = $this->dbtable->get_result("
							SELECT *
							FROM smis_lgs_dpemusnahan_obat
							WHERE id_pemusnahan_obat = '" . $id['id'] . "' AND prop NOT LIKE 'del'
						");
						$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
						foreach($detail_rows as $dr) {
							$stok_row = $stok_obat_dbtable->get_row("
								SELECT id, sisa
								FROM smis_lgs_stok_obat
								WHERE id = '" . $dr->id_stok_obat . "'
							");
							$stok_data = array();
							$stok_data['sisa'] = $stok_row->sisa + $dr->jumlah;
							$stok_id['id'] = $dr->id_stok_obat;
							$stok_obat_dbtable->update($stok_data, $stok_id);
							//logging riwayat stok obat:
							$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_riwayat_stok_obat");
							$data_riwayat = array();
							$data_riwayat['tanggal'] = date("Y-m-d");
							$data_riwayat['id_stok_obat'] = $dr->id_stok_obat;
							$data_riwayat['jumlah_masuk'] = $dr->jumlah;
							$data_riwayat['sisa'] = $stok_row->sisa + $dr->jumlah;
							$data_riwayat['keterangan'] = "Batal Dimusnahkan";
							global $user;
							$data_riwayat['nama_user'] = $user->getName();
							$riwayat_dbtable->insert($data_riwayat);
						}
					}
				}
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
			public function edit() {
				$id = $_POST['id'];
				$data['header'] = $this->dbtable->get_row("
					SELECT *
					FROM smis_lgs_pemusnahan_obat
					WHERE id = '" . $id . "'
				");
				$detail_rows = $this->dbtable->get_result("
					SELECT smis_lgs_dpemusnahan_obat.*, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat.nama_jenis_obat, smis_lgs_stok_obat.tanggal_exp, smis_lgs_stok_obat.produsen, smis_lgs_stok_obat.satuan, smis_lgs_stok_obat.konversi, smis_lgs_stok_obat.satuan_konversi
					FROM smis_lgs_dpemusnahan_obat LEFT JOIN smis_lgs_stok_obat ON smis_lgs_dpemusnahan_obat.id_stok_obat = smis_lgs_stok_obat.id
					WHERE smis_lgs_dpemusnahan_obat.id_pemusnahan_obat = '" . $id . "'
				");
				$detail_list = "";
				$row_id = 0;
				foreach($detail_rows as $dr) {
					$f_tanggal_exp = "";
					if ($dr->tanggal_exp == "0000-00-00")
						$f_tanggal_exp = "-";
					else
						$f_tanggal_exp = ArrayAdapter::format("date d-m-Y", $dr->tanggal_exp);
					$detail_list .= "<tr id='detail_" . $row_id . "'>" .
										"<td id='detail_" . $row_id . "_id' style='display: none;'>" . $dr->id . "</td>" .
										"<td id='detail_" . $row_id . "_id_stok_obat' style='display: none;'>" . $dr->id_stok_obat . "</td>" .
										"<td id='detail_" . $row_id . "_jumlah_dimusnahkan' style='display: none;'>" . $dr->jumlah . "</td>" .
										"<td id='detail_" . $row_id . "_satuan' style='display: none;'>" . $dr->satuan . "</td>" .
										"<td id='detail_" . $row_id . "_konversi' style='display: none;'>" . $dr->konversi . "</td>" .
										"<td id='detail_" . $row_id . "_satuan_konversi' style='display: none;'>" . $dr->satuan_konversi . "</td>" .
										"<td id='detail_" . $row_id . "_tanggal_exp' style='display: none;'>" . $dr->tanggal_exp . "</td>" .
										"<td id='detail_" . $row_id . "_nama_obat'>" . $dr->nama_obat . "</td>" .
										"<td id='detail_" . $row_id . "_nama_jenis_obat'>" . $dr->nama_jenis_obat . "</td>" .
										"<td id='detail_" . $row_id . "_produsen'>" . $dr->produsen . "</td>" .
										"<td id='detail_" . $row_id . "_f_tanggal_exp'>" . $f_tanggal_exp . "</td>" .
										"<td id='detail_" . $row_id . "_f_jumlah'>" . $dr->jumlah . " " . $dr->satuan . "</td>" .
										"<td id='detail_" . $row_id . "_keterangan'>" . $dr->keterangan . "</td>" .
										"<td></td>" .
									"</tr>";
					$row_id++;
				}
				$data['detail_list'] = $detail_list;
				return $data;
			}
		}
		$pemusnahan_obat_dbresponder = new PemusnahanObatDBResponder(
			$pemusnahan_obat_dbtable,
			$pemusnahan_obat_table,
			$pemusnahan_obat_adapter
		);
		$data = $pemusnahan_obat_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$pemusnahan_obat_modal = new Modal("pemusnahan_obat_add_form", "smis_form_container", "pemusnahan_obat");
	$pemusnahan_obat_modal->setTitle("Data Pemusnahan Obat");
	$pemusnahan_obat_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("pemusnahan_obat_id", "pemusnahan_obat_id", "");
	$pemusnahan_obat_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("pemusnahan_obat_tanggal", "pemusnahan_obat_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$pemusnahan_obat_modal->addElement("Tanggal", $tanggal_text);
	$id_karyawan_hidden = new Hidden("pemusnahan_obat_id_karyawan", "pemusnahan_obat_id_karyawan", "");
	$pemusnahan_obat_modal->addElement("", $id_karyawan_hidden);
	$karyawan_button = new Button("", "", "Pilih");
	$karyawan_button->setClass("btn-info");
	$karyawan_button->setIsButton(Button::$ICONIC);
	$karyawan_button->setIcon("icon-white ".Button::$icon_list_alt);
	$karyawan_button->setAction("karyawan.chooser('karyawan', 'karyawan_button', 'karyawan', karyawan)");
	$karyawan_button->setAtribute("id='karyawan_browse'");
	$karyawan_text = new Text("pemusnahan_obat_nama_karyawan", "pemusnahan_obat_nama_karyawan", "");
	$karyawan_text->setAtribute("disabled='disabled'");
	$karyawan_text->setClass("smis-one-option-input");
	$karyawan_input_group = new InputGroup("");
	$karyawan_input_group->addComponent($karyawan_text);
	$karyawan_input_group->addComponent($karyawan_button);
	$pemusnahan_obat_modal->addElement("Karyawan", $karyawan_input_group);
	$keterangan_textarea = new TextArea("pemusnahan_obat_keterangan", "pemusnahan_obat_keterangan", "");
	$keterangan_textarea->setLine(2);
	$pemusnahan_obat_modal->addElement("Keterangan", $keterangan_textarea);
	class DPemusnahanObatTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Tambah");
			$btn->setAction($this->action . ".show_add_form()");
			$btn->setClass("btn-primary");
			$btn->setAtribute("id='dpemusnahan_obat_add'");
			$btn->setIcon("icon-plus icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$dpemusnahan_obat_table = new DPemusnahanObatTable(
		array("Nama Obat", "Jenis Obat", "Produsen", "Tgl. Exp.", "Jumlah", "Keterangan"),
		"",
		null,
		true
	);
	$dpemusnahan_obat_table->setName("dpemusnahan_obat");
	$dpemusnahan_obat_table->setPrintButtonEnable(false);
	$dpemusnahan_obat_table->setReloadButtonEnable(false);
	$dpemusnahan_obat_table->setFooterVisible(false);
	$pemusnahan_obat_modal->addBody("dpemusnahan_obat_table", $dpemusnahan_obat_table);
	$pemusnahan_obat_button = new Button("", "", "Simpan");
	$pemusnahan_obat_button->setClass("btn-success");
	$pemusnahan_obat_button->setIcon("fa fa-floppy-o");
	$pemusnahan_obat_button->setIsButton(Button::$ICONIC);
	$pemusnahan_obat_button->setAtribute("id='pemusnahan_obat_save'");
	$pemusnahan_obat_modal->addFooter($pemusnahan_obat_button);
	$pemusnahan_obat_button = new Button("", "", "OK");
	$pemusnahan_obat_button->setClass("btn-success");
	$pemusnahan_obat_button->setAtribute("id='pemusnahan_obat_ok'");
	$pemusnahan_obat_button->setAction("$($(this).data('target')).smodal('hide')");
	$pemusnahan_obat_modal->addFooter($pemusnahan_obat_button);
	
	$dpemusnahan_obat_modal = new Modal("dpemusnahan_obat_add_form", "smis_form_container", "dpemusnahan_obat");
	$dpemusnahan_obat_modal->setTitle("Data Detail Pemusnahan Obat");
	$id_stok_obat_hidden = new Hidden("dpemusnahan_obat_id_stok_obat", "dpemusnahan_obat_id_stok_obat", "");
	$dpemusnahan_obat_modal->addElement("", $id_stok_obat_hidden);
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setIcon("icon-white ".Button::$icon_list_alt);
	$obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$obat_button->setAtribute("id='obat_browse'");
	$obat_text = new Text("dpemusnahan_obat_nama_obat", "dpemusnahan_obat_nama_obat", "");
	$obat_text->setAtribute("disabled='disabled'");
	$obat_text->setClass("smis-one-option-input");
	$obat_input_group = new InputGroup("");
	$obat_input_group->addComponent($obat_text);
	$obat_input_group->addComponent($obat_button);
	$dpemusnahan_obat_modal->addElement("Obat", $obat_input_group);
	$nama_jenis_obat_text = new Text("dpemusnahan_obat_nama_jenis_obat", "dpemusnahan_obat_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$dpemusnahan_obat_modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	$produsen_text = new Text("dpemusnahan_obat_produsen", "dpemusnahan_obat_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$dpemusnahan_obat_modal->addElement("Produsen", $produsen_text);
	$tanggal_exp_text = new Text("dpemusnahan_obat_tanggal_exp", "dpemusnahan_obat_tanggal_exp", "");
	$tanggal_exp_text->setAtribute("disabled='disabled'");
	$dpemusnahan_obat_modal->addElement("Tgl. Exp.", $tanggal_exp_text);
	$sisa_hidden = new Hidden("dpemusnahan_obat_sisa", "dpemusnahan_obat_sisa", "");
	$dpemusnahan_obat_modal->addElement("", $sisa_hidden);
	$konversi_hidden = new Hidden("dpemusnahan_obat_konversi", "dpemusnahan_obat_konversi", "");
	$dpemusnahan_obat_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("dpemusnahan_obat_satuan_konversi", "dpemusnahan_obat_satuan_konversi", "");
	$dpemusnahan_obat_modal->addElement("", $satuan_konversi_hidden);
	$f_sisa_text = new Text("dpemusnahan_obat_f_sisa", "dpemusnahan_obat_f_sisa", "");
	$f_sisa_text->setAtribute("disabled='disabled'");
	$dpemusnahan_obat_modal->addElement("Sisa", $f_sisa_text);
	$jumlah_text = new Text("dpemusnahan_obat_jumlah", "dpemusnahan_obat_jumlah", "");
	$dpemusnahan_obat_modal->addElement("Jumlah", $jumlah_text);
	$satuan_text = new Text("dpemusnahan_obat_satuan", "dpemusnahan_obat_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$dpemusnahan_obat_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("dpemusnahan_obat_keterangan", "dpemusnahan_obat_keterangan", "");
	$keterangan_textarea->setLine(2);
	$dpemusnahan_obat_modal->addElement("Keterangan", $keterangan_textarea);
	$dpemusnahan_obat_button = new Button("", "", "Simpan");
	$dpemusnahan_obat_button->setClass("btn-success");
	$dpemusnahan_obat_button->setIcon("fa fa-floppy-o");
	$dpemusnahan_obat_button->setIsButton(Button::$ICONIC);
	$dpemusnahan_obat_button->setAtribute("id='dpemusnahan_obat_save'");
	$dpemusnahan_obat_modal->addFooter($dpemusnahan_obat_button);
	
	echo $dpemusnahan_obat_modal->getHtml();
	echo $pemusnahan_obat_modal->getHtml();
	echo $pemusnahan_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function PemusnahanObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PemusnahanObatAction.prototype.constructor = PemusnahanObatAction;
	PemusnahanObatAction.prototype = new TableAction();
	PemusnahanObatAction.prototype.show_add_form = function() {
		$("#pemusnahan_obat_id").val("");
		$("#pemusnahan_obat_tanggal").val("");
		$("#pemusnahan_obat_tanggal").removeAttr("disabled");
		$("#pemusnahan_obat_id_karyawan").val("");
		$("#pemusnahan_obat_nama_karyawan").val("");
		$("#karyawan_browse").removeAttr("onclick");
		$("#karyawan_browse").attr("onclick", "karyawan.chooser('karyawan', 'karyawan_button', 'karyawan', karyawan)");
		$("#karyawan_browse").removeClass("btn-info");
		$("#karyawan_browse").removeClass("btn-inverse");
		$("#karyawan_browse").addClass("btn-info");
		$("#pemusnahan_obat_keterangan").val("");
		$("#pemusnahan_obat_keterangan").removeAttr("disabled");
		$("#dpemusnahan_obat_add").show();
		$("#dpemusnahan_obat_list").children().remove();
		$("#pemusnahan_obat_ok").hide();
		$("#pemusnahan_obat_save").show();
		$("#pemusnahan_obat_save").removeAttr("onclick");
		$("#pemusnahan_obat_save").attr("onclick", "pemusnahan_obat.save()");
		$("#pemusnahan_obat_add_form").smodal("show");
		row_id = 0;
	};
	PemusnahanObatAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var tanggal = $("#pemusnahan_obat_tanggal").val();
		var id_karyawan = $("#pemusnahan_obat_id_karyawan").val();
		var keterangan = $("#pemusnahan_obat_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
			$("#pemusnahan_obat_tanggal").addClass("error_field");
		}
		if (id_karyawan == "") {
			valid = false;
			invalid_msg += "</br><strong>Karyawan</strong> tidak boleh kosong";
			$("#pemusnahan_obat_id_karyawan").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#pemusnahan_obat_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_pemusnahan_obat_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	PemusnahanObatAction.prototype.save = function() {
		if(!this.validate())
			return;
		$("#pemusnahan_obat_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = "";
		data['tanggal'] = $("#pemusnahan_obat_tanggal").val();
		data['id_karyawan'] = $("#pemusnahan_obat_id_karyawan").val();
		data['nama_karyawan'] = $("#pemusnahan_obat_nama_karyawan").val();
		data['keterangan'] = $("#pemusnahan_obat_keterangan").val();
		var detail = {};
		var nor = $("tbody#dpemusnahan_obat_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#dpemusnahan_obat_list").children('tr').eq(i).prop("id");
			d_data['id_stok_obat'] = $("#" + prefix + "_id_stok_obat").text();
			d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
			d_data['keterangan'] = $("#" + prefix + "_keterangan").text();
			detail[i] = d_data;
		}
		data['detail'] = detail;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) { 
					$("#pemusnahan_obat_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	PemusnahanObatAction.prototype.detail = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(	
			"",
			data,
			function (response) {
				var json = getContent(response);
				if (json == null) return;
				$("#pemusnahan_obat_id").val(id);
				$("#pemusnahan_obat_tanggal").val(json.header.tanggal);
				$("#pemusnahan_obat_tanggal").removeAttr("disabled");
				$("#pemusnahan_obat_tanggal").attr("disabled", "disabled");
				$("#pemusnahan_obat_id_karyawan").val(json.header.id_karyawan);
				$("#pemusnahan_obat_id_karyawan").removeAttr("disabled");
				$("#pemusnahan_obat_id_karyawan").attr("disabled", "disabled");
				$("#pemusnahan_obat_nama_karyawan").val(json.header.nama_karyawan);
				$("#pemusnahan_obat_nama_karyawan").removeAttr("disabled");
				$("#pemusnahan_obat_nama_karyawan").attr("disabled", "disabled");
				$("#karyawan_browse").removeAttr("onclick");
				$("#karyawan_browse").removeClass("btn-info");
				$("#karyawan_browse").removeClass("btn-inverse");
				$("#karyawan_browse").addClass("btn-inverse");
				$("#pemusnahan_obat_keterangan").val(json.header.keterangan);
				$("#pemusnahan_obat_keterangan").removeAttr("disabled");
				$("#pemusnahan_obat_keterangan").attr("disabled", "disabled");
				$("#dpemusnahan_obat_add").hide();
				$("#dpemusnahan_obat_list").html(json.detail_list);
				$("#modal_alert_pemusnahan_obat_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#pemusnahan_obat_save").removeAttr("onclick");
				$("#pemusnahan_obat_save").hide();
				$("#pemusnahan_obat_ok").show();
				$("#pemusnahan_obat_add_form").smodal("show");
			}
		);
	};
	PemusnahanObatAction.prototype.cancel = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = id;
		data['dibatalkan'] = "1";
		bootbox.confirm(
			"Yakin membatalkan Pemusnahan Obat ini?",
			function(result) {
				if (result) {
					showLoading();
					$.post(	
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	
	function DPemusnahanObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPemusnahanObatAction.prototype.constructor = DPemusnahanObatAction;
	DPemusnahanObatAction.prototype = new TableAction();
	DPemusnahanObatAction.prototype.show_add_form = function() {
		$("#dpemusnahan_obat_id_stok_obat").val("");
		$("#dpemusnahan_obat_nama_obat").val("");
		$("#dpemusnahan_obat_nama_jenis_obat").val("");
		$("#dpemusnahan_obat_produsen").val("");
		$("#dpemusnahan_obat_tanggal_exp").val("");
		$("#dpemusnahan_obat_sisa").val("");
		$("#dpemusnahan_obat_f_sisa").val("");
		$("#dpemusnahan_obat_konversi").val("");
		$("#dpemusnahan_obat_satuan_konversi").val("");
		$("#dpemusnahan_obat_jumlah").val("");
		$("#dpemusnahan_obat_satuan").val("");
		$("#dpemusnahan_obat_keterangan").val("");
		$("#dpemusnahan_obat_save").removeAttr("onclick");
		$("#dpemusnahan_obat_save").attr("onclick", "dpemusnahan_obat.save()");
		$("#dpemusnahan_obat_add_form").smodal("show");
	};
	DPemusnahanObatAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var id_stok_obat = $("#dpemusnahan_obat_id_stok_obat").val();
		var jumlah = $("#dpemusnahan_obat_jumlah").val();
		var sisa = $("#dpemusnahan_obat_sisa").val();
		var keterangan = $("#dpemusnahan_obat_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (id_stok_obat == "") {
			valid = false;
			invalid_msg += "</br><strong>Obat</strong> tidak boleh kosong";
			$("#dpemusnahan_obat_nama_obat").addClass("error_field");
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#dpemusnahan_obat_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#dpemusnahan_obat_jumlah").addClass("error_field");
		} else if (sisa != "" && is_numeric(sisa) && parseFloat(jumlah) > parseFloat(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi <strong>Sisa</strong>";
			$("#dpemusnahan_obat_jumlah").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#dpemusnahan_obat_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_dpemusnahan_obat_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DPemusnahanObatAction.prototype.save = function() {
		if (!this.validate())
			return;
		var id = "";
		var id_stok_obat = $("#dpemusnahan_obat_id_stok_obat").val();
		var nama_obat = $("#dpemusnahan_obat_nama_obat").val();
		var nama_jenis_obat = $("#dpemusnahan_obat_nama_jenis_obat").val();
		var produsen = $("#dpemusnahan_obat_produsen").val();
		var tanggal_exp = $("#dpemusnahan_obat_tanggal_exp").val();
		var konversi = $("#dpemusnahan_obat_konversi").val();
		var satuan_konversi = $("#dpemusnahan_obat_satuan_konversi").val();
		var jumlah = $("#dpemusnahan_obat_jumlah").val();
		var satuan = $("#dpemusnahan_obat_satuan").val();
		var keterangan = $("#dpemusnahan_obat_keterangan").val();
		$("tbody#dpemusnahan_obat_list").append(
			"<tr id='data_" + row_id + "'>" +
				"<td id='data_" + row_id + "_id' style='display: none;'>" + id + "</td>" +
				"<td id='data_" + row_id + "_id_stok_obat' style='display: none;'>" + id_stok_obat + "</td>" +
				"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + jumlah + "</td>" +
				"<td id='data_" + row_id + "_satuan' style='display: none;'>" + satuan + "</td>" +
				"<td id='data_" + row_id + "_konversi' style='display: none;'>" + konversi + "</td>" +
				"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
				"<td id='data_" + row_id + "_nama_obat'>" + nama_obat + "</td>" +
				"<td id='data_" + row_id + "_nama_jenis_obat'>" + nama_jenis_obat + "</td>" +
				"<td id='data_" + row_id + "_produsen'>" + produsen + "</td>" +
				"<td id='data_" + row_id + "_tanggal_exp'>" + tanggal_exp + "</td>" +
				"<td id='data_" + row_id + "_f_jumlah'>" + jumlah + " " + satuan + "</td>" +
				"<td id='data_" + row_id + "_keterangan'>" + keterangan + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='dpemusnahan_obat.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
							"<i class='icon-edit icon-white'></i>" +
						"</a>" +
						"<a href='#' onclick='dpemusnahan_obat.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		row_id++;
		$("#dpemusnahan_obat_add_form").smodal("hide");
 	};
	DPemusnahanObatAction.prototype.edit = function(r_num) {
		var dpemusnahan_obat_id_stok_obat = $("#data_" + r_num + "_id_stok_obat").text();
		var dpemusnahan_obat_nama_obat = $("#data_" + r_num + "_nama_obat").text();
		var dpemusnahan_obat_nama_jenis_obat = $("#data_" + r_num + "_nama_jenis_obat").text();
		var dpemusnahan_obat_jumlah = $("#data_" + r_num + "_jumlah").text();
		var dpemusnahan_obat_satuan = $("#data_" + r_num + "_satuan").text();
		var dpemusnahan_obat_konversi = $("#data_" + r_num + "_konversi").text();
		var dpemusnahan_obat_satuan_konversi = $("#data_" + r_num + "_satuan_konversi").text();
		var dpemusnahan_obat_produsen = $("#data_" + r_num + "_produsen").text();
		var dpemusnahan_obat_tanggal_exp = $("#data_" + r_num + "_tanggal_exp").text();
		var dpemusnahan_obat_keterangan = $("#data_" + r_num + "_keterangan").text();
		$("#dpemusnahan_obat_id_stok_obat").val(dpemusnahan_obat_id_stok_obat);
		$("#dpemusnahan_obat_nama_obat").val(dpemusnahan_obat_nama_obat);
		$("#dpemusnahan_obat_nama_jenis_obat").val(dpemusnahan_obat_nama_jenis_obat);
		$("#dpemusnahan_obat_jumlah").val(dpemusnahan_obat_jumlah);
		$("#dpemusnahan_obat_satuan").val(dpemusnahan_obat_satuan);
		$("#dpemusnahan_obat_konversi").val(dpemusnahan_obat_konversi);
		$("#dpemusnahan_obat_satuan_konversi").val(dpemusnahan_obat_satuan_konversi);
		$("#dpemusnahan_obat_produsen").val(dpemusnahan_obat_produsen);
		$("#dpemusnahan_obat_tanggal_exp").val(dpemusnahan_obat_tanggal_exp);
		$("#dpemusnahan_obat_keterangan").val(dpemusnahan_obat_keterangan);
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "obat";
		data['command'] = "edit";
		data['id'] = dpemusnahan_obat_id_stok_obat;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				console.log(json);
				$("#dpemusnahan_obat_sisa").val(json.sisa);
				$("#dpemusnahan_obat_f_sisa").val(json.sisa + " " + json.satuan);
				$("#modal_alert_dpemusnahan_obat_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#dpemusnahan_obat_save").removeAttr("onclick");
				$("#dpemusnahan_obat_save").attr("onclick", "dpemusnahan_obat.update(" + r_num + ")");
				$("#dpemusnahan_obat_add_form").smodal("show");
			}
		);
	};
	DPemusnahanObatAction.prototype.update = function(r_num) {
		if(!this.validate())
			return;
		$("#data_" + r_num + "_id_stok_obat").html($("#dpemusnahan_obat_id_stok_obat").val());
		$("#data_" + r_num + "_nama_obat").html($("#dpemusnahan_obat_nama_obat").val());
		$("#data_" + r_num + "_nama_jenis_obat").html($("#dpemusnahan_obat_nama_jenis_obat").val());
		$("#data_" + r_num + "_jumlah").html($("#dpemusnahan_obat_jumlah").val());
		$("#data_" + r_num + "_satuan").html($("#dpemusnahan_obat_satuan").val());
		$("#data_" + r_num + "_konversi").html($("#dpemusnahan_obat_konversi").val());
		$("#data_" + r_num + "_satuan_konversi").html($("#dpemusnahan_obat_satuan_konversi").val());
		$("#data_" + r_num + "_produsen").html($("#dpemusnahan_obat_produsen").val());
		$("#data_" + r_num + "_tanggal_exp").html($("#dpemusnahan_obat_tanggal_exp").val());
		$("#dpemusnahan_obat_add_form").smodal("hide");
	};
	DPemusnahanObatAction.prototype.delete = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		if (id.length == 0) {
			$("#data_" + r_num).remove();
		} else {
			$("#data_" + r_num).attr("style", "display: none;");
			$("#data_" + r_num).attr("class", "deleted");
		}
	};
	
	function KaryawanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	KaryawanAction.prototype.constructor = KaryawanAction;
	KaryawanAction.prototype = new TableAction();
	KaryawanAction.prototype.selected = function(json) {
		$("#pemusnahan_obat_id_karyawan").val(json.id);
		$("#pemusnahan_obat_nama_karyawan").val(json.nama);
	};
	
	function ObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ObatAction.prototype.constructor = ObatAction;
	ObatAction.prototype = new TableAction();
	ObatAction.prototype.selected = function(json) {
		$("#dpemusnahan_obat_id_stok_obat").val(json.id);
		$("#dpemusnahan_obat_nama_obat").val(json.nama_obat);
		$("#dpemusnahan_obat_nama_jenis_obat").val(json.nama_jenis_obat);
		$("#dpemusnahan_obat_produsen").val(json.produsen);
		$("#dpemusnahan_obat_tanggal_exp").val(json.tanggal_exp);
		$("#dpemusnahan_obat_sisa").val(json.sisa);
		$("#dpemusnahan_obat_f_sisa").val(json.sisa + " " + json.satuan);
		$("#dpemusnahan_obat_konversi").val(json.konversi);
		$("#dpemusnahan_obat_satuan_konversi").val(json.satuan_konversi);
		$("#dpemusnahan_obat_satuan").val(json.satuan);
	};
	
	var pemusnahan_obat;
	var dpemusnahan_obat;
	var karyawan;
	var obat;
	var row_id;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "OBAT" || 
				$("#smis-chooser-modal .modal-header h3").text() == "BAHAN") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("full_model");
			} else {
				$("#smis-chooser-modal").removeClass("full_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
		});
		karyawan = new KaryawanAction(
			"karyawan",
			"gudang_logistik",
			"pemusnahan_obat",
			new Array()
		);
		karyawan.setSuperCommand("karyawan");
		obat = new ObatAction(
			"obat",
			"gudang_logistik",
			"pemusnahan_obat",
			new Array()
		);
		obat.setSuperCommand("obat");
		var dpemusnahan_obat_columns = new Array("id", "id_pemusnahan_obat", "id_stok_obat", "jumlah", "keterangan");
		dpemusnahan_obat = new DPemusnahanObatAction(
			"dpemusnahan_obat",
			"gudang_logistik",
			"pemusnahan_obat",
			dpemusnahan_obat_columns
		);
		var pemusnahan_obat_columns = new Array("id", "tanggal", "id_karyawan", "nama_karyawan", "keterangan");
		pemusnahan_obat = new PemusnahanObatAction(
			"pemusnahan_obat",
			"gudang_logistik",
			"pemusnahan_obat",
			pemusnahan_obat_columns
		);
		pemusnahan_obat.view();
	});
</script>