<?php 
	$riwayat_penyesuaian_stok_form = new Form("form_riwayat_penyesuaian_stok", "", "");
	$tanggal_from_text = new Text("riwayat_penyesuaian_stok_tanggal_from", "riwayat_penyesuaian_stok_tanggal_from", date('Y-m-') . "01");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-m-d'");
	$tanggal_from_text->setClass("mydate");
	$riwayat_penyesuaian_stok_form->addElement("Tanggal Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("riwayat_penyesuaian_stok_tanggal_to", "riwayat_penyesuaian_stok_tanggal_to", date('Y-m-d'));
	$tanggal_to_text->setAtribute("data-date-format='yyyy-m-d'");
	$tanggal_to_text->setClass("mydate");
	$riwayat_penyesuaian_stok_form->addElement("Tanggal Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setisButton(Button::$ICONIC);
	$show_button->setAction("riwayat_penyesuaian_stok.view()");
	$riwayat_penyesuaian_stok_form->addElement("", $show_button);
	
	$riwayat_penyesuaian_stok_table = new Table(
		array("Tanggal", "Nama Obat", "Jenis Obat", "Jenis Stok", "Produsen", "Vendor", "Jml. Awal", "Jml. Aktual", "Selisih", "Keterangan", "Petugas Entri"),
		"",
		null,
		true
	);
	$riwayat_penyesuaian_stok_table->setName("riwayat_penyesuaian_stok");
	$riwayat_penyesuaian_stok_table->setAddButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setReloadButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setPrintButtonEnable(false);
	$riwayat_penyesuaian_stok_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class RiwayatPenyesuaianStokAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Jenis Stok'] = self::format("unslug", $row->label);
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jml. Awal'] = $row->jumlah_lama . " " . $row->satuan;
				$array['Jml. Aktual'] = $row->jumlah_baru . " " . $row->satuan;
				$sign = "";
				if ($row->jumlah_baru - $row->jumlah_lama > 0) {
					$sign = "+";
				}
				$array['Selisih'] = $sign . ($row->jumlah_baru - $row->jumlah_lama) . " " . $row->satuan;
				$array['Keterangan'] = $row->keterangan;
				$array['Petugas Entri'] = $row->nama_user;
				return $array;
			}
		}
		$riwayat_penyesuaian_stok_adapter = new RiwayatPenyesuaianStokAdapter();
		$riwayat_penyesuaian_stok_dbtable = new DBTable(
			$db,
			"smis_lgs_penyesuaian_stok"
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_lgs_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_lgs_stok_obat.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_lgs_penyesuaian_stok.nama_user LIKE '%" . $_POST['kriteria'] . "%' OR smis_lgs_stok_obat.produsen LIKE '%" . $_POST['kriteria'] . "%' OR smis_lgs_stok_obat.nama_vendor LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM  (
				SELECT smis_lgs_penyesuaian_stok.*, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat.nama_jenis_obat, smis_lgs_stok_obat.label, smis_lgs_stok_obat.produsen, smis_lgs_stok_obat.nama_vendor, smis_lgs_stok_obat.satuan
				FROM smis_lgs_penyesuaian_stok LEFT JOIN smis_lgs_stok_obat ON smis_lgs_penyesuaian_stok.id_stok_obat = smis_lgs_stok_obat.id
				WHERE smis_lgs_penyesuaian_stok.prop NOT LIKE 'del' AND smis_lgs_penyesuaian_stok.tanggal >= '" . $_POST['tanggal_from'] . "' AND smis_lgs_penyesuaian_stok.tanggal <= '" . $_POST['tanggal_to'] . "' " . $filter . "
			) v_riwayat_penyesuaian_stok
 		";
		$query_count = "
			SELECT COUNT(*)
			FROM  (
				" . $query_value . "
			) v_riwayat_penyesuaian_stok
		";
		$riwayat_penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$riwayat_penyesuaian_stok_dbresponder = new DBResponder(
			$riwayat_penyesuaian_stok_dbtable,
			$riwayat_penyesuaian_stok_table,
			$riwayat_penyesuaian_stok_adapter
		);
		$data = $riwayat_penyesuaian_stok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $riwayat_penyesuaian_stok_form->getHtml();
	echo "<div id'table_content'>";
	echo $riwayat_penyesuaian_stok_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function RiwayatPenyesuaianStokAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	RiwayatPenyesuaianStokAction.prototype.constructor = RiwayatPenyesuaianStokAction;
	RiwayatPenyesuaianStokAction.prototype = new TableAction();
	RiwayatPenyesuaianStokAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['tanggal_from'] = $("#riwayat_penyesuaian_stok_tanggal_from").val();
		data['tanggal_to'] = $("#riwayat_penyesuaian_stok_tanggal_to").val();
		return data;
	};
	
	var riwayat_penyesuaian_stok;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#riwayat_penyesuaian_stok_max").val(50);
		riwayat_penyesuaian_stok = new RiwayatPenyesuaianStokAction(
			"riwayat_penyesuaian_stok", 
			"gudang_logistik", 
			"riwayat_penyesuaian_stok_obat",
			new Array()
		);
		riwayat_penyesuaian_stok.view();
	});
</script>