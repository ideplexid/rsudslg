<?php 	
	class ReturKeluarTable extends Table {
		public function getBodyContent() {
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['dibatalkan'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $dibatalkan) {
			$btn_group = new ButtonGroup("noprint");
			if ($dibatalkan) {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Batal");
				$btn->setAction($this->action . ".cancel('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Batal' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
	$retur_keluar_table = new ReturKeluarTable(
		array("Nomor", "Tanggal", "Jenis Stok", "Vendor", "No. Faktur", "Tgl. Faktur", "Status"),
		"Gudang Logistik : Retur Pembelian Obat",
		null,
		true
	);
	$retur_keluar_table->setName("retur");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "retur") {
		if (isset($_POST['command'])) {
			class ReturObatAdapter extends ArrayAdapter {
				public function adapt($row) {
					$array = array();
					$array['id'] = $row->id;
					$array['dibatalkan'] = $row->dibatalkan;
					$array['Nomor'] = self::format("digit8", $row->id);
					$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
					$array['Jenis Stok'] = self::format("unslug", $row->tipe);
					$array['Vendor'] = $row->nama_vendor;
					$array['No. Faktur'] = $row->no_faktur;
					$array['Tgl. Faktur'] = self::format("date d M Y", $row->tanggal_faktur);
					if ($row->dibatalkan)
						$array['Status'] = "Dibatalkan";
					else
						$array['Status'] = "-";
					return $array;
				}
			}
			$retur_obat_adapter = new ReturObatAdapter();
			$columns = array("id", "id_obat_f_masuk", "id_vendor", "nama_vendor", "no_faktur", "tanggal_faktur", "tanggal", "dibatalkan");
			$retur_obat_dbtable = new DBTable(
				$db,
				"smis_lgs_retur_obat",
				$columns
			);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (smis_lgs_retur_obat.no_faktur LIKE '%" . $_POST['kriteria'] . "%' OR smis_lgs_retur_obat.nama_vendor LIKE '%" . $_POST['kriteria'] . "%')";
			}
			$query_value = "
				SELECT smis_lgs_retur_obat.*, smis_lgs_obat_f_masuk.tipe
				FROM smis_lgs_retur_obat LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_retur_obat.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
				WHERE smis_lgs_retur_obat.prop NOT LIKE 'del' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_lgs_retur_obat.*, smis_lgs_obat_f_masuk.tipe
					FROM smis_lgs_retur_obat LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_retur_obat.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
					WHERE smis_lgs_retur_obat.prop NOT LIKE 'del' " . $filter . "
				) v_retur
			";
			$retur_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			class ReturObatDBResponder extends DBResponder {
				public function save() {
					$header_data = $this->postToArray();
					$id['id'] = $_POST['id'];
					if ($id['id'] == 0 || $id['id'] == "") {
						//do insert header here:
						$result = $this->dbtable->insert($header_data);
						$id['id'] = $this->dbtable->get_inserted_id();
						$success['type'] = "insert";
						if (isset($_POST['detail'])) {
							//do insert detail here:
							$dretur_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_dretur_obat");
							$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
							$detail = $_POST['detail'];
							foreach($detail as $d) {
								$dretur_data = array();
								$dretur_data['id_retur_obat'] = $id['id'];
								$dretur_data['id_stok_obat'] = $d['id_stok_obat'];
								$dretur_data['jumlah'] = $d['jumlah'];
								$dretur_data['keterangan'] = $d['keterangan'];
								$dretur_obat_dbtable->insert($dretur_data);
								$stok_row = $stok_obat_dbtable->get_row("
									SELECT id, sisa, retur
									FROM smis_lgs_stok_obat
									WHERE id = '" . $d['id_stok_obat'] . "'
								");
								$stok_data = array();
								$stok_data['sisa'] = $stok_row->sisa - $d['jumlah'];
								$stok_data['retur'] = $stok_row->retur + $d['jumlah'];
								$stok_id['id'] = $d['id_stok_obat'];
								$stok_obat_dbtable->update($stok_data, $stok_id);
								//logging riwayat stok obat:
								$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_riwayat_stok_obat");
								$data_riwayat = array();
								$data_riwayat['tanggal'] = date("Y-m-d");
								$data_riwayat['id_stok_obat'] = $d['id_stok_obat'];
								$data_riwayat['jumlah_keluar'] = $d['jumlah'];
								$data_riwayat['sisa'] = $stok_row->sisa - $d['jumlah'];
								$data_riwayat['keterangan'] = "Retur Stok ke Vendor";
								global $user;
								$data_riwayat['nama_user'] = $user->getName();
								$riwayat_dbtable->insert($data_riwayat);
							}
						}
					} else {
						//do update header here:
						$result = $this->dbtable->update($header_data, $id);
						$success['type'] = "update";
						if (isset($_POST['dibatalkan']) && $_POST['dibatalkan'] == 1) {
							$detail_rows = $this->dbtable->get_result("
								SELECT *
								FROM smis_lgs_dretur_obat
								WHERE id_retur_obat = '" . $id['id'] . "' AND prop NOT LIKE 'del'
							");
							$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
							foreach($detail_rows as $dr) {
								$stok_row = $stok_obat_dbtable->get_row("
									SELECT id, sisa, retur
									FROM smis_lgs_stok_obat
									WHERE id = '" . $dr->id_stok_obat . "'
								");
								$stok_data = array();
								$stok_data['sisa'] = $stok_row->sisa + $dr->jumlah;
								$stok_data['retur'] = $stok_row->retur - $dr->jumlah;
								$stok_id['id'] = $dr->id_stok_obat;
								$stok_obat_dbtable->update($stok_data, $stok_id);
							}
						}
					}
					$success['id'] = $id['id'];
					$success['success'] = 1;
					if ($result === false) $success['success'] = 0;
					return $success;
				}
				public function edit() {
					$id = $_POST['id'];
					$data['header'] = $this->dbtable->get_row("
						SELECT smis_lgs_retur_obat.*, smis_lgs_obat_f_masuk.tipe
						FROM smis_lgs_retur_obat LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_retur_obat.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
						WHERE smis_lgs_retur_obat.id = '" . $id . "'
					");
					$detail_rows = $this->dbtable->get_result("
						SELECT smis_lgs_dretur_obat.*, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat.nama_jenis_obat, smis_lgs_stok_obat.satuan, smis_lgs_stok_obat.konversi, smis_lgs_stok_obat.satuan_konversi, smis_lgs_stok_obat.tanggal_exp, smis_lgs_stok_obat.produsen
						FROM smis_lgs_dretur_obat LEFT JOIN smis_lgs_stok_obat ON smis_lgs_dretur_obat.id_stok_obat = smis_lgs_stok_obat.id
						WHERE smis_lgs_dretur_obat.prop NOT LIKE 'del' AND smis_lgs_dretur_obat.id_retur_obat = '" . $id . "'
					");
					$detail_list = "";
					$row_id = 0;
					foreach($detail_rows as $dr) {
						$f_tanggal_exp = "";
						if ($dr->tanggal_exp == "0000-00-00")
							$f_tanggal_exp = "-";
						else
							$f_tanggal_exp = ArrayAdapter::format("date d M Y", $dr->tanggal_exp);
						$detail_list .= "<tr id='detail_" . $row_id . "'>" .
											"<td id='detail_" . $row_id . "_id' style='display: none;'>" . $dr->id . "</td>" .
											"<td id='detail_" . $row_id . "_id_stok_obat' style='display: none;'>" . $dr->id_stok_obat . "</td>" .
											"<td id='detail_" . $row_id . "_jumlah_retur' style='display: none;'>" . $dr->jumlah . "</td>" .
											"<td id='detail_" . $row_id . "_satuan' style='display: none;'>" . $dr->satuan . "</td>" .
											"<td id='detail_" . $row_id . "_konversi' style='display: none;'>" . $dr->konversi . "</td>" .
											"<td id='detail_" . $row_id . "_satuan_konversi' style='display: none;'>" . $dr->satuan_konversi . "</td>" .
											"<td id='detail_" . $row_id . "_tanggal_exp' style='display: none;'>" . $dr->tanggal_exp . "</td>" .
											"<td id='detail_" . $row_id . "_nama_obat'>" . $dr->nama_obat . "</td>" .
											"<td id='detail_" . $row_id . "_nama_jenis_obat'>" . $dr->nama_jenis_obat . "</td>" .
											"<td id='detail_" . $row_id . "_produsen'>" . $dr->produsen . "</td>" .
											"<td id='detail_" . $row_id . "_f_tanggal_exp'>" . $f_tanggal_exp . "</td>" .
											"<td id='detail_" . $row_id . "_f_jumlah'>" . $dr->jumlah . " " . $dr->satuan . "</td>" .
											"<td id='detail_" . $row_id . "_keterangan'>" . $dr->keterangan . "</td>" .
											"<td></td>" .
										"</tr>";
						$row_id++;
					}
					$data['detail_list'] = $detail_list;
					return $data;
				}
			}
			$retur_obat_dbresponder = new ReturObatDBResponder(
				$retur_obat_dbtable,
				$retur_keluar_table,
				$retur_obat_adapter
			);
			$data = $retur_obat_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	//obat masuk chooser:
	$obat_masuk_table = new Table(
		array("Nomor", "No. SP", "Vendor", "No. Faktur", "Tgl. Faktur", "Tgl. Datang", "Jatuh Tempo"),
		"",
		null,
		true
	);
	$obat_masuk_table->setName("obat_masuk");
	$obat_masuk_table->setModel(Table::$SELECT);
	$obat_masuk_adapter = new SimpleAdapter();
	$obat_masuk_adapter->add("Nomor", "id", "digit8");
	$obat_masuk_adapter->add("No. SP", "id_po", "digit8");
	$obat_masuk_adapter->add("Vendor", "nama_vendor");
	$obat_masuk_adapter->add("No. Faktur", "no_faktur");
	$obat_masuk_adapter->add("Tgl. Faktur", "tanggal", "date d M Y");
	$obat_masuk_adapter->add("Tgl. Datang", "tanggal_datang", "date d M Y");
	$obat_masuk_adapter->add("Jatuh Tempo", "tanggal_tempo", "date d M Y");
	$obat_masuk_dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
	$obat_masuk_dbtable->addCustomKriteria(" tipe ", " = 'logistik' ");
	$obat_masuk_dbtable->addCustomKriteria(" no_faktur ", " != '-' ");
	$obat_masuk_dbtable->addCustomKriteria(" tanggal ", " != '0000-00-00' ");
	class ObatMasukDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['header'] = $this->dbtable->get_row("
				SELECT *
				FROM smis_lgs_obat_f_masuk 
				WHERE id = '" . $id . "'
			");
			$detail_list = "";
			$dobat_masuk_rows = $this->dbtable->get_result("
				SELECT smis_lgs_stok_obat.*
				FROM smis_lgs_stok_obat LEFT JOIN smis_lgs_dobat_f_masuk ON smis_lgs_stok_obat.id_dobat_masuk = smis_lgs_dobat_f_masuk.id
				WHERE smis_lgs_stok_obat.prop NOT LIKE 'del' AND smis_lgs_stok_obat.turunan = '0' AND smis_lgs_stok_obat.label = 'reguler' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk = '" . $id . "'
			");
			$row_id = 0;
			foreach($dobat_masuk_rows as $dom) {
				$f_tanggal_exp = "";
				if ($dom->tanggal_exp == "0000-00-00")
					$f_tanggal_exp = "-";
				else
					$f_tanggal_exp = ArrayAdapter::format("date d M Y", $dom->tanggal_exp);
				$detail_list .= "<tr id='obat_" . $row_id . "'>" .
									"<td id='obat_" . $row_id . "_id' style='display: none;'></td>" .
									"<td id='obat_" . $row_id . "_id_stok_obat' style='display: none;'>" . $dom->id . "</td>" .
									"<td id='obat_" . $row_id . "_sisa' style='display: none;'>" . $dom->sisa . "</td>" .
									"<td id='obat_" . $row_id . "_jumlah_retur' style='display: none;'>0</td>" .
									"<td id='obat_" . $row_id . "_satuan' style='display: none;'>" . $dom->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_konversi' style='display: none;'>" . $dom->konversi . "</td>" .
									"<td id='obat_" . $row_id . "_satuan_konversi' style='display: none;'>" . $dom->satuan_konversi . "</td>" .
									"<td id='obat_" . $row_id . "_tanggal_exp' style='display: none;'>" . $dom->tanggal_exp . "</td>" .
									"<td id='obat_" . $row_id . "_nama_obat'>" . $dom->nama_obat . "</td>" .
									"<td id='obat_" . $row_id . "_nama_jenis_obat'>" . $dom->nama_jenis_obat . "</td>" .
									"<td id='obat_" . $row_id . "_produsen'>" . $dom->produsen . "</td>" .
									"<td id='obat_" . $row_id . "_f_tanggal_exp'>" . $f_tanggal_exp . "</td>" .
									"<td id='obat_" . $row_id . "_f_sisa'>" . $dom->sisa . " " . $dom->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_f_retur'>0 " . $dom->satuan . "</td>" .
									"<td id='obat_" . $row_id . "_keterangan'>-</td>" .
									"<td>" .
										"<div class='btn-group noprint'>" .
											"<a href='#' onclick='dretur.edit(" . $row_id . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" . 
												"<i class='icon-edit icon-white'></i>" .
											"</a>" .
										"</div>" .
									"</td>" .
								"</tr>";
				$row_id++;
			}
			$data['detail_list'] = $detail_list;
			return $data;
		}
	}
	$obat_masuk_dbresponder = new ObatMasukDBResponder(
		$obat_masuk_dbtable,
		$obat_masuk_table,
		$obat_masuk_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat_masuk", $obat_masuk_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$retur_modal = new Modal("retur_add_form", "smis_form_container", "retur");
	$retur_modal->setTitle("Data Retur Obat");
	$retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("retur_id", "retur_id", "");
	$retur_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("retur_tanggal", "retur_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$retur_modal->addElement("Tanggal", $tanggal_text);
	$obat_masuk_button = new Button("", "", "Pilih");
	$obat_masuk_button->setClass("btn-info");
	$obat_masuk_button->setIsButton(Button::$ICONIC);
	$obat_masuk_button->setIcon("icon-white ".Button::$icon_list_alt);
	$obat_masuk_button->setAction("obat_masuk.chooser('obat_masuk', 'obat_masuk_button', 'obat_masuk', obat_masuk)");
	$obat_masuk_button->setAtribute("id='obat_masuk_browse'");
	$obat_masuk_text = new Text("retur_no_faktur", "retur_no_faktur", "");
	$obat_masuk_text->setAtribute("disabled='disabled'");
	$obat_masuk_text->setClass("smis-one-option-input");
	$obat_masuk_input_group = new InputGroup("");
	$obat_masuk_input_group->addComponent($obat_masuk_text);
	$obat_masuk_input_group->addComponent($obat_masuk_button);
	$retur_modal->addElement("No. Faktur", $obat_masuk_input_group);
	$id_obat_masuk_hidden = new Hidden("retur_id_obat_masuk", "retur_id_obat_masuk", "");
	$retur_modal->addElement("", $id_obat_masuk_hidden);
	$id_vendor_hidden = new Hidden("retur_id_vendor", "retur_id_vendor", "");
	$retur_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("retur_nama_vendor", "retur_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Vendor", $nama_vendor_text);
	$tanggal_faktur_text = new Text("retur_tanggal_faktur", "retur_tanggal_faktur", "");
	$tanggal_faktur_text->setAtribute("disabled='disabled'");
	$retur_modal->addElement("Tgl. Faktur", $tanggal_faktur_text);
	$dretur_table = new Table(
		array("Nama Obat", "Jenis Obat", "Produsen", "Tgl. Exp.", "Sisa", "Jml. Retur", "Keterangan"),
		"",
		null,
		true
	);
	$dretur_table->setName("dretur");
	$dretur_table->setFooterVisible(false);
	$dretur_table->setAddButtonEnable(false);
	$dretur_table->setReloadButtonEnable(false);
	$dretur_table->setPrintButtonEnable(false);
	$retur_modal->addBody("dretur_table", $dretur_table);
	$retur_button = new Button("", "", "Simpan");
	$retur_button->setClass("btn-success");
	$retur_button->setIcon("fa fa-floppy-o");
	$retur_button->setIsButton(Button::$ICONIC);
	$retur_button->setAtribute("id='retur_save'");
	$retur_button->setAction("retur.save()");
	$retur_modal->addFooter($retur_button);
	
	$dretur_modal = new Modal("dretur_add_form", "smis_form_container", "dretur");
	$dretur_modal->setTitle("Data Detail Retur Obat");
	$id_hidden = new Hidden("dretur_id", "dretur_id", "");
	$dretur_modal->addElement("", $id_hidden);
	$nama_obat_text = new Text("dretur_nama_obat", "dretur_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Nama Obat", $nama_obat_text);
	$jenis_obat_text = new Text("dretur_jenis_obat", "dretur_jenis_obat", "");
	$jenis_obat_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Jenis Obat", $jenis_obat_text);
	$produsen_text = new Text("dretur_produsen", "dretur_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Produsen", $produsen_text);
	$tanggal_exp_text = new Text("dretur_tanggal_exp", "dretur_tanggal_exp", "");
	$tanggal_exp_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Tgl. Exp.", $tanggal_exp_text);
	$sisa_hidden = new Hidden("dretur_sisa", "dretur_sisa", "");
	$dretur_modal->addElement("", $sisa_hidden);
	$f_sisa_text = new Text("dretur_f_sisa", "dretur_f_sisa", "");
	$f_sisa_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Sisa", $f_sisa_text);
	$jumlah_retur_text = new Text("dretur_jumlah_retur", "dretur_jumlah_retur", "");
	$dretur_modal->addElement("Jml. Retur", $jumlah_retur_text);
	$satuan_text = new Text("dretur_satuan", "dretur_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$dretur_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("dretur_keterangan", "dretur_keterangan", "");
	$dretur_modal->addElement("Keterangan", $keterangan_textarea);
	$dretur_button = new Button("", "", "Simpan");
	$dretur_button->setClass("btn-success");
	$dretur_button->setIcon("fa fa-floppy-o");
	$dretur_button->setIsButton(Button::$ICONIC);
	$dretur_button->setAtribute("id='dretur_save'");
	$dretur_modal->addFooter($dretur_button);
	
	$v_retur_modal = new Modal("v_retur_add_form", "smis_form_container", "v_retur");
	$v_retur_modal->setTitle("Data Retur Obat");
	$v_retur_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("v_retur_id", "v_retur_id", "");
	$v_retur_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("v_retur_tanggal", "v_retur_tanggal", "");
	$tanggal_text->setAtribute("disabled");
	$v_retur_modal->addElement("Tanggal", $tanggal_text);
	$no_faktur_text = new Text("v_retur_no_faktur", "v_retur_no_faktur", "");
	$no_faktur_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("No. Faktur", $no_faktur_text);
	$id_obat_masuk_hidden = new Hidden("v_retur_id_obat_masuk", "v_retur_id_obat_masuk", "");
	$v_retur_modal->addElement("", $id_obat_masuk_hidden);
	$id_vendor_hidden = new Hidden("v_retur_id_vendor", "v_retur_id_vendor", "");
	$v_retur_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("v_retur_nama_vendor", "v_retur_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Vendor", $nama_vendor_text);
	$tanggal_faktur_text = new Text("v_retur_tanggal_faktur", "v_retur_tanggal_faktur", "");
	$tanggal_faktur_text->setAtribute("disabled='disabled'");
	$v_retur_modal->addElement("Tgl. Faktur", $tanggal_faktur_text);
	$v_dretur_table = new Table(
		array("Obat", "Produsen", "Tgl. Exp.", "Jml. Retur", "Keterangan"),
		"",
		null,
		true
	);
	$v_dretur_table->setName("v_dretur");
	$v_dretur_table->setFooterVisible(false);
	$v_dretur_table->setAddButtonEnable(false);
	$v_dretur_table->setReloadButtonEnable(false);
	$v_dretur_table->setPrintButtonEnable(false);
	$v_retur_modal->addBody("v_dretur_table", $v_dretur_table);
	$v_retur_button = new Button("", "", "OK");
	$v_retur_button->setClass("btn-success");
	$v_retur_button->setAction("$($(this).data('target')).smodal('hide')");
	$v_retur_modal->addFooter($v_retur_button);
	
	echo $v_retur_modal->getHtml();
	echo $dretur_modal->getHtml();
	echo $retur_modal->getHtml();
	echo $retur_keluar_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function ReturAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ReturAction.prototype.constructor = ReturAction;
	ReturAction.prototype = new TableAction();
	ReturAction.prototype.show_add_form = function() {
		var today = new Date();
		$("#retur_id").val("");
		$("#retur_tanggal").val(today.getFullYear() + "-" + (Number(today.getMonth())+1) + "-" + today.getDate());
		$("#retur_no_faktur").val("");
		$("#retur_id_obat_masuk").val("");
		$("#retur_id_vendor").val("");
		$("#retur_nama_vendor").val("");
		$("#retur_tanggal_faktur").val("");
		$("tbody#dretur_list").children("tr").remove();
		$("#modal_alert_retur_add_form").html("");
		$(".error_field").removeClass("error_field");
		$("#retur_add_form").smodal("show");
	};
	ReturAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var tanggal = $("#retur_tanggal").val();
		var id_obat_masuk = $("#retur_id_obat_masuk").val();
		var retur_exist = false;
		var nor = $("tbody#dretur_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var prefix = $("tbody#dretur_list").children("tr").eq(i).prop("id");
			var jumlah_retur = parseFloat($("#" + prefix + "_jumlah_retur").text());
			if (jumlah_retur > 0) {
				retur_exist = true;
				break;
			}
		}
		$(".error_field").removeClass("error_field");
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
			$("#retur_tanggal").addClass("error_field");
		}
		if (id_obat_masuk == "") {
			valid = false;
			invalid_msg += "</br><strong>No. Faktur</strong> tidak boleh kosong";
			$("#retur_no_faktur").addClass("error_field");
		}
		if (!retur_exist) {
			valid = false;
			invalid_msg += "</br>Tidak ada obat yang diretur";
		}
		if (!valid) {
			$("#modal_alert_retur_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	ReturAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		showLoading();
		$("#retur_add_form").smodal("hide");
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "retur";
		data['command'] = "save";
		data['id'] = $("#retur_id").val();
		data['tanggal'] = $("#retur_tanggal").val();
		data['id_obat_f_masuk'] = $("#retur_id_obat_masuk").val();
		data['id_vendor'] = $("#retur_id_vendor").val();
		data['nama_vendor'] = $("#retur_nama_vendor").val();
		data['no_faktur'] = $("#retur_no_faktur").val();
		data['tanggal_faktur'] = $("#retur_tanggal_faktur").val();
		var detail = {};
		var nor = $("tbody#dretur_list").children("tr").length;
		var j = 0;
		for(var i = 0; i < nor; i++) {
			var prefix = $("tbody#dretur_list").children("tr").eq(i).prop("id");
			var jumlah_retur = parseFloat($("#" + prefix + "_jumlah_retur").text());
			if (jumlah_retur > 0) {
				var id_stok_obat = $("#" + prefix + "_id_stok_obat").text();
				var keterangan = $("#" + prefix + "_keterangan").text();
				var d_data = {};
				d_data['id_stok_obat'] = id_stok_obat;
				d_data['jumlah'] = jumlah_retur;
				d_data['keterangan'] = keterangan;
				detail[j] = d_data;
				j++;
			}
		}
		data['detail'] = detail;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#retur_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	ReturAction.prototype.detail = function(id) {
		var data = this.getRegulerData();
		data['super_command'] = "retur";
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#v_retur_id").val(json.header.id);
				$("#v_retur_tanggal").val(json.header.tanggal);
				$("#v_retur_no_faktur").val(json.header.no_faktur);
				$("#v_retur_id_obat_masuk").val(json.header.id_obat_f_masuk);
				$("#v_retur_id_vendor").val(json.header.id_vendor);
				$("#v_retur_nama_vendor").val(json.header.nama_vendor);
				$("#v_retur_tanggal_faktur").val(json.header.tanggal_faktur);
				$("tbody#v_dretur_list").html(json.detail_list);
				$("#v_retur_add_form").smodal("show");
			}
		);
	};
	ReturAction.prototype.cancel = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['super_command'] = "retur";
		data['command'] = "save";
		data['id'] = id;
		data['dibatalkan'] = "1";
		bootbox.confirm(
			"Yakin membatalkan Retur Penjualan Resep ini?",
			function(result) {
				if (result) {
					showLoading();
					$.post(	
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	
	function DReturAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DReturAction.prototype.constructor = DReturAction;
	DReturAction.prototype = new TableAction();
	DReturAction.prototype.edit = function(r_num) {
		var id = $("#obat_" + r_num + "_id").text();
		var nama_obat = $("#obat_" + r_num + "_nama_obat").text();
		var jenis_obat = $("#obat_" + r_num + "_nama_jenis_obat").text();
		var produsen = $("#obat_" + r_num + "_produsen").text();
		var tanggal_exp = $("#obat_" + r_num + "_tanggal_exp").text();
		var sisa = $("#obat_" + r_num + "_sisa").text();
		var jumlah_retur = $("#obat_" + r_num + "_jumlah_retur").text();
		var satuan = $("#obat_" + r_num + "_satuan").text();
		var keterangan = $("#obat_" + r_num + "_keterangan").text();
		$("#dretur_id").val(id);
		$("#dretur_nama_obat").val(nama_obat);
		$("#dretur_jenis_obat").val(jenis_obat);
		$("#dretur_produsen").val(produsen);
		$("#dretur_tanggal_exp").val(tanggal_exp);
		$("#dretur_sisa").val(sisa);
		$("#dretur_f_sisa").val(sisa + " " + satuan);
		$("#dretur_jumlah_retur").val(jumlah_retur);
		$("#dretur_satuan").val(satuan);
		$("#dretur_keterangan").val(keterangan);
		$("#dretur_save").removeAttr("onclick");
		$("#dretur_save").attr("onclick", "dretur.update(" + r_num + ")");
		$("#dretur_add_form").smodal("show");
	};
	DReturAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var sisa = $("#dretur_sisa").val();
		var jumlah_retur = $("#dretur_jumlah_retur").val();
		var keterangan = $("#dretur_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (jumlah_retur == "") {
			valid = false;
			invalid_msg += "</br><strong>Jml. Retur</strong> tidak boleh kosong";
			$("#dretur_jumlah_retur").addClass("error_field");
		} else if (!is_numeric(jumlah_retur)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Retur</strong> seharusnya numerik (0-9)";
			$("#dretur_jumlah_retur").addClass("error_field");
		} else if (parseFloat(jumlah_retur) > parseFloat(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Retur</strong> melebihi <strong>Sisa</strong>";
			$("#dretur_jumlah_retur").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#dretur_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_dretur_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DReturAction.prototype.update = function(r_num) {
		if (!this.validate()) {
			return;
		}
		var jumlah_retur = $("#dretur_jumlah_retur").val();
		var satuan = $("#dretur_satuan").val();
		var keterangan = $("#dretur_keterangan").val();
		$("#obat_" + r_num + "_jumlah_retur").text(jumlah_retur);
		$("#obat_" + r_num + "_f_retur").text(jumlah_retur + " " + satuan);
		$("#obat_" + r_num + "_keterangan").text(keterangan);
		$("#dretur_add_form").smodal("hide");
	};
	
	function ObatMasukAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ObatMasukAction.prototype.constructor = ObatMasukAction;
	ObatMasukAction.prototype = new TableAction();
	ObatMasukAction.prototype.selected = function(json) {
		$("#retur_id_obat_masuk").val(json.header.id);
		$("#retur_no_faktur").val(json.header.no_faktur);
		$("#retur_tanggal_faktur").val(json.header.tanggal);
		$("#retur_id_vendor").val(json.header.id_vendor);
		$("#retur_nama_vendor").val(json.header.nama_vendor);
		$("tbody#dretur_list").html(json.detail_list);
	};
	
	var retur;
	var obat_masuk;
	var dretur;
	$(document).ready(function() {
		$("#smis-chooser-modal").addClass("full_model");
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		obat_masuk = new ObatMasukAction(
			"obat_masuk",
			"gudang_logistik",
			"retur_keluar",
			new Array()
		);
		obat_masuk.setSuperCommand("obat_masuk");
		var dretur_columns = new Array("id", "id_retur_obat", "id_stok_obat", "jumlah", "keterangan");
		dretur = new DReturAction(
			"dretur",
			"gudang_logistik",
			"retur_keluar",
			dretur_columns
		);
		var retur_columns = new Array("id", "id_obat_f_masuk", "tanggal", "id_vendor", "nama_vendor", "no_faktur", "tanggal_faktur");
		retur = new ReturAction(
			"retur",
			"gudang_logistik",
			"retur_keluar",
			retur_columns
		);
		retur.setSuperCommand("retur");
		retur.view();
	});
</script>