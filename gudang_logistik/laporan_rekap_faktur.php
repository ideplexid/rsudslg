<?php 
	global $db;
	
	$laporan_form = new Form("lrf_form", "", "Gudang Logistik : Laporan Rekapitulasi Faktur");
	$tanggal_from_text = new Text("lrf_tanggal_from", "lrf_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lrf_tanggal_to", "lrf_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lrf.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lrf.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lrf_table = new Table(
		array("Tgl. Masuk", "Vendor", "No. Faktur", "Jatuh Tempo", "Total"),
		"",
		null,
		true
	);
	$lrf_table->setName("lrf");
	$lrf_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class LRFAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Tgl. Masuk'] = self::format("date d M Y", $row->tanggal_datang);
				$array['Vendor'] = $row->nama_vendor;
				$array['No. Faktur'] = $row->no_faktur;
				if ($row->tanggal_tempo == '0000-00-00')
					$array['Jatuh Tempo'] = "-";
				else
					$array['Jatuh Tempo'] = self::format("date d M Y", $row->tanggal_tempo);
				$total = $row->total;
				if ($row->t_diskon == 'persen') {
					$diskon = round(($row->total * $row->diskon) / 100);
					$total = round($row->total - $diskon);
				} else {
					$total = round($row->total - $diskon);
				}
				if ($row->tipe != "sito")
					$total = round($total + round($total / 10));
				$array['Total'] = self::format("money Rp. ", $total);
				return $array;
			}
		}
		$lrf_adapter = new LRFAdapter();
		$lrf_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR no_faktur LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT smis_lgs_obat_f_masuk.id, smis_lgs_obat_f_masuk.tipe, smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_obat_f_masuk.tanggal_tempo, smis_lgs_obat_f_masuk.nama_vendor, smis_lgs_obat_f_masuk.no_faktur, smis_lgs_obat_f_masuk.diskon, smis_lgs_obat_f_masuk.t_diskon, v_detail.total AS 'total'
				FROM smis_lgs_obat_f_masuk LEFT JOIN (
					SELECT id_obat_f_masuk, SUM(total) AS 'total'
					FROM (
						SELECT id_obat_f_masuk, IF (
							t_diskon = 'persen',
							ROUND(total_hp - ROUND((total_hp * diskon) / 100)),
							IF (
								t_diskon = 'nominal',
								ROUND(total_hp - diskon),
								total_hp
							)
						) AS 'total'
						FROM (
							SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * ROUND((smis_lgs_dobat_f_masuk.hna * 100) / 110))) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
							FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
							WHERE smis_lgs_obat_f_masuk.tipe = 'logistik' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
							GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
						) v_detail_logistik
						UNION ALL
						SELECT id_obat_f_masuk, IF (
							t_diskon = 'persen',
							ROUND(total_hp - ROUND((total_hp * diskon) / 100)),
							IF (
								t_diskon = 'nominal',
								ROUND(total_hp - diskon),
								ROUND(total_hp)
							)
						) AS 'total'
						FROM (
							SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * smis_lgs_dobat_f_masuk.hna)) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
							FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
							WHERE smis_lgs_obat_f_masuk.tipe = 'sito' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
							GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
						) v_detail_sito
						UNION ALL
						SELECT id_obat_f_masuk, ROUND(total_hp) AS 'total'
						FROM (
							SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * ROUND((smis_lgs_dobat_f_masuk.hna * 100) / 110))) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
							FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
							WHERE smis_lgs_obat_f_masuk.tipe = 'konsinyasi' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
							GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
						) v_detail_konsinyasi
					) v_detail
					GROUP BY id_obat_f_masuk
				) v_detail ON smis_lgs_obat_f_masuk.id = v_detail.id_obat_f_masuk
				WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_obat_f_masuk.tanggal_datang >= '" . $_POST['tanggal_from'] . "' AND smis_lgs_obat_f_masuk.tanggal_datang <= '" . $_POST['tanggal_to'] . "' AND smis_lgs_obat_f_masuk.id <> '0' " . $filter . "
				ORDER BY tanggal_datang, nama_vendor, no_faktur, tanggal_tempo ASC
			) v_lrf
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT smis_lgs_obat_f_masuk.id, smis_lgs_obat_f_masuk.tipe, smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_obat_f_masuk.tanggal_tempo, smis_lgs_obat_f_masuk.nama_vendor, smis_lgs_obat_f_masuk.no_faktur, smis_lgs_obat_f_masuk.diskon, smis_lgs_obat_f_masuk.t_diskon, v_detail.total AS 'total'
				FROM smis_lgs_obat_f_masuk LEFT JOIN (
					SELECT id_obat_f_masuk, SUM(total) AS 'total'
					FROM (
						SELECT id_obat_f_masuk, IF (
							t_diskon = 'persen',
							ROUND(total_hp - ROUND((total_hp * diskon) / 100)),
							IF (
								t_diskon = 'nominal',
								ROUND(total_hp - diskon),
								total_hp
							)
						) AS 'total'
						FROM (
							SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * ROUND((smis_lgs_dobat_f_masuk.hna * 100) / 110))) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
							FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
							WHERE smis_lgs_obat_f_masuk.tipe = 'logistik' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
							GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
						) v_detail_logistik
						UNION ALL
						SELECT id_obat_f_masuk, IF (
							t_diskon = 'persen',
							ROUND(total_hp - ROUND((total_hp * diskon) / 100)),
							IF (
								t_diskon = 'nominal',
								ROUND(total_hp - diskon),
								ROUND(total_hp)
							)
						) AS 'total'
						FROM (
							SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * smis_lgs_dobat_f_masuk.hna)) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
							FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
							WHERE smis_lgs_obat_f_masuk.tipe = 'sito' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
							GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
						) v_detail_sito
						UNION ALL
						SELECT id_obat_f_masuk, ROUND(total_hp) AS 'total'
						FROM (
							SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * ROUND((smis_lgs_dobat_f_masuk.hna * 100) / 110))) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
							FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
							WHERE smis_lgs_obat_f_masuk.tipe = 'konsinyasi' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
							GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
						) v_detail_konsinyasi
					) v_detail
					GROUP BY id_obat_f_masuk
				) v_detail ON smis_lgs_obat_f_masuk.id = v_detail.id_obat_f_masuk
				WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_obat_f_masuk.tanggal_datang >= '" . $_POST['tanggal_from'] . "' AND smis_lgs_obat_f_masuk.tanggal_datang <= '" . $_POST['tanggal_to'] . "' AND smis_lgs_obat_f_masuk.id <> '0' " . $filter . "
				ORDER BY tanggal_datang, nama_vendor, no_faktur, tanggal_tempo ASC
			) v_lrf
		";
		$lrf_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['tanggal_from'];
				$to = $_POST['tanggal_to'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_lgs_obat_f_masuk.id, smis_lgs_obat_f_masuk.tipe, smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_obat_f_masuk.tanggal_tempo, smis_lgs_obat_f_masuk.nama_vendor, smis_lgs_obat_f_masuk.no_faktur, smis_lgs_obat_f_masuk.diskon, smis_lgs_obat_f_masuk.t_diskon, v_detail.total AS 'total'
						FROM smis_lgs_obat_f_masuk LEFT JOIN (
							SELECT id_obat_f_masuk, SUM(total) AS 'total'
							FROM (
								SELECT id_obat_f_masuk, IF (
									t_diskon = 'persen',
									ROUND(total_hp - ROUND((total_hp * diskon) / 100)),
									IF (
										t_diskon = 'nominal',
										ROUND(total_hp - diskon),
										total_hp
									)
								) AS 'total'
								FROM (
									SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * ROUND((smis_lgs_dobat_f_masuk.hna * 100) / 110))) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
									FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
									WHERE smis_lgs_obat_f_masuk.tipe = 'logistik' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
									GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
								) v_detail_logistik
								UNION ALL
								SELECT id_obat_f_masuk, IF (
									t_diskon = 'persen',
									ROUND(total_hp - ROUND((total_hp * diskon) / 100)),
									IF (
										t_diskon = 'nominal',
										ROUND(total_hp - diskon),
										ROUND(total_hp)
									)
								) AS 'total'
								FROM (
									SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * smis_lgs_dobat_f_masuk.hna)) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
									FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
									WHERE smis_lgs_obat_f_masuk.tipe = 'sito' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
									GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
								) v_detail_sito
								UNION ALL
								SELECT id_obat_f_masuk, ROUND(total_hp) AS 'total'
								FROM (
									SELECT smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, ROUND(SUM(smis_lgs_dobat_f_masuk.jumlah * ROUND((smis_lgs_dobat_f_masuk.hna * 100) / 110))) AS 'total_hp', smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
									FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
									WHERE smis_lgs_obat_f_masuk.tipe = 'konsinyasi' AND smis_lgs_dobat_f_masuk.id_obat_f_masuk <> '0' AND smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
									GROUP BY smis_lgs_dobat_f_masuk.id_obat_f_masuk, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.konversi, smis_lgs_dobat_f_masuk.satuan_konversi, smis_lgs_dobat_f_masuk.diskon, smis_lgs_dobat_f_masuk.t_diskon
								) v_detail_konsinyasi
							) v_detail
							GROUP BY id_obat_f_masuk
						) v_detail ON smis_lgs_obat_f_masuk.id = v_detail.id_obat_f_masuk
						WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_obat_f_masuk.tanggal_datang >= '" . $from . "' AND smis_lgs_obat_f_masuk.tanggal_datang <= '" . $to . "' AND smis_lgs_obat_f_masuk.id <> '0' 
						ORDER BY tanggal_datang, nama_vendor, no_faktur, tanggal_tempo ASC
					) v_lrf
				");
				if ($tipe == "logistik") {
					$tipe = "reguler";
				} else if ($tipe == "%%") {
					$tipe = "semua";
				}
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN REKAPITULASI FAKTUR PEMBELIAN OBAT</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>Tgl. Masuk</th>
										<th>Vendor</th>
										<th>No. Faktur</th>
										<th>Jatuh Tempo</th>
										<th>Total</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$tanggal_jt = "";
						if ($d->tanggal_tempo == "0000-00-00")
							$tanggal_jt = "-";
						else
							$tanggal_jt = ArrayAdapter::format("date d M Y", $d->tanggal_tempo);
						$f_total = $d->total;
						if ($d->t_diskon == 'persen') {
							$diskon = round(($d->total * $d->diskon) / 100);
							$f_total = round($d->total - $diskon);
						} else {
							$f_total = round($d->total - $diskon);
						}
						if ($d->tipe != "sito")
							$f_total = round($f_total + round($f_total / 10));
						$print_data .= "<tr>
											<td>" . ArrayAdapter::format("date d M Y", $d->tanggal_datang) . "</td>
											<td>" . $d->nama_vendor . "</td>
											<td>" . $d->no_faktur . "</td>
											<td>" . $tanggal_jt . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $f_total) . "</td>
										</tr>";
						$total += $f_total;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='5' align='center'><i>Tidak terdapat data faktur pembelian obat</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='4' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lrf_dbresponder = new LLSODBResponder(
			$lrf_dbtable,
			$lrf_table,
			$lrf_adapter
		);
		$data = $lrf_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lrf_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOMPPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOMPPAction.prototype.constructor = LOMPPAction;
	LOMPPAction.prototype = new TableAction();
	LOMPPAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lrf_tanggal_from").val();
		data['tanggal_to'] = $("#lrf_tanggal_to").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOMPPAction.prototype.print = function() {
		if ($("#lrf_tanggal_from").val() == "" || $("#lrf_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['produsen'] = $("#lrf_produsen").val();
		data['tanggal_from'] = $("#lrf_tanggal_from").val();
		data['tanggal_to'] = $("#lrf_tanggal_to").val();
		data['tipe'] = $("#lrf_tipe").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lrf;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		lrf = new LOMPPAction(
			"lrf",
			"gudang_logistik",
			"laporan_rekap_faktur",
			new Array()
		)
	});
</script>