<?php 
	global $db;
	
	$laporan_form = new Form("loka_form", "", "Gudang Logistik : Rekap AU-58 Depo Logistik");
	$tanggal_from_text = new Text("loka_tanggal_from", "loka_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("loka_tanggal_to", "loka_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("loka.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("loka.print()");
	$btn_group = new ButtonGroup("noprint");
	$export_button = new Button("", "", "Unduh");
	$export_button->setClass("btn-info");
	$export_button->setIcon("fa fa-download");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setAction("loka.export_xls()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$btn_group->addButton($export_button);
	$laporan_form->addElement("", $btn_group);
	
	$loka_table = new Table(
		array("No. Mutasi", "Tanggal", "Kode Obat", "Nama Obat", "Jumlah", "Satuan", "HPP", "Total HPP", "Tgl. Exp."),
		"",
		null,
		true
	);
	$loka_table->setName("loka");
	$loka_table->setAction(false);
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_logistik/templates/template_obat_keluar.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP AU-58");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B2", "GUDANG FARMASI - " . ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("B3", "UNIT : DEPO FARMASI");
			$objWorksheet->setCellValue("B4", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$dbtable = new DBTable($db, "smis_lgs_obat_keluar");
			$data = $dbtable->get_result("
				SELECT *
				FROM (
					SELECT smis_lgs_obat_keluar.id, smis_lgs_obat_keluar.nomor, smis_lgs_obat_keluar.tanggal, smis_lgs_stok_obat.kode_obat, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat_keluar.jumlah, smis_lgs_stok_obat.satuan, smis_lgs_dobat_keluar.harga_ma AS 'hna', smis_lgs_stok_obat.tanggal_exp
					FROM ((smis_lgs_stok_obat_keluar LEFT JOIN smis_lgs_stok_obat ON smis_lgs_stok_obat_keluar.id_stok_obat = smis_lgs_stok_obat.id) LEFT JOIN smis_lgs_dobat_keluar ON smis_lgs_stok_obat_keluar.id_dobat_keluar = smis_lgs_dobat_keluar.id) LEFT JOIN smis_lgs_obat_keluar ON smis_lgs_dobat_keluar.id_obat_keluar = smis_lgs_obat_keluar.id
					WHERE smis_lgs_obat_keluar.status <> 'dikembalikan' AND smis_lgs_dobat_keluar.prop NOT LIKE 'del' AND smis_lgs_obat_keluar.unit = 'depo_logistik' AND smis_lgs_stok_obat_keluar.jumlah <> 0
				) v_loka
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "'
			");
			if (count($data) - 2 > 0)
				$objWorksheet->insertNewRowBefore(9, count($data) - 2);
			$start_row_num = 8;
			$end_row_num = 8;
			$row_num = $start_row_num;
			$no = 1;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $no++);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("date d-m-Y", $d->tanggal));
				$col_num++;
				$kode_obat = "319" . substr($d->kode_obat, 3);
				if (substr($d->kode_obat, 0, 3) == "REG")
					$kode_obat = "309" . substr($d->kode_obat, 3);
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hna);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah * $d->hna);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("date d-m-Y", $d->tanggal_exp));
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=REKAP_AU58_DEPO_FARMASI_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		class LOKAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Mutasi'] = $row->nomor;
				$array['Tanggal'] = ArrayAdapter::format("date d-m-Y", $row->tanggal);
				$kode_obat = "319" . substr($row->kode_obat, 3);
				if (substr($row->kode_obat, 0, 3) == "REG")
					$kode_obat = "309" . substr($row->kode_obat, 3);
				$array['Kode Obat'] = $kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$array['HPP'] = self::format("money Rp. ", $row->hna);
				$array['Total HPP'] = self::format("money Rp. ", $row->jumlah * $row->hna);
				$array['Tgl. Exp.'] = self::format("date d-m-Y", $row->tanggal_exp);
				return $array;
			}
		}
		$loka_adapter = new LOKAdapter();		
		$loka_dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_lgs_obat_keluar.id, smis_lgs_obat_keluar.nomor, smis_lgs_obat_keluar.tanggal, smis_lgs_stok_obat.kode_obat, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat_keluar.jumlah, smis_lgs_stok_obat.satuan, smis_lgs_dobat_keluar.harga_ma AS 'hna', smis_lgs_stok_obat.tanggal_exp
					FROM ((smis_lgs_stok_obat_keluar LEFT JOIN smis_lgs_stok_obat ON smis_lgs_stok_obat_keluar.id_stok_obat = smis_lgs_stok_obat.id) LEFT JOIN smis_lgs_dobat_keluar ON smis_lgs_stok_obat_keluar.id_dobat_keluar = smis_lgs_dobat_keluar.id) LEFT JOIN smis_lgs_obat_keluar ON smis_lgs_dobat_keluar.id_obat_keluar = smis_lgs_obat_keluar.id
					WHERE smis_lgs_obat_keluar.status <> 'dikembalikan' AND smis_lgs_dobat_keluar.prop NOT LIKE 'del' AND smis_lgs_obat_keluar.unit = 'depo_logistik' AND smis_lgs_stok_obat_keluar.jumlah <> 0
				) v_loka
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v_loka
			";
			$loka_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		class LOKDBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['tanggal_from'];
				$to = $_POST['tanggal_to'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_lgs_obat_keluar.id, smis_lgs_obat_keluar.nomor, smis_lgs_obat_keluar.tanggal, smis_lgs_stok_obat.kode_obat, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat_keluar.jumlah, smis_lgs_stok_obat.satuan, smis_lgs_dobat_keluar.harga_ma AS 'hna', smis_lgs_stok_obat.tanggal_exp
						FROM ((smis_lgs_stok_obat_keluar LEFT JOIN smis_lgs_stok_obat ON smis_lgs_stok_obat_keluar.id_stok_obat = smis_lgs_stok_obat.id) LEFT JOIN smis_lgs_dobat_keluar ON smis_lgs_stok_obat_keluar.id_dobat_keluar = smis_lgs_dobat_keluar.id) LEFT JOIN smis_lgs_obat_keluar ON smis_lgs_dobat_keluar.id_obat_keluar = smis_lgs_obat_keluar.id
						WHERE smis_lgs_obat_keluar.status <> 'dikembalikan' AND smis_lgs_dobat_keluar.prop NOT LIKE 'del' AND smis_lgs_obat_keluar.unit = 'depo_logistik' AND smis_lgs_stok_obat_keluar.jumlah <> 0
					) v_loka
					WHERE tanggal >= '" . $from . "' AND tanggal <= '" . $to . "'
				");
				$print_data = "";
				$print_data .= "<center><strong>REKAP AU-58 DEPO FARMASI</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Mutasi</th>
										<th>Kode Obat</th>
										<th>Nama Obat</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>HPP</th>
										<th>Total HPP</th>
										<th>Tgl. Exp.</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					foreach($data as $d) {
						$hna = $d->hna;
						$total_hna = $d->jumlah * $d->hna;
						$kode_obat = "319" . substr($row->kode_obat, 3);
						if (substr($d->kode_obat, 0, 3) == "REG")
							$kode_obat = "309" . substr($d->kode_obat, 3);
						$print_data .= "<tr>
											<td>" . $d->nomor . "</td>
											<td>" . $kode_obat . "</td>
											<td>" . $d->nama_obat . "</td>
											<td>" . $d->jumlah . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ",  $hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $total_hna) . "</td>
											<td>" . ArrayAdapter::format("date d-m-Y", $d->tanggal_exp) . "</td>
										</tr>";
						$total += $total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='8' align='center'><i>Tidak terdapat data mutasi obat</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='6' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
									<td>&nbsp;</td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$loka_dbresponder = new LOKDBResponder(
			$loka_dbtable,
			$loka_table,
			$loka_adapter
		);
		$data = $loka_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $loka_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOKAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOKAction.prototype.constructor = LOKAction;
	LOKAction.prototype = new TableAction();
	LOKAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#loka_tanggal_from").val();
		data['tanggal_to'] = $("#loka_tanggal_to").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOKAction.prototype.print = function() {
		if ($("#loka_tanggal_from").val() == "" || $("#loka_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['tanggal_from'] = $("#loka_tanggal_from").val();
		data['tanggal_to'] = $("#loka_tanggal_to").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	LOKAction.prototype.export_xls = function() {
		if ($("#loka_tanggal_from").val() == "" || $("#loka_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tanggal_from'] = $("#loka_tanggal_from").val();
		data['tanggal_to'] = $("#loka_tanggal_to").val();
		postForm(data);
	};
	
	var loka;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		loka = new LOKAction(
			"loka",
			"gudang_logistik",
			"laporan_obat_keluar_apotek",
			new Array()
		)
		$('.mydate').datepicker();
	});
</script>