var obat_keluar_kebun;
var dobat_keluar_kebun;
var obat;
var kebun;
var row_id;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	kebun = new KebunAction(
		"kebun",
		"gudang_logistik",
		"obat_keluar_kebun",
		new Array()
	);
	kebun.setSuperCommand("kebun");
	obat = new ObatAction(
		"obat",
		"gudang_logistik",
		"obat_keluar_kebun",
		new Array()
	);
	obat.setSuperCommand("obat");
	var dobat_keluar_kebun_columns = new Array("id", "id_obat_keluar_kebun", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "jumlah", "satuan", "konversi", "satuan_konversi");
	dobat_keluar_kebun = new DObatKeluarKebunAction(
		"dobat_keluar_kebun",
		"gudang_logistik",
		"obat_keluar_kebun",
		dobat_keluar_kebun_columns
	);
	var obat_keluar_kebun_columns = new Array("id", "tanggal", "id_kebun", "kode_kebun", "nama_kebun", "keterangan");
	obat_keluar_kebun = new ObatKeluarKebunAction(
		"obat_keluar_kebun",
		"gudang_logistik",
		"obat_keluar_kebun",
		obat_keluar_kebun_columns
	);
	obat_keluar_kebun.setSuperCommand("obat_keluar_kebun");
	obat_keluar_kebun.view();
});