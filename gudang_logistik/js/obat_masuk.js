var obat_f_masuk;
var dobat_f_masuk;
var po;
var obat;
var row_id;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$(".mydate").datepicker();
	$("#smis-chooser-modal").on("show", function() {
		if ($("#smis-chooser-modal .modal-header h3").text() == "PO") {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").addClass("full_model");
		} else if ($("#smis-chooser-modal .modal-header h3").text() == "OBAT") {
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("half_model");
		}
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("full_model");
		$("#smis-chooser-modal").removeClass("half_model");
	});
	$("#dobat_f_masuk_selisih").on("change", function() {
		if ($("#dobat_f_masuk_selisih").val() == "") {
			$("#dobat_f_masuk_selisih").val("Rp. 0,00");
		}
	});
	po = new POAction(
		"po",
		"gudang_logistik",
		"obat_reguler_masuk",
		new Array()
	);		
	po.setSuperCommand("po");
	obat = new ObatAction(
		"obat",
		"gudang_logistik",
		"obat_reguler_masuk",
		new Array()
	);		
	obat.setSuperCommand("obat");
	var dobat_f_masuk_columns = new Array("id", "kode_obat", "id_obat", "nama_obat", "medis", "inventaris", "nama_jenis_obat", "label", "hna", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "produsen", "diskon", "t_diskon", "selisih");
	dobat_f_masuk = new DObatMasukAction(
		"dobat_f_masuk",
		"gudang_logistik",
		"obat_reguler_masuk",
		dobat_f_masuk_columns
	);
	var obat_f_masuk_columns = new Array("id", "id_po", "no_opl", "no_bbm", "id_vendor", "nama_vendor", "tanggal", "no_faktur", "tanggal", "tanggal_tempo", "tanggal_datang", "diskon", "t_diskon","keterangan", "total");
	obat_f_masuk = new ObatMasukAction(
		"obat_f_masuk",
		"gudang_logistik",
		"obat_reguler_masuk",
		obat_f_masuk_columns
	);
	obat_f_masuk.setSuperCommand("obat_f_masuk");
	obat_f_masuk.view();
	
	$("#obat_f_masuk_diskon").on("change", function() {
		var diskon = $("#obat_f_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#obat_f_masuk_diskon").val("0,00");
		}
	});
	
	$("#obat_f_masuk_t_diskon").on("change", function() {
		var diskon = $("#obat_f_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#obat_f_masuk_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			$("#modal_alert_obat_f_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
				"</div>"
			);
			return;
		}
		$("#modal_alert_obat_f_masuk_add_form").html("");
		obat_f_masuk.refreshTotal();
	});
	
	$("#obat_f_masuk_diskon").on("keyup", function() {
		var diskon = $("#obat_f_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#obat_f_masuk_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			$("#modal_alert_obat_f_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
				"</div>"
			);
			return;
		} 
		$("#modal_alert_obat_f_masuk_add_form").html("");
		obat_f_masuk.refreshTotal();
	});
	
	$('#dobat_f_masuk_satuan').typeahead({
		minLength:2,
		source: function (query, process) {
			var satuan_data = {	
				page: "gudang_logistik",
				action: "obat_reguler_masuk",
				super_command: "satuan",
				command: "list",
				prototype_name: "",
				prototype_slug: "",
				prototype_implement: "",
				kriteria: $("#dobat_f_masuk_satuan").val()
			};
			var items = new Array;
			items = [""];
			$.ajax({
				url: '',
				type: 'POST',
				data: satuan_data,
				success: function(res) {
					var json = getContent(res);
					var the_data_proses = json.dbtable.data;
					items = [""];	
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.satuan,                            
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						items.push(group);
					});
					process(items);
				}
			});
		},
		updater: function (item) {
			var item = JSON.parse(item);
			$("#dobat_f_masuk_satuan").val(item.name);
			return item.name;
		}
	});
	
	$('#dobat_f_masuk_satuan_konversi').typeahead({
		minLength:2,
		source: function (query, process) {
			var satuan_data = {	
				page: "gudang_logistik",
				action: "obat_reguler_masuk",
				super_command: "satuan",
				command: "list",
				prototype_name: "",
				prototype_slug: "",
				prototype_implement: "",
				kriteria: $("#dobat_f_masuk_satuan_konversi").val()
			};
			var items = new Array;
			items = [""];
			$.ajax({
				url: '',
				type: 'POST',
				data: satuan_data,
				success: function(res) {
					var json = getContent(res);
					var the_data_proses = json.dbtable.data;
					items = [""];	
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.satuan,                            
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						items.push(group);
					});
					process(items);
				}
			});
		},
		updater: function (item) {
			var item = JSON.parse(item);
			$("#dobat_f_masuk_satuan_konversi").val(item.name);
			return item.name;
		}
	});
	
	$('#dobat_f_masuk_produsen').typeahead({
		minLength:2,
		source: function (query, process) {
			var produsen_data = {	
				page: "gudang_logistik",
				action: "obat_reguler_masuk",
				super_command: "produsen",
				command: "list",
				prototype_name: "",
				prototype_slug: "",
				prototype_implement: "",
				kriteria: $("#dobat_f_masuk_produsen").val()
			};
			var items = new Array;
			items = [""];
			$.ajax({
				url: '',
				type: 'POST',
				data: produsen_data,
				success: function(res) {
					var json = getContent(res);
					var the_data_proses = json.dbtable.data;
					items = [""];	
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.produsen,                            
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						items.push(group);
					});
					process(items);
				}
			});
		},
		updater: function (item) {
			var item = JSON.parse(item);
			$("#dobat_f_masuk_produsen").val(item.name);
			return item.name;
		}
	});
});