function POAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
POAction.prototype.constructor = POAction;
POAction.prototype = new TableAction();
POAction.prototype.selected = function(json) {
	$("#obat_f_masuk_nopo").val(json.id);
	$("#obat_f_masuk_no_opl").val(json.nomor_surat);
	$("#obat_f_masuk_id_vendor").val(json.id_vendor);
	$("#obat_f_masuk_nama_vendor").val(json.nama_vendor);
	$("tbody#dobat_f_masuk_list").children().remove();
	$("#obat_f_masuk_nopo").removeClass("error_field");
	$("#modal_alert_obat_f_masuk_add_form").html("");
};