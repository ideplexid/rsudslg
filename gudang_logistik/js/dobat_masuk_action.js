function DObatMasukAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DObatMasukAction.prototype.constructor = DObatMasukAction;
DObatMasukAction.prototype = new TableAction();
DObatMasukAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#dobat_f_masuk_nama_obat").val();
	var jumlah_tercatat = $("#dobat_f_masuk_jumlah_tercatat").val();
	var jumlah = $("#dobat_f_masuk_jumlah").val();
	var satuan = $("#dobat_f_masuk_satuan").val();
	var konversi = $("#dobat_f_masuk_konversi").val();
	var satuan_konversi = $("#dobat_f_masuk_satuan_konversi").val();
	var produsen = $("#dobat_f_masuk_produsen").val();
	var diskon = $("#dobat_f_masuk_diskon").val();
	var t_diskon = $("#dobat_f_masuk_t_diskon").val();
	var ed = $("#dobat_f_masuk_ed").val();
	var nobatch = $("#dobat_f_masuk_nobatch").val();
	$(".error_field").removeClass("error_field");
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#dobat_f_masuk_nama_obat").addClass("error_field");
	}
	if (produsen == "") {
		valid = false;
		invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
		$("#dobat_f_masuk_produsen").addClass("error_field");
	}
	if (jumlah_tercatat == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> tidak boleh kosong";
		$("#dobat_f_masuk_jumlah_tercatat").addClass("error_field");
	} else if (!is_numeric(jumlah_tercatat)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> seharusnya numerik (0-9)";
		$("#dobat_f_masuk_jumlah_tercatat").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#dobat_f_masuk_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#dobat_f_masuk_jumlah").addClass("error_field");
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#dobat_f_masuk_satuan").addClass("error_field");
	}
	if (konversi == "") {
		valid = false;
		invalid_msg += "</br><strong>Jml. Terkecil</strong> tidak boleh kosong";
		$("#dobat_f_masuk_konversi").addClass("error_field");
	} else if (!is_numeric(konversi)) {
		valid = false;
		invalid_msg += "</br><strong>Jml. Terkecil</strong> seharusnya numerik (0-9)";
		$("#dobat_f_masuk_konversi").addClass("error_field");
	}
	if (satuan_konversi == "") {
		valid = false;
		invalid_msg += "</br><strong>Stn. Terkecil</strong> tidak boleh kosong";
		$("#dobat_f_masuk_satuan_konversi").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#dobat_f_masuk_diskon").addClass("error_field");
	} else {
		diskon = parseFloat(diskon);
		if (isNaN(diskon)) {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> seharusnya numerik (0-9)";
			$("#dobat_r_masuk_diskon").addClass("error_field");
		} else {
			if (t_diskon == "persen" && diskon > 100) {
				valid = false;
				invalid_msg += "</br><strong>Diskon</strong> seharusnya bernilai 0-100 %";
				$("#dobat_f_masuk_diskon").addClass("error_field");
			} 
			if (diskon < 0) {
				valid = false;
				invalid_msg += "</br><strong>Diskon</strong> seharusnya bernilai >= 0";
				$("#dobat_f_masuk_diskon").addClass("error_field");
			}
		}
	}
	if (t_diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
		$("#dobat_f_masuk_t_diskon").addClass("error_field");
	}
	if (ed == "") {
		valid = false;
		invalid_msg += "</br><strong>Tgl. Exp.</strong> tidak boleh kosong";
		$("#dobat_f_masuk_ed").addClass("error_field");
	}
	if (nobatch == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Batch</strong> tidak boleh kosong";
		$("#dobat_f_masuk_nobatch").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_dobat_f_masuk_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DObatMasukAction.prototype.show_add_form = function() {
	if ($("#obat_f_masuk_nopo").val() == "") {
		$(".error_field").removeClass("error_field");
		$("#modal_alert_obat_f_masuk_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				"</br><strong>No. SP</strong> tidak boleh kosong" +
			"</div>"
		);
		$("#obat_f_masuk_nopo").addClass("error_field");
		return;
	}
	$("#dobat_f_masuk_id").val("");
	$("#dobat_f_masuk_id_obat").val("");
	$("#dobat_f_masuk_kode_obat").val("");
	$("#dobat_f_masuk_nama_obat").val("");
	$("#dobat_f_masuk_medis").val("");
	$("#dobat_f_masuk_inventaris").val("");
	$("#dobat_f_masuk_nama_jenis_obat").val("");
	$("#dobat_id_dpo").val("");
	$("#dobat_f_masuk_stok_terkini").val("");
	$("#dobat_f_masuk_jumlah_tercatat").val("");
	$("#dobat_f_masuk_jumlah").val("");
	$("#dobat_f_masuk_satuan").val("");
	$("#dobat_f_masuk_konversi").val("");
	$("#dobat_f_masuk_satuan_konversi").val("");
	$("#dobat_f_masuk_hna").val("");
	$("#dobat_f_masuk_selisih").val("Rp. 0,00");
	$("#dobat_f_masuk_produsen").val("");
	$("#dobat_f_masuk_diskon").val(0);
	$("#dobat_f_masuk_t_diskon").val("persen");
	$("#dobat_f_masuk_ed").val("");
	$("#dobat_f_masuk_nobatch").val("");
	$("#modal_alert_dobat_f_masuk_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dobat_f_masuk_save").removeAttr("onclick");
	$("#dobat_f_masuk_save").attr("onclick", "dobat_f_masuk.save()");
	$("#dobat_f_masuk_add_form").smodal("show");
};
DObatMasukAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	var id = $("#dobat_f_masuk_id").val();
	var id_obat = $("#dobat_f_masuk_id_obat").val();
	var kode_obat = $("#dobat_f_masuk_kode_obat").val();
	var nama_obat = $("#dobat_f_masuk_nama_obat").val();
	var medis = $("#dobat_f_masuk_medis").val();
	var inventaris = $("#dobat_f_masuk_inventaris").val();
	var nama_jenis_obat = $("#dobat_f_masuk_nama_jenis_obat").val();
	var id_dpo = $("#dobat_f_masuk_id_dpo").val();
	var hna = $("#dobat_f_masuk_hna").val();
	var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var selisih = $("#dobat_f_masuk_selisih").val();
	var v_selisih = parseFloat(selisih.replace(/[^0-9-,]/g, '').replace(",", "."));
	var produsen = $("#dobat_f_masuk_produsen").val();
	var stok_terkini = $("#dobat_f_masuk_stok_terkini").val();
	var jumlah_tercatat = $("#dobat_f_masuk_jumlah_tercatat").val();
	var jumlah = $("#dobat_f_masuk_jumlah").val();
	var subtotal = "Rp. " + ((v_hna + v_selisih) * parseFloat(jumlah)).formatMoney("2", ".", ",");
	var sisa = $("#dobat_f_masuk_jumlah").val();
	var satuan = $("#dobat_f_masuk_satuan").val();
	var konversi = $("#dobat_f_masuk_konversi").val();
	var satuan_konversi = $("#dobat_f_masuk_satuan_konversi").val();
	var diskon = $("#dobat_f_masuk_diskon").val();
	var t_diskon = $("#dobat_f_masuk_t_diskon").val();
	var f_diskon = diskon + " %";
	if (t_diskon == "nominal")
		f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
	var ed = $("#dobat_f_masuk_ed").val();
	var nobatch = $("#dobat_f_masuk_nobatch").val();
	$("tbody#dobat_f_masuk_list").append(
		"<tr id='data_" + row_id + "'>" +
			"<td id='data_" + row_id + "_id' style='display: none;'>" + id + "</td>" +
			"<td id='data_" + row_id + "_id_obat' style='display: none;'>" + id_obat + "</td>" +
			"<td id='data_" + row_id + "_kode_obat' style='display: none;'>" + kode_obat + "</td>" +
			"<td id='data_" + row_id + "_medis' style='display: none;'>" + medis + "</td>" +
			"<td id='data_" + row_id + "_inventaris' style='display: none;'>" + inventaris + "</td>" +
			"<td id='data_" + row_id + "_id_dpo' style='display: none;'>" + id_dpo + "</td>" +
			"<td id='data_" + row_id + "_hna' style='display: none;'>" + hna + "</td>" +
			"<td id='data_" + row_id + "_selisih' style='display: none;'>" + selisih + "</td>" +
			"<td id='data_" + row_id + "_diskon' style='display: none;'>" + diskon + "</td>" +
			"<td id='data_" + row_id + "_t_diskon' style='display: none;'>" + t_diskon + "</td>" +
			"<td id='data_" + row_id + "_stok_terkini' style='display: none;'>" + stok_terkini + "</td>" +
			"<td id='data_" + row_id + "_jumlah_tercatat' style='display: none;'>" + jumlah_tercatat + "</td>" +
			"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + jumlah + "</td>" +
			"<td id='data_" + row_id + "_sisa' style='display: none;'>" + sisa + "</td>" +
			"<td id='data_" + row_id + "_satuan' style='display: none;'>" + satuan + "</td>" +
			"<td id='data_" + row_id + "_konversi' style='display: none;'>" + konversi + "</td>" +
			"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
			"<td id='data_" + row_id + "_nama_obat'>" + nama_obat + "</td>" +
			"<td id='data_" + row_id + "_nama_jenis_obat'>" + nama_jenis_obat + "</td>" +
			"<td id='data_" + row_id + "_produsen'>" + produsen + "</td>" +
			"<td id='data_" + row_id + "_f_subtotal'>" + jumlah + " x (" + hna + " + " + selisih + ") = " + subtotal + "</td>" +
			"<td id='data_" + row_id + "_f_jumlah'>" + jumlah + " " + satuan + "</td>" +
			"<td id='data_" + row_id + "_f_konversi'>1 " + satuan + " = " + konversi + " " + satuan_konversi + "</td>" +
			"<td id='data_" + row_id + "_f_diskon'>" + f_diskon + "</td>" +
			"<td id='data_" + row_id + "_ed'>" + ed + "</td>" +
			"<td id='data_" + row_id + "_nobatch'>" + nobatch + "</td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dobat_f_masuk.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dobat_f_masuk.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	row_id++;
	obat_f_masuk.refreshNoDObatMasuk();
	obat_f_masuk.refreshTotal();
	$("#dobat_f_masuk_add_form").smodal("hide");
};
DObatMasukAction.prototype.edit = function(r_num) {
	var id = $("#data_" + r_num + "_id").text();
	var id_obat = $("#data_" + r_num + "_id_obat").text();
	var kode_obat = $("#data_" + r_num + "_kode_obat").text();
	var nama_obat = $("#data_" + r_num + "_nama_obat").text();
	var medis = $("#data_" + r_num + "_medis").text();
	var inventaris = $("#data_" + r_num + "_inventaris").text();
	var nama_jenis_obat = $("#data_" + r_num + "_nama_jenis_obat").text();
	var id_dpo = $("#data_" + r_num + "_id_dpo").text();
	var stok_terkini = $("#data_" + r_num + "_stok_terkini").text();
	var jumlah_tercatat = $("#data_" + r_num + "_jumlah_tercatat").text();
	var jumlah = $("#data_" + r_num + "_jumlah").text();
	var satuan = $("#data_" + r_num + "_satuan").text();
	var konversi = $("#data_" + r_num + "_konversi").text();
	var satuan_konversi = $("#data_" + r_num + "_satuan_konversi").text();
	var hna = $("#data_" + r_num + "_hna").text();
	var selisih = $("#data_" + r_num + "_selisih").text();
	var produsen = $("#data_" + r_num + "_produsen").text();
	var diskon = $("#data_" + r_num + "_diskon").text();
	var t_diskon = $("#data_" + r_num + "_t_diskon").text();
	var ed = $("#data_" + r_num + "_ed").text();
	var nobatch = $("#data_" + r_num + "_nobatch").text();
	$("#dobat_f_masuk_id").val(id);
	$("#dobat_f_masuk_id_obat").val(id_obat);
	$("#dobat_f_masuk_kode_obat").val(kode_obat);
	$("#dobat_f_masuk_nama_obat").val(nama_obat);
	$("#dobat_f_masuk_medis").val(medis);
	$("#dobat_f_masuk_inventaris").val(inventaris);
	$("#dobat_f_masuk_nama_jenis_obat").val(nama_jenis_obat);
	$("#dobat_f_masuk_id_dpo").val(id_dpo);
	$("#dobat_f_masuk_stok_terkini").val(stok_terkini);
	$("#dobat_f_masuk_jumlah_tercatat").val(jumlah_tercatat);
	$("#dobat_f_masuk_jumlah").val(jumlah);
	$("#dobat_f_masuk_satuan").val(satuan);
	$("#dobat_f_masuk_konversi").val(konversi);
	$("#dobat_f_masuk_satuan_konversi").val(satuan_konversi);
	$("#dobat_f_masuk_hna").val(hna);
	$("#dobat_f_masuk_selisih").val(selisih);
	$("#dobat_f_masuk_produsen").val(produsen);
	$("#dobat_f_masuk_diskon").val(diskon);
	$("#dobat_f_masuk_t_diskon").val(t_diskon);
	$("#dobat_f_masuk_ed").val(ed);
	$("#dobat_f_masuk_nobatch").val(nobatch);
	$("#modal_alert_dobat_f_masuk_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dobat_f_masuk_save").removeAttr("onclick");
	$("#dobat_f_masuk_save").attr("onclick", "dobat_f_masuk.update(" + r_num + ")");
	$("#dobat_f_masuk_add_form").smodal("show");
};
DObatMasukAction.prototype.update = function(r_num) {
	if (!this.validate()) {
		return;
	}
	var id = $("#dobat_f_masuk_id").val();
	var id_obat = $("#dobat_f_masuk_id_obat").val();
	var kode_obat = $("#dobat_f_masuk_kode_obat").val();
	var nama_obat = $("#dobat_f_masuk_nama_obat").val();
	var medis = $("#dobat_f_masuk_medis").val();
	var inventaris = $("#dobat_f_masuk_inventaris").val();
	var nama_jenis_obat = $("#dobat_f_masuk_nama_jenis_obat").val();
	var id_dpo = $("#dobat_f_masuk_id_dpo").val();
	var hna = $("#dobat_f_masuk_hna").val();
	var produsen = $("#dobat_f_masuk_produsen").val();
	var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var selisih = $("#dobat_f_masuk_selisih").val();
	var v_selisih = parseFloat(selisih.replace(/[^0-9-,]/g, '').replace(",", "."));
	var stok_terkini = $("#dobat_f_masuk_stok_terkini").val();
	var jumlah_tercatat = $("#dobat_f_masuk_jumlah_tercatat").val();
	var jumlah = $("#dobat_f_masuk_jumlah").val();
	var subtotal = "Rp. " + ((v_hna + v_selisih) * parseFloat(jumlah)).formatMoney("2", ".", ",");
	var sisa = $("#dobat_f_masuk_jumlah").val();
	var satuan = $("#dobat_f_masuk_satuan").val();
	var konversi = $("#dobat_f_masuk_konversi").val();
	var satuan_konversi = $("#dobat_f_masuk_satuan_konversi").val();
	var diskon = $("#dobat_f_masuk_diskon").val();
	var t_diskon = $("#dobat_f_masuk_t_diskon").val();
	var f_diskon = diskon + " %";
	if (t_diskon == "nominal")
		f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
	var ed = $("#dobat_f_masuk_ed").val();
	var nobatch = $("#dobat_f_masuk_nobatch").val();
	$("#data_" + r_num + "_id").text(id);
	$("#data_" + r_num + "_id_obat").text(id_obat);
	$("#data_" + r_num + "_kode_obat").text(kode_obat);
	$("#data_" + r_num + "_nama_obat").text(nama_obat);
	$("#data_" + r_num + "_medis").text(medis);
	$("#data_" + r_num + "_inventaris").text(inventaris);
	$("#data_" + r_num + "_nama_jenis_obat").text(nama_jenis_obat);
	$("#data_" + r_num + "_id_dpo").text(id_dpo);
	$("#data_" + r_num + "_hna").text(hna);
	$("#data_" + r_num + "_selisih").text(selisih);
	$("#data_" + r_num + "_diskon").text(diskon);
	$("#data_" + r_num + "_t_diskon").text(t_diskon);
	$("#data_" + r_num + "_stok_terkini").text(stok_terkini);
	$("#data_" + r_num + "_jumlah_tercatat").text(jumlah_tercatat);
	$("#data_" + r_num + "_jumlah").text(jumlah);
	$("#data_" + r_num + "_sisa").text(sisa);
	$("#data_" + r_num + "_satuan").text(satuan);
	$("#data_" + r_num + "_konversi").text(konversi);
	$("#data_" + r_num + "_satuan_konversi").text(satuan_konversi);
	$("#data_" + r_num + "_produsen").text(produsen);
	$("#data_" + r_num + "_f_subtotal").text(jumlah + " x (" + hna + " + " + selisih + ") = " + subtotal);
	$("#data_" + r_num + "_f_jumlah").text(jumlah + " " + satuan);
	$("#data_" + r_num + "_f_konversi").text("1 " + satuan + " = " + konversi + " " + satuan_konversi);
	$("#data_" + r_num + "_f_diskon").text(f_diskon);
	$("#data_" + r_num + "_ed").text(ed);
	$("#data_" + r_num + "_nobatch").text(nobatch);
	obat_f_masuk.refreshTotal();
	$("#dobat_f_masuk_add_form").smodal("hide");
};
DObatMasukAction.prototype.delete = function(r_num) {
	var id = $("#data_" + r_num + "_id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	obat_f_masuk.refreshNoDObatMasuk();
	obat_f_masuk.refreshTotal();
};