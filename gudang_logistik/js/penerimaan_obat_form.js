var penerimaan_obat;
var dpenerimaan_obat;
var opl;
var row_num;
var previous_use_ppn;

$(document).ready(function() {
	$(".mydate").datepicker();
	opl = new PenerimaanObatOPLAction(
		"opl",
		"gudang_logistik",
		"penerimaan_obat_form",
		new Array()
	);
	opl.setSuperCommand("opl");
	dpenerimaan_obat = new DPenerimaanObatAction(
		"dpenerimaan_obat",
		"gudang_logistik",
		"penerimaan_obat_form",
		new Array("hpp", "hna", "diskon")
	);
	penerimaan_obat = new PenerimaanObatAction(
		"penerimaan_obat",
		"gudang_logistik",
		"penerimaan_obat_form",
		new Array("diskon", "materai")
	);

	var id = $("#penerimaan_obat_id").val();
	row_num = 0;
	penerimaan_obat.get_footer();
	penerimaan_obat.show_detail_form(id);
	dpenerimaan_obat.setEditMode("false");
	$("#opl_browse").focus();

	$("#penerimaan_obat_diskon").on("change", function() {
		var diskon = $("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#penerimaan_obat_diskon").val("0,00");
		}
	});
	
	$("#penerimaan_obat_t_diskon").on("change", function() {
		var diskon = $("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#penerimaan_obat_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			bootbox.alert("<b>Diskon</b> tidak boleh melebihi 100%");
			return;
		}
		penerimaan_obat.update_total();
	});	
	$("#penerimaan_obat_diskon").on("keyup", function() {
		var diskon = $("#penerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#penerimaan_obat_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			bootbox.alert("<b>Diskon</b> tidak boleh melebihi 100%");
			return;
		}
		penerimaan_obat.update_total();
	});
	$("#penerimaan_obat_no_bbm").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_no_faktur").focus();
		}
	});
	$("#penerimaan_obat_no_faktur").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_tanggal").focus();
		}
	});
	$("#penerimaan_obat_tanggal").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_tanggal_datang").focus();
		}
	});
	$("#penerimaan_obat_tanggal_datang").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_tanggal_tempo").focus();
		}
	});
	$("#penerimaan_obat_tanggal_tempo").keypress(function(e) {
		if (e.which == 13) {
			$("#penerimaan_obat_diskon").focus();
			$("div.datepicker").hide();
		}
	});
	$("#penerimaan_obat_use_ppn").on("focus", function() {
		previous_use_ppn = this.value;
	}).change(function() {
		var hna = $("#dpenerimaan_obat_hpp").val();
		var hpp = $("#dpenerimaan_obat_hna").val();
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if (hna != "") {
			hna = parseFloat($("#dpenerimaan_obat_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			hpp = hna * 1.1;
			if (use_ppn == 0)
				hpp = hna;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hna").val(f_hpp);
		} else if (hpp != "") {
			hpp = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			hna = hpp / 1.1;
			if (use_ppn == 0)
				hna = hpp;
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hpp").val(f_hna);
		}
		var num_rows = $("tbody#dpenerimaan_obat_list").children("tr").length;
		for (var i = 0; i < num_rows; i++) {
			var r_hna = parseFloat($("tbody#dpenerimaan_obat_list tr:eq(" + i + ") td#hna").text());
			
		}
		console.log("sesudah : " + use_ppn + " | sebelum : " + previous_use_ppn);
		penerimaan_obat.update_total();
	});

	$("#dpenerimaan_obat_diskon").on("change", function() {
		var diskon = $("#dpenerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#dpenerimaan_obat_diskon").val("0,00");
		}
	});
	$("#dpenerimaan_obat_jumlah_tercatat").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_jumlah").focus();
		}
	});
	$("#dpenerimaan_obat_jumlah").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_hpp").focus();
		}
	});
	$("#dpenerimaan_obat_hpp").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_hna").focus();
		}
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hpp").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hna = parseFloat($("#dpenerimaan_obat_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hpp = hna * 1.1;
			if (use_ppn == false)
				hpp = hna;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hna").val(f_hpp);
		}
	});
	$("#dpenerimaan_obat_hna").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_diskon").focus();
		}
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hna").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hpp = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hna = hpp / 1.1;
			if (use_ppn == false)
				hna = hpp;
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hpp").val(f_hna);
		}
	});
	$("#dpenerimaan_obat_hpp").on("change", function() {
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hpp").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hna = parseFloat($("#dpenerimaan_obat_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hpp = hna * 1.1;
			if (use_ppn == false)
				hpp = hna;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hna").val(f_hpp);
		}
	});
	$("#dpenerimaan_obat_hna").on("change", function() {
		var use_ppn = $("#penerimaan_obat_use_ppn").val();
		if ($("#dpenerimaan_obat_hna").val() == "") {
			$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
			$("#dpenerimaan_obat_hna").val("Rp. 0,00");
		} else {
			var hpp = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var hna = hpp / 1.1;
			if (use_ppn == false)
				hna = hpp;
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpenerimaan_obat_hpp").val(f_hna);
		}
	});
	$("#dpenerimaan_obat_diskon").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_t_diskon").focus();
		}
	});
	$("#dpenerimaan_obat_t_diskon").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_produsen").focus();
		}
	});
	$("#dpenerimaan_obat_produsen").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_tanggal_exp").focus();
		}
	});
	$("#dpenerimaan_obat_tanggal_exp").keypress(function(e) {
		if (e.which == 13) {
			$("#dpenerimaan_obat_no_batch").focus();
			$("div.datepicker").hide();
		}
	});
});