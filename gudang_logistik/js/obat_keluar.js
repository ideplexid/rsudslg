var obat_keluar;
var dobat_keluar;
var obat;
var row_id;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$("#obat_keluar_max").val(50);
	$(".mydate").datepicker();
	obat = new ObatAction(
		"obat",
		"gudang_logistik",
		"obat_keluar",
		new Array()
	);
	obat.setSuperCommand("obat");
	var dobat_keluar_columns = new Array("id", "id_obat_keluar", "id_obat", "kode_obat", "nama_obat", "nama_jenis_obat", "harga_ma", "stok_entri", "jumlah_diminta", "jumlah", "satuan", "konversi", "satuan_konversi");
	dobat_keluar = new DObatKeluarAction(
		"dobat_keluar",
		"gudang_logistik",
		"obat_keluar",
		dobat_keluar_columns
	);
	var obat_keluar_columns = new Array("id", "nomor", "tanggal", "unit", "status", "keterangan");
	obat_keluar = new ObatKeluarAction(
		"obat_keluar",
		"gudang_logistik",
		"obat_keluar",
		obat_keluar_columns
	);
	obat_keluar.setSuperCommand("obat_keluar");
	obat_keluar.view();

	var barang_data = obat.getViewData();
	$("#dobat_keluar_nama_obat").typeahead({
		minLength	: 3,
		source		: function (query, process) {
			var $items = new Array;
			$items = [""];                
			barang_data['kriteria'] = $('#dobat_keluar_nama_obat').val();
			$.ajax({
				url		: "",
				type	: "POST",
				data	: barang_data,
				success	: function(response) {
					var json = getContent(response);
					var t_data = json.dbtable.data;
					$items = [""];      				
					$.map(t_data, function(data) {
						var group;
						group = {
							id		: data.id,
							name	: data.nama_obat, 
							kode	: data.kode_obat, 
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = "";
								value +=  this.kode + " - " + this.name;
								if(typeof(this.level) != "undefined") {
									value += " <span class='pull-right muted'>";
									value += this.level;
									value += "</span>";
								}
								return String.prototype.replace.apply("<div class='typeaheadiv'>" + value + "</div>", arguments);
							}
						};
						$items.push(group);
					});
					process($items);
				}
			});
		},
		updater		: function (item) {
			var item = JSON.parse(item);  
			obat.select(item.id);
			$("#dobat_keluar_nama_obat").focus();       
			return item.name;
		}
	});

	$("#dobat_keluar_nama_obat").keypress(function(e) {
		if(e.which == 13) {
			$("#dobat_keluar_").focus();
		}
	});
});