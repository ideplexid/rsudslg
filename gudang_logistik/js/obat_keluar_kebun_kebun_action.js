function KebunAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
KebunAction.prototype.constructor = KebunAction;
KebunAction.prototype = new TableAction();
KebunAction.prototype.selected = function(json) {
	$("#obat_keluar_kebun_id_kebun").val(json.id);
	$("#obat_keluar_kebun_kode_kebun").val(json.kode);
	$("#obat_keluar_kebun_nama_kebun").val(json.nama);
};