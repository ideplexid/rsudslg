function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	$("#dobat_keluar_kebun_id_obat").val(json.header.id_obat);
	$("#dobat_keluar_kebun_kode_obat").val(json.header.kode_obat);
	$("#dobat_keluar_kebun_nama_obat").val(json.header.nama_obat);
	$("#dobat_keluar_kebun_nama_jenis_obat").val(json.header.nama_jenis_obat);
	$("#dobat_keluar_kebun_satuan").html(json.satuan_option);
	this.setDetailInfo();
};
ObatAction.prototype.setDetailInfo = function() {
	var part = $("#dobat_keluar_kebun_satuan").val().split("_");
	$("#dobat_keluar_kebun_konversi").val(part[0]);
	$("#dobat_keluar_kebun_satuan_konversi").val(part[1]);
	var data = this.getRegulerData();
	data['super_command'] = "sisa";
	data['command'] = "edit";
	data['id_obat'] = $("#dobat_keluar_kebun_id_obat").val();
	data['satuan'] = $("#dobat_keluar_kebun_satuan").find(":selected").text();
	data['konversi'] = $("#dobat_keluar_kebun_konversi").val();
	data['satuan_konversi'] = $("#dobat_keluar_kebun_satuan_konversi").val();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#dobat_keluar_kebun_stok").val(json.sisa);
			$("#dobat_keluar_kebun_f_stok").val(json.sisa + " " + json.satuan);
		}
	);
};