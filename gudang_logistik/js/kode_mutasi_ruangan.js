var kode_mutasi_ruangan;

$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var kode_mutasi_ruangan_columns = new Array("id", "kode", "slug_ruangan", "nama_ruangan");
	kode_mutasi_ruangan = new TableAction(
		"kode_mutasi_ruangan",
		"gudang_logistik",
		"kode_mutasi_ruangan",
		kode_mutasi_ruangan_columns
	);
	kode_mutasi_ruangan.view();
});