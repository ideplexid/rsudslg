function ObatKeluarAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatKeluarAction.prototype.constructor = ObatKeluarAction;
ObatKeluarAction.prototype = new TableAction();
ObatKeluarAction.prototype.refreshNumber = function() {
	var no = 1;
	var nor_dobat_keluar = $("tbody#dobat_keluar_list").children("tr").length;
	for(var i = 0; i < nor_dobat_keluar; i++) {
		var dr_prefix = $("tbody#dobat_keluar_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html("<div align='right'>" + no + ".</div>");
		no++;
	}
};
ObatKeluarAction.prototype.show_add_form = function() {
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "get_auto_number_and_date";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			row_id = 0;
			$("#obat_keluar_id").val("");
			$("#obat_keluar_nomor").val(json.nomor);
			$("#obat_keluar_tanggal").val(json.tanggal);
			$("#obat_keluar_tanggal").removeAttr("disabled");
			$("#obat_keluar_unit").val("");
			$("#obat_keluar_unit").removeAttr("disabled");
			$("#dobat_keluar_list").children().remove();
			$("#dobat_keluar_add").show();
			$("#modal_alert_obat_keluar_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_keluar_save").removeAttr("onclick");
			$("#obat_keluar_save").attr("onclick", "obat_keluar.save()");
			$("#obat_keluar_save").show();
			$("#obat_keluar_ok").hide();
			$("#obat_keluar_add_form").smodal("show");
		}
	);
};
ObatKeluarAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nomor = $("#obat_keluar_nomor").val();
	var tanggal = $("#obat_keluar_tanggal").val();
	var unit = $("#obat_keluar_unit").find(":selected").text();
	var nord = $("#dobat_keluar_list").children().length;
	$(".error_field").removeClass("error_field");
	if (nomor == "") {
		valid = false;
		invalid_msg += "<br/><strong>Kode</strong> tidak boleh kosong";
		$("#obat_keluar_nomor").addClass("error_field");
	}
	if (tanggal == "") {
		valid = false;
		invalid_msg += "<br/><strong>Tanggal</strong> tidak boleh kosong";
		$("#obat_keluar_tanggal").addClass("error_field");
	}
	if (unit == "") {
		valid = false;
		invalid_msg += "<br/><strong>Unit</strong> tidak boleh kosong";
		$("#obat_keluar_unit").addClass("error_field");
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "<br/><strong>Detail</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_obat_keluar_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ObatKeluarAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	$("#obat_keluar_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "save";
	data['push_command'] = "save";
	data['id'] = "";
	data['tanggal'] = $("#obat_keluar_tanggal").val();
	data['unit'] = $("#obat_keluar_unit").val();
	data['nomor'] = $("#obat_keluar_nomor").val();
	data['status'] = "belum";
	data['keterangan'] = "";
	var detail = {};
	var nor = $("tbody#dobat_keluar_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		var prefix = $("tbody#dobat_keluar_list").children('tr').eq(i).prop("id");
		d_data['id'] = $("#" + prefix + "_id").text();
		d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
		d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
		d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
		d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
		d_data['stok_entri'] = $("#" + prefix + "_stok_entri").text();
		d_data['harga_ma'] = $("#" + prefix + "_harga_ma").text();
		d_data['jumlah_diminta'] = $("#" + prefix + "_jumlah_diminta").text();
		d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
		d_data['satuan'] = $("#" + prefix + "_satuan").text();
		d_data['konversi'] = $("#" + prefix + "_konversi").text();
		d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
		detail[i] = d_data;
	}
	data['detail'] = detail;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) { 
				$("#obat_keluar_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ObatKeluarAction.prototype.copy_header = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "edit";
	data['id'] = id;
	$.post(	
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_keluar_id").val("");
			$("#obat_keluar_nomor").val(json.header.nomor);
			$("#obat_keluar_nomor").removeAttr("disabled");
			$("#obat_keluar_nomor").attr("disabled", "disabled");
			$("#obat_keluar_tanggal").val(json.header.tanggal);
			$("#obat_keluar_tanggal").removeAttr("disabled");
			$("#obat_keluar_tanggal").attr("disabled", "disabled");
			$("#obat_keluar_unit").val(json.header.unit);
			$("#obat_keluar_unit").removeAttr("disabled");
			$("#obat_keluar_unit").attr("disabled", "disabled");
			$("#dobat_keluar_add").show();
			row_id = 0;
			$("#dobat_keluar_list").children("tr").remove();
			$("#modal_alert_obat_keluar_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_keluar_save").removeAttr("onclick");
			$("#obat_keluar_save").attr("onclick", "obat_keluar.save()");
			$("#obat_keluar_save").show();
			$("#obat_keluar_ok").hide();
			$("#obat_keluar_add_form").smodal("show");
		}
	);
};
ObatKeluarAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "edit";
	data['id'] = id;
	$.post(	
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_keluar_id").val(id);
			$("#obat_keluar_nomor").val(json.header.nomor);
			$("#obat_keluar_tanggal").val(json.header.tanggal);
			$("#obat_keluar_tanggal").removeAttr("disabled");
			$("#obat_keluar_tanggal").attr("disabled", "disabled");
			$("#obat_keluar_unit").val(json.header.unit);
			$("#obat_keluar_unit").removeAttr("disabled");
			$("#obat_keluar_unit").attr("disabled", "disabled");
			$("#dobat_keluar_add").hide();
			row_id = 0;
			$("#dobat_keluar_list").children("tr").remove();
			for(var i = 0; i < json.detail.length; i++) {
				var dobat_keluar_id = json.detail[i].id;
				var dobat_keluar_id_obat_keluar = json.detail[i].id_obat_keluar;
				var dobat_keluar_id_obat = json.detail[i].id_obat;
				var dobat_keluar_kode_obat = json.detail[i].kode_obat;
				var dobat_keluar_nama_obat = json.detail[i].nama_obat;
				var dobat_keluar_nama_jenis_obat = json.detail[i].nama_jenis_obat;
				var dobat_keluar_harga_ma = json.detail[i].harga_ma;
				var dobat_keluar_stok_entri = json.detail[i].stok_entri;
				var dobat_keluar_jumlah_diminta = json.detail[i].jumlah_diminta;
				var dobat_keluar_jumlah = json.detail[i].jumlah;
				var dobat_keluar_satuan = json.detail[i].satuan;
				var dobat_keluar_konversi = json.detail[i].konversi;
				var dobat_keluar_satuan_konversi = json.detail[i].satuan_konversi;
				$("tbody#dobat_keluar_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td style='display: none;' id='data_" + row_id + "_id'>" + dobat_keluar_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_kode_obat'>" + dobat_keluar_kode_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_id_obat'>" + dobat_keluar_id_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_harga_ma'>" + dobat_keluar_harga_ma + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_stok_entri'>" + dobat_keluar_stok_entri + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_diminta'>" + dobat_keluar_jumlah_diminta + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dobat_keluar_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dobat_keluar_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dobat_keluar_satuan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dobat_keluar_konversi + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dobat_keluar_satuan_konversi + "</td>" +
						"<td id='data_" + row_id + "_nomor'></td>" +
						"<td id='data_" + row_id + "_nama_obat'>" + dobat_keluar_nama_obat + "</td>" +
						"<td id='data_" + row_id + "_nama_jenis_obat'>" + dobat_keluar_nama_jenis_obat + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah_diminta'>" + dobat_keluar_jumlah_diminta + " " + dobat_keluar_satuan + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + dobat_keluar_jumlah + " " + dobat_keluar_satuan + "</td>" +
						"<td id='data_" + row_id + "_keterangan'>1 " + dobat_keluar_satuan + " = " + dobat_keluar_konversi + " " + dobat_keluar_satuan_konversi + "</td>" +
						"<td></td>" +
					"</tr>"
				);
				row_id++;
			}
			obat_keluar.refreshNumber();
			$("#modal_alert_obat_keluar_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_keluar_save").removeAttr("onclick");
			$("#obat_keluar_save").hide();
			$("#obat_keluar_ok").show();
			$("#obat_keluar_add_form").smodal("show");
		}
	);
};
ObatKeluarAction.prototype.del = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "del";
	data['id'] = id;
	data['push_command'] = "delete";
	bootbox.confirm(
		"Anda yakin menghapus data ini?", 
		function(result) {
			if(result) {
				showLoading();
				$.post(
					'',
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
ObatKeluarAction.prototype.print_mutasi = function(id, status) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "print_mutasi";
	data['id'] = id;
	data['status'] = status;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			smis_print(json);
		}
	);
};
ObatKeluarAction.prototype.download_au58 = function(id) {
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "export_au58";
	data['id'] = id;
	postForm(data);
};
ObatKeluarAction.prototype.download_au58_recap = function(nomor) {
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar";
	data['command'] = "export_au58_nomor";
	data['nomor'] = nomor;
	postForm(data);
};