<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;
	
	$laporan_form = new Form("lmompp_form", "", "Gudang Logistik : Laporan Monitoring Penerimaan Obat");
	$tanggal_from_text = new Text("lmompp_tanggal_from", "lmompp_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lmompp_tanggal_to", "lmompp_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$tipe_option = new OptionBuilder();
	$tipe_option->add("SEMUA", "%%");
	$tipe_option->add("REGULER", "logistik", "1");
	$tipe_option->add("SITO", "sito");
	$tipe_option->add("KONSINYASI", "konsinyasi");
	$tipe_select = new Select("lmompp_tipe", "lmompp_tipe", $tipe_option->getContent());
	$laporan_form->addElement("Jns. Faktur", $tipe_select);
	$filter_option = new OptionBuilder();
	$filter_option->add("MELIPUTI", "IN");
	$filter_option->add("SELAIN", "NOT IN", "1");
	$filter_vendor_select = new Select("lmompp_filter_vendor", "lmompp_filter_vendor", $filter_option->getContent());
	$laporan_form->addElement("Fil. Vendor", $filter_vendor_select);
	$filter_produsen_select = new Select("lmompp_filter_produsen", "lmompp_filter_produsen", $filter_option->getContent());
	$laporan_form->addElement("Fil. Prod.", $filter_produsen_select);
	
	class VendorTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup('noprint');
			$btn = new Button($this->name . "_add", "", "Add");
			$btn->setAction("vendor_m.chooser('vendor_m', 'vendor_m_button', 'vendor_m', vendor_m)");
			$btn->setClass("btn-primary");
			$btn->setIcon("fa fa-plus");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button($this->name . "_clear", "", "Clear");
			$btn->setAction("vendor_m.clear()");
			$btn->setClass("btn-danger");
			$btn->setIcon("fa fa-trash");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$vendor_list_table = new VendorTable(
		array("Vendor"),
		"",
		null,
		true
	);
	$vendor_list_table->setName("vendor_lm");
	$vendor_list_table->setFooterVisible(false);
	
	class ProdusenTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup('noprint');
			$btn = new Button($this->name . "_add", "", "Add");
			$btn->setAction("produsen_m.chooser('produsen_m', 'produsen_m_button', 'produsen_m', produsen_m)");
			$btn->setClass("btn-primary");
			$btn->setIcon("fa fa-plus");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button($this->name . "_clear", "", "Clear");
			$btn->setAction("produsen_m.clear()");
			$btn->setClass("btn-danger");
			$btn->setIcon("fa fa-trash");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group->getHtml();
		}
	}
	$produsen_list_table = new ProdusenTable(
		array("Produsen"),
		"",
		null,
		true
	);
	$produsen_list_table->setName("produsen_lm");
	$produsen_list_table->setFooterVisible(false);
	
	$lmompp_table = new Table(
		array("Tgl. Masuk", "Nama Obat", "Produsen", "Vendor", "Jumlah", "Satuan", "Harga Satuan", "Total H. Satuan"),
		"",
		null,
		true
	);
	$lmompp_table->setName("lmompp");
	$lmompp_table->setAction(false);
	
	//get_daftar_vendor_consumer:
	$vendor_table = new Table(
		array("Nama", "NPWP", "Alamat", "No. Telp."),
		"",
		null,
		true
	);
	$vendor_table->setName("vendor_m");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter();
	$vendor_adapter->add("Nama", "nama");
	$vendor_adapter->add("NPWP", "npwp");
	$vendor_adapter->add("Alamat", "alamat");
	$vendor_adapter->add("No. Telp.", "telpon");
	$vendor_service_responder = new ServiceResponder(
		$db,
		$vendor_table,
		$vendor_adapter,
		"get_daftar_vendor"
	);
	
	//chooser produsen:
	$produsen_m_table = new Table(
		array("Produsen"),
		"",
		null,
		true
	);
	$produsen_m_table->setName("produsen_m");
	$produsen_m_table->setModel(Table::$SELECT);
	$produsen_m_adapter = new SimpleAdapter();
	$produsen_m_adapter->add("Produsen", "produsen");
	$produsen_m_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
	$produsen_m_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (produsen LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT produsen AS 'id', produsen
		FROM (
			SELECT DISTINCT smis_lgs_dobat_f_masuk.produsen
			FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
			WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
		) v_produsen
		WHERE " . $filter . "
		ORDER BY produsen ASC
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT smis_lgs_dobat_f_masuk.produsen
			FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
			WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del'
		) v_produsen
		WHERE " . $filter . "
		ORDER BY produsen ASC
	";
	$produsen_m_dbtable->setPreferredQuery(true, $query_value, $query_count);
	class ProdusenMDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['id'] = $id;
			$data['produsen'] = $id;
			return $data;
		}
	}
	$produsen_m_dbresponder = new ProdusenMDBResponder(
		$produsen_m_dbtable,
		$produsen_m_table,
		$produsen_m_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("vendor_m", $vendor_service_responder);
	$super_command->addResponder("produsen_m", $produsen_m_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Tgl. Masuk'] = self::format("date d M Y", $row->tanggal_datang);
				$array['Nama Obat'] = $row->nama_obat;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$harga_satuan = 100 * $row->hna / 110;
				$array['Harga Satuan'] = self::format("money Rp. ", $harga_satuan);
				$array['Total H. Satuan'] = self::format("money Rp. ", $row->jumlah * $harga_satuan);
				return $array;
			}
		}
		$lmompp_adapter = new LLSOAdapter();		
		$lmompp_dbtable = new DBTable($db, "smis_lgs_dobat_f_masuk");
		$filter_produsen = " NOT IN ('') ";
		if (isset($_POST['produsen'])) {
			$produsens = $_POST['produsen'];
			if (count($produsens) > 0) {
				$filter_produsen = " " . $_POST['filter_produsen'] . " ('" . implode("','", $produsens) . "') ";
			}
		}
		$filter_vendor = " NOT IN ('-1') ";
		if (isset($_POST['vendor'])) {
			$vendors = $_POST['vendor'];
			if (count($vendors) > 0) {
				$filter_vendor = " " . $_POST['filter_vendor'] . " (" . implode(',', $vendors) . ") ";
			}
		}
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR produsen LIKE '%" . $_POST['kriteria'] . "%' OR nama_vendor LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_dobat_f_masuk.id, smis_lgs_dobat_f_masuk.nama_obat, smis_lgs_obat_f_masuk.nama_vendor, SUM(smis_lgs_dobat_f_masuk.jumlah) AS 'jumlah', smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
				FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
				WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.produsen " . $filter_produsen . " AND smis_lgs_obat_f_masuk.id_vendor " . $filter_vendor . " AND smis_lgs_obat_f_masuk.tanggal >= '" . $_POST['tanggal_from'] . "' AND smis_lgs_obat_f_masuk.tanggal <= '" . $_POST['tanggal_to'] . "' AND smis_lgs_obat_f_masuk.tipe LIKE '" . $_POST['tipe'] . "'
				GROUP BY smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_obat_f_masuk.nama_vendor, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
			) v_lmompp
			WHERE " . $filter . "
			ORDER BY tanggal_datang, nama_obat ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_dobat_f_masuk.id, smis_lgs_dobat_f_masuk.nama_obat, smis_lgs_obat_f_masuk.nama_vendor, SUM(smis_lgs_dobat_f_masuk.jumlah) AS 'jumlah', smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
				FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
				WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.produsen " . $filter_produsen . " AND smis_lgs_obat_f_masuk.id_vendor " . $filter_vendor . " AND smis_lgs_obat_f_masuk.tanggal >= '" . $_POST['tanggal_from'] . "' AND smis_lgs_obat_f_masuk.tanggal <= '" . $_POST['tanggal_to'] . "' AND smis_lgs_obat_f_masuk.tipe LIKE '" . $_POST['tipe'] . "'
				GROUP BY smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_obat_f_masuk.nama_vendor, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
			) v_lmompp
			WHERE " . $filter . "
			ORDER BY tanggal_datang, nama_obat ASC
		";
		$lmompp_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['tanggal_from'];
				$to = $_POST['tanggal_to'];
				$filter_produsen = " NOT IN ('') ";
				if (isset($_POST['produsen'])) {
					$produsens = $_POST['produsen'];
					if (count($produsens) > 0) {
						$filter_produsen = " " . $_POST['filter_produsen'] . " ('" . implode("','", $produsens) . "') ";
					}
				}
				$filter_vendor = " NOT IN (-1) ";
				if (isset($_POST['vendor'])) {
					$vendors = $_POST['vendor'];
					if (count($vendors) > 0) {
						$filter_vendor = " " . $_POST['filter_vendor'] . " (" . implode(',', $vendors) . ") ";
					}
				}
				$tipe = $_POST['tipe'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_dobat_f_masuk.id, smis_lgs_dobat_f_masuk.nama_obat, smis_lgs_obat_f_masuk.nama_vendor, SUM(smis_lgs_dobat_f_masuk.jumlah) AS 'jumlah', smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
						FROM smis_lgs_dobat_f_masuk LEFT JOIN smis_lgs_obat_f_masuk ON smis_lgs_dobat_f_masuk.id_obat_f_masuk = smis_lgs_obat_f_masuk.id
						WHERE smis_lgs_obat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.prop NOT LIKE 'del' AND smis_lgs_dobat_f_masuk.produsen " . $filter_produsen . " AND smis_lgs_obat_f_masuk.id_vendor " . $filter_vendor . " AND smis_lgs_obat_f_masuk.tanggal >= '" . $from . "' AND smis_lgs_obat_f_masuk.tanggal <= '" . $to . "' AND smis_lgs_obat_f_masuk.tipe LIKE '" . $tipe . "'
						GROUP BY smis_lgs_obat_f_masuk.tanggal_datang, smis_lgs_dobat_f_masuk.id_obat, smis_lgs_obat_f_masuk.nama_vendor, smis_lgs_dobat_f_masuk.satuan, smis_lgs_dobat_f_masuk.hna, smis_lgs_dobat_f_masuk.produsen
					) v_lmompp
					ORDER BY tanggal_datang, nama_obat ASC
				");
				if ($tipe == "logistik") {
					$tipe = "reguler";
				} else if ($tipe == "%%") {
					$tipe = "semua";
				}
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN MONITORING OBAT MASUK PER PRODUSEN</strong></center><br/>";
				$print_data .= "<table border='0'>";
				if (isset($_POST['nama_vendor'])) {
					$nama_vendors = $_POST['nama_vendor'];
					if (count($nama_vendors) > 0) {
						if ($_POST['filter_vendor'] == "IN") {
							$print_data .= "<tr>
												<td>Vendor</td>
												<td>:</td>
												<td>" . implode(', ', $nama_vendors) . "</td>
											</tr>";
						} else {
							$print_data .= "<tr>
												<td>Vendor</td>
												<td>:</td>
												<td><strong>SELAIN</strong> " . implode(', ', $nama_vendors) . "</td>
											</tr>";
						}
					}
				}
				if (isset($_POST['produsen'])) {
					if (count($produsens) > 0) {
						if ($_POST['filter_produsen'] == "IN") {
							$print_data .= "<tr>
												<td>Produsen</td>
												<td>:</td>
												<td>" . implode(', ', $produsens) . "</td>
											</tr>";
						} else {
							$print_data .= "<tr>
												<td>Produsen</td>
												<td>:</td>
												<td><strong>SELAIN</strong> " . implode(', ', $produsens) . "</td>
											</tr>";
						}
					}
				}
				$print_data .= "	<tr>
										<td>Jns. Penerimaan</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $tipe) . "</td>
									</tr>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>Tgl. Masuk</th>
										<th>Nama Obat</th>
										<th>Produsen</th>
										<th>Vendor</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>Harga Satuan</th>
										<th>Total H. Satuan</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$harga_satuan = 100 * $d->hna / 110;
						$print_data .= "<tr>
											<td>" . ArrayAdapter::format("date d M Y", $d->tanggal_datang) . "</td>
											<td>" . $d->nama_obat . "</td>
											<td>" . $d->produsen . "</td>
											<td>" . $d->nama_vendor . "</td>
											<td>" . $d->jumlah . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $harga_satuan) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->jumlah * $harga_satuan) . "</td>
										</tr>";
						$total += ($d->jumlah * $harga_satuan);
					}
				} else {
					$print_data .= "<tr>
										<td colspan='8' align='center'><i>Tidak terdapat data obat masuk</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='7' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lmompp_dbresponder = new LLSODBResponder(
			$lmompp_dbtable,
			$lmompp_table,
			$lmompp_adapter
		);
		$data = $lmompp_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lmompp.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lmompp.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>" .
				 "<div class='span6'>" .
					 $vendor_list_table->getHtml() .
				 "</div>" .
				 "<div class='span6'>" .
					 $produsen_list_table->getHtml() .
				 "</div>" .
			 "</div>" .
			 "<div class='row-fluid'>" .
				 $btn_group->getHtml() .
			 "</div>" .
		 "</div>";
	echo $lmompp_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOMPPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOMPPAction.prototype.constructor = LOMPPAction;
	LOMPPAction.prototype = new TableAction();
	LOMPPAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		var vendor_m_list = new Array();
		var nor_vendor_m =  $("tbody#vendor_lm_list").children("tr").length;
		for (var i = 0; i < nor_vendor_m; i++) {
			var vendor_m_prefix = $("tbody#vendor_lm_list").children("tr").eq(i).prop("id");
			var id_vendor = $("#" + vendor_m_prefix + "_id").text();
			vendor_m_list.push(id_vendor);
		}
		data['vendor'] = vendor_m_list;
		var produsen_m_list = new Array();
		var nor_produsen_m =  $("tbody#produsen_lm_list").children("tr").length;
		for (var i = 0; i < nor_produsen_m; i++) {
			var produsen_m_prefix = $("tbody#produsen_lm_list").children("tr").eq(i).prop("id");
			var produsen = $("#" + produsen_m_prefix + "_produsen").text();
			produsen_m_list.push(produsen);
		}
		data['produsen'] = produsen_m_list;
		data['tanggal_from'] = $("#lmompp_tanggal_from").val();
		data['tanggal_to'] = $("#lmompp_tanggal_to").val();
		data['tipe'] = $("#lmompp_tipe").val();
		data['filter_vendor'] = $("#lmompp_filter_vendor").val();
		data['filter_produsen'] = $("#lmompp_filter_produsen").val();
		console.log(data);
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOMPPAction.prototype.print = function() {
		if ($("#lmompp_tanggal_from").val() == "" || $("#lmompp_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		var vendor_m_list = new Array();
		var nama_vendor_m_list = new Array();
		var nor_vendor_m =  $("tbody#vendor_lm_list").children("tr").length;
		for (var i = 0; i < nor_vendor_m; i++) {
			var vendor_m_prefix = $("tbody#vendor_lm_list").children("tr").eq(i).prop("id");
			var id_vendor = $("#" + vendor_m_prefix + "_id").text();
			var nama_vendor = $("#" + vendor_m_prefix + "_vendor").text();
			vendor_m_list.push(id_vendor);
			nama_vendor_m_list.push(nama_vendor);
		}
		data['vendor'] = vendor_m_list;
		data['nama_vendor'] = nama_vendor_m_list;
		var produsen_m_list = new Array();
		var nor_produsen_m =  $("tbody#produsen_lm_list").children("tr").length;
		for (var i = 0; i < nor_produsen_m; i++) {
			var produsen_m_prefix = $("tbody#produsen_lm_list").children("tr").eq(i).prop("id");
			var produsen = $("#" + produsen_m_prefix + "_produsen").text();
			produsen_m_list.push(produsen);
		}
		data['produsen'] = produsen_m_list;
		data['tanggal_from'] = $("#lmompp_tanggal_from").val();
		data['tanggal_to'] = $("#lmompp_tanggal_to").val();
		data['tipe'] = $("#lmompp_tipe").val();
		data['filter_vendor'] = $("#lmompp_filter_vendor").val();
		data['filter_produsen'] = $("#lmompp_filter_produsen").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	function VendorMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	VendorMAction.prototype.constructor = VendorMAction;
	VendorMAction.prototype = new TableAction();
	VendorMAction.prototype.selected = function(json) {
		$("tbody#vendor_lm_list").append(
			"<tr id='vendor_lm_" + vendor_m_num + "'>" +
				"<td id='vendor_lm_" + vendor_m_num + "_id' style='display: none;'>" + json.id + "</td>" +
				"<td id='vendor_lm_" + vendor_m_num + "_vendor'>" + json.nama + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='vendor_m.delete(" + vendor_m_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" +
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		vendor_m_num++;
	};
	VendorMAction.prototype.clear = function() {
		$("#vendor_lm_list").empty();
	};
	VendorMAction.prototype.delete = function(r_num) {
		$("#vendor_lm_" + r_num).remove();
	};
	
	function ProdusenMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ProdusenMAction.prototype.constructor = ProdusenMAction;
	ProdusenMAction.prototype = new TableAction();
	ProdusenMAction.prototype.selected = function(json) {
		$("tbody#produsen_lm_list").append(
			"<tr id='produsen_lm_" + produsen_m_num + "'>" +
				"<td id='produsen_lm_" + produsen_m_num + "_id' style='display: none;'>" + json.produsen + "</td>" +
				"<td id='produsen_lm_" + produsen_m_num + "_produsen'>" + json.produsen + "</td>" +
				"<td>" +
					"<div class='btn-group noprint'>" +
						"<a href='#' onclick='produsen_m.delete(" + produsen_m_num + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
							"<i class='icon-remove icon-white'></i>" + 
						"</a>" +
					"</div>" +
				"</td>" +
			"</tr>"
		);
		produsen_m_num++;
	};
	ProdusenMAction.prototype.clear = function() {
		$("#produsen_lm_list").empty();
	};
	ProdusenMAction.prototype.delete = function(r_num) {
		$("#produsen_lm_" + r_num).remove();
	};
	
	var lmompp;
	var produsen_list;
	var vendor_m;
	var vendor_m_num;
	var produsen_m;
	var produsen_m_num;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "VENDOR_M") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("full_model");
			} else if ($("#smis-chooser-modal .modal-header h3").text() == "PRODUSEN_M") {
				$("#smis-chooser-modal").removeClass("half_model");
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("half_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").removeClass("full_model");
		});
		vendor_m_num = 0;
		produsen_m_num = 0;
		vendor_m = new VendorMAction(
			"vendor_m",
			"gudang_logistik",
			"laporan_monitoring_obat_masuk_per_produsen",
			new Array()
		);
		vendor_m.setSuperCommand("vendor_m");
		produsen_m = new ProdusenMAction(
			"produsen_m",
			"gudang_logistik",
			"laporan_monitoring_obat_masuk_per_produsen",
			new Array()
		);
		produsen_m.setSuperCommand("produsen_m");
		lmompp = new LOMPPAction(
			"lmompp",
			"gudang_logistik",
			"laporan_monitoring_obat_masuk_per_produsen",
			new Array()
		)
	});
</script>