<?php 
global $db;
global $user;
show_error();
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$query_count="
			SELECT count(*) as total FROM (SELECT DISTINCT a.nama_obat, a.nama_jenis_obat, a.satuan, b.id_obat
			FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
			WHERE a.prop NOT LIKE 'del' AND a.konversi = 1 AND a.label = 'reguler'
			ORDER BY a.nama_obat, a.nama_jenis_obat, a.satuan ASC)X";
	
	$content=$db->get_var($query_count);
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array("ID", "Kode", "Nama","Satuan","Harga");
$uitable=new Table($header);
$uitable->setName("broadcast_obat")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$query="SELECT DISTINCT a.kode_obat, a.nama_obat, a.nama_jenis_obat, a.satuan, b.id_obat
	FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
	WHERE a.prop NOT LIKE 'del' AND a.konversi = 1 AND a.label = 'reguler'
	ORDER BY a.nama_obat, a.nama_jenis_obat, a.satuan ASC LIMIT ".$_POST['limit_start'].",1";
	
	$one=$db->get_row($query,true);
	$service=new ServiceConsumer($db, "push_nama_obat");
	$service->addData("id_obat", $one->id_obat);
	$service->addData("nama", $one->nama_obat);
	$service->addData("satuan", $one->satuan);
	$service->addData("kategori", $one->nama_jenis_obat);
	
	$query_harga="
			SELECT MAX(a.hna) AS 'max_hpp'
			FROM smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id
			WHERE b.id_obat = '" . $one->id_obat . "' AND a.satuan = '" . $one->satuan . "' AND a.satuan_konversi = '" . $one->satuan . "' AND a.konversi = 1
			GROUP BY b.id_obat, a.satuan, a.konversi, a.satuan_konversi
			";
	$harga= $db->get_var($query_harga);
	$service->addData("harga", $harga);
	$service->execute();
	
	$content=array();
	$one_c=array();
	$one_c['ID']=ArrayAdapter::format("digit8", $one->id_obat);
	$one_c['Kode']=$one->kode_obat;
	$one_c['Nama']=$one->nama_obat;
	$one_c['Satuan']=$one->satuan;
	$one_c['Harga']=ArrayAdapter::format("money Rp.", $harga);
	$content[]=$one_c;
	
	$uitable->setContent($content);
	$ctx=$uitable->getBodyContent();
	
	$pack=new ResponsePackage();
	$pack->setContent($ctx);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("broadcast_obat.rekaptotal()");
$btn_group=new ButtonGroup("");
$btn_group->addButton($action);
$form=$uitable
	  ->getModal()
	  ->setTitle("Gudang Logistik")
	  ->getForm()
	  ->addElement("",$btn_group);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("broadcast_obat.batal()");

$load=new LoadingBar("broadcast_obat_bar", "");
$modal=new Modal("broadcast_obat_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_broadcast_obat'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var broadcast_obat;
	var broadcast_obat_karyawan;
	var broadcast_obat_data;
	var IS_broadcast_obat_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		broadcast_obat=new TableAction("broadcast_obat","gudang_logistik","broadcast_obat",new Array());
		
		broadcast_obat.batal=function(){
			IS_broadcast_obat_RUNNING=false;
			$("#broadcast_obat_modal").modal("hide");
		};
		
	
		broadcast_obat.rekaptotal=function(){
			if(IS_broadcast_obat_RUNNING) return;
			$("#broadcast_obat_bar").sload("true","Fetching total data",0);
			$("#broadcast_obat_modal").modal("show");
			IS_broadcast_obat_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total>0) {
					$("#broadcast_obat_list").html("");
					broadcast_obat.rekaploop(0,total);
				} else {
					$("#broadcast_obat_modal").modal("hide");
					IS_broadcast_obat_RUNNING=false;
				}
			});
		};

		broadcast_obat.rekaploop=function(current,total){
			$("#broadcast_obat_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
			if(current>=total || !IS_broadcast_obat_RUNNING) {
				$("#broadcast_obat_modal").modal("hide");
				IS_broadcast_obat_RUNNING=false;
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['limit_start']=current;			
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#broadcast_obat_list").append(ct);
				setTimeout(function(){broadcast_obat.rekaploop(++current,total)},50);
			});
		};
				
	});
</script>