<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("gudang_logistik/kelas/GudangLogistikInventory.php");
	global $db;

	$laporan_form = new Form("", "", "Gudang Logistik : Stok Opname (SO)");
	$tanggal_from_text = new Text("lpo_tanggal_from", "lpo_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lpo_tanggal_to", "lpo_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$jenis_filter_option = new OptionBuilder();
	$jenis_filter_option->add("SEMUA", "semua", "1");
	$jenis_filter_option->add("PER OBAT", "per_obat");
	$jenis_filter_option->add("PER JENIS", "per_jenis");
	$jenis_filter_select = new Select("lpo_jenis_filter", "lpo_jenis_filter", $jenis_filter_option->getContent());
	$laporan_form->addElement("Jenis Filter", $jenis_filter_select);
	$kode_jenis_obat_hidden = new Hidden("lpo_kode_jenis_obat", "lpo_kode_jenis_obat", "");
	$laporan_form->addElement("", $kode_jenis_obat_hidden);
	$id_obat_hidden = new Hidden("lpo_id_obat", "lpo_id_obat", "");
	$laporan_form->addElement("", $id_obat_hidden);
	$nama_obat_text = new Text("lpo_nama_obat", "lpo_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("lpo_obat.chooser('lpo_obat', 'lpo_obat_button', 'lpo_obat', lpo_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Obat", $input_group);
	$nama_jenis_obat_text = new Text("lpo_nama_jenis_obat", "lpo_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$nama_jenis_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("lpo_jenis_obat.chooser('lpo_jenis_obat', 'lpo_jenis_obat_button', 'lpo_jenis_obat', lpo_jenis_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_jenis_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Jenis Obat", $input_group);
	$urutan_option = new OptionBuilder();
	$urutan_option->addSingle("KODE OBAT", "1");
	$urutan_option->addSingle("NAMA OBAT");
	$urutan_select = new Select("lpo_urutan", "lpo_urutan", $urutan_option->getContent());
	$laporan_form->addElement("Urutan", $urutan_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lpo.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$lpo_table = new Table(
		array("No.", "ID Obat", "Kode Obat", "Nama Obat", "Saldo Awal", "Penyesuaian", "Jml. Masuk", "Jml. Keluar", "Saldo Akhir", "Harga Pokok", "Nilai Pemakaian", "Nilai Akhir"),
		"",
		null,
		true
	);
	$lpo_table->setName("lpo");
	$lpo_table->setAction(false);
	$lpo_table->setFooterVisible(false);

	//chooser nama obat:
	$obat_table = new Table(
		array("ID", "Kode", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("lpo_obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("ID", "id");
	$obat_adapter->add("Kode", "kode");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_dbtable = new DBTable($db, "smis_pr_barang");
	$obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	//chooser jenis obat:
	$jenis_obat_table = new Table(
		array("No.", "Kode", "Jenis Obat"),
		"",
		null,
		true
	);
	$jenis_obat_table->setName("lpo_jenis_obat");
	$jenis_obat_table->setModel(Table::$SELECT);
	$jenis_obat_adapter = new SimpleAdapter(true, "No.");
	$jenis_obat_adapter->add("Kode", "kode");
	$jenis_obat_adapter->add("Jenis Obat", "nama");
	$jenis_obat_dbtable = new DBTable($db, "smis_pr_jenis_barang");
	$jenis_obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$jenis_obat_dbresponder = new DBResponder(
		$jenis_obat_dbtable,
		$jenis_obat_table,
		$jenis_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("lpo_obat", $obat_dbresponder);
	$super_command->addResponder("lpo_jenis_obat", $jenis_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah_obat") {
			$jenis_filter = $_POST['jenis_filter'];
			$params = array();
			$params['order_by'] = $_POST['urutan'];
			if ($jenis_filter == "per_obat")
				$params['filter_id_obat'] = $_POST['id_obat'];
			else
				$params['filter_id_obat'] = "%%";
			if ($jenis_filter == "per_jenis")
				$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
			else
				$params['filter_kode_jenis_obat'] = "%%";
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jumlah_obat_msrs",
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah = 0;
			if ($content != null)
				$jumlah = $content[0];
			$data = array();
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$num = $_POST['num'];
			$params = array();
			$params['num'] = $_POST['num'];
			$params['order_by'] = $_POST['urutan'];
			if ($jenis_filter == "per_obat")
				$params['filter_id_obat'] = $_POST['id_obat'];
			else
				$params['filter_id_obat'] = "%%";
			if ($jenis_filter == "per_jenis")
				$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
			else
				$params['filter_kode_jenis_obat'] = "%%";
			$consumer_service = new ServiceConsumer(
				$db,
				"get_obat_info_msrs",
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$id_obat = $content[0];
			$kode_obat = $content[1];
			$nama_obat = $content[2];
			$nama_jenis_obat = $content[3];
			$satuan = $content[5];
			$satuan_konversi = $content[5];

			// Mendapatkan Saldo Akhir:
			$saldo_sekarang = GudangLogistikInventory::getCurrentStock($db, $id_obat, $satuan, 1, $satuan_konversi);
			$jumlah_masuk = GudangLogistikInventory::getStockIn($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + GudangLogistikInventory::getPenyesuaianStokPositif($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
			$jumlah_keluar = GudangLogistikInventory::getStockOut($db, $id_obat, $satuan, 1, $satuan_konversi,date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + GudangLogistikInventory::getPenyesuaianStokNegatif($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
			$saldo_akhir = $saldo_sekarang - $jumlah_masuk + $jumlah_keluar;
			// Mendapatkan Penyesuaian Stok:
			$penyesuaian_stok = GudangLogistikInventory::getPenyesuaianStokPositif($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to) - GudangLogistikInventory::getPenyesuaianStokNegatif($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
			// Mendapatkan Jumlah Masuk:
			$jumlah_masuk = GudangLogistikInventory::getStockIn($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
			// Mendapatkan Jumlah Keluar:
			$jumlah_keluar = GudangLogistikInventory::getStockOut($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
			// Mendapatkan Saldo Awal:
			$saldo_awal = $saldo_akhir - $jumlah_masuk + $jumlah_keluar - $penyesuaian_stok;
			// Mendapatkan Harga Pokok:
			$harga_pokok = GudangLogistikInventory::getStockValue($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_to);
			$f_harga_pokok = $harga_pokok == 0 ? ArrayAdapter::format("money Rp. ", "0") : ArrayAdapter::format("money Rp. ", $harga_pokok);
			// Nilai Pemakaian:
			$nilai_pemakaian = $jumlah_keluar * $harga_pokok;
			$f_nilai_pemakaian = $nilai_pemakaian == 0 ? ArrayAdapter::format("money Rp. ", "0") : ArrayAdapter::format("money Rp. ", $nilai_pemakaian);
			// Nilai Akhir:
			$nilai_akhir = $saldo_akhir * $harga_pokok;
			$f_nilai_akhir = $nilai_akhir == 0 ? ArrayAdapter::format("money Rp. ", "0") : ArrayAdapter::format("money Rp. ", $nilai_akhir);

			$html = "
				<tr>
					<td id='nomor'></td>
					<td id='id_obat'><small>" .  $id_obat . "</small></td>
					<td id='kode_obat'><small>" .  $kode_obat . "</small></td>
					<td id='nama_obat'><small>" .  $nama_obat . "</small></td>
					<td id='saldo_awal' style='display: none;'><small>" .  $saldo_awal . "</small></td>
					<td id='f_saldo_awal'><small>" .  ArrayAdapter::format("number", $saldo_awal) . "</small></td>
					<td id='penyesuaian_stok' style='display: none;'><small>" .  $penyesuaian_stok . "</small></td>
					<td id='f_penyesuaian_stok'><small>" .  ArrayAdapter::format("number", $penyesuaian_stok) . "</small></td>
					<td id='jumlah_masuk' style='display: none;'><small>" .  $jumlah_masuk . "</small></td>
					<td id='f_jumlah_masuk'><small>" .  ArrayAdapter::format("number", $jumlah_masuk) . "</small></td>
					<td id='jumlah_keluar' style='display: none;'><small>" .  $jumlah_keluar . "</small></td>
					<td id='f_jumlah_keluar'><small>" .  ArrayAdapter::format("number", $jumlah_keluar) . "</small></td>
					<td id='saldo_akhir' style='display: none;'><small>" .  $saldo_akhir . "</small></td>
					<td id='f_saldo_akhir'><small>" .  ArrayAdapter::format("number", $saldo_akhir) . "</small></td>
					<td id='harga_pokok' style='display: none;'><small>" .  $harga_pokok . "</small></td>
					<td id='f_harga_pokok'><small>" .  $f_harga_pokok . "</small></td>
					<td id='nilai_pemakaian' style='display: none;'><small>" .  $nilai_pemakaian . "</small></td>
					<td id='f_nilai_pemakaian'><small>" .  $f_nilai_pemakaian . "</small></td>
					<td id='nilai_akhir' style='display: none;'><small>" .  $nilai_akhir . "</small></td>
					<td id='f_nilai_akhir'><small>" .  $f_nilai_akhir . "</small></td>
				</tr>
			";

			$data = array();
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['kode_obat'] = $kode_obat;
			$data['nama_obat'] = $nama_obat;
			$data['nama_jenis_obat'] = $nama_jenis_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_logistik/templates/template_persediaan_obat.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("STOK OPNAME");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B2", "GUDANG FARMASI - " . ArrayAdapter::format("unslug", $nama_entitas));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->saldo_awal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->penyesuaian_stok);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_masuk);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_keluar);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->saldo_akhir);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga_pokok);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nilai_pemakaian);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nilai_akhir);
				$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("L" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("M" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=STOK_OPNAME_GUDANG_FARMASI_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("so_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lpo.cancel()");
	$loading_modal = new Modal("loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lpo_table->getHtml();
	echo "</div>";
	echo "<div id='info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LPOAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LPOAction.prototype.constructor = LPOAction;
	LPOAction.prototype = new TableAction();
	LPOAction.prototype.view = function() {
		if ($("#lpo_tanggal_from").val() == "" || $("#lpo_tanggal_to").val() == "")
			return;
		var self = this;
		$("#info").empty();
		$("#loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#loading_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_jumlah_obat";
		data['jenis_filter'] = $("#lpo_jenis_filter").val();
		data['id_obat'] = $("#lpo_id_obat").val();
		data['nama_obat'] = $("#lpo_nama_obat").val();
		data['nama_jenis_obat'] = $("#lpo_nama_jenis_obat").val();
		data['kode_jenis_obat'] = $("#lpo_kode_jenis_obat").val();
		data['urutan'] = $("#lpo_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#lpo_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	LPOAction.prototype.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize();
			} else {
				$("#loading_modal").smodal("hide");
				$("#info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info_obat";
		data['num'] = num;
		data['tanggal_from'] = $("#lpo_tanggal_from").val();
		data['tanggal_to'] = $("#lpo_tanggal_to").val();
		data['jenis_filter'] = $("#lpo_jenis_filter").val();
		data['id_obat'] = $("#lpo_id_obat").val();
		data['nama_obat'] = $("#lpo_nama_obat").val();
		data['nama_jenis_obat'] = $("#lpo_nama_jenis_obat").val();
		data['kode_jenis_obat'] = $("#lpo_kode_jenis_obat").val();
		data['urutan'] = $("#lpo_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("tbody#lpo_list").append(
					json.html
				);
				$("#so_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	LPOAction.prototype.finalize = function() {
		var num_rows = $("tbody#lpo_list tr").length;
		for (var i = 0; i < num_rows; i++)
			$("tbody#lpo_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
		$("#loading_modal").smodal("hide");
		$("#info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#export_button").removeAttr("onclick");
		$("#export_button").attr("onclick", "lpo.export_xls()");
	};
	LPOAction.prototype.cancel = function() {
		FINISHED = true;
	};
	LPOAction.prototype.export_xls = function() {
		showLoading();
		var num_rows = $("#lpo_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var nomor = $("tbody#lpo_list tr:eq(" + i + ") td#nomor").text();
			var id_obat = $("tbody#lpo_list tr:eq(" + i + ") td#id_obat").text();
			var kode_obat = $("tbody#lpo_list tr:eq(" + i + ") td#kode_obat").text();
			var nama_obat = $("tbody#lpo_list tr:eq(" + i + ") td#nama_obat").text();
			var saldo_awal = $("tbody#lpo_list tr:eq(" + i + ") td#saldo_awal").text();
			var penyesuaian_stok = $("tbody#lpo_list tr:eq(" + i + ") td#penyesuaian_stok").text();
			var jumlah_masuk = $("tbody#lpo_list tr:eq(" + i + ") td#jumlah_masuk").text();
			var jumlah_keluar = $("tbody#lpo_list tr:eq(" + i + ") td#jumlah_keluar").text();
			var saldo_akhir = $("tbody#lpo_list tr:eq(" + i + ") td#saldo_akhir").text();
			var harga_pokok = $("tbody#lpo_list tr:eq(" + i + ") td#harga_pokok").text();
			var nilai_pemakaian = $("tbody#lpo_list tr:eq(" + i + ") td#nilai_pemakaian").text();
			var nilai_akhir = $("tbody#lpo_list tr:eq(" + i + ") td#nilai_akhir").text();
			d_data[i] = {
				"nomor" 			: nomor,
				"id_obat" 			: id_obat,
				"kode_obat" 		: kode_obat,
				"nama_obat" 		: nama_obat,
				"saldo_awal" 		: saldo_awal,
				"penyesuaian_stok"	: penyesuaian_stok,
				"jumlah_masuk" 		: jumlah_masuk,
				"jumlah_keluar" 	: jumlah_keluar,
				"saldo_akhir" 		: saldo_akhir,
				"harga_pokok"		: harga_pokok,
				"nilai_pemakaian"	: nilai_pemakaian,
				"nilai_akhir"		: nilai_akhir
			};
		}
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tanggal_from'] = $("#lpo_tanggal_from").val();
		data['tanggal_to'] = $("#lpo_tanggal_to").val();
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		postForm(data);
		dismissLoading();
	};

	function LPOObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LPOObatAction.prototype.constructor = LPOObatAction;
	LPOObatAction.prototype = new TableAction();
	LPOObatAction.prototype.selected = function(json) {
		$("#lpo_id_obat").val(json.id);
		$("#lpo_nama_obat").val(json.nama);
	};
	
	function LPOJenisObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LPOJenisObatAction.prototype.constructor = LPOJenisObatAction;
	LPOJenisObatAction.prototype = new TableAction();
	LPOJenisObatAction.prototype.selected = function(json) {
		$("#lpo_kode_jenis_obat").val(json.kode);
		$("#lpo_nama_jenis_obat").val(json.nama);
	};

	var lpo;
	var lpo_obat;
	var lpo_jenis_obat;
	var FINISHED;
	$(document).ready(function() {
		lpo_obat = new LPOObatAction(
			"lpo_obat",
			"gudang_logistik",
			"laporan_persediaan_obat",
			new Array()
		);
		lpo_obat.setSuperCommand("lpo_obat");
		lpo_jenis_obat = new LPOJenisObatAction(
			"lpo_jenis_obat",
			"gudang_logistik",
			"laporan_persediaan_obat",
			new Array()
		);
		lpo_jenis_obat.setSuperCommand("lpo_jenis_obat");
		lpo = new LPOAction(
			"lpo",
			"gudang_logistik",
			"laporan_persediaan_obat",
			new Array()
		);
		$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").hide();
		$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").hide();
		$("#lpo_jenis_filter").on("change", function() {
			var jenis_filter = $("#lpo_jenis_filter").val();
			$("#lpo_id_obat").val("");
			$("#lpo_nama_obat").val("");
			$("#lpo_kode_jenis_obat").val("");
			$("#lpo_jenis_obat").val("");
			if (jenis_filter == "semua") {
				$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").hide();
				$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").hide();
			} else if (jenis_filter == "per_obat") {
				$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").show();
				$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").hide();
			} else if (jenis_filter == "per_jenis") {
				$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(7)").hide();
				$("#laporan_persediaan_obat > div.form-container > form > div:nth-child(8)").show();
			}
		});
		$("#loading_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#loading_modal").on("hide", function() {
			$("a.close").show();
		});
		$("tbody#lpo_list").append(
			"<tr>" +
				"<td colspan='11'><strong><center><small>DATA PERSEDIAAN OBAT BELUM DIPROSES</small></center></strong></td>" +
			"</tr>"
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		})
	});
</script>