<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("gudang_logistik/table/ObatMasukTable.php");
	global $db;

	$table = new ObatMasukTable(
		array("No.", "No. BBM", "No. OPL", "No. Faktur", "Tgl. Faktur", "Rekanan", "Tgl. Diterima", "Jatuh Tempo"),
		"Gudang Logistik : Penerimaan Obat (BBM - OPL)",
		null,
		true
	);
	$table->setName("penerimaan_obat");

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_penagihan") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$id = $_POST['id'];
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_logistik/templates/template_penagihan.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("PENAGIHAN");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
			$header_info = $dbtable->get_row("
				SELECT *
				FROM smis_lgs_obat_f_masuk
				WHERE id = '" . $id . "'
			");
			$objWorksheet->setCellValue("C4", ": " . ArrayAdapter::format("date d-m-Y", $header_info->tanggal));
		    $roman_token = array(
		    	'M'	=>1000, 
		    	'CM'=>900, 
		    	'D'	=>500, 
		    	'CD'=>400, 
		    	'C'	=>100, 
		    	'XC'=>90, 
		    	'L'	=>50, 
		    	'XL'=>40, 
		    	'X'	=>10, 
		    	'IX'=>9, 
		    	'V'	=>5, 
		    	'IV'=>4, 
		    	'I'	=>1
		    ); 
		    $bulan_roman = "";
		    $bulan_integer = ArrayAdapter::format("date m", $header_info->tanggal);
		    while ($bulan_integer > 0) { 
		        foreach ($roman_token as $rom => $arb) { 
		            if ($bulan_integer >= $arb) { 
		                $bulan_integer -= $arb;
		                $bulan_roman .= $rom;
		                break;
		            } 
		        } 
		    }
		    $objWorksheet->setCellValue("A7", ArrayAdapter::format("unslug", $nama_entitas));
		    $objWorksheet->setCellValue("A28", "Direktur " . $nama_entitas);
			$objWorksheet->setCellValue("C5", ":   / FARMASI / BAG. FARMASI / " . $bulan_roman . " / " . ArrayAdapter::format("date Y", $header_info->tanggal));
			$objWorksheet->setCellValue("C6", ": PEMBELIAN OBAT & ALAT KESEHATAN FARMASI");
			$detail_info = $dbtable->get_result("
				SELECT *
				FROM smis_lgs_dobat_f_masuk
				WHERE id_obat_f_masuk = '" . $id . "'
			");
			
			if (count($detail_info) - 2 > 0)
				$objWorksheet->insertNewRowBefore(13, count($detail_info) - 2);
			$start_row_num = 13;
			$end_row_num = 13;
			$row_num = $start_row_num;
			$nomor = 1;
			$total = 0;
			foreach ($detail_info as $d) {
				$col_num = 0;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $nomor++);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, substr($d->nama_obat, 0, 30));
				$col_num+=2;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$hna_barang = $d->hna;
				$diskon = $d->diskon;
				if ($d->t_diskon == "persen")
					$diskon = $hna_barang * $d->diskon / 100;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=ROUND(" . ($hna_barang - $diskon) . ", 0)");
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "=F" . $row_num . "*G" . $row_num);
				$total += $d->jumlah * ($hna_barang - $diskon);
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
			}
			$row_num++;
			$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
			$row_num++;
			loadLibrary("smis-libs-function-math");
			$objWorksheet->setCellValueByColumnAndRow(3, $row_num, "(" . strtoupper(numbertell($total)) . ")");
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename='PENAGIHAN_BBM_" . $header_info->no_bbm . ".xlsx'");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		if ($_POST['command'] == "export_xls") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$id = $_POST['id'];
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_logistik/templates/template_au-53_rev2.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("AU-53.K");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
			$header_info = $dbtable->get_row("
				SELECT *
				FROM smis_lgs_obat_f_masuk
				WHERE id = '" . $id . "'
			");
			$objWorksheet->setCellValue("F4", ": " . $header_info->nama_vendor);
			$objWorksheet->setCellValue("N2", ": " . $header_info->no_opl);
			$objWorksheet->setCellValue("N3", ": " . $header_info->no_faktur);
			$objWorksheet->setCellValue("N4", ": " . $header_info->no_bbm);
			$objWorksheet->setCellValue("N5", ": " . ArrayAdapter::format("date d-m-Y", $header_info->tanggal));
			$detail_info = $dbtable->get_result("
				SELECT a.*, b.no_batch, b.tanggal_exp
				FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_stok_obat b ON a.id = b.id_dobat_masuk
				WHERE a.id_obat_f_masuk = '" . $id . "' AND a.prop NOT LIKE 'del'
			");
			
			$jumlah_item = count($detail_info);
			$jumlah_item_per_halaman = 10;
			$jumlah_halaman = ceil($jumlah_item / $jumlah_item_per_halaman);
			$start_row_index = 1;
			$end_row_index = 27;
			$value_index_incr = 27;

			$cur_item_index = 0;
			$print_area_str = "";
			$total = 0;
			for ($cur_page = 1; $cur_page <= $jumlah_halaman; $cur_page++) {
				$cur_row_index = ($start_row_index + 8) + ($cur_page - 1) * $value_index_incr;
				$start_print_area = $start_row_index + $value_index_incr * ($cur_page - 1);
				$end_print_area = $end_row_index + $value_index_incr * ($cur_page - 1);
				$print_area_str .= "A" . $start_print_area . ":N" . $end_print_area . ",";
				for ($cur_item_num = 1; $cur_item_num <= $jumlah_item_per_halaman && $cur_item_index < $jumlah_item; $cur_item_num++) {
					$objWorksheet->setCellValue("A" . $cur_row_index, $detail_info[$cur_item_index]->kode_obat);
					$objWorksheet->setCellValue("B" . $cur_row_index, $detail_info[$cur_item_index]->nama_obat);
					$objWorksheet->setCellValue("D" . $cur_row_index, $detail_info[$cur_item_index]->satuan);
					$objWorksheet->setCellValue("E" . $cur_row_index, $detail_info[$cur_item_index]->jumlah_tercatat);
					$objWorksheet->getStyle("E" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("F" . $cur_row_index, $detail_info[$cur_item_index]->jumlah);
					$objWorksheet->getStyle("F" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("G" . $cur_row_index, $detail_info[$cur_item_index]->jumlah - $detail_info[$cur_item_index]->jumlah_tercatat);
					$objWorksheet->getStyle("G" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("H" . $cur_row_index, $detail_info[$cur_item_index]->stok_entri + ($detail_info[$cur_item_index]->jumlah * $detail_info[$cur_item_index]->konversi));
					$objWorksheet->getStyle("H" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0");
					$objWorksheet->setCellValue("I" . $cur_row_index, $detail_info[$cur_item_index]->no_batch);
					$tanggal_exp = "-";
					if ($detail_info[$cur_item_index]->tanggal_exp != "0000-00-00")
						$tanggal_exp = ArrayAdapter::format("date d-m-Y", $detail_info[$cur_item_index]->tanggal_exp);
					$objWorksheet->setCellValue("J" . $cur_row_index, $tanggal_exp);
					$hna_barang = ($detail_info[$cur_item_index]->hna / 1.1) * $detail_info[$cur_item_index]->jumlah;
					if ($header_info->use_ppn == 0)
						$hna_barang = $detail_info[$cur_item_index]->hna * $detail_info[$cur_item_index]->jumlah;
					$diskon = $detail_info[$cur_item_index]->diskon;
					if ($detail_info[$cur_item_index]->t_diskon == "persen")
						$diskon = $hna_barang * $detail_info[$cur_item_index]->diskon / 100;
					if ($header_info->use_ppn == 1)
						$objWorksheet->setCellValue("K" . $cur_row_index, ($detail_info[$cur_item_index]->hna / 1.1));
					else
						$objWorksheet->setCellValue("K" . $cur_row_index, $detail_info[$cur_item_index]->hna);
					$objWorksheet->getStyle("K" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
					$objWorksheet->setCellValue("L" . $cur_row_index, $diskon);
					$objWorksheet->getStyle("L" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
					$objWorksheet->setCellValue("M" . $cur_row_index, "=ROUND(" . $hna_barang . "-" . $diskon . ", 0)");
					$objWorksheet->getStyle("M" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
					$total += ($hna_barang - $diskon);
					$cur_item_index++;
					$cur_row_index++;
				}
			}
			$objWorksheet->setCellValue("M19", $total);
			if ($header_info->use_ppn == 0)
				$objWorksheet->setCellValue("M20", 0);
			else
				$objWorksheet->setCellValue("M20", floor($total * 0.1));
			$objWorksheet->getPageSetup()->setPrintArea(rtrim($print_area_str, ","));
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename='AU_53-OPL_" . $header_info->no_opl . "-BBM_" . $header_info->no_bbm . ".xlsx'");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("No. BBM", "no_bbm");
		$adapter->add("No. OPL", "no_opl");
		$adapter->add("No. Faktur", "no_faktur");
		$adapter->add("Tgl. Faktur", "tanggal", "date d-m-Y");
		$adapter->add("Rekanan", "nama_vendor");
		$adapter->add("Tgl. Diterima", "tanggal_datang", "date d-m-Y");
		$adapter->add("Jatuh Tempo", "tanggal_tempo", "date d-m-Y");
		$dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
		$dbtable->addCustomKriteria(" id ", " <> 0 ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("gudang_logistik/js/penerimaan_obat_action.js", false);
	echo addJS("gudang_logistik/js/penerimaan_obat.js", false);
?>