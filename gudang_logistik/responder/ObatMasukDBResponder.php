<?php
require_once("smis-base/smis-include-duplicate.php");

class ObatMasukDBResponder extends DuplicateResponder {
	public function save() {
		$header_data = $this->postToArray();
		$id['id'] = $_POST['id'];
		$result = false;
		if ($id['id'] == 0 || $id['id'] == "") {
			//do insert header here:
			$result = $this->dbtable->insert($header_data);
			$id['id'] = $this->dbtable->get_inserted_id();
			$success['type'] = "insert";
			$subtotal = 0;
			if (isset($_POST['detail'])) {
				//do insert detail here:
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_dobat_f_masuk");
				$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_stok_obat");
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_riwayat_stok_obat");
				$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_kartu_stok");
				$detail = $_POST['detail'];
				$tipe = $header_data['tipe'];
				if ($header_data['tipe'] == "logistik")
					$tipe = "reguler";
				foreach($detail as $d) {
					//detail:
					$detail_data = array();
					$detail_data['id_obat_f_masuk'] = $id['id'];
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['medis'] = $d['medis'];
					$detail_data['inventaris'] = $d['inventaris'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['label'] = $tipe;
					$detail_data['id_dpo'] = $d['id_dpo'];
					$detail_row = $detail_dbtable->get_row("
						SELECT SUM(b.sisa) AS 'jumlah'
						FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_stok_obat b ON a.id = b.id_dobat_masuk
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $d['id_obat'] . "' AND b.prop NOT LIKE 'del'
					");
					$d['stok_entri'] = $detail_row->jumlah;
					$detail_data['stok_entri'] = $d['stok_entri'];
					$detail_data['jumlah_tercatat'] = $d['jumlah_tercatat'];
					$detail_data['jumlah'] = $d['jumlah'];
					$detail_data['sisa'] = 0;
					$detail_data['satuan'] = $d['satuan'];
					$detail_data['konversi'] = $d['konversi'];
					$detail_data['satuan_konversi'] = $d['satuan_konversi'];
					$detail_data['hna'] = $d['hna'];
					$detail_data['selisih'] = $d['selisih'];
					$detail_data['produsen'] = $d['produsen'];
					$detail_data['diskon'] = $d['diskon'];
					$detail_data['t_diskon'] = $d['t_diskon'];
					$detail_data['autonomous'] = "[".$this->getAutonomous()."]";
			        $detail_data['duplicate'] = 0;
			        $detail_data['time_updated'] = date("Y-m-d H:i:s");
			        $detail_data['origin_updated'] = $this->getAutonomous();
					$detail_dbtable->insert($detail_data);
					$id_dobat_masuk = $detail_dbtable->get_inserted_id();
					$subtotal = $subtotal + ($d['hna'] * $d['jumlah']);
					
					//stok:
					$stok_data = array();
					$stok_data['id_dobat_masuk'] = $id_dobat_masuk;
					$stok_data['kode_obat'] = $d['kode_obat'];
					$stok_data['nama_obat'] = $d['nama_obat'];
					$stok_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$stok_data['label'] = $tipe;
					$stok_data['id_vendor'] = $header_data['id_vendor'];
					$stok_data['nama_vendor'] = $header_data['nama_vendor'];
					$stok_data['jumlah'] = $d['jumlah'] * $d['konversi'];
					$stok_data['sisa'] = $d['sisa'] * $d['konversi'];
					$stok_data['satuan'] = $d['satuan_konversi'];
					$stok_data['konversi'] = 1;
					$stok_data['satuan_konversi'] = $d['satuan_konversi'];
					$stok_data['hna'] = ($d['hna'] + $d['selisih']) / $d['konversi'];
					$stok_data['produsen'] = $d['produsen'];
					$stok_data['tanggal_exp'] = $d['ed'];
					$stok_data['no_batch'] = $d['nobatch'];
					$stok_data['autonomous'] = "[".$this->getAutonomous()."]";
			        $stok_data['duplicate'] = 0;
			        $stok_data['time_updated'] = date("Y-m-d H:i:s");
			        $stok_data['origin_updated'] = $this->getAutonomous();
					$stok_dbtable->insert($stok_data);
					$id_stok_obat = $stok_dbtable->get_inserted_id();
					
					//riwayat stok obat:
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $id_stok_obat;
					$data_riwayat['jumlah_masuk'] = $d['jumlah'] * $d['konversi'];
					$data_riwayat['sisa'] = $d['sisa'] * $d['konversi'];
					$data_riwayat['keterangan'] = "Stok Masuk dari " . $header_data['nama_vendor'] . ", No. Faktur: " . $header_data['no_faktur'];
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$data_riwayat['autonomous'] = "[".$this->getAutonomous()."]";
			        $data_riwayat['duplicate'] = 0;
			        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
			        $data_riwayat['origin_updated'] = $this->getAutonomous();
					$riwayat_dbtable->insert($data_riwayat);
					
					//kartu gudang induk:
					$ks_data = array();
					$ks_data['f_id'] = $id_dobat_masuk;
					$ks_data['unit'] = $header_data['nama_vendor'];
					$ks_data['no_bon'] = $header_data['no_bbm'];
					$ks_data['id_obat'] = $d['id_obat'];
					$ks_data['kode_obat'] = $d['kode_obat'];
					$ks_data['nama_obat'] = $d['nama_obat'];
					$ks_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$ks_data['tanggal'] = $header_data['tanggal_datang'];
					$ks_data['masuk'] = $d['jumlah'] * $d['konversi'];
					$ks_data['keluar'] = 0;
					$ks_data['sisa'] = $d['stok_entri'] + ($d['jumlah'] * $d['konversi']);
					$ks_data['autonomous'] = "[".$this->getAutonomous()."]";
			        $ks_data['duplicate'] = 0;
			        $ks_data['time_updated'] = date("Y-m-d H:i:s");
			        $ks_data['origin_updated'] = $this->getAutonomous();
					$ks_dbtable->insert($ks_data);
				}
			}
		} else {
			$result = $this->dbtable->update($header_data, $id);
			$detail = $_POST['detail'];
			foreach($detail as $d) {
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_dobat_f_masuk");
				$d_id['id'] = $d['id'];
				$d_data['diskon'] = $d['diskon'];
				$d_data['t_diskon'] = $d['t_diskon'];
				$detail_dbtable->update($d_data, $d_id);
			}
			$success['type'] = "update";
		}
		$success['id'] = $id['id'];
		$success['success'] = 1;
		if ($result === false) $success['success'] = 0;
		return $success;
	}
	public function edit() {
		$id = $_POST['id'];
		$header_row = $this->dbtable->get_row("
			SELECT *
			FROM smis_lgs_obat_f_masuk
			WHERE id = '" . $id . "'
		");
		$data['header'] = $header_row;
		$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_dobat_f_masuk");
		$data['detail'] = $detail_dbtable->get_result("
			SELECT a.*, CASE WHEN b.tanggal_exp IS NULL THEN '-' ELSE b.tanggal_exp END AS 'tanggal_exp', CASE WHEN b.no_batch IS NULL THEN '-' ELSE b.no_batch END AS 'no_batch'
			FROM smis_lgs_dobat_f_masuk a LEFT JOIN smis_lgs_stok_obat b ON a.id = b.id_dobat_masuk
			WHERE a.id_obat_f_masuk = '" . $id . "' AND a.prop NOT LIKE 'del'
		");
		return $data;
	}
	public function delete() {
		$id['id'] = $_POST['id'];
		if ($this->dbtable->isRealDelete()) {
			$result = $this->dbtable->delete(null,$id);
		} else {
			$data['prop'] = "del";
			$data['autonomous'] = "[".$this->getAutonomous()."]";
	        $data['duplicate'] = 0;
	        $data['time_updated'] = date("Y-m-d H:i:s");
			$result = $this->dbtable->update($data, $id);
		}
		$success['success'] = 1;
		$success['id'] = $_POST['id'];
		if ($result === 'false') $success['success'] = 0;
		return $success;
	}
}
?>