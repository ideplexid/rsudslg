<?php
class ObatKSDBResponder extends DBResponder {
	public function edit() {
		$id = $_POST['id'];
		$row = $this->dbtable->get_row("
			SELECT *
			FROM (
				SELECT DISTINCT id_obat AS 'id', kode_obat, nama_obat, nama_jenis_obat
				FROM smis_lgs_dobat_f_masuk
				ORDER BY kode_obat, nama_jenis_obat, nama_obat ASC
			) v
			WHERE id = '" . $id . "'
		");
		return $row;
	}
}
?>