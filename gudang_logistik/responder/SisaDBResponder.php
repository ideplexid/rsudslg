<?php
class SisaDBResponder extends DBResponder {
	public function edit() {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$data = $this->dbtable->get_row("
			SELECT id_obat, SUM(sisa) AS 'sisa', satuan, konversi, satuan_konversi
			FROM (
				SELECT v_dobat_masuk.id_obat, smis_lgs_stok_obat.sisa, smis_lgs_stok_obat.satuan, smis_lgs_stok_obat.konversi, smis_lgs_stok_obat.satuan_konversi	
				FROM smis_lgs_stok_obat LEFT JOIN (
					SELECT label, id, id_obat, nama_obat, nama_jenis_obat
					FROM smis_lgs_dobat_f_masuk
				) v_dobat_masuk ON smis_lgs_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_lgs_stok_obat.label = v_dobat_masuk.label
				WHERE smis_lgs_stok_obat.prop NOT LIKE 'del' AND v_dobat_masuk.id_obat = '" . $id_obat . "' AND smis_lgs_stok_obat.satuan = '" . $satuan . "' AND smis_lgs_stok_obat.konversi = '" . $konversi . "' AND smis_lgs_stok_obat.satuan_konversi = '" . $satuan_konversi . "'
			) v_stok_obat
			GROUP BY id_obat, satuan, konversi, satuan_konversi
		");
		return $data;
	}
}
?>