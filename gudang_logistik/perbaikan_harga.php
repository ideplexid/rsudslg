<?php
	global $db;
	
	$table = new Table(
		array("No.", "Kode", "Nama", "Jenis", "HPP", "No. BBM", "Tanggal Masuk"),
		"Gudang Logistik : Koreksi Harga",
		null,
		true
	);
	$table->setName("perbaikan_harga");
	$table->setDelButtonEnable();
	
	if (isset($_POST['command'])) {
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Kode", "kode_obat");
		$adapter->add("Nama", "nama_obat");
		$adapter->add("Jenis", "nama_jenis_obat");
		$adapter->add("HPP", "hna", "money Rp. ");
		$adapter->add("No. BBM", "no_bbm");
		$adapter->add("Tanggal Masuk", "tanggal_datang", "date d-m-Y");
		$dbtable = new DBTable($db, "smis_lgs_stok_obat");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (a.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR c.no_bbm LIKE '%" . $_POST['kriteria'] . "%' OR c.tanggal_datang LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT a.*, c.no_bbm, c.tanggal_datang
			FROM (smis_lgs_stok_obat a LEFT JOIN smis_lgs_dobat_f_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_lgs_obat_f_masuk c ON b.id_obat_f_masuk = c.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del' " . $filter . "
			ORDER BY a.id DESC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		
		class PerbaikanHargaDBResponder extends DBResponder {
			public function save() {
				$data = $this->postToArray();
				$id['id'] = $_POST['id'];
				$result = $this->dbtable->update($data, $id);
				
				$dobat_masuk_dbtable = new DBTable($this->dbtable->get_db(), "smis_lgs_dobat_f_masuk");
				$dobat_data = array();
				$dobat_id['id'] = $_POST['id_dobat_masuk'];
				$dobat_data = array();
				$dobat_data['hna'] = $_POST['hna'];
				$dobat_masuk_dbtable->update($dobat_data, $dobat_id);
				
				$success['type'] = 'update';
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
		}
		$dbresponder = new PerbaikanHargaDBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$modal = new Modal("perbaikan_harga_add_form", "smis_form_container", "perbaikan_harga");
	$id_hidden = new Hidden("perbaikan_harga_id", "perbaikan_harga_id", "");
	$modal->addElement("", $id_hidden);
	$id_dobat_masuk_hidden = new Hidden("perbaikan_harga_id_dobat_masuk", "perbaikan_harga_id_dobat_masuk", "");
	$modal->addElement("", $id_dobat_masuk_hidden);
	$kode_text = new Text("perbaikan_harga_kode", "perbaikan_harga_kode", "");
	$kode_text->setAtribute("disabled='disabled'");
	$modal->addElement("Kode", $kode_text);
	$nama_text = new Text("perbaikan_harga_nama", "perbaikan_harga_nama", "");
	$nama_text->setAtribute("disabled='disabled'");
	$modal->addElement("Nama", $nama_text);
	$jenis_text = new Text("perbaikan_harga_jenis", "perbaikan_harga_jenis", "");
	$jenis_text->setAtribute("disabled='disabled'");
	$modal->addElement("Jenis", $jenis_text);
	$hpp_lama_text = new Text("perbaikan_harga_hpp_lama", "perbaikan_harga_hpp_lama", "");
	$hpp_lama_text->setTypical("money");
	$hpp_lama_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\"  disabled='disabled'" );
	$modal->addElement("HPP Sekarang", $hpp_lama_text);
	$hpp_baru_text = new Text("perbaikan_harga_hpp_baru", "perbaikan_harga_hpp_baru", "");
	$hpp_baru_text->setTypical("money");
	$hpp_baru_text->setAtribute(" data-thousands=\".\" data-decimal=\",\" data-prefix=\"Rp. \"  data-precision=\"2\" " );
	$modal->addElement("HPP Baru", $hpp_baru_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAction("perbaikan_harga.save()");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$modal->addFooter($save_button);
	
	echo $modal->getHtml();
	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PerbaikanHargaAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PerbaikanHargaAction.prototype.constructor = PerbaikanHargaAction;
	PerbaikanHargaAction.prototype = new TableAction();
	PerbaikanHargaAction.prototype.edit = function(id) {
		var data = this.getEditData(id);
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#perbaikan_harga_id").val(json.id);
				$("#perbaikan_harga_id_dobat_masuk").val(json.id_dobat_masuk);
				$("#perbaikan_harga_kode").val(json.kode_obat);
				$("#perbaikan_harga_nama").val(json.nama_obat);
				$("#perbaikan_harga_jenis").val(json.nama_jenis_obat);
				$("#perbaikan_harga_hpp_lama").val("Rp. " + parseFloat(json.hna).formatMoney("2", ".", ","));
				$("#perbaikan_harga_hpp_baru").val("Rp. " + parseFloat(json.hna).formatMoney("2", ".", ","));
				$("#perbaikan_harga_add_form").smodal("show");
			}
		);
	};
	PerbaikanHargaAction.prototype.save = function() {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#perbaikan_harga_id").val();
		data['id_dobat_masuk'] = $("#perbaikan_harga_id_dobat_masuk").val();
		data['hna'] = $("#perbaikan_harga_hpp_baru").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		$.post(
			"",
			data,
			function() {
				self.view();
				$("#perbaikan_harga_add_form").smodal("hide");
			}
		);
	};
	
	var perbaikan_harga;
	$(document).ready(function() {
		var perbaikan_harga_columns = new Array("id", "id_dobat_masuk", "kode_obat", "nama_obat", "nama_jenis_obat", "hpp_lama", "hpp_baru");
		perbaikan_harga = new PerbaikanHargaAction(
			"perbaikan_harga",
			"gudang_logistik",
			"perbaikan_harga",
			perbaikan_harga_columns
		);
		perbaikan_harga.view();
	});
</script>