<?php
global $db;
require_once "smis-libs-class/MasterTemplate.php";
$master=new MasterTemplate($db,"smis_lgs_stok_barang_minimum_maksimum","gudang_logistik","pengaturan_stok");
$master->setDateEnable(true);
$uitable=$master->getUItable();
$uitable->setHeader(array("No.","Nama","Jenis","Minimum","Satuan"));
$adapter=$master->getAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add("Nama","nama_barang");
$adapter->add("Jenis","nama_jenis_barang");
$adapter->add("Minimum","jumlah_min");
$adapter->add("Satuan","satuan");
$master->setModalTitle("Pengauran Stok");
$uitable->addModal("id","hidden","","");
$uitable->addModal("id_barang","hidden","","");
$uitable->addModal("nama_barang","text","Nama","");
$uitable->addModal("nama_jenis_barang","text","Jenis","");
$uitable->addModal("medis","hidden","","");
$uitable->addModal("jumlah_min","text","Minimum", "","n","numeric");
$uitable->addModal("jumlah_max","hidden","","");
$uitable->addModal("satuan","text","Satuan","");
$master->initialize();
?>