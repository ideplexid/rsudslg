<?php 
	global $db;
	
	$laporan_form = new Form("lsok_form", "", "Gudang Logistik : Laporan Stok Obat Gudang Logistik Per Kategori");
	$jenis_obat_button = new Button("", "", "Pilih");
	$jenis_obat_button->setClass("btn-info");
	$jenis_obat_button->setAction("jenis_obat.chooser('jenis_obat', 'jenis_obat_button', 'jenis_obat', jenis_obat)");
	$jenis_obat_button->setIcon("icon-white icon-list-alt");
	$jenis_obat_button->setIsButton(Button::$ICONIC);
	$jenis_obat_button->setAtribute("id='jenis_obat_browse'");
	$jenis_obat_text = new Text("lsok_nama_jenis_obat", "lsok_nama_jenis_obat", "");
	$jenis_obat_text->setClass("smis-one-option-input");
	$jenis_obat_text->setAtribute("disabled='disabled'");
	$jenis_obat_input_group = new InputGroup("");
	$jenis_obat_input_group->addComponent($jenis_obat_text);
	$jenis_obat_input_group->addComponent($jenis_obat_button);
	$laporan_form->addElement("Jenis Obat", $jenis_obat_input_group);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lsok.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lsok.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lsok_table = new Table(
		array("No. Stok", "Nama Obat", "Jumlah", "Satuan", "HNA", "Total HNA", "Tgl. Exp."),
		"",
		null,
		true
	);
	$lsok_table->setName("lsok");
	$lsok_table->setAction(false);
	
	//chooser jenis obat:
	$jenis_obat_table = new Table(
		array("Nama Jenis Obat"),
		"",
		null,
		true
	);
	$jenis_obat_table->setName("jenis_obat");
	$jenis_obat_table->setModel(Table::$SELECT);
	$jenis_obat_adapter = new SimpleAdapter();
	$jenis_obat_adapter->add("Nama Jenis Obat", "nama_jenis_obat");
	$jenis_obat_dbtable = new DBTable($db, "smis_lgs_stok_obat");
	$jenis_obat_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT nama_jenis_obat AS 'id', nama_jenis_obat
		FROM (
			SELECT DISTINCT nama_jenis_obat
			FROM smis_lgs_stok_obat
			WHERE prop NOT LIKE 'del'
		) v_jenis_obat
		WHERE " . $filter . "
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT nama_jenis_obat
			FROM smis_lgs_stok_obat
			WHERE prop NOT LIKE 'del'
		) v_jenis_obat
		WHERE " . $filter . "
	";
	$jenis_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	class JenisObatDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$data['id'] = $id;
			$data['nama_jenis_obat'] = $id;
			return $data;
		}
	}
	$jenis_obat_dbresponder = new JenisObatDBResponder(
		$jenis_obat_dbtable,
		$jenis_obat_table,
		$jenis_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("jenis_obat", $jenis_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		class LLSOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Stok'] = ArrayAdapter::format("digit8", $row->id);
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jumlah'] = $row->sisa;
				$array['Satuan'] = $row->satuan;
				$array['HNA'] = self::format("money Rp. ", $row->hna);
				$array['Total HNA'] = self::format("money Rp. ", $row->jumlah * $row->hna);
				$array['Tgl. Exp.'] = self::format("date d-m-Y", $row->tanggal_exp);
				return $array;
			}
		}
		$lsok_adapter = new LLSOAdapter();		
		$lsok_dbtable = new DBTable($db, "smis_lgs_stok_obat");
		$filter = "1";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT *
				FROM smis_lgs_stok_obat
				WHERE prop NOT LIKE 'del' AND nama_jenis_obat = '" . $_POST['nama_jenis_obat'] . "' AND sisa > 0
			) v_lsok
			WHERE " . $filter . "
			ORDER BY nama_obat ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT *
				FROM smis_lgs_stok_obat
				WHERE prop NOT LIKE 'del' AND nama_jenis_obat = '" . $_POST['nama_jenis_obat'] . "' AND sisa > 0
			) v_lsok
			WHERE " . $filter . "
			ORDER BY nama_obat ASC
		";
		$lsok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT *
						FROM smis_lgs_stok_obat
						WHERE prop NOT LIKE 'del' AND nama_jenis_obat = '" . $_POST['nama_jenis_obat'] . "' AND sisa > 0
					) v_lsok
					ORDER BY nama_obat ASC
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN REKAPITULASI STOK OBAT PER KATEGORI GUDANG FARMASI</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Jenis Obat</td>
										<td>:</td>
										<td>" . $_POST['nama_jenis_obat'] . "</td>
									</tr>
									<tr>
										<td>Pada Tanggal</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Stok</th>
										<th>Nama Obat</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>HNA</th>
										<th>Total HNA</th>
										<th>Tgl. Exp.</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					$no = 1;
					foreach($data as $d) {
						$tanggal_part = explode(" ", $d->tanggal);
						$time_part = explode(":", $tanggal_part[1]);
						$print_data .= "<tr>
											<td>" . ArrayAdapter::format("digit8", $d->id) . "</td>
											<td>" . $d->nama_obat . "</td>
											<td>" . $d->sisa . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $d->sisa * $d->hna) . "</td>
											<td>" . ArrayAdapter::format("date d-m-Y", $d->tanggal_exp) . "</td>
										</tr>";
						$total += ($d->sisa * $d->hna);
					}
				} else {
					$print_data .= "<tr>
										<td colspan='7' align='center'><i>Tidak terdapat data stok obat</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='5' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
									<td>&nbsp;</td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lsok_dbresponder = new LLSODBResponder(
			$lsok_dbtable,
			$lsok_table,
			$lsok_adapter
		);
		$data = $lsok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lsok_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LSOKAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LSOKAction.prototype.constructor = LSOKAction;
	LSOKAction.prototype = new TableAction();
	LSOKAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['nama_jenis_obat'] = $("#lsok_nama_jenis_obat").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LSOKAction.prototype.print = function() {
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['nama_jenis_obat'] = $("#lsok_nama_jenis_obat").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	function JenisObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	JenisObatAction.prototype.constructor = JenisObatAction;
	JenisObatAction.prototype = new TableAction();
	JenisObatAction.prototype.selected = function(json) {
		$("#lsok_nama_jenis_obat").val(json.nama_jenis_obat);
	};
	
	var lsok;
	var jenis_obat;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		jenis_obat = new JenisObatAction(
			"jenis_obat",
			"gudang_logistik",
			"laporan_rekap_stok_per_kategori",
			new Array()
		);
		jenis_obat.setSuperCommand("jenis_obat");
		lsok = new LSOKAction(
			"lsok",
			"gudang_logistik",
			"laporan_rekap_stok_per_kategori",
			new Array()
		)
	});
</script>