<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-libs-class/ServiceProviderList.php");
	global $db;
	
	$pengecekan_stok_aktual_unit_table = new Table(
		array("Nomor", "Nama Obat", "Jenis Obat", "Stok Aktual", "Satuan", "Min. HNA", "Maks. HNA", "HJA"),
		"",
		null,
		true
	);
	$pengecekan_stok_aktual_unit_table->setName("pengecekan_stok_aktual_unit");
	$pengecekan_stok_aktual_unit_table->setAction(false);
	$pengecekan_stok_aktual_unit_adapter = new SimpleAdapter();
	$pengecekan_stok_aktual_unit_adapter->add("Nomor", "id", "digit8");
	$pengecekan_stok_aktual_unit_adapter->add("Nama Obat", "nama_obat");
	$pengecekan_stok_aktual_unit_adapter->add("Jenis Obat", "nama_jenis_obat");
	$pengecekan_stok_aktual_unit_adapter->add("Stok Aktual", "sisa");
	$pengecekan_stok_aktual_unit_adapter->add("Satuan", "satuan");
	$pengecekan_stok_aktual_unit_adapter->add("Min. HNA", "min_hna", "money Rp.");
	$pengecekan_stok_aktual_unit_adapter->add("Maks. HNA", "maks_hna", "money Rp.");
	$pengecekan_stok_aktual_unit_adapter->add("HJA", "hja", "money Rp.");
	$unit = isset($_POST['unit'])? $_POST['unit'] : "";
	$pengecekan_stok_aktual_unit_responder = new ServiceResponder(
		$db,
		$pengecekan_stok_aktual_unit_table,
		$pengecekan_stok_aktual_unit_adapter,
		"get_actual_stock_obat",
		$unit
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("pengecekan_stok_aktual_unit", $pengecekan_stok_aktual_unit_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$pengecekan_stok_aktual_unit_form = new Form("pengecekan_stok_aktual_unit_form", "", "Gudang Logistik : Pengecekan Stok Aktual Obat Unit");
	class UnitServiceConsumer extends ServiceConsumer {
		public function __construct($db) {
			parent::__construct($db, "is_inventory");
		}		
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result, true);
			foreach($result as $autonomous) {
				foreach($autonomous as $entity=>$res) {
					if($res['value_obat']=="1"){
						$option = array();
						$option['value'] = $entity;
						$option['name'] = ArrayAdapter::format("unslug", $res['name']);
						$content[] = $option;
					}
				}
			}
			return $content;
		}
	}
	$unit_service_consumer = new UnitServiceConsumer($db);
	$unit_service_consumer->execute();
	$unit_option = $unit_service_consumer->getContent();
	$unit_select = new Select("pengecekan_stok_aktual_unit_unit", "pengecekan_stok_aktual_unit_unit", $unit_option);
	$pengecekan_stok_aktual_unit_form->addElement("Unit", $unit_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("pengecekan_stok_aktual_unit.view()");
	$pengecekan_stok_aktual_unit_form->addElement("", $show_button);
	
	echo $pengecekan_stok_aktual_unit_form->getHtml();
	echo "<div id='table_content'>";
	echo $pengecekan_stok_aktual_unit_table->getHtml();
	echo "</div'>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PengecekanStokAktualUnitAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PengecekanStokAktualUnitAction.prototype.constructor = PengecekanStokAktualUnitAction;
	PengecekanStokAktualUnitAction.prototype = new TableAction();
	PengecekanStokAktualUnitAction.prototype.getViewData = function() {
		var view_data = this.getRegulerData();
		view_data['super_command'] = "pengecekan_stok_aktual_unit";
		view_data['command'] = "list";
		view_data['kriteria'] = $("#"+this.prefix+"_kriteria").val();
		view_data['number'] = $("#"+this.prefix+"_number").val();
		view_data['max'] = $("#"+this.prefix+"_max").val();
		view_data['unit'] = $("#pengecekan_stok_aktual_unit_unit").val();
		return view_data;
	};
	
	var pengecekan_stok_aktual_unit;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		pengecekan_stok_aktual_unit = new PengecekanStokAktualUnitAction(
			"pengecekan_stok_aktual_unit",
			"gudang_logistik",
			"pengecekan_stok_aktual_unit",
			new Array()
		);
		pengecekan_stok_aktual_unit.view();
	});
</script>