<?php 
	global $db;
	require_once("smis-base/smis-include-service-consumer.php");
	
	$laporan_form = new Form("lokr_form", "", "Gudang Logistik : Laporan Mutasi Obat Per Ruangan");
	class UnitServiceConsumer extends ServiceConsumer {
		public function __construct($db) {
			parent::__construct($db, "is_inventory");
		}		
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result, true);
			foreach($result as $autonomous) {
				foreach($autonomous as $entity=>$res) {
					if($res['value_obat']=="1"){
						$option = array();
						$option['value'] = $entity;
						$option['name'] = ArrayAdapter::format("unslug", $res['name']);
						$content[] = $option;
					}
				}
			}
			return $content;
		}
	}
	$unit_service_consumer = new UnitServiceConsumer($db);
	$unit_service_consumer->execute();
	$unit_option = $unit_service_consumer->getContent();
	$unit_select = new Select("lokr_unit", "lokr_unit", $unit_option);
	$laporan_form->addElement("Ruangan", $unit_select);
	$tanggal_from_text = new Text("lokr_tanggal_from", "lokr_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lokr_tanggal_to", "lokr_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lokr.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lokr.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lokr_table = new Table(
		array("No. Mutasi", "Kode Obat", "Nama Obat", "Jumlah", "Satuan", "HNA", "Total HNA", "Tgl. Exp."),
		"",
		null,
		true
	);
	$lokr_table->setName("lokr");
	$lokr_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class LOKAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Mutasi'] = self::format("digit8", $row->id);
				$array['Kode Obat'] = $row->kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$array['HNA'] = self::format("money Rp. ", $row->hna);
				$array['Total HNA'] = self::format("money Rp. ", $row->jumlah * $row->hna);
				$array['Tgl. Exp.'] = self::format("date d-m-Y", $row->tanggal_exp);
				return $array;
			}
		}
		$lokr_adapter = new LOKAdapter();		
		$lokr_dbtable = new DBTable($db, "smis_lgs_obat_f_masuk");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "' AND unit = '" . $_POST['ruangan'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%' OR kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_lgs_obat_keluar.id, smis_lgs_obat_keluar.tanggal, smis_lgs_obat_keluar.unit, smis_lgs_stok_obat.kode_obat, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat_keluar.jumlah, smis_lgs_stok_obat.satuan, smis_lgs_stok_obat.hna, smis_lgs_stok_obat.tanggal_exp
					FROM ((smis_lgs_stok_obat_keluar LEFT JOIN smis_lgs_stok_obat ON smis_lgs_stok_obat_keluar.id_stok_obat = smis_lgs_stok_obat.id) LEFT JOIN smis_lgs_dobat_keluar ON smis_lgs_stok_obat_keluar.id_dobat_keluar = smis_lgs_dobat_keluar.id) LEFT JOIN smis_lgs_obat_keluar ON smis_lgs_dobat_keluar.id_obat_keluar = smis_lgs_obat_keluar.id
					WHERE smis_lgs_obat_keluar.status <> 'dikembalikan' AND smis_lgs_dobat_keluar.prop NOT LIKE 'del' AND smis_lgs_stok_obat_keluar.jumlah <> 0
				) v_lokr
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_lgs_obat_keluar.id, smis_lgs_obat_keluar.tanggal, smis_lgs_obat_keluar.unit, smis_lgs_stok_obat.kode_obat, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat_keluar.jumlah, smis_lgs_stok_obat.satuan, smis_lgs_stok_obat.hna, smis_lgs_stok_obat.tanggal_exp
					FROM ((smis_lgs_stok_obat_keluar LEFT JOIN smis_lgs_stok_obat ON smis_lgs_stok_obat_keluar.id_stok_obat = smis_lgs_stok_obat.id) LEFT JOIN smis_lgs_dobat_keluar ON smis_lgs_stok_obat_keluar.id_dobat_keluar = smis_lgs_dobat_keluar.id) LEFT JOIN smis_lgs_obat_keluar ON smis_lgs_dobat_keluar.id_obat_keluar = smis_lgs_obat_keluar.id
					WHERE smis_lgs_obat_keluar.status <> 'dikembalikan' AND smis_lgs_dobat_keluar.prop NOT LIKE 'del' AND smis_lgs_stok_obat_keluar.jumlah <> 0
				) v_lokr
				WHERE " . $filter . "
			";
			$lokr_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		class LOKDBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['tanggal_from'];
				$to = $_POST['tanggal_to'];
				$ruangan = $_POST['ruangan'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_lgs_obat_keluar.id, smis_lgs_obat_keluar.tanggal, smis_lgs_obat_keluar.unit, smis_lgs_stok_obat.kode_obat, smis_lgs_stok_obat.nama_obat, smis_lgs_stok_obat_keluar.jumlah, smis_lgs_stok_obat.satuan, smis_lgs_stok_obat.hna, smis_lgs_stok_obat.tanggal_exp
						FROM ((smis_lgs_stok_obat_keluar LEFT JOIN smis_lgs_stok_obat ON smis_lgs_stok_obat_keluar.id_stok_obat = smis_lgs_stok_obat.id) LEFT JOIN smis_lgs_dobat_keluar ON smis_lgs_stok_obat_keluar.id_dobat_keluar = smis_lgs_dobat_keluar.id) LEFT JOIN smis_lgs_obat_keluar ON smis_lgs_dobat_keluar.id_obat_keluar = smis_lgs_obat_keluar.id
						WHERE smis_lgs_obat_keluar.status <> 'dikembalikan' AND smis_lgs_dobat_keluar.prop NOT LIKE 'del' AND smis_lgs_stok_obat_keluar.jumlah <> 0
					) v_lokr
					WHERE tanggal >= '" . $from . "' AND tanggal <= '" . $to . "' AND unit = '" . $_POST['ruangan'] . "'
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN MUTASI OBAT PER RUANGAN</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Ruangan</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $ruangan) . "</td>
									</tr>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Mutasi</th>
										<th>Kode Obat</th>
										<th>Nama Obat</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>HNA</th>
										<th>Total HNA</th>
										<th>Tgl. Exp.</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					foreach($data as $d) {
						$hna = $d->hna;
						$total_hna = $d->jumlah * $d->hna;
						$print_data .= "<tr>
											<td>" . ArrayAdapter::format("digit8", $d->id) . "</td>
											<td>" . $d->kode_obat . "</td>
											<td>" . $d->nama_obat . "</td>
											<td>" . $d->jumlah . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ",  $hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $total_hna) . "</td>
											<td>" . ArrayAdapter::format("date d-m-Y", $d->tanggal_exp) . "</td>
										</tr>";
						$total += $total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='8' align='center'><i>Tidak terdapat data mutasi obat</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='6' align='center'><b>T O T A L</b></td>
									<td><b>Rp. " . ArrayAdapter::format("only-money", $total) . "</b></td>
									<td>&nbsp;</td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lokr_dbresponder = new LOKDBResponder(
			$lokr_dbtable,
			$lokr_table,
			$lokr_adapter
		);
		$data = $lokr_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lokr_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOKRAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOKRAction.prototype.constructor = LOKRAction;
	LOKRAction.prototype = new TableAction();
	LOKRAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lokr_tanggal_from").val();
		data['tanggal_to'] = $("#lokr_tanggal_to").val();
		data['ruangan'] = $("#lokr_unit").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOKRAction.prototype.print = function() {
		if ($("#lokr_tanggal_from").val() == "" || $("#lokr_tanggal_to").val() == "" || $("#lokr_unit").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['tanggal_from'] = $("#lokr_tanggal_from").val();
		data['tanggal_to'] = $("#lokr_tanggal_to").val();
		data['ruangan'] = $("#lokr_unit").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lokr;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#lokr_max").val(50);
		lokr = new LOKRAction(
			"lokr",
			"gudang_logistik",
			"laporan_obat_keluar_per_ruangan",
			new Array()
		)
		$('.mydate').datepicker();
	});
</script>