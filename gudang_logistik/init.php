<?php
	global $PLUGINS;
	
	$init['name'] = "gudang_logistik";
	$init['path'] = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Gudang Logistik";
	$init['require'] = "administrator";
	$init['service'] = "";
	$init['version'] = "1.0.2";
	$init['number'] = "2";
	$init['type'] = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>