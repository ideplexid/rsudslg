<?php 

function get_all_ruangan_pegawai($db){
    $dbtable            = new DBTable ( $db, "smis_hrd_ruangan_pegawai" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "ruangan_pegawai ASC" );
    $data               = $dbtable->view ( "", 0 );
    $tng                = new SelectAdapter("ruangan_pegawai", "ruangan_pegawai");
    $ruangan_pegawai    = $tng->getContent ( $data ['data'] );
    $ruangan_pegawai[]  = array("name"=>"","value"=>"");
    return $ruangan_pegawai;
}

?>