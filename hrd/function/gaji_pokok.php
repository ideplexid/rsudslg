<?php 

function gaji_pokok(TablePrint &$table, Array &$_hasil, $dokter){
    $_hasil['Gaji Pokok']=$dokter->gaji_pokok;     
    $table->addColumn("Gaji Pokok [GP]",1,1);
    $table->addColumn("",1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$dokter->gaji_pokok),1,1);
    $table->commit("body");
}

?>