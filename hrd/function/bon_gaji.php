<?php 

function bon_gaji(TablePrint &$table, Array &$_hasil,$id_dokter,$dari,$sampai){
    global $db;
    $table->addColumn("<strong>HUTANG / BON</strong>",3,1,"body",null,"center");
    $query="SELECT SUM(nilai) as total, COUNT(*) as jumlah FROM smis_hrd_insentif 
                WHERE id_pegawai='".$id_dokter."'
                AND tanggal>='".$dari."'
                AND tanggal<'".$sampai."'
                AND prop!='del'
            ";
    $bon=$db->get_row($query);
    $_hasil['Nilai Bon']=$bon->total;
    $_hasil['Jumlah Bon']=$bon->jumlah;
    $table->addColumn("Hutang/Bon [HB]",1,1);
    $table->addColumn($_hasil['Jumlah Bon']." Kali ",1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$_hasil['Nilai Bon']),1,1);
    $table->commit("body");
}

?>