<?php 

function gaji_non_dokter(TablePrint &$table, Array &$_hasil,$id_dokter,$dari,$sampai){
    global $db;
    /*gaji perawat*/
        $query="SELECT SUM(bagi_asisten) as total,count(*) as jumlah 
                FROM smis_hrd_area_lain 
                WHERE tanggal>='".$dari."' 
                AND tanggal<'".$sampai."' 
                AND prop!='del' 
                AND id_asisten='".$id_dokter."'";
        $asisten=$db->get_row($query);
        
        
        if(isset($_POST['perawat']) && $_POST['perawat']=="perawat_khusus"){
            $query="SELECT SUM(bagi_oomloop) as total,count(*) as jumlah 
                FROM smis_hrd_area_lain 
                WHERE tanggal>='".$dari."' 
                AND tanggal<'".$sampai."' 
                AND prop!='del' 
                AND kunci=0
                AND id_asisten!='".$id_dokter."'";     
        }else{
            $query="SELECT SUM(bagi_oomloop) as total,count(*) as jumlah 
                    FROM smis_hrd_area_lain 
                    WHERE tanggal>='".$dari."' 
                    AND tanggal<'".$sampai."' 
                    AND prop!='del' 
                    AND kunci=0
                    AND id_oomloop='".$id_dokter."'";
                
        }
        $oomloop=$db->get_row($query);
        $_hasil['Jumlah Asisten']               = $asisten->jumlah; 
        $_hasil['Jumlah Omloop']                = $oomloop->jumlah; 
        $_hasil['Nilai Asisten']                = $asisten->total;
        $_hasil['Nilai Oomloop']                = $oomloop->total;
        
        if($_hasil['Nilai Asisten']>0){
            $table->addColumn("Gaji Asisten [GA]",1,1);
            $table->addColumn($_hasil['Jumlah Asisten']." x ",1,1);
            $table->addColumn(ArrayAdapter::format("money Rp.",$_hasil['Nilai Asisten']),1,1);
            $table->commit("body");
        }
        
        if($_hasil['Nilai Oomloop']){
            $table->addColumn("Gaji Omloop [GOM]",1,1);
            $table->addColumn($_hasil['Jumlah Omloop']." x ",1,1);
            $table->addColumn(ArrayAdapter::format("money Rp.",$_hasil['Nilai Oomloop']),1,1);
            $table->commit("body");
        }
}

?>