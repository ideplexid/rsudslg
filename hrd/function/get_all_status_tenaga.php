<?php 

function get_all_status_tenaga($db){
    $dbtable        = new DBTable ( $db, "smis_hrd_status_tenaga" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "status_tenaga ASC" );
    $data           = $dbtable->view ( "", 0 );
    $tng            = new SelectAdapter("status_tenaga", "status_tenaga");
    $tenaga         = $tng->getContent ( $data ['data'] );
    $tenaga[]       = array("name"=>"","value"=>"");
    return $tenaga;
}

?>