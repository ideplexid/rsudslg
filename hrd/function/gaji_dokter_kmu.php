<?php 
    function gaji_dokter_kmu(Database $db, $id_dokter,$dari,$sampai){
        require_once "smis-base/smis-include-service-consumer.php";
        $_hasil=array();
        
        /*mengambil data gaji pokok, lembur dan lain-lain*/
            $dbtable=new DBTable($db,"smis_hrd_employee");
            $dokter=$dbtable->select(array("id"=>$id_dokter));  
        
        /*mengambil data jenis pasien*/
            $serv=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
            $serv->execute();
            $jenis_pasien=$serv->getContent();
        
        $table=new TablePrint("");
        $table->setDefaultBootrapClass(true);
        $table->setMaxWidth(false);
        $table->addColumn($dokter->nama,1,3);
        $table->commit("header");
        
     /*KELOMPOK GAJI POKOK*/   
        require_once "hrd/function/gaji_pokok.php";
        
        $table->addColumn("<strong>JASA MEDIS</strong>",3,1,"body",null,"center"); 
        $_hasil['GO']=0;
        foreach($jenis_pasien as $jenis){
            $slug_jenis_pasien=$jenis['value'];
            require_once "hrd/function/gaji_operasi.php";
            $_hasil['GO']+=gaji_operasi($table,$_hasil,$dokter->id,$slug_jenis_pasien,$dari,$sampai);
        }
        $table->addColumn("Gaji Operasi [GO]",2,1);
        $table->addColumn(ArrayAdapter::format("money Rp.",$_hasil['GO']),1,1);
        $table->commit("body");
        
        gaji_pokok($table,$_hasil,$dokter);
        require_once "hrd/function/gaji_non_operasi.php";
        gaji_non_operasi($table,$_hasil,$id_dokter,$dari,$sampai);    
        require_once "hrd/function/gaji_non_dokter.php";
        gaji_non_dokter($table,$_hasil,$id_dokter,$dari,$sampai);    
        require_once "hrd/function/gaji_presensi_dokter.php";
        gaji_presensi_dokter($table, $_hasil,$dokter,$dari,$sampai);
        require_once "hrd/function/gaji_kehadiran_pasien.php";
        gaji_kehadiran_pasien($table, $_hasil,$id_dokter,$dari,$sampai);
        
        $table->addColumn("<strong>LAIN - LAIN</strong>",3,1,"body",null,"center");    
        require_once "hrd/function/gaji_lain_lain.php";
        gaji_lain_lain($table, $_hasil,$dokter->id,$dari,$sampai);    
        require_once "hrd/function/bon_gaji.php";
        bon_gaji($table, $_hasil,$id_dokter,$dari,$sampai);    
        
    /* KELOMPOK TOTAL AKHIR */

        $table->addColumn("<strong>TOTAL AKHIR</strong>",3,1,"body",null,"center");
        $table->addColumn("[GP] + [GO] +[GOR] + [GNO] + [GA]+ [GOM] + [GR] + [Max(GPSM,GPP)] + [L] + [I] -[HD]",2,1);
        $total_gaji=$dokter->gaji_pokok+$_hasil['total_non_katarak']+$_hasil['GO']+$_hasil['Nilai Asisten']+$_hasil['Nilai Oomloop'];
        $total_gaji+=max(array($_hasil['total_gpp'],$_hasil['total_gpsm']));
        $total_gaji+=$_hasil['Nilai Insentif']+$_hasil['Nilai Lembur']-$_hasil['Nilai Bon'];
        $table->addColumn(ArrayAdapter::format("money Rp.",$total_gaji),1,1);
        $table->commit("body");
        
        $result=array();
        $result['table']=$table->getHtml();
        $result['total']=$total_gaji;
        $result['detail']=$_hasil;
        return $result;
    }
    
?>