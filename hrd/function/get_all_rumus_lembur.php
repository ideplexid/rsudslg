<?php 
function get_all_rumus_lembur($db){
    $dbtable        = new DBTable ( $db, "smis_hrd_rumus_lembur" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "nama ASC" );
    $data           = $dbtable->view ( "", 0 );
    $job            = new SelectAdapter("nama", "id");
    $rumus          = $job->getContent ( $data ['data'] );
    $rumus[]        = array("name"=>"","value"=>"");
    return $rumus;
}
?>