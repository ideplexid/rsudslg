<?php 

function gaji_presensi_dokter(TablePrint &$table, Array &$_hasil,$dokter,$dari,$sampai){
    global $db;
    loadLibrary("smis-libs-function-time");
    $table->addColumn("<strong>PRESENSI</strong>",3,1,"body",null,"center");    
    $serv=new ServiceConsumer($db,"get_presensi_dokter",NULL,"registration");
    $serv->addData("dari",$dari);
    $serv->addData("sampai",sub_date($sampai,1));
    $serv->addData("id_dokter",$dokter->id);
    $serv->execute();
    $presensi=$serv->getContent();
    $kelompok=$presensi['kelompok'];
    $pengganti=$presensi['ganti-kelompok'];
    
    //presensi
    $uang_hadir_pagi=$kelompok["P"]*$dokter->uang_hadir_pagi;
    $table->addColumn(" - Presensi Dokter Pagi",1,1);
    $table->addColumn($kelompok["P"]." Hari x Rp. ".ArrayAdapter::format("only-money",$dokter->uang_hadir_pagi),1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$uang_hadir_pagi),1,1);
    $table->commit("body");
    
    $uang_hadir_siang=$kelompok["S"]*$dokter->uang_hadir_siang;
    $table->addColumn(" - Presensi Dokter Sore",1,1);
    $table->addColumn($kelompok["S"]." Hari x Rp. ".ArrayAdapter::format("only-money",$dokter->uang_hadir_siang),1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$uang_hadir_siang),1,1);
    $table->commit("body");
    
    $uang_hadir_malam=$kelompok["M"]*$dokter->uang_hadir_malam;
    $table->addColumn(" - Presensi Dokter Malam",1,1);
    $table->addColumn($kelompok["M"]." Hari x Rp. ".ArrayAdapter::format("only-money",$dokter->uang_hadir_malam),1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$uang_hadir_malam),1,1);
    $table->commit("body");
    
    //absensi dokter
    $uang_absent_pagi=$pengganti["P"]*$dokter->uang_hadir_pagi;
    $table->addColumn(" - Absensi Dokter Pagi",1,1);
    $table->addColumn($pengganti["P"]." Hari x Rp. ".ArrayAdapter::format("only-money",$dokter->uang_hadir_pagi),1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$uang_absent_pagi),1,1);
    $table->commit("body");
    
    $uang_absent_siang=$pengganti["S"]*$dokter->uang_hadir_siang;
    $table->addColumn(" - Absensi Dokter Sore",1,1);
    $table->addColumn($pengganti["S"]." Hari x Rp. ".ArrayAdapter::format("only-money",$dokter->uang_hadir_siang),1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$uang_absent_siang),1,1);
    $table->commit("body");
    
    $uang_absent_malam=$pengganti["M"]*$dokter->uang_hadir_malam;
    $table->addColumn(" - Absensi Dokter Malam",1,1);
    $table->addColumn($pengganti["M"]." Hari x Rp. ".ArrayAdapter::format("only-money",$dokter->uang_hadir_malam),1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$uang_absent_malam),1,1);
    $table->commit("body");
    
    $total_gpsm=$uang_hadir_pagi+$uang_hadir_siang+$uang_hadir_malam;
    $total_gpsm-=($uang_absent_pagi+$uang_absent_siang+$uang_absent_malam);
    $table->addColumn("Total Presensi Dokter",1,1);
    $table->addColumn("Presensi (Pagi + Sore + Malam) - Absensi (Pagi + Sore + Malam) [GPSM]",1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$total_gpsm),1,1);
    $table->commit("body");
    
    $_hasil['total_gpsm']=$total_gpsm;
}

?>