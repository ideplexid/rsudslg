<?php 

function get_all_job($db){
    $dbtable        = new DBTable ( $db, "smis_hrd_job" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "nama ASC" );
    $data           = $dbtable->view ( "", 0 );
    $job            = new SelectAdapter("nama", "id");
    $jabatan        = $job->getContent ( $data ['data'] );
    $jabatan[]      = array("name"=>"","value"=>"");
    return $jabatan;
}

?>