<?php 

function gaji_operasi(TablePrint &$table, Array &$_hasil, $dokter,$slug_jenis_pasien,$dari,$sampai){
    global $db;
    $total=0;
    $unslug=ArrayAdapter::slugFormat("unslug",$slug_jenis_pasien);
    
        /* kelompok gaji - operasi */
        $full=get_full_dokter($db, $dokter,0,$dari,$sampai,$slug_jenis_pasien);
        if($full['uang']>0){
            foreach($full['detail'] as $opr){
                $table->addColumn(" - Mandiri $unslug - ".ArrayAdapter::format("only-money Rp.",$opr['uang']),1,1);
                $table->addColumn(($opr['jumlah'])." x ",1,1);
                $table->addColumn(ArrayAdapter::format("money Rp.", ($opr['uang'] * $opr['jumlah']) ),1,1);
                $table->commit("body");  
            }
            $total+= $full['uang'];
        }
        
        $operator=get_full_dokter($db, $dokter,1,$dari,$sampai,$slug_jenis_pasien);
        if($operator['uang']>0){
            foreach($operator['detail'] as $opr){
                $table->addColumn(" - Operator (Bagdu) $unslug - ".ArrayAdapter::format("only-money Rp.",$opr['uang']/2),1,1);
                $table->addColumn(($opr['jumlah'])." x ",1,1);
                $table->addColumn(ArrayAdapter::format("money Rp.", ($opr['uang'] * $opr['jumlah']/2) ),1,1);
                $table->commit("body");  
            }
            $total+=$operator['uang']/2;              
        }
        
        $perujuk=get_full_dokter($db, $dokter,-1,$dari,$sampai,$slug_jenis_pasien);
        if($perujuk['uang']>0){
            foreach($perujuk['detail'] as $opr){
                $table->addColumn(" - Perujuk (Bagdu) $unslug - ".ArrayAdapter::format("only-money Rp.",$opr['uang']),1,1);
                $table->addColumn(($opr['jumlah'])." x ",1,1);
                $table->addColumn(ArrayAdapter::format("money Rp.", ($opr['uang'] * $opr['jumlah']) ),1,1);
                $table->commit("body");  
            }
           $total+= $perujuk['uang']/2;
        }
        return $total;
}

function get_full_dokter(Database $db, $id_dokter,$opp,$dari,$sampai,$jenis_pasien){
    $query="SELECT SUM(bagi_dokter) as total, count(*) as jumlah, bagi_dokter 
            FROM smis_hrd_area_lain 
            WHERE tanggal>='".$dari."' 
            AND tanggal<'".$sampai."' 
            AND prop!='del' 
            AND id_operator ".($opp>=0?"=":"!=")."'".$id_dokter."'  
            AND id_perujuk  ".($opp<=0?"=":"!=")."'".$id_dokter."'  
            AND carabayar='".$jenis_pasien."'
            GROUP BY bagi_dokter";
    $kode[0]        = "mandiri";
    $kode[1]        = "operator";
    $kode[-1]       = "perujuk";
    
    $c=$kode[$opp];
    $operator=$db->get_result($query);
    $uang           = 0;
    $jumlah         = 0;
    $detail         = array();
    foreach($operator as $x){
        $uang      += $x->total;
        $jumlah    += $x->jumlah;
        $detail []  = array("uang"=>$x->bagi_dokter,"jumlah"=>$x->jumlah);
    }
    $result['uang']     = $uang;
    $result['jumlah']   = $jumlah;
    $result['detail']   = $detail;    
    return $result;
}



?>