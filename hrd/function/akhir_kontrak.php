<?php 
/**
 * fungsi ini dipakai untuk 
 * menghitung akhir kontrak seorang pegawai
 * sehingga bisa dihitung kontrak dia habis tanggal berapa
 * dan ditampilkan menggunakan badge
 * 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @since		: 18 Des 2017
 * @version		: 1.0.0
 * 
 * */
 
function akhir_kontrak($value){
    global $db;
    $durasi=getSettings($db,"hrd-employee-max-kontrak-limit",60);
    if($value*1<$durasi){
        return " <div class='badge badge-important'> < ".$durasi." Hari </div>";
    }
    return "";
};

?>