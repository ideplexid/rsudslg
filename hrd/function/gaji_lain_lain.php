<?php 

function gaji_lain_lain(TablePrint &$table, Array &$_hasil,$id_dokter,$dari,$sampai){
    global $db;
    $query="SELECT SUM(total) as total, COUNT(*) as jumlah FROM smis_hrd_lembur 
                    WHERE id_pegawai='".$id_dokter."'
                    AND tanggal>='".$dari."'
                    AND tanggal<'".$sampai."'
                    AND prop!='del'
                ";
        $lembur=$db->get_row($query);
        $_hasil['Nilai Lembur']=$lembur->total;
        $_hasil['Jumlah Lembur']=$lembur->jumlah;
        
        $table->addColumn("Lembur [L]",1,1);
        $table->addColumn($_hasil['Jumlah Lembur']." Kali ",1,1);
        $table->addColumn(ArrayAdapter::format("money Rp.",$_hasil['Nilai Lembur']),1,1);
        $table->commit("body");
    
    $query="SELECT SUM(nilai) as total, COUNT(*) as jumlah FROM smis_hrd_insentif 
                WHERE id_pegawai='".$id_dokter."'
                AND tanggal>='".$dari."'
                AND tanggal<'".$sampai."'
                AND prop!='del'
            ";
    $insenstif=$db->get_row($query);
    $_hasil['Nilai Insentif']=$insenstif->total;
    $_hasil['Jumlah Insentif']=$insenstif->jumlah;
    $table->addColumn("Insentif [I]",1,1);
    $table->addColumn($_hasil['Jumlah Insentif']." Kali ",1,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$_hasil['Nilai Insentif']),1,1);
    $table->commit("body");    
}

?>