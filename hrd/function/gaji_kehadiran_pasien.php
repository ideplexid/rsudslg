<?php 

function gaji_kehadiran_pasien(TablePrint &$table, Array &$_hasil,$id_dokter,$dari,$sampai){
    global $db;
    $serv=new ServiceConsumer($db,"get_jumlah_patient_by_jadwal_dokter",NULL,"registration");
    $serv->addData("dari",$dari);
    $serv->addData("sampai",$sampai);
    $serv->addData("id_dokter",$id_dokter);
    $serv->execute();
    $con=$serv->getContent();
    
    $table->addColumn("<strong>PEROLEHAN PASIEN</strong>",3,1,"body",null,"center");
    $total_gpp=0;
    foreach($con as $x){
        $harga=getSettings($db,"hrd-bonus-kunjungan-pasien-".$x['carabayar'],"0");
        $total_harga=$x['total']*$harga;
        $table->addColumn(" - Pasien ".$x['carabayar'],1,1);
        $table->addColumn($x['total']." x ".ArrayAdapter::format("only-money Rp.",$harga),1,1);
        $table->addColumn(ArrayAdapter::format("money Rp.",$total_harga),1,1);
        $table->commit("body");            
        $total_gpp+=$total_harga;
    }
    $table->addColumn("Total Pendapatan Pasien [GPP]",2,1);
    $table->addColumn(ArrayAdapter::format("money Rp.",$total_gpp),1,1);
    $table->commit("body");
    
    $_hasil['total_gpp']=$total_gpp;
}

?>