<?php 

function gaji_non_operasi(TablePrint &$table, Array &$_hasil,$id_dokter,$dari,$sampai){
        global $db;    
        /*gaji dokter non-operasi*/
        $serv=new ServiceConsumer($db,"get_gaji_tindakan_dokter");
        $serv->addData("id_dokter",$id_dokter);
        $serv->addData("dari",$dari);
        $serv->addData("sampai",$sampai);
        $serv->execute();
        $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
        $cont=$serv->getContent();
        $total_non_katarak=0;
        $jumlah=0;
        foreach($cont as $x){
            if($x==null)
                return;
            $total_non_katarak+=$x['total'];
            $jumlah+=$x['jumlah'];
        }
        $table->addColumn("Gaji Non Operasi [GNO]",1,1);
        $table->addColumn($jumlah." x ",1,1);
        $table->addColumn(ArrayAdapter::format("money Rp.",$total_non_katarak),1,1);
        $table->commit("body");
        $_hasil['total_non_katarak']=$total_non_katarak;
        /*end gaji dokter non-operasi*/
    
}
?>