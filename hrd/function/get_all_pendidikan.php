<?php 

function get_all_pendidikan($db){
    $dbtable        = new DBTable ( $db, "smis_hrd_pendidikan" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "pendidikan ASC" );
    $data           = $dbtable->view ( "", 0 );
    $pend           = new SelectAdapter("pendidikan", "pendidikan");
    $pendidikan     = $pend->getContent ( $data ['data'] );
    $pendidikan[]   = array("name"=>"","value"=>"");
    return $pendidikan;
}

?>