<?php 

function get_all_golongan($db){
    $dbtable        = new DBTable ( $db, "smis_hrd_golongan" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "golongan ASC" );
    $data           = $dbtable->view ( "", 0 );
    $tng            = new SelectAdapter("golongan", "golongan");
    $golongan       = $tng->getContent ( $data ['data'] );
    $golongan[]     = array("name"=>"","value"=>"");
    return $golongan;
}

?>