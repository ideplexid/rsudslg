<?php
global $PLUGINS;

$init ['name'] = 'hrd';
$init ['path'] = SMIS_DIR . "hrd/";
$init ['description'] = "Human Resource Development";
$init ['require'] = "administrator";
$init ['service'] = "get_employee, get_employee_detail, get_employee_job, get_salary";
$init ['version'] = "4.3.7";
$init ['number'] = "37";
$init ['type'] = "";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>