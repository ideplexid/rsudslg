<?php 
global $db;
require_once "smis-base/smis-include-service-consumer.php";
$kode_akun=$_POST['super_command'];
$uitable=new Table(array("No.","Kode","Nama"));
$uitable->setName($kode_akun);
$uitable->setModel(Table::$SELECT);
if(isset($_POST['command'])){
    $adapter=new SimpleAdapter();
    $adapter->add("Kode","nomor");
    $adapter->add("Nama","nama");
    $adapter->setUseNumber(true,"No.","back.");
    $service=new ServiceResponder($db,$uitable,$adapter,"get_account","accounting");
    $data=$service->command($_POST['command']);
    echo json_encode($data);   
    return;
}
echo $uitable->getHtml();
?>