<?php 

/**
 * digunakan untuk menampilkan informasi 
 * tentang arti dari sebuah warna yang ada dalam 
 * system.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: 	- hrd/resource/php/outsoursing/outsource.php 
 * 					- hrd/resource/php/outsoursing/accreject.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */


echo "<div class='hide'><div id='modal_outsource_help' ><ul><li><p><label class='label label-important'>New</label> :
		adalah data yang baru saja dibuat dan belum di terima (acc) oleh supervisor,
		data baru selama belum di terima oleh supervisor masih bisa di ubah maupun di hapus. 
		jika sebuah data baru sudah di acc akan menjadi data 'Accept', Jika ditolak menjadi data 'Reject'</p></li>";

echo "<li><p><label class='label label-success'>Accept</label> :
		adalah data yang sudah di terima oleh Supervisor (baik data baru, maupun data lama yang di update)
		data yang sudah diterima 'Accept' (acc) tidak serta merta bisa di rubah, jika dirubah akan menjadi status 'Update' .</p></li>";

echo "<li><p><label class='label label-info'>Update</label> : 
		 jika data yang sudah di acc ini dirubah maka akan berubah statusnya 
		menjadi 'Update' dan perubahanya akan disimpan terlebih 
		dahulu sampai di Acc oleh Supervisor, data update yang sudah di acc ulang oleh supervisor akan
		diubah statusnya menjadi 'Accept' </p></li>";

echo "<li><p><label class='label label-inverse'>Reject</label> :
		adalah data baru yang ditolak oleh Supervisor</p></li>";

echo "<li><p><label class='label'>Di Restore</label> :
		adalah data 'Update' (data yang sudah di acc 'Accept', kemudian di rubah 'Update' ) 
		yang mana perubahan dari data tersebut di tolak oleh supervisor dan oleh supervisor data tersebut
		diminta untuk di dikembalikan ke kondisi semua seperti sebelum dirubah (Restore) 
		atau di ruabh lagi (di ajukan ulang untuk perubahan data karena mungkin ada yang kurang tepat)</p></li></ul></div></div>";


?>