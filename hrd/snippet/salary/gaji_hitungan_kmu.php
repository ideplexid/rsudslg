<?php 
/** this page used as formulation example to
 * determine on of the emplooyee salary
 * so the system will determine by it's self 
 * all the programmer needs to do it's just to echo
 * the final value of this current salary component.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 26 Sept 2017
 * @license     : Apache 2
 * version      : 1.2.0
 * 
 * */
global $_HRD_KARYAWAN;
global $_SALARY_POST;
global $_SALARY_RESULT;
global $db;
$id_dokter=$_HRD_KARYAWAN['id'];
$dari=$_POST['f_dari'];
$sampai=$_POST['f_sampai'];
global $querylog;
require_once "hrd/function/gaji_dokter_kmu.php";
$_result=gaji_dokter_kmu($db, $id_dokter,$dari,$sampai);
echo $_result['total'];
$querylog->addMessage($_result['total']);
$querylog->addMessage($_result['table']);


?>