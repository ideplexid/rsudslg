<?php 

/**
 * this file used to calculate the basic value
 * of calculating overtime salary for user.
 * because this some overtime used a formula to 
 * generate a value based on employee main salary.
 * 
 * @author : Nurul Huda
 * @license : LGPLv3
 * @database : smis_hrd_rumus_lembur
 *             smis_hrd_employee
 * @copyright: goblooge@gmail.com
 * @since : 07-Mar-2017
 * @version : 1.0.0
 * 
 * */

global $db;
loadLibrary("smis-libs-function-math");
$id_pegawai=$_POST['id_pegawai'];
$id_rumus=$_POST['rumus'];
$nilai=getSettings($db,"hrd-employee-nilai-lembur","0");
$_hasil=array();
$_hasil['nilai']=$nilai;

$dbtable = new DBTable ( $db, "smis_hrd_rumus_lembur" );
$row=$dbtable->select(array("id"=>$id_rumus));
if($row!=null){
    $formula = $row->formula;
    $pegawai = new DBTable ( $db, "smis_hrd_employee" );
    $pegawai->setFetchMethode(DBTable::$ARRAY_FETCH);
    $dataset=$pegawai->select(array("id"=>$id_pegawai));
    $nilai_formula=mathFormula($dataset,$formula);
    if($nilai_formula!=null){
        $_hasil['nilai']    =   floor($nilai_formula['value']);
        $_hasil['formula']  =   $nilai_formula['formula'];
        $_hasil['rumus']    =   $nilai_formula['rumus'];
    }
}

$res=new ResponsePackage();
$res->setStatus(ResponsePackage::$STATUS_OK);
$res->setContent($_hasil);
echo json_encode($res->getPackage());

?>