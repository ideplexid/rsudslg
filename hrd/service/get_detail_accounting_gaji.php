<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_hrd_salary");
    $salary = $dbtable->selectEventDel($id);
    
    $employee_db = new DBTable($db,"smis_hrd_employee");
    $employee=$employee_db->selectEventDel($salary->id_employee);
    
    $periode_db = new DBTable($db,"smis_hrd_periode_gaji");
    $periode=$periode_db->selectEventDel($salary->id_periode);
    
    
    //content untuk kredit
    $kredit=array();
    $kredit['akun']     = $employee->kredit;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $salary->total_salary;
    $kredit['ket']      = "Hutang Gaji ".$salary->nama_employee." Pada Periode Gaji ".ArrayAdapter::dateFormat("date d M Y",$periode->dari)." - ".ArrayAdapter::dateFormat("date d M Y",$periode->sampai);
    $kredit['code']     = "kredit-gaji-".$salary->id;

    //content untuk debet
    $debet=array();
    $debet['akun']    = $employee->debet;
    $debet['debet']   = $salary->total_salary;
    $debet['kredit']  = 0;
    $debet['ket']     = "Beban Gaji ".$salary->nama_employee." Pada Periode Gaji ".ArrayAdapter::dateFormat("date d M Y",$periode->dari)." - ".ArrayAdapter::dateFormat("date d M Y",$periode->sampai);
    $debet['code']    = "debet-gaji-".$salary->id;

    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $periode->tanggal;
    $header['keterangan']   = "Gaji Karyawan ".$salary->nama_employee." Pada Periode Gaji ".ArrayAdapter::dateFormat("date d M Y",$periode->dari)." - ".ArrayAdapter::dateFormat("date d M Y",$periode->sampai);
    $header['code']         = "gaji-karyawan-".$x->id;
    $header['nomor']        = "GJK-".$x->id;
    $header['debet']        = $salary->total_salary;
    $header['kredit']       = $salary->total_salary;
    $header['io']           = "0";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>