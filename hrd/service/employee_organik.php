<?php
if (isset ( $_POST ['command'] )) {
	$dbtable = new DBTable ( $db, "smis_hrd_employee_aktif" );
	if (isset ( $_POST ['kriteria'] )) {
		$json_kriteria = json_decode ( $_POST ['kriteria'], true );
		$KTR="";
		if ($json_kriteria == null) {
			$KTR="	smis_hrd_employee_aktif.slug LIKE '%" . $_POST ['kriteria'] . "%' 
					OR smis_hrd_employee_aktif.nama_jabatan LIKE '%" . $_POST ['kriteria'] . "%'" ;
		} else if (array_key_exists ( "multiple_kriteria", $json_kriteria )) {
			$jabatan = $json_kriteria ["multiple_kriteria"] ["jabatan"];
			$nama = $json_kriteria ["multiple_kriteria"] ["nama"];
			$nip = $json_kriteria ["multiple_kriteria"] ["nip"];
			$KTR="(  
					(	smis_hrd_employee_aktif.slug LIKE '%" . $jabatan . "%' 
						OR smis_hrd_employee_aktif.nama_jabatan LIKE '%" . $jabatan . "%'
					) AND (
						smis_hrd_employee_aktif.nama LIKE '%" . $nama . "%' 
						OR smis_hrd_employee_aktif.nip LIKE '%" . $nip . "%' 
					) 
				) ";
		} else {
			$regexp = implode ( "|", $json_kriteria );
			$KTR=" smis_hrd_employee_aktif.slug REGEXP '" . $regexp . "' 
					OR smis_hrd_employee_aktif.nama REGEXP '" . $regexp . "' " . "' 
					OR smis_hrd_employee_aktif.keterangan REGEXP '" . $regexp . "' ";
		}
		$dbtable->setCustomKriteria ( "  organik=1  AND  (".$KTR.")" );
        $dbtable->ConjuctionCustomKriteria(" AND ");
	}
	
	$strict=isset($_POST['strict']) && $_POST['strict']=="1";
	if($strict) {
		$dbtable->setCustomHaving(" (ruangan='*' OR ruangan LIKE '%".$_POST['ruangan']."% ' OR ruangan LIKE '%".$_POST['ruangan']."' ) ");
	}
    
	$dbtable->setOrder(" smis_hrd_employee_aktif.nama ASC ");
	
	$q=$dbtable->getQueryView("","0");
	
	//echo $q['query'];
	
	$service = new ServiceProvider ( $dbtable );
	$pack = $service->command ( $_POST ['command'] );
	
	echo json_encode ( $pack );
}
?>