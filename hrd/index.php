<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("hrd", $user,"modul/");	
	$policy=new Policy("hrd", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	
	$policy->addPolicy("data_induk","data_induk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk");
	$policy->addPolicy("keterangan_data_induk", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/keterangan_data_induk");
	$policy->addPolicy("job", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/job");
	$policy->addPolicy("golongan", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/golongan");
	$policy->addPolicy("ruangan_pegawai", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/ruangan_pegawai");
	$policy->addPolicy("status_tenaga", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/status_tenaga");
	$policy->addPolicy("pendidikan", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/pendidikan");
	$policy->addPolicy("jenis_bon", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/jenis_bon");
	$policy->addPolicy("rumus_lembur", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/rumus_lembur");
	$policy->addPolicy("grup_gaji", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/grup_gaji");
	$policy->addPolicy("gaji", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/gaji");
	
	$policy->addPolicy("kepegawaian","kepegawaian",Policy::$DEFAULT_COOKIE_CHANGE,"modul/kepegawaian");
	$list=getAllFileInDir("hrd/resource/php/kepegawaian/",false,true);
    foreach($list as $x){
        $xname=str_replace(".php","",$x);
        $policy->addPolicy($xname, "kepegawaian", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/kepegawaian/".$xname);    
	}
	
	$policy->addPolicy("penggajian_pegawai","penggajian_pegawai",Policy::$DEFAULT_COOKIE_CHANGE,"modul/penggajian_pegawai");
	$list=getAllFileInDir("hrd/resource/php/penggajian_pegawai/",false,true);
    foreach($list as $x){
        $xname=str_replace(".php","",$x);
        $policy->addPolicy($xname, "penggajian_pegawai", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/penggajian_pegawai/".$xname);    
    }    
    
	$policy->addPolicy("outsoursing", "outsoursing", Policy::$DEFAULT_COOKIE_CHANGE,"modul/outsoursing");
	$policy->addPolicy("keterangan_outsoursing", "outsoursing", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/outsoursing/keterangan_outsoursing");
	$policy->addPolicy("client", "outsoursing", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/outsoursing/client");
	$policy->addPolicy("accreject", "outsoursing", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/outsoursing/accreject");
	$policy->addPolicy("branch_company", "outsoursing", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/outsoursing/branch_company");
	$policy->addPolicy("filepair", "outsoursing", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/outsoursing/filepair");
	$policy->addPolicy("outsource", "outsoursing", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/outsoursing/outsource");	
	
	$policy->addPolicy("settings_general", "settings_general", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings_general");
	$policy->addPolicy("settings_kmu", "settings_kmu", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings_kmu");
	$policy->addPolicy("settings_sukma", "settings_sukma", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings_sukma");
	
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>
