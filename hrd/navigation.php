<?php
	global $NAVIGATOR;

	// Administrator Top Menu
	$mr = new Menu ("fa fa-graduation-cap");
	$mr->addProperty ( 'title', 'Human Resource Development' );
	$mr->addProperty ( 'name', 'Personalia' );

	$mr->addSubMenu ( "Data Induk", "hrd", "data_induk", "Data Induk HRD","fa fa-database" );
	$mr->addSubMenu ( "Kepegawaian", "hrd", "kepegawaian", "Kepegawaian","fa fa-users" );
	$mr->addSubMenu ( "Penggajian", "hrd", "penggajian_pegawai", "Penggajian Pegawai","fa fa-money" );
	$mr->addSubMenu ( "Settings General", "hrd", "settings_general", "Settings General" ,"fa fa-cog");
	$mr->addSubMenu ( "Settings KMU", "hrd", "settings_kmu", "Settings KMU" ,"fa fa-cogs");
	$mr->addSubMenu ( "Settings Sukma Wijaya", "hrd", "settings_sukma", "Settings Sukma Wijaya" ,"fa fa-cogs");
	$mr->addSubMenu ( "Outsoursing", "hrd", "outsoursing", "Pegawai Out Source" ," fa fa-warning");
	
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "HRD", "hrd");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'hrd' );
?>
