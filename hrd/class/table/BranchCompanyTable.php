<?php 
/**
 * this class used to present the 
 * ui of BranchCompany, it used 
 * as own table because need to 
 * added Extra Button.
 * 
 * @version 	: 1.0.0
 * @author		: Nurul Huda
 * @license		: LGLv2
 * @copyright 	: goblooge@gmail.com
 * @used 		: hrd/resource/php/outsoursing/branch_company.php
 * @since		: 15 Sept 2016
 * @database	: - smis_hrd_branch
 * 
 * */
class BranchCompanyTable extends Table {
	public function getContentButton($id) {
		$btn_group = new ButtonGroup ( 'noprint' );
		if ($this->model == self::$EDIT) {
			$btn_group->add ( "", "", "Edit", $this->action . ".edit('" . $id . "')", "btn-warning", "" );
			$btn_group->add ( "", "", "Del", $this->action . ".del('" . $id . "')", "btn-danger", "" );
			$btn_group->add ( "", "", "Tenaga", $this->action . ".print_cabang_mkip('" . $id . "')", "btn-info", "" );
		} else if ($this->model == self::$SELECT) {
			$btn_group->add ( "", "", "Select", $this->action . ".select('" . $id . "')", "btn-info", "" );
		}
		return $btn_group;
	}
}

?>