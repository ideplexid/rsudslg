<?php 

class AutoSalaryTable extends Table{
    protected $template;
    public function setTemplate($template){
        $this->template=$template;
        return $this;
    }
    
    public function getPrintedElement($p,$q){
        global $db;
        $peride_tbl=new DBTable($db,"smis_hrd_periode_gaji");
        $periode=$peride_tbl->select($p->id_periode);
        
        $penulisan=array();
        $max_dot=0;
        foreach($this->template as $x){
            if($x->tampilkan==1){
                $penulisan[$x->nomor]=array();
                $penulisan[$x->nomor]['nama']=$x->nama;
                $penulisan[$x->nomor]['slug']=$x->slug;
                $penulisan[$x->nomor]['format']=$x->jenis;
                $penulisan[$x->nomor]['nilai']=$x->nilai;
            }
        }
        
        $list_result=(array)$p;
        ksort($penulisan,SORT_NUMERIC);
        
        $table=new TablePrint("auto_salary");
        $table->setMaxWidth(false);
        $table->setDefaultBootrapClass(true);
        $img="<img src='".getLogo()."' />";
        $salary_name=getSettings($db,"smis_autonomous_title","");
        $salary_address=getSettings($db,"smis_autonomous_address","");
        $salary_logo=$img;
        
        $div="<div class='salary_one'>
                <div class='salary_logo'>".$salary_logo."</div>
                <div class='salary_two'>
                    <div class='salary_name'>".$salary_name."</div>
                    <div class='salary_adress'>".$salary_address."</div>
                </div>
              </div>";
        
        
        $table->addColumn($div,6,1);
        $table->commit("body");
        $table->addColumn("Nomor",1,1,null,null,"salary_name_head");
        $table->addColumn(": ".$periode->periode."-".$list_result['id'],2,1,null,null,"salary_name_head");
        $table->addColumn("Periode",1,1,null,null,"salary_name_head");
        $table->addColumn(": ".ArrayAdapter::dateFormat("date d M Y",$periode->dari)." - ".ArrayAdapter::dateFormat("date d M Y",$periode->sampai),2,1,null,null,"salary_name_head");
        $table->commit("body");
        
        $table->addColumn("Nama",1,1,null,null,"salary_name_head");
        $table->addColumn(": ".$list_result['nama_employee'],2,1,null,null,"salary_name_head");
        $table->addColumn("Tanggal",1,1,null,null,"salary_name_head");
        $table->addColumn(": ".ArrayAdapter::dateFormat("date d M Y",$periode->tanggal),2,1,null,null,"salary_name_head");
        $table->commit("body");
        
        $table->addColumn("NIP/Golongan",1,1,null,null,"salary_name_head");
        $table->addColumn(": ".$list_result['nip_employee']."/".$list_result['golongan_employee'],2,1,null,null,"salary_name_head");
        $table->addColumn("Keterangan",1,1,null,null,"salary_name_head");
        $table->addColumn(": ",2,1,null,null,"salary_name_head");
        $table->commit("body");
        
        
        $table->addColumn(strtoupper($periode->template),6,1,null,null,"salary_template");
        $table->commit("body");
        
        foreach($penulisan as $x=>$settings){
            $value = $list_result[$settings['slug']];
            if($settings['format']==0){
                $value = ArrayAdapter::format("money Rp.",$value);
            }else{
                $value = "<div class='right'>".$value."</div>";
            }
            
            if($settings['nilai']=="-1"){
                $table->addColumn($settings['nama']." : ",6,1,null,null,"salary_head_title ");
                $table->commit("body");
            }else if(strpos($settings['nilai'],".php")!==false || is_numeric($settings['nilai']) ){
                $table->addColumn($settings['nama'],3,1,null,null,"salary_sub_title ");
                $table->addColumn($value,2,1,null,null,"salary_sub_money");
                $table->addSpace(1,1);
                $table->commit("body");
            }else{
                $table->addSpace(1,1);
                $table->addColumn($settings['nama'],2,1,null,null,"salary_title");
                $table->addSpace(1,1);
                $table->addColumn($value,2,1,null,null,"salary_money");
                $table->commit("body");
            }
        }
        
        $table->addSpace(4,1);
        $table->addColumn("Diterima Oleh",2,1,null,null,"salary_bottom");
        $table->commit("body");
        
        $table->addSpace(4,1);
        $table->addColumn("</br></br></br>",2,1,null,null,"salary_bottom");
        $table->commit("body");
        
        $table->addSpace(4,1);
        $table->addColumn($list_result['nama_employee'],2,1,null,null,"salary_bottom");
        $table->commit("body");
        
        $css="<style type='text/css'>".getSettings($db,"hrd-css-print-auto-salary","")."</style>";
        return $table->getHtml().$css;
    
    }
    
    public function getPrintedElementDot($p,$q){
        $penulisan=array();
        $max_dot=0;
        foreach($this->template as $x){
            if($x->nilai=="-1"){
                continue;
            }
            
            if($x->tampilkan==1){
                $penulisan[$x->nomor]=array();
                $penulisan[$x->nomor]['dot']=substr_count($x->nomor,".");
                $penulisan[$x->nomor]['nama']=$x->nama;
                $penulisan[$x->nomor]['slug']=$x->slug;
                $penulisan[$x->nomor]['format']=$x->jenis;
                if($max_dot<$penulisan[$x->nomor]['dot']){
                    $max_dot=$penulisan[$x->nomor]['dot'];
                }
            }
        }
        
        $list_result=(array)$p;
        ksort($penulisan,SORT_STRING);
        $table=new TablePrint("");
        $table->setMaxWidth(false);
        $table->setDefaultBootrapClass(true);
        $table->addColumn("Nama",$max_dot+1,"1");
        $table->addColumn("Nilai",$max_dot+1,"1");
        $table->commit("header");
        foreach($penulisan as $x=>$settings){
            $gap=$max_dot-$settings['dot'];
            if($gap<$max_dot){
                $table->addSpace($gap,"1");
            }
            $name=$x." - ".$settings['nama'];
            $value = $list_result[$settings['slug']];
            if($settings['format']==0){
                $value = ArrayAdapter::format("money Rp.",$value);
            }else{
                $value = "<div class='right'>".$value."</div>";
            }
            $table->addColumn($name,$gap+1,"1");
            $table->addColumn($value,$gap+1,"1");
            if($gap<$max_dot){
                $table->addSpace($gap,"1");
            }
            $table->commit("body");
        }
        $css="<style type='text/css'>".getSettings($db,"hrd-css-print-auto-salary","")."</style>";
        return $table->getHtml().$css;
    }
    
}

?>