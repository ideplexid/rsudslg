<?php 
/**
 * digunakan untuk melakukan manajemen
 * agar tampilan untuk data Table memiliki 
 * tombol-tombol uang berfungsi sebagai bantuak dan mencetak
 * sehingga data-data cetak awal per pegawai 
 * dapat di tampilkan
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/oursoursing/accreject.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

class AccRejectTable extends Table {
	public function getHeaderButton() {
		if ($this->model == self::$EDIT) {
			$btn_group = new ButtonGroup ( 'noprint' );
			if ($this->add_button_enable)
				// $btn_group->add("","","Add",$this->action.".show_add_form()", "btn-primary", "data-content='Add New User' data-toggle='popover'");
				$btn_group->add ( "", "", "Print", $this->action . ".print()", "btn-info", "data-content='Print' data-toggle='popover'" );
			$btn_group->add ( "", "", "Bantuan", $this->action . ".help()", "btn", "data-content='Bantuan' data-toggle='popover'" );
			return $btn_group->getHtml ();
		} else {
			return "";
		}
	}
	public function getContentButton($id) {
		$btn_group = new ButtonGroup ( 'noprint' );
		if ($this->model == self::$EDIT) {
			if (in_array ( $this->current_data ['status'], array ("") )) {
				$btn_group->add ( "", "", "Accept", $this->action . ".acc('" . $id . "')", "btn-success", "" );
				$btn_group->add ( "", "", "Reject", $this->action . ".reject('" . $id . "')", "btn-inverse", "" );
				$btn_group->add ( "", "", "Show", $this->action . ".show_data('" . $id . "')", "btn-info", "" );
			} else if ($this->current_data ['status'] == "update") {
				$btn_group->add ( "", "", "Accept", $this->action . ".acc('" . $id . "')", "btn-success", "" );
				$btn_group->add ( "", "", "Restore", $this->action . ".restore('" . $id . "')", "", "" );
				$btn_group->add ( "", "", "Show", $this->action . ".show_data('" . $id . "')", "btn-info", "" );
			} else {
				$btn_group->add ( "", "", "Show", $this->action . ".show_data('" . $id . "')", "btn-info", "" );
			}
		} else if ($this->model == self::$SELECT) {
			$btn_group->add ( "", "", "Select", $this->action . ".select('" . $id . "')", "btn-info", "" );
		}
		return $btn_group;
	}
}


?>