<?php 
/**
 * digunakan untuk melakukan manajemen
 * agar tampilan untuk data Table memiliki 
 * tombol-tombol uang berfungsi sebagai bantuak dan mencetak
 * sehingga data-data cetak awal per pegawai 
 * dapat di tampilkan
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/oursoursing/outsource.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
class OutSourceTable extends Table {
	public function getHeaderButton() {
		if ($this->model == self::$EDIT) {
			$btn_group = new ButtonGroup ( 'noprint' );
			if ($this->add_button_enable)
				$btn_group->add ( "", "", "Add", $this->action . ".show_add_form()", "btn-primary", "data-content='Add New User' data-toggle='popover'" );
			$btn_group->add ( "", "", "Print", $this->action . ".print()", "btn-info", "data-content='Print' data-toggle='popover'" );
			$btn_group->add ( "", "", "Bantuan", $this->action . ".help()", "btn", "data-content='Bantuan' data-toggle='popover'" );
			return $btn_group->getHtml ();
		} else {
			return "";
		}
	}
	public function getContentButton($id) {
		$btn_group = new ButtonGroup ( 'noprint' );
		$status = $this->current_data ['status'];
		if ($this->model == self::$EDIT) {
			if (in_array ( $status, array ("acc","update","" ) )) {
				$btn_group->add ( "", "", "Edit", $this->action . ".edit('" . $id . "')", "btn-warning", "" );
			}			
			if (in_array ( $status, array ("reject","update" ) )) {
				$btn_group->add ( "", "", "Detail", $this->action . ".show_data('" . $id . "')", "btn-info", "" );
			}			
			if (in_array ( $status, array ("reject","" ) )) {
				$btn_group->add ( "", "", "Del", $this->action . ".del('" . $id . "')", "btn-inverse", "" );
			}			
			if (in_array ( $status, array ("reject" ) )) {
				$btn_group->add ( "", "", "Re-New", $this->action . ".renew('" . $id . "')", "btn-danger", "" );
			}			
			if (in_array ( $status, array ("restore") )) {
				$btn_group->add ( "", "", "Restore", $this->action . ".restore('" . $id . "')", "btn-danger", "" );
			}
		} else if ($this->model == self::$SELECT) {
			$btn_group->add ( "", "", "Select", $this->action . ".select('" . $id . "')", "btn-info", "" );
		}
		return $btn_group;
	}
}


?>