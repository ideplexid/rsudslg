<?php 

/**
 * digunakan untuk melakukan manajemen client
 * yang mana untuk child table yang merupakan cabang
 * agar dimunculkan tenaga.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/outsoursing/client.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
 
class ChildClientTable extends Table {
	public function getContentButton($id) {
		$btn_group = parent::getContentButton ( $id );
		if ($this->model == self::$EDIT) {
			$btn_group->add ( "", "", "Tenaga", $this->action.".print_cabang_perusahaan('" . $this->current_data ['id_perusahaan'] . "','" . $id . "')", "btn-info", "" );
		}
		return $btn_group;
	}
}

?>