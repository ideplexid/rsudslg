<?php 

class DataAreaLainDBResponder extends DBResponder{
    
    public function command($command){
        if($command=="fixing_area_lain"){
            $this->fixing_area_lain();
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="import_count_total"){
            $content=$this->import_count_total();
            $pack=new ResponsePackage();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="lock_count_total"){
            $content=$this->lock_count_total();
            $pack=new ResponsePackage();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="get_one_loop"){
            $content=$this->get_one_loop();
            $pack=new ResponsePackage();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="get_one_lock"){
            $content=$this->get_one_lock();
            $pack=new ResponsePackage();
            $pack->setContent($content);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            return $pack->getPackage();
        }else if($command=="detail_history"){
            $content=$this->getDetailHistory();
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setWarning(true,"Detail History ",$content);
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    private function lock_count_total(){
        if($_POST['dari']=="" || $_POST['sampai']==""){
            return array();
        }
        $query="update smis_hrd_area_lain set kunci='0' 
                where tanggal>='".$_POST['dari']."' 
                AND tanggal<'".$_POST['sampai']."' ";
        $this->dbtable->get_db()->query($query);        
        $this->dbtable->addCustomKriteria(null," tanggal>='".$_POST['dari']."' ");
        $this->dbtable->addCustomKriteria(null," tanggal<'".$_POST['sampai']."' ");
        $this->dbtable->setMaximum(getSettings($this->dbtable->get_db(),"smis-hrd-max-lock"),50);
        $this->dbtable->setOrder(" DATE(tanggal) ASC, kmu ASC ",true);
        $list=$this->dbtable->view("",0);
        return $list['data'];
        
    }
    
    private function import_count_total(){
        if($_POST['dari']=="" || $_POST['sampai']==""){
            return 0;
        }
        
        require_once "smis-base/smis-include-service-consumer.php";
        $db=$this->getDBTable()->get_db();
        $kamar=getSettings($db,"hrd-slug-kamar-operasi","ok");
        
        $query="update smis_hrd_area_lain set prop='del' 
                where id_unit LIKE '%".$kamar."-%' 
                AND tanggal>='".$_POST['dari']."' 
                AND tanggal<'".$_POST['sampai']."' ";
        $db->query($query);
        
        $serv=new ServiceConsumer($db,"get_ok_list");
        $serv->addData("dari",$_POST['dari']);
        $serv->addData("sampai",$_POST['sampai']);
        $serv->addData("command","count");
        $serv->setEntity($kamar);
        $serv->execute();
        $hasil=$serv->getContent();
        return $hasil;
    }
    
    private function get_one_lock(){
        $id['id']=$_POST['id'];
        $up['kunci']=1;
        $this->dbtable->update($up,$id);
        return true;
    }
    
    public function getDetailHistory(){
        $id=$_POST['id'];
        $x=$this->dbtable->select($id);
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->dbtable->get_db(),"get_history_dokter_utama",null,"registration");
        $serv->addData("nrm",$x->nrm);
        $serv->addData("tanggal",$x->tanggal);
        $serv->execute();
        $content=$serv->getContent();  
        return $content;
    }
    
    private function get_one_loop(){
        require_once "smis-base/smis-include-service-consumer.php";
        $db=$this->getDBTable()->get_db();
        $kamar=getSettings($db,"hrd-slug-kamar-operasi","ok");
        $serv=new ServiceConsumer($db,"get_ok_list");
        $serv->setEntity($kamar);
        $serv->addData("dari",$_POST['dari']);
        $serv->addData("sampai",$_POST['sampai']);
        $serv->addData("command","list");
        $serv->addData("max","1");
        $serv->addData("number",$_POST['number']);
        $serv->execute();
        $hasil=$serv->getContent();
        return $this->insertOneData($db,$kamar,$hasil['data'][0]);
    }
    
    private function insertOneData(Database $db,$slug,$hasil){
        if($hasil==null){
            return 0;
        }
        $data['carabayar']      = $hasil['carabayar'];
        $data['nama']           = $hasil['nama_pasien'];
        $data['nrm']            = $hasil['nrm_pasien'];
        $data['id_unit']        = $slug."-".$hasil['id'];
        $data['tanggal']        = $hasil['waktu'];
        $data['kmu']            = getSettings($db,"hrd-slug-area-kamar-operasi","");
        $data['bagi_asisten']   = $hasil['bagi_asisten_operator'];
        $data['bagi_oomloop']   = $hasil['bagi_oomloop'];
        $data['bagi_dokter']    = $hasil['bagi_dokter'];
        $data['perujuk']        = $hasil['nama_perujuk'];
        $data['oomloop']        = $hasil['nama_oomloop_satu'];
        $data['asisten']        = $hasil['nama_asisten_operator_satu'];
        $data['operator']       = $hasil['nama_operator_satu'];
        $data['id_perujuk']     = $hasil['id_perujuk'];
        $data['id_oomloop']     = $hasil['id_oomloop_satu'];
        $data['id_asisten']     = $hasil['id_asisten_operator_satu'];
        $data['id_operator']    = $hasil['id_operator_satu'];
        $data['kode_perujuk']   = $hasil['kode_perujuk'];
        $data['prop']           = "";        
        $id['id_unit']          =$data['id_unit'];        
        return $this->dbtable->insertOrUpdate($data,$id,null);
    }
    
    private function fixing_area_lain(){
        $db=$this->getDBTable()->get_db();
        $query="UPDATE smis_hrd_area_lain 
                LEFT JOIN smis_hrd_employee 
                ON smis_hrd_area_lain.oomloop=smis_hrd_employee.nama 
                set id_oomloop=smis_hrd_employee.id WHERE id_oomloop=0;";
        $db->query($query);

        $query="UPDATE smis_hrd_area_lain 
                LEFT JOIN smis_hrd_employee 
                ON smis_hrd_area_lain.perujuk=smis_hrd_employee.nama 
                set id_perujuk=smis_hrd_employee.id WHERE id_perujuk=0;";
        $db->query($query);

        $query="UPDATE smis_hrd_area_lain 
                LEFT JOIN smis_hrd_employee 
                ON smis_hrd_area_lain.operator=smis_hrd_employee.nama 
                set id_operator=smis_hrd_employee.id WHERE id_operator=0;";
        $db->query($query);
                
        $query="UPDATE smis_hrd_area_lain 
                LEFT JOIN smis_hrd_employee 
                ON smis_hrd_area_lain.asisten=smis_hrd_employee.nama 
                set id_asisten=smis_hrd_employee.id WHERE id_asisten=0;";
        $db->query($query);
    }
    
}

?>