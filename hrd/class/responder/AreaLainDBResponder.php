<?php 

class AreaLainDBResponder extends DBResponder{
    /* @var ExcelImporter */
    private $importer;
    
    public function command($command){
        if($command=="import_proceed"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $content=$this->import_proceed();
            $pack->setContent("");
            $pack->setWarning(true,"Import Done"," Total Data ".$content." Imported");
            return $pack->getPackage();
        }else if($command=="detach_import"){
            $pack=new ResponsePackage();
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            $content=$this->detach_import();
            $pack->setContent("");
            $pack->setWarning(true,"Detach Done"," Total Data ".$content." Detached");
            return $pack->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    /**
     * @brief remove the imported data
     * @return  null
     */
    private function detach_import(){
        $id=$_POST['id'];
        $db=$this->getDBTable()->get_db();
        $query="SELECT count(*) as total FROM smis_hrd_area_lain WHERE id_unit='IM-".$id."'";
        $total=$db->get_var($query);
        $query="DELETE FROM smis_hrd_area_lain WHERE id_unit='IM-".$id."'";
        $db->query($query);
        return $total;
    }
    
    /**
     * @brief importing processs
     * @return  int total data imported
     */
    private function import_proceed(){
        $id=$_POST['id'];
        $row=$this->dbtable->select($id);
        $this->importer->setFilePath($row->file);
        if($row->max_memory!=""){
            ini_set('memory_limit',$row->max_memory);
        }
        if($row->max_time*1>0){
            ini_set('max_execution_time',$row->max_time);
        }
        
        $column_map=array();
        $column_map[0]="nama";
        $column_map[1]="nrm";
        $column_map[2]="tanggal";
        $column_map[3]="kmu";
        $column_map[4]="operator";
        $column_map[5]="perujuk";
        $column_map[6]="bagi_dokter";
        $column_map[7]="asisten";
        $column_map[8]="bagi_asisten";
        $column_map[9]="oomloop";
        $column_map[10]="bagi_oomloop";
        $column_map[11]="carabayar";
        
        $this->importer->addKey("nrm");
        $this->importer->addKey("tanggal");
        $this->importer->addKey("kmu");
        
        $this->importer->addSetup(0,2,$column_map,null);
        $this->importer->addFixValue("id_unit","IM-".$row->id);
        return $this->importer->execute();
    }
    
    public function postToArray(){
        $result=parent::postToArray();
        $this->importer->setFilePath($result['file']);
        $sheet=$this->importer->countSheet();
        $result['sheets']=$sheet;
        return $result;
    }
    
    public function setImporter(ExcelImporter $importer){
        $this->importer=$importer;
        return $this;
    }
    
}


?>