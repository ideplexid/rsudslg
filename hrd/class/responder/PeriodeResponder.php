<?php 

class PeriodeResponder extends DBResponder{
    
    public function command($command){
        if($command=="copy_gaji"){
            $this->copyGaji();
            return parent::command("list");
        }else{
            return parent::command($command);
        }
    }
    
    public function copyGaji(){
        //inser the header
        $this->dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
        $gajix=$this->dbtable->select(array("id"=>$_POST['id']));
        $gaji=(array)$gajix;
        unset($gaji['id']);
        $gaji['periode']="(Copy) ".$gaji['periode'];
        $gaji['sisa']=$gaji['total'];
        $gaji['lunas']=0;
        $this->dbtable->insert($gaji);
        $new_id=$this->dbtable->get_inserted_id();
        
        //inser detail
        $dtable=new DBTable($this->getDBTable()->get_db(),"smis_hrd_salary");
        $dtable->setShowAll(true);
        $dtable->addCustomKriteria(" id_periode ","='".$_POST['id']."'");
        $dtable->setFetchMethode(DBTable::$OBJECT_FETCH);
        $data=$dtable->view("",0);
        $list=$data['data'];
        foreach($list as $x){
            $xx=(array)$x;
            unset($xx['id']);
            $xx['id_periode']=$new_id;
            $dtable->insert($xx);
        }
    }
    
    public function excel_element(){
        $db=$this->getDBTable()->get_db();
        $periode=$this->getDBTable()->select(array("id"=>$_POST['id']));
        $dbtable=new DBTable($db,"smis_hrd_salary");
        $dbtable->addCustomKriteria(" id_periode "," ='".$_POST['id']."' ");
        $dbtable->setShowAll(true);
        $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        $view=$dbtable->view("",0);
        $list_d=$view['data'];
        
        $dbtable=new DBTable($db,"smis_hrd_gaji");
        $dbtable->setCustomKriteria(array());
        $dbtable->addCustomKriteria(" template "," ='".$periode->template."' ");
        $dbtable->addCustomKriteria(" tampilkan "," ='1' ");
        $dbtable->setOrder(" urutan ASC ",true);
        $dbtable->setShowAll(true);
        $view=$dbtable->view("",0);
        $list_v=$view['data'];
        $count=count($list_v);
        
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        
        /*sheet 1*/
        $sheet=$file->getActiveSheet();
        $sheet->setTitle("DETAIL GAJI");
        $logo=getLogo();
        if(strpos($logo,"smis-upload")!==false){
            $this->addLogo($heet,"A1",$logo);            
        }
        
        $END_COLUMN = PHPExcel_Cell::stringFromColumnIndex($count+3);
        $sheet->mergeCells("A1:B3");
        
        $sheet->mergeCells("C1:".$END_COLUMN."1");
        $sheet->setCellValue("C1",getSettings($db,"smis_autonomous_title",""));
        $sheet->mergeCells("C2:".$END_COLUMN."3");
        $sheet->setCellValue("C2",getSettings($db,"smis_autonomous_address",""));
        
        $sheet->mergeCells("A5:".$END_COLUMN."5");
        $sheet->setCellValue("A5",$periode->template." - ".$periode->periode);
        $center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $sheet->getStyle("A5")->applyFromArray($center);
        $sheet->getStyle("A5:".$END_COLUMN."5")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
        $sheet->getStyle("A5:".$END_COLUMN."5")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
        
        $sheet->setCellValue("A7","Termin");
        $sheet->mergeCells("B7:D7");
        $sheet->setCellValue("B7",": ".$periode->periode);
        
        $sheet->setCellValue("A8","Periode");
        $sheet->mergeCells("B8:D8");
        $sheet->setCellValue("B8",": ".ArrayAdapter::dateFormat("date d M Y",$periode->dari) ." - ". ArrayAdapter::dateFormat("date d M Y",$periode->tanggal));
        
        $sheet->setCellValue("A9","Tanggal");
        $sheet->mergeCells("B9:D9");
        $sheet->setCellValue("B9",": ".ArrayAdapter::dateFormat("date d M Y",$periode->tanggal));
        
        $line=11;
        $sheet->setCellValue("A".$line,"No.");
        $sheet->setCellValue("B".$line,"Nama");
        $sheet->setCellValue("C".$line,"NIP");
        $sheet->setCellValue("D".$line,"Golongan");
        $number=3;
        $sheet->getColumnDimension ( "A" )->setAutoSize ( true );
        $sheet->getColumnDimension ( "B" )->setAutoSize ( true );
        $sheet->getColumnDimension ( "C" )->setAutoSize ( true );
        $sheet->getColumnDimension ( "D" )->setAutoSize ( true );
        foreach($list_v as $v){
            $number++;
            $END_COLUMN = PHPExcel_Cell::stringFromColumnIndex($number);
            $sheet->getColumnDimension ( $END_COLUMN )->setAutoSize ( true );
            $sheet->setCellValue($END_COLUMN.$line,$v->nama);
        }
        
        $sheet->getStyle("A1:".$END_COLUMN.$line )->getFont ()->setBold ( true );
        $sheet->getStyle("A".$line.":".$END_COLUMN.$line)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
        
        
        $line++;
        $no=1;
        foreach($list_d as $d){
            $sheet->setCellValue("A".$line,$no.".");
            $sheet->setCellValue("B".$line,$d['nama_employee']);
            $sheet->setCellValue("C".$line,"'".$d['nip_employee']);
            $sheet->setCellValue("D".$line,$d['golongan_employee']);
            $number=3;
            foreach($list_v as $v){
                $number++;
                $END_COLUMN = PHPExcel_Cell::stringFromColumnIndex($number);
                $sheet->setCellValue($END_COLUMN.$line,$d[$v->slug]);
            }
            $sheet->getStyle("A".$line.":".$END_COLUMN.$line)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        
            $no++;
            $line++;
        }
        $sheet->mergeCells("A".$line.":D".$line);
        $sheet->setCellValue("A".$line,"TOTAL");
        
        $number=3;
        foreach($list_v as $v){
            $number++;
            if($v->jenis=="1"){
                continue;
            }
            $END_COLUMN = PHPExcel_Cell::stringFromColumnIndex($number);
            $sheet->setCellValue($END_COLUMN.$line, "=sum(".$END_COLUMN."9:".$END_COLUMN.($line-1).")" );
            $sheet->getStyle($END_COLUMN."9:".$END_COLUMN.$line )->getNumberFormat()->setFormatCode("#,##0.00");
        
        }
        $sheet->getStyle("A".$line.":".$END_COLUMN.$line)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
        $sheet->getStyle ( "A".$line.":".$END_COLUMN.$line )->getFont ()->setBold ( true );
        
        
        /*sheet 2*/
        $sheet=$file->createSheet(1);
        $sheet->setTitle("TANDA TERIMA");
        
        $sheet->setCellValue("A1","No.");
        $sheet->setCellValue("B1","Nama");
        $sheet->mergeCells("C1:D1");
        $sheet->setCellValue("C1","Tanda Terima");
        $no=1;
        $line=2;
        foreach($list_d as $d){
            $sheet->setCellValue("A".$line,$no.".");
            $sheet->setCellValue("B".$line,$d['nama_employee']);
            if($no%2==1){
                $sheet->setCellValue("C".$line,$no.".");    
            }else{
                $sheet->setCellValue("D".$line,$no.".");
            
            }
            $no++;
            $line++;
        }
        $line--;
        $center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $sheet->getStyle("C1:D".$line)->applyFromArray($center);
        $sheet->getStyle("A1:D".$line)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        
        $sheet->getColumnDimension ( "B" )->setAutoSize ( true );
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$periode->template." - ".$periode->periode.'.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );

        
    }
    
    
    function addLogo($sheet, $coordinate, $path){
        $logo = new PHPExcel_Worksheet_Drawing();
		$logo->setPath($path);
		$logo->setCoordinates($coordinate);
		$logo->setOffsetX(0);
		$logo->setWorksheet($sheet);
	}
    
}

?>