<?php 

/**
 * digunakan untuk membuat Responder khusus
 * penampilan Acc-Reject. karena menampilkan cabang lama
 * dan cabang baru. berikut juga menampilkan data backup
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/oursoursing/accreject.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

class AccRejectResponder extends DBResponder {
	public function move($lokasi_lama, $lokasi_baru, $id) {
		if ($lokasi_lama != $lokasi_baru) {
			$db = $this->dbtable->get_db ();
			$dbtable = new DBTable ( $db, "smis_hrd_move" );
			$move ['id_pegawai'] = $id;
			$move ['id_cabang_lama'] = $lokasi_lama;
			$move ['id_cabang_baru'] = $lokasi_baru;
			$dbtable->insert ( $move );
		}
		$result = $this->dbtable->update ( $data, $id );
	}
	public function save() {
		$id ['id'] = $_POST ['id'];
		$before = $this->edit ();
		$after = $before->backup;
		
		if ($_POST ['status'] == "acc" && $before->status == "") { // acc data baru
			$new ['backup'] = "";
			$new ['status'] = "acc";
			$result = $this->dbtable->update ( $new, $id );
		} else if ($_POST ['status'] == "acc" && $before->status == "update") { // acc data update
			$update = json_decode ( $after, true );
			$update ['backup'] = "";
			$update ['status'] = "acc";
			$result = $this->dbtable->update ( $update, $id );
			$this->move ( $before->lokasi, $update ['lokasi'], $_POST ['id'] );
		} else { // reject,restore
			$data ['status'] = $_POST ['status'];
			$result = $this->dbtable->update ( $data, $id );
		}
		
		$success ['type'] = 'update';
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		if ($result === false)
			$success ['success'] = 0;
		return $success;
	}
}

?>