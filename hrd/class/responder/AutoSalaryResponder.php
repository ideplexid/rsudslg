<?php 

/**this class used for counting
 * the salary system for each employee
 * so every single employee will counting 
 * differently, based on the template
 * 
 * @since       24 Sept 2017
 * @author      Nurul Huda
 * @copyright   goblooge@gmail.com
 * @database    - smis_hrd_salary
 *              - smis_hrd_employee
 * @license     GPLv3
 * @version     1.1.0
 * */
class AutoSalaryResponder extends DBResponder{
    protected $template;
    protected $karyawan;
    protected $total_salary;
    public function setTemplate($template){
        $this->template=$template;
        return $this;
    }
    
    public function postToArray(){
        $result=parent::postToArray();
        $variable=array();
        $max_dot=0;
        
        
        foreach($this->template as $x){
            if($x->nilai=="-1")
                continue;
            $variable[$x->slug]=$x->nilai;
            if($x->nilai=="0"){
                $variable[$x->slug]=$_POST[$x->slug];
            }
        }
        $list_result=$this->loopFormula($variable);
        $final=array_merge($result,$list_result);
        $final['total_salary']=$this->total_salary;
        return $final;
    }
    
    public function save(){
        $result = parent::save();
        $id_periode = $_POST['id_periode'];
        $dbtable    = $this->updateTotalGaji($id_periode);
        $this->notifyAccounting($result['id'],$id_periode,$dbtable,$this->postToArray(),"");
        return $result;
    }
    
    public function delete(){
        $id=$_POST['id'];
        $data=$this->dbtable->select($id);
        $id_periode=$data->id_periode;
        $result=parent::delete();
        $dbtable=$this->updateTotalGaji($id_periode);
        $this->notifyAccounting($id,$id_periode,$dbtable,$data,"del");
        return $result;
    }
    
    /**
     * @brief this function need to update the
     *          total of the data in the parent 
     *          data so the parent data of the salary will
     *          be correct (the summary of the total salary)
     * @param string $id_periode 
     * @return  DBTable
     */
    
    public function updateTotalGaji($id_periode){
        $query  = "SELECT sum(total_salary) as total FROM smis_hrd_salary WHERE id_periode='".$id_periode."' AND prop!='del' ";
        $nilai  = $this->getDBTable()->get_db()->get_var($query);
        $update=array();
        $update['total']=$nilai;
        $update['sisa']=$nilai."-lunas";
        $warp=array();
        $warp['sisa']=DBController::$NO_STRIP_ESCAPE_WRAP;
        $idx=array();
        $idx['id']=$id_periode;
        $dbtable= new DBTable($this->getDBTable()->get_db(),"smis_hrd_periode_gaji");
        $dbtable->update($update,$idx,$warp);
        return $dbtable;
    }
    
    /**
     * @brief get the data of the employee detail;
     * @return  array contains the employee detail
     */
    private function getEmployee(){
        if($this->karyawan==null){
            $dbtable=new DBTable($this->getDBTable()->get_db(),"smis_vhrd_employee");
            $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
            $this->karyawan=$dbtable->select(array("id"=>$_POST["id_employee"]));
        }
        return $this->karyawan;
    }
    
    /**
     * @brief pring the salary slip for employee
     * @return  
     */
    public function printElement(){
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
        return parent::printElement();
    }
    
    /**
     * @brief this forumla used to count each salary component 
     * @param \Array $list 
     * @return  Array contains value of each salary component
     */
    protected function loopFormula(Array $list){
        loadLibrary("smis-libs-function-math");
        $result=array();
        foreach($list as $k=>$value){
            if(strpos($value,".php")!==false){
                global $_HRD_KARYAWAN;
                global $_SALARY_POST;
                global $_SALARY_RESULT;
                $_HRD_KARYAWAN=$this->getEmployee();
                $_SALARY_POST=$list;
                $_SALARY_RESULT=$result;
                ob_start();
                require_once "hrd/snippet/salary/".$value;
                $hasil=ob_get_clean();
                $result[$k]=$hasil;
            }else{
                $hasil=oneFormula($result,$value);
                $result[$k]=$hasil;    
            }
            $this->total_salary=$result[$k];
        }
        return $result;
    }
    
    
    public function notifyAccounting($id,$id_periode,DBTable $dbtable,$x,$operation){
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "hutang_gaji";
        $data['id_data']    = $id;
        $data['entity']     = "hrd";
        $data['service']    = "get_detail_accounting_gaji";
        $data['data']       = $id;
        $data['code']       = "SLR-".$id;
        $data['operation']  = $operation;
        
        $search=array("id"=>$id_periode);
        $result=$dbtable->select($search);
        $data['tanggal']    = $result->tanggal;
        $data['nilai']      = $x['total_salary'];
        $data['uraian']     = "Gaji ".$x['nama_employee']." Pada Periode ".$result->periode;
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $this;
    }
    
    public function excel(){
		global $user;
		loadLibrary("smis-libs-function-export");
		$kriteria=isset($_POST['kriteria'])?$_POST['kriteria']:""; 
		$kriteria=$this->dbtable->escaped_string($kriteria);
        $this->dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
		$this->dbtable->setShowAll(true);		
		$d=$this->dbtable->view($kriteria,"0");
        $list=$this->template;
		
		$data=$d['data'];
        date_default_timezone_set ( "Asia/Jakarta" );
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        $file = new PHPExcel ();
        $sheet=$file->getActiveSheet();
        $total=count($list)+4;
        
        $border['borders']=array();
        $border['borders']['top']=array();
        $border['borders']['top']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        $border['borders']['bottom']=array();
        $border['borders']['bottom']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        
        $last_col=PHPExcel_Cell::stringFromColumnIndex($total-1);
        $sheet ->mergeCells("A1:".$last_col."1")
               ->setCellValue("A1","PERIODE GAJI PEGAWAI");
        $sheet ->setCellValue("A2","Periode")
               ->setCellValue("B2","'".$_POST['f_periode']);
        $sheet ->setCellValue("A3","Template")
               ->setCellValue("B3","'".$_POST['f_periode']);
        $sheet ->setCellValue("A4","Tanggal")
               ->setCellValue("B4",ArrayAdapter::format("date d M Y",$_POST['f_tanggal']));
        $sheet ->setCellValue("A5","Dari")
               ->setCellValue("B5",ArrayAdapter::format("date d M Y",$_POST['f_dari']));
        $sheet ->setCellValue("A6","Sampai")
               ->setCellValue("B6",ArrayAdapter::format("date d M Y",$_POST['f_sampai']));
        $row=7;
        if($_POST['f_golongan']!=""){
           $sheet ->setCellValue("A7","Golongan")
                  ->mergeCells("B7:".$last_col."7")
                  ->setCellValue("B7",$_POST['f_golongan']);
           $row++;
        }
        
        /* header */
        $sheet  ->setCellValue("A".$row,"No.")
                ->setCellValue("B".$row,"Nama")
                ->setCellValue("C".$row,"NIP")
                ->setCellValue("D".$row,"Golongan"); 
        $column=3;
        foreach( $list as $x ){
            $column++;
            $cur_col = PHPExcel_Cell::stringFromColumnIndex($column);
            $sheet   ->setCellValue($cur_col.$row,$x->nama);
        }
        $sheet->getStyle ( "A".$row.":".$last_col.$row )->getFont ()->setBold ( true );
        $sheet->getStyle ( "A".$row.":".$last_col.$row )->applyFromArray ( $border );
        $row++;
        $row_start_data=$row;
        $number=0;
        foreach($data as $d){
            $number++;
            $sheet  ->setCellValue("A".$row,$number.".")
                    ->setCellValue("B".$row,$d['nama_employee'])
                    ->setCellValue("C".$row,"'".$d['nip_employee'])
                    ->setCellValue("D".$row,$d['golongan_employee']); 
            $column=3;
            foreach( $list as $x ){
                $column++;
                $cur_col = PHPExcel_Cell::stringFromColumnIndex($column);
                $sheet   ->setCellValue($cur_col.$row,$d[$x->slug]);
            }
            $row_end_data=$row;
            $row++;
        }
        $column=3;
        $sheet  ->setCellValue("A".$row,"Total");
        foreach( $list as $x ){
            if($x->jenis*1==0){
                $cur_col = PHPExcel_Cell::stringFromColumnIndex($column);
                $sheet   ->setCellValue($cur_col.$row,"=SUM(".$cur_col.$row_start_data.":".$cur_col.$row_end_data.")");
                $sheet->getStyle($cur_col.$row_start_data.":".$cur_col.$row)->getNumberFormat()->setFormatCode('#,##0,00');
            }
            $column++;
        }
        $sheet->getStyle ( "A".$row.":".$last_col.$row )->getFont ()->setBold ( true );
        $sheet->getStyle ( "A".$row.":".$last_col.$row )->applyFromArray ( $border );
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Periode Gaji.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
		return;
	}
    
}

?>