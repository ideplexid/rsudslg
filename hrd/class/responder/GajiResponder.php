<?php 

/**this class used to DDL the table
 * smis_hrd_salary based on input in
 * smis_hrd_gaji
 * 
 * @since       24 Sept 2017
 * @author      Nurul Huda
 * @copyright   goblooge@gmail.com
 * @database    - smis_hrd_salary
 * @license     GPLv3
 * @version     1.2.0
 * */
class GajiResponder extends DBResponder{
    
    public function save(){
        $result=parent::save();
        $this->notifyUpdate();
        return $result;
    }
    
    public function delete(){
        $result=parent::delete();
        $this->notifyUpdate();
        return $result;
    }
    
    /**
     * @brief notify to update the structure of database 
     * @return  
     */
    private function notifyUpdate(){
        $this->dbtable->setOrder(" urutan ASC ",true);
        $this->dbtable->addCustomKriteria(" nilai "," !='-1' ");
        $this->dbtable->setShowAll(true);
        $this->dbtable->setFetchMethode(DBTable::$OBJECT_FETCH);
        $data = $this->dbtable->view("",0);
        $list = $data['data'];
        
        global $wpdb;
        require_once "smis-libs-class/DBCreator.php";
        $dbcreator=new DBCreator($wpdb,"smis_hrd_salary");
        $dbcreator->addColumn("nama","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
        $dbcreator->addColumn("id_employee","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
        $dbcreator->addColumn("nama_employee","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
        $dbcreator->addColumn("nip_employee","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
        $dbcreator->addColumn("id_periode","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
        $dbcreator->addColumn("nilai","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
        foreach($list as $x){
            $dbcreator->addColumn($x->slug,"int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
        }
        $dbcreator->initialize();
        
    }
    
}

?>