<?php 
/**
 * digunakan untuk membuat Responder khusus
 * penampilan Acc-Reject. karena menampilkan cabang lama
 * dan cabang baru. berikut juga menampilkan data backup
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/oursoursing/outsource.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
class OutSourceResponder extends DBResponder {
		public function save() {
			$data = $this->postToArray ();
			$id ['id'] = $_POST ['id'];
			if ($_POST ['id'] == 0 || $_POST ['id'] == "") { // new data
				$result = $this->dbtable->insert ( $data );
				$id ['id'] = $this->dbtable->get_inserted_id ();
				$success ['type'] = 'insert';
			} else {
				$before = $this->edit ();
				if (in_array ( $before->status, array (
						"acc",
						"update" 
				) )) { // acc atau update, perubahanya disimpan di backup
					$new ['backup'] = json_encode ( $data );
					$new ['status'] = "update";
					$result = $this->dbtable->update ( $new, $id );
				} else { // rejct , restore, new di update seperti biasa
					$result = $this->dbtable->update ( $data, $id );
				}
				$success ['type'] = 'update';
			}
			$success ['id'] = $id ['id'];
			$success ['success'] = 1;
			if ($result === false)
				$success ['success'] = 0;
			return $success;
		}
	}

?>