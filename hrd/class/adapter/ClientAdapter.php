<?php 
/**
 * digunakan untuk membuat ClientAdapter
 * yang mana namanya digabung antara nama perusahaan
 * dan cabangnya.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: - hrd/resource/php/oursoursing/accreject.php 
 * 				  - hrd/resource/php/oursoursing/outsource.php
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
 
class ClientAdapter extends ArrayAdapter {
	public function adapt($d) {
		$data = array ();
		$data ['name'] = $d->nama_perusahaan . " - " . $d->nama;
		$data ['value'] = $d->id;
		return $data;
	}
}

?>