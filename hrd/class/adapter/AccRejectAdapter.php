<?php 
/**
 * digunakan untuk membuat adapter khusus
 * penampilan Acc-Reject
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/oursoursing/accreject.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

class AccRejectAdapter extends ArrayAdapter {
	public function adapt($onerow) {
		$array = array ();
		$array ['id'] = $onerow->id;
		$array ['Nama'] = $onerow->nama;
		$array ['Cabang'] = $onerow->nama_cabang;
		$array ['status'] = $onerow->status;
		$array ['Perusahaan'] = $onerow->nama_lokasi;
		$array ['Jabatan'] = $onerow->nama_jabatan;
		$array ['TMT'] = static::format ( 'date d-M-Y', $onerow->tmt );
		$array ['Alamat'] = $onerow->alamat;
		$array ['Lahir'] = $onerow->tempat_lahir . " , " . static::format ( 'date d-M-Y', $onerow->tanggal_lahir );
		
		if ($onerow->status == "") {
			$array ['Status'] = "<font class='label label-important'>New</span>";
		} else if ($onerow->status == "acc") {
			$array ['Status'] = "<font class='label label-success'>Accept</span>";
		} else if ($onerow->status == "reject") {
			$array ['Status'] = "<font class='label label-inverse'>Reject</span>";
		} else if ($onerow->status == "update") {
			$array ['Status'] = "<font class='label label-info'>Updated</span>";
		} else if ($onerow->status == "restore") {
			$array ['Status'] = "<font class='label'>Restore</span>";
		}
		
		$update = $onerow->backup;
		$onerow->backup = "";
		$acc = json_encode ( $onerow );
		
		$result = $this->compare ( $acc, $update, true );
		
		if (in_array ( $onerow->status, array (
				"acc",
				"",
				"reject" 
		) )) {
			$komparasi = self::format ( "json", $acc );
		} else {
			$komparasi = "<table class='table table-bordered table-hover table-striped table-condensed '>";
			$komparasi .= "<tr>";
			$komparasi .= "<th>Data Pembanding</th>";
			$komparasi .= "<th>Asli</th>";
			$komparasi .= "<th>Update</th>";
			$komparasi .= "</tr>";
			
			foreach ( $result as $r => $v ) {
				$komparasi .= "<tr>";
				$komparasi .= "<td>" . $r . "</td>";
				$komparasi .= "<td>" . $v ['original'] . "</td>";
				$komparasi .= "<td>" . $v ['update'] . "</td>";
				$komparasi .= "</tr>";
			}
			$komparasi .= "</table>";
		}
		
		$array ['Status'] .= "<div class='hide' >
				<div id='modal_outsource_" . $onerow->id . "' >
							$komparasi
						</div>
				</div>";
		
		return $array;
	}
}

?>