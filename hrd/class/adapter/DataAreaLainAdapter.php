<?php 


class DataAreaLainAdapter extends ArrayAdapter{
    public function adapt($d){
        $this->number++;
        $x=array();
        $x['id']=$d->id;
        $x['No.']=$this->number.".";
        $x['NRM']=self::digitFormat("only-digit",$d->nrm);
        $x['KMU']=strtoupper($d->kmu);
        $x['Nama']=strtoupper($d->nama);
        $x['Kode']=strtoupper($d->kode_perujuk);
        $x['Tanggal']=self::dateFormat("date d M Y H:i:s",$d->tanggal);
        $x['Operator']=$d->operator;
        $x['Perujuk']=$d->perujuk;
        $x['Omloop']=$d->oomloop;
        $x['Asisten']=$d->asisten;
        $x['Bagi Dokter']=self::moneyFormat("money Rp.",$d->bagi_dokter);
        $x['Bagi Omloop']=self::moneyFormat("money Rp.",$d->bagi_oomloop);
        $x['Bagi Asisten']=self::moneyFormat("money Rp.",$d->bagi_asisten);
        $x['Importer']=$d->id_unit;
        $x['Carabayar']=$d->carabayar;
        $x['Waktu']=substr($d->tanggal,11,5);
        if($x['Waktu']<="14:00"){
            $x['Waktu']="P";
        }else if($x['Waktu']<="18:00"){
            $x['Waktu']="S";
        }else{
            $x['Waktu']="M";
        }
        $state="";
        if($d->id_operator==0){
            $state.="<font class='badge badge-important'>Operator</font>";
        }
        if($d->id_perujuk==0){
            $state.="<font class='badge badge-success'>Perujuk</font>";
        }
        if($d->id_oomloop==0){
            $state.="<font class='badge badge-warning'>Omloop</font>";
        }
        if($d->id_asisten==0){
            $state.="<font class='badge badge-info'>Asisten</font>";
        }
        if($d->kunci==1){
            $state.="<font class='badge badge-inverse'><i class='fa fa-lock'></i></font>";
        }
        $x['Info']=$state;
        return $x;
    }
    
}

?>