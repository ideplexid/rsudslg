<?php 
class JaspelAdapter extends ArrayAdapter{
    private $jaspel;
    private $hak;
    private $persen;
    public function adapt($x){
        $this->number++;
        $hak                = $x["jaspel"]/($x["total_perawat"]==0?1:$x["total_perawat"]);     
        $array              = array();
        $array["No."]       = self::format("back.",$this->number);
        $array["Waktu"]     = self::dateFormat("date d M Y H:i",$x["waktu"]);
        $array["Pasien"]    = $x["nama_pasien"];
        $array["NRM"]       = $x["nrm_pasien"];
        $array["Tindakan"]  = $x["nama_tindakan"];
        $array["Cara Bayar"]= self::slugFormat("unslug",$x["carabayar"]);
        $array["Jaspel"]    = self::moneyFormat("money Rp.",$x["jaspel"]);
        $array["Hak"]       = self::moneyFormat("money Rp.",$hak);
        $array["Team"]      = self::format("back Orang",$x["total_perawat"]);
        $array["Ruangan"]   = self::slugFormat("unslug",$x["smis_entity"]);
        $this->jaspel       = $x["jaspel"]+$this->jaspel;
        $this->hak          = $hak+$this->hak;
        return $array;
    }
    
    public function getContent($data){
        $list               = parent::getContent($data);
        $array              = array();
        $array['NRM']       = "<strong>Total</strong>";
        $array['Jaspel']    = self::moneyFormat("money Rp.",$this->jaspel);
        $array['Hak']       = self::moneyFormat("money Rp.",$this->hak);
        $list[]             = $array;
        $array2['NRM']      = "<strong>Persentase (".$this->persen."%)</strong>";
        $array2['Hak']      = self::moneyFormat("money Rp.",$this->hak*$this->persen/100);
        $list[]             = $array2;
        return $list;
    }
    
    public function setPersen($persen){
        if($persen*1==0) $persen=100;
        $this->persen=$persen;
        return $this;
    }
}
?>