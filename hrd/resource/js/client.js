/**
 * digunakan untuk melakukan manajemen client
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/outsoursing/client.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

var client;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var child_column=new Array('id','id_perusahaan','nama','alamat',
			'awal_legal','akhir_legal',"nomor_kontrak","isi_kontrak",
			'jumlah_pegawai',"tenaga_kerja",'direktur','telp','cabang',		
			"tagihan_gjtk","tagihan_tklain","tagihan_lain","tagihan_jasa","ppn",
			"potongan_pph","potongan_lain","tagihan_bersih","no_tagihan",
			"tgl_tagihan","bank_penerima","no_rekening","nilai_diterima","tgl_diterima");
	var parent_column=new Array('id','nama','keterangan');	
	client=new ParentChildAction("client","hrd","client",parent_column,child_column,'id_perusahaan');
	client.parent.view();
	client.parent.print_perusahaan=function(id){
		var data=this.getRegulerData();
		data['command']="download";
		data['id_perusahaan']=id;
		download(data);
	};
	client.child.print_cabang_perusahaan=function(id_perusahaan,id_cabang){
		var data=this.getRegulerData();
		data['command']="download";
		data['id_perusahaan']=id_perusahaan;
		data['id_cabang']=id_cabang;
		download(data);
	};
});