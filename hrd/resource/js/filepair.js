/**
 * digunakan untuk membuat sebagai javascript
 * khusus untuk acc-reject. karena ada file -generated
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/oursoursing/filepair.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
var filepair;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	filepair=new TableAction("filepair","hrd","filepair",new Array());
	filepair.view();
	
});