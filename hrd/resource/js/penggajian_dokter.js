/**
 * digunakan untuk melakukan 
 * penggajian dokter khusus untuk dokter
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/penggajian_dokter.php
 * @since		: 10 Sept 2017
 * @version		: 1.0.0
 * 
 * */

var penggajian_dokter;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	penggajian_dokter=new TableAction("penggajian_dokter","hrd","penggajian_dokter",column);
	penggajian_dokter.addRegulerData=function(a){
        a['dari']=$("#"+this.prefix+"_dari").val();
        a['sampai']=$("#"+this.prefix+"_sampai").val();
        a['dari_bpjs']=$("#"+this.prefix+"_dari_bpjs").val();
        a['sampai_bpjs']=$("#"+this.prefix+"_sampai_bpjs").val();
        a['grup_gaji']=$("#"+this.prefix+"_grup_gaji").val();
        return a;
    };
});