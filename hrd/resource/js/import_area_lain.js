var import_area_lain;
$(document).ready(function(){
    $(".mydatetime").datetimepicker({minuteStep:1});
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
    var column=new Array('id','tanggal',"max_memory","max_time","file");
    import_area_lain=new TableAction("import_area_lain","hrd","import_area_lain",column);
    import_area_lain.view();
    import_area_lain.import_proceed=function(id){
        var data=this.getRegulerData();
        data['id']=id;
        data['command']="import_proceed";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            import_area_lain.view();
            dismissLoading();
        });
    }
    
    import_area_lain.detach_import=function(id){
        var data=this.getRegulerData();
        data['id']=id;
        data['command']="detach_import";
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            import_area_lain.view();
            dismissLoading();
        });
    }
});