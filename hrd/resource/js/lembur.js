$(document).ready(function(){
    lembur.after_form_shown=function(){
        var id=$("#lembur_id").val();
        var id_lembur=$("#lembur_rumus_lembur").val();
        if(id=="" && id_lembur!="" && id_lembur!="0"){
            $("#lembur_id_rumus").val(id_lembur);
            $("#lembur_id_rumus").trigger("change");
        }
    };
    
    $("#lembur_id_rumus").on("change",function(){
        var id=$("#lembur_rumus").val();
        var a=lembur.getRegulerData();
        a['action']="kalkulasi_lembur";
        a['id_pegawai']=$("#lembur_id_pegawai").val();
        a['rumus']=$("#lembur_id_rumus").val();
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            setMoney("#lembur_nilai",Number(json.nilai));
            setMoney("#lembur_formula",Number(json.formula));
            setMoney("#lembur_rumus",Number(json.rumus));
            dismissLoading();
        });
    });
    
});