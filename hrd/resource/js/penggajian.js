/**
 * digunakan untuk melakukan manajemen 
 * gaji karyawan yang lebih dari satu
 * sehingga gaji-gaji karyawan yang tersusun
 * atas banyak komponen dapat ditulis disini
 * pada akhi bulan dapat diketahui berapa total gaji karyawan
 * yang bersangkutan. (Deprecated karena kebanyakan di embed di Employee)
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/penggajian.php
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

function PenggajianAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
PenggajianAction.prototype.constructor = PenggajianAction;
PenggajianAction.prototype=new TableAction();
PenggajianAction.prototype.getViewData=function(){
	var data=TableAction.prototype.getViewData.call(this);
	data['id_employee']=$("#id_employee").val();
	return data;
};

PenggajianAction.prototype.show_add_form=function(){
	var id_employee=$('#id_employee').val();
	if(id_employee==null || id_employee==''){
		smis_alert("Select Employee", "Please Select The Employee First","alert-danger");
		return;
	}	
	TableAction.prototype.show_add_form.call(this);
};

PenggajianAction.prototype.getRegulerData=function(){
	var data=TableAction.prototype.getRegulerData.call(this);
	data['id_dokter']=$("#penggajian_id").val();
	data['dari']=$("#penggajian_dari").val();
	data['sampai']=$("#penggajian_sampai").val();
    data['perawat']=$("#penggajian_perawat").val();
	return data;
};

PenggajianAction.prototype.list_employee=function(){
	var self=this;
	var data=this.getRegulerData();
	data['super_command']="penggajian_employee";
	$.post('',data,function(res){
		$("#penggajian_modal .modal-body").html(res);
		$("#penggajian_modal").smodal('show');		
		penggajian_employee.view();
	});
};

PenggajianAction.prototype.detail = function(id){
	var data=this.getRegulerData();
	data['id_employee']=id;
	data['page']="hrd";
	data['action']="penggajian";
	this.load(data);
};

PenggajianAction.prototype.view=function(){
	var self=this;
	var data=this.getViewData();
    showLoading();
	$.post('',data,function(res){
		var json=getContent(res);
        $("#table_content_penggajian").html(json);
        dismissLoading();
	});
};





/*ACTION SCRIPT*/

var penggajian;
var penggajian_employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	penggajian=new PenggajianAction("penggajian","hrd","penggajian",column);
	
	penggajian_employee=new TableAction("penggajian_employee","hrd","penggajian",column);
	penggajian_employee.setSuperCommand("penggajian_employee");
	var id_employee=$("#id_employee").val();
	if(id_employee!="" && id_employee>0){
		penggajian_employee.select(id_employee);
	}
    penggajian_employee.selected=function(json){
        $("#penggajian_id").val(json.id);
		$("#penggajian_nama").val(json.nama);
		$("#penggajian_nip").val(json.nip);
        $("#penggajian_perawat").val(json.slug);
        $("#penggajian_modal").smodal('hide');	
    };
});