/**
 * digunakan untuk melakukan manajemen 
 * gaji karyawan yang lebih dari satu
 * sehingga gaji-gaji karyawan yang tersusun
 * atas banyak komponen dapat ditulis disini
 * pada akhi bulan dapat diketahui berapa total gaji karyawan
 * yang bersangkutan. (Deprecated karena kebanyakan di embed di Employee)
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/salary.php
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

function SalaryAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
SalaryAction.prototype.constructor = SalaryAction;
SalaryAction.prototype=new TableAction();
SalaryAction.prototype.getViewData=function(){
	var data=TableAction.prototype.getViewData.call(this);
	data['id_employee']=$("#id_employee").val();
	return data;
};

SalaryAction.prototype.show_add_form=function(){
	var id_employee=$('#id_employee').val();
	if(id_employee==null || id_employee==''){
		smis_alert("Select Employee", "Please Select The Employee First","alert-danger");
		return;
	}	
	TableAction.prototype.show_add_form.call(this);
};

SalaryAction.prototype.getRegulerData=function(){
	var data=TableAction.prototype.getRegulerData.call(this);
	data['id_employee']=$("#id_employee").val();
	return data;
};

SalaryAction.prototype.list_employee=function(){
	var self=this;
	var data=this.getRegulerData();
	data['super_command']="salary_employee";
	$.post('',data,function(res){
		$("#employee_modal .modal-body").html(res);
		$("#employee_modal").smodal('show');		
		salary_employee.view();
	});
};

SalaryAction.prototype.detail = function(id){
	var data=this.getRegulerData();
	data['id_employee']=id;
	data['page']="hrd";
	data['action']="salary";
	this.load(data);
};





/*ACTION SCRIPT*/

var salary;
var salary_employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	salary=new SalaryAction("salary","hrd","salary",column);
	
	salary_employee=new TableAction("salary_employee","hrd","salary",column);
	salary_employee.setSuperCommand("salary_employee");
	var id_employee=$("#id_employee").val();
	if(id_employee!="" && id_employee>0){
		salary_employee.select(id_employee);
	}
    salary_employee.selected=function(json){
        $("#id_employee").val(json.id);
		$("#nama_employee").val(json.nama);
		$("#nip_employee").val(json.nip);
        $("#salary_employee_modal").smodal('hide');	
		salary.view(); 
    };
});