/**
 * digunakan untuk melakukan manajemen karyawan 
 * disendirikan karena adanya tambahan fungsi yaitu salary.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/kepegawaian/employee.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

function EmployeeAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
EmployeeAction.prototype.constructor = EmployeeAction;
EmployeeAction.prototype=new TableAction();
EmployeeAction.prototype.salary = function(id){
	var data=this.getRegulerData();
	data['id_employee']=id;
	data['page']="hrd";
	data['action']="salary";
	this.load(data);
};

EmployeeAction.prototype.her_off = function(id){
	var data=this.getRegulerData();
	data['id']=id;
	data['command']="save";
	data['keluar']="1";
	var self=this;
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();
	});
};

EmployeeAction.prototype.her_out = function(id){
	var data=this.getRegulerData();
	data['id']=id;
	data['command']="save";
	data['keluar']="-1";
	var self=this;
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();
	});
};


var employee;
var employee_debet;
var employee_kredit;

$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','alamat','keterangan',"tanggal_masuk",
						'nip','nidn',"pelatihan","tenaga","organik",
						'jabatan',"pendidikan","jk","tgl_lahir",
						"tmp_lahir","noktp","nokk","menikah",
						"suami_istri","anak_lk","anak_pr",
						"ayah","ibu","ruangan","anak_satu","anak_dua",
						"gaji_pokok","jaspel","tunj_struktural",
						"tunj_fungsional","tunj_pasutri",
						"tunj_jabatan","tunj_pendidikan","grup_gaji",
						"tunj_anak","tunj_kehadiran","uang_makan","uang_transport",
                        "uang_operasi","uang_perujuk",
                        "uang_duduk","uang_pasien","rumus_lembur",
                        "uang_hadir_pagi","uang_hadir_siang","uang_hadir_malam",
                        "tgl_validasi","akhir_kontrak","strata","tahun_lulus",
                        "nama_kampus","prodi","tunjangan_pengabdian","tunjangan_golongan",
                        "bpjs_ketenagakerjaan","bpjs_kesehatan",
                        "telp","hp","no_bpjs","no_bpjsk","agama",
                        "ijasah","pengalaman","nama_bank",
                        "cabang_bank","no_rek","npwp","unit_kerja",
                        "foto","t_kerja","debet","kredit",'j_struktural',
                        'golongan','ruangan_pegawai',"ttd");
	employee=new EmployeeAction("employee","hrd","employee",column);
	employee.addRegulerData=function(d){
		d['s_jk']=$("#employee_search_jk").val();
		d['s_pendidikan']=$("#employee_search_pendidikan").val();
		d['s_jabatan']=$("#employee_search_jabatan").val();
		d['s_tenaga']=$("#employee_search_tenaga").val();
		d['s_pg']=$("#employee_search_pg").val();
        d['s_kontrak']=$("#employee_search_kontrak").val();
        d['s_durasi']=$("#employee_search_durasi").val();
		return d;
	};
    employee.setDuplicateNameView("nama");
	employee.view();
    
    
    employee_debet=new TableAction("employee_debet","hrd","kode_akun",new Array());
    employee_debet.setSuperCommand("employee_debet");
    employee_debet.selected=function(json){
        $("#employee_debet").val(json.nomor);
    };
    employee_kredit=new TableAction("employee_kredit","hrd","kode_akun",new Array());
    employee_kredit.setSuperCommand("employee_kredit");
    employee_kredit.selected=function(json){
        $("#employee_kredit").val(json.nomor);
    };
});
