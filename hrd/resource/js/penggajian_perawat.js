/**
 * digunakan untuk melakukan 
 * penggajian perawat khusus untuk perawat
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/penggajian_perawat.php
 * @since		: 10 Sept 2017
 * @version		: 1.0.0
 * 
 * */

var penggajian_perawat;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	penggajian_perawat=new TableAction("penggajian_perawat","hrd","penggajian_perawat",column);
	penggajian_perawat.addRegulerData=function(a){
        a['dari']=$("#"+this.prefix+"_dari").val();
        a['sampai']=$("#"+this.prefix+"_sampai").val();
        a['dari_bpjs']=$("#"+this.prefix+"_dari_bpjs").val();
        a['sampai_bpjs']=$("#"+this.prefix+"_sampai_bpjs").val();
        a['grup_gaji']=$("#"+this.prefix+"_grup_gaji").val();
        return a;
    };
});