/**
 * this js used to present the 
 * ui of BranchCompany. it's only regular 
 * javascript but need to be separated
 * 
 * @version 	: 1.0.0
 * @author		: Nurul Huda
 * @license		: LGLv2
 * @copyright 	: goblooge@gmail.com
 * @used 		: hrd/resource/php/outsoursing/branch_company.php
 * @since		: 15 Sept 2016
 * @database	: - smis_hrd_branch
 * 
 * */
var branch_company;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','alamat','assets','kepala','telp');
	branch_company=new TableAction("branch_company","hrd","branch_company",column);
	branch_company.print_cabang_mkip=function(id){
		var data=this.getRegulerData();
		data['command']="download";
		data['id_cabang_mkip']=id;
		download(data);
	};
	branch_company.view();
});