var mapping_marketing;
var marketing;

$(document).ready(function(){
    marketing = new TableAction("marketing", "hrd", "mapping_marketing", new Array());
    marketing.setSuperCommand("marketing");
    $('[data-toggle="popover"]').popover({trigger: 'hover','placement':'bottom'});
    var column = new Array('id','nama','nip','nama_jabatan','id_marketing','marketing');
    mapping_marketing = new TableAction("mapping_marketing", "hrd", "mapping_marketing", column);
    mapping_marketing.view();
    
    stypeahead("#mapping_marketing_marketing",3,marketing,"nama",function(json){
		$("#mapping_marketing_marketing").val(json.nama);
		$("#mapping_marketing_id_marketing").val(json.id);
	});
    marketing.selected = function(json){
		$("#mapping_marketing_marketing").val(json.nama);
		$("#mapping_marketing_id_marketing").val(json.id);
	};
});