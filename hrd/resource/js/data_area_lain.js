var loading_data_area_lain_cancel=false;
$(document).ready(function(){
    //formatMoney("#data_area_lain_uang_filter");
    data_area_lain.fixing=function(){
        var a=this.getRegulerData();
        a['command']="fixing_area_lain";
        showLoading();
        $.post("",a,function(res){
            var json=getContent(res);
            data_area_lain.view();
            dismissLoading();
        });    
    };
    
    data_area_lain.detail_history=function(id){
        var data=this.getRegulerData();
        data['command']="detail_history";
        data['id']=id;
        showLoading();
        $.post("",data,function(res){
            var json=getContent(res);
            dismissLoading();
        });
    };
    
    data_area_lain.lock_current=function(){
        var a=this.getRegulerData();
        a['command']="lock_count_total";
        a['dari']=$("#data_area_lain_dari_filter").val();
        a['sampai']=$("#data_area_lain_sampai_filter").val();
        loading_data_area_lain_cancel=false;
        smis_loader.showLoader();
        smis_loader.updateLoader('true',"Preparing Importing",0);
        smis_loader.onCancel=function(){
            loading_data_area_lain_cancel=true;
        };
        $.post("",a,function(res){
            var json=getContent(res);
            if(json.length>0){
                data_area_lain.lock_loop(json,json.length,0);
            }else{
                smis_loader.hideLoader();
            }
        });  
    };
    
    data_area_lain.lock_loop=function(data,total,current){
        if(current>=total || loading_data_area_lain_cancel){
            loading_data_area_lain_cancel=true;
            smis_loader.cancel();
            data_area_lain.view();
            return;
        }
        var x=data[current];
        var a=this.getRegulerData();
        a['dari']=$("#data_area_lain_dari_filter").val();
        a['sampai']=$("#data_area_lain_sampai_filter").val();
        a['command']="get_one_lock";
        a['number']=current;
        a['id']=x['id'];
        smis_loader.updateLoader('true',"Processing... ["+current+"/"+total+"] ",current*100/total);
        $.post("",a,function(res){
            var json=getContent(res);
            data_area_lain.lock_loop(data,total,++current);
        });
    };

    data_area_lain.import_current=function(){
        var a=this.getRegulerData();
        a['command']="import_count_total";
        a['dari']=$("#data_area_lain_dari_filter").val();
        a['sampai']=$("#data_area_lain_sampai_filter").val();
        loading_data_area_lain_cancel=false;
        smis_loader.showLoader();
        smis_loader.updateLoader('true',"Preparing Importing",0);
        smis_loader.onCancel=function(){
            loading_data_area_lain_cancel=true;
        };
        $.post("",a,function(res){
            var json=getContent(res);
            if(json>0){
                data_area_lain.import_loop(json,0);
            }else{
                smis_loader.hideLoader();
            }
        });  
    };
    
    data_area_lain.import_loop=function(total,current){
        if(current>=total || loading_data_area_lain_cancel){
            loading_data_area_lain_cancel=true;
            smis_loader.cancel();
            data_area_lain.view();
            return;
        }
        var a=this.getRegulerData();
        a['dari']=$("#data_area_lain_dari_filter").val();
        a['sampai']=$("#data_area_lain_sampai_filter").val();
        a['command']="get_one_loop";
        a['number']=current;
        smis_loader.updateLoader('true',"Processing...["+current+"/"+total+"] ",current*100/total);
        $.post("",a,function(res){
            var json=getContent(res);
            data_area_lain.import_loop(total,++current);
        });
    };
});