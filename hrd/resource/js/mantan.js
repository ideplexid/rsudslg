/**
 * digunakan untuk melakukan manajemen karyawan 
 * karena adanya fungsi mengembalikan mantan
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/kepegawaian/mantan.php 
 * @since		: 27 Sept 2016
 * @version		: 1.0.0
 * 
 * */

function MantanAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
MantanAction.prototype.constructor = MantanAction;
MantanAction.prototype=new TableAction();
MantanAction.prototype.her_on = function(id){
	var data=this.getRegulerData();
	data['id']=id;
	data['command']="save";
	data['keluar']="0";
	var self=this;
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();
	});
};

var mantan;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','alamat','keterangan',"tanggal_masuk",
						'nip','nidn',"pelatihan","tenaga","organik",
						'jabatan',"pendidikan","jk","tgl_lahir",
						"tmp_lahir","noktp","nokk","menikah",
						"suami_istri","anak_lk","anak_pr",
						"ayah","ibu","ruangan","anak_satu","anak_dua",
						"gaji_pokok","jaspel","tunj_struktural",
						"tunj_fungsional","tunj_pasutri",
						"tunj_jabatan","tunj_pendidikan",
						"tunj_anak","tunj_kehadiran","uang_makan","uang_transport"
						);
	mantan=new MantanAction("mantan","hrd","mantan",column);
	mantan.addViewData=function(d){
		d['s_jk']=$("#mantan_search_jk").val();
		d['s_pendidikan']=$("#mantan_search_pendidikan").val();
		d['s_jabatan']=$("#mantan_search_jabatan").val();
		d['s_tenaga']=$("#mantan_search_tenaga").val();
		d['s_pg']=$("#mantan_search_pg").val();
		return d;
	};
	mantan.view();
});
