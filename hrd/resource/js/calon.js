/**
 * digunakan untuk melakukan manajemen karyawan 
 * karena adanya fungsi mendaftarkan atau memberhasilkan calon
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/kepegawaian/calon.php 
 * @since		: 27 Sept 2016
 * @version		: 1.0.0
 * 
 * */

function CalonAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
CalonAction.prototype.constructor = CalonAction;
CalonAction.prototype=new TableAction();
CalonAction.prototype.her_in = function(id){
	var data=this.getRegulerData();
	data['id']=id;
	data['command']="save";
	data['keluar']="0";
	var self=this;
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();
	});
};

var calon;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','alamat','keterangan',"tanggal_masuk",
						'nip','nidn',"pelatihan","tenaga","organik",
						'jabatan',"pendidikan","jk","tgl_lahir",
						"tmp_lahir","noktp","nokk","menikah",
						"suami_istri","anak_lk","anak_pr",
						"ayah","ibu","ruangan","anak_satu","anak_dua",
						"gaji_pokok","jaspel","tunj_struktural",
						"tunj_fungsional","tunj_pasutri",
						"tunj_jabatan","tunj_pendidikan",
						"tunj_anak","tunj_kehadiran","uang_makan","uang_transport"
						);
	calon=new CalonAction("calon","hrd","calon",column);
	calon.addViewData=function(d){
		d['s_jk']=$("#calon_search_jk").val();
		d['s_pendidikan']=$("#calon_search_pendidikan").val();
		d['s_jabatan']=$("#calon_search_jabatan").val();
		d['s_tenaga']=$("#calon_search_tenaga").val();
		d['s_pg']=$("#calon_search_pg").val();
		return d;
	};
	calon.view();
});
