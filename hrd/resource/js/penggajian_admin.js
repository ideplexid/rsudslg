/**
 * digunakan untuk melakukan 
 * penggajian ob khusus untuk petugas lab
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/penggajian_admin.php
 * @since		: 6 Sept 2017
 * @version		: 1.0.0
 * 
 * */

var penggajian_admin;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	penggajian_admin=new TableAction("penggajian_admin","hrd","penggajian_admin",column);
	penggajian_admin.addRegulerData=function(a){
        a['dari']=$("#"+this.prefix+"_dari").val();
        a['sampai']=$("#"+this.prefix+"_sampai").val();
        a['dari_bpjs']=$("#"+this.prefix+"_dari_bpjs").val();
        a['sampai_bpjs']=$("#"+this.prefix+"_sampai_bpjs").val();
        a['grup_gaji']=$("#"+this.prefix+"_grup_gaji").val();
        return a;
    };
});