/**
 * digunakan untuk membuat sebagai javascript
 * khusus untuk acc-reject. karena ada file -generated
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: hrd/resource/php/oursoursing/outsource.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
 
function OutsourceAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
OutsourceAction.prototype.constructor = OutsourceAction;
OutsourceAction.prototype=new TableAction();

var outsource;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nikah','jabatan','tmt','alamat','tempat_lahir','tanggal_lahir','agama','telp','nosurat_penempatan','bank','rekening','pkwt_awal','pkwt_akhir','tgl_terima_seragam','no_kta','no_idcard','no_jamsostek','no_kesehatan','gaji_pokok','npwp','lokasi','cabang','tgl_bayar_1','bayar_1','tgl_bayar_2','bayar_2');
	outsource=new OutsourceAction("outsource","hrd","outsource",column);
	$("#outsource_lokasi").select2();
	$("#outsource_cabang").select2();
	outsource.show_data=function(id){
		var c=$("#modal_outsource_"+id).html();
		showWarning("Perbandingan Data",c);
	};
	outsource.help=function(id){
		var c=$("#modal_outsource_help").html();
		showWarning("Pengertian Lambang",c);
	};

	outsource.edit=function (id){
		var self=this;
		showLoading();	
		var edit_data=this.getEditData(id);
		$.post('',edit_data,function(res){		
			var json=getContent(res);
			if(json==null) return;
			for(var i=0;i<self.column.length;i++){
				var name=self.column[i];
				if(name=="lokasi" || name=="cabang"){
					$("#"+self.prefix+"_"+name).select2("val",json[""+name]);
				}else{
					$("#"+self.prefix+"_"+name).val(json[""+name]);
				}
				
			}
			self.disabledOnEdit(self.column_disabled_on_edit);
			$("#"+self.prefix+"_add_form").smodal('show');
			dismissLoading();
		});
	};
	
	outsource.print=function(){
		var data=this.getRegulerData();
		data['command']="download";
		download(data);
	};

	outsource.restore=function(id){
		var save_data=this.getRegulerData();
		save_data['command']="save";
		save_data['status']="acc";
		save_data['backup']="";
		save_data['id']=id;
		var self=this;
		showLoading();
		$.ajax({url:"",data:save_data,type:'post',success:function(res){
			var json=getContent(res);
			if(json==null) return;
			self.view();
			self.clear();
			dismissLoading();
		}});
	};

	outsource.renew=function(id){
		var save_data=this.getRegulerData();
		save_data['command']="save";
		save_data['status']="";
		save_data['backup']="";
		save_data['id']=id;
		var self=this;
		showLoading();
		$.ajax({url:"",data:save_data,type:'post',success:function(res){
			var json=getContent(res);
			if(json==null) return;
			self.view();
			self.clear();
			dismissLoading();
		}});
	};

	outsource.getSaveData=function(){
		var save_data=this.getRegulerData();
		save_data['command']="save";
		for(var i=0;i<this.column.length;i++){
			var name=this.column[i];
			save_data[name]=$("#"+this.prefix+"_"+name).val();
		}
		save_data['nama_lokasi']=$("#"+this.prefix+"_lokasi option:selected").text();//utnuk kepentingan search
		save_data['nama_cabang']=$("#"+this.prefix+"_cabang option:selected").text();
		save_data['nama_jabatan']=$("#"+this.prefix+"_jabatan option:selected").text();
		return save_data;
	};

	outsource.view();
});
