/**
 * digunakan untuk melakukan 
 * penggajian lab khusus untuk petugas lab
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/penggajian_lab.php
 * @since		: 24 Aug 2017
 * @version		: 1.0.0
 * 
 * */

var penggajian_lab;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	penggajian_lab=new TableAction("penggajian_lab","hrd","penggajian_lab",column);
	penggajian_lab.addRegulerData=function(a){
        a['dari']=$("#"+this.prefix+"_dari").val();
        a['sampai']=$("#"+this.prefix+"_sampai").val();
        a['dari_bpjs']=$("#"+this.prefix+"_dari_bpjs").val();
        a['sampai_bpjs']=$("#"+this.prefix+"_sampai_bpjs").val();
        a['grup_gaji']=$("#"+this.prefix+"_grup_gaji").val();
        return a;
    };
});