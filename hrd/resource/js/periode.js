$(document).ready(function(){
    periode.detail_gaji=function(id){
        var data={
            page:"hrd",
            action:"auto_salary",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_periode:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#periode").html(res);
            dismissLoading();
        })
    };
    
    periode.copy_gaji=function(id){
        var x=this.getViewData();
        x['command']="copy_gaji";
        x['id']=id;
        var self=this;
        showLoading();
        $.post('',x,function(res){
            var json=getContent(res);
            if(json==null) {
                
            }else{
                $("#"+self.prefix+"_list").html(json.list);
                $("#"+self.prefix+"_pagination").html(json.pagination);	
                self.initPopover();
            }
            self.afterview(json);
            dismissLoading();
        });
    }
    
});