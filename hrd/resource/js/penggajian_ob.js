/**
 * digunakan untuk melakukan 
 * penggajian ob khusus untuk petugas lab
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/penggajian_ob.php
 * @since		: 6 Sept 2017
 * @version		: 1.0.0
 * 
 * */

var penggajian_ob;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','nilai');	
	penggajian_ob=new TableAction("penggajian_ob","hrd","penggajian_ob",column);
	penggajian_ob.addRegulerData=function(a){
        a['dari']=$("#"+this.prefix+"_dari").val();
        a['sampai']=$("#"+this.prefix+"_sampai").val();
        a['grup_gaji']=$("#"+this.prefix+"_grup_gaji").val();
        return a;
    };
});