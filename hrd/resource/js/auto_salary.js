/**
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /hrd/resource/php/kepegawaian/auto_salary.php
 * @since		: 23 September 2017
 * @version		: 1.0.0
 * 
 * */


/*ACTION SCRIPT*/
var auto_salary_periode;
var auto_salary;
var auto_salary_employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','id_employee','nama_employee','nip_employee',"golongan_employee");
    var n_column=$.parseJSON($("#auto_salary_item").val());
    var final_column=n_column.concat(column);
    auto_salary=new TableAction("auto_salary","hrd","auto_salary",final_column);
    auto_salary.addRegulerData=function(x){
       x['id_periode']=$("#auto_salary_id_periode").val();
       x['template']=$("#auto_salary_template").val();
       x['urutan']=$("#auto_salary_urutan").val();
       x['f_golongan']=$("#auto_salary_f_golongan").val();
       x['f_periode']=$("#auto_salary_periode").val();
       x['f_tanggal']=$("#auto_salary_tanggal").val();
       x['f_dari']=$("#auto_salary_dari").val();
       x['f_sampai']=$("#auto_salary_sampai").val();
       return x;
    };
    auto_salary.back_to_periode=function(){
        var data={
            page:"hrd",
            action:"periode",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_periode:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#periode").html(res);
            dismissLoading();
        })
    };
    
    auto_salary_periode=new TableAction("auto_salary_periode","hrd","auto_salary",final_column);
	auto_salary_periode.setSuperCommand("auto_salary_periode");
	auto_salary_periode.selected=function(json){
        $("#auto_salary_id_periode").val(json.id);
		$("#auto_salary_periode").val(json.periode);
		$("#auto_salary_tanggal").val(json.tanggal);
        $("#auto_salary_dari").val(json.dari);
        $("#auto_salary_sampai").val(json.sampai);
        $("#auto_salary_template").val(json.template);
        auto_salary.view();
    };
    
    auto_salary_employee=new TableAction("auto_salary_employee","hrd","auto_salary",final_column);
	auto_salary_employee.setSuperCommand("auto_salary_employee");
	auto_salary_employee.selected=function(json){
        $("#auto_salary_id_employee").val(json.id);
		$("#auto_salary_nama_employee").val(json.nama);
		$("#auto_salary_nip_employee").val(json.nip);
        $("#auto_salary_golongan_employee").val(json.golongan);
    };
    
    var id_employee=$("#auto_salary_id").val();
	if(id_employee!="" && id_employee>0){
		auto_salary_employee.select(id_employee);
	}
    if($("#auto_salary_id_periode").val()!=""){
        var id=$("#auto_salary_id_periode").val();
        auto_salary_periode.select(id);
    }
    
    $("#auto_salary_urutan").on("change",function(){
           auto_salary.view(); 
    });
});