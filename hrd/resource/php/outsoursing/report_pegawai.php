<?php

/**
 * digunakan untuk generate file excel.
 * dengan format tertentu
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_perusahaan
 * 				  - smis_hrd_branch
 * 				  - smis_hrd_outsource
 * 				  - smis_hrd_client
 * 				  - smis_hrd_job
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
 
date_default_timezone_set ( "Asia/Jakarta" );
require_once ("smis-libs-out/php-excel/PHPExcel.php");
$file = new PHPExcel ();
$file->getProperties ()->setCreator ( "PT Mahakam Intan Padi" );
$file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
$file->getProperties ()->setTitle ( "Data Pegawai" );
$file->getProperties ()->setSubject ( "Data Pegawai" );
$file->getProperties ()->setDescription ( "Data Pegawai Generated From system" );
$file->getProperties ()->setKeywords ( "Data Pegawai " );
$file->getProperties ()->setCategory ( "Data Pegawai " );

$perusahaan_dbtable = new DBTable ( $db, "smis_hrd_perusahaan" );
$perusahaan_dbtable->setShowAll ( true );
$per = $perusahaan_dbtable->view ( "", "0" );
$perusahaan = $per ['data'];
$file->removeSheetByIndex ( 0 );

$fillcabang = array ();
$fillcabang['fill']=array();
$fillcabang['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$fillcabang['fill']['color']=array();
$fillcabang['fill']['color']['rgb']='EFEFEF';
$fillcabang['borders']=array();
$fillcabang['borders']['allborders']=array();
$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

$fillheader=array();
$fillheader['fill']=array();
$fillheader['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$fillheader['fill']['color']=array();
$fillheader['fill']['color']['rgb']='999999';

$SHEET_ID = 0;
$index = 0;
foreach ( $perusahaan as $p ) {
	$file->createSheet ( NULL, $SHEET_ID );
	$file->setActiveSheetIndex ( $SHEET_ID );
	$sheet = $file->getActiveSheet ( $SHEET_ID );
	$sheet->setTitle ( $p->nama );
	
	$lokasi_dbtable = new DBTable ( $db, "smis_hrd_client" );
	$qv = "SELECT smis_hrd_client.*, smis_hrd_branch.nama as nama_cabang
			FROM smis_hrd_client LEFT JOIN smis_hrd_branch ON smis_hrd_client.cabang=smis_hrd_branch.id";
	$qc = "SELECT count(*) as total FROM smis_hrd_client LEFT JOIN smis_hrd_branch ON smis_hrd_client.cabang=smis_hrd_branch.id";
	$lokasi_dbtable->setUseWhereforView ( true );
	$lokasi_dbtable->setPreferredQuery ( true, $qv, $qc );
	$lokasi_dbtable->addCustomKriteria ( "smis_hrd_client.id_perusahaan ", "='" . $p->id . "'" );
	$lokasi_dbtable->setShowAll ( true );
	$lok = $lokasi_dbtable->view ( "", "0" );
	$lokasi = $lok ['data'];
	
	$index = 0;
	foreach ( $lokasi as $l ) {
		$index ++;
		$start_index = $index;
		$sheet->mergeCells ( 'A' . $index . ':R' . $index . '' )->setCellValue ( 'A' . $index . '', $p->nama . " - " . $l->nama );
		$sheet->getStyle ( "A" . $index )->getFont ()->setBold ( true );
		$index ++;
		$sheet->mergeCells ( 'A' . $index . ':B' . $index . '' )->setCellValue ( 'A' . $index . '', "LEGAL PERUSAHAAN" );
		$sheet->getStyle ( "A" . $index )->getFont ()->setBold ( true );
		$sheet->mergeCells ( 'C' . $index . ':D' . $index . '' )->setCellValue ( 'C' . $index . '', ArrayAdapter::format ( "date d M Y", $l->awal_legal ) . " - " . ArrayAdapter::format ( "date d M Y", $l->akhir_legal ) );
		
		$sheet->mergeCells ( 'E' . $index . ':F' . $index . '' )->setCellValue ( 'E' . $index . '', "JUMLAH PEGAWAI" );
		$sheet->getStyle ( "E" . $index )->getFont ()->setBold ( true );
		$sheet->mergeCells ( 'G' . $index . ':H' . $index . '' )->setCellValue ( 'G' . $index . '', $l->jumlah_pegawai . " ORANG" );
		$sheet->mergeCells ( 'I' . $index . ':J' . $index . '' )->setCellValue ( 'I' . $index . '', "DIREKTUR [TELP]" );
		$sheet->getStyle ( "I" . $index )->getFont ()->setBold ( true );
		$sheet->mergeCells ( 'K' . $index . ':L' . $index . '' )->setCellValue ( 'K' . $index . '', $l->direktur . " [" . $l->telp . "]" );
		$sheet->mergeCells ( 'M' . $index . ':N' . $index . '' )->setCellValue ( 'M' . $index . '', "CLIENT DARI" );
		$sheet->getStyle ( "M" . $index )->getFont ()->setBold ( true );
		$sheet->mergeCells ( 'O' . $index . ':R' . $index . '' )->setCellValue ( 'O' . $index . '', "PT MKIP - " . $l->nama_cabang );
		$letter = array ("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R");
		$index ++;
		$header = array ("NAMA","NO SURAT PENEMPATAN","JABATAN","TMT","TGL TERIMA SERAGAM","NO IDCARD","NBO KTA","NO JAMSOSTEK","NO KESEHATAN","NPWP","CABANG","BANK","NO REKENING","GAJI POKOK","PEMBAYARAN PERTAMA","NILAI","PEMBAYARAN KEDUA","NILAI");
		$number = 0;
		foreach ( $letter as $let ) {
			$sheet->getColumnDimension ( $let )->setAutoSize ( true );
			$sheet->setCellValue ( $let . $index, $header [$number] );
			$number ++;
		}
		$sheet->getStyle ( "A" . $index . ":R" . $index )->getFont ()->setBold ( true );
		
		$outsource_dbtable = new DBTable ( $db, "smis_hrd_outsource" );
		$qv = "SELECT smis_hrd_outsource.*, smis_hrd_job.nama as nama_jabatan, smis_hrd_job.slug as slug,
				smis_hrd_client.nama as lokasi, smis_hrd_perusahaan.nama as perusahaan, smis_hrd_branch.nama as nama_cabang
				FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
				LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
				LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
				LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
				";
		$qc = "SELECT count(*)
				FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
				LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
				LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
				LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
				";
		$outsource_dbtable->setShowAll ( true );
		$outsource_dbtable->setUseWhereforView ( true );
		$outsource_dbtable->setPreferredQuery ( true, $qv, $qc );
		$outsource_dbtable->setForceOrderForQuery ( true, " smis_hrd_outsource.bank ASC " );
		$outsource_dbtable->addCustomKriteria ( "smis_hrd_outsource.lokasi", "='" . $l->id . "'" );
		$out = $outsource_dbtable->view ( "", 0 );
		$outsource = $out ['data'];
		
		foreach ( $outsource as $o ) {
			$index ++;
			$sheet->setCellValue ( "A" . $index, $o->nama );
			$sheet->setCellValue ( "B" . $index, $o->nosurat_penempatan );
			$sheet->setCellValue ( "C" . $index, $o->nama_jabatan );
			$sheet->setCellValue ( "D" . $index, ArrayAdapter::format ( "date d M Y", $o->tmt ) );
			$sheet->setCellValue ( "E" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_terima_seragam ) );
			$sheet->setCellValue ( "F" . $index, $o->no_kta );
			$sheet->setCellValue ( "G" . $index, $o->no_idcard );
			$sheet->setCellValue ( "H" . $index, $o->no_jamsostek );
			$sheet->setCellValue ( "I" . $index, $o->no_kesehatan );
			$sheet->setCellValue ( "J" . $index, $o->npwp );
			$sheet->setCellValue ( "K" . $index, $o->nama_cabang );
			$sheet->setCellValue ( "L" . $index, $o->bank );
			$sheet->setCellValue ( "M" . $index, $o->rekening . " " );
			$sheet->setCellValue ( "N" . $index, $o->gaji_pokok );
			$sheet->setCellValue ( "O" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_bayar_1 ) );
			$sheet->setCellValue ( "P" . $index, $o->bayar_1 );
			$sheet->setCellValue ( "Q" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_bayar_2 ) );
			$sheet->setCellValue ( "R" . $index, $o->bayar_2 );
		}
		$index += 2;
		
		$month = date ( "n" );
		$year = date ( "Y" );
		$num = cal_days_in_month ( CAL_GREGORIAN, $month, $year );
		$start_day = $year . "/" . $month . "/1";
		$end_day = $year . "/" . $month . "/" . $num;
		
		$sheet->mergeCells ( 'A' . $index . ':E' . $index . '' )->setCellValue ( 'A' . $index . '', "PERPINDAHAN" );
		$sheet->getStyle ( "A" . $index )->getFont ()->setBold ( true );
		$index ++;
		$sheet->setCellValue ( "A" . $index, "PINDAH" );
		$sheet->setCellValue ( "B" . $index, "NAMA" );
		$sheet->setCellValue ( "C" . $index, "NO IDCARD" );
		$sheet->setCellValue ( "D" . $index, "KE" );
		$sheet->setCellValue ( "E" . $index, "TANGGAL" );
		$sheet->getStyle ( "A" . $index . ":E" . $index )->getFont ()->setBold ( true );
		
		$move_dbtable = new DBTable ( $db, "smis_hrd_move" );
		$qv = "SELECT smis_hrd_move.waktu as tanggal,
						smis_hrd_outsource.nama as nama_pegawai ,
						smis_hrd_outsource.no_idcard ,
						smis_hrd_perusahaan.nama as nama_perusahaan,
						smis_hrd_client.nama as nama_cabang
						FROM
						smis_hrd_move LEFT JOIN smis_hrd_client ON smis_hrd_move.id_cabang_lama=smis_hrd_client.id
						LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
						LEFT JOIN smis_hrd_outsource ON smis_hrd_move.id_pegawai=smis_hrd_outsource.id
				";
		$qc = "SELECT count(*) as total
						FROM
						smis_hrd_move LEFT JOIN smis_hrd_client ON smis_hrd_move.id_cabang_lama=smis_hrd_client.id
						LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
						LEFT JOIN smis_hrd_outsource ON smis_hrd_move.id_pegawai=smis_hrd_outsource.id
				";
		
		$move_dbtable->setShowAll ( true );
		$move_dbtable->setUseWhereforView ( true );
		$move_dbtable->setPreferredQuery ( true, $qv, $qc );
		$move_dbtable->addCustomKriteria ( "smis_hrd_move.id_cabang_baru", "='" . $l->id . "'" );
		$move_dbtable->addCustomKriteria ( "DATE(smis_hrd_move.waktu)", ">='" . $start_day . "'" );
		$move_dbtable->addCustomKriteria ( "DATE(smis_hrd_move.waktu)", "<='" . $end_day . "'" );
		$mv = $move_dbtable->view ( "", 0 );
		$move = $mv ['data'];
		
		foreach ( $move as $m ) {
			$index ++;
			$sheet->setCellValue ( "A" . $index, "Masuk" );
			$sheet->setCellValue ( "B" . $index, $m->nama_pegawai );
			$sheet->setCellValue ( "C" . $index, $m->no_idcard );
			$sheet->setCellValue ( "D" . $index, $m->nama_perusahaan . " - " . $m->nama_cabang );
			$sheet->setCellValue ( "E" . $index, ArrayAdapter::format ( "date d M Y", $m->tanggal ) );
		}
		
		$move_dbtable = new DBTable ( $db, "smis_hrd_move" );
		$qv = "SELECT smis_hrd_move.waktu as tanggal,
						smis_hrd_outsource.nama as nama_pegawai ,
						smis_hrd_outsource.no_idcard ,
						smis_hrd_perusahaan.nama as nama_perusahaan,
						smis_hrd_client.nama as nama_cabang
						FROM
						smis_hrd_move LEFT JOIN smis_hrd_client ON smis_hrd_move.id_cabang_baru=smis_hrd_client.id
						LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
						LEFT JOIN smis_hrd_outsource ON smis_hrd_move.id_pegawai=smis_hrd_outsource.id
				";
		$qc = "SELECT count(*) as total
						FROM
						smis_hrd_move LEFT JOIN smis_hrd_client ON smis_hrd_move.id_cabang_lama=smis_hrd_client.id
						LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
						LEFT JOIN smis_hrd_outsource ON smis_hrd_move.id_pegawai=smis_hrd_outsource.id
				";
		
		$move_dbtable->setShowAll ( true );
		$move_dbtable->setUseWhereforView ( true );
		$move_dbtable->setPreferredQuery ( true, $qv, $qc );
		$move_dbtable->addCustomKriteria ( "smis_hrd_move.id_cabang_lama", "='" . $l->id . "'" );
		$move_dbtable->addCustomKriteria ( "DATE(smis_hrd_move.waktu)", ">='" . $start_day . "'" );
		$move_dbtable->addCustomKriteria ( "DATE(smis_hrd_move.waktu)", "<='" . $end_day . "'" );
		
		$mv = $move_dbtable->view ( "", 0 );
		$move = $mv ['data'];
		
		foreach ( $move as $m ) {
			$index ++;
			$sheet->setCellValue ( "A" . $index, "Keluar" );
			$sheet->setCellValue ( "B" . $index, $m->nama_pegawai );
			$sheet->setCellValue ( "C" . $index, $m->no_idcard );
			$sheet->setCellValue ( "D" . $index, $m->nama_perusahaan . " - " . $m->nama_cabang );
			$sheet->setCellValue ( "E" . $index, ArrayAdapter::format ( "date d M Y", $m->tanggal ) );
		}
		
		$sheet->getStyle ( 'A' . $start_index . ":R" . $index )->applyFromArray ( $fillcabang );
		$sheet->getStyle ( "A" . $start_index . ":R" . $start_index )->applyFromArray ( $fillheader );
		
		$index += 2;
	}
	$sheet->getProtection ()->setPassword ( 'PHPExcel' );
	$sheet->getProtection ()->setSheet ( true );
	$sheet->getStyle ( 'S1:BZ' . ($index + 100) )->getProtection ()->setLocked ( PHPExcel_Style_Protection::PROTECTION_UNPROTECTED );
	
	$SHEET_ID ++;
}

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="Data-Pegawai.xls"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );
?>