<?php

/**
 * digunakan untuk generate file excel.
 * dengan format tertentu
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_perusahaan
 * 				  - smis_hrd_branch
 * 				  - smis_hrd_outsource
 * 				  - smis_hrd_client
 * 				  - smis_hrd_job
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
 

date_default_timezone_set ( "Asia/Jakarta" );
require_once ("smis-libs-out/php-excel/PHPExcel.php");
$file = new PHPExcel ();
$file->getProperties ()->setCreator ( "PT Mahakam Intan Padi" );
$file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
$file->getProperties ()->setTitle ( "Data Pegawai" );
$file->getProperties ()->setSubject ( "Data Pegawai" );
$file->getProperties ()->setDescription ( "Data Pegawai Generated From system" );
$file->getProperties ()->setKeywords ( "Data Pegawai " );
$file->getProperties ()->setCategory ( "Data Pegawai " );

$perusahaan_dbtable = new DBTable ( $db, "smis_hrd_perusahaan" );
$perusahaan_dbtable->setShowAll ( true );
$per = $perusahaan_dbtable->view ( "", "0" );
$perusahaan = $per ['data'];
$file->removeSheetByIndex ( 0 );

$fillcabang = array ();
$fillcabang['fill']=array();
$fillcabang['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$fillcabang['fill']['color']=array();
$fillcabang['fill']['color']['rgb']='EFEFEF';
$fillcabang['borders']=array();
$fillcabang['borders']['allborders']=array();
$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

$fillheader=array();
$fillheader['fill']=array();
$fillheader['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$fillheader['fill']['color']=array();
$fillheader['fill']['color']['rgb']='999999';


$SHEET_ID = 0;
$index = 0;
foreach ( $perusahaan as $p ) {
	$file->createSheet ( NULL, $SHEET_ID );
	$file->setActiveSheetIndex ( $SHEET_ID );
	$sheet = $file->getActiveSheet ( $SHEET_ID );
	$sheet->setTitle ( $p->nama );
	
	$lokasi_dbtable = new DBTable ( $db, "smis_hrd_client" );
	$qv = "SELECT smis_hrd_client.*, smis_hrd_branch.nama as nama_cabang
			FROM smis_hrd_client LEFT JOIN smis_hrd_branch ON smis_hrd_client.cabang=smis_hrd_branch.id";
	$qc = "SELECT count(*) as total FROM smis_hrd_client LEFT JOIN smis_hrd_branch ON smis_hrd_client.cabang=smis_hrd_branch.id";
	$lokasi_dbtable->setUseWhereforView ( true );
	$lokasi_dbtable->setPreferredQuery ( true, $qv, $qc );
	$lokasi_dbtable->addCustomKriteria ( "smis_hrd_client.id_perusahaan ", "='" . $p->id . "'" );
	$lokasi_dbtable->setShowAll ( true );
	$lok = $lokasi_dbtable->view ( "", "0" );
	$lokasi = $lok ['data'];
	
	$index = 1;
	$start_index = $index;
	$letter = array ("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T" );
	$header = array ("CABANG","LOKASI","NAMA","NO SURAT PENEMPATAN","JABATAN","TMT","TGL TERIMA SERAGAM","NO IDCARD","NBO KTA","NO JAMSOSTEK","NO KESEHATAN","NPWP","CABANG","BANK","NO REKENING","GAJI POKOK","PEMBAYARAN PERTAMA","NILAI","PEMBAYARAN KEDUA","NILAI");
	$number = 0;
	foreach ( $letter as $let ) {
		$sheet->getColumnDimension ( $let )->setAutoSize ( true );
		$sheet->setCellValue ( $let . $index, $header [$number] );
		$number ++;
	}
	$sheet->getStyle ( "A" . $index . ":T" . $index )->getFont ()->setBold ( true );
	
	foreach ( $lokasi as $l ) {
		$LOKASI = $l->nama;
		$outsource_dbtable = new DBTable ( $db, "smis_hrd_outsource" );
		$qv = "SELECT smis_hrd_outsource.*, smis_hrd_job.nama as nama_jabatan, smis_hrd_job.slug as slug,
				smis_hrd_client.nama as lokasi, smis_hrd_perusahaan.nama as perusahaan, smis_hrd_branch.nama as nama_cabang
				FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
				LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
				LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
				LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
				";
		$qc = "SELECT count(*)
				FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
				LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
				LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
				LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
				";
		$outsource_dbtable->setShowAll ( true );
		$outsource_dbtable->setUseWhereforView ( true );
		$outsource_dbtable->setPreferredQuery ( true, $qv, $qc );
		$outsource_dbtable->setForceOrderForQuery ( true, " smis_hrd_outsource.bank ASC " );
		$outsource_dbtable->addCustomKriteria ( "smis_hrd_outsource.lokasi", "='" . $l->id . "'" );
		$out = $outsource_dbtable->view ( "", 0 );
		$outsource = $out ['data'];
		
		foreach ( $outsource as $o ) {
			$index ++;
			$sheet->setCellValue ( "A" . $index, $o->nama_cabang );
			$sheet->setCellValue ( "B" . $index, $LOKASI );
			$sheet->setCellValue ( "C" . $index, $o->nama );
			$sheet->setCellValue ( "D" . $index, $o->nosurat_penempatan );
			$sheet->setCellValue ( "E" . $index, $o->nama_jabatan );
			$sheet->setCellValue ( "F" . $index, ArrayAdapter::format ( "date d M Y", $o->tmt ) );
			$sheet->setCellValue ( "G" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_terima_seragam ) );
			$sheet->setCellValue ( "H" . $index, $o->no_kta );
			$sheet->setCellValue ( "I" . $index, $o->no_idcard );
			$sheet->setCellValue ( "J" . $index, $o->no_jamsostek );
			$sheet->setCellValue ( "K" . $index, $o->no_kesehatan );
			$sheet->setCellValue ( "L" . $index, $o->npwp );
			$sheet->setCellValue ( "M" . $index, $o->nama_cabang );
			$sheet->setCellValue ( "N" . $index, $o->bank );
			$sheet->setCellValue ( "O" . $index, $o->rekening . " " );
			$sheet->setCellValue ( "P" . $index, $o->gaji_pokok );
			$sheet->setCellValue ( "Q" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_bayar_1 ) );
			$sheet->setCellValue ( "R" . $index, $o->bayar_1 );
			$sheet->setCellValue ( "S" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_bayar_2 ) );
			$sheet->setCellValue ( "T" . $index, $o->bayar_2 );
		}
		$sheet->getStyle ( 'A' . $start_index . ":T" . $index )->applyFromArray ( $fillcabang );
	}
	$sheet->getProtection ()->setPassword ( md5 ( date ( "dmyhisu" ) ) );
	$sheet->getProtection ()->setSheet ( true );
	$sheet->getStyle ( 'U1:BZ' . ($index + 100) )->getProtection ()->setLocked ( PHPExcel_Style_Protection::PROTECTION_UNPROTECTED );
	$SHEET_ID ++;
}
header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="Data-Pegawai.xls"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );
?>