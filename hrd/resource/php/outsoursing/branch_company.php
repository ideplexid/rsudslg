<?php
/**
 * this class file used 
 * to save the branch company of the system
 * so the company would have more than one
 * office.
 * 
 * this office need tobe determine weather
 * it's rent assets or own assets.
 * 
 * @version 	: 1.0.0
 * @author		: Nurul Huda
 * @license		: LGLv2
 * @copyright 	: goblooge@gmail.com
 * @used 		: hrd/resource/php/outsoursing/branch_company.php
 * @since		: 15 Sept 2016
 * @database	: - smis_hrd_branch
 * 
 * */
 
global $db;
if (isset ( $_POST ['command'] ) && $_POST ['command'] == 'download') {
	require_once ("hrd/report_pegawai_tiga.php");
	return;
}
require_once "hrd/class/table/BranchCompanyTable.php";
$uitable = new BranchCompanyTable ( array ('Nama','Alamat','Assets','Kepala','Telp' ), "Cabang Perusahaan", NULL, true );
$uitable->setName ( "branch_company" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter();
	$adapter->add("Nama","nama");
	$adapter->add("Alamat","alamat");
	$adapter->add("Assets","assets");
	$adapter->add("Kepala","kepala");
	$adapter->add("Telp","telp");
	
	$column = array ('id','nama','alamat','assets','kepala','telp');
	$dbtable = new DBTable ( $db, "smis_hrd_branch", $column );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

/* This is Modal Form and used for add and edit the table */
$asset=new OptionBuilder();
$asset->add("Milik Sendiri","sendiri");
$asset->add("Sewa","sewa");
$mcolumn = array ('id','nama','alamat','assets','kepala','telp');
$mname = array ('','Nama','Alamat','Assets','Kepala','Telp');
$mtype = array ('hidden','text','text','select','text','text');
$mvalue = array ('','','',$asset->getContent(),'','');

$uitable->setModal ( $mcolumn, $mtype, $mname, $mvalue );
$modal = $uitable->getModal ();
$modal->setTitle ( "Cabang Perusahaan" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "hrd/resource/js/branch_company.js",false );

?>