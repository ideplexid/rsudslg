<?php

/**
 * digunakan untuk generate file excel.
 * dengan format tertentu
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_perusahaan
 * 				  - smis_hrd_outsource
 * 				  - smis_hrd_branch
 * 				  - smis_hrd_client
 * 				  - smis_hrd_job
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
 

require_once ("smis-libs-out/php-excel/PHPExcel.php");
$file = new PHPExcel ();
$file->getProperties ()->setCreator ( "PT Mahakam Intan Padi" );
$file->getProperties ()->setLastModifiedBy ( "Rumah Sakit Bagian Administrator" );
$file->getProperties ()->setTitle ( "Data Pegawai" );
$file->getProperties ()->setSubject ( "Data Pegawai" );
$file->getProperties ()->setDescription ( "Data Pegawai Generated From system" );
$file->getProperties ()->setKeywords ( "Data Pegawai " );
$file->getProperties ()->setCategory ( "Data Pegawai " );

$fillcabang = array ();
$fillcabang['fill']=array();
$fillcabang['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$fillcabang['fill']['color']=array();
$fillcabang['fill']['color']['rgb']='EFEFEF';
$fillcabang['borders']=array();
$fillcabang['borders']['allborders']=array();
$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;

$fillheader=array();
$fillheader['fill']=array();
$fillheader['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
$fillheader['fill']['color']=array();
$fillheader['fill']['color']['rgb']='999999';


$SHEET_ID = 0;
$index = 0;
$sheet = $file->getActiveSheet ();
$index = 1;
$start_index = $index;
$letter = array ("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U" );
$header = array ("STATUS","CABANG MKIP","LOKASI","CABANG LOKASI","NAMA","NO SURAT PENEMPATAN","JABATAN","TMT","TGL TERIMA SERAGAM","NO IDCARD","NO KTA","NO JAMSOSTEK","NO KESEHATAN","NPWP","BANK","NO REKENING","GAJI POKOK","PEMBAYARAN PERTAMA","NILAI","PEMBAYARAN KEDUA","NILAI" );
$number = 0;
foreach ( $letter as $let ) {
	$sheet->getColumnDimension ( $let )->setAutoSize ( true );
	$sheet->setCellValue ( $let . $index, $header [$number] );
	$number ++;
}
$sheet->getStyle ( "A" . $index . ":U" . $index )->getFont ()->setBold ( true );
$outsource_dbtable = new DBTable ( $db, "smis_hrd_outsource" );
$qv = "SELECT smis_hrd_outsource.*, smis_hrd_job.nama as nama_jabatan, smis_hrd_job.slug as slug,
	smis_hrd_client.nama as lokasi, smis_hrd_perusahaan.nama as perusahaan, smis_hrd_branch.nama as nama_cabang
	FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
	LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
	LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
	LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
";

$qc = "SELECT count(*)
	FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
	LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
	LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
	LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
";

$outsource_dbtable->setShowAll ( true );
$outsource_dbtable->setUseWhereforView ( true );
$outsource_dbtable->setPreferredQuery ( true, $qv, $qc );
$outsource_dbtable->setForceOrderForQuery ( true, " smis_hrd_outsource.bank ASC " );

if (isset ( $_POST ['id_perusahaan'] )) {
	$outsource_dbtable->addCustomKriteria ( "smis_hrd_perusahaan.id", "='" . $_POST ['id_perusahaan'] . "'" );
}
if (isset ( $_POST ['id_cabang'] )) {
	$outsource_dbtable->addCustomKriteria ( "smis_hrd_outsource.lokasi", "='" . $_POST ['id_cabang'] . "'" );
}
if (isset ( $_POST ['id_cabang_mkip'] )) {
	$outsource_dbtable->addCustomKriteria ( "smis_hrd_outsource.cabang", "='" . $_POST ['id_cabang_mkip'] . "'" );
}

$out = $outsource_dbtable->view ( "", 0 );
$outsource = $out ['data'];

global $user;
$info_total = count ( $outsource );
$info_time = date ( "l jS \of F Y h:i:s A" );
$info_user = $user->getNameOnly ();
$info_kunci = substr ( md5 ( $info_total . "-" . date ( "dmyhisu" ) ), 7, 24 );
$info_gembok = md5 ( $info_kunci . "-" . $user . "-" . date ( "dmyhisu" ) );

$dbtable = new DBTable ( $db, "smis_hrd_filepair" );
$dbtable->insert ( array (
		"user" => $info_user,
		"data" => "Create " . $info_time . ", Total " . $info_total,
		"kunci" => $info_kunci,
		"gembok" => $info_gembok 
) );

foreach ( $outsource as $o ) {
	$index ++;
	$status = $o->status == "" ? "New" : $o->status;
	$sheet->setCellValue ( "A" . $index, strtoupper ( $status ) );
	$sheet->setCellValue ( "B" . $index, $o->nama_cabang );
	$sheet->setCellValue ( "C" . $index, $o->perusahaan );
	$sheet->setCellValue ( "D" . $index, $o->lokasi );
	$sheet->setCellValue ( "E" . $index, $o->nama );
	$sheet->setCellValue ( "F" . $index, $o->nosurat_penempatan );
	$sheet->setCellValue ( "G" . $index, $o->nama_jabatan );
	$sheet->setCellValue ( "H" . $index, ArrayAdapter::format ( "date d M Y", $o->tmt ) );
	$sheet->setCellValue ( "I" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_terima_seragam ) );
	$sheet->setCellValue ( "J" . $index, $o->no_kta );
	$sheet->setCellValue ( "K" . $index, $o->no_idcard );
	$sheet->setCellValue ( "L" . $index, $o->no_jamsostek );
	$sheet->setCellValue ( "M" . $index, $o->no_kesehatan );
	$sheet->setCellValue ( "N" . $index, $o->npwp );
	$sheet->setCellValue ( "O" . $index, $o->bank );
	$sheet->setCellValue ( "P" . $index, $o->rekening . " " );
	$sheet->setCellValue ( "Q" . $index, $o->gaji_pokok );
	$sheet->setCellValue ( "R" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_bayar_1 ) );
	$sheet->setCellValue ( "S" . $index, $o->bayar_1 );
	$sheet->setCellValue ( "T" . $index, ArrayAdapter::format ( "date d M Y", $o->tgl_bayar_2 ) );
	$sheet->setCellValue ( "U" . $index, $o->bayar_2 );
}
$sheet->getStyle ( 'A' . $start_index . ":U" . $index )->applyFromArray ( $fillcabang );
$sheet->getProtection ()->setPassword ( $info_kunci );
$sheet->getProtection ()->setSheet ( true );
$sheet->getStyle ( 'V1:BZ' . ($index + 100) )->getProtection ()->setLocked ( PHPExcel_Style_Protection::PROTECTION_UNPROTECTED );

$file->createSheet ( NULL, 1 );
$file->setActiveSheetIndex ( 1 );
$sheet = $file->getActiveSheet ( $SHEET_ID );
$sheet->setTitle ( "Signature" );
$sheet->setCellValue ( "A1", "Created Date" );
$sheet->setCellValue ( "B1", $info_time );
$sheet->setCellValue ( "A2", "Created By" );
$sheet->setCellValue ( "B2", $info_user );
$sheet->setCellValue ( "A3", "Signature Padlock" );
$sheet->setCellValue ( "B3", $info_gembok );
$sheet->setCellValue ( "A4", "Total Data" );
$sheet->setCellValue ( "B4", $info_total );
$sheet->getColumnDimension ( "A" )->setAutoSize ( true );
$sheet->getColumnDimension ( "B" )->setAutoSize ( true );
$sheet->getProtection ()->setPassword ( $info_kunci );
$sheet->getProtection ()->setSheet ( true );

header ( 'Content-Type: application/vnd.ms-excel' );
header ( 'Content-Disposition: attachment;filename="Data-Pegawai.xls"' );
header ( 'Cache-Control: max-age=0' );
$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
$writer->save ( 'php://output' );

?>