<?php
/**
 * digunakan untuk melakukan manajemen 
 * data-data yang sudah di download dari system
 * yang diolah ulang dengan menggunakan excel
 * maka data tersebut akan dibuat sedemikian hingga
 * supaya terkunci. untuk mengecek keaslian
 * data tersebut maka digunakanlah pair key disini
 * jika cocok berarti file tersebut asli
 * dan jika tidak cocok maka file tersebut adalah palsu
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_filepair
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */
global $db;
$uitable = new Table ( array ("Gembok",'Kunci','User','Waktu',"Keterangan" ), "Key File Pair", NULL, false );
$uitable->setName ( "filepair" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "User", "user" );
	$adapter->add ( "Waktu", "waktu", "date d-M-Y H:i" );
	$adapter->add ( "Keterangan", "data" );
	$adapter->add ( "Kunci", "kunci" );
	$adapter->add ( "Gembok", "gembok" );
	$dbtable = new DBTable ( $db, "smis_hrd_filepair" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "hrd/resource/js/filepair.js",false );
?>
