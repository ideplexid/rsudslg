<?php

/**
 * digunakan untuk melakukan manajemen 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_perusahaan
 * 				  - smis_hrd_branch
 * 				  - smis_hrd_client
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

global $db;
if (isset ( $_POST ['command'] ) && $_POST ['command'] == 'download') {
	require_once ("hrd/report_pegawai_tiga.php");
	return;
}

require_once ("smis-framework/smis/database/DBParentChildResponder.php");
require_once "hrd/class/table/ChildClientTable.php";
$btn = new Button ( "", "", "" );
$btn->setIsButton ( Button::$ICONIC );
$btn->setIcon ( "icon-black " . BUtton::$icon_heart );
$parent_uitable = new Table ( array ('Nama',"Keterangan" ), "Perusahaan Client", NULL, true );
$parent_uitable->setName ( "client_parent" );
$parent_uitable->setActionName ( "client.parent" );
$parent_uitable->addContentButton ( "print_perusahaan", $btn );

$child_uitable = new ChildClientTable ( array ('Nama','Alamat','Cabang','Direktur','Telp'), "", NULL, true );
$child_uitable->setName ( "client_child" );
$child_uitable->setActionName ( "client.child" );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	$parent_adapter = new SimpleAdapter ();
	$parent_adapter->add("Nama","nama");
	$parent_adapter->add("Keterangan","keterangan");
	
	$parent_column = array ('id','nama','keterangan');
	$parent_dbtable = new DBTable ( $db, "smis_hrd_perusahaan", $parent_column );
	$parent = new DBParentResponder ( $parent_dbtable, $parent_uitable, $parent_adapter );
	$child_adapter = new SimpleAdapter ();
	$child_adapter->add("id_perusahaan","id_perusahaan")
				  ->add("Nama","nama")
				  ->add("Alamat","alamat")
				  ->add("Cabang","nama_cabang")
				  ->add("Direktur","direktur")
				  ->add("Telp","telp");

	$child_dbtable = new DBTable ( $db, "smis_hrd_client" );
	$qv = "SELECT smis_hrd_client.*, smis_hrd_branch.nama as nama_cabang
			FROM smis_hrd_client LEFT JOIN smis_hrd_branch ON smis_hrd_client.cabang=smis_hrd_branch.id";
	$qc = "SELECT count(*) as total FROM smis_hrd_client LEFT JOIN smis_hrd_branch ON smis_hrd_client.cabang=smis_hrd_branch.id";
	$child_dbtable->setUseWhereforView ( true );
	$child_dbtable->setPreferredQuery ( true, $qv, $qc );
	$child = new DBChildResponder ( $child_dbtable, $child_uitable, $child_adapter, "id_perusahaan" );
	
	$dbres = new DBParentChildResponder ( $parent, $child );
	$data = $dbres->command ( $_POST ['super_command'], $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

// child
$dbtable = new DBTable ( $db, "smis_hrd_branch" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", 0 );
$branch_ad = new SelectAdapter ("nama","id");
$branch = $branch_ad->getContent ( $data ['data'] );

$perusahaan_client = new SelectAdapter ("nama","id");
$perusahaan_client = $branch_ad->getContent ( $data ['data'] );
$mcolumn = array ('id','id_perusahaan','nama','alamat','awal_legal','akhir_legal',"nomor_kontrak","isi_kontrak",'jumlah_pegawai',"tenaga_kerja",'direktur','telp','cabang',"tagihan_gjtk","tagihan_tklain","tagihan_lain","tagihan_jasa","ppn","potongan_pph","potongan_lain","tagihan_bersih","no_tagihan","tgl_tagihan","bank_penerima","no_rekening","nilai_diterima","tgl_diterima" );
$mname = array ('','','Nama','Alamat','Awal Legal','Akhir Legal',"Nomor","Isi",'Jumlah TK',"Detail TK",'Direktur','Telp','Cabang',"Tagihan TK","Tagihan TKLain","Tagihan Lain","Tagihan Jasa","PPN","Potongan PPH","Potongan Lain","Tagihan Bersih","No. Tagihan","Tgl Tagihan","Bank Penerima","No Rekening","Nilai Diterima","Tgl Diterima" );
$mtype = array ('hidden','hidden','text','text','date','date',"text","textarea","text","text",'text','text','select',"text","text","text","text","text","text","text","text","text","text","date","text","text","text","date" );
$mvalue = array ('','','','',"","","",'','','','',"",$branch,"","","","","","","","","","","","","","","" );
$child_uitable->setModal ( $mcolumn, $mtype, $mname, $mvalue );
$child_modal = $child_uitable->getModal ();
$child_modal->setTitle ( "Lokasi - Client" );
$child_modal->setComponentSize(Modal::$MEDIUM);

// parent
$mcolumn = array ('id','nama',"keterangan" );
$mname = array ('','Nama',"Keterangan" );
$mtype = array ('hidden','text',"text" );
$mvalue = array ('','',"" );
$parent_uitable->setModal ( $mcolumn, $mtype, $mname, $mvalue );
$parent_modal = $parent_uitable->getModal ();
$parent_modal->setTitle ( "Perusahaan - Client" );
$parent_modal->setComponentSize(Modal::$MEDIUM);
$parent_modal->addBody ( "lokasi_client", $child_uitable );
$parent_modal->setClass ( Modal::$FULL_MODEL );

echo $parent_uitable->getHtml ();
echo $parent_modal->getHtml ();
echo $child_modal->getHtml ();

echo addJS  ( "framework/smis/js/table_action.js" );
echo addJS  ( "framework/smis/js/child_parent.js" );
echo addJS  ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS  ( "hrd/resource/js/client.js",false );

?>

