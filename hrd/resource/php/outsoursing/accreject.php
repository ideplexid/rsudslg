<?php

/**
 * digunakan untuk melakukan manajemen 
 * penerimaan dan penolakan gaji pegawai 
 * khusus outsource, sehingga untuk gaji pegawai
 * yang sudah diterima tidak di lock sehingga 
 * tidak digunakan untuk hal-hal yang kurang 
 * data akan disimpan dalam bentuk excel yang terkunci 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_job
 * 				  - smis_hrd_branch
 * 				  - smis_hrd_perusahaan
 * 				  - smis_hrd_client
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */	

global $db;
if (isset ( $_POST ['command'] ) && $_POST ['command'] == 'download') {
	require_once ("hrd/report_pegawai_tiga.php");
	return;
}
require_once "hrd/class/table/AccRejectTable.php";
$uitable = new AccRejectTable ( array ('Nama','Jabatan','TMT','Alamat','Lahir',"Perusahaan","Cabang","Status" ), "Terima dan Tolak ", NULL, true );
$uitable->setName ( "accreject" );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	require_once "hrd/class/adapter/AccRejectAdapter.php";	
	require_once "hrd/class/responder/AccRejectResponder.php";	
	$adapter = new AccRejectAdapter ();	
	$dbtable = new DBTable ( $db, "smis_hrd_outsource" );
	$qv = "SELECT smis_hrd_outsource.*, smis_hrd_job.nama as nama_jabatan, smis_hrd_job.slug as slug,
			smis_hrd_client.nama as nama_lokasi, smis_hrd_perusahaan.nama as perusahaan, smis_hrd_branch.nama as nama_cabang  
			FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
			LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
			LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
			LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
			";
	$qc = "SELECT count(*) 
			FROM smis_hrd_outsource LEFT JOIN smis_hrd_job ON smis_hrd_outsource.jabatan=smis_hrd_job.id
			LEFT JOIN smis_hrd_client ON smis_hrd_outsource.lokasi=smis_hrd_client.id
			LEFT JOIN smis_hrd_branch ON smis_hrd_outsource.cabang=smis_hrd_branch.id
			LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id
			";
	$dbtable->setUseWhereforView ( true );
	$dbres = new AccRejectResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}


$dbtable = new DBTable ( $db, "smis_hrd_job" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", 0 );
$job = new SelectAdapter ("nama","id");
$jabatan = $job->getContent ( $data ['data'] );

$agama=new OptionBuilder();
$agama->add("Islam","Islam");
$agama->add("Katholik","Katholik");
$agama->add("Kristen","Kristen");
$agama->add("Hindu","Hindu");
$agama->add("Buddha","Buddha");

$dbtable = new DBTable ( $db, "smis_hrd_branch" );
$dbtable->setShowAll ( true );
$data = $dbtable->view ( "", 0 );
$branch_ad = new SelectAdapter ("nama","id");
$branch = $branch_ad->getContent ( $data ['data'] );

$qv = "SELECT smis_hrd_client.*, smis_hrd_perusahaan.nama as nama_perusahaan
			FROM smis_hrd_client 
		LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id";

$qc = "SELECT count(*) as total FROM smis_hrd_client 
		LEFT JOIN smis_hrd_perusahaan ON smis_hrd_client.id_perusahaan=smis_hrd_perusahaan.id";

$dbtable = new DBTable ( $db, "smis_hrd_client" );
$dbtable->setPreferredQuery ( true, $qv, $qc );
$dbtable->setUseWhereforView ( true );
$dbtable->setShowAll ( true );
$dbtable->addCustomKriteria ( "smis_hrd_perusahaan.prop ", " NOT LIKE 'del'" );
$data = $dbtable->view ( "", 0 );

require_once "hrd/class/adapter/ClientAdapter.php";
$client_ad = new ClientAdapter ();
$client = $client_ad->getContent ( $data ['data'] );

$kawin=new OptionBuilder();
$kawin->add("Kawin","Kawin");
$kawin->add("Lajang","Lajang");
$kawin->add("Duda","Duda");
$kawin->add("Janda","Janda");

$mcolumn = array ('id','nama','nikah','jabatan','tmt','alamat','tempat_lahir','tanggal_lahir','agama','telp','nosurat_penempatan','bank','rekening','pkwt_awal','pkwt_akhir','tgl_terima_seragam','no_kta','no_idcard','no_jamsostek','no_kesehatan','gaji_pokok','npwp','lokasi','cabang','tgl_bayar_1','bayar_1','tgl_bayar_2','bayar_2' );
$mname = array ('','Nama','Kawin','Jabatan','TMT','Alamat','Tempat Lahir','Tanggal Lahir','Agama','Telp','No. Surat Penempatan','Bank','Rekening','Awal PKWT','Akhir PKWT','Terima Seragam','No. KTA','No. IDCARD','No. Jamsostek','No. Kesehatan','Gaji Pokok','NPWP','Lokasi','MKIP','Tanggal Pembayaran Pertama','Nilai Pembayaran Pertama','Tanggal Pembayaran Kedua','Nilai Pembayaran Kedua' );
$mtype = array ('hidden','text','select','select','date','text','text','date','select','text','text','text','text','date','date','date','text','text','text','text','text','text','select','select','date','text','date','text' );
$mvalue = array ('','',$kawin,$jabatan,'','','','',$agama->getContent(),'','','','','','','','','','','','','',$client,$branch,'','','','' );
$uitable->setModal ( $mcolumn, $mtype, $mname, $mvalue );
$modal = $uitable->getModal ();
$modal->setTitle ( "Karyawan" );

require_once "hrd/snippet/color_description.php";
echo $uitable->getHtml ();
echo $modal	->setTitle("Terima - Tolak")
			->setComponentSize(Modal::$MEDIUM)
			->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-select.js" );
echo addJS ( "hrd/resource/js/accreject.js",false );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );
?>