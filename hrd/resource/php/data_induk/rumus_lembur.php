<?php

/**
 * digunakan untuk manajemen rumus lembur
 * sehingga setiap petugas akan memiliki rumus lembur sendiri-sendiri
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_rumus_lembur
 * @since		: 03 Mar 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
$head=array ('Nama','Formula','Keterangan');
$uitable = new Table ( $head, "Bagian", NULL, true );
$uitable->setName ( "rumus_lembur" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Formula", "formula" );
	$adapter->add ( "Keterangan", "keterangan" );
	$dbtable = new DBTable ( $db, "smis_hrd_rumus_lembur" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "formula", "text", "Formula", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$modal = $uitable->getModal ();
$modal->setTitle ( "Rumus Lembur" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var rumus_lembur;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','formula','keterangan','nama');
	rumus_lembur=new TableAction("rumus_lembur","hrd","rumus_lembur",column);
	rumus_lembur.view();	
});
</script>
