<?php 
/**
 * digunakan untuk memasterkan ruangan 
 * di pegawai bukan berdasarkan otorisasi, tapi posisi ruangan
 * dimana dia bekerja.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_ruangan_pegawai
 * @since		: 18 Mei 2017
 * @version		: 1.0.0
 * 
 * */

	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
    $ruangan_pegawai=new MasterTemplate($db, "smis_hrd_ruangan_pegawai", "hrd", "ruangan_pegawai");
	$uitable=$ruangan_pegawai->getUItable();
    $uitable->addHeaderElement("Nama")
			->addHeaderElement("Keterangan");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("ruangan_pegawai", "text", "Nama", "")
			->addModal("keterangan", "textarea", "Keterangan", "");
	$adapter= $ruangan_pegawai->getAdapter();
	$adapter->add("Nama", "ruangan_pegawai")
			->add("Keterangan", "keterangan");
	$ruangan_pegawai->setModalTitle("ruangan_pegawai");
    $ruangan_pegawai->setAdapter($adapter);
    $ruangan_pegawai->initialize();
?>