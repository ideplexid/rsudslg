<?php

	/**
	 * digunakan untuk melakukan manajemen 
	 * database jeni-jenis bon, sehingga jenis bon atau hutang karyawan
	 * di perusahaan dapat dibedakan
	 * 
	 * @author 		: Nurul Huda
	 * @copyright 	: goblooge@gmail.com
	 * @license 	: LGPLv2
	 * @database 	: smis_hrd_grup_gaji
	 * @since		: 14 Mei 2015
	 * @version		: 1.0.0
	 * 
	 * */
	
	global $db;
	require_once 'smis-libs-class/MasterTemplate.php';
	$bon=new MasterTemplate($db, "smis_hrd_grup_gaji", "hrd", "grup_gaji");
	$bon->getUItable()->addHeaderElement("No.");
	$bon->getUItable()->addHeaderElement("Nama");
    $bon->getUItable()->addHeaderElement("Slug");
	$bon->getUItable()->addHeaderElement("Keterangan");
    
    $bon->getAdapter()->setUseNumber(true, "No.","back.");
	$bon->getAdapter()->add("Nama", "nama");
	$bon->getAdapter()->add("Slug", "slug");
	$bon->getAdapter()->add("Keterangan", "keterangan");
    $bon->getUItable()->addModal("id", "hidden", "", "");
	$bon->getUItable()->addModal("nama", "text", "Nama", "");
	$bon->getUItable()->addModal("slug", "text", "Slug", "");
	$bon->getUItable()->addModal("keterangan", "text", "Keterangan", "");
    $bon->getUItable()->addModal("0th", "text", "Poin 0 TH", "");
    $bon->getUItable()->addModal("1th", "text", "Poin 1 TH", "");
    $bon->getUItable()->addModal("2th", "text", "Poin 2 TH", "");
    $bon->getUItable()->addModal("3th", "text", "Poin 3 TH", "");
    $bon->getUItable()->addModal("4th", "text", "Poin 4 TH", "");
    $bon->getUItable()->addModal("5th", "text", "Poin 5 TH", "");
    $bon->getUItable()->addModal("6th", "text", "Poin 6 TH", "");
    $bon->getUItable()->addModal("7th", "text", "Poin 7 TH", "");
    $bon->getUItable()->addModal("8th", "text", "Poin 8 TH", "");
    $bon->getUItable()->addModal("9th", "text", "Poin 9 TH", "");
    $bon->getUItable()->addModal("10th", "text", "Poin 10 TH", "");
	$bon->setModalTitle("Grup Gaji");
	$bon->initialize();
?>

