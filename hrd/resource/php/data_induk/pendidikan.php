<?php 
/**
 * digunakan untuk melakukan manajemen 
 * database pendidikan Pegawai, seperti : 
 * - D3 Komputer
 * - S1 Keperawatan
 * dan lain sebagainya sehingga dapat dikelompokan
 * pegawai berdasarkan pendidikanya
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_pendidikan
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

	require_once 'smis-libs-class/MasterTemplate.php';
    require_once "smis-base/smis-include-duplicate.php";
	global $db;
    
    $duplicate=getSettings($db,"hrd-pendidikan-duplicate","0");
    $uitable = new TableSynchronous ( array(), "Pendidikan", NULL, true );
    $uitable->setName ( "pendidikan" );
    $uitable->setLoopDuplicateButtonEnable($duplicate!="0");
    $uitable->setSynchronizeButton(false);
    $uitable->setDuplicateButton($duplicate!="0");
    $uitable->setReloadButtonEnable(false);
    $uitable->setPrintButtonEnable(false);
    $mahasiswa = new MasterTemplate($db, "smis_hrd_pendidikan", "hrd", "pendidikan");
    $dbtable   = $mahasiswa ->getDBTable();
    $dbtable   ->setPreferredView(true,"smis_vhrd_pendidikan");
	$mahasiswa->setUITable($uitable);
	$uitable->addHeaderElement("Nama")
            ->addHeaderElement("Keterangan")
            ->addHeaderElement("Kebutuhan LK")
            ->addHeaderElement("Kebutuhan PR")
            ->addHeaderElement("Tersedia LK")
            ->addHeaderElement("Tersedia PR")
            ->addHeaderElement("Kekurangan LK")
            ->addHeaderElement("Kekurangan PR");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("pendidikan", "text", "Nama", "")
            ->addModal("keterangan", "textarea", "Keterangan", "")
            ->addModal("butuh_lk", "text", "Kebutuhan Laki-Laki", "")
            ->addModal("butuh_pr", "text", "Kebutuhan Perempuan", "");
	$adapter= new SynchronousViewAdapter ();
	$adapter->add("Nama", "pendidikan")
            ->add("Keterangan", "keterangan")
            ->add("Kebutuhan LK", "butuh_lk")
            ->add("Kebutuhan PR", "butuh_pr")
            ->add("Tersedia LK", "tersedia_lk")
            ->add("Tersedia PR", "tersedia_pr")
            ->add("Kekurangan LK", "kurang_lk")
            ->add("Kekurangan PR", "kurang_pr");
	$mahasiswa->setModalTitle("Pendidikan");
    $mahasiswa->setAdapter($adapter);
    $dbres = new DuplicateResponder ( $mahasiswa->getDBtable(), $mahasiswa->getUItable(), $mahasiswa->getAdapter() );
    $dbres->setDuplicate($duplicate=="1","duplicate_hrd");
    $dbres->addNotifyData("table","smis_hrd_pendidikan");
    $dbres->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    $mahasiswa->setDBresponder($dbres);
    $mahasiswa->setLoaderEnable(true);
    $mahasiswa->setDuplicateNameView("pendidikan");
    $mahasiswa->setModalComponentSize(Modal::$MEDIUM);
	$mahasiswa->initialize();