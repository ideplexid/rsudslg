<?php

/**
 * digunakan untuk melakukan manajemen 
 * database jenis-jenis pekerjaan atau Job Desk / bagian
 * dari masing-masing pegawai, dengan demikian
 * dapat dibedakan mana-mana pegawai dengan job desk tertentu
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_job
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once "smis-base/smis-include-duplicate.php";
$duplicate=getSettings($db,"hrd-job-duplicate","0");
$head=array ('Nama','Slug','Keterangan');

$uitable = new TableSynchronous ( $head, "Bagian", NULL, true );
$uitable->setName ( "job" );
$uitable->setLoopDuplicateButtonEnable($duplicate!="0");
$uitable->setDuplicateButton($duplicate!="0");
$uitable->setSynchronizeButton(false);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
if (isset ( $_POST ['command'] )) {
	$adapter = new SynchronousViewAdapter ();
	$adapter->add ( "Nama", "nama" );
	$adapter->add ( "Slug", "slug" );
	$adapter->add ( "Keterangan", "keterangan" );
	$dbtable = new DBTable ( $db, "smis_hrd_job" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres = new DuplicateResponder ( $dbtable, $uitable, $adapter );
    $dbres->setDuplicate($duplicate=="1","duplicate_hrd");
    $dbres->setAutonomous(getSettings($db,"smis_autonomous_id",""));
    $dbres->addNotifyData("table",$dbtable->getName());
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "nama", "text", "Nama", "" );
$uitable->addModal ( "slug", "text", "Slug", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Job" );

echo $uitable->getHtml ();
echo $modal->getHtml ();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );

?>
<script type="text/javascript">
var job;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	job=new TableAction("job","hrd","job",column);
    job.setDuplicateNameView("nama");
	job.view();	
});
</script>
