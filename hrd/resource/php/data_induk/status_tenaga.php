<?php 
/**
 * digunakan untuk master status tenaga kerja
 * seperti tenaga honorer, tenaga kontrak, tenaga out source dst
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_status_tenaga
 * @since		: 18 Mei 2017
 * @version		: 1.0.0
 * 
 * */

	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
    $status_tenaga=new MasterTemplate($db, "smis_hrd_status_tenaga", "hrd", "status_tenaga");
	$uitable=$status_tenaga->getUItable();
    $uitable->addHeaderElement("Nama")
			->addHeaderElement("Keterangan");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("status_tenaga", "text", "Nama", "")
			->addModal("keterangan", "textarea", "Keterangan", "");
	$adapter= $status_tenaga->getAdapter();
	$adapter->add("Nama", "status_tenaga")
			->add("Keterangan", "keterangan");
	$status_tenaga->setModalTitle("status_tenaga");
    $status_tenaga->setAdapter($adapter);
    $status_tenaga->initialize();
?>