<?php 
    require_once "smis-libs-class/MasterTemplate.php";
    require_once "hrd/class/responder/GajiResponder.php";
    
    $master     = new MasterTemplate($db,"smis_hrd_gaji","hrd","gaji");
    $responder  = new GajiResponder($master->getDBtable(),$master->getUItable(),$master->getAdapter());
    $master ->setDBresponder($responder);
    $option = new OptionBuilder();
    $option -> add("Uang","0");
    $option -> add("Angka","1");
    $master ->getDBtable()->setOrder("template ASC, urutan ASC",true);
    $master ->getUItable()
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false)
            ->setHeader(array("Template","Urutan Hitung","Nomor Cetak","Nama","Slug","Nilai","Jenis","Tampilkan"));
    $master ->getUItable()
            ->addModal("id","hidden","","")
            ->addModal("template","text","Template","")
            ->addModal("nama","text","Nama","")
            ->addModal("slug","text","Slug","")
            ->addModal("nilai","text","Nilai","")
            ->addModal("nomor","text","Nomor Cetak","")
            ->addModal("urutan","text","Nomor Hitung","")
            ->addModal("jenis","select","Jenis",$option->getContent())        
            ->addModal("tampilkan","checkbox","Tampilkan",0);
    $master ->getAdapter()
            ->add("Template","template")
            ->add("Nama","nama")
            ->add("Slug","slug")
            ->add("Nilai","nilai")
            ->add("Nomor Cetak","nomor")
            ->add("Urutan Hitung","urutan")
            ->add("Tampilkan","tampilkan","trivial_1_Tampil_Sembunyi")
            ->add("Jenis","jenis","trivial_0_Uang_Angka");
    $master ->setModalTitle("Gaji");
    $master ->setMultipleInput(true);
    $master ->addNoClear("template");
    $master ->setAutofocusOnMultiple("template");
    $master ->setClearOnEditForNoClear(true);
    $master ->initialize();
?>