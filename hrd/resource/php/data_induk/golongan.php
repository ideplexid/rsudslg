<?php 
/**
 * digunakan untuk membuat master golongan pegawai
 * di bagian hrd seperti golongan A1 , A2 dst.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: smis_hrd_golongan
 * @since		: 18 Mei 2017
 * @version		: 1.0.0
 * 
 * */

	require_once 'smis-libs-class/MasterTemplate.php';
	global $db;
    $golongan=new MasterTemplate($db, "smis_hrd_golongan", "hrd", "golongan");
    $uitable=$golongan->getUItable();
	$uitable->addHeaderElement("Nama")
			->addHeaderElement("Keterangan");
	$uitable->addModal("id", "hidden", "", "")
			->addModal("golongan", "text", "Nama", "")
			->addModal("keterangan", "textarea", "Keterangan", "");
	$adapter= $golongan->getAdapter();
	$adapter->add("Nama", "golongan")
			->add("Keterangan", "keterangan");
	$golongan->setModalTitle("Golongan");
    $golongan->setAdapter($adapter);
    $golongan->initialize();
?>