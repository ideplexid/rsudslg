<?php

	/**
	 * digunakan untuk melakukan manajemen 
	 * database jeni-jenis bon, sehingga jenis bon atau hutang karyawan
	 * di perusahaan dapat dibedakan
	 * 
	 * @author 		: Nurul Huda
	 * @copyright 	: goblooge@gmail.com
	 * @license 	: LGPLv2
	 * @database 	: smis_hrd_jenis_bon
	 * @since		: 14 Mei 2015
	 * @version		: 1.0.0
	 * 
	 * */
	
	global $db;
	require_once 'smis-libs-class/MasterTemplate.php';
	$bon=new MasterTemplate($db, "smis_hrd_jenis_bon", "hrd", "jenis_bon");
	$bon->getUItable()->addHeaderElement("No.");
	$bon->getUItable()->addHeaderElement("Jenis Potongan");
	$bon->getAdapter()->setUseNumber(true, "No.","back.");
	$bon->getAdapter()->add("Jenis Potongan", "jenis_potongan");
	$bon->getUItable()->addModal("jenis_potongan", "text", "Jenis Bon", "");
	$bon->setModalTitle("Jenis Potongan");
	$bon->initialize();
?>

