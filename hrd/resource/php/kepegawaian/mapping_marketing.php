<?php

global $db;

//CHOOSER
$marketing_table = new Table (array('Nama', 'NIP'), "", NULL, true);
$marketing_table->setName("marketing");
$marketing_table->setModel(Table::$SELECT);
$marketing_adapter = new SimpleAdapter();
$marketing_adapter->add("Nama", "nama");
$marketing_adapter->add("NIP", "nip");
$marketing_dbtable = new DBTable($db,"smis_vhrd_employee");
$marketing_dbtable->addCustomKriteria(" nama_jabatan ", " LIKE '%marketing%' ");
$marketing_dbresponder = new DBResponder($marketing_dbtable, $marketing_table, $marketing_adapter);

$super = new SuperCommand ();
$super->addResponder ( "marketing", $marketing_dbresponder );
$init = $super->initialize ();
if ($init != null) {
    echo $init;
    return;
}

$header = array("No.","Nama","Marketing");
$uitable = new Table($header,"Mapping Marketing",NULL,true);
$uitable->setName("mapping_marketing");
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setDelButtonEnable(false);

if(isset($_POST['command'])){
    $adapter = new SimpleAdapter(true,"No.");
	$adapter->add("Nama", "nama");
    $adapter->add("Marketing", "marketing");
    $dbtable = new DBTable($db, "smis_vhrd_employee");
    $dbtable->addCustomKriteria("nama_jabatan", " NOT LIKE '%marketing%' ");
    $dbtable->addCustomKriteria("organik","=0");
    $dbresponder = new DBResponder($dbtable,$uitable,$adapter);
    $data = $dbresponder->command($_POST ['command']);
	echo json_encode($data);	
	return;
}

$uitable->addModal("id", "hidden");
$uitable->addModal("nama", "text", "Nama", "", "y", null, true, null, false, null);
$uitable->addModal("marketing", "chooser-mapping_marketing-marketing-Pilih Marketing", "Marketing", "", "n", null, true, null, false);
$uitable->addModal("id_marketing", "hidden");

$modal = $uitable->getModal();
$modal->setTitle("Marketing");
echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS("framework/bootstrap/js/bootstrap-select.js");
echo addCSS ( "framework/bootstrap/css/bootstrap-select.css" );
echo addJS ( "hrd/resource/js/mapping_marketing.js", false );

?>