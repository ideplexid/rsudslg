<?php
global $db;
show_error();
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
$pasien_karyawan=null;
if(isset($_POST['super_command']) && $_POST['super_command']=="keluarga_karyawan"){
    $dkadapter = new SimpleAdapter ();
    $dkadapter->add ( "Jabatan", "nama_jabatan" );
    $dkadapter->add ( "Nama", "nama" );
    $dkadapter->add ( "NIP", "nip" );
    $header=array ('Nama','Jabatan',"NIP" );
    $dktable = new Table ( $header);
    $dktable->setName ( "keluarga_karyawan" );
    $dktable->setModel ( Table::$SELECT );
    $dbtable=new DBTable($db,"smis_vhrd_employee");
    $pasien_karyawan = new DBResponder ( $dbtable, $dktable, $dkadapter );    
}

$pasien_pasien=null;
if(isset($_POST['super_command']) && $_POST['super_command']=="pasien_pasien"){
    $dkadapter = new SimpleAdapter ();
    $dkadapter->add ( "NRM", "id","only-digit8" );
    $dkadapter->add ( "Nama", "nama" );
    $dkadapter->add ( "Alamat", "alamat" );
    $header=array ('NRM','Nama',"Alamat" );
    $dktable = new Table ( $header);
    $dktable->setName ( "pasien_pasien" );
    $dktable->setModel ( Table::$SELECT );
    $pasien_pasien = new ServiceResponder ($db, $dktable, $dkadapter,"set_change_karyawan" );
    $pasien_pasien->addData("id_karyawan","0");

}

$periksa=new MasterSlaveServiceTemplate($db, "set_change_karyawan", "hrd", "pasien");
$periksa->setDateEnable(true);
$header=array ("No.","Nama",'NRM',"Alamat",'Ayah',"Ibu","Hubungan");
$btn=$periksa ->addFlag("id_karyawan", "Pilih Karyawan", "Silakan Pilih Karyawan Terlebih Dahulu")
			  ->addNoClear("id_karyawan")
			  ->addNoClear("nama_karyawan")
			  ->getUItable()
			  ->setHeader($header)
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$periksa ->getUItable()
		 ->setAction(true)
		 ->setFooterVisible(false)
         ->setPrintButtonEnable(false)
         ->setReloadButtonEnable(false)
         ->setAddButtonEnable(true)
         ->addModal("id_karyawan", "hidden", "", "")
		 ->addModal("nama_karyawan", "chooser-pasien-keluarga_karyawan-Pilih Karyawan", "Karyawan", "");
$periksa->getForm(true,"")
		->addElement("", $btn);
$select=new OptionBuilder();
$select->addSingle("Anak Kandung");
$select->addSingle("Ayah Kandung");
$select->addSingle("Ibu Kandung");
$select->addSingle("Adik Kandung");
$select->addSingle("Kakak Kandung");
$select->addSingle("Kakek");
$select->addSingle("Nenek");
$select->addSingle("Adik Ipar");
$select->addSingle("Kakak Ipar");
$select->addSingle("Anak Angkat");
$select->addSingle("Ayah Angkat");
$select->addSingle("Ibu Angkat");
$select->addSingle("Adik Angkat");
$select->addSingle("Kakak Angkat");

$periksa->getUItable()
        ->addModal("nama","chooser-pasien-pasien_pasien-Pilih Pasien","Pasien","")
        ->addModal("id","text","NRM","","y",null,true)
        ->addModal("alamat","text","Alamat","","y",null,true)
        ->addModal("ayah","text","Ayah","","y",null,true)
        ->addModal("ibu","text","Ibu","","y",null,true)
        ->addModal("hubungan","select","Hubungan",$select->getContent(),"y",null);
$periksa->getAdapter()
        ->setUseNumber(true,"No.","back.")
        ->add("Nama", "nama")
		->add("NRM", "id","only-digit8")
		->add("Alamat", "alamat")
        ->add("Hubungan", "hubungan")
		->add("Ayah", "ayah")
		->add("Ibu", "ibu");
$periksa->addViewData("id_karyawan", "id_karyawan")
		->addViewData("nama_karyawan", "nama_karyawan");
$periksa->addSaveData("id_karyawan", "id_karyawan","id-value")
		->addSaveData("nama_karyawan", "nama_karyawan","id-value")
        ->addJSColumn("id")
        ->addJSColumn("nama")
        ->addJSColumn("alamat")
        ->addJSColumn("ayah")
        ->addJSColumn("ibu")
        ->addJSColumn("hubungan");
$periksa->getSuperCommand()->addResponder("keluarga_karyawan", $pasien_karyawan);
$periksa->getSuperCommand()->addResponder("pasien_pasien", $pasien_pasien);
$periksa->addSuperCommand("keluarga_karyawan", array());
$periksa->addSuperCommandArray("keluarga_karyawan", "nama_karyawan", "nama");
$periksa->addSuperCommandArray("keluarga_karyawan", "id_karyawan", "id");
$periksa->addSuperCommandArray("keluarga_karyawan", "smis_action_js", "pasien.view();","smis_action_js");		 
$periksa->addSuperCommand("pasien_pasien", array());
$periksa->addSuperCommandArray("pasien_pasien", "id", "id");
$periksa->addSuperCommandArray("pasien_pasien", "nama", "nama");
$periksa->addSuperCommandArray("pasien_pasien", "alamat", "alamat");
$periksa->addSuperCommandArray("pasien_pasien", "ayah", "ayah");
$periksa->addSuperCommandArray("pasien_pasien", "ibu", "ibu");
$periksa->addSuperCommandArray("pasien_pasien", "hubungan", "hubungan");
$periksa->addResouce("js","hrd/resource/js/pasien.js");
$periksa->setModalTitle("Pasien");
$periksa->initialize();
?>