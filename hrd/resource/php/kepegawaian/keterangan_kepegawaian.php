<ul>
<li>Calon : adalah calon pegawai yang melamar di perusahaan, dimana data - data pelamar dapat disimpan disini agar bisa dibuka lagi jika dibutuhkan</li>
<li>Pegawai : adalah karyawan yang saat ini bekerja dan sebagai pegawai, baik tetap, honorer dan percobaan</li>
<li>Mantan : adalah karyawan yang merupakan mantan pegawai</li>
<li>Arsip : data arsip detail pegawai, meliputi arsip surat lamaran, sertifikat, ijasah dan lain sebagainya</li>
<li>Latihan : data ini sama dengan arsip tetapi dikhususkan untuk sertifikat yang memiliki masa berlaku</li>
<li>Marketing : fitur ini berguna jika terdapat pegawai yang dikhususkan pada bagian marketing</li>
<li>Pasien : ini adalah fitur untuk melakukan binding pegawai yang memiliki hubungan dengan pasien sehingga dapat diketahui pasien tertentu adalah keluarga dari pegawai tertentu</li>
</ul>