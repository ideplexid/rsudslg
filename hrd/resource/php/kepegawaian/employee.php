<?php

/**
 * digunakan untuk melakukan manajemen karyawan
 * meliputi tunjangan miliknya
 * gajinya,nama,alamat,surat keterangan
 * surat NIP dan lain sebagainya.
 * 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_job
 * 				  - smis_hrd_pendidikan
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

if(isset($_POST['super_command']) && $_POST['super_command']!=""){
    require_once "hrd/snippet/kode_akun.php";
    return;
}
 
require_once "smis-base/smis-include-duplicate.php";
$duplicate = getSettings($db,"hrd-employee-duplicate","0");

global $db; $_GRUP_GAJI	    = getSettings($db,"hrd-employee-grup-gaji","0")		    =="1";
$_NIDN			= getSettings($db,"hrd-employee-nidn","0")					=="1";
$_GAPOK			= getSettings($db,"hrd-employee-gaji-pokok","0")			=="1";
$_JASPEL		= getSettings($db,"hrd-employee-jasa-pelayanan","0")		=="1";
$_STRUKTURAL	= getSettings($db,"hrd-employee-tunjangan-struktural","0")	=="1";
$_FUNGSIONAL	= getSettings($db,"hrd-employee-tunjangan-fungsional","0")	=="1";
$_JABATAN		= getSettings($db,"hrd-employee-tunjangan-jabatan","0")	=="1";
$_PENDIDIKAN	= getSettings($db,"hrd-employee-tunjangan-pendidikan","0")	=="1";
$_GOLONGAN		= getSettings($db,"hrd-employee-tunjangan-golongan","0")	=="1";
$_PENGABDIAN	= getSettings($db,"hrd-employee-tunjangan-pengabdian","0")	=="1";
$_PASUTRI		= getSettings($db,"hrd-employee-tunjangan-pasutri","0")	=="1";
$_ANAK			= getSettings($db,"hrd-employee-tunjangan-anak","0")		=="1";
$_HADIR			= getSettings($db,"hrd-employee-tunjangan-kehadiran","0")	=="1";
$_MAKAN			= getSettings($db,"hrd-employee-uang-makan","0")			=="1";
$_PENSIUN		= getSettings($db,"hrd-employee-dana-pensiun","0")			=="1";
$_INSENTIF		= getSettings($db,"hrd-employee-insentif","0")				=="1";
$_BPJS_KERJA	= getSettings($db,"hrd-employee-bpjs-ketenagakerjaan","0")	=="1";
$_BPJS_SEHAT	= getSettings($db,"hrd-employee-bpjs-kesehatan","0")		=="1";
$_TRANSPORT		= getSettings($db,"hrd-employee-uang-transportasi","0")	=="1";
$_LAIN			= getSettings($db,"hrd-employee-lain","0")					=="1";
$_UPAH_OPERASI	= getSettings($db,"hrd-employee-upah-operasi","0")			=="1";
$_UPAH_PERUJUK  = getSettings($db,"hrd-employee-upah-perujuk","0")			=="1";
$_UPAH_DUDUK	= getSettings($db,"hrd-employee-upah-duduk","0")			=="1";
$_UPAH_PASIEN   = getSettings($db,"hrd-employee-upah-pasien","0")			=="1";
$_RUMUS_LEMBUR	= getSettings($db,"hrd-employee-rumus-lembur","0")			=="1";
$_UHADIR_PAGI	= getSettings($db,"hrd-employee-uang-hadir-pagi","0")		=="1";
$_UHADIR_SIANG	= getSettings($db,"hrd-employee-uang-hadir-siang","0")		=="1";
$_UHADIR_MALAM	= getSettings($db,"hrd-employee-uang-hadir-malam","0")		=="1";
 
$off        = new Button("","","Her Off");
$off        ->addClass("btn btn-danger")
            ->setIcon(" fa fa-user-times")
            ->setIsButton(Button::$ICONIC);
$back       = new Button("","","Her On");
$back       ->addClass("btn btn-inverse")
            ->setIcon(" fa fa-user-plus")
            ->setIsButton(Button::$ICONIC);
$header     = array ("No.","ID","Masuk","Akhir",'Nama',"Alamat",'Pendidikan','Keterangan',"Ruang Kerja");
$uitable    = new TableSynchronous ( $header,"",NULL,true );
$uitable    ->setName ( "employee" )
            ->setSynchronizeButton($duplicate!="0")
            ->setLoopDuplicateButtonEnable ($duplicate!="0")
            ->setSynchronizeButton(false)
            ->setDetailButtonEnable(true)
            ->setReloadButtonEnable(false)
            ->setPrintButtonEnable(false)
            ->setDelButtonEnable(false)
            ->addContentButton("her_out",$back)
            ->addContentButton("her_off",$off);

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    require_once "hrd/function/akhir_kontrak.php";
    $adapter = new SynchronousViewAdapter();
    $adapter ->setUseNumber(true,"No.","back.")
             ->add("Nama","nama")
             ->add("ID","id","only-digit8")
             ->add("NIP","nip")
             ->add("Alamat","alamat")
             ->add("Jabatan","struktural")
             ->add("Bagian","nama_jabatan")
             ->add("Keterangan","sisa_kontrak","function-akhir_kontrak")
             ->add("Pendidikan","pendidikan")
             ->add("Masuk","tanggal_masuk","date d M Y")
             ->add("Akhir","akhir_kontrak","date d M Y")
             ->add("Ruang Kerja","ruangan_pegawai");
	    
	$dbtable = new DBTable ( $db,"smis_hrd_employee" );
    $dbtable ->setPreferredView(true,"smis_vhrd_employee")
             ->setUseWhereforView(true)
             ->addCustomKriteria("keluar","=0");
	if($_POST['s_jk']!='-1')        $dbtable->addCustomKriteria("jk","='".$_POST['s_jk']."'");
	if($_POST['s_jabatan']!='')     $dbtable->addCustomKriteria("jabatan"," = '".$_POST['s_jabatan']."'");
	if($_POST['s_pendidikan']!='')  $dbtable->addCustomKriteria("pendidikan"," ='".$_POST['s_pendidikan']."'");
	if($_POST['s_tenaga']!='')      $dbtable->addCustomKriteria("tenaga"," ='".$_POST['s_tenaga']."'");
	if($_POST['s_pg']!='')          $dbtable->addCustomKriteria("organik"," ='".$_POST['s_pg']."'");
    if($_POST['s_kontrak']!='')     $dbtable->addCustomKriteria(NULL," DATEDIFF(akhir_kontrak,'".$_POST['s_kontrak']."') < '".$_POST['s_durasi']."'");

	$dbres  = new DuplicateResponder ( $dbtable,$uitable,$adapter );
    $dbres  ->setDuplicate($duplicate=="1","duplicate_hrd")
            ->setAutonomous(getSettings($db,"smis_autonomous_id",""))
            ->addNotifyData("table",$dbtable->getName());
	$data   = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

require_once "hrd/function/get_all_job.php";
require_once "hrd/function/get_all_rumus_lembur.php";
require_once "hrd/function/get_all_pendidikan.php";
require_once "hrd/function/get_all_status_tenaga.php";
require_once "hrd/function/get_all_golongan.php";
require_once "hrd/function/get_all_ruangan_pegawai.php";
$jabatan         = get_all_job($db);
$rumus           = get_all_rumus_lembur($db);
$pendidikan      = get_all_pendidikan($db);
$tenaga          = get_all_status_tenaga($db);
$golongan        = get_all_golongan($db);
$ruangan_pegawai = get_all_ruangan_pegawai($db);

$jk = new OptionBuilder();
$jk ->add("Laki-Laki","0","0")
	->add("Perempuan","1","0")
	->add("","-1","1");

$menikah = new OptionBuilder();
$menikah ->addSingle("Belum Menikah")
		 ->addSingle("Menikah")
		 ->addSingle("Duda")
		 ->addSingle("Janda");

$pg = new OptionBuilder();
$pg ->add("Pegawai Luar","0")
    ->add("Pegawai Dalam","1")
    ->add("","");

$uitable ->addModal("search_pendidikan","select","Pendidikan",$pendidikan)
         ->addModal("search_jabatan","select","Bagian",$jabatan)
         ->addModal("search_jk","select","Jenis Kelamin",$jk->getContent())
         ->addModal("search_tenaga","select","Tenaga",$tenaga)
         ->addModal("search_pg","select","Pegawai",$pg->getContent())
         ->addModal("search_kontrak","date","Habis Kontrak","")
         ->addModal("search_durasi","text","Durasi (Hari)",getSettings($db,"hrd-employee-max-kontrak-limit",60));
$form    = $uitable->getModal()->getForm();

$search  = new Button("","","");
$search  ->addClass("btn-primary")
	  	 ->setAction("employee.view()")
	  	 ->setIsButton(Button::$ICONIC)
	  	 ->setIcon("fa fa-refresh");
$form 	 ->addElement("",$search);

$print 	 = new Button("","","");
$print   ->addClass("btn-primary")
		 ->setAction("employee.print()")
		 ->setIsButton(Button::$ICONIC)
		 ->setIcon("fa fa-print");
$form	 ->addElement("",$print);
$uitable ->clearContent();

$strata = new OptionBuilder();
$strata ->addSingle("","1")
        ->addSingle("D-I")
        ->addSingle("D-II")
        ->addSingle("D-III")
        ->addSingle("D-IV")
        ->addSingle("S-I")
        ->addSingle("S-II")
        ->addSingle("S-III");

$agama  = new OptionBuilder();
$agama  ->addSingle("Islam","1")
        ->addSingle("Kristen")
        ->addSingle("Katholik")
        ->addSingle("Hindu")
        ->addSingle("Buddha");

$dbtable    = new DBTable($db,"smis_hrd_grup_gaji");
$dbtable    ->setShowAll(true);
$dlist      = $dbtable->view("",0);
$adapter    = new SelectAdapter("nama","slug");
$content    = $adapter->getContent($dlist['data']);
$content[]  = array("name"=>"","value"=>"","default"=>1);

$uitable ->addModal("id","hidden","","")
         ->addModal("tanggal_masuk","date","Tanggal Masuk",date("Y-m-d"))
         ->addModal("nama","text","Nama","")
         ->addModal("jk","select","Jenis Kelamin",$jk->getContent())
         ->addModal("alamat","text","Alamat","")
         ->addModal("tmp_lahir","text","Tempat Lahir","")
         ->addModal("tgl_lahir","date","Tanggal Lahir","")
         ->addModal("pendidikan","select","Pendidikan",$pendidikan)
         ->addModal("strata","select","Strata",$strata->getContent())
         ->addModal("tahun_lulus","text","Tahun Lulus","")
         ->addModal("tgl_validasi","date","Tanggal Validasi","")
         ->addModal("nama_kampus","text","Kampus","")
         ->addModal("akhir_kontrak","date","Akhir Kontrak","")
         ->addModal("prodi","text","Prodi","")
         ->addModal("nip","text","NIP","")
         ->addModal("no_ijin","text","SIP / SIPA","");

/*KODE UNTUK PILIHAN*/
if($_GRUP_GAJI)     $uitable->addModal("grup_gaji","select","Grup Gaji",$content);
if($_NIDN) 			$uitable->addModal("nidn","text","NIDN","");
if($_GAPOK) 		$uitable->addModal("gaji_pokok","money","Gaji Pokok","");
if($_JASPEL) 		$uitable->addModal("jaspel","money","Jasa Pelayanan","");
if($_STRUKTURAL)	$uitable->addModal("tunj_struktural","money","Tunjangan Struktural","");
if($_FUNGSIONAL) 	$uitable->addModal("tunj_fungsional","money","Tunjangan Fungsional","");
if($_GOLONGAN) 		$uitable->addModal("tunj_golongan","money","Tunjangan Golongan","");
if($_JABATAN) 		$uitable->addModal("tunj_jabatan","money","Tunjangan Jabatan","");
if($_PENDIDIKAN) 	$uitable->addModal("tunj_pendidikan","money","Tunjangan Pendidikan","");
if($_PENGABDIAN)	$uitable->addModal("tunjangan_pengabdian","money","Tunjangan Pengabdian","");
if($_PENSIUN) 		$uitable->addModal("pensiun","money","Dana Pensiun","");
if($_INSENTIF) 		$uitable->addModal("insentif","money","Insentif","");
if($_BPJS_KERJA) 	$uitable->addModal("bpjs_ketenagakerjaan","money","BPJS Ketenagakerjaan","");
if($_BPJS_SEHAT) 	$uitable->addModal("bpjs_kesehatan","money","BPJS Kesehatan","");
if($_PASUTRI) 		$uitable->addModal("tunj_pasutri","money","Tunjangan Suami/Istri","");
if($_ANAK) 			$uitable->addModal("tunj_anak","money","Tunjangan Anak","");
if($_HADIR) 		$uitable->addModal("tunj_kehadiran","money","Tunjangan Kehadiran","");
if($_MAKAN) 		$uitable->addModal("uang_makan","money","Uang Makan","");
if($_TRANSPORT) 	$uitable->addModal("uang_transport","money","Uang Transport","");
if($_LAIN) 			$uitable->addModal("lain_lain","money","Lain - Lain","");
if($_UPAH_OPERASI) 	$uitable->addModal("uang_operasi","money","Upah Operasi","");
if($_UPAH_PERUJUK) 	$uitable->addModal("uang_perujuk","money","Upah Perujuk","");
if($_UPAH_DUDUK) 	$uitable->addModal("uang_duduk","money","Upah Duduk","");
if($_UPAH_PASIEN) 	$uitable->addModal("uang_pasien","money","Upah Pasien","");
if($_RUMUS_LEMBUR) 	$uitable->addModal("rumus_lembur","select","Rumus Lembur",$rumus);
if($_UHADIR_PAGI) 	$uitable->addModal("uang_hadir_pagi","money","Uang Hadir Pagi","0");
if($_UHADIR_SIANG) 	$uitable->addModal("uang_hadir_siang","money","Uang Hadir Siang","0");
if($_UHADIR_MALAM) 	$uitable->addModal("uang_hadir_malam","money","Uang Hadir Malam","0");


/*END KODE UNTUK PILIHAN*/
$uitable ->addModal("dokter_spesialis","checkbox","Dokter Spesialis","")
         ->addModal("jabatan","select","Bagian",$jabatan)
         ->addModal("struktural","text","J. Struktural","")
         ->addModal("golongan","select","Golongan",$golongan)
         ->addModal("noktp","text","No KTP","")
         ->addModal("nokk","text","No KK","")
         ->addModal("menikah","select","Status Nikah",$menikah->getContent())
         ->addModal("suami_istri","text","Suami/Istri","")
         ->addModal("anak_lk","text","Jml Anak LK","")
         ->addModal("anak_pr","text","Jml Anak PR","")
         ->addModal("anak_satu","text","Nama Anak I","")
         ->addModal("anak_dua","text","Nama Anak II","")
         ->addModal("ayah","text","Ayah","")
         ->addModal("ibu","text","Ibu","")
         ->addModal("organik","checkbox","Pegawai Dalam","")
         ->addModal("tenaga","select","Status Tenaga Kerja",$tenaga)
         ->addModal("pelatihan","summernote","Pelatihan","")
         ->addModal("keterangan","textarea","Keterangan","")
         ->addModal("ruangan_pegawai","select","Ruangan Pegawai",$ruangan_pegawai)
         ->addModal("ruangan","textarea","Ruangan Otorisasi","")
         ->addModal("telp","text","Telp","")
         ->addModal("hp","text","HP","")
         ->addModal("no_bpjs","text","No. BPJS TK","")
         ->addModal("no_bpjsk","text","No. BPJS K","")
         ->addModal("agama","select","Agama",$agama->getContent())
         ->addModal("ijasah","checkbox","Ijasah","")
         ->addModal("pengalaman","checkbox","Pengalaman","")
         ->addModal("t_kerja","textarea","Tempat Kerja Sebelumnya","")
         ->addModal("nama_bank","text","Bank","")
         ->addModal("cabang_bank","text","Cabang","")
         ->addModal("no_rek","text","No. Rekening","")
         ->addModal("unit_kerja","text","Unit Kerja","")
         ->addModal("foto","file-single-image","Foto","")
         ->addModal("debet","chooser-employee-employee_debet-Kode Akun Debet","Kode Debet","")
         ->addModal("kredit","chooser-employee-employee_kredit-Kode Akun Kredit","Kode Kredit","")
         ->addModal("ttd","draw-file-","Tanda Tangan","");

$modal = $uitable->getModal ()->setComponentSize(Modal::$MEDIUM);
$modal ->setTitle ( "Karyawan" );

echo $form      ->getHtml ();
echo $uitable   ->getHtml ();
echo $modal     ->getHtml ();
echo addJS  ("base-js/smis-base-loading.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("hrd/resource/js/employee.js",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");
?>