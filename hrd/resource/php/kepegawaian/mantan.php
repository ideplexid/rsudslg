<?php

/**
 * digunakan untuk melakukan manajemen karyawan
 * yang sudah keluar dari system (alias mantan pegawai)
 * meliputi tunjangan miliknya
 * gajinya,nama,alamat,surat keterangan
 * surat NIP dan lain sebagainya.
 * 
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_job
 * 				  - smis_hrd_pendidikan
 * @since		: 27 Sept 2016
 * @version		: 1.1.0
 * 
 * */

global $db;
$header  = array ("No.","Masuk",'Nama',"L/P","Alamat",'NIP','Jabatan',"Bagian",'Pendidikan','Keterangan');
$uitable = new Table($header,"",NULL,true);
$btn 	 = new Button("","","Salary");
$btn	 ->setIsButton(Button::$ICONIC)
		 ->setIcon("fa fa-money");
$off 	 = new Button("","","");
$off 	 ->addClass("btn btn-inverse")
		 ->setIcon(" fa fa-user")
		 ->setIsButton(Button::$ICONIC);
$uitable ->setName("mantan")
		 ->setReloadButtonEnable(false)
		 ->setPrintButtonEnable(false)
		 ->setDelButtonEnable(false)
		 ->addContentButton("her_on",$off);

/* this is respond when system have to response */
if (isset($_POST['command'])) {
	
	$adapter = new SimpleAdapter();
	$adapter ->setUseNumber(true,"No.","back.")
			 ->add("Nama","nama")
			 ->add("L/P","jk","trivial_0_Laki-Laki_Perempuan")
			 ->add("NIP","nip")
			 ->add("Alamat","alamat")
			 ->add("Jabatan","struktural")
			 ->add("Bagian","nama_jabatan")
			 ->add("Keterangan","keterangan")
			 ->add("Pendidikan","pendidikan")
			 ->add("Masuk","tanggal_masuk","date d M Y")
			 ->add("NIDN","nidn")
			 ->add("Dana Pensiun","dana_pensiun","money Rp.")
			 ->add("Insentif","insentif","money Rp.")
			 ->add("Lain - Lain","lain_lain","money Rp.")
			 ->add("Uang Makan","uang_makan","money Rp.")
			 ->add("Uang Transport","uang_transport","money Rp.");
	
	$dbtable = new DBTable($db,"smis_hrd_employee");
	$dbtable ->setPreferredView(true,"smis_vhrd_employee")
			 ->setUseWhereforView(true)
			 ->addCustomKriteria("keluar","=1");
	if($_POST['s_jk']!='-1')  		$dbtable->addCustomKriteria("jk","='".$_POST['s_jk']."'");
	if($_POST['s_jabatan']!='') 	$dbtable->addCustomKriteria("jabatan"," = '".$_POST['s_jabatan']."'");
	if($_POST['s_pendidikan']!='') 	$dbtable->addCustomKriteria("pendidikan"," ='".$_POST['s_pendidikan']."'");
	if($_POST['s_tenaga']!='') 		$dbtable->addCustomKriteria("tenaga"," ='".$_POST['s_tenaga']."'");
	if($_POST['s_pg']!='') 			$dbtable->addCustomKriteria("organik"," ='".$_POST['s_pg']."'");
	$dbres 	= new DBResponder($dbtable,$uitable,$adapter);
	$data 	= $dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$dbtable 	= new DBTable($db,"smis_hrd_job");
$dbtable 	->setShowAll(true)
		 	->setOrder("nama ASC");
$data 	 	= $dbtable->view("",0);
$job 	 	= new SelectAdapter("nama","id");
$jabatan   	= $job->getContent($data['data']);
$jabatan[]  = array("name"=>"","value"=>"");


$dbtable 		= new DBTable($db,"smis_hrd_pendidikan");
$dbtable		->setShowAll(true)
				->setOrder("pendidikan ASC");
$data 			= $dbtable->view("",0);
$pend 			= new SelectAdapter("pendidikan","pendidikan");
$pendidikan 	= $pend->getContent($data['data']);
$pendidikan[]	= array("name"=>"","value"=>"");

$jk	= new OptionBuilder();
$jk ->add("Laki-Laki","0","0")
	->add("Perempuan","1","0")
	->add("","-1","1");

$menikah = new OptionBuilder();
$menikah ->addSingle("Belum Menikah")
		 ->addSingle("Menikah")
		 ->addSingle("Duda")
		 ->addSingle("Janda");

$tenaga = new OptionBuilder();
$tenaga ->addSingle("Tenaga Kontrak")
		->addSingle("Tenaga Tetap")
		->addSingle("Tenaga Percobaan")
		->addSingle("Tenaga Honorer")
		->addSingle("");

$pg	= new OptionBuilder();
$pg ->add("Pegawai Luar","0")
	->add("Pegawai Dalam","1")
	->add("","");

$uitable ->addModal("search_pendidikan","select","Pendidikan",$pendidikan)
		 ->addModal("search_jabatan","select","Bagian",$jabatan)
		 ->addModal("search_jk","select","Jenis Kelamin",$jk->getContent())
		 ->addModal("search_tenaga","select","Tenaga",$tenaga->getContent())
		 ->addModal("search_pg","select","Pegawai",$pg->getContent());
$form	 = $uitable->getModal()->getForm();

$search  = new Button("","","");
$search  ->addClass("btn-primary")
	  	 ->setAction("employee.view()")
	  	 ->setIsButton(Button::$ICONIC)
	  	 ->setIcon("fa fa-refresh");
$form 	 ->addElement("",$search);

$print 	 = new Button("","","");
$print   ->addClass("btn-primary")
		 ->setAction("employee.print()")
		 ->setIsButton(Button::$ICONIC)
		 ->setIcon("fa fa-print");
$form	 ->addElement("",$print);

$uitable ->clearContent()
		 ->addModal("id","hidden","","")
		 ->addModal("tanggal_masuk","date","Tanggal Masuk",date("Y-m-d"))
		 ->addModal("nama","text","Nama","")
		 ->addModal("jk","select","Jenis Kelamin",$jk->getContent())
		 ->addModal("alamat","text","Alamat","")
		 ->addModal("tmp_lahir","text","Tempat Lahir","")
		 ->addModal("tgl_lahir","date","Tanggal Lahir","")
		 ->addModal("pendidikan","select","Pendidikan",$pendidikan)
		 ->addModal("nip","text","NIP","")
		 ->addModal("no_ijin","text","SIP / SIPA","")
		 ->addModal("jabatan","select","Bagian",$jabatan)
		 ->addModal("struktural","text","J. Struktural","")
		 ->addModal("noktp","text","No KTP","")
		 ->addModal("nokk","text","No KK","")
		 ->addModal("menikah","select","Status Nikah",$menikah->getContent())
		 ->addModal("suami_istri","text","Suami/Istri","")
		 ->addModal("anak_lk","text","Jml Anak LK","")
		 ->addModal("anak_pr","text","Jml Anak PR","")
		 ->addModal("anak_satu","text","Nama Anak I","")
		 ->addModal("anak_dua","text","Nama Anak II","")
		 ->addModal("ayah","text","Ayah","")
		 ->addModal("ibu","text","Ibu","")
		 ->addModal("organik","checkbox","Pegawai Dalam","")
		 ->addModal("tenaga","select","Tenaga",$tenaga->getContent())
		 ->addModal("pelatihan","summernote","Pelatihan","")
		 ->addModal("keterangan","textarea","Keterangan","")
		 ->addModal("ruangan","textarea","Ruangan","*");

$modal 	= $uitable->getModal ()->setComponentSize(Modal::$MEDIUM);
$modal	->setTitle("Karyawan");

echo $form		->getHtml ();
echo $uitable	->getHtml ();
echo $modal		->getHtml ();
echo addJS	("framework/smis/js/table_action.js");
echo addJS	("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS	("hrd/resource/js/mantan.js",false);
echo addCSS	("framework/bootstrap/css/datepicker.css");
?>