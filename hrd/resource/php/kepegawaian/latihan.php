<?php

/**
 * digunakan untuk melakukan manajemen 
 * data pelatihan apa saja yang sudah dimiliki oleh karyawan 
 * tertentu. sehingga dapat dipetakan karyawan 
 * berdasarkan kemampuanya, dan sertifikat yang mungkin pernah
 * ia miliki
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_berkas
 * 				  - smis_hrd_employee
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';

$header=array ("Nama","Alamat",'L/P','Pendidikan','Struktural' );
$dbtable= new DBTable($db, "smis_hrd_employee");
$dktable = new Table ( $header );
$dktable->setName ( "latihan_employee" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Alamat", "alamat" );
$dkadapter->add ( "L/P", "jk","trivial_0_L_P" );
$dkadapter->add ( "Pendidikan", "pendidikan");
$dkadapter->add ( "Struktural", "struktural" );
$latihan_employee = new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ('No.',"Nama","Keterangan",'Tanggal','Berakhir',"Berkas");
$pemreg=new MasterSlaveTemplate($db, "smis_hrd_berkas", "hrd", "latihan");
$uitable=$pemreg->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setName("latihan");
$pemreg->setUITable($uitable);
if($pemreg->getDBResponder()->isView() && $_POST['super_command']==""){
	$pemreg	->getDBtable()
			->addCustomKriteria("id_pegawai", "='".$_POST['id_pegawai']."'")
			->addCustomKriteria("jenis_berkas", "='sertifikat'");
}

if($pemreg->getDBResponder()->isSave()){
	$pemreg->getDBResponder()->addColumnFixValue("jenis_berkas", "sertifikat");
}
$pemreg->addFlag("id_pegawai", "Pilih Pegawai Dahulu", "Silakan Pilih Pegawai Dahulu")
		 ->addNoClear("id_pegawai")
		 ->addNoClear("nama_pegawai")
		 ->addNoClear("pendidikan")
		 ->addNoClear("struktural")
		 ->setDateEnable(true)
		 ->getUItable()
		 ->addModal("id_pegawai", "hidden", "", "")
		 ->addModal("nama_pegawai", "chooser-latihan-latihan_employee", "Nama", "")
		 ->addModal("pendidikan", "text", "Pendidikan", "","n",null,true)
		 ->addModal("struktural", "text", "Struktural", "","n",null,true);
$pemreg->getForm();
$pemreg->getUItable()
		 ->setHeader($header)
		 ->addModal ( "id", "hidden", "", "")
		 ->addModal ( "nama", "text", "Nama", "")
		 ->addModal ( "file", "file-single-all", "Berkas", "")
		 ->addModal ( "keterangan", "textarea", "Keterangan", "")
		 ->addModal ( "tanggal_dapat", "date", "Tanggal", date("Y-m-d"))
		 ->addModal ( "tanggal_kedaluarsa", "date", "Berakhir", "");
$adapter=$pemreg->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Nama", "nama")
		->add("Keterangan", "keterangan")
		->add("Berkas", "file","download")
		->add("Tanggal", "tanggal_dapat","date d M Y")
		->add("Berakhir", "tanggal_kedaluarsa","date d M Y");
$pemreg->getSuperCommand()
		 ->addResponder("latihan_employee", $latihan_employee);
$pemreg->addSuperCommand("latihan_employee", array())
		 ->addSuperCommandArray("latihan_employee", "id_pegawai", "id")
		 ->addSuperCommandArray("latihan_employee", "nama_pegawai", "nama")
		 ->addSuperCommandArray("latihan_employee", "struktural", "struktural")
		 ->addSuperCommandArray("latihan_employee", "pendidikan", "pendidikan")		 	
		 ->addSuperCommandArray("latihan_employee", "smis_action_js", "latihan.view();","smis_action_js")
		 ->addRegulerData("id_pegawai", "id_pegawai","id-value")
		 ->getModal()
		 ->setTitle("Pelatihan Karyawan");
$pemreg->initialize();
?>

