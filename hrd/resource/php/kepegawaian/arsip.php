<?php
/**
 * digunakan untuk melakukan manajemen 
 * arsip - arsip dari pegawai
 * arsip - arsip dapat berupa, surat lamaran
 * data ijasah, surat kontrak dan lain-lain
 * yang di scan dan dikumpulkan disini
 * sehingga ketika suatu saat nanti butuh 
 * data-data tersebut dapat ditampilkan dengan cepat
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_berkas
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';

$header=array ("Nama","Alamat",'L/P','Pendidikan','Struktural' );
$dbtable= new DBTable($db, "smis_hrd_employee");
$dktable = new Table ( $header );
$dktable->setName ( "arsip_employee" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Alamat", "alamat" );
$dkadapter->add ( "L/P", "jk","trivial_0_L_P" );
$dkadapter->add ( "Pendidikan", "pendidikan");
$dkadapter->add ( "Struktural", "struktural" );
$arsip_employee = new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ('No.',"Nama","Keterangan","Tanggal","Berkas");
$pemreg=new MasterSlaveTemplate($db, "smis_hrd_berkas", "hrd", "arsip");
$uitable=$pemreg->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setName("arsip");
$pemreg->setUITable($uitable);
if($pemreg->getDBResponder()->isView() && $_POST['super_command']==""){
	$pemreg	->getDBtable()
			->addCustomKriteria("id_pegawai", "='".$_POST['id_pegawai']."'")
			->addCustomKriteria("jenis_berkas", "='arsip'");
}

if($pemreg->getDBResponder()->isSave()){
	$pemreg->getDBResponder()->addColumnFixValue("jenis_berkas", "arsip");
}
$pemreg->addFlag("id_pegawai", "Pilih Pegawai Dahulu", "Silakan Pilih Pegawai Dahulu")
		 ->addNoClear("id_pegawai")
		 ->addNoClear("nama_pegawai")
		 ->addNoClear("pendidikan")
		 ->addNoClear("struktural")
		 ->setDateEnable(true)
		 ->getUItable()
		 ->addModal("id_pegawai", "hidden", "", "")
		 ->addModal("nama_pegawai", "chooser-arsip-arsip_employee-Arsip Pegawai", "Nama", "")
		 ->addModal("pendidikan", "text", "Pendidikan", "","n",null,true)
		 ->addModal("struktural", "text", "Struktural", "","n",null,true);
$pemreg->getForm();
$pemreg->getUItable()
		 ->setHeader($header)
		 ->addModal ( "id", "hidden", "", "")
		 ->addModal ( "nama", "text", "Nama", "")
		 ->addModal ( "file", "file-single-all", "Berkas", "")
		 ->addModal ( "keterangan", "textarea", "Keterangan", "")
		 ->addModal ( "tanggal_dapat", "date", "Tanggal", date("Y-m-d"));
$adapter=$pemreg->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
//"Nama","Keterangan","File",'Tanggal','Berakhir'
$adapter->add("Nama", "nama")
		->add("Keterangan", "keterangan")
		->add("Berkas", "file","download")
		->add("Tanggal", "tanggal_dapat","date d M Y");
$pemreg->getSuperCommand()
		 ->addResponder("arsip_employee", $arsip_employee);
$pemreg->addSuperCommand("arsip_employee", array())
		 ->addSuperCommandArray("arsip_employee", "id_pegawai", "id")
		 ->addSuperCommandArray("arsip_employee", "nama_pegawai", "nama")
		 ->addSuperCommandArray("arsip_employee", "struktural", "struktural")
		 ->addSuperCommandArray("arsip_employee", "pendidikan", "pendidikan")		 	
		 ->addSuperCommandArray("arsip_employee", "smis_action_js", "arsip.view();","smis_action_js")
		 ->addRegulerData("id_pegawai", "id_pegawai","id-value")
		 ->getModal()
		 ->setTitle("Pelatihan Karyawan");
$pemreg->initialize();
?>

