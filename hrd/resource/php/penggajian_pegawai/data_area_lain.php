<?php

/**
 * digunakan untuk manajemen data pasien 
 * yang memiliki kepesertaan dengan asuransi
 * tertentu, yang mana nomor-nomor asuransi ini 
 * akan disimpan untuk kemudian hari dipakai
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_area_lain
 * @since		: 16 Mei 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-class/MasterSlaveTemplate.php';
require_once "hrd/class/responder/DataAreaLainDBResponder.php";
$btn=new Button("","","");
$btn->setClass(" btn-inverse");
$btn->setAction("data_area_lain.view()");
$btn->setIcon(" icon-white icon-search");
$btn->setIsButton(Button::$ICONIC);

$fix=new Button("","","Auto Fix");
$fix->setClass(" btn-primary");
$fix->setAction("data_area_lain.fixing()");
$fix->setIcon(" fa fa-flash");
$fix->setIsButton(Button::$ICONIC_TEXT);

$upload=new Button("","","Import");
$upload->setClass(" btn-primary");
$upload->setAction("data_area_lain.import_current()");
$upload->setIcon(" fa fa-cloud-download");
$upload->setIsButton(Button::$ICONIC_TEXT);

$lock=new Button("","","Penguncian");
$lock->setClass(" btn-primary");
$lock->setAction("data_area_lain.lock_current()");
$lock->setIcon(" fa fa-lock");
$lock->setIsButton(Button::$ICONIC_TEXT);

$header=array ('No.',"Nama","NRM","Tanggal","KMU","Kode","Operator","Perujuk","Asisten","Omloop","Bagi Dokter","Bagi Omloop","Bagi Asisten","Importer","Carabayar","Waktu","Info");
$pemreg=new MasterSlaveTemplate($db, "smis_hrd_area_lain", "hrd", "data_area_lain");

$uitable=new Table($header,"");
$pemreg->setDateEnable(true);
$pemreg->setDateTimeEnable(true);
$uitable->setReloadButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setAction(true);
$uitable->setName("data_area_lain");

$hitory=new Button("","","");
$hitory->setIcon(" fa fa-upload");
$hitory->setClass(" btn-info");
$hitory->setIsButton(Button::$ICONIC);
$uitable->addContentButton("detail_history",$hitory);

$pemreg->setUITable($uitable);



$responder=new DataAreaLainDBResponder($pemreg->getDBtable(),$pemreg->getUItable(),$pemreg->getAdapter());
$pemreg->setUITable($uitable);
$pemreg->setDBresponder($responder);
if( $pemreg ->getDBResponder() ->isView()){
	$pemreg	->getDBtable()
            ->setOrder(" DATE(tanggal) ASC, kmu ASC ",true)
			->addCustomKriteria("kmu", " LIKE '%".$_POST['kmu_filter']."%'");
    if($_POST['dari_filter']!="" && $_POST['sampai_filter']!=""){
        $pemreg	->getDBtable()
                ->addCustomKriteria(null, " tanggal >= '".$_POST['dari_filter']."'")
                ->addCustomKriteria(null, " tanggal < '".$_POST['sampai_filter']."'");    
    } 
    
    if($_POST['carabayar_filter']!=""){
        $pemreg	->getDBtable()
                ->addCustomKriteria(null, " carabayar = '".$_POST['carabayar_filter']."'");
    }
    
    if($_POST['operator_filter']!=""){
        $pemreg	->getDBtable()
                ->addCustomKriteria(null, " operator LIKE '%".$_POST['operator_filter']."%'");
    }
    
    if($_POST['perujuk_filter']!=""){
        $pemreg	->getDBtable()
                ->addCustomKriteria(null, " perujuk LIKE '%".$_POST['perujuk_filter']."%'");
    }
    
    if($_POST['uang_filter']!="" && $_POST['uang_filter']!="0"){
        $pemreg	->getDBtable()
                ->addCustomKriteria(null, " bagi_dokter = '".$_POST['uang_filter']."'");
    }
}


$serv=new ServiceConsumer($db,"get_jenis_patient");
$serv->execute();
$carabayar=$serv->getContent();


if($pemreg->getDBResponder()->isPreload()){
    $jenis_pasien=$carabayar;
    $jenis_pasien[]=array("name"=>"","value"=>"","default"=>"1");
    $pemreg  ->addNoClear("kmu_filter")
             ->getUItable()
             ->addModal("kmu_filter", "text", "KMU", "","y")
             ->addModal("dari_filter", "date", "Dari", "","y")
             ->addModal("sampai_filter", "date", "Sampai", "","y")
             ->addModal("carabayar_filter", "select", "Carabayar", $jenis_pasien,"y")
             ->addModal("operator_filter", "chooser-data_area_lain-operator_filter-Operator", "Operator", "","y")
             ->addModal("perujuk_filter", "chooser-data_area_lain-perujuk_filter-Perujuk", "Perujuk", "","y")
             ->addModal("uang_filter", "money", "Bagi Dokter", "","y");
    $form=$pemreg  ->getForm();
    $form->addElement("",$btn);
    $form->addElement("",$fix);
    $form->addElement("",$upload);
    $form->addElement("",$lock);
    $pemreg  ->getUItable()->clearContent();

}

$pemreg  ->getUItable()
         ->addModal("id","hidden","","")
         ->addModal("nama","text","Nama","")
         ->addModal("nrm","text","NRM","")
         ->addModal("tanggal","datetime","Tanggal","")
         ->addModal("kmu","text","KMU","")
         ->addModal("id_operator","hidden","","")
         ->addModal("id_perujuk","hidden","","")
         ->addModal("id_oomloop","hidden","","")
         ->addModal("id_asisten","hidden","","")
         ->addModal("operator","chooser-data_area_lain-operator-Operator","Operator","")
		 ->addModal("perujuk","chooser-data_area_lain-perujuk-Perujuk","Perujuk","")
         ->addModal("asisten","chooser-data_area_lain-asisten-Asisten Operator","Asisten Operator","")
         ->addModal("oomloop","chooser-data_area_lain-oomloop-Omloop","Omloop","")
         ->addModal("bagi_dokter","money","Bagi Dokter","")
         ->addModal("bagi_asisten","money","Bagi Asisten","")
         ->addModal("bagi_oomloop","money","Bagi Omloop","")
         ->addModal("carabayar","select","Cara Bayar",$carabayar)
         ->setHeader($header);
$pemreg->setModalTitle("Data Operasi");
$pemreg->setModalComponentSize(Modal::$MEDIUM);

require_once "hrd/class/adapter/DataAreaLainAdapter.php";
$pemreg->setAdapter(new DataAreaLainAdapter());
$pemreg->addViewData("kmu_filter","kmu_filter");
$pemreg->addViewData("dari_filter","dari_filter");
$pemreg->addViewData("sampai_filter","sampai_filter");
$pemreg->addViewData("carabayar_filter","carabayar_filter");
$pemreg->addViewData("operator_filter","operator_filter");
$pemreg->addViewData("perujuk_filter","perujuk_filter");
$pemreg->addViewData("uang_filter","uang_filter");
$pemreg->setAutoReload(true);

//supercommand_blok
if(isset($_POST['super_command'])&& $_POST['super_command']!=""){
    $super_command=$_POST['super_command'];
    
    $header=array ("Nama","Alamat",'L/P','Pendidikan','Struktural' );
    $dbtable= new DBTable($db, "smis_hrd_employee");
    $dktable = new Table ( $header );
    $dktable->setName ( $super_command);
    $dktable->setModel ( Table::$SELECT );
    $dkadapter = new SimpleAdapter ();
    $dkadapter->add ( "Nama", "nama" );
    $dkadapter->add ( "Alamat", "alamat" );
    $dkadapter->add ( "L/P", "jk","trivial_0_L_P" );
    $dkadapter->add ( "Pendidikan", "pendidikan");
    $dkadapter->add ( "Struktural", "struktural" );
    $arsip_employee = new DBResponder($dbtable, $dktable, $dkadapter);
    $pemreg  ->getSuperCommand()
             ->addResponder($super_command, $arsip_employee);
}


if($pemreg->getDBResponder()->isPreload()){
    $pemreg  ->addSuperCommand("operator", array())
             ->addSuperCommandArray("operator", "id_operator", "id")
             ->addSuperCommandArray("operator", "operator", "nama");
    $pemreg  ->addSuperCommand("perujuk", array())
             ->addSuperCommandArray("perujuk", "id_perujuk", "id")
             ->addSuperCommandArray("perujuk", "perujuk", "nama");
    $pemreg  ->addSuperCommand("operator_filter", array())
             ->addSuperCommandArray("operator_filter", "operator_filter", "nama");
    $pemreg  ->addSuperCommand("perujuk_filter", array())
             ->addSuperCommandArray("perujuk_filter", "perujuk_filter", "nama");        
    $pemreg  ->addSuperCommand("asisten", array())
             ->addSuperCommandArray("asisten", "id_asisten", "id")
             ->addSuperCommandArray("asisten", "asisten", "nama");
    $pemreg  ->addSuperCommand("oomloop", array())
             ->addSuperCommandArray("oomloop", "id_oomloop", "id")
             ->addSuperCommandArray("oomloop", "oomloop", "nama");
}
$pemreg->addResouce("js","smis-base-js/smis-base-loading.js");
$pemreg->addResouce("js","hrd/resource/js/data_area_lain.js");
$pemreg->initialize();
?>

