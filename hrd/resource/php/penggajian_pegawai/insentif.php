<?php

/**
 * digunakan untuk melakukan manajemen 
 * data insentif karyawan. setiap kali karyawan insentif
 * dapat diketahui dan ditulis dengan menu ini.
 * yang mana nantinya setiap gaji insentif karyawan 
 * akan di simpan disini dan ditampilkan sebagai tambahan 
 * gaji di akhir bulan.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_insentif
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';

$header=array ("Nama","Alamat",'L/P','Pendidikan','Struktural' );
$dbtable= new DBTable($db, "smis_hrd_employee");
$dktable = new Table ( $header );
$dktable->setName ( "insentif_employee" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Alamat", "alamat" );
$dkadapter->add ( "L/P", "jk","trivial_0_L_P" );
$dkadapter->add ( "Pendidikan", "pendidikan");
$dkadapter->add ( "Struktural", "struktural" );
$insentif_employee = new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ('No.',"Tanggal","Keterangan","Nilai");
$pemreg=new MasterSlaveTemplate($db, "smis_hrd_insentif", "hrd", "insentif");
$pemreg->setDateEnable(true);
$uitable=$pemreg->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setName("insentif");
$pemreg->getDBtable()->setOrder(" tanggal DESC ");
$pemreg->setUITable($uitable);
if($pemreg->getDBResponder()->isView() && $_POST['super_command']==""){
	$pemreg	->getDBtable()
			->addCustomKriteria("id_pegawai", "='".$_POST['id_pegawai']."'");
}

$pemreg->addFlag("id_pegawai", "Pilih Pegawai Dahulu", "Silakan Pilih Pegawai Dahulu")
		 ->addNoClear("id_pegawai")
		 ->addNoClear("nama_pegawai")
		 ->addNoClear("pendidikan")
		 ->addNoClear("struktural")
		 ->setDateTimeEnable(true)
		 ->getUItable()
		 ->addModal("id_pegawai", "hidden", "", "")
		 ->addModal("nama_pegawai", "chooser-insentif-insentif_employee", "Nama", "")
		 ->addModal("pendidikan", "text", "Pendidikan", "","n",null,true)
		 ->addModal("struktural", "text", "Struktural", "","n",null,true);
         
$pemreg ->getForm();
$pemreg ->getUItable()
		 ->setHeader($header)
		 ->addModal ( "id", "hidden", "", "")
		 ->addModal ( "tanggal", "date", "Tanggal", date("Y-m-d"))
		 ->addModal ( "keterangan", "textarea", "Keterangan", "")
         ->addModal ( "nilai", "money", "Nilai Insntif", "0");

$adapter=$pemreg->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Tanggal", "tanggal","date d M Y")
		->add("Keterangan", "keterangan")
		->add("Nilai", "nilai","money Rp.");
$pemreg ->getSuperCommand()
        ->addResponder("insentif_employee", $insentif_employee);
$pemreg ->addSuperCommand("insentif_employee", array())
		 ->addSuperCommandArray("insentif_employee", "id_pegawai", "id")
		 ->addSuperCommandArray("insentif_employee", "nama_pegawai", "nama")
		 ->addSuperCommandArray("insentif_employee", "struktural", "struktural")
		 ->addSuperCommandArray("insentif_employee", "pendidikan", "pendidikan")
         ->addSuperCommandArray("insentif_employee", "rumus_insentif", "rumus_insentif")		 	
		 ->addSuperCommandArray("insentif_employee", "smis_action_js", "insentif.view();","smis_action_js")
		 ->addRegulerData("id_pegawai", "id_pegawai","id-value")
		 ->getModal()
		 ->setTitle("insentif Karyawan");
$pemreg->addResouce("js","hrd/resource/js/insentif.js");
$pemreg->initialize();
?>

