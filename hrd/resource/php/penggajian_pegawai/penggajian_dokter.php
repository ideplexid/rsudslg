<?php

/**
 * digunakan untuk penggajian_dokter khusus Sukmawijaya
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_grup_gaji
 * @since		: 10 Sep 2017
 * @version		: 1.2.0
 * 
 * */


global $db;
if (isset ( $_POST ['command'] )) {
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_pendapatan_dokter");
    $serv->addData("dari_bpjs",$_POST['dari_bpjs']);
    $serv->addData("dari",$_POST['dari']);
    $serv->addData("sampai_bpjs",$_POST['sampai_bpjs']);
    $serv->addData("sampai",$_POST['sampai']);
    $serv->execute();
    $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $cont=$serv->getContent();
    
    $result=array();
    foreach($cont as $x){
        foreach($x as $id_dokter=>$content){
            if(!isset($result[$id_dokter])){
                $result[$id_dokter]=array();
            }
            foreach($content as $name=>$value){
                if(!isset($result[$id_dokter][$name])){
                    $result[$id_dokter][$name]=0;
                }
                $result[$id_dokter][$name]+=$value;
            }
        }
    }
    
    $dbtable=new DBTable($db,"smis_hrd_grup_gaji");
    $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
    $grup_gaji=$dbtable->select(array("slug"=>$_POST['grup_gaji']));
    
    $dbtable=new DBTable($db,"smis_hrd_employee");
    $dbtable->addCustomKriteria(" grup_gaji "," = '".$_POST['grup_gaji']."'");
    $dbtable->setShowAll(true);
    $data=$dbtable->view("",0);
    $list=$data['data'];
    $total_data =count($list);
    
    require_once("smis-libs-out/php-excel/PHPExcel.php");
	require_once("smis-libs-out/php-excel/PHPExcel/IOFactory.php");
    loadLibrary("smis-libs-function-time");
    $reader = PHPExcel_IOFactory::createReader('Excel2007');
    $excel = $reader->load("hrd/resource/excel/penggajian_dokter.xlsx");
	$sheet = $excel->getSheet(0);
    
    $sheet->setCellValue("D2",ArrayAdapter::format("date d M Y",$_POST['dari']));
    $sheet->setCellValue("F2",ArrayAdapter::format("date d M Y",$_POST['sampai']));
    $sheet->setCellValue("I2",ArrayAdapter::format("date d M Y",$_POST['dari_bpjs']));
    $sheet->setCellValue("K2",ArrayAdapter::format("date d M Y",$_POST['sampai_bpjs']));
    
    $persen_dokter_umum=getSettings($db,"smis-hrd-lab-dokter-umum","20")."%";
    $persen_dokter_spesialis=getSettings($db,"smis-hrd-lab-dokter-spesialis","25")."%";
    
    
    $start=4;
    $sheet->insertNewRowBefore($start+1, $total_data-1);
    foreach($list as $x){
        $sheet->setCellValue("A".$start,$x->id);
        $sheet->setCellValue("B".$start,$x->nama);
        $sheet->setCellValue("C".$start,$x->dokter_spesialis=="1"?$persen_dokter_spesialis:$persen_dokter_umum);  
        if(isset($result[$x->id])){
            $sheet->setCellValue("D".$start,$result[$x->id]['umum_visite']);
            $sheet->setCellValue("E".$start,$result[$x->id]['umum_konsul']);
            $sheet->setCellValue("F".$start,$result[$x->id]['umum_ok']);
            $sheet->setCellValue("G".$start,$result[$x->id]['umum_periksa']);
            $sheet->setCellValue("H".$start,"=".$result[$x->id]['umum_lab']."*C".$start);
            $sheet->setCellValue("I".$start,$result[$x->id]['bpjs_visite']);
            $sheet->setCellValue("J".$start,$result[$x->id]['bpjs_konsul']);
            $sheet->setCellValue("K".$start,$result[$x->id]['bpjs_ok']);
            $sheet->setCellValue("L".$start,$result[$x->id]['bpjs_periksa']);
            $sheet->setCellValue("M".$start,"=".$result[$x->id]['bpjs_lab']."*C".$start);
        }
        $sheet->setCellValue("N".$start,"=sum(D".$start.":M".$start.")"); 
        $start++;
    }
    $sheet->setCellValue("D".$start,"=SUM(D4:D".($start-1).")");
    $sheet->setCellValue("E".$start,"=SUM(E4:E".($start-1).")");
    $sheet->setCellValue("F".$start,"=SUM(F4:F".($start-1).")");
    $sheet->setCellValue("G".$start,"=SUM(G4:G".($start-1).")");
    $sheet->setCellValue("H".$start,"=SUM(H4:H".($start-1).")");
    $sheet->setCellValue("I".$start,"=SUM(I4:I".($start-1).")");
    $sheet->setCellValue("J".$start,"=SUM(J4:J".($start-1).")");
    $sheet->setCellValue("K".$start,"=SUM(K4:K".($start-1).")");
    $sheet->setCellValue("L".$start,"=SUM(L4:L".($start-1).")");
    $sheet->setCellValue("M".$start,"=SUM(M4:M".($start-1).")");
    $sheet->setCellValue("N".$start,"=SUM(N4:N".($start-1).")");
    
    $sheet  ->getStyle('D4:N'.$start)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
    
    header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename='RINCIAN_GAJI_DOKTER.xlsx'");
	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	$writer->save('php://output');
    return;    
}

/* This is Modal Form and used for add and edit the current */

$dbtable=new DBTable($db,"smis_hrd_grup_gaji");
$dbtable->setShowAll(true);
$dlist=$dbtable->view("",0);
$adapter=new SelectAdapter("nama","slug");
$content=$adapter->getContent($dlist['data']);
$content[]=array("name"=>"","value"=>"","default"=>1);
$uitable=new Table(array());
$uitable->setName("penggajian_dokter");
$uitable->addModal("dari","date","Dari N-BPJS","");
$uitable->addModal("sampai","date","Sampai N-BPJS","");
$uitable->addModal("dari_bpjs","date","Dari BPJS","");
$uitable->addModal("sampai_bpjs","date","Sampai BPJS","");
$uitable->addModal("grup_gaji", "select", "Grup Gaji", $content);
$form=$uitable->getModal()->getForm();

$proses=new Button("","","Proses");
$proses->setClass(" btn-primary");
$proses->setIcon("fa fa-refresh");
$proses->setIsButton(Button::$ICONIC_TEXT);
$proses->setAction ( "penggajian_dokter.excel()" );
$form->addElement ( "", $proses );
echo $form->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "hrd/resource/js/penggajian_dokter.js",false );

?>