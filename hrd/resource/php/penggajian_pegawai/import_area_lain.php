<?php
    global $db;
    $head=array ("No.",'Tanggal','Sheets','Max Memory','Max Time','File');
    $uitable = new Table ( $head, "Import Area Lain", NULL, true );
    $uitable->setName ( "import_area_lain" );
    
    $btn=new Button("","","");
    $btn->setIsButton(Button::$ICONIC);
    $btn->setIcon("fa fa-file-excel-o");
    $btn->setClass(" btn btn-inverse");
    $uitable->addContentButton("import_proceed",$btn);
    
    $btn=new Button("","","");
    $btn->setIsButton(Button::$ICONIC);
    $btn->setIcon("fa fa-trash-o");
    $btn->setClass(" btn btn-inverse");
    $uitable->addContentButton("detach_import",$btn);
    if (isset ( $_POST ['command'] )) {
        require_once "smis-libs-class/ExcelImporter.php";
        require_once "hrd/class/responder/AreaLainDBResponder.php";
        $adapter = new SimpleAdapter ();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add ( "File","file" );
        $adapter->add ( "Tanggal","tanggal","date d M Y H:i" );
        $adapter->add ( "Sheets","sheets" );
        $adapter->add ( "Max Memory","max_memory" );
        $adapter->add ( "Max Time","max_time" );
        
        $dbtable = new DBTable ( $db, "smis_hrd_area_lain_importer" );
        
        $dbres = new AreaLainDBResponder ( $dbtable, $uitable, $adapter );
        if($dbres->isSave() || $dbres->is("import_proceed")){
            $importer=new ExcelImporter($db,new DBTable($db,"smis_hrd_area_lain"),"");
            $dbres->setImporter($importer);
        }
        $data = $dbres->command ( $_POST ['command'] );
        echo json_encode ( $data );
        return;
    }
    
    $uitable->addModal("id", "hidden", "", "");
    $uitable->addModal("tanggal", "datetime", "Tanggal", date("Y-m-d H:i:s"));
    $uitable->addModal("max_memory", "text", "Max Memory", "150M");
    $uitable->addModal("max_time", "text", "Max Time", "3600000");
    $uitable->addModal("file", "file-single-document", "File", "");
    echo $uitable->getHtml ();
    echo $uitable->getModal()->setTitle("Import Area Lain")->getHtml();
    echo addJS ( "framework/smis/js/table_action.js" );
    echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
    echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
    echo addJS ( "hrd/resource/js/import_area_lain.js",false );
    
?>

