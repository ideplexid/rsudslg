<?php

/**
 * digunakan untuk penggajian_asisten_ok khusus Sukmawijaya
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_grup_gaji
 * @since		: 10 Sep 2017
 * @version		: 1.2.0
 * 
 * */


global $db;
if (isset ( $_POST ['command'] )) {
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_total_asisten_anastesi");
    $serv->addData("dari",$_POST['dari']);
    $serv->addData("sampai",$_POST['sampai']);
    $serv->execute();
    $serv->setMode(ServiceConsumer::$CLEAN_BOTH);
    $cont=$serv->getContent();
    
    $result=0;
    foreach($cont as $x){
        $result+=$x;
    }
    
    $dbtable=new DBTable($db,"smis_hrd_grup_gaji");
    $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
    $grup_gaji=$dbtable->select(array("slug"=>$_POST['grup_gaji']));
    
    $dbtable=new DBTable($db,"smis_hrd_employee");
    $dbtable->addCustomKriteria(" grup_gaji "," = '".$_POST['grup_gaji']."'");
    $dbtable->setShowAll(true);
    $data=$dbtable->view("",0);
    $list=$data['data'];
    $total_data =count($list);

    require_once("smis-libs-out/php-excel/PHPExcel.php");
    loadLibrary("smis-libs-function-time");
 
    $excel = new PHPExcel ();
	$sheet = $excel->getActiveSheet();
    $sheet->mergeCells("A1:D1");
    $sheet->setCellValue("A1","PENGGAJIAN PENATA OK");    
    $sheet->setCellValue("A2","Periode");
    $sheet->mergeCells("A2:B2");
    $sheet->setCellValue("C2",ArrayAdapter::format("date d M Y",$_POST['dari']));
    $sheet->setCellValue("D2",ArrayAdapter::format("date d M Y",$_POST['sampai']));    
    $sheet->mergeCells("A3:B3");
    $sheet->setCellValue("A3","Total Pendapatan Penata OK");
    $sheet->setCellValue("C3",$result);    
    $sheet->mergeCells("A4:B4");
    $sheet->setCellValue("A4","Jumlah Orang Penata OK");
    $sheet->setCellValue("C4",count($list));    
    $sheet->mergeCells("A5:B5");
    $sheet->setCellValue("A5","Pendapatan Per Petugas");
    $sheet->setCellValue("C5","=C3/C4");
    
    $start=6;
    $sheet->setCellValue("A6","No.");
    $sheet->setCellValue("B6","Nama");
    $sheet->setCellValue("C6","NIP");        
    $sheet->setCellValue("D6","Nilai");
        
    $start=7;
    foreach($list as $x){
        $sheet->setCellValue("A".$start,$start-6);
        $sheet->setCellValue("B".$start,$x->nama);
        $sheet->setCellValue("C".$start,"'".$x->nip);        
        $sheet->setCellValue("D".$start,"=C5");
        $start++;
    }
    
    header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename='Penggajian Penata OK.xlsx'");
	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $writer->setPreCalculateFormulas(true);
	$writer->save('php://output');
    return;
}

/* This is Modal Form and used for add and edit the current */

$dbtable=new DBTable($db,"smis_hrd_grup_gaji");
$dbtable->setShowAll(true);
$dlist=$dbtable->view("",0);
$adapter=new SelectAdapter("nama","slug");
$content=$adapter->getContent($dlist['data']);
$content[]=array("name"=>"","value"=>"","default"=>1);
$uitable=new Table(array());
$uitable->setName("penggajian_asisten_ok");
$uitable->addModal("dari","date","Dari","");
$uitable->addModal("sampai","date","Sampai","");
$uitable->addModal("grup_gaji", "select", "Grup Gaji", $content);
$form=$uitable->getModal()->getForm();

$proses=new Button("","","Proses");
$proses->setClass(" btn-primary");
$proses->setIcon("fa fa-refresh");
$proses->setIsButton(Button::$ICONIC_TEXT);
$proses->setAction ( "penggajian_asisten_ok.excel()" );
$form->addElement ( "", $proses );
echo $form->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "hrd/resource/js/penggajian_penata_ok.js",false );

?>