<?php

/**
 * digunakan untuk melakukan manajemen 
 * data lembur karyawan. setiap kali karyawan lembur
 * dapat diketahui dan ditulis dengan menu ini.
 * yang mana nantinya setiap gaji lembur karyawan 
 * akan di simpan disini dan ditampilkan sebagai tambahan 
 * gaji di akhir bulan.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_lembur
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */

global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';

$header=array ("Nama","Alamat",'L/P','Pendidikan','Struktural' );
$dbtable= new DBTable($db, "smis_hrd_employee");
$dktable = new Table ( $header );
$dktable->setName ( "lembur_employee" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Alamat", "alamat" );
$dkadapter->add ( "L/P", "jk","trivial_0_L_P" );
$dkadapter->add ( "Pendidikan", "pendidikan");
$dkadapter->add ( "Struktural", "struktural" );
$lembur_employee = new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ('No.',"Keterangan","Dari","Sampai","Jam","Nilai Per Jam","Total");
$pemreg=new MasterSlaveTemplate($db, "smis_hrd_lembur", "hrd", "lembur");
$pemreg->setDateEnable(true);
$uitable=$pemreg->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setName("lembur");
$pemreg->getDBtable()->setOrder(" tanggal DESC ");
$pemreg->setUITable($uitable);
if($pemreg->getDBResponder()->isView() && $_POST['super_command']==""){
	$pemreg	->getDBtable()
			->addCustomKriteria("id_pegawai", "='".$_POST['id_pegawai']."'");
}


if($pemreg->getDBResponder()->isSave() ){
	$dari=$_POST['dari'];
	$sampai=$_POST['sampai'];
	$nilai=$_POST['nilai'];
    $menit=minute_different ($sampai, $dari);
	$durasi=0;
	$bulatkan=getSettings($db, "hrd-employee-hitung-lembur", "jam");
	if($bulatkan=="floor-jam"){
		$durasi=floor($menit/60);
	}else if($bulatkan=="ceil-jam"){
		$durasi=ceil($menit/60);
	}else if($bulatkan=="jam"){
		$durasi=round($menit/60);
	}else{
		$durasi=round($menit/60,2);
	}
	$total=$durasi*$nilai;
	$pemreg->getDBResponder()->addColumnFixValue("jam", $durasi);
	$pemreg->getDBResponder()->addColumnFixValue("total", $total);
}

$pemreg->addFlag("id_pegawai", "Pilih Pegawai Dahulu", "Silakan Pilih Pegawai Dahulu")
		 ->addNoClear("id_pegawai")
		 ->addNoClear("nama_pegawai")
		 ->addNoClear("pendidikan")
		 ->addNoClear("struktural")
         ->addNoClear("rumus")
		 ->setDateTimeEnable(true)
		 ->getUItable()
		 ->addModal("id_pegawai", "hidden", "", "")
		 ->addModal("nama_pegawai", "chooser-lembur-lembur_employee", "Nama", "")
		 ->addModal("pendidikan", "text", "Pendidikan", "","n",null,true)
		 ->addModal("struktural", "text", "Struktural", "","n",null,true)
         ->addModal("rumus_lembur", "hidden", "", "","",null,true);
         
$pemreg ->getForm();
$pemreg ->getUItable()
		 ->setHeader($header)
		 ->addModal ( "id", "hidden", "", "")
		 ->addModal ( "tanggal", "date", "Tanggal", date("Y-m-d"))
		 ->addModal ( "dari", "datetime", "Dari", date("Y-m-d H:i"))
		 ->addModal ( "sampai", "datetime", "Sampai", date("Y-m-d H:i"))
         ->addModal ( "keterangan", "textarea", "Keterangan", "")
         ->addModal ( "nilai", "money", "Nilai Per Jam", getSettings($db, "hrd-employee-nilai-lembur", "10000"));

if($pemreg->getDBResponder()->isPreload() && getSettings($db, "hrd-mode-lembur-rumus", "0")=="1"){
    
    $dbtable = new DBTable ( $db, "smis_hrd_rumus_lembur" );
    $dbtable->setShowAll ( true );
    $dbtable->setOrder ( "nama ASC" );
    $data = $dbtable->view ( "", 0 );
    $job = new SelectAdapter("nama", "id");
    $rumus = $job->getContent ( $data ['data'] );
    $rumus[]=array("name"=>"","value"=>"");
    
    $pemreg ->getUItable()
            ->addModal ( "id_rumus", "select", "Rumus", $rumus)
            ->addModal ( "rumus", "hidden", "", "")
            ->addModal ( "kalkulasi", "hidden", "", "");
}


$adapter=$pemreg->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Jam", "jam")
		->add("Tanggal", "tanggal","date d M Y")
		->add("Keterangan", "keterangan")
		->add("Nilai Per Jam", "nilai","money Rp.")
		->add("Total", "total","money Rp.")
		->add("Dari", "dari","date d M Y H:i")
		->add("Sampai", "sampai","date d M Y H:i");
$pemreg ->getSuperCommand()
        ->addResponder("lembur_employee", $lembur_employee);
$pemreg ->addSuperCommand("lembur_employee", array())
		 ->addSuperCommandArray("lembur_employee", "id_pegawai", "id")
		 ->addSuperCommandArray("lembur_employee", "nama_pegawai", "nama")
		 ->addSuperCommandArray("lembur_employee", "struktural", "struktural")
		 ->addSuperCommandArray("lembur_employee", "pendidikan", "pendidikan")
         ->addSuperCommandArray("lembur_employee", "rumus_lembur", "rumus_lembur")		 	
		 ->addSuperCommandArray("lembur_employee", "smis_action_js", "lembur.view();","smis_action_js")
		 ->addRegulerData("id_pegawai", "id_pegawai","id-value")
		 ->getModal()
		 ->setTitle("Lembur Karyawan");
$pemreg->addResouce("js","hrd/resource/js/lembur.js");
$pemreg->initialize();
?>

