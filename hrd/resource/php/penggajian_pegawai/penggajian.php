<?php

/**
 * digunakan untuk penggajian khusus KMU
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_penggajian
 * @since		: 14 Mei 2015
 * @version		: 1.2.0
 * 
 * */


global $db;
/* for handle external data/handling employee detail */
if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == 'penggajian_employee') {
	$uitable = new Table ( array ('Number','Nama'), "", NULL, true );
	$uitable->setName ( "penggajian_employee" );
	$uitable->setModel ( Table::$SELECT );
	if (isset ( $_POST ['command'] )) {
		$adapter = new SimpleAdapter ();
		$adapter->add("Number","nip");
		$adapter->add("Nama","nama");		
		$column = array ('id','nip','nama' );
		$dbtable = new DBTable ( $db, "smis_vhrd_employee", $column );
		$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml ();
	return;
}

/* handling for payout */
$uitable = new Table ( array ('Nama','Nilai' ), "&nbsp;", NULL, true );
$btn=new Button("","","");
$btn->setClass(" btn btn-success");
$btn->setIsButton(Button::$ICONIC);
$btn->setIcon(" fa fa-money");
$uitable->addContentButton("detail",$btn);
$uitable->setName ( "penggajian" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    require_once "hrd/function/gaji_dokter_kmu.php";
    $result=gaji_dokter_kmu($db, $_POST['id_dokter'],$_POST['dari'],$_POST['sampai']);
    
    $response=new ResponsePackage();
    $response->setContent($result['table']);
    $response->setStatus(ResponsePackage::$STATUS_OK);
    echo json_encode($response->getPackage());
    return;
}

/* This is Modal Form and used for add and edit the current */

$uitable->addModal("nip","text","NIP","");
$uitable->addModal("nama","text","Nama","");
$uitable->addModal("dari","date","Dari","");
$uitable->addModal("sampai","date","Sampai","");
$uitable->addModal("perawat","text","Perawat","","y",null,true);
$uitable->addModal("id","hidden","","");
$form=$uitable->getModal()->getForm();

// modal for viewing student list
$modal_employee = new Modal ( "penggajian_modal", '', "Pilih Pegawai" );

// form for student
$id_employee = isset ( $_POST ['id_employee'] ) ? $_POST ['id_employee'] : "";
$id = new Hidden ( "id_employee", "id_employee", $id_employee );
$action = new Button ( 'select_employee', 'Pilih', 'Pilih' );
$action->setClass ( "btn-primary" );
$action->setIcon(" fa fa-list");
$action->setIsButton(Button::$ICONIC_TEXT);
$action->setAction ( "penggajian.list_employee()" );

$proses=new Button("","","Proses");
$proses->setClass(" btn-primary");
$proses->setIcon("fa fa-refresh");
$proses->setIsButton(Button::$ICONIC_TEXT);
$proses->setAction ( "penggajian.view()" );

$form->addElement ( "", $action );
$form->addElement ( "", $proses );
echo $form->getHtml ();

/* table of current content */
echo "<div class='clear line'></div>";
echo "<div id='table_content_penggajian'></div>";
echo $modal_employee->getHtml ();

echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "hrd/resource/js/penggajian.js",false );

?>