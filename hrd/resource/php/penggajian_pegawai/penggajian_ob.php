<?php

/**
 * digunakan untuk penggajian_ob khusus KMU
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_grup_gaji
 * @since		: 14 Mei 2015
 * @version		: 1.2.0
 * 
 * */


global $db;
if (isset ( $_POST ['command'] )) {
    require_once "smis-base/smis-include-service-consumer.php";
    $serv   = new ServiceConsumer($db,"get_total_linen_per_date",null);
    $serv   ->addData("awal",$_POST['dari'])
            ->addData("akhir",$_POST['sampai'])
            ->execute()
            ->setMode(ServiceConsumer::$KEY_ENTITY);
    $pendapatan           = $serv->getContent();
    
    $target_ob            = getSettings($db,"smis-hrd-target-ob",0);
    $persen_target_ob     = getSettings($db,"smis-hrd-persen-target-ob",0);
    $persen_kelebihan_ob  = getSettings($db,"smis-hrd-persen-kelebihan-ob",0);
    
    $dbtable     = new DBTable($db,"smis_hrd_grup_gaji");
    $dbtable     ->setFetchMethode(DBTable::$ARRAY_FETCH);
    $grup_gaji   = $dbtable->select(array("slug"=>$_POST['grup_gaji']));
    
    $dbtable     = new DBTable($db,"smis_hrd_employee");
    $dbtable     ->addCustomKriteria(" grup_gaji "," = '".$_POST['grup_gaji']."'");
    $dbtable     ->setShowAll(true);
    $data        = $dbtable->view("",0);
    $list        = $data['data'];
    $total_point = count($list);
    

    require_once ("smis-libs-out/php-excel/PHPExcel.php");
	require_once ("smis-libs-out/php-excel/PHPExcel/IOFactory.php");
    loadLibrary  ("smis-libs-function-time");
    $reader = PHPExcel_IOFactory::createReader('Excel2007');
    $excel  = $reader->load("hrd/resource/excel/penggajian_ob.xlsx");
	$sheet  = $excel->getSheet(0);
    

    $sheet->setCellValue("B2",ArrayAdapter::format("date d M Y",$_POST['dari']));
    $sheet->setCellValue("C2",ArrayAdapter::format("date d M Y",$_POST['sampai']));
    $sheet->setCellValue("B3",$target_ob);
    $sheet->setCellValue("C3",$persen_target_ob);
    $sheet->setCellValue("C4",$persen_kelebihan_ob);
    
    $kolom="E";
    $start_kolom = PHPExcel_Cell::columnIndexFromString($kolom);
    foreach($pendapatan as $ruang=>$nilai){
        if($nilai==null || $nilai*1<=0)
            continue;
        $kolom = PHPExcel_Cell::stringFromColumnIndex($start_kolom);
        $sheet->setCellValue($kolom."1",ArrayAdapter::slugFormat("unslug",$ruang));
        $sheet->setCellValue($kolom."2",$nilai);
        $start_kolom++;
    }
    $sheet->setCellValue("C5","=SUM(F2:".$kolom."2)");
    $sheet  ->getStyle('F2:'.$kolom.'2')
            ->getNumberFormat()
            ->setFormatCode("#,##0");

    $start = 15;
    $sheet->insertNewRowBefore($start+1, $total_point-1);
    foreach($list as $x){
        $lama_kerja = ceil(day_diff_only($x->tanggal_masuk,$_POST['sampai'])/365);
        $full       = full_date_difference ($x->tanggal_masuk, $_POST['sampai']);
        $string     = "";
        if($full['year']!=0)    $string.=$full['year']." thn";
        if($full['month']!=0)   $string.=$full['month']." bln";
        if($full['day']!=0)     $string.=$full['day']." hr";
        $sheet->setCellValue("A".$start,$x->nama);
        $sheet->setCellValue("B".$start,$string);
        $sheet->setCellValue("C".$start,$grup_gaji[$lama_kerja.'th']+1);
        $sheet->setCellValue("D".$start,'=C'.$start.'*$B$12');
        $sheet->setCellValue("E".$start,$x->gaji_pokok);
        $sheet->setCellValue("G".$start,'=SUM(D'.$start.':F'.$start.')');
        $start++;
    }
    $sheet->setCellValue("C".$start,"=SUM(C15:C".($start-1).")");
    $sheet->setCellValue("D".$start,"=SUM(D15:D".($start-1).")");
    $sheet->setCellValue("E".$start,"=SUM(E15:E".($start-1).")");
    $sheet->setCellValue("F".$start,"=SUM(F15:F".($start-1).")");
    $sheet->setCellValue("G".$start,"=SUM(G15:G".($start-1).")");
    $sheet->setCellValue("B11","=C".$start); //total point
    
    $sheet  ->getStyle('D15:G'.$start)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
    
    header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename='RINCIAN_GAJI_ob.xls'");
	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
	$writer->save('php://output');
    
    return;
    
}

/* This is Modal Form and used for add and edit the current */
$dbtable   = new DBTable($db,"smis_hrd_grup_gaji");
$dbtable   ->setShowAll(true);
$dlist     = $dbtable->view("",0);
$adapter   = new SelectAdapter("nama","slug");
$content   = $adapter->getContent($dlist['data']);
$content[] = array("name"=>"","value"=>"","default"=>1);
$uitable   = new Table(array());
$uitable   ->setName("penggajian_ob")
           ->addModal("dari","date","Dari","")
           ->addModal("sampai","date","Sampai","")
           ->addModal("grup_gaji", "select", "Grup Gaji", $content);
$form      = $uitable->getModal()->getForm();

$proses = new Button("","","Proses");
$proses ->setClass(" btn-primary")
        ->setIcon("fa fa-refresh")
        ->setIsButton(Button::$ICONIC_TEXT)
        ->setAction ( "penggajian_ob.excel()" );
$form   ->addElement ( "", $proses );
echo $form->getHtml ();
echo addJS  ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS  ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS  ( "hrd/resource/js/penggajian_ob.js",false );

?>