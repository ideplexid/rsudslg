<?php

/**
 * digunakan untuk melakukan manajemen 
 * gaji karyawan yang lebih dari satu
 * sehingga gaji-gaji karyawan yang tersusun
 * atas banyak komponen dapat ditulis disini
 * pada akhi bulan dapat diketahui berapa total gaji karyawan
 * yang bersangkutan. (Deprecated karena kebanyakan di embed di Employee)
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_salary
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */


global $db;
/* for handle external data/handling employee detail */
if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == 'salary_employee') {
	$uitable = new Table ( array ('Number','Nama'), "", NULL, true );
	$uitable->setName ( "salary_employee" );
	$uitable->setModel ( Table::$SELECT );
	if (isset ( $_POST ['command'] )) {
		$adapter = new SimpleAdapter ();
		$adapter->add("Number","nip");
		$adapter->add("Nama","nama");		
		$column = array ('id','nip','nama' );
		$dbtable = new DBTable ( $db, "smis_hrd_employee", $column );
		$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml ();
	return;
}

/* handling for payout */
$uitable = new Table ( array ('Nama','Nilai' ), "&nbsp;", NULL, true );
$btn=new Button("","","");
$btn->setClass(" btn btn-success");
$btn->setIsButton(Button::$ICONIC);
$btn->setIcon(" fa fa-money");
$uitable->addContentButton("detail",$btn);
$uitable->setName ( "salary" );
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add("Number","nip");
	$adapter->add("Nama","nama");
    $adapter->add("Nilai","nilai","money Rp.");
	$column = array ('id','nama','id_employee','nilai' );
	$dbtable = new DBTable ( $db, "smis_hrd_salary", $column );
	$id_employee = isset ( $_POST ['id_employee'] ) ? $_POST ['id_employee'] : "";
	$dbtable->setCustomKriteria ( " id_employee='" . $id_employee . "'" );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

/* This is Modal Form and used for add and edit the current */
$mcolumn = array ('id','nama','nilai' );
$mname = array ("","Nama","Nilai" );
$mtype = array ("hidden","text",'money' );
$mvalue = array ("","","");
$empty = array ("","n","y" );
$rule = array ("","free","free" );
$uitable->setModal ( $mcolumn, $mtype, $mname, $mvalue, $empty, $rule );
$modal = $uitable->getModal ();
$modal->setTitle ( "Gaji" );

// modal for viewing student list
$modal_employee = new Modal ( "employee_modal", '', "Pilih Pegawai" );

// form for student
$id_employee = isset ( $_POST ['id_employee'] ) ? $_POST ['id_employee'] : "";
$id = new Hidden ( "id_employee", "id_employee", $id_employee );
$action = new Button ( 'select_employee', 'Select', 'Select' );
$action->setClass ( "btn-primary" );
$action->setAction ( "salary.list_employee()" );

$nip = new Text ( 'nip_employee', 'nip_employee', "" );
$nama = new Text ( 'nama_employee', 'nama_employee', "" );

// form for student
$form = new Form ( "form_employee", "", "Gaji Pegawai" );
$form->addElement ( "", $id );
$form->addElement ( "Nama", $nama );
$form->addElement ( "NIP", $nip );
$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo $modal_employee->getHtml ();
echo "</div>";

echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "hrd/resource/js/salary.js",false );

?>