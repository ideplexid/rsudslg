<?php

/**
 * digunakan untuk melakukan manajemen 
 * hutang pegawai. disini pegawai didata siapa-siapa
 * saja yang melakukan hutang atau kasbon ke perusahaan
 * seperti cicilan rumah, cicilan mobil, hutang karena sakit
 * dan lain sebagainya. data-data tersebut akan dikumpulkan setiap bulan sehingga
 * pada akhir bulan gaji dapat diketahui sisanya.
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_bon
 * 				  - smis_hrd_jenis_bon
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * 
 * */


global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';

$header=array ("Nama","Alamat",'L/P','Pendidikan','Struktural' );
$dbtable= new DBTable($db, "smis_hrd_employee");
$dktable = new Table ( $header );
$dktable->setName ( "bon_employee" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Alamat", "alamat" );
$dkadapter->add ( "L/P", "jk","trivial_0_L_P" );
$dkadapter->add ( "Pendidikan", "pendidikan");
$dkadapter->add ( "Struktural", "struktural" );
$bon_employee = new DBResponder($dbtable, $dktable, $dkadapter);


$header=array ('No.',"Tanggal","Nilai","Keterangan","Jenis");
$pemreg=new MasterSlaveTemplate($db, "smis_hrd_bon", "hrd", "bon");
$uitable=$pemreg->getUItable();
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setName("bon");
$pemreg->getDBtable()->setOrder(" tanggal DESC ");
$pemreg->setUITable($uitable);
$content=NULL;

if($pemreg->getDBResponder()->isPreload() ){
	$dbtable=new DBTable($db, "smis_hrd_jenis_bon");
	$dbtable->setShowAll(true);
	$data=$dbtable->view("","");
	$select=new SelectAdapter("jenis_potongan", "jenis_potongan");
	$content=$select->getContent($data['data']);
}

if($pemreg->getDBResponder()->isView() && $_POST['super_command']==""){
	$pemreg	->getDBtable()
			->addCustomKriteria("id_pegawai", "='".$_POST['id_pegawai']."'");	
}


$pemreg->addFlag("id_pegawai", "Pilih Pegawai Dahulu", "Silakan Pilih Pegawai Dahulu")
		 ->addNoClear("id_pegawai")
		 ->addNoClear("nama_pegawai")
		 ->addNoClear("pendidikan")
		 ->addNoClear("struktural")
		 ->setDateEnable(true)
		 ->getUItable()
		 ->addModal("id_pegawai", "hidden", "", "")
		 ->addModal("nama_pegawai", "chooser-bon-bon_employee", "Nama", "")
		 ->addModal("pendidikan", "text", "Pendidikan", "","n",null,true)
		 ->addModal("struktural", "text", "Struktural", "","n",null,true);
$pemreg->getForm();
$pemreg->getUItable()
		 ->setHeader($header)
		 ->addModal ( "id", "hidden", "", "")
		 ->addModal ( "tanggal", "date", "Tanggal", date("Y-m-d"))
		 ->addModal ( "jenis", "select", "Jenis Potongan", $content)
		 ->addModal ( "nilai", "money", "Nilai", "")
		 ->addModal ( "keterangan", "textarea", "Keterangan", "");
$adapter=$pemreg->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
//"Nama","Keterangan","File",'Tanggal','Berakhir'
$adapter->add("Keterangan", "keterangan")
		->add("Nilai", "nilai","money Rp.")
		->add("Jenis", "jenis")
		->add("Tanggal", "tanggal","date d M Y");
$pemreg->getSuperCommand()
		 ->addResponder("bon_employee", $bon_employee);
$pemreg->addSuperCommand("bon_employee", array())
		 ->addSuperCommandArray("bon_employee", "id_pegawai", "id")
		 ->addSuperCommandArray("bon_employee", "nama_pegawai", "nama")
		 ->addSuperCommandArray("bon_employee", "struktural", "struktural")
		 ->addSuperCommandArray("bon_employee", "pendidikan", "pendidikan")		 	
		 ->addSuperCommandArray("bon_employee", "smis_action_js", "bon.view();","smis_action_js")
		 ->addRegulerData("id_pegawai", "id_pegawai","id-value")
		 ->getModal()
		 ->setTitle("Bon / Potong Gaji Karyawan");
$pemreg->initialize();
?>

