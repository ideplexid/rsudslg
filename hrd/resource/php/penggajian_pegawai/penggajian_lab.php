<?php

/**
 * digunakan untuk penggajian_lab khusus KMU
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_grup_gaji
 * @since		: 14 Mei 2015
 * @version		: 1.2.0
 * 
 * */


global $db;
if (isset ( $_POST ['command'] )) {
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_pendapatan_gaji_lab",null,"laboratory");
    $serv->addData("dari_bpjs",$_POST['dari_bpjs']);
    $serv->addData("dari",$_POST['dari']);
    $serv->addData("sampai_bpjs",$_POST['sampai_bpjs']);
    $serv->addData("sampai",$_POST['sampai']);
    $serv->execute();
    $cont=$serv->getContent();
    $bpjs=$cont['bpjs'];
    $umum=$cont['umum'];
    $target_umum            = getSettings($db,"smis-hrd-target-lab-umum",0);
    $persen_target_umum     = getSettings($db,"smis-hrd-persen-target-lab-umum",0);
    $persen_kelebihan_umum  = getSettings($db,"smis-hrd-persen-kelebihan-lab-umum",0);
    $target_bpjs            = getSettings($db,"smis-hrd-target-lab-bpjs",0);
    $persen_target_bpjs     = getSettings($db,"smis-hrd-persen-target-lab-bpjs",0);
    $persen_kelebihan_bpjs  = getSettings($db,"smis-hrd-persen-kelebihan-lab-bpjs",0);
    
    $dbtable=new DBTable($db,"smis_hrd_grup_gaji");
    $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
    $grup_gaji=$dbtable->select(array("slug"=>$_POST['grup_gaji']));
    
    $dbtable=new DBTable($db,"smis_hrd_employee");
    $dbtable->addCustomKriteria(" grup_gaji "," = '".$_POST['grup_gaji']."'");
    $dbtable->setShowAll(true);
    $data=$dbtable->view("",0);
    $list=$data['data'];
    $total_point =count($list);
    
    /*start excel*/
    require_once("smis-libs-out/php-excel/PHPExcel.php");
	require_once("smis-libs-out/php-excel/PHPExcel/IOFactory.php");
    loadLibrary("smis-libs-function-time");
    $reader = PHPExcel_IOFactory::createReader('Excel2007');
    $excel = $reader->load("hrd/resource/excel/penggajian_lab.xlsx");
	$sheet = $excel->getSheet(0);
    
    /*umum*/
    $sheet->setCellValue("B2",ArrayAdapter::format("date d M Y",$_POST['dari']));
    $sheet->setCellValue("C2",ArrayAdapter::format("date d M Y",$_POST['sampai']));
    $sheet->setCellValue("B3",$target_umum);
    $sheet->setCellValue("C3",$persen_target_umum);
    $sheet->setCellValue("C4",$persen_kelebihan_umum);
    $sheet->setCellValue("B5",$umum);
    
    /*bpjs*/
    $sheet->setCellValue("F2",ArrayAdapter::format("date d M Y",$_POST['dari_bpjs']));
    $sheet->setCellValue("G2",ArrayAdapter::format("date d M Y",$_POST['sampai_bpjs']));
    $sheet->setCellValue("F3",$target_bpjs);
    $sheet->setCellValue("G3",$persen_target_bpjs);
    $sheet->setCellValue("G4",$persen_kelebihan_bpjs);
    $sheet->setCellValue("F5",$bpjs);
    
    /*start put the data*/
    $start=15;
    $sheet->insertNewRowBefore($start+1, $total_point-1);
    foreach($list as $x){
        $lama_kerja=ceil(day_diff_only($x->tanggal_masuk,$_POST['sampai'])/365);
        $full=full_date_difference ($x->tanggal_masuk, $_POST['sampai']);
        $string="";
        if($full['year']!=0) $string.=$full['year']." thn";
        if($full['month']!=0) $string.=$full['month']." bln";
        if($full['day']!=0) $string.=$full['day']." hr";
        $sheet->setCellValue("A".$start,$x->nama);
        $sheet->setCellValue("B".$start,$string);
        $sheet->setCellValue("C".$start,$grup_gaji[$lama_kerja.'th']+1);
        $sheet->setCellValue("D".$start,'=C'.$start.'*$B$12');
        $sheet->setCellValue("E".$start,$x->gaji_pokok);
        $sheet->setCellValue("G".$start,'=SUM(D'.$start.':F'.$start.')');
        $start++;
    }
    $sheet->setCellValue("C".$start,"=SUM(C15:C".($start-1).")");
    $sheet->setCellValue("D".$start,"=SUM(D15:D".($start-1).")");
    $sheet->setCellValue("E".$start,"=SUM(E15:E".($start-1).")");
    $sheet->setCellValue("F".$start,"=SUM(F15:F".($start-1).")");
    $sheet->setCellValue("G".$start,"=SUM(G15:G".($start-1).")");
    $sheet->setCellValue("B11","=C".$start); //total point
    
    $sheet  ->getStyle('D15:G'.$start)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
    
    header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename='RINCIAN_GAJI_LAB.xlsx'");
	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	$writer->save('php://output');
    return;
    
}

/* This is Modal Form and used for add and edit the current */

$dbtable=new DBTable($db,"smis_hrd_grup_gaji");
$dbtable->setShowAll(true);
$dlist=$dbtable->view("",0);
$adapter=new SelectAdapter("nama","slug");
$content=$adapter->getContent($dlist['data']);
$content[]=array("name"=>"","value"=>"","default"=>1);
$uitable=new Table(array());
$uitable->setName("penggajian_lab");
$uitable->addModal("dari","date","Dari N-BPJS","");
$uitable->addModal("sampai","date","Sampai N-BPJS","");
$uitable->addModal("dari_bpjs","date","Dari BPJS","");
$uitable->addModal("sampai_bpjs","date","Sampai BPJS","");
$uitable->addModal("grup_gaji", "select", "Grup Gaji", $content);
$form=$uitable->getModal()->getForm();

$proses=new Button("","","Proses");
$proses->setClass(" btn-primary");
$proses->setIcon("fa fa-refresh");
$proses->setIsButton(Button::$ICONIC_TEXT);
$proses->setAction ( "penggajian_lab.excel()" );
$form->addElement ( "", $proses );
echo $form->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "hrd/resource/js/penggajian_lab.js",false );

?>