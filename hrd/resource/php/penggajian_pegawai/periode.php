<?php 
    require_once "smis-libs-class/MasterTemplate.php";    
    $query="SELECT DISTINCT template FROM smis_hrd_gaji WHERE prop!='del'";
    $list=$db->get_result($query);
    $option=new OptionBuilder();
    foreach($list as $x){
        $option->addSingle($x->template);
    }
    
    $detail=new Button("","","Detail Gaji");
    $detail->setIcon("fa fa-list-alt");
    $detail->setClass("btn btn-primary");
    $detail->setIsButton(Button::$ICONIC);
    
    $copy=new Button("","","Copy Periode");
    $copy->setIcon("fa fa-copy");
    $copy->setClass("btn btn-success");
    $copy->setIsButton(Button::$ICONIC);
    $copy->setPopover("Copy This Periode");
    
    
    require_once "hrd/class/responder/PeriodeResponder.php";
    $master = new MasterTemplate($db,"smis_hrd_periode_gaji","hrd","periode");
    $responder=new PeriodeResponder($master->getDBtable(),$master->getUItable(),$master->getAdapter());
    $master->setDBresponder($responder);
    $master->setDateEnable(true);
    $master ->getUItable()
            ->setExcelElementButtonEnable(true)
            ->addContentButton("detail_gaji",$detail)
            ->addContentButton("copy_gaji",$copy)
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false)
            ->setHeader(array("No.","Periode","Tanggal","Dari","Sampai","Template","Total","Lunas","Sisa"));
    $master ->getUItable()
            ->addModal("id","hidden","","")
            ->addModal("template","select","Template",$option->getContent())
            ->addModal("periode","text","Periode","")
            ->addModal("tanggal","date","Tanggal",date("Y-m-d"))
            ->addModal("dari","date","Dari","")
            ->addModal("sampai","date","Sampai","");
    $master ->getAdapter()
            ->setUseNumber(true,"No.","back.")
            ->add("Template","template")
            ->add("Total","total","money Rp.")
            ->add("Lunas","lunas","money Rp.")
            ->add("Sisa","sisa","money Rp.")
            ->add("Periode","periode")
            ->add("Tanggal","tanggal","date d M Y")
            ->add("Dari","dari","date d M Y")
            ->add("Sampai","sampai","date d M Y");
    $master ->setModalTitle("Gaji");
    $master ->addResouce("js","hrd/resource/js/periode.js");
    $master ->initialize();
?>