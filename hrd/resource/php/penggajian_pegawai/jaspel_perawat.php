<?php

/**
 * digunakan untuk penggajian khusus SUKMAWIJAYA
 * untuk penggajian multi perawat
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * @since		: 19 Mei 2017
 * @version		: 1.0.0
 * 
 * */
 
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'hrd/class/adapter/JaspelAdapter.php';
require_once 'hrd/function/sort_perawat.php';


$list=new ServiceProviderList($db, "get_pv_perawat_by_id_perawat");
$list->execute();
$ruangan=$list->getContent();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"all","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "perawat_jaspel_perawat" );
$dktable->setModel ( Table::$SELECT );
$perawat = new EmployeeResponder ( $db, $dktable, $dkadapter, "perawat" );

$urutkan=new OptionBuilder();
$urutkan->add("","","1");
$urutkan->add("Perawat","sort_perawat_name");
$urutkan->add("Tanggal","sort_perawat_tanggal");

$jaspel_perawat=new MasterSlaveServiceTemplate($db, "get_pv_perawat_by_id_perawat", "hrd", "jaspel_perawat");
$jaspel_perawat->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);

$jaspel_perawat->setDateTimeEnable(true);
$header=array ("No.","Waktu","Pasien",'NRM',"Tindakan",'Jaspel',"Team","Hak","Cara Bayar","Ruangan");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$jaspel_perawat->getServiceResponder()->addData("dari",$_POST['dari']);
	$jaspel_perawat->getServiceResponder()->addData("sampai",$_POST['sampai']);
	$jaspel_perawat->getServiceResponder()->addData("nama_tindakan",$_POST['nama_tindakan']);
	$jaspel_perawat->setEntity($_POST['ruangan']);
    $jaspel_perawat->getServiceResponder()->setSortingMethode($_POST['urutkan']);
}
$btn=$jaspel_perawat->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
                  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
                  ->addFlag("id_perawat", "Silakan Pilih Perawat", "Silakan Pilih Perawat")
                  ->addFlag("perawat", "Silakan Pilih Perawat", "Silakan Pilih Perawat")
                  ->addNoClear("dari")
                  ->addNoClear("sampai")
                  ->addNoClear("id_perawat")
                  ->addNoClear("perawat")
                  ->getUItable()
                  ->setAddButtonEnable(false)
                  ->getHeaderButton();
$jaspel_perawat->getUItable()
             ->setAction(false)
             ->addModal("dari", "datetime", "Dari", "")
             ->addModal("sampai", "datetime", "Sampai", "")
             ->addModal("ruangan", "select", "Ruangan", $ruangan)
             ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
             ->addModal("urutkan", "select", "Urutkan", $urutkan->getContent())
             ->addModal("id_perawat", "hidden", "", "","n")
             ->addModal("persen", "text", "Persen(%)", "100","")
             ->addModal("perawat", "chooser-jaspel_perawat-perawat_jaspel_perawat-Pilih Perawat", "Perawat", "","n")
             ->addModal("nama_tindakan", "chooser-jaspel_perawat-ptp_tindakan_perawat-Tindakan", "Tindakan", "");
$jaspel_perawat->getForm(true,"")
                ->addElement("", $btn);
$jaspel_perawat->getUItable()
             ->setHeader($header)
             ->setFooterVisible(false);
$summary=new JaspelAdapter();
if($jaspel_perawat->getDBResponder()->isView()){
        $summary->setPersen($_POST['persen']);
}
$jaspel_perawat  ->setAdapter($summary);
$jaspel_perawat->addViewData("dari", "dari")
               ->addViewData("sampai", "sampai")
               ->addViewData("perawat", "perawat")
               ->addViewData("carabayar", "carabayar")
               ->addViewData("ruangan", "ruangan")
               ->addViewData("perawat", "perawat")
               ->addViewData("urutkan", "urutkan")
               ->addViewData("id_perawat", "id_perawat")
               ->addViewData("nama_tindakan","nama_tindakan")
               ->addViewData("persen","persen");
		
$dktable = new Table ( array ("Nama","Kelas","Tarif"), "", NULL, true );
$dktable->setName ( "ptp_tindakan_perawat" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Kelas", "kelas", "unslug" );
$dkadapter->add ( "Tarif", "tarif", "money Rp." );
$tarif = new ServiceResponder ( $db, $dktable, $dkadapter, "get_keperawatan" );

$jaspel_perawat->getSuperCommand()->addResponder("perawat_jaspel_perawat", $perawat);
$jaspel_perawat->addSuperCommand("perawat_jaspel_perawat", array());
$jaspel_perawat->addSuperCommandArray("perawat_jaspel_perawat", "perawat", "nama");
$jaspel_perawat->addSuperCommandArray("perawat_jaspel_perawat", "id_perawat", "id");

$jaspel_perawat->getSuperCommand()->addResponder("ptp_tindakan_perawat", $tarif);
$jaspel_perawat->addSuperCommand("ptp_tindakan_perawat", array());
$jaspel_perawat->addSuperCommandArray("ptp_tindakan_perawat", "nama_tindakan", "nama");
$jaspel_perawat->initialize();
?>
