<?php

/**
 * digunakan untuk penggajian_perawat khusus Sukmawijaya
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_grup_gaji
 * @since		: 10 Sep 2017
 * @version		: 1.2.0
 * 
 * */


global $db;
if (isset ( $_POST ['command'] )) {
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_pendapatan_perawat");
    $serv->addData("dari_bpjs",$_POST['dari_bpjs']);
    $serv->addData("dari",$_POST['dari']);
    $serv->addData("sampai_bpjs",$_POST['sampai_bpjs']);
    $serv->addData("sampai",$_POST['sampai']);
    $serv->execute();
    $serv->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
    $cont=$serv->getContent();
    
    $result=array();
    foreach($cont as $x){
        foreach($x as $id_perawat=>$content){
            if(!isset($result[$id_perawat])){
                $result[$id_perawat]=array();
            }
            foreach($content as $name=>$value){
                if(!isset($result[$id_perawat][$name])){
                    $result[$id_perawat][$name]=0;
                }
                $result[$id_perawat][$name]+=$value;
            }
        }
    }
    
    
    
    $dbtable=new DBTable($db,"smis_hrd_grup_gaji");
    $dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
    $grup_gaji=$dbtable->select(array("slug"=>$_POST['grup_gaji']));
    
    $dbtable=new DBTable($db,"smis_hrd_employee");
    $dbtable->addCustomKriteria(" grup_gaji "," = '".$_POST['grup_gaji']."'");
    $dbtable->setShowAll(true);
    $data=$dbtable->view("",0);
    $list=$data['data'];
    $total_data =count($list);

    require_once("smis-libs-out/php-excel/PHPExcel.php");
	require_once("smis-libs-out/php-excel/PHPExcel/IOFactory.php");
    loadLibrary("smis-libs-function-time");
    $reader = PHPExcel_IOFactory::createReader('Excel2007');
    $excel = $reader->load("hrd/resource/excel/penggajian_perawat.xlsx");
	$sheet = $excel->getSheet(0);
    
    $sheet->setCellValue("B2",ArrayAdapter::format("date d M Y",$_POST['dari']));
    $sheet->setCellValue("C2",ArrayAdapter::format("date d M Y",$_POST['sampai']));
    $sheet->setCellValue("F2",ArrayAdapter::format("date d M Y",$_POST['dari_bpjs']));
    $sheet->setCellValue("G2",ArrayAdapter::format("date d M Y",$_POST['sampai_bpjs']));
    
    $persen_perawat_umum        = getSettings($db,"smis-hrd-persen-perawat-umum","100")."%";
    $poin_pertama_umum          = getSettings($db,"smis-hrd-poin-pertama-perawat-umum","4");
    $target_pertama_umum        = getSettings($db,"smis-hrd-target-pertama-perawat-umum","300000");
    $poin_berikutnya_umum       = getSettings($db,"smis-hrd-poin-berikutnya-perawat-umum","1");
    $target_berikutnya_umum     = getSettings($db,"smis-hrd-target-berikutnya-perawat-umum","300000");
    
    $persen_perawat_bpjs        = getSettings($db,"smis-hrd-persen-perawat-bpjs","100")."%";
    $poin_pertama_bpjs          = getSettings($db,"smis-hrd-poin-pertama-perawat-bpjs","4");
    $target_pertama_bpjs        = getSettings($db,"smis-hrd-target-pertama-perawat-bpjs","300000");
    $poin_berikutnya_bpjs       = getSettings($db,"smis-hrd-poin-berikutnya-perawat-bpjs","1");
    $target_berikutnya_bpjs     = getSettings($db,"smis-hrd-target-berikutnya-perawat-bpjs","300000");
    
    $sheet->setCellValue("B3",$target_pertama_umum);
    $sheet->setCellValue("B4",$target_berikutnya_umum);
    $sheet->setCellValue("B5",$persen_perawat_umum);
    $sheet->setCellValue("C3",$poin_pertama_umum);
    $sheet->setCellValue("C4",$poin_berikutnya_umum);
    
    $sheet->setCellValue("F3",$target_pertama_bpjs);
    $sheet->setCellValue("F4",$target_berikutnya_bpjs);
    $sheet->setCellValue("F5",$persen_perawat_bpjs);
    $sheet->setCellValue("G3",$poin_pertama_bpjs);
    $sheet->setCellValue("G4",$poin_berikutnya_bpjs);
    
    $start=9;
    $total_row=$start+$total_data;
    $sheet->insertNewRowBefore($start+1, $total_data-1);
    $sheet->setCellValue("C".$total_row,"=SUM(C9:C".($total_row-1).")");
    $sheet->setCellValue("D".$total_row,"=SUM(D9:D".($total_row-1).")");
    $sheet->setCellValue("E".$total_row,"=SUM(E9:E".($total_row-1).")");
    $sheet->setCellValue("F".$total_row,"=SUM(F9:F".($total_row-1).")");
    $sheet->setCellValue("G".$total_row,"=SUM(G9:G".($total_row-1).")");
    $sheet->setCellValue("H".$total_row,"=SUM(H9:H".($total_row-1).")");
    $sheet->setCellValue("I".$total_row,"=SUM(I9:I".($total_row-1).")");
    $sheet->setCellValue("J".$total_row,"=SUM(J9:J".($total_row-1).")");
    $sheet->setCellValue("K".$total_row,"=SUM(K9:K".($total_row-1).")");
    $sheet->setCellValue("L".$total_row,"=SUM(L9:L".($total_row-1).")");
    
    foreach($list as $x){
        $lama_kerja=ceil(day_diff_only($x->tanggal_masuk,$_POST['sampai'])/365);
        $full=full_date_difference ($x->tanggal_masuk, $_POST['sampai']);
        $string="";
        if($full['year']!=0) $string.=$full['year']." thn";
        if($full['month']!=0) $string.=$full['month']." bln";
        if($full['day']!=0) $string.=$full['day']." hr";
        $sheet->setCellValue("A".$start,$x->nama);
        $sheet->setCellValue("B".$start,$string);
        $sheet->setCellValue("C".$start,$grup_gaji[$lama_kerja.'th']+1);        
        $sheet->setCellValue("D".$start,$result[$x->id]['umum']);
        $sheet->setCellValue("E".$start,'=IF(D'.$start.'>0,$C$3,0)+IF(D'.$start.'>$B$3,ROUNDUP((D'.$start.'-$B$3)*$C$4/$B$4,0),0)');
        $sheet->setCellValue("F".$start,'=$D$'.$total_row.'*(E'.$start.'+C'.$start.')/($C$'.$total_row.'+$E$'.$total_row.')');
        $sheet->setCellValue("G".$start,$result[$x->id]['bpjs']);
        $sheet->setCellValue("H".$start,'=IF(G'.$start.'>0,$G$3,0)+IF(G'.$start.'>$F$3,ROUNDUP((G'.$start.'-$F$3)*$G$4/$F$4,0),0)');
        $sheet->setCellValue("I".$start,'=$G$'.$start.'*(H9+C9)/($C$'.$start.'+$H$'.$start.')');
        $sheet->setCellValue("J".$start,$x->gaji_pokok);
        $sheet->setCellValue("L".$start,'=SUM(I'.$start.':K'.$start.')+F'.$start);   
        $start++;
    }
    
    $sheet  ->getStyle('D9:D'.$start)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
    $sheet  ->getStyle('F9:G'.$start)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
    $sheet  ->getStyle('I9:L'.$start)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
    
    header('Content-type: application/vnd.ms-excel');	
	header("Content-Disposition: attachment; filename='RINCIAN_GAJI_PERAWAT.xlsx'");
	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $writer->setPreCalculateFormulas(true);
	$writer->save('php://output');
    return;
}

/* This is Modal Form and used for add and edit the current */

$dbtable=new DBTable($db,"smis_hrd_grup_gaji");
$dbtable->setShowAll(true);
$dlist=$dbtable->view("",0);
$adapter=new SelectAdapter("nama","slug");
$content=$adapter->getContent($dlist['data']);
$content[]=array("name"=>"","value"=>"","default"=>1);
$uitable=new Table(array());
$uitable->setName("penggajian_perawat");
$uitable->addModal("dari","date","Dari N-BPJS","");
$uitable->addModal("sampai","date","Sampai N-BPJS","");
$uitable->addModal("dari_bpjs","date","Dari BPJS","");
$uitable->addModal("sampai_bpjs","date","Sampai BPJS","");
$uitable->addModal("grup_gaji", "select", "Grup Gaji", $content);
$form=$uitable->getModal()->getForm();

$proses=new Button("","","Proses");
$proses->setClass(" btn-primary");
$proses->setIcon("fa fa-refresh");
$proses->setIsButton(Button::$ICONIC_TEXT);
$proses->setAction ( "penggajian_perawat.excel()" );
$form->addElement ( "", $proses );
echo $form->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "hrd/resource/js/penggajian_perawat.js",false );

?>