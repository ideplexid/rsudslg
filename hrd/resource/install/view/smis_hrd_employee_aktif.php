<?php 
global $wpdb;
$query="create or replace view 
        smis_hrd_employee_aktif as 
        SELECT smis_hrd_employee.*, 
        smis_hrd_job.nama as nama_jabatan, 
        smis_hrd_job.slug as slug  
        FROM smis_hrd_employee 
        LEFT JOIN smis_hrd_job ON smis_hrd_employee.jabatan=smis_hrd_job.id 
        WHERE smis_hrd_employee.keluar=0";
$wpdb->query ( $query );	
?>