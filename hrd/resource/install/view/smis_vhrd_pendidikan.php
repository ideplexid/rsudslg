<?php 
global $wpdb;
$query="create or replace view 
        smis_vhrd_pendidikan as 
        SELECT smis_hrd_pendidikan.* , 
        sum(if(smis_hrd_employee.jk=0,1,0)) as tersedia_lk,
        sum(if(smis_hrd_employee.jk=1,1,0)) as tersedia_pr,
        butuh_lk - sum(if(smis_hrd_employee.jk=0,1,0)) as kurang_lk,
        butuh_pr - sum(if(smis_hrd_employee.jk=1,1,0)) as kurang_pr
        FROM smis_hrd_pendidikan LEFT JOIN smis_hrd_employee 
        ON smis_hrd_pendidikan.pendidikan=smis_hrd_employee.pendidikan 
        GROUP BY smis_hrd_pendidikan.pendidikan";
$wpdb->query ( $query );	
?>