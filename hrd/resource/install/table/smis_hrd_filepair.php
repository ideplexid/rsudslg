<?php 
    $dbcreator=new DBCreator($wpdb,"smis_hrd_filepair");
    $dbcreator->addColumn("user","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("data","text",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("kunci","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("gembok","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("waktu","timestamp",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_CURRENT_TIMESTAMP,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->initialize();
?>