<?php 
$dbcreator=new DBCreator($wpdb,"smis_hrd_bon");
$dbcreator->addColumn("jenis","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("id_pegawai","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("keterangan","varchar(128)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("nilai","int(11)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->addColumn("tanggal","date",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
$dbcreator->initialize();
?>