<?php 
    $dbcreator=new DBCreator($wpdb,"smis_hrd_rumus_lembur");
    $dbcreator->addColumn("nama","varchar(32)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("formula","varchar(64)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->addColumn("keterangan","varchar(128)",DBCreator::$SIGN_NONE,false,DBCreator::$DEFAULT_NONE,false,DBCreator::$ON_UPDATE_NONE,"");
    $dbcreator->initialize();
?>