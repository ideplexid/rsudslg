<?php 
global $wpdb;
$query = "	
		INSERT INTO `smis_hrd_job` (`id`, `nama`, `keterangan`, `slug`, `prop`) VALUES
		(1, 'Dokter Tamu', 'dokter yang bukan organik', 'dokter-tamu', ''),
		(2, 'Dokter Organik', 'dokter yang organik', 'dokter-organik', ''),
		(3, 'Supir', 'supir - supir', 'supir', ''),
		(4, 'Perawat', 'perawat yang bekerja di poliklinik', 'perawat', ''),
		(5, 'Bidan', 'bidan yang bekerja di Verlos Kamer atau Ruang NICU dll.', 'bidan', ''),
		(6, 'Petugas', 'petugas-petugas yang bertugas seperti membersihkan ruangan', 'petugas', ''),
		(7, 'Team', 'Team OK, Team VK adalah team yang bekerja untuk mebersihkan dan membantu proses operasi di OK dan VK', 'team', ''),
		(8, 'Customer Service', 'para Customer service yang bekerja di bagian font office', 'customer_service', ''),
		(9, 'Teller', 'petugas-petugas yang bertugas sebegai teller seperti kasir dll', 'teller', ''),
		(10, 'Teknisi', 'adalah petugas teknisi', 'teknisi', ''),
		(11, 'Cleaning Service', 'adalah petugas kebersihan', 'cleaning_service', ''),
		(12, 'Ahli Gizi', 'petugas yang bekerja di bagian gizi untuk asuhan gizi', 'pengasuh_gizi', ''),
		(13, 'Satpam', 'petugas keamanan', 'security', ''),
		(14, 'Marketing', 'petugas marketing', 'marketing', ''),
		(15, 'Office Boy/Girl', 'petugas office boy', 'ob', ''),
		(16, 'Waitress', 'petugas dan pelayan', 'waitress', ''),
		(17, 'Apoteker', 'petugas apotek', 'apoteker', ''),
		(18, 'Laborant', 'petugas yang bekerja di laboratrium', 'laborant', ''),
		(19, 'Koki', 'koki yang memasak di dapur', 'chef', ''),
		(20, 'Administrasi', 'petugas yang berada di bagian administrasi', 'administrasi', ''),
		(21, 'Petugas EKG', 'petugas yang bertugas di bagian EKG', 'ekg', '');
	";
	$wpdb->query ( $query );

 ?>