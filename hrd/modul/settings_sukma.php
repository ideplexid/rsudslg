<?php
	global $db;	
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	$smis = new SettingsBuilder($db,"hrd_settings","hrd","settings_sukma","Pengaturan Sukma Wijaya");
	$smis ->setShowDescription(true);
    $smis ->setTabulatorMode(Tabulator::$POTRAIT);
    if($smis->isGroupAdded("setup_sukma","Setup Sukmawijaya","fa fa-flask")){
        $smis   ->addSectionCurrent("Laboratory")
                ->addCurrent("hrd-ui-kepegawaian-penggajian_lab","Aktifkan","0","checkbox","Aktifkan Penggajian Lab")
                ->addCurrent("smis-hrd-target-lab-umum","Target Minimal Pasien Umum","0","money","Minimum Target Pasien Umum Pendapatan Lab")
                ->addCurrent("smis-hrd-persen-target-lab-umum","Nilai Persentase Target Lab Umum","0","text","Nilai Persentase Target Lab Umum")
                ->addCurrent("smis-hrd-persen-kelebihan-lab-umum","Nilai Persentase Kelebihan Target Lab Umum","0","text","Nilai Persentase Kelebihan Target Lab Umum")
                ->addCurrent("smis-hrd-target-lab-bpjs","Target Minimal Pasien BPJS","0","money","Minimum Target Pasien Umum Pendapatan Lab")
                ->addCurrent("smis-hrd-persen-target-lab-bpjs","Nilai Persentase Target Lab BPJS","0","text","Nilai Persentase Target Lab BPJS")
                ->addCurrent("smis-hrd-persen-kelebihan-lab-bpjs","Nilai Persentase Kelebihan Target Lab BPJS","0","text","Nilai Persentase Kelebihan Target Lab BPJS")
                ->addSectionCurrent("Dokter")
                ->addCurrent("hrd-ui-kepegawaian-penggajian_dokter","Penggajian Dokter","0","checkbox","Aktifkan Penggajian Dokter")
                ->addCurrent("smis-hrd-lab-dokter-umum","Persentase Dokter Umum","0","text","Persentase Bagi Hasil Lab untuk Dokter Umum")
                ->addCurrent("smis-hrd-lab-dokter-spesialis","Persentase Dokter Spesiali","0","text","Persentase Bagi Hasil Lab untuk Dokter Spesialis")
                ->addSectionCurrent("Kamar Operasi")
                ->addCurrent("hrd-ui-kepegawaian-penggajian_penata_ok","Penggajian Penata OK","0","checkbox","Aktifkan Penggajian Penata OK")
                ->addCurrent("hrd-ui-kepegawaian-penggajian_asisten_ok","Penggajian Asisten OK","0","checkbox","Aktifkan Penggajian Asisten OK")
                ->addSectionCurrent("Perawat")
                ->addCurrent("hrd-ui-kepegawaian-penggajian_perawat","Aktifkan","0","checkbox","Aktifkan Penggajian Perawat")
                ->addCurrent("smis-hrd-persen-perawat-umum","Persentase Bagi Hasil Perawat Pasien Umum","0","text","Persentase Bagi Hasil Perawat Pasien Umum")
                ->addCurrent("smis-hrd-poin-pertama-perawat-umum","Point Pertama Perawat Umum","0","text","Poin Pertama jika Mencapai Target Pasien Umum")
                ->addCurrent("smis-hrd-target-pertama-perawat-umum","Nilai Maksimum Target Pertama Pasien Umum","0","money","Nilai Maksimum Pertama Pasien Umum")
                ->addCurrent("smis-hrd-poin-berikutnya-perawat-umum","Point Berikutnya Perawat Umum","0","text","Poin Berikutnya jika Mencapai Target Pasien Umum")
                ->addCurrent("smis-hrd-target-berikutnya-perawat-umum","Nilai Target Berikutnya Pasien Umum","0","money","Nilai Berikutnya Pasien Umum")
                ->addCurrent("smis-hrd-persen-perawat-bpjs","Persentase Bagi Hasil Perawat Pasien BPJS","0","text","Persentase Bagi Hasil Perawat Pasien BPJS")
                ->addCurrent("smis-hrd-poin-pertama-perawat-bpjs","Point Pertama Perawat BPJS","0","text","Poin Pertama jika Mencapai Target Pasien BPJS")
                ->addCurrent("smis-hrd-target-pertama-perawat-bpjs","Nilai Maksimum Target Pertama Pasien BPJS","0","money","Nilai Maksimum Pertama Pasien BPJS")
                ->addCurrent("smis-hrd-poin-berikutnya-perawat-bpjs","Point Berikutnya Perawat BPJS","0","text","Poin Berikutnya jika Mencapai Target Pasien BPJS")
                ->addCurrent("smis-hrd-target-berikutnya-perawat-bpjs","Nilai Target Berikutnya Pasien BPJS","0","money","Nilai Berikutnya Pasien BPJS")
                ->addSectionCurrent("Administrator")
                ->addCurrent("hrd-ui-kepegawaian-penggajian_admin","Aktifkan","0","checkbox","Aktifkan Penggajian Administrator")
                ->addCurrent("smis-hrd-target-admin-umum","Target Minimal Pasien Umum","0","money","Minimum Target Pasien Umum Pendapatan Admin")
                ->addCurrent("smis-hrd-persen-target-admin-umum","Nilai Persentase Target Lab Umum","0","text","Nilai Persentase Target Admin Umum")
                ->addCurrent("smis-hrd-persen-kelebihan-admin-umum","Nilai Persentase Kelebihan Target Lab Umum","0","text","Nilai Persentase Kelebihan Target Admin Umum")
                ->addCurrent("smis-hrd-target-admin-bpjs","Target Minimal Pasien BPJS","0","money","Minimum Target Pasien Umum Pendapatan Admin")
                ->addCurrent("smis-hrd-persen-target-admin-bpjs","Nilai Persentase Target Lab BPJS","0","text","Nilai Persentase Target Admin BPJS")
                ->addCurrent("smis-hrd-persen-kelebihan-admin-bpjs","Nilai Persentase Kelebihan Target Lab BPJS","0","text","Nilai Persentase Kelebihan Target Admin BPJS")
                ->addSectionCurrent("Office Boy")
                ->addCurrent("hrd-ui-kepegawaian-penggajian_ob","Aktifkan","0","checkbox","Aktifkan Penggajian OB")
                ->addCurrent("smis-hrd-target-ob","Target Minimal Office Boy","0","money","Minimum Target Pendapatan Kamar untuk Office Boy")
                ->addCurrent("smis-hrd-persen-target-ob","Nilai Persentase Target Office Boy","0","text","Nilai Persentase Target Office Boy")
                ->addCurrent("smis-hrd-persen-kelebihan-ob","Nilai Persentase Kelebihan Target Office Boy","0","text","Nilai Persentase Kelebihan Target Office Boy");
    }
    $smis->setPartialLoad(true);
    $response = $smis->init ();
?>