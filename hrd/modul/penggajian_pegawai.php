<?php 

$tabs=new Tabulator("penggajian_pegawai","Penggajian Pegawai",Tabulator::$LANDSCAPE_RIGHT);
if(getSettings($db,"hrd-ui-kepegawaian-periode","1")=="1")              $tabs->add("periode","Periode Gaji","",Tabulator::$TYPE_HTML," fa fa-money");
if(getSettings($db,"hrd-ui-kepegawaian-salary","1")=="1")               $tabs->add("salary","Gaji","",Tabulator::$TYPE_HTML," fa fa-money");
if(getSettings($db,"hrd-ui-kepegawaian-lembur","1")=="1")               $tabs->add("lembur","Lembur","",Tabulator::$TYPE_HTML," fa fa-gear");
if(getSettings($db,"hrd-ui-kepegawaian-insentif","1")=="1")             $tabs->add("insentif","Insentif","",Tabulator::$TYPE_HTML," fa fa-cc-visa");
if(getSettings($db,"hrd-ui-kepegawaian-bon","1")=="1")                  $tabs->add("bon","Bon","",Tabulator::$TYPE_HTML," fa fa-at");

if(getSettings($db,"hrd-ui-kepegawaian-jaspel_perawat","1")=="1")       $tabs->add("jaspel_perawat","Jaspel Perawat","",Tabulator::$TYPE_HTML," fa fa-money");
if(getSettings($db,"hrd-ui-kepegawaian-penggajian","1")=="1")           $tabs->add("penggajian","Penggajian","",Tabulator::$TYPE_HTML," fa fa-money");
if(getSettings($db,"hrd-ui-kepegawaian-import_area_lain","1")=="1")     $tabs->add("import_area_lain","Importer Area Lain","",Tabulator::$TYPE_HTML," fa fa-file-excel-o");
if(getSettings($db,"hrd-ui-kepegawaian-data_area_lain","1")=="1")       $tabs->add("data_area_lain","Data Area Lain","",Tabulator::$TYPE_HTML," fa fa-map");

if(getSettings($db,"hrd-ui-kepegawaian-penggajian_asisten_ok","1")=="1")$tabs->add("penggajian_asisten_ok","Penggajian Asisten OK","",Tabulator::$TYPE_HTML," fa fa-scissors");
if(getSettings($db,"hrd-ui-kepegawaian-penggajian_penata_ok","1")=="1") $tabs->add("penggajian_penata_ok","Penggajian Penata OK","",Tabulator::$TYPE_HTML," fa fa-scissors");
if(getSettings($db,"hrd-ui-kepegawaian-penggajian_perawat","1")=="1")   $tabs->add("penggajian_perawat","Penggajian Perawat","",Tabulator::$TYPE_HTML," fa fa-user");
if(getSettings($db,"hrd-ui-kepegawaian-penggajian_dokter","1")=="1")    $tabs->add("penggajian_dokter","Penggajian Dokter","",Tabulator::$TYPE_HTML," fa fa-user-md");
if(getSettings($db,"hrd-ui-kepegawaian-penggajian_ob","1")=="1")        $tabs->add("penggajian_ob","Penggajian OB","",Tabulator::$TYPE_HTML," fa fa-bath");
if(getSettings($db,"hrd-ui-kepegawaian-penggajian_admin","1")=="1")     $tabs->add("penggajian_admin","Penggajian Admin","",Tabulator::$TYPE_HTML," fa fa-ticket");
if(getSettings($db,"hrd-ui-kepegawaian-penggajian_lab","1")=="1")       $tabs->add("penggajian_lab","Penggajian Lab","",Tabulator::$TYPE_HTML," fa fa-flask");

$tabs->setPartialLoad(true, "hrd", "penggajian_pegawai", "penggajian_pegawai",true);
echo $tabs->getHtml();

?>