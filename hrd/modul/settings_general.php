<?php
	global $db;	
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	$smis = new SettingsBuilder($db,"hrd_settings","hrd","settings_general","Pengaturan Personalia");
	$smis ->setShowDescription(true);
	$smis ->setTabulatorMode(Tabulator::$LANDSCAPE_RIGHT);
    
    if($smis->isGroupAdded("print","Print Out","fa fa-print")){
        $smis->addCurrent("hrd-css-print-auto-salary","CSS Auto Salary","","textarea","gunakan .salary_one,.salary_logo,.salary_two,.salary_name,.salary_address");
    }
    
    if($smis->isGroupAdded("duplicate","Duplikasi","fa fa-cloud-upload")){
        $smis   ->addCurrent("hrd-job-duplicate","Aktifkan Duplikasi Database HRD Job","0","text","0 - Tidak Aktif,1. Auto Duplicate ,2. Manual Duplicate")
                ->addCurrent("hrd-pendidikan-duplicate","Aktifkan Duplikasi Database HRD Pendidikan","0","text","0 - Tidak Aktif,1. Auto Duplicate ,2. Manual Duplicate")
                ->addCurrent("hrd-employee-duplicate","Aktifkan Duplikasi Database HRD Employee","0","text","0 - Tidak Aktif,1. Auto Duplicate ,2. Manual Duplicate")
                ->addCurrent("hrd-job-duplicate","Aktifkan Duplikasi Database HRD Job","0","text","0 - Tidak Aktif,1. Auto Duplicate ,2. Manual Duplicate")
                ->addCurrent("hrd-pendidikan-duplicate","Aktifkan Duplikasi Database HRD Pendidikan","0","text","0 - Tidak Aktif,1. Auto Duplicate ,2. Manual Duplicate")
                ->addCurrent("hrd-employee-duplicate","Aktifkan Duplikasi Database HRD Employee","0","text","0 - Tidak Aktif,1. Auto Duplicate ,2. Manual Duplicate");
    }
    
    if($smis->isGroupAdded("data_induk","Tampilan Data Induk","fa fa-database")){
        $list = getAllFileInDir("hrd/resource/php/data_induk/",false,true);
        foreach($list as $x){
            $x      = str_replace(".php","",$x);
            $xname  = ArrayAdapter::slugFormat("unslug-ucword",$x);
            $smis   ->addCurrent("hrd-ui-data-induk-".$x,$xname,"1","checkbox","pengaktifan menu $xname");
        }
    }
    
    if($smis->isGroupAdded("kepegawaian","Kepegawaian","fa fa-users")){
        $smis   ->addCurrent("hrd-employee-max-kontrak-limit","Maksimum Durasi Kontrak","60","text","Maksimum Durasi Kontrak")
                ->addCurrent("hrd-employee-nidn","Tampilkan NIDN","","checkbox","Tampilkan NIDN dalam Menu Pegawai")
                ->addCurrent("hrd-employee-grup-gaji","Grup Gaji","","checkbox","Grup Gaji")
                ->addCurrent("hrd-employee-rumus-lembur","Tampilkan Rumus Lembur","","checkbox","Tampilkan Rumus Lembur")
                ->addSectionCurrent("Gaji Tetap")
                ->addCurrent("hrd-employee-gaji-pokok","Tampilkan Gaji Pokok","","checkbox","Tampilkan Gaji Pokok")
                ->addCurrent("hrd-employee-tunjangan-struktural","Tampilkan Tunjangan Struktural","","checkbox","Tampilkan Tunjangan Struktural")
                ->addCurrent("hrd-employee-tunjangan-fungsional","Tampilkan Tunjangan Fungsional","","checkbox","Tampilkan Tunjangan Fungsional")
                ->addCurrent("hrd-employee-tunjangan-golongan","Tampilkan Tunjangan Golongan","","checkbox","Tampilkan Tunjangan Golongan")
                ->addCurrent("hrd-employee-tunjangan-pengabdian","Tampilkan Tunjangan Pengabdian","","checkbox","Tampilkan Tunjangan Pengabdian")
                ->addCurrent("hrd-employee-tunjangan-jabatan","Tampilkan Tunjangan Jabatan","","checkbox","Tampilkan Tunjangan Jabatan")
                ->addCurrent("hrd-employee-tunjangan-pendidikan","Tampilkan Tunjangan Pendidikan","","checkbox","Tampilkan Tunjangan Pendidikan")
                ->addCurrent("hrd-employee-tunjangan-pasutri","Tampilkan Tunjangan Suami / Istri","","checkbox","Tampilkan Tunjangan Suami / Istri")
                ->addCurrent("hrd-employee-tunjangan-anak","Tampilkan Tunjangan Anak","","checkbox","Tampilkan Tunjangan Anak")
                ->addSectionCurrent("Gaji Tunjangan dan BPJS")
                ->addCurrent("hrd-employee-tunjangan-kehadiran","Tampilkan Tunjangan Absensi dan Kehadiran","","checkbox","Tampilkan Tunjangan Absensi dan Kehadiran")
                ->addCurrent("hrd-employee-dana-pensiun","Tampilkan Dana Pensiun","","checkbox","Tampilkan Dana Pensiun")
                ->addCurrent("hrd-employee-insentif","Tampilkan Dana Insentif","","checkbox","Tampilkan Dana Insentif")
                ->addCurrent("hrd-employee-bpjs-kesehatan","Tampilkan Dana BPJS Kesehatan","","checkbox","Tampilkan Dana BPJS Kesehatan")
                ->addCurrent("hrd-employee-bpjs-ketenagakerjaan","Tampilkan BPJS Ketenaga Kerjaan","","checkbox","Tampilkan BPJS Ketenagakerjaan")
                ->addCurrent("hrd-employee-lain","Tampilkan Lain-Lain","","checkbox","Tampilkan Lain-Lain")
                ->addSectionCurrent("Uang Kehadiran")
                ->addCurrent("hrd-employee-uang-hadir-pagi","Tampilkan Untuk Uang Hadir Pagi","","checkbox","Tampilkan Untuk Uang Hadir Pagi")
                ->addCurrent("hrd-employee-uang-hadir-siang","Tampilkan Untuk Uang Hadir Siang","","checkbox","Tampilkan Untuk Uang Hadir Siang")
                ->addCurrent("hrd-employee-uang-hadir-malam","Tampilkan Untuk Uang Hadir Malam","","checkbox","Tampilkan Untuk Uang Hadir Malam")
                ->addCurrent("hrd-employee-uang-makan","Tampilkan Uang Makan","","checkbox","Tampilkan Uang Makan")
                ->addCurrent("hrd-employee-uang-transportasi","Tampilkan Uang Transportasi","","checkbox","Tampilkan Uang Transportasi")
                ->addSectionCurrent("Jasa Medis dan Pelayanan")
                ->addCurrent("hrd-employee-jasa-pelayanan","Tampilkan Jasa Pelayanan","","checkbox","Tampilkan Jasa Pelayanan")
                ->addCurrent("hrd-employee-upah-operasi","Tampilkan Upah Operasi","","checkbox","Tampilkan Upah Operasi")
                ->addCurrent("hrd-employee-upah-perujuk","Tampilkan Upah Perujuk","","checkbox","Tampilkan Upah Perujuk")
                ->addCurrent("hrd-employee-upah-duduk","Tampilkan Upah Duduk","","checkbox","Tampilkan Upah Duduk")
                ->addCurrent("hrd-employee-upah-pasien","Tampilkan Upah Pasien","","checkbox","Tampilkan Upah Pasien");

        $smis->addSectionCurrent("Tab Kepegawaian");
        $list = getAllFileInDir("hrd/resource/php/kepegawaian/",false,true);
        foreach($list as $x){
            $x      = str_replace(".php","",$x);
            $xname  = ArrayAdapter::slugFormat("unslug-ucword",$x);
            $smis->addCurrent("hrd-ui-kepegawaian-".$x,$xname,"0","checkbox","pengaktifan menu $xname");
        }
    }
    

    if($smis->isGroupAdded("penggajian","Penggajian","fa fa-money")){
        $op = new OptionBuilder();
        $op ->add("Bulatkan Ke Bawah Sampai Jam ","floor-jam","1")
            ->add("Bulatkan Ke Atas Sampai Jam ","ceil-jam","1")
            ->add("Bulatkan Sampai Jam (0.5 Keatas dianggap 1) ","jam","1")
            ->add("Bulatkan Kebawah Sampai Menit (2 Digit di belakang Koma) ","menit");
    
        $smis ->addSectionCurrent("Penggajian")
              ->addCurrent("hrd-ui-kepegawaian-periode","Aktifkan Menu Periode Penggajian Pada Penggajian","0","checkbox","jika dicentang Periode Penggajian akan aktif di penggajian")
              ->addCurrent("hrd-ui-kepegawaian-insentif","Aktifkan Menu Insentif Pada Penggajian","0","checkbox","jika dicentang Insentif akan aktif di penggajian")
              ->addCurrent("hrd-ui-kepegawaian-bon","Aktifkan Menu Bon Pada Penggajian","0","checkbox","jika dicentang Bon akan aktif di penggajian")
              ->addSectionCurrent("Lembur")
              ->addCurrent("hrd-ui-kepegawaian-lembur","Aktifkan Menu Lembur Pada Penggajian","0","checkbox","jika dicentang Model Lembur akan aktif di penggajian")
              ->addCurrent("hrd-mode-lembur-rumus","Aktifkan Rumus pada Perhitungan Lembur","0","checkbox","jika di centang,maka perhitungan lembur akan mengikuti rumus")
              ->addCurrent("hrd-employee-nilai-lembur","Nilai Per Jam Lembur","0","money","Nilai Lembur Per Jam")
              ->addCurrent("hrd-employee-hitung-lembur","Perhitungan Lembur",$op->getContent(),"select","Perhitungan Lembur");
    }

    $smis->setPartialLoad(true);
    $response = $smis->init ();
?>