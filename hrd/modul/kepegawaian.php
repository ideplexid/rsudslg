<?php 

$tabs = new Tabulator("kepegawaian","Kepegawaian",Tabulator::$LANDSCAPE_RIGHT);
if(getSettings($db,"hrd-ui-kepegawaian-employee","1")=="1")             
    $tabs->add("employee","Pegawai","",Tabulator::$TYPE_HTML," fa fa-user");

if(getSettings($db,"hrd-ui-kepegawaian-mantan","1")=="1")               
    $tabs->add("mantan","Mantan","",Tabulator::$TYPE_HTML," fa fa-user-times");

if(getSettings($db,"hrd-ui-kepegawaian-calon","1")=="1")                
    $tabs->add("calon","Calon","",Tabulator::$TYPE_HTML," fa fa-user-plus");

if(getSettings($db,"hrd-ui-kepegawaian-arsip","1")=="1")                
    $tabs->add("arsip","Arsip","",Tabulator::$TYPE_HTML," fa fa-folder-open");

if(getSettings($db,"hrd-ui-kepegawaian-latihan","1")=="1")              
    $tabs->add("latihan","Latihan","",Tabulator::$TYPE_HTML," fa fa-graduation-cap");

if(getSettings($db,"hrd-ui-kepegawaian-pasien","1")=="1")               
    $tabs->add("pasien","Pasien Keluarga","",Tabulator::$TYPE_HTML," fa fa-users");

if(getSettings($db,"hrd-ui-kepegawaian-mapping_marketing","1")=="1")    
    $tabs->add("mapping_marketing","Mapping Marketing","",Tabulator::$TYPE_HTML," fa fa-map");

$tabs->setPartialLoad(true, "hrd", "kepegawaian", "kepegawaian",true);
echo $tabs->getHtml();

?>