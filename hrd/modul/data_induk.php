<?php 

$tabs=new Tabulator("data_induk","Data Induk",Tabulator::$LANDSCAPE_RIGHT);
if(getSettings($db,"hrd-ui-data-induk-keterangan_data_induk","1")=="1")
    $tabs->add("keterangan_data_induk","Keterangan","",Tabulator::$TYPE_HTML," fa fa-file");
if(getSettings($db,"hrd-ui-data-induk-rumus_lembur","1")=="1")
    $tabs->add("rumus_lembur","Rumus Lembur","",Tabulator::$TYPE_HTML," fa fa-money");
if(getSettings($db,"hrd-ui-data-induk-job","1")=="1")
    $tabs->add("job","Bagian","",Tabulator::$TYPE_HTML," fa fa-star");
if(getSettings($db,"hrd-ui-data-induk-pendidikan","1")=="1")
    $tabs->add("pendidikan","Pendidikan","",Tabulator::$TYPE_HTML," fa fa-graduation-cap");
if(getSettings($db,"hrd-ui-data-induk-jenis_bon","1")=="1")
    $tabs->add("jenis_bon","Jenis Bon","",Tabulator::$TYPE_HTML," fa fa-share-alt");
if(getSettings($db,"hrd-ui-data-induk-gaji","1")=="1")
    $tabs->add("gaji","Master Gaji","",Tabulator::$TYPE_HTML," fa fa-money");
if(getSettings($db,"hrd-ui-data-induk-grup_gaji","1")=="1")
    $tabs->add("grup_gaji","Grup Pegawai","",Tabulator::$TYPE_HTML," fa fa-id-badge");
if(getSettings($db,"hrd-ui-data-induk-golongan","1")=="1")
    $tabs->add("golongan","Golongan","",Tabulator::$TYPE_HTML," fa fa-user");
if(getSettings($db,"hrd-ui-data-induk-ruangan_pegawai","1")=="1")
    $tabs->add("ruangan_pegawai","Ruangan Pegawai","",Tabulator::$TYPE_HTML," fa fa-university");
if(getSettings($db,"hrd-ui-data-induk-status_tenaga","1")=="1")
    $tabs->add("status_tenaga","Status Tenaga","",Tabulator::$TYPE_HTML," fa fa-users");

$tabs->setPartialLoad(true, "hrd", "data_induk", "data_induk",true);    
echo $tabs->getHtml();

?>