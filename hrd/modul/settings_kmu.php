<?php
	global $db;	
	require_once 'smis-framework/smis/api/SettingsBuilder.php';
	$smis = new SettingsBuilder($db,"hrd_settings","hrd","settings_kmu","Pengaturan KMU");
	$smis ->setShowDescription(true);
    $smis ->setTabulatorMode(Tabulator::$POTRAIT);
    if($smis->isGroupAdded("setup_kmu","Setup KMU","fa fa-calculator")){
        require_once 'smis-base/smis-include-service-consumer.php';
        $set     = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
        $set     ->execute();
        $content = $set->getContent();
        foreach($content as $x=>$y){
            $smis->addCurrent("hrd-bonus-kunjungan-pasien-".$y['value'],"Bonus Pasien ".$y['name'],"","money","Nilai Uang Per Kunjunga Pasien ".$y['name']);
        }  
        $smis   ->addSectionCurrent("Pengaturan Kamar Operasi")
                ->addCurrent("hrd-slug-kamar-operasi","Slug Kamar Operasi","","text","Slug dari Kamar Operasi")
                ->addCurrent("hrd-slug-area-kamar-operasi","Area Kamar Operasi","","text","Nama Area Kamar Operasi (gresik,lamongan,dll)")
                ->addCurrent("hrd-slug-igd","Slug IGD","","text","Slug dari IGD")
                ->addSectionCurrent("Import Data dari Cabang Lain")
                ->addCurrent("hrd-ui-kepegawaian-data_area_lain","Data Area Lain","0","checkbox","Aktifkan Manajemen Data Area Lain")
                ->addCurrent("hrd-ui-kepegawaian-import_area_lain","Import Data Area Lain","0","checkbox","Aktifkan Import Data Area Lain")
                ->addSectionCurrent("Kustomisasi Penggajian")
                ->addCurrent("hrd-ui-kepegawaian-jaspel_perawat","Jasa Pelayanan Perawat","0","checkbox","Aktifkan Jasa Pelayanan Perawat")
                ->addCurrent("smis-hrd-max-lock","Penguncian Operasi Pasien","50","text","Maksimum Penguncian Operasi Pasien ")
                ->addCurrent("hrd-ui-kepegawaian-penggajian","Penggajian Dokter & Perawat KMU","0","checkbox","Aktifkan Model Penggajian Dokter dan Perawat KMU");
    }
    $smis->setPartialLoad(true);
    $response = $smis->init ();
?>