<?php

$tabs=new Tabulator("outsoursing","Out Source",Tabulator::$LANDSCAPE_RIGHT);
$tabs->add("keterangan_outsoursing","Keterangan","",Tabulator::$TYPE_HTML," fa fa-file");
$tabs->add("branch_company","Cabang Perusahaan","",Tabulator::$TYPE_HTML," fa fa-building");
$tabs->add("outsource","Pegawai","",Tabulator::$TYPE_HTML," fa fa-users");
$tabs->add("accreject","Penerimaan","",Tabulator::$TYPE_HTML," fa fa-check-square-o");
$tabs->add("client","Klien","",Tabulator::$TYPE_HTML," fa fa-user");
$tabs->add("filepair","File Pair","",Tabulator::$TYPE_HTML," fa fa-key");
$tabs->setPartialLoad(true, "hrd", "outsoursing", "outsoursing",true);
echo $tabs->getHtml();

?>