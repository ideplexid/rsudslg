<?php
	global $wpdb;
    
    require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->setUsing(true, true, true);
	$install->extendInstall("hrd");
	$install->install();
    
    require_once 'smis-libs-class/DBCreator.php';
    require_once "hrd/resource/install/table/smis_hrd_berkas.php";
    require_once "hrd/resource/install/table/smis_hrd_bon.php";
    require_once "hrd/resource/install/table/smis_hrd_branch.php";
    require_once "hrd/resource/install/table/smis_hrd_client.php";
    require_once "hrd/resource/install/table/smis_hrd_employee.php";
    require_once "hrd/resource/install/table/smis_hrd_filepair.php";
    require_once "hrd/resource/install/table/smis_hrd_insentif.php";
    require_once "hrd/resource/install/table/smis_hrd_jenis_bon.php";
    require_once "hrd/resource/install/table/smis_hrd_job.php";
    require_once "hrd/resource/install/table/smis_hrd_lembur.php";
    require_once "hrd/resource/install/table/smis_hrd_move.php";
    require_once "hrd/resource/install/table/smis_hrd_outsource.php";
    require_once "hrd/resource/install/table/smis_hrd_pendidikan.php";
    require_once "hrd/resource/install/table/smis_hrd_perusahaan.php";
    require_once "hrd/resource/install/table/smis_hrd_rumus_lembur.php";
    require_once "hrd/resource/install/table/smis_hrd_salary.php";
    require_once "hrd/resource/install/table/smis_hrd_gaji.php";
    require_once "hrd/resource/install/table/smis_hrd_grup_gaji.php";
    require_once "hrd/resource/install/table/smis_hrd_periode_gaji.php";
    require_once "hrd/resource/install/table/smis_hrd_golongan.php";
    require_once "hrd/resource/install/table/smis_hrd_status_tenaga.php";
    require_once "hrd/resource/install/table/smis_hrd_ruangan_pegawai.php";
    
    require_once "hrd/resource/install/table/smis_hrd_area_lain.php";
    require_once "hrd/resource/install/table/smis_hrd_area_lain_importer.php";
    
    require_once "hrd/resource/install/view/smis_hrd_employee_aktif.php";
    require_once "hrd/resource/install/view/smis_vhrd_employee.php";
    require_once "hrd/resource/install/view/smis_vhrd_pendidikan.php";
    
    
?>
