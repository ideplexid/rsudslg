<?php 

require_once 'smis-libs-class/MasterTemplate.php';
global $db;
$jadwal=new MasterTemplate($db, "smis_lab_schedule", "laboratory", "jadwal");
$jadwal	->setTimeEnable(true)
		->setModalTitle("Jadwal");
$uitable=$jadwal->getUItable();
$uitable->setTitle("Plafon Jadwal");
$header=array("Awal","Akhir","Jam","Hari");
$uitable->setHeader($header);
$uitable->addModal("id", "hidden", "", "")
		->addModal("b_awal", "time", "Awal", "")
		->addModal("b_akhir", "time", "Akhir", "")
		->addModal("pindah", "checkbox", "Besok", "")
		->addModal("jam","time", "Jam", "");
$adapter=$jadwal->getAdapter();
$adapter->add("Awal", "b_awal","date H:i")
		->add("Akhir", "b_akhir","date H:i")
		->add("Hari","pindah","trivial_0_Hari Yang Sama_Hari Esok")
		->add("Jam", "jam","date H:i");
$jadwal->initialize();

?>