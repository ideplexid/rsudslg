<?php 
global  $db;
require_once ("smis-base/smis-include-service-consumer.php");
require_once "smis-libs-class/MasterTemplate.php";
require_once "smis-base/smis-include-service-consumer.php";

$layanan=new MasterTemplate($db,"smis_lab_layanan","laboratory","layanan");
$layanan->getUItable()->addModal("id","hidden","","");
$layanan->getUItable()->addModal("nama","text","Nama","");
$layanan->getUItable()->addModal("slug","text","Slug","");
$layanan->getUItable()->addModal("ganti","text","Replace","");
$layanan->getUItable()->addModal("grup","text","Grup","");
$layanan->getUItable()->addModal("indent","checkbox","Indent","");
$layanan->getUItable()->addModal("lis","textarea","LIS","");
$layanan->getUItable()->addModal("debet", "chooser-layanan-debet_layanan-Kode Debet", "Kode Debet Default", "","y",NULL,true,NULL,false);
$layanan->getUItable()->addModal("kredit", "chooser-layanan-kredit_layanan-Kode Kredit", "Kode Kredit Default", "","y",NULL,true,NULL,false);

$layanan->setModalTitle("Layanan");

$array=array("No.","Grup","Nama","Slug","Indent","LIS", "Kode Debet", "Kode Kredit");
$layanan->getUItable()->setHeader($array);
$layanan->getAdapter()->add("Grup","grup");
$layanan->getAdapter()->add("Nama","nama");
$layanan->getAdapter()->add("Slug","slug");
$layanan->getAdapter()->add("Indent","indent","trivial_1_X_");
$layanan->getAdapter()->add("LIS","lis");
$layanan->getAdapter()->add("Kode Debet","debet");
$layanan->getAdapter()->add("Kode Kredit","kredit");
$layanan->getAdapter()->setUseNumber(true,"No.","back.");
$layanan->getDBTable()->setOrder(true," grup ASC, nama ASC ");

if($layanan->getDBResponder()->isPreload() || $_POST['super_command']!=""  ){
    if(strpos($_POST['super_command'],"debet")!==false || strpos($_POST['super_command'],"kredit")!==false ){
        $uitable   = new Table(array('Nomor','Nama'),"");
        $uitable   ->setName($_POST['super_command'])
                   ->setModel(Table::$SELECT);
        $adapter   = new SimpleAdapter();
        $adapter   ->add("Nomor","nomor")
                   ->add("Nama","nama");
        $responder = new ServiceResponder($db, $uitable, $adapter, "get_account", "accounting");
        $layanan   ->getSuperCommand()
                   ->addResponder($_POST['super_command'],$responder);
        $layanan   ->addSuperCommand($_POST['super_command'],array())
                   ->addSuperCommandArray($_POST['super_command'],substr($_POST['super_command'],0,5),"nomor");
    }
}

if($layanan->getDBResponder()->isPreload() && $_POST['super_command']==""  ){
    $serv = new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
    $serv ->execute();
    $cons = $serv->getContent();
    global $wpdb;
    foreach($cons as $x){
        require_once "smis-libs-class/DBCreator.php";
        $dbcreator = new DBCreator($wpdb,"smis_lab_layanan",DBCreator::$ENGINE_MYISAM);
        $dbcreator ->addColumn("debet_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->addColumn("kredit_".$x['value'], "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
        $dbcreator ->initialize();
        $layanan   ->getUItable()->addModal("debet_".$x['value'], "chooser-layanan-debet_layanan_".$x['value']."-Kode Debet ".$x['name'], "Kode Debet ".$x['name'], "","y",NULL,true,NULL,false);
        $layanan   ->getUItable()->addModal("kredit_".$x['value'], "chooser-layanan-kredit_layanan_".$x['value']."-Kode Kredit".$x['name'], "Kode Kredit ".$x['name'], "","y",NULL,true,NULL,false);
        
        $layanan   ->addSuperCommand("debet_layanan_".$x['value'],array())
                   ->addSuperCommandArray("debet_layanan_".$x['value'],"debet_".$x['value'],"nomor");
        $layanan   ->addSuperCommand("kredit_layanan_".$x['value'],array())
                   ->addSuperCommandArray("kredit_layanan_".$x['value'],"kredit_".$x['value'],"nomor");
    }
    
    $layanan  ->addSuperCommand("debet_layanan",array())
              ->addSuperCommandArray("debet_layanan","debet","nomor");
    $layanan  ->addSuperCommand("kredit_layanan",array())
              ->addSuperCommandArray("kredit_layanan","kredit","nomor");
        
}
$layanan->setModalComponentSize(Modal::$MEDIUM);
$layanan->initialize();

?>