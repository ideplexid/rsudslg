<?php
$tabulator = new Tabulator ( "laporan", "laporan" );
$tabulator->add ( "lap_resume_hasil", "Resume Hasil", "", Tabulator::$TYPE_HTML ," fa fa-book");
$tabulator->add ( "lap_detail_hasil", "Detail Hasil", "", Tabulator::$TYPE_HTML ," fa fa-list");
$tabulator->add ( "lap_resume_layanan", "Resume Layanan", "", Tabulator::$TYPE_HTML ," fa fa-list-alt");
$tabulator->add ( "lap_detail_layanan", "Detail Layanan", "", Tabulator::$TYPE_HTML ," fa fa-money");
$tabulator->add ( "laporan_pend_perlayanan", "Pendapatan", "", Tabulator::$TYPE_HTML ," fa fa-usd");
$tabulator->add ( "laporan_sensus_harian", "Sensus Harian", "", Tabulator::$TYPE_HTML ," fa fa-list");
$tabulator->setPartialLoad(true,"laboratory","laporan","laporan",true);
echo $tabulator->getHtml ();
?>