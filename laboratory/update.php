<?php
	global $wpdb;
	
	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->extendInstall("lab");
	$install->install();
	
	require_once "laboratory/resource/install/table/order_lab.php";
	require_once "laboratory/resource/install/table/hasil_lab.php";
	require_once "laboratory/resource/install/table/smis_lab_dpesanan_lain.php";
	require_once "laboratory/resource/install/table/smis_lab_hasil.php";
	require_once "laboratory/resource/install/table/smis_lab_layanan.php";
	require_once "laboratory/resource/install/table/smis_lab_pertindakan.php";
	require_once "laboratory/resource/install/table/smis_lab_pesanan.php";
	require_once "laboratory/resource/install/table/smis_lab_schedule.php";
	require_once "laboratory/resource/install/table/smis_lab_rl52.php";
	
?>