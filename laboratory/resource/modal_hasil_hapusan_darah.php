<?php
global $db;
//$default_hapusan_darah = '<p><br></p><table class="table table-bordered"><tbody><tr><td><span style="font-weight: bold;">Jenis Sel</span></td><td><span style="font-weight: bold;">Hasil Pemeriksaan</span></td></tr><tr><td>Eritrosit</td><td>-</td></tr><tr><td>Leukosit</td><td>-</td></tr><tr><td>Trombosit</td><td>-</td></tr><tr><td><span style="font-weight: bold;">Kesimpulan</span></td><td>-</td></tr></tbody></table><p><br></p>';
$default_hapusan_darah = getSettings($db, "laboratory-template-hapusan-darah");
?>
<textarea id="laboratory_hapusan_darah"><?php echo $default_hapusan_darah; ?></textarea>
<div class='hide' id='default_hapusan_darah'><?php echo $default_hapusan_darah; ?></div>

<style type="text/css">
@media print {
	#hapusan_darah {-webkit-print-color-adjust: exact; }
	#hapusan_darah table.table { border: solid 1px #000;}
	#hapusan_darah table.table td { border: solid 1px #000;}
}
</style>

<script type="text/javascript">

function cetak_hapusan_darah(){
    if(LAB_ACTION.cekSave() == true) {
        showLoading();
        var id=$("#"+lab_the_action+"_id").val();
        var data={
                page:lab_the_page,
                action:lab_the_action,
                command:"calc_time_hapusan",
                prototype_name:lab_the_protoname,
                prototype_slug:lab_the_protoslug,
                prototype_implement:lab_the_protoimplement,
                id:id
            };
        $.post("",data,function(res){
            var json=getContent(res);
            $("#waktu_pemeriksaan_lab").html(": "+json.waktu_daftar);
            $("#waktu_sampel_masuk_lab").html(": "+json.waktu_ditangani_hapusan);
            $("#waktu_cetak_hasil_lab").html(": "+json.waktu_selesai_hapusan);
            $("#response_time_lab").html(": "+json.response_time_hapusan);
            dismissLoading();
            do_cetak_hapusan_darah();
        });
    }
}


function do_cetak_hapusan_darah(){
	var pemeriksa=$("#"+LAB_PREFIX+"_nama_konsultan").val();
	$("#lab_nama_pemeriksa").html(pemeriksa);
	
   var hapusan_darah_tepi=$('#laboratory_hapusan_darah').code();
   var header="";
   var content=hapusan_darah_tepi;
   var footer=$("#print_footer").html();
   var cara=$("#"+lab_the_action+"_carabayar").val();
   var p_content="";
   
   if(cara=="Asuransi" || $("#asuransi_cek").is(":checked")){
	    $("#g_utk_rm").hide();
		$("#g_utk_dokter").hide();
		$("#g_utk_lab").hide();
		$("#g_utk_asuransi").show();
		header=$("#print_header_gabungan").html();
		p_content="<div class='plong'>"+header+content+footer+"</div>";
					
	}
   
   	if($("#rekamedis_cek").is(":checked")){
   		$("#g_utk_rm").show();
		$("#g_utk_dokter").hide();
		$("#g_utk_asuransi").hide();
		$("#g_utk_lab").hide();
	    header=$("#print_header_gabungan").html();
	    p_content+="<div class='pagebreak'> </div><div class='plong'>"+header+content+footer+"</div>";
   	}
   
   	if($("#pasien_cek").is(":checked")){
   		$("#g_utk_rm").hide();
		$("#g_utk_dokter").show();
		$("#g_utk_asuransi").hide();
		$("#g_utk_lab").hide();
	    header=$("#print_header_gabungan").html();
		p_content+="<div class='pagebreak'> </div><div class='plong'>"+header+content+footer+"</div>";
	}

	if($("#lab_cek").is(":checked")){
	   	$("#g_utk_rm").hide();
		$("#g_utk_dokter").hide();
		$("#g_utk_asuransi").hide();
		$("#g_utk_lab").show();
		header=$("#print_header_gabungan").html();
		p_content+="<div class='pagebreak'> </div><div class='plong'>"+header+content+footer+"</div>";
	}
   	
   	if(p_content!="")
   		smis_print(p_content);
   	$("#g_utk_rm").show();
	$("#g_utk_dokter").show();
	$("#g_utk_asuransi").show();
	$("#g_utk_lab").show();
	
	var id=$("#"+lab_the_action+"_id").val();
	
	var data={
			page:lab_the_page,
			action:lab_the_action,
			command:"save",
			cetak_3:content,
			prototype_name:lab_the_protoname,
			prototype_slug:lab_the_protoslug,
			prototype_implement:lab_the_protoimplement,
			id:id
		};
	$.post("",data,function(res){});	
}
/*
	function lab_print_hasil3(){

		var pemeriksa=$("#"+LAB_PREFIX+"_nama_petugas").val();
		$("#lab_nama_pemeriksa").html(pemeriksa);
	
		
		var data=$('#laboratory_hapusan_darah').code();
		var header="";
		var content="<div id='hapusan_darah'>"+data+"</div>";
		var footer=$("#print_footer").html();
		var cara=$("#"+lab_the_action+"_carabayar").val();
		var p_content="";
			
			if(cara=="Asuransi"){
		    	$("#utk_rm").hide();
				$("#utk_dokter").hide();
				$("#utk_asuransi").show();
				header=$("#print_header").html();
				p_content=header+content+footer;
				smis_print(p_content);
			}
		    $("#utk_rm").show();
			$("#utk_dokter").hide();
			$("#utk_asuransi").hide();
		    header=$("#print_header").html();
			p_content=header+content+footer;
			smis_print(p_content);
			
			$("#utk_rm").hide();
			$("#utk_dokter").show();
			$("#utk_asuransi").hide();
		    header=$("#print_header").html();
			p_content=header+content+footer;
			smis_print(p_content);

			$("#utk_rm").show();
			$("#utk_dokter").show();
			$("#utk_asuransi").show();

			var id=$("#"+lab_the_action+"_id").val();
			var data={
					page:lab_the_page,
					action:lab_the_action,
					command:"save",
					cetak_3:content,
					prototype_name:lab_the_protoname,
					prototype_slug:lab_the_protoslug,
					prototype_implement:lab_the_protoimplement,
					id:id
				};
			$.post("",data,function(res){});		
		   	
		
	}*/

	$(document).ready(function(){
		$('#laboratory_hapusan_darah').summernote({
			  height: 400,                 // set editor height
			  minHeight: 300,             // set minimum height of editor
			  maxHeight: 600,             // set maximum height of editor 
			  onImageUpload: function(files, editor, welEditable) {
	                sendFile(files[0], editor, welEditable);
	            }
	  		}
		);
	});

</script>