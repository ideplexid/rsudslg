<?php
global $db;
require_once 'smis-libs-class/MasterSlaveTemplate.php';
$presensi=new MasterSlaveTemplate($db, "smis_lab_pesanan", "laboratory", "laporan_sensus_harian");
$presensi->setDateEnable(true);

require_once "laboratory/class/responder/SensusResponder.php";
$sensus = new SensusResponder($presensi->getDBtable(),$presensi->getUItable(),$presensi->getAdapter());
$presensi ->setDBresponder($sensus);

require_once 'smis-base/smis-include-service-consumer.php';
$urjip=new ServiceConsumer($db, "get_jenis_patient",array(),"registration");
$urjip->execute();
$content=$urjip->getContent();
$jenis_pasien=array();
foreach($content as $one){
	$option=array();
	$option['value']=$one['value'];
	$option['name']=$one['name'];
	$jenis_pasien[]=$option;
}
$sensus->setJenisPasien($jenis_pasien);
$kunjungan=array();
$kunjungan['baru']="0";
$kunjungan['lama']="1";
	
$dbtable=$presensi->getDBtable()->setView($column);
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$qv=" SELECT tanggal ";
	foreach($jenis_pasien as $one){
		$qv.=" , SUM( IF (carabayar='".$one['value']."',1,0) ) as ".$one['value']."  ";
	}	
	foreach($kunjungan as $name=>$value){
		$qv.=" , SUM( IF (barulama='".$value."',1,0) ) as ".$name."  ";
	}	
	$qv.=" , SUM( IF (ruangan LIKE '%poli%',1,0) ) as poli  ";
	$qv.=" , SUM( IF (ruangan='Pendaftaran' OR ruangan='pendaftaran',1,0) ) as rujukan  ";
	$qv.=" , SUM( IF (ruangan NOT LIKE '%poli%' AND ruangan!='Pendaftaran' AND ruangan!='pendaftaran' ,1,0) ) as rawat_inap ";
	
	$qv.=" , SUM(1) as total";
	$qv.=" FROM smis_lab_pesanan ";
	$qc="SELECT COUNT(*) FROM smis_lab_pesanan ";
	
	$dbtable->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, " tanggal<'".$_POST['sampai']."'");
	$dbtable->setPreferredQuery(true, $qv, $qc);
	$dbtable->setUseWhereforView(true);
	$dbtable->setGroupBy(true, " tanggal ");
	$dbtable->setOrder(" tanggal ASC ");
	$dbtable->setShowAll(true);
}
$btn=$presensi->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setExcelButtonEnable(true)	
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();



$presensi->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "date", "Dari", "")
		 ->addModal("sampai", "date", "Sampai", "");
$presensi->getForm(true,"Laporan Per Tanggal")
		 ->addElement("", $btn);
		 
$summary=new SummaryAdapter();
$presensi->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Tanggal", "tanggal","date d M Y")
		 ->addFixValue("Tanggal","TOTAL");

$header=array ("No.","Tanggal");

$header_top=" <th colspan='2' >Jenis Kunjungan</th> ";
foreach($kunjungan as $name=>$v){
	$header[]=$name;
	$summary->add($name, $name);
	$summary->addSummary($name,$name);
	$header_second_top.=" <th>".strtoupper($name)."</th> ";
}

$header_top.=" <th colspan='3' >Asal Pasien</th> ";
$header[]="rawat_inap";
$summary->add("rawat_inap", "rawat_inap");
$summary->addSummary("rawat_inap","rawat_inap");
$header_second_top.=" <th>Rawat Inap</th> ";
$header[]="poli";
$summary->add("poli", "poli");
$summary->addSummary("poli","poli");
$header_second_top.=" <th>Poli</th> ";
$header[]="rujukan";
$summary->add("rujukan", "rujukan");
$summary->addSummary("rujukan","rujukan");
$header_second_top.=" <th>Rujukan</th> ";


$header_top_colspan=count($jenis_pasien);
$header_top.=" <th colspan=".$header_top_colspan." >Jenis Pasien</th> ";
foreach($jenis_pasien as $one){
	$header[]=$one['value'];
	$summary->add($one['value'], $one['value']);
	$summary->addSummary($one['value'], $one['value']);
	$header_second_top.=" <th>".$one['name']."</th> ";
}


$header[]="Total";
$summary->add("Total", "total");
$summary->addSummary("Total", "total");
$summary->setRemoveZeroEnable(true);
$header_top.=" <th rowspan='2'>Total</th>";-

$header_top="<tr> <th rowspan='2'>No.</th> <th rowspan='2'>Tanggal</th> ".$header_top."</tr>";
$header_top_second="<tr>".$header_top."</tr>";


$presensi->getUItable()
		 ->setHeader($header)
		 ->setHeaderVisible(false)
		 ->addHeader("before", $header_top)
		 ->addHeader("before", $header_second_top)
		 ->setFooterVisible(false);



		 
$presensi->addViewData("dari", "dari")
		 ->addViewData("sampai", "sampai");
$presensi->initialize();
?>


