<?php
setChangeCookie ( false );
$header = array ();
$header [] = "Tanggal";
$header [] = "Nama";
$header [] = "NRM";
$header [] = "No Reg";
$header [] = "Nomor";
$header [] = "Nilai";
$header [] = "Kelas";
$header [] = "Ruangan";
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "lap_detail_hasil" );
class LabHasilAdapter extends ArrayAdapter {
	private $id_layanan;
	public function __construct($id) {
		$this->id_layanan = $id;
	}
	public function adapt($d) {
		$a = array ();
		$a ['Tanggal'] = self::format ( "date d M Y", $d->tanggal );
		$a ['Nama'] = $d->nama_pasien;
		$a ['NRM'] = self::format ( "digit6", $d->nrm_pasien );
		$a ['No Reg'] = self::format ( "digit6", $d->noreg_pasien );
		$a ['Nomor'] = $d->no_lab;
		$a ['Kelas'] = self::format ( "unslug", $d->kelas );
		$a ['Ruangan'] = self::format ( "unslug", $d->ruangan );
		$hasil = json_decode ( $d->hasil, true );
		if ($hasil != null) {
			$a ["Nilai"] = $hasil [$this->id_layanan];
		} else {
			$a ["Nilai"] = "";
		}
		return $a;
	}
}

if (isset ( $_POST ['command'] )) {
	$iddata = isset ( $_POST ['layanan'] ) ? $_POST ['layanan'] : "";
	$adapter = new LabHasilAdapter ( $iddata );
	$dbtable = new DBTable ( $db, "smis_lab_pesanan" );
	if (isset ( $_POST ['dari'] ) && isset ( $_POST ['sampai'] ) && $_POST ['dari'] != "" && $_POST ['sampai'] != "") {
		$dari = $_POST ['dari'];
		$sampai = $_POST ['sampai'];
		$layanan = $_POST ['layanan'];
		$dbtable->addCustomKriteria ( "'" . $dari . "'", "<=tanggal" );
		$dbtable->addCustomKriteria ( "'" . $sampai . "'", ">=tanggal" );
		if ($layanan != "") {
			$dbtable->addCustomKriteria ( "hasil", " NOT LIKE '%\"" . $layanan . "\":\"\"%' " );
			$dbtable->addCustomKriteria ( "hasil != ", "''" );
		}
	}
	$dbtable->setOrder ( " tanggal ASC " );
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

require_once 'laboratory/resource/LaboratoryResource.php';
$resource = new LaboratoryResource ();
$layanan = new OptionBuilder ();
$layanan->add ( "", "" );
foreach ( $resource->list_hasil as $content ) {
	$layanan->add ( ArrayAdapter::format ( "unslug", $content ), $content );
}

$uitable->addModal ( "dari", "date", "Dari", "" );
$uitable->addModal ( "sampai", "date", "Sampai", "" );
$uitable->addModal ( "ruangan", "hidden", "", "igd" );
$uitable->addModal ( "layanan", "select", "Layanan", $layanan->getContent () );

$modal = $uitable->getModal ();
$form = $modal->getForm ();

$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setIcon ( "fa fa-refresh");
$button->setAction ( "lap_detail_hasil.view()" );
$button->setClass("btn btn-primary");
$form->addElement ( "", $button );
$button = new Button ( "", "", "" );
$button->setIsButton ( Button::$ICONIC );
$button->setClass("btn btn-primary");
$button->setIcon ( "fa fa-print");
$button->setAction ( "smis_print($('#print_table_lap_detail_hasil').html())" );
$form->addElement ( "", $button );
echo $form->getHtml ();

echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>
<script type="text/javascript">
var lap_detail_hasil;
//var employee;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','nama','keterangan','slug');
	lap_detail_hasil=new TableAction("lap_detail_hasil","laboratory","lap_detail_hasil",column);
	lap_detail_hasil.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement,
				dari:$("#"+this.prefix+"_dari").val(),
				sampai:$("#"+this.prefix+"_sampai").val(),
				layanan:$("#"+this.prefix+"_layanan").val()
				};
		return reg_data;
	};
	lap_detail_hasil.view();
	
});
</script>
