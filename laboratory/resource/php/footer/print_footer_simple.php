<?php
$nama_konsultan = getSettings ( $db, "laboratory-konsultan-nama", "" );
$id_konsultan   = getSettings ( $db, "laboratory-konsultan-id", "" );
$ttd_pj         = getSettings ( $db, "laboratory-ttd_pj", "0" );
$town           = getSettings($db,"smis_autonomous_town","");
$town           = strtolower($town);
$town           = ucfirst($town);

$tp = new TablePrint ( "pfooter" );
$tp->setMaxWidth ( true );
$dd = ArrayAdapter::format ( "date d M Y", date ( "Y-m-d" ) );       
$tp ->addSpace  (3, 1)
    ->addColumn ( "&nbsp;", 3, 1,NULL,NULL,"centergap")
    ->addColumn ( "Penanggung Jawab,", 4, 1,NULL,NULL,"fcenter")
    ->commit("footer" );
        
$tp ->addSpace  (3, 1)
    ->addColumn ( "&nbsp;", 3, 1,NULL,NULL,"centergap")
    ->addColumn ( "Dokter Ahli Patologi Klinik,", 4, 1,NULL,NULL,"fcenter")
    ->commit("footer" );
        
for($x=0;$x<10;$x++){
    $tp	->addSpace  (1, 1);
}
$tp ->commit("footer" );
    
$tp	->addSpace  (10, 1)
    ->commit("footer" );
$tp	->addSpace  (10, 1)
    ->commit("footer" );

    

    
$tp ->addSpace ( 3, 1)
    ->addColumn ( "&nbsp;", 3, 1,NULL,NULL,"centergap")
    ->addColumn ( "<font id='lab_nama_konsultan'></font>", 4, 1,NULL,NULL,"fcenter")
    ->commit( "footer" );
echo "<div id='print_footer' class='hide'>" . $tp->getHtml () . "</div>";
?>
<style type="text/css">
	.fleft{ text-align:left !important; float:left !important; font-size:13px; font-weight:100;}
	.fcenter{ text-align:center; font-size:13px; font-weight:100;}
    table.pfooter{border:none !important; padding-top:10px; margin-top:10px; bottom:80px; }
	<?php
		$centergap=getSettings($db, "laboratory-centergap", "40%");
		echo ".centergap{ width:$centergap;}"; 
	?>	
	@media print { 
		.pfooter { page-break-inside:avoid; }			
	}
</style>