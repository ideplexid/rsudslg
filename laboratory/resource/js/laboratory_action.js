function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   // Directly return the joined string
   return splitStr.join(' '); 
}

function LaboratoryAction(name, page, action, column) {
    this.initialize(name, page, action, column);
    this.sudah_tutup = false;
}

LaboratoryAction.prototype.constructor = LaboratoryAction;
LaboratoryAction.prototype = new TableAction();
LaboratoryAction.prototype.chooser = function(a,b,c,d,e){
	if(c=="lab_pasien" && this.get("id")!="" && this.get("id")!="0"){
		return;
	}else{
		TableAction.prototype.chooser.call(this,a,b,c,d,e);
	}
};
LaboratoryAction.prototype.getRegulerData=function(){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:lab_polislug,
			noreg_pasien:lab_noreg,
			nama_pasien:lab_nama_pasien,
			nrm_pasien:lab_nrm_pasien,
			mode:LAB_MODE,
			jk:LAB_JK
			};

	return reg_data;
};
LaboratoryAction.prototype.selesai=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:lab_polislug,
			};			
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=1;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
	
};


LaboratoryAction.prototype.cekTutupTagihan = function(){
    var reg_data={	
        page:this.page,
        action:this.action,
        super_command:this.super_command,
        prototype_name:this.prototype_name,
        prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        polislug:lab_polislug,
        };			
    var noreg                 = $("#"+this.prefix+"_noreg_pasien").val();
    if(noreg==""){
        smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
        return;
    }
	reg_data['command']        = 'cek_tutup_tagihan';
	reg_data['noreg_pasien']  = noreg;
    
    var res = $.ajax({
        type: "POST",
        url: "",
        data:reg_data,
        async: false
    }).responseText;

    var json = getContent(res);
    if(json=="1"){
        return false;
    }else{
        return true;
    }
};

LaboratoryAction.prototype.load_lis_crawler=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:lab_polislug,
			};			
	reg_data['command']='hasil_lis_crawler';
	reg_data['id']=id;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		$("#hasil_lis_crawler_container").html(json);
		dismissLoading();			
	});
	
};

LaboratoryAction.prototype.kembalikan=function(id){
	var reg_data={	
			page:this.page,
			action:this.action,
			super_command:this.super_command,
			prototype_name:this.prototype_name,
			prototype_slug:this.prototype_slug,
			prototype_implement:this.prototype_implement,
			polislug:lab_polislug,
		};			
	reg_data['command']='save';
	reg_data['id']=id;
	reg_data['selesai']=0;
	var self=this;
	showLoading();
	$.post('',reg_data,function(res){
		var json=getContent(res);
		self.view();
		dismissLoading();					
	});
	
};

LaboratoryAction.prototype.batal=function(id){
    showLoading();
    var cek = this.cekTutupTagihan();
    if(cek){
        showLoading();
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement,
                polislug:lab_polislug,
                };			
        reg_data['command']='save';
        reg_data['id']=id;
        reg_data['selesai']=-2;
        var self=this;
        $.post('',reg_data,function(res){
            var json=getContent(res);
            self.view();
            dismissLoading();					
        });
    }
    dismissLoading();
};


LaboratoryAction.prototype.getSaveData=function(){
	var save_data=this.getRegulerData();
	save_data['command']="save";
	
	if(LAB_MODE=="arsip"){
		/* pada mode arsip usr tidak diperkenankan untuk melaklukan penyimpanan data_hasil
		 * sehingga data yang ada akan dibuat sedemikian rupa , 
		 * ketika save tidak ada data yang di post
		 * kan*/
		return save_data;
	}
    
   /* if(this.sudah_tutup){
        save_data['id']=$("#"+this.prefix+"_id").val();
    }else{ */
        for(var i=0;i<this.column.length;i++){
            var name=this.column[i];
            var typical=$("#"+this.prefix+"_"+name).attr('typical');
            var type=$("#"+this.prefix+"_"+name).attr('type');
            if(typical=="money"){
                save_data[name]=$("#"+this.prefix+"_"+name).maskMoney('unmasked')[0];
            }else if(type=="checkbox"){
                save_data[name]=$("#"+this.prefix+"_"+name).is(':checked')?1:0;
            }else{
                save_data[name]=$("#"+this.prefix+"_"+name).val();
            }
        }    
    //}

	if( ( LAB_MODE=="pendaftaran" || LAB_EDIT_HASIL=="1" ) && !this.sudah_tutup ){
		/**
		 * ketika berada pada mode daftar
		 * yang mana dipakai oleh petugas ruangan yang melakukan inputan
		 * maka hasil tidak boleh diubah, tetapi data pemesanan boleh berubah
		 * */
		var data_pesan={};
		for (var i = 0; i < lab_list_pesan.length; i++) {
			var name_list=lab_list_pesan[i];
			data_pesan[name_list]=$("#laboratory_"+name_list).is(':checked')?1:0;
		}
		save_data['periksa']=data_pesan;	
	}

	if(LAB_MODE=="pemeriksaan" || LAB_EDIT_LAYANAN=="1"){
		/**
		 * ketika berada pada mode pemeriksaan
		 * yang mana dilakukan oleh petugas laboratory
		 * maka hasil boleh berubah tetapi pemesiksaan tidak boleh berubah
		 * */
		var data_hasil={};
		for (var i = 0; i < lab_list_hasil.length; i++) {
			var name_list=lab_list_hasil[i];
			data_hasil[name_list]=$("#laboratory_"+name_list).val();
		}
        save_data['hasil']=data_hasil;
        save_data['hapusan_darah']=$('#laboratory_hapusan_darah').code();	
	}
				
	
    // layanan lain :
    if(!this.sudah_tutup){
        var data_layanan_lain = {};
        var nor = $("tbody#layanan_lain_list").children("tr").length;
        for (var i = 0; i < nor; i++) {
            var layanan_prefix = $("tbody#layanan_lain_list").children("tr").eq(i).prop("id");
            var id = $("#" + layanan_prefix + "_id").text();
            var nama = $("#" + layanan_prefix + "_nama").text();
            var harga = $("#" + layanan_prefix + "_harga").text();
            var jumlah = $("#" + layanan_prefix + "_jumlah").text();
            var d_data = {};
            d_data['id'] = id;
            d_data['nama'] = nama;
            d_data['harga'] = parseFloat(harga.replace(/[^0-9-,]/g, '').replace(",", "."));
            d_data['jumlah'] = jumlah;
            if ($("#" + layanan_prefix).attr("class") == "deleted") {
                d_data['cmd'] = "delete";
            } else if (id == "") {
                d_data['cmd'] = 'insert';
            } else {
                d_data['cmd'] = 'update';
            }
            data_layanan_lain[i] = d_data;
        }
        save_data['layanan_lain'] = JSON.stringify(data_layanan_lain);
    }
	return save_data;
};

LaboratoryAction.prototype.clear=function(){	
	for(var i=0;i<this.column.length;i++){
		
		var name=this.column[i];	
		
			if(name=="hapusan_darah"){
				$('#laboratory_hapusan_darah').code($("#default_hapusan_darah").html());
			}else if($("#"+this.prefix+"_"+name).is(':checkbox')){
				var val=$("#"+this.prefix+"_"+name).attr("dv");
				$("#"+this.prefix+"_"+name).attr('checked', val=="1");
			}else if($("#"+this.prefix+"_"+name).attr('typical')=="money"){
				var val=$("#"+this.prefix+"_"+name).attr("dv");
				$("#"+this.prefix+"_"+name).maskMoney('mask',Number(val));
			}else{
				var val=$("#"+this.prefix+"_"+name).attr("dv");
				$("#"+this.prefix+"_"+name).val(val);
				$("#"+this.prefix+"_"+name).change();
			}
	}
	this.enabledOnNotEdit(this.column_disabled_on_edit);	
	try{
		for (var i = 0; i < lab_list_hasil.length; i++) {
			var name_list=lab_list_hasil[i];
			$("#laboratory_"+name_list).val("");
			$("#laboratory_"+name_list).change();
		}
		for (var i = 0; i < lab_list_pesan.length; i++) {
			var name_list=lab_list_pesan[i];
			$("#laboratory_"+name_list).prop('checked', false);
		}	
	}catch(e){

	}
	
	//clear layanan lain:
	$("tbody#layanan_lain_list").html("");
	$("#hasil_lis_crawler_container").html("");
	layanan_lain_num = 0;
    var id_alt="#modal_alert_"+LAB_PREFIX+"_add_form";		
    $(id_alt).html("");
};

LaboratoryAction.prototype.lis=function(id){
	var data=this.getRegulerData();
	data['command']='lis_preview';
	data['id']=id;
	showLoading();
	$.post("",data,function(res){
		var json=getContent(res);
		dismissLoading();
		if(json!="" || json!=null){
			bootbox.confirm(json, function(result) {
				   if(result){
					   showLoading();
						data['command']='lis_save';
						$.post("",data,function(res){
								var json=getContent(res);
								dismissLoading();
						});
				   }
			}); 
		}
	});
	
};

LaboratoryAction.prototype.save = function(){
    showLoading();
    var self = this;
    var cek = this.cekTutupTagihan();
    this.sudah_tutup = !cek;
    if(cek){
        TableAction.prototype.save.call(this);
    }else{
        $("#"+this.prefix+"_add_form").smodal('hide');
        bootbox.confirm("Pasien ini sudah ditutup, hanya hasil lab saja yang akan disimpan !", function(result){ 
            if(result){
                TableAction.prototype.save.call(self);
            }else{
                $("#"+self.prefix+"_add_form").smodal('show');
            }
        });
    }
    dismissLoading();
};


LaboratoryAction.prototype.edit=function (id){
	var self=this;
	showLoading();	
	var edit_data=this.getEditData(id);
	this.clear();
	 /*$("#asuransi_cek").prop('checked', false);
	 $("#pasien_cek").prop('checked', false);
	 $("#rekamedis_cek").prop('checked', false);
	 $("#lab_cek").prop('checked', true);*/
	 
	$.post('',edit_data,function(res){		
		var json=getContent(res);
		if(json==null) return;
		for(var i=0;i<self.column.length;i++){
			var name=self.column[i];						
			var typical=$("#"+self.prefix+"_"+name).attr('typical');
			var type=$("#"+self.prefix+"_"+name).attr('type');
			if(name=="hapusan_darah"){
				$('#laboratory_hapusan_darah').code(json[""+name]);
			}else if(typical=="money"){
				$("#"+self.prefix+"_"+name).maskMoney('mask',Number(json[""+name]));
			}else if(type=="checkbox"){
				if(json[""+name]=="1") $("#"+self.prefix+"_"+name).prop('checked', true);
				else $("#"+self.prefix+"_"+name).prop('checked', false);
			}else{
				$("#"+self.prefix+"_"+name).val(json[""+name]);
			}
		}
		
		// layanan lain:
		layanan_lain_num = json['layanan_lain_num'];
		$("tbody#layanan_lain_list").html(json['layanan_lain_html']);
		layanan_lain.refresh_number();
        var alamat_lengkap = '';   
        if(json['kelurahan'] != null || json['kelurahan'] != '') {
            alamat_lengkap = json['kelurahan'];
        }
        if(json['kecamatan'] != null || json['kecamatan'] != '') {
            alamat_lengkap = alamat_lengkap + ' - ' + json['kecamatan'];
        }
        if(json['kabupaten'] != null || json['kabupaten'] != '') {
            alamat_lengkap = alamat_lengkap + ' - ' + json['kabupaten'];
        }
		var alamat = titleCase(json['alamat']);
        alamat_lengkap = titleCase(alamat_lengkap);
		$("#"+self.prefix+"_carabayar").val(json["carabayar"]);
		$("#ph_alamat").html(": "+alamat);
		$("#ph_nama").html(": "+json['nama_pasien']);
		$("#ph_pengirim").html(": "+json['nama_dokter']);
		$("#ph_nolab").html(": "+json['no_lab']);
		$("#ph_ibu").html(": "+json['ibu']);
		$("#ph_umur").html(": "+json['umur']);
        $("#ph_alamat_lengkap").html("  "+alamat_lengkap);
		$("#ph_jk").html(": "+json['jk']==0?": L":": P");
		$("#ph_noreg").html(": "+json['noreg_pasien']);
		$("#ph_nrm").html(": "+json['nrm_pasien']);
		$("#ph_konsultan").html(": "+json['nama_konsultan']);
		$("#ph_diagnosa").html(": "+json['diagnosa']);
        $("#ph_nrm_nolab").html(": "+json['nrm_pasien']+" - "+json['no_lab']);
        if(json['jk']=='0') {
            $("#ph_jk_umur").html(": L"+" - "+json['umur']);
            $("#ph_jk").html(": L");
            $("#phg_jk_umur").html(": L"+" - "+json['umur']);
            $("#phg_jk").html(": L");
        } else {
            $("#ph_jk_umur").html(": P"+" - "+json['umur']);
            $("#ph_jk").html(": P");
            $("#phg_jk_umur").html(": P"+" - "+json['umur']);
            $("#phg_jk").html(": P");
        }

		$("#phg_alamat").html(": "+alamat);
        $("#phg_alamat_lengkap").html("  "+alamat_lengkap);
		$("#phg_nama").html(": "+json['nama_pasien']);
		$("#phg_pengirim").html(": "+json['nama_dokter']);
		$("#phg_nolab").html(": "+json['no_lab']);
		$("#phg_ibu").html(": "+json['ibu']);
		$("#phg_umur").html(": "+json['umur']);
		$("#phg_jk").html(": "+(json['jk']==0?"L":"P"));
		$("#phg_noreg").html(": "+json['noreg_pasien']);
		$("#phg_nrm").html(": "+json['nrm_pasien']);
		$("#phg_konsultan").html(": "+json['nama_konsultan']);
		$("#phg_diagnosa").html(": "+json['diagnosa']);
        $("#phg_nrm_nolab").html(": "+json['nrm_pasien']+" - "+json['no_lab']);
		
		var kls=json['kelas'];
		kls=kls.replace(/_/g," ").toUpperCase();				
		var ruang=json['ruangan'];
		ruang=ruang.replace(/_/g," ").toUpperCase();
		$("#ph_rkls").html(": "+ruang+" - "+kls);
        $("#ph_ruangan").html(": "+ruang);
		$("#phg_rkls").html(": "+ruang+" - "+kls);
        $("#phg_ruangan").html(": "+ruang);

		//console.log(json['jk']+"-->"+": "+(json['jk']==0?": L":": P"));
		
		if(json['hasil']!=""){
			list_hasil_json=$.parseJSON(json['hasil']);
			for (var i = 0; i < lab_list_hasil.length; i++) {
				var name_list=lab_list_hasil[i];
				var value_list=list_hasil_json[name_list];
				$("#laboratory_"+name_list).val(value_list);
				$("#laboratory_"+name_list).change();
			}
		}
		if(json['periksa']!=""){
			try{
				list_periksa_json=$.parseJSON(json['periksa']);
				for (var i = 0; i < lab_list_pesan.length; i++) {
					var name_list=lab_list_pesan[i];
					var value_list=list_periksa_json[name_list];
					if(value_list=="1") $("#laboratory_"+name_list).prop('checked', true);
					else $("#laboratory_"+name_list).prop('checked', false);
				}
			}catch(e){

			}
		}
        cekList();
        
		self.load_lis_crawler(id);							
		dismissLoading();
		self.disabledOnEdit(self.column_disabled_on_edit);
		self.show_form();
        var p=$("#lab_diagnosa_anchor").parent();
        if($(p).hasClass("active")){
            $("#lab_diagnosa_anchor").trigger("click");
        }
	});
};

LaboratoryAction.prototype.printelement=function(id){
	this.edit(id);
}


LaboratoryAction.prototype.rehab_medik=function(id){
	LoadSmisPage({
        page:this.page,
        action:"rehab_medik",
        prototype_name:this.prototype_name,
		prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        id_antrian:id
    });
};

LaboratoryAction.prototype.pelayanan_khusus=function(id){
    LoadSmisPage({
        page:this.page,
        action:"pelayanan_khusus",
        prototype_name:this.prototype_name,
		prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        id_antrian:id
    });	
};

LaboratoryAction.prototype.lap_rl52=function(id){
    LoadSmisPage({
        page:this.page,
        action:"lap_rl52",
        prototype_name:this.prototype_name,
		prototype_slug:this.prototype_slug,
        prototype_implement:this.prototype_implement,
        id_antrian:id
    });	
};

