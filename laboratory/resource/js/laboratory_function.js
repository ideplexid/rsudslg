var GABUNGAN_CONTENT="";
var LAYANAN_LAIN_CONTENT="";
var CUR_GRUP="";
var CATATAN="";
//var rsuk = $("#LAB_CETAK_RSUK").val();
var auto_save_after_print = $("#LAB_AUTO_SAVE_AFTER_PRINT").val();

function cekList() {
    try{
        for (var i = 0; i < lab_list_pesan.length; i++) {
            //console.log(lab_list_pesan[i]);
            var name_list="#laboratory_"+lab_list_pesan[i];
            if($(name_list).is(':checked')) {
                for(var j = 0; j < lab_list_hasil.length; j++) {
                    var list_hasil = "#laboratory_"+lab_list_hasil[j];
                    var a = list_hasil.split(name_list);
                    if(a[0] == "") {
                        $(list_hasil).closest("tr").css({"display": ""});
                    }
                }
            } else {
                for(var j = 0; j < lab_list_hasil.length; j++) {
                    var list_hasil = "#laboratory_"+lab_list_hasil[j];
                    var a = list_hasil.split(name_list);
                    if(a[0] == "") {
                        $(list_hasil).closest("tr").css({"display": "none"});
                        $(list_hasil).val("");
                    }
                }
            }
        }
    
    }catch(e){

    }

}

function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   // Directly return the joined string
   return splitStr.join(' '); 
}

function lab_print_gabungan(){
    var id = $("#"+LAB_PREFIX+"_id").val();
    var cetak_hasil_ke = $("#"+LAB_PREFIX+"_cetak_hasil_ke").val();
    var waktu_ditangani = $("#"+LAB_PREFIX+"_waktu_ditangani_hapusan").val();
    
    if(id == null || id == "") {
        var id_alt="#modal_alert_"+LAB_PREFIX+"_add_form";		
        var warn='<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>Daftarkan Pasien Terlebih Dahulu</div>';
        $(id_alt).html(warn);
    } else {
        if((cetak_hasil_ke == 0 || cetak_hasil_ke == '0' || cetak_hasil_ke == '' || cetak_hasil_ke == null) && (waktu_ditangani == null || waktu_ditangani == '0000-00-00 00:00:00')) {
            var id_alt="#modal_alert_"+LAB_PREFIX+"_add_form";		
            var warn='<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>Darah Masuk Masih Kosong atau Tidak Valid</div>';
            $(id_alt).html(warn);
        } else {
            showLoading();
            var data={
                page:LAB_PAGE,
                action:LAB_PREFIX,
                command:"calc_time",
                prototype_name:lab_the_protoname,
                prototype_slug:lab_the_protoslug,
                prototype_implement:lab_the_protoimplement,
                id:id,
                waktu_ditangani:waktu_ditangani
            };
            $.post("",data,function(res){
                var json=getContent(res);
                console.log(json);
                $("#waktu_pemeriksaan_lab").html(": "+json.waktu_daftar);
                $("#waktu_sampel_masuk_lab").html(": "+json.waktu_ditangani);
                $("#waktu_cetak_hasil_lab").html(": "+json.waktu_selesai);
                $("#response_time_lab").html(": "+json.response_time);
                $("#cetak_hasil_ke").html(": "+json.cetak_hasil_ke);
                var sample = json.waktu_daftar;
                var jam_sample = sample.split(" ");
                var terima = json.waktu_ditangani;
                var jam_terima = terima.split(" ");
                var selesai = json.waktu_selesai;
                var jam_selesai = selesai.split(" ");
                $("#ph_tgl_periksa").html(": "+jam_sample[0]+" "+jam_sample[1]+" "+jam_sample[2]);
                $("#phg_tgl_periksa").html(": "+jam_sample[0]+" "+jam_sample[1]+" "+jam_sample[2]);
                $("#ph_sample_terima_selesai").html(": "+jam_sample[3]+" - "+jam_terima[3]+" - "+jam_selesai[3]);
                $("#phg_sample_terima_selesai").html(": "+jam_sample[3]+" - "+jam_terima[3]+" - "+jam_selesai[3]);
                dismissLoading();
                do_print_lab_gabungan();
            });
        }
    }
}

function do_print_lab_gabungan(){
	GABUNGAN_CONTENT="";
	CUR_GRUP="";
	CATATAN="";
	LabAddGabunganContent("JENIS PEMERIKSAAN","HASIL","NILAI RUJUKAN", "METODE","black_text");
	
	var pemeriksa   = $("#"+LAB_PREFIX+"_nama_petugas").val();
	$("#lab_nama_pemeriksa").html(pemeriksa);
    var konsultan   = $("#"+LAB_PREFIX+"_nama_konsultan").val();
	$("#lab_nama_konsultan").html(konsultan);
    generateContentHasil1_ext();
   var header    = "";
   var content   = "<tbody>"+GABUNGAN_CONTENT+CATATAN+LAYANAN_LAIN_CONTENT+"</tbody>";
   var footer    = "<tfoot>"+$("#print_footer table#pfooter tfoot").html()+"</tfoot>";
   var cara      = $("#"+LAB_PREFIX+"_carabayar").val();
   var p_content = "";
   
   if(cara=="Asuransi" || $("#asuransi_cek").is(":checked")){
	    $("#g_utk_rm").hide();
		$("#g_utk_dokter").hide();
		$("#g_utk_lab").hide();
		$("#g_utk_asuransi").show();		
		header      = "<thead>"+$("#print_header_gabungan table#pheader thead ").html()+"</thead>";
		p_content   = "<div class='plong'><table class='table lab_gabungan_table table_condensed' >"+header+content+footer+"</table></div>";
        p_content  += "<div class='pagebreak'> </div>";	
	}
   
   	if($("#rekamedis_cek").is(":checked")){
		$("#g_utk_rm").show();
		$("#g_utk_dokter").hide();
		$("#g_utk_asuransi").hide();
		$("#g_utk_lab").hide();
	    header      = "<thead>"+$("#print_header_gabungan table#pheader thead ").html()+"</thead>";
	    p_content   += "<div class='plong'><table class='table lab_gabungan_table table_condensed' >"+header+content+footer+"</table></div>";
        p_content   += "<div class='pagebreak'> </div>";
   	}
   
   	if($("#pasien_cek").is(":checked")){
	   	$("#g_utk_rm").hide();
		$("#g_utk_dokter").show();
		$("#g_utk_asuransi").hide();
		$("#g_utk_lab").hide();
	    header      = "<thead>"+$("#print_header_gabungan table#pheader thead ").html()+"</thead>";
		p_content   += "<div class='plong'><table class='table lab_gabungan_table table_condensed' >"+header+content+footer+"</table></div>";
        p_content   += "<div class='pagebreak'> </div>";
	}
   	
   	if($("#lab_cek").is(":checked")){
	   	$("#g_utk_rm").hide();
		$("#g_utk_dokter").hide();
		$("#g_utk_asuransi").hide();
		$("#g_utk_lab").show();
	    header       = $("#print_header_gabungan").html();
		p_content   += "<div class='plong'><table class='table lab_gabungan_table table_condensed' >"+header+content+footer+"</table></div>";
        p_content   += "<div class='pagebreak'> </div>";
	}
   	
   	if(p_content!=""){
        smis_print_force(p_content);
    }
   		
	$("#g_utk_rm").show();
	$("#g_utk_dokter").show();
	$("#g_utk_asuransi").show();
	$("#g_utk_lab").show();
	
	var id = $("#"+LAB_PREFIX+"_id").val();
	
	var data = {
			page                : LAB_PAGE,
			action              : LAB_PREFIX,
			command             : "cetak_gabung",
			cetak_gabung        : p_content,
			prototype_name      : lab_the_protoname,
			prototype_slug      : lab_the_protoslug,
			prototype_implement : lab_the_protoimplement,
			id                  : id
		};
	$.post("",data,function(res){});	
}

function lihathasil(id) {
    var data={
			page:LAB_PAGE,
			action:LAB_PREFIX,
			command:"get_data_pasien",
			prototype_name:lab_the_protoname,
			prototype_slug:lab_the_protoslug,
			prototype_implement:lab_the_protoimplement,
			id:id
		};
	$.post("",data,function(res){
        var json = JSON.parse(res);
        console.log(json['nama_petugas']);
        var alamat = titleCase(json['alamat']);
        $("#"+self.prefix+"_carabayar").val(json["carabayar"]);
        $("#ph_alamat").html(": "+alamat);
		$("#ph_nama").html(": "+json['nama_pasien']);
		$("#ph_pengirim").html(": "+json['nama_dokter']);
		$("#ph_nolab").html(": "+json['no_lab']);
		$("#ph_ibu").html(": "+json['ibu']);
		$("#ph_umur").html(": "+json['umur']);
		
		$("#ph_noreg").html(": "+json['noreg_pasien']);
		$("#ph_nrm").html(": "+json['nrm_pasien']);
		$("#ph_konsultan").html(": "+json['nama_konsultan']);
		$("#ph_diagnosa").html(": "+json['diagnosa']);
        $("#ph_nrm_nolab").html(": "+json['nrm_pasien']+" - "+json['no_lab']);
        if(json['jk']=='0') {
            $("#ph_jk_umur").html(": L"+" - "+json['umur']);
            $("#ph_jk").html(": L");
            $("#phg_jk_umur").html(": L"+" - "+json['umur']);
            $("#phg_jk").html(": L");
        } else {
            $("#ph_jk_umur").html(": P"+" - "+json['umur']);
            $("#ph_jk").html(": P");
            $("#phg_jk_umur").html(": P"+" - "+json['umur']);
            $("#phg_jk").html(": P");
        }
        $("#phg_alamat").html(": "+alamat);
		$("#phg_nama").html(": "+json['nama_pasien']);
		$("#phg_pengirim").html(": "+json['nama_dokter']);
		$("#phg_nolab").html(": "+json['no_lab']);
		$("#phg_ibu").html(": "+json['ibu']);
		$("#phg_umur").html(": "+json['umur']);
		$("#phg_noreg").html(": "+json['noreg_pasien']);
		$("#phg_nrm").html(": "+json['nrm_pasien']);
        $("#phg_konsultan").html(": "+json['nama_konsultan']);
		$("#phg_diagnosa").html(": "+json['diagnosa']);
        $("#phg_nrm_nolab").html(": "+json['nrm_pasien']+" - "+json['no_lab']);
        
        var kls=json['kelas'];
		kls=kls.replace("_"," ").toUpperCase();				
		var ruang=json['ruangan'];
		ruang=ruang.replace("_"," ").toUpperCase();
		$("#phg_rkls").html(": "+ruang+" - "+kls);
        
        GABUNGAN_CONTENT="";
        CUR_GRUP="";
        CATATAN="";
        LabAddGabunganContent("JENIS PEMERIKSAAN","HASIL","NILAI RUJUKAN", "METODE","black_text");
        
        var pemeriksa = json['nama_petugas'];
        $("#lab_nama_pemeriksa_hasil").html(pemeriksa);
        
        var konsultan = json['nama_konsultan'];;
        $("#lab_nama_konsultan").html(konsultan);
        
        if(json['hasil']!=""){
			list_hasil_json=$.parseJSON(json['hasil']);
			for (var i = 0; i < lab_list_hasil.length; i++) {
				var name_list=lab_list_hasil[i];
				var value_list=list_hasil_json[name_list];
				$("#laboratory_"+name_list).val(value_list);
				$("#laboratory_"+name_list).change();							
			}
		}
        
        generateContentHasil1_ext();
        
        var header="";
        var content="<table class='table lab_gabungan_table table_condensed' >"+GABUNGAN_CONTENT+CATATAN+"</table>";
        var footer=$("#print_footer_hasil").html();
        var cara=$("#"+LAB_PREFIX+"_carabayar").val(json['carabayar']);
        var p_content="";
        
        $("#g_utk_rm").hide();
        $("#g_utk_dokter").show();
        $("#g_utk_asuransi").hide();
        $("#g_utk_lab").hide();
        header=$("#print_header_gabungan").html();
        p_content+="<div class='plong'>"+header+content+footer+"</div>";
        p_content+="<div class='pagebreak'> </div>";
        
        if(p_content!="") {
            smis_print_force(p_content);//smis_print(p_content);
        }
    });
}

function LabAddGabunganContent(grup,nama,nilai,normal,red){
	GABUNGAN_CONTENT+="<tr>";
		GABUNGAN_CONTENT+="<td colspan='2'>"+grup+"</td>";
		GABUNGAN_CONTENT+="<td colspan='2' class='"+red+"'>"+nama+"</td>";
		GABUNGAN_CONTENT+="<td colspan='4'>"+nilai+"</td>";
		GABUNGAN_CONTENT+="<td colspan='2'>"+normal+"</td>";
	GABUNGAN_CONTENT+="</tr>";
}

function LabAddGabunganContentGrup(grup,kelas){
	GABUNGAN_CONTENT+="<tr>";
		GABUNGAN_CONTENT+="<td colspan='10' class='"+kelas+"' >"+grup+"</td>";
	GABUNGAN_CONTENT+="</tr>";
}

function LabAddGabunganContentOne(value){
	GABUNGAN_CONTENT+="<tr>";
		GABUNGAN_CONTENT+="<td colspan='10'>"+value+"</td>";
	GABUNGAN_CONTENT+="</tr>";
}


function catatan(value){
	CATATAN+="<tr>";
	CATATAN+="<td colspan='10'>"+value+"</td>";
	CATATAN+="</tr>";
}

function generateContentHasil1_ext() {
    
	for (var i = 0; i < lab_list_hasil.length; i++) {
		var name_list="laboratory_"+lab_list_hasil[i];
		var val_list=$("#"+name_list).val();
		if(name_list=="laboratory_dl_diff_count" && val_list!="") val_list=dl_diffcount();
		else if(name_list=="laboratory_h_diff_count" && val_list!="") val_list=h_diffcount();
		//console.log("#"+name_list+"  "+val_list);
		var the_class="";
        /*if(rsuk == '1') {
            var the_class="black_text";
        } else {*/
            if($("#"+name_list).hasClass("red_text")){
                $("#copy_"+name_list).addClass("red_text").removeClass("black_text");
                the_class="red_text";
            }else{
                $("#copy_"+name_list).addClass("black_text").removeClass("red_text");
                the_class="black_text";
            }	
        //}
		var the_nama=$(".entry_"+name_list).html();
		
		var the_normal="";
		if(name_list=="laboratory_Rump_Leede") the_normal=$(".nn_"+name_list+" font").html()+"";
		else the_normal=$(".nn_"+name_list).html()+"";			
		the_normal=the_normal.replace(/(\r\n|\n|\r)/gm,"  ");
		
		var the_metode="";
		the_metode=$(".metode_"+name_list).html()+"";
		the_metode=the_metode.replace(/(\r\n|\n|\r)/gm,"  ");
		
		var the_value=val_list;
        
		var the_grup=$("#"+name_list).attr("grup");
		if(the_value!=""){
			if(name_list=="laboratory_catatan"){
                //console.log(val_list);
				catatan("<strong>CATATAN : </strong></br>"+the_value);
				continue;
			}
			if(the_grup!=CUR_GRUP) {
				CUR_GRUP=the_grup;
				LabAddGabunganContentGrup("<b>"+the_grup.toUpperCase()+"</b>",the_class);
			}
			LabAddGabunganContent("&Tab;"+the_nama,the_value,the_normal,the_metode,the_class);
		}
	}
    /*if(rsuk == '1') {
        catatan("<br><br><br><strong>CATATAN:<strong>");
    }*/
    
    //Untuk Cetak Hasil Layanan Lain-Lain
    var no_layanan_lain = $("tbody#layanan_lain_list").children("tr").length;
    if(no_layanan_lain > 0) {
        headerLayananLain("NOMOR", "LAYANAN", "HARGA", "JUMLAH", "SUBTOTAL");
        for(var i = 0; i < no_layanan_lain; i++) {
            var dr_prefix = $("tbody#layanan_lain_list").children("tr").eq(i).prop("id");
            var nomor = dr_prefix + '_nomor';
            var nomor_val = $('#'+nomor).html();
            var nama = dr_prefix + '_nama';
            var nama_val = $('#'+nama).html();
            var harga = dr_prefix + '_harga';
            var harga_val = $('#'+harga).html();
            var jumlah = dr_prefix + '_jumlah';
            var jumlah_val = $('#'+jumlah).html();
            var subtotal = dr_prefix + '_subtotal';
            var subtotal_val = $('#'+subtotal).html();
            addLayananLain(nomor_val, nama_val, harga_val, jumlah_val, subtotal_val);
        }
        footerLayananLain();
    }
}

function generateContentHasil1(){	
	for (var i = 0; i < lab_list_hasil.length; i++) {
		var name_list="laboratory_"+lab_list_hasil[i];
		var val_list=$("#"+name_list).val();
		if(name_list=="laboratory_Diff_Count" && val_list!="") val_list=diffcount();
		
		
		var the_class="";
		if($("#"+name_list).hasClass("red_text")){
			$("#copy_"+name_list).addClass("red_text").removeClass("black_text");
			the_class="red_text";
		}else{
			$("#copy_"+name_list).addClass("black_text").removeClass("red_text");
			the_class="black_text";
		}		
		var the_nama=$(".entry_"+name_list).html();
		
		var the_normal="";
		if(name_list=="laboratory_Rump_Leede") the_normal=$(".nn_"+name_list+" font").html()+"";
		else the_normal=$(".nn_"+name_list).html()+"";			
		the_normal=the_normal.replace(/(\r\n|\n|\r)/gm,"  ");
		
		var the_value=val_list;		
		var the_grup=$("#"+name_list).attr("grup");
		
		
		if(the_value!=""){
			if(name_list=="laboratory_catatan"){
				catatan("<strong>CATATAN : </strong></br>"+the_value);
				continue;
			}
			if(the_grup==CUR_GRUP){ the_grup=""; }
			else CUR_GRUP=the_grup;
			LabAddGabunganContent("<strong>"+the_grup+"</strong>",the_nama,the_value,the_normal,the_class);
		}
	}	
	
	/*var hapusan_darah_tepi=$('#laboratory_hapusan_darah').code();
	if(isHapusan(hapusan_darah_tepi)){
		hapusan_darah_tepi="<strong>HAPUSAN DARAH TEPI : </strong></br><div id='hapusan_darah'>"+hapusan_darah_tepi+"</div>";
		LabAddGabunganContentOne(hapusan_darah_tepi);
	}*/
		
}


function isHapusan(html){
	var satu=$("<div>"+html+"</div>").find("table tbody tr:nth-child(2) td:nth-child(2)").html();
	if(satu!="" && satu!="-") return true;
	var dua=$("<div>"+html+"</div>").find("table tbody tr:nth-child(3) td:nth-child(2)").html();
	if(dua!="" && dua!="-") return true;
	var tiga=$("<div>"+html+"</div>").find("table tbody tr:nth-child(4) td:nth-child(2)").html();
	if(tiga!="" && tiga!="-") return true;
	var empat=$("<div>"+html+"</div>").find("table tbody tr:nth-child(5) td:nth-child(2)").html();
	if(empat!="" && empat!="-") return true;
	return false;
}

function hasil_pdf() {
    var id = $("#"+LAB_PREFIX+"_id").val();
    var waktu_ditangani = $("#"+LAB_PREFIX+"_waktu_ditangani_hapusan").val();
    var cetak_hasil_ke = $("#"+LAB_PREFIX+"_cetak_hasil_ke").val();
    
    if(id == null || id == "") {
        var id_alt="#modal_alert_"+LAB_PREFIX+"_add_form";		
        var warn='<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>Daftarkan Pasien Terlebih Dahulu</div>';
        $(id_alt).html(warn);
    } else {
        if((cetak_hasil_ke == 0 || cetak_hasil_ke == '0' || cetak_hasil_ke == '' || cetak_hasil_ke == null) && (waktu_ditangani == null || waktu_ditangani == '0000-00-00 00:00:00')) {
            var id_alt="#modal_alert_"+LAB_PREFIX+"_add_form";		
            var warn='<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4>Darah Masuk Masih Kosong atau Tidak Valid</div>';
            $(id_alt).html(warn);
        } else {
            showLoading();
            var data={
                page:LAB_PAGE,
                action:LAB_PREFIX,
                command:"calc_time",
                prototype_name:lab_the_protoname,
                prototype_slug:lab_the_protoslug,
                prototype_implement:lab_the_protoimplement,
                id:id,
                waktu_ditangani:waktu_ditangani
            };
            $.post("",data,function(res){
                var json=getContent(res);
                
                var data = LAB_ACTION.getRegulerData();
                /*if(rsuk == "1") {
                    data['action'] = "rsuk_hasil_pdf";
                } else {*/
                    data['action'] = "hasil_pdf";
                //}
                data['id'] = id;
                data['nama_pasien'] = $("#"+LAB_PREFIX+"_nama_pasien").val();
                data['ibu']         = $("#"+LAB_PREFIX+"_ibu").val();
                data['nrm']         = $("#"+LAB_PREFIX+"_nrm_pasien").val();
                data['noreg']       = $("#"+LAB_PREFIX+"_noreg_pasien").val();
                data['alamat']      = $("#"+LAB_PREFIX+"_alamat").val();
                data['umur']        = $("#"+LAB_PREFIX+"_umur").val();
                data['pengirim']    = $("#"+LAB_PREFIX+"_nama_dokter").val();
                data['konsultan']   = $("#"+LAB_PREFIX+"_nama_konsultan").val();
                data['tanggal']     = $("#"+LAB_PREFIX+"_tanggal").val();
                if($("#"+LAB_PREFIX+"_jk").val() == '0') {
                    data['jk'] = 'L';
                } else {
                    data['jk'] = 'P';
                }
                data['ruangan']     = $("#"+LAB_PREFIX+"_ruangan").val();
                data['nama_ruangan']     = $("#"+LAB_PREFIX+"_ruangan option:selected").text();
                data['kelas']       = $("#"+LAB_PREFIX+"_kelas").val();
                data['no_lab']      = $("#"+LAB_PREFIX+"_no_lab").val();
                
                var sample      = json.waktu_daftar;
                var jam_sample  = sample.split(" ");
                var terima      = json.waktu_ditangani;
                var jam_terima  = terima.split(" ");
                var selesai     = json.waktu_selesai;
                var jam_selesai = selesai.split(" ");
                $("#"+LAB_PREFIX+"_waktu_selesai").val(json.waktu_selesai_raw);
                data['sample_selesai']  = jam_sample[3]+" - "+jam_selesai[3];
                data['alamat_lengkap']  = $("#"+LAB_PREFIX+"_kelurahan").val() + " - " + $("#"+LAB_PREFIX+"_kecamatan").val() + " - " + $("#"+LAB_PREFIX+"_kabupaten").val() + " - " + $("#"+LAB_PREFIX+"_provinsi").val();
                
                if($("#asuransi_cek").is(":checked")) {
                    data['iks_cek'] = '1';
                } else {
                    data['iks_cek'] = '0';
                }
                if($("#pasien_cek").is(":checked")) {
                    data['pasien_cek'] = '1';
                } else {
                    data['pasien_cek'] = '0';
                }
                if($("#rekamedis_cek").is(":checked")) {
                    data['rekamedis_cek'] = '1';
                } else {
                    data['rekamedis_cek'] = '0';
                }
                if($("#lab_cek").is(":checked")) {
                    data['lab_cek'] = '1';
                } else {
                    data['lab_cek'] = '0';
                }
                data['diagnosa'] = $("#"+LAB_PREFIX+"_diagnosa").val();
                
                var pemeriksaan = new Array();
                var index_p = 0;
                for (var i = 0; i < lab_list_hasil.length; i++) {
                    var name_list = "laboratory_"+lab_list_hasil[i];
                    var hasil = $("#"+name_list).val();
                    var grup = $("#"+name_list).attr("grup");
                    var nama=$(".entry_"+name_list).html();
                    var metode=$(".metode_"+name_list).html();
                    var normal=$(".nn_"+name_list).html();
                    console.log(normal);
                    if($("#"+name_list).hasClass("red_text")) {
                        color = 'red';
                    } else {
                        color = 'black';
                    }
                    if(hasil != '') {
                        pemeriksaan[index_p] = grup+' $ '+nama+' $ '+hasil+' $ '+normal+' $ '+metode+' $ '+color;
                        index_p++;
                    }
                }
                data['pemeriksaan'] = pemeriksaan;
                data['layanan_lain_lain'] = '';
                //Untuk Cetak Hasil Layanan Lain-Lain
                var layanan_lain_lain = new Array();
                var no_layanan_lain = $("tbody#layanan_lain_list").children("tr").length;
                if(no_layanan_lain > 0) {
                    for(var i = 0; i < no_layanan_lain; i++) {
                        var dr_prefix = $("tbody#layanan_lain_list").children("tr").eq(i).prop("id");
                        var nomor = dr_prefix + '_nomor';
                        var nomor_val = $('#'+nomor).html();
                        var nama = dr_prefix + '_nama';
                        var nama_val = $('#'+nama).html();
                        var harga = dr_prefix + '_harga';
                        var harga_val = $('#'+harga).html();
                        var jumlah = dr_prefix + '_jumlah';
                        var jumlah_val = $('#'+jumlah).html();
                        var subtotal = dr_prefix + '_subtotal';
                        var subtotal_val = $('#'+subtotal).html();
                        layanan_lain_lain[i] = nomor_val+' $ '+nama_val+' $ '+harga_val+' $ '+jumlah_val+' $ '+subtotal_val;
                    }
                    data['layanan_lain_lain'] = layanan_lain_lain;
                }
                
                var waktu_permintaan = json.waktu_daftar;
                var waktu_sample_masuk = json.waktu_ditangani;
                var waktu_cetak_hasil = json.waktu_selesai;
                data['waktu_permintaan'] = waktu_permintaan;
                data['waktu_sample_masuk'] = waktu_sample_masuk;
                data['waktu_cetak_hasil'] = waktu_cetak_hasil;
                data['response_time'] = json.response_time;
                data['cetak_hasil_ke'] = json.cetak_hasil_ke;
                
                $.post("",data,function(res){
                    if(auto_save_after_print == 1 || auto_save_after_print == "1") {
                        LAB_ACTION.save();
                        var data = {
                                page                : LAB_PAGE,
                                action              : LAB_PREFIX,
                                command             : "save",
                                prototype_name      : lab_the_protoname,
                                prototype_slug      : lab_the_protoslug,
                                prototype_implement : lab_the_protoimplement,
                                id                  : id,
                                selesai             : 1
                            };
                        $.post("",data,function(res){});
                    }                    
                    dismissLoading();
                    json = JSON.parse(res);
                    var getUrl = window.location['pathname']+json;
                    window.open(getUrl, 'pdf');
                });
            });
        }
    }
}

function headerLayananLain(nomor,layanan,harga,jumlah,subtotal){
    LAYANAN_LAIN_CONTENT+="<tr><td class='header_layanan_lain' colspan='10'>LAYANAN LAIN-LAIN :</td></tr>";
	LAYANAN_LAIN_CONTENT+="<tr>";
		LAYANAN_LAIN_CONTENT+="<td colspan='1' class='header_pesanan_lain'>"+nomor+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='3' class='header_pesanan_lain'>"+layanan+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='2' class='header_pesanan_lain'>"+harga+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='2' class='header_pesanan_lain'>"+jumlah+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='2' class='header_pesanan_lain'>"+subtotal+"</td>";
	LAYANAN_LAIN_CONTENT+="</tr>";
}

function addLayananLain(nomor,layanan,harga,jumlah,subtotal){
	LAYANAN_LAIN_CONTENT+="<tr>";
		LAYANAN_LAIN_CONTENT+="<td colspan='1' class='pesanan_lain_center'>"+nomor+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='3' class='pesanan_lain_center'>"+layanan+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='2' class='pesanan_lain_right'>"+harga+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='2' class='pesanan_lain_center'>"+jumlah+"</td>";
		LAYANAN_LAIN_CONTENT+="<td colspan='2' class='pesanan_lain_right'>"+subtotal+"</td>";
	LAYANAN_LAIN_CONTENT+="</tr>";
}

function footerLayananLain(){
    LAYANAN_LAIN_CONTENT+="<tr><td colspan='10'><br></td></tr>";
}
	
function cetak_kwitansi(id){
	var data=LAB_ACTION.getRegulerData();
	data['command']='print-element';
	data['slug']='print-element';
	data['id']=id;
	$.post("",data,function(res){
		var json=getContent(res);
		if(json==null) return;
		smis_print(json);
	});
}

function dateFormat(d) {
    var months = new Array();
    months['01'] = 'Januari';
    months['02'] = 'Februari';
    months['03'] = 'Maret';
    months['04'] = 'April';
    months['05'] = 'Mei';
    months['06'] = 'Juni';
    months['07'] = 'Juli';
    months['08'] = 'Agustus';
    months['09'] = 'September';
    months['10'] = 'Oktober';
    months['11'] = 'November';
    months['12'] = 'Desember';
  
    var waktu = d.split(" ");
    var tanggal = waktu[0];
    var jam = waktu[1];

    tanggal = tanggal.split("-");
    var tahun = tanggal[0];
    var bulan = tanggal[1];
    var tgl = tanggal[2];

    jam = jam.split(":");
    var hour = jam[0];
    var minute = jam[1];

    return tgl+' '+months[bulan]+' '+tahun+' '+hour+':'+minute;
}