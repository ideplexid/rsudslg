/**
 * digunakan untuk menampilkan data pasien 
 * yang rawat jalan disemua unit rawat inap
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used 		: registration/resource/php/laporan/ruang_pasien_inap.php 
 * @since		: 14 Mei 2015
 * @version		: 1.0.0
 * @service		: get_bed_only
 * 
 * */
$(document).ready(function(){
	$('#search_ruang_pasien_inap').keyup(function() {
		var $rows = $('table#table_ruang_pasien_table_inap tbody tr');
		var val=$(this).val();
		$rows.show().filter(function() {
	        var this_text=$(this).text().toLowerCase();
	    	if(this_text.indexOf(val) === -1){
				$(this).hide();
			}else{
				$(this).show();
			}                
		});
	});
	
});