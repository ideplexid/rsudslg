var lab_dokter;
var lab_konsultan;
var lab_petugas;
var lab_pasien;
var lab_marketing;
var lab_pengirim;
				
lab_pasien=new TableAction("lab_pasien",LAB_PAGE,LAB_PREFIX,new Array());
lab_pasien.setSuperCommand("lab_pasien");
lab_pasien.setPrototipe(lab_the_protoname,lab_the_protoslug,lab_the_protoimplement);
lab_pasien.selected=function(json){
	$("#"+LAB_PREFIX+"_nama_pasien").val(json.nama_pasien);
	$("#"+LAB_PREFIX+"_nrm_pasien").val(json.nrm);
	$("#"+LAB_PREFIX+"_noreg_pasien").val(json.id);
	$("#"+LAB_PREFIX+"_jk").val(json.kelamin);
	$("#"+LAB_PREFIX+"_umur").val(json.umur);
	$("#"+LAB_PREFIX+"_ibu").val(json.ibu);
	$("#"+LAB_PREFIX+"_alamat").val(json.alamat_pasien);
    $("#"+LAB_PREFIX+"_kelurahan").val(json.nama_kelurahan);
	$("#"+LAB_PREFIX+"_kecamatan").val(json.nama_kecamatan);
	$("#"+LAB_PREFIX+"_kabupaten").val(json.nama_kabupaten);
	$("#"+LAB_PREFIX+"_provinsi").val(json.nama_provinsi);
	$("#"+LAB_PREFIX+"_carabayar").val(json.carabayar);
    lab_pasien.get_last_ruangan(json.id,json.jenislayanan,json.kamar_inap);
};

lab_pasien.get_last_ruangan = function(noreg,rajal,ranap){
    var cur_ruang = (ranap==null || ranap=="") ? rajal:ranap;
    var data = lab_pasien.getRegulerData();
    data['super_command'] = "";
    data['command'] = "last_position";
    data['noreg_pasien'] = noreg;
    showLoading();
    $.post("",data,function(res){
        var json = getContent(res);
        if(json!=""){
            cur_ruang = json;
        }
        $("#"+LAB_PREFIX+"_ruangan").val(cur_ruang);
        if($("#"+LAB_PREFIX+"_setting_kelas").val() == "Sesuai Kelas Pasien") {
            var data=lab_pasien.getRegulerData();
            data['action']="get_kelas_ruangan";
            data['polislug']=cur_ruang;
            $.post("",data,function(res){
                var hasil=getContent(res);
                console.log(hasil.slug_kelas+" "+cur_ruang);
                $("#"+LAB_PREFIX+"_kelas").val(hasil.slug_kelas);
            });
        }
        dismissLoading();
    });
};

lab_dokter=new TableAction("lab_dokter",LAB_PAGE,LAB_PREFIX,new Array());
lab_dokter.setSuperCommand("lab_dokter");
lab_dokter.setPrototipe(lab_the_protoname,lab_the_protoslug,lab_the_protoimplement);
lab_dokter.selected=function(json){
	$("#"+LAB_PREFIX+"_nama_dokter").val(json.nama);
	$("#"+LAB_PREFIX+"_id_dokter").val(json.id);
};

lab_petugas=new TableAction("lab_petugas",LAB_PAGE,LAB_PREFIX,new Array());
lab_petugas.setSuperCommand("lab_petugas");
lab_petugas.setPrototipe(lab_the_protoname,lab_the_protoslug,lab_the_protoimplement);
lab_petugas.selected=function(json){
	$("#"+LAB_PREFIX+"_nama_petugas").val(json.nama);
	$("#"+LAB_PREFIX+"_id_petugas").val(json.id);
};

lab_konsultan=new TableAction("lab_konsultan",LAB_PAGE,LAB_PREFIX,new Array());
lab_konsultan.setSuperCommand("lab_konsultan");
lab_konsultan.setPrototipe(lab_the_protoname,lab_the_protoslug,lab_the_protoimplement);
lab_konsultan.selected=function(json){
	$("#"+LAB_PREFIX+"_nama_konsultan").val(json.nama);
	$("#"+LAB_PREFIX+"_id_konsultan").val(json.id);
};

lab_marketing=new TableAction("lab_marketing",LAB_PAGE,LAB_PREFIX,new Array());
lab_marketing.setSuperCommand("lab_marketing");
lab_marketing.setPrototipe(lab_the_protoname,lab_the_protoslug,lab_the_protoimplement);
lab_marketing.selected=function(json){
	$("#"+LAB_PREFIX+"_marketing").val(json.nama);
	$("#"+LAB_PREFIX+"_id_marketing").val(json.id);
};

lab_pengirim=new TableAction("lab_pengirim",LAB_PAGE,LAB_PREFIX,new Array());
lab_pengirim.setSuperCommand("lab_pengirim");
lab_pengirim.setPrototipe(lab_the_protoname,lab_the_protoslug,lab_the_protoimplement);
lab_pengirim.selected=function(json){
	$("#"+LAB_PREFIX+"_pengirim").val(json.nama);
	$("#"+LAB_PREFIX+"_id_pengirim").val(json.id);
};