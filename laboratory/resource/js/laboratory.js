var lab_list_hasil          = $.parseJSON($("#LAB_LIST_HASIL").val());
var lab_list_pesan          = $.parseJSON($("#LAB_LIST_PESAN").val());
var LAB_MODE                = $("#LAB_MODE").val();
var lab_noreg               = $("#LAB_NOREG").val();
var lab_nama_pasien         = $("#LAB_NAMA").val();
var lab_nrm_pasien          = $("#LAB_NRM").val();
var lab_polislug            = $("#LAB_POLISLUG").val();
var LAB_PAGE                = $("#LAB_PAGE").val();
var LAB_PREFIX              = $("#LAB_PREFIX").val();
var lab_the_protoslug       = $("#LAB_PROTOSLUG").val();
var lab_the_protoname       = $("#LAB_PROTONAME").val();
var lab_the_protoimplement  = $("#LAB_PROTOIMPLEMENT").val();			
var layanan_lain;
var layanan_lain_num;
var LAB_JK                  = $("#LAB_JK").val();	
var LAB_EDIT_HASIL          = $("#LAB_EDIT_HASIL").val();	
var LAB_EDIT_LAYANAN        = $("#LAB_EDIT_LAYANAN").val();	
var lab_the_action          = LAB_PREFIX;
var lab_the_page            = LAB_PAGE;
var LAB_ACTION;

$(document).ready(function() {
	$('.modal').on('shown.bs.modal', function() {
		$(this).find('[autofocus]').focus();
	});
	
	/*melakukan disabled pada pelayanan , tidak bisa di edit oleh petugas lab, degan syarat settingan di aktifkan*/
	$("div#pesan_header div.div-check input[type='checkbox']").prop("disabled",(LAB_EDIT_HASIL==0));
	layanan_lain = new LayananLainAction("layanan_lain",LAB_PAGE,LAB_PREFIX,new Array("harga", "subtotal"));
	
	$("#layanan_lain_harga, #layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13)
			return;
		$(".btn").removeAttr("disabled");
		$(".btn").attr("disabled", "disabled");
		var harga = $("#layanan_lain_harga").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var jumlah = $("#layanan_lain_jumlah").val();
		if (!is_numeric(jumlah))
			jumlah = 0;
		var subtotal = parseFloat(jumlah) * parseFloat(harga);
		subtotal = "Rp. " + (parseFloat(subtotal)).formatMoney("2", ".", ",");
		$("#layanan_lain_subtotal").val(subtotal);
		$(".btn").removeAttr("disabled");
	});
	
	$("#layanan_lain_nama").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_harga").focus();
		}
	});
	
	$("#layanan_lain_harga").on("keyup", function(e) {
		if (e.which == 13) {
			$("#layanan_lain_jumlah").focus();
		}
	});
	
	$("#layanan_lain_jumlah").on("keyup", function(e) {
		if (e.which == 13) {
			$('#layanan_lain_save_btn').trigger('click');
		}
	});
	$(".mydate").datepicker();
	$('.mydatetime').datetimepicker({ minuteStep: 1});
	
	
	var column=new Array(
			"id","tanggal","ruangan","kelas",
			"nama_pasien","noreg_pasien","nrm_pasien",
            "id_marketing", "marketing", "id_pengirim", "pengirim",
			"no_lab","id_dokter","nama_dokter",
			"id_konsultan","nama_konsultan",
			"id_petugas","nama_petugas",
			"jk","hapusan_darah","umur",
			"ibu","limapuluh","alamat", "kelurahan", "kecamatan", "kabupaten", "provinsi","carabayar",
			"waktu_daftar","waktu_selesai",
			"waktu_ditangani_hapusan",
			"waktu_ditangani","uri","diagnosa", "file", "status"
		);
	LAB_ACTION=new LaboratoryAction(LAB_PREFIX,LAB_PAGE,LAB_PREFIX,column);
	LAB_ACTION.setPrototipe(lab_the_protoname,lab_the_protoslug,lab_the_protoimplement);
	LAB_ACTION.view();
    
    for(var i = 0; i < lab_list_hasil.length; i++) {
        var list_hasil = "#laboratory_"+lab_list_hasil[i];
        $(list_hasil).closest("tr").css({"display": "none"});
        $(list_hasil).val("");
    }
    
});