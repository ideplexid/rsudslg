<?php
global $db;
global $user;
$nama_konsultan = getSettings ( $db, "laboratory-konsultan-nama", "" );
$id_konsultan = getSettings ( $db, "laboratory-konsultan-id", "" );
$ttd_pj = getSettings ( $db, "laboratory-ttd_pj", "0" );

$tp = new TablePrint ( "pfooter" );
$tp->setMaxWidth ( true );
$dd = ArrayAdapter::format ( "date d M Y", date ( "Y-m-d" ) ) . " WIB";

$tp	->addColumn("", "1", "1")
	->addColumn("", "1", "1")
	->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
	->addColumn("", "1", "1")
	->commit("footer" );
    
$tp ->addColumn("", "1", "1")
    ->addColumn("", "1", "1")
    ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
    ->addColumn ( getSettings($db,"smis_autonomous_town","").", " . $dd, 1, 1,NULL,NULL,"fcenter")
	->commit( "footer" );

if($ttd_pj == "0" || $ttd_pj == 0) {
    $tp ->addColumn ( "", 1, 1,NULL,NULL,"fcenter")
        ->addColumn("", "1", "1")
        ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
        ->addColumn ( "Pemeriksa", 1, 1,NULL,NULL,"fcenter")
        ->commit("footer" );
}
if($ttd_pj == "1" || $ttd_pj == 1) {
    $tp ->addColumn ( "Penanggung Jawab", 2, 1,NULL,NULL,"fcenter")
        ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
        ->addColumn ( "Pemeriksa", 1, 1,NULL,NULL,"fcenter")
        ->commit("footer" );
}
    
$tp ->addColumn("", "1", "1")
    ->addColumn("", "1", "1")
    ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
    ->addColumn("", "1", "1")
	->commit( "footer" );
    
$tp ->addColumn("", "1", "1")
    ->addColumn("", "1", "1")
    ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
    ->addColumn("", "1", "1")
	->commit( "footer" );
    
$tp ->addColumn("", "1", "1")
    ->addColumn("", "1", "1")
    ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
    ->addColumn("", "1", "1")
	->commit( "footer" );

if($ttd_pj == "0" || $ttd_pj == 0) {
    $tp ->addColumn ( "", 1, 1,NULL,NULL,"fcenter")
        ->addColumn ( "", 1, 1,NULL,NULL,"fcenter")
        ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
        ->addColumn ( "<font id='lab_nama_pemeriksa_hasil'></font>", 1, 1,NULL,NULL,"fcenter")
        ->commit( "footer" );
}
if($ttd_pj == "1" || $ttd_pj == 1) {
    $tp ->addColumn ( $nama_konsultan, 2, 1,NULL,NULL,"fcenter")
        ->addColumn ( "&nbsp;", 1, 1,NULL,NULL,"centergap")
        ->addColumn ( "<font id='lab_nama_pemeriksa_hasil'></font>", 1, 1,NULL,NULL,"fcenter")
        ->commit( "footer" );
}


echo "<div id='print_footer_hasil' class='hide'>" . $tp->getHtml () . "</div>";
?>
<style type="text/css">
	table.pfooter{border-top:solid 2px #000; padding-top:10px; margin-top:10px;}
	.fleft{ text-align:left !important; float:left !important; font-size:13px; font-weight:100;}
	.fcenter{ text-align:center; font-size:13px; font-weight:100;}
	<?php 
		$centergap=getSettings($db, "laboratory-centergap", "40%");
		echo ".centergap{ width:$centergap;}"; 
	?>
	
	@media print { 
		.pfooter { page-break-inside:avoid; }			
	}
</style>















