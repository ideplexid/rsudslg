<?php
require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"hasil_lab",DBCreator::$ENGINE_MYISAM);
$dbcreator->addColumn("no_lab", "varchar(15)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("asal_lab", "varchar(4)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("no_registrasi", "varchar(25)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("no_rm", "varchar(10)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tgl_order", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("no_urut", "int(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("kode_test_his", "varchar(10)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("kode_test", "varchar(6)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_test", "varchar(35)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("flag", "varchar(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("hasil", "varchar(3000)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("unit", "varchar(15)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nilai_normal", "varchar(200)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("kode", "varchar(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("status", "tinyint(4)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addUniqueKey('no_lab',array('no_lab','no_registrasi','no_rm'));
$dbcreator->addUniqueKey('status',array('status'));
$dbcreator->setDuplicate(false);
$dbcreator->initialize();
?>