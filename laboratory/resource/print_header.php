<?php
/**
 * this class used for print header of Laboratory Reporting
 * this use to maintain that header always fits with all system
 * 
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @author goblooge
 * @license LGPL
 * @since 14 Nov 2014
 */

global $db;
if(getSettings ( $db, "laboratory-print-hasil-rsukaliwates" ) == '1') {
    require_once("laboratory/resource/php/header/header_2_kolom.php");
} else {
    require_once("laboratory/resource/php/header/header_3_kolom.php");
}

?>
<style type="text/css">
	.pbig{ font-size:15px !important;}

	<?php 
        if(getSettings($db, "laboratory-print-hasil-rsukaliwates") == '1') {
            echo "#pheader{ margin-left:10px; margin-bottom:15px !important;}";
        } else {
            echo "#pheader{ margin-left:10px;}";
        }
    ?>
	#header_container {	padding-bottom: 16px;}
	#header_logo {	text-align: right;}
	#header_logo img { height: 100px; width: auto; margin: 0;}
	#autonomous_title { padding-top:30px !important; font-weight: 800; color: green; text-align: center; text-transform:uppercase; vertical-align: text-center; padding-left:20px !important;}
	#autonomous_address {color: black; text-align: center; padding-left:20px !important;}
    #autonomous_contact {color: black; text-align: center; padding-left:20px !important; border-bottom:dashed 1px #7777; padding-bottom:30px !important;}
	<?php 
		$hsize=getSettings($db, "laboratory-header-size", "35");
		$shsize=getSettings($db, "laboratory-sheader-size", "25");
		echo "#autonomous_title {font-size:".$hsize."px !important;}";
		echo "#autonomous_address {font-size:".$shsize."px !important;}";
		?>
	#header_info>div>div>div:nth-child(2) { font-weight: 800; text-transform: uppercase; }
	#header_info>div>div>div { min-height: 10px; }
	#header_info>div { margin: 0px; padding: 0px;}
    <?php 
        if(getSettings($db, "laboratory-print-hasil-rsukaliwates") == '1') {
            echo ".pheader { width: 100%; margin-bottom: 5px; padding-top:85px;}";
            echo "table.pheader thead tr td:nth-child(3) {width:215px;}";
        } else {
            echo ".pheader { width: 100%; margin-bottom: 5px;}";
        }
    ?>
	
	.pheader tr td,#pheader tr th { border: none; margin: 0; padding: 0; font-size:11px;}
	
	.phttl { font-weight: 800;}
	@media print { 
		.untuk .label {font-size:13px !important;}
		.untuk .utk_asuransi {margin-left:-10px !important;}
		.untuk .utk_dokter {margin-left:-7px !important;}
		.untuk .utk_rm {margin-left:-4px !important;}
		.untuk .utk_lab {margin-left:-15px !important;}
		#untuk , #autonomous_title { -webkit-print-color-adjust: exact; }
		#autonomous_title { color: green !important; }
		#utk_rm, #g_utk_rm { color: red !important; }
		#utk_dokter,#g_utk_dokter { color: green !important; }
		#utk_asuransi,#g_utk_asuransi { color: blue !important;}
		#utk_lab,#g_utk_lab{ color: black !important;}
	}
</style>
