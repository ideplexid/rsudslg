<?php
global $PLUGINS;

$init ['name'] = 'laboratory';
$init ['path'] = SMIS_DIR . "laboratory/";
$init ['description'] = "Laboratory";
$init ['require'] = "administrator";
$init ['service'] = "";
$init ['version'] = "6.3.4";
$init ['number'] = "34";
$init ['type'] = "";
$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>
