<?php
class PesananResponder extends DBResponder {
	public function save() {
		$data = $this->postToArray ();
		$id ['id'] = $_POST ['id'];
		
		if( isset($_POST['selesai']) && $_POST['selesai']=="-2"){
			unset($data['noreg_pasien']);
			unset($data['nama_pasien']);
			unset($data['nrm_pasien']);
			unset($data['jk']);
			unset($data['kelas']);
		}
		
		$cur=$this->select($_POST ['id']);
		if($cur==null || $cur->selesai!="1"){
			if (isset ( $data ['periksa'] )) {
				$kelas = $data ['kelas'];
				$sklas = strtolower ( $kelas );
				$sklas = str_replace ( " ", "_", $sklas );
				$harga = $data ['harga'];
				$data ['harga'] = $harga;
				$periksa = json_decode ( $data ['periksa'], true );
				$biaya = 0;
				if ($harga != null) {
					$dharga = json_decode ( $harga, true );
					foreach ( $periksa as $k => $v ) {
						if ($v == "1") {
							$the_key = $sklas . "_" . $k;
							$biaya += ($dharga [$the_key] * 1);
						}
					}
				}
				$data ['biaya'] = $biaya;
			}
			
			if ($_POST ['id'] == 0 || $_POST ['id'] == "") {
				$result = $this->dbtable->insert ( $data );
				$id ['id'] = $this->dbtable->get_inserted_id ();
				$success ['type'] = 'insert';
			} else {
				$result = $this->dbtable->update ( $data, $id );
				$success ['type'] = 'update';
			}
		}
		
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		if ($result === false)
			$success ['success'] = 0;
		return $success;
	}
}

?>