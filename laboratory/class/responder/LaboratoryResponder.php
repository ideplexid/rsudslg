<?php
class LaboratoryResponder extends DBResponder {
	
	private $replace;
	private $labresource;
	private $lis_connect;
	private $tgl_lahir;
	private $jns_rawat;
	
	public function __construct($dbtable, $uitable, $adapter,LaboratoryResource $labres){
		parent::__construct($dbtable, $uitable, $adapter);
		$this->labresource=$labres;
		$this->lis_connect=getSettings($dbtable->get_db() ,"laboratory-lis-connect", "0")=="1";
	}
	
	public function setJenisRawat($jns_rawat){
		$this->jns_rawat=$jns_rawat;
	}
	
	public function setTglLahir($tgl_lahir){
		$this->tgl_lahir=$tgl_lahir;
	}
	
	public function loadLisCrawler(){
		$table=new TablePrint("hasil_lis_crawler_table");
		$table->setMaxWidth(false);
		$table->setDefaultBootrapClass(true);
		
		$table->addColumn("No.",1,1);
		$table->addColumn("Nama",1,1);
		$table->addColumn("Hasil",1,1);
		$table->addColumn("Unit",1,1);
		$table->addColumn("Nilai Normal",1,1);
		$table->commit("header");
		
		global $db;
		$id=$_POST['id'];
		$no_lab=ArrayAdapter::format("only-digit8", $id);
		$query="SELECT * FROM hasil_lab WHERE no_lab LIKE '%".$no_lab."';";
		$result=$db->get_result($query);
		$no=0;
		foreach($result as $one){
			$no++;
			$table->addColumn($no.".",1,1);
			$table->addColumn($one->nama_test,1,1);
			$table->addColumn($one->hasil."  ".$one->flag,1,1);
			$table->addColumn($one->unit,1,1);
			$table->addColumn($one->nilai_normal,1,1);
			$table->commit("body");
		}
		return $table->getHtml();
	}
	
	public function command($command){
		if($command=="last_position"){
            $pack=new ResponsePackage();
			$service = new ServiceConsumer ( $this->getDBTable(), "get_last_position",$params = array("noreg_pasien"=> $_POST['noreg_pasien']),"medical_record" );
	        $service->execute();
            $content=$service->getContent();
    
            $pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
        }else if($command=="hasil_lis_crawler"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->loadLisCrawler();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if($command=="calc_time"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->calc_time();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if($command=="calc_time_hapusan"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->calc_time_hapusan();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if($command=="lis_preview"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$content=$this->lis_preview();
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else if($command=="lis_save"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
			$this->lis_save();
			$pack->setContent("");
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(true);
			$pack->setAlertContent("Berhasil", "Penyimpanan ke LIS Berhasil");
			return $pack->getPackage();
		}else if($command=="get_data_pasien"){
			$id['id']=$_POST['id'];
            $row=$this->dbtable->select($id);
			return $row;
		}else if($command=="cetak_gabung"){
			return parent::command("save");
		}else if($command=="cek_tutup_tagihan"){
			$pack=new ResponsePackage();
			$content=NULL;
			$status='not-command';	//not-authorized, not-command, fail, success
			$alert=array();
            $result = $this->cek_tutup_tagihan();
            
            if($result=="1"){
                $pack->setAlertVisible(true);
			    $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }

			$pack->setContent($result);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			return $pack->getPackage();
		}else if($command=="del" && $this->getDeleteCapability()){
            $pack=new ResponsePackage();
			$content		 = $this->delete();
            if($content['success']==1){
                $pack->setAlertContent("Data Removed", "Your Data Had Been Removed", ResponsePackage::$TIPE_INFO);
            }else if($content['success']==-1){
                $pack->setAlertContent("Peringatan", "<strong>Pasien Telah Di Tutup Tagihanya Oleh Kasir untuk proses pembayaran</strong>, </br>silakan menghubungi kasir dahulu bila akan menambah, menubah, mengurangi dan menghapus tagihan pasien",Alert::$DANGER);
            }else{
                $pack->setAlertContent("Data Not Removed", "Your Data Not Removed", ResponsePackage::$TIPE_INFO);    
            }
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
            $pack->setAlertVisible(true);	
            return $pack->getPackage();		
		}else{
			return parent::command($command);
		}
    }
    
    public function cek_tutup_tagihan(){
        global $db;
        if(getSettings($db,"laboratory-aktifkan-tutup-tagihan","0")=="0"){
            return 0;
        }
        require_once "smis-base/smis-include-service-consumer.php";
        $serv = new ServiceConsumer($this->getDBTable()->get_db(),"cek_tutup_tagihan",NULL,"registration");
        $serv ->addData("noreg_pasien",$_POST['noreg_pasien']);
        $serv ->execute();
        $result = $serv ->getContent();
        return $result;
    }

    
    public function delete(){
        $id['id']				= $_POST['id'];
        $one = $this->dbtable->select($id);
        $_POST['noreg_pasien'] = $one->noreg_pasien;
        $cek = $this->cek_tutup_tagihan();

        if($cek=="1"){
            $success['success']	= -1;
            return $success;
        }

		if($this->dbtable->isRealDelete()){
			$result				= $this->dbtable->delete($id['id']);
		}else{
			$data['prop']		= "del";
			$result				= $this->dbtable->update($data,$id);
        }
        /**synch to accuting */
        $this-> synchronizeToAccounting($this->dbtable->get_db(), $_POST['id'] ,"del");
		$success['success']		= 1;
		if($result==='false') {
			$success['success']	= 0;
		}
		return $success;
	}
	
    
	
	public function calc_time(){
        loadLibrary("smis-libs-function-time");
		$id=array();
		$id['id']=$_POST['id'];
		$row=$this->dbtable->select($id);
		$waktu_ditangani = $_POST['waktu_ditangani']."";
        if($row->cetak_hasil_ke == '0' || $row->cetak_hasil_ke == 0 || $row->cetak_hasil_ke == NULL) {
            $waktu_sekarang = date("Y-m-d H:i:s")."";
        } else {
            $waktu_sekarang = $row->waktu_selesai."";
        }
		$response_time=minute_different($waktu_sekarang,$waktu_ditangani, "hour");
		if($response_time<0) $response_time=0;
		$update=array();
		$update['waktu_selesai']            = $waktu_sekarang;
		$update['waktu_ditangani']          = $waktu_ditangani;
		$update['waktu_ditangani_hapusan']  = $waktu_ditangani;
		$update['response_time']            = $response_time;		
		$update['cetak_hasil_ke']           = $row->cetak_hasil_ke + 1;
		$this->dbtable->update($update, $id);
		$this->saveLISDone($id['id'], $waktu_sekarang);
        $update['waktu_selesai']            = ArrayAdapter::format("date d M Y H:i", $waktu_sekarang);
		$update['waktu_daftar']             = ArrayAdapter::format("date d M Y H:i", $row->waktu_daftar);
		$update['waktu_ditangani']          = ArrayAdapter::format("date d M Y H:i", $waktu_ditangani);
        $update['response_time']            = $response_time." Menit";
        $update['waktu_selesai_raw']        = $waktu_sekarang;	
		return $update;		
	}
	
	public function calc_time_hapusan(){
        loadLibrary("smis-libs-function-time");
		$id=array();
		$id['id']=$_POST['id'];
		$row=$this->dbtable->select($id);
		$waktu_ditangani=$row->waktu_ditangani_hapusan."";
		$waktu_sekarang=date("Y-m-d H:i:s")."";
		$response_time=minute_different($waktu_sekarang,$waktu_ditangani);
		if($response_time<0) $response_time=0;
		$update=array();
		$update['waktu_selesai_hapusan']=ArrayAdapter::format("date d M Y H:i", $waktu_sekarang);
		$update['response_time_hapusan']=$response_time." Menit";
		$this->dbtable->update($update, $id);
		$update['waktu_daftar']=ArrayAdapter::format("date d M Y H:i", $row->waktu_daftar);
		$update['waktu_ditangani']=ArrayAdapter::format("date d M Y H:i", $row->waktu_ditangani_hapusan);
		return $update;
	}
    
	
	public function save() {
		$data = $this->postToArray ();
		$id ['id'] = $_POST ['id'];
		$this->replace=array();
		
		if( isset($_POST['selesai']) && $_POST['selesai']=="-2"){
			unset($data['noreg_pasien']);
			unset($data['nama_pasien']);
			unset($data['nrm_pasien']);
			unset($data['jk']);
			unset($data['kelas']);
		}
		
		$cur=$this->select($_POST ['id']);
		if($cur==null || $cur->selesai!="1"){
			if (isset ( $data ['periksa'] )) {
				$sklas = $data ['kelas'];
				//$sklas = strtolower ( $kelas );
				$sklas = str_replace ( " ", "_", $sklas );
				$harga = $data ['harga'];
				//$data ['harga'] = $harga;
				$periksa = json_decode ( $data ['periksa'], true );
				$biaya = 0;
				if ($harga != null) {
					$dharga = json_decode ( $harga, true );
					foreach ( $periksa as $k => $v ) {
						if ($v == "1") {
							$the_key = $sklas . "_" . $k;
							$lp_replace=$this->labresource->list_group[$k];//mengambil replacable
							if($lp_replace!=""){
								if(!in_array($lp_replace, $this->replace)){
									$this->replace[]=$lp_replace;
									$biaya += ($dharga [$the_key] * 1);
								}
							}else{
								$biaya += ($dharga [$the_key] * 1);
							}
						}
					}
				}
				$data ['biaya'] = $biaya;
			}
			
			if ($_POST ['id'] == 0 || $_POST ['id'] == "") {

				$nrm_pasien=$_POST['nrm_pasien'];
				$query="SELECT COUNT(*) as total FROM smis_lab_pesanan WHERE nrm_pasien='".$nrm_pasien."' AND prop!='del'";
				$total=$this->dbtable->get_db()->get_var($query);
				if($total*1>0){
					$data['barulama']="1";
				}else{
					$data['barulama']="0";
				}

				$data['waktu_daftar']=date("Y-m-d H:i:s");
                if($data['waktu_ditangani_hapusan'] != NULL || $data['waktu_ditangani_hapusan'] != "") {
                    $data['waktu_ditangani'] = $data['waktu_ditangani_hapusan'];
                } else {
                    $data['waktu_ditangani']=$this->getTimePlafon($data['waktu_daftar']);
                    $data['waktu_ditangani_hapusan']=$this->getTimePlafon($data['waktu_daftar']);
                }
				$result = $this->dbtable->insert ( $data );
				$id ['id'] = $this->dbtable->get_inserted_id ();
				$success ['type'] = 'insert';
				$this->notifInsert();
				$this->saveLIS($id ['id'], $data['waktu_daftar'], NULL);
			} else {
				if(isset($_POST['selesai']) && $_POST['selesai']=="-2"){
					$this->notifBatal($cur);
					$this->cancelLIS($id ['id']);
				}
				
				//unset($data['waktu_daftar']);
				//unset($data['waktu_selesai']);
				//unset($data['waktu_ditangani']);
                $data['waktu_ditangani'] = $data['waktu_ditangani_hapusan'];
				$result = $this->dbtable->update ( $data, $id );
				$this->saveLIS($id ['id'], NULL, NULL);
				$success ['type'] = 'update';
			}
		}
		
        // save layanan lain:
        $total_layananlain = 0;
		if (isset($_POST['layanan_lain'])) {
			$layanan_lain_dbtable = new DBTable($this->getDBTable()->get_db(), "smis_lab_dpesanan_lain");
			$id_pesanan = $id['id'];
			$layanan_lain_data = json_decode($_POST['layanan_lain'], true);
			foreach($layanan_lain_data as $lld) {
				if ($lld['cmd'] == "insert") {
					$save_data = array();
					$save_data['nama_layanan'] = $lld['nama'];
					$save_data['harga_layanan'] = $lld['harga'];
					$save_data['jumlah'] = $lld['jumlah'];
					$save_data['id_pesanan'] = $id_pesanan;
                    $layanan_lain_dbtable->insert($save_data);
                    $total_layananlain += ($lld['jumlah']*$lld['harga']);
				} else if ($lld['cmd'] == "update") {
					$update_data = array();
					$update_data['nama_layanan'] = $lld['nama'];
					$update_data['harga_layanan'] = $lld['harga'];
					$update_data['jumlah'] = $lld['jumlah'];
					$update_data['id_pesanan'] = $id_pesanan;
					$update_id = array();
					$update_id['id'] = $lld['id'];
                    $layanan_lain_dbtable->update($update_data, $update_id);
                    $total_layananlain += ($lld['jumlah']*$lld['harga']);
				} else if ($lld['cmd'] == "delete") {
					$del_data = array();
					$del_data['prop'] = "del";
					$del_id = array();
					$del_id['id'] = $lld['id'];
					$layanan_lain_dbtable->update($del_data, $del_id);
				}
			}
		}
		// end of save layanan lain
		
		$success ['id'] = $id ['id'];
		$success ['success'] = 1;
		if ($result === false)
			$success ['success'] = 0;
        $this-> synchronizeToAccounting($this->dbtable->get_db(),$success ['id'] ,"");
		return $success;
	}
	
	function edit() {
		$row = parent::edit();
		// layanan lain:
		$id_pesanan = $_POST['id'];
		$layanan_lain_rows = $this->dbtable->get_result("
			SELECT *
			FROM smis_lab_dpesanan_lain
			WHERE id_pesanan = '" . $id_pesanan . "' AND prop != 'del'
		");
		$layanan_lain_html = "";
		$layanan_lain_num = 0;
		foreach($layanan_lain_rows as $llr) {
			$layanan_lain_html .= 	"<tr id='layanan_lain_" . $layanan_lain_num . "'>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_id' style='display: none;'>" . $llr->id . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_nomor'></td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_nama'>" . $llr->nama_layanan . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_harga'>" . ArrayAdapter::format("only-money Rp. ", $llr->harga_layanan) . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_jumlah'>" . $llr->jumlah . "</td>" .
										"<td id='layanan_lain_" . $layanan_lain_num . "_subtotal'>" . ArrayAdapter::format("only-money Rp. ", $llr->harga_layanan * $llr->jumlah) . "</td>" .
										"<td>" .
											"<div class='btn-group noprint'>" .
												"<a href='#' onclick='layanan_lain.edit(" . $layanan_lain_num . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" . 
													"<i class='icon-edit icon-white'></i>" .
												"</a>" .
												"<a href='#' onclick='layanan_lain.del(" . $layanan_lain_num . ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" . 
													"<i class='icon-remove icon-white'></i>" .
												"</a>" .
											"</div>" .
										"</td>" .
									"</tr>";
			$layanan_lain_num++;
		}
		$row->layanan_lain_html = $layanan_lain_html;
		$row->layanan_lain_num = $layanan_lain_num;
		return $row;
	}
	
	public function printing() {
		$id = $_POST['id'];
		$single_row = $this->dbtable->get_row("
			SELECT *, (biaya + biaya_lain) AS 'total_biaya'
			FROM (
				SELECT a.*, SUM(CASE WHEN b.prop = 'del' OR b.jumlah IS NULL THEN 0 ELSE b.jumlah END * CASE WHEN b.prop = 'del' OR b.harga_layanan IS NULL THEN 0 ELSE b.harga_layanan END) AS 'biaya_lain'
				FROM smis_lab_pesanan a LEFT JOIN smis_lab_dpesanan_lain b ON a.id = b.id_pesanan
				WHERE a.id = '" . $id . "'
				GROUP BY a.id
			) v
		");
		$uidata = $this->adapter->getContent((array)$single_row);
		$raw = $single_row;
		$row = $uidata[0];
		$slug=$_POST['slug'];
		$print_data="";
		
		if($slug=="print-element"){
			$print_data=$this->uitable->getPrintedElement($raw,$row); 
		}else{
			$print_data=$this->uitable->getPrinted($raw,$row,$slug);
		}
		return $print_data;
	}
	
	public static function getPlafon($now){
		global $db;
		$ntime=substr($now, 11,5);
		$today=substr($now, 0,10);
		$tomorrow_timestamp = strtotime("+ 1 day");
		$besok=date("Y-m-d", $tomorrow_timestamp);
		
		$query="SELECT * FROM smis_lab_schedule WHERE b_awal<='".$ntime."' AND b_akhir>='".$ntime."' AND prop != 'del' LIMIT 0,1";
		$row=$db->get_row($query,true);
        if($row == NULL) {
            return "0000-00-00 00:00";
        }
		$jam=$row->jam;
		if($row->pindah!=0 || $row->pindah!="0"){
			return $besok." ".$jam;
		}
		return $today." ".$jam;
	}
	
	private function getTimePlafon($now){
		/*$ntime=substr($now, 10,5);
		$today=substr($now, 0,10);
		$tomorrow_timestamp = strtotime("+ 1 day");
		$besok=date("Y-m-d", $tomorrow_timestamp);
		
		$query="SELECT * FROM smis_lab_schedule WHERE b_awal<='".$ntime."' AND b_akhir>='".$ntime."' LIMIT 0,1";
		$row=$this->dbtable->get_db()->get_row($query,true);
		$jam=$row->jam;
		if($row->pindah!=0 || $row->pindah!="0"){
			return $besok." ".$jam;
		}
		return $today." ".$jam;*/
		return self::getPlafon($now);
	}
	
	private function notifInsert(){
		global $notification;
		$slug="Masuk Laboratory";
		$tgl=ArrayAdapter::format("date d M Y H:i:s", $_POST['tanggal']." ".date("H:i:s"));
		$ruang=ArrayAdapter::format("unslug", $_POST['ruangan']);
		$message="Pasien <strong>".$_POST['nama_pasien']."</strong> Masuk Laboratory Pada <strong>".$tgl."</strong> Dari <strong>".$ruang."</strong>";
		$key=md5($message.$slug);
		$notification->addNotification($slug, $key, $message,"laboratory","pemeriksaan");
	}
	
	private function saveLISDone($id,$waktu_selesai){
		if($this->lis_connect){
			require_once 'laboratory/driver/LISOrder.php';
			$lis=new LISOrder($this->dbtable->get_db(),$this->labresource);			
			$lis->setDataLab($id,NULL, $waktu_selesai);
			$lis->saveData();
		}
	}
	
	public function getTanggalLahir($noreg_pasien){
		if($this->tgl_lahir==NULL){
			$data_post = array (
					"command" => "edit",
					"id" => $noreg_pasien
			);
			$service = new ServiceConsumer ( $this->dbtable->get_db(), "get_registered", $data_post );
			$service->execute ();
			$data = $service->getContent ();
			$this->tgl_lahir=$data['tgl_lahir'];
		}
		
		return $this->tgl_lahir;
	}
	
	private function saveLIS($id,$waktu_daftar,$waktu_selesai){
		//DO LIS HERE
		if($this->lis_connect){
			require_once 'laboratory/driver/LISOrder.php';
			$lis=new LISOrder($this->dbtable->get_db(),$this->labresource);
			$nama=$_POST['nama_pasien'];
			$nrm=$_POST['nrm_pasien'];
			$noreg=$_POST['noreg_pasien'];
			$alamat=$_POST['alamat'];
			$tgl_lahir=$this->getTanggalLahir($noreg);
			$usia=$_POST['umur'];
			$jns_rawat=$this->jns_rawat=="1"?"RI":"RJ";
			$kode_ruang=$_POST['ruangan'];
			$nama_ruang=ArrayAdapter::format("unslug", $_POST['ruangan']);
			$kode_cara_bayar=$_POST['carabayar'];
			$jk=$_POST['jk'];
			$cara_bayar=ArrayAdapter::format("unslug", $_POST['carabayar']);
			$lis->setPasien($nama, $nrm, $noreg, $alamat, $tgl_lahir, $usia, $jns_rawat, $kode_ruang, $nama_ruang, $kode_cara_bayar, $cara_bayar,$jk);
			$lis->setDataLab($id,$waktu_daftar, $waktu_selesai);
			$lis->setDokterPembaca($_POST['nama_konsultan']);
			$lis->setTest($_POST['periksa']);
			$lis->setDiagnosa($_POST['diagnosa']);
			$lis->setDokterPengirim($_POST['id_dokter'], $_POST['nama_dokter']);
			$lis->saveData();
		}
	}
	
	private function cancelLIS($id){
		//DO LIS HERE
		if($this->lis_connect){
			require_once 'laboratory/driver/LISOrder.php';
			$lis=new LISOrder($this->dbtable->get_db(),$this->labresource);
			$lis->setBatal("1");
			$lis->saveData();
		}
	}
	
	private function notifBatal($curent){
		global $notification;
		$slug="Batal Masuk Laboratory";
		$ruang=ArrayAdapter::format("unslug", $curent->ruangan);
		$message="Pasien <strong>".$curent->nama_pasien."</strong> dengan Nomor <strong>".$curent->no_lab."</strong> Dari <strong>".$ruang."</strong> Dibatalkan";
		$key=md5($message.$slug.$curent->id);
		$notification->addNotification($slug, $key, $message,"laboratory","pemeriksaan");
	}
	
	private function lis_preview(){
		$id=$_POST['id'];
		$X=$this->dbtable->select($id);
		$periksa=json_decode($X->periksa);
		$result="Nama Pasien : <strong>".$X->nama_pasien."</strong></br>";
		$result.="NRM Pasien : <strong>".$X->nrm_pasien."</strong></br>";
		$result.="Noreg Pasien : <strong>".$X->noreg_pasien."</strong></br>";
		$result.="Asal Pasien : <strong>".ArrayAdapter::format("unslug", $X->ruangan)."</strong></br>";
		$result.="Nomor LAB : <strong>".$X->no_lab."</strong></br>";
		$result.="Diagnosa : <strong>".$X->diagnosa."</strong></br>";
		
		$result.=" Pemeriksaan yang Dilakukan <ul>";
		foreach($periksa as $slug=>$value){
			if($value=="1" || $value==1){
				$name=$this->labresource->list_reporting[$slug];
				$result.="<li><i>".$name['name']."</i></li>";
			}
		}
		$result.="</ul> <strong>Apakah Anda Yakin Mengirim ke LIS ??</strong> ";
		return $result;
		
	}
	
	private function lis_save(){
		require_once 'laboratory/driver/LISOrder.php';		
		$id=$_POST['id'];
		$X=$this->dbtable->select($id);
		$lis=new LISOrder($this->dbtable->get_db(),$this->labresource);
		$nama=$X->nama_pasien;
		$nrm=$X->nrm_pasien;
		$noreg=$X->noreg_pasien;
		$alamat=$X->alamat;
		$tgl_lahir=$this->getTanggalLahir($noreg);
		$usia=$X->umur;
		$jns_rawat=$this->jns_rawat=="1"?"RI":"RJ";
		$kode_ruang=$X->ruangan;
		$nama_ruang=$X->ruangan;
		$kode_cara_bayar=$X->carabayar;
		$jk=$X->jk;
		$cara_bayar=ArrayAdapter::format("unslug", $kode_cara_bayar);
		$lis->setPasien($nama, $nrm, $noreg, $alamat, $tgl_lahir, $usia, $jns_rawat, $kode_ruang, $nama_ruang, $kode_cara_bayar, $cara_bayar,$jk);
		$lis->setDataLab($id,$waktu_daftar, $waktu_selesai);
		$lis->setDokterPembaca($X->nama_konsultan);
		$lis->setTest($X->periksa);
		$lis->setDiagnosa($X->diagnosa);
		$lis->setDokterPengirim($X->id_dokter, $X->nama_dokter);
		$lis->saveData();
	}
	
	public function getModalHasil() {
		$hasil = array ();
		foreach ( $this->nama_hasil_uji as $name => $value ) {
			$hasil [$name] = $value;
		}
		return $hasil;
	}
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        $x=$this->dbtable->selectEventDel($id);
        
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penjualan";
        $data['id_data']    = $id;
        $data['entity']     = "laboratory";
        $data['service']    = "get_detail_accounting";
        $data['data']       = $id;
        $data['code']       = "lab-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu_daftar;
        $data['uraian']     = "Laboratory Pasien ".$x->nama_pasien." Pada Noreg ".$x->id;;
        $data['nilai']      = $x->biaya;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }
}

?>