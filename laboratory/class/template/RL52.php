<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once ("smis-base/smis-include-service-consumer.php");
require_once "medical_record/class/service/RuanganService.php";
class RL52 extends ModulTemplate {
	protected $db;
	protected $polislug;
	protected $page;
	protected $protoslug;
	protected $protoname;
	protected $protoimplement;
	protected $action;
	protected $dbtable;
	protected $uitable;
	protected $noreg_pasien;
	protected $nama_pasien;
	protected $nrm_pasien;
    protected $sebutan;
    protected $jk;
    protected $profile_number;
	protected $ds;
	protected $kr;
	protected $rujukan;
	protected $kelanjutan;
	protected $tanggal;
	protected $carabayar;
	
	public function __construct($db, $polislug = "all", $noreg = "", $nrm = "", $nama = "", $page = "medical_record", $action = "lap_rl52", $protoslug = "", $protoname = "", $protoimplement = "",$tanggal=NULL) {
		$this->db = $db;
		$this->tanggal=$tanggal==NULL?date("Y-m-d H:i:s"):$tanggal;
		$this->noreg_pasien = $noreg;
		$this->nama_pasien = $nama;
		$this->nrm_pasien = $nrm;
		$this->polislug = $polislug;
		$this->dbtable = new DBTable ( $this->db, "smis_lab_rl52" );
		$this->page = $page;
		$this->protoslug = $protoslug;
		$this->protoimplement = $protoimplement;
		$this->protoname = $protoname;
		$this->action = $action;

		if ($noreg != "")
			$this->dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
		
		$array=array ();
		$this->uitable = new Table ( $array, " Indikator " . ($this->polislug == "all" ? "" : ucfirst ( $this->protoname )), NULL, true );
		$this->uitable->setName ( $action );
		
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Pasien", "nama_pasien" );
		$adapter->add ( "NRM", "nrm_pasien", "digit8" );
		$adapter->add ( "No Register", "noreg_pasien", "digit8" );
		$dbres = new DBResponder ( $this->dbtable, $this->uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	public function phpPreLoad() {
		$this->uitable->addModal ( "nama_pasien", "text", "Pasien", $this->nama_pasien, "n", null, true );
        $this->uitable->addModal ( "nrm_pasien", "text", "NRM", $this->nrm_pasien, "n", null, true );
        $this->uitable->addModal ( "noreg_pasien", "text", "No Reg", $this->noreg_pasien, "n", null, true );
        $this->uitable->addModal ( "ruangan", "text", "Ruangan", $this->polislug, "n", null, true );
		
		
		$the_row = array ();
		$the_row ['rl52'] = "";
		$the_row ['tanggal'] = $this->tanggal;
        $the_row ['carabayar'] = $this->carabayar;
        
		$exist ['noreg_pasien'] = $this->noreg_pasien;
		$exist ['nama_pasien'] = $this->nama_pasien;
		$exist ['nrm_pasien'] = $this->nrm_pasien;
		$exist ['ruangan'] = $this->polislug;
		if ($this->dbtable->is_exist ( $exist )) {
			$row = $this->dbtable->select ( $exist );
			$the_row = array ();
			$the_row ['tanggal'] = $this->tanggal;
            $the_row ['carabayar'] = $this->carabayar;
            $the_row ['rl52'] = $this->rl52;
			$the_row ["id"] = $row->id;
        }
        
        global $db;
		$rows = $db->get_result("
			SELECT *
			FROM smis_mr_jenis_kegiatan_rl52
			WHERE prop = ''
			ORDER BY nama ASC
		");
		$opsi_rl52 	= new OptionBuilder();
		$opsi_rl52->add("");
		if ($rows != null) {
			foreach ($rows as $row)
				$opsi_rl52->add($row->nama,NULL,$the_row ['rl52']==$row->nama?1:0);
		}
		$opsi = $opsi_rl52->getContent();
		
		$this->uitable->addModal ( "id", "hidden", "", $the_row ['id'] );
        $this->uitable->addModal ( "tanggal", "datetime", "Tanggal", $the_row ['tanggal'],NULL,false  );
        $this->uitable->addModal ( "carabayar", "hidden", "", $the_row ['carabayar'],NULL,false  );
        $this->uitable->addModal ( "rl52", "select", "Jenis Kegiatan", $opsi,NULL,false  );
        
		$modal = $this->uitable->getModal ()->setTitle("Laporan Jenis Kegiatan");
		$modal->setTitle ( "Laporan Jenis Pelayanan" );
		echo "<div class='hide'>" . $this->uitable->getHtml () . "</div>";
        $modal->setAlwaysShow ( true );
        
        $buton = new Button("","","Back");
        $buton ->setAction("back_to_pemeriksaan()");
        $buton ->setIcon("fa fa-backward");
        $buton ->setClass("btn btn-primary");
        $buton ->setIsButton(BUtton::$ICONIC_TEXT);
        $form = $modal->joinFooterAndForm ()->setTitle("Laporan Jenis Kegiatan");
        $form ->addElement("",$buton);

		echo $form->getHtml ();
	}
	public function jsLoader() {
		echo addJS ( "framework/bootstrap/js/bootstrap-datetimepicker.js" );
		echo addJS ( "framework/smis/js/table_action.js" );
		
		loadLibrary ( "smis-libs-function-javascript" );
	}
	public function cssLoader() {
		echo addCSS ( "framework/bootstrap/css/bootstrap-datetimepicker.css" );
	}
	
	
	/* when it's star build */
	public function jsPreLoad() {
		?>
<script type="text/javascript">
		var <?php echo $this->action; ?>;
		var rl52_pasien;
		var rl52_noreg="<?php echo $this->noreg_pasien; ?>";
		var rl52_nama_pasien="<?php echo $this->nama_pasien; ?>";
		var rl52_nrm_pasien="<?php echo $this->nrm_pasien; ?>";
		var rl52_polislug="<?php echo $this->polislug; ?>";
		var rl52_the_page="<?php echo $this->page; ?>";
		var rl52_the_protoslug="<?php echo $this->protoslug; ?>";
		var rl52_the_protoname="<?php echo $this->protoname; ?>";
		var rl52_the_protoimplement="<?php echo $this->protoimplement; ?>";
		$(document).ready(function() {
			$(".mydatetime").datetimepicker({ minuteStep: 1});
			rl52_pasien=new TableAction("rl52_pasien",rl52_the_page,"<?php echo $this->action; ?>",new Array());
			rl52_pasien.setSuperCommand("rl52_pasien");
			rl52_pasien.setPrototipe(rl52_the_protoname,rl52_the_protoslug,rl52_the_protoimplement);
			rl52_pasien.selected=function(json){
				var nama=json.nama_pasien;
				var nrm=json.nrm;
				var noreg=json.id;		
				$("#<?php echo $this->action; ?>_nama_pasien").val(nama);
				$("#<?php echo $this->action; ?>_nrm_pasien").val(nrm);
				$("#<?php echo $this->action; ?>_noreg_pasien").val(noreg);
			};
			
			var column=new Array(
					"id","tanggal","rl52", "carabayar"
					);
			<?php echo $this->action; ?>=new TableAction("<?php echo $this->action; ?>",rl52_the_page,"<?php echo $this->action; ?>",column);
			<?php echo $this->action; ?>.setPrototipe(rl52_the_protoname,rl52_the_protoslug,rl52_the_protoimplement);
			<?php echo $this->action; ?>.getRegulerData=function(){
				var reg_data={	
						page:this.page,
						action:this.action,
						super_command:this.super_command,
						prototype_name:this.prototype_name,
						prototype_slug:this.prototype_slug,
						prototype_implement:this.prototype_implement,
						polislug:rl52_polislug,
						noreg_pasien:rl52_noreg,
						nama_pasien:rl52_nama_pasien,
						nrm_pasien:rl52_nrm_pasien
						};
				return reg_data;
			};

            <?php echo $this->action . ".clear=function(){return;};"; ?>
        });
        
        function back_to_pemeriksaan(){
                LoadSmisPage({
                    page:rl52_the_page,
                    action:"pemeriksaan",
                    prototype_name:"",
                    prototype_slug:"",
                    prototype_implement:""
                });
            }
		</script>
<?php
	}
}