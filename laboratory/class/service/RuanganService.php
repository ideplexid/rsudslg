<?php
class RuanganService extends ServiceConsumer {
    private $ruangan;
	public function __construct($db,$ruangan="") {
		parent::__construct ( $db, "get_urjip" );
        $this->ruangan=$ruangan;
        $this->setCached(true,"get_urjip");
	}
	public function proceedResult() {
		$ruangan = array ();
		$option = array ();
		$option ['value'] = "Pendaftaran";
		$option ['name'] = "PENDAFTARAN" ;
		$option ['default']="Pendaftaran"==$this->ruangan?"1":"0";
        $ruangan [] = $option;
        
		$content = json_decode ( $this->result, true );
		foreach ($content as $autonomous=>$ruang){
			foreach($ruang as $nama_ruang=>$jip){
                $option			 	 = array();
                $option['value'] 	 = $nama_ruang;
                if(isset($jip['name'])){
                    $option['name']	 = $jip['name'];
                }else{
                    $option['name']	 = ArrayAdapter::format("unslug", $nama_ruang);
                }
                $ruangan[]			 = $option;              
			}
		}
		return $ruangan;
	}
}