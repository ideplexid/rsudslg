<?php
require_once ("smis-base/smis-include-service-consumer.php");
class TarifLaboratory extends ServiceConsumer {
	public function __construct($db, $kelas) {
		$data = array (
				"kelas" => $kelas 
		);
		parent::__construct ( $db, "get_laboratory", $data, "manajemen" );
		$this->setMode ( ServiceConsumer::$SINGLE_MODE );
	}
}

?>