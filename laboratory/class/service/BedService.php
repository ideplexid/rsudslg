<?php 


class BedService extends ServiceConsumer{
	private $node_empty;
	private $node_used;
	private $table;
	private $dbtable;
	private $ruangan;
	public function __construct($db,$service="get_bed_only"){
		parent::__construct($db, $service);
		$this->setMode(ServiceConsumer::$MULTIPLE_MODE);
		$this->node_empty=new Tree("bed_list_empty", "Daftar Kamar Kosong", "");
		$this->node_used=new Tree("bed_list_used", "Daftar Pasien Inap", "");
		$this->table=array();
		$this->dbtable=new DBTable($db, "smis_rg_patient");
		$this->ruangan=array();
	}
	
	public function getEmptyRoom(){
		return $this->node_empty;
	}
	
	public function getUsedRoom(){
		return $this->node_used;
	}
	
	public function getList(){
		return $this->table;
	}
	
	public function getResume(){
		return $this->ruangan;
	}
	
	public function proceedResult(){
		$content=array();
		$result=parent::proceedResult();
		foreach($result as $autonomous=>$entity){
			foreach($entity as $entity_name=>$response){
				$tarif=$response['tarif'];
				$list_bed=$response['list'];
				if(count($list_bed)==0){
					continue;
				}
				$content[$entity_name]=ArrayAdapter::format("unslug", $entity_name);
				$entity_tree_empty=new Tree($entity_name, ArrayAdapter::format("unslug", $entity_name), " -- ".ArrayAdapter::format("only-money Rp.", $tarif));
				$entity_tree_empty->setIcon("fa fa-bed");
				$entity_tree_empty->setClass("badge-info badge");
				$entity_tree_used=new Tree($entity_name, ArrayAdapter::format("unslug", $entity_name), " -- ".ArrayAdapter::format("only-money Rp.", $tarif));
				$entity_tree_used->setIcon("fa fa-user");
				
				foreach ($list_bed as $bed){
					$pasien=$bed['nama_pasien']!=""?" : <a href='#'>".ArrayAdapter::format( "only-digit6", $bed['noreg_pasien'])." - ".$bed['nama_pasien']."</a>":" - <strong>".$bed['keterangan']."</strong>";
					$bed_tree=new Tree($bed['nama'], $bed['nama'],$pasien);
					
					if($bed['terpakai']=="1") 
						$entity_tree_used->addChild($bed_tree);
					else 
						$entity_tree_empty->addChild($bed_tree);
						
					if($bed['nama_pasien']!=""){
						$one=array();
						$one['nama']=$bed['nama_pasien'];
						//$one['nrm']=$bed['noreg_pasien'];
						//$one['noreg']=$bed['nrm_pasien'];
						$one['nrm']=$bed['nrm_pasien'];
						$one['noreg']=$bed['noreg_pasien'];
						$one['ruang']=$entity_name;
						
						
						$px=NULL;
						if($this->service=="get_bed_only"){
							$px=$this->dbtable->select($one['noreg']);
							$one['bed']=$bed['nama'];
						}else{
							$px=$this->dbtable->select($one['nrm']);
							$one['bed']=$bed['bed'];
						}
						
						$one['ayah']=$px->ayah;
						$one['ibu']=$px->ibu;
						$one['alamat']=$px->alamat;
						$one['rt']=$px->rt;
						$one['rw']=$px->rw;
						$one['desa']=$px->nama_kelurahan;
						$one['kecamatan']=$px->nama_kecamatan;
						$one['kabupaten']=$px->nama_kabupaten;
						$one['propinsi']=$px->propinsi;
						$one['jk']=$px->kelamin;
						if($one['noreg']*1>0){
							$one['status'].="<font class='badge badge-success'>Integrated</font>";
						}else{
							$one['status'].="<font class='badge badge-inverse'>Stand Alone</font>";
						}
						
						if(isset($this->ruangan[$one['ruang']])){
							$this->ruangan[$one['ruang']]["L"]+=($one['jk']=="0"?1:0);
							$this->ruangan[$one['ruang']]["P"]+=($one['jk']=="1"?1:0);
							$this->ruangan[$one['ruang']]["Total"]+=1;
						}else{
							$this->ruangan[$one['ruang']]['ruangan']=$one['ruang'];
							$this->ruangan[$one['ruang']]["L"]=($one['jk']=="0"?1:0);
							$this->ruangan[$one['ruang']]["P"]=($one['jk']=="1"?1:0);
							$this->ruangan[$one['ruang']]["Total"]=1;
						}
						
						
						$this->table[]=$one;
					}
				}
				
				if($entity_tree_empty->countChild()>0) $this->node_empty->addChild($entity_tree_empty);
				if($entity_tree_used->countChild()>0) $this->node_used->addChild($entity_tree_used);
				
			}
		}
		return $content;
	}


}



?>