<?php 
require_once 'laboratory/class/MapRuangan.php';
class LaboratoryAdapter extends SimpleAdapter{
	public function adapt($d){
		$a=parent::adapt($d);
        $a['Nomor']=substr($d->tanggal, 2,2).self::format("only-digit8", $d->id);
        $a['Ruangan']=MapRuangan::getRealName($d->ruangan);
		return $a;
	}	
}