<?php

/**
 * this class is used for calculating the Salary and Provit Sharing
 * of laboratory, it will be consume by the Service for 
 * Finance Purpose.
 * 
 * @since 31 October 2014
 * @version 1.0.0
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @author goblooge
 *
 */
class LaboratorySalaryAdapter extends ArrayAdapter {
	private $content;
	protected $id_karyawan;
	protected $ruangan;
	protected $tipe;
	public static $TYPE_INDIVIDU="individual";
	public static $TYPE_COMMUNAL="communal";
		
	public function __construct($id_karyawan,$tipe="individual") {
		parent::__construct ();
		$this->content = array ();
		$this->ruangan = "laboratory";
		$this->id_karyawan = $id_karyawan;
		$this->tipe=$tipe;
	}

	
	
	public function adapt($d) {
		$pembagian = json_decode ( $d ['pembagian'], true );
		$periksa = json_decode($d ['periksa'],true);
		$nilai_asli=$d['biaya'];
		$kelas=$d['kelas'];		
		$ruangan = $this->ruangan;
		$waktu = $d ['tanggal'];
		$nama = $d ['nama_pasien'];
		$nrm = $d ['nrm_pasien'];
		$ket = self::format ( "unslug", $d ['kelas'] );
		
		if ($this->is($d ['id_dokter'],$pembagian['s-dokter-pengirim'])  ) {
			$honor_persen = $pembagian ['dokter-pengirim'];
			$honor_sebagai = "Dokter Pengirim Laboratory ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_dokter'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
		
		if ($this->is($d ['id_konsultan'],$pembagian['s-dokter-konsultan']) ) {
			$honor_persen = $pembagian ['dokter-konsultan'];
			$honor_sebagai = "Dokter Konsultan Laboratory ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_konsultan'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}
		
		if ($this->is($d ['id_petugas'],$pembagian['s-petugas']) ) {
			$honor_persen = $pembagian ['petugas'];
			$honor_sebagai = "Petugas Laboratory ";
			$nilai = $honor_persen * $nilai_asli / 100;
			$karyawan=$d ['nama_petugas'];
			$this->pushContent ( $waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $karyawan, $ket );
		}	
	}
	
	/**
	 * determine if this data match the kriteria 
	 * is it individual or communal, and did the current karyawan match the kriteria
	 * @param string $id_karyawan is the karyawan id
	 * @param array $pembagian  is the adjective of current provit sharing
	 * @return boolean 
	 */
	public function is($id_karyawan,$pembagian){
		if($this->tipe==self::$TYPE_INDIVIDU && $pembagian==self::$TYPE_INDIVIDU  && ($this->id_karyawan==$id_karyawan || $this->id_karyawan =="-1"))
			return true;
		else if($this->tipe==self::$TYPE_COMMUNAL && $pembagian==self::$TYPE_COMMUNAL)
			return true;
		return false;
	}

	/**
	 * this is a format for every single salary, 
	 * this format represent from server service
	 * 
	 * @param unknown $waktu
	 * @param unknown $ruangan
	 * @param unknown $honor_sebagai
	 * @param unknown $honor_persen
	 * @param unknown $nilai
	 * @param unknown $nilai_asli
	 * @param unknown $nrm
	 * @param unknown $nama
	 * @param string $nama_karyawan
	 * @param string $ket
	 */
	protected function pushContent($waktu, $ruangan, $honor_sebagai, $honor_persen, $nilai, $nilai_asli, $nrm, $nama, $nama_karyawan="", $ket = "") {
		$array = array ();
		$array ['waktu'] = self::format ( "date d M Y", $waktu );
		$array ['ruangan'] = $ruangan;
		$array ['sebagai'] = $honor_sebagai;
		$array ['percentage'] = $honor_persen . "%";
		$array ['nilai'] = $nilai;
		$array ['asli'] = $nilai_asli;
		$array ['pasien'] = self::format ( "only-digit6", $nrm ) . " : " . $nama;
		$array ['keterangan'] = $ket;
		$array ['karyawan'] = $nama_karyawan;		
		$this->content [] = $array;
	}
	
	public function getContent($data) {
		foreach ( $data as $d ) {
			$this->adapt ( $d );
		}
		return $this->content;
	}
}

?>