<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$dbtable = new DBTable($db, "smis_lab_pesanan");
		$rows = $dbtable->get_result("
			SELECT periksa
			FROM smis_lab_pesanan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$layanan = array (
			'hemoglobin' 				=> 'Haemoglobin ',
			'antal_erythrocit' 			=> 'Antal Erythrocit',
			'antal_leocosit' 			=> 'Antal Leococyte',
			'antal_trombosit' 			=> 'Antal Trombocit',
			'bbs' 						=> 'Laju Endapan Darah (BBS)',
			'pvc_hematokrit' 			=> 'PCV/Hematocryt',
			'diff_count' 				=> 'Differentian Count',
			'waktu_pembekuan' 			=> 'Waktu Pembekuan / CT',
			'waktu_pendarahan' 			=> 'Waktu Pendarahan / BT',
			'aptt' 						=> 'APTT',
			'p_pt' 						=> 'PPT',
			'golongan_darah_abo' 		=> 'Golongan Darah ABO',
			'golongan_darah_rhesus' 	=> 'Golongan Darah Rhesus',
			'hapus_darah' 				=> 'Hapusan Darah',
			'malaria_preparat_parasit'	=> 'Malaria ',
			'dl' 						=> 'DL',
			'mcv' 						=> 'MCV',
			'mch' 						=> 'MCH',
			'mchc' 						=> 'MCHC',
			'bun' 						=> 'Blood Ureum Netrogen (BUN)',
			'serum_creatin' 			=> 'Serum Creatin (SC)',
			'uric_acid' 				=> 'Uric Acid (UA)',
			'rft' 						=> 'R.F.T.',
			'albumin_alb' 				=> 'Albumin ',
			'alkali_fosfat' 			=> 'Alkali Fosfat ',
			'bilirubin_darah_direct' 	=> '3.1 Bilirubin Direct',
			'bilirubin_darah_total' 	=> '3.2 Bilirubin Total',
			'globuline' 				=> 'Globuline',
			'sgot' 						=> 'SGOT',
			'sgpt' 						=> 'SGPT',
			'total_protein' 			=> 'Total Protein',
			'gamma_gt' 					=> 'Gamma GT',
			'lft' 						=> 'LFT',
			'hbs_ag_stik' 				=> 'HBs Ag Stik',
			'anti_hbs_stik' 			=> 'Anti HBs Stik',
			'anti_hcv_stik' 			=> 'Anti HCV Stik',
			'cholesterol_total' 		=> 'Chollesterol Total',
			'ldl_cholesterol' 			=> 'LDL Cholesterol',
			'hdl_cholesterol' 			=> 'HDL Cholesterol',
			'tri_glyseride' 			=> 'Triglyseride',
			'lemak_lengkap' 			=> 'Lemak Lengkap',
			'kga' 						=> 'Kadar Gula Darah Acak (KGA)',
			'bsn' 						=> 'Kadar Gula Darah Puasa (BSN)',
			'kgd_2_jpp' 				=> 'Kadar Gula Darah 2 JPP',
			'kga_stik' 					=> 'Kadar Gula Darah Acak (KGA) Stik',
			'hba1c' 					=> 'HBa1C',
			'faeces_lengkap' 			=> 'Faeces Lengkap',
			'benzidin_test' 			=> 'Benzidin Test',
			'protein_albumin' 			=> 'Protein / Albumin Urine',
			'bilirubine_urine' 			=> 'Bilirubine Urine',
			'urobiline'				 	=> 'Urobiline',
			'reduksi' 					=> 'Reduksi',
			'keton_aceton' 				=> 'Keton',
			'nitrit' 					=> 'Nitrit',
			'sangur_test' 				=> 'Sangur Test',
			'sediment' 					=> 'Sediment',
			'urine_lengkap' 			=> 'Urine Lengkap',
			'tes_kehamilan' 			=> 'Tes Kehamilan',
			'ampethamine' 				=> 'Ampethamine',
			'oplate' 					=> 'Opiate',
			'thc' 						=> 'THC',
			'metampethamine' 			=> 'Metampethamine',
			'benzodiazepine' 			=> 'Benzodiazepine',
			'morphine' 					=> 'Morphine',
			'paket_elektrolit' 			=> 'Paket Elektrolit (Na, K, Cl)',
			'calcium' 					=> 'Calcium',
			'phospat' 					=> 'Phospat',
			'magenesium' 				=> 'Magnesium',
			'widal_slide' 				=> 'Widal Slide',
			'rapid_dengue' 				=> 'Rapid Dangue',
			'hiv_rapid_test' 			=> 'HIV Rapid Test',
			'igm_salmonella' 			=> 'IgM Salmonella / Tubex TF',
			'print_ulang_hasil_lab' 	=> 'Print Ulang Hasil Lab ke-2 / Lembar',
			'pengambilan_dirumah' 		=> 'Pengambilan di Rumah + Jasa Transportasi',
			'bta_pagi' 					=> 'BTA Pagi',
			'bta_sewaktu_1' 			=> 'BTA Sewaktu 1',
			'bta_sewaktu_2' 			=> 'BTA Sewaktu 2',
			'throponin' 				=> 'Throponin'
		);
		$jaspel = 0;
		foreach ($rows as $row) {
			$periksa = json_decode($row->periksa, true);
			foreach ($periksa as $key => $value)
				if ($value == "1") {
					$jaspel_row = $dbtable->get_row("
						SELECT jaspel
						FROM jaspel_laborat_2016
						WHERE layanan LIKE '" . $layanan[$key] . "'
					");
					$jaspel += $jaspel_row->jaspel;
				}
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"	=> "laboratory",
			"jaspel" 	=> $jaspel
		);
		echo json_encode($data);
	}
?>