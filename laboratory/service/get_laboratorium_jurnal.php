<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		//pemeriksaan rutin:
		$dbtable = new DBTable($db, "smis_lab_pesanan");
		$row = $dbtable->get_row("
			SELECT SUM(biaya) AS 'pemeriksaan_reguler'
			FROM smis_lab_pesanan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$pemeriksaan_reguler = 0;
		if ($row != null) {
			$pemeriksaan_reguler += $row->pemeriksaan_reguler;
		}
		//pemeriksaan khusus:
		$dbtable = new DBTable($db, "smis_lab_dpesanan_lain");
		$row = $dbtable->get_row("
			SELECT SUM(a.jumlah * a.harga_layanan) AS 'pemeriksaan_cito'
			FROM smis_lab_dpesanan_lain a LEFT JOIN smis_lab_pesanan b ON a.id_pesanan = b.id
			WHERE b.noreg_pasien = '" . $noreg_pasien . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
		");
		$pemeriksaan_cito = 0;
		if ($row != null) {
			$pemeriksaan_cito += $row->pemeriksaan_cito;
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"				=> "laboratory",
			"pemeriksaan_reguler"	=> $pemeriksaan_reguler,
			"pemeriksaan_cito"		=> $pemeriksaan_cito
		);
		echo json_encode($data);
	}
?>