<?php
	global $db;
	
	if (isset($_POST['noreg_pasien'])) {
		$noreg_pasien = $_POST['noreg_pasien'];
		
		$rows = $dbtable->get_result("
			SELECT kelas, periksa, harga
			FROM smis_lab_pesanan
			WHERE noreg_pasien = '" . $noreg_pasien . "' AND prop NOT LIKE 'del'
		");
		$tindakan_jaspel = array();
		$layanan_name_arr = array (
			'hemoglobin' => 'Hemoglobin',
			'antal_erythrocit' => 'Antal Erythrocit',
			'antal_leocosit' => 'Antal Leococyte',
			'antal_trombosit' => 'Antal Trombocit',
			'bbs' => 'Laju Endapan Darah (BBS)',
			'pvc_hematokrit' => 'PCV/Hematocryt',
			'diff_count' => 'Differentian Count',
			'waktu_pembekuan' => 'Waktu Pembekuan / CT',
			'waktu_pendarahan' => 'Waktu Pendarahan / BT',
			'aptt' => 'APTT',
			'p_pt' => 'PPT',
			'golongan_darah_abo' => 'Golongan Darah ABO',
			'golongan_darah_rhesus' => 'Golongan Darah Rhesus',
			'hapus_darah' => 'Hapusan Darah',
			'malaria_preparat_parasit' => 'Malaria / Preparat Parasit',
			'dl' => 'DL',
			'mcv' => 'MCV',
			'mch' => 'MCH',
			'mchc' => 'MCHC',
			'bun' => 'Blood Ureum Nitrogen (BUN)',
			'serum_creatin' => 'Serum Creatin (SC)',
			'uric_acid' => 'Uric Acid (UA)',
			'rft' => 'R.F.T.',
			'albumin_alb' => 'Albumin (ALB)',
			'alkali_fosfat' => 'Alkali Fosfat (AP)',
			'bilirubin_darah_direct' => 'D/T - Bilirubin Direct',
			'bilirubin_darah_total' => 'D/T - Bilirubin Total',
			'globuline' => 'Globuline',
			'sgot' => 'SGOT',
			'sgpt' => 'SGPT',
			'total_protein' => 'Total Protein',
			'gamma_gt' => 'Gamma GT',
			'lft' => 'LFT',
			'hbs_ag_stik' => 'HBs Ag Stik',
			'anti_hbs_stik' => 'Anti HBs Stik',
			'anti_hcv_stik' => 'Anti HCV Stik',
			'cholesterol_total' => 'Cholesterol Total',
			'ldl_cholesterol' => 'LDL Cholesterol',
			'hdl_cholesterol' => 'HDL Cholesterol',
			'tri_glyseride' => 'Tri Glyseride',
			'lemak_lengkap' => 'Lemak Lengkap',
			'kga' => 'Kadar Gula Acak (KGA)',
			'bsn' => 'Kadar Gula Puasa (BSN)',
			'kgd_2_jpp' => 'Kadar Gula Darah 2 JPP',
			'kga_stik' => 'Kadar Gula Acak (KGA) Stik',
			'hba1c' => 'HBa1C',
			'faeces_lengkap' => 'Faeces Lengkap',
			'benzidin_test' => 'Benzidin Test',
			'protein_albumin' => 'Protein (Albumin)',
			'bilirubine_urine' => 'Bilirubine Urine',
			'urobiline' => 'Urobiline',
			'reduksi' => 'Reduksi',
			'keton_aceton' => 'Keton / Aceton',
			'nitrit' => 'Nitrit',
			'sangur_test' => 'Sangur Test',
			'sediment' => 'Sediment',
			'urine_lengkap' => 'Urine Lengkap',
			'tes_kehamilan' => 'Tes Kehamilan',
			'ampethamine' => 'Ampethamine',
			'oplate' => 'Oplate',
			'thc' => 'THC',
			'metampethamine' => 'Metampethamine',
			'benzodiazepine' => 'Benzodiazepine',
			'morphine' => 'Morphine',
			'paket_elektrolit' => 'Paket Elektrolit (Na, K, Cl)',
			'calcium' => 'Calcium',
			'phospat' => 'Phospat',
			'magenesium' => 'Magnesium',
			'widal_slide' => 'Widal Slide',
			'rapid_dengue' => 'Rapid Dengue',
			'hiv_rapid_test' => 'HIV Rapid Test',
			'igm_salmonella' => 'IgM Salmonella / Tubex TF',
			'print_ulang_hasil_lab' => 'Print Ulang Hasil / Lembar',
			'pengambilan_dirumah' => 'Pengambilan di Rumah',
			'bta_pagi' => 'BTA Pagi',
			'bta_sewaktu_1' => 'BTA Sewaktu 1',
			'bta_sewaktu_2' => 'BTA Sewaktu 2',
			'throponin' => 'Throponin'
		);
		foreach ($rows as $row) {
			$layanan_arr = json_decode($row->periksa, true);
			$harga_arr = json_decode($row->harga, true);
			foreach ($layanan_arr as $key => $value) {
				if ($value == 1) {
					$harga_layanan = $harga_arr[$row->kelas . "_" . $key];
					$nama_layanan = ArrayAdapter::format("unslug", $layanan_name_arr[$key]);
					$jaspel_row = $dbtable->get_row("
						SELECT *
						FROM jaspel_laboratorium_2016
						WHERE layanan = '" . $nama_layanan . "'
						LIMIT 0, 1
					");
					$jaspel_layanan = 0;
					if ($jaspel_row != null)
						$jaspel_layanan = $jaspel_row->jaspel;
					$d_tindakan_jaspel = array(
						"nama_tindakan"		=> $nama_layanan,
						"harga_tindakan"	=> $harga_layanan,
						"jaspel_tindakan"	=> $jaspel_tindakan
					);
					$tindakan_jaspel[] = $d_tindakan_jaspel;
				}
			}
		}
		
		$data = array();
		$data['data'] = array(
			"ruangan"			=> "laboratory",
			"tindakan_jaspel" 	=> $tindakan_jaspel
		);
		echo json_encode($data);
	}
?>