<?php 
require_once "laboratory/resource/LaboratoryResource.php";
global $db;
$id=$_POST['data'];

$dbtable= new DBTable($db,"smis_lab_pesanan");
$x          = $dbtable->selectEventDel($id);
$resource   = new LaboratoryResource();

/*transaksi untuk pasien Laboratory*/
    
$list   = array();
$periksa=json_decode($x->periksa,true);
$harga=json_decode($x->harga,true);
foreach($periksa as $slug=>$v){
    if($v=="0")
        continue;
    
    $biaya=$harga[$x->kelas."_".$slug];
    $nama=$resource->getListName($slug,$x->carabayar);
    $debet=array();
    $dk=$resource->getDebitKredit($slug);
    $debet['akun']    = $dk['d'];
    $debet['debet']   = $biaya;
    $debet['kredit']  = 0;
    $debet['ket']     = "Piutang Laboratory - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $debet['code']    = "debet-laboratory-".$x->id;
    $list[] = $debet;
     
    $kredit=array();
    $kredit['akun']    = $dk['k'];
    $kredit['debet']   = 0;
    $kredit['kredit']  = $biaya;
    $kredit['ket']     = "Pendapatan Laboratory - ".$nama." - ".$x->no_lab." pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
    $kredit['code']    = "kredit-laboratory-".$x->id;
    $list[] = $kredit;
}

//content untuk header
$header=array();
$header['tanggal']      = $x->waktu_daftar;
$header['keterangan']   = "Transaksi Laboratory ".$x->no_lab." Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Laboratory-".$x->id;
$header['nomor']        = "LAB-".$x->id;
$header['debet']        = $x->biaya;
$header['kredit']       = $x->biaya;
$header['io']           = "1";

$transaction_Laboratory=array();
$transaction_Laboratory['header']=$header;
$transaction_Laboratory['content']=$list;


/*transaksi keseluruhan*/
$transaction=array();
$transaction[]=$transaction_Laboratory;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting']=1;
$id['id']=$x->id;
$dbtable->setName("smis_lab_pesanan");
$dbtable->update($update,$id);

?>