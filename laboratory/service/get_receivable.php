<?php 
global $db;
$awal=$_POST['awal'];
$akhir=$_POST['akhir'];

$response=array();
$response['page']="0"; 													// always 0
$response['max_page']="0"; 												// always 0.
$response['data']=array();

$query="SELECT SUM(biaya) FROM smis_lab_pesanan WHERE tanggal>='$awal' AND tanggal<='$akhir' AND uri=0 AND ruangan!='Pendaftaran'";
$nilai=$db->get_var($query);											// array of string
$response['data']["0"]=array();											// array of string number 0
$response['data']["0"]["layanan"]="Laboratory";							// based on layanan sistem, resep obat, tindakan perawat, tindakan dokter dll
$response['data']["0"]["nilai"]=$nilai;									// jumlahan total per tanggal
$response['data']["0"]["urjip"]="urj";									// berisi tiga tempat URI, URJ atau Unit Penunjang

$query="SELECT SUM(biaya) FROM smis_lab_pesanan WHERE tanggal>='$awal' AND tanggal<='$akhir' AND uri=1";
$nilai=$db->get_var($query);											// array of string
$response['data']["1"]=array();											// array of string number 0
$response['data']["1"]["layanan"]="Laboratory";							// based on layanan sistem, resep obat, tindakan perawat, tindakan dokter dll
$response['data']["1"]["nilai"]=$nilai;									// jumlahan total per tanggal
$response['data']["1"]["urjip"]="uri";									// berisi tiga tempat URI, URJ atau Unit Penunjang

$query="SELECT SUM(biaya) FROM smis_lab_pesanan WHERE tanggal>='$awal' AND tanggal<='$akhir' AND uri=0 AND ruangan='Pendaftaran'";
$nilai=$db->get_var($query);											// array of string
$response['data']["2"]=array();											// array of string number 0
$response['data']["2"]["layanan"]="Laboratory";							// based on layanan sistem, resep obat, tindakan perawat, tindakan dokter dll
$response['data']["2"]["nilai"]=$nilai;									// jumlahan total per tanggal
$response['data']["2"]["urjip"]="up";									// berisi tiga tempat URI, URJ atau Unit Penunjang


echo json_encode($response);

?>