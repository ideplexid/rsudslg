<?php
global $db;
require_once "laboratory/resource/LaboratoryResource.php";
$resource = new LaboratoryResource();
$names=$resource->list_name;

if (isset ( $_POST ['noreg_pasien'] )) {
	$noreg = $_POST ['noreg_pasien'];
	$dbtable = new DBTable ( $db, "smis_lab_pesanan" );
	$dbtable->addCustomKriteria ( "noreg_pasien", "='" . $noreg . "'" );
	$dbtable->setShowAll ( true );	
	$data = $dbtable->view ( "", "0" );
	$rows = $data['data'];
	$result = array ();
	foreach ( $rows as $d ) {
		// biaya layanan lain:
		$layanan_lain_row = $dbtable->get_row("
			SELECT SUM(jumlah * harga_layanan) AS 'biaya_lain'
			FROM smis_lab_dpesanan_lain
			WHERE id_pesanan = '" . $d->id . "' AND prop NOT LIKE 'del'
			GROUP BY id_pesanan
		");
		$biaya_lain = $layanan_lain_row->biaya_lain != null ? $layanan_lain_row->biaya_lain : 0;
		
		$periksa = json_decode ( $d->periksa, true );
		$harga = json_decode ( $d->harga, true );
		$kls = str_replace ( " ", "_", $d->kelas );		
		$ket = array (
			'no' => $d->no_lab,
			'dokter' => $d->nama_dokter,
			'biaya_lain' => $biaya_lain,
			'kelas' => $d->kelas
		);
		
		foreach ( $periksa as $p => $val ) {
			if ($val == "1") {
				$ket['periksa'][$names[$p]] = $harga[$kls.'_'.$p];
			}
		}
		
		$result[] = array(
			'id' => $d->id,
			'nama' => empty($d->no_lab) ? "Laboratorium #$d->id" : "Laboratorium $d->no_lab",	
			'waktu' => ArrayAdapter::format ( "date d M Y", $d->tanggal ),
			'ruangan' => $d->ruangan,		
			'start' => $d->tanggal,
			'end' => $d->tanggal,
			'biaya' => $d->biaya + $biaya_lain,
			'jumlah' => 1,
			'keterangan' => $ket,
            'debet' => getSettings($db, "laboratory-accounting-debit-global-".$d->carabayar, ""),
            'kredit' => getSettings($db, "laboratory-accounting-kredit-global-".$d->carabayar, ""),
            
            "nama_dokter" =>$d->nama_dokter,
            "id_dokter" =>$d->id_dokter,
            "jaspel_dokter" => 0,
            "urjigd" => $d->uri==1?"URI":($d->ruangan=="igd"?"IGD":"URJ"),
            "tanggal_tagihan" =>$d->tanggal,
		);		
	}

	$query = "SELECT count(*) as total FROM smis_lab_pesanan WHERE noreg_pasien = '".$noreg."' AND `status` !='Selesai' AND prop!='del' ";
	$ct = $db->get_var($query);
	$selesai = $ct>0?"0":"1";
	echo json_encode(array(			
		'selesai' => $selesai,
		'exist' => '1',
		'reverse' => '0',
		'cara_keluar' => "Selesai",
		'data' => array(
			'laboratory' => array(
				"result" => $result,
				"jasa_pelayanan" => "1"
			)
		)
	));
}

?>
