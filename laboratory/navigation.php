<?php
	global $NAVIGATOR;
	$mr = new Menu ("fa fa-flask");
	$mr->addProperty ( 'title', 'Laboratory' );
	$mr->addProperty ( 'name', 'Laboratory' );
	$mr->addSubMenu ( "Daftar Pasien", "laboratory", "pemeriksaan", "Daftar Pasien Laboratory" ,"fa fa-binoculars");
	$mr->addSubMenu ( "Arsip", "laboratory", "arsip", "Arsip Laboratory" ,"fa fa-archive");
	$mr->addSubMenu ( "Jadwal", "laboratory", "jadwal", "Jadwal" ,"fa fa-calendar");
	$mr->addSubMenu ( "Arsip Admin", "laboratory", "arsip_admin", "Arsip Admin" ,"fa fa-archive");
	$mr->addSubMenu ( "Ruang Pasien Inap", "laboratory", "ruang_pasien_inap", "Laporan Pasien Rawat Inap" ,"fa fa-map");
	$mr->addSeparator ();
	$mr->addSubMenu ( "Layanan", "laboratory", "layanan", "Layanan Hasil" ,"fa fa-list");
	$mr->addSubMenu ( "List Hasil", "laboratory", "list_hasil", "Setting List Hasil" ,"fa fa-list-alt");
	$mr->addSubMenu ( "Laporan Hasil dan Layanan", "laboratory", "laporan", "Laporan","fa fa-line-chart" );
	$mr->addSubMenu ( "Settings", "laboratory", "settings", "Settings Laboratory" ,"fa fa-gear");
	$mr->addSeparator ();
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Laboratory", "laboratory");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'laboratory' );
?>