<?php 

class LISOrder {
	
	private $no_lab;
	private $asal_lab;
	private $no_registrasi;
	private $no_rm;
	private $tgl_order;
	private $nama_pas;
	private $jenis_kel;
	private $tgl_lahir;
	private $usia;
	private $alamat;
	private $kode_dok_kirim;
	private $nama_dok_kirim;
	private $kode_ruang;
	private $nama_ruang;
	private $kode_cara_bayar;
	private $cara_bayar;
	private $ket_klinis;
	private $test;
	private $waktu_kirim;
	private $prioritas;
	private $jns_rawat;
	private $dok_jaga;
	private $batal;
	private $status;
	private $db;
	private $labres;
	
	public function __construct($db,$labres){
		$this->labres=$labres;
		$this->db=$db;
		$this->alamat			=NULL;
		$this->asal_lab			=NULL;
		$this->cara_bayar		=NULL;
		$this->dok_jaga			=NULL;
		$this->jenis_kel		=NULL;
		$this->jns_rawat		=NULL;
		$this->ket_klinis		=NULL;
		$this->kode_cara_bayar	=NULL;
		$this->kode_dok_kirim	=NULL;
		$this->kode_ruang		=NULL;
		$this->nama_dok_kirim	=NULL;
		$this->nama_pas			=NULL;
		$this->nama_ruang		=NULL;
		$this->no_lab			=NULL;
		$this->no_registrasi	=NULL;
		$this->no_rm			=NULL;
		$this->prioritas		=NULL;
		$this->status			=NULL;
		$this->test				=NULL;
		$this->tgl_lahir		=NULL;
		$this->tgl_order		=NULL;
		$this->batal			=NULL;
		$this->usia				=NULL;
		$this->waktu_kirim		=NULL;		
	}
	
	public function setPasien($nama,$nrm,$noreg,$alamat,$tgl_lahir,$usia,$jns_rawat,$kode_ruang,$nama_ruang,$kode_cara_bayar,$cara_bayar,$jk){
		$this->nama_pas			=$nama;
		$this->no_rm			=$nrm;
		$this->no_registrasi	=$noreg;
		$this->alamat			=$alamat;
		$this->usia				=$usia;
		$this->tgl_lahir		=$tgl_lahir;
		$this->kode_cara_bayar	=$kode_cara_bayar;
		$this->cara_bayar		=$cara_bayar;
		$this->jns_rawat		=$jns_rawat;
		$this->kode_ruang		=$kode_ruang;
		$this->nama_ruang		=$nama_ruang;
		$this->jenis_kel		=$jk=="1"?"0":"1";
	}
	
	public function setTest($test){
		$resource=$this->labres->list_lis;	
		$result=array();
		foreach($test as $code=>$value){
			if($value=="1" || $value==1)
				$result[]=$resource[$code];
		}
		$hsl=implode(",", $result);//json_encode($result);
		$this->test=$hsl;
	}
	
	public function setDiagnosa($diagnosa){
		$this->ket_klinis=$diagnosa;
	}
	
	public function setDokterPengirim($id_dokter_pengirim, $nama_dokter_pengirim){
		$this->kode_dok_kirim=$id_dokter_pengirim;
		$this->nama_dok_kirim=$nama_dokter_pengirim;
	}
	
	public function setDokterPembaca($nama_dokter){
		$this->dok_jaga		=$nama_dokter;
	}
	
	public function setDataLab($id,$tgl_order,$waktu_kirim){
		$this->no_lab		=$id;
		$this->tgl_order	=($tgl_order==NULL?date("Y-m-d H:i:s"):$tgl_order);
		$this->waktu_kirim	=$waktu_kirim;
	}
	
	public function setBatal($batal){
		$this->batal=$batal;
	}
	
	public function loadData($id){
		$dbtable=new DBTable($this->db, "order_lab",array());
		$row=$dbtable->select(array("no_lab"=>$id),false,false);		
		$this->alamat			=$row->alamat;
		$this->asal_lab			=$row->asal_lab;
		$this->cara_bayar		=$row->cara_bayar;
		$this->dok_jaga			=$row->dok_jaga;
		$this->jenis_kel		=$row->jenis_kel;
		$this->jns_rawat		=$row->jns_rawat;
		$this->ket_klinis		=$row->ket_klinis;
		$this->kode_cara_bayar	=$row->kode_cara_bayar;
		$this->kode_dok_kirim	=$row->kode_dok_kirim;
		$this->kode_ruang		=$row->kode_ruang;
		$this->nama_dok_kirim	=$row->nama_dok_kirim;
		$this->nama_pas			=$row->nama_pas;
		$this->nama_ruang		=$row->nama_ruang;
		$this->no_lab			=$row->no_lab;
		$this->no_registrasi	=$row->no_registrasi;
		$this->no_rm			=$row->no_rm;
		$this->prioritas		=$row->prioritas;
		$this->status			=$row->status;
		$this->test				=$row->test;
		$this->tgl_lahir		=$row->tgl_lahir;
		$this->tgl_order		=$row->tgl_order;
		$this->batal			=$row->batal;
		$this->usia				=$row->usia;
		$this->waktu_kirim		=$row->waktu_kirim;
	}
	
	public function saveData(){
		$save=array();
		if($this->alamat!=NULL) 		$save['alamat']=$this->alamat;
		if($this->no_lab!=NULL) 		$save['no_lab']=$this->no_lab;
		if($this->asal_lab!=NULL) 		$save['asal_lab']=$this->asal_lab;
		if($this->cara_bayar!=NULL) 	$save['cara_bayar']=$this->cara_bayar;
		if($this->dok_jaga!=NULL) 		$save['dok_jaga']=$this->dok_jaga;
		if($this->jenis_kel!=NULL) 		$save['jenis_kel']=$this->jenis_kel;
		if($this->jns_rawat!=NULL) 		$save['jns_rawat']=$this->jns_rawat;
		if($this->ket_klinis!=NULL) 	$save['ket_klinis']=$this->ket_klinis;
		if($this->kode_cara_bayar!=NULL)$save['kode_cara_bayar']=$this->kode_cara_bayar;
		if($this->kode_dok_kirim!=NULL) $save['kode_dok_kirim']=$this->kode_dok_kirim;
		if($this->kode_ruang!=NULL) 	$save['kode_ruang']=$this->kode_ruang;
		if($this->nama_dok_kirim!=NULL) $save['nama_dok_kirim']=$this->nama_dok_kirim;
		if($this->nama_pas!=NULL) 		$save['nama_pas']=$this->nama_pas;
		if($this->nama_ruang!=NULL) 	$save['nama_ruang']=$this->nama_ruang;
		if($this->no_lab!=NULL) 		$save['no_lab']=$this->no_lab;
		if($this->no_registrasi!=NULL) 	$save['no_registrasi']=$this->no_registrasi;
		if($this->no_rm!=NULL) 			$save['no_rm']=$this->no_rm;
		if($this->prioritas!=NULL) 		$save['prioritas']=$this->prioritas;
		if($this->status!=NULL) 		$save['status']=$this->status;
		if($this->test!=NULL) 			$save['test']=$this->test;
		if($this->tgl_order!=NULL) 		$save['tgl_order']=$this->tgl_order;
		if($this->batal!=NULL) 			$save['batal']=$this->batal;
		if($this->usia!=NULL) 			$save['usia']=$this->usia;
		if($this->waktu_kirim!=NULL) 	$save['waktu_kirim']=$this->waktu_kirim;
		if($this->tgl_lahir!=NULL && $this->tgl_lahir!="0000-00-00") 		$save['tgl_lahir']=$this->tgl_lahir;
		$dbtable=new DBTable($this->db, "order_lab",array_keys ($save));
		$up=array("no_lab"=>$this->no_lab);
		if($dbtable->is_exist($up,true)){
			/* melakukan checking bila ternyata ada update dibagian 
			 * yang di test atau dibagian batalnya, maka statusnya kembali di set 0
			 * sehingga LIS dapat mengambil ulang data yang ada
			 * */
			
			if(isset($save['batal'])){
				$save['status']=0;
			}else if(isset($save['test']) && $save['test']!="" ){
				$this->loadData($this->no_lab);
				if($save['test']!=$this->test){
					$save['status']=0;
				}
			}
			$this->db->update("order_lab",$save,$up);
		}else{
			$dbtable->insert($save,false);
		}
	}
	
	
}

?>