<?php 

global $db;
require_once("smis-base/smis-include-service-consumer.php");

/* melakukan crawler data diagnosa pasien yang sedang dipilih ke bagian rekam medis
 * sehingga user dapat melihat data diagnosa pasien ini */
$noreg=$_POST['noreg_pasien'];
$nrm=$_POST['nrm_pasien'];

$serv=new ServiceConsumer($db,"get_diagnosa",NULL,"medical_record");
$serv->addData("noreg_pasien",$noreg);
$serv->addData("command","list");
$serv->execute();
$hasil=$serv->getContent();
$data=$hasil['data'];

$adapter=new SimpleAdapter();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add ( "Tanggal", "tanggal", "date d M Y" );
$adapter->add ( "Dokter", "nama_dokter" );
$adapter->add ( "Diagnosa", "diagnosa" );
$adapter->add ( "ICD", "kode_icd" );
$adapter->add ( "Penyakit", "nama_icd" );
$adapter->add ( "Ruangan", "ruangan","unslug" );
$content=$adapter->getContent($data);

$array=array ("No.",'Tanggal',"Dokter",'Diagnosa',"ICD","Penyakit","Ruangan" );
$uitable=new Table($array,"");
$uitable->setContent($content);
$uitable->setAction(false);
$uitable->setFooterVisible(false);
//$content=$uitable->getBodyContent();

$pack=new ResponsePackage();
$pack->setContent($uitable->getHtml());
$pack->setStatus(ResponsePackage::$STATUS_OK);
echo json_encode($pack->getPackage());



?>