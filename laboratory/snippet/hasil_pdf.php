<?php

global $db;
global $user;

$nama_konsultan = getSettings ( $db, "laboratory-konsultan-nama", "" );
$id_konsultan = getSettings ( $db, "laboratory-konsultan-id", "" );
$ttd_pj = getSettings ( $db, "laboratory-ttd_pj", "0" );
$link_ttd_pj = getSettings ( $db, "laboratory-pj-ttd-file", "" );
$file_ttd_pj = "smis-upload/".$link_ttd_pj;
$town = getSettings($db,"smis_autonomous_town","");
$town = strtolower($town);
$town = ucfirst($town);
$dd = ArrayAdapter::format ( "date d M Y", date ( "Y-m-d" ) );
$logo = getLogoNonInterlaced ();
$username = $user->getNameOnly();
//$ttd_konsultan = getSettings ( $db, "laboratory-konsultan-ttd", "" );
$ttd = "smis-upload/".$ttd_konsultan;
$font_size_title = getSettings($db, "laboratory-ukuran-font-title-cetak-pdf", "18");
$font_size_subtitle = getSettings($db, "laboratory-ukuran-font-sub-title-cetak-pdf", "12");
$font_size_header = getSettings($db, "laboratory-ukuran-font-header-cetak-pdf", "10");
$font_size_content = getSettings($db, "laboratory-ukuran-font-content-cetak-pdf", "8");

require_once("smis-libs-out/fpdf/fpdf.php");

$fpdf = new FPDF();
$fpdf->AliasNbPages();
if($_POST['iks_cek'] == '1') {
    $fpdf->AddPage("P");
    $fpdf->SetFont("Times", "B", $font_size_title);
    $fpdf->Cell(50, 0, $fpdf->Image($logo, $fpdf->GetX(), $fpdf->GetY(), 22, 22), 0, 0, "R");
    $fpdf->Ln(5);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_title"), 0, 0, "C");
    $fpdf->Ln(7);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_address"), 0, 0, "C");
    $fpdf->Ln(5);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_contact"), 0, 0, "C");
    $fpdf->Ln(15);
    $line_break = 5;
    $fpdf->SetFont("Times", "B", $font_size_header);
    $fpdf->Cell(35, 0, "Nama", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nama_pasien'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ibu Kandung", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['ibu'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "NRM", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nrm'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Noreg", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['noreg'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Alamat", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['alamat'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Umur", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['umur'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Pengirim", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['pengirim'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Konsultan", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['konsultan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "L/P", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['jk'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ruang-Kelas", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $ruang_kelas = trim($_POST['nama_ruangan']);
    $fpdf->Cell(60, 0, $ruang_kelas, 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "No. Lab", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['no_lab'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Cetakan Untuk", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, "IKS", 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Diagnosa/Ket. Klinis", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['diagnosa'], 0, 0, "L");
    $fpdf->Ln(10);
    $fpdf->SetFont("Times", "B", $font_size_content);
    $fpdf->Cell(65, 6, "JENIS PEMERIKSAAN", "TB", 0, "L");
    $fpdf->Cell(30, 6, "HASIL", "TB", 0, "L");
    $fpdf->Cell(60, 6, "NILAI RUJUKAN", "TB", 0, "L");
    $fpdf->Cell(40, 6, "METODE", "TB", 0, "L");
    $fpdf->Ln($line_break);
    $current_grup = '';
    for($i = 0; $i < sizeof($_POST['pemeriksaan']); $i++) {
        $pemeriksaan = explode(" $ ", $_POST['pemeriksaan'][$i]);
        $grup = $pemeriksaan[0];
        $jenis = $pemeriksaan[1];
        $hasil = $pemeriksaan[2];
        $normal = $pemeriksaan[3];
        $normal = htmlspecialchars_decode($normal);
        $metode = $pemeriksaan[4];
        $color = $pemeriksaan[5];
        if($current_grup == $grup) {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", "B", 0, "L");
                $fpdf->Cell(63, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", 0, 0, "L");
                $fpdf->Cell(62, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        } else {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", "B", 0, "L");
                $fpdf->Cell(62, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", 0, 0, "L");
                $fpdf->Cell(63, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        }
    }
    //Layanan Lain-Lain
    if($_POST['layanan_lain_lain'] != '' && $_POST['layanan_lain_lain'] != NULL) {
        $fpdf->Ln($line_break);
        $fpdf->SetFont("Times", "B", $font_size_content);
        $fpdf->Cell(20, 6, "LAYANAN LAIN:", 0, 0, "C");
        $fpdf->Ln($line_break);
        $fpdf->Cell(20, 6, "NOMOR", "TB", 0, "C");
        $fpdf->Cell(65, 6, "LAYANAN", "TB", 0, "C");
        $fpdf->Cell(40, 6, "HARGA", "TB", 0, "R");
        $fpdf->Cell(30, 6, "JUMLAH", "TB", 0, "C");
        $fpdf->Cell(40, 6, "SUBTOTAL", "TB", 0, "R");
        $fpdf->Ln($line_break);
        for($i = 0; $i < sizeof($_POST['layanan_lain_lain']); $i++) {
            $layanan_lain = explode(" $ ", $_POST['layanan_lain_lain'][$i]);
            $nomor = $layanan_lain[0];
            $layanan = $layanan_lain[1];
            $harga = $layanan_lain[2];
            $jumlah = $layanan_lain[3];
            $subtotal = $layanan_lain[4];
            $fpdf->SetFont("Times", "", $font_size_content);
            if($i+1 == sizeof($_POST['layanan_lain_lain'])) {
                $fpdf->Cell(20, 6, $nomor, "B", 0, "C");
                $fpdf->Cell(65, 6, $layanan, "B", 0, "C");
                $fpdf->Cell(40, 6, $harga, "B", 0, "R");
                $fpdf->Cell(30, 6, $jumlah, "B", 0, "C");
                $fpdf->Cell(40, 6, $subtotal, "B", 0, "R");
            } else {
                $fpdf->Cell(20, 6, $nomor, 0, 0, "C");
                $fpdf->Cell(65, 6, $layanan, 0, 0, "C");
                $fpdf->Cell(40, 6, $harga, 0, 0, "R");
                $fpdf->Cell(30, 6, $jumlah, 0, 0, "C");
                $fpdf->Cell(40, 6, $subtotal, 0, 0, "R");
            }
            $fpdf->Ln($line_break);
        }
        $fpdf->Ln($line_break);
    }
    $fpdf->Ln($line_break);
    $fpdf->SetFont("Times", "", $font_size_content);
    $fpdf->Cell(23, 6, "Permintaan", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_permintaan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Sample Masuk", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_sample_masuk'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetak Hasil", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_cetak_hasil'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Response Time", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['response_time'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetakan ke", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['cetak_hasil_ke'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Ln($line_break);
    $tempat_waktu = $town.", " . $dd;
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, $tempat_waktu, 0, 0, "C");
    $fpdf->Ln($line_break);
    if($ttd_pj == "1" || $ttd_pj == 1) {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "Penanggung Jawab", 0, 0, "C");
        if($link_ttd_pj != "") {
            $fpdf->Ln(5);
            $fpdf->Cell(65, 6, $fpdf->Image($file_ttd_pj, 163, $fpdf->GetY(), 20, 20), 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Ln(20);
        } else {
            $fpdf->Ln(20);
        }
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, $nama_konsultan, 0, 0, "C");
    } else {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Ln(20);
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
    }
}
if($_POST['pasien_cek'] == '1') {
    $fpdf->AddPage("P");
    $fpdf->SetFont("Times", "B", $font_size_title);
    $fpdf->Cell(50, 0, $fpdf->Image($logo, $fpdf->GetX(), $fpdf->GetY(), 22, 22), 0, 0, "R");
    $fpdf->Ln(5);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_title"), 0, 0, "C");
    $fpdf->Ln(7);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_address"), 0, 0, "C");
    $fpdf->Ln(5);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_contact"), 0, 0, "C");
    $fpdf->Ln(15);
    $line_break = 5;
    $fpdf->SetFont("Times", "B", $font_size_header);
    $fpdf->Cell(35, 0, "Nama", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nama_pasien'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ibu Kandung", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['ibu'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "NRM", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nrm'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Noreg", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['noreg'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Alamat", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['alamat'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Umur", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['umur'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Pengirim", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['pengirim'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Konsultan", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['konsultan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "L/P", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['jk'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ruang-Kelas", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    //$ruang_kelas = ArrayAdapter::slugFormat("unslug", $_POST['ruangan'])." - ".ArrayAdapter::slugFormat("unslug", $_POST['kelas']);
    $ruang_kelas = trim($_POST['nama_ruangan']);
    $fpdf->Cell(60, 0, $ruang_kelas, 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "No. Lab", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['no_lab'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Cetakan Untuk", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, "Pasien", 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Diagnosa/Ket. Klinis", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['diagnosa'], 0, 0, "L");
    $fpdf->Ln(10);
    $fpdf->SetFont("Times", "B", $font_size_content);
    $fpdf->Cell(65, 6, "JENIS PEMERIKSAAN", "TB", 0, "L");
    $fpdf->Cell(30, 6, "HASIL", "TB", 0, "L");
    $fpdf->Cell(60, 6, "NILAI RUJUKAN", "TB", 0, "L");
    $fpdf->Cell(40, 6, "METODE", "TB", 0, "L");
    $fpdf->Ln($line_break);
    $current_grup = '';
    for($i = 0; $i < sizeof($_POST['pemeriksaan']); $i++) {
        $pemeriksaan = explode(" $ ", $_POST['pemeriksaan'][$i]);
        $grup = $pemeriksaan[0];
        $jenis = $pemeriksaan[1];
        $hasil = $pemeriksaan[2];
        $normal = $pemeriksaan[3];
        $normal = htmlspecialchars_decode($normal);
        $metode = $pemeriksaan[4];
        $color = $pemeriksaan[5];
        if($current_grup == $grup) {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", "B", 0, "L");
                $fpdf->Cell(63, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", 0, 0, "L");
                $fpdf->Cell(62, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        } else {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", "B", 0, "L");
                $fpdf->Cell(62, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", 0, 0, "L");
                $fpdf->Cell(63, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        }
    }
    //Layanan Lain-Lain
    if($_POST['layanan_lain_lain'] != '' && $_POST['layanan_lain_lain'] != NULL) {
        $fpdf->Ln($line_break);
        $fpdf->SetFont("Times", "B", $font_size_content);
        $fpdf->Cell(20, 6, "LAYANAN LAIN:", 0, 0, "C");
        $fpdf->Ln($line_break);
        $fpdf->Cell(20, 6, "NOMOR", "TB", 0, "C");
        $fpdf->Cell(65, 6, "LAYANAN", "TB", 0, "C");
        $fpdf->Cell(40, 6, "HARGA", "TB", 0, "R");
        $fpdf->Cell(30, 6, "JUMLAH", "TB", 0, "C");
        $fpdf->Cell(40, 6, "SUBTOTAL", "TB", 0, "R");
        $fpdf->Ln($line_break);
        for($i = 0; $i < sizeof($_POST['layanan_lain_lain']); $i++) {
            $layanan_lain = explode(" $ ", $_POST['layanan_lain_lain'][$i]);
            $nomor = $layanan_lain[0];
            $layanan = $layanan_lain[1];
            $harga = $layanan_lain[2];
            $jumlah = $layanan_lain[3];
            $subtotal = $layanan_lain[4];
            $fpdf->SetFont("Times", "", $font_size_content);
            if($i+1 == sizeof($_POST['layanan_lain_lain'])) {
                $fpdf->Cell(20, 6, $nomor, "B", 0, "C");
                $fpdf->Cell(65, 6, $layanan, "B", 0, "C");
                $fpdf->Cell(40, 6, $harga, "B", 0, "R");
                $fpdf->Cell(30, 6, $jumlah, "B", 0, "C");
                $fpdf->Cell(40, 6, $subtotal, "B", 0, "R");
            } else {
                $fpdf->Cell(20, 6, $nomor, 0, 0, "C");
                $fpdf->Cell(65, 6, $layanan, 0, 0, "C");
                $fpdf->Cell(40, 6, $harga, 0, 0, "R");
                $fpdf->Cell(30, 6, $jumlah, 0, 0, "C");
                $fpdf->Cell(40, 6, $subtotal, 0, 0, "R");
            }
            $fpdf->Ln($line_break);
        }
        $fpdf->Ln($line_break);
    }
    $fpdf->Ln($line_break);
    $fpdf->SetFont("Times", "", $font_size_content);
    $fpdf->Cell(23, 6, "Permintaan", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_permintaan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Sample Masuk", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_sample_masuk'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetak Hasil", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_cetak_hasil'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Response Time", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['response_time'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetakan ke", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['cetak_hasil_ke'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Ln($line_break);
    $tempat_waktu = $town.", " . $dd;
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, $tempat_waktu, 0, 0, "C");
    $fpdf->Ln($line_break);
    if($ttd_pj == "1" || $ttd_pj == 1) {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "Penanggung Jawab", 0, 0, "C");
        if($link_ttd_pj != "") {
            $fpdf->Ln(5);
            $fpdf->Cell(65, 6, $fpdf->Image($file_ttd_pj, 163, $fpdf->GetY(), 20, 20), 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Ln(20);
        } else {
            $fpdf->Ln(20);
        }
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, $nama_konsultan, 0, 0, "C");
    } else {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Ln(20);
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
    }
}
if($_POST['rekamedis_cek'] == '1') {
    $fpdf->AddPage("P");
    $fpdf->SetFont("Times", "B", $font_size_title);
    $fpdf->Cell(50, 0, $fpdf->Image($logo, $fpdf->GetX(), $fpdf->GetY(), 22, 22), 0, 0, "R");
    $fpdf->Ln(5);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_title"), 0, 0, "C");
    $fpdf->Ln(7);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_address"), 0, 0, "C");
    $fpdf->Ln(5);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_contact"), 0, 0, "C");
    $fpdf->Ln(15);
    $line_break = 5;
    $fpdf->SetFont("Times", "B", $font_size_header);
    $fpdf->Cell(35, 0, "Nama", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nama_pasien'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ibu Kandung", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['ibu'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "NRM", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nrm'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Noreg", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['noreg'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Alamat", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['alamat'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Umur", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['umur'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Pengirim", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['pengirim'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Konsultan", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['konsultan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "L/P", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['jk'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ruang-Kelas", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    //$ruang_kelas = ArrayAdapter::slugFormat("unslug", $_POST['ruangan'])." - ".ArrayAdapter::slugFormat("unslug", $_POST['kelas']);
    $ruang_kelas = trim($_POST['nama_ruangan']);
    $fpdf->Cell(60, 0, $ruang_kelas, 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "No. Lab", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['no_lab'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Cetakan Untuk", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, "Rekam Medis", 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Diagnosa/Ket. Klinis", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['diagnosa'], 0, 0, "L");
    $fpdf->Ln(10);
    $fpdf->SetFont("Times", "B", $font_size_content);
    $fpdf->Cell(65, 6, "JENIS PEMERIKSAAN", "TB", 0, "L");
    $fpdf->Cell(30, 6, "HASIL", "TB", 0, "L");
    $fpdf->Cell(60, 6, "NILAI RUJUKAN", "TB", 0, "L");
    $fpdf->Cell(40, 6, "METODE", "TB", 0, "L");
    $fpdf->Ln($line_break);
    $current_grup = '';
    for($i = 0; $i < sizeof($_POST['pemeriksaan']); $i++) {
        $pemeriksaan = explode(" $ ", $_POST['pemeriksaan'][$i]);
        $grup = $pemeriksaan[0];
        $jenis = $pemeriksaan[1];
        $hasil = $pemeriksaan[2];
        $normal = $pemeriksaan[3];
        $normal = htmlspecialchars_decode($normal);
        $metode = $pemeriksaan[4];
        $color = $pemeriksaan[5];
        if($current_grup == $grup) {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", "B", 0, "L");
                $fpdf->Cell(63, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", 0, 0, "L");
                $fpdf->Cell(62, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        } else {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", "B", 0, "L");
                $fpdf->Cell(62, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", 0, 0, "L");
                $fpdf->Cell(63, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        }
    }
    //Layanan Lain-Lain
    if($_POST['layanan_lain_lain'] != '' && $_POST['layanan_lain_lain'] != NULL) {
        $fpdf->Ln($line_break);
        $fpdf->SetFont("Times", "B", $font_size_content);
        $fpdf->Cell(20, 6, "LAYANAN LAIN:", 0, 0, "C");
        $fpdf->Ln($line_break);
        $fpdf->Cell(20, 6, "NOMOR", "TB", 0, "C");
        $fpdf->Cell(65, 6, "LAYANAN", "TB", 0, "C");
        $fpdf->Cell(40, 6, "HARGA", "TB", 0, "R");
        $fpdf->Cell(30, 6, "JUMLAH", "TB", 0, "C");
        $fpdf->Cell(40, 6, "SUBTOTAL", "TB", 0, "R");
        $fpdf->Ln($line_break);
        for($i = 0; $i < sizeof($_POST['layanan_lain_lain']); $i++) {
            $layanan_lain = explode(" $ ", $_POST['layanan_lain_lain'][$i]);
            $nomor = $layanan_lain[0];
            $layanan = $layanan_lain[1];
            $harga = $layanan_lain[2];
            $jumlah = $layanan_lain[3];
            $subtotal = $layanan_lain[4];
            $fpdf->SetFont("Times", "", $font_size_content);
            if($i+1 == sizeof($_POST['layanan_lain_lain'])) {
                $fpdf->Cell(20, 6, $nomor, "B", 0, "C");
                $fpdf->Cell(65, 6, $layanan, "B", 0, "C");
                $fpdf->Cell(40, 6, $harga, "B", 0, "R");
                $fpdf->Cell(30, 6, $jumlah, "B", 0, "C");
                $fpdf->Cell(40, 6, $subtotal, "B", 0, "R");
            } else {
                $fpdf->Cell(20, 6, $nomor, 0, 0, "C");
                $fpdf->Cell(65, 6, $layanan, 0, 0, "C");
                $fpdf->Cell(40, 6, $harga, 0, 0, "R");
                $fpdf->Cell(30, 6, $jumlah, 0, 0, "C");
                $fpdf->Cell(40, 6, $subtotal, 0, 0, "R");
            }
            $fpdf->Ln($line_break);
        }
        $fpdf->Ln($line_break);
    }
    $fpdf->Ln($line_break);
    $fpdf->SetFont("Times", "", $font_size_content);
    $fpdf->Cell(23, 6, "Permintaan", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_permintaan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Sample Masuk", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_sample_masuk'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetak Hasil", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_cetak_hasil'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Response Time", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['response_time'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetakan ke", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['cetak_hasil_ke'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Ln($line_break);
    $tempat_waktu = $town.", " . $dd;
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, $tempat_waktu, 0, 0, "C");
    $fpdf->Ln($line_break);
    if($ttd_pj == "1" || $ttd_pj == 1) {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "Penanggung Jawab", 0, 0, "C");
        if($link_ttd_pj != "") {
            $fpdf->Ln(5);
            $fpdf->Cell(65, 6, $fpdf->Image($file_ttd_pj, 163, $fpdf->GetY(), 20, 20), 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Ln(20);
        } else {
            $fpdf->Ln(20);
        }
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, $nama_konsultan, 0, 0, "C");
    } else {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Ln(20);
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
    }
}
if($_POST['lab_cek'] == '1') {
    $fpdf->AddPage("P");
    $fpdf->SetFont("Times", "B", $font_size_title);
    $fpdf->Cell(50, 0, $fpdf->Image($logo, $fpdf->GetX(), $fpdf->GetY(), 22, 22), 0, 0, "R");
    $fpdf->Ln(5);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_title"), 0, 0, "C");
    $fpdf->Ln(7);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_address"), 0, 0, "C");
    $fpdf->Ln(5);
    $fpdf->SetFont("Times", "", $font_size_subtitle);
    $fpdf->Cell(0, 0, getSettings($db,"smis_autonomous_contact"), 0, 0, "C");
    $fpdf->Ln(15);
    $line_break = 5;
    $fpdf->SetFont("Times", "B", $font_size_header);
    $fpdf->Cell(35, 0, "Nama", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nama_pasien'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ibu Kandung", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['ibu'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "NRM", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['nrm'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Noreg", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['noreg'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Alamat", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['alamat'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Umur", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['umur'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Pengirim", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['pengirim'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Konsultan", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['konsultan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "L/P", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['jk'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Ruang-Kelas", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    //$ruang_kelas = ArrayAdapter::slugFormat("unslug", $_POST['ruangan'])." - ".ArrayAdapter::slugFormat("unslug", $_POST['kelas']);
    $ruang_kelas = trim($_POST['nama_ruangan']);
    $fpdf->Cell(60, 0, $ruang_kelas, 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "No. Lab", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['no_lab'], 0, 0, "L");
    $fpdf->Cell(5, 0, "", 0, 0, "L");
    $fpdf->Cell(35, 0, "Cetakan Untuk", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, "Laboratorium", 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(35, 0, "Diagnosa/Ket. Klinis", 0, 0, "L");
    $fpdf->Cell(2, 0, ":", 0, 0, "L");
    $fpdf->Cell(60, 0, $_POST['diagnosa'], 0, 0, "L");
    $fpdf->Ln(10);
    $fpdf->SetFont("Times", "B", $font_size_content);
    $fpdf->Cell(65, 6, "JENIS PEMERIKSAAN", "TB", 0, "L");
    $fpdf->Cell(30, 6, "HASIL", "TB", 0, "L");
    $fpdf->Cell(60, 6, "NILAI RUJUKAN", "TB", 0, "L");
    $fpdf->Cell(40, 6, "METODE", "TB", 0, "L");
    $fpdf->Ln($line_break);
    $current_grup = '';
    for($i = 0; $i < sizeof($_POST['pemeriksaan']); $i++) {
        $pemeriksaan = explode(" $ ", $_POST['pemeriksaan'][$i]);
        $grup = $pemeriksaan[0];
        $jenis = $pemeriksaan[1];
        $hasil = $pemeriksaan[2];
        $normal = $pemeriksaan[3];
        $normal = htmlspecialchars_decode($normal);
        $metode = $pemeriksaan[4];
        $color = $pemeriksaan[5];
        if($current_grup == $grup) {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", "B", 0, "L");
                $fpdf->Cell(63, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", 0, 0, "L");
                $fpdf->Cell(62, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        } else {
            if($i == sizeof($_POST['pemeriksaan']) - 1) {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(3, 6, "", "B", 0, "L");
                $fpdf->Cell(62, 6, $jenis, "B", 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, "B", 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, "B", 0, "L");
                $fpdf->Cell(40, 6, $metode, "B", 0, "L");
                $fpdf->Ln($line_break);
            } else {
                $fpdf->SetFont("Times", "B", $font_size_content);
                $fpdf->Cell(65, 6, $grup, 0, 0, "L");
                $fpdf->Cell(30, 6, "", 0, 0, "L");
                $fpdf->Cell(60, 6, "", 0, 0, "L");
                $fpdf->Cell(40, 6, "", 0, 0, "L");
                $fpdf->Ln($line_break);
                $current_grup = $grup;
                $fpdf->SetFont("Times", "", $font_size_content);
                $fpdf->Cell(2, 6, "", 0, 0, "L");
                $fpdf->Cell(63, 6, $jenis, 0, 0, "L");
                if($color == 'red') {
                    $fpdf->SetTextColor(255, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                } else {
                    $fpdf->SetTextColor(0, 0, 0);
                    $fpdf->Cell(30, 6, $hasil, 0, 0, "L");
                }
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->Cell(60, 6, $normal, 0, 0, "L");
                $fpdf->Cell(40, 6, $metode, 0, 0, "L");
                $fpdf->Ln($line_break);
            }
        }
    }
    //Layanan Lain-Lain
    if($_POST['layanan_lain_lain'] != '' && $_POST['layanan_lain_lain'] != NULL) {
        $fpdf->Ln($line_break);
        $fpdf->SetFont("Times", "B", $font_size_content);
        $fpdf->Cell(20, 6, "LAYANAN LAIN:", 0, 0, "C");
        $fpdf->Ln($line_break);
        $fpdf->Cell(20, 6, "NOMOR", "TB", 0, "C");
        $fpdf->Cell(65, 6, "LAYANAN", "TB", 0, "C");
        $fpdf->Cell(40, 6, "HARGA", "TB", 0, "R");
        $fpdf->Cell(30, 6, "JUMLAH", "TB", 0, "C");
        $fpdf->Cell(40, 6, "SUBTOTAL", "TB", 0, "R");
        $fpdf->Ln($line_break);
        for($i = 0; $i < sizeof($_POST['layanan_lain_lain']); $i++) {
            $layanan_lain = explode(" $ ", $_POST['layanan_lain_lain'][$i]);
            $nomor = $layanan_lain[0];
            $layanan = $layanan_lain[1];
            $harga = $layanan_lain[2];
            $jumlah = $layanan_lain[3];
            $subtotal = $layanan_lain[4];
            $fpdf->SetFont("Times", "", $font_size_content);
            if($i+1 == sizeof($_POST['layanan_lain_lain'])) {
                $fpdf->Cell(20, 6, $nomor, "B", 0, "C");
                $fpdf->Cell(65, 6, $layanan, "B", 0, "C");
                $fpdf->Cell(40, 6, $harga, "B", 0, "R");
                $fpdf->Cell(30, 6, $jumlah, "B", 0, "C");
                $fpdf->Cell(40, 6, $subtotal, "B", 0, "R");
            } else {
                $fpdf->Cell(20, 6, $nomor, 0, 0, "C");
                $fpdf->Cell(65, 6, $layanan, 0, 0, "C");
                $fpdf->Cell(40, 6, $harga, 0, 0, "R");
                $fpdf->Cell(30, 6, $jumlah, 0, 0, "C");
                $fpdf->Cell(40, 6, $subtotal, 0, 0, "R");
            }
            $fpdf->Ln($line_break);
        }
        $fpdf->Ln($line_break);
    }
    $fpdf->Ln($line_break);
    $fpdf->SetFont("Times", "", $font_size_content);
    $fpdf->Cell(23, 6, "Permintaan", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_permintaan'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Sample Masuk", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_sample_masuk'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetak Hasil", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['waktu_cetak_hasil'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Response Time", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['response_time'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Cell(23, 6, "Cetakan ke", 0, 0, "L");
    $fpdf->Cell(2, 6, ":", 0, 0, "L");
    $fpdf->Cell(50, 6, $_POST['cetak_hasil_ke'], 0, 0, "L");
    $fpdf->Ln($line_break);
    $fpdf->Ln($line_break);
    $tempat_waktu = $town.", " . $dd;
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, "", 0, 0, "C");
    $fpdf->Cell(65, 6, $tempat_waktu, 0, 0, "C");
    $fpdf->Ln($line_break);
    if($ttd_pj == "1" || $ttd_pj == 1) {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "Penanggung Jawab", 0, 0, "C");
        if($link_ttd_pj != "") {
            $fpdf->Ln(5);
            $fpdf->Cell(65, 6, $fpdf->Image($file_ttd_pj, 163, $fpdf->GetY(), 20, 20), 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Cell(65, 6, "", 0, 0, "C");
            $fpdf->Ln(20);
        } else {
            $fpdf->Ln(20);
        }
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, $nama_konsultan, 0, 0, "C");
    } else {
        $fpdf->Cell(65, 6, "Pemeriksa", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Ln(20);
        $fpdf->Cell(65, 6, $username, 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
        $fpdf->Cell(65, 6, "", 0, 0, "C");
    }
}

//$md5 = md5($username);
$filename = "Hasil_Lab_".$_POST['id'].".pdf";
$pathfile = "smis-temp/".$filename;

$fpdf->Output($pathfile, "F");

echo json_encode($pathfile);
return;

?>