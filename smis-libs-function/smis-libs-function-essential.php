<?php

/**
 * getting client ip address
 * @return string the ip addres
 */
function getClientIP()
{
	$ipaddress = '';
	if (getenv('HTTP_CLIENT_IP'))
		$ipaddress = getenv('HTTP_CLIENT_IP');
	else if (getenv('HTTP_X_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	else if (getenv('HTTP_X_FORWARDED'))
		$ipaddress = getenv('HTTP_X_FORWARDED');
	else if (getenv('HTTP_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	else if (getenv('HTTP_FORWARDED'))
		$ipaddress = getenv('HTTP_FORWARDED');
	else if (getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
	else
		$ipaddress = 'UNKNOWN';

	return $ipaddress;
}

function get_laravel()
{
	if (defined("SMIS_LARAVEL")) {
		return SMIS_LARAVEL;
	}
	return '';
}

/**
 * @brief this function used for indicated that 
 *          this methode is direct function
 *          that used user to take data withour login
 * @return  true if this is a direct calling
 */
function is_direct()
{
	if (isset($_GET['smis_direct']) && $_GET['smis_direct'] != "") {
		return true;
	}
	return false;
}

/**
 * @brief checking weather the 
 *        chache for service is enabled or not
 * @return  boolean true ir service cache enabled
 */
function is_cached()
{
	if (!defined("SMIS_SERVICE_CACHE")) {
		return false;
	}
	return SMIS_SERVICE_CACHE == 1;
}

/**
 * check is this request in terminal
 * php based for scheduled event
 * it called in index.php
 * @param Array $argv
 * @return boolean
 */
function is_terminal($argv)
{
	if (php_sapi_name() == 'cli' && $argv != null) {
		return true;
	}
	return false;
}


/**
 * logout from smis
 */
function logout()
{
	if (isset($_COOKIE["smis-index"])) {
		unset($_COOKIE["smis-index"]);
		setcookie("smis-index", '', time() - 7000000);
		changeCookie();
	}
	setSession('userid', null);
	session_destroy();
}

/**
 * get this Autonomous Title
 * if not set will return default SMIS Abrevation
 * @return String Title
 */
function getTitle()
{
	if (function_exists("getSettings")) {
		global $db;
		return getSettings($db, "smis_autonomous_abbrevation", "SMIS");;
	}
	return "SMIS";
}

function logoDefault()
{
	require 'smis-base/smis-logo.php';
	if (!file_exists("smis-temp/smis-logo.jpg")) {
		base64_to_jpeg($SMIS_LOGO, "smis-temp/smis-logo.jpg");
		if (!file_exists("smis-temp/smis-logo.jpg")) {
			return $SMIS_LOGO;
		}
	}
	return "smis-temp/smis-logo.jpg";
}



/**
 * get this autonomous Logo, if not set
 * will return the smis default logo
 * @return String this autonomous logo
 */
function getLogo($essential = false)
{
	$logo = "";
	$default = true;
	if (function_exists("getSettings") && !$essential) {
		global $db;
		$logo = getSettings($db, "smis_autonomous_logo", "");
		if ($logo != "" && file_exists("smis-upload/" . $logo)) {
			$logo = get_fileurl($logo);
			$default = false;
		}
	}

	if ($default) {
		$logo = logoDefault();
	}
	return $logo;
}

/**return the logo for PDF 
 * note that logo for PDF not support
 * interlaced
 */
function getLogoNonInterlaced()
{
	$logo = "";
	$default = true;
	if (function_exists("getSettings") && !$essential) {
		global $db;
		$logo = getSettings($db, "smis_autonomous_non_interlaced_logo", "");
		if ($logo != "" && file_exists("smis-upload/" . $logo)) {
			$logo = get_fileurl($logo);
			$default = false;
		}
	}

	if ($default) {
		$logo = logoDefault();
	}
	return $logo;
}



/**
 * this function used to convert from base64 image into
 * binary image file
 */
function base64_to_jpeg($base64_string, $output_file)
{
	$ifp = fopen($output_file, 'wb');
	$data = explode(',', $base64_string);
	fwrite($ifp, base64_decode($data[1]));
	fclose($ifp);
	return $output_file;
}


/**
 * get this autonomous Logo, if not set
 * will return the smis default logo
 * @return String this autonomous logo
 */
function getLogoBW($essential = false)
{
	$logo = "";
	$default = true;
	if (function_exists("getSettings") && !$essential) {
		global $db;
		$logo = getSettings($db, "smis_autonomous_bw_logo", "");
		if ($logo != "" && file_exists("smis-upload/" . $logo)) {
			$logo = get_fileurl($logo);
			$default = false;
		}
	}

	if ($default) {
		$logo = getLogo($essential);
	}
	return $logo;
}

/**
 * get this autonomous Logo, if not set
 * will return the smis default logo
 * @return String this autonomous logo
 */
function getLogoLoading($essential = false)
{
	$logo = "";
	if (function_exists("getSettings") && !$essential) {
		global $db;
		$logo = getSettings($db, "smis_autonomous_loading_logo", "");
		if ($logo == "") {
			require 'smis-base/smis-logo.php';
			$logo = $SMIS_LOGO;
		} else {
			if (file_exists("smis-upload/" . $logo)) {
				$logo = get_fileurl($logo);
			} else {
				require 'smis-base/smis-logo.php';
				$logo = $SMIS_LOGO;
			}
		}
	} else {
		require 'smis-base/smis-logo.php';
		$logo = $SMIS_LOGO;
	}
	return $logo;
}

/**
 * get the sound of this system
 * @return string sound url
 */
function getSound()
{
	return 'smis-base-res/pixiedust.wav';
}


function loadPage()
{
	$page = $_POST['page'];
	$action = $_POST['action'];
	$pro_name = isset($_POST['prototype_name']) ? $_POST['prototype_name'] : "";
	$pro_slug = isset($_POST['prototype_slug']) ? $_POST['prototype_slug'] : "";
	$pro_implement = isset($_POST['prototype_implement']) ? $_POST['prototype_implement'] : "";

	if (file_exists($page . "/index.php")) {
		return $page . "/index.php";
	} else if (file_exists($pro_implement . "/index.php")) {
		return $pro_implement . "/index.php";
	} else {
		return "smis-administrator/404.php";
	}
}

/**
 * set to change the cookie
 * @param boolean $bool
 */
function setChangeCookie($bool)
{
	global $CHANGE_COOKIE;
	$CHANGE_COOKIE = $bool;
}

/**
 * change the system cookie
 */
function changeCookie()
{
	$page = isset($_POST['page']) ? $_POST['page'] : "";
	$action = isset($_POST['action']) ? $_POST['action'] : "";
	$pro_name = isset($_POST['prototype_name']) ? $_POST['prototype_name'] : "";
	$pro_slug = isset($_POST['prototype_slug']) ? $_POST['prototype_slug'] : "";
	$pro_implement = isset($_POST['prototype_implement']) ? $_POST['prototype_implement'] : "";
	if ($page != "smis-base" && !isset($_POST['download_token']) &&  $action != "notification") {
		$array = array(
			"page" => $page,
			"action" => $action,
			"prototype_name" => $pro_name,
			"prototype_slug" => $pro_slug,
			"prototype_implement" => $pro_implement
		);
		setrawcookie("smis-index", rawurlencode(json_encode($array)));
	}
}

/**
 * @brief force to change the cookie.
 * @param String $action 
 * @return  
 */
function forceChangeCookie($page, $action)
{
	$pro_name = isset($_POST['prototype_name']) ? $_POST['prototype_name'] : "";
	$pro_slug = isset($_POST['prototype_slug']) ? $_POST['prototype_slug'] : "";
	$pro_implement = isset($_POST['prototype_implement']) ? $_POST['prototype_implement'] : "";
	if ($page != "smis-base" && !isset($_POST['download_token']) &&  $action != "notification") {
		$array = array(
			"page" => $page,
			"action" => $action,
			"prototype_name" => $pro_name,
			"prototype_slug" => $pro_slug,
			"prototype_implement" => $pro_implement
		);
		setrawcookie("smis-index", rawurlencode(json_encode($array)));
	}
}

/**
 * checking is the sytem in installed
 * @return boolean
 */
function is_system_installed()
{

	if (defined("MULTIPLE_INSTALLATOR")) {
		return false;
	}

	if (!file_exists("smis-base/smis-config.php")) {
		return false;
	}
	require_once("smis-base/smis-config.php");
	if (!defined('SMIS_SERVER')) {
		return false;
	}
	$dbconnector = new DBConnector(SMIS_SERVER, SMIS_DATABASE, SMIS_USERNAME, SMIS_PASSWORD);
	$result = $dbconnector->connect();
	if ($result == null) {
		return false;
	}
	global $wpdb;
	$wpdb = new DBController();
	if (!$wpdb->is_installed()) {
		return false;
	}
	return true;
}

/**
 * check wether this is multisite installator or not.
 * if true then do the multisite installator.
 * 
 * */
function is_multisite()
{
	return defined("MULTIPLE_INSTALLATOR") || defined("MULTISITE");
}

/**
 * checking is that installation process
 * @return boolean
 */
function is_installing_process()
{
	if (isset($_POST['action']) && $_POST['action'] == "smis-install-action")
		return true;
	return false;
}

/**
 * this used for checking is the system an ajax or not
 * @return boolean
 */
function is_ajax()
{
	if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' &&  isset($_POST['page']) && isset($_POST['action'])) {
		return true;
	}
	return false;
}

function getSessionPrefix()
{
	if (!defined('SMIS_SESSION_PREFIX')) {
		define('SMIS_SESSION_PREFIX', 'SMIS');
	}
	return SMIS_SESSION_PREFIX;
}

function getSession($name)
{
	return $_SESSION[getSessionPrefix() . "-" . $name];
}

function setSession($name, $value)
{
	$_SESSION[getSessionPrefix() . "-" . $name] = $value;
}

function issetSession($name)
{
	return isset($_SESSION[getSessionPrefix() . "-" . $name]);
}


/**
 * check is user authorize to do something
 * it called in index.php
 * @param string $is_realtime
 * @return boolean
 */
function is_authorize($is_realtime = false)
{
	if (issetSession('userid')) {
		global $user;
		$user = new User();
		$user->loadUser(getSession('userid'));
		return $user->checkSession($is_realtime);
	}
	return false;
}

/**
 * check whether reques is download mode
 * true if yes
 * @return boolean
 */
function is_download()
{
	if (isset($_POST['smis_download']))
		return true;
	return false;
}

/**
 * check is this a server bus that request this system
 * @return boolean
 */
function is_serverbus_request()
{
	if (isset($_POST['msg']) && isset($_POST['smis-agent']) && $_POST['smis-agent'] == "smis-service")
		return true;
	return false;
}


/**
 * check is the system try to register
 * @return boolean
 */
function is_registration()
{
	if (isset($_POST['action']) && $_POST['action'] == "smis-registration")
		return true;
	return false;
}

/**
 * check is this a server bus that request this system
 * @return boolean
 */
function is_try_login()
{
	if (isset($_POST['action']) && $_POST['action'] == "smis-login-page")
		return true;
	return false;
}

/**
 * check whether is the realtime mode
 * if yes return true, otherwise return false
 * @return boolean
 */
function is_realtime()
{
	if (isset($_POST['action']) && $_POST['action'] == "smis-realtime")
		return true;
	return false;
}


function getFilePerm($file)
{
	$perms = fileperms($file);

	if (($perms & 0xC000) == 0xC000) { // Socket
		$info = 's';
	} elseif (($perms & 0xA000) == 0xA000) { // Symbolic Link
		$info = 'l';
	} elseif (($perms & 0x8000) == 0x8000) { // Regular
		$info = '-';
	} elseif (($perms & 0x6000) == 0x6000) { // Block special
		$info = 'b';
	} elseif (($perms & 0x4000) == 0x4000) { // Directory
		$info = 'd';
	} elseif (($perms & 0x2000) == 0x2000) { // Character special
		$info = 'c';
	} elseif (($perms & 0x1000) == 0x1000) { // FIFO pipe
		$info = 'p';
	} else { // Unknown
		$info = 'u';
	}

	// Owner
	$info .= (($perms & 0x0100) ? 'r' : '-');
	$info .= (($perms & 0x0080) ? 'w' : '-');
	$info .= (($perms & 0x0040) ?
		(($perms & 0x0800) ? 's' : 'x') : (($perms & 0x0800) ? 'S' : '-'));

	// Group
	$info .= (($perms & 0x0020) ? 'r' : '-');
	$info .= (($perms & 0x0010) ? 'w' : '-');
	$info .= (($perms & 0x0008) ?
		(($perms & 0x0400) ? 's' : 'x') : (($perms & 0x0400) ? 'S' : '-'));

	// World
	$info .= (($perms & 0x0004) ? 'r' : '-');
	$info .= (($perms & 0x0002) ? 'w' : '-');
	$info .= (($perms & 0x0001) ?
		(($perms & 0x0200) ? 't' : 'x') : (($perms & 0x0200) ? 'T' : '-'));
	return $info;
}

/**
 * get all file inside Directory
 * @param string $dir path to the dir
 * @param boolean $recursive if true it will return recursively
 * @return array of string.
 */
function getAllFileInDir($directory, $recursive = false, $file_only = true, $showfileinfo = false)
{
	if (!file_exists($directory)) return array();
	$dir = new DirectoryIterator($directory);
	$result = array();
	$key = array();
	foreach ($dir as $fileinfo) {
		if (!$fileinfo->isDot()) {
			$name = $fileinfo->getFileName();
			$result_content = $name;
			if ($showfileinfo) {
				$result_content = array();
				$result_content['name'] = $name;
				$result_content['path'] = $directory . "/" . $name;
				$result_content['owner'] = fileowner($directory . "/" . $name);
				$psix['name'] = "windows";
				if (function_exists('posix_getpwuid')) {
					$psix = posix_getpwuid($result_content['owner']);
				}

				$result_content['user'] = $psix['name'];
				$result_content['size'] = filesize($result_content['path']);
				$result_content['hidden'] = startsWith($name, ".");
				$result_content['mode'] = decoct(fileperms($result_content['path']) & 0777);
				$result_content['fd'] = is_file($result_content['path']) ? "file" : "directory";
				$result_content['istext'] = substr(finfo_file(finfo_open(FILEINFO_MIME), $result_content['path']), 0, 4) == 'text';
			}
			$is_file = is_file($directory . "/" . $name);
			if ($is_file || ($is_file && !$recursive && !$file_only)) {
				$key[] = $name;
				$result[] = $result_content;
			} else if (is_dir($directory . "/" . $name)) {
				if (!$file_only) {
					$key[] = $name;
					$result[] = $result_content;
				}
				if ($recursive) {
					$key[] = $name;
					$result[][$name] = getAllFileInDir($directory . "/" . $name, $recursive, $file_only, $showfileinfo);
				}
			}
		}
	}

	if (empty($key)) asort($result);
	else array_multisort($key, SORT_ASC, $result);

	return $result;
}

function show_error()
{
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

function listFullPath(&$array, $directory, $list)
{
	if (is_array($list)) {
		foreach ($list as $name => $content) {
			if (is_string($name)) $array[$name] = $directory . "/" . $name;
			if (is_string($content)) $array[$content] = $directory . "/" . $content;
			if (is_array($content)) {
				if (is_string($name)) listFullPath($array, $directory . "/" . $name, $content);
				else listFullPath($array, $directory, $content);
			}
		}
	}
}

function get_entity_name($db, $slug)
{
	$dbtable = new DBTable($db, "smis_adm_prototype");
	$x		 = $dbtable->select(array("slug" => $slug));
	if ($x != null) {
		return $x->nama;
	}
	return "";
}
