<?php 

/**
 * @brief this function used to list all combination of given array
 *          recursively
 * @param Array $arr contains the data set 
 * @param int $level how many combination that wanted to
 * @param Array $result the result of all combination 
 * @param Array $curr the combination current 
 * @param Boolean $is_duplicate if duplicate is allowed then it will be set that duplicate is true 
 * @param Boolean $is_position if position is true , then same set with different position consider as different
 * @return  
 */
function combinations($arr, $level, &$result, $curr=array(), $is_duplicate=false,$is_position=false) {
    for($i = 0; $i < count($arr); $i++) {
        if(  $is_duplicate || !in_array($arr[$i],$curr)){
            $new = array_merge($curr, array($arr[$i]));
            if($level == 1) {
                if($is_position){
                    $result[] = $new;
                }else{
                    sort($new);
                    if (!in_array($new, $result)) {
                        $result[] = $new;          
                    }
                }                
            } else {
                combinations($arr, $level - 1, $result, $new,$is_duplicate,$is_position);
            }
        }
    }
}

/**
 * @brief used to count the all combination of given array
 * @param Array $arr the set data that need to be combine 
 * @param Boolean $is_duplicate if true then duplicate is allowed0 
 * @param Boolean $is_position different position is different set 
 * @param int $maximal, if -1 then the data set would be list all posibility 
 * @return  Array of the result.
 */
function combine($arr,$is_duplicate,$is_position,$maximal=-1){   
    $result = array(); 
    if($maximal==-1){
       $maximal=count($arr);
    }
    for ($i = 0; $i<$maximal; $i++) {
        combinations($arr, $i+1, $result,array(),$is_duplicate,$is_position);
    }
    return $result;
}

/**
 * @brief say the number name of given integer
 * @param int $x 
 * @return  String that contains the number name
 */
function numbertell($x){
	$abil = array(
			"", 
			"Satu", "Dua", "Tiga", 
			"Empat", "Lima", "Enam", 
			"Tujuh", "Delapan", "Sembilan", 
			"Sepuluh", "Sebelas"
	);
	
	if ($x < 12) 
	 return " ".$abil[$x];
	elseif ($x<20) 
	 return numbertell($x-10)." Belas";
	elseif ($x<100) 
	 return numbertell($x/10)." Puluh".numbertell($x%10);
	elseif ($x<200) 
	 return " Seratus".numbertell($x-100);
	elseif ($x<1000) 
	 return numbertell($x/100)." Ratus".numbertell($x % 100);
	elseif ($x<2000) 
	 return " Seribu".numbertell($x-1000);
	elseif ($x<1000000) 	
	 return numbertell($x/1000)." Ribu".numbertell($x%1000);
	elseif ($x<1000000000) 
	 return numbertell($x/1000000)." Juta".numbertell($x%1000000);
	elseif ($x<1000000000000) 
	 return numbertell($x/1000000000)." Milyar".numbertell($x%1000000000);
	elseif ($x<1000000000000000) 
	 return numbertell($x/1000000000000)." Trilyun".numbertell($x%1000000000000);
}


/**
 * @brief change given integer into roman numeral
 * @param int $num 
 * @return  String roman numeral.
 */
function romanNumerals($num){
	$n      = intval($num);
	$res    = '';
	$roman_numerals = array('M'  => 1000,'CM' => 900,'D'  => 500,'CD' => 400,'C'  => 100,'XC' => 90,	'L'  => 50,'XL' => 40,'X'  => 10,'IX' => 9,'V'  => 5,'IV' => 4,'I'  => 1);
	foreach ($roman_numerals as $roman => $number){
		$matches  = intval($n / $number);
		$res     .= str_repeat($roman, $matches);
		$n        = $n % $number;
	}
	return $res;
}

/**
 * @brief this function used to determine
 *          the formula that given based on string
 *          counting a formula from a string and array given.
 * @param Array $data contains variable name and value  
 * @param String $formula is the formula that will be calculate 
 * @return  $___hasil is the  calculation result of formula  
 */
function mathFormula(Array $data,$formula){
    $math            = array();
    $math["formula"] = $formula;
    
    foreach($data as $i=>$v){        
        if(!is_numeric($v)){
            continue;
        }
        if($v=="") $v="0";
        $eval='$__'.$i.'='.$v.";";
        eval($eval);
    }
    try{
       $formula=str_replace('$','$__',$formula);
       if($formula=="") 
           $fomula="0";
       
        $eval='$____hasil='.$formula.';';
        $rumus='$____rumus='.$formula.';';
        eval($eval);
        eval($rumus);
        
        $math["value"]=$____hasil;
        $math["rumus"]=$____rumus;
        return $math; 
    }catch(Exception $e){
        return null;
    }
}

/**
 * @brief this function to count one formula based on given data
 * @param \Array $data 
 * @param string $formula 
 * @return  int value that as the result of the formula
 */
function oneFormula(Array $data,$formula){
    foreach($data as $i=>$v){        
        $formula=str_replace($i,$v,$formula);
    }
    $eval='$____hasil='.$formula.';';
    eval($eval);
    return $____hasil;
}

/**
 * @brief this function used to calculate 
 *          all function and formula inside the 
 *          array, the array must right in formula sequence
 * @param \Array $list 
 * @return  Array $result of the calculation of the formula;
 */
function loopFormula(Array $list){
    $result=array();
    foreach($list as $k=>$value){
        $hasil=oneFormula($result,$value);
        $result[$k]=$hasil;
    }
    return $result;
}

/**
 * @brief function to round money until thousand, million etc
 * @param float or double $value 
 * @param round that wwanted to $round 
 * @return  number that rounded
 */
function smis_money_round($value,$round,$typical="ceil"){
    if($round*1==-1){
        return $value;
    }
    $var = 0;
    if($typical=="ceil") {
        $var = ceil($value/$round);
    }else if($typical=="floor"){
        $var = floor($value/$round);
    }else{
        $var = round($value/$round);
    }
    $var = $var*$round;
    return $var;
}


?>