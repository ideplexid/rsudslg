<?php 

function arrayToJSVar($var_name,$array,$use_key=true) {
	$r="";
	if($use_key) $r=$var_name."={";
	else $r=$var_name."=[";
	
	$total=count($array);
	$number=0;
	foreach($array as $k=>$v){
		$number++;
		if($use_key){
			$r.='"'.$k.'":"'.$v.'"';
		}else{
			$r.='"'.$v.'"';
		}
		
		if($number<$total){
			$r.=",";
		}
		
	}
	if($use_key) $r.="};";
	else $r.="];";
	
	return $r;
}



?>