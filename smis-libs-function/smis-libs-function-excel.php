<?php 

/*formatting date in excel*/
function datetime_excel_format($sheet,$column,$value){
    if($value=="" || $value=="0000-00-00 00:00:00")
        return;
    
    $waktu = new DateTime($value);
    $dateVal = PHPExcel_Shared_Date::PHPToExcel($waktu);
    $sheet  ->getStyle($column)
            ->getNumberFormat()
            ->setFormatCode("dd-mm-yyyy HH:mm");
    $sheet  ->setCellValue($column,$dateVal);
}

/*formatting date in excel*/
function date_excel_format($sheet,$column,$value){
    if($value=="" || $value=="0000-00-00")
        return;
    
    $waktu = new DateTime($value);
    $dateVal = PHPExcel_Shared_Date::PHPToExcel($waktu);
    $sheet  ->getStyle($column)
            ->getNumberFormat()
            ->setFormatCode("dd-mm-yyyy");
    $sheet  ->setCellValue($column,$dateVal);    
}

function get_date_excel($format,$value){
     return date($format, PHPExcel_Shared_Date::ExcelToPHP($value)); 
}



?>