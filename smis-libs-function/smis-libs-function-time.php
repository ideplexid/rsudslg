<?php 

/**
 * @brief convert the day name of indonesia
 *          in hexadecimal code
 * @param string $day 
 * @return  int of hexadecimal
 */
function day_id_to_hexa($day){
    if($day=="minggu") return 0x01;
    else if($day=="senin")return 0x02;
    else if($day=="selasa")return 0x04;
    else if($day=="rabu")return 0x08;
    else if($day=="kamis")return 0x10;
    else if($day=="jumat")return 0x20;
    else if($day=="sabtu")return 0x40;
    return 0;
}

/**
 * @brief convert the day name of indonesia
 *          in int code
 * @param string $day 
 * @return  int of hexadecimal
 */
function day_id_to_int($day){
    if($day=="minggu") return 1;
    else if($day=="senin")return 2;
    else if($day=="selasa")return 3;
    else if($day=="rabu")return 4;
    else if($day=="kamis")return 5;
    else if($day=="jumat")return 6;
    else if($day=="sabtu")return 7;
    return 0;
}

/**
 * @brief get the count of the day beetween two dates
 *        like count of sunday, count of monday etc.
 *        $days could be assig as 
 *         0x01         - sunday
 *         0x02         - monday
 *         0x04         - tuesday
 *         0x08         - wednesday
 *         0x10         - thursday
 *         0x20         - friday
 *         0x40         - saturday
 *         0x01 | 0x02  - sunday and monday
 * @param int $days is the integet count number in hexa 
 * @param strtime $start 
 * @param strtime $end 
 * @return  the number of given day
 */
function number_of_days($days, $start, $end) {
    $w = array(date('w', $start), date('w', $end));
    //604800 is one week, // 7 * 24 * 60 * 60
    $x = floor(($end-$start)/604800);
    $sum = 0;

    for ($day = 0;$day < 7;++$day) {
        if ($days & pow(2, $day)) {
            $sum += $x + ($w[0] > $w[1]?$w[0] <= $day || $day <= $w[1] : $w[0] <= $day && $day <= $w[1]);
        }
    }
    return $sum;
}

/**
 * @brief get the time different beetween two date
 *          certain format
 * @param string $start of date in YYYY-MM-DD HH:ii:ss 
 * @param string $end of date in YYYY-MM-DD HH:ii:ss
 * @param string $format of the datetime 
 * @return  string of the time different
 */
function timediff($start,$end,$format){
	if($format=="minute") $format="%i";
	if($format=="hour") $format="%H";
	if($format=="second") $format="%s";
	if($format=="day") $format="%d";
	if($format=="month") $format="%m";
	if($format=="year") $format="%Y";

	$dt1 = DateTime::createFromFormat("Y-m-d H:i:s", $start);
	$dt2 = DateTime::createFromFormat("Y-m-d H:i:s", $end);
	$interval = $dt1->diff($dt2);
	return $interval->format($format);
}

/**
 * @brief adding or subtract a year in 
 * @param String YYYY-MM-DD / YYYY-MM-DD HH:II:SS
 * @param Int $increment could be Positif or Negatif 
 * @return  Adding or subtract a year in YYYY-MM-DD / YYYY-MM-DD HH:II:SS format
 */
function addYear($date,$increment){
	$tahun=substr($date,0,4);
	$rest=substr($date,4);
	$tahun=$tahun*1+$increment;
	return $tahun."".$rest;
}

/**
 * count the different about two time 
 * in minute, start must bigger than end
 * @param unknown_type $start
 * @param unknown_type $end
 */
function minute_different($start,$end){
	$hm=date_difference ($start, $end);
	$minute=$hm['mins']+$hm['hours']*60;	
	if(strcmp($start,$end)<0)
	$minute=0-$minute;
	return $minute;
}


function full_date_difference ($start, $end) {
	$datetime1 = strtotime($start);
	$datetime2 = strtotime($end);
	$interval  = abs($datetime2 - $datetime1);

	$minutes   = round($interval / (60)    );
	$hour=floor($minutes/60);
	$allday=floor($hour/24);
    $day=$allday;
    
	$y=floor($day/365);
    $day=$day-365*$y;
    $month=floor($day/31);
    $d=$day-31*$month;    
    $h=$hour-$allday*24;
    $m=$interval-$hour*60;
    
    $result=array('year'=>$y,'month'=>$month,'day'=>$d,'hour'=>$h, 'mins'=>$m);
	return $result;
}

function to_romawi($number){
	$number=$number*1;
	switch ($number){
		case 1 : return "I"; break;
		case 2 : return "II"; break;
		case 3 : return "III"; break;
		case 4 : return "IV"; break;
		case 5 : return "V"; break;
		case 6 : return "VI"; break;
		case 7 : return "VII"; break;
		case 8 : return "VIII"; break;
		case 9 : return "IX"; break;
		case 10 : return "X"; break;
		case 11 : return "XI"; break;
		case 12 : return "XII"; break;
	}
}

function id_date_format($date,$mini=false){
	$en=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$id=array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");
	if($mini) $id=array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nop","Des");
	return str_replace($en,$id,$date);
}

function enid_month($month,$id=true){
	if($id){
		if($month==1) return "Januari";
		if($month==2) return "Februari";
		if($month==3) return "Maret";
		if($month==4) return "April";
		if($month==5) return "Mei";
		if($month==6) return "Juni";
		if($month==7) return "Juli";
		if($month==8) return "Agustus";
		if($month==9) return "September";
		if($month==10) return "Oktober";
		if($month==11) return "Nopember";
		if($month==12) return "Desember";
	}else{
		if($month==1) return "January";
		if($month==2) return "February";
		if($month==3) return "March";
		if($month==4) return "April";
		if($month==5) return "May";
		if($month==6) return "June";
		if($month==7) return "July";
		if($month==8) return "August";
		if($month==9) return "September";
		if($month==10) return "October";
		if($month==11) return "November";
		if($month==12) return "December";
	}
}

function reverse_month_format($m){
	$month=array();
	$month["Januari"]="01";
	$month["Februari"]="02";
	$month["Maret"]="03";
	$month["April"]="04";
	$month["Mei"]="05";
	$month["Juni"]="06";
	$month["Juli"]="07";
	$month["Agustus"]="08";
	$month["September"]="09";
	$month["Oktober"]="10";
	$month["Nopember"]="11";
	$month["Desember"]="12";
	return $month[$m];
}

function rom_date_format($date){
	$en=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$id=array("I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII");
	return str_replace($en,$id,$date);
}


function date_difference ($start, $end) {
	$datetime1 = strtotime($start);
	$datetime2 = strtotime($end);
	$interval  = abs($datetime2 - $datetime1);
	$minutes   = round($interval / (60));
	$h=floor($minutes/60);
	$m=$minutes%60;
	return array('hours'=>$h, 'mins'=>$m);
}

function add_year ($start, $years) {
	$year=substr($start, 0,4);
	$tgl=substr($start, 4,6);
	$year+=($years*1);
	return $year.$tgl;
}

function month_different($start,$end){
	$ts1 = strtotime($start);
	$ts2 = strtotime($end);
	
	$year1 = date('Y', $ts1);
	$year2 = date('Y', $ts2);
	
	$month1 = date('m', $ts1);
	$month2 = date('m', $ts2);
	
	$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
	return $diff ;
}

function hour_different($start,$end){
	if(	$end=="0000-00-00 00:00:00" || $end=="" || $start=="0000-00-00 00:00:00" || $start=="" )
		return 0;
	$ret=date_difference ($start, $end);
	return $ret['hours'];
}

function date_compare($one,$two){
	$date1  = DateTime::createFromFormat("Y-m-d", $one);
	$date2  = DateTime::createFromFormat("Y-m-d", $two);
	if($date1>$date2) return 1;
	if($date1<$date2) return -1;
	return 0;
}

function datetime_compare($one,$two){
    $date1  = DateTime::createFromFormat("Y-m-d H:i:s", $one);
	$date2  = DateTime::createFromFormat("Y-m-d H:i:s", $two);
    if($date1>$date2) return 1;
	if($date1<$date2) return -1;
	return 0;
}

/**
 * @brief count the day different beetween 
 * 		  two days, if start smaller than end
 * 		  it will return day in minus
 * @param string $start 
 * @param string $end 
 * @return  
 */
function day_diff_non_absolute($start,$end){
	if(	$end=="0000-00-00 00:00:00" || $end=="" || $start=="0000-00-00 00:00:00" || $start=="" ) 
		return 0;
	$startTimeStamp = strtotime($start);
	$endTimeStamp = strtotime($end);
	$timeDiff = $endTimeStamp - $startTimeStamp;
	$numberDays = $timeDiff/86400;  // 86400 seconds in one day
	// and you might want to convert to integer
	$numberDays = $numberDays*1;
	return $numberDays;
}

/**
 * @brief count day different betweem two date
 * 		  it will count including the time. so if it's not more than 
 * 		  24 hours it will count less than different. count will be absolute
 * @param string $start 
 * @param string $end 
 * @return  
 */
function day_diff($start,$end){
	if(	$end=="0000-00-00 00:00:00" || $end=="" || $start=="0000-00-00 00:00:00" || $start=="" ) 
		return 0;
	$startTimeStamp = strtotime($start);
	$endTimeStamp = strtotime($end);
	$timeDiff = abs($endTimeStamp - $startTimeStamp);
	$numberDays = $timeDiff/86400;  // 86400 seconds in one day
	// and you might want to convert to integer
	$numberDays = intval($numberDays);
	return $numberDays;
}

/**
 * @brief same as day_diff , it count the day different 
 * 		  only it doesn;t care about the hour. only the date.
 * 		  it also return absolute
 * @param string $start 
 * @param string $end 
 * @return  
 */
 
function day_diff_only($start,$end){
	if(	$end=="0000-00-00 00:00:00" || $end=="" || $start=="0000-00-00 00:00:00" || $start=="" )
		return 0;	
	$str_start=substr($start, 0,10);
	$str_end=substr($end, 0,10);	
	$startTimeStamp = strtotime($str_start);
	$endTimeStamp = strtotime($str_end);
	$timeDiff = abs($endTimeStamp - $startTimeStamp);
	$numberDays = $timeDiff/86400;  // 86400 seconds in one day
	// and you might want to convert to integer
	$numberDays = intval($numberDays);
	return $numberDays;
}

/**
 * @brief add a day in a geiven date
 * @param string $start of the date it should be YYYY-MM-DD
 * @param int $day, number day that will need to be added 
 * @return  
 */
 
function add_date($start,$day){
	$date = new DateTime($start);
    $date->add(new DateInterval("P".$day."D"));
	return $date->format('Y-m-d');
}


/**
 * @brief mengurangi hari dari tanggal input
 * @param String $start 
 * @param int $day (in minus) 
 * @return  date afer subtract
 */
function sub_date($start,$day){    
   $newdate = strtotime ( '-'.$day.' day' , strtotime ( $start ) ) ;
   $newdate = date ( 'Y-m-d' , $newdate );
   return $newdate;
}



/**
 * @brief create date in array range
 * @param string date $mulai in YYYY-MM-DD 
 * @param string date $selesai in YYYY-MM-DD 
 * @return  Array of date
 */
function createDateRangeArray($mulai,$selesai){
	$result = array();
	$dari   = mktime(1,0,0,substr($mulai,5,2),substr($mulai,8,2),substr($mulai,0,4));
	$sampai = mktime(1,0,0,substr($selesai,5,2),substr($selesai,8,2),substr($selesai,0,4));
	if ($sampai>=$dari){
		array_push($result,date('Y-m-d',$dari)); // first entry
		while ($dari<$sampai){
			$dari+=86400; // add 24 hours in seconds
			array_push($result,date('Y-m-d',$dari));
		}
	}
	return $result;
}

function createMonthDateRange($arrayDate){
	$month=array();
	foreach($arrayDate as $day){
		$m=substr($day,0,7);
		if(!isset($month[$m])){
			$month[$m]=0;
		}
		$month[$m]++;
	}
	return $month;
}

function createYearMonthRange($arrayMonth){
	$year=array();
	foreach($arrayMonth as $month=>$total){
		$y=substr($month,0,4);
		if(!isset($year[$y])){
			$year[$y]=0;
		}
		$year[$y]+=$total;
	}
	return $year;
}

/**
 * @brief check is the date in beetween a range of date
 * @param string $start date in YYYY-MM-DD 
 * @param string $end date in YYYY-MM-DD
 * @param string $date date in YYYY-MM-DD
 * @return  true if in beetween given date
 */
function is_date_in_beetween($start,$end,$date){		
	$dstart = strtotime($start);
	$dend = strtotime($end);
	$ddates=strtotime($date);
	if($ddates>=$dstart && $ddates<$dend){
		return true;
	}
    return false;
}

/**
 * @brief get the day name
 * @param int $day_number 
 * @return  string day dayname
 */
function dayName($day_number){
	$name_day[0]="Minggu";
	$name_day[1]="Senin";
	$name_day[2]="Selasa";
	$name_day[3]="Rabu";
	$name_day[4]="Kamis";
	$name_day[5]="Jumat";
	$name_day[6]="Sabtu";
	return $name_day[$day_number%7];
}

/**
 * @brief get the day number
 * @param string $day_name 
 * @return  int day number
 */
function reverseDayName($day_name){
	$name_day["Minggu"]=0;
	$name_day["Senin"]=1;
	$name_day["Selasa"]=2;
	$name_day["Rabu"]=3;
	$name_day["Kamis"]=4;
	$name_day["Jumat"]=5;
	$name_day["Sabtu"]=6;
	return $name_day[$day_name];
}

/**
 * @brief convert dayname english to dayname indonesian
 * @param string $dayname 
 * @return  string $dayname
 */
function dayEnId($dayname){
    switch($dayname){
        case "Sunday"       :       return "Minggu";
        case "Monday"       :       return "Senin";
        case "Tuesday"      :       return "Selasa";
        case "Wednesday"    :       return "Rabu";
        case "Thursday"     :       return "Kamis";
        case "Friday"       :       return "Jumat";
        case "Saturday"     :       return "Sabtu";
    }
    return "";
}




?>