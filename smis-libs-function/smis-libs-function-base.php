<?php 

function curPageURL() {
	$pageURL = 'http';
	if ( isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ( isset($_SERVER["SERVER_PORT"])  && $_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function curMultisitePageURL() {
	$pageURL=curPageURL();
	$pos=strpos($pageURL,"smis");
	$url=substr($pageURL,0,$pos);
	return $url;
}

function getFolderUrl($name){
	$pos=strrpos($name,"/");
	$url=substr($name,0,$pos);
	return $url;
}

/**
 * @brief same as the getSetting but this one
 * 		  used for locking system for instance that 
 * 		  should be used only by one user.
 * 		  handling the concurrent user.
 * @param Database $db the database 
 * @param String $name name of string
 * @param mix $default is the default value
 * @return  mix value of settings
 */
function getLockSettings($db, $name, $default){
	if($db==null){
		global $db;
		if($db==null){
			global $wpdb;
			$db=$wpdb;
		}
	}
	$query="SELECT value FROM smis_adm_settings WHERE name='$name' ";
	$value=$db->get_var($query);	
	if( empty($value) || $value==null ){
		return $default;
	}
	return $value;
}

/**
 * @brief this set setting usign locking purpose for
 * 		  round robbin system. for system that need only one user at a time 
 * 		  that could access, this is the lock for round robin purpose or handling
 * 		  concurrent user.
 * @param Database $db object the database
 * @param String $name is the settings name
 * @param Mix $value the value of the settings
 * @param String $value_condition default value condition
 * @return  
 */
function setLockSettings($db, $name,$value, $value_condition=""){
	if($db==null){
		global $db;
	}	
	if(isSettingsExist($db, $name)){
		$affected_rows=$db->update("smis_adm_settings",array("value"=>$value), array("name"=>$name,"value"=>$value_condition));
		if($affected_rows==1) 
			return true;
	}else{
		$db->insert("smis_adm_settings",array("value"=>$value,"name"=>$name));
		return true;
	}
	return false;
}

/**
 * @brief get the setting of smis
 * 		  this using table smis_adm_settings
 * 		  where the simple data store.
 * 		  the cached used for detecting the id. because the id
 * 		  used for Logging.
 * @param Database $db 
 * @param String $name is the name of settings 
 * @param mix $default is the default, if not found  
 * @param boolean $chached if true than it will cached otherwise will always took from database 
 * @return String value of the settings
 */
function getSettings($db, $name, $default="",$chached=true){
	if($db==null){
		global $db;
		if($db==null){
			global $wpdb;
			$db=$wpdb;
		}
	}
	global $CACHED_SETTINGS;	
	$CACHED_SETTINGS[$name."__dbid"]="0"; //the database save for logging purpose
	$value=null;
	if(isset($CACHED_SETTINGS[$name])){
		return $CACHED_SETTINGS[$name];
	}
	$query="SELECT id,value FROM smis_adm_settings WHERE name='$name'";
	$vx=$db->get_row($query);
	if($vx!=null){
		$value=$vx->value;
		$CACHED_SETTINGS[$name."__dbid"]=$vx->id; //the database save for logging purpose
	}
	if($value!=="0" && (empty($value) || $value==null || $value=="") ) {
		$value=$default;
	}
	if($chached){
		$CACHED_SETTINGS[$name]=$value;
	}
	return $value;
}

/**
 * @brief 	set the smis_adm_setting
 * 			value so the system can be do as it set
 * 			the cache settings used to take the id for 
 * 			logging system.
 * @param Database $db 
 * @param String $name 
 * @param mix $value 
 * @return  null
 */
function setSettings($db, $name,$value){
	if($db==null){
		global $db;
	}
    $result=false;
	$vbefore=getSettings($db, $name, "");
	global $CACHED_SETTINGS;
	$dbid=$CACHED_SETTINGS[$name."__dbid"];
	
	$before=json_encode(array("value"=>$vbefore,"id"=>$dbid));
	$after=json_encode(array("value"=>$value,"id"=>$dbid));
	if(isSettingsExist($db, $name)){
        $set=array();
        $set['value']=$value;
        $set['autonomous']="";
        $set['time_updated']=date("Y-m-d H:i:s");
        $result=$db->update("smis_adm_settings",$set, array("name"=>$name));
	}else{
        $set=array();
        $set['name']=$name;
        $set['value']=$value;
        $set['time_updated']=date("Y-m-d H:i:s");
        $result=$db->insert("smis_adm_settings",$set);
	}
	$db->log("smis_adm_settings", "Settings", $before, $after,$dbid);
    return $result;
}

function isSettingsExist($db,$name){
	if($db==null){
		global $db;
	}
	$query="SELECT count(*) as total FROM smis_adm_settings WHERE name='$name'";
	$value=$db->get_var($query);
	if($value<=0)
		return false;
	return true;
}

function getUserAuthorization($settings, $menu, $submenu){
	if($settings!=null && $settings!="" && is_array($settings) && isset($settings[$menu]) && isset($settings[$menu][$submenu]) ){
		$val=$settings[$menu][$submenu];
		if($val=="1" || $val==1){
			return true;
		}
	}
	return false;
}

function addUserAuthorization($settings,$menu,$submenu,$value){
	if($settings==null || $settings=="" || !is_array($settings))
		$settings=array();
	if(!isset($settings[$menu]))
		$settings[$menu]=array();
	$settings[$menu][$submenu]=$value;
	return $settings;
}

function getAllUserWithAccess($db,$page,$action){
	$username=array();
	$dbtable=new DBTable($db, "smis_adm_user");
	$dbtable->addCustomKriteria("menu"," LIKE '%\"".$page."\":{%\"".$action."\":\"1\"%}%' ");
	$dbtable->setShowAll(true);
	$data=$dbtable->view("", "0");
	$list=$data['data'];
	foreach($list as $n=>$val){
		$username[]=$val->username;
	}
	$user=implode(" ", $username);
	return $user;
}

function getListActivePlugins($include_prototype=true){
	$json=getSettings(null,"smis_plugins",null);
	$data=json_decode($json,true);
	$list=array();
	foreach($data as $plugin=>$settings){		
		if(/*$plugin!="smis-administrator" && $plugin!="smis-serverbus" &&*/ $settings['actived']=="true"){			
			if($settings['type']=="true" && $include_prototype){	//this is for prototype
				global $db;
				$proto=get_all_prototype($db, $plugin);
				foreach ($proto as $p){
					if($p->status=="actived"){
						$list[$p->slug]=$plugin; //link that checked
					}
				}
			}else{						//for non prototype
				$list[$plugin]=$plugin;
			}
		}
	}
	return $list;
}

function getAllActivePrototypeOf($db, $prototype){
	$json=getSettings(null,"smis_plugins",null);
	$data=json_decode($json,true);
	$list=array();
	foreach($data as $plugin=>$settings){
		if($plugin==$prototype && $settings['type']=="true" ){
			global $db;
			$proto=get_all_prototype($db, $plugin);
			foreach ($proto as $p){
				if($p->status=="actived"){
					$list[$p->slug]=$p->nama; //link that checked
				}
			}
		}
	}
	return $list;
}


function getAllPlugins(){
	$json=getSettings(null,"smis_plugins",null);
	$data=json_decode($json,true);
	return $data;
}

function getContentUrl($msg,$ip,$user,$acaller,$url){
	if(is_array($msg)){
		$params=$msg;
	}else{
		$params = array('msg' => $msg);
	}
	$params['ip']=$ip;
	$params['user']=$user;
	$params['acaller']=$acaller;
	$query = http_build_query ($params);
	$contextData = array (
			'method' => 'POST',
			'header' => "Connection: close\r\n".
			"Content-Type: application/x-www-form-urlencoded\r\n".
			"Content-Length: ".strlen($query)."\r\n",
			'content'=> $query );
	$context = stream_context_create (array ( 'http' => $contextData ));
	$result = file_get_contents($url, false, $context);
	return $result;
}

/**
 * @brief get the error caller info
 * @return  String that contains caller error info
 */
function get_caller_info() {
	$result="<ol class='error-list'>";
	$trace = debug_backtrace();
	$number=0;
	foreach($trace as $t){
		if(!isset($t['function']) || ($t['function']!="get_caller_info" && $t['function']!="smisErrorHandler") ){
			$file="";
            if(isset($t['file'])){
                $file=basename($t['file']);
            }
            $line=isset($t['line'])?" -  <font class='badge badge-success'> ".$t['line']."</font>":" ";
			$class=isset($t['class'])?$t['class']."->":"";
			$arg=isset($t['args'])?"<font class='text-error'>".json_encode($t['args'])."</font>":"";
			$function=isset($t['function'])?$t['function']."(".$arg.")":"";
			
			if ((substr($function, 0, 7) == 'include') || (substr($function, 0, 7) == 'require')) {
				$function = '';
			}
			
			$result.="<li><strong class='text-success'>".$file.$line."</strong> ".$class.$function."</li>";
		}
	}
	$result.="</ol>";
	return($result);
}

/**
 * @brief get the stack trace of an error
 * @return  Strign that contain the stack trace
 */
function get_stack_trace() {
	ob_start();
	debug_print_backtrace();
	$trace = ob_get_contents();
	ob_end_clean();
	$trace = preg_replace ('/^#0\s+' . __FUNCTION__ . "[^\n]*\n/", '', $trace, 1);
	$trace = preg_replace ('/^#(\d+)/me', '\'#\' . ($1 - 1)', $trace);
	return $trace;
}


/**
 * @brief handling the error of each system
 * 		  and automatically saved it 
 * 		  in querylog for log purpose
 * @param int $errno is int error code 
 * @param String $errstr error string description 
 * @param File $errfile tbe file that make an error
 * @param int $errline is number line that error
 * @return  null
 */
function smisErrorHandler($errno, $errstr, $errfile, $errline){
	if (!(error_reporting() & $errno)) {
		return;
	}
	switch ($errno) {
		case E_USER_ERROR: $data="ERROR : "; break;
		case E_USER_WARNING: $data="WARNING :  "; break;
		case E_USER_NOTICE: $data="NOTICE :  "; break; 	
		default: $data="UNKNOW : "; break;
	}
	$data.="<strong>[$errno] $errstr</strong> : On Line <strong>$errline</strong> in file <strong>$errfile</strong></br>";
	$data.=get_caller_info();
	global $querylog;
	if($querylog!=null)
	$querylog->addError($data);
}

set_error_handler("smisErrorHandler");

function confirmURL($url,$suffix){
	if(!endsWith($url,"/")){
		if(!endsWith($url,".php")){
			return $url."/".$suffix;
		}else{
			return $url;
		}
	}else{
		return $url.$suffix;
	}
}

/**
 * @brief create the service code name
 * 		  it's based on md5 hash system
 * 		  to tracking smis service
 * @param String $username 
 * @param String $ip 
 * @param String $autonomous 
 * @return  
 */
function serviceCodeCreator($username,$ip,$autonomous){
	$rand=rand();
	$time=date("dmyhisu");
	$md5=md5($rand.$time.$ip.$username.$autonomous);
	return $md5;
}

/**
 * @brief getting all prototype in smis system
 * 		  all of the protoype would be get using this system
 * @param Database $db or the Database 
 * @param String $slug is the slug of prototype 
 * @return  
 */
function get_all_prototype($db,$slug){
	$dbtable=new DBTable($db, "smis_adm_prototype");
	$dbtable->addCustomKriteria("parent"," LIKE '%".$slug."%'");
	$dbtable->addCustomKriteria("status"," ='actived' ");
	$dbtable->setOrder(" parent ASC, nama ASC ");
	$dbtable->setShowAll(true);
	$data=$dbtable->view("","0");
	return $data["data"];
}

/**
 * @brief this function whether 
 * 		  is querylog save on file or in database
 * 		  if this save on file then the query log and error will 
 *  	  save in smis-log/smis-adm-error/
 * @used smis-farmework/smis/log/QueryLog.php
 * @return  
 */
function is_log_save_on_file(){
	if(defined('LOG_SAVE_ONFILE')){
		return LOG_SAVE_ONFILE=="1";
	}
	return false;
}

/**
 * @brief this function whether 
 * 		  is querylog save or not save the log
 * @used smis-farmework/smis/log/QueryLog.php
 * @return  
 */
function is_log_disabled($level=0){
    if(defined('LOG_SAVE_ONFILE')){
        if(defined('LOG_LEVEL') && LOG_SAVE_ONFILE!="-1"){
            return (LOG_LEVEL&$level)==0;
        }
        return LOG_SAVE_ONFILE=="-1";
	}
	return false;
}

/**
 * @brief create a folder then make it writable
 * @param String $path 
 * @used smis-farmework/smis/log/QueryLog.php
 * @return  
 */
function createFolder($path){
	if(!file_exists($path) || !is_dir($path)){
		return mkdir($path,0777);
	}
	return true;
}

/**
 * @brief create log direcotry
 * @param String $date 
 * @used smis-farmework/smis/log/QueryLog.php
 * @return  
 */
function createLogDirectory($date,$page,$action){
	$year=substr($date,0,4);
	$month=substr($date,5,2);
	$day=substr($date,8,2);
	$hour=substr($date,11,2);
	createFolder("smis-log");
	$path="smis-log/requestlog";
	createFolder($path);
	$path.="/".$page;
	createFolder($path);
	$path.="/".$action;
	createFolder($path);	
	$path.="/".$year;
	createFolder($path);
	$path.="/".$month;
	createFolder($path);
	$path.="/".$day;
	createFolder($path);
	$path.="/".$hour;
	createFolder($path);	
	return $path."/";
}


/**
 * @brief create Service directory
 * @param String $code 
 * @used smis-farmework/smis/log/QueryLog.php
 * @return  
 */
function createLogServiceDirectory($code,$service){
	createFolder("smis-log");
	$path="smis-log/servicelog";
	createFolder($path);
	$path.="/".$service;
	createFolder($path);
	$array_code=str_split($code);
	foreach($array_code as $k=>$x){
		$path.="/".$x;
		createFolder($path);
	}
	return $path."/";
}

/**
 * @brief this function used to save the database id
 * @param $table 
 * @param $service 
 * @return  String path of the folder
 * @used smis-farmework/smis/log/QueryLog.php
 */
function createDatabaseLogDirectory($table,$dbid,$date){
	$path="smis-log/tablelog";
	createFolder($path);
	$name_path=explode("_",$table);
	foreach($name_path as $k=>$x){
		$path.="/".$x;
		createFolder($path);
	}
	
	//the id split into several folder
	$array_code=str_split($dbid."");
	foreach($array_code as $k=>$x){
		$path.="/".$x;
		createFolder($path);
	}
	createFolder($path);
	return $path."/";
}



?>