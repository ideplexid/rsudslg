<?php 

function endsWith($haystack, $needle){
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}
	return (substr($haystack, -$length) === $needle);
}

function startsWith($haystack, $needle){
	return $needle === "" || strpos($haystack, $needle) === 0;
}


function decodeUntilArray($json){
	$decode=json_decode($json,true);
	if($decode==null) return $json;
	if(is_string($decode)) return decodeUntilArray($decode);
	if(is_array($decode)) return json_encode($decode,JSON_PRETTY_PRINT);
}

/*UPLOAD FUNCTION*/

function is_file_image($filename){
	if(endsWith($filename, ".jpg")) return true;
	if(endsWith($filename, ".png")) return true;
	if(endsWith($filename, ".bmp")) return true;
	if(endsWith($filename, ".jpeg")) return true;
	if(endsWith($filename, ".gif")) return true;
	return false;
}

function file_name_upload($name){
	return substr($name, 33,strlen($name)-33);
}

function get_file_link($filename){
	$thename=file_name_upload($filename);
	return "<a href='#' onclick=\"download_file('".$filename."')\">".$thename."</a>";
}

function get_file_link_dv($filename){
	$thename=file_name_upload($filename);
	return "<a href='#' onclick=\"download_file_dv(this)\" data-dv=\"".$filename."\" >".$thename."</a>";
}


function get_fileurl($filename){
	return "smis-upload/".$filename;
}

function is_file_exist($filename){
    return file_exists(get_fileurl($filename));
}

function getFileNameList($json){
	$files=json_decode($json,true);
	$file=array();
	if($files!=null){
		$number=1;
		foreach($files as $f){
			$file[]=get_file_link($f);//"<a href='#' onclick=\"download_file('".$f."')\">".file_name_upload($f)."</a>";
		}
	}
	return $file;
}

function upload_filesize($path)
{
	$size = filesize($path);
	$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	$power = $size > 0 ? floor(log($size, 1024)) : 0;
	return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function array_sort($array, $on, $order=SORT_ASC){

	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}

function array_delete($array, $element) {
	return array_diff($array, $element);//return array_diff($array, [$element]);
}

function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

/**
 * @brief cleaning all special character
 * @param String $string 
 * @return  cleaned string
 */
function str_clean($string) {
   $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

/**
 * this function used to explode 
 * a set of string base of it's space or 
 * white space, the combine if less than max
 * if more then max it will be new set of array.
 * 
 * @param $string is the string that need to explode
 * @param $max is the maximum letter in one single array
 */
function explodeLessThan($string,$max){
	$words		= explode(" ",$string);
	$cword 		= "";
	$lcword 	= 0;
	$rninlai 	= array();
	foreach($words as $word){
		$curlen=strlen(word)+1;
		if($lcword+$curlen>$max){
			$rninlai[] = $cword;
			$lcword = 0;
			$cword  = "";      
		}
		$cword.=$word." ";
		$lcword+=$curlen;
	}
	if($lcword>0){
		$rninlai[] = $cword;
	}
	return $rninlai;
}

?>