<?php 

	function get_operation_state(){
		$option = new OptionBuilder();
		$option ->addSingle("Persiapan Operasi")
				->addSingle("Sedang Operasi")
				->addSingle("Pasca Operasi")
				->addSingle("");
		return $option->getContent();
	}

	function get_operation_room(){
		$option = new OptionBuilder();
		$option ->addSingle("Ruang Premedikasi")
				->addSingle("Ruang Kamar Operasi")
				->addSingle("Ruang Pulih Sadar (RR)")
				->addSingle("ICU")
				->addSingle("");
		return $option->getContent();
	}

	function medical_service(){
		$replace							= array();
		$replace['radiology']				= "Radiology";
		$replace['fisiotherapy']			= "Fisiotherapy";
		$replace['laboratory']				= "Laboratory";
        $replace['elektromedis']			= "Elektromedis";
		$replace['ambulan']					= "Ambulance";
		$replace['gizi']					= "Asuhan Gizi";
        $replace['asuhan_farmasi']			= "Asuhan Farmasi";
		$replace['tindakan_perawat']		= "Tindakan Perawat";
		$replace['tindakan_perawat_igd']	= "Tindakan Perawat IGD";
		$replace['tindakan_igd']			= "Tindakan Perawat IGD";
		$replace['tindakan_dokter']			= "Tindakan Dokter";
		$replace['konsul']					= "Konsul";
		$replace['visite']					= "Visite";
		$replace['konsultasi_dokter']		= "Periksa";
		$replace['bed']						= "Kamar Inap";
		$replace['penjualan_resep']			= "Penjualan Resep Apotek";
		$replace['return_resep']			= "Return Resep Apotek";
		$replace['vk']						= "VK";
		$replace['ok']						= "Operasi";
		$replace['rr']						= "Recovery Room";
		$replace['alok']					= "Alat dan Obat";
		$replace['oksigen_central']			= "Oksigen Central";
		$replace['oksigen_manual']			= "Oksigen Manual";
		$replace['audiometry']				= "Audiometry";
		$replace['bronchoscopy']			= "Bronchoscopy";
		$replace['faal_paru']				= "Faal Paru";
		$replace['spirometry']				= "Spirometry";
		$replace['endoscopy']				= "Endoscopy";
		$replace['ekg']						= "EKG";
		$replace['registration']			= "Registration";
		$replace['administrasi']			= "Administrasi";
		$replace['bank_darah']				= "Bank Darah";
		$replace['hemodialisa']				= "Hemodialisa";
		$replace['kamar_mayat']				= "Kamar Mayat";
		$replace['umum']					= "Umum";
		return $replace;
	}
    
    function medical_layanan_mapping($layanan){
        $replace							= array();
		$replace['gizi']					= "asuhan_gizi";
		$replace['tindakan_perawat_igd']	= "tindakan_perawat";
		$replace['tindakan_igd']			= "tindakan_perawat";
		$replace['konsultasi_dokter']		= "periksa";
		$replace['ok']						= "operasi";
		$replace['rr']						= "recovery_room";
		$replace['alok']					= "alat_obat";
        $ganti								= isset($replace[$layanan])?$replace[$layanan]:$layanan;
		return $ganti; 
    }
    
    function medical_service_finance(){
		$a					= medical_service();
        unset($a['tindakan_igd']);
        unset($a['tindakan_perawat_igd']);
        unset($a['konsultasi_dokter']);
        unset($a['gizi']);
        unset($a['ok']);
        unset($a['rr']);
        unset($a['alok']);        
        $a['periksa']		= "Periksa Dokter";
        $a['asuhan_gizi']	= "Asuhan Gizi";
        $a['operasi']		= "Operasi";
        $a['recovery_room']	= "Recovery Room";
        $a['alat_obat']		= "Alat Obat";        
        return $a;
	}
	
	function medical_service_zero(){
		$data			= medical_service();
		$result			= array();
		foreach($data as $k=>$v){
			$result[$k]	= 0;
		}
		return $result;
	}
    
    function medical_service_finance_zero(){
		$data			= medical_service_finance();
		$result			= array();
		foreach($data as $k=>$v){
			$result[$k]	= 0;
		}
		return $result;
	}
	
	function medical_service_slug(){
		return array_keys(medical_service());
	}

	function medical_carapulang_value($cara){
		$carapulang[""]											= -1;
		$carapulang["Tidak Datang"]								= 0;
		$carapulang["Dipulangkan Hidup"]						= 2;
		$carapulang["Dipulangkan Mati < 1 Jam Post Operasi"]	= 3;
		$carapulang["DIPULANGKAN Mati <=24 Jam"]				= 4;
		$carapulang["Dipulangkan Mati <=48 Jam"]				= 5;
		$carapulang["Dipulangkan Mati >48 Jam"]					= 6;
		$carapulang["Pulang Paksa"]								= 7;
		$carapulang["Kabur"]									= 6;
		$carapulang["Rawat Inap"]								= 1;
		$carapulang["Masuk IGD"]								= 1;
		$carapulang["Pindah Kamar"]								= 1;
		$carapulang["Rujuk Poli Lain"]							= 1;
		$carapulang["Rujuk RS Lain"]							= 4;
		$carapulang["Dikembalikan ke Perujuk"]					= 4;
		return $carapulang[$cara];
	}

	function medical_carapulang($pulang="Dipulangkan Hidup"){
		$keluar = new OptionBuilder();
		$keluar	->add("Tidak Datang","Tidak Datang",$pulang=="Tidak Datang"?"1":"0")
				->add("Dipulangkan Hidup","Dipulangkan Hidup",$pulang=="Dipulangkan Hidup"||$pulang==""?"1":"0")
				//->add("Dipulangkan Mati < 1 Jam Post Operasi","Dipulangkan Mati < 1 Jam Post Operasi",$pulang=="Dipulangkan Mati < 1 Jam Post Operasi"?"1":"0")
				->add("Dipulangkan Mati <=24 Jam","Dipulangkan Mati <=24 Jam",$pulang=="Dipulangkan Mati <=24 Jam"?"1":"0")
				->add("Dipulangkan Mati <=48 Jam","Dipulangkan Mati <=48 Jam",$pulang=="Dipulangkan Mati <=48 Jam"?"1":"0")
				->add("Dipulangkan Mati >48 Jam","Dipulangkan Mati >48 Jam",$pulang=="Dipulangkan Mati >48 Jam"?"1":"0")
				->add("Pulang Paksa","Pulang Paksa",$pulang=="Pulang Paksa"?"1":"0")
				->add("Kabur","Kabur",$pulang=="Kabur"?"1":"0")
				//->add("Rawat Inap","Rawat Inap",$pulang=="Rawat Inap"?"1":"0")
				->add("Masuk IGD","Masuk IGD",$pulang=="Masuk IGD"?"1":"0")
				->add("Pindah Kamar","Pindah Kamar",$pulang=="Pindah Kamar"?"1":"0")
				->add("Dirujuk Ke Poli Lain","Rujuk Poli Lain",$pulang=="Rujuk Poli Lain"?"1":"0")
				->add("Dirujuk Ke RS Lain","Rujuk RS Lain",$pulang=="Rujuk RS Lain"?"1":"0")
				->add("Dikembalikan ke Perujuk","Dikembalikan ke Perujuk",$pulang=="Dikembalikan ke Perujuk"?"1":"0");
		return $keluar->getContent();
	}

	function medical_carapulang_simple(){
		$keluar = new OptionBuilder();
		$keluar	->add("Tidak Datang","Tidak Datang")
				->add("Dipulangkan Hidup","Dipulangkan Hidup")
				//->add("Dipulangkan Mati < 1 Jam Post Operasi","Dipulangkan Mati < 1 Jam Post Operasi")
				->add("Dipulangkan Mati <=48 Jam","Dipulangkan Mati <=48 Jam")
				->add("Dipulangkan Mati <=24 Jam","Dipulangkan Mati <=24 Jam")
				->add("Dipulangkan Mati >48 Jam","Dipulangkan Mati >48 Jam")
				->add("Pulang Paksa","Pulang Paksa")
				->add("Kabur","Kabur")
				->add("Dirujuk Ke RS Lain","Rujuk RS Lain")
				->add("Dikembalikan ke Perujuk","Dikembalikan ke Perujuk")
				->add("Rawat Inap","Rawat Inap");
		return $keluar->getContent();
	}
	
	function medical_metode_obat(){
		$metode = new OptionBuilder();
		$metode ->add("Intra Kapiler")
				->add("Extra Kapiler")
				->add("Oral")
				->add("Anal");
		return $metode->getContent();
	}
	
	function medical_gol_umur($start,$end){
        loadLibrary("smis-libs-function-time");
		$dif = full_date_difference ($start, $end);
		if($dif['year']==0){
			if($dif['month']==0 && $dif['day']<28){
				return "0 - 28 HR";
			}
			return "28 HR - 1 TH";
		}else if($dif['year']<5) return "1-4 TH";
		else if($dif['year']<15) return "5-14 TH";
		else if($dif['year']<25) return "15-24 TH";
		else if($dif['year']<45) return "25-44 TH";
		else if($dif['year']<65) return "45-65 TH";
		else return ">65 TH";
	}
	
	function medical_gol_umur_index_penyakit($start,$end){
		if($start=="" || $start=="0000-00-00" || $start=="0000-00-00 00:00:00" || $end=="" || $end=="0000-00-00" || $end=="0000-00-00 00:00:00"){
			return "TGL LAHIR TDK VALID";
		}		
		$dif = full_date_difference ($start, $end);
		if($dif['year']==0){
			if($dif['month']==0 && $dif['day']<28){
				return "0 - 28 HR";
			}
			return "28 HR - 1 TH";
		}
		else if($dif['year']<5) return "1-4 TH";
		else if($dif['year']<15) return "5-14 TH";
		else if($dif['year']<25) return "15-24 TH";
		else if($dif['year']<45) return "25-44 TH";		
		else if($dif['year']<60) return "45-59 TH";
		else if($dif['year']<65) return "60-64 TH";
		else return "65 TH+";
	}
	
	function medical_secondary_gol_umur($start,$end){
		$dif = full_date_difference ($start, $end);
		if($dif['year']==0){
			if($dif['month']==0 && $dif['day']<28){
				return "0 - 28 HR";
			}
			return "1 - 12 BLN";
		}else if($dif['year']<13) return "1 - 12 TH";
		else if($dif['year']<21) return "13 - 20 TH";
		else if($dif['year']<31) return "21 - 30 TH";
		else return "> 31 TH";
	}
	
	function medical_umur_to_gol_umur($umur){
		if($umur=="TGL LAHIR TDK VALID"){
			return $umur;
		}

		$tahun 	   = 0;
		$bulan 	   = 0;
		$hari  	   = 0;
		$pos_umur  = strpos($umur, "Tahun");
		if($pos_umur!==false){
			$tahun = substr($umur, 0,$pos_umur)*1;
			$umur  = substr($umur, $pos_umur+5);
		}
		
		$pos_bulan = strpos($umur, "Bulan");
		if($pos_bulan!==false){
			$bulan = substr($umur, 0,$pos_bulan)*1;
			$umur  = substr($umur, $pos_bulan+5);
		}
		
		$pos_hari  = strpos($umur, "Hari");
		if($pos_hari!==false){
			$hari  = substr($umur, 0,$pos_hari)*1;
			$umur  = substr($umur, $pos_hari+5);
		}
		
		if($tahun>0){
			if($tahun<5) return "1-4 TH";
			else if($tahun<15) return "5-14 TH";
			else if($tahun<25) return "15-24 TH";
			else if($tahun<45) return "25-44 TH";
			else if($tahun<65) return "45-65 TH";
		}else if($bulan>0 || $hari>28){
			return "28 HR - 1 TH";
		}else{
			return "0 - 28 HR";
		}
	}
	
	function get_medical_gol_umur($gm){
		$ob	= new OptionBuilder();
		$ob ->add("TGL LAHIR TDK VALID", "TGL LAHIR TDK VALID","TGL LAHIR TDK VALID"==$gm?"1":"0")
			->add("0 - 28 HR", "0 - 28 HR","0 - 28 HR"==$gm?"1":"0")
			->add("28 HR - 1 TH", "28 HR - 1 TH","28 HR - 1 TH"==$gm?"1":"0")
			->add("1-4 TH", "1-4 TH","1-4 TH"==$gm?"1":"0")
			->add("5-14 TH", "5-14 TH","5-14 TH"==$gm?"1":"0")
			->add("15-24 TH", "15-24 TH","15-24 TH"==$gm?"1":"0")
			->add("25-44 TH", "25-44 TH","25-44 TH"==$gm?"1":"0")
			->add("45-65 TH", "45-65 TH","45-65 TH"==$gm?"1":"0")
			->add(">65 TH", ">65 TH",">65 TH"==$gm?"1":"0");
		return $ob->getContent();
	}
	
	function medical_umur($start,$end){
        loadLibrary("smis-libs-function-time");
		$dif	= full_date_difference ($start, $end);
		$ret	= "";
		if($dif['year']!=0)  $ret .= $dif['year']." Tahun ";
		if($dif['month']!=0) $ret .= $dif['month']." Bulan ";
		if($dif['day']!=0)   $ret .= $dif['day']." Hari ";
		
		return $ret;
		
	}
	
	function get_dokumen_status($df=""){
		$dokumen = new OptionBuilder();
		$dokumen ->addSingle("Belum Menerima Dokumen",$df=="Belum Menerima Dokumen"?"1":"0")
				 ->addSingle("Telah Menerima Dokumen",$df=="Telah Menerima Dokumen"?"1":"0")
				 ->addSingle("Tidak Menerima Dokumen",$df=="Tidak Menerima Dokumen"?"1":"0")
				 ->addSingle("Dokumen Pindah Ruangan Lain",$df=="Dokumen Pindah Ruangan Lain"?"1":"0")
				 ->addSingle("Dikembalikan Ke Rekam Medis",$df=="Dikembalikan Ke Rekam Medis"?"1":"0");
		return $dokumen->getContent();
	}
	
	function get_rl13_option(){
		$opsigrup = new OptionBuilder();
		$opsigrup ->addSingle("Penyakit Dalam")
				  ->addSingle("Kesehatan Anak")
				  ->addSingle("Obstetri")
				  ->addSingle("Genekologi")
				  ->addSingle("Bedah")
				  ->addSingle("Bedah Orthopedi")
				  ->addSingle("Bedah Saraf")
				  ->addSingle("Luka Bakar")
				  ->addSingle("Saraf")
				  ->addSingle("Jiwa")
				  ->addSingle("Psikologi")
				  ->addSingle("Penatalaksana Penyalahgunaan NAPZA")
				  ->addSingle("THT")
				  ->addSingle("Mata")
				  ->addSingle("Kulit & Kelamin")
				  ->addSingle("Kardiologi")
				  ->addSingle("Paru-paru")
				  ->addSingle("Geriatri")
				  ->addSingle("Radioterapi")
				  ->addSingle("Kedokteran Nuklir")
				  ->addSingle("Kusta")
				  ->addSingle("Rehabilitasi Medik")
				  ->addSingle("Isolasi")
				  ->addSingle("ICU")
				  ->addSingle("ICCU")
				  ->addSingle("NICU / PICU")
				  ->addSingle("Umum")
				  ->addSingle("Gigi & Mulut")
				  ->addSingle("Pelayanan Rawat Darurat")
				  ->addSingle("Perinatologi/Bayi");
		return $opsigrup->getContent();
	}
	
	function get_rl52_option(){
		global $db;
		$rows = $db->get_result("
			SELECT *
			FROM smis_mr_jenis_kegiatan_rl52
			WHERE prop = ''
			ORDER BY nama ASC
		");
		$opsi_rl52 	= new OptionBuilder();
		$opsi_rl52->add("");
		if ($rows != null) {
			foreach ($rows as $row)
				$opsi_rl52->add($row->nama);
		}
		return $opsi_rl52->getContent();
	}

?>