<?php 
function addCSS($url,$smis=true){
	if($smis) return "<link href='smis-".$url."' rel='stylesheet' />";
	else return "<link href='".$url."' rel='stylesheet' />";
}

function addJS($url,$smis=true){
	if($smis) return "<script type='text/javascript' src='smis-".$url."' ></script>";
	else return "<script type='text/javascript' src='".$url."' ></script>";
}

?>