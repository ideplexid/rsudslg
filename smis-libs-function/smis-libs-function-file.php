<?php 

function appendFile($filename,$data){
	$handle=fopen($filename, "a");	
	fwrite($handle,$data);
	fclose($handle);
}

/**
 * $mode = 0 ; deleted included the folder, file and it's content
 * $mode = 1 ; deleted only it's content, but not the current folder
 * $mode = 2 ; deleted only file but not the folder and subfolder
 * 
 * @param unknown $dirPath
 * @param string $mode
 * @return boolean
 */
function deleteFileDir($dirPath,$mode=0) {
	if (! is_dir($dirPath)) {
		if(file_exists($dirPath)){
			unlink($dirPath);
			return true;
		}
		return false;
	}
	
	if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
		$dirPath .= '/';
	}
	$files = glob($dirPath . '*', GLOB_MARK);
	foreach ($files as $file) {
		if (is_dir($file)) {
			deleteFileDir($file,$mode==1?0:$mode);
		} else {
			unlink($file);
		}
	}
	
	if($mode==0) rmdir($dirPath);
	return true;
}

/**
 * remove dir in system
 * @param string $dir that want o remove
 */
function rrmdir($dir) {
	if (is_dir($dir)) {
		$files = scandir($dir);
		foreach ($files as $file)
			if ($file != "." && $file != "..") rrmdir("$dir/$file");
		rmdir($dir);
	}
	else if (file_exists($dir)) unlink($dir);
}

/**
 * recursive copy
 * @param string $src the path
 * @param string $dst the_destionation
 */
function rcopy($src, $dst) {
	if (file_exists ( $dst ))
		rrmdir ( $dst );
	if (is_dir ( $src )) {
		mkdir ( $dst );
		$files = scandir ( $src );
		foreach ( $files as $file )
			if ($file != "." && $file != "..")
			rcopy ( "$src/$file", "$dst/$file" );
	} else if (file_exists ( $src ))
		copy ( $src, $dst );
}

/**
 * recursive move
 * @param string $src the path
 * @param string $dst the_destionation
 */
function rmove($src,$dst){
	rcopy($src, $dst);
	rrmdir($src);
}




?>