<?php 
/**
 * get smis_plugins settings inside the database
 * this settings (smis_plugins) provide any information
 * about which plugins are active or inactive
 * @param unknown $db
 */
function init_database($db){
	$plugins	= getSettings($db, "smis_plugins", "",true);
	if($plugins==NULL || $plugins==""){
		$plugin							= array();
		$administrator					= array();
		$administrator['installed']		= "true";
		$administrator['actived']		= "true";
		$administrator['number']		= "1";
		$administrator['version']		= "1.0.0";
		$administrator['type']			= "false";
		$plugin['smis-administrator']	= $administrator;
		$data							= json_encode($plugin);
		setSettings($db, "smis_plugins", $data);
	}
}

/**
 * crawl thourh all folder inside the database and find the
 * init.php inside folder. the init.php indicate that was a plugins
 * and collect them. inside the init.php of each folder,
 * the global variable $PLUGINS filled with the plugins settings.
 * after that we can init the navigation.
 * @param unknown $db
 */
function init_plugins($db){
	$dir = new DirectoryIterator("./");
	foreach ($dir as $fileinfo) {
		if (!$fileinfo->isDot()) {
			$path=$fileinfo->getPathName();
			$name=$fileinfo->getFileName();
			if(substr($name,0,1)!='.' && is_dir($path)){
				$installation_init=$path."/init.php";
				if(is_file($installation_init)){
					require_once($installation_init);
				}
			}
		}
	}
}


/**
 * after global $PLUGINS filled with all plugins
 * this one will filtered which plugins are actived
 * and which one is not actived. in this navigation of user
 * was created
 */
function init_navigation(){
	global $PLUGINS;
	$plug=$PLUGINS['smis-administrator'];
	$plug->init_navigator(); //administrator is essential it should be init on the first left
	foreach($PLUGINS as $p){
		if($p->is_installed() && $p->is_actived() &&  !$p->is_administrator()){
			$p->init_navigator();
		}
	}
}



/**
 * for plugins that removed form the database
 * it will not saved in smis_plugins anymore
 * @param unknown $db
 * @return unknown
 */
function clean_plugins($db){
	$clean_plugins=array();
	$json_plugins=getSettings($db,"smis_plugins", "",true);
	$installed_plugins=json_decode($json_plugins,true);
	$dir = new DirectoryIterator("./");
	foreach ($dir as $fileinfo) {
		if (!$fileinfo->isDot()) {
			$path=$fileinfo->getPathName();
			$name=$fileinfo->getFileName();
			if(substr($name,0,1)!='.' && is_dir($path)){
				$installation=$path."/init.php";
				if(is_file($installation)){
					$clean_plugins[$name]=$installed_plugins[$name];
				}
			}
		}
	}
	$json=json_encode($clean_plugins);
	setSettings($db,"smis_plugins", $json);
	return $json_plugins;
}


?>