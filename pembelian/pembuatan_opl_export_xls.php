<?php
	global $db;
	require_once("smis-libs-out/php-excel/PHPExcel.php");
	
	$id = $_POST['id'];
	$dbtable = new DBTable($db, "smis_pb_opl");
	$header_row = $dbtable->select($id);
	$detail_info = $dbtable->get_result("
		SELECT *
		FROM smis_pb_dopl
		WHERE id_opl = '" . $id . "' AND prop NOT LIKE 'del' AND jumlah_dipesan > 0
	");
	$jumlah_item = count($detail_info);
	$jumlah_item_per_halaman = 28;
	$jumlah_halaman = ceil($jumlah_item / $jumlah_item_per_halaman);
	$start_row_index = 1;
	$end_row_index = 62;
	$value_index_incr = 62;

	$objPHPExcel = PHPExcel_IOFactory::load("pembelian/templates/opl_template.xlsx");
	
	$objPHPExcel->setActiveSheetIndexByName("OPL");
	$objWorksheet = $objPHPExcel->getActiveSheet();
	
	$logo_path = getSettings($db, "smis_autonomous_logo", "");
	if ($logo_path != "") {
		$logo = new PHPExcel_Worksheet_Drawing();
		$logo->setPath("smis-upload/" . $logo_path);
		$logo->setCoordinates("A1");
		$logo->setHeight(60);
		$logo->setWorksheet($objWorksheet);
		$logo = new PHPExcel_Worksheet_Drawing();
		$logo->setPath("smis-upload/" . $logo_path);
		$logo->setCoordinates("A63");
		$logo->setHeight(60);
		$logo->setWorksheet($objWorksheet);
		$logo = new PHPExcel_Worksheet_Drawing();
		$logo->setPath("smis-upload/" . $logo_path);
		$logo->setCoordinates("A125");
		$logo->setHeight(60);
		$logo->setWorksheet($objWorksheet);
		$logo = new PHPExcel_Worksheet_Drawing();
		$logo->setPath("smis-upload/" . $logo_path);
		$logo->setCoordinates("A187");
		$logo->setHeight(60);
		$logo->setWorksheet($objWorksheet);
	}
	$nama_instansi = getSettings($db, "smis_autonomous_title", "SIMRS");
	$alamat_instansi = getSettings($db, "smis_autonomous_address");
	if ($nama_instansi == "")
		$objWorksheet->setCellValue("B1", "SIMRS");
	else
		$objWorksheet->setCellValue("B1", $nama_instansi);
	if ($alamat_instansi == "")
		$objWorksheet->setCellValue("B2", "ALAMAT SIMRS");
	else
		$objWorksheet->setCellValue("B2", $alamat_instansi);
	$objWorksheet->setCellValue("I1", ": " . $header_row->nomor);
	$objWorksheet->setCellValue("I2", ": " . ArrayAdapter::format("date d-m-Y", $header_row->tanggal));
	$objWorksheet->setCellValue("D6", strtoupper($header_row->nama_vendor));
	$objWorksheet->setCellValue("D7", $header_row->kode_vendor);
	$objWorksheet->setCellValue("D8", strtoupper($header_row->alamat_vendor));
	$objWorksheet->setCellValue("H52", "(" . getSettings($db, "nama_pj_farmasi", "") . ")");
	$objWorksheet->setCellValue("H53", getSettings($db, "sipa_pj_farmasi", ""));
	$objWorksheet->setCellValue("E52", "(" . getSettings($db, "nama_verifikator", "") . ")");
	$objWorksheet->setCellValue("A52", "(" . getSettings($db, "nama_manager_umum", "") . ")");
	
	$cur_item_index = 0;
	$print_area_str = "";
	$total = 0;
	for ($cur_page = 1; $cur_page <= $jumlah_halaman; $cur_page++) {
		$objWorksheet->setCellValue("I3", ": " . $cur_page . " / " . $jumlah_halaman);
		$jumlah_row_index = 43 + ($cur_page - 1) * $value_index_incr;
		$objWorksheet->getStyle("I" . $jumlah_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
		$objWorksheet->getStyle("I" . ($jumlah_row_index + 1))->getNumberFormat()->setFormatCode("#,##0.00");
		$objWorksheet->getStyle("I" . ($jumlah_row_index + 2))->getNumberFormat()->setFormatCode("#,##0.00");
		$cur_row_index = ($start_row_index + 14) + ($cur_page - 1) * $value_index_incr;
		$start_print_area = $start_row_index + $value_index_incr * ($cur_page - 1);
		$end_print_area = $end_row_index + $value_index_incr * ($cur_page - 1);
		$print_area_str .= "A" . $start_print_area . ":J" . $end_print_area . ",";
		for ($cur_item_num = 1; $cur_item_num <= $jumlah_item_per_halaman && $cur_item_index < $jumlah_item; $cur_item_num++) {
			$objWorksheet->setCellValue("A" . $cur_row_index, $cur_item_index + 1);
			$objWorksheet->setCellValue("E" . $cur_row_index, $detail_info[$cur_item_index]->satuan);
			$objWorksheet->setCellValue("F" . $cur_row_index, $detail_info[$cur_item_index]->jumlah_dipesan);
			$objWorksheet->setCellValue("D" . $cur_row_index, $detail_info[$cur_item_index]->nama_barang);
			$objWorksheet->setCellValue("H" . $cur_row_index, $detail_info[$cur_item_index]->kode_barang);
			$objWorksheet->setCellValue("G" . $cur_row_index, $detail_info[$cur_item_index]->hpp / 1.1);
			$objWorksheet->getStyle("G" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->setCellValue("H" . $cur_row_index, "=F" . $cur_row_index . "*G" . $cur_row_index);
			$objWorksheet->getStyle("H" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
			$total += ($detail_info[$cur_item_index]->hpp / 1.1 * $detail_info[$cur_item_index]->jumlah_dipesan);
			$cur_item_index++;
			$cur_row_index++;
		}
	}
	$objWorksheet->setCellValue("H43", $total);
	$objWorksheet->getPageSetup()->setPrintArea(rtrim($print_area_str, ","));
		
	header("Content-type: application/vnd.ms-excel");	
	header("Content-Disposition: attachment; filename=OPL_" . $header_row->nomor . "_" . ArrayAdapter::format("date Ymd", $header_row->tanggal) . "_" . date("Ymd_His") . ".xlsx");
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$objWriter->save("php://output");
?>