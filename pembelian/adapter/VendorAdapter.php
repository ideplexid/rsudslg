<?php
	class VendorAdapter extends ArrayAdapter {
		public function adapt($row) {
			$array = array();
			$array['id'] = $row->id;
			$array['Nomor'] = self::format("digit8", $row->id);
			$array['Kode'] = $row->kode;
			$array['Kode PPn'] = $row->kode_ppn;
			$array['Nama'] = $row->nama;
			$array['Jenis'] = $row->medis == 1 ? "Medis" : "Non-Medis";
			$array['NPWP'] = $row->npwp;
			$array['Kota'] = $row->kota;
			$array['No. Telp.'] = $row->telpon;
			return $array;
		}
	}
?>