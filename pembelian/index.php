<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';

	$policy = new Policy("pembelian", $user);
	$policy->addPolicy("vendor", "vendor", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("mapping_acc_vendor", "mapping_acc_vendor", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("riwayat_kerjasama", "riwayat_kerjasama", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("pembuatan_opl", "pembuatan_opl", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("pembuatan_opl_form", "pembuatan_opl", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("pembuatan_opl_mandiri_form", "pembuatan_opl", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("pembuatan_opl_export_xls", "pembuatan_opl", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("pembuatan_opl_nm", "pembuatan_opl_nm", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->addPolicy("pembuatan_opl_nm_form", "pembuatan_opl_nm", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("pembuatan_opl_nm_mandiri_form", "pembuatan_opl_nm", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("pembuatan_opl_nm_export_xls", "pembuatan_opl_nm", Policy::$DEFAULT_COOKIE_KEEP);
	$policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->initialize();
?>