<?php
	require_once("pembelian/table/PersetujuanOPLTable.php");
	global $db;
	
	$table = new PersetujuanOPLTable(
		array("No.", "No. OPL", "Tanggal", "Kode Vendor", "Nama Vendor"),
		"Pembelian : Persetujuan OPL Farmasi",
		null,
		true
	);
	$table->setName("persetujuan_opl");
	$table->setAddButtonEnable(false);
	$table->setPrintButtonEnable(false);

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			require_once("pembelian/pembuatan_opl_export_xls.php");
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("id", "id");
		$adapter->add("lock_opl", "lock_opl");
		$adapter->add("lock_acc", "lock_acc");
		$adapter->add("No. OPL", "nomor");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("Kode Vendor", "kode_vendor");
		$adapter->add("Nama Vendor", "nama_vendor");
		$dbtable = new DBTable($db, "smis_pb_opl");
		$dbtable->addCustomKriteria(" lock_opl ", " = '1' ");
		$dbtable->addCustomKriteria(" lock_acc ", " = '0' ");
		$dbtable->addCustomKriteria(" medis ", " = '1' ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $table->getHtml();
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("pembelian/js/persetujuan_opl_action.js", false);
	echo addJS("pembelian/js/persetujuan_opl.js", false);
?>