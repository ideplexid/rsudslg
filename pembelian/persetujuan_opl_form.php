<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("pembelian/responder/PembuatanOPLDBResponder.php");
	global $db;
	
	$header_form = new Form("", "", "Pembelian : Persetujuan OPL Farmasi");

	$id = isset($_POST['id']) ? $_POST['id'] : "";
	$f_id = isset($_POST['f_id']) ? $_POST['f_id'] : "";
	$tanggal = !isset($_POST['tanggal']) || $_POST['tanggal'] == "" ? date("Y-m-d") : $_POST['tanggal'];
	$nomor = isset($_POST['nomor']) ? $_POST['nomor'] : "";
	$id_vendor = isset($_POST['id_vendor']) ? $_POST['id_vendor'] : "";
	$kode_vendor = isset($_POST['kode_vendor']) ? $_POST['kode_vendor'] : "";
	$nama_vendor = isset($_POST['nama_vendor']) ? $_POST['nama_vendor'] : "";
	$alamat_vendor = isset($_POST['alamat_vendor']) ? $_POST['alamat_vendor'] : "";
	$kota_vendor = isset($_POST['kota_vendor']) ? $_POST['kota_vendor'] : "";
	$kodepos_vendor = isset($_POST['kodepos_vendor']) ? $_POST['kodepos_vendor'] : "";
	$medis = isset($_POST['medis']) ? $_POST['medis'] : 1;
	$editable = isset($_POST['editable']) ? $_POST['editable'] : "false";
	
	$id_hidden = new Hidden("persetujuan_opl_id", "persetujuan_opl_id", $id);
	$header_form->addElement("", $id_hidden);
	$f_id_hidden = new Hidden("persetujuan_opl_f_id", "persetujuan_opl_f_id", $f_id);
	$header_form->addElement("", $f_id_hidden);
	$medis_hidden = new Hidden("persetujuan_opl_medis", "persetujuan_opl_medis", $medis);
	$header_form->addElement("", $medis_hidden);
	$tanggal_text = new Text("persetujuan_opl_tanggal", "persetujuan_opl_tanggal", $tanggal);
	if ($editable == "true") {
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$tanggal_text->setClass("mydate");
	}
	else
		$tanggal_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Tanggal", $tanggal_text);
	$nomor_text = new Text("persetujuan_opl_nomor", "persetujuan_opl_nomor", $nomor);
	$nomor_text->setAtribute("disabled='disabled'");
	$header_form->addElement("No. OPL", $nomor_text);
	$id_vendor_hidden = new Hidden("persetujuan_opl_id_vendor", "persetujuan_opl_id_vendor", $id_vendor);
	$header_form->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("persetujuan_opl_nama_vendor", "persetujuan_opl_nama_vendor", $nama_vendor);
	$nama_vendor_text->setAtribute("disabled='disabled'");
	if ($editable == "true")
		$nama_vendor_text->setClass("smis-one-option-input");
	$vendor_browse_button = new Button("", "", "Pilih");
	$vendor_browse_button->setClass("btn-info");
	$vendor_browse_button->setIsButton(Button::$ICONIC);
	$vendor_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$vendor_browse_button->setAction("rencana_pembelian_farmasi.chooser('rencana_pembelian_farmasi', 'rencana_pembelian_farmasi', 'rencana_pembelian_farmasi', rencana_pembelian_farmasi)");
	$vendor_browse_button->setAtribute("id='rencana_pembelian_farmasi_browse'");
	$vendor_input_group = new InputGroup("");
	$vendor_input_group->addComponent($nama_vendor_text);
	$vendor_input_group->addComponent($vendor_browse_button);
	if ($editable == "true")
		$header_form->addElement("Nama Vendor", $vendor_input_group);
	else
		$header_form->addElement("Nama Vendor", $nama_vendor_text);
	$kode_vendor_hidden = new Hidden("persetujuan_opl_kode_vendor", "persetujuan_opl_kode_vendor", $kode_vendor);
	$header_form->addElement("", $kode_vendor_hidden);
	$alamat_vendor_hidden = new Hidden("persetujuan_opl_alamat_vendor", "persetujuan_opl_alamat_vendor", "");
	$header_form->addElement("", $alamat_vendor_hidden);
	$kota_vendor_hidden = new Hidden("persetujuan_opl_kota_vendor", "persetujuan_opl_kota_vendor", "");
	$header_form->addElement("", $kota_vendor_hidden);
	$kodepos_vendor_hidden = new Hidden("persetujuan_opl_kodepos_vendor", "persetujuan_opl_kodepos_vendor", "");
	$header_form->addElement("", $kodepos_vendor_hidden);
	
	$table = new Table(
		array("No.", "Kode Barang", "Nama Barang", "Jenis Barang", "Jumlah Dipesan", "Satuan", "HNA", "Subtotal"),
		"",
		null,
		true
	);
	$table->setName("dpersetujuan_opl");
	$table->setFooterVisible(false);
	$table->setAction(false);

	$button_group = new ButtonGroup("");
	$back_button = new Button("", "", "Kembali");
	$back_button->setClass("btn-inverse");
	$back_button->setIsButton(Button::$ICONIC_TEXT);
	$back_button->setIcon("fa fa-chevron-left");
	$back_button->setAction("persetujuan_opl.back()");
	$back_button->setAtribute("id='persetujuan_opl_back'");
	$button_group->addButton($back_button);
	$approve_button = new Button("", "", "Setujui OPL");
	$approve_button->setClass("btn-success");
	$approve_button->setIsButton(Button::$ICONIC_TEXT);
	$approve_button->setIcon("fa fa-lock");
	$approve_button->setAction("persetujuan_opl.approve()");
	$approve_button->setAtribute("id='persetujuan_opl_approve'");
	$button_group->addButton($approve_button);
	$disapprove_button = new Button("", "", "Kembalikan OPL");
	$disapprove_button->setClass("btn-danger");
	$disapprove_button->setIsButton(Button::$ICONIC_TEXT);
	$disapprove_button->setIcon("fa fa-unlock");
	$disapprove_button->setAction("persetujuan_opl.disapprove()");
	$disapprove_button->setAtribute("id='persetujuan_opl_disapprove'");
	$button_group->addButton($disapprove_button);
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "show_detail") {
			$id = $_POST['id'];
			$dbtable = new DBTable($db, "smis_pb_opl");
			$header_row = $dbtable->select($id);
			$detail_rows = $dbtable->get_result("
				SELECT *
				FROM smis_pb_dopl
				WHERE id_opl = '" . $id . "' AND prop NOT LIKE 'del'
			");
			$locked = $header_row->lock_opl;
			$html = "";
			$num = 0;
			if ($detail_rows != null) {
				foreach ($detail_rows as $row) {
					$subtotal = $row->hpp / 1.1 * $row->jumlah_dipesan;
					$f_subtotal = ArrayAdapter::format("money", $subtotal);
					$html .= "
						<tr id='data_" . $num . "'>
							<td id='nomor'></td>
							<td id='id' style='display: none;'>" . $row->id . "</td>
							<td id='f_id' style='display: none;'>" . $row->f_id . "</td>
							<td id='id_barang' style='display: none;'>" . $row->id_barang . "</td>
							<td id='kode_barang'><small>" . $row->kode_barang . "</small></td>
							<td id='nama_barang'><small>" . $row->nama_barang . "</small></td>
							<td id='nama_jenis_barang'><small>" . $row->nama_jenis_barang . "</small></td>
							<td id='jumlah_diajukan' style='display: none;'>" . $row->jumlah_diajukan . "</td>
							<td id='jumlah_dipesan' style='display: none;'>" . $row->jumlah_dipesan . "</td>
							<td id='f_jumlah_dipesan'><small>" . ArrayAdapter::format("number", $row->jumlah_dipesan) . "</small></td>
							<td id='satuan'><small>" . $row->satuan . "</small></td>
							<td id='hpp' style='display: none;'>" . $row->hpp . "</td>
							<td id='hna' style='display: none;'>" . $row->hpp / 1.1 . "</td>
							<td id='f_hna'><small>" . ArrayAdapter::format("money", $row->hpp / 1.1) . "</small></td>
							<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='f_subtotal'><small>" . $f_subtotal . "</small></td>
							<td id='medis' style='display: none;'>" . $row->medis . "</td>
							<td id='inventaris' style='display: none;'>" . $row->inventaris . "</td>
						</tr>
					";
					$num++;
				}
			}
			$data = array();
			$data['html'] = $html;
			$data['row_num'] = $num;
			$data['editable'] = $editable;
			echo json_encode($data);
		} else if ($_POST['command'] == "show_footer") {
			$html = "<tfoot>
						<tr>
							<td colspan='7'><div align='right'><small><strong>Total</strong></small></div></td>
							<td id='total'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='7'><div align='right'><small><strong>PPn</strong></small></div></td>
							<td id='ppn'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='7'><div align='right'><small><strong>Total + PPn</strong></small></div></td>
							<td id='total_tagihan'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
					</tfoot>";
			$data = array();
			$data['html'] = $html;
			echo json_encode($data);
		} else {
			$adapter = new SimpleAdapter(true, "No.");
			$adapter->add("No. RP", "id", "digit8");
			$adapter->add("Tanggal", "tanggal", "date d-m-Y");
			$adapter->add("Kode Vendor", "kode_vendor");
			$adapter->add("Nama Vendor", "nama_vendor");
			$dbtable = new DBTable($db, "smis_pb_opl");
			$dbresponder = new PembuatanOPLDBResponder(
				$dbtable,
				$table,
				$adapter
			);
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
		}
		return;
	}
	
	echo $header_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>";
		echo	 "<div class='span12'>" .
					 $table->getHtml() .
				 "</div>";
	echo 	 "</div>" .
			 "<div class='row-fluid'>" .
				 "<div class='span12' align='right'>" .
					$button_group->getHtml() .
				 "</div>" .
			 "</div>" .
		 "</div>";
	echo addJS("pembelian/js/persetujuan_opl_action.js", false);
	echo addJS("pembelian/js/persetujuan_opl_form.js", false);
?>