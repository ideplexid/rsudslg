<?php
	require_once ("smis-framework/smis/interface/LockerInterface.php");
	require_once ("smis-framework/smis/database/DuplicateResponder.php");
    require_once ("smis-base/smis-include-service-consumer.php");

    $kode_pembelian_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_pembelian_table->setName("kode_pembelian");
	$kode_pembelian_table->setModel(Table::$SELECT);
	$kode_pembelian_adapter = new SimpleAdapter(true, "No.");
	$kode_pembelian_adapter->add("Kode Acc.", "nomor");
	$kode_pembelian_adapter->add("Label", "nama");
	$kode_pembelian_service_responder = new ServiceResponder(
		$db,
		$kode_pembelian_table,
		$kode_pembelian_adapter,
		"get_account",
		"accounting"
	);

    $kode_hutang_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_hutang_table->setName("kode_hutang");
	$kode_hutang_table->setModel(Table::$SELECT);
	$kode_hutang_adapter = new SimpleAdapter(true, "No.");
	$kode_hutang_adapter->add("Kode Acc.", "nomor");
	$kode_hutang_adapter->add("Label", "nama");
	$kode_hutang_service_responder = new ServiceResponder(
		$db,
		$kode_hutang_table,
		$kode_hutang_adapter,
		"get_account",
		"accounting"
	);

	$kode_ppn_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_ppn_table->setName("kode_ppn");
	$kode_ppn_table->setModel(Table::$SELECT);
	$kode_ppn_adapter = new SimpleAdapter(true, "No.");
	$kode_ppn_adapter->add("Kode Acc.", "nomor");
	$kode_ppn_adapter->add("Label", "nama");
	$kode_ppn_service_responder = new ServiceResponder(
		$db,
		$kode_ppn_table,
		$kode_ppn_adapter,
		"get_account",
		"accounting"
	);

	$kode_diskon_pembelian_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_diskon_pembelian_table->setName("kode_diskon_pembelian");
	$kode_diskon_pembelian_table->setModel(Table::$SELECT);
	$kode_diskon_pembelian_adapter = new SimpleAdapter(true, "No.");
	$kode_diskon_pembelian_adapter->add("Kode Acc.", "nomor");
	$kode_diskon_pembelian_adapter->add("Label", "nama");
	$kode_diskon_pembelian_service_responder = new ServiceResponder(
		$db,
		$kode_diskon_pembelian_table,
		$kode_diskon_pembelian_adapter,
		"get_account",
		"accounting"
	);

	$kode_materai_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_materai_table->setName("kode_materai");
	$kode_materai_table->setModel(Table::$SELECT);
	$kode_materai_adapter = new SimpleAdapter(true, "No.");
	$kode_materai_adapter->add("Kode Acc.", "nomor");
	$kode_materai_adapter->add("Label", "nama");
	$kode_materai_service_responder = new ServiceResponder(
		$db,
		$kode_materai_table,
		$kode_materai_adapter,
		"get_account",
		"accounting"
	);

	$kode_ekspedisi_table = new Table(
		array("No.", "Kode Acc.", "Label"),
		"",
		null,
		true
	);
	$kode_ekspedisi_table->setName("kode_ekspedisi");
	$kode_ekspedisi_table->setModel(Table::$SELECT);
	$kode_ekspedisi_adapter = new SimpleAdapter(true, "No.");
	$kode_ekspedisi_adapter->add("Kode Acc.", "nomor");
	$kode_ekspedisi_adapter->add("Label", "nama");
	$kode_ekspedisi_service_responder = new ServiceResponder(
		$db,
		$kode_ekspedisi_table,
		$kode_ekspedisi_adapter,
		"get_account",
		"accounting"
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("kode_pembelian", $kode_pembelian_service_responder);
	$super_command->addResponder("kode_hutang", $kode_hutang_service_responder);
	$super_command->addResponder("kode_ppn", $kode_ppn_service_responder);
	$super_command->addResponder("kode_diskon_pembelian", $kode_diskon_pembelian_service_responder);
	$super_command->addResponder("kode_materai", $kode_materai_service_responder);
	$super_command->addResponder("kode_ekspedisi", $kode_ekspedisi_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	$vendor_table = new Table(
		array("No.", "ID", "Kode", "Nama", "Kota", "Kode Acc. Hutang", "Kode Acc. PPn", "Kode Acc. Materai", "Kode Acc. Bea Kirim", "Kode Acc. Diskon Pembelian", "Kode Acc. Persediaan"),
		"Pembelian : Mapping Acc. Vendor",
		null,
		true
	);
	$vendor_table->setName("mapping_acc_vendor");
	$vendor_table->setAddButtonEnable(false);
	$vendor_table->setDelButtonEnable(false);
	if (isset($_POST['command'])) {
		$vendor_adapter = new SimpleAdapter(true, "No.");
		$vendor_adapter->add("ID", "id");
		$vendor_adapter->add("Kode", "kode");
		$vendor_adapter->add("Nama", "nama");
		$vendor_adapter->add("Kota", "kota");
		$vendor_adapter->add("Kode Acc. Hutang", "acc_kode_hutang");
		$vendor_adapter->add("Kode Acc. PPn", "acc_kode_ppn");
		$vendor_adapter->add("Kode Acc. Materai", "acc_kode_materai");
		$vendor_adapter->add("Kode Acc. Bea Kirim", "acc_kode_ekspedisi");
		$vendor_adapter->add("Kode Acc. Diskon Pembelian", "acc_kode_diskon_pembelian");
		$vendor_adapter->add("Kode Acc. Persediaan", "acc_kode_pembelian");
		$vendor_dbtable = new DBTable(
			$db,
			"smis_pb_vendor"
		);
		$vendor_dbresponder = new DBResponder(
			$vendor_dbtable,
			$vendor_table,
			$vendor_adapter
		);
		$data = $vendor_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	$vendor_modal_columns = array("id", "kode", "nama", "kota", "acc_kode_hutang", "acc_kode_ppn", "acc_kode_diskon_pembelian", "acc_kode_pembelian", "acc_kode_materai", "acc_kode_ekspedisi");
	$vendor_modal_names = array("", "Kode", "Nama", "Kota", "KA. Hutang", "KA. PPn", "KA. Disk. Beli", "KA. Persediaan", "KA. Materai", "KA. Bea Kirim");
	$vendor_modal_values = array("", "", "", "", "", "", "", "", "", "");
	$vendor_modal_types = array("hidden", "text", "text", "text", "chooser-mapping_acc_vendor-kode_hutang", "chooser-mapping_acc_vendor-kode_ppn", "chooser-mapping_acc_vendor-kode_diskon_pembelian", "chooser-mapping_acc_vendor-kode_pembelian", "chooser-mapping_acc_vendor-kode_materai", "chooser-mapping_acc_vendor-kode_ekspedisi");
	$vendor_modal_emptiness = array("y", "y", "n", "y", "y", "y", "y", "y", "y", "y");
	$vendor_modal_disables = array(true, true, true, true, true, true, true, true, true, true);
	$vendor_table->setModal(
		$vendor_modal_columns,
		$vendor_modal_types,
		$vendor_modal_names,
		$vendor_modal_values,
		$vendor_modal_emptiness,
		null,
		$vendor_modal_disables
	);
	$vendor_modal = $vendor_table->getModal();
	$vendor_modal->setTitle("Data Vendor");
	$clear_button = new Button("", "", "Hapus Akun");
	$clear_button->setClass("btn-inverse");
	$clear_button->setAction("mapping_acc_vendor.clear_accounts()");
	$clear_button->setIcon("fa fa-eraser");
	$clear_button->setIsButton(Button::$ICONIC);
	$vendor_modal->addFooter($clear_button);
	
	echo $vendor_modal->getHtml();
	echo $vendor_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function KodePembelianAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	KodePembelianAction.prototype.constructor = KodePembelianAction;
	KodePembelianAction.prototype = new TableAction();
	KodePembelianAction.prototype.selected = function(json) {
		$("#mapping_acc_vendor_acc_kode_pembelian").val(json.nomor);
	};

	function KodeHutangAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	KodeHutangAction.prototype.constructor = KodeHutangAction;
	KodeHutangAction.prototype = new TableAction();
	KodeHutangAction.prototype.selected = function(json) {
		$("#mapping_acc_vendor_acc_kode_hutang").val(json.nomor);
	};

	function KodePPnAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	KodePPnAction.prototype.constructor = KodePPnAction;
	KodePPnAction.prototype = new TableAction();
	KodePPnAction.prototype.selected = function(json) {
		$("#mapping_acc_vendor_acc_kode_ppn").val(json.nomor);
	};

	function KodeDiskonPembelianAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	KodeDiskonPembelianAction.prototype.constructor = KodeDiskonPembelianAction;
	KodeDiskonPembelianAction.prototype = new TableAction();
	KodeDiskonPembelianAction.prototype.selected = function(json) {
		$("#mapping_acc_vendor_acc_kode_diskon_pembelian").val(json.nomor);
	};

	function MappingAccVendorAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	MappingAccVendorAction.prototype.constructor = MappingAccVendorAction;
	MappingAccVendorAction.prototype = new TableAction();
	MappingAccVendorAction.prototype.clear_accounts = function() {
		$("#mapping_acc_vendor_acc_kode_pembelian").val("");
		$("#mapping_acc_vendor_acc_kode_hutang").val("");
		$("#mapping_acc_vendor_acc_kode_ppn").val("");
		$("#mapping_acc_vendor_acc_kode_diskon_pembelian").val("");
	};

	function KodeMateraiAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	KodeMateraiAction.prototype.constructor = KodeMateraiAction;
	KodeMateraiAction.prototype = new TableAction();
	KodeMateraiAction.prototype.selected = function(json) {
		$("#mapping_acc_vendor_acc_kode_materai").val(json.nomor);
	};

	function KodeEkspedisiAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	KodeEkspedisiAction.prototype.constructor = KodeEkspedisiAction;
	KodeEkspedisiAction.prototype = new TableAction();
	KodeEkspedisiAction.prototype.selected = function(json) {
		$("#mapping_acc_vendor_acc_kode_ekspedisi").val(json.nomor);
	};

	function MappingAccVendorAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	MappingAccVendorAction.prototype.constructor = MappingAccVendorAction;
	MappingAccVendorAction.prototype = new TableAction();
	MappingAccVendorAction.prototype.clear_accounts = function() {
		$("#mapping_acc_vendor_acc_kode_pembelian").val("");
		$("#mapping_acc_vendor_acc_kode_hutang").val("");
		$("#mapping_acc_vendor_acc_kode_ppn").val("");
		$("#mapping_acc_vendor_acc_kode_diskon_pembelian").val("");
	};

	var mapping_acc_vendor;
	var kode_hutang;
	var kode_ppn;
	var kode_diskon_pembelian;
	var kode_pembelian;
	var kode_materai;
	var kode_ekspedisi;

	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		kode_pembelian = new KodePembelianAction(
			"kode_pembelian",
			"pembelian",
			"mapping_acc_vendor",
			new Array()
		);
		kode_pembelian.setSuperCommand("kode_pembelian");
		kode_hutang = new KodeHutangAction(
			"kode_hutang",
			"pembelian",
			"mapping_acc_vendor",
			new Array()
		);
		kode_hutang.setSuperCommand("kode_hutang");
		kode_ppn = new KodePPnAction(
			"kode_ppn",
			"pembelian",
			"mapping_acc_vendor",
			new Array()
		);
		kode_ppn.setSuperCommand("kode_ppn");
		kode_diskon_pembelian = new KodeDiskonPembelianAction(
			"kode_diskon_pembelian",
			"pembelian",
			"mapping_acc_vendor",
			new Array()
		);
		kode_diskon_pembelian.setSuperCommand("kode_diskon_pembelian");
		kode_materai = new KodeMateraiAction(
			"kode_materai",
			"pembelian",
			"mapping_acc_vendor",
			new Array()
		);
		kode_materai.setSuperCommand("kode_materai");
		kode_ekspedisi = new KodeEkspedisiAction(
			"kode_ekspedisi",
			"pembelian",
			"mapping_acc_vendor",
			new Array()
		);
		kode_ekspedisi.setSuperCommand("kode_ekspedisi");
		var mapping_acc_vendor_columns = new Array("id", "kode", "nama", "kota", "acc_kode_hutang", "acc_kode_ppn", "acc_kode_diskon_pembelian", "acc_kode_pembelian", "acc_kode_materai", "acc_kode_ekspedisi");
		mapping_acc_vendor = new MappingAccVendorAction(
			"mapping_acc_vendor",
			"pembelian",
			"mapping_acc_vendor",
			mapping_acc_vendor_columns
		);
		mapping_acc_vendor.view();
	});
</script>