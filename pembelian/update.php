<?php 
	require_once("smis-libs-class/DBCreator.php");
	global $wpdb;
	
	$dbcreator = new DBCreator($wpdb, "smis_pb_vendor");
	$dbcreator->addColumn("kode", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_ppn", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("npwp", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("alamat", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kota", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kodepos", "VARCHAR(8)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("telpon", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_cp", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("telpon_cp", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_pembelian", "VARCHAR(64)", DBCreator::$SIGN_NONE, "", DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_hutang", "VARCHAR(64)", DBCreator::$SIGN_NONE, "", DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_ppn", "VARCHAR(64)", DBCreator::$SIGN_NONE, "", DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_diskon_pembelian", "VARCHAR(64)", DBCreator::$SIGN_NONE, "", DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_materai", "VARCHAR(64)", DBCreator::$SIGN_NONE, "", DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("acc_kode_ekspedisi", "VARCHAR(64)", DBCreator::$SIGN_NONE, "", DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_pb_riwayat_kerja_sama");
	$dbcreator->addColumn("id_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("waktu_awal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("waktu_akhir", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_pb_opl");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nomor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("alamat_vendor", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kota_vendor", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kodepos_vendor", "VARCHAR(8)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("lock_opl", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ppn", "DOUBLE", DBCreator::$SIGN_NONE, true, 10, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_pb_dopl");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_opl", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_barang", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_barang", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_barang", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hpp", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ppn", "DOUBLE", DBCreator::$SIGN_NONE, true, 10, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_diajukan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_dipesan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("inventaris", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
?>