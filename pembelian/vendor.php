<?php
	$vendor_table = new Table(
		array("Nomor", "Kode", "Kode PPn", "Nama", "NPWP", "Kota", "No. Telp.", "Medis/Non-Medis"),
		"Pembelian : Vendor",
		null,
		true
	);
	$vendor_table->setName("vendor");
	$export_button = new Button("", "","Export");
	$export_button->setAction("vendor.export_xls()");
	$export_button->setClass("btn-info");
	$export_button->setAtribute("data-content='Export XLS' data-toggle='popover'");
	$export_button->setIcon("fa fa-download");
	$export_button->setIsButton(Button::$ICONIC);
	$vendor_table->addHeaderButton($export_button);
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			require_once("smis-libs-out/php-excel/PHPExcel.php");

			$dbtable = new DBTable($db, "smis_pb_vendor");
			$data = $dbtable->get_result("
				SELECT *
				FROM smis_pb_vendor
				WHERE prop NOT LIKE 'del'
			");
			$objPHPExcel = PHPExcel_IOFactory::load("pembelian/templates/vendor_template.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("VENDOR");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			if (count($data) - 2 > 2)
				$objWorksheet->insertNewRowBefore(6, count($data) - 2);
			$start_row_num = 5;
			$end_row_num = 5;
			$row_num = $start_row_num;
			$no = 1;
			foreach ($data as $d) {
				$col_num = 0;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $no++);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->npwp);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->alamat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kota);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kodepos);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->telpon);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_cp);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->telpon_cp);
				$row_num++;
				$end_row_num++;
				$no++;
			}			
				
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=DATA_VENDOR_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		class VendorAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Kode'] = $row->kode;
				$array['Kode PPn'] = $row->kode_ppn;
				$array['Nama'] = $row->nama;
				$array['NPWP'] = $row->npwp;
				$array['Kota'] = $row->kota;
				$array['No. Telp.'] = $row->telpon;
				$array['Medis/Non-Medis'] = ArrayAdapter::format("trivial_1_Medis_Non-Medis", $row->medis);
				return $array;
			}
		}
		$vendor_adapter = new VendorAdapter();
		$columns = array("id", "kode", "kode_ppn", "nama", "npwp", "alamat", "kota", "kodepos", "medis", "telpon", "nama_cp", "telpon_cp", "keterangan");
		$vendor_dbtable = new DBTable(
			$db,
			"smis_pb_vendor",
			$columns
		);
		$vendor_dbresponder = new DBResponder(
			$vendor_dbtable,
			$vendor_table,
			$vendor_adapter
		);
		$data = $vendor_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	$vendor_modal_columns = array("id", "kode", "kode_ppn", "nama", "npwp", "alamat", "kota", "kodepos", "medis", "telpon" , "nama_cp", "telpon_cp", "keterangan");
	$vendor_modal_names = array("", "Kode", "Kode PPn", "Nama", "NPWP", "Alamat", "Kota", "Kode Pos", "Medis", "No. Telp.", "Nama CP", "Telp. CP", "Keterangan");
	$vendor_modal_values = array("", "", "", "", "", "", "", "", "", "", "", "", "");
	$vendor_modal_types = array("hidden", "text", "text", "text", "text", "textarea", "text", "text", "checkbox", "text", "text", "text", "summernote");
	$vendor_modal_emptiness = array("y", "n", "n", "n", "n", "n", "n", "n", "n", "n", "n", "n", "y");
	$vendor_table->setModal(
		$vendor_modal_columns,
		$vendor_modal_types,
		$vendor_modal_names,
		$vendor_modal_values,
		$vendor_modal_emptiness
	);
	$vendor_modal = $vendor_table->getModal();
	$vendor_modal->setTitle("Vendor");
	
	echo $vendor_modal->getHtml();
	echo $vendor_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function VendorAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	VendorAction.prototype.constructor = VendorAction;
	VendorAction.prototype = new TableAction();
	VendorAction.prototype.export_xls = function() {
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		postForm(data);
	};

	var vendor;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		var vendor_columns = new Array("id", "kode", "kode_ppn", "nama", "npwp", "alamat", "kota", "kodepos", "medis", "telpon", "nama_cp", "telpon_cp", "keterangan");
		vendor = new VendorAction(
			"vendor",
			"pembelian",
			"vendor",
			vendor_columns
		);
		vendor.view();
	});
</script>