<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-base/smis-include-duplicate.php");
	global $db;
	
	$riwayat_kerjasama_table = new Table(
		array("Nomor", "Awal", "Akhir", "Keterangan"),
		"",
		null,
		true
	);
	$riwayat_kerjasama_table->setName("riwayat_kerjasama");
	
	$riwayat_kerjasama_form = new Form("riwayat_kerjasama_form", "", "Riwayat Kerjasama Vendor");
	$id_hidden = new Hidden("vendor_id", "vendor_id", "");
	$riwayat_kerjasama_form->addElement("", $id_hidden);
	$nama_text = new Text("vendor_nama", "vendor_nama", "");
	$nama_text->setAtribute("disabled='disabled'");
	$riwayat_kerjasama_form->addElement("Nama Vendor", $nama_text);
	$npwp_text = new Text("vendor_npwp", "vendor_npwp", "");
	$npwp_text->setAtribute("disabled='disabled'");
	$riwayat_kerjasama_form->addElement("NPWP", $npwp_text);
	$alamat_text = new Text("vendor_alamat", "vendor_alamat", "");
	$alamat_text->setAtribute("disabled='disabled'");
	$riwayat_kerjasama_form->addElement("Alamat", $alamat_text);
	$kota_text = new Text("vendor_kota", "vendor_kota", "");
	$kota_text->setAtribute("disabled='disabled'");
	$riwayat_kerjasama_form->addElement("Kota", $kota_text);
	$kodepos_text = new Text("vendor_kodepos", "vendor_kodepos", "");
	$kodepos_text->setAtribute("disabled='disabled'");
	$riwayat_kerjasama_form->addElement("Kode Pos", $kodepos_text);
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-primary");
	$browse_button->setIcon("icon-white icon-list-alt");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("vendor.chooser('vendor', 'vendor_button', 'vendor', vendor)");
	$riwayat_kerjasama_form->addElement("", $browse_button);
	if (isset($_POST['super_command']) && $_POST['super_command'] == "riwayat_kerjasama") {
		if (isset($_POST['command'])) {
			$riwayat_kerjasama_adapter = new SimpleAdapter();
			$riwayat_kerjasama_adapter->add("Nomor", "id", "digit8");
			$riwayat_kerjasama_adapter->add("Awal", "waktu_awal", "date d M Y");
			$riwayat_kerjasama_adapter->add("Akhir", "waktu_akhir", "date d M Y");
			$riwayat_kerjasama_adapter->add("Keterangan", "keterangan");
			$riwayat_kerjasama_columns = array("id", "id_vendor", "waktu_awal", "waktu_akhir", "keterangan");
			$riwayat_kerjasama_dbtable = new DBTable(
				$db,
				"smis_pb_riwayat_kerja_sama",
				$riwayat_kerjasama_columns
			);
			$riwayat_kerjasama_dbtable->addCustomKriteria(" id_vendor ", " = '" . $_POST['id_vendor'] . "' ");
			$riwayat_kerjasama_dbresponder = new DuplicateResponder(
				$riwayat_kerjasama_dbtable,
				$riwayat_kerjasama_table,
				$riwayat_kerjasama_adapter
			);
			$data = $riwayat_kerjasama_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	$riwayat_kerjasama_modal = new Modal("riwayat_kerjasama_add_form", "smis_form_container", "riwayat_kerjasama");
	$riwayat_kerjasama_modal->setTitle("Data Riwayat Kerjasama Vendor");
	$id_hidden = new Hidden("riwayat_kerjasama_id", "riwayat_kerjasama_id", "");
	$riwayat_kerjasama_modal->addElement("", $id_hidden);
	$id_vendor_hidden = new Hidden("riwayat_kerjasama_id_vendor", "riwayat_kerjasama_id_vendor", "");
	$riwayat_kerjasama_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("riwayat_kerjasama_nama_vendor", "riwayat_kerjasama_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$riwayat_kerjasama_modal->addElement("Nama Vendor", $nama_vendor_text);
	$awal_kerjasama_text = new Text("riwayat_kerjasama_awal", "riwayat_kerjasama_awal", "");
	$awal_kerjasama_text->setClass("mydate");
	$awal_kerjasama_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$riwayat_kerjasama_modal->addElement("Awal", $awal_kerjasama_text);
	$akhir_kerjasama_text = new Text("riwayat_kerjasama_akhir", "riwayat_kerjasama_akhir", "");
	$akhir_kerjasama_text->setClass("mydate");
	$akhir_kerjasama_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$riwayat_kerjasama_modal->addElement("Akhir", $akhir_kerjasama_text);
	$keterangan_textarea = new TextArea("riwayat_kerjasama_keterangan", "riwayat_kerjasama_keterangan", "");
	$riwayat_kerjasama_modal->addElement("Keterangan", $keterangan_textarea);
	$riwayat_kerjasama_button = new Button("", "", "Simpan");
	$riwayat_kerjasama_button->setClass("btn-success");
	$riwayat_kerjasama_button->setIcon("fa fa-floppy-o");
	$riwayat_kerjasama_button->setIsButton(Button::$ICONIC);
	$riwayat_kerjasama_button->setAction("riwayat_kerjasama.save()");
	$riwayat_kerjasama_modal->addFooter($riwayat_kerjasama_button);
	
	//chooser:
	$vendor_table = new Table(
		array("Nomor", "Nama Vendor", "NPWP", "Alamat", "Kota", "Kode Pos"),
		"",
		null,
		true
	);
	$vendor_table->setName("vendor");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter();
	$vendor_adapter->add("Nomor", "id", "digit8");
	$vendor_adapter->add("Nama Vendor", "nama");
	$vendor_adapter->add("NPWP", "npwp");
	$vendor_adapter->add("Alamat", "alamat");
	$vendor_adapter->add("Kota", "kota");
	$vendor_adapter->add("Kode Pos", "kodepos");
	$vendor_dbtable = new DBTable($db, "smis_pb_vendor");
	$vendor_dbresponder = new DBResponder(
		$vendor_dbtable,
		$vendor_table,
		$vendor_adapter
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("vendor", $vendor_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	echo $riwayat_kerjasama_modal->getHtml();
	echo $riwayat_kerjasama_form->getHtml();
	echo "<div id='table_content'>";
	echo $riwayat_kerjasama_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function RiwayatKerjasamaAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	RiwayatKerjasamaAction.prototype.constructor = RiwayatKerjasamaAction;
	RiwayatKerjasamaAction.prototype = new TableAction();
	RiwayatKerjasamaAction.prototype.getViewData = function() {
		var data = TableAction.prototype.getViewData.call(this);
		data['id_vendor'] = $("#vendor_id").val();
		return data;
	};
	RiwayatKerjasamaAction.prototype.show_add_form = function() {
		if ($("#vendor_id").val() == "") {
			return;
		}
		$("#riwayat_kerjasama_id").val();
		$("#riwayat_kerjasama_id_vendor").val($("#vendor_id").val());
		$("#riwayat_kerjasama_nama_vendor").val($("#vendor_nama").val());
		$("#riwayat_kerjasama_awal").val("");
		$("#riwayat_kerjasama_akhir").val("");
		$("#riwayat_kerjasama_keterangan").val("");
		$("#riwayat_kerjasama_add_form").smodal("show");
	};
	RiwayatKerjasamaAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var awal = $("#riwayat_kerjasama_awal").val();
		var akhir = $("#riwayat_kerjasama_akhir").val();
		var keterangan = $("#riwayat_kerjasama_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (awal == "") {
			valid = false;
			invalid_msg += "</br><strong>Waktu Awal</strong> tidak boleh kosong";
			$("#riwayat_kerjasama_awal").addClass("error_field");
		}
		if (akhir == "") {
			valid = false;
			invalid_msg += "</br><strong>Waktu Akhir</strong> tidak boleh kosong";
			$("#riwayat_kerjasama_akhir").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#riwayat_kerjasama_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_riwayat_kerjasama_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	RiwayatKerjasamaAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		var data = this.getRegulerData();
		data['super_command'] = "riwayat_kerjasama";
		data['command'] = "save";
		data['id'] = $("#riwayat_kerjasama_id").val();
		data['id_vendor'] = $("#riwayat_kerjasama_id_vendor").val();
		data['waktu_awal'] = $("#riwayat_kerjasama_awal").val();
		data['waktu_akhir'] = $("#riwayat_kerjasama_akhir").val();
		data['keterangan'] = $("#riwayat_kerjasama_keterangan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				riwayat_kerjasama.setSuperCommand("riwayat_kerjasama");
				riwayat_kerjasama.view();
				$("#riwayat_kerjasama_add_form").smodal("hide");
			}
		);
	};
	RiwayatKerjasamaAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['super_command'] = "riwayat_kerjasama";
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#riwayat_kerjasama_id").val(id);
				$("#riwayat_kerjasama_id_vendor").val($("#vendor_id").val());
				$("#riwayat_kerjasama_nama_vendor").val($("#vendor_nama").val());
				$("#riwayat_kerjasama_awal").val(json.waktu_awal);
				$("#riwayat_kerjasama_akhir").val(json.waktu_akhir);
				$("#riwayat_kerjasama_keterangan").val(json.keterangan);
				$("#riwayat_kerjasama_add_form").smodal("show");
			}
		);
	};
	
	function VendorAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	VendorAction.prototype.constructor = VendorAction;
	VendorAction.prototype = new TableAction();
	VendorAction.prototype.selected = function(json) {
		$("#vendor_id").val(json.id);
		$("#vendor_nama").val(json.nama);
		$("#vendor_npwp").val(json.npwp);
		$("#vendor_alamat").val(json.alamat);
		$("#vendor_kota").val(json.kota);
		$("#vendor_kodepos").val(json.kodepos);
		riwayat_kerjasama.setSuperCommand("riwayat_kerjasama");
		riwayat_kerjasama.view();
	};
	
	var riwayat_kerjasama;
	var vendor;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		$("#smis-chooser-modal").on("show", function() {
			if ($("#smis-chooser-modal .modal-header h3").text() == "VENDOR") {
				$("#smis-chooser-modal").removeClass("full_model");
				$("#smis-chooser-modal").addClass("full_model");
			} else {
				$("#smis-chooser-modal").removeClass("full_model");
			}
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("full_model");
		});
		vendor = new VendorAction(
			"vendor",
			"pembelian",
			"riwayat_kerjasama",
			new Array()
		);
		vendor.setSuperCommand("vendor");
		var riwayat_kerjasama_columns = new Array("id", "id_vendor", "waktu_awal", "waktu_akhir", "keterangan");
		riwayat_kerjasama = new RiwayatKerjasamaAction(
			"riwayat_kerjasama",
			"pembelian",
			"riwayat_kerjasama",
			riwayat_kerjasama_columns
		);
		riwayat_kerjasama.setSuperCommand("riwayat_kerjasama");
		riwayat_kerjasama.view();
	});
</script>