function PembuatanOPLBarangAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PembuatanOPLBarangAction.prototype.constructor = PembuatanOPLBarangAction;
PembuatanOPLBarangAction.prototype = new TableAction();
PembuatanOPLBarangAction.prototype.selected = function(json) {
	showLoading();
	$("#dpembuatan_opl_id_barang").val(json.id);
	$("#dpembuatan_opl_kode_barang").val(json.kode);
	$("#dpembuatan_opl_name_barang").val(json.nama);
	$("#dpembuatan_opl_nama_barang").val(json.nama);
	$("#dpembuatan_opl_nama_jenis_barang").val(json.nama_jenis_barang);
	$("#dpembuatan_opl_satuan").val(json.satuan);
	$("#dpembuatan_opl_konversi").val(json.konversi);
	$("#dpembuatan_opl_satuan_konversi").val(json.satuan_konversi);
	$("#dpembuatan_opl_medis").val(json.medis);
	$("#dpembuatan_opl_inventaris").val(json.inventaris);
	$("ul.typeahead").hide();
	var data = this.getRegulerData();
	data['command'] = "get_last_hpp";
	data['super_command'] = "";
	data['id_obat'] = json.id;
	data['satuan'] = json.satuan;
	data['konversi'] = json.konversi;
	data['satuan_konversi'] = json.satuan_konversi;
	$.post(
		"",
		data,
		function(hpp_response) {
			var json_hpp = JSON.parse(hpp_response);
			if (json_hpp == null) {
				$("#dpembuatan_opl_hna").val("Rp. 0,00");
				$("#dpembuatan_opl_hpp").val("Rp. 0,00");
			}
			else {
				$("#dpembuatan_opl_hna").val(json_hpp.hna);
				$("#dpembuatan_opl_hpp").val(json_hpp.hpp);
			}
			data['command'] = "get_current_stock";
			$.post(
				"",
				data,
				function(sisa_response) {
					var json_sisa = JSON.parse(sisa_response);
					if (json_sisa == null)
						$("#dpembuatan_opl_sisa").val(0);
					else
						$("#dpembuatan_opl_sisa").val(json_sisa.jumlah);
					$("#dpembuatan_opl_jumlah").focus();
					dismissLoading();
				}
			);
		}
	);
};