function RencanaPembelianFarmasiAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
RencanaPembelianFarmasiAction.prototype.constructor = RencanaPembelianFarmasiAction;
RencanaPembelianFarmasiAction.prototype = new TableAction();
RencanaPembelianFarmasiAction.prototype.selected = function(json) {
	$("#pembuatan_opl_f_id").val(json.id);
	$("#pembuatan_opl_id_vendor").val(json.id_vendor);
	$("#pembuatan_opl_kode_vendor").val(json.kode_vendor);
	$("#pembuatan_opl_nama_vendor").val(json.nama_vendor);
	$("#pembuatan_opl_medis").val(json.medis);
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_info_detail_vendor";
	data['super_command'] = "";
	data['id_vendor'] = json.id_vendor;
	showLoading();
	$.post(
		"",
		data,
		function(response) {
			var js_vendor = JSON.parse(response);
			if (js_vendor == null) return;
			$("#pembuatan_opl_alamat_vendor").val(js_vendor.data.alamat);
			$("#pembuatan_opl_kota_vendor").val(js_vendor.data.kota);
			$("#pembuatan_opl_kodepos_vendor").val(js_vendor.data.kodepos);
			data = self.getRegulerData();
			data['command'] = "show_detail_rp";
			data['super_command'] = "";
			data['id_rp'] = json.id;
			data['row_num'] = row_num;
			$.post(
				"",
				data,
				function(rspns) {
					var js_rp = JSON.parse(rspns);
					if (js_rp == null) {
						dismissLoading();
						return;
					}
					var n_rows = $("tbody#dpembuatan_opl_list").children("tr").length;
					for (var i = 0; i < n_rows; i++) {
						var f_id = $("tr#data_" + i + " td#f_id").text();
						if (f_id != "")
							dpembuatan_opl.del(i);
					}
					$("#dpembuatan_opl_list").append(js_rp.html);
					row_num = js_rp.row_num;
					pembuatan_opl.update_total();
					dismissLoading();
				}
			);
		}
	);
};