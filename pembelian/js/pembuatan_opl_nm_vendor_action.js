function PembuatanOPLVendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PembuatanOPLVendorAction.prototype.constructor = PembuatanOPLVendorAction;
PembuatanOPLVendorAction.prototype = new TableAction();
PembuatanOPLVendorAction.prototype.selected = function(json) {
	$("#pembuatan_opl_id_vendor").val(json.id);
	$("#pembuatan_opl_kode_vendor").val(json.kode);
	$("#pembuatan_opl_nama_vendor").val(json.nama);
	$("#pembuatan_opl_alamat_vendor").val(json.alamat);
	$("#pembuatan_opl_kota_vendor").val(json.kota);
	$("#pembuatan_opl_kodepos_vendor").val(json.kodepos);
	$("#pembuatan_opl_medis").val(json.medis);
};