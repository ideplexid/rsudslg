function DPembuatanOPLAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPembuatanOPLAction.prototype.constructor = DPembuatanOPLAction;
DPembuatanOPLAction.prototype = new TableAction();
DPembuatanOPLAction.prototype.validate = function() {
	var valid = true;
	var invalid_message = "";
	var id_barang = $("#dpembuatan_opl_id_barang").val();
	var jumlah_dipesan = $("#dpembuatan_opl_jumlah").val();
	var satuan = $("#dpembuatan_opl_satuan").val();
	var konversi = $("#dpembuatan_opl_konversi").val();
	var satuan_konversi = $("#dpembuatan_opl_satuan_konversi").val();
	var hna = parseFloat($("#dpembuatan_opl_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var hpp = parseFloat($("#dpembuatan_opl_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	$(".error_field").removeClass("error_field");
	if (id_barang == "") {
		valid = false;
		invalid_message += "</br><strong>Barang</strong> tidak boleh kosong.";
		$("#dpembuatan_opl_nama_barang").addClass("error_field");
		$("#dpembuatan_opl_nama_barang").focus();
	}
	if (jumlah_dipesan == "") {
		valid = false;
		invalid_message += "</br><strong>Jumlah</strong> tidak boleh kosong.";
		$("#dpembuatan_opl_jumlah").addClass("error_field");
		$("#dpembuatan_opl_jumlah").focus();
	} else if (!is_numeric(jumlah_dipesan)) {
		valid = false;
		invalid_message += "</br><strong>Jumlah</strong> seharusnya numerik (0-9).";
		$("#dpembuatan_opl_jumlah").addClass("error_field");
		$("#dpembuatan_opl_jumlah").focus();
	}
	if (hna == "" || hna == 0) {
		valid = false;
		invalid_message += "</br><strong>HNA</strong> tidak boleh kosong.";
		$("#dpembuatan_opl_hna").addClass("error_field");
		$("#dpembuatan_opl_hna").focus();
	}
	if (hpp == "" || hpp == 0) {
		valid = false;
		invalid_message += "</br><strong>HPP</strong> tidak boleh kosong.";
		$("#dpembuatan_opl_hpp").addClass("error_field");
		$("#dpembuatan_opl_hpp").focus();
	}
	if (satuan == "") {
		valid = false;
		invalid_message += "</br><strong>Satuan</strong> tidak boleh kosong.";
		$("#dpembuatan_opl_satuan").addClass("error_field");
		$("#dpembuatan_opl_satuan").focus();
	}
	if (konversi == "") {
		valid = false;
		invalid_message += "</br><strong>Konversi</strong> tidak boleh kosong.";
		$("#dpembuatan_opl_konversi").addClass("error_field");
		$("#dpembuatan_opl_konversi").focus();
	}
	if (satuan_konversi == "") {
		valid = false;
		invalid_message += "</br><strong>Sat. Konversi</strong> tidak boleh kosong.";
		$("#dpembuatan_opl_satuan_konversi").addClass("error_field");
		$("#dpembuatan_opl_satuan_konversi").focus();
	}
	if (!valid)
		bootbox.alert(invalid_message);
	return valid;
};
DPembuatanOPLAction.prototype.clear = function() {
	$("#dpembuatan_opl_row_num").val("");
	$("#dpembuatan_opl_id_barang").val("");
	$("#dpembuatan_opl_kode_barang").val("");
	$("#dpembuatan_opl_name_barang").val("");
	$("#dpembuatan_opl_nama_barang").val("");
	$("#dpembuatan_opl_nama_jenis_barang").val("");
	$("#dpembuatan_opl_sisa").val("");
	$("#dpembuatan_opl_jumlah").val("");
	$("#dpembuatan_opl_satuan").val("");
	$("#dpembuatan_opl_konversi").val("");
	$("#dpembuatan_opl_satuan_konversi").val("");
	$("#dpembuatan_opl_hna").val("Rp. 0,00");
	$("#dpembuatan_opl_hpp").val("Rp. 0,00");
};
DPembuatanOPLAction.prototype.save = function() {
	if (!this.validate())
		return;
	var r_num = $("#dpembuatan_opl_row_num").val();
	var id_barang = $("#dpembuatan_opl_id_barang").val();
	var nama_barang = $("#dpembuatan_opl_name_barang").val();
	var kode_barang = $("#dpembuatan_opl_kode_barang").val();
	var nama_jenis_barang = $("#dpembuatan_opl_nama_jenis_barang").val();
	var jumlah_dipesan = parseFloat($("#dpembuatan_opl_jumlah").val());
	var f_jumlah_dipesan = jumlah_dipesan.formatMoney("0", ".", ",");
	var satuan = $("#dpembuatan_opl_satuan").val();
	var konversi = $("#dpembuatan_opl_konversi").val();
	var satuan_konversi = $("#dpembuatan_opl_satuan_konversi").val();
	var hna = parseFloat($("#dpembuatan_opl_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var hpp = parseFloat($("#dpembuatan_opl_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var medis = $("#dpembuatan_opl_medis").val();
	var inventaris = $("#dpembuatan_opl_inventaris").val();
	var f_hna = hna.formatMoney("2", ".", ",");
	var subtotal = jumlah_dipesan * hna;
	var f_subtotal = subtotal.formatMoney("2", ".", ",");
	
	if (r_num.length == 0) {
		$("tbody#dpembuatan_opl_list").append(
			"<tr id='data_" + row_num + "'>" +
				"<td id='nomor'></td>" +
				"<td id='id' style='display: none;'></td>" +
				"<td id='f_id' style='display: none;'></td>" +
				"<td id='id_barang' style='display: none;'>" + id_barang + "</td>" +
				"<td id='kode_barang'><small>" + kode_barang + "</small></td>" +
				"<td id='nama_barang'><small>" + nama_barang + "</small></td>" +
				"<td id='nama_jenis_barang'><small>" + nama_jenis_barang + "</small></td>" +
				"<td id='jumlah_diajukan' style='display: none;'>0</td>" +
				"<td id='jumlah_dipesan' style='display: none;'>" + jumlah_dipesan + "</td>" +
				"<td id='f_jumlah_dipesan'><small><div align='right'>" + f_jumlah_dipesan + "</div></small></td>" +
				"<td id='satuan'><small>" + satuan + "</small></td>" +
				"<td id='hpp' style='display: none;'>" + hpp + "</td>" +
				"<td id='hna' style='display: none;'>" + hna + "</td>" +
				"<td id='f_hna'><small><div align='right'>" + f_hna + "</div></small></td>" +
				"<td id='subtotal' style='display: none;'>" + subtotal + "</td>" +
				"<td id='f_subtotal'><small><div align='right'>" + f_subtotal + "</div></small></td>" +
				"<td id='medis' style='display: none;'>" + medis + "</td>" +
				"<td id='inventaris' style='display: none;'>" + inventaris + "</td>" +
				"<td id='konversi' style='display: none;'>" + konversi + "</td>" +
				"<td id='satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
				"<td>" +	
					"<a href='#' onclick='dpembuatan_opl.edit(" + row_num + ")' class='input btn btn-warning'>" +
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
				"</td>" +
				"<td>" +
					"<a href='#' onclick='dpembuatan_opl.del(" + row_num + ")' class='input btn btn-danger'>" +
						"<i class='fa fa-trash-o'></i>" +
					"</a>" +
				"</td>" +
			"</tr>"
		);
		row_num++;
	} else {
		$("tr#data_" + r_num + " td#jumlah_dipesan").html(jumlah_dipesan);
		$("tr#data_" + r_num + " td#f_jumlah_dipesan").html("<small><div align='right'>" + f_jumlah_dipesan + "</div></small>");
		$("tr#data_" + r_num + " td#hpp").html(hpp);
		$("tr#data_" + r_num + " td#hna").html(hna);
		$("tr#data_" + r_num + " td#f_hna").html("<small><div align='right'>" + f_hna + "</div></small>");
		$("tr#data_" + r_num + " td#subtotal").html(subtotal);
		$("tr#data_" + r_num + " td#f_subtotal").html("<small><div align='right'>" + f_subtotal + "</div></small>");
	}
	this.clear();
	pembuatan_opl.update_total();
	this.setEditMode("false");
};
DPembuatanOPLAction.prototype.setEditMode = function(enable) {
	if (enable == "true") {
		$("#dpembuatan_opl_nama_barang").removeClass("smis-one-option-input");
		$("#dpembuatan_opl_nama_barang").removeAttr("disabled");
		$("#dpembuatan_opl_nama_barang").attr("disabled", "disabled");
		$("#barang_browse").hide();
		$("#dpembuatan_opl_hna").removeAttr("disabled");
		$("#dpembuatan_opl_hna").attr("disabled", "disabled");
		$("#dpembuatan_opl_hpp").removeAttr("disabled");
		$("#dpembuatan_opl_hpp").attr("disabled", "disabled");
		$("#pembuatan_opl_form_save").hide();
		$("#pembuatan_opl_form_update").show();
		$("#pembuatan_opl_form_cancel").show();
	} else {
		$("#dpembuatan_opl_nama_barang").addClass("smis-one-option-input");
		$("#dpembuatan_opl_nama_barang").removeAttr("disabled");
		$("#barang_browse").show();
		$("#dpembuatan_opl_hna").removeAttr("disabled");
		$("#dpembuatan_opl_hpp").removeAttr("disabled");
		$("#pembuatan_opl_form_save").show();
		$("#pembuatan_opl_form_update").hide();
		$("#pembuatan_opl_form_cancel").hide();
	}
};
DPembuatanOPLAction.prototype.edit = function(r_num) {
	var id_barang = $("tr#data_" + r_num + " td#id_barang").text();
	var kode_barang = $("tr#data_" + r_num + " td#kode_barang").text();
	var nama_barang = $("tr#data_" + r_num + " td#nama_barang").text();
	var nama_jenis_barang = $("tr#data_" + r_num + " td#nama_jenis_barang").text();
	var jumlah_dipesan = parseFloat($("tr#data_" + r_num + " td#jumlah_dipesan").text());
	var satuan = $("tr#data_" + r_num + " td#satuan").text();
	var konversi = $("tr#data_" + r_num + " td#konversi").text();
	var satuan_konversi = $("tr#data_" + r_num + " td#satuan_konversi").text();
	var hna = "Rp. " + parseFloat($("tr#data_" + r_num + " td#hna").text()).formatMoney("2", ".", ",");
	var hpp = "Rp. " + parseFloat($("tr#data_" + r_num + " td#hpp").text()).formatMoney("2", ".", ",");
	this.setEditMode("true");
	$("#dpembuatan_opl_row_num").val(r_num);
	$("#dpembuatan_opl_id_barang").val(id_barang);
	$("#dpembuatan_opl_kode_barang").val(kode_barang);
	$("#dpembuatan_opl_name_barang").val(nama_barang);
	$("#dpembuatan_opl_nama_barang").val(nama_barang);
	$("#dpembuatan_opl_nama_jenis_barang").val(nama_jenis_barang);
	$("#dpembuatan_opl_jumlah").val(jumlah_dipesan);
	$("#dpembuatan_opl_satuan").val(satuan);
	$("#dpembuatan_opl_konversi").val(konversi);
	$("#dpembuatan_opl_satuan_konversi").val(satuan_konversi);
	$("#dpembuatan_opl_hna").val(hna);
	$("#dpembuatan_opl_hpp").val(hpp);
};
DPembuatanOPLAction.prototype.del = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	pembuatan_opl.update_total();
};
DPembuatanOPLAction.prototype.cancel = function() {
	this.clear();
	this.setEditMode("false");
};