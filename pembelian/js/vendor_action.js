function VendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
VendorAction.prototype.constructor = VendorAction;
VendorAction.prototype = new TableAction();
VendorAction.prototype.cekSave = function() {
	var result = TableAction.prototype.cekSave.call(this);
	var err_message = $("#modal_alert_vendor_add_form div").html();
	var alamat = $("#vendor_alamat").val();
	if (alamat == "") {
		result = false;
		err_message += "</br><strong>Alamat</strong> tidak boleh kosong";
		$("#vendor_alamat").addClass("error_field");
	}
	if (!result) {
		$("#modal_alert_vendor_add_form div").html(err_message);
	}
	return result;
};
VendorAction.prototype.export_xls = function() {
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	postForm(data);
};