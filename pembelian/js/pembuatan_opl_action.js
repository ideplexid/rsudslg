function PembuatanOPLAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PembuatanOPLAction.prototype.constructor = PembuatanOPLAction;
PembuatanOPLAction.prototype = new TableAction();
PembuatanOPLAction.prototype.show_reguler_opl_add_form = function() {
	var data = this.getRegulerData();
	data['action'] = "pembuatan_opl_form";
	data['super_command'] = "";
	data['id'] = "";
	data['f_id'] = "";
	data['nomor'] = "";
	data['tanggal'] = "";
	data['id_vendor'] = "";
	data['kode_vendor'] = "";
	data['nama_vendor'] = "";
	data['alamat_vendor'] = "";
	data['kota_vendor'] = "";
	data['kodepos_vendor'] = "";
	data['medis'] = "";
	data['ppn'] = "";
	data['editable'] = "true";
	LoadSmisPage(data);
};
PembuatanOPLAction.prototype.show_mandiri_opl_add_form = function() {
	var data = this.getRegulerData();
	data['action'] = "pembuatan_opl_mandiri_form";
	data['super_command'] = "";
	data['id'] = "";
	data['f_id'] = "";
	data['nomor'] = "";
	data['tanggal'] = "";
	data['id_vendor'] = "";
	data['kode_vendor'] = "";
	data['nama_vendor'] = "";
	data['alamat_vendor'] = "";
	data['kota_vendor'] = "";
	data['kodepos_vendor'] = "";
	data['medis'] = "";
	data['ppn'] = "";
	data['editable'] = "true";
	LoadSmisPage(data);	
};
PembuatanOPLAction.prototype.show_detail_form = function(id) {
	if ($("#total").length != 0)
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_detail";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("#dpembuatan_opl_list").html(json.html);
			self.update_total();
			row_num = json.row_num;
		}
	);
};
PembuatanOPLAction.prototype.get_footer = function() {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_footer";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#dpembuatan_opl_list").after(json.html);
		}
	);
};
PembuatanOPLAction.prototype.update_total = function() {
	var num_rows = $("tbody#dpembuatan_opl_list").children("tr").length;
	var total = 0;
	var nomor = 1;
	var persen_ppn = $("#pembuatan_opl_ppn").val();
	for (var i = 0; i < num_rows; i++) {
		var hna = parseFloat($("tbody#dpembuatan_opl_list tr:eq(" + i + ") td#hna").text());
		var hpp = hna * (1 + (persen_ppn / 100));
		$("tbody#dpembuatan_opl_list tr:eq(" + i + ") td#hpp").html(hpp);
		var subtotal = parseFloat($("tbody#dpembuatan_opl_list tr:eq(" + i + ") td#subtotal").text());
		var deleted = $("tbody#dpembuatan_opl_list tr:eq(" + i + ")").attr("class") == "deleted" ? true : false;
		if (!deleted) {
			total += subtotal;
			$("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(0)").html("<small>" + nomor + "</small>");
			nomor++;
		}
	}
	var ppn = total * persen_ppn / 100;
	var total_tagihan = total + ppn;
	$("#total").html("<small><strong><div align='right'>" + total.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#ppn").html("<small><strong><div align='right'>" + ppn.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#total_tagihan").html("<small><strong><div align='right'>" + total_tagihan.formatMoney("2", ".", ",") + "</div></strong></small>");
};
PembuatanOPLAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#pembuatan_opl_id").val();
	data['f_id'] = $("#pembuatan_opl_f_id").val();
	data['tanggal'] = $("#pembuatan_opl_tanggal").val();
	data['id_vendor'] = $("#pembuatan_opl_id_vendor").val();
	data['kode_vendor'] = $("#pembuatan_opl_kode_vendor").val();
	data['nama_vendor'] = $("#pembuatan_opl_nama_vendor").val();
	data['alamat_vendor'] = $("#pembuatan_opl_alamat_vendor").val();
	data['kota_vendor'] = $("#pembuatan_opl_kota_vendor").val();
	data['kodepos_vendor'] = $("#pembuatan_opl_kodepos_vendor").val();
	data['medis'] = $("#pembuatan_opl_medis").val();
	data['ppn'] = $("#pembuatan_opl_ppn").val();
	data['keterangan'] = "-";
	var num_rows = $("tbody#dpembuatan_opl_list").children("tr").length;
	var detail = {};
	for (var i = 0; i < num_rows; i++) {
		var item = {
			id 					: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(1)").text(),
			f_id				: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(2)").text(),
			id_barang			: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(3)").text(),
			kode_barang			: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(4)").text(),
			nama_barang			: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(5)").text(),
			nama_jenis_barang	: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(6)").text(),
			jumlah_diajukan		: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(7)").text(),
			jumlah_dipesan		: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(8)").text(),
			satuan				: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(10)").text(),
			konversi			: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(18)").text(),
			satuan_konversi		: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(19)").text(),
			hpp					: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(11)").text(),
			medis				: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(16)").text(),
			inventaris			: $("tbody#dpembuatan_opl_list tr:eq(" + i + ") td:eq(17)").text(),
			deleted				: $("tbody#dpembuatan_opl_list tr:eq(" + i + ")").hasClass("deleted")
		};
		detail[i] = item;
	}
	data['detail'] = JSON.stringify(detail);
	return data;
};
PembuatanOPLAction.prototype.validate = function() {
	var valid = true;
	var invalid_message = "";
	var tanggal = $("#pembuatan_opl_tanggal").val();
	var id_vendor = $("#pembuatan_opl_id_vendor").val();
	$(".error_field").removeClass("error_field");
	if (tanggal == "") {
		valid = false;
		invalid_message += "</br><strong>Tanggal</strong> tidak boleh kosong.";
		$("#pembuatan_opl_tanggal").addClass("error_field");
		$("#pembuatan_opl_tanggal").focus();
	}
	if (id_vendor == "") {
		valid = false;
		invalid_message += "</br><strong>Vendor</strong> tidak boleh kosong.";
		$("#pembuatan_opl_nama_vendor").addClass("error_field");
		$("#rencana_pembelian_farmasi_browse").focus();
	}
	if (!valid)
		bootbox.alert(invalid_message);
	return valid;
};
PembuatanOPLAction.prototype.save = function() {
	if (!this.validate())
		return;
	var self = this;
	var data = this.getSaveData();
	bootbox.dialog({
		message : "Yakin melakukan penyimpanan OPL ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
PembuatanOPLAction.prototype.edit = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			console.log(json);
			if (json.f_id > 0)
				data['action'] = "pembuatan_opl_form";
			else
				data['action'] = "pembuatan_opl_mandiri_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['f_id'] = json.f_id;
			data['nomor'] = json.nomor;
			data['tanggal'] = json.tanggal;
			data['id_vendor'] = json.id_vendor;
			data['kode_vendor'] = json.kode_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['alamat_vendor'] = json.alamat_vendor;
			data['kota_vendor'] = json.kota_vendor;
			data['kodepos_vendor'] = json.kodepos_vendor;
			data['medis'] = json.medis;
			data['ppn'] = json.ppn;
			data['editable'] = "true";
			LoadSmisPage(data);
		}
	);
};
PembuatanOPLAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			data['action'] = "pembuatan_opl_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['f_id'] = json.f_id;
			data['nomor'] = json.nomor;
			data['tanggal'] = json.tanggal;
			data['id_vendor'] = json.id_vendor;
			data['kode_vendor'] = json.kode_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['alamat_vendor'] = json.alamat_vendor;
			data['kota_vendor'] = json.kota_vendor;
			data['kodepos_vendor'] = json.kodepos_vendor;
			data['medis'] = json.medis;
			data['ppn'] = json.ppn;
			data['editable'] = "false";
			LoadSmisPage(data);
		}
	);
};
PembuatanOPLAction.prototype.lock_opl = function(id) {
	if (!this.validate())
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = id;
	data['lock_opl'] = 1;
	bootbox.dialog({
		message : "Yakin melakukan penguncian OPL ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.view();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
PembuatanOPLAction.prototype.export_xls = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "export_xls";
	data['id'] = id;
	postForm(data);
};
PembuatanOPLAction.prototype.back = function() {
	var data = this.getRegulerData();
	data['action'] = "pembuatan_opl";
	data['super_command'] = "";
	data['kriteria'] = "";
	data['max'] = 5;
	data['number'] = 0;
	LoadSmisPage(data);
};