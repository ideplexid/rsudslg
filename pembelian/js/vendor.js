var vendor;
$(document).ready(function() {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	var vendor_columns = new Array("id", "kode", "kode_ppn", "nama", "medis", "npwp", "alamat", "kota", "kodepos", "telpon", "nama_cp", "telpon_cp", "keterangan");
	vendor = new VendorAction(
		"vendor",
		"pembelian",
		"vendor",
		vendor_columns
	);
	vendor.view();
});