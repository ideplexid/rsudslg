function PersetujuanOPLAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PersetujuanOPLAction.prototype.constructor = PersetujuanOPLAction;
PersetujuanOPLAction.prototype = new TableAction();
PersetujuanOPLAction.prototype.approve = function() {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#persetujuan_opl_id").val();
	data['lock_acc'] = "1";
	bootbox.dialog({
		message : "Yakin memberikan persetujuan OPL ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
PersetujuanOPLAction.prototype.edit = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) 
				return;
			data = self.getRegulerData();
			data['action'] = "persetujuan_opl_form";
			data['super_command'] = "";
			data['id'] = json.id;
			data['f_id'] = json.f_id;
			data['nomor'] = json.nomor;
			data['tanggal'] = json.tanggal;
			data['id_vendor'] = json.id_vendor;
			data['kode_vendor'] = json.kode_vendor;
			data['nama_vendor'] = json.nama_vendor;
			data['alamat_vendor'] = json.alamat_vendor;
			data['kota_vendor'] = json.kota_vendor;
			data['kodepos_vendor'] = json.kodepos_vendor;
			data['medis'] = json.medis;
			data['editable'] = "false";
			LoadSmisPage(data);
		}
	);
};
PersetujuanOPLAction.prototype.show_detail_form = function(id) {
	if ($("#total").length != 0)
		return;
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_detail";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			console.log(json);
			$("#dpersetujuan_opl_list").html(json.html);
			self.update_total();
			row_num = json.row_num;
		}
	);
};
PersetujuanOPLAction.prototype.get_footer = function() {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "show_footer";
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			$("tbody#dpersetujuan_opl_list").after(json.html);
		}
	);
};
PersetujuanOPLAction.prototype.update_total = function() {
	var num_rows = $("tbody#dpersetujuan_opl_list").children("tr").length;
	var total = 0;
	var nomor = 1;
	for (var i = 0; i < num_rows; i++) {
		var subtotal = parseFloat($("tbody#dpersetujuan_opl_list tr:eq(" + i + ") td#subtotal").text());
		var deleted = $("tbody#dpersetujuan_opl_list tr:eq(" + i + ")").attr("class") == "deleted" ? true : false;
		if (!deleted) {
			total += subtotal;
			$("tbody#dpersetujuan_opl_list tr:eq(" + i + ") td:eq(0)").html("<small>" + nomor + "</small>");
			nomor++;
		}
	}
	var ppn = total * 0.1;
	var total_tagihan = total + ppn;
	$("#total").html("<small><strong><div align='right'>" + total.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#ppn").html("<small><strong><div align='right'>" + ppn.formatMoney("2", ".", ",") + "</div></strong></small>");
	$("#total_tagihan").html("<small><strong><div align='right'>" + total_tagihan.formatMoney("2", ".", ",") + "</div></strong></small>");
};
PersetujuanOPLAction.prototype.disapprove = function() {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "save";
	data['id'] = $("#persetujuan_opl_id").val();
	data['lock_opl'] = 0;
	bootbox.dialog({
		message : "Yakin mengembalikan OPL ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
PersetujuanOPLAction.prototype.back = function() {
	var data = this.getRegulerData();
	data['action'] = "persetujuan_opl";
	data['super_command'] = "";
	data['kriteria'] = "";
	data['max'] = 5;
	data['number'] = 0;
	LoadSmisPage(data);
};