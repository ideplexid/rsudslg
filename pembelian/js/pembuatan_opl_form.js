var pembuatan_opl;
var dpembuatan_opl;
var rencana_pembelian_farmasi;
var barang;
var row_num;
$(document).ready(function() {
	$(".mydate").datepicker();
	barang = new PembuatanOPLBarangAction(
		"barang",
		"pembelian",
		"pembuatan_opl_form",
		new Array()
	);
	barang.setSuperCommand("barang");
	rencana_pembelian_farmasi = new RencanaPembelianFarmasiAction(
		"rencana_pembelian_farmasi",
		"pembelian",
		"pembuatan_opl_form",
		new Array()
	);
	rencana_pembelian_farmasi.setSuperCommand("rencana_pembelian_farmasi");
	dpembuatan_opl = new DPembuatanOPLAction(
		"dpembuatan_opl",
		"pembelian",
		"pembuatan_opl_form",
		new Array("hna", "hpp")
	);
	pembuatan_opl = new PembuatanOPLAction(
		"pembuatan_opl",
		"pembelian",
		"pembuatan_opl_form",
		new Array()
	);

	var id = $("#pembuatan_opl_id").val();
	row_num = 0;
	pembuatan_opl.get_footer();
	pembuatan_opl.show_detail_form(id);
	dpembuatan_opl.setEditMode("false");
	$("#pembuatan_opl_tanggal").focus();

	$("#pembuatan_opl_tanggal").keypress(function(e) {
		if (e.which == 13) {
			$("#rencana_pembelian_farmasi_browse").focus();
			$("div.datepicker").hide();
		}
	});
	$("#rencana_pembelian_farmasi_browse").keypress(function(e) {
		if (e.which == 13) {
			$("#rencana_pembelian_farmasi_browse").trigger("click");
		}
	});

	var barang_data = barang.getViewData();
	$("#dpembuatan_opl_nama_barang").typeahead({
		minLength	: 3,
		source		: function (query, process) {
			var $items = new Array;
			$items = [""];                
			barang_data['kriteria'] = $('#dpembuatan_opl_nama_barang').val();
			$.ajax({
				url		: "",
				type	: "POST",
				data	: barang_data,
				success	: function(response) {
					var json = getContent(response);
					var t_data = json.d.data;
					$items = [""];      				
					$.map(t_data, function(data) {
						var group;
						group = {
							id		: data.id,
							name	: data.nama, 
							kode	: data.kode, 
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = "";
								value +=  this.kode + " - " + this.name;
								if(typeof(this.level) != "undefined") {
									value += " <span class='pull-right muted'>";
									value += this.level;
									value += "</span>";
								}
								return String.prototype.replace.apply("<div class='typeaheadiv'>" + value + "</div>", arguments);
							}
						};
						$items.push(group);
					});
					process($items);
				}
			});
		},
		updater		: function (item) {
			var item = JSON.parse(item);  
			barang.select(item.id);  
			$("#dpembuatan_opl_jumlah").focus();       
			return item.name;
		}
	});
	$("#dpembuatan_opl_jumlah").keypress(function(e) {
		if (e.which == 13) {
			$("#dpembuatan_opl_hna").focus();
		}
	});
	$("#dpembuatan_opl_hna").keydown(function(e) {
		if (e.which == 13) {
			$("#dpembuatan_opl_hpp").focus();
		}
		if ($("#dpembuatan_opl_hna").val() == "") {
			$("#dpembuatan_opl_hna").val("Rp. 0,00");
			$("#dpembuatan_opl_hpp").val("Rp. 0,00");
		} else {
			var hna = parseFloat($("#dpembuatan_opl_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var ppn = parseFloat($("#pembuatan_opl_ppn").val());
			var hpp = hna * (100 + ppn) / 100;
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpembuatan_opl_hpp").val(f_hpp);
		}
	});
	$("#dpembuatan_opl_hpp").keydown(function(e) {
		if (e.which == 13) {
			$("#pembuatan_opl_form_save").trigger("click");
		}
		if ($("#dpembuatan_opl_hpp").val() == "") {
			$("#dpembuatan_opl_hna").val("Rp. 0,00");
			$("#dpembuatan_opl_hpp").val("Rp. 0,00");
		} else {
			var hpp = parseFloat($("#dpembuatan_opl_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var ppn = parseFloat($("#pembuatan_opl_ppn").val());
			var hna = hpp / ((100 + ppn) / 100);
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpembuatan_opl_hna").val(f_hna);
		}
	});
	$("#dpembuatan_opl_hna").on("change", function() {
		if ($("#dpembuatan_opl_hna").val() == "") {
			$("#dpembuatan_opl_hna").val("Rp. 0,00");
			$("#dpembuatan_opl_hpp").val("Rp. 0,00");
		} else {
			var hna = parseFloat($("#dpembuatan_opl_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var ppn = parseFloat($("#pembuatan_opl_ppn").val());
			var hpp = hna * ((100 + ppn) / 100);
			var f_hpp = "Rp. " + hpp.formatMoney("2", ".", ",");
			$("#dpembuatan_opl_hpp").val(f_hpp);
		}
	});
	$("#dpembuatan_opl_hpp").on("change", function() {
		if ($("#dpembuatan_opl_hpp").val() == "") {
			$("#dpembuatan_opl_hna").val("Rp. 0,00");
			$("#dpembuatan_opl_hpp").val("Rp. 0,00");
		} else {
			var hpp = parseFloat($("#dpembuatan_opl_hpp").val().replace(/[^0-9-,]/g, '').replace(",", "."));
			var ppn = parseFloat($("#pembuatan_opl_ppn").val());
			var hna = hpp / ((100 + ppn) / 100);
			var f_hna = "Rp. " + hna.formatMoney("2", ".", ",");
			$("#dpembuatan_opl_hna").val(f_hna);
		}
	});
	$("#pembuatan_opl_ppn").on("change", function () {
		$("#dpembuatan_opl_hna").trigger("change");
		pembuatan_opl.update_total();
	});
});