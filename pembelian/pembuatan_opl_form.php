<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("pembelian/responder/PembuatanOPLDBResponder.php");
	global $db;
	
	$header_form = new Form("", "", "Pembelian : Pembuatan OPL Farmasi Reguler");

	$id = isset($_POST['id']) ? $_POST['id'] : "";
	$f_id = isset($_POST['f_id']) ? $_POST['f_id'] : "";
	$tanggal = !isset($_POST['tanggal']) || $_POST['tanggal'] == "" ? date("Y-m-d") : $_POST['tanggal'];
	$nomor = isset($_POST['nomor']) ? $_POST['nomor'] : "";
	$id_vendor = isset($_POST['id_vendor']) ? $_POST['id_vendor'] : "";
	$kode_vendor = isset($_POST['kode_vendor']) ? $_POST['kode_vendor'] : "";
	$nama_vendor = isset($_POST['nama_vendor']) ? $_POST['nama_vendor'] : "";
	$alamat_vendor = isset($_POST['alamat_vendor']) ? $_POST['alamat_vendor'] : "";
	$kota_vendor = isset($_POST['kota_vendor']) ? $_POST['kota_vendor'] : "";
	$kodepos_vendor = isset($_POST['kodepos_vendor']) ? $_POST['kodepos_vendor'] : "";
	$medis = isset($_POST['medis']) ? $_POST['medis'] : 1;
	$ppn = isset($_POST['ppn']) && $_POST['ppn'] != "" ? $_POST['ppn'] : 11;
	$editable = isset($_POST['editable']) ? $_POST['editable'] : "false";
	
	$id_hidden = new Hidden("pembuatan_opl_id", "pembuatan_opl_id", $id);
	$header_form->addElement("", $id_hidden);
	$f_id_hidden = new Hidden("pembuatan_opl_f_id", "pembuatan_opl_f_id", $f_id);
	$header_form->addElement("", $f_id_hidden);
	$medis_hidden = new Hidden("pembuatan_opl_medis", "pembuatan_opl_medis", $medis);
	$header_form->addElement("", $medis_hidden);
	$tanggal_text = new Text("pembuatan_opl_tanggal", "pembuatan_opl_tanggal", $tanggal);
	if ($editable == "true") {
		$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
		$tanggal_text->setClass("mydate");
	}
	else
		$tanggal_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Tanggal", $tanggal_text);
	$nomor_text = new Text("pembuatan_opl_nomor", "pembuatan_opl_nomor", $nomor);
	$nomor_text->setAtribute("disabled='disabled'");
	$header_form->addElement("No. OPL", $nomor_text);
	$id_vendor_hidden = new Hidden("pembuatan_opl_id_vendor", "pembuatan_opl_id_vendor", $id_vendor);
	$header_form->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("pembuatan_opl_nama_vendor", "pembuatan_opl_nama_vendor", $nama_vendor);
	$nama_vendor_text->setAtribute("disabled='disabled'");
	if ($editable == "true")
		$nama_vendor_text->setClass("smis-one-option-input");
	$vendor_browse_button = new Button("", "", "Pilih");
	$vendor_browse_button->setClass("btn-info");
	$vendor_browse_button->setIsButton(Button::$ICONIC);
	$vendor_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$vendor_browse_button->setAction("rencana_pembelian_farmasi.chooser('rencana_pembelian_farmasi', 'rencana_pembelian_farmasi', 'rencana_pembelian_farmasi', rencana_pembelian_farmasi)");
	$vendor_browse_button->setAtribute("id='rencana_pembelian_farmasi_browse'");
	$vendor_input_group = new InputGroup("");
	$vendor_input_group->addComponent($nama_vendor_text);
	$vendor_input_group->addComponent($vendor_browse_button);
	if ($editable == "true")
		$header_form->addElement("Nama Vendor", $vendor_input_group);
	else
		$header_form->addElement("Nama Vendor", $nama_vendor_text);
	$ppn_option = new OptionBuilder();
	if ($ppn == 0) {
		$ppn_option->add("0%", "0", "1");
	} else {
		$ppn_option->add("0%", "0");
	}
	if ($ppn == 10) {
		$ppn_option->add("10%", "10", "1");
	} else {
		$ppn_option->add("10%", "10");
	}
	if ($ppn == 11) {
		$ppn_option->add("11%", "11", "1");
	} else {
		$ppn_option->add("11%", "11");
	}
	$ppn_select = new Select("pembuatan_opl_ppn", "pembuatan_opl_ppn", $ppn_option->getContent());
	if ($editable != "true")
		$ppn_select->setAtribute("disabled='disabled'");
	$header_form->addElement("PPn", $ppn_select);
	$kode_vendor_hidden = new Hidden("pembuatan_opl_kode_vendor", "pembuatan_opl_kode_vendor", $kode_vendor);
	$header_form->addElement("", $kode_vendor_hidden);
	$alamat_vendor_hidden = new Hidden("pembuatan_opl_alamat_vendor", "pembuatan_opl_alamat_vendor", "");
	$header_form->addElement("", $alamat_vendor_hidden);
	$kota_vendor_hidden = new Hidden("pembuatan_opl_kota_vendor", "pembuatan_opl_kota_vendor", "");
	$header_form->addElement("", $kota_vendor_hidden);
	$kodepos_vendor_hidden = new Hidden("pembuatan_opl_kodepos_vendor", "pembuatan_opl_kodepos_vendor", "");
	$header_form->addElement("", $kodepos_vendor_hidden);
	
	$detail_form = new Form("", "", "");
	$row_num_hidden = new Hidden("dpembuatan_opl_row_num", "dpembuatan_opl_row_num", "");
	$detail_form->addElement("", $row_num_hidden);
	$id_barang_hidden = new Hidden("dpembuatan_opl_id_barang", "dpembuatan_opl_id_barang", "");
	$detail_form->addElement("", $id_barang_hidden);
	$name_barang_hidden = new Hidden("dpembuatan_opl_name_barang", "dpembuatan_opl_name_barang", "");
	$detail_form->addElement("", $name_barang_hidden);
	$nama_barang_text = new Text("dpembuatan_opl_nama_barang", "dpembuatan_opl_nama_barang", "");
	$nama_barang_text->setClass("smis-one-option-input");
	$barang_browse_button = new Button("", "", "Pilih");
	$barang_browse_button->setClass("btn-info");
	$barang_browse_button->setIsButton(Button::$ICONIC);
	$barang_browse_button->setIcon("icon-white ".Button::$icon_list_alt);
	$barang_browse_button->setAction("barang.chooser('barang', 'barang_button', 'barang', barang)");
	$barang_browse_button->setAtribute("id='barang_browse'");
	$barang_input_group = new InputGroup("");
	$barang_input_group->addComponent($nama_barang_text);
	$barang_input_group->addComponent($barang_browse_button);
	$detail_form->addElement("Nama Barang", $barang_input_group);
	$kode_barang_text = new Text("dpembuatan_opl_kode_barang", "dpembuatan_opl_kode_barang", "");
	$kode_barang_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Kode Barang", $kode_barang_text);
	$nama_jenis_barang_text = new Text("dpembuatan_opl_nama_jenis_barang", "dpembuatan_opl_nama_jenis_barang", "");
	$nama_jenis_barang_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Jenis Barang", $nama_jenis_barang_text);
	$sisa_text = new Text("dpembuatan_opl_sisa", "dpembuatan_opl_sisa", "");
	$sisa_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Persediaan", $sisa_text);
	$jumlah_text = new Text("dpembuatan_opl_jumlah", "dpembuatan_opl_jumlah", "");
	$detail_form->addElement("Jml. Disetujui", $jumlah_text);
	$satuan_text = new Text("dpembuatan_opl_satuan", "dpembuatan_opl_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Satuan", $satuan_text);
	$konversi_text = new Text("dpembuatan_opl_konversi", "dpembuatan_opl_konversi", "");
	$konversi_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Konversi", $konversi_text);
	$satuan_konversi_text = new Text("dpembuatan_opl_satuan_konversi", "dpembuatan_opl_satuan_konversi", "");
	$satuan_konversi_text->setAtribute("disabled='disabled'");
	$detail_form->addElement("Sat. Konversi", $satuan_konversi_text);
	$hna_text = new Text("dpembuatan_opl_hna", "dpembuatan_opl_hna", "");
	$hna_text->setTypical("money");
	$hna_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$detail_form->addElement("HNA", $hna_text);
	$hpp_text = new Text("dpembuatan_opl_hpp", "dpembuatan_opl_hpp", "");
	$hpp_text->setTypical("money");
	$hpp_text->setAtribute("data-thousands='.' data-decimal=',' data-prefix='Rp. '  data-precision='2'");
	$detail_form->addElement("HNA + PPn", $hpp_text);
	$medis_hidden = new Hidden("dpembuatan_opl_medis", "dpembuatan_opl_medis", "");
	$detail_form->addElement("", $medis_hidden);
	$inventaris_hidden = new Hidden("dpembuatan_opl_inventaris", "dpembuatan_opl_inventaris", "");
	$detail_form->addElement("", $inventaris_hidden);

	$save_button = new Button("", "", "Tambahkan");
	$save_button->setClass("btn-info");
	$save_button->setIsButton(Button::$ICONIC_TEXT);
	$save_button->setIcon("fa fa-chevron-right");
	$save_button->setAction("dpembuatan_opl.save()");
	$save_button->setAtribute("id='pembuatan_opl_form_save'");

	$update_button = new Button("", "", "Perbarui");
	$update_button->setClass("btn-info");
	$update_button->setIsButton(Button::$ICONIC_TEXT);
	$update_button->setIcon("fa fa-chevron-right");
	$update_button->setAction("dpembuatan_opl.save()");
	$update_button->setAtribute("id='pembuatan_opl_form_update'");

	$cancel_button = new Button("", "", "Batal");
	$cancel_button->setClass("btn-danger");
	$cancel_button->setIsButton(Button::$ICONIC_TEXT);
	$cancel_button->setIcon("icon-white icon-remove");
	$cancel_button->setAction("dpembuatan_opl.cancel()");
	$cancel_button->setAtribute("id='pembuatan_opl_form_cancel'");	

	$button_group = new ButtonGroup("");
	$button_group->addButton($update_button);
	$button_group->addButton($cancel_button);

	$detail_form->addElement("", $save_button);
	$detail_form->addElement("", $button_group);
	
	$table = new Table(
		array("No.", "Kode Barang", "Nama Barang", "Jenis Barang", "Jumlah Dipesan", "Satuan", "HNA", "Subtotal", "Ubah", "Hapus"),
		"",
		null,
		true
	);
	$table->setName("dpembuatan_opl");
	$table->setFooterVisible(false);
	$table->setAction(false);
	
	$button_group = new ButtonGroup("");
	$back_button = new Button("", "", "Kembali");
	$back_button->setClass("btn-inverse");
	$back_button->setIsButton(Button::$ICONIC_TEXT);
	$back_button->setIcon("fa fa-chevron-left");
	$back_button->setAction("pembuatan_opl.back()");
	$back_button->setAtribute("id='pembuatan_opl_back'");
	$button_group->addButton($back_button);
	if ($editable == "true") {
		$save_button = new Button("", "", "Simpan OPL");
		$save_button->setClass("btn-success");
		$save_button->setIsButton(Button::$ICONIC_TEXT);
		$save_button->setIcon("fa fa-lock");
		$save_button->setAction("pembuatan_opl.save()");
		$save_button->setAtribute("id='pembuatan_opl_save'");
		$button_group->addButton($save_button);
	}
	
	$rpf_table = new Table(
		array("No.", "No. RP", "Tanggal", "Vendor"),
		"",
		null,
		true
	);
	$rpf_table->setName("rencana_pembelian_farmasi");
	$rpf_table->setModel(Table::$SELECT);
	$rpf_adapter = new SimpleAdapter(true, "No.");
	$rpf_adapter->add("No. RP", "id", "digit8");
	$rpf_adapter->add("Tanggal", "tanggal", "date d-m-Y");
	$rpf_adapter->add("Vendor", "nama_vendor");
	$rpf_service_responder = new ServiceResponder(
		$db,
		$rpf_table,
		$rpf_adapter,
		"get_daftar_rencana_pembelian_farmasi"
	);
	$barang_table = new Table(
		array("No.", "Kode", "Barang", "Jenis Barang"),
		"",
		null,
		true
	);
	$barang_table->setName("barang");
	$barang_table->setModel(Table::$SELECT);
	$barang_adapter = new SimpleAdapter(true, "No.");
	$barang_adapter->add("Kode", "kode");
	$barang_adapter->add("Barang", "nama");
	$barang_adapter->add("Jenis Barang", "nama_jenis_barang");
	$barang_service_responder = new ServiceResponder(
		$db,
		$barang_table,
		$barang_adapter,
		"get_daftar_barang_m"
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("rencana_pembelian_farmasi", $rpf_service_responder);
	$super_command->addResponder("barang", $barang_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_info_detail_vendor") {
			$id = $_POST['id_vendor'];
			$dbtable = new DBTable($db, "smis_pb_vendor");
			$row = $dbtable->select($id);
			$data = array();
			$data['data'] = $row;
			echo json_encode($data);
		} else if ($_POST['command'] == "show_detail_rp") {
			$persentase_ppn = $_POST['ppn'];
			$id = $_POST['id_rp'];
			$row_num = $_POST['row_num'];
			$params = array();
			$params['id_rp'] = $id;
			$consumer_service = new ServiceConsumer(
				$db,
				"get_detail_rencana_pembelian_farmasi",
				$params
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$rows = json_decode(json_encode($content[0]['detail']), false);
			$html = "";
			foreach ($rows as $row) {
				if ($row->jumlah_diajukan > 0) {
					$subtotal = $row->jumlah_diajukan * $row->hps / ((100 + $persentase_ppn) / 100);
					$f_subtotal = ArrayAdapter::format("money", $subtotal);
					$html_edit_action = "<a href='#' onclick='dpembuatan_opl.edit(" . $row_num . ")' class='input btn btn-warning'><i class='icon-edit icon-white'></i></a>";
					$html_del_action = "";
					$html .= "
						<tr id='data_" . $row_num . "'>
							<td id='nomor'></td>
							<td id='id' style='display: none;'></td>
							<td id='f_id' style='display: none;'>" . $row->id . "</td>
							<td id='id_barang' style='display: none;'>" . $row->id_barang . "</td>
							<td id='kode_barang'><small>" . $row->kode_barang . "</small></td>
							<td id='nama_barang'><small>" . $row->nama_barang . "</small></td>
							<td id='nama_jenis_barang'><small>" . $row->nama_jenis_barang . "</small></td>
							<td id='jumlah_diajukan' style='display: none;'>" . $row->jumlah_diajukan . "</td>
							<td id='jumlah_dipesan' style='display: none;'>" . $row->jumlah_diajukan . "</td>
							<td id='f_jumlah_dipesan'><small>" . ArrayAdapter::format("number", $row->jumlah_diajukan) . "</small></td>
							<td id='satuan'><small>" . $row->satuan . "</small></td>
							<td id='hpp' style='display: none;'>" . $row->hps . "</td>
							<td id='hna' style='display: none;'>" . $row->hps / ((100 + $persentase_ppn) / 100) . "</td>
							<td id='f_hna'><small>" . ArrayAdapter::format("money", $row->hps / ((100 + $persentase_ppn) / 100)) . "</small></td>
							<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='f_subtotal'><small>" . $f_subtotal . "</small></td>
							<td id='medis' style='display: none;'>" . $row->medis . "</td>
							<td id='inventaris' style='display: none;'>" . $row->inventaris . "</td>
							<td id='konversi' style='display: none;'>" . $row->konversi . "</td>
							<td id='satuan_konversi' style='display: none;'>" . $row->satuan_konversi . "</td>
							<td>" . $html_edit_action . "</td>
							<td>" . $html_del_action . "</td>
						</tr>";
					$row_num++;
				}
			}
			$data = array();
			$data['html'] = $html;
			$data['row_num'] = $row_num;
			echo json_encode($data);
		} else if ($_POST['command'] == "show_detail") {
			$id = $_POST['id'];
			$dbtable = new DBTable($db, "smis_pb_opl");
			$header_row = $dbtable->select($id);
			$persentase_ppn = $header_row->ppn;
			$detail_rows = $dbtable->get_result("
				SELECT *
				FROM smis_pb_dopl
				WHERE id_opl = '" . $id . "' AND prop NOT LIKE 'del'
			");
			$locked = $header_row->lock_opl;
			$html = "";
			$num = 0;
			if ($detail_rows != null) {
				foreach ($detail_rows as $row) {
					$html_edit_action = "";
					if (!$locked)
						$html_edit_action = "<a href='#' onclick='dpembuatan_opl.edit(" . $num . ")' class='input btn btn-warning'><i class='icon-edit icon-white'></i></a>";
					$html_del_action = "<a href='#' onclick='dpembuatan_opl.del(" . $num . ")' class='input btn btn-danger'><i class='fa fa-trash-o'></i></a>";
					if ($locked || $row->f_id > 0)
						$html_del_action = "";
					$subtotal = $row->hpp /  ((100 + $persentase_ppn) / 100) * $row->jumlah_dipesan;
					$f_subtotal = ArrayAdapter::format("money", $subtotal);
					$html .= "
						<tr id='data_" . $num . "'>
							<td id='nomor'></td>
							<td id='id' style='display: none;'>" . $row->id . "</td>
							<td id='f_id' style='display: none;'>" . $row->f_id . "</td>
							<td id='id_barang' style='display: none;'>" . $row->id_barang . "</td>
							<td id='kode_barang'><small>" . $row->kode_barang . "</small></td>
							<td id='nama_barang'><small>" . $row->nama_barang . "</small></td>
							<td id='nama_jenis_barang'><small>" . $row->nama_jenis_barang . "</small></td>
							<td id='jumlah_diajukan' style='display: none;'>" . $row->jumlah_diajukan . "</td>
							<td id='jumlah_dipesan' style='display: none;'>" . $row->jumlah_dipesan . "</td>
							<td id='f_jumlah_dipesan'><small>" . ArrayAdapter::format("number", $row->jumlah_dipesan) . "</small></td>
							<td id='satuan'><small>" . $row->satuan . "</small></td>
							<td id='hpp' style='display: none;'>" . $row->hpp . "</td>
							<td id='hna' style='display: none;'>" . $row->hpp /  ((100 + $persentase_ppn) / 100) . "</td>
							<td id='f_hna'><small>" . ArrayAdapter::format("money", $row->hpp /  ((100 + $persentase_ppn) / 100)) . "</small></td>
							<td id='subtotal' style='display: none;'>" . $subtotal . "</td>
							<td id='f_subtotal'><small>" . $f_subtotal . "</small></td>
							<td id='medis' style='display: none;'>" . $row->medis . "</td>
							<td id='inventaris' style='display: none;'>" . $row->inventaris . "</td>
							<td id='konversi' style='display: none;'>" . $row->konversi . "</td>
							<td id='satuan_konversi' style='display: none;'>" . $row->satuan_konversi . "</td>
							<td>" . $html_edit_action . "</td>
							<td>" . $html_del_action . "</td>
						</tr>
					";
					$num++;
				}
			}
			$data = array();
			$data['html'] = $html;
			$data['row_num'] = $num;
			$data['editable'] = $editable;
			echo json_encode($data);
		} else if ($_POST['command'] == "show_footer") {
			$html = "<tfoot>
						<tr>
							<td colspan='7'><div align='right'><small><strong>Total</strong></small></div></td>
							<td id='total'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='7'><div align='right'><small><strong>PPn</strong></small></div></td>
							<td id='ppn'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='7'><div align='right'><small><strong>Total + PPn</strong></small></div></td>
							<td id='total_tagihan'></td>
							<td colspan='2'>&nbsp;</td>
						</tr>
					</tfoot>";
			$data = array();
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_current_stock") {
			$params = array();
			$params['id_obat'] = $_POST['id_obat'];
			$params['satuan'] = $_POST['satuan_konversi'];
			$params['konversi'] = 1;
			$params['satuan_konversi'] = $_POST['satuan_konversi'];
			$consumer_service = new ServiceConsumer(
				$db,
				"get_single_stock_info",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah = 0;
			if ($content != null)
				$jumlah = number_format($content[0], 0, ",", ".");
			$data = array(
				"jumlah" => $jumlah
			);
			echo json_encode($data);
		} else if ($_POST['command'] == "get_last_hpp") {
			$params = array();
			$params['id_obat'] = $_POST['id_obat'];
			$params['satuan'] = $_POST['satuan'];
			$params['konversi'] = $_POST['konversi'];
			$params['satuan_konversi'] = $_POST['satuan_konversi'];
			$consumer_service = new ServiceConsumer(
				$db,
				"get_last_hpp",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$hpp = "Rp. 0,00";
			$hna = "Rp. 0,00";
			$ppn = $_POST['ppn'];
			if ($content != null) {
				$last_ppn = $content[0]['ppn'];
				$last_hpp = $content[0]['hpp'];
				$hna = $last_hpp / ((100 + $last_ppn) / 100);
				$hpp = $hna * ((100 + $ppn) / 100);
				$hna = ArrayAdapter::format("only-money", $hna);
				$hpp = ArrayAdapter::format("only-money", $hpp);
			}
			$data = array(
				"hpp" => $hpp,
				"hna" => $hna
			);
			echo json_encode($data);
		} else {
			$adapter = new SimpleAdapter(true, "No.");
			$adapter->add("No. RP", "id", "digit8");
			$adapter->add("Tanggal", "tanggal", "date d-m-Y");
			$adapter->add("Kode Vendor", "kode_vendor");
			$adapter->add("Nama Vendor", "nama_vendor");
			$dbtable = new DBTable($db, "smis_pb_opl");
			$dbtable->addCustomKriteria(" medis ", " = '1' ");
			$dbresponder = new PembuatanOPLDBResponder(
				$dbtable,
				$table,
				$adapter
			);
			if ($dbresponder->isSave() && ($_POST['id'] == 0 || $_POST['id'] == "")) {
				$tahun = date("Y");
				$nomor = getSettings($db, "no_opl-" . $tahun, "0") + 1;
				setSettings($db, "no_opl-" . $tahun, $nomor);
				$nomor_opl = ArrayAdapter::format("only-digit2", $nomor) . "/" . date("m") . "/" . date("Y");
				$dbresponder->addColumnFixValue("nomor", $nomor_opl);
			}
			$data = $dbresponder->command($_POST['command']);
			echo json_encode($data);
		}
		return;
	}
	
	echo $header_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>";
	if ($editable == "true") {
		echo	 "<div class='span4'>" .
					 $detail_form->getHtml() .
				 "</div>" .
				 "<div class='span8'>" .
					 $table->getHtml() .
				 "</div>";
	} else {
		echo	 "<div class='span12'>" .
					 $table->getHtml() .
				 "</div>";
	}
	echo 	 "</div>" .
			 "<div class='row-fluid'>" .
				 "<div class='span12' align='right'>" .
					$button_group->getHtml() .
				 "</div>" .
			 "</div>" .
		 "</div>";
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("pembelian/js/pembuatan_opl_barang_action.js", false);
	echo addJS("pembelian/js/pembuatan_opl_rencana_pembelian_farmasi_action.js", false);
	echo addJS("pembelian/js/dpembuatan_opl_action.js", false);
	echo addJS("pembelian/js/pembuatan_opl_action.js", false);
	echo addJS("pembelian/js/pembuatan_opl_form.js", false);
?>