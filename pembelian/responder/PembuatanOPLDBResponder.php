<?php
	require_once("smis-base/smis-include-duplicate.php");

	class PembuatanOPLDBResponder extends DuplicateResponder {
		public function save() {
			$header_data = $this->postToArray();
			$id['id'] = $_POST['id'];

			if ($id['id'] == 0 || $id['id'] == "") {
				$result = $this->dbtable->insert($header_data);
				$id['id'] = $this->dbtable->get_inserted_id();
				$success['type'] = "insert";
				if (isset($_POST['detail'])) {
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_pb_dopl");
					$detail = json_decode($_POST['detail']);
					foreach ($detail as $d) {
						$detail_data = array();
						$detail_data['id_opl'] = $id['id'];
						if ($d->f_id != null)
							$detail_data['f_id'] = $d->f_id;
						if ($d->id_barang != null)
							$detail_data['id_barang'] = $d->id_barang;
						if ($d->kode_barang != null)
							$detail_data['kode_barang'] = $d->kode_barang;
						if ($d->nama_barang != null)
							$detail_data['nama_barang'] = $d->nama_barang;
						if ($d->nama_jenis_barang != null)
							$detail_data['nama_jenis_barang'] = $d->nama_jenis_barang;
						if ($d->jumlah_diajukan != null)
							$detail_data['jumlah_diajukan'] = $d->jumlah_diajukan;
						if ($d->jumlah_dipesan != null)
							$detail_data['jumlah_dipesan'] = $d->jumlah_dipesan;
						if ($d->jumlah_dipesan != null)
							$detail_data['sisa'] = $d->jumlah_dipesan;
						if ($d->satuan != null)
							$detail_data['satuan'] = $d->satuan;
						if ($d->konversi != null)
							$detail_data['konversi'] = $d->konversi;
						if ($d->satuan_konversi != null)
							$detail_data['satuan_konversi'] = $d->satuan_konversi;
						if ($d->hpp != null)
							$detail_data['hpp'] = $d->hpp;
						if ($d->medis != null)
							$detail_data['medis'] = $d->medis;
						if ($d->inventaris != null)
							$detail_data['inventaris'] = $d->inventaris;
						if (isset($_POST['ppn'])) {
							$detail_data['ppn'] = $_POST['ppn'];
						}
						$detail_data['autonomous'] = "[".$this->autonomous."]";
				        $detail_data['duplicate'] = 0;
				        $detail_data['time_updated'] = date("Y-m-d H:i:s");
				        $detail_data['origin_updated'] = $this->autonomous;
						$detail_dbtable->insert($detail_data);
					}
				}
			} else {
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['detail'])) {
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_pb_dopl");
					$detail = json_decode($_POST['detail']);
					foreach ($detail as $d) {
						$detail_id['id'] = $d->id;
						$detail_data = array();
						if ($d->f_id != null)
							$detail_data['f_id'] = $d->f_id;
						if ($d->id_barang != null)
							$detail_data['id_barang'] = $d->id_barang;
						if ($d->kode_barang != null)
							$detail_data['kode_barang'] = $d->kode_barang;
						if ($d->nama_barang != null)
							$detail_data['nama_barang'] = $d->nama_barang;
						if ($d->nama_jenis_barang != null)
							$detail_data['nama_jenis_barang'] = $d->nama_jenis_barang;
						if ($d->jumlah_diajukan != null)
							$detail_data['jumlah_diajukan'] = $d->jumlah_diajukan;
						if ($d->jumlah_dipesan != null)
							$detail_data['jumlah_dipesan'] = $d->jumlah_dipesan;
						if ($d->jumlah_dipesan != null)
							$detail_data['sisa'] = $d->jumlah_dipesan;
						if ($d->satuan != null)
							$detail_data['satuan'] = $d->satuan;
						if ($d->konversi != null)
							$detail_data['konversi'] = $d->konversi;
						if ($d->satuan_konversi != null)
							$detail_data['satuan_konversi'] = $d->satuan_konversi;
						if ($d->hpp != null)
							$detail_data['hpp'] = $d->hpp;
						if ($d->medis != null)
							$detail_data['medis'] = $d->medis;
						if ($d->inventaris != null)
							$detail_data['inventaris'] = $d->inventaris;
						if (isset($_POST['ppn'])) {
							$detail_data['ppn'] = $_POST['ppn'];
						}
						if ($d->id == "" || $d->id == 0) {
							$detail_data['id_opl'] = $id['id'];
							$detail_data['autonomous'] = "[".$this->autonomous."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
					        $detail_data['origin_updated'] = $this->autonomous;
							$detail_dbtable->insert($detail_data);
						} else {
							if ($d->deleted)
								$detail_data['prop'] = "del";
							$detail_data['autonomous'] = "[".$this->autonomous."]";
					        $detail_data['duplicate'] = 0;
					        $detail_data['time_updated'] = date("Y-m-d H:i:s");
							$detail_dbtable->update($detail_data, $detail_id);
						}
					}
				}
			}
		}
	}
?>