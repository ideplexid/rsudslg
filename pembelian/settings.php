<?php
	require_once("smis-framework/smis/api/SettingsBuilder.php");
	global $db;
	
	$settings_builder = new SettingsBuilder($db, "pembelian_settings", "pembelian", "settings");
	$settings_builder->setShowDescription(true);
	$settings_builder->setTabulatorMode(Tabulator::$POTRAIT);
	
	$settings_builder->addTabs("pembelian_opl", "Order Pembelian Langsung (OPL)");
	$year = date("Y");
	$settings_item = new SettingsItem($db, "no_opl-" . $year, "No. OPL " . $year, 0, "text", "Nomor OPL Terakhir Tahun " . $year);
	$settings_builder->addItem("pembelian_opl", $settings_item);
	$settings_item = new SettingsItem($db, "nama_pj_farmasi", "Nama PJ. Farmasi ", "", "text", "Nama PJ. Farmasi");
	$settings_builder->addItem("pembelian_opl", $settings_item);
	$settings_item = new SettingsItem($db, "sipa_pj_farmasi", "SIPA PJ. Farmasi ", "", "text", "SIPA PJ. Farmasi");
	$settings_builder->addItem("pembelian_opl", $settings_item);
	$settings_item = new SettingsItem($db, "nama_verifikator", "Nama Verifikator ", "", "text", "Nama Verifikator");
	$settings_builder->addItem("pembelian_opl", $settings_item);
	$settings_item = new SettingsItem($db, "nama_manager_umum", "Nama Manager Umum ", "", "text", "Nama Manager Umum");
	$settings_builder->addItem("pembelian_opl", $settings_item);
	
	$response = $settings_builder->init();
?>