<?php
	class PersetujuanOPLTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn_group->setMax(4, "");
			$btn = new Button("", "", "Ubah");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("icon-edit icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
?>