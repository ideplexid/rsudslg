<?php
	class PembuatanOPLTable extends Table {
		public function getHeaderButton() {
			$btn_group = new ButtonGroup("noprint");
			$btn_add_reguler_opl = new Button("", "", "OPL Reguler");
			$btn_add_reguler_opl->setAction("pembuatan_opl.show_reguler_opl_add_form()");
			$btn_add_reguler_opl->setAtribute("id='reguler_opl_add'");
			$btn_add_reguler_opl->setClass("btn-primary");
			$btn_add_reguler_opl->setIcon("icon-plus icon-white");
			$btn_add_reguler_opl->setIsButton(Button::$ICONIC_TEXT);
			$btn_group->addElement($btn_add_reguler_opl);
			$btn_add_mandiri_opl = new Button("", "", "OPL Mandiri");
			$btn_add_mandiri_opl->setAction("pembuatan_opl.show_mandiri_opl_add_form()");
			$btn_add_mandiri_opl->setAtribute("id='mandiri_opl_add'");
			$btn_add_mandiri_opl->setClass("btn-inverse");
			$btn_add_mandiri_opl->setIcon("icon-plus icon-white");
			$btn_add_mandiri_opl->setIsButton(Button::$ICONIC_TEXT);
			$btn_group->addElement($btn_add_mandiri_opl);
			return $btn_group->getHtml();
		}
		public function getBodyContent() {
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['lock_opl'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $lock_opl) {
			$btn_group = new ButtonGroup("noprint");
			$btn_group->setMax(4, "");
			if ($lock_opl == false) {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Hapus");
				$btn->setAction($this->action . ".del('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Hapus' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Kunci");
				$btn->setAction($this->action . ".lock_opl('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Terbitkan' data-toggle='popover'");
				$btn->setIcon("fa fa-lock");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "View");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='View' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
				$btn = new Button("", "", "Eksport XLS");
				$btn->setAction($this->action . ".export_xls('" . $id . "')");
				$btn->setClass("btn-inverse");
				$btn->setAtribute("data-content='Cetak Perencanaan' data-toggle='popover'");
				$btn->setIcon("fa fa-download");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
?>