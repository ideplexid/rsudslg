<?php 
	if (isset($_POST['command'])) {
		$opl_dbtable = new DBTable($db, "smis_pb_opl");
		$opl_dbtable->addCustomKriteria(" medis ", " = '1' ");
		$opl_dbtable->addCustomKriteria(" lock_opl ", " = '1' ");
		$opl_dbtable->setOrder(" id DESC ");
		$service_provider = new ServiceProvider($opl_dbtable);
		$data = $service_provider->command($_POST['command']);
		$service_provider->setDebuggable(true);
		echo json_encode($data);
	}
?>