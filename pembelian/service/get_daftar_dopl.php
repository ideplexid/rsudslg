<?php 
	if (isset($_POST['command'])) {
		$dopl_dbtable = new DBTable($db, "smis_pb_dopl");
		$dopl_dbtable->addCustomKriteria(" id_opl ", " = '" . $_POST['id_opl'] . "' ");
		if (isset($_POST['not_in']) && $_POST['not_in'] != "")
			$dopl_dbtable->addCustomKriteria(" id ", " NOT IN (" . $_POST['not_in'] . ") ");
		$dopl_dbtable->setOrder(" nama_barang ASC ");
		$service_provider = new ServiceProvider($dopl_dbtable);
		$data = $service_provider->command($_POST['command']);
		$service_provider->setDebuggable(true);
		echo json_encode($data);
	}
?>