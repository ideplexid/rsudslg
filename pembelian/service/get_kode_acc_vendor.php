<?php
	global $db;

	if (isset($_POST['id'])) {
		$id = $_POST['id'];
		$dbtable = new DBTable($db, "smis_pb_vendor");
		$row = $dbtable->get_row("
			SELECT *
			FROM smis_pb_vendor
			WHERE id = '" . $id . "'
		");
		$kode_hutang = "";
		$kode_diskon_pembelian = "";
		$kode_ppn = "";
		$kode_pembelian = "";
		$kode_materai = "";
		$kode_ekspedisi = "";
		if ($row != null) {
			$kode_hutang = $row->acc_kode_hutang;
			$kode_diskon_pembelian = $row->acc_kode_diskon_pembelian;
			$kode_ppn = $row->acc_kode_ppn;
			$kode_pembelian = $row->acc_kode_pembelian;
			$kode_materai = $row->acc_kode_materai;
			$kode_ekspedisi = $row->acc_kode_ekspedisi;
		}
		$data = array(
			"kode_hutang"			=> $kode_hutang,
			"kode_diskon_pembelian"	=> $kode_diskon_pembelian,
			"kode_ppn"				=> $kode_ppn,
			"kode_pembelian"		=> $kode_pembelian,
			"kode_materai"			=> $kode_materai,
			"kode_ekspedisi"		=> $kode_ekspedisi
		);
		echo json_encode($data);
	}
?>