<?php 
	if (isset($_POST['command'])) {
		$dpo_dbtable = new DBTable($db, "smis_pb_dopl");
		if ($_POST['command'] == "list") {
			$dpo_dbtable->setViewForSelect(true);
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (smis_pb_dopl.nama_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_pb_dopl.nama_jenis_barang LIKE '%" . $_POST['kriteria'] . "%' OR smis_pb_dopl.id_barang LIKE '" . $_POST['kriteria'] . "')";
			}
			$query_value = "
				SELECT smis_pb_dopl.*
				FROM smis_pb_dopl
				WHERE smis_pb_dopl.prop NOT LIKE 'del' AND smis_pb_dopl.sisa > 0 AND (medis = '0' OR inventaris = '1') AND smis_pb_dopl.id_opl = '" . $_POST['id_po'] . "' " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM smis_pb_dopl
				WHERE smis_pb_dopl.prop NOT LIKE 'del' AND smis_pb_dopl.sisa > 0 AND (medis = '0' OR inventaris = '1') AND smis_pb_dopl.id_opl = '" . $_POST['id_po'] . "' " . $filter . "
			";
			$dpo_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		$dpo_dbtable->setDebuggable(true);
		$service_provider = new ServiceProvider($dpo_dbtable);
		$data = $service_provider->command($_POST['command']);
		echo json_encode($data);
	}
?>