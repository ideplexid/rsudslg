<?php 
	if (isset($_POST['command'])) {
		$vendor_dbtable = new DBTable($db, "smis_pb_vendor");
		$vendor_dbtable->setOrder(" nama ASC ");
		$service_provider = new ServiceProvider($vendor_dbtable);
		$data = $service_provider->command($_POST['command']);
		$service_provider->setDebuggable(true);
		echo json_encode($data);
	}
?>