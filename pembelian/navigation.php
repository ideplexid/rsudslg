<?php 
	global $NAVIGATOR;
	
	$pembelian_menu = new Menu("fa fa-shopping-cart");
	$pembelian_menu->addProperty('title', "Pembelian");
	$pembelian_menu->addProperty('name', "Pembelian");
	$pembelian_menu->addSubMenu("Vendor", "pembelian", "vendor", "Data Vendor", "fa fa-group");
	$pembelian_menu->addSubMenu("Mapping Acc. Vendor", "pembelian", "mapping_acc_vendor", "Data Mapping Acc. Vendor", "fa fa-random");
	$pembelian_menu->addSubMenu("Riwayat Kerjasama Vendor", "pembelian", "riwayat_kerjasama", "Riwayat Kerjasama Vendor", "fa fa-file-text");
	$pembelian_menu->addSubMenu("Pembuatan OPL Farmasi", "pembelian", "pembuatan_opl", "Pembuatan OPL Farmasi", "fa fa-file-text-o");
	$pembelian_menu->addSubMenu("Pembuatan OPL Non-Farmasi", "pembelian", "pembuatan_opl_nm", "Pembuatan OPL Non-Farmasi", "fa fa-file-text-o");
	$pembelian_menu->addSubMenu("Settings", "pembelian", "settings", "Settings", "fa fa-gear");
	$NAVIGATOR->addMenu($pembelian_menu, "pembelian");
?>