<?php
	require_once("smis-base/smis-include-duplicate.php");
	require_once("pembelian/table/PembuatanOPLTable.php");
	global $db;
	
	$table = new PembuatanOPLTable(
		array("No.", "No. OPL", "Tanggal", "Kode Vendor", "Nama Vendor"),
		"Pembelian : Pembuatan OPL Non-Farmasi",
		null,
		true
	);
	$table->setName("pembuatan_opl");
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			require_once("pembelian/pembuatan_opl_nm_export_xls.php");
			return;
		}
		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("id", "id");
		$adapter->add("lock_opl", "lock_opl");
		$adapter->add("No. OPL", "nomor");
		$adapter->add("Tanggal", "tanggal", "date d-m-Y");
		$adapter->add("Kode Vendor", "kode_vendor");
		$adapter->add("Nama Vendor", "nama_vendor");
		$dbtable = new DBTable($db, "smis_pb_opl");
		$dbtable->addCustomKriteria(" medis ", " = 0 ");
		$dbtable->setOrder(" id DESC ");
		$dbresponder = new DuplicateResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $table->getHtml();
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("pembelian/js/pembuatan_opl_nm_action.js", false);
	echo addJS("pembelian/js/pembuatan_opl_nm.js", false);
?>