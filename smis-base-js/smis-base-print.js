function smis_print(html){
	$("#printing_area").html(html);
	window.print();
}

function smis_print_self(html){
	$("#printing_area").html(html);
}

/**
 * action melakukan pencetakan dengan menggunakan jsprint setup
 * sehingga bisa di handle dengan mudah
 */
function bootbox_choose_print_action(){
	var printer = $("#bootbox_choose_print_printer option:selected").text();
	var copies  = Number($("#bootbox_choose_print_copies").val());
	jsPrintSetup.setPaperSizeData(101);
	jsPrintSetup.setOption('headerStrLeft', '');
	jsPrintSetup.setOption('headerStrCenter', '');
	jsPrintSetup.setOption('headerStrRight', '');
	jsPrintSetup.setOption('footerStrLeft', '');
	jsPrintSetup.setOption('footerStrCenter', '');
	jsPrintSetup.setOption('footerStrRight', '');
	jsPrintSetup.setOption('marginTop', 0);
	jsPrintSetup.setOption('marginBottom', 0);
	jsPrintSetup.setOption('marginLeft', 0);
	jsPrintSetup.setOption('marginRight', 0);
	jsPrintSetup.setOption('startPageRange',1);
	jsPrintSetup.setOption('endPageRange',1);
	//jsPrintSetup.setOption('numCopies',copies);						
	jsPrintSetup.setOption('scaling',100);
	jsPrintSetup.setOption('shrinkToFit',false);
	jsPrintSetup.clearSilentPrint();
	jsPrintSetup.setOption('printSilent', 1);
	jsPrintSetup.setPrinter(printer);
	bootbox.hideAll();
	//jsPrintSetup.print();
	loop_print(copies);
}

/** fungsi untuk melakukan loop print sebanyak copy yan gdiinginkan
  * @param int loop - int jumlah loop
  * @return null
  */
function loop_print(loop){
	if(loop<=0){
		return;
	}
	loop--;
	jsPrintSetup.print();
	setTimeout(function(){
		loop_print(loop);
	},2000);
}

/**
 * fungsi ini berfungsi untuk memanggil printer
 * yang tersedia dalam komputer
 * @returns {String} HTML Select untuk memilih printer
 */
function getBootBoxChoosePrint(){
	var a=jsPrintSetup.getPrintersList();
	var list=a.split(",");
	var select="<select id='bootbox_choose_print_printer'>";
	for (var i = 0; i < list.length; i++) { 
	    select += "<option value='"+list[i]+"'>"+list[i]+"</option>";
	}
	select+="</select>";
	return select;
}

/**
 * fungsi ini cuma untuk menampilkan select
 * untuk melakukan print sebanyak berapa kai
 * sengaja dibuat terpisah agar tampilan source code rapi dan tidak amburadul
 * @returns {String} HTML select untuk memilih jumlah
 */
function getJumlahBootBoxChoosePrint(){
	var jumlah="<select id='bootbox_choose_print_copies'>" +
	"<option value=' 1'  selected='selected'>1</option>"+ 
	"<option value=' 2' > 2</option>"+ 
	"<option value=' 3' > 3</option>"+ 
	"<option value=' 4' > 4</option>"+ 
	"<option value=' 5' > 5</option>"+ 
	"<option value=' 6' > 6</option>"+ 
	"<option value=' 7' > 7</option>"+ 
	"<option value=' 8' > 8</option>"+ 
	"<option value=' 9' > 9</option>"+ 
	"<option value='10' >10</option>"+ 
	"<option value='11' >11</option>"+ 
	"<option value='12' >12</option>"+ 
	"</select>";
	return jumlah;
}

/**
 * fungsi untuk menampilkan dialog 
 * dimana printer yang dikehendaki untuk dijadikan 
 * fungsi pencetakan menggunakan jsprintsetup
 */
function bootbox_choose_print(){
	var select=getBootBoxChoosePrint();
	var jumlah=getJumlahBootBoxChoosePrint();	
	try{
		/* bootbox versi 3x */
		var content="Printing : "+select+" </br> Sejumlah : "+jumlah;
		var buttons=[{
		    		"label" 	: "<i class='fa fa-print'></i> Print",
		    		"class" 	: "btn-success",
		    		"callback"	: function() {
		    						bootbox_choose_print_action();
		    					  }
				},{
					"label" 	: "<i class='fa fa-remove'></i> Cancel",
					"class" 	: "btn-danger",
					"callback"	: function() {
									bootbox.hideAll();
								  }
			}];
		 bootbox.dialog(content, buttons);
	}catch(err){
		console.log(err);
		/* bootbox versi 4x */
		var params={};
		params['message']="Printing : "+select+" </br> Sejumlah : "+jumlah;
		params['buttons']={
					success:{
					    label 		: "<i class='fa fa-print'></i> Print",
					    className 	: "btn-success",
					    callback	: function() {
					    				bootbox_choose_print_action();
					    			  }
					},
					"Batal": {
						 label 		: "<i class='fa fa-remove'></i> Batal",
					     className	: "btn-danger",
					     callback	: function() {
					    	 			bootbox.hideAll();
					    	 		  }
					}
		};
		bootbox.dialog(params);	
	}
}







function smis_print_force(html){
	$("#printing_area").css("display","block");
	smis_print(html);
	$("#printing_area").css("display","none");
}

function smis_print_barcode(html,tag){
	$("#printing_area").html(html);
	$(tag).sbarcode();
	window.print();
}

function smis_print_force_barcode(html,tag){
	$("#printing_area").css("display","block");
	smis_print_barcode(html,tag);
	$("#printing_area").css("display","none");
}

function smis_print_dm(text){
	var myWindow=window.open("data:text/text;charset=utf-8," + escape(text));
	myWindow.focus();
	myWindow.print();
	//myWindow.close();
}

function smis_print_save(html,grup,name){
	var data={
		page:"smis-tools",
		action:"print",
		command:"save",
		nama:name,
		html:html,
		grup:grup
	};
	
	$.post("",data,function(res){
		var json=getContent(res);
		if(json!=null && json.success==1 ){
			$("#printing_area").css("display","block");
			smis_print(html);
			$("#printing_area").css("display","none");
		}else{
			showWarning("Save Fail","Saving Print Fail");
		}
		
		
	});
}
