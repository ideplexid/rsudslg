/* 
 * define the only methode of copy to clipboard
 * this methode used of copy a text in the clipboard
 * 
 * @author 	: jfriends00
 * @editted	: goblooge
 * @link	: http://stackoverflow.com/questions/22581345/click-button-copy-to-clipboard-using-jquery
 * @params	: - elem is the id of HTML element
 * 			  - type is the type of copy, text (copy as text), inner (copy the inner HTML), outer (copy the outer HTML)
 * */

function copyToClipboard(elem,type) {
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
		if(type=="text"){
			target.textContent = elem.textContent;
		}else if(type=="inner"){
			target.textContent = elem.innerHTML;
		}else{
	        target.textContent = elem.outerHTML;
		}
		
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
