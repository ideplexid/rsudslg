function changeColor(){
    var grayscale=$("#color_grayscale").val()/100;
    var slider=$("#color_slider").val();
    var invert=$("#color_invert").is(":checked")?"1":"0";
    $("body").css('-webkit-filter', 'hue-rotate('+slider+'deg)  invert('+invert+')  grayscale('+grayscale+')');
}
$(document).ready(function(){
    $("#color_slider").slider().on('slideStop', function(ev){
        changeColor();
    });
    $("#color_grayscale").slider().on('slideStop', function(ev){
        changeColor();
    });
    $(" #color_invert").on("change",function(){
        changeColor();
    });
    
});