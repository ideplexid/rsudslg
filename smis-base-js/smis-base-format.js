

/*CHECKING FORMAT*/
function is_alphanumeric(pwd){
	if(pwd.match("^[a-zA-Z0-9 /./,]*$")){
	   return true;
	}
	return false;
}

function is_match(a,b){
	if(a==b) return true;
	return false;
}

function is_date(date_val){
	var matches = /(\d{4})[-\/](\d{2})[-\/](\d{2})/.exec(date_val);
	if (matches == null || date_val.length>10)
    	return false;
    return true;
}

function is_alpha(pwd){
	if(pwd.match("^[a-zA-Z /./,]*$")){
	   return true;
	}
	return false;
}

function is_numeric(pwd){
	if(pwd.match("^[0-9]*$")){
	   return true;
	}
	return false;
}

function getFormattedDate(input){
    var pattern=/(.*?)\-(.*?)\-(.*?)$/;
    var result = input.replace(pattern,function(match,p1,p2,p3){
        var months=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember'];
        return p3+" "+months[(p2-1)]+" "+p1;
    });
    return result;
}

function getFormattedTime(input){
    var the_date=input.substring(0,10);
    var the_time=input.substring(11);
    var result=getFormattedDate(the_date)+" "+the_time;
    return result;
}

function getAge(born) {
	  var now = new Date();
	  var today = new Date(now.getYear(),now.getMonth(),now.getDate());

	  var yearNow = now.getYear();
	  var monthNow = now.getMonth();
	  var dateNow = now.getDate();

	  var dob = born;
	  var yearDob = dob.getYear();
	  var monthDob = dob.getMonth();
	  var dateDob = dob.getDate();
	  var age = {};
	  var ageString = "";
	  var yearString = "";
	  var monthString = "";
	  var dayString = "";


	  yearAge = yearNow - yearDob;

	  if (monthNow >= monthDob)
	    var monthAge = monthNow - monthDob;
	  else {
	    yearAge--;
	    var monthAge = 12 + monthNow -monthDob;
	  }

	  if (dateNow >= dateDob)
	    var dateAge = dateNow - dateDob;
	  else {
	    monthAge--;
	    var dateAge = 31 + dateNow - dateDob;

	    if (monthAge < 0) {
	      monthAge = 11;
	      yearAge--;
	    }
	  }

	  age = {
	      years: yearAge,
	      months: monthAge,
	      days: dateAge
	      };

	  if ( age.years > 1 ) yearString = " Tahun";
	  else yearString = " Tahun";
	  if ( age.months> 1 ) monthString = " Bulan";
	  else monthString = " Bulan";
	  if ( age.days > 1 ) dayString = " Hari";
	  else dayString = " Hari";
	  
	  var ageString = age.years + " Tahun " + age.months + " Bulan " + age.days + " Hari";
	  return ageString;
	}


function calcDateDiff(date1,date2) {
    var diff = Math.floor(date1.getTime() - date2.getTime());
    var day = 1000 * 60 * 60 * 24;
    var days = Math.floor(diff/day);
    
    var the_days=days%31;
    var the_months = Math.floor(days/31)%12;
    var the_years = Math.floor(days/(365));
    var message= the_years + " Tahun " + the_months+" Bulan "+the_days+" Hari ";
    return message;
}


/*END OF CHECKING FORMAT*/