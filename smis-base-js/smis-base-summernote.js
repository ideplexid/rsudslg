function smis_summernote_write(title,id,id_modal){
	var content=$("#"+id).val();
	$("#summernote_button_save").attr("data-id",id);
	$("#smis-summernote-modal .modal-header h3").html(title);
	$('#summernote').code(content);
	$("#smis-summernote-modal ").smodal('show');
}

function smis_summernote_show(title,id,id_modal){
	var content=$("#"+id).val();
	$("#confirm .modal-body").html(content);
	$("#confirm .modal-header h3").html(title);
	$("#confirm_button").val("OK");	
	$("#confirm").smodal("show");
}


function  smis_summernote_print(title,id,id_modal){
	var content=$("#"+id).val();
	smis_print(content);
	
} 

function smis_summernote_save(){	
	var content= $('#summernote').code();
	var id=$("#summernote_button_save").attr("data-id");
	$("#"+id).val(content);
	$("#smis-summernote-modal").smodal("hide");
}

function sendFile(file, editor, welEditable) {
    data = new FormData();
    data.append("file", file);
    data.append("page","smis-base");
    data.append("action","smis-summernote-upload");
    
    $.ajax({
        data: data,
        type: "POST",
        url: "",
        cache: false,
        contentType: false,
        processData: false,
        success: function(res) {        	
        	var json=$.parseJSON(res);
        	if(json.type=="error"){
        		showWarning("Error", json.result);
        		return;
        	}else if(json.type=="image"){
        		editor.insertImage(welEditable, json.result);
        	}else{
        		editor.recordUndo(welEditable);
        		document.execCommand("insertHTML",false,json.result);
        	}
        }
    });
}
