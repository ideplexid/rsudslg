/**
 * this application to handle barcode creator
 * 
 * @author goblooge
 * @since 31 May 2016
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGL v3
 * @param $
 */

(function( $ ) {
	$.fn.sbarcode= function( ) {
    	var bcode=this.data("bcode")+"";
    	var bwidth=Number(this.data("bwidth"));
    	var bheight=Number(this.data("bheight"));
    	var decode_barcode=this.data("barcode")+"";
    	var boutput=this.data("boutput")+"";
    	var bhri=this.data("bhri")=="1";
    	//this.barcode(barcode,bcode,{barWidth:bwidth,barHeight:bheight});
    	this.barcode(decode_barcode,bcode,{barWidth:bwidth, barHeight:bheight,showHRI:bhri,output:boutput});
        return this;
    };
}( jQuery ));
