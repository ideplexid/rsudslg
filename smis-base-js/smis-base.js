
$(document).ready(function(){
	smis_navigator_init();
	var cur=$("#smis_notification").html();
	if(cur!="" && cur!="0"){
		play_sound();
	}
	$('#loading').on('hidden', function () {
		total_loading=1;
		stop=false;
		dismissLoading();
	});
    
    $('#smis-help-modal').on('hidden', function () {
        $("#smis-help-modal .modal-body").html("");
    });
});

var CURRENT_SMIS_CHOOSER=null;


function smis_navigator_init(){
	$(".smis_action").click(function(){
		var page=$(this).attr("page");
		var action=$(this).attr("action");
		var prototype_name=$(this).attr("prototype_name");
		var prototype_slug=$(this).attr("prototype_slug");
		var prototype_implement=$(this).attr("prototype_implement");
		var desc=$(this).attr("title");
		setBreadcrumb(desc);
		LoadSmisPage({
			page:page,
			action:action,
			prototype_name:prototype_name,
			prototype_slug:prototype_slug,
			prototype_implement:prototype_implement
		});
	});
}

function getContent(res){	
	try{
		var json=$.parseJSON(res);
		var status=json.status;
		var alert=json.alert;
		var content=json.content;
		var warning=json.warning;
		var splash=json.splash;
		
		if(alert.status=='y'){
			var acolor=alert.color;
			var acontent=alert.content;
			var atitle=alert.title;
			smis_alert(atitle, acontent, acolor);
		}
		
		if(status=='unauthorized'){
				dismissLoading();
				bootbox.confirm("Your Are Not Authorize to Access This Page , Reload Now ?", function(result) {
				   if(result){
					   location.reload();
				   }
				}); 
		}else if(warning.show=='y'){
			dismissLoading();
			showWarning(warning.title, warning.content);
		}
		
		if(status=='ok'){
			return content;
		}else if(status=='no-install' || status=="logout" ){
			location.reload();
		}else{
			return null;
		}
	}catch(e){
		total_loading=1;
		stop=false;
		dismissLoading();
        var php=res;
        var posting=JSON.stringify($_data_post,null,3);
        var jsscript=e.stack;
		var problem=" <label class='label label-important'> Server Response </label> ";
		problem+="<pre>"+php+"</pre>";
		problem+="</br>";
		problem+=" <label class='label label-info'> Data Sent  </label> ";
		problem+="<pre class='fetreefy'>"+posting+"</pre>";
		problem+="</br>";
		problem+=" <label class='label label-warning'> Javascript  </label> ";
		problem+="<pre>"+jsscript+"</pre>";
		
        var jdl="<h3> <i class='fa fa-warning'></i> Server Communication Error !!</h3>";
        if($("#slack_enable_mode").val()=="1"){         
            var dialog = bootbox.dialog({
                title           : jdl,
                message         : problem,
                buttons: {
                    reload: {
                        label: "<i class='fa fa-refresh'></i> Reload",
                        className: 'btn-warning',
                        callback: function(){
                            location.reload();
                        }
                    },
                    cancel: {
                        label: "<i class='fa fa-close'></i> Cancel",
                        className: 'btn-success',
                        callback: function(){
                            return true;
                        }
                    },
                    slack: {
                        label: "<i class='fa fa-slack'></i> Slack",
                        className: 'btn-inverse',
                        callback: function(){
                            slack_posting(php,posting,jsscript);
                            return true;
                        }
                    }
                }    
            });   
        }else{
           bootbox.confirm({title:jdl,message:problem, callback:function(result) {
               if(result){
                   location.reload();
               }
            }}); 
        }
        try{
            $(".fetreefy").fetreefy();    
        }catch(e){ } 
	}
}

function slack_posting(php,posting,jscript){
    var data={
        page:"smis-tools",
        action:"slack_error_reporting",
        prototype_name:"",
        prototype_slug:"",
        prototype_implement:"",
        res_php:php,
        res_posting:posting,
        res_jscript:jscript
    }
    showLoading();
    $.post("",data,function(res){
        getContent(res);
        dismissLoading();
    });
}


function help(page,action){
	var data={
			page:page,
			action:"help/user/"+action,
			smis_help:"help",
			prototype_implement:"",
			prototype_slug:"",
			prototype_name:"",		
	};
	
	$.post('',data,function(res){
        var action_name=replaceAll("_"," ",action);
		$("#smis-help-modal .modal-body").html(res);
		$("#smis-help-modal .modal-header h3").html(action_name.toUpperCase());
		$("#smis-help-modal").smodal("show");
	});
}

function helptable(page,action,slug,name,implement){
	var data={
			page:page,
			action:"help/user/"+action,
			smis_help:"help",
			prototype_implement:implement,
			prototype_slug:slug,
			prototype_name:name,		
	};
	
	$.post('',data,function(res){
        var action_name=replaceAll("_"," ",action);
		$("#smis-help-modal .modal-body").html(res);
		$("#smis-help-modal .modal-header h3").html(action_name.toUpperCase());
		$("#smis-help-modal").smodal("show");
	});
	
}

//WRAP HTML
function to_html(id){
	var x = $(id).wrap('<p/>').parent().html();
	$(id).unwrap();
	return x;
}


function LoadSmisPage(data){
	showLoading();	
	$.post('',data,function(res){
		try {
		    var data = JSON.parse(res);
		    getContent(res);
		    return;
		} catch(e) {
			try{
				$("#smis_container").html(res);
				dismissLoading();
			}catch (e) {
				dismissLoading();
				
				var rres=replaceAll("<","&lt;",res);
				rres=replaceAll(">","&gt;",rres);				
				
				var problem="";
				problem+=" <label class='label label-warning'> Javascript  </label> ";
				problem+="<pre>"+e.stack+"</pre>";
				problem+="</br>";
				problem+="</br>";
				problem+=" <label class='label label-info'> Server  </label> ";
				problem+="<pre>"+rres+"</pre>";
				
				showFullWarning("Error Javascript Loading Page",problem,"full_model");
			}finally{
				if($("#smis_window_wrapper").is(":hidden")){
					$("#smis_window_wrapper").show("fast");
				}				
			}
		}		
	});
}




function next(current,next,prevent){
	$(current).keypress(function(e) { 
		if(prevent) e.preventDefault(); 
		if(e.which == 13)  
			$(next).focus();
	});
}

function play_sound() {
	try{
		if($("#smis_notification").html()!="") {
			document.getElementById('smis_sound_source').play();
		}		
	}catch(e){
		
	}
}

function replaceAll(find, replace, str) {
	  return str.replace(new RegExp(find, 'g'), replace);
}

function stypeahead(id,minlength,table_action,search_name,the_updater){
	$(id).typeahead({
		minLength:minlength,
        source: function (query, process) {
	     var action_data=table_action.getViewData();
	     action_data["kriteria"]=$(id).val();
         var $items = new Array;
           $items = [""];				                
          $.ajax({
            url: '',
            type: 'POST',
            data: action_data,
            success: function(res) {
              var json=getContent(res);
              var the_data_proses=null;
              if(typeof (json["d"]) != "undefined" ){
            	  the_data_proses=json.d.data;
              }else{
            	  the_data_proses=json.dbtable.data;
              }
               $items = [""];	
              $.map(the_data_proses, function(data){
                  var group;
                  group = {
                      name: data[search_name],
                      object:data,
                      toString: function () {
                    	  return JSON.stringify(this);
                      },
                      toLowerCase: function () {
                          return this.name.toLowerCase();
                      },
                      indexOf: function (string) {
                          return String.prototype.indexOf.apply(this.name, arguments);
                      },
                      replace: function (string) {
                          var value = '';
                          value +=  this.name;
                          if(typeof(this.level) != 'undefined') {
                              value += ' <span class="pull-right muted">';
                              value += this.level;
                              value += '</span>';
                          }
                          return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
                      }
                  };
                  $items.push(group);
              });
              
              process($items);
            }
          });
        },
        updater: function(item){
        	var json=JSON.parse(item);
        	the_updater(json.object,id,table_action,search_name);
        	 return json.name;
        }
      });
}


function activeTab(tab){
    $('.nav-tabs a[href="' + tab + '"]').tab('show');
};

function postForm(data) {
	if ($('#form_temp').length == 0) {
		$('body').append('<form id="form_temp" method="post" style="display:none"></form>');
	}
	var $form = $('#form_temp');
	$form.empty();
	$.each(data, function(i, val) {
		$('<input type="hidden">').attr('name', i).attr('value', val).appendTo($form);
	});
	$form.submit();
}


var $_data_post;
var $_smis_post_max_timeout;
(function($){
    var originalPostMethod = jQuery.ajax;
    $.post = function(URL,data,fungsi){
        $_data_post=data;
        var tim=$_smis_post_max_timeout;
        var settings = {
          type : "POST", //predefine request type to POST
          'url'  : URL,
          'data' : data,
          'success' : fungsi
        };
        if(tim!=-1){
              settings['timeout']=tim;  
        }
        var origin_methode = originalPostMethod(settings);
        origin_methode.error(function(){
            smis_alert("<i class='fa fa-plug'></i> Request Time Out", "* Request Taking Too Long to response,</br>* Tooking More Than  ("+tim+" ms). </br>* Perhaps Caused By Network Problem </br>* Please Press F5 to Reload This Page,</br>* and start over again !!", "alert-danger",10000);
            dismissLoading();
        });
    }
})(jQuery);

function setAjaxTimeOut(timout){
    $_smis_post_max_timeout=timout;
}

function setAjaxTimeOutUnlimited(){
    setAjaxTimeOut(-1);
}

function setAjaxTimeOutDefault(){
    setAjaxTimeOut(Number($("#smis_post_max_timeout").val()));
}

$(document).ready(function(){
    setAjaxTimeOutDefault();
});