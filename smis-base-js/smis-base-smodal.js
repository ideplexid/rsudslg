/**
 * 
 * this one to handle the bootstrap overlap flip modal
 * so it will never be overlapped and 
 * stop to make stack overflow
 * 
 * @author goblooge
 * @since 7 Nov 2014
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGL v3
 * @param $
 */

(function( $ ) {
	var stack	= new Array();
	var curshow	= null;
    $.fn.smodal	= function( action ) {
    	if(action=="current"){
    		return curshow;
    	}            
		var nextshow	= this.selector+"";
        if ( action === "show" && nextshow != curshow) {
        	if(curshow==null){
            	curshow	= nextshow;
            }else{
            	$(curshow).modal("hide");
            	stack.push(curshow);
            	curshow	= nextshow;
            }
            $(curshow).modal("show");
        }
        if ( action === "hide" ) {
            if(curshow!=null){
            	$(curshow).modal("hide");
            	prevshow	= stack.pop();
            	curshow		= prevshow;
            	if(curshow!=null){
            		$(curshow).modal("show");
            	}
            }
        }        
        return this;
    };
}( jQuery ));
