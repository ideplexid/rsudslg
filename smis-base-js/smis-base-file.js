
/**
 * DOWNLOAD FILE 
 * @param filename file yang akan di download lewat smis-upload
 */
function download_file(filename){
	var text=	"<form target='downloadFrame' method='POST' action='' id='form_download' >";
		text+="<input type='hidden' value='"+filename+"' name='smis_download'>";
	text+="</form>";
	text+='<iframe name="downloadFrame" style="display: none;" src="" />';
	$("#printing_area").html(text);
	var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
	var android = navigator.userAgent.toLowerCase().match(/android/g) ? true : false;
	if (iOS || android)
	    $("#form_download").attr("target", "_blank");
	$("#form_download").submit();
}

function download_file_dv(e){
	var dv=$(e).data("dv");
	download_file(dv);
}


/**
 * download file generated like pdf or others
 * @param data is array containt page, action, etc
 */
function download(data){
	var text=	"<form target='downloadFrame' method='POST' action='' id='form_download' >";
			for(var key in data){
				text+="<input type='hidden' value='"+data[key]+"' name='"+key+"'>";
			}
			text+="<input type='hidden' value='download_token' name='download_token'>";
		text+="</form>";
		text+='<iframe name="downloadFrame" style="display: none;" src="" />';
		$("#printing_area").html(text);
		var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
        var android = navigator.userAgent.toLowerCase().match(/android/g) ? true : false;
        if (iOS || android)
            $("#form_download").attr("target", "_blank");
        $("#form_download").submit();
}

function remove_file(filename){
	$.post("",{
		page:"smis-base",
		action:"smis-remove-file",
		filename:filename
	},function(res){
		
	});
}

function showImage(url){
	$("#smis-image-modal .modal-body").html("<img src='"+url+"' class='img-modal-item' />");
	$("#smis-image-modal").smodal("show");
}


/*UPLOAD HANDLER*/
function uploadfile(modal,element,model,filetype){

	$.post("",{
		page:"smis-base",
		action:"smis-upload-form",
		modal:modal,
		element:element,
		model:model,
		filetype:filetype,
		curval:$("#"+element).val()
	},function(res){
		$("#smis-upload-modal .modal-body").html(res);
		$("#smis-upload-modal").smodal("show");
	});
}

function uploadDone(){
	var elm=$("#element_upload").val();
	var elmv=$("#element_upload_value").val();
	$("#"+elm).val(elmv);
	$("#smis-upload-modal").smodal("hide");
}