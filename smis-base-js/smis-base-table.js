function smis_format(column,prefix){
	if(column==null) return;
	for(var i=0;i<column.length;i++){
		var name=column[i];
		var the_id="#"+prefix+name;
		var type=$(the_id).attr('type');
		if(type=='text' || type=='textarea'){
			var typical=$(the_id).attr('typical');
			if(typical=="money"){
				var val=$(the_id).val();
				$(the_id).maskMoney();
				$(the_id).maskMoney('mask',Number(val));
			}else if($(the_id).hasClass("typehead")){
				var the_free=$(the_id).attr("freeinput")=="y";
				var label="label label-default";
				var data=$(the_id+"_others").html();
				var sc=[];
				try{ 
					sc=$.parseJSON(data); 
					label=the_free?"label label-success":"label label-info";
				}catch(e){
					label=the_free?"label label-warning":"label label-danger label-important";
				}
				$(the_id).tagsinput({
					tagClass:label,
					typeahead: {
						displayKey: 'name',
					    valueKey: 'value',
					    source:sc
			        },
			        freeInput:the_free
				});
			}
		}
	}	
}

function smis_get_data_ux(the_id){
	var val=null;
	var typical=$(the_id).attr('typical');
	var type=$(the_id).attr('type');
	if(typical=="money"){
		val=$(the_id).maskMoney('unmasked')[0];
	}else if(type=="text"){
		val =$(the_id).val();
	}else if(type=="select"){
		val =$(the_id+" option:selected ").text();
	}else if(type=="checkbox"){
		val=$(the_id).is(':checked')?"Checked":"Un Checked";
	}else if($(the_id).hasClass("typehead")){
		val=$(the_id).tagsinput('items');			
	}else{
		val=$(the_id).val();
	}	
	return val;
}

function smis_get_data(the_id){
	var val=null;
	var typical=$(the_id).attr('typical');
	var type=$(the_id).attr('type');
	if(typical=="money"){
		val=$(the_id).maskMoney('unmasked')[0];
	}else if(type=="text" && $(the_id).hasClass("mydate") && $(the_id).data("date-format")=="dd-mm-yyyy"){
		var the_date=$(the_id).val();
		var t=the_date.split("-");
		val=(t[2]+"-"+t[1]+"-"+t[0]);
	}else if(type=="text" && $(the_id).hasClass("itextdate") && $(the_id).data("date-format")=="dd-mm-yyyy"){
		var the_date=$(the_id).val();
		var t=the_date.split("-");
		val=(t[2]+"-"+t[1]+"-"+t[0]);
	}else if(type=="text" && $(the_id).hasClass("mydatetime") && $(the_id).data("date-format")=="dd-mm-yyyy hh:ii"){
		var the_datetime=$(the_id).val();
		var dt=the_datetime.split(" ");
		var the_date=dt[0];
		var t=the_date.split("-");
		val=(t[2]+"-"+t[1]+"-"+t[0]+" "+dt[1]);		
	}else if(type=="checkbox"){
		val=$(the_id).is(':checked')?1:0;
	}else if($(the_id).hasClass("typehead")){
		val=$(the_id).tagsinput('items');			
	}else{
		val=$(the_id).val();
	}	
	return val;
}

function smis_edit(the_id,the_val){
	var typical=$(the_id).attr('typical');
	var type=$(the_id).attr('type');	
	if(typical=="money"){
		$(the_id).maskMoney('mask',Number(the_val));
	}else if(type=="select"){		
        if($(the_id).attr("multiple") != 'undefined'){
            try{
                var val_json=$.parseJSON(the_val);
                if(val_json!=null){
                    $(the_id).val(val_json);
                    return;
                }
            }catch(e){}
        }
        $(the_id).val(the_val);
	}else if(type=="checkbox"){
		if(the_val=="1") $(the_id).prop('checked', true).change();
		else $(the_id).prop('checked', false).change();
	}else if(type=="text" && $(the_id).hasClass("itextdate") && $(the_id).data("date-format")=="dd-mm-yyyy"){	
		if(the_val=="00-00-0000" || the_val==""){
			$(the_id).val("");
		}else{
			var t=the_val.split("-");
			val=(t[2]+"-"+t[1]+"-"+t[0]);
			$(the_id).val(val);
		}
	}else if(type=="text" && $(the_id).hasClass("mydate") && $(the_id).data("date-format")=="dd-mm-yyyy"){
		if(the_val=="0000-00-00" || the_val==""){
			$(the_id).val("");
		}else{
			var t=the_val.split("-");
			val=(t[2]+"-"+t[1]+"-"+t[0]);
			$(the_id).val(val);
		}
	}else if(type=="text" && $(the_id).hasClass("mydatetime") && $(the_id).data("date-format")=="dd-mm-yyyy hh:ii"){
		if(the_val=="0000-00-00 00:00:00" || the_val==""){
			$(the_id).val("");
		}else{
			var the_datetime=the_val;
			var dt=the_datetime.split(" ");
			var the_date=dt[0];
			var t=the_date.split("-");
			val=(t[2]+"-"+t[1]+"-"+t[0]+" "+dt[1]);
			$(the_id).val(val);
		}
	}else if($(the_id).hasClass("typehead")){
		$(the_id).tagsinput('removeAll');
		try{
			var data=the_val;
			var ldata=$.parseJSON("["+data+"]");
			data=ldata[0];
			$.each(data,function(e,v){
				$(the_id).tagsinput('add', v);
			});					
		}catch(e){}
	}else if(type=="text" && $(the_id).hasClass("mydate") && (the_val=="0000-00-00" || the_val=="") ){
        $(the_id).val("");
    }else if(type=="text" && $(the_id).hasClass("mydatetime") && (the_val=="0000-00-00 00:00:00" || the_val=="") ){
        $(the_id).val("");
    }else{
		$(the_id).val(the_val);
	}
}

function smis_clear(the_id){
	if($(the_id).is(':checkbox')){
		var val=$(the_id).attr("dv");
		$(the_id).prop('checked', val=="1").change();
	}else if($(the_id).attr('typical')=="money"){
		var val=$(the_id).attr("dv");
		$(the_id).maskMoney('mask',Number(val));
	}else if($(the_id).hasClass("typehead")){
		$(the_id).tagsinput('removeAll');
	}else{
		var val=$(the_id).attr("dv");
		smis_edit(the_id,val);
	}	
}


(function( $ ) {
	var sfixheadlist=new Array();
    $.fn.sfixhead= function( action ) {
    	id=this.attr("id");
    	$("#smis_container").unbind("scroll");
    	sfixheadlist.push(id);
    	$.each(sfixheadlist,function(a,val){
    		if($("#"+val).length>0) {
    			fixTableHead(val);
    		}else{
    			sfixheadlist.splice(a,1);
    		}
    	});
        return this;
    };
}( jQuery ));

function fixTableHead(id){
	$("#smis_container").scroll(function(){
		var width=$("#"+id).width();
		var clas=$("#"+id).attr("class");
		var thead=$("#"+id).find("thead");
		var tfixelement="";
		$.each(thead.children(),function(a){
			var tag=this.nodeName;
			var cclas=$(this).attr("class");
			tfixelement+="<"+tag+" class='"+cclas+"' >";
			$.each(this.children,function(e){
				var ctag=this.nodeName;
				var html=$(this).html();
				var cwidth=$(this).width();				
				tfixelement+="<"+ctag+" style='width:"+cwidth+"px !important;'>";
				tfixelement+=html;
				tfixelement+="</"+ctag+">";				
			});
			tfixelement+="</"+tag+">";
		});		
		var tfix="<table style='z-index:300; width:"+width+"px !important;' class='"+clas+"'><thead>"+tfixelement+"</thead></table>";
		if(!thead.visible()){
			$("#fix_"+id).html(tfix);
		}else{
			$("#fix_"+id).html("");
		}
	});
}

function next_focus(current,next,savefocus,prevent,prefix){
	if( $(next).is(":disabled") || !$(next).is(":visible") || $(next).attr('type') == 'hidden' || $(next).is('[readonly]') ){
		var new_nxt=$(next).attr('data-next-enter');
		if(new_nxt=="undefined"){
			console.log(next+" next candidate undefined");
			console.log(" Trace Stop ");
			return;
		}
		next_focus(current,"#"+prefix+"_"+new_nxt,savefocus,prevent,prefix);
	}else{
		$(next).focus();
		return;
	}
}

function next_enter(current,next,savefocus,prevent,prefix){
	if(!prevent){
		$(current).keyup(function(e) { 
			if(e.keyCode == 13) {
				if(e.ctrlKey===true && savefocus!=""){
					$(savefocus).focus();
				}else{
					next_focus(current,next,savefocus,prevent,prefix);
				}
			}
		});
	}else if(prevent=="select"){
		$(current).keypress(function(e) { 
			e.preventDefault(); 
		});
		$(current).keypress(function(e) { 
			e.preventDefault(); 
		});
		
		$(current).keyup(function(e) { 			
			if(e.keyCode == 13) {
				if(e.ctrlKey===true && savefocus!=""){
					$(savefocus).focus();
				}else{
					next_focus(current,next,savefocus,prevent,prefix);
				}
			}
		});
	}else if(prevent=="textarea"){
		$(current).keyup(function(e) { 			 
			if(e.keyCode == 13) { 
				 if (e.shiftKey !== true){
					 e.preventDefault(); 
					 e.stopPropagation();
					 next_focus(current,next,savefocus,prevent,prefix);
			     }else if(e.ctrlKey===true && savefocus!=""){
			    	 e.preventDefault(); 
					 e.stopPropagation();
					 $(savefocus).focus();
			     }
			}
		});
	}
}

