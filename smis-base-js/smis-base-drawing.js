var _sdraw           = new Object();
_sdraw._scontext;
_sdraw._total        = 0;
_sdraw._maxTotal     = 0;
_sdraw._clickX       = new Array();
_sdraw._clickY       = new Array();
_sdraw._clickDrag    = new Array();
_sdraw._colorStroke  = new Array();
_sdraw._sizeStroke  = new Array();
_sdraw._tools        = new Array();
_sdraw._paint        = false;
_sdraw._canvasWidth  = 400;
_sdraw._canvasHeight = 400;
_sdraw._offsetModalX = 0;
_sdraw._offsetModalY = 0;
_sdraw._imageWidth   = 0 ;
_sdraw._imageHeight  = 0 ;
_sdraw._scale        = 1 ;
_sdraw._imageObject  = null;
_sdraw._sClickX      = new Array();
_sdraw._sClickY      = new Array();
_sdraw._canvas       = null;
_sdraw._canvasDiv    = null;
_sdraw._pencil       = true;
_sdraw._padlock      = true;
_sdraw._drawed       = 0;
_sdraw._saving_place = "";
_sdraw._image_enable = false;

_sdraw.undo=function(){
   var x=this._total;
   if(!this._clickDrag[this._total]){
       x--;
   }
   for(;x>0;x--){
       if(!this._clickDrag[x]){
           this._total=x;
           this._drawed=0;
           _sdraw.redraw();
           return;
       }
   }
};

_sdraw.color=function(color){
   $("#sdraw-color").val(color);
   _sdraw._scontext.strokeStyle  = color;
};

_sdraw.lock=function(){
   this._padlock=(!this._padlock);
   if(this._padlock){
       smis_alert("Pen Active", "Pen Has Been Activated", "alert-info");
   }else{
       smis_alert("Pen Active", "Pen Has Been Deactivated", "alert-danger");
   }
};


_sdraw.redo=function(){
   if(this._maxTotal>=this._total+1){
       var x=this._total;
       if(!this._clickDrag[this._total]){
           x++;
       }
       for(;x<this._maxTotal;x++){
           if(!this._clickDrag[x]){
               this._total=x;
               _sdraw.redraw();
               return;
           }
       }
       this._total=this._maxTotal;
       _sdraw.redraw();
       return;
   }
};

_sdraw.eraser=function(){
   this._pencil=false;  
};

_sdraw.clear=function(){
   this._total=0;
   this._drawed=0;
   this.redraw();
};

_sdraw.marker=function(){
   this._pencil=true;  
};

_sdraw.save=function(){
    this._sClickX        = new Array();
    this._sClickY        = new Array();
    for(var i=0; i < this._total; i++) {
        this._sClickX[i] = Math.ceil(this._clickX[i]/this._scale);
        this._sClickY[i] = Math.ceil(this._clickY[i]/this._scale);
    }
    json_result = _sdraw.compress();
    $(this._saving_place).val(JSON.stringify(json_result));
    $("#smis-drawing-modal").smodal("hide");
};

_sdraw.loader=function(){    
    this._total         = 0;
    this._drawed        = 0;
    this._clickX        = new Array();
    this._clickY        = new Array();
    for(var i=0; i < _sdraw._sClickX.length; i++) {
        this._clickX[i] = this._sClickX[i]*this._scale;
        this._clickY[i] = this._sClickY[i]*this._scale;
        this._total++;
    }
    this._maxTotal      = this._total;
};

_sdraw.extract = function(json_result){
    _sdraw._sClickX     = new Array();
    _sdraw._sClickY     = new Array();
    _sdraw._colorStroke = new Array();
    _sdraw._sizeStroke  = new Array();
    _sdraw._clickDrag   = new Array();    
    var curcol = null;
    var cursize = null;
    for(var i=0; i < json_result.length; i++) {
        var x=json_result[i];
        _sdraw._sClickX[i]      = x[0];
        _sdraw._sClickY[i]      = x[1];
        if(x[2]!=null){
            _sdraw._clickDrag[i]    = false;
        }else{
            _sdraw._clickDrag[i]    = true;
        }        
        if(x[3]!=null){
            _sdraw._colorStroke[i]    = x[3];
            curcol = x[3];
        }else{
            _sdraw._colorStroke[i]    = curcol;
        }
        if(x[4]!=null){
            _sdraw._sizeStroke[i]    = x[4];
            curcol = x[3];
        }else{
            _sdraw._sizeStroke[i]    = cursize;
        }
        this._total++;
    }
};

_sdraw.compress = function(){
    var result  = new Array();
    var curcol  = null;
    var cursize = null;
    for(var i=0; i < this._total; i++) {
       var x = new Array();
       x[0] = _sdraw._sClickX[i];
       x[1] = _sdraw._sClickY[i];
       if(!_sdraw._clickDrag[i]){
          x[2] = 0;
          if(curcol==null || curcol!=_sdraw._colorStroke[i]){
              curcol = _sdraw._colorStroke[i];
              x[3]   = curcol;
          }
          if(cursize==null || cursize!=_sdraw._sizeStroke[i]){
             cursize = _sdraw._sizeStroke[i];
             x[4]   = cursize;
          }
       }
       result.push(x);
    };
    return result;
};


_sdraw.showDrawing=function(image,saving_result){  
  $("#smis-drawing").html("");
  $("#smis-drawing-modal").smodal("show");
  setTimeout(function(){
     var svt                    = $(saving_result).val();
     try{
         var json_result        = $.parseJSON(svt);
         if(json_result!=null){
            _sdraw.extract(json_result);
            $("#sdraw-color").val(_sdraw._colorStroke[_sdraw._colorStroke.length-1]);
            $("#sdraw-size").val(_sdraw._sizeStroke[_sdraw._sizeStroke.length-1]);       
         }    
     }catch(e){
         _sdraw._sClickX        = new Array();
         _sdraw._sClickY        = new Array();
         _sdraw._colorStroke    = new Array();
         _sdraw._sizeStroke     = new Array();
         _sdraw._clickDrag      = new Array();
     }
     _sdraw._canvasWidth        = $("#smis-drawing-modal div.modal-body-title").width();
     _sdraw.sDrawImageInit(image);
     _sdraw._saving_place       = saving_result;         
  },1000);
};

_sdraw.showDrawingID=function(image_id,saving_result){  
   var urlImage = $(image_id).val();
   _sdraw.showDrawing(urlImage,saving_result);
};

_sdraw.addClick=function(x, y, dragging,color,is_marker,pen_size){
    this._clickX[this._total]       = x;
    this._clickY[this._total]       = y;
    this._clickDrag[this._total]    = dragging;
    this._colorStroke[this._total]  = color;
    this._sizeStroke[this._total]   = pen_size;
    this._tools[this._total]        = is_marker;
    this._total++;
    this._maxTotal                  = this._total;
};

_sdraw.drawImage=function(){
    if(this._imageObject!=null && this._image_enable){
        this._scontext.drawImage(this._imageObject,0,0,this._imageWidth,this._imageHeight,0,0,this._canvasWidth,this._canvasHeight);
    }
}

_sdraw.redraw=function(){
    this._scontext.lineJoin         = "round";
    this._scontext.lineWidth      = 1;
    if(this._drawed==0){
        this._scontext.clearRect(0, 0, this._scontext.canvas.width, this._scontext.canvas.height);
        this.drawImage();
    }
    var i = 0;
    for(i=this._drawed; i < this._total; i++) {		
        this._scontext.strokeStyle  = this._colorStroke[i];
        this._scontext.lineWidth    = this._sizeStroke[i];
        this._scontext.beginPath();
        if(this._clickDrag[i] && i){
          this._scontext.moveTo(this._clickX[i-1], this._clickY[i-1]);
        }else{
          this._scontext.moveTo(this._clickX[i]-1, this._clickY[i]);
        }
        //if(this._tools[i]){
            this._scontext.globalCompositeOperation="source-over";
        //}else{
          //  this._scontext.globalCompositeOperation="destination-out";
        //}
        this._scontext.lineTo(this._clickX[i], this._clickY[i]);
        this._scontext.closePath();
        this._scontext.stroke();
    }
    this._drawed=i;
};

_sdraw.sDrawImageInit=function(imageUrl){
    _sdraw._canvasHeight  = 400;
    if(imageUrl != ""){
        this._imageObject        = new Image();
        this._imageObject.onload = function() {
            _sdraw._imageWidth   = this.width;
            _sdraw._imageHeight  = this.height;
            _sdraw._scale        = (_sdraw._canvasWidth/_sdraw._imageWidth);
            _sdraw._canvasHeight = _sdraw._scale*_sdraw._imageHeight; 
            _sdraw._image_enable=true;
            _sdraw.loader();
            _sdraw.drawingInit();
            _sdraw.redraw();
        };
        this._imageObject.onerror=function(){
            _sdraw.loader();
            _sdraw.drawingInit();
            _sdraw.redraw();
            _sdraw._imageObject=null;
            _sdraw._image_enable=false;
        };
        this._imageObject.src = imageUrl;
    }else{
        _sdraw.loader();
        _sdraw.drawingInit();
        _sdraw.redraw();
    }
}

_sdraw.drawingInit=function(){
    this._canvasDiv             = document.getElementById('smis-drawing');
    this._canvas                = document.createElement('canvas');
    this._canvas.setAttribute('width', this._canvasWidth);
    this._canvas.setAttribute('height', this._canvasHeight);
    this._canvas.setAttribute('id', 'smis-canvas-drawing');
    this._canvasDiv.appendChild(this._canvas);
    this._scontext              = this._canvas.getContext("2d");
    this._scontext.font         = "15px Arial";
    this._scontext.strokeStyle  = "#000000";
    
    $('#smis-canvas-drawing').mousedown(function(e){  
        _sdraw._offsetModalX    = $("#smis-drawing-modal").offset().left+ this.offsetLeft;
        _sdraw._offsetModalY    = $("#smis-drawing-modal").offset().top+ this.offsetTop+50;
        var mouseX              = (e.pageX - _sdraw._offsetModalX ) + $("#smis-drawing-modal div.modal-body").scrollLeft();
        var mouseY              = (e.pageY - _sdraw._offsetModalY ) + $("#smis-drawing-modal div.modal-body").scrollTop();
        _sdraw._paint           = true;
        _sdraw.addClick(mouseX, mouseY,false,_sdraw._scontext.strokeStyle,_sdraw._pencil,_sdraw._scontext.lineWidth);
        _sdraw.redraw();
    });
    $('#smis-canvas-drawing').mousemove(function(e){
        if(_sdraw._paint){
           _sdraw._offsetModalX = $("#smis-drawing-modal").offset().left+ this.offsetLeft;
           _sdraw._offsetModalY = $("#smis-drawing-modal").offset().top+ this.offsetTop+50;
           var mouseX           = (e.pageX - _sdraw._offsetModalX ) + $("#smis-drawing-modal div.modal-body").scrollLeft();
           var mouseY           = (e.pageY - _sdraw._offsetModalY ) + $("#smis-drawing-modal div.modal-body").scrollTop();
           _sdraw.addClick(mouseX, mouseY, true,_sdraw._scontext.strokeStyle,_sdraw._pencil,_sdraw._scontext.lineWidth);
           _sdraw.redraw();
        }
    });
    $('#smis-canvas-drawing').mouseup(function(e){ _sdraw._paint = false; });
    $('#smis-canvas-drawing').mouseleave(function(e){ _sdraw._paint = false; });
    this._canvas.addEventListener("touchstart", function(e){
        if(!_sdraw._padlock){
            smis_alert("Pen Disabled","Pen Disabled, Please Activate it","smis-info");
            return;
        }
        _sdraw._offsetModalX    = $("#smis-drawing-modal").offset().left+ this.offsetLeft;
        _sdraw._offsetModalY    = $("#smis-drawing-modal").offset().top+ this.offsetTop + 50;
        var mouseX              = (e.changedTouches ? e.changedTouches[0].pageX : e.pageX) - _sdraw._offsetModalX + $("#smis-drawing-modal div.modal-body").scrollLeft();
        var mouseY              = (e.changedTouches ? e.changedTouches[0].pageY : e.pageY) - _sdraw._offsetModalY  + $("#smis-drawing-modal div.modal-body").scrollTop();
        _sdraw.addClick(mouseX, mouseY,false,_sdraw._scontext.strokeStyle,_sdraw._pencil,_sdraw._scontext.lineWidth);
        _sdraw.redraw();
    }, false);
    
    this._canvas.addEventListener("touchmove", function(e){
        if(!_sdraw._padlock){
            smis_alert("Pen Disabled","Pen Disabled, Please Activate it","smis-info");
            return;
        }
        _sdraw._offsetModalX    = $("#smis-drawing-modal").offset().left+ this.offsetLeft;
        _sdraw._offsetModalY    = $("#smis-drawing-modal").offset().top+ this.offsetTop + 50;
        var mouseX              = (e.changedTouches ? e.changedTouches[0].pageX : e.pageX) - _sdraw._offsetModalX + $("#smis-drawing-modal div.modal-body").scrollLeft();
        var mouseY              = (e.changedTouches ? e.changedTouches[0].pageY : e.pageY) - _sdraw._offsetModalY  + $("#smis-drawing-modal div.modal-body").scrollTop();
        _sdraw.addClick(mouseX, mouseY, true,_sdraw._scontext.strokeStyle,_sdraw._pencil,_sdraw._scontext.lineWidth);
        _sdraw.redraw();
    }, false);
};

$(document).ready(function(){
    $("#sdraw-color").colorpicker().on('changeColor', function() {
       $(this).parent().find('.color').css("background-color",  );
       var color                     = $("#sdraw-color").val();
       _sdraw._scontext.strokeStyle  = color;
    });
    $("#sdraw-size").on('change', function() {
        _sdraw._scontext.lineWidth  = Number($(this).val());
     });
});
