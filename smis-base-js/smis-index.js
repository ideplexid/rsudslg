

function window_updater() {
	var width = $(window).width();
	var height = $(window).height();
	$("#smis_window_wrapper").resizable({
		containment: 'parent'
	}).draggable({
		handle: "#smis_control_top",
		drag: function (e, ui) { }
	});
}


$(document).ready(function () {
	var cookie = getCookie("smis-index");
	if (cookie != "") {
		var json = JSON.parse(decodeURIComponent(cookie));
		LoadSmisPage(json);
	};
	$('#summernote').summernote({
		height: 400,
		minHeight: 300,
		onImageUpload: function (files, editor, welEditable) {
			sendFile(files[0], editor, welEditable);
		},
		toolbar: [
			['insert', ['table', 'picture', 'link', 'video', 'hr']],
			['fontsize', ['fontsize']],
			['style', ['fontname', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'help']]
		]
	}
	);

	$(document).keyup(function (e) {
		e.preventDefault();
		if (e.keyCode == 27) {
			var a = $("x").smodal("current");
			if (a != null || a != "") {
				a = a + "";
				$(a).smodal("hide");
			}
		}
		return false;
	});


	$("#search_menu_box").keyup(function (e) {
		var filter = $(this).val().toLowerCase();
		$("div#sidebar > ul > li").each(function (index) {
			if (filter == "") {
				$(this).removeClass("hide");
			} else {
				var x = $(this).children().eq(0).html();
				var index = x.indexOf("</i>");
				var name = x.slice(index + 4).trim().toLowerCase();
				if (name.lastIndexOf(filter) !== -1) {
					$(this).removeClass("hide");
				} else {
					$(this).addClass("hide");
				}
			}
		});
	}).focus(function () {
		$(this).val("");
	});



});

function sidebar() {
	if ($(".smis_sidebar").is(":visible")) {
		$(".smis_sidebar").hide('fast');
	} else {
		$(".smis_sidebar").show('fast');
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function delete_cookie(name) {
	document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function reload_page() {
	delete_cookie("smis-index");
	location.reload();
}

function logout() {
	var data = {
		page: "smis-base",
		action: "smis-login-out"
	};
	$.post("", data, function (res) {
		delete_cookie("smis-index");
		getContent(res);
		if (ux != "") {
			window.location.href = ux + "/logout_from_simrs";
		}
	});
}



/*IF USER IS LOGIN*/
function changePassword() {
	if ($("#old").val() == "" || $("#new").val() == $("#old").val() || $("#new").val() != $("#retype").val() || $("#new").val().length < 6) {
		showWarning("Error di Password", "<ul><li>Pastikan Password Lama Benar</li> <li>Ketikan Password Baru Dua Kali</li> <li>Password baru Tidak Boleh Sama dengan Password Lama</li> <li>Password Baru Harus Lebih dari 6 Karakter</li></ul>");
		return;
	}
	$.post("",
		{
			page: "smis-base",
			action: "smis-edit-user",
			edit: "password",
			old: $("#old").val(),
			pass: $("#new").val()
		},
		function (res) {
			content = getContent(res);
			if (content != null) {
				$("#old").val('');
				$("#new").val('');
				$("#retype").val('');
			}
		});
}


function save_settings() {
	var login_timeout = $("#login_timeout").val();
	var st_mmode = $("#st_mmode").val();
	var st_smode = $("#st_smode").val();
	var colorslider = $("#color_slider").val();
	var colorinvert = $("#color_invert").is(":checked") ? "1" : "0";
	var colorgrayscale = $("#color_grayscale").val();
	var css = $("#css").val();
	$.post("",
		{
			page: "smis-base",
			action: "smis-edit-user",
			edit: "settings",
			timeout: login_timeout,
			color_slider: colorslider,
			color_invert: colorinvert,
			color_grayscale: colorgrayscale,
			st_mmode: st_mmode,
			st_smode: st_smode,
			css: css
		},
		function (res) {
			content = getContent(res);
		});
}
