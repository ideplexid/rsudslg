/**
 * this used to handle mobile device so 
 * the system can handle swipe left or swipe right while
 * dealing with the mobile device.
 * 
 * @author      : Nurul Huda
 * @since       : 13 Jan 2018
 * @version     : 1.0.0
 * @license     : LPGLv3
 * @copyright   : goblooge@gmail.com
 * 
 * */

$(document).ready(function(){
    $("div#smis_window_wrapper, div#navigator").swipe( {        
        swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
            if(!$(".smis_sidebar").is(":visible") ){
                $(".smis_sidebar").show('fast');
            }
        },
        swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
            if($(".smis_sidebar").is(":visible") ){
                $(".smis_sidebar").hide('fast');
            }
        },
        threshold:10
    });
    
    $("div.smis_sidebar").swipe( {        
        swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
            if($(".smis_sidebar").is(":visible") ){
                $(".smis_sidebar").hide('fast');
            }
        },
        threshold:10
    });
});