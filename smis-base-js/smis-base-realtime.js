var SMIS_REALTIME_TIMEOUT=-1;
function realtime(){
	var data={
			page:"smis-base",
			action:"smis-realtime"
	};
	//realtimeload(true);
	$.post("",data,function(res){
		var json=getContent(res);
		if(json!=null){
			is_new_notification(json.notification);
			reinit_time(json.realtime);			
		}
		//realtimeload(false);
		if(SMIS_REALTIME_TIMEOUT>15000){
			setTimeout(realtime, SMIS_REALTIME_TIMEOUT);
		}
	});
}

function reinit_time(s_num){
	try{
		var n=Number(s_num);
		SMIS_REALTIME_TIMEOUT=n;		
	}catch(e){
		
	}
}

function realtimeload(b){
	if(b){
		$("#smis_spin").addClass("fa-spin");
		$("#smis_activity").html("Checking");
	}else{
		$("#smis_spin").removeClass("fa-spin");
		$("#smis_activity").html("Standby");
	}
}

function is_new_notification(nbr){
	number=Number(nbr);
	var cur=$("#smis_notification").html();
	if(cur=="0" && number>0 || Number(cur) < number  ){
		$("#smis_notification").html(number);
		play_sound();
	}else {
		$("#smis_notification").html(number);
	}
}

$(document).ready(function(){
	var init_time=$("#realtime_init_time").val();
	var n=Number(init_time);
	SMIS_REALTIME_TIMEOUT=n;
	realtime();
});
