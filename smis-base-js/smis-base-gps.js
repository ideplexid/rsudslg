var _sgps = new Object();
_sgps.loaded = false;
_sgps_initMap = function(){
    var uluru = {lat: -25.344, lng: 131.036};
    var element = document.getElementById('map-modal-viewer');
    var geocoder= new google.maps.Geocoder;
    var map = new google.maps.Map(element, {
        center: uluru,
        zoom: 8
      });
      var marker;
      $("#smis-map-chooser").smodal("show");

      google.maps.event.addListener(map, 'click', function(args) {
        if(marker!=null){
            marker.setMap(null);
        }
        marker = new google.maps.Marker({position: args.latLng, map: map});
        $("#smis-map-longitude").val(args.latLng.lng);
        $("#smis-map-latitude").val(args.latLng.lat);

        geocoder.geocode({'location': args.latLng}, function(results, status) {
            if (status === 'OK') {
            if (results[1]) {
                $("#smis-map-address").val(results[1].formatted_address);
            } else {
                $("#smis-map-address").val("");
            }
            } else {
                $("#smis-map-address").val("");
            }
        });
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('smis-map-search');
    console.log(input);
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }
          map.setCenter(place.geometry.location)

          if (place.geometry.viewport) {
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
};

_sgps.show = function(tbl_action,lon,lat){
    if(!_sgps.loaded){
        $.getScript( "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&" +
        "callback=_sgps_initMap&&key=AIzaSyDVUl5iuUbxug0Wgq1KGt5cdhFtL5ufOC0" )
        .done(function( script, textStatus ) {
            _sgps.loaded = true;
        })
        .fail(function( jqxhr, settings, exception ) {
            showWarning("Connection Problem...","Failed Load Map Caused By Network Problem");
        });    
    }else{
        $("#smis-map-chooser").smodal("show");
    }
};


