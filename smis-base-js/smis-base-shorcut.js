
$(document).ready(function(){	
    shortcut.add("Ctrl+Shift+H", function() {
        sidebar();
    });

    shortcut.add("Ctrl+Shift+M", function() {
        $("#search_menu_box").focus();
    });

    shortcut.add("Ctrl+Shift+P", function() {
        if($("div#non_printing_area").is(':visible')){
            $("div#non_printing_area").hide();
            $("div#printing_area").show();
        }else{
            $("div#non_printing_area").show();
            $("div#printing_area").hide();			
        }
    });

    shortcut.add("Ctrl+Shift+L", function() {
        if($("div#non_printing_area").is(':visible')){
            $("div#non_printing_area").hide();
            $("div#printing_area").show();
            smis_print();
        }else{
            $("div#non_printing_area").show();
            $("div#printing_area").hide();			
        }
    });
});