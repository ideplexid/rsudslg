
function show_hide_smis_container(){
	if($("#smis_window_wrapper").is(":hidden")){
		$("#smis_window_wrapper").show("slow");
	}else{
		$("#smis_window_wrapper").hide("slow");
	}
}

function showPrintPreview(text){
	$("#smis-print-preview .modal-body").html(text);
	$("#smis-print-preview").smodal("show");
}

function showFullWarning(title,text,size){
	$("#warning").addClass(size);
	$("#warning .modal-body").html(text);
	$("#warning .modal-header h3").html(title);
	$("#warning").smodal("show");
}

function showWarning(title,text){
	$("#warning").removeClass("full_model").removeClass("normal_model").removeClass("half_model");
	$("#warning .modal-body").html(text);
	$("#warning .modal-header h3").html(title);
	$("#warning").smodal("show");
}

function showConfirm(title,text,confirm){
	$("#confirm .modal-body").html(text);
	$("#confirm .modal-header h3").html(title);
	$("#confirm_button").val(confirm);
	$("#confirm").smodal("show");
}


function setBreadcrumb(name){
	$("#breadcrumb").html(name);
}


function smis_alert(title, content, css,time_out=3000){
	var alrt="<div class='alert alert-block "+css+" '>";
	  alrt=alrt+"<h4>"+title+"</h4>";
	  alrt=alrt+content;
	  alrt=alrt+"</div>";
	  $("#alert").html(alrt);
	  $("#alert").show('slow');
	  window.setTimeout(function() { $("#alert").hide(); $("#alert .alert").alert('close'); }, time_out);
}

function show_chooser(action_table,title,text,is_show,modal_title){
	if(modal_title==null || modal_title=="" ) modal_title=title;
	$("#smis-chooser-modal .modal-body").html(text);
	$("#smis-chooser-modal .modal-header h3").html(modal_title.toUpperCase());
	$("#smis-chooser-modal ").smodal("show");
}

function hide_chooser(){
	if($("#smis-chooser-modal").hasClass("in")){
		$("#smis-chooser-modal").smodal("hide");
	}
}

function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

function scrolToAnimate(targetElement, speed) {
    var scrollWidth = $(targetElement).get(0).scrollWidth;
    var clientWidth = $(targetElement).get(0).clientWidth;
    $(targetElement).animate({ scrollLeft: scrollWidth - clientWidth },
    {
        duration: speed,
        complete: function () {
            /*targetElement.animate({ scrollLeft: 0 },
            {
                duration: speed,
                complete: function () {
                    scrolToAnimate(targetElement, speed);
                }
            });*/
        }
    });
};