function money(number){
	var formattedMoney = 'Rp. ' + number.formatMoney(2,',','.');
	return formattedMoney ;
}

function setMoney(id,money){
	var num=Number(money);
	$(id).maskMoney('mask',num);
}

function getMoney(the_id){
	var val=$(the_id).maskMoney('unmasked')[0];
	return val;
}

function formatMoney(the_id){
	var val=$(the_id).val();
	$(the_id).maskMoney();
	$(the_id).maskMoney('mask',Number(val));
}

