/**
 * @version 1.0
 * @author goblooge
 * @license LGPL v3
 * @since 16 Nov 2014
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @param $
 * 
 * this function automatically prettyfy the json source code string
 * into good mode
 */
(function( $ ) {
	$.fn.fetreefy= function( ) {
		return this.each(function() {
    		var json=$(this).html();
    		 json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    		 json=json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
 		        var style = 'color:black;font-weight:800;';//number
 		        if (/^"/.test(match)) {
 		            if (/:$/.test(match)) {
 		            	style = 'color:black;';//key
 		            } else {
 		            	style = 'color:darkgreen;';//string
 		            }
 		        } else if (/true|false/.test(match)) {
 		        	style = 'color:darkblue;font-weight:800;';//boolean
 		        } else if (/null/.test(match)) {
 		        	style = 'color:red;text-transform: uppercase;font-weight:800;';//null
 		        }
 		        return '<span style="' + style + '">' + match + '</span>';
 		    });
	 		$(this).html(json);
        });
    };
}( jQuery ));
