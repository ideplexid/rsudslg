var sceditormirror;
var sceditor;
CodeMirror.commands.autocomplete = function(cm) {
		cm.showHint({hint: CodeMirror.hint.anyword});
}

function makeMarker() {
		var marker = document.createElement("div");
		marker.style.color = "#822";
		marker.innerHTML = "●";
		return marker;
}
	
	
$(document).ready(function(){	
	sceditor=new TableAction("sceditor","smis-base","sceditor");
	sceditor.scsave=function(){
		var hasil=sceditormirror.getValue();
		var d=this.getRegulerData();
		d['action']="smis-file-saver";
		d['filepath']=$("#sceditormirror_filename").val();
		d['filecontent']=hasil;
		showLoading();
		$.post("",d,function(res){
			var json=getContent(res);
			dismissLoading();
		});
	}	
	sceditor.scload=function(){
		var val = $("#sceditormirror_filename").val();		
		var d=this.getRegulerData();
		d['action']="smis-file-info";
		d['filepath']=$("#sceditormirror_filename").val();
		showLoading();
		$.post("",d,function(res){
			dismissLoading();
			var json=getContent(res);
			if(json!=null && json.exist==1){
				if(json.msg!=""){
					showWarning("Error",json.msg);
				}else{
					if(val!=""){
						var m, mode, spec;
						  if (m = /.+\.([^.]+)$/.exec(val)) {
						    var info = CodeMirror.findModeByExtension(m[1]);
						    if (info) {
						      mode = info.mode;
						      spec = info.mime;
						    }
						  } else if (/\//.test(val)) {
						    var info = CodeMirror.findModeByMIME(val);
						    if (info) {
						      mode = info.mode;
						      spec = val;
						    }
						  } else {
						    mode = spec = val;
						  }
						  if (mode) {
							CodeMirror.modeURL = "smis-framework/codemirror/js/%N.js";
						    sceditormirror.setOption("mode", spec);
						    CodeMirror.autoLoadMode(sceditormirror, mode);
						    $("#sceditormirror_filetype").val(spec);
						  } else {
							sceditormirror.setOption("mode", "text/plain");
							 $("#sceditormirror_filetype").val("text/plain");
						    showWarning("Warning","Could not find a mode corresponding to " + val+", Mode Set To text/plain !!");
						  }	
					}
				}
				$("#sceditormirror_filesize").val(json.size);
				$("#sceditormirror_fileread").val(json.read+"");
				$("#sceditormirror_filewrite").val(json.write+"");
				$("#sceditormirror_fileexec").val(json.exec+"");
				$("#sceditormirror_filelines").val(json.lines);
				sceditormirror.setOption("readOnly", json.write+""=="false");
				sceditormirror.getDoc().setValue(json.filecontent);
				sceditormirror.setOption("theme", $("#sceditormirror_theme").val());
			}else{
				showWarning("Error","Error Fetching File Perhaps it's not Folder or File Not Exist or Damage !!");
			}
		});		
	};
	sceditormirror = CodeMirror(document.getElementById("sceditormirror"), {
	  value: "<\?php \?>",
	  mode:  "application/x-httpd-php",
	  indentUnit:5,
	  electricChars:false,
	  lineNumbers: true,
	  lineWrapping: false,
	  autofocus:true,
      autoCloseTags: true,
	  styleActiveLine: true,
	  matchTags: {bothTags: true},
	  autoCloseBrackets: true,
	  viewportMargin: Infinity,
	  showTrailingSpace: true,
	  commentBlankLines:true,
	  highlightSelectionMatches: {showToken: /\w/},
	  extraKeys: {"Ctrl-Space": "autocomplete","Ctrl-F11": function(cm) {cm.setOption("fullScreen", !cm.getOption("fullScreen"));}},
	  foldGutter: true,
	  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter","breakpoints"]		  
	});

	sceditormirror.on("gutterClick", function(cm, n) {
	  var info = cm.lineInfo(n);
	  cm.setGutterMarker(n, "breakpoints", info.gutterMarkers ? null : makeMarker());
	});

	$("#sceditormirror_theme, #sceditormirror_linenumber, #sceditormirror_linewrap, #sceditormirror_actag, #sceditormirror_acbrac, #sceditormirror_aline, #sceditormirror_tspace, #sceditormirror_foldgutter").on("change",function(){
		var id=this.id;
		if(id=="sceditormirror_theme") sceditormirror.setOption("theme", this.value);
		if(id=="sceditormirror_linenumber") sceditormirror.setOption("lineNumbers", this.value=="y");
		if(id=="sceditormirror_linewrap") sceditormirror.setOption("lineWrapping", this.value=="y");
		if(id=="sceditormirror_actag") sceditormirror.setOption("autoCloseTags", this.value=="y");
		if(id=="sceditormirror_acbrac") sceditormirror.setOption("autoCloseBrackets", this.value=="y");
		if(id=="sceditormirror_aline") sceditormirror.setOption("styleActiveLine", this.value=="y");
		if(id=="sceditormirror_tspace") sceditormirror.setOption("showTrailingSpace", this.value=="y");
		if(id=="sceditormirror_foldgutter") sceditormirror.setOption("foldGutter", this.value=="y");
	});
    
    
});