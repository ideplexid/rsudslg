/**
 * create tree based on Bootstrap 2.3.2 UI div and span
 * this tree should associate with an HTML like theese :
 * 
 * <div class='tree well' id='manual_service'>
 * 	<ul>
 * 		<li style='display:list-item;' class='parent_li'>
 * 			<span id='kasir_span' class='badge badge-info' title='Collapse this branch'>
 * 				<i id='kasir_icon' class='fa fa-plug'></i>
 * 				<font id='kasir_name'> KASIR </font>
 * 				<font id='kasir_value'></font>
 * 			</span>
 * 			<ul>
 * 				<li>
 * 					<span id='get_tagihan_span' class=''>
 * 						<i id='get_tagihan_icon' class=''></i>
 * 						<font id='get_tagihan_name'> get_tagihan </font>
 * 						<font id='get_tagihan_value'> 
 * 							<a href='#' onclick='manual_service('kasir','get_tagihan')'>
 * 								<i class='fa fa-book'></i>
 * 							</a>
 * 						</font>
 * 					</span>
 * 				</li>
 * 			</ul>
 * 		</li>
 * 	 </ul>
 *  </div> 
 * 
 * example using with this html :
 * $('#manual_service').stree();
 * 
 * @see Tree
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 * 
 */

(function( $ ) {
	$.fn.stree= function( ) {
    	var name=this.selector+"";
    	$(name+' li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
	    $(name+' li.parent_li > span').on('click', function (e) {
	        var children = $(this).parent('li.parent_li').find(' > ul > li');
	        if (children.is(":visible")) {
	            children.hide('fast');
	        } else {
	            children.show('fast');
	        }
	        e.stopPropagation();
	    });
    };
}( jQuery ));

