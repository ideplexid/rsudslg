/**
 * 
 * this one to handle the bootstrap overlap flip modal
 * so it will never be overlapped and 
 * stop to make stack overflow
 * 
 * @author goblooge
 * @since 7 Nov 2014
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGL v3
 * @param $
 */

(function( $ ) {
    $.fn.sload= function( active,name,value ) {
    	if ( active==="true") {
        	this.find("#loading_bar").addClass("active");
        	this.removeClass("hide");
        }else{
        	this.addClass("hide");
        	this.find("#loading_bar").removeClass("active");
        }
    	this.find("#loading_bar").find(".bar").css("width",value+"%");
    	this.find(".loading_title").html(name);
        return this;
    };
}( jQuery ));

/**
 * 
 * this one will handle enter in 
 * input element and change it into tab
 * 
 * @author goblooge
 * @since 7 Nov 2014
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGL v3
 * @param $
 */

(function($) {
    $.fn.enterAsTab = function(options) {
        var settings = $.extend({
            'allowSubmit': false
        }, options);
        $(this).find('input, select, textarea, button').live("keydown", {localSettings: settings}, function(event) {
            if (settings.allowSubmit) {
                var type = $(this).attr("type");
                if (type == "submit") {
                    return true;
                }
            }
            if (event.keyCode == 13) {
                var inputs = $(this).parents("form").eq(0).find(":input:visible:not(:disabled):not([readonly])");
                var idx = inputs.index(this);
                if (idx == inputs.length - 1) {
                    idx = -1;
                } else {
                    inputs[idx + 1].focus(); // handles submit buttons
                }
                try {
                    inputs[idx + 1].select();
                }
                catch (err) {
                    // handle objects not offering select
                }
                return false;
            }
        });
        return this;
    };
})(jQuery);


/**
 * this object used to simplify and handle the 
 * loader property in smis system
 * so user did not need to define and create 
 * a loader by their selves
 * use this loader and it will simply
 * make it work.
 * 
 * @author Nurul Huda
 * @since 7 Nov 2014
 * @copyright goblooge@gmail.com
 * @license LGL v3
 * @param $
 * */
var smis_loader=new Object();
smis_loader.is_shown=false;
/** show the loader*/
smis_loader.showLoader=function(){
    if(!this.is_shown){
        this.is_shown=true;
        $("#smis-loader-modal").smodal("show");    
    }
};
/** hide the loader*/
smis_loader.hideLoader=function(){
    this.is_shown=false;
    $("#smis-loader-modal").smodal("hide");
};
/** update the loader*/
smis_loader.updateLoader=function(active,title,percentage){
    $("#smis-loader-bar").sload(active,title,percentage);
};
/** cancel the loader **/
smis_loader.cancel=function(){
    this.onCancel();
    this.hideLoader();
};
/** while cancel **/
smis_loader.onCancel=function(){
    /*user must override this if want to to his own process cancel */
};

smis_loader.isShown=function(){
    return this.is_shown;
};
