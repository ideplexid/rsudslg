<?php
class ObatPBAdapter extends ArrayAdapter {
	public function adapt($row) {
		$array = array();
		$array['id'] = $row['id'];
		if (substr($row['kode_barang'], 0, 4) == "JKN.")
			$array['Kode'] = "319." . substr($row['kode_barang'], 4);
		else if (substr($row['kode_barang'], 0, 4) == "REG.")
			$array['Kode'] = "309." . substr($row['kode_barang'], 4);
		else
			$array['Kode'] = $row['kode'];
		$array['Obat'] = $row['nama_barang'];
		$array['Jenis'] = $row['nama_jenis_barang'];
		$array['Sisa'] = $row->sisa;
		$array['Satuan'] = $row->satuan;
		$array['Keterangan'] = $row->keterangan;
		return $array;
	}
}
?>