<?php 
    global $db;
    require_once "smis-libs-class/MasterTemplate.php";
    $query = "SELECT a.nama_obat, SUM(b.sisa) AS sum_sisa FROM smis_fr_dobat_f_masuk AS a, smis_fr_stok_obat AS b WHERE a.id = b.id_dobat_masuk";
    $result = $db->get_result($query);
?>
<html>
    <table class='table table-bordered'>
        <thead>
            <tr>
                <td>Nomor</td>
                <td>Nama Obat</td>
                <td>Stok Depo</td>
                <td>Info</td>
            </tr>
        </thead>
        <tbody>
            <?php 
                $no = 1;
                //$stok_min = 5;
                $info1 = 'In stock';
                $info2 = 'Out of stock';
                foreach($result as $x){
                    echo "<tr>";
                        echo "<td>".$no."</td>";
                        echo "<td>".$x->nama_obat."</td>";
                        echo "<td>".$x->sum_sisa."</td>";
                        echo "<td>".$info2."</td>";
                    echo "</tr>";
                    $no = $no + 1;
                }
            ?>
        </tbody>
    </table>
</html>    
