<?php 
	$daftar_obat_table = new Table(
		array("Nomor", "Kode Obat", "Nama Obat", "Jenis Obat", "Stok"),
		"Gudang Farmasi : Daftar Obat",
		null,
		true
	);
	$daftar_obat_table->setName("daftar_obat");
	$daftar_obat_table->setAddButtonEnable(false);
	$daftar_obat_table->setPrintButtonEnable(false);
	$daftar_obat_table->setReloadButtonEnable(false);
	$daftar_obat_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class DaftarObatAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id_obat;
				$array['Nomor'] = self::format("digit8", $row->id_obat);
				$array['Kode Obat'] = $row->kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Stok'] = $row->stok;
				return $array;
			}
		}
		$daftar_obat_adapter = new DaftarObatAdapter();
		$columns = array("id_obat", "nama_obat", "nama_jenis_obat", "sisa");
		$daftar_obat_dbtable = new DBTable(
			$db,
			"smis_fr_stok_obat",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_fr_stok_obat.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok'
				FROM (
					SELECT v_dobat_masuk.id_obat, smis_fr_stok_obat.kode_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, SUM(smis_fr_stok_obat.sisa) AS 'sisa', smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, CASE smis_fr_stok_obat.label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
					FROM smis_fr_stok_obat LEFT JOIN (
						SELECT label, id, id_obat
						FROM smis_fr_dobat_f_masuk
					) v_dobat_masuk ON smis_fr_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_fr_stok_obat.label = v_dobat_masuk.label
					WHERE smis_fr_stok_obat.prop NOT LIKE 'del' " . $filter . "
					GROUP BY v_dobat_masuk.id_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, smis_fr_stok_obat.label
				) v_obat
				WHERE sisa > 0
				GROUP BY id_obat
			) v_stok
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok'
				FROM (
					SELECT v_dobat_masuk.id_obat, smis_fr_stok_obat.kode_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, SUM(smis_fr_stok_obat.sisa) AS 'sisa', smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, CASE smis_fr_stok_obat.label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
					FROM smis_fr_stok_obat LEFT JOIN (
						SELECT label, id, id_obat
						FROM smis_fr_dobat_f_masuk
					) v_dobat_masuk ON smis_fr_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_fr_stok_obat.label = v_dobat_masuk.label
					WHERE smis_fr_stok_obat.prop NOT LIKE 'del' " . $filter . "
					GROUP BY v_dobat_masuk.id_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, smis_fr_stok_obat.label
				) v_obat
				WHERE sisa > 0
				GROUP BY id_obat
			) v_stok
		";
		$daftar_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$daftar_obat_dbresponder = new DBResponder(
			$daftar_obat_dbtable,
			$daftar_obat_table,
			$daftar_obat_adapter
		);
		$data = $daftar_obat_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $daftar_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function DaftarObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DaftarObatAction.prototype.constructor = DaftarObatAction;
	DaftarObatAction.prototype = new TableAction();
	
	var daftar_obat;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#daftar_obat_max").val(50);
		var daftar_obat_columns = new Array("id_obat", "nama_obat", "nama_jenis_obat", "sisa");
		daftar_obat = new DaftarObatAction(
			"daftar_obat",
			"gudang_farmasi",
			"daftar_obat",
			daftar_obat_columns
		);
		daftar_obat.view();
	});
</script>