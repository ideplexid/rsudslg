<?php
    global $db;

    if (isset($_POST['command'])) {
        if ($_POST['command'] == "verify_period") {
            $selected_period = strtotime($_POST['periode'] . "-01");
            $current_period = strtotime(date("Y-m-") . "01");
            $valid = 0;
            if ($selected_period < $current_period)
                $valid = 1;
            echo json_encode($valid);
        } else if ($_POST['command'] == "verify_transactions") {
            $start_date = $_POST['periode'] . "-01";
            $end_date = date("Y-m-d");
            
            // Cek Status Restok pada Semua Transaksi Retur Obat Unit Gudang Farmasi 
            $row = $db->get_row("
                SELECT
                    COUNT(*) jumlah
                FROM
                    smis_fr_retur_obat_unit
                WHERE
                    prop = '' 
                        AND (
                            (status = 'sudah' AND restok = 0)
                                OR (status = 'belum')
                        )
                        AND tanggal >= '" . $start_date . "'
                        AND tanggal <= '" . $end_date . "'
            ");
            $jumlah_transaksi_retur = 0;
            if ($row != null)
                $jumlah_transaksi_retur = $row->jumlah;
            
            // Cek Status pada Semua Transaksi Obat Keluar Unit Gudang Farmasi
            $row = $db->get_row("
                SELECT
                    COUNT(*) jumlah
                FROM
                    smis_fr_obat_keluar
                WHERE
                    prop = '' 
                        AND status = 'belum'
                        AND tanggal >= '" . $start_date . "'
                        AND tanggal <= '" . $end_date . "'
            ");
            $jumlah_transaksi_obat_keluar_unit = 0;
            if ($row != null)
                $jumlah_transaksi_obat_keluar_unit = $row->jumlah;
            
            // Cek Status pada Semua Transaksi Mutasi Antar Unit Depo Farmasi ke Ruangan Non-Depo :
            $depo_valid = 1;
            $data_suffix_arr = array(
                "depo_farmasi_irja_irna"    => "dfm",
                "depo_farmasi_igd"          => "dfm2",
                "depo_farmasi_ok"           => "dfm3",
                "depo_farmasi_icu"          => "dfm4",
                "depo_farmasi_akhp"         => "dfm5"
            );
            $data_transaksi_arr = array(
                "depo_farmasi_irja_irna" => array(
                    "masuk"     => 0,
                    "keluar"    => 0
                ),
                "depo_farmasi_igd" => array(
                    "masuk"     => 0,
                    "keluar"    => 0
                ),
                "depo_farmasi_ok" => array(
                    "masuk"     => 0,
                    "keluar"    => 0
                ),
                "depo_farmasi_icu" => array(
                    "masuk"     => 0,
                    "keluar"    => 0
                ),
                "depo_farmasi_akhp" => array(
                    "masuk"     => 0,
                    "keluar"    => 0
                )
            );
            foreach ($data_suffix_arr as $depo => $suffix) {                
                $row = $db->get_row("
                    SELECT
                        COUNT(*) jumlah
                    FROM
                        smis_" . $suffix . "_obat_masuk
                    WHERE
                        prop = '' 
                            AND unit NOT LIKE 'gudang_farmasi'
                            AND tanggal >= '" . $start_date . "'
                            AND tanggal <= '" . $end_date . "'
                            AND status = 'belum'
                ");
                if ($row != null) {
                    $data_transaksi_arr[$depo]['masuk'] = $row->jumlah;
                    if ($row->jumlah > 0)
                        $depo_valid = 0;
                }
                
                $row = $db->get_row("
                    SELECT
                        COUNT(*) jumlah
                    FROM
                        smis_" . $suffix . "_mutasi_keluar
                    WHERE
                        prop = '' 
                            AND waktu >= '" . $start_date . "'
                            AND waktu <= '" . $end_date . "'
                            AND status = 'belum'
                ");
                if ($row != null) {
                    $data_transaksi_arr[$depo]['keluar'] = $row->jumlah;
                    if ($row->jumlah > 0)
                        $depo_valid = 0;
                }
            }

            $valid = 1;
            $message = "";
            if ($jumlah_transaksi_retur > 0 || $jumlah_transaksi_obat_keluar_unit > 0 || $depo_valid == 0) {
                $message = "Tidak dapat menyajikan data dikarenakan :<br/><ul>";
                if ($jumlah_transaksi_retur > 0)
                    $message .= "<li>Terdapat " . $jumlah_transaksi_retur . " transaksi Retur Obat Unit di GUDANG FARMASI yang belum terkonfirmasi.</li>";
                if ($jumlah_transaksi_obat_keluar_unit > 0)
                    $message .= "<li>Terdapat " . $jumlah_transaksi_obat_keluar_unit . " transaksi Obat Keluar Unit di GUDANG FARMASI yang belum terkonfirmasi.</li>";
                
                foreach ($data_transaksi_arr as $depo => $data) {
                    if ($data['masuk'] > 0)
                        $message .= "<li>Terdapat " . $data['masuk'] . " transaksi Mutasi Obat Antarunit - Masuk di " . ArrayAdapter::format("unslug", $depo) . " yang belum terkonfirmasi.</li>";
                    if ($data['keluar'] > 0)
                        $message .= "<li>Terdapat " . $data['keluar'] . " transaksi Mutasi Obat Antarunit - Keluar di " . ArrayAdapter::format("unslug", $depo) . " yang belum terkonfirmasi.</li>";
                }

                $message .= "</ul>";
                $valid = 0;
            }
            echo json_encode(array(
                'message'   => $message,
                'valid'     => $valid
            ));
        } else if ($_POST['command'] == "get_jumlah_obat") {
            $data_obat_row = $db->get_row("
                SELECT 
                    COUNT(*) jumlah
                FROM
                    smis_pr_barang
                WHERE
                    prop = '' AND medis = 1 AND inventaris = 0
            ");
			$jumlah = 0;
			if ($data_obat_row != null)
				$jumlah = $data_obat_row->jumlah;
			$data = array();
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
            $periode = $_POST['periode'];
            $periode_parts = explode("-", $periode);
            $bulan = $periode_parts[1];
            $tahun = $periode_parts[0];
            $num = $_POST['num'];

            $id_obat = "";
            $kode_obat = "";
            $nama_obat = "";
            $nama_jenis_obat = "";
            $html = "";

            $data_obat_row = $db->get_row("
                SELECT 
                    *
                FROM
                    smis_pr_barang
                WHERE
                    prop = '' AND medis = 1 AND inventaris = 0
                ORDER BY 
                    nama_jenis_barang ASC,
                    nama ASC
                LIMIT
                    " . $num . ", 1
            ");
            if ($data_obat_row != null) {
                $id_obat = $data_obat_row->id;
                $kode_obat = $data_obat_row->kode;
                $nama_obat = $data_obat_row->nama;
                $nama_jenis_obat = $data_obat_row->nama_jenis_barang;

                $nama_generik = "";
                $nama_bermerk = "";
                $bentuk_sediaan = "";
                $kekuatan = "";
                $stok_awal = 0;
                $jumlah_penerimaan = 0;
                $jumlah_penggunaan_rawat_jalan = 0;
                $jumlah_penggunaan_rawat_inap = 0;
                $jumlah_penggunaan_total = 0;
                $harga_per_item = 0;
                $total_harga = 0;

                /// Nama Generik / Bermerk :
                if ($data_obat_row->generik == 1)
                    $nama_generik = $nama_obat;
                else
                    $nama_bermerk = $nama_obat;
                //. End of Nama Generik / Bermerk

                /// Bentuk Sediaan :
                $bentuk_sediaan = $data_obat_row->satuan_konversi;
                //. End of Bentuk Sediaan

                $depo_farmasi_arr = array(
                    "depo_farmasi_irja_irna"    => "dfm",
                    "depo_farmasi_igd"          => "dfm2",
                    "depo_farmasi_ok"           => "dfm3",
                    "depo_farmasi_icu"          => "dfm4",
                    "depo_farmasi_akhp"         => "dfm5"
                );
                /// Stok Awal :
                $stok_gudang = 0;
                $stok_gudang_row = $db->get_row("
                    SELECT
                        SUM(a.sisa) stok
                    FROM
                        smis_fr_stok_obat a
                            INNER JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
                    WHERE
                        a.prop = '' AND a.sisa > 0 AND b.id_obat = '" . $id_obat . "'
                ");
                if ($stok_gudang_row != null)
                    $stok_gudang = $stok_gudang_row->stok;
                $riwayat_stok_gudang_masuk = 0;
                $riwayat_stok_gudang_keluar = 0;
                $riwayat_stok_gudang_row = $db->get_row("
                    SELECT
                        SUM(a.jumlah_masuk) masuk, SUM(a.jumlah_keluar) keluar
                    FROM 
                        smis_fr_riwayat_stok_obat a 
                            INNER JOIN smis_fr_stok_obat b ON a.id_stok_obat = b.id
                            INNER JOIN smis_fr_dobat_f_masuk c ON b.id_dobat_masuk = c.id
                    WHERE
                        c.id_obat = '" . $id_obat . "' 
                            AND a.prop = '' 
                            AND a.tanggal >= '" . $periode . "-01'
                ");
                if ($riwayat_stok_gudang_row != null) {
                    $riwayat_stok_gudang_masuk = $riwayat_stok_gudang_row->masuk;
                    $riwayat_stok_gudang_keluar = $riwayat_stok_gudang_row->keluar;
                }
                $stok_awal += $stok_gudang - $riwayat_stok_gudang_masuk + $riwayat_stok_gudang_keluar;

                foreach ($depo_farmasi_arr as $depo_farmasi => $prefix) {
                    $stok_depo = 0;
                    $stok_depo_row = $db->get_row("
                        SELECT
                            SUM(a.sisa) stok
                        FROM 
                            smis_" . $prefix . "_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_obat_masuk b ON a.id_obat_masuk = b.id
                        WHERE
                            a.prop = '' 
                                AND b.prop = '' 
                                AND b.status = 'sudah'
                                AND a.id_obat = '" . $id_obat . "'
                    ");
                    if ($stok_depo_row != null)
                        $stok_depo = $stok_depo_row->stok;
                    $riwayat_stok_depo_masuk = 0;
                    $riwayat_stok_depo_keluar = 0;
                    $riwayat_stok_depo_row = $db->get_row("
                        SELECT
                            SUM(a.jumlah_masuk) masuk, SUM(a.jumlah_keluar) keluar
                        FROM 
                            smis_" . $prefix . "_riwayat_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_stok_obat b ON a.id_stok_obat = b.id
                        WHERE
                            b.id_obat = '" . $id_obat . "' 
                                AND a.prop = '' 
                                AND MONTH(a.tanggal) = '" . $bulan . "' 
                                AND YEAR(a.tanggal) = '" . $tahun ."' 
                    ");
                    if ($riwayat_stok_depo_row != null) {
                        $riwayat_stok_depo_masuk = $riwayat_stok_depo_row->masuk;
                        $riwayat_stok_depo_keluar = $riwayat_stok_depo_row->keluar;
                    }
                    $stok_awal += $stok_depo - $riwayat_stok_depo_masuk + $riwayat_stok_depo_keluar;
                }
                //. End of Stok Awal

                /// Penerimaan :
                $jumlah_bbm_masuk = 0;
                $bbm_masuk_row = $db->get_row("
                    SELECT
                        SUM(c.jumlah) jumlah
                    FROM
                        smis_fr_obat_f_masuk a 
                            INNER JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
                            INNER JOIN smis_fr_stok_obat c ON b.id = c.id_dobat_masuk
                    WHERE 
                        MONTH(a.tanggal) = '" . $bulan . "' 
                            AND YEAR(a.tanggal) = '" . $tahun . "' 
                            AND b.prop = ''
                            AND b.id_obat = '" . $id_obat . "'
                            AND c.jumlah > 0
                ");
                if ($bbm_masuk_row != null)
                    $jumlah_bbm_masuk = $bbm_masuk_row->jumlah;
                $jumlah_penerimaan += $jumlah_bbm_masuk;

                $jumlah_bbm_dibatalkan = 0;
                $bbm_dibatalkan_row = $db->get_row("
                    SELECT
                        SUM(c.jumlah) jumlah
                    FROM
                        smis_fr_obat_f_masuk a 
                            INNER JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
                            INNER JOIN smis_fr_stok_obat c ON b.id = c.id_dobat_masuk
                    WHERE
                        a.prop NOT LIKE '' 
                            AND MONTH(a.time_updated) = '" . $bulan . "' 
                            AND YEAR(a.time_updated) = '" . $tahun . "' 
                            AND b.prop = ''
                            AND b.id_obat = '" . $id_obat . "'
                            AND c.jumlah > 0
                ");
                if ($bbm_dibatalkan_row != null)
                    $jumlah_bbm_dibatalkan = $bbm_dibatalkan_row->jumlah;
                $jumlah_penerimaan -= $jumlah_bbm_dibatalkan;

                $jumlah_restok_retur_obat_gudang = 0;
                $restok_retur_obat_gudang_row = $db->get_row("
                    SELECT 
                        SUM(jumlah) jumlah
                    FROM
                        smis_fr_retur_obat_unit
                    WHERE
                        prop = '' 
                            AND status = 'sudah' 
                            AND restok = 1 
                            AND id_obat = '" . $id_obat . "' 
                            AND MONTH(tanggal) = '" . $bulan . "'
                            AND YEAR(tanggal) = '" . $tahun . "'
                ");
                if ($restok_retur_obat_gudang_row != null)
                    $jumlah_restok_retur_obat_gudang = $restok_retur_obat_gudang_row->jumlah;
                $jumlah_penerimaan += $jumlah_restok_retur_obat_gudang;

                foreach ($depo_farmasi_arr as $depo_farmasi => $prefix) {
                    $mutasi_antar_unit_masuk = 0;
                    $mutasi_antar_unit_masuk_row = $db->get_row("
                        SELECT
                            SUM(a.jumlah_masuk) jumlah
                        FROM 
                            smis_" . $prefix . "_riwayat_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_stok_obat b ON a.id_stok_obat = b.id
                        WHERE
                            a.prop = '' 
                                AND b.id_obat = '" . $id_obat . "'
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND a.keterangan LIKE 'Mutasi Antar Unit Masuk dari %'
                                AND a.keterangan NOT LIKE 'Mutasi Antar Unit Masuk dari DEPO%'
                    ");
                    if ($mutasi_antar_unit_masuk_row != null)
                        $mutasi_antar_unit_masuk = $mutasi_antar_unit_masuk_row->jumlah;
                    $jumlah_penerimaan += $mutasi_antar_unit_masuk;

                    $pembatalan_penjualan = 0;
                    $pembatalan_penjualan_row = $db->get_row("
                        SELECT
                            SUM(a.jumlah_masuk) jumlah
                        FROM 
                            smis_" . $prefix . "_riwayat_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_stok_obat b ON a.id_stok_obat = b.id
                        WHERE
                            a.prop = '' 
                                AND b.id_obat = '" . $id_obat . "'
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND a.keterangan LIKE 'Stok Batal Digunakan Penjualan%'
                    ");
                    if ($pembatalan_penjualan_row != null)
                        $pembatalan_penjualan = $pembatalan_penjualan_row->jumlah;
                    $jumlah_penerimaan += $pembatalan_penjualan;

                    $retur_penjualan = 0;
                    $retur_penjualan_row = $db->get_row("
                        SELECT
                            SUM(a.jumlah_keluar) jumlah
                        FROM 
                            smis_" . $prefix . "_riwayat_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_stok_obat b ON a.id_stok_obat = b.id
                        WHERE
                            a.prop = '' 
                                AND b.id_obat = '" . $id_obat . "'
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND a.keterangan LIKE 'Stok Retur Penjualan%'
                    ");
                    if ($retur_penjualan_row != null)
                        $retur_penjualan = $retur_penjualan_row->jumlah;
                    $jumlah_penerimaan += $retur_penjualan;

                    $batal_retur_penjualan = 0;
                    $batal_retur_penjualan_row = $db->get_row("
                        SELECT
                            SUM(a.jumlah_keluar) jumlah
                        FROM 
                            smis_" . $prefix . "_riwayat_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_stok_obat b ON a.id_stok_obat = b.id
                        WHERE
                            a.prop = '' 
                                AND b.id_obat = '" . $id_obat . "'
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND a.keterangan LIKE 'Pembatalan Retur Penjualan%'
                    ");
                    if ($batal_retur_penjualan_row != null)
                        $batal_retur_penjualan = $batal_retur_penjualan_row->jumlah;
                    $jumlah_penerimaan -= $batal_retur_penjualan;
                }
                //. End of Penerimaan

                /// Penggunaan Rawat Jalan
                foreach ($depo_farmasi_arr as $depo_farmasi => $prefix) {
                    /// Cluster : Penjualan KIUP Pasien Rawat Jalan
                    $penjualan_obat_jadi_rj = 0;
                    $penjualan_obat_jadi_rj_row = $db->get_row("
                        SELECT
                            SUM(b.jumlah) jumlah
                        FROM
                            smis_" . $prefix . "_penjualan_resep a
                                INNER JOIN smis_" . $prefix . "_penjualan_obat_jadi b ON a.id = b.id_penjualan_resep
                        WHERE
                            a.prop = '' 
                                AND a.dibatalkan = 0
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND b.id_obat = '" . $id_obat . "'
                                AND a.uri = '0'
                                AND a.tipe = 'resep'
                    ");
                    if ($penjualan_obat_jadi_rj_row != null)
                        $jumlah_obat_jadi_rj = $penjualan_obat_jadi_rj_row->jumlah;
                    $jumlah_penggunaan_rawat_jalan += $jumlah_obat_jadi_rj;

                    $penjualan_obat_racikan_rj = 0;
                    $penjualan_obat_racikan_rj_row = $db->get_row("
                        SELECT
                            SUM(c.jumlah) jumlah
                        FROM
                            smis_" . $prefix . "_penjualan_resep a
                                INNER JOIN smis_" . $prefix . "_penjualan_obat_racikan b ON a.id = b.id_penjualan_resep
                                INNER JOIN smis_" . $prefix . "_bahan_pakai_obat_racikan c ON b.id = c.id_penjualan_obat_racikan
                        WHERE
                            a.prop = '' 
                                AND a.dibatalkan = 0
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND c.id_obat = '" . $id_obat . "'
                                AND a.uri = '0'
                                AND a.tipe = 'resep'
                    ");
                    if ($penjualan_obat_racikan_rj_row != null)
                        $jumlah_obat_racikan_rj = $penjualan_obat_racikan_rj_row->jumlah;
                    $jumlah_penggunaan_rawat_jalan += $jumlah_obat_racikan_rj;
                    //. End of Cluster : Penjualan KIUP Pasien Rawat Jalan

                    $riwayat_mutasi_rj = 0;
                    $riwayat_mutasi_rj_row = $db->get_row("
                        SELECT
                            SUM(a.jumlah_keluar) jumlah
                        FROM 
                            smis_" . $prefix . "_riwayat_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_stok_obat b ON a.id_stok_obat = b.id
                        WHERE
                            b.id_obat = '" . $id_obat . "' 
                                AND a.prop = '' 
                                AND MONTH(a.tanggal) = '" . $bulan . "' 
                                AND YEAR(a.tanggal) = '" . $tahun . "' 
                                AND (
                                    a.keterangan LIKE 'Mutasi Unit ke IGD%'
                                        OR a.keterangan LIKE 'Mutasi Unit ke POLI%'
                                )
                    ");
                    if ($riwayat_mutasi_rj_row != null)
                        $riwayat_mutasi_rj = $riwayat_mutasi_rj_row->jumlah;
                    $jumlah_penggunaan_rawat_jalan += $riwayat_mutasi_rj;
                }
                //. End of Penggunaan Rawat Jalan

                /// Penggunaan Rawat Inap
                foreach ($depo_farmasi_arr as $depo_farmasi => $prefix) {
                    /// Cluster : Penjualan KIUP Pasien Rawat Inap/Penjualan Non-KIUP/Penjualan Bebas/Penjualan UP
                    $penjualan_obat_jadi_ri = 0;
                    $penjualan_obat_jadi_ri_row = $db->get_row("
                        SELECT
                            SUM(b.jumlah) jumlah
                        FROM
                            smis_" . $prefix . "_penjualan_resep a
                                INNER JOIN smis_" . $prefix . "_penjualan_obat_jadi b ON a.id = b.id_penjualan_resep
                        WHERE
                            a.prop = '' 
                                AND a.dibatalkan = 0
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND b.id_obat = '" . $id_obat . "'
                                AND (
                                    (
                                        a.uri = '1'
                                            AND a.tipe = 'resep'
                                    ) OR a.tipe = 'resep_luar'
                                        OR a.tipe = 'bebas'
                                        OR a.tipe = 'instansi_lain'
                                )
                    ");
                    if ($penjualan_obat_jadi_ri_row != null)
                        $jumlah_obat_jadi_ri = $penjualan_obat_jadi_ri_row->jumlah;
                    $jumlah_penggunaan_rawat_inap += $jumlah_obat_jadi_ri;

                    $penjualan_obat_racikan_ri = 0;
                    $penjualan_obat_racikan_ri_row = $db->get_row("
                        SELECT
                            SUM(c.jumlah) jumlah
                        FROM
                            smis_" . $prefix . "_penjualan_resep a
                                INNER JOIN smis_" . $prefix . "_penjualan_obat_racikan b ON a.id = b.id_penjualan_resep
                                INNER JOIN smis_" . $prefix . "_bahan_pakai_obat_racikan c ON b.id = c.id_penjualan_obat_racikan
                        WHERE
                            a.prop = '' 
                                AND a.dibatalkan = 0
                                AND MONTH(a.tanggal) = '" . $bulan . "'
                                AND YEAR(a.tanggal) = '" . $tahun . "'
                                AND c.id_obat = '" . $id_obat . "'
                                AND (
                                    (
                                        a.uri = '1'
                                            AND a.tipe = 'resep'
                                    ) OR a.tipe = 'resep_luar'
                                )
                    ");
                    if ($penjualan_obat_racikan_ri_row != null)
                        $jumlah_obat_racikan_ri = $penjualan_obat_racikan_ri_row->jumlah;
                    $jumlah_penggunaan_rawat_inap += $jumlah_obat_racikan_ri;
                    //. End of Cluster : Penjualan KIUP Pasien Rawat Inap/Penjualan Non-KIUP/Penjualan Bebas/Penjualan UP

                    $riwayat_mutasi_ri = 0;
                    $riwayat_mutasi_ri_row = $db->get_row("
                        SELECT
                            SUM(a.jumlah_keluar) jumlah
                        FROM 
                            smis_" . $prefix . "_riwayat_stok_obat a 
                                INNER JOIN smis_" . $prefix . "_stok_obat b ON a.id_stok_obat = b.id
                        WHERE
                            b.id_obat = '" . $id_obat . "' 
                                AND a.prop = '' 
                                AND MONTH(a.tanggal) = '" . $bulan . "' 
                                AND YEAR(a.tanggal) = '" . $tahun . "' 
                                AND a.keterangan NOT LIKE 'Mutasi Unit ke IGD%'
                                AND a.keterangan NOT LIKE 'Mutasi Unit ke POLI%'
                                AND a.keterangan NOT LIKE 'Mutasi Unit ke DEPO%'
                                AND a.keterangan LIKE 'Mutasi Unit ke %'
                    ");
                    if ($riwayat_mutasi_ri_row != null)
                        $riwayat_mutasi_ri = $riwayat_mutasi_ri_row->jumlah;
                    $jumlah_penggunaan_rawat_inap += $riwayat_mutasi_ri;
                }
                //. End of Penggunaan Rawat Inap

                /// Jumlah Penggunaan Total
                $jumlah_penggunaan_total = $jumlah_penggunaan_rawat_jalan + $jumlah_penggunaan_rawat_inap;
                //. End of Jumlah Penggunaan Total

                /// Harga per Item
                $bbm_terakhir_row = $db->get_row("
                    SELECT
                        MAX(a.id) id
                    FROM
                        smis_fr_obat_f_masuk a 
                            INNER JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
                    WHERE
                        b.id_obat = '" . $id_obat . "'
                            AND a.tanggal <= '" . date("Y-m-t", strtotime($periode . "-01")) . "'
                            AND a.prop = ''
                ");
                if ($bbm_terakhir_row != null) {
                    $harga_terakhir_row = $db->get_row("
                        SELECT
                            c.hna harga
                        FROM
                            smis_fr_obat_f_masuk a
                                INNER JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
                                INNER JOIN smis_fr_stok_obat c ON b.id = c.id_dobat_masuk
                        WHERE
                            b.prop = ''
                                AND a.id = '" . $bbm_terakhir_row->id . "'
                                AND b.id_obat = '" . $id_obat . "'
                        ORDER BY
                            b.id DESC
                        LIMIT
                            0, 1
                    ");
                    if ($harga_terakhir_row != null)
                        $harga_per_item = $harga_terakhir_row->harga;
                }
                //. End of Harga per Item

                /// Total Harga
                $total_harga = $jumlah_penggunaan_total * $harga_per_item;
                //. End of Total Harga

                $f_harga_per_item = "0,00";
                if ($harga_per_item > 0)
                    $f_harga_per_item = ArrayAdapter::format("only-money", $harga_per_item);
                $f_total_harga = "0,00";
                if ($total_harga > 0)
                    $f_total_harga = ArrayAdapter::format("only-money", $total_harga);

                $html = "
                    <tr>
                        <td id='nomor'></td>
                        <td id='nama_generik'>" . $nama_generik . "</td>
                        <td id='nama_bermerk'>" . $nama_bermerk . "</td>
                        <td id='bentuk_sediaan'>" . $bentuk_sediaan . "</td>
                        <td id='kekuatan'></td>
                        <td id='stok_awal'><div align='right'>" . $stok_awal . "</div></td>
                        <td id='jumlah_penerimaan'><div align='right'>" . $jumlah_penerimaan . "</div></td>
                        <td id='jumlah_penggunaan_rawat_jalan'><div align='right'>" . $jumlah_penggunaan_rawat_jalan . "</div></td>
                        <td id='jumlah_penggunaan_rawat_inap'><div align='right'>" . $jumlah_penggunaan_rawat_inap . "</div></td>
                        <td id='jumlah_penggunaan_total'><div align='right'>" . $jumlah_penggunaan_total . "</div></td>
                        <td id='harga_per_item' style='display: none;'>" . $harga_per_item . "</td>
                        <td id='f_harga_per_item'><div align='right'>" . $f_harga_per_item . "</div></td>
                        <td id='total_harga' style='display: none;'>" . $total_harga . "</td>
                        <td id='f_total_harga'><div align='right'>" . $f_total_harga . "</div></td>
                    </tr>
                ";
            }

			$data = array();
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['nama_obat'] = $nama_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
            $periode = $_POST['periode'];

            $nama_rs = $_POST['nama_rs'];
            $kelas_rs = $_POST['kelas_rs'];
            $alamat_rs = $_POST['alamat_rs'];
            $kabupaten_kota = $_POST['kabupaten_kota'];
            $propinsi = $_POST['propinsi'];
            $nama_ka_farmasi = $_POST['nama_ka_farmasi'];
            $jumlah_apoteker = $_POST['jumlah_apoteker'];
            $no_telpon_rs = $_POST['no_telpon_rs'];
            $bulan = ArrayAdapter::format("date M", $periode . "-01");
            $tahun = ArrayAdapter::format("date Y", $periode . "-01");

            require_once("smis-libs-out/php-excel/PHPExcel.php");

            $file = new PHPExcel();
            $file->getProperties()->setCreator(getSettings($db, "smis_autonomous_abbrevation", "Rumah Sakit"));
            $file->getProperties()->setTitle("Laporan Pelayanan Kefarmasian");
            $file->getProperties()->setSubject("Pelayanan Kefarmasian/" . getSettings($db, "smis_autonomous_abbrevation", "Rumah Sakit"));
            $file->getProperties()->setDescription("Laporan Pelayanan Kefarmasian Generated by System");
            $file->getProperties()->setKeywords("Laporan Pelayanan Kefarmasian");
            $file->getProperties()->setCategory("Laporan Pelayanan Kefarmasian");

            $sheet = $file->getActiveSheet();
            $sheet->setTitle("Laporan Pelayanan Kefarmasian");
            $i = 1;

            $sheet->mergeCells("A" . $i . ":N" . $i)->setCellValue("A" . $i, "LAPORAN PELAYANAN KEFARMASIAN DI RUMAH SAKIT");
            $sheet->getStyle("A" . $i . ":N" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":N" . $i)->setCellValue("A" . $i, "DIREKTORAT PELAYANAN KEFARMASIAN");
            $sheet->getStyle("A" . $i . ":N" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":N" . $i)->setCellValue("A" . $i, "KEMENTRIAN KESEHATAN REPUBLIK INDONESIA");
            $sheet->getStyle("A" . $i . ":N" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 3;

            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Nama Rumah Sakit");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $nama_rs);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Tipe Rumah Sakit");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $kelas_rs);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Alamat Rumah Sakit");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $alamat_rs);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Kabupaten / Kota");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $kabupaten_kota);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Provinsi");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $propinsi);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Nama Ka. Instalasi Farmasi");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $nama_ka_farmasi);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Jumlah Apoteker");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $jumlah_apoteker);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "No. Telpon / HP");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $no_telpon_rs);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Bulan");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $bulan);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("A" . $i . ":F" . $i)->setCellValue("A" . $i, "Tahun");
            $sheet->getStyle("A" . $i . ":F" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $sheet->mergeCells("G" . $i . ":J" . $i)->setCellValue("G" . $i, ": " . $tahun);
            $sheet->getStyle("G" . $i . ":J" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("G" . $i)->getFont()->setBold(true);
            $i = $i + 2;
            $sheet->mergeCells("A" . $i . ":D" . $i)->setCellValue("A" . $i, "I. Manajemen Farmasi");
            $sheet->getStyle("A" . $i . ":D" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle("A" . $i)->getFont()->setBold(true);
            $i = $i + 1;
            $sheet->mergeCells("B" . $i . ":D" . $i)->setCellValue("B" . $i, "A. Penggunaan Obat");
            $sheet->getStyle("B" . $i . ":D" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $i = $i + 1;

            $border_start = $i;

            $sheet->mergeCells("C" . $i . ":C" . ($i + 1))->setCellValue("C" . $i, "No");
            $sheet->mergeCells("D" . $i . ":E" . $i)->setCellValue("D" . $i, "Nama Obat");
            $sheet->setCellValue("D" . ($i + 1), "Generik");
            $sheet->setCellValue("E" . ($i + 1), "Bermerk");
            $sheet->mergeCells("F" . $i . ":F" . ($i + 1))->setCellValue("F" . $i, "Bentuk Sediaan");
            $sheet->mergeCells("G" . $i . ":G" . ($i + 1))->setCellValue("G" . $i, "Kekuatan");
            $sheet->mergeCells("H" . $i . ":H" . ($i + 1))->setCellValue("H" . $i, "Stok Awal");
            $sheet->mergeCells("I" . $i . ":I" . ($i + 1))->setCellValue("I" . $i, "Jumlah Penerimaan");
            $sheet->mergeCells("J" . $i . ":L" . $i)->setCellValue("J" . $i, "Jumlah Penggunaan");
            $sheet->setCellValue("J" . ($i + 1), "Rawat Jalan");
            $sheet->setCellValue("K" . ($i + 1), "Rawat Inap");
            $sheet->setCellValue("L" . ($i + 1), "Total");
            $sheet->mergeCells("M" . $i . ":M" . ($i + 1))->setCellValue("M" . $i, "Harga per Item");
            $sheet->mergeCells("N" . $i . ":N" . ($i + 1))->setCellValue("N" . $i, "Total Harga");
            $sheet->getStyle("C" . $i . ":N" . ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle("C" . $i . ":N" . ($i + 1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle("C" . $i . ":N" . ($i + 1))->getFont()->setBold(true);
            $sheet->getStyle("C" . $i . ":N" . ($i + 1))->getAlignment()->setWrapText(true);
            $i = $i + 2;
            $scol = "C";
            for ($nomor = 1; $nomor <= 12; $nomor++) {
                $sheet->setCellValue($scol . $i, "(" . $nomor . ")");
                $sheet->getStyle($scol . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle($scol . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('808080');
                $scol++;
            }
            $i = $i + 1;

            $data = json_decode($_POST['d_data']);
            if (count($data) > 0) {
                foreach ($data as $d) {
                    $sheet->setCellValue("C" . $i, $d->nomor);
                    $sheet->setCellValue("D" . $i, $d->nama_generik);
                    $sheet->setCellValue("E" . $i, $d->nama_bermerk);
                    $sheet->setCellValue("F" . $i, $d->bentuk_sediaan);
                    $sheet->setCellValue("G" . $i, $d->kekuatan);
                    $sheet->setCellValue("H" . $i, $d->stok_awal);
                    $sheet->setCellValue("I" . $i, $d->jumlah_penerimaan);
                    $sheet->setCellValue("J" . $i, $d->jumlah_penggunaan_rawat_jalan);
                    $sheet->setCellValue("K" . $i, $d->jumlah_penggunaan_rawat_inap);
                    $sheet->setCellValue("L" . $i, $d->jumlah_penggunaan_total);
                    $sheet->setCellValue("M" . $i, $d->harga_per_item);
                    $sheet->setCellValue("N" . $i, $d->total_harga);
                    $sheet->getStyle("H" . $i)->getNumberFormat()->setFormatCode("#,##0");
                    $sheet->getStyle("I" . $i)->getNumberFormat()->setFormatCode("#,##0");
                    $sheet->getStyle("J" . $i)->getNumberFormat()->setFormatCode("#,##0");
                    $sheet->getStyle("K" . $i)->getNumberFormat()->setFormatCode("#,##0");
                    $sheet->getStyle("L" . $i)->getNumberFormat()->setFormatCode("#,##0");
                    $sheet->getStyle("M" . $i)->getNumberFormat()->setFormatCode("#,##0");
                    $sheet->getStyle("N" . $i)->getNumberFormat()->setFormatCode("#,##0");
                    $i = $i + 1;
                }
            }

            $thin = array();
            $thin['borders'] = array();
            $thin['borders']['allborders'] = array();
            $thin['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
            $sheet->getStyle("C" . $border_start . ":N" . ($i - 1))->applyFromArray($thin);

            $sheet->getColumnDimension('A')->setWidth(3);
            $sheet->getColumnDimension('B')->setWidth(3);
            $sheet->getColumnDimension('C')->setWidth(5);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(20);
            $sheet->getColumnDimension('F')->setWidth(16);
            $sheet->getColumnDimension('G')->setWidth(16);
            $sheet->getColumnDimension('H')->setWidth(8);
            $sheet->getColumnDimension('I')->setWidth(10);
            $sheet->getColumnDimension('J')->setWidth(12);
            $sheet->getColumnDimension('K')->setWidth(12);
            $sheet->getColumnDimension('L')->setWidth(12);
            $sheet->getColumnDimension('M')->setWidth(12);
            $sheet->getColumnDimension('N')->setWidth(12);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Laporan Pelayanan Kefarmasian - Periode ' . $bulan . ' ' . $tahun . '.xls"');
            header('Cache-Control: max-age=0');
            $writer = PHPExcel_IOFactory::createWriter($file, 'Excel5');
            $writer->save('php://output');
		}
        return;
    }

    $table = new Table(
        array("No.", "Nama Obat Generik", "Nama Obat Bermerk", "Bentuk Sediaan", "Kekuatan", "Stok Awal", "Jumlah Penerimaan", "Jml. Penggunaan Rawat Jalan", "Jml. Penggunaan Rawat Inap", "Jml. Penggunaan Total", "Harga per Item", "Total Harga"),
        "",
        null,
        true
    );
    $table->setName("lpk");
    $table->setAction(false);
    $table->setFooterVisible(false);
    $table->setHeaderVisible(false);
    $table->addHeader("before", "
		<tr>
			<th rowspan='2'>No.</th>
			<th colspan='2'>Nama Obat</th>
			<th rowspan='2'>Bentuk Sediaan</th>
            <th rowspan='2'>Kekuatan Obat</th>
            <th rowspan='2'>Stok Awal</th>
            <th rowspan='2'>Jumlah Penerimaan</th>
            <th colspan='3'>Jumlah Penggunaan</th>
            <th rowspan='2'>Harga per Item</th>
            <th rowspan='2'>Total Harga</th>
		</tr>
		<tr>
			<th>Generik</th>
            <th>Bermerk</th>
            <th>Rawat Jalan</th>
            <th>Rawat Inap</th>
            <th>Total</th>
		</tr>
		<tr>
			<th><small>(1)</small></th>
			<th><small>(2)</small></th>
			<th><small>(3)</small></th>
			<th><small>(4)</small></th>
			<th><small>(5)</small></th>
			<th><small>(6)</small></th>
			<th><small>(7)</small></th>
			<th><small>(8)</small></th>
			<th><small>(9)</small></th>
			<th><small>(10)</small></th>
			<th><small>(11)</small></th>
			<th><small>(12)</small></th>
		</tr>
	");

    $nama_rs = "";
    $kelas_rs = "";
    $alamat_rs = "";
    $kabupaten_kota = "";
    $propinsi = "Jawa Timur";
    $nama_ka_farmasi = getSettings($db, "gudang_farmasi-nama_ka_farmasi", "");
    $jumlah_apoteker = getSettings($db, "gudang_farmasi-jumlah_apoteker", "0");
    $no_telpon_rs = "";
    $info_rs_row = $db->get_row("
        SELECT 
            *
        FROM 
            smis_mr_data_dasar_rs
        WHERE 
            prop = ''
        LIMIT 
            0, 1
    ");
    if ($info_rs_row != null) {
        $nama_rs = $info_rs_row->nama_rs;
        $kelas_rs = $info_rs_row->kelas_rs;
        $alamat_rs = $info_rs_row->alamat_rs;
        $kabupaten_kota = $info_rs_row->kota_kabupaten;
        $no_telpon_rs = $info_rs_row->no_telpon;
    }

    $form = new Form("lpk_form", "lpk_form", "Laporan Pelayanan Kefarmasian");
    $nama_rs_text = new Text("lpk_nama_rs", "lpk_nama_rs", $nama_rs);
    $nama_rs_text->setAtribute("disabled='disabled'");
    $form->addElement("Nama Rumah Sakit", $nama_rs_text);
    $kelas_rs_text = new Text("lpk_kelas_rs", "lpk_kelas_rs", $kelas_rs);
    $kelas_rs_text->setAtribute("disabled='disabled'");
    $form->addElement("Tipe Rumah Sakit", $kelas_rs_text);
    $alamat_rs_text = new Text("lpk_alamat_rs", "lpk_alamat_rs", $alamat_rs);
    $alamat_rs_text->setAtribute("disabled='disabled'");
    $form->addElement("Alamat Rumah Sakit", $alamat_rs_text);
    $kabupaten_kota_text = new Text("lpk_kabupaten_kota", "lpk_kabupaten_kota", $kabupaten_kota);
    $kabupaten_kota_text->setAtribute("disabled='disabled'");
    $form->addElement("Kabupaten / Kota", $kabupaten_kota_text);
    $propinsi_text = new Text("lpk_propinsi", "lpk_propinsi", $propinsi);
    $propinsi_text->setAtribute("disabled='disabled'");
    $form->addElement("Provinsi", $propinsi_text);
    $nama_ka_farmasi_text = new Text("lpk_nama_ka_farmasi", "lpk_nama_ka_farmasi", $nama_ka_farmasi);
    $nama_ka_farmasi_text->setAtribute("disabled='disabled'");
    $form->addElement("Nama Ka. Instalasi Farmasi", $nama_ka_farmasi_text);
    $jumlah_apoteker_text = new Text("lpk_jumlah_apoteker", "lpk_jumlah_apoteker", $jumlah_apoteker);
    $jumlah_apoteker_text->setAtribute("disabled='disabled'");
    $form->addElement("Jumlah Apoteker", $jumlah_apoteker_text);
    $no_telpon_rs_text = new Text("lpk_no_telpon_rs", "lpk_no_telpon_rs", $no_telpon_rs);
    $no_telpon_rs_text->setAtribute("disabled='disabled'");
    $form->addElement("No. Telpon / HP", $no_telpon_rs_text);
    $periode_text = new Text("lpk_periode", "lpk_periode", date("Y-m"));
    $periode_text->setClass("myperiod");
    $periode_text->setAtribute("data-date-format='yyyy-mm'");
    $form->addElement("Periode", $periode_text);
    $show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lpk.view()");
	$download_button = new Button("", "", "Unduh");
	$download_button->setClass("btn-inverse");
	$download_button->setIcon("fa fa-download");
	$download_button->setIsButton(Button::$ICONIC);
	$download_button->setAtribute("id='lpk_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($download_button);
	$form->addElement("", $btn_group);

    $loading_bar = new LoadingBar("lpk_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lpo.cancel()");
	$loading_modal = new Modal("lpk_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

    echo $loading_modal->getHtml();
    echo $form->getHtml();
    echo $table->getHtml();
    echo "<div id='lpk_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
    var lpk;
    var FINISHED;
    $(document).ready(function() {
        $(".myperiod").datepicker({
            format: "yyyy-mm",
            viewMode: "months", 
            minViewMode: "months"
        });
        lpk = new TableAction(
            "lpk",
            "gudang_farmasi",
            "laporan_pelayanan_farmasi",
            new Array()
        );
        lpk.view = function() {
            if ($("#lpk_periode").val() == "")
			    return;
            var self = this;
            var data = this.getRegulerData();
            data['command'] = "verify_period";
            data['periode'] = $("#lpk_periode").val();
            showLoading();
            $.post(
                "",
                data,
                function(response_vp) {
                    var json_vp = JSON.parse(response_vp);
                    if (json_vp == null) {
                        dismissLoading();
                        return;
                    }
                    dismissLoading();
                    if (json_vp == 0) {
                        bootbox.alert("Tidak dapat menyajikan data pada periode berjalan dan periode setelahnya.");
                        return;
                    }
                    data['command'] = "verify_transactions";
                    showLoading();
                    $.post(
                        "",
                        data,
                        function(response_vt) {
                            var json_vt = JSON.parse(response_vt);
                            if (json_vt == null) {
                                dismissLoading();
                                return;
                            }
                            dismissLoading();
                            if (json_vt.valid == 0) {
                                bootbox.alert(json_vt.message);
                                return;
                            }
                            $("#lpk_info").empty();
                            $("#lpk_loading_bar").sload("true", "Harap ditunggu...", 0);
                            $("#lpk_loading_modal").smodal("show");
                            FINISHED = false;
                            var data = self.getRegulerData();
                            data['command'] = "get_jumlah_obat";
                            $.post(
                                "",
                                data,
                                function(response) {
                                    var json = JSON.parse(response);
                                    if (json == null) return;
                                    $("#lpk_list").empty();
                                    self.fillHtml(0, json.jumlah);
                                }
                            );
                        }
                    );      
                }
            );
        };
        lpk.fillHtml = function(num, limit) {
            if (FINISHED || num == limit) {
                if (FINISHED == false && num == limit) {
                    this.finalize();
                } else {
                    $("#lpk_loading_modal").smodal("hide");
                    $("#lpk_info").html(
                        "<div class='alert alert-block alert-inverse'>" +
                            "<center><strong>PROSES DIBATALKAN</strong></center>" +
                        "</div>"
                    );
                    $("#lpk_export_button").removeAttr("onclick");
                }
                return;
            }
            var self = this;
            var data = this.getRegulerData();
            data['command'] = "get_info_obat";
            data['periode'] = $("#lpk_periode").val();
            data['num'] = num;
            $.post(
                "",
                data,
                function(response) {
                    var json = JSON.parse(response);
                    if (json == null) return;
                    $("tbody#lpk_list").append(
                        json.html
                    );
                    $("#lpk_loading_bar").sload("true", "Loading Data (" + (num + 1) + " / " + limit + ") : " + json.id_obat + " - " + json.nama_obat, (num + 1) * 100 / limit - 1);
                    self.fillHtml(num + 1, limit);
                }
            );
        };
        lpk.finalize = function() {
            var num_rows = $("tbody#lpk_list tr").length;
            for (var i = 0; i < num_rows; i++)
                $("tbody#lpk_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
            $("#lpk_loading_modal").smodal("hide");
            $("#lpk_info").html(
                "<div class='alert alert-block alert-info'>" +
                    "<center><strong>PROSES SELESAI</strong></center>" +
                "</div>"
            );
            $("#lpk_export_button").removeAttr("onclick");
            $("#lpk_export_button").attr("onclick", "lpk.export_xls()");
        };
        lpk.cancel = function() {
            FINISHED = true;
        };
        lpk.export_xls = function() {
            showLoading();
            var num_rows = $("#lpk_list").children("tr").length;
            var d_data = {};
            for (var i = 0; i < num_rows; i++) {
                var nomor = $("tbody#lpk_list tr:eq(" + i + ") td#nomor").text();
                var nama_generik = $("tbody#lpk_list tr:eq(" + i + ") td#nama_generik").text();
                var nama_bermerk = $("tbody#lpk_list tr:eq(" + i + ") td#nama_bermerk").text();
                var bentuk_sediaan = $("tbody#lpk_list tr:eq(" + i + ") td#bentuk_sediaan").text();
                var kekuatan = $("tbody#lpk_list tr:eq(" + i + ") td#kekuatan").text();
                var stok_awal = $("tbody#lpk_list tr:eq(" + i + ") td#stok_awal").text();
                var jumlah_penerimaan = $("tbody#lpk_list tr:eq(" + i + ") td#jumlah_penerimaan").text();
                var jumlah_penggunaan_rawat_jalan = $("tbody#lpk_list tr:eq(" + i + ") td#jumlah_penggunaan_rawat_jalan").text();
                var jumlah_penggunaan_rawat_inap = $("tbody#lpk_list tr:eq(" + i + ") td#jumlah_penggunaan_rawat_inap").text();
                var jumlah_penggunaan_total = $("tbody#lpk_list tr:eq(" + i + ") td#jumlah_penggunaan_total").text();
                var harga_per_item = $("tbody#lpk_list tr:eq(" + i + ") td#harga_per_item").text();
                var total_harga = $("tbody#lpk_list tr:eq(" + i + ") td#total_harga").text();
                d_data[i] = {
                    "nomor" 			            : nomor,
                    "nama_generik" 			        : nama_generik,
                    "nama_bermerk" 			        : nama_bermerk,
                    "bentuk_sediaan" 			    : bentuk_sediaan,
                    "kekuatan" 			            : kekuatan,
                    "stok_awal" 			        : stok_awal,
                    "jumlah_penerimaan" 			: jumlah_penerimaan,
                    "jumlah_penggunaan_rawat_jalan" : jumlah_penggunaan_rawat_jalan,
                    "jumlah_penggunaan_rawat_inap" 	: jumlah_penggunaan_rawat_inap,
                    "jumlah_penggunaan_total" 		: jumlah_penggunaan_total,
                    "harga_per_item" 			    : harga_per_item,
                    "total_harga"                   : total_harga
                };
            }
            var data = this.getRegulerData();
            data['command'] = "export_xls";
            data['nama_rs'] = $("#lpk_nama_rs").val();
            data['kelas_rs'] = $("#lpk_kelas_rs").val();
            data['alamat_rs'] = $("#lpk_alamat_rs").val();
            data['kabupaten_kota'] = $("#lpk_kabupaten_kota").val();
            data['propinsi'] = $("#lpk_propinsi").val();
            data['nama_ka_farmasi'] = $("#lpk_nama_ka_farmasi").val();
            data['jumlah_apoteker'] = $("#lpk_jumlah_apoteker").val();
            data['no_telpon_rs'] = $("#lpk_no_telpon_rs").val();
            data['periode'] = $("#lpk_periode").val();
            data['d_data'] = JSON.stringify(d_data);
            data['num_rows'] = num_rows;
            postForm(data);
            dismissLoading();
        };
        $("tbody#lpk_list").append(
			"<tr>" +
				"<td colspan='12'><strong><center><small>DATA BELUM DIPROSES</small></center></strong></td>" +
			"</tr>"
		);
        $("#lpk_loading_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#lpk_loading_modal").on("hide", function() {
			$("a.close").show();
		});
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
        });
    });
</script>
<style type="text/css">
    #lpk_form label {
        width: 200px !important;
    }
    #table_lpk thead tr th  {
        text-align: center !important;
        vertical-align: middle !important;
        background-color: #f1f1f1;
    }
</style>