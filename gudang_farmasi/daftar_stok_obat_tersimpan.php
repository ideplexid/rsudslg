<?php 
	$stok_obat_table = new Table(
		array("Nomor", "Kode Obat", "Nama Obat", "Jenis Obat", "Produsen", "Vendor", "Jenis Stok", "Sisa", "Harga Satuan", "Ket. Jumlah", "Tgl. Exp.", "No. Batch"),
		"",
		null,
		true
	);
	$stok_obat_table->setName("stok_obat_t");
	$stok_obat_table->setReloadButtonEnable(false);
	$stok_obat_table->setPrintButtonEnable(false);
	$stok_obat_table->setAddButtonEnable(false);
	$stok_obat_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class StokObatAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Kode Obat'] = "309." . $row->kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Jenis Stok'] = self::format("unslug", $row->label);
				$array['Sisa'] = $row->sisa . " " . $row->satuan;
				$array['Harga Satuan'] = self::format("money Rp.", $row->hna);
				if ($row->konversi > 1)
					$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				else
					$array['Ket. Jumlah'] = "-";
				if ($row->tanggal_exp == "0000-00-00")
					$array['Tgl. Exp.'] = "-";
				else
					$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				if ($row->no_batch == null)
					$array['No. Batch'] = "-";
				else
					$array['No. Batch'] = $row->no_batch;
				return $array;
			}
		}
		$stok_obat_adapter = new StokObatAdapter();
		$columns = array("id", "id_dobat_masuk", "nama_obat", "nama_jenis_obat", "label", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "id_vendor", "nama_vendor", "produsen");
		$stok_obat_dbtable = new DBTable(
			$db,
			"smis_fr_stok_obat",
			$columns
		);
		$stok_obat_dbtable->addCustomKriteria(" sisa ", " > 0 ");
		$stok_obat_dbresponder = new DBResponder(
			$stok_obat_dbtable,
			$stok_obat_table,
			$stok_obat_adapter
		);
		$data = $stok_obat_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $stok_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">	
	var stok_obat_t;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#stok_obat_t_max").val(50);
		var stok_obat_t_columns = new Array("id", "id_dobat_masuk", "nama_obat", "nama_jenis_obat", "label", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "id_vendor", "nama_vendor", "produsen");
		stok_obat_t = new TableAction(
			"stok_obat_t",
			"gudang_farmasi",
			"daftar_stok_obat_tersimpan",
			stok_obat_t_columns
		);
		stok_obat_t.view();
	});
</script>