<?php 
	global $db;
	
	$laporan_form = new Form("lmhpo_form", "", "Gudang Farmasi : Laporan Monitoring Harga Per Obat");
	$id_obat_hidden = new Hidden("lmhpo_id_obat", "lmhpo_id_obat", "");
	$laporan_form->addElement("", $id_obat_hidden);
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$obat_button->setIcon("icon-white icon-list-alt");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setAtribute("id='obat_browse'");
	$obat_text = new Text("lmhpo_nama_obat", "lmhpo_nama_obat", "");
	$obat_text->setClass("smis-one-option-input");
	$obat_text->setAtribute("disabled='disabled'");
	$obat_input_group = new InputGroup("");
	$obat_input_group->addComponent($obat_text);
	$obat_input_group->addComponent($obat_button);
	$laporan_form->addElement("Nama Obat", $obat_input_group);
	$jenis_obat_text = new Text("lmhpo_jenis_obat", "lmhpo_jenis_obat", "");
	$jenis_obat_text->setAtribute("disabled='disabled'");
	$laporan_form->addElement("Jenis Obat", $jenis_obat_text);
	$label_text = new Text("lmhpo_label", "lmhpo_label", "");
	$label_text->setAtribute("disabled='disabled'");
	$laporan_form->addElement("Jns. Pembelian", $label_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lmhpo.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lmhpo.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lmhpo_table = new Table(
		array("HNA", "Tgl. Msk. Awal", "Tgl. Msk. Akhir", "Daftar Faktur - Vendor - Tgl. Masuk", "Daftar No. Stok"),
		"",
		null,
		true
	);
	$lmhpo_table->setName("lmhpo");
	$lmhpo_table->setAction(false);
	
	//chooser obat:
	$obat_table = new Table(
		array("Nomor", "Nama Obat", "Jenis Obat", "Jenis Pembelian"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("Nomor", "id", "digit8");
	$obat_adapter->add("Nama Obat", "nama_obat");
	$obat_adapter->add("Jenis Obat", "nama_jenis_obat");
	$obat_adapter->add("Jenis Pembelian", "label", "unslug");
	$obat_dbtable = new DBTable($db, "smis_fr_stok_obat");
	$obat_dbtable->setViewForSelect(true);
	$filter = "1";
	if (isset($_POST['kriteria'])) {
		$filter .= " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR label LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT *
		FROM (
			SELECT DISTINCT smis_fr_dobat_f_masuk.id_obat AS 'id', smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.label
			FROM smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id
			WHERE smis_fr_stok_obat.prop NOT LIKE 'del'
		) v_obat
		WHERE " . $filter . "
		ORDER BY nama_obat, nama_jenis_obat, label ASC
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT DISTINCT smis_fr_dobat_f_masuk.id_obat AS 'id', smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.label
			FROM smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id
			WHERE smis_fr_stok_obat.prop NOT LIKE 'del'
		) v_obat
		WHERE " . $filter . "
		ORDER BY nama_obat, nama_jenis_obat, label ASC
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	class ObatDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];
			$row = $this->dbtable->get_row("
				SELECT *
				FROM (
					SELECT DISTINCT smis_fr_dobat_f_masuk.id_obat AS 'id', smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.label
					FROM smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id
					WHERE smis_fr_stok_obat.prop NOT LIKE 'del'
				) v_obat
				WHERE id = '" . $id . "'
			");
			$data['id'] = $id;
			$data['nama_obat'] = $row->nama_obat;
			$data['nama_jenis_obat'] = $row->nama_jenis_obat;
			$data['label'] = $row->label;
			return $data;
		}
	}
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		class LMHPOAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = "";
				$array['HNA'] = self::format("money Rp. ", $row->hna);
				$array['Tgl. Msk. Awal'] = self::format("date d M Y", $row->tanggal_awal);
				$array['Tgl. Msk. Akhir'] = self::format("date d M Y", $row->tanggal_akhir);
				$array['Daftar Faktur - Vendor - Tgl. Masuk'] = $row->daftar_faktur_vendor_tanggal;
				$array['Daftar No. Stok'] = $row->daftar_no_stok;
				return $array;
			}
		}
		$lmhpo_adapter = new LMHPOAdapter();		
		$lmhpo_dbtable = new DBTable($db, "smis_fr_stok_obat");
		$filter = "id_obat = '" . $_POST['id_obat'] . "'";
		if (isset($_POST['kriteria'])) {
			$filter .= " AND (daftar_faktur_vendor_tanggal LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT id_obat, nama_obat, nama_jenis_obat, hna, MIN(tanggal_datang) AS 'tanggal_awal', MAX(tanggal_datang) AS 'tanggal_akhir', GROUP_CONCAT(CONCAT(no_faktur, ' - ', nama_vendor, ' - ', DATE_FORMAT(tanggal_datang, '%d/%m/%Y')) ORDER BY tanggal_datang, nama_vendor, no_faktur ASC SEPARATOR '<br/>') AS 'daftar_faktur_vendor_tanggal', GROUP_CONCAT(id ORDER BY id ASC SEPARATOR '<br/>') AS 'daftar_no_stok'
				FROM (
					SELECT smis_fr_stok_obat.*, smis_fr_dobat_f_masuk.id_obat, smis_fr_obat_f_masuk.no_faktur, smis_fr_obat_f_masuk.tanggal_datang
					FROM (smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id) LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
					WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_dobat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.tanggal_datang <> '0000-00-00'
					ORDER BY smis_fr_obat_f_masuk.tanggal_datang, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.produsen, smis_fr_stok_obat.nama_vendor ASC
				) v_stok
				GROUP BY id_obat, hna
				ORDER BY nama_obat, nama_jenis_obat, min(tanggal_datang) ASC
			) v_stok
			WHERE " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT id_obat, nama_obat, nama_jenis_obat, hna, MIN(tanggal_datang) AS 'tanggal_awal', MAX(tanggal_datang) AS 'tanggal_akhir', GROUP_CONCAT(CONCAT(no_faktur, ' - ', nama_vendor, ' - ', DATE_FORMAT(tanggal_datang, '%d/%m/%Y')) ORDER BY tanggal_datang, nama_vendor, no_faktur ASC SEPARATOR '<br/>') AS 'daftar_faktur_vendor_tanggal', GROUP_CONCAT(id ORDER BY id ASC SEPARATOR '<br/>') AS 'daftar_no_stok'
				FROM (
					SELECT smis_fr_stok_obat.*, smis_fr_dobat_f_masuk.id_obat, smis_fr_obat_f_masuk.no_faktur, smis_fr_obat_f_masuk.tanggal_datang
					FROM (smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id) LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
					WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_dobat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.tanggal_datang <> '0000-00-00'
					ORDER BY smis_fr_obat_f_masuk.tanggal_datang, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.produsen, smis_fr_stok_obat.nama_vendor ASC
				) v_stok
				GROUP BY id_obat, hna
				ORDER BY nama_obat, nama_jenis_obat, min(tanggal_datang) ASC
			) v_stok
			WHERE " . $filter . "
		";
		$lmhpo_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class LLSODBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$id_obat = $_POST['id_obat'];
				$nama_obat = $_POST['nama_obat'];
				$nama_jenis_obat = $_POST['nama_jenis_obat'];
				$label = $_POST['label'];
				$data = $this->dbtable->get_result("
					SELECT id_obat, nama_obat, nama_jenis_obat, hna, tanggal_datang AS 'tanggal_datang', no_faktur, nama_vendor, id AS 'no_stok'
					FROM (
						SELECT smis_fr_stok_obat.*, smis_fr_dobat_f_masuk.id_obat, smis_fr_obat_f_masuk.no_faktur, smis_fr_obat_f_masuk.tanggal_datang
						FROM (smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id) LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
						WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_dobat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.tanggal_datang <> '0000-00-00'
						ORDER BY smis_fr_obat_f_masuk.tanggal_datang, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.produsen, smis_fr_stok_obat.nama_vendor ASC
					) v_stok
					WHERE id_obat = '" . $id_obat . "'
					ORDER BY nama_obat, nama_jenis_obat, tanggal_datang, hna ASC
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN MONITORING HARGA OBAT GUDANG FARMASI</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Nomor</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("digit8", $id_obat) . "</td>
									</tr>
									<tr>
										<td>Nama Obat</td>
										<td>:</td>
										<td>" . $nama_obat . "</td>
									</tr>
									<tr>
										<td>Jenis Obat</td>
										<td>:</td>
										<td>" . $nama_jenis_obat . "</td>
									</tr>
									<tr>
										<td>Jns. Pembelian</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $label) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No.</th>
										<th>HNA</th>
										<th>Tgl. Masuk</th>
										<th>No. Faktur</th>
										<th>Vendor</th>
										<th>No. Stok</th>
									</tr>";
				if (count($data) > 0) {
					$no = 1;
					$hna_b = "";
					foreach($data as $d) {
						$print_data .= "<tr>";
						if ($hna_b != $d->hna) {
							$print_data .= "<td>" . $no++ . "</td>";
							$print_data .= "<td>" . ArrayAdapter::format("money Rp. ", $d->hna) . "</td>";
						} else {
							$print_data .= "<td>&Tab;</td>";
							$print_data .= "<td>&Tab;</td>";
						}
							$print_data .= "<td>" . ArrayAdapter::format("date d M Y", $d->tanggal_datang) . "</td>";
							$print_data .= "<td>" . $d->no_faktur . "</td>";
							$print_data .= "<td>" . $d->nama_vendor . "</td>";
							$print_data .= "<td>" . ArrayAdapter::format("digit8", $d->no_stok) . "</td>";
						$print_data .= "</tr>";
						$hna_b = $d->hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='6' align='center'><i>Tidak terdapat data harga obat</i></td>
									</tr>";
				}
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lmhpo_dbresponder = new LLSODBResponder(
			$lmhpo_dbtable,
			$lmhpo_table,
			$lmhpo_adapter
		);
		$data = $lmhpo_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lmhpo_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LMHPOAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LMHPOAction.prototype.constructor = LMHPOAction;
	LMHPOAction.prototype = new TableAction();
	LMHPOAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['id_obat'] = $("#lmhpo_id_obat").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LMHPOAction.prototype.print = function() {
		if ($("#lmhpo_id_obat").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['id_obat'] = $("#lmhpo_id_obat").val();
		data['nama_obat'] = $("#lmhpo_nama_obat").val();
		data['nama_jenis_obat'] = $("#lmhpo_jenis_obat").val();
		data['label'] = $("#lmhpo_label").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				console.log(json);
				smis_print(json);
			}
		);
	};
	
	function ObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ObatAction.prototype.constructor = ObatAction;
	ObatAction.prototype = new TableAction();
	ObatAction.prototype.selected = function(json) {
		$("#lmhpo_id_obat").val(json.id);
		$("#lmhpo_nama_obat").val(json.nama_obat);
		$("#lmhpo_jenis_obat").val(json.nama_jenis_obat);
		$("#lmhpo_label").val(json.label);
	};
	
	var lmhpo;
	var obat;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		obat = new ObatAction(
			"obat",
			"gudang_farmasi",
			"laporan_monitoring_harga_per_obat",
			new Array()
		);
		obat.setSuperCommand("obat");
		lmhpo = new LMHPOAction(
			"lmhpo",
			"gudang_farmasi",
			"laporan_monitoring_harga_per_obat",
			new Array()
		)
	});
</script>