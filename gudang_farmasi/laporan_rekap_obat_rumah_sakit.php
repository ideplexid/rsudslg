<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$laporan_form = new Form("", "", "Gudang Farmasi : Rekap Obat Rumah Sakit - Form. A");
	$tanggal_from_text = new Text("lrors_tanggal_from", "lrors_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lrors_tanggal_to", "lrors_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$jenis_filter_option = new OptionBuilder();
	$jenis_filter_option->add("SEMUA", "semua", "1");
	$jenis_filter_option->add("PER OBAT", "per_obat");
	$jenis_filter_option->add("PER JENIS", "per_jenis");
	$jenis_filter_select = new Select("lrors_jenis_filter", "lrors_jenis_filter", $jenis_filter_option->getContent());
	$laporan_form->addElement("Jenis Filter", $jenis_filter_select);
	$kode_jenis_obat_hidden = new Hidden("lrors_kode_jenis_obat", "lrors_kode_jenis_obat", "");
	$laporan_form->addElement("", $kode_jenis_obat_hidden);
	$id_obat_hidden = new Hidden("lrors_id_obat", "lrors_id_obat", "");
	$laporan_form->addElement("", $id_obat_hidden);
	$nama_obat_text = new Text("lrors_nama_obat", "lrors_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("lrors_obat.chooser('lrors_obat', 'lrors_obat_button', 'lrors_obat', lrors_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Obat", $input_group);
	$nama_jenis_obat_text = new Text("lrors_nama_jenis_obat", "lrors_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$nama_jenis_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("lrors_jenis_obat.chooser('lrors_jenis_obat', 'lrors_jenis_obat_button', 'lrors_jenis_obat', lrors_jenis_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_jenis_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Jenis Obat", $input_group);
	$urutan_option = new OptionBuilder();
	$urutan_option->addSingle("KODE OBAT", "1");
	$urutan_option->addSingle("NAMA OBAT");
	$urutan_select = new Select("lrors_urutan", "lrors_urutan", $urutan_option->getContent());
	$laporan_form->addElement("Urutan", $urutan_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lrors.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='lrors_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$lrors_table = new Table(
		array(
			"No.", "Kode Obat", "Nama Obat", "Satuan", 
			"Stok Awal",
			"Jml. Beli", "Total Beli",
			"Depo IGD Jml. Jual", "Depo IGD Total Jual",
			"Depo IRJA Jml. Jual", "Depo IRJA Total Jual",
			"Depo IRNA I Jml. Jual", "Depo IRNA I Total Jual",
			"Depo IRNA II Jml. Jual", "Depo IRNA II Total Jual",
			"Depo IRNA III Jml. Jual", "Depo IRNA III Total Jual",
			"Stok Akhir"
		),
		"",
		null,
		true
	);
	$lrors_table->setName("lrors");
	$lrors_table->setAction(false);
	$lrors_table->setFooterVisible(false);
	$lrors_table->setHeaderVisible(false);
	$lrors_table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>ID Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Kode Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Nama Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Satuan</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Stok Awal</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Gudang Farmasi</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Depo Farmasi IGD</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Depo Farmasi IRJA</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Depo Farmasi IRNA I</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Depo Farmasi IRNA II</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Depo Farmasi IRNA III</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Stok Akhir</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jml. Beli</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total Beli</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jml. Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jml. Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jml. Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jml. Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jml. Jual</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total Jual</small></center>
			</th>
		</tr>
	");

	//chooser nama obat:
	$obat_table = new Table(
		array("No.", "ID Obat", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("lrors_obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter(true, "No.");
	$obat_adapter->add("ID Obat", "id", "digit6");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_dbtable = new DBTable($db, "smis_pr_barang");
	$obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	//chooser jenis obat:
	$jenis_obat_table = new Table(
		array("No.", "Kode", "Jenis Obat"),
		"",
		null,
		true
	);
	$jenis_obat_table->setName("lrors_jenis_obat");
	$jenis_obat_table->setModel(Table::$SELECT);
	$jenis_obat_adapter = new SimpleAdapter(true, "No.");
	$jenis_obat_adapter->add("Kode", "kode");
	$jenis_obat_adapter->add("Jenis Obat", "nama");
	$jenis_obat_dbtable = new DBTable($db, "smis_pr_jenis_barang");
	$jenis_obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$jenis_obat_dbresponder = new DBResponder(
		$jenis_obat_dbtable,
		$jenis_obat_table,
		$jenis_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("lrors_obat", $obat_dbresponder);
	$super_command->addResponder("lrors_jenis_obat", $jenis_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah_obat") {
			$jenis_filter = $_POST['jenis_filter'];
			$params = array();
			$params['order_by'] = $_POST['urutan'];
			if ($jenis_filter == "per_obat")
				$params['filter_id_obat'] = $_POST['id_obat'];
			else
				$params['filter_id_obat'] = "%%";
			if ($jenis_filter == "per_jenis")
				$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
			else
				$params['filter_kode_jenis_obat'] = "%%";
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jumlah_obat_msrs",
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah = 0;
			if ($content != null)
				$jumlah = $content[0];
			$data = array();
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$num = $_POST['num'];
			$params = array();
			$params['num'] = $_POST['num'];
			$params['order_by'] = $_POST['urutan'];
			if ($jenis_filter == "per_obat")
				$params['filter_id_obat'] = $_POST['id_obat'];
			else
				$params['filter_id_obat'] = "%%";
			if ($jenis_filter == "per_jenis")
				$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
			else
				$params['filter_kode_jenis_obat'] = "%%";
			$consumer_service = new ServiceConsumer(
				$db,
				"get_obat_info_msrs",
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$id_obat = $content[0];
			$kode_obat = $content[1];
			$nama_obat = $content[2];
			$nama_jenis_obat = $content[3];
			$satuan = $content[4];
			$satuan_konversi = $content[5];

			$stok_awal = 0;
			$stok_akhir = 0;

			$params = array();
			$params['id_obat'] = $id_obat;
			$params['satuan'] = $satuan;
			$params['konversi'] = 1;
			$params['satuan_konversi'] = $satuan_konversi;
			$params['tanggal_from'] = $tanggal_from;
			$params['tanggal_to'] = $tanggal_to;
			/// a. Data Gudang Farmasi
			$consumer_service = new ServiceConsumer(
				$db,
				"get_detail_info_obat",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_awal_gudang = $content[0];
			$stok_akhir_gudang = $content[1];
			$jumlah_beli_gudang = $content[2];
			$nominal_beli_gudang = $content[3];
			$stok_awal += $stok_awal_gudang;
			$stok_akhir += $stok_akhir_gudang;

			/// b. Data Depo IGD
			$consumer_service = new ServiceConsumer(
				$db,
				"get_detail_info_obat",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_awal_igd = $content[0];
			$stok_akhir_igd = $content[1];
			$jumlah_jual_igd = $content[2];
			$nominal_jual_igd = $content[3];
			$stok_awal += $stok_awal_igd;
			$stok_akhir += $stok_akhir_igd;

			/// c. Data Depo IRJA
			$consumer_service = new ServiceConsumer(
				$db,
				"get_detail_info_obat",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_awal_irja = $content[0];
			$stok_akhir_irja = $content[1];
			$jumlah_jual_irja = $content[2];
			$nominal_jual_irja = $content[3];
			$stok_awal += $stok_awal_irja;
			$stok_akhir += $stok_akhir_irja;
			
			/// d. Data Depo IRNA I
			$consumer_service = new ServiceConsumer(
				$db,
				"get_detail_info_obat",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_awal_irna = $content[0];
			$stok_akhir_irna = $content[1];
			$jumlah_jual_irna = $content[2];
			$nominal_jual_irna = $content[3];
			$stok_awal += $stok_awal_irna;
			$stok_akhir += $stok_akhir_irna;
			
			/// e. Data Depo IRNA II
			$consumer_service = new ServiceConsumer(
				$db,
				"get_detail_info_obat",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_awal_irna_ii = $content[0];
			$stok_akhir_irna_ii = $content[1];
			$jumlah_jual_irna_ii = $content[2];
			$nominal_jual_irna_ii = $content[3];
			$stok_awal += $stok_awal_irna_ii;
			$stok_akhir += $stok_akhir_irna_ii;
			
			/// f. Data Depo IRNA III
			$consumer_service = new ServiceConsumer(
				$db,
				"get_detail_info_obat",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_awal_irna_iii = $content[0];
			$stok_akhir_irna_iii = $content[1];
			$jumlah_jual_irna_iii = $content[2];
			$nominal_jual_irna_iii = $content[3];
			$stok_awal += $stok_awal_irna_iii;
			$stok_akhir += $stok_akhir_irna_iii;
						
			$html = "
				<tr>
					<td id='nomor'><small>" . ($num + 1) . "</small></td>
					<td id='id_obat'><small>" .  $id_obat . "</small></td>
					<td id='kode_obat'><small>" .  $kode_obat . "</small></td>
					<td id='nama_obat'><small>" .  $nama_obat . "</small></td>
					<td id='satuan'><small>" .  $satuan . "</small></td>
					<td id='stok_awal'><small>" . ArrayAdapter::format("number", $stok_awal) . "</small></td>
					<td id='gudang_jumlah_beli'><small>" . ArrayAdapter::format("number", $jumlah_beli_gudang) . "</small></td>
					<td id='gudang_subtotal_beli'><small>" . ArrayAdapter::format("money", $nominal_beli_gudang) . "</small></td>
					<td id='depo_igd_jumlah_jual'><small>" . ArrayAdapter::format("number", $jumlah_jual_igd) . "</small></td>
					<td id='depo_igd_subtotal_jual'><small>" . ArrayAdapter::format("money", $nominal_jual_igd) . "</small></td>
					<td id='depo_irja_jumlah_jual'><small>" . ArrayAdapter::format("number", $jumlah_jual_irja) . "</small></td>
					<td id='depo_irja_subtotal_jual'><small>" . ArrayAdapter::format("money", $nominal_jual_irja) . "</small></td>
					<td id='depo_irna_jumlah_jual'><small>" . ArrayAdapter::format("number", $jumlah_jual_irna) . "</small></td>
					<td id='depo_irna_subtotal_jual'><small>" . ArrayAdapter::format("money", $nominal_jual_irna) . "</small></td>
					<td id='depo_irna_ii_jumlah_jual'><small>" . ArrayAdapter::format("number", $jumlah_jual_irna_ii) . "</small></td>
					<td id='depo_irna_ii_subtotal_jual'><small>" . ArrayAdapter::format("money", $nominal_jual_irna_ii) . "</small></td>
					<td id='depo_irna_iii_jumlah_jual'><small>" . ArrayAdapter::format("number", $jumlah_jual_irna_iii) . "</small></td>
					<td id='depo_irna_iii_subtotal_jual'><small>" . ArrayAdapter::format("money", $nominal_jual_irna_iii) . "</small></td>
					<td id='stok_akhir'><small>" . ArrayAdapter::format("number", $stok_akhir) . "</small></td>
				</tr>
			";

			$data = array();
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['kode_obat'] = $kode_obat;
			$data['nama_obat'] = $nama_obat;
			$data['nama_jenis_obat'] = $nama_jenis_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/template_rekap_obat.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP OBAT RS");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B2", ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("G9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("H9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("J9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("L9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("N9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("P9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("R9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("T9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("I9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("K9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("M9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("O9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("Q9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("S9")->getNumberFormat()->setFormatCode("#,##0.00");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_awal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->gudang_jumlah_beli);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->gudang_subtotal_beli);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_igd_jumlah_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_igd_subtotal_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irja_jumlah_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irja_subtotal_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irna_jumlah_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irna_subtotal_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irna_ii_jumlah_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irna_ii_subtotal_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irna_iii_jumlah_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->depo_irna_iii_subtotal_jual);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_akhir);
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("L" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("N" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("P" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("R" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("T" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("M" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("O" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("Q" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("S" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=REKAP_OBAT_RS_FORM_A_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}

	$loading_bar = new LoadingBar("lrors_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lrors.cancel()");
	$loading_modal = new Modal("lrors_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo $lrors_table->getHtml();
	echo "<div id='lrors_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LRORSAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRORSAction.prototype.constructor = LRORSAction;
	LRORSAction.prototype = new TableAction();
	LRORSAction.prototype.view = function() {
		if ($("#lrors_tanggal_from").val() == "" || $("#lrors_tanggal_to").val() == "")
			return;
		var self = this;
		$("#lrors_info").empty();
		$("#lrors_loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#lrors_loading_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_jumlah_obat";
		data['jenis_filter'] = $("#lrors_jenis_filter").val();
		data['id_obat'] = $("#lrors_id_obat").val();
		data['nama_obat'] = $("#lrors_nama_obat").val();
		data['nama_jenis_obat'] = $("#lrors_nama_jenis_obat").val();
		data['kode_jenis_obat'] = $("#lrors_kode_jenis_obat").val();
		data['urutan'] = $("#lrors_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#lrors_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	LRORSAction.prototype.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize();
			} else {
				$("#lrors_loading_modal").smodal("hide");
				$("#lrors_info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#lrors_export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info_obat";
		data['num'] = num;
		data['tanggal_from'] = $("#lrors_tanggal_from").val();
		data['tanggal_to'] = $("#lrors_tanggal_to").val();
		data['jenis_filter'] = $("#lrors_jenis_filter").val();
		data['id_obat'] = $("#lrors_id_obat").val();
		data['nama_obat'] = $("#lrors_nama_obat").val();
		data['nama_jenis_obat'] = $("#lrors_nama_jenis_obat").val();
		data['kode_jenis_obat'] = $("#lrors_kode_jenis_obat").val();
		data['urutan'] = $("#lrors_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("tbody#lrors_list").append(
					json.html
				);
				$("#lrors_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	LRORSAction.prototype.finalize = function() {
		var num_rows = $("tbody#lrors_list tr").length;
		for (var i = 0; i < num_rows; i++)
			$("tbody#lrors_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
		$("#lrors_loading_modal").smodal("hide");
		$("#lrors_info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#lrors_export_button").removeAttr("onclick");
		$("#lrors_export_button").attr("onclick", "lrors.export_xls()");
	};
	LRORSAction.prototype.cancel = function() {
		FINISHED = true;
	};
	LRORSAction.prototype.export_xls = function() {
		showLoading();
		var num_rows = $("#lrors_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var nomor = $("tbody#lrors_list tr:eq(" + i + ") td#nomor").text();
			var id_obat = $("tbody#lrors_list tr:eq(" + i + ") td#id_obat").text();
			var kode_obat = $("tbody#lrors_list tr:eq(" + i + ") td#kode_obat").text();
			var nama_obat = $("tbody#lrors_list tr:eq(" + i + ") td#nama_obat").text();
			var satuan = $("tbody#lrors_list tr:eq(" + i + ") td#satuan").text();
			var stok_awal = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#stok_awal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var gudang_jumlah_beli = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#gudang_jumlah_beli").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var gudang_subtotal_beli = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#gudang_subtotal_beli").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_igd_jumlah_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_igd_jumlah_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_igd_subtotal_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_igd_subtotal_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irja_jumlah_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irja_jumlah_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irja_subtotal_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irja_subtotal_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irna_jumlah_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irna_jumlah_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irna_subtotal_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irna_subtotal_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irna_ii_jumlah_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irna_ii_jumlah_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irna_ii_subtotal_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irna_ii_subtotal_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irna_iii_jumlah_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irna_iii_jumlah_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var depo_irna_iii_subtotal_jual = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#depo_irna_iii_subtotal_jual").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var stok_akhir = parseFloat($("tbody#lrors_list tr:eq(" + i + ") td#stok_akhir").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			d_data[i] = {
				"nomor" 						: nomor,
				"id_obat" 						: id_obat,
				"kode_obat" 					: kode_obat,
				"nama_obat" 					: nama_obat,
				"satuan" 						: satuan,
				"stok_awal"						: stok_awal,
				"gudang_jumlah_beli"			: gudang_jumlah_beli,
				"gudang_subtotal_beli"			: gudang_subtotal_beli,
				"depo_igd_jumlah_jual"			: depo_igd_jumlah_jual,
				"depo_igd_subtotal_jual"		: depo_igd_subtotal_jual,
				"depo_irja_jumlah_jual"			: depo_irja_jumlah_jual,
				"depo_irja_subtotal_jual"		: depo_irja_subtotal_jual,
				"depo_irna_jumlah_jual"			: depo_irna_jumlah_jual,
				"depo_irna_subtotal_jual"		: depo_irna_subtotal_jual,
				"depo_irna_ii_jumlah_jual"		: depo_irna_ii_jumlah_jual,
				"depo_irna_ii_subtotal_jual"	: depo_irna_ii_subtotal_jual,
				"depo_irna_iii_jumlah_jual"		: depo_irna_iii_jumlah_jual,
				"depo_irna_iii_subtotal_jual"	: depo_irna_iii_subtotal_jual,
				"stok_akhir"					: stok_akhir
			};
		}
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tanggal_from'] = $("#lrors_tanggal_from").val();
		data['tanggal_to'] = $("#lrors_tanggal_to").val();
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		postForm(data);
		dismissLoading();
	};

	function LRORSObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRORSObatAction.prototype.constructor = LRORSObatAction;
	LRORSObatAction.prototype = new TableAction();
	LRORSObatAction.prototype.selected = function(json) {
		$("#lrors_id_obat").val(json.id);
		$("#lrors_nama_obat").val(json.nama);
	};
	
	function LRORSJenisObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRORSJenisObatAction.prototype.constructor = LRORSJenisObatAction;
	LRORSJenisObatAction.prototype = new TableAction();
	LRORSJenisObatAction.prototype.selected = function(json) {
		$("#lrors_kode_jenis_obat").val(json.kode);
		$("#lrors_nama_jenis_obat").val(json.nama);
	};

	var lrors;
	var lrors_obat;
	var lrors_jenis_obat;
	var FINISHED;
	$(document).ready(function() {
		lrors_obat = new LRORSObatAction(
			"lrors_obat",
			"gudang_farmasi",
			"laporan_rekap_obat_rumah_sakit",
			new Array()
		);
		lrors_obat.setSuperCommand("lrors_obat");
		lrors_jenis_obat = new LRORSJenisObatAction(
			"lrors_jenis_obat",
			"gudang_farmasi",
			"laporan_rekap_obat_rumah_sakit",
			new Array()
		);
		lrors_jenis_obat.setSuperCommand("lrors_jenis_obat");
		lrors = new LRORSAction(
			"lrors",
			"gudang_farmasi",
			"laporan_rekap_obat_rumah_sakit",
			new Array()
		);
		$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(7)").hide();
		$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(8)").hide();
		$("#lrors_jenis_filter").on("change", function() {
			var jenis_filter = $("#lrors_jenis_filter").val();
			$("#lrors_id_obat").val("");
			$("#lrors_nama_obat").val("");
			$("#lrors_kode_jenis_obat").val("");
			$("#lrors_jenis_obat").val("");
			if (jenis_filter == "semua") {
				$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(7)").hide();
				$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(8)").hide();
			} else if (jenis_filter == "per_obat") {
				$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(7)").show();
				$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(8)").hide();
			} else if (jenis_filter == "per_jenis") {
				$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(7)").hide();
				$("#laporan_rekap_obat_rumah_sakit > div.form-container > form > div:nth-child(8)").show();
			}
		});
		$("#lrors_loading_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#lrors_loading_modal").on("hide", function() {
			$("a.close").show();
		});
		$("tbody#lrors_list").append(
			"<tr>" +
				"<td colspan='22'><strong><center><small>DATA REKAP OBAT BELUM DIPROSES</small></center></strong></td>" +
			"</tr>"
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		})
	});
</script>