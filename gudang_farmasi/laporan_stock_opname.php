<?php
    require_once("smis-base/smis-include-service-consumer.php");
    require_once("gudang_farmasi/kelas/GudangFarmasiInventory.php");
	global $db;

	$laporan_form = new Form("", "", "Gudang Farmasi : Stock Opname");
	$tanggal_from_text = new Text("lso_tanggal_from", "lso_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lso_tanggal_to", "lso_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lso.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='lso_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$lso_table = new Table(
		array("No.", "Kode Obat", "Nama Obat", "Jenis Obat", "Stok Sebelum Diubah", "Stok Setelah Diubah", "Selisih", "Nilai"),
		"",
		null,
		true
	);
	$lso_table->setName("lso");
	$lso_table->setAction(false);
	$lso_table->setFooterVisible(false);

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$row = $db->get_row("
				SELECT COUNT(DISTINCT id_obat) jumlah
				FROM smis_fr_stock_opname
				WHERE prop = '' AND DATE(tanggal_so) >= '" . $tanggal_from . "' AND DATE(tanggal_so) AND DATE(tanggal_so) <= '" . $tanggal_to . "'
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			$data = array();
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$num = $_POST['num'];
			
			$row = $db->get_row("
				SELECT DISTINCT id_obat, kode_obat, nama_obat, nama_jenis_obat
				FROM smis_fr_stock_opname
				WHERE prop = '' AND DATE(tanggal_so) >= '" . $tanggal_from . "' AND DATE(tanggal_so) <= '" . $tanggal_to . "'
				ORDER BY nama_obat ASC
				LIMIT " . $num . ", 1
			");

			$id_obat = $row->id_obat;
			$kode_obat = $row->kode_obat;
			$nama_obat = $row->nama_obat;
			$nama_jenis_obat = $row->nama_jenis_obat;

			$stok_sebelum_row = $db->get_row("
				SELECT stok_sebelum
				FROM smis_fr_stock_opname
				WHERE prop = '' AND DATE(tanggal_so) >= '" . $tanggal_from . "' AND DATE(tanggal_so) <= '" . $tanggal_to . "' AND id_obat = '" . $id_obat . "'
				ORDER BY tanggal_so ASC, id ASC
				LIMIT 0, 1
			");
			$stok_sebelum = 0;
			if ($stok_sebelum_row != null)
				$stok_sebelum = $stok_sebelum_row->stok_sebelum;

			$stok_sesudah_row = $db->get_row("
				SELECT stok_sesudah
				FROM smis_fr_stock_opname
				WHERE prop = '' AND DATE(tanggal_so) >= '" . $tanggal_from . "' AND DATE(tanggal_so) <= '" . $tanggal_to . "' AND id_obat = '" . $id_obat . "'
				ORDER BY tanggal_so DESC, id DESC
				LIMIT 0, 1
			");
			$stok_sesudah = 0;
			if ($stok_sesudah_row != null)
				$stok_sesudah = $stok_sesudah_row->stok_sesudah;

			$selisih = $stok_sesudah - $stok_sebelum;
			$nilai = $selisih * GudangFarmasiInventory::getLastHPP($db, $id_obat, $tanggal_to);
			$f_nilai = $nilai == 0 ? ArrayAdapter::format("money Rp. ", "0") : ArrayAdapter::format("money Rp. ", $nilai);
			
			$html = "
				<tr id='data_lso_" . $num . "'>
					<td id='nomor'></td>
					<td id='id_obat' style='display: none;'>" .  $id_obat . "</td>
					<td id='kode_obat'><small>" .  $kode_obat . "</small></td>
					<td id='nama_obat'><small>" .  $nama_obat . "</small></td>
					<td id='nama_jenis_obat'><small>" .  $nama_jenis_obat . "</small></td>
					<td id='stok_sebelum' style='display: none;'>" .  $stok_sebelum . "</td>
					<td id='f_stok_sebelum'><small>" .  ArrayAdapter::format("number", $stok_sebelum) . "</small></td>
					<td id='stok_sesudah' style='display: none;'>" .  $stok_sesudah . "</td>
					<td id='f_stok_sesudah'><small>" .  ArrayAdapter::format("number", $stok_sesudah) . "</small></td>
					<td id='selisih' style='display: none;'>" .  $selisih . "</td>
					<td id='f_selisih'><small>" .  ArrayAdapter::format("number", $selisih) . "</small></td>
					<td id='nilai' style='display: none;'>" .  $nilai . "</td>
					<td id='f_nilai'><small>" .  $f_nilai . "</small></td>
				</tr>
			";

			$data = array();
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['kode_obat'] = $kode_obat == null ? "N/A" : $kode_obat;
			$data['nama_obat'] = $nama_obat;
			$data['nama_jenis_obat'] = $nama_jenis_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/template_laporan_stock_opname.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("STOCK OPNAME");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$nama_instansi = getSettings($db, "smis_autonomous_title", "SIMRS");
			if ($nama_instansi == "")
				$objWorksheet->setCellValue("B2", "GUDANG FARMASI");
			else
				$objWorksheet->setCellValue("B2", "GUDANG FARMASI - " . $nama_instansi);
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_sebelum);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_sesudah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->selisih);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nilai);
				$objWorksheet->getStyle("F" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=STOCK_OPNAME_GUDANG_FARMASI_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("laporan_stock_opname_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lso.cancel()");
	$loading_modal = new Modal("lso_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lso_table->getHtml();
	echo "</div>";
	echo "<div id='lso_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
	echo addJS("gudang_farmasi/js/laporan_stock_opname_action.js", false);
	echo addJS("gudang_farmasi/js/laporan_stock_opname.js", false);
?>