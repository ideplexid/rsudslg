<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	global $db;

	$form = new Form("lrp_form", "", "Gudang Farmasi : Laporan Rekap Pembelian");
	$tanggal_from_text = new Text("lrp_tanggal_from", "lrp_tanggal_from", date("Y-m-") . "01");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lrp_tanggal_to", "lrp_tanggal_to", date("Y-m-d"));
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Waktu Akhir", $tanggal_to_text);
	$id_vendor_hidden = new Hidden("lrp_id_vendor", "lrp_id_vendor", "%%");
	$form->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("lrp_nama_vendor", "lrp_nama_vendor", "SEMUA");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$nama_vendor_text->setClass("smis-two-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setAction("lrp_vendor.chooser('lrp_vendor', 'lrp_vendor_button', 'lrp_vendor', lrp_vendor)");
	$browse_button->setIcon("icon-white icon-list-alt");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAtribute("id='lrp_vendor_browse'");
	$clear_button = new Button("", "", "Hapus");
	$clear_button->setClass("btn-inverse");
	$clear_button->setAction("lrp_vendor.clear()");
	$clear_button->setIcon("fa fa-times");
	$clear_button->setIsButton(Button::$ICONIC);
	$clear_button->setAtribute("id='lrp_vendor_clear'");
	$vendor_input_group = new InputGroup("");
	$vendor_input_group->addComponent($nama_vendor_text);
	$vendor_input_group->addComponent($browse_button);
	$vendor_input_group->addComponent($clear_button);
	$form->addElement("Distributor", $vendor_input_group);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lrp.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setAtribute("id='lrp_download'");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$form->addElement("", $btn_group);

	$table = new Table(
		array("No.", "Tanggal Diterima", "Tanggal Faktur", "No. OPL", "Kode Beli", "Faktur", "Supplier", "ID Obat", "Kode Obat", "Nama Obat", "Harga (Rp)", "Jumlah", "Subtotal (Rp)", "Potongan", "PPn", "Materai", "Total (Rp)"),
		"",
		null,
		true
	);
	$table->setName("lrp");
	$table->setAction(false);
	$table->setFooterVisible(false);

	//get_daftar_vendor_consumer:
	$vendor_table = new Table(
		array("Nama", "NPWP", "Alamat", "No. Telp."),
		"",
		null,
		true
	);
	$vendor_table->setName("lrp_vendor");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter();
	$vendor_adapter->add("Nama", "nama");
	$vendor_adapter->add("NPWP", "npwp");
	$vendor_adapter->add("Alamat", "alamat");
	$vendor_adapter->add("No. Telp.", "telpon");
	$vendor_service_responder = new ServiceResponder(
		$db,
		$vendor_table,
		$vendor_adapter,
		"get_daftar_vendor"
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("lrp_vendor", $vendor_service_responder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_item_number") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$id_vendor = $_POST['id_vendor'];
			$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
			$row = $dbtable->get_row("
				SELECT COUNT(*) AS 'jumlah'
				FROM smis_fr_obat_f_masuk
				WHERE prop NOT LIKE 'del' AND tanggal_datang >= '" . $tanggal_from . "' AND tanggal_datang <= '" . $tanggal_to . "' AND id > 0 AND id_vendor LIKE '" . $id_vendor . "'
			");
			$data = array();
			$data['jumlah'] = $row->jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_item_info") {
			$num = $_POST['num'];
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$id_vendor = $_POST['id_vendor'];
			$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
			$html = "";
			$header_row = $dbtable->get_row("
				SELECT *
				FROM smis_fr_obat_f_masuk
				WHERE prop NOT LIKE 'del' AND tanggal_datang >= '" . $tanggal_from . "' AND tanggal_datang <= '" . $tanggal_to . "' AND id > 0 AND id_vendor LIKE '" . $id_vendor . "'
				LIMIT " . $num . ", 1
			");
			$detail_rows = $dbtable->get_result("
				SELECT *
				FROM smis_fr_dobat_f_masuk
				WHERE id_obat_f_masuk = '" . $header_row->id . "'
			");
			$total_subtotal = 0;
			$total_potongan = 0;
			$num_is_exist = false;
			foreach ($detail_rows as $detail_row) {
				$hna = $detail_row->hna / 1.1;
				if ($header_row->use_ppn == 0)
					$hna = $detail_row->hna;
				$diskon = round($detail_row->diskon / 100 * $detail_row->jumlah * $ppn, 2);
				if ($detail_row->t_diskon == "nominal")
					$diskon = $detail_row->diskon;
				$nomor = "";
				if (!$num_is_exist) {
					$nomor = $num + 1;
					$num_is_exist = true;
				}
				$html .= "
					<tr>
						<td id='lrp_nomor'><small>" . $nomor . "</small></td>
						<td id='lrp_tanggal_datang'><small>" . ArrayAdapter::format("date d-m-Y", $header_row->tanggal_datang) . "</small></td>
						<td id='lrp_tanggal'><small>" . ArrayAdapter::format("date d-m-Y", $header_row->tanggal) . "</small></td>
						<td id='lrp_no_opl'><small>" . $header_row->no_opl . "</small></td>
						<td id='lrp_no_bbm'><small>" . $header_row->no_bbm . "</small></td>
						<td id='lrp_no_faktur'><small>" . $header_row->no_faktur . "</small></td>
						<td id='lrp_nama_vendor'><small>" . $header_row->nama_vendor . "</small></td>
						<td id='lrp_id_obat'><small>" . $detail_row->id_obat . "</small></td>
						<td id='lrp_kode_obat'><small>" . $detail_row->kode_obat . "</small></td>
						<td id='lrp_nama_obat'><small>" . $detail_row->nama_obat . "</small></td>
						<td id='lrp_hna'><small><div align='right'>" . ArrayAdapter::format("only-money", $hna) . "</div></small></td>
						<td id='lrp_jumlah'><small>" . ArrayAdapter::format("number", $detail_row->jumlah) . "</small></td>
						<td id='lrp_total' style='display: none;'>" . round($detail_row->jumlah * $hna) . "</td>
						<td id='lrp_f_total'><small><div align='right'>" . ArrayAdapter::format("only-money", round($detail_row->jumlah * $hna)) . "</div></small></td>
						<td id='lrp_diskon' style='display: none;'>" . $diskon . "</td>
						<td id='lrp_f_diskon'><small><div align='right'>" . ArrayAdapter::format("only-money", $diskon) . "</div></small></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				";
				$total_subtotal += round($detail_row->jumlah * $hna);
				$total_potongan += $diskon;
			}
			$diskon_global = round(($total_subtotal - $total_potongan) * $header_row->diskon / 100, 2);
			if ($header_row->t_diskon == "nominal")
				$diskon_global = $header_row->diskon;
			$total_potongan = $total_potongan + $diskon_global;
			$total_netto = $total_subtotal - $total_potongan;
			$ppn = floor($total_netto * 0.1);
			if ($header_row->use_ppn == 0)
				$ppn = 0;
			$total_tagihan = $total_netto + $ppn + $header_row->materai;
			$html .= "
				<tr>
					<td colspan='12' style='vertical-align: middle !important;'><center><small><strong>SUBTOTAL</strong></small></center></td>
					<td id='lrp_t_subtotal' style='display: none;'>" . $total_subtotal . "</td>
					<td id='lrp_f_t_subtotal'><small><div align='right'>" . ArrayAdapter::format("only-money", $total_subtotal) . "</div></small></td>
					<td id='lrp_t_potongan' style='display: none;'>" . $total_potongan . "</td>
					<td id='lrp_f_t_potongan'><small><div align='right'>" . ArrayAdapter::format("only-money", $total_potongan) . "</div></small></td>
					<td id='lrp_t_ppn' style='display: none;'>" . $ppn . "</td>
					<td id='lrp_f_t_ppn'><small><div align='right'>" . ArrayAdapter::format("only-money", $ppn) . "</div></small></td>
					<td id='lrp_t_materai' style='display: none;'>" . $header_row->materai . "</td>
					<td id='lrp_f_t_materai'><small><div align='right'>" . ArrayAdapter::format("only-money", $header_row->materai) . "</div></small></td>
					<td id='lrp_t_tagihan' style='display: none;'>" . $total_tagihan . "</td>
					<td id='lrp_f_t_tagihan'><small><div align='right'>" . ArrayAdapter::format("only-money", $total_tagihan) . "</div></small></td>
				</tr>
			";
			$data = array();
			$data['no_faktur'] = $header_row->no_faktur;
			$data['nama_vendor'] = $header_row->nama_vendor;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "download") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/rekap_pembelian_template.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("Rekap Pembelian");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("A1", ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("A2", $alamat_entitas);
			$objWorksheet->setCellValue("A4", "Laporan Rekap Pembelian Tanggal : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));			
			$data = json_decode($_POST['d_data']);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				if ($d->label == "detail") {
					$col_num = 0;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("date d-m-Y", $d->tanggal_datang));
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("date d-m-Y", $d->tanggal));
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no_opl);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no_bbm);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->no_faktur);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_vendor);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hna);
					$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
					$objWorksheet->getStyle("L" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->subtotal);
					$objWorksheet->getStyle("M" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "");
					$style = array(
						"font"		=> array(
							"bold"			=> false
						)
					);
					$objWorksheet->getStyle("A" . $row_num . ":Q" . $row_num)->applyFromArray($style);
					$style = array(
						"alignment"	=> array(
							"horizontal"	=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT
						)
					);
					$objWorksheet->getStyle("A" . $row_num . ":J" . $row_num)->applyFromArray($style);
					$style = array(
						"alignment"	=> array(
							"horizontal"	=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
						)
					);
					$objWorksheet->getStyle("K" . $row_num . ":Q" . $row_num)->applyFromArray($style);
				} else {
					$objWorksheet->mergeCells("A" . $row_num . ":L" . $row_num);
					$style = array(
						"alignment"	=> array(
							"horizontal"	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
						),
						"font"		=> array(
							"bold"			=> true
						)
					);
					$objWorksheet->getStyle("A" . $row_num . ":Q" . $row_num)->applyFromArray($style);
					$style = array(
						"alignment"	=> array(
							"horizontal"	=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
						)
					);
					$objWorksheet->getStyle("K" . $row_num . ":Q" . $row_num)->applyFromArray($style);
					$col_num = 0;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, "SUBTOTAL");
					$col_num += 12;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->t_subtotal);
					$objWorksheet->getStyle("M" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->t_potongan);
					$objWorksheet->getStyle("N" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->t_ppn);
					$objWorksheet->getStyle("O" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->t_materai);
					$objWorksheet->getStyle("P" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
					$col_num++;
					$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->t_tagihan);
					$objWorksheet->getStyle("Q" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				}
				$objWorksheet->insertNewRowBefore($row_num + 1, 1);
				$row_num++;
				$end_row_num++;
			}
			$objWorksheet->getRowDimension($row_num)->setVisible(false);
			$objWorksheet->getRowDimension($row_num + 1)->setVisible(false);
			
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=REKAP_PEMBELIAN_" . $_POST['tanggal_from'] . "_" . $_POST['tanggal_to'] . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}

	$loading_bar = new LoadingBar("lrp_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lrp.cancel()");
	$loading_modal = new Modal("lrp_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);

	echo $loading_modal->getHtml();
	echo $form->getHtml();
	echo "<div id='table_content'>";
	echo $table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LRPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRPAction.prototype.constructor = LRPAction;
	LRPAction.prototype = new TableAction();
	LRPAction.prototype.view = function() {
		var self = this;
		var data = this.getRegulerData();
		data['tanggal_from'] = $("#lrp_tanggal_from").val();
		data['tanggal_to'] = $("#lrp_tanggal_to").val();
		data['id_vendor'] = $("#lrp_id_vendor").val();
		data['command'] = "get_item_number";
		$("#lrp_download").removeAttr("onclick");
		FINISHED = false;
		$("#lrp_list").empty();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#lrp_loading_bar").sload("true", "Harap ditunggu...", 0);
				$("#lrp_modal").smodal("show");
				self.fill_html(0, json.jumlah);
			}
		);
	};
	LRPAction.prototype.fill_html = function(num, limit) {
		if (num >= limit || FINISHED) {
			this.finalize();
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['num'] = num;
		data['tanggal_from'] = $("#lrp_tanggal_from").val();
		data['tanggal_to'] = $("#lrp_tanggal_to").val();
		data['id_vendor'] = $("#lrp_id_vendor").val();
		data['command'] = "get_item_info";
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#lrp_list").append(json.html);
				$("#lrp_loading_bar").sload("true", json.no_faktur + " - " + json.nama_vendor + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fill_html(num + 1, limit);
			}
		);
	};
	LRPAction.prototype.finalize = function() {
		$("#lrp_loading_bar").sload("true", "Finalisasi..", 100);
		$("#lrp_modal").smodal("hide");
		$("#lrp_loading_bar").sload("true", "", 0);
		$("#lrp_download").attr("onclick", "lrp.download()");
	};
	LRPAction.prototype.cancel = function() {
		FINISHED = true;
	};
	LRPAction.prototype.download = function() {
		var nor = $("#lrp_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < nor; i++) {
			var dd_data = {};
			var first_col_val = $("#lrp_list tr:eq(" + i + ") td:eq(0)").text();
			if (first_col_val == "SUBTOTAL") {
				var label = "SUBTOTAL";
				var t_subtotal = $("#lrp_list tr:eq(" + i + ") td#lrp_t_subtotal").text();
				var t_potongan = $("#lrp_list tr:eq(" + i + ") td#lrp_t_potongan").text();
				var t_ppn = $("#lrp_list tr:eq(" + i + ") td#lrp_t_ppn").text();
				var t_materai = $("#lrp_list tr:eq(" + i + ") td#lrp_t_materai").text();
				var t_tagihan = $("#lrp_list tr:eq(" + i + ") td#lrp_t_tagihan").text();
				dd_data['t_subtotal'] = t_subtotal;
				dd_data['t_potongan'] = t_potongan;
				dd_data['t_ppn'] = t_ppn;
				dd_data['t_materai'] = t_materai;
				dd_data['t_tagihan'] = t_tagihan;
				dd_data['label'] = "subtotal";
			} else {
				var nomor = $("#lrp_list tr:eq(" + i + ") td#lrp_nomor").text();
				var tanggal_datang = $("#lrp_list tr:eq(" + i + ") td#lrp_tanggal_datang").text();
				var tanggal = $("#lrp_list tr:eq(" + i + ") td#lrp_tanggal").text();
				var no_opl = $("#lrp_list tr:eq(" + i + ") td#lrp_no_opl").text();
				var no_bbm = $("#lrp_list tr:eq(" + i + ") td#lrp_no_bbm").text();
				var no_faktur = $("#lrp_list tr:eq(" + i + ") td#lrp_no_faktur").text();
				var nama_vendor = $("#lrp_list tr:eq(" + i + ") td#lrp_nama_vendor").text();
				var id_obat = $("#lrp_list tr:eq(" + i + ") td#lrp_id_obat").text();
				var kode_obat = $("#lrp_list tr:eq(" + i + ") td#lrp_kode_obat").text();
				var nama_obat = $("#lrp_list tr:eq(" + i + ") td#lrp_nama_obat").text();
				var hna = $("#lrp_list tr:eq(" + i + ") td#lrp_hna").text().replace(/[^0-9-,]/g, '').replace(",", ".");
				var jumlah = $("#lrp_list tr:eq(" + i + ") td#lrp_jumlah").text().replace(/[^0-9-,]/g, '').replace(",", ".");
				var subtotal = $("#lrp_list tr:eq(" + i + ") td#lrp_total").text();
				var diskon = $("#lrp_list tr:eq(" + i + ") td#lrp_diskon").text();
				dd_data['nomor'] = nomor;
				dd_data['tanggal_datang'] = tanggal_datang;
				dd_data['tanggal'] = tanggal;
				dd_data['no_opl'] = no_opl;
				dd_data['no_bbm'] = no_bbm;
				dd_data['no_faktur'] = no_faktur;
				dd_data['nama_vendor'] = nama_vendor;
				dd_data['id_obat'] = id_obat;
				dd_data['kode_obat'] = kode_obat;
				dd_data['nama_obat'] = nama_obat;
				dd_data['hna'] = hna;
				dd_data['jumlah'] = jumlah;
				dd_data['subtotal'] = subtotal;
				dd_data['diskon'] = diskon;
				dd_data['label'] = "detail";
			}
			d_data[i] = dd_data;
		}
		var data = this.getRegulerData();
		data['command'] = "download";
		data['d_data'] = JSON.stringify(d_data);
		data['tanggal_from'] = $("#lrp_tanggal_from").val();
		data['tanggal_to'] = $("#lrp_tanggal_to").val();
		postForm(data);
	};

	function LRPVendorAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRPVendorAction.prototype.constructor = LRPVendorAction;
	LRPVendorAction.prototype = new TableAction();
	LRPVendorAction.prototype.selected = function(json) {
		$("#lrp_id_vendor").val(json.id);
		$("#lrp_nama_vendor").val(json.nama);
	};
	LRPVendorAction.prototype.clear = function() {
		$("#lrp_id_vendor").val("%%");
		$("#lrp_nama_vendor").val("SEMUA");
	};

	var lrp;
	var lrp_vendor;
	var FINISHED;
	$(document).ready(function() {
		lrp_vendor = new LRPVendorAction(
			"lrp_vendor",
			"gudang_farmasi",
			"laporan_rekap_pembelian",
			new Array()
		);
		lrp_vendor.setSuperCommand("lrp_vendor");
		lrp = new LRPAction(
			"lrp",
			"gudang_farmasi",
			"laporan_rekap_pembelian",
			new Array()
		);
		$("#lrp_list").append("<tr><td colspan='17'><small><center><strong>DATA BELUM DITAMPILKAN</strong></center></small></td></tr>");
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		})
	});
</script>