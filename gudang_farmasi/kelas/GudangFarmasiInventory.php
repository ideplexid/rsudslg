<?php
	class GudangFarmasiInventory {
		public static function getLastHPP($db, $id_obat, $tanggal_to) {
			$row = $db->get_row("
				SELECT a.id_obat_f_masuk AS 'id', (a.hna / a.konversi) AS 'hpp', a.diskon AS 'diskon_detail', a.t_diskon AS 't_diskon_detail', b.diskon AS 'diskon_global', b.t_diskon AS 't_diskon_global', a.jumlah
				FROM smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_obat_f_masuk b ON a.id_obat_f_masuk = b.id
				WHERE b.tanggal_datang <= '" . $tanggal_to . "' AND a.id_obat = '" . $id_obat . "' AND b.prop NOT LIKE 'del' AND a.prop NOT LIKE 'del' AND a.jumlah > 0
				ORDER BY b.tanggal_datang DESC, a.id DESC
				LIMIT 0, 1
			");
			$hpp_terakhir = 0;
			if ($row != null) {
				$hpp_terakhir = $row->hpp;
				if ($row->t_diskon_detail == "persen") {
					$diskon = $hpp_terakhir * $row->diskon_detail / 100;
					$hpp_terakhir -= $diskon;
				} else if ($row->t_diskon_detail == "nominal") {
					$diskon = $row->diskon_detail / $row->jumlah;
					$hpp_terakhir -= $diskon;
				}
				if ($row->t_diskon_global == "persen") {
					$diskon = $hpp_terakhir * $row->diskon_global / 100;
					$hpp_terakhir -= $diskon;
				} else if ($row->t_diskon_global == "nominal") {
					$total_row = $db->get_row("
						SELECT SUM(jumlah * hna) AS 'total'
						FROM smis_fr_dobat_f_masuk
						WHERE id_obat_f_masuk = '" . $row->id . "' AND prop NOT LIKE 'del'
					");
					$diskon = $row->diskon_global * $row->hpp / $total_row->total;
					$hpp_terakhir -= $diskon;
				}
			}
			return $hpp_terakhir;
		}

		public static function getHPPPenjualan($db, $id_obat, $ppn = 10) {
			$ppn = $ppn / 100;
			$hpp_penjualan = 0;
			$hpp_penjualan_row = $db->get_row("
				SELECT MAX(ROUND(a.hna / ((100 + a.ppn) / 100), 2)) hna
				FROM (smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_fr_obat_f_masuk c ON b.id_obat_f_masuk = c.id
				WHERE b.id_obat = '" . $id_obat . "' AND a.prop = '' AND b.prop = '' AND c.prop = ''
			");
			if ($hpp_penjualan_row != null) {
				$hpp_penjualan = $hpp_penjualan_row->hna;
				$hpp_penjualan = $hpp_penjualan + ($ppn * $hpp_penjualan);
			}
			return $hpp_penjualan;
		}

		public static function getCurrentStock($db, $id_obat, $satuan, $konversi, $satuan_konversi) {
			$dbtable = new DBTable($db, "smis_fr_stok_obat");
			$jumlah = 0;
			$row = $dbtable->get_row("
				SELECT SUM(a.sisa) AS 'jumlah'
				FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getStockValue($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_to) {
			$row = $db->get_row("
				SELECT a.id_obat_f_masuk AS 'id', (a.hna / a.konversi) AS 'hpp', a.diskon AS 'diskon_detail', a.t_diskon AS 't_diskon_detail', b.diskon AS 'diskon_global', b.t_diskon AS 't_diskon_global', a.jumlah
				FROM smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_obat_f_masuk b ON a.id_obat_f_masuk = b.id
				WHERE b.tanggal_datang <= '" . $tanggal_to . "' AND a.id_obat = '" . $id_obat . "' AND b.prop NOT LIKE 'del' AND a.prop NOT LIKE 'del' AND a.jumlah > 0
				ORDER BY b.tanggal_datang DESC, a.id DESC
				LIMIT 0, 1
			");
			$hpp_terakhir = 0;
			if ($row != null) {
				$hpp_terakhir = $row->hpp;
				if ($row->t_diskon_detail == "persen") {
				 	$diskon = $hpp_terakhir * $row->diskon_detail / 100;
				 	$hpp_terakhir -= $diskon;
				} else if ($row->t_diskon_detail == "nominal") {
				 	$diskon = ($row->diskon_detail / $row->konversi) / $row->jumlah;
				 	$hpp_terakhir -= $diskon;
				}
				if ($row->t_diskon_global == "persen") {
				 	$diskon = $hpp_terakhir * $row->diskon_global / 100;
				 	$hpp_terakhir -= $diskon;
				} else if ($row->t_diskon_global == "nominal") {
				 	$total_row = $db->get_row("
				 		SELECT SUM(jumlah * hna) AS 'total'
				 		FROM smis_fr_dobat_f_masuk
				 		WHERE id_obat_f_masuk = '" . $row->id . "' AND prop NOT LIKE 'del'
				 	");
				 	$diskon = $row->diskon_global * $row->hpp / $total_row->total;
				 	$hpp_terakhir -= $diskon;
				}
			}
			return $hpp_terakhir;
		}

		public static function getStockIn($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
			$jumlah = 0;
			// penerimaan faktur:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah * a.konversi) AS 'jumlah'
				FROM (smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_fr_obat_f_masuk c ON b.id_obat_f_masuk = c.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND c.tanggal_datang >= '" . $tanggal_from . "' AND c.tanggal_datang <= '" . $tanggal_to . "' AND b.id_dpo != -1
			");
			$jumlah += $row->jumlah;
			// penerimaan retur unit:
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM smis_fr_retur_obat_unit
				WHERE prop NOT LIKE 'del' AND status = 'sudah' AND restok = '1' AND id_obat = '" . $id_obat . "' AND tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// penyesuaian stok positif:
			// $jumlah += $this->getPenyesuaianStokPositif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to);
			return $jumlah;
		}

		public static function getOrderIn($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
			$jumlah = 0;
			// penerimaan faktur:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah * a.konversi) AS 'jumlah'
				FROM (smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id) LEFT JOIN smis_fr_obat_f_masuk c ON b.id_obat_f_masuk = c.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND c.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND c.tanggal_datang >= '" . $tanggal_from . "' AND c.tanggal_datang <= '" . $tanggal_to . "' AND c.id > 0
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getVendorReturn($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_retur_obat");
			$jumlah = 0;
			// retur vendor (faktur):
			$row = $dbtable->get_row("
				SELECT SUM(c.jumlah) AS 'jumlah'
				FROM ((smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_stok_obat b ON a.id = b.id_dobat_masuk) LEFT JOIN smis_fr_dretur_obat c ON b.id = c.id_stok_obat) LEFT JOIN smis_fr_retur_obat d ON c.id_retur_obat = d.id
				WHERE c.prop NOT LIKE 'del' AND d.prop NOT LIKE 'del' AND d.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND d.tanggal >= '" . $tanggal_from . "' AND d.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// retur vendor (non-faktur):
			$row = $dbtable->get_row("
				SELECT SUM(c.jumlah) AS 'jumlah'
				FROM (smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_stok_obat b ON a.id = b.id_dobat_masuk) LEFT JOIN smis_fr_retur_obat_non_faktur c ON b.id = c.id_stok_obat
				WHERE c.prop NOT LIKE 'del' AND c.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND c.tanggal >= '" . $tanggal_from . "' AND c.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getBMHPOut($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_obat_keluar");
			$jumlah = 0;
			// bmhp keluar:
			$row = $dbtable->get_row("
				SELECT SUM(b.jumlah) AS 'jumlah'
				FROM smis_fr_obat_keluar a LEFT JOIN smis_fr_dobat_keluar b ON a.id = b.id_obat_keluar 
				WHERE a.prop NOT LIKE 'del' AND a.status NOT LIKE 'dikembalikan' AND b.prop NOT LIKE 'del' AND b.id_obat = '" . $id_obat . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND unit NOT LIKE 'depo_farmasi%'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			return $jumlah;
		}

		public static function getBMHPIn($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_retur_obat_unit");
			$jumlah = 0;
			// bmhp keluar:
			$row = $dbtable->get_row("
				SELECT SUM(jumlah) AS 'jumlah'
				FROM smis_fr_retur_obat_unit
				WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '" . $konversi . "' AND satuan_konversi = '" . $satuan_konversi . "' AND tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "' AND unit NOT LIKE 'depo_farmasi%'
			");
			if ($row != null)
				$jumlah += $row->jumlah;
			return $jumlah;
		}		

		public static function getStockOut($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
			$jumlah = 0;
			// mutasi obat:
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah) AS 'jumlah'
				FROM smis_fr_dobat_keluar a LEFT JOIN smis_fr_obat_keluar b ON a.id_obat_keluar = b.id
				WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.status NOT LIKE 'dikembalikan' AND a.id_obat = '" . $id_obat . "' AND b.tanggal >= '" . $tanggal_from . "' AND b.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// retur vendor (faktur):
			$row = $dbtable->get_row("
				SELECT SUM(c.jumlah) AS 'jumlah'
				FROM ((smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_stok_obat b ON a.id = b.id_dobat_masuk) LEFT JOIN smis_fr_dretur_obat c ON b.id = c.id_stok_obat) LEFT JOIN smis_fr_retur_obat d ON c.id_retur_obat = d.id
				WHERE c.prop NOT LIKE 'del' AND d.prop NOT LIKE 'del' AND d.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND d.tanggal >= '" . $tanggal_from . "' AND d.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// retur vendor (non-faktur):
			$row = $dbtable->get_row("
				SELECT SUM(c.jumlah) AS 'jumlah'
				FROM (smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_stok_obat b ON a.id = b.id_dobat_masuk) LEFT JOIN smis_fr_retur_obat_non_faktur c ON b.id = c.id_stok_obat
				WHERE c.prop NOT LIKE 'del' AND c.dibatalkan = '0' AND a.id_obat = '" . $id_obat . "' AND c.tanggal >= '" . $tanggal_from . "' AND c.tanggal <= '" . $tanggal_to . "'
			");
			$jumlah += $row->jumlah;
			// penyesuaian stok negatif:
			// $jumlah += $this->getPenyesuaianStokNegatif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to);
			return $jumlah;
		}

		public static function getPenyesuaianStokPositif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_penyesuaian_stok");
			$row = $dbtable->get_row("
				SELECT SUM(a.jumlah_baru - a.jumlah_lama) AS 'jumlah'
			 	FROM (smis_fr_penyesuaian_stok a LEFT JOIN smis_fr_stok_obat b ON a.id_stok_obat = b.id) LEFT JOIN smis_fr_dobat_f_masuk c ON b.id_dobat_masuk = c.id
			 	WHERE a.prop NOT LIKE 'del' AND c.id_obat = '" . $id_obat . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru >= a.jumlah_lama
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			return $jumlah;
		}

		public static function getPenyesuaianStokNegatif($db, $id_obat, $satuan, $konversi, $satuan_konversi, $tanggal_from, $tanggal_to) {
			$dbtable = new DBTable($db, "smis_fr_penyesuaian_stok");
			$row = $dbtable->get_row("
			 	SELECT SUM(a.jumlah_lama - a.jumlah_baru) AS 'jumlah'
			 	FROM (smis_fr_penyesuaian_stok a LEFT JOIN smis_fr_stok_obat b ON a.id_stok_obat = b.id) LEFT JOIN smis_fr_dobat_f_masuk c ON b.id_dobat_masuk = c.id
			 	WHERE a.prop NOT LIKE 'del' AND c.id_obat = '" . $id_obat . "' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah_baru < a.jumlah_lama
			");
			$jumlah = 0;
			if ($row != null)
				$jumlah = $row->jumlah;
			return $jumlah;
		}
	}
?>