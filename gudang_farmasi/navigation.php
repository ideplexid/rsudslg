<?php 
	global $NAVIGATOR;
	$gudang_farmasi_menu = new Menu("fa fa-medkit");
	$gudang_farmasi_menu->addProperty('title', "Gudang Farmasi");
	$gudang_farmasi_menu->addProperty('name', "Gudang Farmasi");
	$gudang_farmasi_menu->addSubMenu("Penerimaan Obat (BBM - OPL)", "gudang_farmasi", "penerimaan_obat", "Data Penerimaan Obat", "fa fa-folder-o");
	$gudang_farmasi_menu->addSeparator();
	$gudang_farmasi_menu->addSubMenu("Detail Stok Obat", "gudang_farmasi", "penyimpanan_obat", "Data Detail Stok Obat","fa fa-suitcase");
	$gudang_farmasi_menu->addSubMenu("Daftar Obat", "gudang_farmasi", "daftar_obat", "Data Obat","fa fa-tablet");
	$gudang_farmasi_menu->addSubMenu("Kartu Gudang Induk", "gudang_farmasi", "kartu_stok", "Data Obat","fa fa-calendar");
	$gudang_farmasi_menu->addSubMenu("Riwayat Stok Obat", "gudang_farmasi", "riwayat_stok_obat", "Data Riwayat Stok Obat","fa fa-history");
	$gudang_farmasi_menu->addSubMenu("Pengecekan Stok ED", "gudang_farmasi", "pengecekan_stok_ed", "Pengecekan Stok ED","fa fa-trash");
	$gudang_farmasi_menu->addSubMenu("Pengecekan Stok Minimum Unit", "gudang_farmasi", "pengecekan_stok_minimum_unit", "Data Stok Obat Minimumum Unit","fa fa-search-minus");
	$gudang_farmasi_menu->addSubMenu("Pengecekan Stok Aktual Unit", "gudang_farmasi", "pengecekan_stok_aktual_unit", "Data Stok Obat Aktual Unit","fa fa-binoculars");
	$gudang_farmasi_menu->addSeparator();
	$gudang_farmasi_menu->addSubMenu("Penggunaan Obat", "gudang_farmasi", "penggunaan_obat", "Data Penggunaan Obat","fa fa-minus-square");
	$gudang_farmasi_menu->addSubMenu("Penyesuaian Stok Obat", "gudang_farmasi", "penyesuaian_stok_obat", "Penyesuaian Stok Obat","fa fa-refresh");
	$gudang_farmasi_menu->addSubMenu("Permintaan Obat Unit", "gudang_farmasi", "permintaan_obat_unit", "Data Permintaan Obat Unit","fa fa-tag");
	$gudang_farmasi_menu->addSubMenu("Obat Keluar - Unit", "gudang_farmasi", "obat_keluar", "Data Obat Keluar Unit","fa fa-random");
	$gudang_farmasi_menu->addSubMenu("Obat Keluar - Permintaan Obat Unit", "gudang_farmasi", "obat_keluar_dari_permintaan", "Data Obat Keluar dari Permintaan Obat Unit","fa fa-random");
	$gudang_farmasi_menu->addSubMenu("Kode Ruangan Mutasi", "gudang_farmasi", "kode_mutasi_ruangan", "Data Kode Ruangan Mutasi","fa fa-barcode");
	$gudang_farmasi_menu->addSubMenu("Retur Obat Unit", "gudang_farmasi", "retur_obat_unit", "Data Retur Obat Unit","fa fa-recycle");
	$gudang_farmasi_menu->addSubMenu("Retur Pembelian Obat", "gudang_farmasi", "retur_keluar", "Data Retur Pembelian Obat","fa fa-recycle");
	$gudang_farmasi_menu->addSubMenu("Retur Pembelian (Non-Faktur)", "gudang_farmasi", "retur_keluar_non_faktur", "Data Retur Pembelian Obat (Non-Faktur)","fa fa-recycle");
	$gudang_farmasi_menu->addSubMenu("Pemusnahan Obat", "gudang_farmasi", "pemusnahan_obat", "Data Pemusnahan Obat","fa fa-chain-broken");
    $gudang_farmasi_menu->addSeparator();
	$gudang_farmasi_menu->addSubMenu("Rencana Kebutuhan Barang", "gudang_farmasi", "rkbu", "Data Rencana Kebutuhan Barang Unit", "fa fa-book");
	$gudang_farmasi_menu->addSubMenu("Barang dan Inventaris", "gudang_farmasi", "boi", "Barang dan Inventaris", "fa fa-cubes");
	$gudang_farmasi_menu->addSeparator();
	$gudang_farmasi_menu->addSubMenu("Laporan", "gudang_farmasi", "laporan", "Laporan" ,"fa fa-book");
	$gudang_farmasi_menu->addSeparator();
	$gudang_farmasi_menu->addSubMenu("Stock Opname", "gudang_farmasi", "stock_opname", "Stock Opname", "fa fa-check");
	$gudang_farmasi_menu->addSubMenu("Perbaikan Harga", "gudang_farmasi", "perbaikan_harga", "Perbaikan Harga" ,"fa fa-list");
	$gudang_farmasi_menu->addSubMenu("Broadcast Tarif ALOK", "gudang_farmasi", "broadcast_obat", "Broadcast Tarif ALOK" ,"fa fa-book");
	$gudang_farmasi_menu->addSubMenu("Settings", "gudang_farmasi", "settings", "Settings" ,"fa fa-gear");
    $gudang_farmasi_menu->addSubMenu("View Stok Obat Minimum", "gudang_farmasi", "view_stok_obat_minimum", "View Stok Obat Minimum" ,"fa fa-book");
    $gudang_farmasi_menu->addSubMenu("Pengaturan Stok Minimum", "gudang_farmasi", "pengaturan_stok_minimum", "Pengaturan Stok Minimum" ,"fa fa-unsorted");
	$NAVIGATOR->addMenu($gudang_farmasi_menu, "gudang_farmasi");
?>