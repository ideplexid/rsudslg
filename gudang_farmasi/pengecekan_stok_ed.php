<?php 
	$pengecekan_stok_obat_ed_form = new Form("psoed_form", "", "Pengecekan Stok ED");
	$tanggal_text = new Text("psoed_tanggal", "psoed_tanggal", date('Y-m-d'));
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$pengecekan_stok_obat_ed_form->addElement("Tgl. Sekarang", $tanggal_text);
	$rentang_option = new OptionBuilder();
	$rentang_option->add("7 hari", "7");
	$rentang_option->add("30 hari", "30");
	$rentang_option->add("60 hari", "60");
	$rentang_option->add("90 hari", "90");
	$rentang_option->add("180 hari", "180", "1");
	$rentang_select = new Select("psoed_rentang", "psoed_rentang", $rentang_option->getContent());
	$pengecekan_stok_obat_ed_form->addElement("Rentang", $rentang_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("psoed.view()");
	$pengecekan_stok_obat_ed_form->addElement("", $show_button);
	
	$psoed_table = new Table(
		array("Nomor", "Nama Obat", "Jenis Obat", "Produsen", "Vendor", "No. Faktur", "Tgl. Faktur", "Jumlah", "Ket. Jumlah", "Tgl. Exp."),
		"",
		null,
		true
	);
	$psoed_table->setName("psoed");
	$psoed_table->setAddButtonEnable(false);
	$psoed_table->setReloadButtonEnable(false);
	$psoed_table->setPrintButtonEnable(false);
	$psoed_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class PSOEDAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['No. Faktur'] = $row->no_faktur;
				$array['Tgl. Faktur'] = self::format("date d M Y", $row->tanggal);
				$array['Jumlah'] = $row->sisa . " " . $row->satuan;
				$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				return $array;
			}
		}
		$psoed_adapter = new PSOEDAdapter();
		$psoed_dbtable = new DBTable(
			$db,
			"smis_fr_stok_obat"
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_fr_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.produsen LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT smis_fr_stok_obat.*, smis_fr_obat_f_masuk.no_faktur, smis_fr_obat_f_masuk.tanggal, smis_fr_obat_f_masuk.tanggal_datang, smis_fr_obat_f_masuk.tipe
			FROM (smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id) LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
			WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_stok_obat.tanggal_exp <> '0000-00-00' AND DATEDIFF(smis_fr_stok_obat.tanggal_exp, '" . $_POST['tanggal'] . "') <= " . $_POST['rentang'] . " AND smis_fr_stok_obat.sisa > 0 " . $filter . "
			ORDER BY smis_fr_stok_obat.tanggal_exp, smis_fr_stok_obat.nama_obat ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (" . $query_value . ") v
		";
		$psoed_dbtable->setPreferredQuery(true, $query_value, $query_count);
		$psoed_dbresponder = new DBResponder(
			$psoed_dbtable,
			$psoed_table,
			$psoed_adapter
		);
		$data = $psoed_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $pengecekan_stok_obat_ed_form->getHtml();
	echo "<div id='table_content'>";
	echo $psoed_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function PSOEDAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PSOEDAction.prototype.constructor = PSOEDAction;
	PSOEDAction.prototype = new TableAction();
	PSOEDAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal'] = $("#psoed_tanggal").val();
		data['rentang'] = $("#psoed_rentang").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	
	var psoed;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		psoed = new PSOEDAction(
			"psoed",
			"gudang_farmasi",
			"pengecekan_stok_ed",
			new Array()
		);
		psoed.view();
	});
</script>