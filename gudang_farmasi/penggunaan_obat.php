<?php 
	global $db;
	class PenggunaanObatTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Lihat");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			$btn = new Button("", "", "Batal");
			$btn->setAction($this->action . ".del('" . $id . "')");
			$btn->setClass("btn-danger");
			$btn->setAtribute("data-content='Batal' data-toggle='popover'");
			$btn->setIcon("icon-remove icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$penggunaan_obat_table = new PenggunaanObatTable(
		array("Nomor", "Tanggal", "Obat", "Jumlah", "Satuan"),
		"Gudang Farmasi : Penggunaan Obat",
		null,
		true
	);
	$penggunaan_obat_table->setName("penggunaan_obat");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "penggunaan_obat") {
		if (isset($_POST['command'])) {
			$penggunaan_obat_adapter = new SimpleAdapter();
			$penggunaan_obat_adapter->add("Nomor", "id", "digit8");
			$penggunaan_obat_adapter->add("Tanggal", "tanggal", "date d M Y");
			$penggunaan_obat_adapter->add("Obat", "nama_obat");
			$penggunaan_obat_adapter->add("Jumlah", "jumlah");
			$penggunaan_obat_adapter->add("Satuan", "satuan");
			$penggunaan_obat_dbtable = new DBTable($db, "smis_fr_penggunaan_obat");
			$filter = "";
			if (isset($_POST['kriteria'])) {
				$filter = " AND (nama_obat LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_fr_penggunaan_obat.*, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.satuan
					FROM smis_fr_penggunaan_obat LEFT JOIN smis_fr_stok_obat ON smis_fr_penggunaan_obat.id_stok_obat = smis_fr_stok_obat.id
					WHERE smis_fr_penggunaan_obat.prop NOT LIKE 'del'
				) v_penggunaan
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_fr_penggunaan_obat.*, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.satuan
					FROM smis_fr_penggunaan_obat LEFT JOIN smis_fr_stok_obat ON smis_fr_penggunaan_obat.id_stok_obat = smis_fr_stok_obat.id
					WHERE smis_fr_penggunaan_obat.prop NOT LIKE 'del'
				) v_penggunaan
			";
			$penggunaan_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
			class PenggunaanObatDBResponder extends DBResponder {
				public function save() {
					$data = $this->postToArray();
					// do insert here:
					$result = $this->dbtable->insert($data);
					$id['id'] = $this->dbtable->get_inserted_id();
					$success['type'] = "insert";
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_obat");
					$stok_obat_row = $stok_obat_dbtable->select($data['id_stok_obat']);
					$stok_data = array();
					$stok_data['sisa'] = $stok_obat_row->sisa - $data['jumlah'];
					$stok_id['id'] = $data['id_stok_obat'];
					$stok_obat_dbtable->update($stok_data, $stok_id);
					// logging riwayat stok:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_riwayat_stok_obat");
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $data['id_stok_obat'];
					$data_riwayat['jumlah_keluar'] = $data['jumlah'];
					$data_riwayat['sisa'] = $stok_obat_row->sisa - $data['jumlah'];
					$keterangan = $data['keterangan'];
					if ($keterangan == "")
						$keterangan = "-";
					$data_riwayat['keterangan'] = "Stok Digunakan: " . $keterangan;
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
					$success['id'] = $id['id'];
					$success['success'] = 1;
					if ($result === false) $success['success'] = 0;
					return $success;
				}
				public function edit() {
					$id = $_POST['id'];
					$data = $this->dbtable->get_row("
								SELECT smis_fr_penggunaan_obat.*, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.tanggal_exp
								FROM smis_fr_penggunaan_obat LEFT JOIN smis_fr_stok_obat ON smis_fr_penggunaan_obat.id_stok_obat = smis_fr_stok_obat.id
								WHERE smis_fr_penggunaan_obat.prop NOT LIKE 'del' AND smis_fr_penggunaan_obat.id = '" . $id . "'
							");
					return $data;
				}
				public function delete() {
					$id['id'] = $_POST['id'];
					if ($this->dbtable->isRealDelete()) {
						$result = $this->dbtable->delete(null,$id);
					} else {
						$data['prop'] = "del";
						$result = $this->dbtable->update($data, $id);
					}
					$penggunaan_obat_row = $this->dbtable->select($id['id']);
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_obat");
					$stok_obat_row = $stok_obat_dbtable->select($penggunaan_obat_row->id_stok_obat);
					$stok_data = array();
					$stok_data['sisa'] = $stok_obat_row->sisa + $penggunaan_obat_row->jumlah;
					$stok_id['id'] = $penggunaan_obat_row->id_stok_obat;
					$stok_obat_dbtable->update($stok_data, $stok_id);
					//logging riwayat stok:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_riwayat_stok_obat");
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $penggunaan_obat_row->id_stok_obat;
					$data_riwayat['jumlah_masuk'] = $penggunaan_obat_row->jumlah;
					$data_riwayat['sisa'] = $stok_obat_row->sisa + $penggunaan_obat_row->jumlah;
					$data_riwayat['keterangan'] = "Stok Batal Digunakan";
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
					$success['success'] = 1;
					$success['id'] = $_POST['id'];
					if ($result === 'false') $success['success'] = 0;
					return $success;
				}
			}
			$penggunaan_obat_dbresponder = new PenggunaanObatDBResponder(
				$penggunaan_obat_dbtable,
				$penggunaan_obat_table,
				$penggunaan_obat_adapter
			);
			$data = $penggunaan_obat_dbresponder->command($_POST['command']);
			echo json_encode($data);
			return;
		}
		return;
	}
	
	// stok obat chooser:
	$obat_table = new Table(array("No. Stok", "Nama Obat", "Jenis Obat", "Produsen", "Sisa", "Satuan", "Tgl. Exp."));
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter();
	$obat_adapter->add("No. Stok", "id", "digit8");
	$obat_adapter->add("Nama Obat", "nama_obat");
	$obat_adapter->add("Jenis Obat", "nama_jenis_obat");
	$obat_adapter->add("Produsen", "produsen");
	$obat_adapter->add("Sisa", "sisa");
	$obat_adapter->add("Satuan", "satuan");
	$obat_adapter->add("Tgl. Exp.", "tanggal_exp", "date d-m-Y");
	$columns = array("id", "nama_obat", "nama_jenis_obat", "sisa", "satuan", "produsen", "tanggal_exp");
	$obat_dbtable = new DBTable(
		$db,
		"smis_fr_stok_obat",
		$columns
	);
	$obat_dbtable->addCustomKriteria(" sisa ", " > 0 ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$penggunaan_obat_modal = new Modal("penggunaan_obat_add_form", "smis_form_container", "penggunaan_obat");
	$penggunaan_obat_modal->setTitle("Penggunaan Obat");
	$id_hidden = new Hidden("penggunaan_obat_id", "penggunaan_obat_id", "");
	$penggunaan_obat_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("penggunaan_obat_tanggal", "penggunaan_obat_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$penggunaan_obat_modal->addElement("Tanggal", $tanggal_text);
	$id_stok_obat_hidden = new Hidden("penggunaan_obat_id_stok_obat", "penggunaan_obat_id_stok_obat", "");
	$penggunaan_obat_modal->addElement("", $id_stok_obat_hidden);
	$sisa_hidden = new Hidden("penggunaan_obat_sisa", "penggunaan_obat_sisa", "");
	$penggunaan_obat_modal->addElement("", $sisa_hidden);
	$obat_button = new Button("", "", "Pilih");
	$obat_button->setClass("btn-info");
	$obat_button->setIsButton(Button::$ICONIC);
	$obat_button->setIcon("icon-white ".Button::$icon_list_alt);
	$obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$obat_button->setAtribute("id='obat_browse'");
	$nama_obat_text = new Text("penggunaan_obat_nama_obat", "penggunaan_obat_nama_obat", "");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$obat_input_group = new InputGroup("");
	$obat_input_group->addComponent($nama_obat_text);
	$obat_input_group->addComponent($obat_button);
	$penggunaan_obat_modal->addElement("Obat", $obat_input_group);
	$stok_text = new Text("penggunaan_obat_stok", "penggunaan_obat_stok", "");
	$stok_text->setAtribute("disabled='disabled'");
	$penggunaan_obat_modal->addElement("Stok", $stok_text);
	$jumlah_text = new Text("penggunaan_obat_jumlah", "penggunaan_obat_jumlah", "");
	$penggunaan_obat_modal->addElement("Jumlah", $jumlah_text);
	$satuan_text = new Text("penggunaan_obat_satuan", "penggunaan_obat_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$penggunaan_obat_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("penggunaan_obat_keterangan", "penggunaan_obat_keterangan", "");
	$penggunaan_obat_modal->addElement("Keterangan", $keterangan_textarea);
	$penggunaan_obat_button = new Button("", "", "Simpan");
	$penggunaan_obat_button->setClass("btn-success");
	$penggunaan_obat_button->setIsButton(Button::$ICONIC);
	$penggunaan_obat_button->setIcon("icon-white ".Button::$icon_list_alt);
	$penggunaan_obat_button->setAction("penggunaan_obat.save()");
	$penggunaan_obat_button->setAtribute("id='penggunaan_obat_save'");
	$penggunaan_obat_modal->addFooter($penggunaan_obat_button);
	$penggunaan_obat_button = new Button("", "", "OK");
	$penggunaan_obat_button->setClass("btn-success");
	$penggunaan_obat_button->setAction("$($(this).data('target')).smodal('hide')");
	$penggunaan_obat_button->setAtribute("id='penggunaan_obat_ok'");
	$penggunaan_obat_modal->addFooter($penggunaan_obat_button);
	
	echo $penggunaan_obat_modal->getHtml();
	echo $penggunaan_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");	
?>
<script type="text/javascript">
	function PenggunaanObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PenggunaanObatAction.prototype.constructor = PenggunaanObatAction;
	PenggunaanObatAction.prototype = new TableAction();
	PenggunaanObatAction.prototype.show_add_form = function() {
		var today = new Date();		
		$("#penggunaan_obat_id").val("");
		$("#penggunaan_obat_tanggal").val(today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate());
		$("#penggunaan_obat_tanggal").removeAttr("disabled");
		$("#penggunaan_obat_id_stok_obat").val("");
		$("#penggunaan_obat_nama_obat").val("");
		$("#penggunaan_obat_nama_obat").removeAttr("disabled");
		$("#penggunaan_obat_nama_obat").attr("disabled", "disabled");
		$("#obat_browse").removeAttr("onclick");
		$("#obat_browse").attr("onclick", "obat.chooser('obat','obat','obat',obat)");
		$("#obat_browse").removeClass("btn-info");
		$("#obat_browse").removeClass("btn-inverse");
		$("#obat_browse").addClass("btn-info");
		$("#penggunaan_obat_stok").val("");
		$(".penggunaan_obat_stok").show();
		$("#penggunaan_obat_jumlah").val("");
		$("#penggunaan_obat_jumlah").removeAttr("disabled");
		$("#penggunaan_obat_satuan").val("");
		$("#penggunaan_obat_keterangan").val("");
		$("#penggunaan_obat_keterangan").removeAttr("disabled");
		$("#penggunaan_obat_save").show();
		$("#penggunaan_obat_ok").hide();
		$("#penggunaan_obat_add_form").smodal("show");
	};
	PenggunaanObatAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var tanggal = $("#penggunaan_obat_tanggal").val();
		var id_stok_obat = $("#penggunaan_obat_id_stok_obat").val();
		var sisa = $("#penggunaan_obat_sisa").val();
		var jumlah = $("#penggunaan_obat_jumlah").val();
		var keterangan = $("#penggunaan_obat_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
			$("#penggunaan_obat_tanggal").addClass("error_field");
		}
		if (id_stok_obat == "") {
			valid = false;
			invalid_msg += "</br><strong>Obat</strong> tidak boleh kosong";
			$("#penggunaan_obat_nama_obat").addClass("error_field");
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#penggunaan_obat_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#penggunaan_obat_jumlah").addClass("error_field");
		} else if (sisa != "" && is_numeric(sisa) && parseFloat(jumlah) > parseFloat(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi stok";
			$("#penggunaan_obat_jumlah").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#penggunaan_obat_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_penggunaan_obat_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	PenggunaanObatAction.prototype.save = function() {
		if (!this.validate())
			return;
		$("#penggunaan_obat_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();				
		data['command'] = "save";
		data['id'] = $("#penggunaan_obat_id").val();
		data['id_stok_obat'] = $("#penggunaan_obat_id_stok_obat").val();
		data['tanggal'] = $("#penggunaan_obat_tanggal").val();
		data['jumlah'] = $("#penggunaan_obat_jumlah").val();
		data['keterangan'] = $("#penggunaan_obat_keterangan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#penggunaan_obat_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	PenggunaanObatAction.prototype.detail = function(id) {
		var data = this.getRegulerData();				
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#penggunaan_obat_id").val("");
				$("#penggunaan_obat_tanggal").val(json.tanggal);
				$("#penggunaan_obat_tanggal").removeAttr("disabled");
				$("#penggunaan_obat_tanggal").attr("disabled", "disabled");
				$("#penggunaan_obat_id_stok_obat").val(json.id_stok_obat);
				$("#penggunaan_obat_nama_obat").val(json.nama_obat);
				$("#penggunaan_obat_nama_obat").removeAttr("disabled");
				$("#penggunaan_obat_nama_obat").attr("disabled", "disabled");
				$("#obat_browse").removeAttr("onclick");
				$("#obat_browse").removeClass("btn-info");
				$("#obat_browse").removeClass("btn-inverse");
				$("#obat_browse").addClass("btn-inverse");
				$("#penggunaan_obat_sisa").val(json.sisa);
				$("#penggunaan_obat_stok").val(json.sisa + " " + json.satuan);
				$(".penggunaan_obat_stok").hide();
				$("#penggunaan_obat_jumlah").val(json.jumlah);
				$("#penggunaan_obat_jumlah").removeAttr("disabled");
				$("#penggunaan_obat_jumlah").attr("disabled", "disabled");
				$("#penggunaan_obat_satuan").val(json.satuan);
				$("#penggunaan_obat_keterangan").val(json.keterangan);
				$("#penggunaan_obat_keterangan").attr("disabled", "disabled");
				$("#modal_alert_penggunaan_obat_add_form").html();
				$(".error_field").removeClass("error_field");
				$("#penggunaan_obat_save").hide();
				$("#penggunaan_obat_ok").show();
				$("#penggunaan_obat_add_form").smodal("show");
			}
		);
	};
	
	function ObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ObatAction.prototype.constructor = ObatAction;
	ObatAction.prototype = new TableAction();
	ObatAction.prototype.selected = function(json) {
		$("#penggunaan_obat_id_stok_obat").val(json.id);
		$("#penggunaan_obat_nama_obat").val(json.nama_obat);
		$("#penggunaan_obat_stok").val(json.sisa + " " + json.satuan);
		$("#penggunaan_obat_sisa").val(json.sisa);
		$("#penggunaan_obat_satuan").val(json.satuan);
	};
	
	var penggunaan_obat;
	var obat;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$(".mydate").datepicker();
		$("#smis-chooser-modal").on("show", function() {
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("full_model");
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("full_model");
		});
		obat = new ObatAction(
			"obat",
			"gudang_farmasi",
			"penggunaan_obat",
			new Array()
		);
		obat.setSuperCommand("obat");
		var penggunaan_obat_columns = new Array("id", "id_stok_obat", "tanggal", "jumlah", "keterangan");
		penggunaan_obat = new PenggunaanObatAction(
			"penggunaan_obat",
			"gudang_farmasi",
			"penggunaan_obat",
			penggunaan_obat_columns
		);
		penggunaan_obat.setSuperCommand("penggunaan_obat");
		penggunaan_obat.view();
	});
</script>