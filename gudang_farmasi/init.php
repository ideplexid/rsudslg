<?php
	global $PLUGINS;
	
	$init['name'] = "gudang_farmasi";
	$init['path'] = SMIS_DIR . $init['name'] . "/";
	$init['description'] = "Gudang Farmasi";
	$init['require'] = "administrator";
	$init['service'] = "";
	$init['version'] = "1.1.1";
	$init['number'] = "5";
	$init['type'] = "";
	
	$plugin = new Plugin($init);
	$PLUGINS[$init['name']] = $plugin;
?>