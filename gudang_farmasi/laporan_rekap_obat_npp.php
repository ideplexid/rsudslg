<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$laporan_form = new Form("", "", "Gudang Farmasi : Laporan Rekap Obat Narkotika - Psikotropika - Prekursor Farmasi");
	$tanggal_from_text = new Text("lrnpp_tanggal_from", "lrnpp_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lrnpp_tanggal_to", "lrnpp_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$jenis_filter_option = new OptionBuilder();
	$jenis_filter_option->add("NARKOTIKA", "narkotika", "1");
	$jenis_filter_option->add("PSIKOTROPIKA", "psikotropika");
	$jenis_filter_option->add("PREKURSOR FARMASI", "prekursor_farmasi");
	$jenis_filter_select = new Select("lrnpp_jenis_filter", "lrnpp_jenis_filter", $jenis_filter_option->getContent());
	$laporan_form->addElement("Jenis Filter", $jenis_filter_select);
	$urutan_option = new OptionBuilder();
	$urutan_option->addSingle("KODE OBAT", "1");
	$urutan_option->addSingle("NAMA OBAT");
	$urutan_select = new Select("lrnpp_urutan", "lrnpp_urutan", $urutan_option->getContent());
	$laporan_form->addElement("Urutan", $urutan_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lrnpp.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='npp_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$lrnpp_table = new Table(
		array(
			"No.", "ID Obat", "Kode Obat", "Nama Obat", "Satuan", 
			"Stok Awal", "Nilai Stok Awal",
			"Jml. Beli", "Total Beli",
			"Jml. Jual", "Total Jual",
			"Stok Akhir", "Nilai Stok Akhir"),
		"",
		null,
		true
	);
	$lrnpp_table->setName("lrnpp");
	$lrnpp_table->setAction(false);
	$lrnpp_table->setFooterVisible(false);
	$lrnpp_table->setHeaderVisible(false);
	$lrnpp_table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>ID Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Kode Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Nama Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Satuan</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Stok Awal</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Pembelian</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Penjualan</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Stok Akhir</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Nilai</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Nilai</small></center>
			</th>
		</tr>
	");

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah_obat") {
			$jenis_filter = $_POST['jenis_filter'];
			$params = array();
			$params['order_by'] = $_POST['urutan'];
			$service = "";
			if ($jenis_filter == "narkotika")
				$service = "get_jumlah_obat_narkotika";
			else if ($jenis_filter == "psikotropika")
				$service = "get_jumlah_obat_psikotropika";
			else if ($jenis_filter == "prekursor_farmasi")
				$service = "get_jumlah_obat_prekursor";
			$consumer_service = new ServiceConsumer(
				$db,
				$service,
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah = 0;
			if ($content != null)
				$jumlah = $content[0];
			$data = array();
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$num = $_POST['num'];
			$params = array();
			$params['num'] = $_POST['num'];
			$params['order_by'] = $_POST['urutan'];
			$service = "";
			if ($jenis_filter == "narkotika")
				$service = "get_obat_info_narkotika";
			else if ($jenis_filter == "psikotropika")
				$service = "get_obat_info_psikotropika";
			else if ($jenis_filter == "prekursor_farmasi")
				$service = "get_obat_info_prekursor";
			$consumer_service = new ServiceConsumer(
				$db,
				$service,
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$id_obat = $content[0];
			$kode_obat = $content[1];
			$nama_obat = $content[2];
			$nama_jenis_obat = $content[3];
			$satuan = $content[4];
			$satuan_konversi = $content[5];

			$stok_awal = 0;
			$stok_akhir = 0;

			$params = array();
			$params['id_obat'] = $id_obat;
			$params['satuan'] = $satuan;
			$params['konversi'] = 1;
			$params['satuan_konversi'] = $satuan_konversi;
			$params['tanggal_from'] = date("Y-m-d", strtotime($tanggal_to . " +1 day"));
			$params['tanggal_to'] = date("Y-m-d");			
			/// a. Data Gudang Farmasi
			/// a.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_gudang = $content[0];
			/// a.2. Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_ordered_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_beli = $content[0];
			/// a.3. Retur Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_vendor_return",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_retur_beli = $content[0];
			/// a.4. Jumlah Mutasi BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_out",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_keluar = $content[0];
			/// a.5. Jumlah Retur BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_masuk = $content[0];

			/// b. Data Depo Farmasi IGD
			/// b.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_igd = $content[0];
			/// b.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_igd = $content[0];

			/// c. Data Depo Farmasi IRJA
			/// c.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irja = $content[0];
			/// c.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irja = $content[0];

			/// d. Data Depo Farmasi IRNA I
			/// d.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irna = $content[0];
			/// d.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna = $content[0];

			/// e. Data Depo Farmasi IRNA II
			/// e.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irna_ii = $content[0];
			/// e.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_ii = $content[0];

			/// f. Data Depo Farmasi IRNA III
			/// f.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irna_iii = $content[0];
			/// f.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_iii = $content[0];

			$stok_sekarang = $stok_skr_gudang + $stok_skr_depo_igd + $stok_skr_depo_irja + $stok_skr_depo_irna + $stok_skr_depo_irna_ii + $stok_skr_depo_irna_iii;
			$jumlah_masuk = $jumlah_beli - $jumlah_retur_beli;
			$jumlah_keluar = $jumlah_bmhp_keluar - $jumlah_bmhp_masuk + $penjualan_depo_igd + $penjualan_depo_irja + $penjualan_depo_irna + $penjualan_depo_irna_ii + $penjualan_depo_irna_iii;
			$stok_akhir = $stok_sekarang - $jumlah_masuk + $jumlah_keluar;

			$params['tanggal_from'] = $tanggal_from;
			$params['tanggal_to'] = $tanggal_to;
			/// a. Data Gudang Farmasi
			/// a.1. Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_ordered_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_beli = $content[0];
			/// a.2. Retur Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_vendor_return",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_retur_beli = $content[0];
			/// a.3. Jumlah Mutasi BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_out",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_keluar = $content[0];
			/// a.4. Jumlah Retur BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_masuk = $content[0];

			/// b. Data Depo Farmasi IGD
			/// b.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_igd = $content[0];

			/// c. Data Depo Farmasi IRJA
			/// c.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irja = $content[0];

			/// d. Data Depo Farmasi IRNA I
			/// d.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna = $content[0];

			/// e. Data Depo Farmasi IRNA II
			/// e.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_ii = $content[0];

			/// f. Data Depo Farmasi IRNA III
			/// f.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_iii = $content[0];

			$jumlah_pembelian = $jumlah_beli - $jumlah_retur_beli;
			$jumlah_penjualan = $jumlah_bmhp_keluar - $jumlah_bmhp_masuk + $penjualan_depo_igd + $penjualan_depo_irja + $penjualan_depo_irna + $penjualan_depo_irna_ii + $penjualan_depo_irna_iii;
			$stok_awal = $stok_akhir - $jumlah_pembelian + $jumlah_penjualan;
			
			/// g. Nominal Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_purchasing_cost",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_pembelian = $content[0];

			$total_penjualan = 0;
			/// h. Nominal Penjualan Depo IGD :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// i. Nominal Penjualan Depo IRJA :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// j. Nominal Penjualan Depo IRNA I :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// h. Nominal Penjualan Depo IRNA II :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// h. Nominal Penjualan Depo IRNA III :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// i. Nominal Penjualan Gudang Farmasi :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// j. Nilai Persediaan Item :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_last_hpp_until",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$nilai_persediaan = round($content[0] / 1.1, 2);
			$nilai_persediaan_awal = $stok_awal * $nilai_persediaan;
			$nilai_persediaan_akhir = $stok_akhir * $nilai_persediaan;

			$html = "
				<tr>
					<td id='nomor'><small>" . ($num + 1) . "</small></td>
					<td id='id_obat'><small>" .  $id_obat . "</small></td>
					<td id='kode_obat'><small>" .  $kode_obat . "</small></td>
					<td id='nama_obat'><small>" .  $nama_obat . "</small></td>
					<td id='satuan'><small>" .  $satuan . "</small></td>
					<td id='stok_awal' style='display: none;'>" . $stok_awal . "</td>
					<td id='f_stok_awal'><small>" . ArrayAdapter::format("number", $stok_awal) . "</small></td>
					<td id='nilai_stok_awal' style='display: none;'>" . $nilai_persediaan_awal . "</td>
					<td id='f_nilai_stok_awal'><small>" . ArrayAdapter::format("money", $nilai_persediaan_awal) . "</small></td>
					<td id='jumlah_pembelian' style='display: none;'>" . $jumlah_pembelian . "</td>
					<td id='f_jumlah_pembelian'><small>" . ArrayAdapter::format("number", $jumlah_pembelian) . "</small></td>
					<td id='total_pembelian' style='display: none;'>" . $total_pembelian . "</td>
					<td id='f_total_pembelian'><small>" . ArrayAdapter::format("money", $total_pembelian) . "</small></td>
					<td id='jumlah_penjualan' style='display: none;'>" . $jumlah_penjualan . "</td>
					<td id='f_jumlah_penjualan'><small>" . ArrayAdapter::format("number", $jumlah_penjualan) . "</small></td>
					<td id='total_penjualan' style='display: none;'>" . $total_penjualan . "</td>
					<td id='f_total_penjualan'><small>" . ArrayAdapter::format("money", $total_penjualan) . "</small></td>
					<td id='stok_akhir' style='display: none;'>" . $stok_akhir . "</td>
					<td id='f_stok_akhir'><small>" . ArrayAdapter::format("number", $stok_akhir) . "</small></td>
					<td id='nilai_stok_akhir' style='display: none;'>" . $nilai_persediaan_akhir . "</td>
					<td id='f_nilai_stok_akhir'><small>" . ArrayAdapter::format("money", $nilai_persediaan_akhir) . "</small></td>
				</tr>

			";

			$data = array();
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['kode_obat'] = $kode_obat;
			$data['nama_obat'] = $nama_obat;
			$data['nama_jenis_obat'] = $nama_jenis_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/template_rekap_obat_npp.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP OBAT NPP");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B1", "DATA OBAT " . ArrayAdapter::format("unslug", $_POST['jenis_filter']));
			$objWorksheet->setCellValue("B2", ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("G9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("I9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("K9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("M9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("H9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("J9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("L9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("N9")->getNumberFormat()->setFormatCode("#,##0.00");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_awal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nilai_stok_awal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_pembelian);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total_pembelian);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_penjualan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total_penjualan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_akhir);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nilai_stok_akhir);
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("M" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("L" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("N" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=REKAP_OBAT_NPP_" . strtoupper($_POST['jenis_filter']) . "_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}
	$loading_bar = new LoadingBar("npp_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lrnpp.cancel()");
	$loading_modal = new Modal("npp_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lrnpp_table->getHtml();
	echo "</div>";
	echo "<div id='npp_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("base-js/smis-base-loading.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LRNPPAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRNPPAction.prototype.constructor = LRNPPAction;
	LRNPPAction.prototype = new TableAction();
	LRNPPAction.prototype.view = function() {
		if ($("#lrnpp_tanggal_from").val() == "" || $("#lrnpp_tanggal_to").val() == "")
			return;
		var self = this;
		$("#npp_info").empty();
		$("#npp_loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#npp_loading_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_jumlah_obat";
		data['jenis_filter'] = $("#lrnpp_jenis_filter").val();
		data['urutan'] = $("#lrnpp_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#lrnpp_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	LRNPPAction.prototype.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize();
			} else {
				$("#npp_loading_modal").smodal("hide");
				$("#npp_info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#npp_export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info_obat";
		data['num'] = num;
		data['tanggal_from'] = $("#lrnpp_tanggal_from").val();
		data['tanggal_to'] = $("#lrnpp_tanggal_to").val();
		data['jenis_filter'] = $("#lrnpp_jenis_filter").val();
		data['urutan'] = $("#lrnpp_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("tbody#lrnpp_list").append(
					json.html
				);
				$("#npp_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	LRNPPAction.prototype.finalize = function() {
		var num_rows = $("tbody#lrnpp_list tr").length;
		for (var i = 0; i < num_rows; i++)
			$("tbody#lrnpp_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
		$("#npp_loading_modal").smodal("hide");
		$("#npp_info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#npp_export_button").removeAttr("onclick");
		$("#npp_export_button").attr("onclick", "lrnpp.export_xls()");
	};
	LRNPPAction.prototype.cancel = function() {
		FINISHED = true;
	};
	LRNPPAction.prototype.export_xls = function() {
		showLoading();
		var num_rows = $("#lrnpp_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var nomor = $("tbody#lrnpp_list tr:eq(" + i + ") td#nomor").text();
			var id_obat = $("tbody#lrnpp_list tr:eq(" + i + ") td#id_obat").text();
			var kode_obat = $("tbody#lrnpp_list tr:eq(" + i + ") td#kode_obat").text();
			var nama_obat = $("tbody#lrnpp_list tr:eq(" + i + ") td#nama_obat").text();
			var satuan = $("tbody#lrnpp_list tr:eq(" + i + ") td#satuan").text();
			var stok_awal = parseFloat($("tbody#lrnpp_list tr:eq(" + i + ") td#stok_awal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var nilai_stok_awal = parseFloat($("tbody#lrnpp_list tr:eq(" + i + ") td#nilai_stok_awal").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			var jumlah_pembelian = $("tbody#lrnpp_list tr:eq(" + i + ") td#jumlah_pembelian").text();
			var total_pembelian = $("tbody#lrnpp_list tr:eq(" + i + ") td#total_pembelian").text();
			var jumlah_penjualan = $("tbody#lrnpp_list tr:eq(" + i + ") td#jumlah_penjualan").text();
			var total_penjualan = $("tbody#lrnpp_list tr:eq(" + i + ") td#total_penjualan").text();
			var stok_akhir = $("tbody#lrnpp_list tr:eq(" + i + ") td#stok_akhir").text();
			var nilai_stok_akhir = parseFloat($("tbody#lrnpp_list tr:eq(" + i + ") td#nilai_stok_akhir").text().replace(/[^0-9-,]/g, '').replace(",", "."));
			d_data[i] = {
				"nomor" 			: nomor,
				"id_obat" 			: id_obat,
				"kode_obat" 		: kode_obat,
				"nama_obat" 		: nama_obat,
				"satuan" 			: satuan,
				"stok_awal"			: stok_awal,
				"nilai_stok_awal"	: nilai_stok_awal,
				"jumlah_pembelian"	: jumlah_pembelian,
				"total_pembelian"	: total_pembelian,
				"jumlah_penjualan"	: jumlah_penjualan,
				"total_penjualan"	: total_penjualan,
				"stok_akhir"		: stok_akhir,
				"nilai_stok_akhir"	: nilai_stok_akhir
			};
		}
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tanggal_from'] = $("#lrnpp_tanggal_from").val();
		data['tanggal_to'] = $("#lrnpp_tanggal_to").val();
		data['jenis_filter'] = $("#lrnpp_jenis_filter").val();
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		postForm(data);
		dismissLoading();
	};

	var lrnpp;
	var FINISHED;
	$(document).ready(function() {
		lrnpp = new LRNPPAction(
			"lrnpp",
			"gudang_farmasi",
			"laporan_rekap_obat_npp",
			new Array()
		);
		$("#loading_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#loading_modal").on("hide", function() {
			$("a.close").show();
		});
		$("tbody#lrnpp_list").append(
			"<tr>" +
				"<td colspan='13'><strong><center><small>DATA OBAT NPP BELUM DIPROSES</small></center></strong></td>" +
			"</tr>"
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		})
	});
</script>