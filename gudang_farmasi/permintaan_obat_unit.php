<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	
	class PermintaanObatTable extends Table {
		public function getBodyContent(){
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['status'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $status) {
			$btn_group = new ButtonGroup("noprint");
			if ($status != "sudah") {
				$btn = new Button("", "", "Ubah");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("icon-edit icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} else {
				$btn = new Button("", "", "Lihat");
				$btn->setAction($this->action . ".detail('" . $id . "')");
				$btn->setClass("btn-success");
				$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
				$btn->setIcon("icon-eye-open icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			} 
			$btn = new Button("", "", "Cetak");
			$btn->setAction($this->action . ".print_permintaan('" . $id . "')");
			$btn->setClass("btn-inverse");
			$btn->setAtribute("data-content='Cetak' data-toggle='popover'");
			$btn->setIcon("fa fa-print");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$permintaan_obat_table = new PermintaanObatTable(
		array("Unit", "Nomor", "Tanggal", "Status"),
		"Gudang Farmasi : Permintaan Obat Unit",
		null,
		true
	);
	$permintaan_obat_table->setName("permintaan_obat");
	$permintaan_obat_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class PermintaanObatAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['status'] = $row->status;
				$array['Unit'] = self::format("unslug", $row->unit);
				$array['Nomor'] = self::format("digit8", $row->f_id);
				$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
				if ($row->status == "belum") {
					$array['Status'] = "Belum Diterima";
				} else if ($row->status == "sudah") {
					$array['Status'] = "Sudah Diterima";
				}
				return $array;
			}
		}
		$permintaan_obat_adapter = new PermintaanObatAdapter();
		$columns = array("id", "f_id", "unit", "tanggal", "status");
		$permintaan_obat_dbtable = new DBTable(
			$db,
			"smis_fr_permintaan_obat_unit",
			$columns
		);
		$permintaan_obat_dbtable->addCustomKriteria(" status ", " !='dikembalikan' ");
		class PermintaanObatDBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_permintaan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_permintaan") {
					$pack->setContent($this->print_permintaan());
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_permintaan() {
				$nama_entitas = getSettings($this->dbtable->get_db(), "smis_autonomous_title", "");
				$alamat_entitas = getSettings($this->dbtable->get_db(), "smis_autonomous_address", "");
				$id = $_POST['id'];
				$header_data = $this->dbtable->select($id);
				$detail_data = $this->dbtable->get_result("
					SELECT *
					FROM smis_fr_dpermintaan_obat_unit
					WHERE prop NOT LIKE 'del' AND id_permintaan_obat = '" . $id . "'
				");
				$print_data = "<b>" . ArrayAdapter::format("unslug", $nama_entitas) . "</b><br/>
							   " . $alamat_entitas . "<br/>
							   <br/><br/><br/>
							   <center><b>SURAT PERMINTAAN OBAT</b></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Tanggal</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $header_data->tanggal) . "</td>
									</tr>
									<tr>
										<td>Kepada</td>
										<td>:</td>
										<td>GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>Unit/Bag. yang memohon</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $header_data->unit) . "</td>
									</tr>
								</table>";
				$print_data .= "Mohon diberikan sejumlah obat sebagai berikut:<br/>";
				$print_data .= "<table border='1'>";
				$print_data .= "<tr>
									<th>No.</th>
									<th>Nama Obat</th>
									<th>Jumlah Permintaan</th>
									<th>Jumlah Diberikan</th>
									<th>Keterangan</th>
								</tr>";
				$no = 1;
				foreach ($detail_data as $d) {
					$dipenuhi = $d->jumlah_dipenuhi . " " . $d->satuan_dipenuhi;
					if ($d->dipenuhi == 0 && $d->satuan_dipenuhi == "-")
						$dipenuhi = "-";
					$print_data .= "<tr>
										<td>" . $no++ . "</td>
										<td>" . $d->nama_obat . "</td>
										<td>" . $d->jumlah_permintaan . " " . $d->satuan_permintaan . "</td>
										<td>" . $dipenuhi . "</td>
										<td>" . $d->keterangan . "</td>
									</tr>";
				}
				$print_data .= "</table><br/>";
				$print_data .= "<table border 1>
									<tr>
										<th>Pemohon</th>
										<th>Diketahui<br/>Kepala Seksi</th>
										<th>Diketahui<br/>Kepala Bagian</th>
										<th>Bagian<br/>Gudang Farmasi</th>
									</tr>
									<tr>
										<td>&Tab;&Tab;&Tab;&Tab;<br/><br/><br/><br/><br/></td>
										<td>&Tab;&Tab;&Tab;&Tab;</td>
										<td>&Tab;&Tab;&Tab;&Tab;</td>
										<td>&Tab;&Tab;&Tab;&Tab;</td>
									</tr>
								</table>";
				return $print_data;$print_data = "<b>" . ArrayAdapter::format("unslug", $nama_entitas) . "</b><br/>
							   " . $alamat_entitas . "<br/>
							   <br/><br/><br/>
							   <center><b>SURAT PERMINTAAN OBAT</b></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Tanggal</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $header_data->tanggal) . "</td>
									</tr>
									<tr>
										<td>Kepada</td>
										<td>:</td>
										<td>GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>Unit/Bag. yang memohon</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("unslug", $this->unit) . "</td>
									</tr>
								</table>";
				$print_data .= "Mohon diberikan sejumlah obat sebagai berikut:<br/>";
				$print_data .= "<table border='1'>";
				$print_data .= "<tr>
									<th>No.</th>
									<th>Nama Obat</th>
									<th>Jumlah Permintaan</th>
									<th>Jumlah Diberikan</th>
									<th>Keterangan</th>
								</tr>";
				$no = 1;
				foreach ($detail_data as $d) {
					$dipenuhi = $d->jumlah_dipenuhi . " " . $d->satuan_dipenuhi;
					if ($d->dipenuhi == 0 && $d->satuan_dipenuhi == "-")
						$dipenuhi = "-";
					$print_data .= "<tr>
										<td>" . $no++ . "</td>
										<td>" . $d->nama_obat . "</td>
										<td>" . $d->jumlah_permintaan . " " . $d->satuan_permintaan . "</td>
										<td>" . $dipenuhi . "</td>
										<td>" . $d->keterangan . "</td>
									</tr>";
				}
				$print_data .= "</table><br/>";
				$print_data .= "<table border 1>
									<tr>
										<th>Pemohon</th>
										<th>Diketahui<br/>Kepala Seksi</th>
										<th>Diketahui<br/>Kepala Bagian</th>
										<th>Bagian<br/>Gudang Farmasi</th>
									</tr>
									<tr>
										<td>&Tab;&Tab;&Tab;&Tab;<br/><br/><br/><br/><br/></td>
										<td>&Tab;&Tab;&Tab;&Tab;</td>
										<td>&Tab;&Tab;&Tab;&Tab;</td>
										<td>&Tab;&Tab;&Tab;&Tab;</td>
									</tr>
								</table>";
				return $print_data;
			}
			public function save() {
				$header_data = $this->postToArray();
				$id['id'] = $_POST['id'];
				//do update here:
				$result = $this->dbtable->update($header_data, $id);
				$success['type'] = "update";
				if (isset($_POST['detail'])) {
					//do update detail here:
					$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_dpermintaan_obat_unit");
					$detail = $_POST['detail'];
					foreach($detail as $d) {
						$detail_data = array();
						$detail_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
						$detail_data['satuan_dipenuhi'] = $d['satuan_dipenuhi'];
						$detail_data['keterangan'] = $d['keterangan'];
						$detail_id['id'] = $d['id'];
						$detail_dbtable->update($detail_data, $detail_id);
					}
				}
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
			public function edit() {
				$id = $_POST['id'];
				$header_row = $this->dbtable->get_row("
					SELECT *
					FROM smis_fr_permintaan_obat_unit
					WHERE id = '" . $id . "'
				");
				$data['header'] = $header_row;
				$detail_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_dpermintaan_obat_unit");
				$data['detail'] = $detail_dbtable->get_result("
					SELECT *
					FROM smis_fr_dpermintaan_obat_unit
					WHERE id_permintaan_obat = '" . $id . "' AND prop NOT LIKE 'del'
				");
				return $data;
			}
		}
		$permintaan_obat_dbresponder = new PermintaanObatDBResponder(
			$permintaan_obat_dbtable,
			$permintaan_obat_table,
			$permintaan_obat_adapter
		);
		$data = $permintaan_obat_dbresponder->command($_POST['command']);
		if (isset($_POST['push_command'])) {
			class SetPermintaanObatStatusServiceConsumer extends ServiceConsumer {
				public function __construct($db, $f_id, $unit, $status, $detail, $command) {
					$array['id'] = $f_id;
					$array['status'] = $status;
					$array['detail'] = json_encode($detail);
					$array['command'] = $command;
					parent::__construct($db, "set_permintaan_obat_status", $array, $unit);
				}
				public function proceedResult() {
					$content = array();
					$result = json_decode($this->result,true);
					foreach ($result as $autonomous) {
						foreach ($autonomous as $entity) {
							if ($entity != null || $entity != "")
								return $entity;
						}
					}
					return $content;
				}
			}
			$permintaan_obat_dbtable = new DBTable($db, "smis_fr_permintaan_obat_unit");
			$permintaan_obat_row = $permintaan_obat_dbtable->get_row("	
				SELECT *
				FROM smis_fr_permintaan_obat_unit
				WHERE id = '" . $data['content']['id'] . "'
			");
			$dpermintaan_obat_dbtable = new DBTable($db, "smis_ap_dpermintaan_obat");
			$detail = $dpermintaan_obat_dbtable->get_result("
				SELECT *
				FROM smis_fr_dpermintaan_obat_unit
				WHERE id_permintaan_obat = '" . $data['content']['id'] . "' AND prop NOT LIKE 'del'
			");
			$command = "push_" . $_POST['push_command'];
			$set_status_permintaan_obat_service_consumer = new SetPermintaanObatStatusServiceConsumer($db, $permintaan_obat_row->f_id, $permintaan_obat_row->unit, $permintaan_obat_row->status, $detail, $command);
			$set_status_permintaan_obat_service_consumer->execute();
		}
		echo json_encode($data);
		return;
	}
	
	$permintaan_obat_modal = new Modal("permintaan_obat_add_form", "smis_form_container", "permintaan_obat");
	$permintaan_obat_modal->setTitle("Permintaan Obat");
	$permintaan_obat_modal->setClass(Modal::$FULL_MODEL);
	$id_hidden = new Hidden("permintaan_obat_id", "permintaan_obat_id", "");
	$permintaan_obat_modal->addElement("", $id_hidden);
	$unit_text = new Text("permintaan_obat_unit", "permintaan_obat_unit", "");
	$unit_text->setAtribute("disabled='disabled'");
	$permintaan_obat_modal->addElement("Unit", $unit_text);
	$tanggal_text = new Text("permintaan_obat_tanggal", "permintaan_obat_tanggal", "");
	$tanggal_text->setAtribute("disabled='disabled'");
	$permintaan_obat_modal->addElement("Tanggal", $tanggal_text);
	$status_option = array(
		array(
			'name'	=> "Belum Diterima",
			'value'	=> "belum"
		),
		array(
			'name'	=> "Diterima",
			'value'	=> "sudah"
		),
		array(
			'name'	=> "Dikembalikan",
			'value'	=> "dikembalikan"
		)
	);
	$status_select = new Select("permintaan_obat_status", "permintaan_obat_status", $status_option);
	$permintaan_obat_modal->addElement("Status", $status_select);
	$dpermintaan_obat_table = new Table(
		array("Obat", "Jml. Permintaan", "Jml. Dipenuhi", "Keterangan"),
		"",
		null,
		true
	);
	$dpermintaan_obat_table->setName("dpermintaan_obat");
	$dpermintaan_obat_table->setAddButtonEnable(false);
	$dpermintaan_obat_table->setReloadButtonEnable(false);
	$dpermintaan_obat_table->setPrintButtonEnable(false);
	$dpermintaan_obat_table->setFooterVisible(false);
	$permintaan_obat_modal->addBody("dpermintaan_obat_table", $dpermintaan_obat_table);
	$permintaan_obat_button = new Button("", "", "Simpan");
	$permintaan_obat_button->setClass("btn-success");
	$permintaan_obat_button->setAtribute("id='permintaan_obat_save'");
	$permintaan_obat_button->setIcon("fa fa-floppy-o");
	$permintaan_obat_button->setIsButton(Button::$ICONIC);
	$permintaan_obat_modal->addFooter($permintaan_obat_button);
	$permintaan_obat_button = new Button("", "", "OK");
	$permintaan_obat_button->setClass("btn-success");
	$permintaan_obat_button->setAtribute("id='permintaan_obat_ok'");
	$permintaan_obat_button->setAction("$($(this).data('target')).smodal('hide')");
	$permintaan_obat_modal->addFooter($permintaan_obat_button);
	
	$dpermintaan_obat_modal = new Modal("dpermintaan_obat_add_form", "smis_form_container", "dpermintaan_obat");
	$dpermintaan_obat_modal->setTitle("Detail Permintaan Obat");
	$id_hidden =  new Hidden("dpermintaan_obat_id", "dpermintaan_obat_id", "");
	$dpermintaan_obat_modal->addElement("", $id_hidden);
	$nama_obat_text = new Text("dpermintaan_obat_nama_obat", "dpermintaan_obat_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$dpermintaan_obat_modal->addElement("Obat", $nama_obat_text);
	$f_jumlah_diminta_text = new Text("dpermintaan_obat_f_jumlah_diminta", "dpermintaan_obat_f_jumlah_diminta", "");
	$f_jumlah_diminta_text->setAtribute("disabled='disabled'");
	$dpermintaan_obat_modal->addElement("Jm. Diminta", $f_jumlah_diminta_text);
	$jumlah_dipenuhi_text = new Text("dpermintaan_obat_jumlah_dipenuhi", "dpermintaan_obat_jumlah_dipenuhi", "");
	$dpermintaan_obat_modal->addElement("Jm. Dipenuhi", $jumlah_dipenuhi_text);
	$satuan_dipenuhi_text = new Text("dpermintaan_obat_satuan_dipenuhi", "dpermintaan_obat_satuan_dipenuhi", "");
	$dpermintaan_obat_modal->addElement("Satuan", $satuan_dipenuhi_text);
	$keterangan_textarea = new TextArea("dpermintaan_obat_keterangan", "dpermintaan_obat_keterangan", "");
	$dpermintaan_obat_modal->addElement("Keterangan", $keterangan_textarea);
	$dpermintaan_obat_button = new Button("", "", "Simpan");
	$dpermintaan_obat_button->setClass("btn-success");
	$dpermintaan_obat_button->setAtribute("id='dpermintaan_obat_save'");
	$dpermintaan_obat_button->setIcon("fa fa-floppy-o");
	$dpermintaan_obat_button->setIsButton(Button::$ICONIC);
	$dpermintaan_obat_modal->addFooter($dpermintaan_obat_button);
	
	echo $dpermintaan_obat_modal->getHtml();
	echo $permintaan_obat_modal->getHtml();
	echo $permintaan_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function PermintaanObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PermintaanObatAction.prototype.constructor = PermintaanObatAction;
	PermintaanObatAction.prototype = new TableAction();
	PermintaanObatAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#permintaan_obat_id").val(json.header.id);
				$("#permintaan_obat_unit").val(json.header.unit.replace("_", " ").toUpperCase());
				$("#permintaan_obat_tanggal").val(json.header.tanggal);
				$("#permintaan_obat_status").val(json.header.status);
				$("#permintaan_obat_status").removeAttr("disabled");
				row_id = 0;
				$("tbody#dpermintaan_obat_list").children("tr").remove();
				$("#modal_alert_permintaan_obat_add_form").html("");
				$(".error_field").removeClass("error_field");
				for(var i = 0; i < json.detail.length; i++) {
					var d_id = json.detail[i].id;
					var d_id_permintaan_obat = json.detail[i].id_permintaan_obat;
					var d_nama_obat = json.detail[i].nama_obat;
					var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
					var d_satuan_diminta = json.detail[i].satuan_permintaan;
					var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
					var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
					var d_keterangan = json.detail[i].keterangan;
					var f_jumlah_dipenuhi = "-";
					if (Number(d_jumlah_dipenuhi) != 0) {
						f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
					}
					$("tbody#dpermintaan_obat_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td id='data_" + row_id + "_id' style='display: none;'>" + d_id + "</td>" +
							"<td id='data_" + row_id + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
							"<td id='data_" + row_id + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_nama_obat'>" + d_nama_obat + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_keterangan'>" + d_keterangan + "</td>" +
							"<td>" +
								"<div class='btn-group noprint'>" +
									"<a href='#' onclick='dpermintaan_obat.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
										"<i class='icon-edit icon-white'></i>" +
									 "</a>" +
								"</div>" +
							"</td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#permintaan_obat_save").removeAttr("onclick");
				$("#permintaan_obat_save").attr("onclick", "permintaan_obat.update(" + id + ")");
				$("#permintaan_obat_save").show();
				$("#permintaan_obat_ok").hide();
				$("#modal_alert_permintaan_obat_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#permintaan_obat_add_form").smodal("show");
			}
		);
	};
	PermintaanObatAction.prototype.update = function(id) {
		$("#permintaan_obat_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['push_command'] = "update";
		data['id'] = $("#permintaan_obat_id").val();
		data['status'] = $("#permintaan_obat_status").val();
		var detail = {};
		var nor = $("tbody#dpermintaan_obat_list").children("tr").length;
		for(var i = 0; i < nor; i++) {
			var d_data = {};
			var prefix = $("tbody#dpermintaan_obat_list").children('tr').eq(i).prop("id");
			d_data['id'] = $("#" + prefix + "_id").text();
			d_data['jumlah_dipenuhi'] = $("#" + prefix + "_jumlah_dipenuhi").text();
			d_data['satuan_dipenuhi'] = $("#" + prefix + "_satuan_dipenuhi").text();
			d_data['keterangan'] = $("#" + prefix + "_keterangan").text();
			detail[i] = d_data;
		}
		data['detail'] = detail;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#permintaan_obat_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	PermintaanObatAction.prototype.print_permintaan = function(id) {
		var data = this.getRegulerData();
		data['command'] = "print_permintaan";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null)
					return;
				smis_print(json);
			}
		);
	};
	PermintaanObatAction.prototype.detail = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#permintaan_obat_id").val(json.header.id);
				$("#permintaan_obat_unit").val(json.header.unit.replace("_", " ").toUpperCase());
				$("#permintaan_obat_tanggal").val(json.header.tanggal);
				$("#permintaan_obat_status").val(json.header.status);
				$("#permintaan_obat_status").removeAttr("disabled");
				$("#permintaan_obat_status").attr("disabled", "disabled");
				row_id = 0;
				$("tbody#dpermintaan_obat_list").children("tr").remove();
				$("#modal_alert_permintaan_obat_add_form").html("");
				$(".error_field").removeClass("error_field");
				for(var i = 0; i < json.detail.length; i++) {
					var d_id = json.detail[i].id;
					var d_id_permintaan_obat = json.detail[i].id_permintaan_obat;
					var d_nama_obat = json.detail[i].nama_obat;
					var d_jumlah_diminta = json.detail[i].jumlah_permintaan;
					var d_satuan_diminta = json.detail[i].satuan_permintaan;
					var d_jumlah_dipenuhi = json.detail[i].jumlah_dipenuhi;
					var d_satuan_dipenuhi = json.detail[i].satuan_dipenuhi;
					var d_keterangan = json.detail[i].keterangan;
					var f_jumlah_dipenuhi = "-";
					if (Number(d_jumlah_dipenuhi) != 0) {
						f_jumlah_dipenuhi = d_jumlah_dipenuhi + " " + d_satuan_dipenuhi;
					}
					$("tbody#dpermintaan_obat_list").append(
						"<tr id='data_" + row_id + "'>" +
							"<td id='data_" + row_id + "_id' style='display: none;'>" + d_id + "</td>" +
							"<td id='data_" + row_id + "_jumlah_diminta' style='display: none;'>" + d_jumlah_diminta + "</td>" +
							"<td id='data_" + row_id + "_satuan_diminta' style='display: none;'>" + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_jumlah_dipenuhi' style='display: none;'>" + d_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_satuan_dipenuhi' style='display: none;'>" + d_satuan_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_nama_obat'>" + d_nama_obat + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_diminta'>" + d_jumlah_diminta + " " + d_satuan_diminta + "</td>" +
							"<td id='data_" + row_id + "_f_jumlah_dipenuhi'>" + f_jumlah_dipenuhi + "</td>" +
							"<td id='data_" + row_id + "_keterangan'>" + d_keterangan + "</td>" +
							"<td></td>" +
						"</tr>"
					);
					row_id++;
				}
				$("#permintaan_obat_save").removeAttr("onclick");
				$("#permintaan_obat_save").hide();
				$("#permintaan_obat_ok").show();
				$("#modal_alert_permintaan_obat_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#permintaan_obat_add_form").smodal("show");
			}
		);
	};
	
	function DPermintaanObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	DPermintaanObatAction.prototype.constructor = DPermintaanObatAction;
	DPermintaanObatAction.prototype = new TableAction();
	DPermintaanObatAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var jumlah_dipenuhi = $("#dpermintaan_obat_jumlah_dipenuhi").val();
		var satuan_dipenuhi = $("#dpermintaan_obat_satuan_dipenuhi").val();
		var keterangan = $("#dpermintaan_obat_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (jumlah_dipenuhi == "") {
			valid = false;
			invalid_msg += "<br/><strong>Jm. Dipenuhi</strong> tidak boleh kosong";
			$("#dpermintaan_obat_jumlah_dipenuhi").addClass("error_field");
		} else if (!is_numeric(jumlah_dipenuhi)) {
			valid = false;
			invalid_msg += "<br/><strong>Jm. Dipenuhi</strong> seharusnya numerik (0-9)";
			$("#dpermintaan_obat_jumlah_dipenuhi").addClass("error_field");
		}
		if (satuan_dipenuhi == "") {
			valid = false;
			invalid_msg += "<br/><strong>Satuan</strong> tidak boleh kosong";
			$("#dpermintaan_obat_satuan_dipenuhi").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "<br/><strong>Keterangan</strong> tidak boleh kosong";
			$("#dpermintaan_obat_satuan_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_dpermintaan_obat_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	DPermintaanObatAction.prototype.edit = function(r_num) {
		var id = $("#data_" + r_num + "_id").text();
		var nama_obat = $("#data_" + r_num + "_nama_obat").text();
		var jumlah_diminta = $("#data_" + r_num + "_jumlah_diminta").text();
		var satuan_diminta = $("#data_" + r_num + "_satuan_diminta").text();
		var jumlah_dipenuhi = $("#data_" + r_num + "_jumlah_dipenuhi").text();
		var satuan_dipenuhi = $("#data_" + r_num + "_satuan_dipenuhi").text();
		var keterangan = $("#data_" + r_num + "_keterangan").text();
		$("#dpermintaan_obat_id").val(id);
		$("#dpermintaan_obat_nama_obat").val(nama_obat);
		$("#dpermintaan_obat_f_jumlah_diminta").val(jumlah_diminta + " " + satuan_diminta);
		$("#dpermintaan_obat_jumlah_dipenuhi").val(jumlah_dipenuhi);
		$("#dpermintaan_obat_satuan_dipenuhi").val(satuan_dipenuhi);
		$("#dpermintaan_obat_keterangan").val(keterangan);
		$("#dpermintaan_obat_save").removeAttr("onclick");
		$("#dpermintaan_obat_save").attr("onclick", "dpermintaan_obat.update(" + r_num + ")");
		$("#dpermintaan_obat_add_form").smodal("show");
	};
	DPermintaanObatAction.prototype.update = function(r_num) {
		if (!this.validate()) {
			return;
		}
		var jumlah_dipenuhi = $("#dpermintaan_obat_jumlah_dipenuhi").val();
		var satuan_dipenuhi = $("#dpermintaan_obat_satuan_dipenuhi").val();
		var keterangan = $("#dpermintaan_obat_keterangan").val();
		$("#data_" + r_num + "_jumlah_dipenuhi").text(jumlah_dipenuhi);
		$("#data_" + r_num + "_satuan_dipenuhi").text(satuan_dipenuhi);
		$("#data_" + r_num + "_f_jumlah_dipenuhi").text(jumlah_dipenuhi + " " + satuan_dipenuhi);
		$("#data_" + r_num + "_keterangan").text(keterangan);
		$("#dpermintaan_obat_add_form").smodal("hide");
	};
	
	var permintaan_obat;
	var dpermintaan_obat;
	var row_id;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		var dpermintaan_obat_columns = new Array("id", "f_id", "nama_obat", "jumlah_permintaan", "satuan_permintaan", "jumlah_dipenuhi", "satuan_dipenuhi", "keterangan");
		dpermintaan_obat = new DPermintaanObatAction(
			"dpermintaan_obat",
			"gudang_farmasi",
			"permintaan_obat_unit",
			dpermintaan_obat_columns
		);
		var permintaan_obat_columns = new Array("id", "f_id", "unit", "tanggal", "status");
		permintaan_obat = new PermintaanObatAction(
			"permintaan_obat",
			"gudang_farmasi",
			"permintaan_obat_unit",
			permintaan_obat_columns
		);
		permintaan_obat.view();
	});
</script>