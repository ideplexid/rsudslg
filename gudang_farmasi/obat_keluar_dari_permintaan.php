<?php
	require_once("gudang_farmasi/responder/MutasiObatKeluarDBResponder.php");
	global $db;

	$table = new Table(
		array("No.", "Tgl. Permintaan", "Tgl. Dipenuhi", "No. Mutasi", "Unit", "Status Mutasi"),
		"Gudang Farmasi : Obat Keluar - Permintaan Unit",
		null,
		true
	);
	$table->setName("obat_keluar_dari_permintaan");
	$table->setPrintButtonEnable(false);
	$table->setEditButtonEnable(false);
	$table->setDelButtonEnable(false);
	$detail_button = new Button("", "", "Detail");
	$detail_button->setClass("btn-success");
	$detail_button->setIsButton(Button::$ICONIC);
	$detail_button->setIcon("fa fa-eye");;
	$table->addContentButton("detail", $detail_button);

	if (isset($_POST['command'])) {
		$dbtable = new DBTable($db, "smis_fr_mutasi_obat_keluar");
		if ($_POST['command'] == "list") {
			$query_value = "
				SELECT a.*, b.nomor, b.status
				FROM smis_fr_mutasi_obat_keluar a INNER JOIN smis_fr_obat_keluar b ON a.f_id_obat_keluar = b.id
				WHERE a.prop = '' AND b.prop = ''
				ORDER BY a.id DESC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$dbtable->setPreferredQuery(true, $query_value, $query_count);
		} else {
			$dbtable->setOrder(" id DESC ");
		}

		$adapter = new SimpleAdapter(true, "No.");
		$adapter->add("Tgl. Permintaan", "tanggal_permintaan", "date d-m-Y");
		$adapter->add("Tgl. Dipenuhi", "tanggal_mutasi", "date d-m-Y");
		$adapter->add("No. Mutasi", "nomor");
		$adapter->add("Unit", "unit", "unslug");
		$adapter->add("Status Mutasi", "status", "unslug");
		$dbresponder = new MutasiObatKeluarDBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("gudang_farmasi/js/obat_keluar_dari_permintaan_action.js", false);
	echo addJS("gudang_farmasi/js/obat_keluar_dari_permintaan.js", false);
?>