<?php
	require_once("smis-base/smis-include-service-consumer.php");
	global $db;

	$laporan_form = new Form("", "", "Gudang Farmasi : Rekap Obat Rumah Sakit - Form. B");
	$tanggal_from_text = new Text("lrobpk_tanggal_from", "lrobpk_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lrobpk_tanggal_to", "lrobpk_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$jenis_filter_option = new OptionBuilder();
	$jenis_filter_option->add("SEMUA", "semua", "1");
	$jenis_filter_option->add("PER OBAT", "per_obat");
	$jenis_filter_option->add("PER JENIS", "per_jenis");
	$jenis_filter_select = new Select("lrobpk_jenis_filter", "lrobpk_jenis_filter", $jenis_filter_option->getContent());
	$laporan_form->addElement("Jenis Filter", $jenis_filter_select);
	$kode_jenis_obat_hidden = new Hidden("lrobpk_kode_jenis_obat", "lrobpk_kode_jenis_obat", "");
	$laporan_form->addElement("", $kode_jenis_obat_hidden);
	$id_obat_hidden = new Hidden("lrobpk_id_obat", "lrobpk_id_obat", "");
	$laporan_form->addElement("", $id_obat_hidden);
	$nama_obat_text = new Text("lrobpk_nama_obat", "lrobpk_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);  
	$browse_button->setAction("lrobpk_obat.chooser('lrobpk_obat', 'lrobpk_obat_button', 'lrobpk_obat', lrobpk_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Obat", $input_group);
	$nama_jenis_obat_text = new Text("lrobpk_nama_jenis_obat", "lrobpk_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$nama_jenis_obat_text->setClass("smis-one-option-input");
	$browse_button = new Button("", "", "Pilih");
	$browse_button->setClass("btn-info");
	$browse_button->setIcon("fa fa-list");
	$browse_button->setIsButton(Button::$ICONIC);
	$browse_button->setAction("lrobpk_jenis_obat.chooser('lrobpk_jenis_obat', 'lrobpk_jenis_obat_button', 'lrobpk_jenis_obat', lrobpk_jenis_obat)");
	$input_group = new InputGroup("");
	$input_group->addComponent($nama_jenis_obat_text);
	$input_group->addComponent($browse_button);
	$laporan_form->addElement("Jenis Obat", $input_group);
	$urutan_option = new OptionBuilder();
	$urutan_option->addSingle("KODE OBAT", "1");
	$urutan_option->addSingle("NAMA OBAT");
	$urutan_select = new Select("lrobpk_urutan", "lrobpk_urutan", $urutan_option->getContent());
	$laporan_form->addElement("Urutan", $urutan_select);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lrobpk.view()");
	$print_button = new Button("", "", "Unduh");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("fa fa-download");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAtribute("id='lrobpk_export_button'");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);

	$lrobpk_table = new Table(
		array(
			"No.", "ID Obat", "Kode Obat", "Nama Obat", "Satuan", 
			"Stok Awal", "Nilai Stok Awal",
			"Jml. Beli", "Total Beli",
			"Jml. Jual", "Total Jual",
			"Stok Akhir", "Nilai Stok Akhir"
		),
		"",
		null,
		true
	);
	$lrobpk_table->setName("lrobpk");
	$lrobpk_table->setAction(false);
	$lrobpk_table->setFooterVisible(false);
	$lrobpk_table->setHeaderVisible(false);
	$lrobpk_table->addHeader("after", "
		<tr class='inverse'>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>No.</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>ID Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Kode Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Nama Obat</small></center>
			</th>
			<th rowspan='2' style='vertical-align: middle !important;'>
				<center><small>Satuan</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Stok Awal</small></center>
			</th>
			<th colspan='3' style='vertical-align: middle !important;'>
				<center><small>Pembelian</small></center>
			</th>
			<th colspan='3' style='vertical-align: middle !important;'>
				<center><small>Penjualan</small></center>
			</th>
			<th colspan='2' style='vertical-align: middle !important;'>
				<center><small>Stok Akhir</small></center>
			</th>
		</tr>
		<tr class='inverse'>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Nilai</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Harga Terakhir</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Harga Terakhir</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Total</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Jumlah</small></center>
			</th>
			<th colspan='1' style='vertical-align: middle !important;'>
				<center><small>Nilai</small></center>
			</th>
		</tr>
	");

	//chooser nama obat:
	$obat_table = new Table(
		array("No.", "ID Obat", "Nama Obat", "Jenis Obat"),
		"",
		null,
		true
	);
	$obat_table->setName("lrobpk_obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter(true, "No.");
	$obat_adapter->add("ID Obat", "id", "digit6");
	$obat_adapter->add("Nama Obat", "nama");
	$obat_adapter->add("Jenis Obat", "nama_jenis_barang");
	$obat_dbtable = new DBTable($db, "smis_pr_barang");
	$obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	//chooser jenis obat:
	$jenis_obat_table = new Table(
		array("No.", "Kode", "Jenis Obat"),
		"",
		null,
		true
	);
	$jenis_obat_table->setName("lrobpk_jenis_obat");
	$jenis_obat_table->setModel(Table::$SELECT);
	$jenis_obat_adapter = new SimpleAdapter(true, "No.");
	$jenis_obat_adapter->add("Kode", "kode");
	$jenis_obat_adapter->add("Jenis Obat", "nama");
	$jenis_obat_dbtable = new DBTable($db, "smis_pr_jenis_barang");
	$jenis_obat_dbtable->addCustomKriteria(" medis ", " = 1 ");
	$jenis_obat_dbresponder = new DBResponder(
		$jenis_obat_dbtable,
		$jenis_obat_table,
		$jenis_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("lrobpk_obat", $obat_dbresponder);
	$super_command->addResponder("lrobpk_jenis_obat", $jenis_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command'])) {
		if ($_POST['command'] == "get_jumlah_obat") {
			$jenis_filter = $_POST['jenis_filter'];
			$params = array();
			$params['order_by'] = $_POST['urutan'];
			if ($jenis_filter == "per_obat")
				$params['filter_id_obat'] = $_POST['id_obat'];
			else
				$params['filter_id_obat'] = "%%";
			if ($jenis_filter == "per_jenis")
				$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
			else
				$params['filter_kode_jenis_obat'] = "%%";
			$consumer_service = new ServiceConsumer(
				$db,
				"get_jumlah_obat_msrs",
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah = 0;
			if ($content != null)
				$jumlah = $content[0];
			$data = array();
			$data['jumlah'] = $jumlah;
			echo json_encode($data);
		} else if ($_POST['command'] == "get_info_obat") {
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			$jenis_filter = $_POST['jenis_filter'];
			$num = $_POST['num'];
			$params = array();
			$params['num'] = $_POST['num'];
			$params['order_by'] = $_POST['urutan'];
			if ($jenis_filter == "per_obat")
				$params['filter_id_obat'] = $_POST['id_obat'];
			else
				$params['filter_id_obat'] = "%%";
			if ($jenis_filter == "per_jenis")
				$params['filter_kode_jenis_obat'] = $_POST['kode_jenis_obat'];
			else
				$params['filter_kode_jenis_obat'] = "%%";
			$consumer_service = new ServiceConsumer(
				$db,
				"get_obat_info_msrs",
				$params,
				"perencanaan"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$id_obat = $content[0];
			$kode_obat = $content[1];
			$nama_obat = $content[2];
			$nama_jenis_obat = $content[3];
			$satuan = $content[4];
			$satuan_konversi = $content[5];

			$stok_awal = 0;
			$stok_akhir = 0;

			$params = array();
			$params['id_obat'] = $id_obat;
			$params['satuan'] = $satuan;
			$params['konversi'] = 1;
			$params['satuan_konversi'] = $satuan_konversi;
			$params['tanggal_from'] = date("Y-m-d", strtotime($tanggal_to . " +1 day"));
			$params['tanggal_to'] = date("Y-m-d");			
			/// a. Data Gudang Farmasi
			/// a.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_gudang = $content[0];
			/// a.2. Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_ordered_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_beli = $content[0];
			/// a.3. Retur Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_vendor_return",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_retur_beli = $content[0];
			/// a.4. Jumlah Mutasi BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_out",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_keluar = $content[0];
			/// a.5. Jumlah Retur BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_masuk = $content[0];

			/// b. Data Depo Farmasi IGD
			/// b.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_igd = $content[0];
			/// b.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_igd = $content[0];

			/// c. Data Depo Farmasi IRJA
			/// c.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irja = $content[0];
			/// c.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irja = $content[0];

			/// d. Data Depo Farmasi IRNA I
			/// d.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irna = $content[0];
			/// d.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna = $content[0];

			/// e. Data Depo Farmasi IRNA II
			/// e.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irna_ii = $content[0];
			/// e.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_ii = $content[0];

			/// f. Data Depo Farmasi IRNA III
			/// f.1. Stok Sekarang :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_current_stock",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$stok_skr_depo_irna_iii = $content[0];
			/// f.2. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_iii = $content[0];

			$stok_sekarang = $stok_skr_gudang + $stok_skr_depo_igd + $stok_skr_depo_irja + $stok_skr_depo_irna + $stok_skr_depo_irna_ii + $stok_skr_depo_irna_iii;
			$jumlah_masuk = $jumlah_beli - $jumlah_retur_beli;
			$jumlah_keluar = $jumlah_bmhp_keluar - $jumlah_bmhp_masuk + $penjualan_depo_igd + $penjualan_depo_irja + $penjualan_depo_irna + $penjualan_depo_irna_ii + $penjualan_depo_irna_iii;
			$stok_akhir = $stok_sekarang - $jumlah_masuk + $jumlah_keluar;

			$params['tanggal_from'] = $tanggal_from;
			$params['tanggal_to'] = $tanggal_to;
			/// a. Data Gudang Farmasi
			/// a.1. Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_ordered_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_beli = $content[0];
			/// a.2. Retur Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_vendor_return",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_retur_beli = $content[0];
			/// a.3. Jumlah Mutasi BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_out",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_keluar = $content[0];
			/// a.4. Jumlah Retur BMHP :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_bmhp_in",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$jumlah_bmhp_masuk = $content[0];

			/// b. Data Depo Farmasi IGD
			/// b.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_igd = $content[0];

			/// c. Data Depo Farmasi IRJA
			/// c.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irja = $content[0];

			/// d. Data Depo Farmasi IRNA I
			/// d.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna = $content[0];

			/// e. Data Depo Farmasi IRNA II
			/// e.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_ii = $content[0];

			/// f. Data Depo Farmasi IRNA III
			/// f.1. Penjualan :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_out",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$penjualan_depo_irna_iii = $content[0];

			$jumlah_pembelian = $jumlah_beli - $jumlah_retur_beli;
			$jumlah_penjualan = $jumlah_bmhp_keluar - $jumlah_bmhp_masuk + $penjualan_depo_igd + $penjualan_depo_irja + $penjualan_depo_irna + $penjualan_depo_irna_ii + $penjualan_depo_irna_iii;
			$stok_awal = $stok_akhir - $jumlah_pembelian + $jumlah_penjualan;
			
			/// g. Nominal Pembelian :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_purchasing_cost",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_pembelian = $content[0];

			$total_penjualan = 0;
			/// h. Nominal Penjualan Depo IGD :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_igd"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// i. Nominal Penjualan Depo IRJA :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irja"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// j. Nominal Penjualan Depo IRNA I :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irna"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// k. Nominal Penjualan Depo IRNA II :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irna_ii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// l. Nominal Penjualan Depo IRNA III :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"depo_farmasi_irna_iii"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// m. Nominal Penjualan Gudang Farmasi :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_sold_income",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$total_penjualan += $content[0];

			/// n. Nilai Persediaan Item :
			$consumer_service = new ServiceConsumer(
				$db,
				"get_last_hpp_until",
				$params,
				"gudang_farmasi"
			);
			$consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
			$content = $consumer_service->execute()->getContent();
			$nilai_persediaan = $content[0];
			$nilai_persediaan_awal = $stok_awal * $nilai_persediaan;
			$nilai_persediaan_akhir = $stok_akhir * $nilai_persediaan;

			/// o. Harga Pembelian Terakhir :
			$harga_pembelian_terakhir = $nilai_persediaan;

			/// p. Harga Penjualan Terakhir :
			$consumer_service = new ServiceConsumer(
		        $db,
		        "get_last_hja_until",
		        $params
		    );
		    $consumer_service->setMode(ServiceConsumer::$CLEAN_BOTH);
		    $content = $consumer_service->execute()->getContent();
		    $harga_jual_terakhir = 0;
		    if ($content != null) {
		    	$name = "tanggal_unix";
			    usort($content, function ($a, $b) use(&$name) {
	      			return $b[$name] - $a[$name];}
	      		);
	      		$harga_jual_terakhir = $content[0]['hja'];
			}
						
			$html = "
				<tr>
					<td id='nomor'><small>" . ($num + 1) . "</small></td>
					<td id='id_obat'><small>" .  $id_obat . "</small></td>
					<td id='kode_obat'><small>" .  $kode_obat . "</small></td>
					<td id='nama_obat'><small>" .  $nama_obat . "</small></td>
					<td id='satuan'><small>" .  $satuan . "</small></td>
					<td id='stok_awal' style='display: none;'>" . $stok_awal . "</td>
					<td id='f_stok_awal'><small>" . ArrayAdapter::format("number", $stok_awal) . "</small></td>
					<td id='nilai_stok_awal' style='display: none;'>" . $nilai_persediaan_awal . "</td>
					<td id='f_nilai_stok_awal'><small>" . ArrayAdapter::format("money", $nilai_persediaan_awal) . "</small></td>
					<td id='jumlah_pembelian' style='display: none;'>" . $jumlah_pembelian . "</td>
					<td id='f_jumlah_pembelian'><small>" . ArrayAdapter::format("number", $jumlah_pembelian) . "</small></td>
					<td id='harga_pembelian_terakhir' style='display: none;'>" . $harga_pembelian_terakhir . "</td>
					<td id='f_harga_pembelian_terakhir'><small>" . ArrayAdapter::format("money", $harga_pembelian_terakhir) . "</small></td>
					<td id='total_pembelian' style='display: none;'>" . $total_pembelian . "</td>
					<td id='f_total_pembelian'><small>" . ArrayAdapter::format("money", $total_pembelian) . "</small></td>
					<td id='jumlah_penjualan' style='display: none;'>" . $jumlah_penjualan . "</td>
					<td id='f_jumlah_penjualan'><small>" . ArrayAdapter::format("number", $jumlah_penjualan) . "</small></td>
					<td id='harga_penjualan_terakhir' style='display: none;'>" . $harga_jual_terakhir . "</td>
					<td id='f_harga_penjualan_terakhir'><small>" . ArrayAdapter::format("money", $harga_jual_terakhir) . "</small></td>
					<td id='total_penjualan' style='display: none;'>" . $total_penjualan . "</td>
					<td id='f_total_penjualan'><small>" . ArrayAdapter::format("money", $total_penjualan) . "</small></td>
					<td id='stok_akhir' style='display: none;'>" . $stok_akhir . "</td>
					<td id='f_stok_akhir'><small>" . ArrayAdapter::format("number", $stok_akhir) . "</small></td>
					<td id='nilai_stok_akhir' style='display: none;'>" . $nilai_persediaan_akhir . "</td>
					<td id='f_nilai_stok_akhir'><small>" . ArrayAdapter::format("money", $nilai_persediaan_akhir) . "</small></td>
				</tr>
			";

			$data = array();
			$data['id_obat'] = ArrayAdapter::format("only-digit6", $id_obat);
			$data['kode_obat'] = $kode_obat;
			$data['nama_obat'] = $nama_obat;
			$data['nama_jenis_obat'] = $nama_jenis_obat;
			$data['html'] = $html;
			echo json_encode($data);
		} else if ($_POST['command'] == "export_xls") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			set_time_limit(0);
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/template_rekap_obat_b.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP OBAT RS");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B2", ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("B3", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$data = json_decode($_POST['d_data']);
			$objWorksheet->getStyle("G9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("I9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("K9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("M9")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("H9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("J9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("L9")->getNumberFormat()->setFormatCode("#,##0.00");
			$objWorksheet->getStyle("N9")->getNumberFormat()->setFormatCode("#,##0.00");
			if ($_POST['num_rows'] - 2 > 0)
				$objWorksheet->insertNewRowBefore(8, $_POST['num_rows'] - 2);
			$start_row_num = 7;
			$end_row_num = 7;
			$row_num = $start_row_num;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->id_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_awal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nilai_stok_awal);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_pembelian);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga_beli_terakhir);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total_pembelian);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah_penjualan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->harga_jual_terakhir);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->total_penjualan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->stok_akhir);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nilai_stok_akhir);
				$objWorksheet->getStyle("G" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("I" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("L" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("O" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("M" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("N" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$objWorksheet->getStyle("P" . $row_num)->getNumberFormat()->setFormatCode("#,##0.00");
				$row_num++;
				$end_row_num++;
				$no++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=REKAP_OBAT_RS_FORM_B_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
		}
		return;
	}

	$loading_bar = new LoadingBar("lrobpk_loading_bar", "");
	$button = new Button("", "", "Batal");
	$button->addClass("btn-primary");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$button->setIcon("fa fa-close");
	$button->setAction("lrobpk.cancel()");
	$loading_modal = new Modal("lrobpk_loading_modal", "", "Proses..");
	$loading_modal->addHtml($loading_bar->getHtml(), "after");
	$loading_modal->addFooter($button);
	
	echo $loading_modal->getHtml();
	echo $laporan_form->getHtml();
	echo $lrobpk_table->getHtml();
	echo "<div id='lrobpk_info'></div>";
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	function LRORSAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRORSAction.prototype.constructor = LRORSAction;
	LRORSAction.prototype = new TableAction();
	LRORSAction.prototype.view = function() {
		if ($("#lrobpk_tanggal_from").val() == "" || $("#lrobpk_tanggal_to").val() == "")
			return;
		var self = this;
		$("#lrobpk_info").empty();
		$("#lrobpk_loading_bar").sload("true", "Harap ditunggu...", 0);
		$("#lrobpk_loading_modal").smodal("show");
		FINISHED = false;
		var data = this.getRegulerData();
		data['command'] = "get_jumlah_obat";
		data['jenis_filter'] = $("#lrobpk_jenis_filter").val();
		data['id_obat'] = $("#lrobpk_id_obat").val();
		data['nama_obat'] = $("#lrobpk_nama_obat").val();
		data['nama_jenis_obat'] = $("#lrobpk_nama_jenis_obat").val();
		data['kode_jenis_obat'] = $("#lrobpk_kode_jenis_obat").val();
		data['urutan'] = $("#lrobpk_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("#lrobpk_list").empty();
				self.fillHtml(0, json.jumlah);
			}
		);
	};
	LRORSAction.prototype.fillHtml = function(num, limit) {
		if (FINISHED || num == limit) {
			if (FINISHED == false && num == limit) {
				this.finalize();
			} else {
				$("#lrobpk_loading_modal").smodal("hide");
				$("#lrobpk_info").html(
					"<div class='alert alert-block alert-inverse'>" +
						 "<center><strong>PROSES DIBATALKAN</strong></center>" +
					 "</div>"
				);
				$("#lrobpk_export_button").removeAttr("onclick");
			}
			return;
		}
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "get_info_obat";
		data['num'] = num;
		data['tanggal_from'] = $("#lrobpk_tanggal_from").val();
		data['tanggal_to'] = $("#lrobpk_tanggal_to").val();
		data['jenis_filter'] = $("#lrobpk_jenis_filter").val();
		data['id_obat'] = $("#lrobpk_id_obat").val();
		data['nama_obat'] = $("#lrobpk_nama_obat").val();
		data['nama_jenis_obat'] = $("#lrobpk_nama_jenis_obat").val();
		data['kode_jenis_obat'] = $("#lrobpk_kode_jenis_obat").val();
		data['urutan'] = $("#lrobpk_urutan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = JSON.parse(response);
				if (json == null) return;
				$("tbody#lrobpk_list").append(
					json.html
				);
				$("#lrobpk_loading_bar").sload("true", json.id_obat + " - " + json.kode_obat + " - " + json.nama_obat + " - " + json.nama_jenis_obat + " (" + (num + 1) + " / " + limit + ")", (num + 1) * 100 / limit - 1);
				self.fillHtml(num + 1, limit);
			}
		);
	};
	LRORSAction.prototype.finalize = function() {
		var num_rows = $("tbody#lrobpk_list tr").length;
		for (var i = 0; i < num_rows; i++)
			$("tbody#lrobpk_list tr:eq(" + i + ") td#nomor").html("<small>" + (i + 1) + "</small>");
		$("#lrobpk_loading_modal").smodal("hide");
		$("#lrobpk_info").html(
			"<div class='alert alert-block alert-info'>" +
				 "<center><strong>PROSES SELESAI</strong></center>" +
			 "</div>"
		);
		$("#lrobpk_export_button").removeAttr("onclick");
		$("#lrobpk_export_button").attr("onclick", "lrobpk.export_xls()");
	};
	LRORSAction.prototype.cancel = function() {
		FINISHED = true;
	};
	LRORSAction.prototype.export_xls = function() {
		showLoading();
		var num_rows = $("#lrobpk_list").children("tr").length;
		var d_data = {};
		for (var i = 0; i < num_rows; i++) {
			var nomor = $("tbody#lrobpk_list tr:eq(" + i + ") td#nomor").text();
			var id_obat = $("tbody#lrobpk_list tr:eq(" + i + ") td#id_obat").text();
			var kode_obat = $("tbody#lrobpk_list tr:eq(" + i + ") td#kode_obat").text();
			var nama_obat = $("tbody#lrobpk_list tr:eq(" + i + ") td#nama_obat").text();
			var satuan = $("tbody#lrobpk_list tr:eq(" + i + ") td#satuan").text();
			var stok_awal = $("tbody#lrobpk_list tr:eq(" + i + ") td#stok_awal").text();
			var nilai_stok_awal = $("tbody#lrobpk_list tr:eq(" + i + ") td#nilai_stok_awal").text();
			var jumlah_pembelian = $("tbody#lrobpk_list tr:eq(" + i + ") td#jumlah_pembelian").text();
			var harga_beli_terakhir = $("tbody#lrobpk_list tr:eq(" + i + ") td#harga_pembelian_terakhir").text();
			var total_pembelian = $("tbody#lrobpk_list tr:eq(" + i + ") td#total_pembelian").text();
			var jumlah_penjualan = $("tbody#lrobpk_list tr:eq(" + i + ") td#jumlah_penjualan").text();
			var harga_jual_terakhir = $("tbody#lrobpk_list tr:eq(" + i + ") td#harga_penjualan_terakhir").text();
			var total_penjualan = $("tbody#lrobpk_list tr:eq(" + i + ") td#total_penjualan").text();
			var stok_akhir = $("tbody#lrobpk_list tr:eq(" + i + ") td#stok_akhir").text();
			var nilai_stok_akhir = $("tbody#lrobpk_list tr:eq(" + i + ") td#nilai_stok_akhir").text();
			d_data[i] = {
				"nomor" 				: nomor,
				"id_obat" 				: id_obat,
				"kode_obat" 			: kode_obat,
				"nama_obat" 			: nama_obat,
				"satuan" 				: satuan,
				"stok_awal"				: stok_awal,
				"nilai_stok_awal"		: nilai_stok_awal,
				"jumlah_pembelian"		: jumlah_pembelian,
				"harga_beli_terakhir"	: harga_beli_terakhir,
				"total_pembelian"		: total_pembelian,
				"jumlah_penjualan"		: jumlah_penjualan,
				"harga_jual_terakhir"	: harga_jual_terakhir,
				"total_penjualan"		: total_penjualan,
				"stok_akhir"			: stok_akhir,
				"nilai_stok_akhir"		: nilai_stok_akhir
			};
		}
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tanggal_from'] = $("#lrobpk_tanggal_from").val();
		data['tanggal_to'] = $("#lrobpk_tanggal_to").val();
		data['d_data'] = JSON.stringify(d_data);
		data['num_rows'] = num_rows;
		postForm(data);
		dismissLoading();
	};

	function LRORSObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRORSObatAction.prototype.constructor = LRORSObatAction;
	LRORSObatAction.prototype = new TableAction();
	LRORSObatAction.prototype.selected = function(json) {
		$("#lrobpk_id_obat").val(json.id);
		$("#lrobpk_nama_obat").val(json.nama);
	};
	
	function LRORSJenisObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LRORSJenisObatAction.prototype.constructor = LRORSJenisObatAction;
	LRORSJenisObatAction.prototype = new TableAction();
	LRORSJenisObatAction.prototype.selected = function(json) {
		$("#lrobpk_kode_jenis_obat").val(json.kode);
		$("#lrobpk_nama_jenis_obat").val(json.nama);
	};

	var lrobpk;
	var lrobpk_obat;
	var lrobpk_jenis_obat;
	var FINISHED;
	$(document).ready(function() {
		lrobpk_obat = new LRORSObatAction(
			"lrobpk_obat",
			"gudang_farmasi",
			"laporan_rekap_obat_bpk",
			new Array()
		);
		lrobpk_obat.setSuperCommand("lrobpk_obat");
		lrobpk_jenis_obat = new LRORSJenisObatAction(
			"lrobpk_jenis_obat",
			"gudang_farmasi",
			"laporan_rekap_obat_bpk",
			new Array()
		);
		lrobpk_jenis_obat.setSuperCommand("lrobpk_jenis_obat");
		lrobpk = new LRORSAction(
			"lrobpk",
			"gudang_farmasi",
			"laporan_rekap_obat_bpk",
			new Array()
		);
		$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(7)").hide();
		$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(8)").hide();
		$("#lrobpk_jenis_filter").on("change", function() {
			var jenis_filter = $("#lrobpk_jenis_filter").val();
			$("#lrobpk_id_obat").val("");
			$("#lrobpk_nama_obat").val("");
			$("#lrobpk_kode_jenis_obat").val("");
			$("#lrobpk_jenis_obat").val("");
			if (jenis_filter == "semua") {
				$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(7)").hide();
				$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(8)").hide();
			} else if (jenis_filter == "per_obat") {
				$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(7)").show();
				$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(8)").hide();
			} else if (jenis_filter == "per_jenis") {
				$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(7)").hide();
				$("#laporan_rekap_obat_bpk > div.form-container > form > div:nth-child(8)").show();
			}
		});
		$("#lrobpk_loading_modal").on("show", function() {
			$("a.close").hide();
		});
		$("#lrobpk_loading_modal").on("hide", function() {
			$("a.close").show();
		});
		$("tbody#lrobpk_list").append(
			"<tr>" +
				"<td colspan='13'><strong><center><small>DATA REKAP OBAT BELUM DIPROSES</small></center></strong></td>" +
			"</tr>"
		);
		$(document).keyup(function(e) {
			if (e.which == 27) {
				FINISHED = true;
			}
		})
	});
</script>