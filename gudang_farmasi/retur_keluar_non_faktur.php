<?php 
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("smis-base/smis-include-duplicate.php");
	
	class ReturKeluarTable extends Table {
		public function getBodyContent() {
			$content = "";
			if ($this->content!=NULL) {
				foreach ($this->content as $d) {
					$content .= "<tr>";
					foreach ($this->header as $h) {
						$content .= "<td>" . $d[$h] . "</td>";
					}
					if ($this->is_action) {
						$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['dibatalkan'])->getHtml() . "</td>";
					}
					$content .= "</tr>";
				}
			}
			return $content;
		}
		public function getFilteredContentButton($id, $dibatalkan) {
			$btn_group = new ButtonGroup("noprint");
			if (!$dibatalkan) {
				$btn = new Button("", "", "Batal");
				$btn->setAction($this->action . ".cancel('" . $id . "')");
				$btn->setClass("btn-danger");
				$btn->setAtribute("data-content='Batal' data-toggle='popover'");
				$btn->setIcon("icon-remove icon-white");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
			return $btn_group;
		}
	}
	$retur_keluar_table = new ReturKeluarTable(
		array("Nomor", "Tanggal", "Vendor", "No. Faktur", "Tgl. Faktur", "Obat", "Jumlah", "Keterangan", "Status"),
		"Gudang Farmasi : Retur Pembelian (Non-Faktur)",
		null,
		true
	);
	$retur_keluar_table->setName("retur_keluar");
	
	$vendor_table = new Table(
		array("Nomor", "Vendor", "Alamat"),
		"",
		null,
		true
	);
	$vendor_table->setName("vendor");
	$vendor_table->setModel(Table::$SELECT);
	$vendor_adapter = new SimpleAdapter();
	$vendor_adapter->add("Nomor", "id", "digit8");
	$vendor_adapter->add("Vendor", "nama");
	$vendor_adapter->add("Alamat", "alamat");
	$vendor_service_responder = new ServiceResponder(
		$db,
		$vendor_table,
		$vendor_adapter,
		"get_daftar_vendor"
	);
	
	$stok_obat_table = new Table(
		array("Nomor", "Nama Obat", "Jenis", "Stok", "Satuan", "Tgl. ED"),
		"",
		null,
		true
	);
	$stok_obat_table->setName("stok_obat");
	$stok_obat_table->setModel(Table::$SELECT);
	$stok_obat_adapter = new SimpleAdapter();
	$stok_obat_adapter->add("Nomor", "id", "digit8");
	$stok_obat_adapter->add("Nama Obat", "nama_obat");
	$stok_obat_adapter->add("Jenis", "nama_jenis_obat");
	$stok_obat_adapter->add("Stok", "sisa");
	$stok_obat_adapter->add("Satuan", "satuan");
	$stok_obat_adapter->add("Tgl. ED", "tanggal_exp", "date d-m-Y");
	$stok_obat_dbtable = new DBTable($db, "smis_fr_stok_obat");
	$stok_obat_dbresponder = new DBResponder(
		$stok_obat_dbtable,
		$stok_obat_table,
		$stok_obat_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("vendor", $vendor_service_responder);
	$super_command->addResponder("stok_obat", $stok_obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	if (isset($_POST['command'])) {
		class ReturKeluarAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['dibatalkan'] = $row->dibatalkan;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Tanggal'] = self::format("date d M Y", $row->tanggal);
				$array['Vendor'] = $row->nama_vendor;
				$array['No. Faktur'] = $row->no_faktur;
				$array['Tgl. Faktur'] = self::format("date d M Y", $row->tanggal_faktur);
				$array['Obat'] = $row->nama_obat;
				$array['Jumlah'] = $row->jumlah . " " . $row->satuan;
				$array['Keterangan'] = $row->keterangan;
				if ($row->dibatalkan)
					$array['Status'] = "Dibatalkan";
				else
					$array['Status'] = "-";
				return $array;
			}
		}
		$retur_keluar_adapter = new ReturKeluarAdapter();
		$retur_keluar_dbtable = new DBTable($db, "smis_fr_retur_obat_non_faktur");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = "AND (smis_fr_retur_obat_non_faktur.nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_retur_obat_non_faktur.no_faktur LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_retur_obat_non_faktur.keterangan LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT smis_fr_retur_obat_non_faktur.*, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.satuan
			FROM smis_fr_retur_obat_non_faktur LEFT JOIN smis_fr_stok_obat ON smis_fr_retur_obat_non_faktur.id_stok_obat = smis_fr_stok_obat.id
			WHERE smis_fr_retur_obat_non_faktur.prop NOT LIKE 'del' " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT smis_fr_retur_obat_non_faktur.*, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.satuan
				FROM smis_fr_retur_obat_non_faktur LEFT JOIN smis_fr_stok_obat ON smis_fr_retur_obat_non_faktur.id_stok_obat = smis_fr_stok_obat.id
				WHERE smis_fr_retur_obat_non_faktur.prop NOT LIKE 'del' " . $filter . "
			) v_retur
		";
		$retur_keluar_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class ReturKeluarDBResponder extends DuplicateResponder {
			public function save() {
				$data = $this->postToArray();
				$id['id'] = $_POST['id'];
				if ($id['id'] == 0 || $id['id'] == "") {
					//do insert:
					$result = $this->dbtable->insert($data);
					$id['id'] = $this->dbtable->get_inserted_id();
					$success['type'] = "insert";
					//update stok:
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_obat");
					$stok_obat_row = $stok_obat_dbtable->get_row("
						SELECT a.id, a.sisa, a.retur, b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat
						FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
						WHERE a.id = '" . $_POST['id_stok_obat'] . "'
					");
					$stok_data = array();
					$stok_data['sisa'] = $stok_obat_row->sisa - $_POST['jumlah'];
					$stok_data['retur'] = $stok_obat_row->retur + $_POST['jumlah'];
					$stok_id['id'] = $stok_obat_row->id;
					$stok_obat_dbtable->update($stok_data, $stok_id);
					//logging riwayat stok obat:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_riwayat_stok_obat");
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $_POST['id_stok_obat'];
					$data_riwayat['jumlah_keluar'] = $_POST['jumlah'];
					$data_riwayat['sisa'] = $stok_obat_row->sisa - $_POST['jumlah'];
					$data_riwayat['keterangan'] = "Retur Stok ke Vendor";
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
					//kartu gudang induk:
					$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_kartu_stok");
					$ks_data = array();
					$ks_data['f_id'] = $id['id'];
					$ks_data['unit'] = "Non Faktur";
					$ks_data['no_bon'] = "Retur Non Faktur";
					$ks_data['id_obat'] = $stok_obat_row->id_obat;
					$ks_data['kode_obat'] = $stok_obat_row->kode_obat;
					$ks_data['nama_obat'] = $stok_obat_row->nama_obat;
					$ks_data['nama_jenis_obat'] = $stok_obat_row->nama_jenis_obat;
					$ks_data['tanggal'] = date("Y-m-d");
					$ks_data['masuk'] = 0;
					$ks_data['keluar'] = $_POST['jumlah'];
					$ks_data['sisa'] = $stok_obat_row->sisa - $_POST['jumlah'];
					$ks_data['autonomous'] = "[".$this->getAutonomous()."]";
			        $ks_data['duplicate'] = 0;
			        $ks_data['time_updated'] = date("Y-m-d H:i:s");
			        $ks_data['origin_updated'] = $this->getAutonomous();
			        $ks_data['origin'] = $this->getAutonomous();
					$ks_dbtable->insert($ks_data);
				} else {
					$result = $this->dbtable->update($data, $id);
					$success['type'] = "update";
					$retur_row = $this->dbtable->get_row("
						SELECT *
						FROM smis_fr_retur_obat_non_faktur
						WHERE id = '" . $_POST['id'] . "'
					");
					//update stok:
					$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_obat");
					$stok_obat_row = $stok_obat_dbtable->get_row("
						SELECT a.id, a.sisa, a.retur, b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat
						FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
						WHERE a.id = '" . $retur_row->id_stok_obat . "'
					");
					$stok_data = array();
					$stok_data['sisa'] = $stok_obat_row->sisa + $retur_row->jumlah;
					$stok_data['retur'] = $stok_obat_row->retur - $retur_row->jumlah;
					$stok_id['id'] = $stok_obat_row->id;
					$stok_obat_dbtable->update($stok_data, $stok_id);
					//logging riwayat stok obat:
					$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_riwayat_stok_obat");
					$data_riwayat = array();
					$data_riwayat['tanggal'] = date("Y-m-d");
					$data_riwayat['id_stok_obat'] = $retur_row->id_stok_obat;
					$data_riwayat['jumlah_masuk'] = $retur_row->jumlah;
					$data_riwayat['sisa'] = $stok_obat_row->sisa + $retur_row->jumlah;
					$data_riwayat['keterangan'] = "Pembatalan Retur Stok ke Vendor";
					global $user;
					$data_riwayat['nama_user'] = $user->getName();
					$riwayat_dbtable->insert($data_riwayat);
					//kartu gudang induk:
					$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_kartu_stok");
					$ks_data = array();
					$ks_data['f_id'] = $id['id'];
					$ks_data['unit'] = "Non Faktur";
					$ks_data['no_bon'] = "Retur Non Faktur";
					$ks_data['id_obat'] = $stok_obat_row->id_obat;
					$ks_data['kode_obat'] = $stok_obat_row->kode_obat;
					$ks_data['nama_obat'] = $stok_obat_row->nama_obat;
					$ks_data['nama_jenis_obat'] = $stok_obat_row->nama_jenis_obat;
					$ks_data['tanggal'] = date("Y-m-d");
					$ks_data['masuk'] = $retur_row->jumlah;
					$ks_data['keluar'] = 0;
					$ks_data['sisa'] = $stok_obat_row->sisa + $retur_row->jumlah;
					$ks_data['autonomous'] = "[".$this->getAutonomous()."]";
			        $ks_data['duplicate'] = 0;
			        $ks_data['time_updated'] = date("Y-m-d H:i:s");
			        $ks_data['origin_updated'] = $this->getAutonomous();
			        $ks_data['origin'] = $this->getAutonomous();
				}
				$success['id'] = $id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
		}
		$retur_keluar_dbresponder = new ReturKeluarDBResponder(
			$retur_keluar_dbtable,
			$retur_keluar_table,
			$retur_keluar_adapter
		);
		$data = $retur_keluar_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	$retur_keluar_modal = new Modal("retur_keluar_add_form", "smis_form_container", "retur_keluar");
	$retur_keluar_modal->setTitle("Data Retur Pembelian (Non-Faktur)");
	$id_hidden = new Hidden("retur_keluar_id", "retur_keluar_id", "");
	$retur_keluar_modal->addElement("", $id_hidden);
	$tanggal_text = new Text("retur_keluar_tanggal", "retur_keluar_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$retur_keluar_modal->addElement("Tanggal", $tanggal_text);
	$id_vendor_hidden = new Hidden("retur_keluar_id_vendor", "retur_keluar_id_vendor", "");
	$retur_keluar_modal->addElement("", $id_vendor_hidden);
	$vendor_text = new Text("retur_keluar_vendor", "retur_keluar_vendor", "");
	$vendor_text->setAtribute("disabled='disabled'");
	$vendor_text->setClass("smis-one-option-input");
	$vendor_button = new Button("", "", "Pilih");
	$vendor_button->setClass("btn-info");
	$vendor_button->setIsButton(Button::$ICONIC);
	$vendor_button->setIcon("icon-white " . Button::$icon_list_alt);
	$vendor_button->setAction("retur_keluar.chooser('vendor', 'vendor_button', 'vendor', vendor)");
	$vendor_button->setAtribute("id='vendor_browse'");
	$vendor_input_group = new InputGroup("");
	$vendor_input_group->addComponent($vendor_text);
	$vendor_input_group->addComponent($vendor_button);
	$retur_keluar_modal->addElement("Vendor", $vendor_input_group);
	$no_faktur_text = new Text("retur_keluar_no_faktur", "retur_keluar_no_faktur", "");
	$retur_keluar_modal->addElement("No. Faktur", $no_faktur_text);
	$tanggal_faktur_text = new Text("retur_keluar_tanggal_faktur", "retur_keluar_tanggal_faktur", "");
	$tanggal_faktur_text->setClass("mydate");
	$tanggal_faktur_text->setAtribute("data-date-format='yyyy-m-d'");
	$retur_keluar_modal->addElement("Tgl. Faktur", $tanggal_faktur_text);
	$id_stok_obat_hidden = new Hidden("retur_keluar_id_stok_obat", "retur_keluar_id_stok_obat", "");
	$retur_keluar_modal->addElement("", $id_stok_obat_hidden);
	$stok_obat_text = new Text("retur_keluar_stok_obat", "retur_keluar_stok_obat", "");
	$stok_obat_text->setAtribute("disabled='disabled'");
	$stok_obat_text->setClass("smis-one-option-input");
	$stok_obat_button = new Button("", "", "Pilih");
	$stok_obat_button->setClass("btn-info");
	$stok_obat_button->setIsButton(Button::$ICONIC);
	$stok_obat_button->setIcon("icon-white " . Button::$icon_list_alt);
	$stok_obat_button->setAction("retur_keluar.chooser('stok_obat', 'stok_obat_button', 'stok_obat', stok_obat)");
	$stok_obat_button->setAtribute("id='stok_obat_browse'");
	$stok_obat_input_group = new InputGroup("");
	$stok_obat_input_group->addComponent($stok_obat_text);
	$stok_obat_input_group->addComponent($stok_obat_button);
	$retur_keluar_modal->addElement("Obat", $stok_obat_input_group);
	$tanggal_exp_text = new Text("retur_keluar_ed", "retur_keluar_ed", "");
	$tanggal_exp_text->setAtribute("disabled='disabled'");
	$retur_keluar_modal->addElement("Tgl. ED", $tanggal_exp_text);
	$sisa_hidden = new Hidden("retur_keluar_sisa", "retur_keluar_sisa", "");
	$retur_keluar_modal->addElement("", $sisa_hidden);
	$f_sisa_text = new Text("retur_keluar_f_sisa", "retur_keluar_f_sisa", "");
	$f_sisa_text->setAtribute("disabled='disabled'");
	$retur_keluar_modal->addElement("Sisa", $f_sisa_text);
	$jumlah_text = new Text("retur_keluar_jumlah", "retur_keluar_jumlah", "");
	$retur_keluar_modal->addElement("Jumlah", $jumlah_text);
	$satuan_text = new Text("retur_keluar_satuan", "retur_keluar_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$retur_keluar_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("retur_keluar_keterangan", "retur_keluar_textarea", "");
	$retur_keluar_modal->addElement("Keterangan", $keterangan_textarea);
	$retur_keluar_button = new Button("", "", "Simpan");
	$retur_keluar_button->setClass("btn-success");
	$retur_keluar_button->setIsButton(Button::$ICONIC);
	$retur_keluar_button->setIcon("fa fa-floppy-o");
	$retur_keluar_button->setAction("retur_keluar.save()");
	$retur_keluar_modal->addFooter($retur_keluar_button);
		
	echo $retur_keluar_modal->getHtml();
	echo $retur_keluar_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function VendorAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	VendorAction.prototype.constructor = VendorAction;
	VendorAction.prototype = new TableAction();
	VendorAction.prototype.selected = function(json) {
		$("#retur_keluar_id_vendor").val(json.id);
		$("#retur_keluar_vendor").val(json.nama);
	};
	
	function StokObatAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	StokObatAction.prototype.constructor = StokObatAction;
	StokObatAction.prototype = new TableAction();
	StokObatAction.prototype.selected = function(json) {
		$("#retur_keluar_id_stok_obat").val(json.id);
		$("#retur_keluar_stok_obat").val(json.nama_obat);
		$("#retur_keluar_ed").val(json.tanggal_exp);
		$("#retur_keluar_sisa").val(json.sisa);
		$("#retur_keluar_f_sisa").val(json.sisa + " " + json.satuan);
		$("#retur_keluar_satuan").val(json.satuan);
	};
	
	function ReturKeluarAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	ReturKeluarAction.prototype.constructor = ReturKeluarAction;
	ReturKeluarAction.prototype = new TableAction();
	ReturKeluarAction.prototype.show_add_form = function() {
		var today = new Date();
		$("#retur_keluar_id").val("");
		$("#retur_keluar_tanggal").val(today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
		$("#retur_keluar_id_vendor").val("");
		$("#retur_keluar_vendor").val("");
		$("#retur_keluar_no_faktur").val("");
		$("#retur_keluar_tanggal_faktur").val("");
		$("#retur_keluar_id_stok_obat").val("");
		$("#retur_keluar_stok_obat").val("");
		$("#retur_keluar_ed").val("");
		$("#retur_keluar_sisa").val("");
		$("#retur_keluar_f_sisa").val("");
		$("#retur_keluar_jumlah").val("");
		$("#retur_keluar_satuan").val("");
		$("#retur_keluar_keterangan").val("");
		$("#retur_keluar_save").show();
		$("#retur_keluar_add_form").smodal("show");
	};
	ReturKeluarAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var tanggal = $("#retur_keluar_tanggal").val();
		var vendor = $("#retur_keluar_vendor").val();
		var no_faktur = $("#retur_keluar_no_faktur").val();
		var tanggal_faktur = $("#retur_keluar_tanggal_faktur").val();
		var stok_obat = $("#retur_keluar_stok_obat").val();
		var jumlah = $("#retur_keluar_jumlah").val();
		var keterangan = $("#retur_keluar_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
			$("#retur_keluar_tanggal").addClass("error_field");
		}
		if (vendor == "") {
			valid = false;
			invalid_msg += "</br><strong>Vendor</strong> tidak boleh kosong";
			$("#retur_keluar_vendor").addClass("error_field");
		}
		if (no_faktur == "") {
			valid = false;
			invalid_msg += "</br><strong>No. Faktur</strong> tidak boleh kosong";
			$("#retur_keluar_no_faktur").addClass("error_field");
		}
		if (tanggal_faktur == "") {
			valid = false;
			invalid_msg += "</br><strong>Tanggal Faktur</strong> tidak boleh kosong";
			$("#retur_keluar_tanggal_faktur").addClass("error_field");
		}
		if (stok_obat == "") {
			valid = false;
			invalid_msg += "</br><strong>Obat</strong> tidak boleh kosong";
			$("#retur_keluar_stok_obat").addClass("error_field");
		}
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#retur_keluar_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya angka (0-9)";
			$("#retur_keluar_jumlah").addClass("error_field");
		} else if (parseFloat(jumlah) > parseFloat(stok_obat)) {
			
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#retur_keluar_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_retur_keluar_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	ReturKeluarAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#retur_keluar_add_form").smodal("hide");
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#retur_keluar_id").val();
		data['tanggal'] = $("#retur_keluar_tanggal").val();
		data['id_vendor'] = $("#retur_keluar_id_vendor").val();
		data['nama_vendor'] = $("#retur_keluar_vendor").val();
		data['no_faktur'] = $("#retur_keluar_no_faktur").val();
		data['tanggal_faktur'] = $("#retur_keluar_tanggal_faktur").val();
		data['id_stok_obat'] = $("#retur_keluar_id_stok_obat").val();
		data['jumlah'] = $("#retur_keluar_jumlah").val();
		data['keterangan'] = $("#retur_keluar_keterangan").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#retur_keluar_add_form").smodal("show");
				} else {
					self.view();
				}
				dismissLoading();
			}
		);
	};
	ReturKeluarAction.prototype.cancel = function(id) {
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = id;
		data['dibatalkan'] = 1;
		bootbox.confirm(
			"Yakin membatalkan data Retur Pembelian ini?",
			function(result) {
				if (result) {
					showLoading();
					$.post(
						"",
						data,
						function(response) {
							var json = getContent(response);
							if (json == null) return;
							self.view();
							dismissLoading();
						}
					);
				}
			}
		);
	};
	
	var retur_keluar;
	var vendor;
	var stok_obat;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#smis-chooser-modal").on("show", function() {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").addClass("half_model");
		});
		$("#smis-chooser-modal").on("hidden", function() {
			$("#smis-chooser-modal").removeClass("half_model");
		});
		$(".mydate").datepicker();
		vendor = new VendorAction(
			"vendor",
			"gudang_farmasi",
			"retur_keluar_non_faktur",
			new Array()
		);
		vendor.setSuperCommand("vendor");
		stok_obat = new StokObatAction(
			"stok_obat",
			"gudang_farmasi",
			"retur_keluar_non_faktur",
			new Array()
		);
		stok_obat.setSuperCommand("stok_obat");
		var retur_keluar_columns = new Array("id", "tanggal", "id_vendor", "nama_vendor", "no_faktur", "tanggal_faktur", "id_stok_obat", "jumlah", "keterangan", "dibatalkan");
		retur_keluar = new ReturKeluarAction(
			"retur_keluar",
			"gudang_farmasi",
			"retur_keluar_non_faktur",
			retur_keluar_columns
		);
		retur_keluar.view();
	});
</script>