<?php
class ObatKeluarTable extends Table {
	public function getBodyContent(){
		$content = "";
		if ($this->content!=NULL) {
			foreach ($this->content as $d) {
				$content .= "<tr>";
				foreach ($this->header as $h) {
					$content .= "<td>" . $d[$h] . "</td>";
				}
				if ($this->is_action) {
					$content .= "<td>" . $this->getFilteredContentButton($d['id'], $d['nomor'], $d['status'])->getHtml() . "</td>";
				}
				$content .= "</tr>";
			}
		}
		return $content;
	}
	public function getFilteredContentButton($id, $nomor, $status) {
		$btn_group = new ButtonGroup("noprint");
		if ($status == "sudah") {
			$btn = new Button("", "", "Lihat");
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setClass("btn-success");
			$btn->setAtribute("data-content='Lihat' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
		} else {
			$btn = new Button("", "", "Lihat");
			if ($status == "dikembalikan") {
				$btn->setClass("btn-inverse");
			} else {
				$btn->setClass("btn-info");
			}
			$btn->setAction($this->action . ".detail('" . $id . "')");
			$btn->setAtribute("data-content='Detail' data-toggle='popover'");
			$btn->setIcon("icon-eye-open icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			if ($status != "dikembalikan") {
				$btn = new Button("", "", "Edit");
				$btn->setAction($this->action . ".edit('" . $id . "')");
				$btn->setClass("btn-warning");
				$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
				$btn->setIcon("fa fa-pencil");
				$btn->setIsButton(Button::$ICONIC);
				$btn_group->addElement($btn);
			}
		}
		// $btn = new Button("", "", "Mutasi Susulan");
		// $btn->setAction($this->action . ".copy_header('" . $id . "')");
		// $btn->setClass("btn-info");
		// $btn->setAtribute("data-content='Mutasi Susulan' data-toggle='popover'");
		// $btn->setIcon("fa fa-copy");
		// $btn->setIsButton(Button::$ICONIC);
		// $btn_group->addElement($btn);
		$btn = new Button("", "", "Unduh XLS Per Nomor");
		$btn->setAction($this->action . ".download_au58('" . $id . "', '" . $status . "')");
		$btn->setClass("btn-inverse");
		$btn->setAtribute("data-content='Unduh XLS Per Nomor' data-toggle='popover'");
		$btn->setIcon("fa fa-download");
		$btn->setIsButton(Button::$ICONIC);
		$btn_group->addElement($btn);
		return $btn_group;
	}
}
?>