<?php 
	global $db;
	
	$laporan_form = new Form("lom_form", "", "Gudang Farmasi : Laporan Penerimaan Obat dari Vendor");
	$tanggal_from_text = new Text("lom_tanggal_from", "lom_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lom_tanggal_to", "lom_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lom.view()");
	$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lom.print()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$laporan_form->addElement("", $btn_group);
	
	$lom_table = new Table(
		array("No. Faktur", "Vendor", "Nama Obat", "Jumlah", "Satuan", "HNA", "Total HNA"),
		"",
		null,
		true
	);
	$lom_table->setName("lom");
	$lom_table->setAction(false);
	
	if (isset($_POST['command'])) {
		class LOMAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Faktur'] = $row->no_faktur;
				$array['Vendor'] = $row->nama_vendor;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$array['HNA'] = self::format("money Rp. ", $row->hna + $row->selisih);
				$array['Total HNA'] = self::format("money Rp. ", ($row->jumlah * ($row->hna + $row->selisih)));
				return $array;
			}
		}
		$lom_adapter = new LOMAdapter();		
		$lom_dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (no_faktur LIKE '%" . $_POST['kriteria'] . "%' OR nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_fr_dobat_f_masuk.id, smis_fr_obat_f_masuk.tanggal, smis_fr_obat_f_masuk.no_faktur, smis_fr_obat_f_masuk.nama_vendor, smis_fr_dobat_f_masuk.nama_obat, smis_fr_dobat_f_masuk.jumlah, smis_fr_dobat_f_masuk.satuan, smis_fr_dobat_f_masuk.hna, smis_fr_dobat_f_masuk.selisih, smis_fr_dobat_f_masuk.diskon AS 'd_diskon', smis_fr_dobat_f_masuk.t_diskon AS 'd_t_diskon', smis_fr_obat_f_masuk.diskon AS 'h_diskon', smis_fr_obat_f_masuk.t_diskon AS 'h_t_diskon', smis_fr_obat_f_masuk.tipe
					FROM smis_fr_dobat_f_masuk LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
					WHERE smis_fr_dobat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.id <> '0'
				) v_lom
				WHERE " . $filter . "
				ORDER BY id ASC
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					SELECT smis_fr_dobat_f_masuk.id, smis_fr_obat_f_masuk.tanggal, smis_fr_obat_f_masuk.no_faktur, smis_fr_obat_f_masuk.nama_vendor, smis_fr_dobat_f_masuk.nama_obat, smis_fr_dobat_f_masuk.jumlah, smis_fr_dobat_f_masuk.satuan, smis_fr_dobat_f_masuk.hna, smis_fr_dobat_f_masuk.selisih, smis_fr_dobat_f_masuk.diskon AS 'd_diskon', smis_fr_dobat_f_masuk.t_diskon AS 'd_t_diskon', smis_fr_obat_f_masuk.diskon AS 'h_diskon', smis_fr_obat_f_masuk.t_diskon AS 'h_t_diskon', smis_fr_obat_f_masuk.tipe
					FROM smis_fr_dobat_f_masuk LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
					WHERE smis_fr_dobat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.id <> '0'
				) v_lom
				WHERE " . $filter . "
				ORDER BY id ASC
			";
			$lom_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		class LOMDBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['from'];
				$to = $_POST['to'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_fr_dobat_f_masuk.id, smis_fr_obat_f_masuk.tanggal, smis_fr_obat_f_masuk.no_faktur, smis_fr_obat_f_masuk.nama_vendor, smis_fr_dobat_f_masuk.nama_obat, smis_fr_dobat_f_masuk.jumlah, smis_fr_dobat_f_masuk.satuan, smis_fr_dobat_f_masuk.hna, smis_fr_dobat_f_masuk.selisih, smis_fr_dobat_f_masuk.diskon AS 'd_diskon', smis_fr_dobat_f_masuk.t_diskon AS 'd_t_diskon', smis_fr_obat_f_masuk.diskon AS 'h_diskon', smis_fr_obat_f_masuk.t_diskon AS 'h_t_diskon', smis_fr_obat_f_masuk.tipe
						FROM smis_fr_dobat_f_masuk LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
						WHERE smis_fr_dobat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.prop NOT LIKE 'del' AND smis_fr_obat_f_masuk.id <> '0'
					) v_lom
					WHERE tanggal >= '" . $from . "' AND tanggal <= '" . $to . "'
					ORDER BY id ASC
				");
				$print_data = "";
				$print_data .= "<center><strong>LAPORAN PENERIMAAN OBAT GUDANG FARMASI DARI VENDOR</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Faktur</th>
										<th>Vendor</th>
										<th>Nama Obat</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>HNA</th>
										<th>Total HNA</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					foreach($data as $d) {
						$hna = $d->hna + $d->selisih;
						$total_hna = $d->jumlah * ($d->hna + $d->selisih);
						$print_data .= "<tr>
											<td>" . $d->no_faktur . "</td>
											<td>" . $d->nama_vendor . "</td>
											<td>" . $d->nama_obat . "</td>
											<td>" . $d->jumlah . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ",  $hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $total_hna) . "</td>
										</tr>";
						$total += $total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='7' align='center'><i>Tidak terdapat data penerimaan</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='6' align='center'><b>T O T A L</b></td>
									<td><b>" . ArrayAdapter::format("money Rp. ", $total) . "</b></td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lom_dbresponder = new LOMDBResponder(
			$lom_dbtable,
			$lom_table,
			$lom_adapter
		);
		$data = $lom_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lom_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOMAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOMAction.prototype.constructor = LOMAction;
	LOMAction.prototype = new TableAction();
	LOMAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lom_tanggal_from").val();
		data['tanggal_to'] = $("#lom_tanggal_to").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOMAction.prototype.print = function() {
		if ($("#lom_tanggal_from").val() == "" || $("#lom_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['from'] = $("#lom_tanggal_from").val();
		data['to'] = $("#lom_tanggal_to").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	
	var lom;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		lom = new LOMAction(
			"lom",
			"gudang_farmasi",
			"laporan_obat_masuk",
			new Array()
		)
		$('.mydate').datepicker();
	});
</script>