<?php
	global $db;

	$table = new Table(
		array("Kode", "Nama Obat", "Sisa", "Satuan", "Umur", "Kategori"),
		"Gudang Farmasi : Umur Obat",
		null,
		true
	);
	$table->setName("luo");
	$table->setAddButtonEnable(false);
	$table->setEditButtonEnable(false);
	$table->setDelButtonEnable(false);

	if (isset($_POST['command'])) {
		class UmurObatAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				if (substr($row->kode_obat, 0, 4) == "JKN.")
					$array['Kode'] = "319." . substr($row->kode_obat, 4);
				else if (substr($row->kode_obat, 0, 4) == "REG.")
					$array['Kode'] = "309." . substr($row->kode_obat, 4);
				else
					$array['Kode'] = $row->kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Sisa'] = ArrayAdapter::format("number", $row->sisa);
				$array['Satuan'] = $row->satuan;
				$array['Umur'] = round($row->umur / 30);
				if (round($row->umur / 30) < 4)
					$array['Kategori'] = "I";
				else if (round($row->umur / 30) < 7)
					$array['Kategori'] = "II";
				else if (round($row->umur / 30) < 10)
					$array['Kategori'] = "III";
				else if (round($row->umur / 30) <= 12)
					$array['Kategori'] = "IV";
				else
					$array['Kategori'] = "V";
				return $array;
			}
		}
		$adapter = new UmurObatAdapter();
		$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (b.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR b.nama_obat LIKE '%" . $_POST['kriteria'] . "%') ";
		}
		$query_value = "
			SELECT b.id_obat AS 'id', b.kode_obat, b.nama_obat, SUM(c.sisa) AS 'sisa', b.satuan, DATEDIFF(NOW(), CASE WHEN MIN(a.tanggal_datang) IS NULL OR MIN(a.tanggal_datang) = '0000-00-00' THEN '2016-04-25' ELSE MIN(a.tanggal_datang) END) AS 'umur'
			FROM (smis_fr_obat_f_masuk a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk) LEFT JOIN smis_fr_stok_obat c ON b.id = c.id_dobat_masuk 
			WHERE c.sisa > 0 AND b.konversi = 1  " . $filter . "
			GROUP BY b.id_obat, b.satuan
			ORDER BY DATEDIFF(NOW(), CASE WHEN MIN(a.tanggal_datang) IS NULL OR MIN(a.tanggal_datang) = '0000-00-00' THEN '2016-04-25' ELSE MIN(a.tanggal_datang) END) DESC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$dbtable->setPreferredQuery(true, $query_value, $query_count);
		$dbresponder = new DBResponder(
			$dbtable,
			$table,
			$adapter
		);
		$data = $dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}

	echo $table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
?>
<script type="text/javascript">
	var luo;
	$(document).ready(function() {
		luo = new TableAction(
			"luo",
			"gudang_farmasi",
			"laporan_umur_obat",
			new Array()
		);
		luo.view();
	});
</script>