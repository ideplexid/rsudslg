<?php 
	$laporan_tabulator = new Tabulator("laporan", "", Tabulator::$LANDSCAPE);
	$laporan_tabulator->add("laporan_pelayanan_farmasi", "Lap. Pelayanan Farmasi", "gudang_farmasi/laporan_pelayanan_farmasi.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_pareto", "Laporan Pareto", "gudang_farmasi/laporan_pareto.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_pembelian", "Rekap Pembelian", "gudang_farmasi/laporan_rekap_pembelian.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_rekap_pembelian_per_pbf", "Rekap Pembelian Per Distributor", "gudang_farmasi/laporan_rekap_pembelian_per_pbf.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_persediaan_obat", "Persediaan Obat", "gudang_farmasi/laporan_persediaan_obat.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_stock_opname", "Lap. Stock Opname", "gudang_farmasi/laporan_stock_opname.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_obat_keluar_non_apotek", "Rekap Mutasi Obat", "gudang_farmasi/laporan_obat_keluar_non_apotek.php", Tabulator::$TYPE_INCLUDE);
	$laporan_tabulator->add("laporan_obat_keluar_per_ruangan", "Mutasi Obat Per Ruangan", "gudang_farmasi/laporan_obat_keluar_per_ruangan.php", Tabulator::$TYPE_INCLUDE);

	echo $laporan_tabulator->getHtml();
?>
<style type="text/css">
	@page {
		margin-left: 50px;
		margin-right: 50px;
		margin-top: 50px;
		margin-bottom: 50px;
	}
	@media print{
		#signature {
			page-break-inside: avoid; 
		}
	}
</style>