<?php
	require_once("gudang_farmasi/responder/PermintaanObatKeluarDBResponder.php");
	global $db;

	// chooser stok obat
	$obat_table = new Table(
		array("No.", "ID Stok", "Kode", "Obat", "Jenis", "Sisa", "Satuan", "Tgl. Exp.", "No. Batch"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new SimpleAdapter(true, "No.");
	$obat_adapter->add("ID Stok", "id");
	$obat_adapter->add("Kode", "kode_obat");
	$obat_adapter->add("Obat", "nama_obat");
	$obat_adapter->add("Jenis", "nama_jenis_obat");
	$obat_adapter->add("Sisa", "sisa");
	$obat_adapter->add("Satuan", "satuan");
	$obat_adapter->add("Tgl. Exp.", "tanggal_exp");
	$obat_adapter->add("No. Batch", "no_batch");
	$obat_dbtable = new DBTable($db, "smis_fr_stok_obat");
	if ($_POST['command'] == "list") {
		$obat_dbtable->setViewForSelect(true);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (a.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR a.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR b.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
		}
		if ($_POST['id_stok_obat'] != "") {
			$filter .= " AND a.id NOT IN(" . rtrim($_POST['id_stok_obat'], ",") . ") ";
		}
		$query_value = "
			SELECT *
			FROM (
				SELECT a.*, b.id_obat
				FROM smis_fr_stok_obat a INNER JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE a.prop = '' AND a.sisa > 0 AND b.id_obat = '" . $_POST['id_obat'] . "' " . $filter . "
			) v_stok
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				SELECT a.*, b.id_obat
				FROM smis_fr_stok_obat a INNER JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE a.prop = '' AND a.sisa > 0 AND b.id_obat = '" . $_POST['id_obat'] . "' " . $filter . "
			) v_stok
		";
		$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	}
	$obat_dbresponder = new DBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);

	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	if (isset($_POST['command']) && $_POST['command'] == "get_detail") {
		$id_mutasi_obat_keluar = $_POST['id_mutasi_obat_keluar'];
		$html = "";
		$detail_rows = $db->get_result("
			SELECT *
			FROM smis_fr_dmutasi_obat_keluar
			WHERE prop = '' AND id_mutasi_obat_keluar = '" . $id_mutasi_obat_keluar . "'
		");
		if ($detail_rows != null) {
			$nomor = 1;
			foreach ($detail_rows as $dr) {
				$html .= 	"<tr>" .
								"<td><strong>" . $nomor . "</strong></td>" .
								"<td><strong>" . $dr->kode_obat . "</strong></td>" .
								"<td><strong>" . $dr->nama_obat . "</strong></td>" .
								"<td><strong>" . $dr->nama_jenis_obat . "</strong></td>" .
								"<td><strong>" . $dr->jumlah_diminta . " " . $dr->satuan . "</strong></td>" .
								"<td><strong>" . $dr->jumlah_dipenuhi . " " . $dr->satuan . "</strong></td>" .
							"</tr>";
				$stok_rows = $db->get_result("
					SELECT a.*, b.tanggal_exp, b.no_batch, b.produsen, b.satuan
					FROM smis_fr_stok_mutasi_obat_keluar a INNER JOIN smis_fr_stok_obat b ON a.id_stok_obat = b.id
					WHERE a.prop = '' AND a.id_dmutasi_obat_keluar = '" . $dr->id . "'
				");
				if ($stok_rows != null) {
					foreach ($stok_rows as $sr) {
						$html .= 	"<tr>" .
										"<td></td>" .
										"<td>ID Stok : " . $sr->id_stok_obat . "</td>" .
										"<td>ED : " . $sr->tanggal_exp . "</td>" .
										"<td>No. Batch : " . $sr->no_batch . "</td>" .
										"<td>Produsen : " . $sr->produsen . "</td>" .
										"<td>" . $sr->jumlah . " " . $sr->satuan . "</td>" .
									"</tr>";
					}
				}
				$nomor++;
			}
		}
		echo json_encode(array("html" => $html));
		return;
	}

	$id = isset($_POST['id']) ? $_POST['id'] : "";
	$f_id_permintaan_obat_unit = isset($_POST['f_id_permintaan_obat_unit']) ? $_POST['f_id_permintaan_obat_unit'] : "0";
	$f_id_obat_keluar = isset($_POST['f_id_obat_keluar']) ? $_POST['f_id_obat_keluar'] : "";
	$tanggal_permintaan = isset($_POST['tanggal_permintaan']) ? $_POST['tanggal_permintaan'] : date("Y-m-d");
	$tanggal_mutasi = isset($_POST['tanggal_mutasi']) ? $_POST['tanggal_mutasi'] : date("Y-m-d");
	$unit = isset($_POST['unit']) ? $_POST['unit'] : "";
	$editable = isset($_POST['editable']) ? $_POST['editable'] : "true";

	$permintaan_table = new Table(
		array("No.", "Tanggal Permintaan", "Unit"),
		"",
		null,
		true
	);
	$permintaan_table->setName("permintaan");
	$permintaan_table->setModel(Table::$SELECT);
	$permintaan_adapter = new SimpleAdapter(true, "No.");
	$permintaan_adapter->add("Tanggal Permintaan", "tanggal", "date d-m-Y");
	$permintaan_adapter->add("Unit", "unit", "unslug");
	$permintaan_dbtable = new DBTable($db, "smis_fr_permintaan_obat_unit");
	$permintaan_dbtable->addCustomKriteria(" status ", " ='belum' ");
	$permintaan_dbtable->setOrder(" id DESC ");
	$permintaan_dbresponder = new PermintaanObatKeluarDBResponder(
		$permintaan_dbtable,
		$permintaan_table,
		$permintaan_adapter
	);
	$super_command = new SuperCommand();
	$super_command->addResponder("permintaan", $permintaan_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}

	$header_form = new Form("", "", "Gudang Farmasi : Obat Keluar - Permintaan Unit");
	$id_hidden = new Hidden("mutasi_obat_keluar_id", "mutasi_obat_keluar_id", $id);
	$header_form->addElement("", $id_hidden);
	$f_id_hidden = new Hidden("mutasi_obat_keluar_f_id", "mutasi_obat_keluar_f_id", "");
	$header_form->addElement("", $f_id_hidden);
	$f_id_permintaan_obat_unit_hidden = new Hidden("mutasi_obat_keluar_f_id_permintaan_obat_unit", "mutasi_obat_keluar_f_id_permintaan_obat_unit", $f_id_permintaan_obat_unit);
	$header_form->addElement("", $f_id_permintaan_obat_unit_hidden);
	$f_id_obat_keluar_hidden = new Hidden("mutasi_obat_keluar_f_id_obat_keluar", "mutasi_obat_keluar_f_id_obat_keluar", $f_id_obat_keluar);
	$header_form->addElement("", $f_id_obat_keluar_hidden);
	$slug_hidden = new Hidden("mutasi_obat_keluar_slug", "mutasi_obat_keluar_slug", $unit);
	$header_form->addElement("", $slug_hidden);
	$unit_text = new Text("mutasi_obat_keluar_unit", "mutasi_obat_keluar_unit", ArrayAdapter::format("unslug", $unit));
	$unit_text->setAtribute("disabled='disabled'");
	if ($editable == "true") {
		$unit_text->setClass("smis-one-option-input");
		$unit_browse_button = new Button("", "", "Pilih");
		$unit_browse_button->setClass("btn-info");
		$unit_browse_button->setIsButton(Button::$ICONIC);
		$unit_browse_button->setIcon("fa fa-list");
		$unit_browse_button->setAction("permintaan.chooser('permintaan', 'permintaan', 'permintaan', permintaan,'Permintaan Obat Unit')");
		$unit_browse_button->setAtribute("id='permintaan_browse'");
		$unit_input_group = new InputGroup("");
		$unit_input_group->addComponent($unit_text);
		$unit_input_group->addComponent($unit_browse_button);
		$header_form->addElement("Unit", $unit_input_group);
	} else
		$header_form->addElement("Unit", $unit_text);
	$tanggal_permintaan_text = new Text("mutasi_obat_keluar_tanggal_permintaan", "mutasi_obat_keluar_tanggal_permintaan", $tanggal_permintaan);
	$tanggal_permintaan_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Tgl. Permintaan", $tanggal_permintaan_text);
	$tanggal_mutasi_text = new Text("mutasi_obat_keluar_tanggal_mutasi", "mutasi_obat_keluar_tanggal_mutasi", $tanggal_mutasi);
	$tanggal_mutasi_text->setAtribute("disabled='disabled'");
	$header_form->addElement("Tgl. Mutasi", $tanggal_mutasi_text);

	$table = new Table(
		array("No.", "Kode", "Obat", "Jenis", "Jml. Diminta", "Jml. Dipenuhi", ""),
		"",
		null,
		true
	);
	$table->setName("dmutasi_obat_keluar");
	$table->setFooterVisible(false);
	$table->setAction(false);

	$mutasi_obat_keluar_modal = new Modal("mutasi_obat_keluar_add_form", "smis_form_container", "mutasi_obat_keluar");
	$mutasi_obat_keluar_modal->setTitle("Data Stok Obat");
	$row_num_hidden = new Hidden("mutasi_obat_keluar_row_num", "mutasi_obat_keluar_row_num", "");
	$mutasi_obat_keluar_modal->addElement("", $row_num_hidden);
	$id_stok_obat_hidden = new Hidden("mutasi_obat_keluar_id_stok_obat", "mutasi_obat_keluar_id_stok_obat", "");
	$mutasi_obat_keluar_modal->addElement("", $id_stok_obat_hidden);
	$kode_obat_text = new Text("mutasi_obat_keluar_kode_obat", "mutasi_obat_keluar_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$mutasi_obat_keluar_modal->addElement("Kode Obat", $kode_obat_text);
	$nama_obat_text = new Text("mutasi_obat_keluar_nama_obat", "mutasi_obat_keluar_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$nama_obat_text->setClass("smis-one-option-input");
	$obat_browse_button = new Button("", "", "Pilih");
	$obat_browse_button->setClass("btn-info");
	$obat_browse_button->setIsButton(Button::$ICONIC);
	$obat_browse_button->setIcon("fa fa-list");
	$obat_browse_button->setAction("obat.chooser('obat', 'obat', 'obat', obat,'Data Stok Obat')");
	$obat_browse_button->setAtribute("id='obat_browse'");
	$obat_input_group = new InputGroup("");
	$obat_input_group->addComponent($nama_obat_text);
	$obat_input_group->addComponent($obat_browse_button);
	$mutasi_obat_keluar_modal->addElement("Nama Obat", $obat_input_group);
	$nama_jenis_obat_text = new Text("mutasi_obat_keluar_nama_jenis_obat", "mutasi_obat_keluar_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$mutasi_obat_keluar_modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	$sisa_text = new Text("mutasi_obat_keluar_sisa", "mutasi_obat_keluar_sisa", "");
	$sisa_text->setAtribute("disabled='disabled'");
	$mutasi_obat_keluar_modal->addElement("Sisa", $sisa_text);
	$jumlah_text = new Text("mutasi_obat_keluar_jumlah", "mutasi_obat_keluar_jumlah", "");
	$mutasi_obat_keluar_modal->addElement("Jumlah", $jumlah_text);
	$satuan_text = new Text("mutasi_obat_keluar_satuan", "mutasi_obat_keluar_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$mutasi_obat_keluar_modal->addElement("Satuan", $satuan_text);
	$tanggal_exp_text = new Text("mutasi_obat_keluar_tanggal_exp", "mutasi_obat_keluar_tanggal_exp", "");
	$tanggal_exp_text->setAtribute("disabled='disabled'");
	$mutasi_obat_keluar_modal->addElement("Tanggal Exp.", $tanggal_exp_text);
	$no_batch_text = new Text("mutasi_obat_keluar_no_batch", "mutasi_obat_keluar_no_batch", "");
	$no_batch_text->setAtribute("disabled='disabled'");
	$mutasi_obat_keluar_modal->addElement("No. Batch", $no_batch_text);
	$produsen_text = new Text("mutasi_obat_keluar_produsen", "mutasi_obat_keluar_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$mutasi_obat_keluar_modal->addElement("Produsen", $produsen_text);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setIsButton(Button::$ICONIC);
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setAction("dmutasi_obat_keluar.save()");
	$save_button->setAtribute("id='dmutasi_obat_keluar_save'");
	$mutasi_obat_keluar_modal->addFooter($save_button);

	$button_group = new ButtonGroup("");
	$back_button = new Button("", "", "Kembali");
	$back_button->setClass("btn-inverse");
	$back_button->setIsButton(Button::$ICONIC_TEXT);
	$back_button->setIcon("fa fa-chevron-left");
	$back_button->setAction("mutasi_obat_keluar.back()");
	$back_button->setAtribute("id='mutasi_obat_keluar_back'");
	$button_group->addButton($back_button);
	if ($editable == "true") {
		$save_button = new Button("", "", "Simpan");
		$save_button->setClass("btn-success");
		$save_button->setIsButton(Button::$ICONIC_TEXT);
		$save_button->setIcon("fa fa-floppy-o");
		$save_button->setAction("mutasi_obat_keluar.save()");
		$save_button->setAtribute("id='mutasi_obat_keluar_save'");
		$button_group->addButton($save_button);
	}

	echo $mutasi_obat_keluar_modal->getHtml();
	echo $header_form->getHtml();
	echo "<div id='table_content'>" .
			 "<div class='row-fluid'>";
	echo	 	"<div class='span12'>" .
					 $table->getHtml() .
					 "<div align='right'>" . $button_group->getHtml() . "</div>" .
				 "</div>";
	echo 	 "</div>" .
		 "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("gudang_farmasi/js/obat_keluar_dari_permintaan_permintaan_action.js", false);
	echo addJS("gudang_farmasi/js/obat_keluar_dari_permintaan_stok_obat_action.js", false);
	echo addJS("gudang_farmasi/js/obat_keluar_dari_permintaan_action.js", false);
	echo addJS("gudang_farmasi/js/dobat_keluar_dari_permintaan_action.js", false);
	echo addJS("gudang_farmasi/js/obat_keluar_dari_permintaan_form.js", false);
?>