<?php 
	global $db;
	
	$laporan_form = new Form("lok_form", "", "Gudang Farmasi : Rekap Mutasi Obat");
	$tanggal_from_text = new Text("lok_tanggal_from", "lok_tanggal_from", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Awal", $tanggal_from_text);
	$tanggal_to_text = new Text("lok_tanggal_to", "lok_tanggal_to", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$laporan_form->addElement("Waktu Akhir", $tanggal_to_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("lok.view()");
	/*$print_button = new Button("", "", "Cetak");
	$print_button->setClass("btn-inverse");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC);
	$print_button->setAction("lok.print()");*/
	$btn_group = new ButtonGroup("noprint");
	$export_button = new Button("", "", "Unduh");
	$export_button->setClass("btn-inverse");
	$export_button->setIcon("fa fa-download");
	$export_button->setIsButton(Button::$ICONIC);
	$export_button->setAction("lok.export_xls()");
	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	// $btn_group->addButton($print_button);
	$btn_group->addButton($export_button);
	$laporan_form->addElement("", $btn_group);
	
	$lok_table = new Table(
		array("No. Mutasi", "Tanggal", "Ruangan", "Kode Obat", "Nama Obat", "Jumlah", "Satuan", "HPP", "Total HPP", "Tgl. Exp."),
		"",
		null,
		true
	);
	$lok_table->setName("lok");
	$lok_table->setAction(false);
	
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "export_xls") {
			$nama_entitas = getSettings($db, "smis_autonomous_title", "");
			$alamat_entitas = getSettings($db, "smis_autonomous_address", "");
			$tanggal_from = $_POST['tanggal_from'];
			$tanggal_to = $_POST['tanggal_to'];
			require_once("smis-libs-out/php-excel/PHPExcel.php");
			$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/template_rekap_mutasi_obat.xlsx");
			$objPHPExcel->setActiveSheetIndexByName("REKAP MUTASI OBAT");
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setCellValue("B2", "GUDANG FARMASI - " . ArrayAdapter::format("unslug", $nama_entitas));
			$objWorksheet->setCellValue("B3", "UNIT : SEMUA");
			$objWorksheet->setCellValue("B4", "PERIODE : " . ArrayAdapter::format("date d-m-Y", $tanggal_from) . " s/d " . ArrayAdapter::format("date d-m-Y", $tanggal_to));
			$dbtable = new DBTable($db, "smis_fr_obat_keluar");
			$data = $dbtable->get_result("
				SELECT *
				FROM (
					SELECT smis_fr_obat_keluar.id, smis_fr_obat_keluar.nomor, smis_fr_obat_keluar.unit, smis_fr_obat_keluar.tanggal, smis_fr_stok_obat.kode_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat_keluar.jumlah, smis_fr_stok_obat.satuan, (smis_fr_dobat_keluar.harga_ma / 1.1) AS 'hna', smis_fr_stok_obat.tanggal_exp
					FROM ((smis_fr_stok_obat_keluar LEFT JOIN smis_fr_stok_obat ON smis_fr_stok_obat_keluar.id_stok_obat = smis_fr_stok_obat.id) LEFT JOIN smis_fr_dobat_keluar ON smis_fr_stok_obat_keluar.id_dobat_keluar = smis_fr_dobat_keluar.id) LEFT JOIN smis_fr_obat_keluar ON smis_fr_dobat_keluar.id_obat_keluar = smis_fr_obat_keluar.id
					WHERE smis_fr_obat_keluar.status <> 'dikembalikan' AND smis_fr_dobat_keluar.prop NOT LIKE 'del'  AND smis_fr_stok_obat_keluar.jumlah <> 0
				) v_loka
				WHERE tanggal >= '" . $tanggal_from . "' AND tanggal <= '" . $tanggal_to . "'
			");
			if (count($data) - 2 > 0)
				$objWorksheet->insertNewRowBefore(9, count($data) - 2);
			$start_row_num = 8;
			$end_row_num = 8;
			$row_num = $start_row_num;
			$no = 1;
			foreach ($data as $d) {
				$col_num = 1;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $no++);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nomor);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("date d-m-Y", $d->tanggal));
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("unslug", $d->unit));
				$col_num++;
				$kode_obat = $d->kode_obat;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $kode_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->nama_obat);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->satuan);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->hna);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, $d->jumlah * $d->hna);
				$col_num++;
				$objWorksheet->setCellValueByColumnAndRow($col_num, $row_num, ArrayAdapter::format("date d-m-Y", $d->tanggal_exp));
				$objWorksheet->getStyle("H" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("J" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$objWorksheet->getStyle("K" . $row_num)->getNumberFormat()->setFormatCode("#,##0");
				$row_num++;
				$end_row_num++;
			}
			header("Content-type: application/vnd.ms-excel");	
			header("Content-Disposition: attachment; filename=REKAP_MUTASI_OBAT_GUDANG_FARMASI_KE_SEMUA_UNIT_" . ArrayAdapter::format("date Ymd", $tanggal_from) . "_" . ArrayAdapter::format("date Ymd", $tanggal_to) . "_" . date("Ymd_His") . ".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save("php://output");
			return;
		}
		class LOKAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['No. Mutasi'] = $row->nomor;
				$array['Tanggal'] = ArrayAdapter::format("date d-m-Y", $row->tanggal);
				$array['Ruangan'] = self::format("unslug", $row->unit);
				$kode_obat = $row->kode_obat;
				$array['Kode Obat'] = $kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jumlah'] = $row->jumlah;
				$array['Satuan'] = $row->satuan;
				$array['HPP'] = self::format("money Rp. ", $row->hna);
				$array['Total HPP'] = self::format("money Rp. ", $row->jumlah * $row->hna);
				$array['Tgl. Exp.'] = self::format("date d-m-Y", $row->tanggal_exp);
				return $array;
			}
		}
		$lok_adapter = new LOKAdapter();		
		$lok_dbtable = new DBTable($db, "smis_fr_obat_keluar");
		if (isset($_POST['command']) == "list") {
			$filter = "tanggal >= '" . $_POST['tanggal_from'] . "' AND tanggal <= '" . $_POST['tanggal_to'] . "'";
			if (isset($_POST['kriteria'])) {
				$filter .= " AND (id LIKE '%" . $_POST['kriteria'] . "%' OR unit LIKE '%" . $_POST['kriteria'] . "%' OR nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR satuan LIKE '%" . $_POST['kriteria'] . "%') ";
			}
			$query_value = "
				SELECT *
				FROM (
					SELECT smis_fr_obat_keluar.id, smis_fr_obat_keluar.nomor, smis_fr_obat_keluar.tanggal, smis_fr_obat_keluar.unit, smis_fr_stok_obat.kode_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat_keluar.jumlah, smis_fr_stok_obat.satuan, (smis_fr_dobat_keluar.harga_ma / 1.1) AS 'hna', smis_fr_stok_obat.tanggal_exp
					FROM ((smis_fr_stok_obat_keluar LEFT JOIN smis_fr_stok_obat ON smis_fr_stok_obat_keluar.id_stok_obat = smis_fr_stok_obat.id) LEFT JOIN smis_fr_dobat_keluar ON smis_fr_stok_obat_keluar.id_dobat_keluar = smis_fr_dobat_keluar.id) LEFT JOIN smis_fr_obat_keluar ON smis_fr_dobat_keluar.id_obat_keluar = smis_fr_obat_keluar.id
					WHERE smis_fr_obat_keluar.status <> 'dikembalikan' AND smis_fr_dobat_keluar.prop NOT LIKE 'del' AND smis_fr_stok_obat_keluar.jumlah <> 0
				) v_lok
				WHERE " . $filter . "
			";
			$query_count = "
				SELECT COUNT(*)
				FROM (
					" . $query_value . "
				) v
			";
			$lok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		}
		class LOKDBResponder extends DBResponder {
			public function command($command) {
				if ($command != "print_laporan") {
					return parent::command($command);
				}
				$pack = new ResponsePackage();
				if ($command == "print_laporan") {
					$content = $this->print_report();
					$pack->setContent($content);
					$pack->setStatus(ResponsePackage::$STATUS_OK);
				}
				return $pack->getPackage();
			}
			public function print_report() {
				$from = $_POST['tanggal_from'];
				$to = $_POST['tanggal_to'];
				$data = $this->dbtable->get_result("
					SELECT *
					FROM (
						SELECT smis_fr_obat_keluar.id, smis_fr_obat_keluar.nomor, smis_fr_obat_keluar.tanggal, smis_fr_obat_keluar.unit, smis_fr_stok_obat.kode_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat_keluar.jumlah, smis_fr_stok_obat.satuan, (smis_fr_dobat_keluar.harga_ma / 1.1) AS 'hna', smis_fr_stok_obat.tanggal_exp
						FROM ((smis_fr_stok_obat_keluar LEFT JOIN smis_fr_stok_obat ON smis_fr_stok_obat_keluar.id_stok_obat = smis_fr_stok_obat.id) LEFT JOIN smis_fr_dobat_keluar ON smis_fr_stok_obat_keluar.id_dobat_keluar = smis_fr_dobat_keluar.id) LEFT JOIN smis_fr_obat_keluar ON smis_fr_dobat_keluar.id_obat_keluar = smis_fr_obat_keluar.id
						WHERE smis_fr_obat_keluar.status <> 'dikembalikan' AND smis_fr_dobat_keluar.prop NOT LIKE 'del' AND smis_fr_stok_obat_keluar.jumlah <> 0
					) v_lok
					WHERE tanggal >= '" . $from . "' AND tanggal <= '" . $to . "'
				");
				$print_data = "";
				$print_data .= "<center><strong>REKAP MUTASI OBAT</strong></center><br/>";
				$print_data .= "<table border='0'>
									<tr>
										<td>Dari</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $from) . "</td>
									</tr>
									<tr>
										<td>Sampai</td>
										<td>:</td>
										<td>" . ArrayAdapter::format("date d M Y", $to) . "</td>
									</tr>
								</table>";
				$print_data .= "<table border='1'>
									<tr>
										<th>No. Mutasi</th>
										<th>Ruangan</th>
										<th>Kode Obat</th>
										<th>Nama Obat</th>
										<th>Jumlah</th>
										<th>Satuan</th>
										<th>HNA</th>
										<th>Total HNA</th>
										<th>Tgl. Exp.</th>
									</tr>";
				$total = 0;
				if (count($data) > 0) {
					foreach($data as $d) {
						$hna = $d->hna;
						$total_hna = $d->jumlah * $d->hna;
						$kode_obat = "319" . substr($d->kode_obat, 3);
						if (substr($d->kode_obat, 0, 3) == "REG")
							$kode_obat = "309" . substr($d->kode_obat, 3);
						$print_data .= "<tr>
											<td>" . $d->nomor . "</td>
											<td>" . ArrayAdapter::format("unslug", $d->unit) . "</td>
											<td>" . $kode_obat . "</td>
											<td>" . $d->nama_obat . "</td>
											<td>" . $d->jumlah . "</td>
											<td>" . $d->satuan . "</td>
											<td>" . ArrayAdapter::format("money Rp. ",  $hna) . "</td>
											<td>" . ArrayAdapter::format("money Rp. ", $total_hna) . "</td>
											<td>" . ArrayAdapter::format("date d-m-Y", $d->tanggal_exp) . "</td>
										</tr>";
						$total += $total_hna;
					}
				} else {
					$print_data .= "<tr>
										<td colspan='9' align='center'><i>Tidak terdapat data mutasi obat</i></td>
									</tr>";
				}
				$print_data .= "<tr>
									<td colspan='7' align='center'><b>T O T A L</b></td>
									<td><b>Rp. " . ArrayAdapter::format("only-money", $total) . "</b></td>
									<td>&nbsp;</td>
								</tr>";
				$print_data .= "</table><br/>";
				global $user;
				$print_data .= "<table border='0' align='center'>
									<tr>
										<td align='center'>" . ArrayAdapter::format("date d M Y", date("Y-m-d")) . "</td>
									</tr>
									<tr>
										<td align='center'>PETUGAS GUDANG FARMASI</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&Tab;</td>
									</tr>
									<tr>
										<td>&Tab;</td>
									</tr>
									<tr>
										<td>&Tab;</td>
									</tr>
									<tr>
										<td align='center'><b>" . $user->getNameOnly() . "</b></td>
									</tr>
								</table>";
				return $print_data;
			}
		}
		$lok_dbresponder = new LOKDBResponder(
			$lok_dbtable,
			$lok_table,
			$lok_adapter
		);
		$data = $lok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	echo $laporan_form->getHtml();
	echo "<div id='table_content'>";
	echo $lok_table->getHtml();
	echo "</div>";
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function LOKAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	LOKAction.prototype.constructor = LOKAction;
	LOKAction.prototype = new TableAction();
	LOKAction.prototype.view = function() {
		var self = this;
		var data = this.getViewData();
		data['tanggal_from'] = $("#lok_tanggal_from").val();
		data['tanggal_to'] = $("#lok_tanggal_to").val();
		showLoading();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#" + self.prefix + "_list").html(json.list);
				$("#" + self.prefix + "_pagination").html(json.pagination);
				dismissLoading();
			}
		);
	};
	LOKAction.prototype.print = function() {
		if ($("#lok_tanggal_from").val() == "" || $("#lok_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "print_laporan";
		data['tanggal_from'] = $("#lok_tanggal_from").val();
		data['tanggal_to'] = $("#lok_tanggal_to").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				smis_print(json);
			}
		);
	};
	LOKAction.prototype.export_xls = function() {
		if ($("#lok_tanggal_from").val() == "" || $("#lok_tanggal_to").val() == "")
			return;
		var data = this.getRegulerData();
		data['command'] = "export_xls";
		data['tanggal_from'] = $("#lok_tanggal_from").val();
		data['tanggal_to'] = $("#lok_tanggal_to").val();
		postForm(data);
	};
	
	var lok;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#lok_max").val(50);
		lok = new LOKAction(
			"lok",
			"gudang_farmasi",
			"laporan_obat_keluar_non_apotek",
			new Array()
		)
		$('.mydate').datepicker();
		$("#lok_pagination").html("");
	});
</script>