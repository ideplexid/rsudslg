<?php
	require_once("smis-framework/smis/api/SettingsBuilder.php");
	global $db;
	
	$settings_builder = new SettingsBuilder($db, "gudang_farmasi_settings", "gudang_farmasi", "settings");
	$settings_builder->setShowDescription(true);
	$settings_builder->setTabulatorMode(Tabulator::$POTRAIT);
	
	$settings_builder->addTabs("penerimaan_obat", "Penerimaan Obat (BBM - OPL)");
	$settings_item = new SettingsItem($db, "no_bbm", "No. BBM ", 0, "text", "Nomor Bukti Barang Masuk (BBM)");
	$settings_builder->addItem("penerimaan_obat", $settings_item);

	$settings_builder->addTabs("warning_system", "Sistem Peringatan");
	$waning_system_option = new OptionBuilder();
	$waning_system_option->add("Tidak Aktif", "0", "1");
	$waning_system_option->add("Aktif", "1");
	$settings_item = new SettingsItem($db, "gudang_farmasi-bbm-warning_system_active", "Sistem Peringatan", $waning_system_option->getContent(), "select", "Aktif/Tidak Aktif Sistem Peringatan (Default : Tidak Aktif)");
	$settings_builder->addItem("warning_system", $settings_item);
	$settings_item = new SettingsItem($db, "gudang_farmasi-bbm-delta_hna", "Ambang Batas Perbedaan Harga (%)", 10, "text", "Persentase Perbedaan HNA (Default : 10)");
	$settings_builder->addItem("warning_system", $settings_item);
	$strict_option = new OptionBuilder();
	$strict_option->add("Tidak", "0", "1");
	$strict_option->add("Ya", "1");
	$settings_item = new SettingsItem($db, "gudang_farmasi-bbm-strict_mode", "Mode Strict", $strict_option->getContent(), "select", "Mode Strict Sistem Peringatan (Default : Tidak)");
	$settings_builder->addItem("warning_system", $settings_item);

	$settings_builder->addTabs("obat_keluar", "Obat Keluar");
	$settings_item = new SettingsItem($db, "no_mutasi", "No. Mutasi ", 0, "text", "Nomor Mutasi");
	$settings_builder->addItem("obat_keluar", $settings_item);

	$settings_builder->addTabs("laporan_pelayanan_farmasi", "Lap. Pelayanan Farmasi");
	$settings_item = new SettingsItem($db, "gudang_farmasi-nama_ka_farmasi", "Nama Ka. Farmasi", "", "text", "Nama Ka. Instalasi Farmasi");
	$settings_builder->addItem("laporan_pelayanan_farmasi", $settings_item);
	$settings_item = new SettingsItem($db, "gudang_farmasi-jumlah_apoteker", "Jumlah Apoteker", "0", "text", "Jumlah Apoteker");
	$settings_builder->addItem("laporan_pelayanan_farmasi", $settings_item);
	
	$response = $settings_builder->init();
?>