<?php 
	require_once("smis-base/smis-include-service-consumer.php");;
	global $notification;
	
	$service_consumer = new ServiceConsumer($db, "get_minimum_stok_obat_status");
	$service_consumer->setMode(ServiceConsumer::$CLEAN_AUTONOMOUS);
	$service_consumer->execute();
	$unit_status = $service_consumer->getContent();
	$num = 1;
	$unit_minimum = "";
	foreach($unit_status as $unit=>$value) {
		$status = $value[0];
		if ($status && $num <= 3) {
			if ($num > 1)
				$unit_minimum .= ", ";
			$unit_minimum .= ArrayAdapter::format("unslug", $unit);
			$num++;
		}
	}
	$message = $unit_minimum;
	if ($num > 3) {
		$message .= ", dll";
	}
	$message = "Stok pada unit " . $message . " kurang dari batas minimum stok.";
	$type = "Stok Minimum";
	$code = "gudang_farmasi-stok-minimum-" . date("Y-m-d") . $message;
	if ($unit_minimum != "")
		$notification->addNotification($type, md5($code), $message, "gudang_farmasi", "pengecekan_stok_minimum_unit");
?>