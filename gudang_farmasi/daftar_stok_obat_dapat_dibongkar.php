<?php 
	class StokObatBTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Ubah");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("icon-edit icon-white");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$stok_obat_table = new StokObatBTable(
		array("Nomor", "Kode Obat", "Nama Obat", "Jenis Obat", "Jenis Stok", "Produsen", "Vendor", "Sisa", "Harga Satuan", "Ket. Jumlah", "Tgl. Exp.", "No. Batch"),
		"",
		null,
		true
	);
	$stok_obat_table->setName("stok_obat_b");
	$stok_obat_table->setReloadButtonEnable(false);
	$stok_obat_table->setPrintButtonEnable(false);
	$stok_obat_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class StokObatAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Nomor'] = self::format("digit8", $row->id);
				$array['Kode Obat'] = $row->kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Jenis Stok'] = self::format("unslug", $row->label);
				$array['Produsen'] = $row->produsen;
				$array['Vendor'] = $row->nama_vendor;
				$array['Sisa'] = $row->sisa . " " . $row->satuan;
				$array['Harga Satuan'] = self::format("money Rp.", $row->hna);
				$array['Ket. Jumlah'] = "1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi;
				if ($row->tanggal_exp == "0000-00-00")
					$array['Tgl. Exp.'] = "-";
				else
					$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				if ($row->no_batch == null)
					$array['No. Batch'] = "-";
				else
					$array['No. Batch'] = $row->no_batch;
				return $array;
			}
		}
		$stok_obat_adapter = new StokObatAdapter();
		$columns = array("id", "id_dobat_masuk", "kode_obat", "nama_obat", "nama_jenis_obat", "label", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "id_vendor", "nama_vendor", "produsen");
		$stok_obat_dbtable = new DBTable(
			$db,
			"smis_fr_stok_obat",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_fr_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_vendor LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.produsen LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT *
			FROM smis_fr_stok_obat
			WHERE sisa > 0 AND konversi != 1 AND prop NOT LIKE 'del' " . $filter . "
		";
		$query_count = "
			SELECT COUNT(*)
			FROM smis_fr_stok_obat
			WHERE sisa > 0 AND konversi != 1 AND prop NOT LIKE 'del' " . $filter . "
		";
		$stok_obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class StokObatDBResponder extends DBResponder {
			public function save() {				
				//update stok lama:
				$data_lama['sisa'] = $_POST['sisa'];
				$data_lama_id['id'] = $_POST['id'];
				$result = $this->dbtable->update($data_lama, $data_lama_id);
				//logging riwayat stok lama:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_riwayat_stok_obat");
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $_POST['id'];
				$data_riwayat['jumlah_keluar'] = $_POST['jumlah'];
				$data_riwayat['sisa'] = $_POST['sisa'];
				$data_riwayat['keterangan'] = "Stok Dibongkar";
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$riwayat_dbtable->insert($data_riwayat);
				//insert stok baru:
				$data_baru['id_dobat_masuk'] = $_POST['id_dobat_masuk'];
				$data_baru['kode_obat'] = $_POST['kode_obat'];
				$data_baru['nama_obat'] = $_POST['nama_obat'];
				$data_baru['nama_jenis_obat'] = $_POST['nama_jenis_obat'];
				$data_baru['label'] = $_POST['label'];
				$data_baru['formularium'] = $_POST['formularium'];
				$data_baru['generik'] = $_POST['generik'];
				$data_baru['berlogo'] = $_POST['berlogo'];
				$data_baru['jumlah'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_baru['sisa'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_baru['satuan'] = $_POST['satuan'];
				$data_baru['konversi'] = "1";
				$data_baru['satuan_konversi'] = $_POST['satuan'];
				$data_baru['hna'] = $_POST['hna'];
				$data_baru['tanggal_exp'] = $_POST['tanggal_exp'];
				$data_baru['no_batch'] = $_POST['no_batch'];
				$data_baru['id_vendor'] = $_POST['id_vendor'];
				$data_baru['nama_vendor'] = $_POST['nama_vendor'];
				$data_baru['produsen'] = $_POST['produsen'];
				$data_baru['turunan'] = "1";
				$this->dbtable->insert($data_baru);
				$id_stok_baru = $this->dbtable->get_inserted_id();
				//logging riwayat stok baru:
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $id_stok_baru;
				$data_riwayat['jumlah_masuk'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_riwayat['sisa'] = $_POST['jumlah'] * $_POST['konversi'];
				$data_riwayat['keterangan'] = "Hasil Bongkar Stok";
				$data_riwayat['nama_user'] = $user->getName();
				$riwayat_dbtable->insert($data_riwayat);
				$success['type'] = "update";
				$success['id'] = $data_lama_id['id'];
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
		}
		$stok_obat_dbresponder = new StokObatDBResponder(
			$stok_obat_dbtable,
			$stok_obat_table,
			$stok_obat_adapter
		);
		$data = $stok_obat_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$stok_obat_modal = new Modal("stok_obat_b_add_form", "smis_form_container", "stok_obat_b");
	$stok_obat_modal->setTitle("Data Stok Obat");
	$id_hidden = new Hidden("stok_obat_b_id", "stok_obat_b_id", "");
	$stok_obat_modal->addElement("", $id_hidden);
	$id_dobat_masuk_hidden = new Hidden("stok_obat_b_id_dobat_masuk", "stok_obat_b_id_dobat_masuk", "");
	$stok_obat_modal->addElement("", $id_dobat_masuk_hidden);
	$formularium_hidden = new Hidden("stok_obat_b_formularium", "stok_obat_b_formularium", "");
	$stok_obat_modal->addElement("", $formularium_hidden);
	$berlogo_hidden = new Hidden("stok_obat_b_berlogo", "stok_obat_b_berlogo", "");
	$stok_obat_modal->addElement("", $berlogo_hidden);
	$generik_hidden = new Hidden("stok_obat_b_generik", "stok_obat_b_generik", "");
	$stok_obat_modal->addElement("", $generik_hidden);
	$kode_obat_text = new Text("stok_obat_b_kode_obat", "stok_obat_b_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$stok_obat_modal->addElement("Kode Obat", $kode_obat_text);
	$nama_obat_text = new Text("stok_obat_b_nama_obat", "stok_obat_b_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$stok_obat_modal->addElement("Nama Obat", $nama_obat_text);
	$nama_jenis_obat_text = new Text("stok_obat_b_nama_jenis_obat", "stok_obat_b_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$stok_obat_modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	$label_hidden = new Hidden("stok_obat_b_label", "stok_obat_b_label", "");
	$stok_obat_modal->addElement("", $label_hidden);
	$sisa_hidden = new Hidden("stok_obat_b_sisa", "stok_obat_b_sisa", "");
	$stok_obat_modal->addElement("", $sisa_hidden);
	$satuan_hidden = new Hidden("stok_obat_b_satuan", "stok_obat_b_satuan", "");
	$stok_obat_modal->addElement("", $satuan_hidden);
	$konversi_hidden = new Hidden("stok_obat_b_konversi", "stok_obat_b_konversi", "");
	$stok_obat_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("stok_obat_b_satuan_konversi", "stok_obat_b_satuan_konversi", "");
	$stok_obat_modal->addElement("", $satuan_konversi_hidden);
	$hna_hidden = new Hidden("stok_obat_b_hna", "stok_obat_b_hna", "");
	$stok_obat_modal->addElement("", $hna_hidden);
	$tanggal_exp_hidden = new Hidden("stok_obat_b_tanggal_exp", "stok_obat_b_tanggal_exp", "");
	$stok_obat_modal->addElement("", $tanggal_exp_hidden);
	$no_batch_hidden = new Hidden("stok_obat_b_no_batch", "stok_obat_b_no_batch", "");
	$stok_obat_modal->addElement("", $no_batch_hidden);
	$produsen_text = new Text("stok_obat_b_produsen", "stok_obat_b_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$stok_obat_modal->addElement("Produsen", $produsen_text);
	$id_vendor_hidden = new Hidden("stok_obat_b_id_vendor", "stok_obat_b_id_vendor", "");
	$stok_obat_modal->addElement("", $id_vendor_hidden);
	$nama_vendor_text = new Text("stok_obat_b_nama_vendor", "stok_obat_b_nama_vendor", "");
	$nama_vendor_text->setAtribute("disabled='disabled'");
	$stok_obat_modal->addElement("Vendor", $nama_vendor_text);
	$f_sisa_text = new Text("stok_obat_b_f_sisa", "stok_obat_b_f_sisa", "");
	$f_sisa_text->setAtribute("disabled='disabled'");
	$stok_obat_modal->addElement("Sisa", $f_sisa_text);
	$keterangan_text = new Text("stok_obat_b_keterangan", "stok_obat_b_keterangan", "");
	$keterangan_text->setAtribute("disabled='disabled'");
	$stok_obat_modal->addElement("Keterangan", $keterangan_text);
	$jumlah_text = new Text("stok_obat_b_jumlah", "stok_obat_b_jumlah", "");
	$stok_obat_modal->addElement("Jumlah", $jumlah_text);
	$stok_obat_button = new Button("", "", "Simpan");
	$stok_obat_button->setClass("btn-success");
	$stok_obat_button->setAction("stok_obat_b.save()");
	$stok_obat_button->setIcon("fa fa-floppy-o");
	$stok_obat_button->setIsButton(Button::$ICONIC);
	$stok_obat_modal->addFooter($stok_obat_button);
	
	echo $stok_obat_modal->getHtml();
	echo $stok_obat_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");	
?>
<script type="text/javascript">
	function StokObatBAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	StokObatBAction.prototype.constructor = StokObatBAction;
	StokObatBAction.prototype = new TableAction();
	StokObatBAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#stok_obat_b_id").val(json.id);
				$("#stok_obat_b_id_dobat_masuk").val(json.id_dobat_masuk);
				$("#stok_obat_b_formularium").val(json.formularium);
				$("#stok_obat_b_berlogo").val(json.berlogo);
				$("#stok_obat_b_generik").val(json.generik);
				$("#stok_obat_b_kode_obat").val(json.kode_obat);
				$("#stok_obat_b_nama_obat").val(json.nama_obat);
				$("#stok_obat_b_nama_jenis_obat").val(json.nama_jenis_obat);
				$("#stok_obat_b_label").val(json.label);
				$("#stok_obat_b_sisa").val(json.sisa);
				$("#stok_obat_b_satuan").val(json.satuan);
				$("#stok_obat_b_konversi").val(json.konversi);
				$("#stok_obat_b_satuan_konversi").val(json.satuan_konversi);
				$("#stok_obat_b_hna").val(json.hna);
				$("#stok_obat_b_tanggal_exp").val(json.tanggal_exp);
				$("#stok_obat_b_no_batch").val(json.no_batch);
				$("#stok_obat_b_produsen").val(json.produsen);
				$("#stok_obat_b_id_vendor").val(json.id_vendor);
				$("#stok_obat_b_nama_vendor").val(json.nama_vendor);
				$("#stok_obat_b_f_sisa").val(json.sisa + " " + json.satuan);
				$("#stok_obat_b_keterangan").val("1 " + json.satuan + " = " + json.konversi + " " + json.satuan_konversi);
				$("#stok_obat_b_jumlah").val("");
				$("#modal_alert_stok_obat_b_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#stok_obat_b_add_form").smodal("show");
			}
		);
	};
	StokObatBAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var sisa = $("#stok_obat_b_sisa").val();
		var jumlah = $("#stok_obat_b_jumlah").val();
		$(".error_field").removeClass("error_field");
		if (jumlah == "") {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
			$("#stok_obat_b_jumlah").addClass("error_field");
		} else if (!is_numeric(jumlah)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
			$("#stok_obat_b_jumlah").addClass("error_field");
		} else if (parseFloat(jumlah) > parseFloat(sisa)) {
			valid = false;
			invalid_msg += "</br><strong>Jumlah</strong> melebihi sisa";
			$("#stok_obat_b_jumlah").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_stok_obat_b_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	StokObatBAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#stok_obat_b_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = $("#stok_obat_b_id").val();
		data['id_dobat_masuk'] = $("#stok_obat_b_id_dobat_masuk").val();
		data['kode_obat'] = $("#stok_obat_b_kode_obat").val();
		data['nama_obat'] = $("#stok_obat_b_nama_obat").val();
		data['nama_jenis_obat'] = $("#stok_obat_b_nama_jenis_obat").val();
		data['label'] = $("#stok_obat_b_label").val();
		data['jumlah'] = parseFloat($("#stok_obat_b_jumlah").val());
		data['konversi'] = parseFloat($("#stok_obat_b_konversi").val());
		data['sisa'] = parseFloat($("#stok_obat_b_sisa").val()) - parseFloat($("#stok_obat_b_jumlah").val());
		data['satuan'] = $("#stok_obat_b_satuan_konversi").val();
		data['hna'] = parseFloat($("#stok_obat_b_hna").val()) / parseFloat($("#stok_obat_b_konversi").val());
		data['produsen'] = $("#stok_obat_b_produsen").val();
		data['tanggal_exp'] = $("#stok_obat_b_tanggal_exp").val();
		data['no_batch'] = $("#stok_obat_b_no_batch").val();
		data['formularium'] = $("#stok_obat_b_formularium").val();
		data['berlogo'] = $("#stok_obat_b_berlogo").val();
		data['generik'] = $("#stok_obat_b_generik").val();
		data['id_vendor'] = $("#stok_obat_b_id_vendor").val();
		data['nama_vendor'] = $("#stok_obat_b_nama_vendor").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#stok_obat_b_add_form").smodal("show");
				} else {
					self.view();
					stok_obat_t.view();
				}
				dismissLoading();
			}
		);
	};
	
	var stok_obat_b;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#stok_obat_b_max").val(50);
		$(".mydate").datepicker();
		var stok_obat_b_columns = new Array("id", "formularium", "generik", "berlogo", "id_dobat_masuk", "kode_obat", "nama_obat", "nama_jenis_obat", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "tanggal_exp", "no_batch", "id_vendor", "nama_vendor", "produsen");
		stok_obat_b = new StokObatBAction(
			"stok_obat_b",
			"gudang_farmasi",
			"daftar_stok_obat_dapat_dibongkar",
			stok_obat_b_columns
		);
		stok_obat_b.view();
	});
</script>