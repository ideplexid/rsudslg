<?php
class StokDBResponder extends DBResponder {
	public function edit() {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$data = $this->dbtable->get_row("
			SELECT smis_fr_dobat_f_masuk.id_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, SUM(smis_fr_stok_obat.sisa) AS 'sisa', smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi
			FROM smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id
			WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_dobat_f_masuk.id_obat = '" . $id_obat . "' AND smis_fr_stok_obat.satuan = '" . $satuan . "' AND smis_fr_dobat_f_masuk.konversi = '" . $konversi . "' AND smis_fr_stok_obat.satuan_konversi = '" . $satuan_konversi . "'
			GROUP BY smis_fr_dobat_f_masuk.id_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi
		");
		return $data;
	}
}
?>