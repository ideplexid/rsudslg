<?php
	class PermintaanObatKeluarDBResponder extends DBResponder {
		public function edit() {
			$id = $_POST['id'];

			$header_row = $this->dbtable->get_row("
				SELECT *
				FROM smis_fr_permintaan_obat_unit
				WHERE id = '" . $id . "'
			");

			$detail_rows = $this->dbtable->get_result("
				SELECT *
				FROM smis_fr_dpermintaan_obat_unit
				WHERE id_permintaan_obat = '" . $id . "' AND prop = ''
			");

			$detail_html = "";
			$no = 1;
			if ($detail_rows != null) {
				foreach ($detail_rows as $dr) {
					$action_html = "";
					if ($dr->id_obat > 0)
						$action_html = "<div class='btn-group noprint'>" .
								"<a href='#' onclick='dmutasi_obat_keluar.add(" . $no . ")' data-content='Ubah' data-toggle='popover' class='input btn btn-info'>" .
									"<i class='fa fa-plus'></i>" .
								"</a>" .
							"</div>";
					$detail_html .= "
						<tr id='data_" . $no . "' class='detail'>
							<td id='id_dpermintaan_obat_unit' style='display: none;'>" . $dr->id . "</td>
							<td id='id_obat' style='display: none;'>" . $dr->id_obat . "</td>
							<td id='jumlah_diminta' style='display: none;'>" . $dr->jumlah_permintaan . "</td>
							<td id='jumlah_dipenuhi' style='display: none;'>" . $dr->jumlah_dipenuhi . "</td>
							<td id='sisa_dipenuhi' style='display: none;'>" . ($dr->jumlah_permintaan - $dr->jumlah_dipenuhi) . "</td>
							<td id='satuan' style='display: none;'>" . $dr->satuan_permintaan . "</td>
							<td id='nomor'><strong>" . $no . "</strong></td>
							<td id='kode'><strong>" . $dr->kode_obat . "</strong></td>
							<td id='nama_obat'><strong>" . $dr->nama_obat . "</strong></td>
							<td id='nama_jenis_obat'><strong>" . $dr->nama_jenis_obat . "</strong></td>
							<td id='f_jumlah_permintaan'><strong>" . $dr->jumlah_permintaan . " " . $dr->satuan_permintaan . "</strong></td>
							<td id='f_jumlah_dipenuhi'><strong>0 " . $dr->satuan_permintaan . "</strong></td>
							<td>" .
								$action_html .		
							"</td>" .
						"</tr>" .
						"<tr id='detail_data_" . $no . "'>" .
							"<td colspan='7' style='display: none;'></td>" .
						"</tr>";
					$no++;
				}
			}

			$data = array(
				'header' 		=> $header_row,
				'detail_html'	=> $detail_html
			);
			return $data;
		}
	}
?>