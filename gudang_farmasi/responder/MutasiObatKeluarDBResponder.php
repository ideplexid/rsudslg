<?php
	require_once("smis-base/smis-include-duplicate.php");
	require_once("gudang_farmasi/service_consumer/SetPermintaanObatStatusServiceConsumer.php");
	require_once("gudang_farmasi/service_consumer/PushObatServiceConsumer.php");

	class MutasiObatKeluarDBResponder extends DuplicateResponder {
		public function save() {
			$mutasi_obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_mutasi_obat_keluar");
			$dmutasi_obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_dmutasi_obat_keluar");
			$stok_mutasi_obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_mutasi_obat_keluar");
			$obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_obat_keluar");
			$dobat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_dobat_keluar");
			$stok_obat_keluar_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_obat_keluar");
			$permintaan_obat_unit_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_permintaan_obat_unit");
			$dpermintaan_obat_unit_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_dpermintaan_obat_unit");
			$stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_obat");
			$kartu_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_kartu_stok");
			$riwayat_stok_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_riwayat_stok_obat");

			// 1.a. Insert Header of Obat Keluar Unit :
			$no_mutasi = getSettings($this->dbtable->get_db(), "no_mutasi", 0) + 1;
			setSettings($this->dbtable->get_db(), "no_mutasi", $no_mutasi);

			$data_obat_keluar = array(
				'tanggal' 		=> date("Y-m-d H:i:s"),
				'nomor'			=> $no_mutasi,
				'unit'			=> $_POST['unit'],
				'status'		=> "belum",
				'keterangan'	=> "Mutasi Obat Keluar dari Permintaan"
			);
			$obat_keluar_dbtable->insert($data_obat_keluar);
			$id_obat_keluar = $obat_keluar_dbtable->get_inserted_id();

			// 1.b. Update Header of Permintaan Obat Unit
			$data_permintaan_obat_unit = array(
				'status'	=> "sudah"
			);
			$id['id'] = $_POST['f_id_permintaan_obat_unit'];
			$permintaan_obat_unit_dbtable->update($data_permintaan_obat_unit, $id);

			// 1.c. Insert Header of Mutasi Obat Keluar
			$data_mutasi_obat_keluar = array(
				'f_id_permintaan_obat_unit'	=> $_POST['f_id_permintaan_obat_unit'],
				'f_id_obat_keluar'			=> $id_obat_keluar,
				'tanggal_permintaan'		=> $_POST['tanggal_permintaan'],
				'tanggal_mutasi'			=> date("Y-m-d H:i:s"),
				'unit'						=> $_POST['unit']
			);
			$mutasi_obat_keluar_dbtable->insert($data_mutasi_obat_keluar);
			$id_mutasi_obat_keluar = $mutasi_obat_keluar_dbtable->get_inserted_id();

			$detail = $_POST['detail'];
			if (count($detail) > 0) {
				foreach ($detail as $d) {
					$stok_row = $stok_dbtable->get_row("
						SELECT SUM(b.sisa) AS 'jumlah'
						FROM smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_stok_obat b ON a.id = b.id_dobat_masuk
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $d['id_obat'] . "'
					");

					$harga_ma_row = $stok_dbtable->get_row("
						SELECT id_obat, satuan, konversi, satuan_konversi, COUNT(*) AS 'jumlah', AVG(hna) AS 'harga_ma'
						FROM (
							SELECT DISTINCT smis_fr_dobat_f_masuk.id_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, smis_fr_stok_obat.hna
							FROM smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id
							WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_dobat_f_masuk.id_obat = '" . $d['id_obat'] . "' AND smis_fr_stok_obat.satuan = '" . $d['satuan'] . "' AND smis_fr_stok_obat.konversi = '" . $d['konversi'] . "' AND smis_fr_stok_obat.satuan_konversi = '" . $d['satuan_konversi'] . "'
						) v_ma
						GROUP BY id_obat, satuan, konversi, satuan_konversi
					");

					// 2.a. Insert Detail of Obat Keluar Unit :
					$detail_data = array();
					$detail_data['id_obat_keluar'] = $id_obat_keluar;
					$detail_data['id_obat'] = $d['id_obat'];
					$detail_data['kode_obat'] = $d['kode_obat'];
					$detail_data['nama_obat'] = $d['nama_obat'];
					$detail_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_data['stok_entri'] = $stok_row->jumlah;
					$detail_data['harga_ma'] = $harga_ma_row->harga_ma;
					$detail_data['jumlah_diminta'] = $d['jumlah_diminta'];
					$detail_data['jumlah'] = $d['jumlah_dipenuhi'];
					$detail_data['satuan'] = $d['satuan'];
					$detail_data['konversi'] = 1;
					$detail_data['satuan_konversi'] = $d['satuan'];
					$dobat_keluar_dbtable->insert($detail_data);
					$id_dobat_keluar = $dobat_keluar_dbtable->get_inserted_id();

					// 2.b. Insert Kartu Stok of Obat Keluar Unit :
					$ks_data = array();
					$ks_data['f_id'] = $id_dobat_keluar;
					$ks_data['unit'] = ArrayAdapter::format("unslug", $_POST['unit']);
					$ks_data['no_bon'] = ArrayAdapter::format("only-digit6", $id_obat_keluar);
					$ks_data['id_obat'] = $d['id_obat'];
					$ks_data['kode_obat'] = $d['kode_obat'];
					$ks_data['nama_obat'] = $d['nama_obat'];
					$ks_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$ks_data['tanggal'] = date("Y-m-d");
					$ks_data['masuk'] = 0;
					$ks_data['keluar'] = $d['jumlah_dipenuhi'];
					$ks_data['sisa'] = $stok_row->jumlah - $d['jumlah_dipenuhi'];
					$kartu_stok_dbtable->insert($ks_data);

					// 2.c. Insert Detail Mutasi Obat Keluar
					$detail_mutasi_obat_keluar_data = array();
					$detail_mutasi_obat_keluar_data['id_mutasi_obat_keluar'] = $id_mutasi_obat_keluar;
					$detail_mutasi_obat_keluar_data['f_id_dpermintaan_obat_unit'] = $d['id_dpermintaan_obat_unit'];
					$detail_mutasi_obat_keluar_data['f_id_dobat_keluar'] = $id_dobat_keluar;
					$detail_mutasi_obat_keluar_data['id_obat'] = $d['id_obat'];
					$detail_mutasi_obat_keluar_data['kode_obat'] = $d['kode_obat'];
					$detail_mutasi_obat_keluar_data['nama_obat'] = $d['nama_obat'];
					$detail_mutasi_obat_keluar_data['nama_jenis_obat'] = $d['nama_jenis_obat'];
					$detail_mutasi_obat_keluar_data['jumlah_diminta'] = $d['jumlah_diminta'];
					$detail_mutasi_obat_keluar_data['jumlah_dipenuhi'] = $d['jumlah_dipenuhi'];
					$detail_mutasi_obat_keluar_data['satuan'] = $d['satuan'];
					$detail_mutasi_obat_keluar_data['konversi'] = $d['konversi'];
					$detail_mutasi_obat_keluar_data['satuan_konversi'] = $d['satuan_konversi'];
					$dmutasi_obat_keluar_dbtable->insert($detail_mutasi_obat_keluar_data);
					$id_dmutasi_obat_keluar = $dmutasi_obat_keluar_dbtable->get_inserted_id();

					if (isset($d['stok'])) {
						$stok = $d['stok'];
						if (count($stok) > 0) {
							foreach ($stok as $s) {							
								// 3.a. Insert Stok Pakai Obat Keluar
								$stok_obat_keluar_data = array();
								$stok_obat_keluar_data['id_dobat_keluar'] = $id_dobat_keluar;
								$stok_obat_keluar_data['id_stok_obat'] = $s['id_stok_obat'];
								$stok_obat_keluar_data['jumlah'] = $s['jumlah'];
								$stok_obat_keluar_dbtable->insert($stok_obat_keluar_data);
								$id_stok_obat_keluar = $stok_obat_keluar_dbtable->get_inserted_id();

								// 3.b. Update Stok and Insert Riwayat Stok Obat
								$id['id'] = $s['id_stok_obat'];
								$stok_row = $stok_dbtable->select($id);
								$sisa = $stok_row->sisa - $s['jumlah'];

								$stok_data = array();
								$stok_data['sisa'] = $sisa;
								$stok_dbtable->update($stok_data, $id);

								$riwayat_stok_data = array();
								$riwayat_stok_data['tanggal'] = date("Y-m-d");
								$riwayat_stok_data['id_stok_obat'] = $s['id_stok_obat'];
								$riwayat_stok_data['jumlah_masuk'] = 0;
								$riwayat_stok_data['jumlah_keluar'] = $s['jumlah'];
								$riwayat_stok_data['sisa'] = $sisa;
								$riwayat_stok_data['keterangan'] = "Mutasi Atas Permintaan Unit " . ArrayAdapter::format("unslug", $_POST['unit']) . " ID : " . $id_obat_keluar;
								global $user;
								$riwayat_stok_data['nama_user'] = $user->getName();
								$riwayat_stok_dbtable->insert($riwayat_stok_data);

								// 3.c. Insert Stok Pakai Mutasi Obat Keluar
								$stok_mutasi_obat_keluar_data = array();
								$stok_mutasi_obat_keluar_data['id_dmutasi_obat_keluar'] = $id_dmutasi_obat_keluar;
								$stok_mutasi_obat_keluar_data['f_id_stok_obat_keluar'] = $id_stok_obat_keluar;
								$stok_mutasi_obat_keluar_data['id_stok_obat'] = $s['id_stok_obat'];
								$stok_mutasi_obat_keluar_data['jumlah'] = $s['jumlah'];
								$stok_mutasi_obat_keluar_dbtable->insert($stok_mutasi_obat_keluar_data);
							}
						}
					}

					// 2.d Update Detail of Permintaan Obat Unit :
					$id['id'] = $d['id_dpermintaan_obat_unit'];
					$dpermintaan_obat_unit_data = array(
						'jumlah_dipenuhi'	=> $d['jumlah_dipenuhi'],
						'satuan_dipenuhi'	=> $d['satuan']
					);
					$dpermintaan_obat_unit_dbtable->update($dpermintaan_obat_unit_data, $id);
				}
			}

			// 4. Push Permintaan Data and Mutasi Data
			$detail_obat_keluar = $stok_obat_keluar_dbtable->get_result("
				SELECT smis_fr_stok_obat_keluar.id AS 'id_stok_obat_keluar', smis_fr_dobat_keluar.id_obat, smis_fr_dobat_keluar.kode_obat, smis_fr_dobat_keluar.nama_obat, smis_fr_dobat_keluar.nama_jenis_obat, smis_fr_stok_obat.formularium, smis_fr_stok_obat.berlogo, smis_fr_stok_obat.generik, smis_fr_stok_obat.label, smis_fr_stok_obat.id_vendor, smis_fr_stok_obat.nama_vendor, smis_fr_stok_obat_keluar.jumlah, smis_fr_dobat_keluar.satuan, smis_fr_dobat_keluar.konversi, smis_fr_dobat_keluar.satuan_konversi, smis_fr_stok_obat.hna, smis_fr_stok_obat.produsen, smis_fr_stok_obat.tanggal_exp, smis_fr_stok_obat.no_batch, smis_fr_stok_obat_keluar.prop
				FROM (smis_fr_stok_obat_keluar LEFT JOIN smis_fr_stok_obat ON smis_fr_stok_obat_keluar.id_stok_obat = smis_fr_stok_obat.id) LEFT JOIN smis_fr_dobat_keluar ON smis_fr_stok_obat_keluar.id_dobat_keluar = smis_fr_dobat_keluar.id
				WHERE smis_fr_dobat_keluar.id_obat_keluar = '" . $id_obat_keluar . "'
			");
			$push_obat_service_consumer = new PushObatServiceConsumer(
				$this->dbtable->get_db(), 
				$id_obat_keluar, 
				$data_obat_keluar['tanggal'], 
				$data_obat_keluar['unit'], 
				$data_obat_keluar['status'], 
				$data_obat_keluar['keterangan'], 
				$detail_obat_keluar, 
				"push_save"
			);
			$push_obat_service_consumer->execute();

			$detail_permintaan = $dpermintaan_obat_unit_dbtable->get_result("
				SELECT *
				FROM smis_fr_dpermintaan_obat_unit
				WHERE id_permintaan_obat = '" . $data_mutasi_obat_keluar['f_id_permintaan_obat_unit'] . "' AND prop NOT LIKE 'del'
			");
			$set_status_permintaan_obat_service_consumer = new SetPermintaanObatStatusServiceConsumer(
				$this->dbtable->get_db(), 
				$_POST['f_id'], 
				$data_obat_keluar['unit'], 
				"sudah", 
				$detail_permintaan, 
				"push_update"
			);
			$set_status_permintaan_obat_service_consumer->execute();

			$success['id'] = $id['id'];
			$success['success'] = 1;
			$success['message'] = "Success";
			return $success;
		}
	}
?>