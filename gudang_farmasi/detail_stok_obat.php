<?php 
	require_once("smis-base/smis-include-duplicate.php");

	class PenyesuaianStokTable extends Table {
		public function getContentButton($id) {
			$btn_group = new ButtonGroup("noprint");
			$btn = new Button("", "", "Ubah");
			$btn->setAction($this->action . ".edit('" . $id . "')");
			$btn->setClass("btn-warning");
			$btn->setAtribute("data-content='Ubah' data-toggle='popover'");
			$btn->setIcon("icon-white icon-edit");
			$btn->setIsButton(Button::$ICONIC);
			$btn_group->addElement($btn);
			return $btn_group;
		}
	}
	$penyesuaian_stok_table = new PenyesuaianStokTable(
		array("Kode Obat", "Nama Obat", "Jenis Obat", "Jenis Stok", "Stok", "Produsen", "Tgl. Exp.", "No. BBM", "Tgl. Masuk"),
		"",
		null,
		true
	);
	$penyesuaian_stok_table->setName("penyesuaian_stok");
	$penyesuaian_stok_table->setAddButtonEnable(false);
	
	if (isset($_POST['command'])) {
		class PenyesuaianStokAdapter extends ArrayAdapter {
			public function adapt($row) {
				$array = array();
				$array['id'] = $row->id;
				$array['Kode Obat'] = $row->kode_obat;
				$array['Nama Obat'] = $row->nama_obat;
				$array['Jenis Obat'] = $row->nama_jenis_obat;
				$array['Jenis Stok'] = self::format("unslug", $row->label);
				$array['Produsen'] = $row->produsen;
				if ($row->konversi == 1)
					$array['Stok'] = $row->sisa . " " . $row->satuan;
				else
					$array['Stok'] = $row->sisa . " " . $row->satuan . " (1 " . $row->satuan . " = " . $row->konversi . " " . $row->satuan_konversi . ")";
				if ($row->tanggal_exp == "0000-00-00")
					$array['Tgl. Exp.'] = "-";
				else
					$array['Tgl. Exp.'] = self::format("date d M Y", $row->tanggal_exp);
				$array['No. BBM'] = $row->no_bbm;
				if ($row->tanggal_datang == null || $row->tanggal_datang == "0000-00-00")
					$array['Tgl. Masuk'] = "N/A";	
				else
					$array['Tgl. Masuk'] = ArrayAdapter::format("date d-m-Y", $row->tanggal_datang);
				return $array;
			}
		}
		$penyesuaian_stok_adapter = new PenyesuaianStokAdapter();
		$columns = array("id", "id_stok_obat", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan", "nama_user");
		$penyesuaian_stok_dbtable = new DBTable(
			$db,
			"smis_fr_penyesuaian_stok",
			$columns
		);
		$filter = "";
		if (isset($_POST['kriteria'])) {
			$filter = " AND (smis_fr_stok_obat.kode_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_obat_f_masuk.no_bbm LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_obat_f_masuk.tanggal_datang LIKE '%" . $_POST['kriteria'] . "%')";
		}
		$query_value = "
			SELECT smis_fr_stok_obat.*, smis_fr_obat_f_masuk.no_bbm, smis_fr_obat_f_masuk.tanggal_datang
			FROM (smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id) LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
			WHERE smis_fr_stok_obat.prop NOT LIKE 'del' " . $filter . "
			ORDER BY smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.tanggal_exp ASC
		";
		$query_count = "
			SELECT COUNT(*)
			FROM (
				" . $query_value . "
			) v
		";
		$penyesuaian_stok_dbtable->setPreferredQuery(true, $query_value, $query_count);
		class PenyesuaianStokDBResponder extends DuplicateResponder {
			public function save() {
				$data = $this->postToArray();
				$result = $this->dbtable->insert($data);
				$stok_obat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_stok_obat");
				$stok_data = array();
				$stok_data['sisa'] = $_POST['jumlah_baru'];
				$stok_id['id'] = $_POST['id_stok_obat'];
				$stok_obat_lama_row = $this->dbtable->get_row("
					SELECT a.sisa, b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat
					FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
					WHERE a.id = '" . $_POST['id_stok_obat'] . "'
				");
				$stok_obat_dbtable->update($stok_data, $stok_id);
				$stok_obat_baru_row = $this->dbtable->get_row("
					SELECT a.sisa, b.id_obat, b.kode_obat, b.nama_obat, b.nama_jenis_obat
					FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
					WHERE a.id = '" . $_POST['id_stok_obat'] . "'
				");
				//logging riwayat stok obat:
				$riwayat_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_riwayat_stok_obat");
				$data_riwayat = array();
				$data_riwayat['tanggal'] = date("Y-m-d");
				$data_riwayat['id_stok_obat'] = $_POST['id_stok_obat'];
				if ($_POST['jumlah_baru'] > $_POST['jumlah_lama'])
					$data_riwayat['jumlah_masuk'] = $_POST['jumlah_baru'] - $_POST['jumlah_lama'];
				else
					$data_riwayat['jumlah_keluar'] = $_POST['jumlah_lama'] - $_POST['jumlah_baru'];
				$data_riwayat['sisa'] = $_POST['jumlah_baru'];
				$data_riwayat['keterangan'] = "Penyesuaian Stok";
				global $user;
				$data_riwayat['nama_user'] = $user->getName();
				$riwayat_dbtable->insert($data_riwayat);
				//kartu gudang induk:
				$ks_dbtable = new DBTable($this->dbtable->get_db(), "smis_fr_kartu_stok");
				$ks_data = array();
				$ks_data['f_id'] = $_POST['id_stok_obat'];
				$ks_data['unit'] = "Gudang Farmasi";
				$ks_data['no_bon'] = "Penyesuaian Stok";
				$ks_data['id_obat'] = $stok_obat_baru_row->id_obat;
				$ks_data['kode_obat'] = $stok_obat_baru_row->kode_obat;
				$ks_data['nama_obat'] = $stok_obat_baru_row->nama_obat;
				$ks_data['nama_jenis_obat'] = $stok_obat_baru_row->nama_jenis_obat;
				$ks_data['tanggal'] = date("Y-m-d");
				$ks_data['masuk'] = $stok_obat_baru_row->sisa >= $stok_obat_lama_row->sisa ? $stok_obat_baru_row->sisa - $stok_obat_lama_row->sisa : 0;
				$ks_data['keluar'] = $stok_obat_baru_row->sisa < $stok_obat_lama_row->sisa ? $stok_obat_lama_row->sisa - $stok_obat_baru_row->sisa : 0;
				$ks_data['sisa'] = $stok_obat_baru_row->sisa;
				$ks_data['autonomous'] = "[".$this->getAutonomous()."]";
		        $ks_data['duplicate'] = 0;
		        $ks_data['time_updated'] = date("Y-m-d H:i:s");
		        $ks_data['origin_updated'] = $this->getAutonomous();
		        $ks_data['origin'] = $this->getAutonomous();
		        $ks_dbtable->insert($ks_data);
				$success['type'] = "insert";
				$success['id'] = $this->dbtable->get_inserted_id();
				$success['success'] = 1;
				if ($result === false) $success['success'] = 0;
				return $success;
			}
			public function edit() {
				$id = $_POST['id'];
				$data = $this->dbtable->get_row("
					SELECT smis_fr_stok_obat.*, smis_fr_obat_f_masuk.no_bbm, CASE WHEN smis_fr_obat_f_masuk.tanggal_datang IS NULL OR smis_fr_obat_f_masuk.tanggal_datang = '0000-00-00' THEN 'N/A' ELSE smis_fr_obat_f_masuk.tanggal_datang END AS 'tanggal_datang'
					FROM (smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id) LEFT JOIN smis_fr_obat_f_masuk ON smis_fr_dobat_f_masuk.id_obat_f_masuk = smis_fr_obat_f_masuk.id
					WHERE smis_fr_stok_obat.id = '" . $id . "'
				");
				return $data;
			}
		}
		$penyesuaian_stok_dbresponder = new PenyesuaianStokDBResponder(
			$penyesuaian_stok_dbtable,
			$penyesuaian_stok_table,
			$penyesuaian_stok_adapter
		);
		if ($penyesuaian_stok_dbresponder->is("save")) {
			global $user;
			$penyesuaian_stok_dbresponder->addColumnFixValue("nama_user", $user->getName());
		}
		$data = $penyesuaian_stok_dbresponder->command($_POST['command']);
		echo json_encode($data);
		return;
	}
	
	$penyesuaian_stok_modal = new Modal("penyesuaian_stok_add_form", "smis_form_container", "penyesuaian_stok");
	$penyesuaian_stok_modal->setTitle("Penyesuaian Stok Obat");
	$id_stok_obat_hidden = new Hidden("penyesuaian_stok_id_stok_obat", "penyesuaian_stok_id_stok_obat", "");
	$penyesuaian_stok_modal->addElement("", $id_stok_obat_hidden);
	$tanggal_text = new Text("penyesuaian_stok_tanggal", "penyesuaian_stok_tanggal", date("Y-m-d"));
	$tanggal_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Tanggal", $tanggal_text);
	$nama_obat_text = new Text("penyesuaian_stok_nama_obat", "penyesuaian_stok_nama_obat", "");
	$nama_obat_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Nama Obat", $nama_obat_text);
	$nama_jenis_text = new Text("penyesuaian_stok_nama_jenis", "penyesuaian_stok_nama_jenis", "");
	$nama_jenis_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jenis Obat", $nama_jenis_text);
	$jenis_stok_text = new Text("penyesuaian_stok_jenis_stok", "penyesuaian_stok_jenis_stok", "");
	$jenis_stok_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jenis Stok", $jenis_stok_text);
	$produsen_text = new Text("penyesuaian_stok_produsen", "penyesuaian_stok_produsen", "");
	$produsen_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Produsen", $produsen_text);
	$no_bbm_text = new Text("penyesuaian_stok_no_bbm", "penyesuaian_stok_no_bbm", "");
	$no_bbm_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("No. BBM", $no_bbm_text);
	$tanggal_datang_text = new Text("penyesuaian_stok_tanggal_datang", "penyesuaian_stok_tanggal_datang", "");
	$tanggal_datang_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Tgl. Masuk", $tanggal_datang_text);
	$f_jumlah_lama_text = new Text("penyesuaian_stok_f_jumlah_lama", "penyesuaian_stok_f_jumlah_lama", "");
	$f_jumlah_lama_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Jml. Tercatat", $f_jumlah_lama_text);
	$jumlah_lama_hidden = new Hidden("penyesuaian_stok_jumlah_lama", "penyesuaian_stok_jumlah_lama", "");
	$penyesuaian_stok_modal->addElement("", $jumlah_lama_hidden);
	$jumlah_baru_text = new Text("penyesuaian_stok_jumlah_baru", "penyesuaian_stok_jumlah_baru", "");
	$penyesuaian_stok_modal->addElement("Jml. Aktual", $jumlah_baru_text);
	$satuan_text = new text("penyesuaian_stok_satuan", "penyesuaian_stok_satuan", "");
	$satuan_text->setAtribute("disabled='disabled'");
	$penyesuaian_stok_modal->addElement("Satuan", $satuan_text);
	$keterangan_textarea = new TextArea("penyesuaian_stok_keterangan", "penyesuaian_stok_keterangan", "");
	$penyesuaian_stok_modal->addElement("Keterangan", $keterangan_textarea);
	$penyesuaian_stok_button = new Button("", "", "Simpan");
	$penyesuaian_stok_button->setClass("btn-success");
	$penyesuaian_stok_button->setIcon("fa fa-floppy-o");
	$penyesuaian_stok_button->setIsButton(Button::$ICONIC);
	$penyesuaian_stok_button->setAction("penyesuaian_stok.save()");
	$penyesuaian_stok_modal->addFooter($penyesuaian_stok_button);
	
	echo $penyesuaian_stok_modal->getHtml();
	echo $penyesuaian_stok_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	function PenyesuaianStokAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PenyesuaianStokAction.prototype.constructor = PenyesuaianStokAction;
	PenyesuaianStokAction.prototype = new TableAction();
	PenyesuaianStokAction.prototype.edit = function(id) {
		var data = this.getRegulerData();
		data['command'] = "edit";
		data['id'] = id;
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) return;
				$("#penyesuaian_stok_id_stok_obat").val(id);
				$("#penyesuaian_stok_nama_obat").val(json.nama_obat);
				$("#penyesuaian_stok_nama_jenis").val(json.nama_jenis_obat);
				$("#penyesuaian_stok_jenis_stok").val(json.label.replace("_", " ").toUpperCase());
				$("#penyesuaian_stok_produsen").val(json.produsen);
				$("#penyesuaian_stok_f_jumlah_lama").val(json.sisa + " " + json.satuan);
				$("#penyesuaian_stok_jumlah_lama").val(json.sisa);
				$("#penyesuaian_stok_jumlah_baru").val(json.sisa);
				$("#penyesuaian_stok_satuan").val(json.satuan);
				$("#penyesuaian_stok_keterangan").val("");
				$("#penyesuaian_stok_no_bbm").val(json.no_bbm);
				$("#penyesuaian_stok_tanggal_datang").val(json.tanggal_datang);
				$("#modal_alert_penyesuaian_stok_add_form").html("");
				$(".error_field").removeClass("error_field");
				$("#penyesuaian_stok_add_form").smodal("show");
			}
		);
	};
	PenyesuaianStokAction.prototype.validate = function() {
		var valid = true;
		var invalid_msg = "";
		var tanggal = $("#penyesuaian_stok_tanggal").val();
		var jumlah_aktual = $("#penyesuaian_stok_jumlah_baru").val();
		var jumlah_tercatat = $("#penyesuaian_stok_jumlah_tercatat").val();
		var keterangan = $("#penyesuaian_stok_keterangan").val();
		$(".error_field").removeClass("error_field");
		if (jumlah_aktual == "") {
			valid = false;
			invalid_msg += "</br><strong>Jml. Aktual</strong> tidak boleh kosong";
			$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
		} else if (!is_numeric(jumlah_aktual)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Aktual</strong> seharusnya numerik (0-9)";
			$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
		} else if (parseFloat(jumlah_aktual) == parseFloat(jumlah_tercatat)) {
			valid = false;
			invalid_msg += "</br><strong>Jml. Aktual</strong> dan <strong>Jml. Tercatat</strong> tidak terdapat selisih";
			$("#penyesuaian_stok_jumlah_baru").addClass("error_field");
		}
		if (tanggal == "") {
			valid = false;
			invalid_msg += "</br><strong>Tanggal</strong> tidak boleh kosong";
			$("#penyesuaian_stok_tanggal").addClass("error_field");
		}
		if (keterangan == "") {
			valid = false;
			invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
			$("#penyesuaian_stok_keterangan").addClass("error_field");
		}
		if (!valid) {
			$("#modal_alert_penyesuaian_stok_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					invalid_msg +
				"</div>"
			);
		}
		return valid;
	};
	PenyesuaianStokAction.prototype.save = function() {
		if (!this.validate()) {
			return;
		}
		$("#penyesuaian_stok_add_form").smodal("hide");
		showLoading();
		var self = this;
		var data = this.getRegulerData();
		data['command'] = "save";
		data['id'] = "";
		data['id_stok_obat'] = $("#penyesuaian_stok_id_stok_obat").val();
		data['tanggal'] = $("#penyesuaian_stok_tanggal").val();
		data['jumlah_baru'] = $("#penyesuaian_stok_jumlah_baru").val();
		data['jumlah_lama'] = $("#penyesuaian_stok_jumlah_lama").val();
		data['keterangan'] = $("#penyesuaian_stok_keterangan").val();
		$.post(
			"",
			data,
			function(response) {
				var json = getContent(response);
				if (json == null) {
					$("#penyesuaian_stok_add_form").smodal("show");
				} else {
					self.view();
					riwayat_penyesuaian_stok.view();
				}
				dismissLoading();
			}
		);
	};
	
	var penyesuaian_stok;
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'top'
		});
		$("#penyesuaian_stok_max").val(50);
		$(".mydate").datepicker();
		var penyesuaian_stok_columns = new Array("id", "id_stok_obat", "tanggal", "jumlah_baru", "jumlah_lama", "keterangan");
		penyesuaian_stok = new PenyesuaianStokAction(
			"penyesuaian_stok",
			"gudang_farmasi",
			"detail_stok_obat",
			penyesuaian_stok_columns
		);
		penyesuaian_stok.view();
	});
</script>