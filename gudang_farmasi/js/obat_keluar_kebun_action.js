function ObatKeluarKebunAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatKeluarKebunAction.prototype.constructor = ObatKeluarKebunAction;
ObatKeluarKebunAction.prototype = new TableAction();
ObatKeluarKebunAction.prototype.refreshNumber = function() {
	var no = 1;
	var nor_dobat_keluar_kebun = $("tbody#dobat_keluar_kebun_list").children("tr").length;
	for(var i = 0; i < nor_dobat_keluar_kebun; i++) {
		var dr_prefix = $("tbody#dobat_keluar_kebun_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html("<div align='right'>" + no + ".</div>");
		no++;
	}
};
ObatKeluarKebunAction.prototype.show_add_form = function() {
	row_id = 0;
	var today = new Date();
	$("#obat_keluar_kebun_id").val("");
	$("#obat_keluar_kebun_id_kebun").val("");
	$("#obat_keluar_kebun_kode_kebun").val("");
	$("#obat_keluar_kebun_nama_kebun").val("");
	$("#kebun_browse").removeAttr("onclick");
	$("#kebun_browse").attr("onclick", "kebun.chooser('kebun', 'kebun_button', 'kebun', kebun)");
	$("#kebun_browse").removeClass("btn-info");
	$("#kebun_browse").removeClass("btn-inverse");
	$("#kebun_browse").addClass("btn-info");
	$("#dobat_keluar_kebun_list").children().remove();
	$("#dobat_keluar_kebun_add").show();
	$("#modal_alert_obat_keluar_kebun_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_keluar_kebun_save").removeAttr("onclick");
	$("#obat_keluar_kebun_save").attr("onclick", "obat_keluar_kebun.save()");
	$("#obat_keluar_kebun_save").show();
	$("#obat_keluar_kebun_ok").hide();
	$("#obat_keluar_kebun_add_form").smodal("show");
};
ObatKeluarKebunAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_kebun = $("#obat_keluar_kebun_nama_kebun").val();
	var nord = $("#dobat_keluar_kebun_list").children().length;
	$(".error_field").removeClass("error_field");
	if (nama_kebun == "") {
		valid = false;
		invalid_msg += "<br/><strong>Kebun</strong> tidak boleh kosong";
		$("#obat_keluar_kebun_nama_kebun").addClass("error_field");
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "<br/><strong>Detail</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_obat_keluar_kebun_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ObatKeluarKebunAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	$("#obat_keluar_kebun_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_kebun";
	data['command'] = "save";
	data['id'] = "";
	data['id_kebun'] = $("#obat_keluar_kebun_id_kebun").val();
	data['kode_kebun'] = $("#obat_keluar_kebun_kode_kebun").val();
	data['nama_kebun'] = $("#obat_keluar_kebun_nama_kebun").val();
	data['keterangan'] = "";
	var detail = {};
	var nor = $("tbody#dobat_keluar_kebun_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		var prefix = $("tbody#dobat_keluar_kebun_list").children('tr').eq(i).prop("id");
		d_data['id'] = $("#" + prefix + "_id").text();
		d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
		d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
		d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
		d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
		d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
		d_data['satuan'] = $("#" + prefix + "_satuan").text();
		d_data['konversi'] = $("#" + prefix + "_konversi").text();
		d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
		detail[i] = d_data;
	}
	data['detail'] = detail;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) { 
				$("#obat_keluar_kebun_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ObatKeluarKebunAction.prototype.edit = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_kebun";
	data['command'] = "edit";
	data['id'] = id;
	$.post(	
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_keluar_kebun_id").val(id);
			$("#obat_keluar_kebun_id_kebun").val(json.header.id_kebun);
			$("#obat_keluar_kebun_kode_kebun").val(json.header.kode_kebun);
			$("#obat_keluar_kebun_nama_kebun").val(json.header.nama_kebun);
			$("#kebun_browse").removeAttr("onclick");
			$("#kebun_browse").attr("onclick", "kebun.chooser('kebun', 'kebun_button', 'kebun', kebun)");
			$("#kebun_browse").removeClass("btn-info");
			$("#kebun_browse").removeClass("btn-inverse");
			$("#kebun_browse").addClass("btn-info");
			$("#dobat_keluar_kebun_add").show();
			row_id = 0;
			$("#dobat_keluar_kebun_list").children("tr").remove();
			for(var i = 0; i < json.detail.length; i++) {
				var dobat_keluar_kebun_id = json.detail[i].id;
				var dobat_keluar_kebun_id_obat_keluar_kebun = json.detail[i].id_obat_keluar_kebun;
				var dobat_keluar_kebun_kode_obat = json.detail[i].kode_obat;
				var dobat_keluar_kebun_id_obat = json.detail[i].id_obat;
				var dobat_keluar_kebun_nama_obat = json.detail[i].nama_obat;
				var dobat_keluar_kebun_nama_jenis_obat = json.detail[i].nama_jenis_obat;
				var dobat_keluar_kebun_jumlah = json.detail[i].jumlah;
				var dobat_keluar_kebun_satuan = json.detail[i].satuan;
				var dobat_keluar_kebun_konversi = json.detail[i].konversi;
				var dobat_keluar_kebun_satuan_konversi = json.detail[i].satuan_konversi;
				$("tbody#dobat_keluar_kebun_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td style='display: none;' id='data_" + row_id + "_id'>" + dobat_keluar_kebun_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_id_obat'>" + dobat_keluar_kebun_id_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_kode_obat'>" + dobat_keluar_kebun_kode_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dobat_keluar_kebun_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dobat_keluar_kebun_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dobat_keluar_kebun_satuan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dobat_keluar_kebun_konversi + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dobat_keluar_kebun_satuan_konversi + "</td>" +
						"<td id='data_" + row_id + "_nomor'></td>" +
						"<td id='data_" + row_id + "_nama_obat'>" + dobat_keluar_kebun_nama_obat + "</td>" +
						"<td id='data_" + row_id + "_nama_jenis_obat'>" + dobat_keluar_kebun_nama_jenis_obat + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + dobat_keluar_kebun_jumlah + " " + dobat_keluar_kebun_satuan + "</td>" +
						"<td id='data_" + row_id + "_keterangan'>1 " + dobat_keluar_kebun_satuan + " = " + dobat_keluar_kebun_konversi + " " + dobat_keluar_kebun_satuan_konversi + "</td>" +
						"<td>" + 
							"<div class='btn-group noprint'>" +
								"<a href='#' onclick='dobat_keluar_kebun.edit_jumlah(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
									"<i class='icon-edit icon-white'></i>" +
								"</a>" +
								"<a href='#' onclick='dobat_keluar_kebun.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
									"<i class='icon-remove icon-white'></i>" + 
								"</a>" +
							 "</div>" +
						"</td>" +
					"</tr>"
				);
				row_id++;
			}
			obat_keluar_kebun.refreshNumber();
			$("#modal_alert_obat_keluar_kebun_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_keluar_kebun_save").removeAttr("onclick");
			$("#obat_keluar_kebun_save").attr("onclick", "obat_keluar_kebun.update(" + id + ")");
			$("#obat_keluar_kebun_save").show();
			$("#obat_keluar_kebun_ok").hide();
			$("#obat_keluar_kebun_add_form").smodal("show");
		}
	);
};
ObatKeluarKebunAction.prototype.update = function(id) {
	if (!this.validate()) {
		return;
	}
	$("#obat_keluar_kebun_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_kebun";
	data['command'] = "save";
	data['id'] = id;
	data['id_kebun'] = $("#obat_keluar_kebun_id_kebun").val();
	data['kode_kebun'] = $("#obat_keluar_kebun_kode_kebun").val();
	data['nama_kebun'] = $("#obat_keluar_kebun_nama_kebun").val();
	data['keterangan'] = "";
	var detail = {};
	var nor = $("tbody#dobat_keluar_kebun_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		var prefix = $("tbody#dobat_keluar_kebun_list").children('tr').eq(i).prop("id");
		d_data['id'] = $("#" + prefix + "_id").text();
		d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
		d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
		d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
		d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
		d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
		d_data['jumlah_lama'] = $("#" + prefix + "_jumlah_lama").text();
		d_data['satuan'] = $("#" + prefix + "_satuan").text();
		d_data['konversi'] = $("#" + prefix + "_konversi").text();
		d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
		if ($("#" + prefix).attr("class") == "deleted") {
			d_data['cmd'] = "delete";
		} else if ($("#" + prefix + "_id").text() == "") {
			d_data['cmd'] = "insert";
		} else {
			d_data['cmd'] = "update";
		}
		detail[i] = d_data;
	}
	data['detail'] = detail;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) { 
				$("#obat_keluar_kebun_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ObatKeluarKebunAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_kebun";
	data['command'] = "edit";
	data['id'] = id;
	$.post(	
		"",
		data,
		function (response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_keluar_kebun_id").val(id);
			$("#obat_keluar_kebun_id_kebun").val(json.header.id_kebun);
			$("#obat_keluar_kebun_kode_kebun").val(json.header.kode_kebun);
			$("#obat_keluar_kebun_nama_kebun").val(json.header.nama_kebun);
			$("#kebun_browse").removeAttr("onclick");
			$("#kebun_browse").removeClass("btn-info");
			$("#kebun_browse").removeClass("btn-inverse");
			$("#kebun_browse").addClass("btn-inverse");
			$("#dobat_keluar_kebun_add").hide();
			row_id = 0;
			$("#dobat_keluar_kebun_list").children("tr").remove();
			for(var i = 0; i < json.detail.length; i++) {
				var dobat_keluar_kebun_id = json.detail[i].id;
				var dobat_keluar_kebun_id_obat_keluar_kebun = json.detail[i].id_obat_keluar_kebun;
				var dobat_keluar_kebun_id_obat = json.detail[i].id_obat;
				var dobat_keluar_kebun_kode_obat = json.detail[i].kode_obat;
				var dobat_keluar_kebun_nama_obat = json.detail[i].nama_obat;
				var dobat_keluar_kebun_nama_jenis_obat = json.detail[i].nama_jenis_obat;
				var dobat_keluar_kebun_jumlah = json.detail[i].jumlah;
				var dobat_keluar_kebun_satuan = json.detail[i].satuan;
				var dobat_keluar_kebun_konversi = json.detail[i].konversi;
				var dobat_keluar_kebun_satuan_konversi = json.detail[i].satuan_konversi;
				$("tbody#dobat_keluar_kebun_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td style='display: none;' id='data_" + row_id + "_id'>" + dobat_keluar_kebun_id + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_kode_obat'>" + dobat_keluar_kebun_kode_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_id_obat'>" + dobat_keluar_kebun_id_obat + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah_lama'>" + dobat_keluar_kebun_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_jumlah'>" + dobat_keluar_kebun_jumlah + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan'>" + dobat_keluar_kebun_satuan + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_konversi'>" + dobat_keluar_kebun_konversi + "</td>" +
						"<td style='display: none;' id='data_" + row_id + "_satuan_konversi'>" + dobat_keluar_kebun_satuan_konversi + "</td>" +
						"<td id='data_" + row_id + "_nomor'></td>" +
						"<td id='data_" + row_id + "_nama_obat'>" + dobat_keluar_kebun_nama_obat + "</td>" +
						"<td id='data_" + row_id + "_nama_jenis_obat'>" + dobat_keluar_kebun_nama_jenis_obat + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + dobat_keluar_kebun_jumlah + " " + dobat_keluar_kebun_satuan + "</td>" +
						"<td id='data_" + row_id + "_keterangan'>1 " + dobat_keluar_kebun_satuan + " = " + dobat_keluar_kebun_konversi + " " + dobat_keluar_kebun_satuan_konversi + "</td>" +
						"<td></td>" +
					"</tr>"
				);
				row_id++;
			}
			obat_keluar_kebun.refreshNumber();
			$("#modal_alert_obat_keluar_kebun_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_keluar_kebun_save").removeAttr("onclick");
			$("#obat_keluar_kebun_save").hide();
			$("#obat_keluar_kebun_ok").show();
			$("#obat_keluar_kebun_add_form").smodal("show");
		}
	);
};
ObatKeluarKebunAction.prototype.del = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_kebun";
	data['command'] = "del";
	data['id'] = id;
	data['push_command'] = "delete";
	bootbox.confirm(
		"Anda yakin menghapus data ini?", 
		function(result) {
			if(result) {
				showLoading();
				$.post(
					'',
					data,
					function(response) {
						var json = getContent(response);
						if (json == null) return;
						self.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
ObatKeluarKebunAction.prototype.print_mutasi = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_keluar_kebun";
	data['command'] = "print_mutasi";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			smis_print(json);
		}
	);
};