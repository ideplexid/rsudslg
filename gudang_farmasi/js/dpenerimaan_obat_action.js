function DPenerimaanObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DPenerimaanObatAction.prototype.constructor = DPenerimaanObatAction;
DPenerimaanObatAction.prototype = new TableAction();
DPenerimaanObatAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#dpenerimaan_obat_name_obat").val();
	var jumlah_tercatat = $("#dpenerimaan_obat_jumlah_tercatat").val();
	var jumlah = $("#dpenerimaan_obat_jumlah").val();
	var konversi = $("#dpenerimaan_obat_konversi").val();
	var hna = $("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var diskon = $("#dpenerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
	var produsen = $("#dpenerimaan_obat_produsen").val();
	var tanggal_exp = $("#dpenerimaan_obat_tanggal_exp").val();
	var no_batch = $("#dpenerimaan_obat_no_batch").val();
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_nama_obat").addClass("error_field");
	}
	if (jumlah_tercatat == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_jumlah_tercatat").addClass("error_field");
	} else if (!is_numeric(jumlah_tercatat)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> seharusnya numerik (0-9)";
		$("#dpenerimaan_obat_jumlah_tercatat").addClass("error_field");
	} else if (jumlah_tercatat <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tercatat</strong> seharusnya > 0";
		$("#dpenerimaan_obat_jumlah_tercatat").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> seharusnya numerik (0-9)";
		$("#dpenerimaan_obat_jumlah").addClass("error_field");
	} else if (jumlah <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Diterima</strong> seharusnya > 0";
		$("#dpenerimaan_obat_jumlah").addClass("error_field");
	}
	if (konversi == "") {
		valid = false;
		invalid_msg += "</br><strong>Konversi</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_konversi").addClass("error_field");
	} else if (!is_numeric(konversi)) {
		valid = false;
		invalid_msg += "</br><strong>Konversi</strong> seharusnya numerik (0-9)";
		$("#dpenerimaan_obat_konversi").addClass("error_field");
	} else if (jumlah <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Konversi</strong> seharusnya > 0";
		$("#dpenerimaan_obat_konversi").addClass("error_field");
	}
	if (hna == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_hna").addClass("error_field");
	} else if (jumlah <= 0) {
		valid = false;
		invalid_msg += "</br><strong>Harga</strong> tidak boleh 0";
		$("#dpenerimaan_obat_hna").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_diskon").addClass("error_field");
	}
	if (produsen == "") {
		valid = false;
		invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_produsen").addClass("error_field");
	}
	if (tanggal_exp == "") {
		valid = false;
		invalid_msg += "</br><strong>Tanggal Exp.</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_tanggal_exp").addClass("error_field");
	}
	if (no_batch == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Batch</strong> tidak boleh kosong";
		$("#dpenerimaan_obat_no_batch").addClass("error_field");
	}
	if (!valid)
		bootbox.alert(invalid_msg);
	return valid;
};
DPenerimaanObatAction.prototype.clear = function() {
	$("#dpenerimaan_obat_row_num").val("");
	$("#dpenerimaan_obat_id_obat").val("");
	$("#dpenerimaan_obat_kode_obat").val("");
	$("#dpenerimaan_obat_name_obat").val("");
	$("#dpenerimaan_obat_nama_obat").val("");
	$("#dpenerimaan_obat_nama_jenis_obat").val("");
	$("#dpenerimaan_obat_jumlah_tercatat").val("");
	$("#dpenerimaan_obat_jumlah").val("");
	$("#dpenerimaan_obat_satuan").val("");
	$("#dpenerimaan_obat_konversi").val("");
	$("#dpenerimaan_obat_satuan_konversi").val("");
	$("#dpenerimaan_obat_hpp").val("Rp. 0,00");
	$("#dpenerimaan_obat_hna").val("Rp. 0,00");
	$("#dpenerimaan_obat_diskon").val("0,00");
	$("#dpenerimaan_obat_t_diskon").val("persen");
	$("#dpenerimaan_obat_produsen").val("");
	$("#dpenerimaan_obat_tanggal_exp").val("");
	$("#dpenerimaan_obat_no_batch").val("");
};
DPenerimaanObatAction.prototype.save = function() {
	if (!this.validate())
		return;
	var self = this;
	var r_num = $("#dpenerimaan_obat_row_num").val();
	var id_obat = $("#dpenerimaan_obat_id_obat").val();
	var jumlah_tercatat = $("#dpenerimaan_obat_jumlah_tercatat").val();
	var f_jumlah_tercatat = parseFloat(jumlah_tercatat).formatMoney("0", ".", ",");
	var jumlah = $("#dpenerimaan_obat_jumlah").val();
	var f_jumlah = parseFloat(jumlah).formatMoney("0", ".", ",");
	var konversi = $("#dpenerimaan_obat_konversi").val();
	var hna = parseFloat($("#dpenerimaan_obat_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var ppn = parseFloat($("#penerimaan_obat_ppn").val());
	var f_hna = (hna / ((100 + ppn) / 100)).formatMoney("2", ".", ",");
	var use_ppn = $("#penerimaan_obat_use_ppn").val();
	if (use_ppn == 0)
		f_hna = hna.formatMoney("2", ".", ",");
	var diskon = parseFloat($("#dpenerimaan_obat_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	var t_diskon = $("#dpenerimaan_obat_t_diskon").val();
	var f_diskon = diskon + " %";
	if (t_diskon == "nominal")
		f_diskon = diskon.formatMoney("2", ".", ",");
	var produsen = $("#dpenerimaan_obat_produsen").val();
	var tanggal_exp = $("#dpenerimaan_obat_tanggal_exp").val();
	var no_batch = $("#dpenerimaan_obat_no_batch").val();
	var subtotal = jumlah * (hna / ((100 + ppn) / 100));
	if (use_ppn == 0)
		subtotal = jumlah * hna;
	if (t_diskon == "persen")
		subtotal = subtotal - (diskon * subtotal / 100);
	else
		subtotal = subtotal - diskon;
	subtotal = Math.round(subtotal);
	var f_subtotal = subtotal.formatMoney("2", ".", ",");

	var data = this.getRegulerData();
	data['command'] = "validate_harga";
	data['id_obat'] = id_obat;
	data['harga'] = hna;
	data['konversi'] = konversi;

	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			if (json == null) return;
			if (json.valid == false) {
				bootbox.alert(json.message);
				return;
			}
			legend = "";
			if (json.state == "over") {
				legend = " <sup>&#8657;" + json.persen_selisih + "%</sup>";
				bootbox.alert(json.message);
			} else if (json.state == "under") {
				legend = " <sup>&#8659;" + json.persen_selisih + "%</sup>";
				bootbox.alert(json.message);
			}
			$("tr#data_" + r_num + " td#jumlah_tercatat").html(jumlah_tercatat);
			$("tr#data_" + r_num + " td#f_jumlah_tercatat").html("<small><div align='right'>" + f_jumlah_tercatat + "</div></small>");
			$("tr#data_" + r_num + " td#jumlah").html(jumlah);
			$("tr#data_" + r_num + " td#f_jumlah").html("<small><div align='right'>" + f_jumlah + "</div></small>");
			$("tr#data_" + r_num + " td#konversi").html(konversi);
			$("tr#data_" + r_num + " td#d_ppn").html(ppn);
			$("tr#data_" + r_num + " td#hna").html(hna);
			$("tr#data_" + r_num + " td#f_hna").html("<small><div align='right'>" + f_hna + legend + "</div></small>");
			$("tr#data_" + r_num + " td#diskon").html(diskon);
			$("tr#data_" + r_num + " td#t_diskon").html(t_diskon);
			$("tr#data_" + r_num + " td#f_diskon").html("<small><div align='right'>" + f_diskon + "</div></small>");
			$("tr#data_" + r_num + " td#produsen").html(produsen);
			$("tr#data_" + r_num + " td#tanggal_exp").html(tanggal_exp);
			$("tr#data_" + r_num + " td#no_batch").html(no_batch);
			$("tr#data_" + r_num + " td#subtotal").html(subtotal);
			$("tr#data_" + r_num + " td#f_subtotal").html("<small><div align='right'>" + f_subtotal + "</div></small>");
			self.clear();
			penerimaan_obat.update_total();
			self.setEditMode("false");
		}
	);
};
DPenerimaanObatAction.prototype.setEditMode = function(enable) {
	var limited = $("#penerimaan_obat_limited").val();
	if (enable == "true") {
		if (limited == "false") {
			$("#dpenerimaan_obat_jumlah_tercatat").removeAttr("disabled");
			$("#dpenerimaan_obat_jumlah").removeAttr("disabled");
			$("#dpenerimaan_obat_konversi").removeAttr("disabled");
			$("#dpenerimaan_obat_hpp").removeAttr("disabled");
			$("#dpenerimaan_obat_hna").removeAttr("disabled");
			$("#dpenerimaan_obat_produsen").removeAttr("disabled");
			$("#dpenerimaan_obat_tanggal_exp").removeAttr("disabled");
			$("#dpenerimaan_obat_no_batch").removeAttr("disabled");
		}
		$("#dpenerimaan_obat_diskon").removeAttr("disabled");
		$("#dpenerimaan_obat_t_diskon").removeAttr("disabled");
		$("#penerimaan_obat_form_update").show();
		$("#penerimaan_obat_form_cancel").show();
	} else {
		$("#dpenerimaan_obat_jumlah_tercatat").removeAttr("disabled");
		$("#dpenerimaan_obat_jumlah_tercatat").attr("disabled", "disabled");
		$("#dpenerimaan_obat_jumlah").removeAttr("disabled");
		$("#dpenerimaan_obat_jumlah").attr("disabled", "disabled");
		$("#dpenerimaan_obat_konversi").removeAttr("disabled");
		$("#dpenerimaan_obat_konversi").attr("disabled", "disabled");
		$("#dpenerimaan_obat_hpp").removeAttr("disabled");
		$("#dpenerimaan_obat_hpp").attr("disabled", "disabled");
		$("#dpenerimaan_obat_hna").removeAttr("disabled");
		$("#dpenerimaan_obat_hna").attr("disabled", "disabled");
		$("#dpenerimaan_obat_diskon").removeAttr("disabled");
		$("#dpenerimaan_obat_diskon").attr("disabled", "disabled");
		$("#dpenerimaan_obat_t_diskon").removeAttr("disabled");
		$("#dpenerimaan_obat_t_diskon").attr("disabled", "disabled");
		$("#dpenerimaan_obat_produsen").removeAttr("disabled");
		$("#dpenerimaan_obat_produsen").attr("disabled", "disabled");
		$("#dpenerimaan_obat_tanggal_exp").removeAttr("disabled");
		$("#dpenerimaan_obat_tanggal_exp").attr("disabled", "disabled");
		$("#dpenerimaan_obat_no_batch").removeAttr("disabled");
		$("#dpenerimaan_obat_no_batch").attr("disabled", "disabled");
		$("#penerimaan_obat_form_update").hide();
		$("#penerimaan_obat_form_cancel").hide();
	}
};
DPenerimaanObatAction.prototype.edit = function(r_num) {
	var id_obat = $("tr#data_" + r_num + " td#id_obat").text();
	var kode_obat = $("tr#data_" + r_num + " td#kode_obat").text();
	var nama_obat = $("tr#data_" + r_num + " td#nama_obat").text();
	var nama_jenis_obat = $("tr#data_" + r_num + " td#nama_jenis_obat").text();
	var jumlah_tercatat = parseFloat($("tr#data_" + r_num + " td#jumlah_tercatat").text());
	var jumlah = parseFloat($("tr#data_" + r_num + " td#jumlah").text());
	var satuan = $("tr#data_" + r_num + " td#satuan").text();
	var konversi = $("tr#data_" + r_num + " td#konversi").text();
	var satuan_konversi = $("tr#data_" + r_num + " td#satuan_konversi").text();
	var hna = "Rp. " + parseFloat($("tr#data_" + r_num + " td#hna").text()).formatMoney("2", ".", ",");
	var ppn = parseFloat($("#penerimaan_obat_ppn").val());
	var hpp = "Rp. " + (parseFloat($("tr#data_" + r_num + " td#hna").text()) / ((100 + ppn) / 100)).formatMoney("2", ".", ",");
	var use_ppn = $("#penerimaan_obat_use_ppn").val();
	if (use_ppn == 0)
		hna = hpp;
	var diskon = $("tr#data_" + r_num + " td#diskon").text();
	var t_diskon = $("tr#data_" + r_num + " td#t_diskon").text();
	var produsen = $("tr#data_" + r_num + " td#produsen").text();
	var tanggal_exp = $("tr#data_" + r_num + " td#tanggal_exp").text();
	var no_batch = $("tr#data_" + r_num + " td#no_batch").text();
	this.setEditMode("true");
	$("#dpenerimaan_obat_row_num").val(r_num);
	$("#dpenerimaan_obat_id_obat").val(id_obat);
	$("#dpenerimaan_obat_kode_obat").val(kode_obat);
	$("#dpenerimaan_obat_name_obat").val(nama_obat);
	$("#dpenerimaan_obat_nama_obat").val(nama_obat);
	$("#dpenerimaan_obat_nama_jenis_obat").val(nama_jenis_obat);
	$("#dpenerimaan_obat_jumlah_tercatat").val(jumlah_tercatat);
	$("#dpenerimaan_obat_jumlah").val(jumlah);
	$("#dpenerimaan_obat_satuan").val(satuan);
	$("#dpenerimaan_obat_konversi").val(konversi);
	$("#dpenerimaan_obat_satuan_konversi").val(satuan_konversi);
	$("#dpenerimaan_obat_hpp").val(hpp);
	$("#dpenerimaan_obat_hna").val(hna);
	$("#dpenerimaan_obat_diskon").val(diskon);
	$("#dpenerimaan_obat_t_diskon").val(t_diskon);
	$("#dpenerimaan_obat_produsen").val(produsen);
	$("#dpenerimaan_obat_tanggal_exp").val(tanggal_exp);
	$("#dpenerimaan_obat_no_batch").val(no_batch);
};
DPenerimaanObatAction.prototype.empty = function(r_num) {
	$("tr#data_" + r_num + " td#jumlah").html(0);
	$("tr#data_" + r_num + " td#f_jumlah").html("<small><div align='right'>0</div></small>");
	$("tr#data_" + r_num + " td#jumlah_tercatat").html(0);
	$("tr#data_" + r_num + " td#f_jumlah_tercatat").html("<small><div align='right'>0</div></small>");
	$("tr#data_" + r_num + " td#subtotal").html(0);
	$("tr#data_" + r_num + " td#f_subtotal").html("<small><div align='right'>0,00</div></small>");
	penerimaan_obat.update_total();
};
DPenerimaanObatAction.prototype.del = function(r_num) {
	var id = $("tr#data_" + r_num + " td#id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	penerimaan_obat.update_total();
};
DPenerimaanObatAction.prototype.cancel = function() {
	this.clear();
	this.setEditMode("false");
};