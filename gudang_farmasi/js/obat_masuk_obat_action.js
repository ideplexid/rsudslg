function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.getViewData = function(){
	var view_data = this.getRegulerData();
	view_data['super_command'] = "obat";
	view_data['command'] = "list";
	view_data['id_po'] = $("#obat_f_masuk_nopo").val();
	view_data['kriteria'] = $("#" + this.prefix + "_kriteria").val();
	view_data['number'] = $("#" + this.prefix + "_number").val();
	view_data['max'] = $("#" + this.prefix + "_max").val();
	return view_data;
};
ObatAction.prototype.selected = function(json) {
	$("#dobat_f_masuk_id_obat").val(json.id_barang);
	$("#dobat_f_masuk_kode_obat").val(json.kode_barang);
	$("#dobat_f_masuk_nama_obat").val(json.nama_barang);
	$("#dobat_f_masuk_medis").val(json.medis);
	$("#dobat_f_masuk_inventaris").val(json.inventaris);
	$("#dobat_f_masuk_nama_jenis_obat").val(json.nama_jenis_barang);
	$("#dobat_f_masuk_id_dpo").val(json.id);
	var hna = "Rp. " + (parseFloat(json.hna)).formatMoney("2", ".", ",");
	$("#dobat_f_masuk_hna").val(hna);
	$("#dobat_f_masuk_jumlah_tercatat").val(json.sisa);
	$("#dobat_f_masuk_jumlah").val(json.sisa);
	$("#dobat_f_masuk_satuan").val(json.satuan);
	$("#dobat_f_masuk_konversi").val(1);
	$("#dobat_f_masuk_satuan_konversi").val(json.satuan);
	var data = this.getRegulerData();
	data['super_command'] = "stok";
	data['command'] = "edit";
	data['id_obat'] = json.id_barang;
	data['satuan'] = json.satuan;
	data['konversi'] = 1;
	data['satuan_konversi'] = json.satuan;
	$.post(
		"",
		data,
		function(response) {
			var json_sisa = getContent(response);
			if (json_sisa == null) return;
			console.log(json_sisa);
			$("#dobat_f_masuk_stok_terkini").val(json_sisa.sisa);
		}
	);
};