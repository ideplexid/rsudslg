function ObatKeluarDariPermintaanAction(name, page, action, columns) {
	this.initialize(name, page, action, columns);
}	
ObatKeluarDariPermintaanAction.prototype.constructor = ObatKeluarDariPermintaanAction;
ObatKeluarDariPermintaanAction.prototype = new TableAction();
ObatKeluarDariPermintaanAction.prototype.show_add_form = function() {
	var data = this.getRegulerData();
	data['action'] = "obat_keluar_dari_permintaan_form";
	data['editable'] = "true";
	LoadSmisPage(data);
};
ObatKeluarDariPermintaanAction.prototype.back = function() {
	var data = this.getRegulerData();
	data['action'] = "obat_keluar_dari_permintaan";
	data['super_command'] = "";
	data['kriteria'] = "";
	data['max'] = 5;
	data['number'] = 0;
	LoadSmisPage(data);
};
ObatKeluarDariPermintaanAction.prototype.getSaveData = function() {
	var data = this.getRegulerData();
	data['command'] = "save";
	data['action'] = "obat_keluar_dari_permintaan";
	data['f_id'] = $("#mutasi_obat_keluar_f_id").val();
	data['unit'] = $("#mutasi_obat_keluar_slug").val();
	data['f_id_permintaan_obat_unit'] = $("#mutasi_obat_keluar_f_id_permintaan_obat_unit").val();
	data['tanggal_permintaan'] = $("#mutasi_obat_keluar_tanggal_permintaan").val();

	var detail = {};
	var jumlah_detail = $("tr.detail").length;
	for (var i = 0; i < jumlah_detail; i++) {
		var id_dpermintaan_obat_unit = $("tr.detail:eq(" + i + ") td#id_dpermintaan_obat_unit").text();
		var id_obat = $("tr.detail:eq(" + i + ") td#id_obat").text();
		var kode_obat = $("tr.detail:eq(" + i + ") td#kode").text();
		var nama_obat = $("tr.detail:eq(" + i + ") td#nama_obat").text();
		var nama_jenis_obat = $("tr.detail:eq(" + i + ") td#nama_jenis_obat").text();
		var jumlah_diminta = $("tr.detail:eq(" + i + ") td#jumlah_diminta").text();
		var jumlah_dipenuhi = $("tr.detail:eq(" + i + ") td#jumlah_dipenuhi").text();
		var satuan = $("tr.detail:eq(" + i + ") td#satuan").text();
		var konversi = 1;
		var satuan_konversi = $("tr.detail:eq(" + i + ") td#satuan").text();

		var stok = {};
		var prefix = "tr." + $("tr.detail:eq(" + i + ")").prop("id");
		var jumlah_stok = $(prefix).length;
		if (jumlah_stok > 0) {
			for (var j = 0; j < jumlah_stok; j++) {
				var id_stok_obat = $(prefix + ":eq(" + j + ") td#id_stok_obat").text();
				var jumlah = $(prefix + ":eq(" + j + ") td#jumlah").text();
				var satuan = $(prefix + ":eq(" + j + ") td#satuan").text();
				var tanggal_exp = $(prefix + ":eq(" + j + ") td#tanggal_exp").text();
				var no_batch = $(prefix + ":eq(" + j + ") td#no_batch").text();

				stok[j] = {
					'id_stok_obat' 	: id_stok_obat,
					'jumlah'		: jumlah,
					'satuan'		: satuan,
					'tanggal_exp'	: tanggal_exp,
					'no_batch'		: no_batch
				};
			}
		}

		detail[i] = {
			'id_dpermintaan_obat_unit'	: id_dpermintaan_obat_unit,
			'id_obat'					: id_obat,
			'kode_obat'					: kode_obat,
			'nama_obat'					: nama_obat,
			'nama_jenis_obat'			: nama_jenis_obat,
			'jumlah_diminta'			: jumlah_diminta,
			'jumlah_dipenuhi'			: jumlah_dipenuhi,
			'satuan'					: satuan,
			'konversi'					: konversi,
			'satuan_konversi'			: satuan_konversi,
			'stok'						: stok
		};
	}
	data['detail'] = detail;

	return data;
};
ObatKeluarDariPermintaanAction.prototype.validate = function() {
	var valid = true;
	$(".error_field").removeClass("error_field");
	var unit = $("#mutasi_obat_keluar_slug").val();
	var jumlah_data_stok = $("tr.stok").length;
	if (unit == "") {
		valid = false;
		bootbox.alert({
			message : "Unit harus dipilih.",
			size	: "small"
		});
		$("#mutasi_obat_keluar_unit").addClass("error_field");
	}
	if (jumlah_data_stok == 0) {
		valid = false;
		bootbox.alert({
			message : "Tidak ada detail permintaan yang dipenuhi.",
			size	: "small"
		});
	}
	return valid;
};
ObatKeluarDariPermintaanAction.prototype.save = function() {
	if (!this.validate())
		return;
	var self = this;
	var data = this.getSaveData();
	bootbox.dialog({
		message : "Yakin melakukan penyimpanan transaksi ini?",
		title	: "Konfirmasi",
		buttons	: {
			success : {
				label 		: "Lanjutkan",
				className 	: "btn-success",
				callback	: function() {
					showLoading();
					$.post(
						"",
						data,
						function() {
							dismissLoading();
							self.back();
						}
					);
				}
			},
			danger 	: {
				label 		: "Batal",
				className	: "btn-inverse"
			}
		}
	});
};
ObatKeluarDariPermintaanAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			var dt = self.getRegulerData();
			dt['action'] = "obat_keluar_dari_permintaan_form";
			dt['editable'] = "false";
			dt['id'] = id;
			dt['f_id_permintaan_obat_unit'] = json.f_id_permintaan_obat_unit;
			dt['f_id_obat_keluar_unit'] = json.f_id_obat_keluar_unit;
			dt['tanggal_permintaan'] = json.tanggal_permintaan;
			dt['tanggal_mutasi'] = json.tanggal_mutasi;
			dt['unit'] = json.unit;
			LoadSmisPage(dt);
		}
	);
};
ObatKeluarDariPermintaanAction.prototype.fillDetail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['command'] = "get_detail";
	data['id_mutasi_obat_keluar'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = JSON.parse(response);
			$("tbody#dmutasi_obat_keluar_list").html(json.html);
		}
	);
};