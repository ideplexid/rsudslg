function StokObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
StokObatAction.prototype.constructor = StokObatAction;
StokObatAction.prototype = new TableAction();
StokObatAction.prototype.addViewData = function(data) {
	data['id_obat'] = $("tr#data_" + $("#mutasi_obat_keluar_row_num").val() + " td#id_obat").text();
	data['id_stok_obat'] = dmutasi_obat_keluar.getAllCSVIDStok();
	return data;
};
StokObatAction.prototype.selected = function(json) {
	$("#mutasi_obat_keluar_id_stok_obat").val(json.id);
	$("#mutasi_obat_keluar_kode_obat").val(json.kode_obat);
	$("#mutasi_obat_keluar_nama_obat").val(json.nama_obat);
	$("#mutasi_obat_keluar_nama_jenis_obat").val(json.nama_jenis_obat);
	$("#mutasi_obat_keluar_sisa").val(json.sisa);
	$("#mutasi_obat_keluar_satuan").val(json.satuan);
	$("#mutasi_obat_keluar_tanggal_exp").val(json.tanggal_exp);
	$("#mutasi_obat_keluar_no_batch").val(json.no_batch);
	$("#mutasi_obat_keluar_produsen").val(json.produsen);
	$("#mutasi_obat_keluar_jumlah").focus();
};