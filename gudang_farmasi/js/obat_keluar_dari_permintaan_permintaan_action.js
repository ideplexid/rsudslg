function PermintaanAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PermintaanAction.prototype.constructor = PermintaanAction;
PermintaanAction.prototype = new TableAction();
PermintaanAction.prototype.selected = function(json) {
	$("#mutasi_obat_keluar_f_id").val(json.header.f_id);
	$("#mutasi_obat_keluar_f_id_permintaan_obat_unit").val(json.header.id);
	$("#mutasi_obat_keluar_slug").val(json.header.unit);
	$("#mutasi_obat_keluar_unit").val(json.header.unit.replace(/_/g, " ").toUpperCase());
	$("#mutasi_obat_keluar_tanggal_permintaan").val(json.header.tanggal);
	$("tbody#dmutasi_obat_keluar_list").html(json.detail_html);
};