function DObatFMasukAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
DObatFMasukAction.prototype.constructor = DObatFMasukAction;
DObatFMasukAction.prototype = new TableAction();
DObatFMasukAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nama_obat = $("#dobat_r_masuk_name_obat").val();
	var hna = $("#dobat_r_masuk_hna").val();
	var jumlah_tertulis = $("#dobat_r_masuk_jumlah_tertulis").val();
	var jumlah = $("#dobat_r_masuk_jumlah").val();
	var satuan = $("#dobat_r_masuk_satuan").val();
	var konversi = $("#dobat_r_masuk_konversi").val();
	var satuan_konversi = $("#dobat_r_masuk_satuan_konversi").val();
	var produsen = $("#dobat_r_masuk_produsen").val();
	var diskon = $("#dobat_r_masuk_diskon").val();
	var t_diskon = $("#dobat_r_masuk_t_diskon").val();
	var ed = $("#dobat_r_masuk_ed").val();
	var nobatch = $("#dobat_r_masuk_nobatch").val();
	$(".error_field").removeClass("error_field");
	if (nama_obat == "") {
		valid = false;
		invalid_msg += "</br><strong>Nama Obat</strong> tidak boleh kosong";
		$("#dobat_r_masuk_nama_obat").addClass("error_field");
	}
	if (produsen == "") {
		valid = false;
		invalid_msg += "</br><strong>Produsen</strong> tidak boleh kosong";
		$("#dobat_r_masuk_produsen").addClass("error_field");
	}
	if (hna == "") {
		valid = false;
		invalid_msg += "</br><strong>Harga Satuan</strong> tidak boleh kosong";
		$("#dobat_r_masuk_subtotal").addClass("error_field");
	} else {
		var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
		if (v_hna == 0) {
			valid = false;
			invalid_msg += "</br><strong>Harga Satuan</strong> tidak boleh 0 (nol)";
			$("#dobat_masuk_hna").addClass("error_field");
		}
	}
	if (jumlah_tertulis == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tertulis</strong> tidak boleh kosong";
		$("#dobat_r_masuk_jumlah_tertulis").addClass("error_field");
	} else if (!is_numeric(jumlah_tertulis)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah Tertulis</strong> seharusnya numerik (0-9)";
		$("#dobat_r_masuk_jumlah_tertulis").addClass("error_field");
	}
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#dobat_r_masuk_jumlah").addClass("error_field");
	} else if (!is_numeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#dobat_r_masuk_jumlah").addClass("error_field");
	}
	if (satuan == "") {
		valid = false;
		invalid_msg += "</br><strong>Satuan</strong> tidak boleh kosong";
		$("#dobat_r_masuk_satuan").addClass("error_field");
	}
	if (konversi == "") {
		valid = false;
		invalid_msg += "</br><strong>Jml. Terkecil</strong> tidak boleh kosong";
		$("#dobat_r_masuk_konversi").addClass("error_field");
	} else if (!is_numeric(konversi)) {
		valid = false;
		invalid_msg += "</br><strong>Jml. Terkecil</strong> seharusnya numerik (0-9)";
		$("#dobat_r_masuk_konversi").addClass("error_field");
	}
	if (satuan_konversi == "") {
		valid = false;
		invalid_msg += "</br><strong>Stn. Terkecil</strong> tidak boleh kosong";
		$("#dobat_r_masuk_satuan_konversi").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#dobat_r_masuk_diskon").addClass("error_field");
	} else {
		diskon = parseFloat(diskon);
		if (isNaN(diskon)) {
			valid = false;
			invalid_msg += "</br><strong>Diskon</strong> seharusnya numerik (0-9)";
			$("#dobat_r_masuk_diskon").addClass("error_field");
		} else {
			if (t_diskon == "persen" && diskon > 100) {
				valid = false;
				invalid_msg += "</br><strong>Diskon</strong> seharusnya bernilai 0-100 %";
				$("#dobat_r_masuk_diskon").addClass("error_field");
			} 
			if (diskon < 0) {
				valid = false;
				invalid_msg += "</br><strong>Diskon</strong> seharusnya bernilai >= 0";
				$("#dobat_r_masuk_diskon").addClass("error_field");
			}
		}
	}
	if (t_diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
		$("#dobat_r_masuk_t_diskon").addClass("error_field");
	}
	if (ed == "") {
		valid = false;
		invalid_msg += "</br><strong>Tgl. Exp.</strong> tidak boleh kosong";
		$("#dobat_r_masuk_ed").addClass("error_field");
	}
	if (nobatch == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Batch</strong> tidak boleh kosong";
		$("#dobat_r_masuk_nobatch").addClass("error_field");
	}
	if (!valid) {
		$("#modal_alert_dobat_r_masuk_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DObatFMasukAction.prototype.show_add_form = function() {
	$("#dobat_r_masuk_id").val("");
	$("#dobat_r_masuk_id_obat").val("");
	$("#dobat_r_masuk_kode_obat").val("");
	$("#dobat_r_masuk_nama_obat").val("");
	$("#dobat_r_masuk_name_obat").val("");
	$("#dobat_r_masuk_medis").val("");
	$("#dobat_r_masuk_inventaris").val("");
	$("#dobat_r_masuk_nama_jenis_obat").val("");
	$("#dobat_r_masuk_stok_terkini").val("");
	$("#dobat_r_masuk_jumlah_tertulis").val("");
	$("#dobat_r_masuk_jumlah").val("");
	$("#dobat_r_masuk_satuan").val("");
	$("#dobat_r_masuk_konversi").val("");
	$("#dobat_r_masuk_satuan_konversi").val("");
	$("#dobat_r_masuk_last_hna").val("Rp. 0,00");
	$("#dobat_r_masuk_total_hna").val("Rp. 0,00");
	$("#dobat_r_masuk_harga").val("Rp. 0,00");
	$("#dobat_r_masuk_hna").val("Rp. 0,00");
	$("#dobat_r_masuk_produsen").val("");
	$("#dobat_r_masuk_diskon").val(0);
	$("#dobat_r_masuk_t_diskon").val("persen");
	$("#dobat_r_masuk_ed").val("");
	$("#dobat_r_masuk_nobatch").val("");
	$("#modal_alert_dobat_r_masuk_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dobat_r_masuk_save").removeAttr("onclick");
	$("#dobat_r_masuk_save").attr("onclick", "dobat_r_masuk.save()");
	$("#dobat_r_masuk_add_form").smodal("show");
};
DObatFMasukAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	var id = $("#dobat_r_masuk_id").val();
	var id_obat = $("#dobat_r_masuk_id_obat").val();
	var kode_obat = $("#dobat_r_masuk_kode_obat").val();
	var nama_obat = $("#dobat_r_masuk_name_obat").val();
	var medis = $("#dobat_r_masuk_medis").val();
	var inventaris = $("#dobat_r_masuk_inventaris").val();
	var nama_jenis_obat = $("#dobat_r_masuk_nama_jenis_obat").val();
	var last_hna = $("#dobat_r_masuk_last_hna").val();
	var hna = $("#dobat_r_masuk_hna").val();
	var v_last_hna = parseFloat(last_hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var class_attr = "";
	var selisih = Math.abs(parseFloat(v_hna) - parseFloat(v_last_hna));
	var persentase_selisih = Math.round((selisih * 100) / v_last_hna);
	if (persentase_selisih > 10) { // ambang batas kewajaran kenaikan harga
		class_attr = "class='error'";
	}
	var stok_terkini = $("#dobat_r_masuk_stok_terkini").val();
	var jumlah_tertulis = $("#dobat_r_masuk_jumlah_tertulis").val();
	var jumlah = $("#dobat_r_masuk_jumlah").val();
	var subtotal = "Rp. " + (v_hna * parseFloat(jumlah)).formatMoney("2", ".", ",");
	var sisa = $("#dobat_r_masuk_jumlah").val();
	var satuan = $("#dobat_r_masuk_satuan").val();
	var konversi = $("#dobat_r_masuk_konversi").val();
	var satuan_konversi = $("#dobat_r_masuk_satuan_konversi").val();
	var produsen = $("#dobat_r_masuk_produsen").val();
	var diskon = $("#dobat_r_masuk_diskon").val();
	var t_diskon = $("#dobat_r_masuk_t_diskon").val();
	var f_diskon = diskon + " %";
	if (t_diskon == "nominal")
		f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
	var ed = $("#dobat_r_masuk_ed").val();
	var nobatch = $("#dobat_r_masuk_nobatch").val();
	$("tbody#dobat_r_masuk_list").append(
		"<tr id='data_" + row_id + "' " + class_attr + ">" +
			"<td id='data_" + row_id + "_id' style='display: none;'>" + id + "</td>" +
			"<td id='data_" + row_id + "_id_obat' style='display: none;'>" + id_obat + "</td>" +
			"<td id='data_" + row_id + "_kode_obat' style='display: none;'>" + kode_obat + "</td>" +
			"<td id='data_" + row_id + "_medis' style='display: none;'>" + medis + "</td>" +
			"<td id='data_" + row_id + "_inventaris' style='display: none;'>" + inventaris + "</td>" +
			"<td id='data_" + row_id + "_hna' style='display: none;'>" + hna + "</td>" +
			"<td id='data_" + row_id + "_last_hna' style='display: none;'>" + last_hna + "</td>" +
			"<td id='data_" + row_id + "_diskon' style='display: none;'>" + diskon + "</td>" +
			"<td id='data_" + row_id + "_t_diskon' style='display: none;'>" + t_diskon + "</td>" +
			"<td id='data_" + row_id + "_stok_terkini' style='display: none;'>" + stok_terkini + "</td>" +
			"<td id='data_" + row_id + "_jumlah_tertulis' style='display: none;'>" + jumlah_tertulis + "</td>" +
			"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + jumlah + "</td>" +
			"<td id='data_" + row_id + "_sisa' style='display: none;'>" + sisa + "</td>" +
			"<td id='data_" + row_id + "_satuan' style='display: none;'>" + satuan + "</td>" +
			"<td id='data_" + row_id + "_konversi' style='display: none;'>" + konversi + "</td>" +
			"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + satuan_konversi + "</td>" +
			"<td id='data_" + row_id + "_nomor'></td>" +
			"<td id='data_" + row_id + "_nama_obat'>" + nama_obat + "</td>" +
			"<td id='data_" + row_id + "_nama_jenis_obat'>" + nama_jenis_obat + "</td>" +
			"<td id='data_" + row_id + "_produsen'>" + produsen + "</td>" +
			"<td id='data_" + row_id + "_f_subtotal'>" + jumlah + " x " + hna + " = " + subtotal + "</td>" +
			"<td id='data_" + row_id + "_f_jumlah'>" + jumlah + " " + satuan + "</td>" +
			"<td id='data_" + row_id + "_f_konversi'>1 " + satuan + " = " + konversi + " " + satuan_konversi + "</td>" +
			"<td id='data_" + row_id + "_f_diskon'>" + f_diskon + "</td>" +
			"<td id='data_" + row_id + "_ed'>" + ed + "</td>" +
			"<td id='data_" + row_id + "_nobatch'>" + nobatch + "</td>" +
			"<td>" +
				"<div class='btn-group noprint'>" +
					"<a href='#' onclick='dobat_r_masuk.edit(" + row_id + ")' data-content='Ubah' data-toggle='popover' class='input btn btn-warning'>" + 
						"<i class='icon-edit icon-white'></i>" +
					"</a>" +
					"<a href='#' onclick='dobat_r_masuk.delete(" + row_id + ")' data-content='Hapus' data-toggle='popover' class='input btn btn-danger'>" + 
						"<i class='icon-remove icon-white'></i>" + 
					"</a>" +
				"</div>" +
			"</td>" +
		"</tr>"
	);
	row_id++;
	obat_r_masuk.refreshNoDObatMasuk();
	obat_r_masuk.refreshTotal();
	obat_r_masuk.check_warning();
	
	$("#dobat_r_masuk_id").val("");
	$("#dobat_r_masuk_id_obat").val("");
	$("#dobat_r_masuk_kode_obat").val("");
	$("#dobat_r_masuk_nama_obat").val("");
	$("#dobat_r_masuk_name_obat").val("");
	$("#dobat_r_masuk_medis").val("");
	$("#dobat_r_masuk_inventaris").val("");
	$("#dobat_r_masuk_nama_jenis_obat").val("");
	$("#dobat_r_masuk_stok_terkini").val("");
	$("#dobat_r_masuk_jumlah_tertulis").val("");
	$("#dobat_r_masuk_jumlah").val("");
	$("#dobat_r_masuk_satuan").val("");
	$("#dobat_r_masuk_konversi").val("");
	$("#dobat_r_masuk_satuan_konversi").val("");
	$("#dobat_r_masuk_last_hna").val("Rp. 0,00");
	$("#dobat_r_masuk_total_hna").val("Rp. 0,00");
	$("#dobat_r_masuk_harga").val("Rp. 0,00");
	$("#dobat_r_masuk_hna").val("Rp. 0,00");
	$("#dobat_r_masuk_produsen").val("");
	$("#dobat_r_masuk_diskon").val(0);
	$("#dobat_r_masuk_t_diskon").val("persen");
	$("#dobat_r_masuk_ed").val("");
	$("#dobat_r_masuk_nobatch").val("");
	$("#modal_alert_dobat_r_masuk_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dobat_r_masuk_nama_obat").focus();
};
DObatFMasukAction.prototype.edit = function(r_num) {
	var id = $("#data_" + r_num + "_id").text();
	var id_obat = $("#data_" + r_num + "_id_obat").text();
	var kode_obat = $("#data_" + r_num + "_kode_obat").text();
	var nama_obat = $("#data_" + r_num + "_nama_obat").text();
	var medis = $("#data_" + r_num + "_medis").text();
	var inventaris = $("#data_" + r_num + "_inventaris").text();
	var nama_jenis_obat = $("#data_" + r_num + "_nama_jenis_obat").text();
	var stok_terkini = $("#data_" + r_num + "_stok_terkini").text();
	var jumlah_tertulis = $("#data_" + r_num + "_jumlah_tertulis").text();
	var jumlah = $("#data_" + r_num + "_jumlah").text();
	var satuan = $("#data_" + r_num + "_satuan").text();
	var konversi = $("#data_" + r_num + "_konversi").text();
	var satuan_konversi = $("#data_" + r_num + "_satuan_konversi").text();
	var last_hna = $("#data_" + r_num + "_last_hna").text();
	var hna = $("#data_" + r_num + "_hna").text();
	var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var harga = v_hna / 1.1;
	var f_harga = "Rp. " + parseFloat(harga).formatMoney("2", ".", ",");
	var total_hna = harga * jumlah * konversi;
	var f_total_hna = "Rp. " + parseFloat(total_hna).formatMoney("2", ".", ",");
	var produsen = $("#data_" + r_num + "_produsen").text();
	var diskon = $("#data_" + r_num + "_diskon").text();
	var t_diskon = $("#data_" + r_num + "_t_diskon").text();
	var ed = $("#data_" + r_num + "_ed").text();
	var nobatch = $("#data_" + r_num + "_nobatch").text();
	$("#dobat_r_masuk_id").val(id);
	$("#dobat_r_masuk_id_obat").val(id_obat);
	$("#dobat_r_masuk_kode_obat").val(kode_obat);
	$("#dobat_r_masuk_nama_obat").val(nama_obat);
	$("#dobat_r_masuk_name_obat").val(nama_obat);
	$("#dobat_r_masuk_medis").val(medis);
	$("#dobat_r_masuk_inventaris").val(inventaris);
	$("#dobat_r_masuk_nama_jenis_obat").val(nama_jenis_obat);
	$("#dobat_r_masuk_stok_terkini").val(stok_terkini);
	$("#dobat_r_masuk_jumlah_tertulis").val(jumlah_tertulis);
	$("#dobat_r_masuk_jumlah").val(jumlah);
	$("#dobat_r_masuk_satuan").val(satuan);
	$("#dobat_r_masuk_konversi").val(konversi);
	$("#dobat_r_masuk_satuan_konversi").val(satuan_konversi);
	$("#dobat_r_masuk_last_hna").val(last_hna);
	$("#dobat_r_masuk_total_hna").val(f_total_hna);
	$("#dobat_r_masuk_harga").val(f_harga);
	$("#dobat_r_masuk_hna").val(hna);
	$("#dobat_r_masuk_produsen").val(produsen);
	$("#dobat_r_masuk_diskon").val(diskon);
	$("#dobat_r_masuk_t_diskon").val(t_diskon);
	$("#dobat_r_masuk_ed").val(ed);
	$("#dobat_r_masuk_nobatch").val(nobatch);
	$("#modal_alert_dobat_r_masuk_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#dobat_r_masuk_save").removeAttr("onclick");
	$("#dobat_r_masuk_save").attr("onclick", "dobat_r_masuk.update(" + r_num + ")");
	$("#dobat_r_masuk_add_form").smodal("show");
};
DObatFMasukAction.prototype.update = function(r_num) {
	if (!this.validate()) {
		return;
	}
	var id = $("#dobat_r_masuk_id").val();
	var id_obat = $("#dobat_r_masuk_id_obat").val();
	var kode_obat = $("#dobat_r_masuk_kode_obat").val();
	var nama_obat = $("#dobat_r_masuk_name_obat").val();
	var medis = $("#dobat_r_masuk_medis").val();
	var inventaris = $("#dobat_r_masuk_inventaris").val();
	var nama_jenis_obat = $("#dobat_r_masuk_nama_jenis_obat").val();
	var last_hna = $("#dobat_r_masuk_last_hna").val();
	var hna = $("#dobat_r_masuk_hna").val();
	var v_last_hna = parseFloat(last_hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_hna = parseFloat(hna.replace(/[^0-9-,]/g, '').replace(",", "."));
	var selisih = Math.abs(parseFloat(v_hna) - parseFloat(v_last_hna));
	var persentase_selisih = Math.round((selisih * 100) / v_last_hna);
	if (persentase_selisih > 10) { // ambang batas kewajaran kenaikan harga
		$("tr#data_" + r_num).removeAttr("class");
		$("tr#data_" + r_num).attr("class", "error");
	} else {
		$("tr#data_" + r_num).removeAttr("class");
	}
	var produsen = $("#dobat_r_masuk_produsen").val();
	var stok_terkini = $("#dobat_r_masuk_stok_terkini").val();
	var jumlah_tertulis = $("#dobat_r_masuk_jumlah_tertulis").val();
	var jumlah = $("#dobat_r_masuk_jumlah").val();
	var subtotal = "Rp. " + (v_hna * parseFloat(jumlah)).formatMoney("2", ".", ",");
	var selisih = Number($("#data_" + r_num + "_jumlah").text()) - Number($("#data_" + r_num + "_sisa").text());
	var sisa = Number(jumlah) - Number(selisih);
	var satuan = $("#dobat_r_masuk_satuan").val();
	var konversi = $("#dobat_r_masuk_konversi").val();
	var satuan_konversi = $("#dobat_r_masuk_satuan_konversi").val();
	var diskon = $("#dobat_r_masuk_diskon").val();
	var t_diskon = $("#dobat_r_masuk_t_diskon").val();
	var f_diskon = diskon + " %";
	if (t_diskon == "nominal")
		f_diskon = "Rp. " + (parseFloat(diskon)).formatMoney("2", ".", ",");
	var ed = $("#dobat_r_masuk_ed").val();
	var nobatch = $("#dobat_r_masuk_nobatch").val();
	$("#data_" + r_num + "_id").text(id);
	$("#data_" + r_num + "_id_obat").text(id_obat);
	$("#data_" + r_num + "_kode_obat").text(kode_obat);
	$("#data_" + r_num + "_nama_obat").text(nama_obat);
	$("#data_" + r_num + "_medis").text(medis);
	$("#data_" + r_num + "_inventaris").text(inventaris);
	$("#data_" + r_num + "_nama_jenis_obat").text(nama_jenis_obat);
	$("#data_" + r_num + "_last_hna").text(last_hna);
	$("#data_" + r_num + "_hna").text(hna);
	$("#data_" + r_num + "_diskon").text(diskon);
	$("#data_" + r_num + "_t_diskon").text(t_diskon);
	$("#data_" + r_num + "_stok_terkini").text(stok_terkini);
	$("#data_" + r_num + "_jumlah_tertulis").text(jumlah_tertulis);
	$("#data_" + r_num + "_jumlah").text(jumlah);
	$("#data_" + r_num + "_sisa").text(sisa);
	$("#data_" + r_num + "_satuan").text(satuan);
	$("#data_" + r_num + "_konversi").text(konversi);
	$("#data_" + r_num + "_satuan_konversi").text(satuan_konversi);
	$("#data_" + r_num + "_produsen").text(produsen);
	$("#data_" + r_num + "_f_subtotal").text(jumlah + " x " + hna + " = " + subtotal);
	$("#data_" + r_num + "_f_jumlah").text(jumlah + " " + satuan);
	$("#data_" + r_num + "_f_konversi").text("1 " + satuan + " = " + konversi + " " + satuan_konversi);
	$("#data_" + r_num + "_f_diskon").text(f_diskon);
	$("#data_" + r_num + "_ed").text(ed);
	$("#data_" + r_num + "_nobatch").text(nobatch);
	obat_r_masuk.refreshTotal();
	obat_r_masuk.check_warning();
	$("#dobat_r_masuk_add_form").smodal("hide");
};
DObatFMasukAction.prototype.delete = function(r_num) {
	var id = $("#data_" + r_num + "_id").text();
	if (id.length == 0) {
		$("#data_" + r_num).remove();
	} else {
		$("#data_" + r_num).attr("style", "display: none;");
		$("#data_" + r_num).attr("class", "deleted");
	}
	obat_r_masuk.refreshNoDObatMasuk();
	obat_r_masuk.refreshTotal();
};