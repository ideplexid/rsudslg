function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	$("#dpenerimaan_obat_id_obat").val(json.id);
	$("#dpenerimaan_obat_kode_obat").val(json.kode);
	$("#dpenerimaan_obat_nama_obat").val(json.nama);
	$("#dpenerimaan_obat_name_obat").val(json.nama);
	$("#dpenerimaan_obat_medis").val(json.medis);
	$("#dpenerimaan_obat_inventaris").val(json.inventaris);
	$("#dpenerimaan_obat_nama_jenis_obat").val(json.nama_jenis_barang);
	$("#dpenerimaan_obat_jumlah").val("");
	$("#dpenerimaan_obat_satuan").val(json.satuan);
	$("#dpenerimaan_obat_konversi").val(json.konversi);
	$("#dpenerimaan_obat_satuan_konversi").val(json.satuan_konversi);
	$("#dpenerimaan_obat_produsen").val(json.produsen);
	$("#dpenerimaan_obat_tanggal_exp").val("");
	$("#dpenerimaan_obat_no_batch").val("");
};