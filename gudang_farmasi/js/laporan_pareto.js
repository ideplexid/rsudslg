var laporan_pareto;
var laporan_pareto_result;
var laporan_parero_sorted_index;
var nilai_per_pareto ={};
var is_running = false;
var total_omset = 0;
$(document).ready(function(){
    $("#pareto_modal").attr("data-keyboard","false");

    $("#laporan_pareto_bulan_tahun_dari").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });

    $("#laporan_pareto_bulan_tahun_sampai").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });

    laporan_pareto = new TableAction("laporan_pareto","gudang_farmasi","laporan_pareto", new Array());
    laporan_pareto.addRegulerData = function(x){
        x['bulan_tahun_dari'] = laporan_pareto.get("bulan_tahun_dari");
        x['bulan_tahun_sampai'] = laporan_pareto.get("bulan_tahun_sampai");
        x['unit'] = laporan_pareto.get("unit");
        return x;
    };

    laporan_pareto.reload = function(){
        is_running = false;
    }

    laporan_pareto.formatRupiah = function(number) {
        var num = number.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        num = num.replaceAll('.', '*');
        num = num.replaceAll(',', '.');
        num = num.replaceAll('*', ',');
        return num;
    };

    laporan_pareto.download_excel = function(){
        showLoading();
        let dt = this.getRegulerData();
        dt['urutan'] = JSON.stringify(laporan_parero_sorted_index);
        dt['mentahan'] = JSON.stringify(laporan_pareto_result);
        dt['pareto'] = JSON.stringify(nilai_per_pareto);
        dt['command'] = "download_excel";
        laporan_pareto.download_pareto(dt);
    }

    laporan_pareto.download_pareto = function(data){
        var text=	"<form target='downloadFrame' method='POST' action='' id='form_download' style='display:none;' >";
                for(var key in data){
                    text+="<textarea name='"+key+"'>"+data[key]+"</textarea>";
                }
                text+="<input type='hidden' value='download_token' name='download_token'>";
            text+="</form>";
            text+='<iframe name="downloadFrame" style="display: none;" src="" />';
            $("#printing_area").html(text);
            var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
            var android = navigator.userAgent.toLowerCase().match(/android/g) ? true : false;
            if (iOS || android)
                $("#form_download").attr("target", "_blank");
            $("#form_download").submit();
            dismissLoading();
    }


    laporan_pareto.proses = function(){
        if(is_running){
            return;
        }
        is_running  = true;
        laporan_pareto_result = {};
        let dt = this.getRegulerData();
        dt['command'] = "ambil_daftar";
        showLoading();
        $.post("",dt,function(res){
            var json = getContent(res);
            dismissLoading();
            total_omset = 0;
            $("#pareto_modal").modal("show");
            $("#pareto_bar").sload("true","Loading... ",0);
            $("#laporan_pareto_list").html("");
            laporan_pareto.loop(0,json.length,json);
        });
    };

    laporan_pareto.loop = function(start,total,list){
        if(start==total){
            laporan_pareto.sorting();
            return;
        }
        if(!is_running){
            //$("#pareto_modal").modal("hide");
            laporan_pareto.sorting();
            return;
        }
        current = list[start];
        let dt = this.getRegulerData();
        dt['command'] = "ambil_satu";
        dt['id_obat'] = current.id;
        $("#pareto_bar").sload("true",current.nama+"..... [ "+(start)+" / "+total+" ]",(start)*100/total);
        $.post("",dt,function(res){
            var crt = getContent(res);
            laporan_pareto_result[current.id] = {};
            laporan_pareto_result[current.id]["id"] = current.id;
            laporan_pareto_result[current.id]["nama"] = current.nama;
            laporan_pareto_result[current.id]["total_omset"] = Number(crt.total_omset);
            laporan_pareto_result[current.id]["jumlah_penjualan"] = crt.jumlah_penjualan;
            laporan_pareto_result[current.id]["sisa_stok"] = crt.sisa_stok;
            laporan_pareto_result[current.id]["nilai_sisa_stok"] = Number(crt.nilai_sisa_stok);
            total_omset += Number(crt.total_omset);

            //laporan_pareto_result[current.id]["total_omset_format"] = crt.total_omset_format;
            // /laporan_pareto_result[current.id]["nilai_sisa_stok_format"] = crt.nilai_sisa_stok_format;
            start++;            
            $("#laporan_pareto_list").append(
                "<tr>"
                    +"<td>"+start+".</td>"
                    +"<td>"+current.nama+"</td>"
                    +"<td>"+laporan_pareto.formatRupiah(crt.total_omset)+"</td>"
                    +"<td>"+crt.jumlah_penjualan+"</td>"
                    +"<td>"+crt.sisa_stok+"</td>"
                    +"<td>"+laporan_pareto.formatRupiah(crt.nilai_sisa_stok)+"</td>"
                    +"<td></td>"
                    +"<td></td>"
                    +"<td></td>"
                +"</tr>"
            );
            setTimeout(function(){
                laporan_pareto.loop(start,total,list);
            },100);

        });
    }

    laporan_pareto.sorting = function(){
        laporan_parero_sorted_index = Object.keys(laporan_pareto_result).sort( function(keyA, keyB) {
            return laporan_pareto_result[keyB]["total_omset"] - laporan_pareto_result[keyA]["total_omset"];
        });
        laporan_pareto.fill_pareto();
    }

    laporan_pareto.fill_pareto = function(){
        nilai_per_pareto["A"] = 0
        nilai_per_pareto["B"] = 0;
        nilai_per_pareto["C"] = 0;
        nilai_per_pareto["P"] = 0;

       

        var kumulatif_persen = 0;
        var kumulatif_total_omset = 0;
        
        $("#pareto_bar").sload("true","Sorting & Labeling....",100);
        $.each( laporan_parero_sorted_index, function( key, value ) {
            kumulatif_total_omset = Number(laporan_pareto_result[value]["total_omset"]);
            let persen_nilai = kumulatif_total_omset*100/total_omset;
            kumulatif_persen += persen_nilai;

            let cum_fix = Number(kumulatif_persen.toFixed(2));
            let persen_fix = Number(persen_nilai.toFixed(2));

            laporan_pareto_result[value]["persen_nilai"] = persen_fix;
            laporan_pareto_result[value]["komulatif"] = cum_fix;
            if(Number(laporan_pareto_result[value]["total_omset"])==0){
                laporan_pareto_result[value]["pareto"] = "P";
                nilai_per_pareto["P"] += Number(laporan_pareto_result[value]["nilai_sisa_stok"]);
            }else if(kumulatif_persen<=80){
                laporan_pareto_result[value]["pareto"] = "A";
                nilai_per_pareto["A"] += Number(laporan_pareto_result[value]["nilai_sisa_stok"]);
            }else if(kumulatif_persen<=95){
                laporan_pareto_result[value]["pareto"] = "B";
                nilai_per_pareto["B"] += Number(laporan_pareto_result[value]["nilai_sisa_stok"]);
            }else if(kumulatif_persen<=100){
                laporan_pareto_result[value]["pareto"] = "C";
                nilai_per_pareto["C"] += Number(laporan_pareto_result[value]["nilai_sisa_stok"]);
            }else{
                laporan_pareto_result[value]["pareto"] = "P";
                nilai_per_pareto["P"] += Number(laporan_pareto_result[value]["nilai_sisa_stok"]);
            }
            //laporan_pareto_result[value]["urutan"] = key;
            
        });

        start = 1;
        $("#pareto_bar").sload("true","Building....",100);
        $("#laporan_pareto_list").html("");
        $.each( laporan_parero_sorted_index, function( key, value ) {
            var crt = laporan_pareto_result[value];
            $("#laporan_pareto_list").append(
                "<tr>"
                    +"<td>"+start+".</td>"
                    +"<td>"+crt.nama+"</td>"
                    +"<td>"+laporan_pareto.formatRupiah(crt.total_omset)+"</td>"
                    +"<td>"+crt.jumlah_penjualan+"</td>"
                    +"<td>"+crt.sisa_stok+"</td>"
                    +"<td>"+laporan_pareto.formatRupiah(crt.nilai_sisa_stok)+"</td>"
                    +"<td>"+crt.persen_nilai+"%</td>"
                    +"<td>"+crt.komulatif+"%</td>"
                    +"<td>"+crt.pareto+"</td>"
                +"</tr>"
            );
            start++;
        });

        var total_pareto = nilai_per_pareto["A"]+nilai_per_pareto["B"]+nilai_per_pareto["C"]+nilai_per_pareto["P"];
        $("#resume_pareto_list").html("");

        //pareto A
        $("#resume_pareto_list").append(
            "<tr>"
                +"<td>NILAI STOK A</td>"
                +"<td>"+laporan_pareto.formatRupiah(nilai_per_pareto['A'])+"</td>"
                +"<td>"+(nilai_per_pareto['A']*100/total_pareto).toFixed(2)+"%</td>"
            +"</tr>"
        );

        $("#resume_pareto_list").append(
            "<tr>"
                +"<td>NILAI STOK B</td>"
                +"<td>"+laporan_pareto.formatRupiah(nilai_per_pareto['B'])+"</td>"
                +"<td>"+(nilai_per_pareto['B']*100/total_pareto).toFixed(2)+"%</td>"
            +"</tr>"
        );

        $("#resume_pareto_list").append(
            "<tr>"
                +"<td>NILAI STOK B</td>"
                +"<td>"+laporan_pareto.formatRupiah(nilai_per_pareto['C'])+"</td>"
                +"<td>"+(nilai_per_pareto['C']*100/total_pareto).toFixed(2)+"%</td>"
            +"</tr>"
        );

        $("#resume_pareto_list").append(
            "<tr>"
                +"<td>NILAI STOK P</td>"
                +"<td>"+laporan_pareto.formatRupiah(nilai_per_pareto['P'])+"</td>"
                +"<td>"+(nilai_per_pareto['P']*100/total_pareto).toFixed(2)+"%</td>"
            +"</tr>"
        );

        $("#resume_pareto_list").append(
            "<tr>"
                +"<td>TOTAL NILAI STOK</td>"
                +"<td>"+laporan_pareto.formatRupiah(total_pareto)+"</td>"
                +"<td>100%</td>"
            +"</tr>"
        );


        $("#pareto_modal").modal("hide");
        laporan_pareto.reload();
    }
});