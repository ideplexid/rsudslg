function DObatKeluarDariPermintaanAction(name, page, action, columns) {
	this.initialize(name, page, action, columns);
}	
DObatKeluarDariPermintaanAction.prototype.constructor = DObatKeluarDariPermintaanAction;
DObatKeluarDariPermintaanAction.prototype = new TableAction();
DObatKeluarDariPermintaanAction.prototype.clear = function() {
	$("#mutasi_obat_keluar_row_num").val("");
	$("#mutasi_obat_keluar_id_stok_obat").val("");
	$("#mutasi_obat_keluar_kode_obat").val("");
	$("#mutasi_obat_keluar_nama_obat").val("");
	$("#mutasi_obat_keluar_nama_jenis_obat").val("");
	$("#mutasi_obat_keluar_sisa").val("");
	$("#mutasi_obat_keluar_jumlah").val("");
	$("#mutasi_obat_keluar_satuan").val("");
	$("#mutasi_obat_keluar_tanggal_exp").val("");
	$("#mutasi_obat_keluar_no_batch").val("");
	$("#mutasi_obat_keluar_produsen").val("");
};
DObatKeluarDariPermintaanAction.prototype.add = function(r_num) {
	this.clear();
	$("#mutasi_obat_keluar_row_num").val(r_num);
	var sisa_dipenuhi = parseFloat($("tr#data_" + r_num + " td#sisa_dipenuhi").text());
	$("#mutasi_obat_keluar_jumlah").val(sisa_dipenuhi);
	$("#mutasi_obat_keluar_add_form").smodal("show");
};
DObatKeluarDariPermintaanAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var sisa = $("#mutasi_obat_keluar_sisa").val();
	var jumlah = $("#mutasi_obat_keluar_jumlah").val();
	$(".error_field").removeClass("error_field");
	if (jumlah == "") {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh kosong";
		$("#mutasi_obat_keluar_jumlah").addClass("error_field");
		$("#mutasi_obat_keluar_jumlah").focus();
	} else if (!$.isNumeric(jumlah)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> seharusnya numerik (0-9)";
		$("#mutasi_obat_keluar_jumlah").addClass("error_field");
		$("#mutasi_obat_keluar_jumlah").focus();
	} else if (sisa != "" && $.isNumeric(sisa) && parseFloat(jumlah) > parseFloat(sisa)) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh melebihi sisa";
		$("#mutasi_obat_keluar_jumlah").addClass("error_field");
		$("#mutasi_obat_keluar_jumlah").focus();
	} else if (parseFloat(jumlah) == 0) {
		valid = false;
		invalid_msg += "</br><strong>Jumlah</strong> tidak boleh nol";
		$("#mutasi_obat_keluar_jumlah").addClass("error_field");
		$("#mutasi_obat_keluar_jumlah").focus();
	}
	if (!valid) {
		$("#modal_alert_mutasi_obat_keluar_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
DObatKeluarDariPermintaanAction.prototype.save = function() {
	if (!this.validate())
		return;
	var r_num = $("#mutasi_obat_keluar_row_num").val();
	var id_stok_obat = $("#mutasi_obat_keluar_id_stok_obat").val();
	var kode_obat = $("#mutasi_obat_keluar_kode_obat").val();
	var nama_obat = $("#mutasi_obat_keluar_nama_obat").val();
	var nama_jenis_obat = $("#mutasi_obat_keluar_nama_jenis_obat").val();
	var jumlah = $("#mutasi_obat_keluar_jumlah").val();
	var satuan = $("#mutasi_obat_keluar_satuan").val();
	var tanggal_exp = $("#mutasi_obat_keluar_tanggal_exp").val();
	var no_batch = $("#mutasi_obat_keluar_no_batch").val();
	var produsen = $("#mutasi_obat_keluar_produsen").val();

	var html = 	"<tr id='stok_" + id_stok_obat + "' class='stok stok_" + r_num + " data_" + r_num + "'>" +
					"<td id='id_stok_obat' style='display: none;'>" + id_stok_obat + "</td>" +
					"<td id='jumlah' style='display: none;'>" + jumlah + "</td>" +
					"<td id='satuan' style='display: none;'>" + satuan + "</td>" +
					"<td id='tanggal_exp' style='display: none;'>" + tanggal_exp + "</td>" +
					"<td id='no_batch' style='display: none;'>" + no_batch + "</td>" +
					"<td id='produsen' style='display: none;'>" + produsen + "</td>" +
					"<td></td>" +
					"<td id='f_id_stok_obat'>ID Stok : " + id_stok_obat + "</td>" +
					"<td id='f_tanggal_exp'>ED. : " + tanggal_exp + "</td>" +
					"<td id='f_no_batch'>No. Batch : " + no_batch + "</td>" +
					"<td id='f_produsen'>Produsen : " + produsen + "</td>" +
					"<td id='f_jumlah'>" + jumlah + " " + satuan + "</td>" +
					"<td>" +
						"<div class='btn-group noprint'>" +
							"<a href='#' onclick='dmutasi_obat_keluar.delete(\"stok_" + id_stok_obat + "\", \"" + r_num + "\")' data-content='Ubah' data-toggle='popover' class='input btn btn-danger'>" +
								"<i class='fa fa-trash'></i>" +
							"</a>" +
						"</div>" +
					"</td>" +
				"</tr>";
	$("#detail_data_" + r_num).after(html);
	$("#mutasi_obat_keluar_add_form").smodal("hide");
	this.refreshDetail(r_num);
};
DObatKeluarDariPermintaanAction.prototype.delete = function(id_element, r_num) {
	$("#" + id_element).remove();
	this.refreshDetail(r_num);	
};
DObatKeluarDariPermintaanAction.prototype.refreshDetail = function(r_num) {
	var jumlah_baris = $("tr.stok_" + r_num).length;
	var total_dipenuhi = 0;
	for (var i = 0; i < jumlah_baris; i++)
		total_dipenuhi += parseFloat($("tr.stok_" + r_num + ":eq(" + i + ") td#jumlah").text());
	var sisa_dipenuhi = parseFloat($("tr#data_" + r_num + " td#sisa_dipenuhi").text());
	sisa_dipenuhi -= total_dipenuhi;
	var satuan = $("tr#data_" + r_num + " td#satuan").text();
	$("tr#data_" + r_num + " td#jumlah_dipenuhi").html(total_dipenuhi);
	$("tr#data_" + r_num + " td#f_jumlah_dipenuhi").html("<strong>" + total_dipenuhi + " " + satuan + "</strong>");
	$("tr#data_" + r_num + " td#sisa_dipenuhi").html("<strong>" + sisa_dipenuhi + "</strong>");
};
DObatKeluarDariPermintaanAction.prototype.getAllCSVIDStok = function() {
	var jumlah_baris = $("tr.stok").length;
	var csv = "";
	for (var i = 0; i < jumlah_baris; i++)
		csv += $("tr.stok:eq(" + i + ") td#id_stok_obat").text() + ",";
	return csv;
};