function PenerimaanObatOPLAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
PenerimaanObatOPLAction.prototype.constructor = PenerimaanObatOPLAction;
PenerimaanObatOPLAction.prototype = new TableAction();
PenerimaanObatOPLAction.prototype.selected = function(json) {
	showLoading();
	$("#penerimaan_obat_id_po").val(json.id);
	$("#penerimaan_obat_no_opl").val(json.nomor);
	$("#penerimaan_obat_id_vendor").val(json.id_vendor);
	$("#penerimaan_obat_nama_vendor").val(json.nama_vendor);
	$("#penerimaan_obat_ppn").val(json.ppn);
	$("#penerimaan_obat_no_bbm").focus();
	var data = this.getRegulerData();
	data['command'] = "show_detail_opl";
	data['super_command'] = "";
	data['id_opl'] = json.id;
	data['row_num'] = row_num;
	$.post(
		"",
		data,
		function(response) {
			var js_opl = JSON.parse(response);
			if (js_opl == null) {
				dismissLoading();
				return;
			}
			var n_rows = $("tbody#dpenerimaan_obat_list").children("tr").length;
			for (var i = 0; i < n_rows; i++) {
				var id_dpo = $("tr#data_" + i + " td#id_dpo").text();
				if (id_dpo != "")
					dpenerimaan_obat.del(i);
			}
			$("#dpenerimaan_obat_list").append(js_opl.html);
			row_num = js_opl.row_num;
			penerimaan_obat.update_total();
			dismissLoading();
		}
	);
};