function ObatMasukAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatMasukAction.prototype.constructor = ObatMasukAction;
ObatMasukAction.prototype = new TableAction();
ObatMasukAction.prototype.refreshTotal = function() {
	var total = 0;
	var nord = $("tbody#dobat_f_masuk_list").children("tr").length;
	for(var i = 0; i < nord; i++) {
		var prefix = $("tbody#dobat_f_masuk_list").children("tr").eq(i).prop("id");
		var v_hna = parseFloat($("#" + prefix + "_hna").text().replace(/[^0-9-,]/g, '').replace(",", "."));
		var v_harga = Math.round((v_hna * 100) / 110);
		var t_diskon = $("#" + prefix + "_t_diskon").text();
		var v_diskon = parseFloat($("#" + prefix + "_diskon").text());
		var v_jumlah = parseFloat($("#" + prefix + "_jumlah").text());
		var v_subtotal = Math.round(v_jumlah * v_harga);
		if (t_diskon == "persen") {
			v_diskon = Math.round((v_diskon * v_subtotal) / 100);
			v_subtotal = Math.round(v_subtotal - v_diskon);
		} else {
			v_subtotal = Math.round(v_subtotal - v_diskon);
		}
		total += v_subtotal;
	}
	var v_global_diskon = $("#obat_f_masuk_diskon").val();
	v_global_diskon = parseFloat(v_global_diskon.replace(/[^0-9-,]/g, '').replace(",", "."));
	var v_global_t_diskon = $("#obat_f_masuk_t_diskon").val();
	if (v_global_t_diskon == "persen") {
		v_global_diskon = Math.round((v_global_diskon * total) / 100);
	}
	total = Math.round(total - v_global_diskon);
	total = Math.round(total + Math.round(total / 10));
	total = "Rp. " + parseFloat(total).formatMoney("2", ".", ",");
	$("#obat_f_masuk_total").val(total);
};
ObatMasukAction.prototype.refreshNoDObatMasuk = function() {
	var no = 1;
	var nor_dobat_masuk = $("tbody#dobat_f_masuk_list").children("tr").length;
	for(var i = 0; i < nor_dobat_masuk; i++) {
		var dr_prefix = $("tbody#dobat_f_masuk_list").children("tr").eq(i).prop("id");
		$("#" + dr_prefix + "_nomor").html("<div align='right'>" + no + ".</div>");
		no++;
	}
};
ObatMasukAction.prototype.validate = function() {
	var valid = true;
	var invalid_msg = "";
	var nopo = $("#obat_f_masuk_nopo").val();
	var no_bbm = $("#obat_f_masuk_no_bbm").val();
	var no_faktur = $("#obat_f_masuk_nofaktur").val();
	var tanggal = $("#obat_f_masuk_tanggal").val();
	var tanggal_datang = $("#obat_f_masuk_tanggal_datang").val();
	var tanggal_tempo = $("#obat_f_masuk_tanggal_tempo").val();
	var diskon = $("#obat_f_masuk_diskon").val();
	var t_diskon = $("#obat_f_masuk_t_diskon").val();
	var keterangan = $("#obat_f_masuk_keterangan").val();
	var nord = $("tbody#dobat_f_masuk_list").children().length;
	$(".error_field").removeClass("error_field");
	if (nopo == "") {
		valid = false;
		invalid_msg += "</br><strong>No. SP</strong> tidak boleh kosong";
		$("#obat_f_masuk_nopo").addClass("error_field");
	}
	if (no_bbm == "") {
		valid = false;
		invalid_msg += "</br><strong>No. BBM</strong> tidak boleh kosong";
		$("#obat_f_masuk_no_bbm").addClass("error_field");
	}
	if (no_faktur == "") {
		valid = false;
		invalid_msg += "</br><strong>No. Faktur</strong> tidak boleh kosong";
		$("#obat_f_masuk_nofaktur").addClass("error_field");
	}
	if (tanggal == "") {
		valid = false;
		invalid_msg += "</br><strong>Tgl. Faktur</strong> tidak boleh kosong";
		$("#obat_f_masuk_tanggal").addClass("error_field");
	}
	if (tanggal_datang == "") {
		valid = false;
		invalid_msg += "</br><strong>Tgl. Datang</strong> tidak boleh kosong";
		$("#obat_f_masuk_tanggal_datang").addClass("error_field");
	}
	if (tanggal_datang == "") {
		valid = false;
		invalid_msg += "</br><strong>Jatuh Tempo</strong> tidak boleh kosong";
		$("#obat_f_masuk_tanggal_tempo").addClass("error_field");
	}
	if (diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Diskon</strong> tidak boleh kosong";
		$("#obat_f_masuk_diskon").addClass("error_field");
	} 
	if (t_diskon == "") {
		valid = false;
		invalid_msg += "</br><strong>Tipe Diskon</strong> tidak boleh kosong";
		$("#obat_f_masuk_t_diskon").addClass("error_field");
	}
	if (keterangan == "") {
		valid = false;
		invalid_msg += "</br><strong>Keterangan</strong> tidak boleh kosong";
		$("#obat_f_masuk_keterangan").addClass("error_field");
	}
	if (nord == 0) {
		valid = false;
		invalid_msg += "</br><strong>Detil Obat Masuk</strong> tidak boleh kosong";
	}
	if (!valid) {
		$("#modal_alert_obat_f_masuk_add_form").html(
			"<div class='alert alert-block alert-danger'>" +
				"<h4>Peringatan</h4>" +
				invalid_msg +
			"</div>"
		);
	}
	return valid;
};
ObatMasukAction.prototype.show_add_form = function() {
	row_id = 0;
	$("#obat_f_masuk_id").val("");
	$("#obat_f_masuk_label").val("farmasi");
	$("#obat_f_masuk_label").removeAttr("disabled");
	$("#obat_f_masuk_nopo").val("");
	$("#obat_f_masuk_nopo_btn").removeAttr("onclick");
	$("#obat_f_masuk_nopo_btn").attr("onclick", "po.chooser('po', 'po_button', 'po', po)");
	$("#obat_f_masuk_nopo_btn").removeClass("btn-info");
	$("#obat_f_masuk_nopo_btn").removeClass("btn-inverse");
	$("#obat_f_masuk_nopo_btn").addClass("btn-info");
	$("#obat_f_masuk_id_vendor").val("");
	$("#obat_f_masuk_nama_vendor").val("");
	$("#obat_f_masuk_opl").val("");
	$("#obat_f_masuk_no_bbm").val("");
	$("#obat_f_masuk_nofaktur").val("");
	$("#obat_f_masuk_nofaktur").removeAttr("disabled");
	$("#obat_f_masuk_tanggal").val("");
	$("#obat_f_masuk_tanggal").removeAttr("disabled");
	$("#obat_f_masuk_tanggal_datang").val("");
	$("#obat_f_masuk_tanggal_datang").removeAttr("disabled");
	$("#obat_f_masuk_tanggal_tempo").val("");
	$("#obat_f_masuk_tanggal_tempo").removeAttr("disabled");
	$("#obat_f_masuk_diskon").val("0,00");
	$("#obat_f_masuk_diskon").removeAttr("disabled");
	$("#obat_f_masuk_t_diskon").val("persen");
	$("#obat_f_masuk_t_diskon").removeAttr("disabled");
	$("#obat_f_masuk_keterangan").val("-");
	$("#obat_f_masuk_keterangan").removeAttr("disabled");
	$("#obat_f_masuk_total").val("Rp. 0,00");
	$("#dobat_f_masuk_add").show();
	$("#dobat_f_masuk_list").children().remove();
	$("#obat_f_masuk_save").removeAttr("onclick");
	$("#obat_f_masuk_save").attr("onclick", "obat_f_masuk.save()");
	$("#obat_f_masuk_save").show();
	$("#obat_f_masuk_ok").hide();
	$("#modal_alert_obat_f_masuk_add_form").html("");
	$(".error_field").removeClass("error_field");
	$("#obat_f_masuk_add_form").smodal("show");
};
ObatMasukAction.prototype.save = function() {
	if (!this.validate()) {
		return;
	}
	$("#obat_f_masuk_add_form").smodal("hide");
	showLoading();
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_f_masuk";
	data['command'] = "save";
	data['id'] = $("#obat_f_masuk_id").val();
	data['tipe'] = $("#obat_f_masuk_label").val();
	data['id_po'] = $("#obat_f_masuk_nopo").val();
	data['no_opl'] = $("#obat_f_masuk_no_opl").val();
	data['no_bbm'] = $("#obat_f_masuk_no_bbm").val();
	data['id_vendor'] = $("#obat_f_masuk_id_vendor").val();
	data['nama_vendor'] = $("#obat_f_masuk_nama_vendor").val();
	data['no_faktur'] = $("#obat_f_masuk_nofaktur").val();
	data['tanggal'] = $("#obat_f_masuk_tanggal").val();
	data['tanggal_tempo'] = $("#obat_f_masuk_tanggal_tempo").val();
	data['tanggal_datang'] = $("#obat_f_masuk_tanggal_datang").val();
	data['diskon'] = parseFloat($("#obat_f_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", "."));
	data['t_diskon'] = $("#obat_f_t_diskon").val();
	data['keterangan'] = $("#obat_f_masuk_keterangan").val();
	var detail = {};
	var nor = $("tbody#dobat_f_masuk_list").children("tr").length;
	for(var i = 0; i < nor; i++) {
		var d_data = {};
		var prefix = $("tbody#dobat_f_masuk_list").children("tr").eq(i).prop("id");
		d_data['id_obat'] = $("#" + prefix + "_id_obat").text();
		d_data['kode_obat'] = $("#" + prefix + "_kode_obat").text();
		d_data['nama_obat'] = $("#" + prefix + "_nama_obat").text();
		d_data['medis'] = $("#" + prefix + "_medis").text();
		d_data['inventaris'] = $("#" + prefix + "_inventaris").text();
		d_data['nama_jenis_obat'] = $("#" + prefix + "_nama_jenis_obat").text();
		d_data['id_dpo'] = $("#" + prefix + "_id_dpo").text();
		d_data['stok_entri'] = $("#" + prefix + "_stok_terkini").text();
		d_data['jumlah_tercatat'] = $("#" + prefix + "_jumlah_tercatat").text();
		d_data['jumlah'] = $("#" + prefix + "_jumlah").text();
		d_data['sisa'] = $("#" + prefix + "_sisa").text();
		d_data['satuan'] = $("#" + prefix + "_satuan").text();
		d_data['konversi'] = $("#" + prefix + "_konversi").text();
		d_data['satuan_konversi'] = $("#" + prefix + "_satuan_konversi").text();
		var hna = $("#" + prefix + "_hna").text();
		hna = hna.replace(/[^0-9-,]/g, '').replace(",", ".");
		d_data['hna'] = parseFloat(hna);
		var selisih = $("#" + prefix + "_selisih").text();
		selisih = selisih.replace(/[^0-9-,]/g, '').replace(",", ".");
		d_data['selisih'] = parseFloat(selisih);
		d_data['produsen'] = $("#" + prefix + "_produsen").text();
		d_data['diskon'] = $("#" + prefix + "_diskon").text();
		d_data['t_diskon'] = $("#" + prefix + "_t_diskon").text();
		d_data['ed'] = $("#" + prefix + "_ed").text();
		d_data['nobatch'] = $("#" + prefix + "_nobatch").text();
		detail[i] = d_data;
	}
	data['detail'] = detail;
	data['push_command'] = "decrease";
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) {
				$("#obat_f_masuk_add_form").smodal("show");
			} else {
				self.view();
			}
			dismissLoading();
		}
	);
};
ObatMasukAction.prototype.del = function(id) {
	var self = this;
	var del_data = this.getRegulerData();
	del_data['super_command'] = "obat_f_masuk";
	del_data['command'] = "del";
	del_data['id'] = id;
	del_data['push_command'] = "increase";
	bootbox.confirm(
		"Anda yakin ingin menghapus Obat Masuk ini?", 
		function(result) {
		   if (result) {
			   showLoading();
				$.post(
					"",
					del_data,
					function(res){
						var json = getContent(res);
						if (json == null) return;
						self.view();
						dismissLoading();
					}
				);
			}
		}
	);
};
ObatMasukAction.prototype.detail = function(id) {
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_f_masuk";
	data['command'] = "edit";
	data['id'] = id;
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#obat_f_masuk_id").val(json.header.id);
			$("#obat_f_masuk_label").val(json.header.tipe);
			$("#obat_f_masuk_label").removeAttr("disabled");
			$("#obat_f_masuk_label").attr("disabled", "disabled");
			$("#obat_f_masuk_nopo").val(json.header.id_po);
			$("#obat_f_masuk_nopo_btn").removeAttr("onclick");
			$("#obat_f_masuk_nopo_btn").removeClass("btn-info");
			$("#obat_f_masuk_nopo_btn").removeClass("btn-inverse");
			$("#obat_f_masuk_nopo_btn").addClass("btn-inverse");
			$("#obat_f_masuk_no_opl").val(json.header.no_opl);
			$("#obat_f_masuk_no_opl").removeAttr("disabled");
			$("#obat_f_masuk_no_opl").attr("disabled", "disabled");
			$("#obat_f_masuk_no_bbm").val(json.header.no_bbm);
			$("#obat_f_masuk_no_bbm").removeAttr("disabled");
			$("#obat_f_masuk_no_bbm").attr("disabled", "disabled");
			$("#obat_f_masuk_id_vendor").val(json.header.id_vendor);
			$("#obat_f_masuk_nama_vendor").val(json.header.nama_vendor);
			$("#obat_f_masuk_nofaktur").val(json.header.no_faktur);
			$("#obat_f_masuk_nofaktur").removeAttr("disabled");
			$("#obat_f_masuk_nofaktur").attr("disabled", "disabled");
			$("#obat_f_masuk_tanggal").val(json.header.tanggal);
			$("#obat_f_masuk_tanggal").removeAttr("disabled");
			$("#obat_f_masuk_tanggal").attr("disabled", "disabled");
			$("#obat_f_masuk_tanggal_datang").val(json.header.tanggal_datang);
			$("#obat_f_masuk_tanggal_datang").removeAttr("disabled");
			$("#obat_f_masuk_tanggal_datang").attr("disabled", "disabled");
			$("#obat_f_masuk_tanggal_tempo").val(json.header.tanggal_tempo);
			$("#obat_f_masuk_tanggal_tempo").removeAttr("disabled");
			$("#obat_f_masuk_tanggal_tempo").attr("disabled", "disabled");
			$("#obat_f_masuk_diskon").val(parseFloat(json.header.diskon).formatMoney("2", ".", ","));
			$("#obat_f_masuk_diskon").removeAttr("disabled");
			$("#obat_f_masuk_diskon").attr("disabled", "disabled");
			$("#obat_f_masuk_t_diskon").val(json.header.t_diskon);
			$("#obat_f_masuk_t_diskon").removeAttr("disabled");
			$("#obat_f_masuk_t_diskon").attr("disabled", "disabled");
			$("#obat_f_masuk_keterangan").val(json.header.keterangan);
			$("#obat_f_masuk_keterangan").removeAttr("disabled");
			$("#obat_f_masuk_keterangan").attr("disabled", "disabled");
			$("#dobat_f_masuk_add").hide();
			row_id = 0;
			$("#dobat_f_masuk_list").children().remove();
			for(var i = 0; i < json.detail.length; i++) {
				var dobat_f_masuk_id = json.detail[i].id;
				var dobat_f_masuk_id_obat_f_masuk = json.detail[i].id_obat_f_masuk;
				var dobat_f_masuk_id_dpo = json.detail[i].id_dpo;
				var dobat_f_masuk_id_obat = json.detail[i].id_obat;
				var dobat_f_masuk_kode_obat = json.detail[i].kode_obat;
				var dobat_f_masuk_nama_obat = json.detail[i].nama_obat;
				var dobat_f_masuk_medis = json.detail[i].medis;
				var dobat_f_masuk_inventaris = json.detail[i].inventaris;
				var dobat_f_masuk_nama_jenis_obat = json.detail[i].nama_jenis_obat;
				var dobat_f_masuk_stok_terkini = json.detail[i].stok_entri;
				var dobat_f_masuk_jumlah_tercatat = json.detail[i].jumlah_tercatat;
				var dobat_f_masuk_jumlah = json.detail[i].jumlah;
				var dobat_f_masuk_sisa = json.detail[i].sisa;
				var dobat_f_masuk_satuan = json.detail[i].satuan;
				var dobat_f_masuk_konversi = json.detail[i].konversi;
				var dobat_f_masuk_satuan_konversi = json.detail[i].satuan_konversi;
				var dobat_f_masuk_hna = "Rp. " + (parseFloat(json.detail[i].hna)).formatMoney("2", ".", ",");
				var dobat_f_masuk_selisih = "Rp. " + (parseFloat(json.detail[i].selisih)).formatMoney("2", ".", ",");
				var dobat_f_masuk_produsen = json.detail[i].produsen;
				var dobat_f_masuk_subtotal = "Rp. " + ((parseFloat(json.detail[i].hna) + parseFloat(json.detail[i].selisih)) * parseFloat(dobat_f_masuk_jumlah)).formatMoney("2", ".", ",");
				var dobat_f_masuk_diskon = json.detail[i].diskon;
				var dobat_f_masuk_t_diskon = json.detail[i].t_diskon;
				var f_diskon = dobat_f_masuk_diskon + " %";
				if (dobat_f_masuk_t_diskon == "nominal")
					f_diskon = "Rp. " + (parseFloat(dobat_f_masuk_diskon)).formatMoney("2", ".", ",");
				var dobat_f_masuk_ed = json.detail[i].tanggal_exp;
				var dobat_f_masuk_nobatch = json.detail[i].no_batch;
				$("tbody#dobat_f_masuk_list").append(
					"<tr id='data_" + row_id + "'>" +
						"<td id='data_" + row_id + "_id' style='display: none;'>" + dobat_f_masuk_id + "</td>" +
						"<td id='data_" + row_id + "_id_obat' style='display: none;'>" + dobat_f_masuk_id_obat + "</td>" +
						"<td id='data_" + row_id + "_kode_obat' style='display: none;'>" + dobat_f_masuk_kode_obat + "</td>" +
						"<td id='data_" + row_id + "_medis' style='display: none;'>" + dobat_f_masuk_medis + "</td>" +
						"<td id='data_" + row_id + "_inventaris' style='display: none;'>" + dobat_f_masuk_inventaris + "</td>" +
						"<td id='data_" + row_id + "_id_dpo' style='display: none;'>" + dobat_f_masuk_id_dpo + "</td>" +
						"<td id='data_" + row_id + "_hna' style='display: none;'>" + dobat_f_masuk_hna + "</td>" +
						"<td id='data_" + row_id + "_selisih' style='display: none;'>" + dobat_f_masuk_selisih + "</td>" +
						"<td id='data_" + row_id + "_diskon' style='display: none;'>" + dobat_f_masuk_diskon + "</td>" +
						"<td id='data_" + row_id + "_t_diskon' style='display: none;'>" + dobat_f_masuk_t_diskon + "</td>" +
						"<td id='data_" + row_id + "_stok_terkini' style='display: none;'>" + dobat_f_masuk_stok_terkini + "</td>" +
						"<td id='data_" + row_id + "_jumlah_tercatat' style='display: none;'>" + dobat_f_masuk_jumlah_tercatat + "</td>" +
						"<td id='data_" + row_id + "_jumlah' style='display: none;'>" + dobat_f_masuk_jumlah + "</td>" +
						"<td id='data_" + row_id + "_sisa' style='display: none;'>" + dobat_f_masuk_sisa + "</td>" +
						"<td id='data_" + row_id + "_satuan' style='display: none;'>" + dobat_f_masuk_satuan + "</td>" +
						"<td id='data_" + row_id + "_konversi' style='display: none;'>" + dobat_f_masuk_konversi + "</td>" +
						"<td id='data_" + row_id + "_satuan_konversi' style='display: none;'>" + dobat_f_masuk_satuan_konversi + "</td>" +
						"<td id='data_" + row_id + "_nama_obat'>" + dobat_f_masuk_nama_obat + "</td>" +
						"<td id='data_" + row_id + "_nama_jenis_obat'>" + dobat_f_masuk_nama_jenis_obat + "</td>" +
						"<td id='data_" + row_id + "_produsen'>" + dobat_f_masuk_produsen + "</td>" +
						"<td id='data_" + row_id + "_f_subtotal'>" + dobat_f_masuk_jumlah + " x (" + dobat_f_masuk_hna + " + " + dobat_f_masuk_selisih + ") = " + dobat_f_masuk_subtotal + "</td>" +
						"<td id='data_" + row_id + "_f_jumlah'>" + dobat_f_masuk_jumlah + " " + dobat_f_masuk_satuan + "</td>" +
						"<td id='data_" + row_id + "_f_konversi'>1 " + dobat_f_masuk_satuan + " = " + dobat_f_masuk_konversi + " " + dobat_f_masuk_satuan_konversi + "</td>" +
						"<td id='data_" + row_id + "_f_diskon'>" + f_diskon + "</td>" +
						"<td id='data_" + row_id + "_ed'>" + dobat_f_masuk_ed + "</td>" +
						"<td id='data_" + row_id + "_nobatch'>" + dobat_f_masuk_nobatch + "</td>" +
						"<td></td>" +
					"</tr>"
				);
				row_id++;
			}
			obat_f_masuk.refreshNoDObatMasuk();
			obat_f_masuk.refreshTotal();
			$("#obat_f_masuk_save").removeAttr("onclick");
			$("#obat_f_masuk_save").hide();
			$("#obat_f_masuk_ok").show();
			$("#modal_alert_obat_f_masuk_add_form").html("");
			$(".error_field").removeClass("error_field");
			$("#obat_f_masuk_add_form").smodal("show");
		}
	);
};
ObatMasukAction.prototype.download_au53 = function(id) {
	var data = this.getRegulerData();
	data['super_command'] = "obat_f_masuk";
	data['command'] = "export_au53";
	data['id'] = id;
	postForm(data);
};