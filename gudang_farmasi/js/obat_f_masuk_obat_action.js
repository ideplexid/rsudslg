function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	$("#dobat_r_masuk_id_obat").val(json.id);
	$("#dobat_r_masuk_kode_obat").val(json.kode);
	$("#dobat_r_masuk_nama_obat").val(json.nama);
	$("#dobat_r_masuk_name_obat").val(json.nama);
	$("#dobat_r_masuk_medis").val(json.medis);
	$("#dobat_r_masuk_inventaris").val(json.inventaris);
	$("#dobat_r_masuk_nama_jenis_obat").val(json.nama_jenis_barang);
	$("#dobat_r_masuk_jumlah").val("");
	$("#dobat_r_masuk_satuan").val(json.satuan);
	$("#dobat_r_masuk_konversi").val(json.konversi);
	$("#dobat_r_masuk_satuan_konversi").val(json.satuan_konversi);
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "obat_r_masuk";
	data['command'] = "get_last_item";
	data['id_obat'] = json.id;
	$.post(
		"",
		data,
		function(response) {
			var jsn = getContent(response);
			if (jsn == null) {
				$("#dobat_r_masuk_hna").val("Rp. 0,00");
				$("#dobat_r_masuk_harga").val("Rp. 0,00");
				$("#dobat_r_masuk_total_hna").val("Rp. 0,00");
				$("#dobat_r_masuk_stok_terkini").val("0");
			} else {
				var hna = jsn.hna;
				var satuan = jsn.satuan;
				var konversi = jsn.konversi;
				var satuan_konversi = jsn.satuan_konversi;
				var produsen = jsn.produsen;
				var harga = (hna * 100) / 110;
				hna = "Rp. " + parseFloat(hna).formatMoney("2", ".", ",");
				harga = "Rp. " + parseFloat(harga).formatMoney("2", 	".", ",");
				$("#dobat_r_masuk_satuan").val(satuan);
				$("#dobat_r_masuk_konversi").val(konversi);
				$("#dobat_r_masuk_satuan_konversi").val(satuan_konversi);
				$("#dobat_r_masuk_produsen").val(produsen);
				$("#dobat_r_masuk_last_hna").val(hna);
				$("#dobat_r_masuk_hna").val(hna);
				$("#dobat_r_masuk_total_hna").val("Rp. 0,00");
				$("#dobat_r_masuk_harga").val(harga);
				var data_stok = self.getRegulerData();
				data_stok['super_command'] = "stok";
				data_stok['command'] = "edit";
				data_stok['id_obat'] = json.id;
				data_stok['satuan'] = jsn.satuan;
				data_stok['konversi'] = jsn.konversi;
				data_stok['satuan_konversi'] = jsn.satuan_konversi;
				$.post(
					"",
					data_stok,
					function(rspns) {
						var json_stok = getContent(rspns);
						if (json_stok == null) return;
						$("#dobat_r_masuk_stok_terkini").val(json_stok.sisa);
					}
				);
			}
		}
	);
};