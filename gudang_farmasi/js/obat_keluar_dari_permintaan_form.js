var row_num;
var mutasi_obat_keluar;
var dmutasi_obat_keluar;
var permintaan;
var obat;

$(document).ready(function() {
	$("#smis-chooser-modal").on("show", function() {
		if ($("#smis-chooser-modal .modal-header h3").text() == "DATA STOK OBAT") {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("full_model");
		} else {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").removeClass("full_model");
			$("#smis-chooser-modal").addClass("half_model");
		}
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("full_model");
		$("#smis-chooser-modal").removeClass("half_model");
	});
	obat = new StokObatAction(
		"obat",
		"gudang_farmasi",
		"obat_keluar_dari_permintaan_form",
		new Array()
	);
	obat.setSuperCommand("obat");
	permintaan = new PermintaanAction(
		"permintaan",
		"gudang_farmasi",
		"obat_keluar_dari_permintaan_form",
		new Array()
	);
	permintaan.setSuperCommand("permintaan");

	mutasi_obat_keluar = new ObatKeluarDariPermintaanAction(
		"mutasi_obat_keluar",
		"gudang_farmasi",
		"obat_keluar_dari_permintaan_form",
		new Array()
	);

	dmutasi_obat_keluar = new DObatKeluarDariPermintaanAction(
		"dmutasi_obat_keluar",
		"gudang_farmasi",
		"obat_keluar_dari_permintaan_form",
		new Array()
	);

	row_num = 0;
	$("tbody#dmutasi_obat_keluar_list").html("<tr><td colspan='7'><center>Permintaan Obat Unit Belum Dipilih</center></td></tr>");

	if ($("#mutasi_obat_keluar_id").val() != "") {
		mutasi_obat_keluar.fillDetail($("#mutasi_obat_keluar_id").val());
	}
});