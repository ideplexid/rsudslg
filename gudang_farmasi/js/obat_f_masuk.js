var obat_r_masuk;
var dobat_r_masuk;
var vendor;
var obat;
var row_id;
$(document).ready(function() {
	$('.modal').on('shown.bs.modal', function() {
		$(this).find('[autofocus]').focus();
	});
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		'placement': 'top'
	});
	$(".mydate").datepicker();
	$("#smis-chooser-modal").on("show", function() {
		if ($("#smis-chooser-modal .modal-header h3").text() == "VENDOR") {
			$("#smis-chooser-modal").removeClass("half_model");
			$("#smis-chooser-modal").addClass("half_model");
		} else {
			$("#smis-chooser-modal").removeClass("half_model");
		}
	});
	$("#smis-chooser-modal").on("hidden", function() {
		$("#smis-chooser-modal").removeClass("half_model");
	});
	vendor = new VendorAction(
		"vendor",
		"gudang_farmasi",
		"obat_reguler_non_sp_masuk",
		new Array()
	);		
	vendor.setSuperCommand("vendor");
	obat = new ObatAction(
		"obat",
		"gudang_farmasi",
		"obat_reguler_non_sp_masuk",
		new Array()
	);		
	obat.setSuperCommand("obat");
	var dobat_r_masuk_columns = new Array("id", "id_obat", "kode_obat", "nama_obat", "medis", "inventaris", "nama_jenis_obat", "label", "jumlah", "sisa", "satuan", "konversi", "satuan_konversi", "hna", "produsen", "diskon", "t_diskon", "harga", "total_hna");
	dobat_r_masuk = new DObatFMasukAction(
		"dobat_r_masuk",
		"gudang_farmasi",
		"obat_reguler_non_sp_masuk",
		dobat_r_masuk_columns
	);
	var obat_r_masuk_columns = new Array("id", "id_po", "no_opl", "no_bbm", "id_vendor", "nama_vendor", "tanggal", "no_faktur", "tanggal", "tanggal_datang", "tanggal_tempo", "diskon", "t_diskon", "keterangan", "total");
	obat_r_masuk = new ObatFMasukAction(
		"obat_r_masuk",
		"gudang_farmasi",
		"obat_reguler_non_sp_masuk",
		obat_r_masuk_columns
	);
	obat_r_masuk.setSuperCommand("obat_r_masuk");
	obat_r_masuk.view();
	
	$("#obat_r_masuk_diskon").on("change", function() {
		var diskon = $("#obat_r_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (diskon == "") {
			$("#obat_r_masuk_diskon").val("0,00");
		}
	});
	
	$("#obat_r_masuk_t_diskon").on("change", function() {
		var diskon = $("#obat_r_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#obat_r_masuk_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			$("#modal_alert_obat_r_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
				"</div>"
			);
			return;
		}
		$("#modal_alert_obat_r_masuk_add_form").html("");
		obat_r_masuk.refreshTotal();
	});
	
	$("#obat_r_masuk_diskon").on("keyup", function() {
		var diskon = $("#obat_r_masuk_diskon").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		var t_diskon = $("#obat_r_masuk_t_diskon").val();
		if (diskon > 100 && t_diskon == "persen") {
			$("#modal_alert_obat_r_masuk_add_form").html(
				"<div class='alert alert-block alert-danger'>" +
					"<h4>Peringatan</h4>" +
					"<br /><b>Diskon</b> tidak boleh melebihi 100%" +
				"</div>"
			);
			return;
		} 
		$("#modal_alert_obat_r_masuk_add_form").html("");
		obat_r_masuk.refreshTotal();
	});
	
	$("#dobat_r_masuk_harga").on("change", function() {
		var hna = $("#dobat_r_masuk_harga").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (hna == "") {
			$("#dobat_r_masuk_harga").val("Rp. 0,00");
		}
	});
	$("#dobat_r_masuk_hna").on("change", function() {
		var hpp = $("#dobat_r_masuk_hna").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (hpp == "") {
			$("#dobat_r_masuk_hna").val("Rp. 0,00");
		}
	});
	$("#dobat_r_masuk_total_hna").on("change", function() {
		var total_hna = $("#dobat_r_masuk_total_hna").val().replace(/[^0-9-,]/g, '').replace(",", ".");
		if (total_hna == "") {
			$("#dobat_r_masuk_total_hna").val("Rp. 0,00");
		}
	});
	
	$("#dobat_r_masuk_jumlah, #dobat_r_masuk_total_hna").on("keyup", function() {
		var total_hna = parseFloat($("#dobat_r_masuk_total_hna").val().replace(/[^0-9-,]/g, '').replace(",", "."));
		var jumlah = $("#dobat_r_masuk_jumlah").val();
		if (total_hna != "" && jumlah != "") {
			var hna = total_hna / jumlah;
			var hpp = hna + hna / 10;
			var f_hna = "Rp. " + parseFloat(hna).formatMoney("2", ".", ",");
			var f_hpp = "Rp. " + parseFloat(hpp).formatMoney("2", ".", ",");
			$("#dobat_r_masuk_harga").val(f_hna);
			$("#dobat_r_masuk_hna").val(f_hpp);
		}
	});
	
	$('#dobat_r_masuk_nama_obat').typeahead({
		minLength:2,
		source: function (query, process) {
			var produsen_data = {	
				page: "gudang_farmasi",
				action: "obat_reguler_non_sp_masuk",
				super_command: "obat",
				command: "list",
				prototype_name: "",
				prototype_slug: "",
				prototype_implement: "",
				kriteria: $("#dobat_r_masuk_nama_obat").val()
			};
			var items = new Array;
			items = [""];
			$.ajax({
				url: '',
				type: 'POST',
				data: produsen_data,
				success: function(res) {
					var json = getContent(res);
					var the_data_proses = json.d.data;
					items = [""];	
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.nama,
							nama: data.nama,
							kode: data.kode,
							medis: data.medis,
							inventaris: data.inventaris,
							nama_jenis_barang: data.nama_jenis_barang,
							satuan: data.satuan,
							konversi: data.konversi,
							satuan_konversi: data.satuan_konversi,
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						items.push(group);
					});
					process(items);
				}
			});
		},
		updater: function (item) {
			var item = JSON.parse(item);		
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			obat.selected(item);	
			$("#dobat_r_masuk_produsen").focus();
			return item.name;
		}
	});
	
	$("#dobat_r_masuk_nama_obat").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_produsen").focus();
		}
	});
	
	$('#dobat_r_masuk_produsen').typeahead({
		minLength:2,
		source: function (query, process) {
			var produsen_data = {	
				page: "gudang_farmasi",
				action: "obat_reguler_non_sp_masuk",
				super_command: "produsen",
				command: "list",
				prototype_name: "",
				prototype_slug: "",
				prototype_implement: "",
				kriteria: $("#dobat_r_masuk_produsen").val()
			};
			var items = new Array;
			items = [""];
			$.ajax({
				url: '',
				type: 'POST',
				data: produsen_data,
				success: function(res) {
					var json = getContent(res);
					var the_data_proses = json.dbtable.data;
					items = [""];	
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.produsen,                            
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						items.push(group);
					});
					process(items);
				}
			});
		},
		updater: function (item) {
			var item = JSON.parse(item);
			$("#dobat_r_masuk_produsen").val(item.name);
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			$("#dobat_r_masuk_jumlah_tertulis").focus();
			return item.name;
		}
	});
	
	$("#dobat_r_masuk_produsen").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_jumlah_tertulis").focus();
		}
	});
	
	$("#dobat_r_masuk_jumlah_tertulis").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_jumlah").focus();
		}
	});
	
	$("#dobat_r_masuk_jumlah").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_satuan").focus();
		}
	});
	
	$('#dobat_r_masuk_satuan').typeahead({
		minLength:2,
		source: function (query, process) {
			var satuan_data = {	
				page: "gudang_farmasi",
				action: "obat_reguler_non_sp_masuk",
				super_command: "satuan",
				command: "list",
				prototype_name: "",
				prototype_slug: "",
				prototype_implement: "",
				kriteria: $("#dobat_r_masuk_satuan").val()
			};
			var items = new Array;
			items = [""];
			$.ajax({
				url: '',
				type: 'POST',
				data: satuan_data,
				success: function(res) {
					var json = getContent(res);
					var the_data_proses = json.dbtable.data;
					items = [""];	
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.satuan,                            
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						items.push(group);
					});
					process(items);
				}
			});
		},
		updater: function (item) {
			var item = JSON.parse(item);
			$("#dobat_r_masuk_satuan").val(item.name);
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			$("#dobat_r_masuk_konversi").focus();
			return item.name;
		}
	});
	
	$("#dobat_r_masuk_satuan").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_konversi").focus();
		}
	});
	
	$("#dobat_r_masuk_konversi").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_satuan_konversi").focus();
		}
	});
	
	$('#dobat_r_masuk_satuan_konversi').typeahead({
		minLength:2,
		source: function (query, process) {
			var satuan_data = {	
				page: "gudang_farmasi",
				action: "obat_reguler_non_sp_masuk",
				super_command: "satuan",
				command: "list",
				prototype_name: "",
				prototype_slug: "",
				prototype_implement: "",
				kriteria: $("#dobat_r_masuk_satuan_konversi").val()
			};
			var items = new Array;
			items = [""];
			$.ajax({
				url: '',
				type: 'POST',
				data: satuan_data,
				success: function(res) {
					var json = getContent(res);
					var the_data_proses = json.dbtable.data;
					items = [""];	
					$.map(the_data_proses, function(data) {
						var group;
						group = {
							id: data.id,
							name: data.satuan,                            
							toString: function () {
								return JSON.stringify(this);
							},
							toLowerCase: function () {
								return this.name.toLowerCase();
							},
							indexOf: function (string) {
								return String.prototype.indexOf.apply(this.name, arguments);
							},
							replace: function (string) {
								var value = '';
								value +=  this.name;
								if(typeof(this.level) != 'undefined') {
									value += ' <span class="pull-right muted">';
									value += this.level;
									value += '</span>';
								}
								return String.prototype.replace.apply('<div class="typeaheadiv">' + value + '</div>', arguments);
							}
						};
						items.push(group);
					});
					process(items);
				}
			});
		},
		updater: function (item) {
			var item = JSON.parse(item);
			$("#dobat_r_masuk_satuan_konversi").val(item.name);
			$("ul.typeahead").html("");
			$("ul.typeahead").hide();
			$("#dobat_r_masuk_total_hna").focus();
			return item.name;
		}
	});
	
	$("#dobat_r_masuk_satuan_konversi").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_total_hna").focus();
		}
	});
	
	$("#dobat_r_masuk_total_hna").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_diskon").focus();
		}
	});
	
	$("#dobat_r_masuk_diskon").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_t_diskon").focus();
		}
	});
	
	$("#dobat_r_masuk_t_diskon").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		e.preventDefault();
		if(e.which == 13) {
			$("#dobat_r_masuk_nobatch").focus();
		}
	});
	
	$("#dobat_r_masuk_nobatch").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$("#dobat_r_masuk_ed").focus();
		}
	});
	
	$("#dobat_r_masuk_ed").keypress(function(e) {
		$("ul.typeahead").html("");
		$("ul.typeahead").hide();
		if(e.which == 13) {
			$(".datepicker").hide();
			$('#dobat_r_masuk_save').trigger('click');
		}
	});
});