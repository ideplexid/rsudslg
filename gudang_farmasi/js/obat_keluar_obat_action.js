function ObatAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
ObatAction.prototype.constructor = ObatAction;
ObatAction.prototype = new TableAction();
ObatAction.prototype.selected = function(json) {
	$("#dobat_keluar_id_obat").val(json.header.id_obat);
	$("#dobat_keluar_kode_obat").val(json.header.kode_obat);
	$("#dobat_keluar_name_obat").val(json.header.nama_obat);
	$("#dobat_keluar_nama_obat").val(json.header.nama_obat);
	$("#dobat_keluar_nama_jenis_obat").val(json.header.nama_jenis_obat);
	$("#dobat_keluar_satuan").html(json.satuan_option);
	this.setDetailInfo();
};
ObatAction.prototype.setDetailInfo = function() {
	var part = $("#dobat_keluar_satuan").val().split("_");
	$("#dobat_keluar_konversi").val(part[0]);
	$("#dobat_keluar_satuan_konversi").val(part[1]);
	var self = this;
	var data = this.getRegulerData();
	data['super_command'] = "sisa";
	data['command'] = "edit";
	data['id_obat'] = $("#dobat_keluar_id_obat").val();
	data['satuan'] = $("#dobat_keluar_satuan").find(":selected").text();
	data['konversi'] = $("#dobat_keluar_konversi").val();
	data['satuan_konversi'] = $("#dobat_keluar_satuan_konversi").val();
	$.post(
		"",
		data,
		function(response) {
			var json = getContent(response);
			if (json == null) return;
			$("#dobat_keluar_stok").val(json.sisa);
			$("#dobat_keluar_f_stok").val(json.sisa + " " + json.satuan);
			data['super_command'] = "harga_ma";
			$.post(
				"",
				data, 
				function(rspns) {
					var jsn = getContent(rspns);
					if (jsn == null) return;
					$("#dobat_keluar_harga_ma").val(jsn.harga_ma);
				}
			);
		}
	);
};