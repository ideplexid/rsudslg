function VendorAction(name, page, action, column) {
	this.initialize(name, page, action, column);
}
VendorAction.prototype.constructor = VendorAction;
VendorAction.prototype = new TableAction();
VendorAction.prototype.selected = function(json) {
	$("#obat_r_masuk_id_vendor").val(json.id);
	$("#obat_r_masuk_nama_vendor").val(json.nama);
};