<?php
	class SetPermintaanObatStatusServiceConsumer extends ServiceConsumer {
		public function __construct($db, $f_id, $unit, $status, $detail, $command) {
			$array['id'] = $f_id;
			$array['status'] = $status;
			$array['detail'] = json_encode($detail);
			$array['command'] = $command;
			parent::__construct($db, "set_permintaan_obat_status", $array, $unit);
		}
		public function proceedResult() {
			$content = array();
			$result = json_decode($this->result,true);
			foreach ($result as $autonomous) {
				foreach ($autonomous as $entity) {
					if ($entity != null || $entity != "")
						return $entity;
				}
			}
			return $content;
		}
	}
?>