<?php
class PushObatPembelianUPServiceConsumer extends ServiceConsumer {
	public function __construct($db, $id_obat_keluar, $tanggal, $unit, $status, $keterangan, $username, $detail, $command) {
		$array['id_obat_keluar'] = $id_obat_keluar;
		$array['tanggal'] = $tanggal;
		$array['unit'] = $unit;
		$array['status'] = $status;
		$array['keterangan'] = $keterangan;
		$array['username'] = $username;
		$array['detail'] = json_encode($detail);
		$array['command'] = $command;
		parent::__construct($db, "push_obat_pembelian_up", $array, $unit);
	}
	public function proceedResult() {
		$content = array();
		$result = json_decode($this->result,true);
		foreach ($result as $autonomous) {
			foreach ($autonomous as $entity) {
				if ($entity != null || $entity != "")
					return $entity;
			}
		}
		return $content;
	}
}
?>