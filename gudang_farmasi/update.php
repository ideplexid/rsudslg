<?php 
	require_once("smis-libs-class/DBCreator.php");
	require_once("smis-libs-inventory/install.php");
	global $wpdb;
	
	$install = new InventoryInstallator($wpdb, "", "");
	$install->setPropertyObat("obat_masuk", false);
	$install->setPropertyObat("stok_obat", false);
	$install->setPropertyObat("riwayat_stok_obat", false);
	$install->setPropertyObat("penyesuaian", false);
	$install->setPropertyObat("retur", false);
	$install->setPropertyObat("permintaan", false);
	$install->extendInstall("fr");
	$install->install();

	$dbcreator = new DBCreator($wpdb,"smis_fr_mutasi_obat_keluar", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("f_id_permintaan_obat_unit", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("f_id_obat_keluar", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_permintaan", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_mutasi", "datetime", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->setDuplicate(false);
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb,"smis_fr_dmutasi_obat_keluar", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_mutasi_obat_keluar", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("f_id_dpermintaan_obat_unit", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("f_id_dobat_keluar", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "varchar(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "varchar(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_diminta", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_dipenuhi", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "varchar(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->setDuplicate(false);
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb,"smis_fr_stok_mutasi_obat_keluar", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("id_dmutasi_obat_keluar", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("f_id_stok_obat_keluar", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "int(11)", DBCreator::$SIGN_NONE, true, 0, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->setDuplicate(false);
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_obat_f_masuk");
	$dbcreator->addColumn("id_po", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_opl", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_bbm", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_faktur", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_datang", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_tempo", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diskon", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("t_diskon", "ENUM('persen','nominal')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("materai", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tipe", "ENUM('farmasi','sito','konsinyasi')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("use_ppn", "TINYINT(1)", DBCreator::$SIGN_NONE, true, 1, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ppn", "DOUBLE", DBCreator::$SIGN_NONE, true, 10, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("total_tagihan", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("total_dibayar", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_dobat_f_masuk");
	$dbcreator->addColumn("id_obat_f_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_dpo", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("medis", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("inventaris", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("formularium", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("berlogo", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("generik", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("label", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("stok_entri", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_tercatat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hna", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ppn", "DOUBLE", DBCreator::$SIGN_NONE, true, 10, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("selisih", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diskon", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("t_diskon", "ENUM('persen','nominal')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("produsen", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_stok_obat");
	$dbcreator->addColumn("id_dobat_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("formularium", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("berlogo", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("generik", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("label", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("retur", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hna", "double", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ppn", "DOUBLE", DBCreator::$SIGN_NONE, true, 10, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("produsen", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_exp", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_batch", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("turunan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_kartu_stok");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_bon", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_obat_keluar");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nomor", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_dobat_keluar");
	$dbcreator->addColumn("id_obat_keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("stok_entri", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("harga_ma", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_diminta", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_stok_obat_keluar");
	$dbcreator->addColumn("id_dobat_keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_retur_obat_unit");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("lf_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("label", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("konversi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_konversi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("hna", "DOUBLE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("ppn", "DOUBLE", DBCreator::$SIGN_NONE, true, 10, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("produsen", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_exp", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_batch", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("turunan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("restok", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_penyesuaian_stok");
	$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_lama", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_baru", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_user", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_permintaan_obat_unit");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("unit", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("status", "ENUM('belum','sudah','dikembalikan')", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
		
	$dbcreator = new DBCreator($wpdb, "smis_fr_dpermintaan_obat_unit");
	$dbcreator->addColumn("f_id", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_permintaan_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(256)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_permintaan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_permintaan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_dipenuhi", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("satuan_dipenuhi", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_retur_obat");
	$dbcreator->addColumn("id_obat_f_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_faktur", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_faktur", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("dibatalkan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_dretur_obat");
	$dbcreator->addColumn("id_retur_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_riwayat_stok_obat");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_masuk", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah_keluar", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("sisa", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_user", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_retur_obat_non_faktur");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("no_faktur", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_faktur", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("dibatalkan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_pemusnahan_obat");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_karyawan", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_karyawan", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("dibatalkan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_dpemusnahan_obat");
	$dbcreator->addColumn("id_pemusnahan_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_stok_obat", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("jumlah", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_faktur_non_stok");
	$dbcreator->addColumn("no_faktur", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_datang", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_tempo", "DATE", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_vendor", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_vendor", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("total", "FLOAT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diskon", "FLOAT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("t_diskon", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("dibatalkan", "TINYINT(1)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();	
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_dfaktur_non_stok");
	$dbcreator->addColumn("id_faktur_non_stok", "INT(11)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_tagihan", "VARCHAR(64)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nominal", "FLOAT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("diskon", "FLOAT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_ZERO, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("t_diskon", "VARCHAR(32)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("keterangan", "TEXT", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
	
	$dbcreator = new DBCreator($wpdb, "smis_fr_kode_mutasi_ruangan");
	$dbcreator->addColumn("kode", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("slug_ruangan", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_ruangan", "VARCHAR(128)", DBCreator::$SIGN_NONE, true, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();

	$dbcreator = new DBCreator($wpdb, "smis_fr_stock_opname", DBCreator::$ENGINE_INNODB);
	$dbcreator->addColumn("waktu", "DATETIME", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("tanggal_so", "DATETIME", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("id_obat", "INT", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("kode_obat", "VARCHAR(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_obat", "VARCHAR(256)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("nama_jenis_obat", "VARCHAR(256)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("stok_sebelum", "DOUBLE", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("stok_sesudah", "DOUBLE", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->addColumn("operator", "VARCHAR(256)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false, DBCreator::$ON_UPDATE_NONE, "");
	$dbcreator->initialize();
?>