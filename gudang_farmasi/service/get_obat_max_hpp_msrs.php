<?php
	global $db;
	
	if (isset($_POST['id_obat'])) {
		$id_obat = $_POST['id_obat'];
		$dbtable = new DBTable($db, "smis_fr_stok_obat");
		$max_hpp = 0;
		$row = $dbtable->get_row("
			SELECT MAX(hna) AS 'max_hpp'
			FROM (
				SELECT DISTINCT a.hna
				FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND a.konversi = 1 AND b.id_obat = '" . $id_obat . "'
				UNION
				SELECT DISTINCT (a.hna / a.konversi) AS 'hna'
				FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND a.konversi > 1 AND b.id_obat = '" . $id_obat . "'
			) v
		");
		if ($row != null)
			$max_hpp = $row->max_hpp;
		$data = array();
		$data['max_hpp'] = $max_hpp;
		echo json_encode($data);
	}
?>