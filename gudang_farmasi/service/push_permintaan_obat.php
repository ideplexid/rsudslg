<?php 
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_save") {
			$permintaan_obat_dbtable = new DBTable($db, "smis_fr_permintaan_obat_unit");
			$permintaan_obat_row = $permintaan_obat_dbtable->get_row("
				SELECT id
				FROM smis_fr_permintaan_obat_unit
				WHERE f_id = '" . $_POST['id_permintaan_obat'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($permintaan_obat_row == null) {
				//do insert:
				$permintaan_obat_data = array();
				$permintaan_obat_data['f_id'] = $_POST['id_permintaan_obat'];
				$permintaan_obat_data['unit'] = $_POST['unit'];
				$permintaan_obat_data['tanggal'] = $_POST['tanggal'];
				$permintaan_obat_data['status'] = $_POST['status'];
				$permintaan_obat_dbtable->insert($permintaan_obat_data);
				$permintaan_obat_id = $permintaan_obat_dbtable->get_inserted_id();
				$permintaan_obat_detail = json_decode($_POST['detail'], true);
				$dpermintaan_obat_dbtable = new DBTable($db, "smis_fr_dpermintaan_obat_unit");
				foreach($permintaan_obat_detail as $dpermintaan_obat) {
					$dpermintaan_obat_data = array();
					$dpermintaan_obat_data['f_id'] = $dpermintaan_obat['id'];
					$dpermintaan_obat_data['id_permintaan_obat'] = $permintaan_obat_id;
					$dpermintaan_obat_data['id_obat'] = $dpermintaan_obat['id_obat'];
					$dpermintaan_obat_data['kode_obat'] = $dpermintaan_obat['kode_obat'];
					$dpermintaan_obat_data['nama_obat'] = $dpermintaan_obat['nama_obat'];
					$dpermintaan_obat_data['nama_jenis_obat'] = $dpermintaan_obat['nama_jenis_obat'];
					$dpermintaan_obat_data['jumlah_permintaan'] = $dpermintaan_obat['jumlah_permintaan'];
					$dpermintaan_obat_data['satuan_permintaan'] = $dpermintaan_obat['satuan_permintaan'];
					$dpermintaan_obat_data['jumlah_dipenuhi'] = $dpermintaan_obat['jumlah_dipenuhi'];
					$dpermintaan_obat_data['satuan_dipenuhi'] = $dpermintaan_obat['satuan_dipenuhi'];
					$dpermintaan_obat_data['keterangan'] = $dpermintaan_obat['keterangan'];
					$dpermintaan_obat_dbtable->insert($dpermintaan_obat_data);
				}
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$permintaan_obat_id);
				$msg="Permintaan Obat dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> masuk";
				$notification->addNotification("Permintaan Obat Unit Masuk", $key, $msg, "gudang_farmasi","permintaan_obat_unit");
				$notification->commit();
			} else {
				//do update:
				$permintaan_obat_id_data['id'] = $permintaan_obat_row->id;
				$permintaan_obat_data = array();
				$permintaan_obat_data['tanggal'] = $_POST['tanggal'];
				$permintaan_obat_data['status'] = $_POST['status'];
				$permintaan_obat_dbtable->update($permintaan_obat_data, $permintaan_obat_id_data);
				$permintaan_obat_detail = json_decode($_POST['detail'], true);
				$dpermintaan_obat_dbtable = new DBTable($db, "smis_fr_dpermintaan_obat_unit");
				foreach($permintaan_obat_detail as $dpermintaan_obat) {
					$dpermintaan_obat_row = $dpermintaan_obat_dbtable->get_row("
						SELECT id
						FROM smis_fr_dpermintaan_obat_unit
						WHERE f_id = '" . $dpermintaan_obat['id'] . "' AND id_permintaan_obat = '" . $permintaan_obat_row->id . "'
					");
					if ($dpermintaan_obat_row == null) {
						//do insert if f_id not exist:
						$dpermintaan_obat_data = array();
						$dpermintaan_obat_data['f_id'] = $dpermintaan_obat['id'];
						$dpermintaan_obat_data['id_permintaan_obat'] = $permintaan_obat_id;
						$dpermintaan_obat_data['id_obat'] = $dpermintaan_obat['id_obat'];
						$dpermintaan_obat_data['kode_obat'] = $dpermintaan_obat['kode_obat'];
						$dpermintaan_obat_data['nama_obat'] = $dpermintaan_obat['nama_obat'];
						$dpermintaan_obat_data['nama_jenis_obat'] = $dpermintaan_obat['nama_jenis_obat'];
						$dpermintaan_obat_data['jumlah_permintaan'] = $dpermintaan_obat['jumlah_permintaan'];
						$dpermintaan_obat_data['satuan_permintaan'] = $dpermintaan_obat['satuan_permintaan'];
						$dpermintaan_obat_data['jumlah_dipenuhi'] = $dpermintaan_obat['jumlah_dipenuhi'];
						$dpermintaan_obat_data['satuan_dipenuhi'] = $dpermintaan_obat['satuan_dipenuhi'];
						$dpermintaan_obat_data['keterangan'] = $dpermintaan_obat['keterangan'];
						$dpermintaan_obat_dbtable->insert($dpermintaan_obat_data);
					} else {
						//do update or delete if f_id exist:
						$dpermintaan_obat_id_data['id'] = $dpermintaan_obat_row->id;
						$dpermintaan_obat_data = array();
						$dpermintaan_obat_data['id_obat'] = $dpermintaan_obat['id_obat'];
						$dpermintaan_obat_data['kode_obat'] = $dpermintaan_obat['kode_obat'];
						$dpermintaan_obat_data['nama_obat'] = $dpermintaan_obat['nama_obat'];
						$dpermintaan_obat_data['nama_jenis_obat'] = $dpermintaan_obat['nama_jenis_obat'];
						$dpermintaan_obat_data['jumlah_permintaan'] = $dpermintaan_obat['jumlah_permintaan'];
						$dpermintaan_obat_data['satuan_permintaan'] = $dpermintaan_obat['satuan_permintaan'];
						$dpermintaan_obat_data['prop'] = $dpermintaan_obat['prop'];
						$dpermintaan_obat_dbtable->update($dpermintaan_obat_data, $dpermintaan_obat_id_data);
					}
				}
				$response_package->setStatus(ResponsePackage::$STATUS_OK);
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$permintaan_obat_row->id);
				$msg="Permintaan Obat dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> diperbarui";
				$notification->addNotification("Permintaan Obat Unit Diperbarui", $key, $msg, "gudang_farmasi","permintaan_obat_unit");
				$notification->commit();
			}
		} else if ($_POST['command'] == "push_delete") {
			$permintaan_obat_dbtable = new DBTable($db, "smis_fr_permintaan_obat_unit");
			$permintaan_obat_row = $permintaan_obat_dbtable->get_row("
				SELECT id
				FROM smis_fr_permintaan_obat_unit
				WHERE f_id = '" . $_POST['id_permintaan_obat'] . "' AND unit = '" . $_POST['unit'] . "'
			");
			if ($permintaan_obat_row != null) {
				$permintaan_obat_id_data['id'] = $permintaan_obat_row->id;
				$permintaan_obat_data['prop'] = "del";
				$permintaan_obat_dbtable->update($permintaan_obat_data, $permintaan_obat_id_data);
				//notify:
				global $notification;
				$key=md5($_POST['unit']." ".$permintaan_obat_row->id);
				$msg="Permintaan Obat dari <strong>".ArrayAdapter::format("unslug", $_POST['unit'])."</strong> dibatalkan";
				$notification->addNotification("Permintaan Obat Unit Batal", $key, $msg, "gudang_farmasi","permintaan_obat_unit");
				$notification->commit();
			}
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());
?>