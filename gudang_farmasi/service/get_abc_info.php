<?php
	global $db;
	
	if (isset($_POST['nama_obat']) && 
		isset($_POST['nama_jenis_obat']) &&
		isset($_POST['tanggal_awal']) &&
		isset($_POST['tanggal_akhir'])) {
		$nama_obat = $_POST['nama_obat'];
		$nama_jenis_obat = $_POST['nama_jenis_obat'];
		$tanggal_awal = $_POST['tanggal_awal'];
		$tanggal_akhir = $_POST['tanggal_akhir'];
		$dbtable = new DBTable($db, "smis_fr_dobat_f_masuk");
		$row = $dbtable->get_row("
			SELECT b.nama_obat, b.nama_jenis_obat, SUM(b.jumlah) AS 'jumlah', SUM(b.jumlah * b.hna) AS 'nominal_investasi'
			FROM smis_fr_obat_f_masuk a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
			WHERE a.prop NOT LIKE 'del' AND a.id > 0 AND a.tanggal >= '" . $tanggal_awal . "' AND a.tanggal <= '" . $tanggal_akhir . "' AND b.prop NOT LIKE 'del' AND b.nama_obat = '" . $nama_obat . "' AND b.nama_jenis_obat = '" . $nama_jenis_obat . "'
			GROUP BY b.nama_obat, b.nama_jenis_obat
		");
		$nominal_investasi = 0;
		$jumlah = 0;
		if ($row != null) {
			$nominal_investasi = $row->nominal_investasi;
			$jumlah = $row->jumlah;
		}
		
		$data = array();
		$data['nominal_investasi'] = $nominal_investasi;
		$data['jumlah'] = $jumlah;
		$data['unit'] = "gudang_farmasi";
		echo json_encode($data);
	}
?>