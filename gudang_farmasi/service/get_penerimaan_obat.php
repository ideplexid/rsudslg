<?php
	global $db;

	$bulan = $_POST['bulan'];
	$tahun = $_POST['tahun'];
	$id_obat_csv = $_POST['id_obat_csv'];
	$jumlah = 0;
	$row = $db->get_row("
		SELECT SUM(b.jumlah) jumlah
		FROM smis_fr_obat_f_masuk a INNER JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
		WHERE a.prop = '' AND b.prop = '' AND YEAR(a.tanggal_datang) = '" . $tahun . "' AND MONTH(a.tanggal_datang) LIKE '" . $bulan . "' AND b.id_obat IN (" . $id_obat_csv . ")
	");
	if ($row != null)
		$jumlah = $row->jumlah;
	$data = array(
		'jumlah' => $jumlah
	);
	echo json_encode($data);
?>