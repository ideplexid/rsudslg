<?php
	global $db;

	if (isset($_POST['id_obat']) && isset($_POST['satuan'])) {
		$dbtable = new DBTable($db, "smis_fr_dobat_f_masuk");
		$max_row = $dbtable->get_row("
			SELECT MAX(id) AS 'id'
			FROM smis_fr_dobat_f_masuk
			WHERE id_obat = '" . $_POST['id_obat'] . "' AND satuan = '" . $_POST['satuan'] . "' AND konversi = '" . $_POST['konversi'] . "' AND satuan_konversi = '" . $_POST['satuan_konversi'] . "'
		");
		$last_hpp = 0;
		$last_ppn = 0;
		if ($max_row != null) {
			$row = $dbtable->select($max_row->id);
			$last_hpp = $row->hna;
			$last_ppn = $row->ppn;
		}
		$data['data'] = array(
			"hpp"	=> $last_hpp,
			"ppn"	=> $last_ppn
		);
		echo json_encode($data);
	}
?>