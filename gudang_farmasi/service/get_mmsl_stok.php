<?php
	global $db;
	
	if (isset($_POST['nama_obat']) && 
		isset($_POST['nama_jenis_obat'])) {
		$nama_obat = $_POST['nama_obat'];
		$nama_jenis_obat = $_POST['nama_jenis_obat'];
		$dbtable = new DBTable($db, "smis_fr_stok_obat");
		$row = $dbtable->get_row("
			SELECT nama_obat, nama_jenis_obat, SUM(sisa) AS 'jumlah'
			FROM smis_fr_stok_obat
			WHERE prop NOT LIKE 'del' AND nama_obat = '" . $nama_obat . "' AND nama_jenis_obat = '" . $nama_jenis_obat . "'
			GROUP BY nama_obat, nama_jenis_obat
		");
		$sisa_stok = 0;
		if ($row != null)
			$sisa_stok = $row->jumlah;
		
		$data = array();
		$data['jumlah'] = $sisa_stok;
		$data['unit'] = "gudang_farmasi";
		echo json_encode($data);
	}
?>