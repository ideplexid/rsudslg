<?php
	global $db;
	
	if (isset($_POST['id_obat'])) {
		$id_obat = $_POST['id_obat'];
		$dbtable = new DBTable($db, "smis_fr_stok_obat");
		$hna_ma = 0;
		$row = $dbtable->get_row("
			SELECT AVG(hna) AS 'hna_ma'
			FROM (
				SELECT DISTINCT (a.hna / a.konversi) AS 'hna'
				FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND a.konversi = 1 AND b.id_obat = '" . $id_obat . "'
				UNION
				SELECT DISTINCT (a.hna / a.konversi) AS 'hna'
				FROM smis_fr_stok_obat a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk = b.id
				WHERE a.prop NOT LIKE 'del' AND a.konversi > 1 AND b.id_obat = '" . $id_obat . "'
			) v
		");
		if ($row != null)
			$hna_ma = $row->hna_ma;
		$data = array();
		$data['hna_ma'] = $hna_ma;
		echo json_encode($data);
	}
?>