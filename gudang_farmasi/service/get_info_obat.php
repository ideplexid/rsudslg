<?php 
	$response_package = new ResponsePackage();
	if (isset($_POST['command']) && $_POST['command'] == "edit" && isset($_POST['id_obat']) && isset($_POST['nama_obat']) && isset($_POST['nama_jenis_obat'])) {
		$id_obat = $_POST['id_obat'];
		$nama_obat = $_POST['nama_obat'];
		$nama_jenis_obat = $_POST['nama_jenis_obat'];
		$stock_dbtable = new DBTable($db, "smis_fr_dobat_f_masuk");
		$row = $stock_dbtable->get_row("
			SELECT MAX(a.id) AS 'max_id'
			FROM smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_obat_f_masuk b ON a.id_obat_f_masuk = b.id
			WHERE a.id_obat = '" . $id_obat . "' AND a.nama_obat = '" . $nama_obat . "' AND a.nama_jenis_obat = '" . $nama_jenis_obat . "' AND a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del'
		");
		$info_row = $stock_dbtable->get_row("
			SELECT *
			FROM smis_fr_dobat_f_masuk
			WHERE id = '" . $row->max_id . "'
		");
		$response_package->setContent($info_row);
		$response_package->setStatus(ResponsePackage::$STATUS_OK);
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());
?>