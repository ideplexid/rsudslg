<?php
	global $db;
	
	$tahun_rows = $db->get_result("
		SELECT DISTINCT YEAR(tanggal_datang) tahun
		FROM smis_fr_obat_f_masuk
		WHERE prop = '' AND YEAR(tanggal_datang) > 0
		ORDER BY YEAR(tanggal_datang) ASC
	");
	$data = array();
	if ($tahun_rows != null) {
		foreach ($tahun_rows as $tr)
			$data[] = $tr->tahun;
	}
	echo json_encode($data);
?>