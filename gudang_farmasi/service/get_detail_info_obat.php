<?php
	require_once("gudang_farmasi/kelas/GudangFarmasiInventory.php");
	global $db;

	if (isset($_POST['id_obat']) && isset($_POST['satuan']) && isset($_POST['konversi']) && isset($_POST['satuan_konversi']) && isset($_POST['tanggal_from']) && isset($_POST['tanggal_to'])) {
		$id_obat = $_POST['id_obat'];
		$satuan = $_POST['satuan'];
		$konversi = $_POST['konversi'];
		$satuan_konversi = $_POST['satuan_konversi'];
		$tanggal_from = $_POST['tanggal_from'];
		$tanggal_to = $_POST['tanggal_to'];
		// Mendapatkan Saldo Akhir:
		$saldo_sekarang = GudangFarmasiInventory::getCurrentStock($db, $id_obat, $satuan, 1, $satuan_konversi);
		$jumlah_masuk = GudangFarmasiInventory::getStockIn($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + GudangFarmasiInventory::getPenyesuaianStokPositif($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
		$jumlah_keluar = GudangFarmasiInventory::getStockOut($db, $id_obat, $satuan, 1, $satuan_konversi,date("Y-m-d", strtotime($tanggal_to . " +1 day")), date("Y-m-d")) + GudangFarmasiInventory::getPenyesuaianStokNegatif($db, $id_obat, $satuan, 1, $satuan_konversi, date("Y-m-d", strtotime($tanggal_to . "+1 day")), date("Y-m-d"));
		$saldo_akhir = $saldo_sekarang - $jumlah_masuk + $jumlah_keluar;
		// Mendapatkan Penyesuaian Stok:
		$penyesuaian_stok = GudangFarmasiInventory::getPenyesuaianStokPositif($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to) - GudangFarmasiInventory::getPenyesuaianStokNegatif($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
		// Mendapatkan Jumlah Masuk:
		$jumlah_masuk = GudangFarmasiInventory::getStockIn($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
		// Mendapatkan Jumlah Keluar:
		$jumlah_keluar = GudangFarmasiInventory::getStockOut($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
		// Mendapatkan Saldo Awal:
		$saldo_awal = $saldo_akhir - $jumlah_masuk + $jumlah_keluar - $penyesuaian_stok;

		// Jumlah Beli:
		$jumlah_beli = GudangFarmasiInventory::getOrderIn($db, $id_obat, $satuan, 1, $satuan_konversi, $tanggal_from, $tanggal_to);
		// Nominal Beli:
		$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
		$data = $dbtable->get_result("
			SELECT a.id AS 'id_obat_masuk', b.id_obat, b.kode_obat, b.nama_obat, b.satuan, b.jumlah, b.hna AS 'hpp', a.diskon AS 'diskon_global', a.t_diskon AS 't_diskon_global', b.diskon AS 'diskon_detail', b.t_diskon AS 't_diskon_detail'
			FROM smis_fr_obat_f_masuk a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND a.tanggal_datang >= '" . $tanggal_from . "' AND a.tanggal_datang <= '" . $tanggal_to . "' AND b.jumlah > 0 AND b.id_obat = '" . $id_obat . "' AND b.satuan = '" . $satuan . "' AND b.konversi = '" . $konversi . "' AND b.satuan_konversi = '" . $satuan_konversi . "'
		");
		$nominal_beli = 0;
		if ($data != null) {
			foreach ($data as $d) {
				$hna = $d->hpp / 1.1;
				$jumlah_harga_1 = $d->jumlah * $hna;
				$jumlah_harga_2 = $jumlah_harga_1;
				$diskon_1 = 0;
				$diskon_2 = 0;
				if ($d->t_diskon_detail == "persen") {
				 	$diskon_1 = $jumlah_harga_1 * $d->diskon_detail / 100;
				 	$jumlah_harga_2 -= $diskon_1;
				} else if ($d->t_diskon_detail == "nominal") {
				 	$diskon_1 = $d->diskon_detail;
				 	$jumlah_harga_2 -= $diskon_1;
				}
				if ($d->t_diskon_global == "persen") {
				 	$diskon_2 = $jumlah_harga_1 * $d->diskon_global / 100;
				 	$jumlah_harga_2 -= $diskon_2;
				} else if ($d->t_diskon_global == "nominal") {
				 	$total_row = $dbtable->get_row("
				 		SELECT SUM(jumlah * hna) AS 'total'
				 		FROM smis_fr_dobat_f_masuk
				 		WHERE id_obat_f_masuk = '" . $d->id_obat_masuk . "' AND prop NOT LIKE 'del'
				 	");
				 	$subtotal = $d->jumlah * $d->hpp;
				 	$diskon_2 = $d->diskon_global * $subtotal / $total_row->total;
				 	$jumlah_harga_2 -= $diskon_2;
				}
				$jumlah_harga_2 = round($jumlah_harga_2);
				$ppn = floor($jumlah_harga_2 * 0.1);
				$hutang = $jumlah_harga_2 + $ppn;
				$nominal_beli += $hutang;
			}
		}
		// Nominal Retur Vendor - Faktur:
		$data = $dbtable->get_result("
			SELECT e.id AS 'id_obat_masuk', d.id_obat, d.kode_obat, d.nama_obat, d.satuan, a.jumlah, d.hna AS 'hpp', e.diskon AS 'diskon_global', e.t_diskon AS 't_diskon_global', d.diskon AS 'diskon_detail', d.t_diskon AS 't_diskon_detail'
			FROM (((smis_fr_dretur_obat a LEFT JOIN smis_fr_retur_obat b ON a.id_retur_obat = b.id) LEFT JOIN smis_fr_stok_obat c ON a.id_stok_obat = c.id) LEFT JOIN smis_fr_dobat_f_masuk d ON c.id_dobat_masuk = d.id) LEFT JOIN smis_fr_obat_f_masuk e ON d.id_obat_f_masuk = e.id
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND b.tanggal >= '" . $tanggal_from . "' AND b.tanggal <= '" . $tanggal_to . "' AND a.jumlah > 0 AND d.id_obat = '" . $id_obat . "' AND d.satuan = '" . $satuan . "' AND d.konversi = '" . $konversi . "' AND d.satuan_konversi = '" . $satuan_konversi . "'
		");
		if ($data != null) {
			foreach ($data as $d) {
				$hna = $d->hpp / 1.1;
				$jumlah_harga_1 = $d->jumlah * $hna;
				$jumlah_harga_2 = $jumlah_harga_1;
				$diskon_1 = 0;
				$diskon_2 = 0;
				if ($d->t_diskon_detail == "persen") {
				 	$diskon_1 = $jumlah_harga_1 * $d->diskon_detail / 100;
				 	$jumlah_harga_2 -= $diskon_1;
				} else if ($d->t_diskon_detail == "nominal") {
				 	$diskon_1 = $d->diskon_detail;
				 	$jumlah_harga_2 -= $diskon_1;
				}
				if ($d->t_diskon_global == "persen") {
				 	$diskon_2 = $jumlah_harga_1 * $d->diskon_global / 100;
				 	$jumlah_harga_2 -= $diskon_2;
				} else if ($d->t_diskon_global == "nominal") {
				 	$total_row = $dbtable->get_row("
				 		SELECT SUM(jumlah * hna) AS 'total'
				 		FROM smis_fr_dobat_f_masuk
				 		WHERE id_obat_f_masuk = '" . $d->id_obat_masuk . "' AND prop NOT LIKE 'del'
				 	");
				 	$subtotal = $d->jumlah * $d->hpp;
				 	$diskon_2 = $d->diskon_global * $subtotal / $total_row->total;
				 	$jumlah_harga_2 -= $diskon_2;
				}
				$jumlah_harga_2 = round($jumlah_harga_2);
				$ppn = floor($jumlah_harga_2 * 0.1);
				$potongan = $jumlah_harga_2 + $ppn;
				$nominal_beli -= $potongan;
			}
		}
		// Nominal Retur Vendor - Non-Faktur:
		$data = $dbtable->get_result("
			SELECT d.id AS 'id_obat_masuk', c.id_obat, c.kode_obat, c.nama_obat, c.satuan, a.jumlah, c.hna AS 'hpp', d.diskon AS 'diskon_global', d.t_diskon AS 't_diskon_global', c.diskon AS 'diskon_detail', c.t_diskon AS 't_diskon_detail'
			FROM ((smis_fr_retur_obat_non_faktur a LEFT JOIN smis_fr_stok_obat b ON a.id_stok_obat = b.id) LEFT JOIN smis_fr_dobat_f_masuk c ON b.id_dobat_masuk = c.id) LEFT JOIN smis_fr_obat_f_masuk d ON c.id_obat_f_masuk = d.id
			WHERE a.prop NOT LIKE 'del' AND a.tanggal >= '" . $tanggal_from . "' AND a.tanggal <= '" . $tanggal_to . "' AND a.jumlah > 0 AND c.id_obat = '" . $id_obat . "' AND c.satuan = '" . $satuan . "' AND c.konversi = '" . $konversi . "' AND c.satuan_konversi = '" . $satuan_konversi . "'
		");
		if ($data != null) {
			foreach ($data as $d) {
				$hna = $d->hpp / 1.1;
				$jumlah_harga_1 = $d->jumlah * $hna;
				$jumlah_harga_2 = $jumlah_harga_1;
				$diskon_1 = 0;
				$diskon_2 = 0;
				if ($d->t_diskon_detail == "persen") {
				 	$diskon_1 = $jumlah_harga_1 * $d->diskon_detail / 100;
				 	$jumlah_harga_2 -= $diskon_1;
				} else if ($d->t_diskon_detail == "nominal") {
				 	$diskon_1 = $d->diskon_detail;
				 	$jumlah_harga_2 -= $diskon_1;
				}
				if ($d->t_diskon_global == "persen") {
				 	$diskon_2 = $jumlah_harga_1 * $d->diskon_global / 100;
				 	$jumlah_harga_2 -= $diskon_2;
				} else if ($d->t_diskon_global == "nominal") {
				 	$total_row = $dbtable->get_row("
				 		SELECT SUM(jumlah * hna) AS 'total'
				 		FROM smis_fr_dobat_f_masuk
				 		WHERE id_obat_f_masuk = '" . $d->id_obat_masuk . "' AND prop NOT LIKE 'del'
				 	");
				 	$subtotal = $d->jumlah * $d->hpp;
				 	$diskon_2 = $d->diskon_global * $subtotal / $total_row->total;
				 	$jumlah_harga_2 -= $diskon_2;
				}
				$jumlah_harga_2 = round($jumlah_harga_2);
				$ppn = floor($jumlah_harga_2 * 0.1);
				$potongan = $jumlah_harga_2 + $ppn;
				$nominal_beli -= $potongan;
			}
		}
		
		$data = array(
			"saldo_awal"	=> $saldo_awal,
			"saldo_akhir"	=> $saldo_akhir,
			"jumlah_beli"	=> $jumlah_beli,
			"nominal_beli"	=> $nominal_beli
		);
		echo json_encode($data);
	}
?>