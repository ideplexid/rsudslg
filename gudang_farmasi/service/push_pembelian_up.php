<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("gudang_farmasi/service_consumer/PushObatPembelianUPServiceConsumer.php");
	global $db;

	if (isset($_POST['command'])) {
		$obat_masuk_dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
		$dobat_masuk_dbtable = new DBTable($db, "smis_fr_dobat_f_masuk");
		$stok_obat_dbtable = new DBTable($db, "smis_fr_stok_obat");
		$obat_keluar_dbtable = new DBTable($db, "smis_fr_obat_keluar");
		$dobat_keluar_dbtable = new DBTable($db, "smis_fr_dobat_keluar");
		$stok_obat_keluar_dbtable = new DBTable($db, "smis_fr_stok_obat_keluar");
		$riwayat_stok_obat_dbtable = new DBTable($db, "smis_fr_riwayat_stok_obat");
		$kartu_stok_dbtable = new DBTable($db, "smis_fr_kartu_stok");

		$unit = $_POST['unit'];
		$username = $_POST['user'];
		$header_pembelian_up = $_POST['header'];
		$detail_pembelian_up = $_POST['detail'];

		if ($_POST['command'] == "insert") {
			$data = array();
			$data['id_po'] = 0;
			$data['no_opl'] = "";
			$data['no_bbm']	= "";
			$data['id_vendor'] = $header_pembelian_up['id_vendor'];
			$data['nama_vendor'] = $header_pembelian_up['nama_vendor'];
			$data['no_faktur'] = $header_pembelian_up['no_kwitansi'];
			$data['tanggal'] = date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d");
			$data['tanggal_datang'] = date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d");
			$data['tanggal_tempo'] = date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d");
			$data['diskon']	= 0;
			$data['t_diskon'] = "persen";
			$data['materai'] = 0;
			$data['tipe'] = "sito";
			$data['use_ppn'] = 1;
			$obat_masuk_dbtable->insert($data);
			$id_obat_masuk = $obat_masuk_dbtable->get_inserted_id();

			if ($detail_pembelian_up != null) {
				foreach ($detail_pembelian_up as $d) {
					$stok_row = $stok_obat_dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM smis_fr_stok_obat a INNER JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk =  b.id
						WHERE b.id_obat = '" . $d['id_obat'] . "' AND a.prop NOT LIKE 'del'
					");
					$stok_per_id_obat = 0;
					if ($stok_row != null)
						$stok_per_id_obat = $stok_row->sisa;

					$data = array();
					$data = array(
						"id_obat_f_masuk"	=> $id_obat_masuk,
						"id_dpo"			=> 0,
						"id_obat"			=> $d['id_obat'],
						"kode_obat"			=> $d['kode_obat'],
						"nama_obat"			=> $d['nama_obat'],
						"nama_jenis_obat"	=> $d['nama_jenis_obat'],
						"medis"				=> 1,
						"inventaris"		=> 0,
						"label"				=> "sito",
						"stok_entri"		=> $stok_per_id_obat,
						"jumlah_tercatat"	=> $d['jumlah'],
						"jumlah"			=> $d['jumlah'],
						"sisa"				=> 0,
						"satuan"			=> $d['satuan'],
						"konversi"			=> $d['konversi'],
						"satuan_konversi"	=> $d['satuan_konversi'],
						"hna"				=> $d['hna'],
						"selisih"			=> 0,
						"diskon"			=> 0,
						"t_diskon"			=> "persen",
						"produsen"			=> $d['produsen']
					);
					$dobat_masuk_dbtable->insert($data);
					$id_dobat_masuk = $dobat_masuk_dbtable->get_inserted_id();

					$data = array();
					$data = array(
						"id_dobat_masuk"	=> $id_dobat_masuk,
						"kode_obat"			=> $d['kode_obat'],
						"nama_obat"			=> $d['nama_obat'],
						"nama_jenis_obat"	=> $d['nama_jenis_obat'],
						"label"				=> "sito",
						"id_vendor"			=> $header_pembelian_up['id_vendor'],
						"nama_vendor"		=> $header_pembelian_up['nama_vendor'],
						"jumlah"			=> $d['jumlah'] * $d['konversi'],
						"sisa"				=> 0,
						"retur"				=> 0,
						"satuan"			=> $d['satuan_konversi'],
						"konversi"			=> 1,
						"satuan_konversi"	=> $d['satuan_konversi'],
						"hna"				=> $d['hna'] / $d['konversi'],
						"produsen"			=> $d['produsen'],
						"tanggal_exp"		=> $d['tanggal_exp'],
						"no_batch"			=> $d['no_batch'],
						"turunan"			=> 0
					);
					$stok_obat_dbtable->insert($data);
					$id_stok = $stok_obat_dbtable->get_inserted_id();

					$data = array();
					$data = array(
						"tanggal"			=> date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d"),
						"id_stok_obat"		=> $id_stok,
						"jumlah_masuk"		=> $d['jumlah'] * $d['konversi'],
						"sisa"				=> $d['jumlah'] * $d['konversi'],
						"keterangan"		=> "Pembelian UP " . ArrayAdapter::format("unslug", $unit) . " " . $header_pembelian_up['nama_vendor'],
						"nama_user"			=> $username
					);
					$riwayat_stok_obat_dbtable->insert($data);

					$data = array();
					$data = array(
						"f_id"				=> $id_obat_masuk,
						"no_bon"			=> $id_obat_masuk . " - " . $header_pembelian_up['no_kwitansi'],
						"unit"				=> $unit,
						"id_obat"			=> $d['id_obat'],
						"kode_obat"			=> $d['kode_obat'],
						"nama_obat"			=> $d['nama_obat'],
						"nama_jenis_obat"	=> $d['nama_jenis_obat'],
						"tanggal"			=> date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d"),
						"masuk"				=> $d['jumlah'] * $d['konversi'],
						"keluar"				=> 0,
						"sisa"				=> $stok_per_id_obat + $d['jumlah'] * $d['konversi']
					);
					$kartu_stok_dbtable->insert($data);		
				}
			}

			$data = array();
			$data['tanggal'] = date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d");
			$data['nomor']	= $header_pembelian_up['no_kwitansi'] . " - " . $id_obat_masuk;
			$data['unit'] = $unit;
			$data['status'] = "sudah";	
			$data['keterangan'] = "Mutasi Pembelian UP " . ArrayAdapter::format("unslug", $unit) . " " . $header_pembelian_up['nama_vendor'];
			$obat_keluar_dbtable->insert($data);
			$id_obat_keluar = $obat_keluar_dbtable->get_inserted_id();

			$detail_pembelian_up_rows = $dobat_masuk_dbtable->get_result("
				SELECT c.id AS 'id_stok_obat', b.id_obat, c.*
				FROM (smis_fr_obat_f_masuk a INNER JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk) INNER JOIN smis_fr_stok_obat c ON b.id = c.id_dobat_masuk
				WHERE a.id = '" . $id_obat_masuk . "'
			");
			if ($detail_pembelian_up_rows != null) {
				foreach ($detail_pembelian_up_rows as $d) {
					$stok_row = $stok_obat_dbtable->get_row("
						SELECT SUM(a.sisa) AS 'sisa'
						FROM smis_fr_stok_obat a INNER JOIN smis_fr_dobat_f_masuk b ON a.id_dobat_masuk =  b.id
						WHERE b.id_obat = '" . $d->id_obat . "' AND a.prop NOT LIKE 'del'
					");
					$stok_per_id_obat = 0;
					if ($stok_row != null)
						$stok_per_id_obat = $stok_row->sisa;

					$harga_ma_row = $stok_obat_dbtable->get_row("
						SELECT id_obat, satuan, konversi, satuan_konversi, COUNT(*) AS 'jumlah', AVG(hna) AS 'harga_ma'
						FROM (
							SELECT DISTINCT smis_fr_dobat_f_masuk.id_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, smis_fr_stok_obat.hna
							FROM smis_fr_stok_obat LEFT JOIN smis_fr_dobat_f_masuk ON smis_fr_stok_obat.id_dobat_masuk = smis_fr_dobat_f_masuk.id
							WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_dobat_f_masuk.id_obat = '" . $d->id_obat . "' AND smis_fr_stok_obat.satuan = '" . $d->satuan . "' AND smis_fr_stok_obat.konversi = '" . $d->konversi . "' AND smis_fr_stok_obat.satuan_konversi = '" . $d->satuan_konversi . "'
						) v_ma
						GROUP BY id_obat, satuan, konversi, satuan_konversi
					");
					$harga_ma = 0;
					if ($harga_ma_row != null)
						$harga_ma = $harga_ma_row->harga_ma;

					$data = array();
					$data = array(
						"id_obat_keluar"	=> $id_obat_keluar,
						"id_obat"			=> $d->id_obat,
						"kode_obat"			=> $d->kode_obat,
						"nama_obat"			=> $d->nama_obat,
						"nama_jenis_obat"	=> $d->nama_jenis_obat,
						"stok_entri"		=> $stok_per_id_obat,
						"harga_ma"			=> $harga_ma,
						"jumlah"			=> $d->jumlah * $d->konversi,
						"satuan"			=> $d->satuan_konversi,
						"konversi"			=> 1,
						"satuan_konversi"	=> $d->satuan_konversi
					);
					$dobat_keluar_dbtable->insert($data);
					$id_dobat_keluar = $dobat_keluar_dbtable->get_inserted_id();

					$data = array();
					$data = array(
						"id_dobat_keluar"	=> $id_dobat_keluar,
						"id_stok_obat"		=> $d->id_stok_obat,
						"jumlah"			=> $d->jumlah * $d->konversi
					);
					$stok_obat_keluar_dbtable->insert($data);

					$data = array();
					$data = array(
						"tanggal"			=> date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d"),
						"id_stok_obat"		=> $d->id_stok_obat,
						"jumlah_keluar"		=> $d->jumlah * $d->konversi,
						"sisa"				=> 0,
						"keterangan"		=> "Mutasi Pembelian UP " . ArrayAdapter::format("unslug", $unit) . " " . $header_pembelian_up['nama_vendor'],
						"nama_user"			=> $username
					);
					$riwayat_stok_obat_dbtable->insert($data);

					$data = array();
					$data = array(
						"f_id"				=> $id_obat_keluar,
						"no_bon"			=> $id_obat_keluar . " - " . $header_pembelian_up['no_kwitansi'],
						"unit"				=> $unit,
						"id_obat"			=> $d->id_obat,
						"kode_obat"			=> $d->kode_obat,
						"nama_obat"			=> $d->nama_obat,
						"nama_jenis_obat"	=> $d->nama_jenis_obat,
						"tanggal"			=> date_format(date_create($header_pembelian_up['tanggal']),"Y-m-d"),
						"masuk"				=> 0,
						"keluar"			=> $d->jumlah * $d->konversi,
						"sisa"				=> $stok_per_id_obat
					);
					$kartu_stok_dbtable->insert($data);
				}
			}

			// push mutasi obat by id obat keluar :
			$obat_keluar_row = $obat_keluar_dbtable->get_row("
				SELECT *
				FROM smis_fr_obat_keluar
				WHERE id = '" . $id_obat_keluar . "'
			");
			$detail = $stok_obat_keluar_dbtable->get_result("
				SELECT smis_fr_stok_obat_keluar.id AS 'id_stok_obat_keluar', smis_fr_dobat_keluar.id_obat, smis_fr_dobat_keluar.kode_obat, smis_fr_dobat_keluar.nama_obat, smis_fr_dobat_keluar.nama_jenis_obat, smis_fr_stok_obat.formularium, smis_fr_stok_obat.berlogo, smis_fr_stok_obat.generik, smis_fr_stok_obat.label, smis_fr_stok_obat.id_vendor, smis_fr_stok_obat.nama_vendor, smis_fr_stok_obat_keluar.jumlah, smis_fr_dobat_keluar.satuan, smis_fr_dobat_keluar.konversi, smis_fr_dobat_keluar.satuan_konversi, smis_fr_stok_obat.hna, smis_fr_stok_obat.produsen, smis_fr_stok_obat.tanggal_exp, smis_fr_stok_obat.no_batch, smis_fr_stok_obat_keluar.prop
				FROM (smis_fr_stok_obat_keluar LEFT JOIN smis_fr_stok_obat ON smis_fr_stok_obat_keluar.id_stok_obat = smis_fr_stok_obat.id) LEFT JOIN smis_fr_dobat_keluar ON smis_fr_stok_obat_keluar.id_dobat_keluar = smis_fr_dobat_keluar.id
				WHERE smis_fr_dobat_keluar.id_obat_keluar = '" . $id_obat_keluar . "'
			");
			$command = "push_save";
			$push_obat_service_consumer = new PushObatPembelianUPServiceConsumer($db, $obat_keluar_row->id, $obat_keluar_row->tanggal, $obat_keluar_row->unit, $obat_keluar_row->status, $obat_keluar_row->keterangan, $username, $detail, $command);
			$push_obat_service_consumer->execute();

			echo json_encode(array("data" => $id_obat_masuk));
		}
	}
?>