<?php
	global $db;
	
	$dbtable = new DBTable($db, "smis_fr_obat_f_masuk");
	$row = $dbtable->get_row("
		SELECT COUNT(*) AS 'jumlah'
		FROM (
			SELECT DISTINCT b.nama_obat, b.nama_jenis_obat
			FROM smis_fr_obat_f_masuk a LEFT JOIN smis_fr_dobat_f_masuk b ON a.id = b.id_obat_f_masuk
			WHERE a.prop NOT LIKE 'del' AND b.prop NOT LIKE 'del' AND a.id > 0 AND b.jumlah > 0
			ORDER BY b.nama_obat, b.nama_jenis_obat ASC
		) v
	");
	$data = array();
	$data['jumlah'] = $row->jumlah;
	echo json_encode($data);
?>