<?php 
	global $db;
	
	$response_package = new ResponsePackage();
	if (isset($_POST['command'])) {
		if ($_POST['command'] == "push_update") {
			$id['id'] = $_POST['id'];
			$data['status'] = $_POST['status'];
			$data['keterangan'] = $_POST['keterangan'];
			$data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
	        $data['duplicate'] = 0;
	        $data['time_updated'] = date("Y-m-d H:i:s");
			$obat_keluar_dbtable = new DBTable($db, "smis_fr_obat_keluar");
			$obat_keluar_dbtable->update($data, $id);
			$response_package->setStatus(ResponsePackage::$STATUS_OK);
			$row = $obat_keluar_dbtable->get_row("
				SELECT *
				FROM smis_fr_obat_keluar
				WHERE id = '" . $id['id'] . "'
			");
			if ($data['status'] == "dikembalikan") {
				// restock:
				$dobat_keluar_rows = $obat_keluar_dbtable->get_result("
					SELECT *
					FROM smis_fr_dobat_keluar
					WHERE id_obat_keluar = '" . $id['id'] . "' AND prop NOT LIKE 'del'
				");
				foreach ($dobat_keluar_rows as $dokr) {
					$stok_obat_keluar_dbtable = new DBTable($db, "smis_fr_stok_obat_keluar");
					$stok_keluar_query = "
						SELECT *
						FROM smis_fr_stok_obat_keluar
						WHERE id_dobat_keluar = '" . $dokr->id . "'
						ORDER BY id DESC
					";
					$stok_keluar = $stok_obat_keluar_dbtable->get_result($stok_keluar_query);
					foreach($stok_keluar as $sk) {
						$stok_keluar_data = array();
						$stok_keluar_data['jumlah'] = 0;
						$stok_keluar_data['prop'] = "del";
						$stok_keluar_data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
				        $stok_keluar_data['duplicate'] = 0;
				        $stok_keluar_data['time_updated'] = date("Y-m-d H:i:s");
						$stok_keluar_id['id'] = $sk->id;
						$stok_obat_keluar_dbtable->update($stok_keluar_data, $stok_keluar_id);
						$stok_obat_dbtable = new DBTable($db, "smis_fr_stok_obat");
						$stok_query = "
							SELECT *
							FROM smis_fr_stok_obat
							WHERE id = '" . $sk->id_stok_obat . "'
						";
						$stok_obat_row = $stok_obat_dbtable->get_row($stok_query);
						$stok_data = array();
						$stok_data['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
						$stok_data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
				        $stok_data['duplicate'] = 0;
				        $stok_data['time_updated'] = date("Y-m-d H:i:s");
						$stok_id['id'] = $sk->id_stok_obat;
						$stok_dbtable = new DBTable($db, "smis_fr_stok_obat");
						$stok_dbtable->update($stok_data, $stok_id);
						//logging riwayat stok obat:
						$riwayat_dbtable = new DBTable($db, "smis_fr_riwayat_stok_obat");
						$data_riwayat = array();
						$data_riwayat['tanggal'] = date("Y-m-d");
						$data_riwayat['id_stok_obat'] = $sk->id_stok_obat;
						$data_riwayat['jumlah_masuk'] = $sk->jumlah;
						$data_riwayat['sisa'] = $stok_obat_row->sisa + $sk->jumlah;
						$data_riwayat['keterangan'] = "Pembatalan Stok Keluar ke " . ArrayAdapter::format("unslug", $row->unit);
						global $user;
						$data_riwayat['nama_user'] = $_POST['nama_user'];
						$data_riwayat['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
				        $data_riwayat['duplicate'] = 0;
				        $data_riwayat['time_updated'] = date("Y-m-d H:i:s");
				        $data_riwayat['origin_updated'] = getSettings($db, "smis_autonomous_id", "");
						$riwayat_dbtable->insert($data_riwayat);
					}
					//kartu gudang induk:
					$ks_data = array();
					$ks_data['f_id'] = $id['id'];
					$ks_data['unit'] = "Pengembalian Mutasi " . ArrayAdapter::format("unslug", $row->unit);
					$ks_data['no_bon'] = $id['id'];
					$ks_data['id_obat'] = $dokr->id_obat;
					$ks_data['kode_obat'] = $dokr->kode_obat;
					$ks_data['nama_obat'] = $dokr->nama_obat;
					$ks_data['nama_jenis_obat'] = $dokr->nama_jenis_obat;
					$ks_data['tanggal'] = date("Y-m-d");
					$ks_data['masuk'] = $dokr->jumlah;
					$ks_data['keluar'] = 0;
					$ks_dbtable = new DBTable($db, "smis_fr_kartu_stok");
					$ks_row = $ks_dbtable->get_row("
						SELECT SUM(b.sisa) AS 'jumlah'
						FROM smis_fr_dobat_f_masuk a LEFT JOIN smis_fr_stok_obat b ON a.id = b.id_dobat_masuk
						WHERE a.prop NOT LIKE 'del' AND a.id_obat = '" . $dokr->id_obat . "'
					");
					$ks_data['sisa'] = $ks_row->jumlah + $dokr->jumlah;
					$ks_data['autonomous'] = "[".getSettings($db, "smis_autonomous_id", "")."]";
			        $ks_data['duplicate'] = 0;
			        $ks_data['time_updated'] = date("Y-m-d H:i:s");
			        $ks_data['origin_updated'] = getSettings($db, "smis_autonomous_id", "");
					$ks_dbtable->insert($ks_data);
				}
			}
			//notify:
			global $notification;
			$key=md5($row->unit." ".$row->id);
			$status = ArrayAdapter::format("unslug", $_POST['status']);
			if ($status == "SUDAH")
				$status .= " DITERIMA";
			$msg="Obat Keluar ke <strong>".ArrayAdapter::format("unslug", $row->unit)."</strong> sudah dikonfirmasi (<strong>" . $status . "</strong>)";
			$notification->addNotification("Konfirmasi Obat Keluar", $key, $msg, "gudang_farmasi","obat_keluar");
		} else {
			$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
		}
	} else {
		$response_package->setStatus(ResponsePackage::$STATUS_FAIL);
	}
	echo json_encode($response_package->getPackage());	
?>