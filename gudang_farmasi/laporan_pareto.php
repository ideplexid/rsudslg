<?php
global $db;
$uitabel = new Table(array("No.","Nama Obat","Total Omzet","Jumlah Penjualan","Sisa Stok","Nilai Sisa Stok","% Nilai","Kumulatif","Pareto" ));
$uitabel ->setName("laporan_pareto");

if(isset($_POST['command']) && $_POST['command']!=""){
    $response  = new ResponsePackage();
    $response ->setStatus(ResponsePackage::$STATUS_OK);

    $bulan_tahun_dari = $_POST['bulan_tahun_dari'];
    $eps = explode("-",$bulan_tahun_dari);
    $bulan_dari  =  $eps[0]*1;
    $tahun_dari  =  $eps[1];

    $bulan_tahun_sampai = $_POST['bulan_tahun_sampai'];
    $eps = explode("-",$bulan_tahun_sampai);
    $bulan_sampai  =  $eps[0]*1;
    $tahun_sampai  =  $eps[1];


    $unit   = $_POST['unit'];
    $depo = array(
        "depo_farmasi_irja_irna"=>"",
        "depo_farmasi_igd"=>"2",
        "depo_farmasi_ok"=>"3",
        "depo_farmasi_icu"=>"4",
        "depo_farmasi_akhp"=>"5"
    );
    $result = array();
    if($_POST['command']=="ambil_daftar"){
        $query = "SELECT id,nama FROM smis_pr_barang WHERE prop!='del' ";
        $result = $db->get_result($query);
        $response ->setContent($result);
    }else if($_POST['command']=="ambil_satu"){
        $id_obat            = $_POST['id_obat']; 
        $total_omset        = 0;
        $jumlah_penjualan   = 0;
        $sisa_stok          = 0;
        $nilai_sisa_stok    = 0;

        //data untuk 4 depo
        foreach($depo as $dp=>$notbl){
            if($unit == "" || $unit==$dp){
                $tbl_penjualan_resep = "smis_dfm".$notbl."_penjualan_resep";
                $tbl_penjualan_obat_jadi = "smis_dfm".$notbl."_penjualan_obat_jadi";
                $tbl_penjualan_obat_racikan = "smis_dfm".$notbl."_penjualan_obat_racikan";
                $tbl_penjualan_bahan_racikan = "smis_dfm".$notbl."_bahan_pakai_obat_racikan";
                $tbl_obat_masuk = "smis_dfm".$notbl."_obat_masuk";
                $tbl_stok_obat = "smis_dfm".$notbl."_stok_obat";
                
                //ambil dari get_penjualan.php
                $query_omset_jumlah = " 
                    SELECT SUM(X.jumlah) as jumlah, SUM(X.total_penjualan) as total_penjualan FROM (
                        SELECT b.id , b.jumlah, b.subtotal-b.embalase-b.tusla as total_penjualan
                        FROM $tbl_penjualan_resep a INNER JOIN $tbl_penjualan_obat_jadi b ON a.id = b.id_penjualan_resep
                        WHERE a.prop = '' AND a.dibatalkan = 0 AND b.prop = '' AND 
                                YEAR(a.tanggal) >= '" . $tahun_dari . "' AND MONTH(a.tanggal) >= '" . $bulan_dari . "'  AND
                                YEAR(a.tanggal) <= '" . $tahun_sampai . "' AND MONTH(a.tanggal) <= '" . $bulan_sampai . "' 
                                AND b.id_obat = $id_obat
                        UNION ALL
                        SELECT c.id, c.jumlah, c.jumlah*c.harga as total_penjualan
                        FROM ($tbl_penjualan_resep a INNER JOIN $tbl_penjualan_obat_racikan b ON a.id = b.id_penjualan_resep) INNER JOIN $tbl_penjualan_bahan_racikan c ON b.id = c.id_penjualan_obat_racikan
                        WHERE a.prop = '' AND a.dibatalkan = 0 AND b.prop = '' AND c.prop = '' AND 
                        YEAR(a.tanggal) >= '" . $tahun_dari . "' AND MONTH(a.tanggal) >= '" . $bulan_dari . "' AND 
                        YEAR(a.tanggal) <= '" . $tahun_sampai . "' AND MONTH(a.tanggal) <= '" . $bulan_sampai . "'  
                        AND c.id_obat = $id_obat
                    )X
                ";
                $hasil = $db->get_row($query_omset_jumlah);
                $total_omset += $hasil->total_penjualan;
                $jumlah_penjualan += $hasil->jumlah;

                //mengambil return
                $tbl_stok = "smis_dfm".$notbl."_stok_obat";
                $tbl_retur = "smis_dfm".$notbl."_retur_penjualan_resep";
                $tbl_retur_detail = "smis_dfm".$notbl."_dretur_penjualan_resep";

                $query_retur = " 
                    SELECT SUM(X.jumlah) as jumlah, SUM(X.total_penjualan) as total_penjualan FROM (
                        SELECT b.id , b.jumlah, b.subtotal*a.persentase_retur/100 as total_penjualan
                        FROM $tbl_retur a INNER JOIN $tbl_retur_detail b ON a.id = b.id_retur_penjualan_resep
                        INNER JOIN $tbl_stok c ON b.id_stok_obat = c.id
                        WHERE a.prop = '' AND a.dibatalkan = 0 AND b.prop = '' 
                        AND YEAR(a.tanggal) >= '" . $tahun_dari . "' AND MONTH(a.tanggal) >= '" . $bulan_dari . "' 
                        AND YEAR(a.tanggal) <= '" . $tahun_sampai . "' AND MONTH(a.tanggal) >= '" . $bulan_sampai . "' 
                        AND c.id_obat = $id_obat 
                    )X 
                ";

                $hasil = $db->get_row($query_retur);
                $total_omset -= $hasil->total_penjualan;
                $jumlah_penjualan -= $hasil->jumlah;


                //mengambil sisa stok
                $query_sisa = "  SELECT SUM(sisa) as sisa
                            FROM $tbl_obat_masuk a LEFT JOIN $tbl_stok_obat b ON 
                            a.id = b.id_obat_masuk WHERE b.id_obat = $id_obat 
                            AND a.status = 'sudah' 
                            AND a.prop='' AND b.prop=''
                        ";
                $hasil_stok = $db->get_var($query_sisa);
                if($hasil_stok!=NULL){
                    $sisa_stok += $hasil_stok;
                }

                //mengambil nilai sisa stok
                if($hasil_stok!=NULL){
                    $query_nilai_stok = "SELECT hna FROM  $tbl_stok_obat a WHERE id_obat = $id_obat ORDER BY id DESC LIMIT 0,1";
                    $hasil_nilai = $db->get_var($query_nilai_stok);
                    if($hasil_nilai!=NULL){
                        $nilai_sisa_stok += ($hasil_nilai*$hasil_stok);
                    }    
                }

            }    
        }

        //stok gudang
        $tbl_stok_gudang = "smis_fr_stok_obat";
        $detail_obat = "smis_fr_dobat_f_masuk";

         //mengambil sisa stok
         $query_sisa_gudang = " SELECT SUM(a.sisa) as sisa
                                FROM $tbl_stok_gudang a RIGHT JOIN $detail_obat b ON 
                                a.id_dobat_masuk = b.id WHERE b.id_obat = $id_obat                                 
                                AND a.prop='' AND b.prop='' 
                                ";
        $hasil_stok = $db->get_var($query_sisa_gudang);
        if($hasil_stok!=NULL){
            $sisa_stok += $hasil_stok;
        }

        //mengambil nilai sisa stok
        if($hasil_stok!=NULL){
            $query_nilai_stok = "SELECT hna FROM  $detail_obat a WHERE id_obat = $id_obat ORDER BY id DESC LIMIT 0,1";
            $hasil_nilai = $db->get_var($query_nilai_stok);
            if($hasil_nilai!=NULL){
                $nilai_sisa_stok += ($hasil_nilai*$hasil_stok);
            }    
        }

        if($total_omset<0){
            $total_omset = 0;
        }
        if($jumlah_penjualan<0){
            $jumlah_penjualan=0;
        }

        $result                     = array();
        $result['total_omset']      = $total_omset;
        $result['jumlah_penjualan'] = $jumlah_penjualan;
        $result['sisa_stok']        = $sisa_stok;
        $result['nilai_sisa_stok']  = $nilai_sisa_stok;

       //$result['nilai_sisa_stok_format']  = ArrayAdapter::format("money Rp.",$nilai_sisa_stok);
        //$result['total_omset_format'] =  ArrayAdapter::format("money Rp.",$total_omset);

        $response ->setContent($result);
    }else if($_POST['command']=="download_excel"){
        require_once("smis-libs-out/php-excel/PHPExcel.php");
        $border = array();
        $border['borders'] = array();
        $border['borders']['allborders'] = array();
        $border['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

        $urutan = json_decode($_POST['urutan'],true);
        $mentahan = json_decode($_POST['mentahan'],true);
        $pareto = json_decode($_POST['pareto'],true);
        
       // file_put_contents("smis-temp/mentahan.json",$_POST['mentahan']);
       
        $listbulan = array(
            1=>"Januari",
            2=>"Februari",
            3=>"Maret",
            4=>"April",
            5=>"Mei",
            6=>"Juni",
            7=>"Juli",
            8=>"Agustus",
            9=>"September",
            10=>"Oktober",
            11=>"Nopember",
            12=>"Desember"
        );

        $file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
        $sheet  ->setTitle ("LAPORAN PARETO");

        $sheet->mergeCells("A1:I1")->setCellValue ( "A1", strtoupper("LAPORAN PARETO BULAN ".($listbulan [ $bulan ])." ".$tahun) );
        $sheet->setCellValue("A3","No.");
        $sheet->setCellValue("B3","Nama Obat");
        $sheet->setCellValue("C3","Total Omset");
        $sheet->setCellValue("D3","Jumlah Penjualan");
        $sheet->setCellValue("E3","Sisa Stok");
        $sheet->setCellValue("F3","Nilai Sisa Stok");
        $sheet->setCellValue("G3","% Nilai");
        $sheet->setCellValue("H3","Komulatif");
        $sheet->setCellValue("I3","Pareto");
        $sheet->getStyle ( "A1:I3")->getFont ()->setBold ( true );

        $row = 3;
        $start = 0;
        foreach($urutan as $index=>$key){
            $start ++;
            $row++;
            $current = $mentahan[$key];
            $sheet->setCellValue("A".$row,$start.". ");
            $sheet->setCellValue("B".$row, $current['nama']);
            $sheet->setCellValue("C".$row,$current['total_omset']);
            $sheet->setCellValue("D".$row,$current['jumlah_penjualan']);
            $sheet->setCellValue("E".$row,$current['sisa_stok']);
            $sheet->setCellValue("F".$row,$current['nilai_sisa_stok']);
            $sheet->setCellValue("G".$row," ".substr($current['persen_nilai'],0,5)."% ");
            $sheet->setCellValue("H".$row," ".substr($current['komulatif'],0,5)."% ");
            $sheet->setCellValue("I".$row,$current['pareto']);
        }

        $sheet->getStyle('C4:C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('F4:F'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle ( 'A3:I'.$row )->applyFromArray ( $border );

        $row=$row+4;
        $total  = $pareto["A"]+$pareto["B"]+$pareto["C"]+$pareto["P"];
        $sheet->setCellValue("A".$row,"Nilai Stok");
        $sheet->setCellValue("B".$row,"Nilai");
        $sheet->setCellValue("C".$row,"Persen");
        
        $sheet->setCellValue("A".($row+1),"NILAI STOK A");
        $sheet->setCellValue("B".($row+1),$pareto["A"]);
        $sheet->setCellValue("C".($row+1),round($pareto["A"]*100/$total,2)."% " );
        $sheet->getStyle('B'.($row+1))->getNumberFormat()->setFormatCode('#,##0.00');

        $sheet->setCellValue("A".($row+2),"NILAI STOK B");
        $sheet->setCellValue("B".($row+2),$pareto["B"]);
        $sheet->setCellValue("C".($row+2),round($pareto["B"]*100/$total,2)."% " );
        $sheet->getStyle('B'.($row+2))->getNumberFormat()->setFormatCode('#,##0.00');

        $sheet->setCellValue("A".($row+3),"NILAI STOK C");
        $sheet->setCellValue("B".($row+3),$pareto["C"]);
        $sheet->setCellValue("C".($row+3),round($pareto["C"]*100/$total,2)."% " );
        $sheet->getStyle('B'.($row+3))->getNumberFormat()->setFormatCode('#,##0.00');

        $sheet->setCellValue("A".($row+4),"NILAI STOK P");
        $sheet->setCellValue("B".($row+4),$pareto["P"]);
        $sheet->setCellValue("C".($row+4),round($pareto["P"]*100/$total,2)."% " );
        $sheet->getStyle('B'.($row+4))->getNumberFormat()->setFormatCode('#,##0.00');

        $sheet->setCellValue("A".($row+5),"TOTAL NILAI STOK");
        $sheet->setCellValue("B".($row+5),$total);
        $sheet->setCellValue("C".($row+5),"100% " );
        $sheet->getStyle('B'.($row+5))->getNumberFormat()->setFormatCode('#,##0.00');

        $sheet->getStyle ( 'A'.$row.':C'.($row+5) )->applyFromArray ( $border );
        $sheet->getStyle ( "A$row:C$row")->getFont ()->setBold ( true );

        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getColumnDimension("D")->setAutoSize(true);
        $sheet->getColumnDimension("E")->setAutoSize(true);
        $sheet->getColumnDimension("F")->setAutoSize(true);
        $sheet->getColumnDimension("G")->setAutoSize(true);
        $sheet->getColumnDimension("H")->setAutoSize(true);
        $sheet->getColumnDimension("I")->setAutoSize(true);

        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Pareto '.($listbulan [$bulan*1]).' '.$tahun.'.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );  
        return;
        
    }

    echo json_encode($response ->getPackage());
    return;
}

$bulan = new OptionBuilder();
$bulan ->add("Januari","1");
$bulan ->add("Februari","2");
$bulan ->add("Maret","3");
$bulan ->add("April","4");
$bulan ->add("Mei","5");
$bulan ->add("Juni","6");
$bulan ->add("Juli","7");
$bulan ->add("Agustus","8");
$bulan ->add("September","9");
$bulan ->add("Oktober","10");
$bulan ->add("Nopember","11");
$bulan ->add("Desember","12");

$unit = new OptionBuilder();
$unit ->add("SEMUA DEPO","","1");
$unit ->add("Depo Farmasi AKHP","depo_farmasi_akhp");
$unit ->add("Depo Farmasi ICU","depo_farmasi_icu");
$unit ->add("Depo Farmasi IGD","depo_farmasi_igd");
$unit ->add("Depo Farmasi IRJA IRNA","depo_farmasi_irja_irna");
$unit ->add("Depo Farmasi OK","depo_farmasi_ok");



$uitabel ->addModal("unit","select","Unit",$unit->getContent());
$uitabel ->addModal("bulan_tahun_dari","text","Dari Bulan","");
$uitabel ->addModal("bulan_tahun_sampai","text","Ke Bulan","");


$form = $uitabel ->getModal()->getForm()->setTitle("Laporan Pareto");
$button = new Button("","","Proses");
$button ->setClass("btn-primary");
$button ->setIsButton(Button::$ICONIC_TEXT);
$button ->setAction("laporan_pareto.proses()");
$form ->addElement("",$button);


$excel = new Button("","","Download");
$excel ->setClass("btn-primary");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("laporan_pareto.download_excel()");
$form ->addElement("",$excel);

$uitabel ->setAction(false);
$uitabel ->setFooterVisible(false);


$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("laporan_pareto.reload()");
$person=new LoadingBar("pareto_bar", "");
$modal=new Modal("pareto_modal", "", "Process in Progress");
$modal	->addHTML($person->getHtml(),"after")
        ->addFooter($close);
        
echo $form->getHtml();
echo $uitabel->getHtml();
echo $modal->getHtml();
echo addJS ( "base-js/smis-base-loading.js");
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS("gudang_farmasi/js/laporan_pareto.js",false);


$uitabel = new Table(array("NILAI STOK","NILAI","PERSEN") );
$uitabel ->setName("resume_pareto");
$uitabel ->setAction(false);
$uitabel ->setFooterVisible(false);
echo $uitabel ->getHtml();

?>

<style type="text/css" >
    #pareto_modal .close{
        display:none;
    }
</style>