<?php
	require_once("smis-base/smis-include-service-consumer.php");
	require_once("gudang_farmasi/table/ObatKeluarTable.php");
	require_once("gudang_farmasi/adapter/ObatKeluarAdapter.php");
	require_once("gudang_farmasi/responder/ObatKeluarDBResponder.php");
	require_once("gudang_farmasi/service_consumer/PushObatServiceConsumer.php");
	require_once("gudang_farmasi/responder/ObatDBResponder.php");
	require_once("gudang_farmasi/responder/SisaDBResponder.php");
	require_once("gudang_farmasi/responder/MADBResponder.php");
	require_once("gudang_farmasi/service_consumer/UnitServiceConsumer.php");
	require_once("gudang_farmasi/table/DObatKeluarTable.php");
	require_once("gudang_farmasi/adapter/ObatAdapter.php");
	
	$obat_keluar_table = new ObatKeluarTable(
		array("Nomor", "Tanggal Keluar", "Unit", "Status"),
		"Gudang Farmasi : Obat Keluar - Unit",
		null,
		true
	);
	$obat_keluar_table->setName("obat_keluar");
	
	if (isset($_POST['super_command']) && $_POST['super_command'] == "obat_keluar") {
		if (isset($_POST['command'])) {
			if ($_POST['command'] == "get_auto_number_and_date") {
				$data = array(
					"nomor"		=> "MO-" . ArrayAdapter::format("only-digit8", (getSettings($db, "no_mutasi", 0) + 1)),
					"tanggal"	=> date("Y-m-d")
				);
				echo json_encode($data);
				return;
			} else if ($_POST['command'] == "export_au58") {
				require_once("smis-libs-out/php-excel/PHPExcel.php");
				$id = $_POST['id'];
				
				$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/template_bukti_mutasi.xlsx");
				$objPHPExcel->setActiveSheetIndexByName("BUKTI MUTASI");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				$logo_path = getSettings($db, "smis_autonomous_logo", "");
				if ($logo_path != "") {
					for ($i = 1, $j = 2; $i <= 25; $i++, $j += 29) {
						$logo = new PHPExcel_Worksheet_Drawing();
						$logo->setPath("smis-upload/" . $logo_path);
						$logo->setCoordinates("A" . $j);
						$logo->setHeight(60);
						$logo->setWorksheet($objWorksheet);
					}	
				}
				$nama_instansi = getSettings($db, "smis_autonomous_title", "SIMRS");
				$alamat_instansi = getSettings($db, "smis_autonomous_address");
				if ($nama_instansi == "")
					$objWorksheet->setCellValue("B2", "SIMRS");
				else
					$objWorksheet->setCellValue("B2", $nama_instansi);
				if ($alamat_instansi == "")
					$objWorksheet->setCellValue("B3", "ALAMAT SIMRS");
				else
					$objWorksheet->setCellValue("B3", $alamat_instansi);
				
				$dbtable = new DBTable($db, "smis_fr_obat_keluar");
				$header_info = $dbtable->get_row("
					SELECT *
					FROM smis_fr_obat_keluar
					WHERE id = '" . $id . "'
				");
				$ruangan_info = $dbtable->get_row("
					SELECT *
					FROM smis_fr_kode_mutasi_ruangan
					WHERE slug_ruangan = '" . $header_info->unit . "'
					LIMIT 0, 1
				");
				$kode_ruang = $ruangan_info != null ? $ruangan_info->kode : "-";
				$objWorksheet->setCellValue("H4", ArrayAdapter::format("unslug", $header_info->unit));
				$objWorksheet->setCellValue("L2", ": " . $header_info->nomor);
				$objWorksheet->setCellValue("L3", ": " . ArrayAdapter::format("date d-m-Y", $header_info->tanggal));
				$objWorksheet->setCellValue("L4", ": " . $kode_ruang);
				$detail_info = $dbtable->get_result("
					SELECT *
					FROM smis_fr_dobat_keluar
					WHERE id_obat_keluar = '" . $id . "'
				");
				$jumlah_item = count($detail_info);
				$jumlah_item_per_halaman = 15;
				$jumlah_halaman = ceil($jumlah_item / $jumlah_item_per_halaman);
				$start_row_index = 1;
				$end_row_index = 29;
				$value_index_incr = 29;

				$cur_item_index = 0;
				$print_area_str = "";
				for ($cur_page = 1; $cur_page <= $jumlah_halaman; $cur_page++) {
					$cur_row_index = ($start_row_index + 8) + ($cur_page - 1) * $value_index_incr;
					$start_print_area = $start_row_index + $value_index_incr * ($cur_page - 1);
					$end_print_area = $end_row_index + $value_index_incr * ($cur_page - 1);
					$print_area_str .= "A" . $start_print_area . ":L" . $end_print_area . ",";
					for ($cur_item_num = 1; $cur_item_num <= $jumlah_item_per_halaman && $cur_item_index < $jumlah_item; $cur_item_num++) {
						$kode_obat = $detail_info[$cur_item_index]->kode_obat;
						$objWorksheet->setCellValue("A" . $cur_row_index, $kode_obat);
						$objWorksheet->setCellValue("B" . $cur_row_index, $detail_info[$cur_item_index]->nama_obat);
						$objWorksheet->setCellValue("D" . $cur_row_index, $detail_info[$cur_item_index]->satuan);
						$objWorksheet->setCellValue("E" . $cur_row_index, $detail_info[$cur_item_index]->stok_entri);
						$objWorksheet->setCellValue("F" . $cur_row_index, $detail_info[$cur_item_index]->jumlah_diminta);
						$objWorksheet->setCellValue("G" . $cur_row_index, $detail_info[$cur_item_index]->jumlah);
						$objWorksheet->setCellValue("H" . $cur_row_index, $detail_info[$cur_item_index]->jumlah);
						$objWorksheet->setCellValue("I" . $cur_row_index, $detail_info[$cur_item_index]->harga_ma);
						$objWorksheet->getStyle("I" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
						$objWorksheet->setCellValue("K" . $cur_row_index, $kode_ruang);
						$cur_item_index++;
						$cur_row_index++;
					}
				}
				$objWorksheet->getPageSetup()->setPrintArea(rtrim($print_area_str, ","));
				
				header("Content-type: application/vnd.ms-excel");	
				header("Content-Disposition: attachment; filename=BUKTI_MUTASI-" . ArrayAdapter::format("unslug", $header_info->unit) . "_" . ArrayAdapter::format("only-digit6", $header_info->id) . "_" . ArrayAdapter::format("date Ymd", $header_info->tanggal) . ".xlsx");
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				$objWriter->save("php://output");
				return;
			}
			if ($_POST['command'] == "export_au58_nomor") {
				require_once("smis-libs-out/php-excel/PHPExcel.php");
				$nomor = $_POST['nomor'];
				
				$objPHPExcel = PHPExcel_IOFactory::load("gudang_farmasi/templates/template_bukti_mutasi.xlsx");
				$objPHPExcel->setActiveSheetIndexByName("BUKTI MUTASI");
				$objWorksheet = $objPHPExcel->getActiveSheet();
				$logo_path = getSettings($db, "smis_autonomous_logo", "");
				if ($logo_path != "") {
					for ($i = 1, $j = 2; $i <= 25; $i++, $j += 29) {
						$logo = new PHPExcel_Worksheet_Drawing();
						$logo->setPath("smis-upload/" . $logo_path);
						$logo->setCoordinates("A" . $j);
						$logo->setHeight(60);
						$logo->setWorksheet($objWorksheet);
					}	
				}
				$nama_instansi = getSettings($db, "smis_autonomous_title", "SIMRS");
				$alamat_instansi = getSettings($db, "smis_autonomous_address");
				if ($nama_instansi == "")
					$objWorksheet->setCellValue("B2", "SIMRS");
				else
					$objWorksheet->setCellValue("B2", $nama_instansi);
				if ($alamat_instansi == "")
					$objWorksheet->setCellValue("B3", "ALAMAT SIMRS");
				else
					$objWorksheet->setCellValue("B3", $alamat_instansi);
				
				$dbtable = new DBTable($db, "smis_fr_obat_keluar");
				$header_info = $dbtable->get_row("
					SELECT *
					FROM smis_fr_obat_keluar
					WHERE nomor = '" . $nomor . "' AND prop NOT LIKE 'del'
					LIMIT 0, 1
				");
				$ruangan_info = $dbtable->get_row("
					SELECT *
					FROM smis_fr_kode_mutasi_ruangan
					WHERE slug_ruangan = '" . $header_info->unit . "'
					LIMIT 0, 1
				");
				$kode_ruang = $ruangan_info != null ? $ruangan_info->kode : "-";
				$objWorksheet->setCellValue("H4", ArrayAdapter::format("unslug", $header_info->unit));
				$objWorksheet->setCellValue("L2", ": " . $header_info->nomor);
				$objWorksheet->setCellValue("L3", ": " . ArrayAdapter::format("date d-m-Y", $header_info->tanggal));
				$objWorksheet->setCellValue("L4", ": " . $kode_ruang);

				$rows = $dbtable->get_result("
					SELECT *
					FROM smis_fr_obat_keluar
					WHERE nomor = '" . $nomor . "' AND unit = '" . $header_info->unit . "' AND prop NOT LIKE 'del'
				");
				
				$detail_info = array();
				$di_index = 0;
				foreach ($rows as $row) {
					$di_arr = $dbtable->get_result("
						SELECT *
						FROM smis_fr_dobat_keluar
						WHERE id_obat_keluar = '" . $row->id . "'
					");
					foreach ($di_arr as $di)
						$detail_info[$di_index++] = $di;
				}
				$jumlah_item = count($detail_info);
				$jumlah_item_per_halaman = 15;
				$jumlah_halaman = ceil($jumlah_item / $jumlah_item_per_halaman);
				$start_row_index = 1;
				$end_row_index = 29;
				$value_index_incr = 29;

				$cur_item_index = 0;
				$print_area_str = "";
				for ($cur_page = 1; $cur_page <= $jumlah_halaman; $cur_page++) {
					$cur_row_index = ($start_row_index + 8) + ($cur_page - 1) * $value_index_incr;
					$start_print_area = $start_row_index + $value_index_incr * ($cur_page - 1);
					$end_print_area = $end_row_index + $value_index_incr * ($cur_page - 1);
					$print_area_str .= "A" . $start_print_area . ":L" . $end_print_area . ",";
					for ($cur_item_num = 1; $cur_item_num <= $jumlah_item_per_halaman && $cur_item_index < $jumlah_item; $cur_item_num++) {
						$kode_obat = $detail_info[$cur_item_index];
						$objWorksheet->setCellValue("A" . $cur_row_index, $kode_obat);
						$objWorksheet->setCellValue("B" . $cur_row_index, $detail_info[$cur_item_index]->nama_obat);
						$objWorksheet->setCellValue("D" . $cur_row_index, $detail_info[$cur_item_index]->satuan);
						$objWorksheet->setCellValue("E" . $cur_row_index, $detail_info[$cur_item_index]->stok_entri);
						$objWorksheet->setCellValue("F" . $cur_row_index, $detail_info[$cur_item_index]->jumlah_diminta);
						$objWorksheet->setCellValue("I" . $cur_row_index, $detail_info[$cur_item_index]->harga_ma);
						$objWorksheet->getStyle("I" . $cur_row_index)->getNumberFormat()->setFormatCode("#,##0.00");
						$objWorksheet->setCellValue("K" . $cur_row_index, $kode_ruang);
						$cur_item_index++;
						$cur_row_index++;
					}
				}
				$objWorksheet->getPageSetup()->setPrintArea(rtrim($print_area_str, ","));
				
				header("Content-type: application/vnd.ms-excel");	
				header("Content-Disposition: attachment; filename=BUKTI_MUTASI-" . ArrayAdapter::format("unslug", $header_info->unit) . "_NOMOR_" . $header_info->nomor . "_" . ArrayAdapter::format("date Ymd", $header_info->tanggal) . ".xlsx");
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				$objWriter->save("php://output");
				return;
			}
			$obat_keluar_adapter = new ObatKeluarAdapter();
			$columns = array("id", "nomor", "tanggal", "unit", "status", "keterangan");
			$obat_keluar_dbtable = new DBTable(
				$db,
				"smis_fr_obat_keluar",
				$columns
			);
			$obat_keluar_dbtable->setOrder(" id DESC ");
			$obat_keluar_dbresponder = new ObatKeluarDBResponder(
				$obat_keluar_dbtable,
				$obat_keluar_table,
				$obat_keluar_adapter
			);
			if ($obat_keluar_dbresponder->isSave() && ($_POST['id'] == 0 || $_POST['id'] == "")) {
				$no_mutasi = getSettings($db, "no_mutasi", 0) + 1;
				setSettings($db, "no_mutasi", $no_mutasi);
			}
			$data = $obat_keluar_dbresponder->command($_POST['command']);
            
            /*if($_POST['command'] == 'save') {
                if($data['content']['success'] == 1) {
                    var_dump($data['content']['message']);
                } else {
                    
                }
            }*/
            
			//push_obat_service:
			if (isset($_POST['push_command'])) {
				$obat_keluar_dbtable = new DBTable($db, "smis_fr_obat_keluar");
				$obat_keluar_row = $obat_keluar_dbtable->get_row("
					SELECT *
					FROM smis_fr_obat_keluar
					WHERE id = '" . $data['content']['id'] . "'
				");
				$stok_obat_keluar_dbtable = new DBTable($db, "smis_fr_stok_obat_keluar");
				$detail = $stok_obat_keluar_dbtable->get_result("
					SELECT smis_fr_stok_obat_keluar.id AS 'id_stok_obat_keluar', smis_fr_dobat_keluar.id_obat, smis_fr_dobat_keluar.kode_obat, smis_fr_dobat_keluar.nama_obat, smis_fr_dobat_keluar.nama_jenis_obat, smis_fr_stok_obat.formularium, smis_fr_stok_obat.berlogo, smis_fr_stok_obat.generik, smis_fr_stok_obat.label, smis_fr_stok_obat.id_vendor, smis_fr_stok_obat.nama_vendor, smis_fr_stok_obat_keluar.jumlah, smis_fr_dobat_keluar.satuan, smis_fr_dobat_keluar.konversi, smis_fr_dobat_keluar.satuan_konversi, smis_fr_stok_obat.hna, smis_fr_stok_obat.produsen, smis_fr_stok_obat.tanggal_exp, smis_fr_stok_obat.no_batch, smis_fr_stok_obat.ppn, smis_fr_stok_obat_keluar.prop
					FROM (smis_fr_stok_obat_keluar LEFT JOIN smis_fr_stok_obat ON smis_fr_stok_obat_keluar.id_stok_obat = smis_fr_stok_obat.id) LEFT JOIN smis_fr_dobat_keluar ON smis_fr_stok_obat_keluar.id_dobat_keluar = smis_fr_dobat_keluar.id
					WHERE smis_fr_dobat_keluar.id_obat_keluar = '" . $data['content']['id'] . "'
				");
				$command = "push_" . $_POST['push_command'];
				$push_obat_service_consumer = new PushObatServiceConsumer($db, $obat_keluar_row->id, $obat_keluar_row->tanggal, $obat_keluar_row->unit, $obat_keluar_row->status, $obat_keluar_row->keterangan, $detail, $command);
				$push_obat_service_consumer->execute();
			}
			echo json_encode($data);
			return;
		}
		return;
	}

	//get obat chooser:
	$obat_table = new Table(
		array("Kode", "Obat", "Jenis", "Stok"),
		"",
		null,
		true
	);
	$obat_table->setName("obat");
	$obat_table->setModel(Table::$SELECT);
	$obat_adapter = new ObatAdapter();
	$obat_dbtable = new DBTable($db, "smis_fr_stok_obat");
	$obat_dbtable->setViewForSelect(true);
	$filter = "";
	if (isset($_POST['kriteria'])) {
		$filter = " AND (smis_fr_stok_obat.nama_obat LIKE '%" . $_POST['kriteria'] . "%' OR smis_fr_stok_obat.nama_jenis_obat LIKE '%" . $_POST['kriteria'] . "%')";
	}
	$query_value = "
		SELECT *
		FROM (
			SELECT id_obat AS id, kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok'
			FROM (
				SELECT v_dobat_masuk.id_obat, smis_fr_stok_obat.kode_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, SUM(smis_fr_stok_obat.sisa) AS 'sisa', smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, CASE smis_fr_stok_obat.label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM smis_fr_stok_obat LEFT JOIN (
					SELECT label, id, id_obat
					FROM smis_fr_dobat_f_masuk
				) v_dobat_masuk ON smis_fr_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_fr_stok_obat.label = v_dobat_masuk.label
				WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_stok_obat.konversi = '1' " . $filter . "
				GROUP BY v_dobat_masuk.id_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, smis_fr_stok_obat.label
			) v_obat
			GROUP BY id_obat
		) v_stok
	";
	$query_count = "
		SELECT COUNT(*)
		FROM (
			SELECT id_obat, kode_obat, nama_obat, nama_jenis_obat, GROUP_CONCAT(CONCAT(sisa, ' ', satuan, ' ', label) ORDER BY satuan, sisa, label ASC SEPARATOR ', ') AS 'stok'
			FROM (
				SELECT v_dobat_masuk.id_obat, smis_fr_stok_obat.kode_obat, smis_fr_stok_obat.nama_obat, smis_fr_stok_obat.nama_jenis_obat, SUM(smis_fr_stok_obat.sisa) AS 'sisa', smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, CASE smis_fr_stok_obat.label WHEN 'reguler' THEN '(R)' WHEN 'sito' THEN '(S)' ELSE '(K)' END AS 'label'
				FROM smis_fr_stok_obat LEFT JOIN (
					SELECT label, id, id_obat
					FROM smis_fr_dobat_f_masuk
				) v_dobat_masuk ON smis_fr_stok_obat.id_dobat_masuk = v_dobat_masuk.id AND smis_fr_stok_obat.label = v_dobat_masuk.label
				WHERE smis_fr_stok_obat.prop NOT LIKE 'del' AND smis_fr_stok_obat.konversi = '1' " . $filter . "
				GROUP BY v_dobat_masuk.id_obat, smis_fr_stok_obat.satuan, smis_fr_stok_obat.konversi, smis_fr_stok_obat.satuan_konversi, smis_fr_stok_obat.label
			) v_obat
			GROUP BY id_obat
		) v_stok
	";
	$obat_dbtable->setPreferredQuery(true, $query_value, $query_count);
	$obat_dbresponder = new ObatDBResponder(
		$obat_dbtable,
		$obat_table,
		$obat_adapter
	);
	
	//get sisa by id_obat, satuan, konversi, dan satuan_konversi:
	$sisa_table = new Table(
		array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi"),
		"",
		null,
		true
	);
	$sisa_table->setName("sisa");
	$sisa_adapter = new SimpleAdapter();
	$sisa_adapter->add("id_obat", "id_obat");
	$sisa_adapter->add("sisa", "sisa");
	$sisa_adapter->add("satuan", "satuan");
	$sisa_adapter->add("konversi", "konversi");
	$sisa_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "sisa", "satuan", "konversi", "satuan_konversi");
	$sisa_dbtable = new DBTable(
		$db,
		"smis_fr_stok_obat",
		$columns
	);
	$sisa_dbresponder = new SisaDBResponder(
		$sisa_dbtable,
		$sisa_table,
		$sisa_adapter
	);
	
	//get harga ma:
	$harga_ma_table = new Table(
		array("id_obat", "harga_ma", "jumlah", "satuan", "konversi", "satuan_konversi"),
		"",
		null,
		true
	);
	$harga_ma_table->setName("harga_ma");
	$harga_ma_adapter = new SimpleAdapter();
	$harga_ma_adapter->add("id_obat", "id_obat");
	$harga_ma_adapter->add("harga_ma", "harga_ma");
	$harga_ma_adapter->add("satuan", "satuan");
	$harga_ma_adapter->add("konversi", "konversi");
	$harga_ma_adapter->add("satuan_konversi", "satuan_konversi");
	$columns = array("id_obat", "harga_ma", "jumlah", "satuan", "konversi", "satuan_konversi");
	$harga_ma_dbtable = new DBTable(
		$db,
		"smis_fr_stok_obat",
		$columns
	);
	$harga_ma_dbresponder = new MADBResponder(
		$harga_ma_dbtable,
		$harga_ma_table,
		$harga_ma_adapter
	);
	
	$super_command = new SuperCommand();
	$super_command->addResponder("obat", $obat_dbresponder);
	$super_command->addResponder("sisa", $sisa_dbresponder);
	$super_command->addResponder("harga_ma", $harga_ma_dbresponder);
	$init = $super_command->initialize();
	if ($init != null) {
		echo $init;
		return;
	}
	
	$obat_keluar_modal = new Modal("obat_keluar_add_form", "smis_form_container", "obat_keluar");
	$obat_keluar_modal->setTitle("Obat Keluar");
	$obat_keluar_modal->setClass(Modal::$FULL_MODEL);
	$id_text = new Text("obat_keluar_id", "obat_keluar_id", "");
	$id_text->setAtribute("disabled='disabled'");
	$obat_keluar_modal->addElement("Nomor", $id_text);
	$nomor_hidden = new Hidden("obat_keluar_nomor", "obat_keluar_nomor", "");
	$obat_keluar_modal->addElement("", $nomor_text);
	$tanggal_text = new Text("obat_keluar_tanggal", "obat_keluar_tanggal", "");
	$tanggal_text->setClass("mydate");
	$tanggal_text->setAtribute("data-date-format='yyyy-m-d'");
	$obat_keluar_modal->addElement("Tanggal", $tanggal_text);
	$unit_service_consumer = new UnitServiceConsumer($db);
	$unit_service_consumer->execute();
	$unit_option = $unit_service_consumer->getContent();
	$unit_select = new Select("obat_keluar_unit", "obat_keluar_unit", $unit_option);
	$obat_keluar_modal->addElement("Unit", $unit_select);
	$dobat_keluar_table = new DObatKeluarTable(
		array("No.", "Nama Obat", "Jenis Obat", "Diminta", "Diberikan", "Keterangan"),
		"",
		null,
		true
	);
	$dobat_keluar_table->setName("dobat_keluar");
	$dobat_keluar_table->setFooterVisible(false);
	$obat_keluar_modal->addBody("dobat_keluar_table", $dobat_keluar_table);
	$save_button = new Button("", "", "Simpan");
	$save_button->setClass("btn-success");
	$save_button->setAtribute("id='obat_keluar_save'");
	$save_button->setIcon("fa fa-floppy-o");
	$save_button->setIsButton(Button::$ICONIC);
	$obat_keluar_modal->addFooter($save_button);
	$ok_button = new Button("", "", "OK");
	$ok_button->setClass("btn-success");
	$ok_button->setAtribute("id='obat_keluar_ok'");
	$ok_button->setAction("$($(this).data('target')).smodal('hide')");
	$obat_keluar_modal->addFooter($ok_button);
	
	$dobat_keluar_modal = new Modal("dobat_keluar_add_form", "smis_form_container", "dobat_keluar");
	$dobat_keluar_modal->setTitle("Detail Obat Keluar");
	$id_hidden = new Hidden("dobat_keluar_id", "dobat_keluar_id", "");
	$dobat_keluar_modal->addElement("", $id_hidden);
	$id_obat_hidden = new Hidden("dobat_keluar_id_obat", "dobat_keluar_id_obat", "");
	$dobat_keluar_modal->addElement("", $id_obat_hidden);
	$name_obat_hidden = new Hidden("dobat_keluar_name_obat", "dobat_keluar_name_obat", "");
	$dobat_keluar_modal->addElement("", $name_obat_hidden);
	$nama_obat_button = new Button("", "", "Pilih");
	$nama_obat_button->setClass("btn-info");
	$nama_obat_button->setAction("obat.chooser('obat', 'obat_button', 'obat', obat)");
	$nama_obat_button->setIcon("icon-white icon-list-alt");
	$nama_obat_button->setAtribute("id='obat_browse'");
	$nama_obat_button->setIsButton(Button::$ICONIC);
	$nama_obat_text = new Text("dobat_keluar_nama_obat", "dobat_keluar_nama_obat", "");
	$nama_obat_text->setClass("smis-one-option-input");
	$nama_obat_input_group = new InputGroup("");
	$nama_obat_input_group->addComponent($nama_obat_text);
	$nama_obat_input_group->addComponent($nama_obat_button);
	$dobat_keluar_modal->addElement("Nama Obat", $nama_obat_input_group);
	$nama_jenis_obat_text = new Text("dobat_keluar_nama_jenis_obat", "dobat_keluar_nama_jenis_obat", "");
	$nama_jenis_obat_text->setAtribute("disabled='disabled'");
	$dobat_keluar_modal->addElement("Jenis Obat", $nama_jenis_obat_text);
	$kode_obat_text = new Text("dobat_keluar_kode_obat", "dobat_keluar_kode_obat", "");
	$kode_obat_text->setAtribute("disabled='disabled'");
	$dobat_keluar_modal->addElement("Kode Obat", $kode_obat_text);
	$satuan_select = new Select("dobat_keluar_satuan", "dobat_keluar_satuan", "");
	$dobat_keluar_modal->addElement("Satuan", $satuan_select);
	$konversi_hidden = new Hidden("dobat_keluar_konversi", "dobat_keluar_konversi", "");
	$konversi_hidden->setAtribute("disabled='disabled'");
	$dobat_keluar_modal->addElement("", $konversi_hidden);
	$satuan_konversi_hidden = new Hidden("dobat_keluar_satuan_konversi", "dobat_keluar_satuan_konversi", "");
	$dobat_keluar_modal->addElement("", $satuan_konversi_hidden);
	$stok_hidden = new Hidden("dobat_keluar_stok", "dobat_keluar_stok", "");
	$dobat_keluar_modal->addElement("", $stok_hidden);
	$harga_ma_hidden = new Hidden("dobat_keluar_harga_ma", "dobat_keluar_harga_ma", "");
	$dobat_keluar_modal->addElement("", $harga_ma_hidden);
	$f_stok_text = new Text("dobat_keluar_f_stok", "dobat_keluar_f_stok", "");
	$f_stok_text->setAtribute("disabled='disabled'");
	$dobat_keluar_modal->addElement("Stok", $f_stok_text);
	$jumlah_diminta_text = new Text("dobat_keluar_jumlah_diminta", "dobat_keluar_jumlah_diminta", "");
	$dobat_keluar_modal->addElement("Jml. Diminta", $jumlah_diminta_text);
	$jumlah_lama_hidden = new Hidden("dobat_keluar_jumlah_lama", "dobat_keluar_jumlah_lama", "");
	$dobat_keluar_modal->addElement("", $jumlah_lama_hidden);
	$jumlah_text = new Text("dobat_keluar_jumlah", "dobat_keluar_jumlah", "");
	$dobat_keluar_modal->addElement("Jml. Diberikan", $jumlah_text);
	$dobat_keluar_button = new Button("", "", "Simpan");
	$dobat_keluar_button->setClass("btn-success");
	$dobat_keluar_button->setAtribute("id='dobat_keluar_save'");
	$dobat_keluar_button->setIcon("fa fa-floppy-o");
	$dobat_keluar_button->setIsButton(Button::$ICONIC);
	$dobat_keluar_modal->addFooter($dobat_keluar_button);
	
	echo $dobat_keluar_modal->getHtml();
	echo $obat_keluar_modal->getHtml();
	echo $obat_keluar_table->getHtml();
	echo addJS("framework/smis/js/table_action.js");
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");	
	echo addJS("gudang_farmasi/js/obat_keluar_action.js", false);
	echo addJS("gudang_farmasi/js/dobat_keluar_action.js", false);
	echo addJS("gudang_farmasi/js/obat_keluar_obat_action.js", false);
	echo addJS("gudang_farmasi/js/obat_keluar.js", false);
?>