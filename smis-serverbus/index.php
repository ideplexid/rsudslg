
<?php
global $db;
global $user;
require_once 'smis-libs-class/Policy.php';
$policy=new Policy("smis-serverbus", $user);
if($user->getCapability()=="administrator"){
	if(file_exists("smis-serverbus/modul/".$_POST['action'].".php")){
		$policy->setDefaultCookie(Policy::$DEFAULT_COOKIE_CHANGE);
		$policy->allow($_POST['page'], "modul/".$_POST['action']);
		setChangeCookie($_POST['action']=="server");
	}
}else{
	$policy->denied();
}
?>