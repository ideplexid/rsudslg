<?php 
	/**
	 * this one used for when an entity call for service
	 * Entity call to Server Bus
	 */

if(isset($_POST['msg'])){
	
	$DIR		= getcwd();
	$SMIS_DIR	= $DIR."/../";
	chdir($SMIS_DIR);
	require_once 'smis-base/smis-include-system.php';
	require_once 'smis-base/smis-include-service.php';
	require_once "smis-base/smis-config.php";
	require_once 'smis-base/smis-function.php';
	require_once 'smis-base/smis-service-variable.php';
	
	session_name(getSessionPrefix());
	session_start();

	global $wpdb;
	global $db;
	global $user;
	global $querylog;
	
	$server_key		= getSettings($db, "smis_serverbus_key", "no-set");//get server bus key
	$requester_key	= "not set yet"; //take it from message that send from consumer
	
	$comunication	= new Communication();
	$comunication->setKey($requester_key, $server_key);	//communication beetween consumer and server bus
	$msg			= $_POST['msg']; 								//get message from consumer
	
	$REQUEST_CONTENT		= $comunication->receive_command($msg);		//extract message from consumer using server_key
	$RAW_SUCCESSOR_SEND		= array();
	$RAW_SUCCESSOR_GET		= array();
	$RAW_SUCCESSOR			= array();
	$AUTONOMOUS_GET			= array();
	$CODE					= "NOT SET";
	$requester_id			= "NOT SET";
	if($REQUEST_CONTENT!=null){							//if content is null the 100% consumer using the wrong code for server bus
		$request			= json_decode($REQUEST_CONTENT,true); 	//extract the full meaning of requester		
		$entity				= $request['entity'];						//get the entity that the consumer want , default is all
		$autonomous			= $request['autonomous'];				//get autonomouse that consumer want. default is all
		$service			= $request['service'];					//what service that the consumer want
		$requester_key		= $request['return_key'];			//this is the key that used serverbus to repack the request
		$requester_id		= $request['autonomous_id'];		//this is the autonomous id, should check with the requester_key 
		$data				= $request['data'];							//data that will be stored to produser, can be string, or array
		$acaller			= $request['acaller'];					//the caller of this request (the autonomous that consume this request)
		$ip					= $request['ip'];								//ip address of user that access the system
		$nuser				= $request['user'];
		$CODE				= $request['code'];
		$user->setRawUser($nuser, $ip);
		
		//user that using this system
		$dbtable			= new DBTable($db, "smis_sb_autonomous");		
		if(!$dbtable->is_exist(array("autonomous_id"=>$requester_id,"oauth"=>$requester_key))){
			return null;
		}
        
        if(getSettings($db,"smis-serverbus-activate-duplicate",0)==1){
            if(isset($data['smis-synch-consumer-token']) && $data['smis-synch-consumer-token']==1){
                /* if duplicate process but this current autonomous is not duplicate  */
                $dbtable->addCustomKriteria("is_duplicate","='1'");
            }else{
                /* if not duplicate process but this current autonomous is duplicate  */
                $dbtable->addCustomKriteria("is_duplicate","='0'");
            }
        }
        
        if($autonomous!=="all"){
            $dbtable->addCustomKriteria("autonomous_id","='".$autonomous."'");
        }
        
		$dbtable->setShowAll(true);
		$list				= $dbtable->view("", 0);
		$all_autonomous		= $list['data'];
		$RESPONSE_CONTENT	= array();
		$AUTONOMOUS_GET		= json_encode($all_autonomous);
		foreach($all_autonomous as $atnm){
			//komunikasi dengan provider atau service		
            if($atnm->autonomous_id==$autonomous || $autonomous=='all'){
				$produser_package = new PackageComunication($autonomous, $entity, $service,$data);
				$produser_package ->setParams($server_key, $user->getNameOnly(), $ip,$CODE);
					
				$send			  	= new SendRequest($atnm->address,$querylog);
				$send->setPackage($produser_package);
				$send->setKeys($atnm->oauth, $server_key);
				$result			  	= $send->execute();

				$result_non_json				 = json_decode($result);
				$RESPONDER_KEY[$atnm->name]		 = $atnm->oauth;
				$RESPONSE_CONTENT[$atnm->name]	 = $result_non_json;
				$RAW_SUCCESSOR_SEND[$atnm->name] = $send->getSendedMessage();
				$RAW_SUCCESSOR_GET[$atnm->name]  = $result_non_json;
			}
		}
		

		$comunication->setKey($requester_key, $server_key);
		$RESPONSE_CONTENT_STRING = json_encode($RESPONSE_CONTENT);
		$RAW_RESPONSE			 = $comunication->send_command($RESPONSE_CONTENT_STRING);
		echo $RAW_RESPONSE;
	}
	$sblog['raw_request']		= $_POST['msg'];
	$sblog['code']				= $CODE;
	$sblog['key_server']		= $server_key;
	$sblog['key_requester']		= $requester_key;
	$sblog['key_responder']		= json_encode($RESPONDER_KEY);	
	$sblog['raw_response']		= $RAW_RESPONSE;
	$sblog['request']			= $REQUEST_CONTENT;
	$sblog['response']			= $RESPONSE_CONTENT_STRING;
	$sblog['successor_send']	= json_encode($RAW_SUCCESSOR_SEND);
	$sblog['successor_get']		= json_encode($RAW_SUCCESSOR_GET);
	$sblog['autonomous']		= $all_autonomous;
	$querylog->saveServerBusLog($service,$CODE,$sblog,$requester_id);
}

?>