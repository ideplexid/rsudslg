<?php 


require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
$smis = new SettingsBuilder ( $db, "serverbus_settings", "smis-serverbus", "server" );
$smis->setShowDescription ( true );
$smis->setTabulatorMode ( Tabulator::$POTRAIT );
$smis->addTabs ( "system", "System"," fa fa-desktop" );
$smis->addItem ( "system", new SettingsItem ( $db, "smis_serverbus_key", "Server Bus Key", md5(date('mdyhisu')), "text", "Serverbus Key " ) );
$smis->addItem ( "system", new SettingsItem ( $db, "smis-serverbus-activate-duplicate", "Activate Duplicate Filter", 0, "checkbox", "Duplicate Filterring would activated if check" ) );
$response = $smis->init ();

?>
