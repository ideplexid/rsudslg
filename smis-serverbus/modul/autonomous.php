<?php 
/**
 * this is handling about element where used for patient management
 * @author goblooge
 *
 */

class AutonomousTable extends Table{
	public function getContentButton($id){
		$btn_group=parent::getContentButton($id);
		$btn_group->add("","","Test",$this->name.".test('".$id."')", "btn-primary","");
		return $btn_group;
	}
}

$uitable=new AutonomousTable(array('ID','Name','Address','Component','Service','OAuth','Duplicate'),"Server Bus : Autonomous", NULL,true);
$uitable->setName("autonomous");

/*this is respond when system have to response*/
if(isset($_POST['command'])){
	$dbtable=new DBTable($db, "smis_sb_autonomous");
	
	if($_POST['command']=="test"){
		require_once 'smis-base/smis-include-service.php';

		$select=$dbtable->select($_POST['id'],false);
		$url=$select->address;
		$send_key=$select->oauth;	
		$receive_key=getSettings($db, "smis_serverbus_key", md5(date('mdyhisu')));
		
		$pakage=new PackageComunication('all','all','ping','all');
	
		$send=new SendRequest($url);
		$send->setPackage($pakage);		
		$send->setKeys($send_key, $receive_key);
		$result=$send->execute();
		
		$response=new ResponsePackage();
		$response->setAlertContent("Testing Selesai", $result, ResponsePackage::$TIPE_WARNING);
		$response->setAlertVisible(true);
		$response->setStatus(ResponsePackage::$STATUS_OK);
		
		echo json_encode($response->getPackage());
		return;
	}
	
	class myadapter extends ArrayAdapter{
		public function adapt($onerow){
			$array=array();
			$array['id']=$onerow->id;
			$array['ID']=$onerow->autonomous_id;
			$array['Name']=$onerow->name;
			$array['Address']=$onerow->address;
			$array['Component']=$onerow->component;
			$array['Service']=$onerow->service;
			$array['OAuth']=$onerow->oauth;
            $array['Duplicate']=$onerow->is_duplicate==1?"<i class='fa fa-check'></i>":"";			
			return $array;
		}
	}
	
	class autonomousResponder extends DBResponder{
		public function postToArray(){
			$result=parent::postToArray();
			if(isset($result['address'])){
				$result['address']=confirmURL($result['address'],"");
			}
			return $result;
		}
	}
	
	$adapter=new myadapter();
	$dbres=new autonomousResponder($dbtable,$uitable,$adapter);	
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

/*This is Modal Form and used for add and edit the table*/
$mcolumn=array('id','name','autonomous_id','address','component','service','oauth',"is_duplicate");
$mname=array("","Name","Autonom ID","Address","Component","Service","OAuth","Duplicate");
$mtype=array("hidden","text","text","text","text","text","text","checkbox");
$mvalue=array("","","","","","","","0");

$uitable->setModal($mcolumn, $mtype, $mname, $mvalue);
$modal=$uitable->getModal();
$modal->setTitle("Autonomous");

echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/smis/js/table_action.js");

?>
<script type="text/javascript">

function AutonomousAction(name,page,action,column){
	this.initialize(name, page, action, column);
}
AutonomousAction.prototype.constructor = AutonomousAction;
AutonomousAction.prototype= Object.create( TableAction.prototype );
AutonomousAction.prototype.test = function(id){
	var data=this.getRegulerData();
	data['id']=id;
	data['command']="test";
	$.post('',data,function(res){
		getContent(res);
	});
};


var autonomous;

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','name','autonomous_id','address','component','service','oauth','is_duplicate');
	autonomous=new AutonomousAction("autonomous","smis-serverbus","autonomous",column);
	autonomous.view();
});
</script>
