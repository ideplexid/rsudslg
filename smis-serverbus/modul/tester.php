<?php 
global $db;
if(isset($_POST['command'])){	
	$autonomous=$_POST['autonomous'];
	$entity=$_POST['entity'];
	$service=$_POST['service'];
	$user=$_POST['user'];
	$ip=$_POST['ip'];
	$data=$_POST['data'];
	$via=$_POST['via'];
	$command=$_POST['command'];	
	$proto=$_POST['proto'];
	$package=new ResponsePackage();
	
	if($command=="json"){
		$data=json_decode($data,true);
	}
	
	if($data==null){
		$package->setWarning(true,"JSON Error","Your JSON Format on Data is Incorrect </br>".$_POST['data']);
		echo json_encode($package->getPackage());
		return;
	}
	
	
	if($via=="direct"){
		if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $autonomous)){
			$package->setWarning(true,"Autonomous Error ".$autonomous,"Autonomous Should an URL because Via Direct ");
			echo json_encode($package->getPackage());
			return;
		}
		$path_url=$entity."/service/".$service.".php";
		$data['smis_path_url']=$path_url;
		$autonomous=getFolderUrl($autonomous);
		$multi="";
		if(is_multisite()){
			$multi=MULTISITE;
		}
		$raw=getContentUrl($data,$ip,$user,"Tester",$autonomous."/smis-base/smis-service-caller.php?entity=".$proto."&&multisite=".$multi);		
	}else{
		include_once 'smis-framework/smis/connection/PackageComunication.php';
		include_once 'smis-framework/smis/connection/Communication.php';
		include_once 'smis-framework/smis/connection/SendRequest.php';
		include_once 'smis-framework/smis/package/ResponsePackage.php';
		$return_key=getSettings($db, "smis_autonomous_key", "");
		$pc=new PackageComunication($autonomous, $entity, $service,$data);
		$pc->setParams($return_key, $user, $ip);
		
		$url=getSettings($db, "smis_serverbus_url", md5(date('mdyhisu')));
		$send_key=getSettings($db, "smis_serverbus_key", md5(date('mdyhisu')));
		$receive_key=$return_key;
		$url=confirmURL($url,"smis-serverbus/service.php");
		
		$send=new SendRequest($url);
		$send->setPackage($pc);
		$send->setKeys($send_key, $receive_key);
		$raw=$send->execute();
	}
	
	$raw_data=json_decode($raw);
	if($raw_data==null){
		ob_start();
		var_dump($raw);
		$result = ob_get_clean();
	}else{
		ob_start();
		var_dump($raw_data);
		$result = ob_get_clean();
	}
	
	$package->setAlertVisible(true);
	$package->setContent(array("result"=>$result,"raw"=>$raw));
	$package->setStatus("ok");
	$package->setAlertContent("Fetch Success","Response Success From Via ".$_POST['via'],"alert-success");
	echo json_encode($package->getPackage());
	return;
}

$dbtable_auto=new DBTable($db,"smis_sb_autonomous");
$dbtable_auto->setOrder("name ASC ");
$dbtable_auto->setShowAll(true);
$data=$dbtable_auto->view("",0);
$autonadapter=new SelectAdapter("name", "address");
$autonom=$autonadapter->getContent($data['data']);
$autonom[]=array("name"=>"All","value"=>"all","default"=>"1");

$modal=new Modal("tester", "tester", "Testing Server Bus");
$modal->addElement("Autonomous", new Select("autonomous_tester", "autonomous_tester", $autonom));
$modal->addElement("Entity", new Text("entity", "entity", "all"));
$modal->addElement("Service", new Text("service", "service", "get_entity"));
$modal->addElement("Return Keys", new Text("return_key", "return_key", "0123456789"));
$modal->addElement("User", new Text("user", "user", "goblooge"));
$modal->addElement("IP", new Text("ip", "ip", "192.168.1.1"));
$modal->addElement("Data", new Text("data", "data", "all"));
$modal->addElement("Prototype", new Text("proto", "proto", ""));
$modal->addElement("Via", new Select("via", "via", array(array("value"=>"serverbus","name"=>"Server Bus"),array("value"=>"direct","name"=>"Direct"))));

$form=$modal->getForm();
$btg=new ButtonGroup("");

$button=new Button("save", "save", "Json");
$button->setClass("btn-success");
$button->setAction("test('json')");
$btg->addElement($button);
$button=new Button("save", "save", "Text");
$button->setClass("btn-warning");
$button->setAction("test('text')");
$btg->addElement($button);
$form->addElement("", $btg);
echo $modal->getForm()->getHtml(); 
?>


<div class='clear line'></div>
<div class="row-fluid">
	<div class='span6'>
			<div>Result Test</div>
			<div id="result_container">
				<textarea id="result_tester"></textarea>
  			</div>
  	</div>
	<div class='span6'>
		<div>Result Raw</div>
		<div id="raw_container">
			<textarea id="raw_tester"></textarea>
  		</div>
	</div>
</div>

<style type="text/css">
	#raw_tester, #result_tester{
		width:100%;
		max-height:300px;
		height:300px;
		overflow-y:auto;
	}
</style>

<script type="text/javascript">
function test(data_type){
	$.post('',{
		autonomous:$("#autonomous_tester").val(),
		entity:$("#entity").val(),
		service:$("#service").val(),
		user:$("#user").val(),
		proto:$("#proto").val(),
		ip:$("#ip").val(),
		data:$("#data").val(),
		via:$("#via").val(),
		page:"smis-serverbus",
		action:"tester",
		command:data_type
		},function(res){
			var json=getContent(res);
			if(json!=null)
			$("#result_tester").val(json.result);
			$("#raw_tester").val(json.raw);
	});
}
</script>