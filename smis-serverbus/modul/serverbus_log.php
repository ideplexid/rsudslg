<?php 
setChangeCookie(false);
class LogUITable extends Table{
	public function getHeaderButton(){
		return "";
	}
	public function getContentButton($id){
		$btn_group=new ButtonGroup('noprint');
		$btn=new Button("", "", "");
		$btn->setClass("btn btn-info");
		$btn->setAction($this->name.".show('".$id."')");
		$btn->setIsButton(Button::$ICONIC);
		$btn->setIcon("icon-white  icon-resize-full");
		$btn_group->addButton($btn);
		return $btn_group;
	}
}

$uitable=new LogUITable(array('Autonomous',"Code","User","IP",'Waktu'),"Server Bus Log", NULL,true);
$uitable->setName("serverbus_log");
$button=new Button("", "", "");
$button->setAction("serverbus_log.clean()");
$button->setIsButton(Button::$ICONIC);
$button->setIcon("fa fa-trash");
$button->setClass("btn-primary");
$uitable->addFooterButton($button);

if(isset($_POST['command']) && $_POST['command'] =="clean"){
	$db->query("truncate smis_sb_log");
	return "";
}

/*this is respond when system have to response*/
if(isset($_POST['command'])){
	class myadapter extends ArrayAdapter{
		public function adapt($onerow){
			$array=array();
			$array['id']=$onerow->id;
			$request=json_decode($onerow->request);
			$array['Autonomous']=$request->acaller;
			$array['Code']=$request->code;
			$array['Service']=$request->service;
			$array['User']=$request->user;
			$array['IP']=$request->ip;
			$array['Waktu']=self::format('date d-M-Y, H:i:s', $onerow->waktu);
			
			$autonomous=json_decode($onerow->autonomous);
			$at=array();
			foreach($autonomous as $a){
				$at[$a->name]=$a->address;
			}
			$responder=self::format("array", $at);
			
			$key_server=$onerow->key_server;	
			$key_responder=self::format("json", $onerow->key_responder);
			$key_requester=$onerow->key_requester;
							
			$request=$onerow->request;
			$response=str_replace("<","&lt;",$onerow->response);
			$raw_request=$onerow->raw_request;
			$raw_response=$onerow->raw_response;
			$successor_send=$onerow->successor_send;
			$successor_get=str_replace("<","&lt;",$onerow->successor_get);
			
				
			$RESP="		<div class='label label-warning'>ENCRYPTED</div></br></br>
						<div><div contenteditable='true'>$raw_response</div></div>
						<div class='label'>RAW</div>
						<div><div contenteditable='true'>$response</div></div></br>
						<div class='label label-success'>FETREEFY</div>
						<div><pre>".json_encode(json_decode($response,true),JSON_PRETTY_PRINT)."</pre></div></br>
						";
			
			$REQ="		<div class='label label-important'>ENCRYPTED</div>
						<div><div contenteditable='true'>$raw_request</div></div></br>
						<div class='label'>RAW</div>
						<div><div contenteditable='true'>$request</div></div></br>
						<div class='label label-success'>FETREEFY</div>
						<div><pre>".json_encode(json_decode($request,true),JSON_PRETTY_PRINT)."</pre></div></br>
						";
			
			$SUCCESSOR="<div class='label'>SEND</div>
						<div><div contenteditable='true'>$successor_send</div></div></br>
						<div class='label label-success'>SEND FETREEFY</div>
						<div><pre>".json_encode(json_decode($successor_send,true),JSON_PRETTY_PRINT)."</pre></div></br>
						<div class='label'>GET</div>
						<div><div contenteditable='true'>$successor_get</div></div></br>
						<div class='label label-success'>GET FETREEFY</div>
						<div><pre>".json_encode(json_decode($successor_get,true),JSON_PRETTY_PRINT)."</pre></div></br>
			";
			
			$RESPOND="<div class='label label-success'>Autonomous</div>
						<div>$responder</div>
						";
			
			$key['Key Serverbus']=$key_server;
			$key['Key Autonomous Consumer']=$key_requester;
			$key['Keys Autonomous Responder']=$key_responder;
				
			
			$KEY=ArrayAdapter::format("array", $key);
			$tabulator=new Tabulator("", "");
			$tabulator->add("key_".$onerow->id, "Key", $KEY, Tabulator::$TYPE_HTML,"fa fa-key");
			$tabulator->add("responder_".$onerow->id, "Autonomous Responder", $RESPOND, Tabulator::$TYPE_HTML,"fa fa-desktop");
			$tabulator->add("request_".$onerow->id, "Request", $REQ, Tabulator::$TYPE_HTML,"fa fa-long-arrow-right");
			$tabulator->add("successor_".$onerow->id, "Successor", $SUCCESSOR, Tabulator::$TYPE_HTML,"fa fa-arrows-h");
			$tabulator->add("response_".$onerow->id, "Response", $RESP, Tabulator::$TYPE_HTML,"fa fa-long-arrow-left");
			$array['tabs']=$tabulator->getHtml();			
			return $array;
		}
		
		
	}
	$adapter=new myadapter();
	$dbtable=new DBTable($db, "smis_sb_log");
	$dbtable->setOrder("id DESC");
	$dbres=new DBResponder($dbtable,$uitable,$adapter);
	$dbres->setUseAdapterForSelect(true);
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

/*This is Modal Form and used for add and edit the table*/
echo $uitable->getHtml();
echo addJS("framework/smis/js/table_action.js");
echo addJS("base-js/smis-base-fetreefy-json.js");

?>
<script type="text/javascript">
var serverbus_log;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('name','alias','no_dtd','icd','id');
	serverbus_log=new TableAction("serverbus_log","smis-serverbus","serverbus_log",column);
	serverbus_log.show=function(id){
		var data=this.getEditData(id);
		data['id']=id;
		data['command']="select";
		showLoading();
		$.post("",data,function(res){
			var json=getContent(res);
			showWarning("Server Bus Log : "+id,json.tabs);
			$("pre").fetreefy();
			dismissLoading();
		});
	};
	serverbus_log.clean=function(){
		var data=this.getRegulerData();
		data['command']="clean";
		$.post("",data,function(res){
			serverbus_log.view();
		});
	};
	
	serverbus_log.view();
});
</script>
<style>
.area{	max-width:780px; min-width:780px; min-height:100px; height:auto;}
</style>