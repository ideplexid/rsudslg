<?php
global $PLUGINS;

$init['name'] = 'aplicare';
$init['path'] = SMIS_DIR . $init['name'] . '/';
$init['description'] = 'Aplicare';
$init['required'] = 'Aplikasi Laravel (Eksternal)';
$init['service'] = '';
$init['type'] = '';
$init['version'] = "1.0.0";
$init['number'] = '1';

$plugin = new Plugin($init);
$PLUGINS[$init['name']] = $plugin;
