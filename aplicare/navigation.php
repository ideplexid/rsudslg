<?php
global $NAVIGATOR;

$menu = new Menu('fa fa-user-md');
$menu->addProperty('title', 'Aplicare');
$menu->addProperty('name', 'Aplicare');

$menu->addLaravel("Aplicare", 'aplicare', 'aplicare', 'Aplicare', 'fa fa-stethoscope');
$menu->addLaravel("Credentials", 'aplicare', 'credentials', 'Credentials', 'fa fa-key');

$NAVIGATOR->addMenu($menu, 'aplicare');
