<?php
global $TIME_START;
global $MEMORY_START;
$TIME_START		= microtime(true);
$MEMORY_START	= memory_get_peak_usage();
global $CHANGE_COOKIE;
$CHANGE_COOKIE 	= true;
if(file_exists("smis-base/smis-config.php")){
	require_once ("smis-base/smis-config.php");
}
require_once 'smis-base/smis-include-ui.php';
require_once 'smis-base/smis-include-system.php';
require_once 'smis-libs-function/smis-libs-function-essential.php';
session_name(getSessionPrefix());	
session_start ();
$ajax 			= is_ajax() || ( isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'POST'); // bypass non ajax request
$installed 		= is_system_installed (); 						                                        // check is this system was installed
$installing 	= is_installing_process (); 					                                        // check is this request is installing request

if (! $installed) {
	if (! $ajax) {
		require_once 'smis-libs-function/smis-libs-function-ui.php';
		require_once 'smis-base/smis-index.php';
	} else if ($installing) { // install using ajax
		require_once 'smis-base/smis-install-action.php';
		require_once 'smis-base/smis-end-process.php';
	} else { // ajax but not installed
		if (isset ( $_COOKIE ["smis-index"] )) {
			setcookie ( "smis-index", '', time () - 7000000 );
		}
		setSession("user",null);
		session_destroy ();
		
		$response = new ResponsePackage();
		$response ->setAlertVisible(true)
				  ->setStatus(ResponsePackage::$STATUS_NOT_INSTALLED)
				  ->setAlertContent("System Not Installed", "The smis system is not installed", "alert-danger")
				  ->setContent("");
		echo json_encode($response->getPackage());
	}
} else {
	$realtime			= is_realtime();			// check is this the realtime request
	$authorize 			= is_authorize($realtime); 	// check is the user was authorized
	$is_serverbus 		= is_serverbus_request(); 	// check is that smis-service.php try to access
	$is_try_login 		= is_try_login(); 			// check is that trying login
	$is_registration 	= is_registration(); 		// check is that trying login
	
	if ($is_serverbus) {
		require_once 'smis-base/smis-service.php';
		require_once 'smis-base/smis-end-process.php';
	} else if ($is_try_login) {
		require_once 'smis-base/smis-login-page.php';
		require_once 'smis-base/smis-end-process.php';
	} else if ($is_registration) {
		require_once 'smis-base/smis-function.php';
		require_once 'smis-base/smis-init-variable.php';
		require_once 'smis-base/smis-registration.php';
		require_once 'smis-base/smis-end-process.php';
	} else if (! $authorize) {
        if(is_direct()){
            require_once "smis-base/smis-direct.php";
        }else if(isset($argv) && is_terminal($argv)){
            /* scheduled event go here */
            require_once 'smis-base/smis-terminal-invoker.php';
        }else if ($ajax) {
			require_once 'smis-base/smis-login-out.php';
		} else {
        	require_once 'smis-base/smis-login-out-reload.php';
			require_once 'smis-base/smis-function.php';
			require_once 'smis-base/smis-index.php';
		}
		require_once 'smis-base/smis-end-process.php';
	} else if (is_download ()) { // download file only
		require_once ("smis-base/smis-download.php");
	} else {
		$dbconnector = new DBConnector ( SMIS_SERVER, SMIS_DATABASE, SMIS_USERNAME, SMIS_PASSWORD );
		$result 	 = $dbconnector->connect ();
		require_once 'smis-base/smis-function.php';
		require_once 'smis-base/smis-init-variable.php';
        require_once "smis-base/smis-php-ini.php";
		date_default_timezone_set ( getSettings ( $db, "smis_autonomous_timezone", "Asia/Jakarta" ) );
		     
        if (! isset ( $_POST ['page'] )){
            /* then ready show the default home page*/
            require_once 'smis-base/smis-base.php';               
        }	
        if (isset ( $_POST ['download_token'] ) || $ajax) {
            ob_start ();
            if(getSettings($db, "smis-debuggable-mode","0")=="1"){
                show_error();
            }
            require_once (loadPage ());
            $result = ob_get_clean ();
            if ($CHANGE_COOKIE && ! isset ( $_POST ['smis_help'] )) {
                changeCookie ();
            }
            echo $result;
        } else{
            require_once 'smis-base/smis-index.php';
        }
		require_once 'smis-base/smis-end-process.php';
	}
}
?>