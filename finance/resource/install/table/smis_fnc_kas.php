<?php
    require_once "smis-libs-class/DBCreator.php";
    $dbcreator=new DBCreator($wpdb,"smis_fnc_kas",DBCreator::$ENGINE_MYISAM);
    $dbcreator->addColumn("nama", "varchar(128)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("keterangan", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->addColumn("nomor_akun", "varchar(16)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
    $dbcreator->setDuplicate(true);
    $dbcreator->initialize();
?>