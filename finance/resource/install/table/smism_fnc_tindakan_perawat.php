<?php
require_once "smis-libs-class/DBCreator.php";
$dbcreator=new DBCreator($wpdb,"smism_fnc_tindakan_perawat",DBCreator::$ENGINE_MYISAM);
$dbcreator->addColumn("tanggal", "date", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("pasien", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("tindakan", "varchar(64)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("biaya_awal", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("persentase", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("bag_karyawan", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("bag_rs", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("pajak", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("dana_pengembangan", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("carabayar", "varchar(16)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("id_karyawan", "int(11)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("nama_karyawan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->addColumn("ruangan", "varchar(32)", DBCreator::$SIGN_NONE, false, DBCreator::$DEFAULT_NONE, false,DBCreator::$ON_UPDATE_NONE, "");
$dbcreator->setDuplicate(false);
$dbcreator->initialize();
?>