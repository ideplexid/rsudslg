<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']!="") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");

	$actable=new Table($head);
	$actable->setName("rekap_pv_bronchoscopy_accounting");
	$actable->setModel(Table::$SELECT);
	$accounting=new EmployeeResponder($db,$actable,$dkadapter,"finance");

	$super=new SuperCommand();
	$super->addResponder("rekap_pv_bronchoscopy_accounting",$accounting);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

$header=array("Dokter","Biaya Awal","Bagian Dokter","Bagian RS","Pajak","Dana Pengembangan");
$uitable=new Table($header);
$head00="<tr>
		<th colspan='30' id='surat_bukti'>REKAP BRONCHOSCOPY DOKTER<font id='untuk_table_rekap_pv_bronchoscopy'></font></th>
		</tr>";
$head0="<tr>
		<th>Nomor</th>
		<th  id='kode_table_rekap_pv_bronchoscopy'></th>
		<th>Waktu</th>
		<th colspan='3' id='waktu_table_rekap_pv_bronchoscopy'></th>
		</tr>";
$head2="<tr>
		<th >Dari</th>
		<th id='dari_table_rekap_pv_bronchoscopy'></th>
		<th >Sampai</th>
		<th colspan='3'  id='sampai_table_rekap_pv_bronchoscopy'></th>
		</tr>";

$head3="<tr>
		<th >Pajak</th>
		<th id='pajak_table_rekap_pv_bronchoscopy' colspan='20'></th>
		</tr>";

$uitable->addHeader("before", $head00)
		->addHeader("before", $head0)
		->addHeader("before", $head2)
		->addHeader("before", $head3)
		->setName("rekap_pv_bronchoscopy")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']== "list") {	
	require_once 'finance/class/adapter/ProvitShareAdapter.php';
	require_once 'finance/class/adapter/RecapitulationProvitShareAdapter.php';
	loadLibrary("smis-libs-function-math");
	$service=new ServiceConsumer($db, "get_pv_bronchoscopy");
	$service->setMode(ServiceConsumer::$CLEAN_BOTH)
			->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->addData("id_karyawan", "-1");
	$content=$service->execute()->getContent();
	$adapter=new RecapitulationProvitShareAdapter($_POST['pajak']);
	$uidata=$adapter->setUseID(false)
					->getContent($content);
	$list=$uitable->setContent($uidata)
				  ->getBodyContent();
	$json['list']=$list;
	$json['pagination']="";
	$json['number']="0";
	$json['number_p']="0";
	$json['data']=$uidata;
	$json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
	$json['nomor']="RBRO-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
	$ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
	$to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	
	$json['json']=json_encode($uidata);
	$json['status']=1;
	
	$pack=new ResponsePackage();
	$pack->setContent($json);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$uitable->addModal("nama_accounting", "chooser-rekap_pv_bronchoscopy-rekap_pv_bronchoscopy_accounting-Accounting", "Accounting", "")
		->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("pajak", "text", "Pajak (%) ", $pajak);
$action=new Button("","","");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("rekap_pv_bronchoscopy.view()");
$print_acc=new Button("","","Accounting");
$print_acc->setClass("btn-primary")
	  	  ->setIsButton(Button::$ICONIC_TEXT)
	  	  ->setIcon("fa fa-print")
	  	  ->setAction("rekap_pv_bronchoscopy.print()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action)
		  ->addButton($print_acc);
$form=$uitable
	  ->getModal()
	  ->setTitle("Tindakan")
	  ->getForm()
	  ->addElement("",$btn_froup);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_rekap_pv_bronchoscopy'>".$uitable->getHtml()."</div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">
	var rekap_pv_bronchoscopy;
	var rekap_pv_bronchoscopy_karyawan;
	var rekap_pv_bronchoscopy_accounting;
	var rekap_pv_bronchoscopy_data;
	$(document).ready(function(){
		$('.mydate').datepicker();
		rekap_pv_bronchoscopy=new TableAction("rekap_pv_bronchoscopy","finance","rekap_pv_bronchoscopy",new Array());

		rekap_pv_bronchoscopy_accounting=new TableAction("rekap_pv_bronchoscopy_accounting","finance","rekap_pv_bronchoscopy",new Array());
		rekap_pv_bronchoscopy_accounting.setSuperCommand("rekap_pv_bronchoscopy_accounting");
		rekap_pv_bronchoscopy_accounting.selected=function(json){
			$("#rekap_pv_bronchoscopy_nama_accounting").val(json.nama);
		};

		rekap_pv_bronchoscopy.addRegulerData=function(data){
			data['dari']=$("#rekap_pv_bronchoscopy_dari").val();
			data['sampai']=$("#rekap_pv_bronchoscopy_sampai").val();
			data['pajak']=$("#rekap_pv_bronchoscopy_pajak").val();
			$("#dari_table_rekap_pv_bronchoscopy").html(getFormattedDate(data['dari']));
			$("#sampai_table_rekap_pv_bronchoscopy").html(getFormattedDate(data['sampai']));
			$("#pajak_table_rekap_pv_bronchoscopy").html(data['pajak']+"%");			
			return data;
		};

		rekap_pv_bronchoscopy.afterview=function(json){
			if(json!=null){
				$("#kode_table_rekap_pv_bronchoscopy").html(json.nomor);
				$("#waktu_table_rekap_pv_bronchoscopy").html(json.waktu);
				rekap_pv_bronchoscopy_data=json;
			}
		};


		
		
		
				
	});
</script>