<?php
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-libs-hrd/EmployeeResponder.php';	
	require_once 'finance/class/adapter/PerawatRecapitulationProfitShareAdapter.php';
	loadLibrary("smis-libs-function-math");
	global $db;
	global $user;

	$form = new Form("pvrtk_form", "", "");
	$id_akuntan = new Hidden("pvrtk_id_akuntan", "pvrtk_id_akuntan", "");
	$form->addElement("", $id_akuntan);
	$akuntan_button = new Button("", "", "Pilih");
	$akuntan_button->setClass("btn-info");
	$akuntan_button->setAction("rekap_pv_tindakan_keperawatan_akuntan.chooser('rekap_pv_tindakan_keperawatan_akuntan', 'rekap_pv_tindakan_keperawatan_akuntan_button', 'rekap_pv_tindakan_keperawatan_akuntan', rekap_pv_tindakan_keperawatan_akuntan)");
	$akuntan_button->setIcon("icon-white icon-list-alt");
	$akuntan_button->setIsButton(Button::$ICONIC);
	$akuntan_button->setAtribute("id='akuntan_pvrtk_browse'");
	$akuntan_text = new Text("pvrtk_nama_akuntan", "pvrtk_nama_akuntan", "");
	$akuntan_text->setClass("smis-one-option-input");
	$akuntan_text->setAtribute("disabled='disabled'");
	$akuntan_input_group = new InputGroup("");
	$akuntan_input_group->addComponent($akuntan_text);
	$akuntan_input_group->addComponent($akuntan_button);
	$form->addElement("Akuntan", $akuntan_input_group);
    
	$tanggal_from_text = new Text("pvrtk_dari", "pvrtk_dari", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Dari", $tanggal_from_text);
    
	$tanggal_to_text = new Text("pvrtk_sampai", "pvrtk_sampai", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Sampai", $tanggal_to_text);
    
	$pjkserv = new ServiceConsumer($db, "get_pajak");
	$pjkserv->execute();
	$pajak = $pjkserv->getContent();
	$pajak_text = new Text("pvrtk_pajak", "pvrtk_pajak", $pajak);
	$form->addElement("Pajak (%)", $pajak_text);
    
    $option=new OptionBuilder();
    $option->add("","both",1);
    $option->add("Individu","individu",0);
    $option->add("Communal","communal",0);
    $select=new Select("pvrtk_type","",$option->getContent());
    $form->addElement("Jenis", $select);
    
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("pvrtk.view()");
	$print_button = new Button("", "", "Akuntansi");
	$print_button->setClass("btn-primary");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC_TEXT);
    $print_button->setAction("pvrtk.print_akuntan()");
    
    $excel = new Button("","","Excel");
    $excel ->setIsButton(Button::$ICONIC_TEXT);
    $excel ->setIcon("fa fa-file-excel");
    $excel ->setClass("btn btn-primary");
    $excel ->setAction("pvrtk.excel()");

	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
    $form->addElement("", $btn_group);
    $form->addElement("", $excel);
    
	$table = new Table(array("Karyawan", "Biaya Awal", "Bagian Karyawan", "Bagian RS", "Pajak", "Dana Pengembangan"),"",null,true);
	$table->setName("pvrtk_table");
	$table->setAction(false);
	$table->setHeaderVisible(false);
	$table->setFooterVisible(false);
	$header_html = "
		<tr>
			<th colspan='6'>REKAP RENUMERASI TINDAKAN KEPERAWATAN</th>
		</tr>
		<tr>
			<th colspan='1'>Nomor</th>
			<th colspan='1' id='info_pvrtk_nomor'></th>
			<th colspan='1'>Waktu</th>
			<th colspan='3' id='info_pvrtk_waktu'></th>
		</tr>
		<tr>
			<th colspan='1'>Dari</th>
			<th colspan='1' id='info_pvrtk_dari'></th>
			<th colspan='1'>Sampai</th>
			<th colspan='3' id='info_pvrtk_sampai'></th>
		</tr>
		<tr>
			<th colspan='1'>Pajak</th>
			<th colspan='5' id='info_pvrtk_pajak'></th>
		</tr>
		<tr>
			<th colspan='1'>Karyawan</th>
			<th colspan='1'>Biaya Awal</th>
			<th colspan='1'>Bagian Karyawan</th>
			<th colspan='1'>Bagian RS</th>
			<th colspan='1'>Pajak</th>
			<th colspan='1'>Dana Pengembangan</th>
		</tr>
	";
	$table->addHeader("after", $header_html);
	
	if(isset($_POST['super_command']) && $_POST['super_command']!="") {
		$head = array('Nama','Jabatan',"NIP");
		$dkadapter = new SimpleAdapter();
		$dkadapter->add("Jabatan","nama_jabatan");
		$dkadapter->add("Nama","nama");
		$dkadapter->add("NIP","nip");
	
		$actable = new Table($head);
		$actable->setName("rekap_pv_tindakan_keperawatan_akuntan");
		$actable->setModel(Table::$SELECT);
		$accounting = new EmployeeResponder($db,$actable,$dkadapter,"aku");
	
		$super = new SuperCommand();
		$super->addResponder("rekap_pv_tindakan_keperawatan_akuntan", $accounting);
		$init = $super->initialize();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	if (isset($_POST['command']) &&  ( $_POST['command'] == "list" || $_POST['command'] == "excel" ) ) {
		$service    = new ServiceConsumer($db, "get_pv_tindakan_keperawatan");
		$service    ->setMode(ServiceConsumer::$CLEAN_BOTH)
				    ->addData("dari", $_POST['dari'])
				    ->addData("sampai", $_POST['sampai'])
                    ->addData("type", $_POST['type'])
				    ->addData("id_karyawan", "-1");
		$content    = $service->execute()->getContent();
        $adapter    = new PerawatRecapitulationProfitShareAdapter($_POST['pajak']);
        $adapter    ->setExcel($_POST['command']=="excel");
		$uidata     = $adapter  ->setUseID(false)
                                ->getContent($content);
        if($_POST['command']=="list"){
            $list = $table	->setContent($uidata) ->getBodyContent();
            $json['list']       = $list;
            $json['pagination'] = "";
            $json['number']     = "0";
            $json['number_p']   = "0";
            $json['data']       = $uidata;
            $json['waktu']      = ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
            $json['nomor']      = "RPWT-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
            $ft                 = ArrayAdapter::format("date d M Y", $_POST['dari']);
            $to                 = ArrayAdapter::format("date d M Y", $_POST['sampai']);	
            $json['json']       = json_encode($uidata);
            $json['status']     = 1;
            $pack               = new ResponsePackage();
            $pack->setContent($json);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            echo json_encode($pack->getPackage());
        }else{
            $column      = array();
            $column['A'] = "Karyawan";
            $column['B'] = "Biaya Awal";
            $column['C'] = "Bagian Karyawan";
            $column['D'] = "Bagian RS";
            $column['E'] = "Pajak";
            $column['F'] = "Dana Pengembangan";
            
            require_once "finance/class/ExcelRekapitulasi.php";
            $excel = new ExcelRekapitulasi("REKAP RENUMERASI TINDAKAN KEPERAWATAN","RPWT",$_POST['dari'],$_POST['sampai'],$_POST['pajak'],$column);
            $excel->createExcel($uidata);

        }
		return;
	}
	
	echo $form->getHtml();
	echo "<div class='clear'></div>";
	echo "<div id='result_pvrtk'>".$table->getHtml()."</div>";
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("framework/smis/js/table_action.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");	
?>
<script type="text/javascript">
	function PVRTKAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PVRTKAction.prototype.constructor = PVRTKAction;
	PVRTKAction.prototype = new TableAction();
	PVRTKAction.prototype.addRegulerData = function(data) {
		data['dari'] = $("#pvrtk_dari").val();
		data['sampai'] = $("#pvrtk_sampai").val();
		data['pajak'] = $("#pvrtk_pajak").val();
        data['type'] = $("#pvrtk_type").val();


		$("#info_pvrtk_dari").html(getFormattedDate(data['dari']));
		$("#info_pvrtk_sampai").html(getFormattedDate(data['sampai']));
		$("#info_pvrtk_pajak").html(data['pajak'] + "%");			
		return data;
	};
	PVRTKAction.prototype.afterview = function(json) {
		if (json != null) {			
			$("#info_pvrtk_nomor").html(json.nomor);
			$("#info_pvrtk_waktu").html(json.waktu);
			$("#pvrtk_table_list").html(json.list);
			pvrtk_data = json;
		}
	};
	PVRTKAction.prototype.print_akuntan = function() {
		smis_print($("#result_pvrtk").html());
	};

	function PVRTKAkuntanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PVRTKAkuntanAction.prototype.constructor = PVRTKAkuntanAction;
	PVRTKAkuntanAction.prototype = new TableAction();
	PVRTKAkuntanAction.prototype.selected = function(json) {
		$("#pvrtk_id_akuntan").val(json.id);
		$("#pvrtk_nama_akuntan").val(json.nama);
	};
	
	var pvrtk;
	var pvrtk_data;
	var rekap_pv_tindakan_keperawatan_akuntan;
	$(document).ready(function() {
		$('.mydate').datepicker();
		rekap_pv_tindakan_keperawatan_akuntan = new PVRTKAkuntanAction(
			"rekap_pv_tindakan_keperawatan_akuntan",
			"finance",
			"rekap_pv_tindakan_keperawatan",
			new Array()
		);
		rekap_pv_tindakan_keperawatan_akuntan.setSuperCommand("rekap_pv_tindakan_keperawatan_akuntan");
		pvrtk = new PVRTKAction(
			"pvrtk",
			"finance",
			"rekap_pv_tindakan_keperawatan",
			new Array()
		);
	});
</script>
