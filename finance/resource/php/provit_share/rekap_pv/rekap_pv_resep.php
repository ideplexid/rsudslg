<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="proceed") {
	$service=new ServiceConsumer($db, "pv_resep_atomic",NULL,$_POST['entity']);
	$service->setMode(ServiceConsumer::$SINGLE_MODE)
			->addData("id", $_POST['id'])
			->addData("retsell", $_POST['retsell']);
	$content=$service->execute()->getContent();
	
	$up=array();
	$up['id_dokter']=$content['id_dokter'];
	$up['id_item']=$_POST['id'];
	$up['nama_dokter']=$content['nama_dokter'];
	$up['noresep']=$content['no_resep'];
	$up['harga']=$content['harga'];
	$up['waktu']=$content['waktu'];
	$up['tipe']=($_POST['retsell']=="sell"?1:0);
	$up['ruang']=ArrayAdapter::format("unslug", $_POST['entity']);
	$up['prop']="";
	$up['bagian_dokter']=$content['bagian_dokter'];
	
	$id=array();
	$id['id_item']=$_POST['id'];
	$id['tipe']=$_POST['retsell'];
	$id['noresep']=$content['no_resep'];
	$id['ruang']=$up['ruang'];
	
	$dbtable=new DBTable($db,"smis_fnc_resep");
	$content=$dbtable->insertOrUpdate($up, $id);
	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($content);
	echo json_encode($res->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="get_total") {
	$dbtable=new DBTable($db,"smis_fnc_resep");
	$dbtable->truncate();
	$service=new ServiceConsumer($db, "get_pv_resep_total");
	$service->setMode(ServiceConsumer::$MULTIPLE_MODE)
			->addData("from", $_POST['dari'])
			->addData("to", $_POST['sampai'])
			->addData("id_dokter", "-1");
	$content=$service->execute()->getContent();
	$hasil=array();
	foreach($content as $server=>$entity){
		foreach ($entity as $name=>$c){
			$hasil[$name]=$c;
		}		
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($hasil);
	echo json_encode($res->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']!="") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");

	$actable=new Table($head);
	$actable->setName("rekap_pv_resep_accounting");
	$actable->setModel(Table::$SELECT);
	$accounting=new EmployeeResponder($db,$actable,$dkadapter,"finance");

	$super=new SuperCommand();
	$super->addResponder("rekap_pv_resep_accounting",$accounting);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}



$header=array("Nama","Total","Bagian Dokter","Bagian RS");
$uitable=new Table($header);
$head00="<tr>
		<th colspan='30' id='surat_bukti'>REKAP RESEP DOKTER<font id='untuk_table_rekap_pv_resep'></font></th>
		</tr>";
$head0="<tr>
		<th>Nomor</th>
		<th  id='kode_table_rekap_pv_resep'></th>
		<th>Waktu</th>
		<th colspan='3' id='waktu_table_rekap_pv_resep'></th>
		</tr>";
$head2="<tr>
		<th >Dari</th>
		<th id='dari_table_rekap_pv_resep'></th>
		<th >Sampai</th>
		<th colspan='3'  id='sampai_table_rekap_pv_resep'></th>
		</tr>";

$head3="<tr>
		<th >Pajak</th>
		<th id='pajak_table_rekap_pv_resep' colspan='20'></th>
		</tr>";

$uitable->addHeader("before", $head00)
		->addHeader("before", $head0)
		->addHeader("before", $head2)
		->addHeader("before", $head3)
		->setName("rekap_pv_resep")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']== "list") {
	require_once 'finance/class/adapter/RecapitulationResepAdapter.php';	
	$qv="SELECT SUM(harga) as harga, SUM(bagian_dokter) as bagian_dokter, id_dokter, nama_dokter FROM smis_fnc_resep ";
	$qc="SELECT count(SUM(harga)) as total FROM smis_fnc_resep ";
	$dbtable=new DBTable($db, "smis_fnc_resep");
	$dbtable->addCustomKriteria(NULL, " waktu>='".$_POST['dari']."' ");
	$dbtable->addCustomKriteria(NULL, " waktu<'".$_POST['sampai']."' ");
	$dbtable->setGroupBy(true, " id_dokter, nama_dokter ");
	$dbtable->setPreferredQuery(true, $qv, $qc);
	$dbtable->setUseWhereforView(true);
	$dbtable->setShowAll(true);
	
	$simple=new RecapitulationResepAdapter();
	$simple->add("Nama", "nama_dokter");
	$simple->add("Total", "harga","money Rp.");
	$simple->add("Bagian Dokter", "bagian_dokter","money Rp.");
	$dbres=new DBResponder($dbtable, $uitable, $simple);
	echo json_encode($dbres->command($_POST['command']));
	return;
}

$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$uitable->addModal("nama_accounting", "chooser-rekap_pv_resep-rekap_pv_resep_accounting-Accounting", "Accounting", "")
		->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("pajak", "text", "Pajak (%) ", $pajak);
$action=new Button("","","");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("rekap_pv_resep.getTotal()");
$print_acc=new Button("","","Accounting");
$print_acc->setClass("btn-primary")
	  	  ->setIsButton(Button::$ICONIC_TEXT)
	  	  ->setIcon("fa fa-print")
	  	  ->setAction("rekap_pv_resep.print()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action)
		  ->addButton($print_acc);
$form=$uitable
	  ->getModal()
	  ->setTitle("Tindakan")
	  ->getForm()
	  ->addElement("",$btn_froup);

$entity=new LoadingBar("rekap_resep_bar", "Progress");
$entity->setHidden(false);
$modal=new Modal("rekap_resep_loder_modal", "", "Proses Rekap");
$modal->addBody("Ruangan", $entity);

echo $modal->getHtml();
echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_rekap_pv_resep'>".$uitable->getHtml()."</div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var rekap_pv_resep;
	var rekap_pv_resep_karyawan;
	var rekap_pv_resep_accounting;
	var rekap_pv_resep_data;	
	$(document).ready(function(){
		$('.mydate').datepicker();
		rekap_pv_resep=new TableAction("rekap_pv_resep","finance","rekap_pv_resep",new Array());
		rekap_pv_resep_accounting=new TableAction("rekap_pv_resep_accounting","finance","rekap_pv_resep",new Array());
		rekap_pv_resep_accounting.setSuperCommand("rekap_pv_resep_accounting");
		rekap_pv_resep_accounting.selected=function(json){
			$("#rekap_pv_resep_nama_accounting").val(json.nama);
		};

		rekap_pv_resep.getTotal=function(){
			var d=this.getRegulerData();
			d['super_command']="get_total";
			showLoading();
			$.post("",d,function(res){
				dismissLoading();
				var json=getContent(res);
				if(json!=null){
					$("#rekap_resep_loder_modal").smodal('show');
					rekap_pv_resep.loading_list(json);
				}							
			});
		};

		rekap_pv_resep.loading_list=function(list){
			$("#rekap_resep_entity").sload("true","Preparing The List ",0);
			var allist=new Array();
			$.each(list,function(index, value) {
				var lreturn=value['lreturn'];
				var lsell=value['lsell'];
				var rtotal=Number(value['return']);
				var stotal=Number(value['sell']);
				
				$.each(lsell,function(idx,value){
					var dtl=new Array();
					dtl['entity']=index;
					dtl['retsell']="sell";
					dtl['id']=value;
					allist.push(dtl);			
				});
				$.each(lreturn,function(idx,value){
					var dtl=new Array();
					dtl['entity']=index;
					dtl['retsell']="ret";
					dtl['id']=value;
					allist.push(dtl);			
				});
			});	
			var tt=allist.length;
			rekap_pv_resep.proceed(allist,0,tt);
		};

		rekap_pv_resep.proceed=function(the_list,current_recipe,total_recipe){
			if(current_recipe>=total_recipe){
				rekap_pv_resep.view();
				$("#rekap_resep_loder_modal").smodal('hide');
				return;
			}
			$("#rekap_resep_bar").sload("true","Processing...[ "+current_recipe+" / "+total_recipe+"]",current_recipe*100/total_recipe);
			var d=the_list[current_recipe];
			var dt=this.getRegulerData();
			dt['entity']=d['entity'];
			dt['retsell']=d['retsell'];
			dt['id']=d['id'];
			dt['super_command']="proceed";
			$.post("",dt,function(res){
				current_recipe++;
				setTimeout(function (){rekap_pv_resep.proceed(the_list,current_recipe,total_recipe);},20);
			});
		};

		rekap_pv_resep.addRegulerData=function(data){
			data['dari']=$("#rekap_pv_resep_dari").val();
			data['sampai']=$("#rekap_pv_resep_sampai").val();
			data['pajak']=$("#rekap_pv_resep_pajak").val();
			$("#dari_table_rekap_pv_resep").html(getFormattedDate(data['dari']));
			$("#sampai_table_rekap_pv_resep").html(getFormattedDate(data['sampai']));
			$("#pajak_table_rekap_pv_resep").html(data['pajak']+"%");			
			return data;
		};

		/*rekap_pv_resep.afterview=function(json){
			if(json!=null){
				$("#kode_table_rekap_pv_resep").html(json.nomor);
				$("#waktu_table_rekap_pv_resep").html(json.waktu);
				rekap_pv_resep_data=json;
			}
		};*/
		
	});
</script>