<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';


if(isset($_POST['super_command']) && $_POST['super_command']=="rekap_pv_radiology_accounting") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");

	$actable=new Table($head);
	$actable->setName("rekap_pv_radiology_accounting");
	$actable->setModel(Table::$SELECT);
	$accounting=new EmployeeResponder($db,$actable,$dkadapter,"finance");

	$super=new SuperCommand();
	$super->addResponder("rekap_pv_radiology_accounting",$accounting);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$service=new ServiceConsumer($db, "get_total_pv_radiology",NULL,"radiology");
	$service->setMode(ServiceConsumer::$SINGLE_MODE);
	$service->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->addData("id_karyawan", "-1");
	$query="update smis_fnc_radiology set prop='del' where waktu>='".$_POST['dari']."' AND waktu<='".$_POST['sampai']."' ";
	$db->query($query);
	
	$content=$service->execute()->getContent();
	$pack=new ResponsePackage();
	$pack->setContent($content);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$header=array("Dokter","Biaya Awal","Bagian Dokter","Bagian RS");
$uitable=new Table($header);
if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$service=new ServiceConsumer($db, "get_pv_radiology");
	$service->setMode(ServiceConsumer::$CLEAN_BOTH)
			->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->addData("limit_start", $_POST['limit_start'])
			->addData("limit_length", 1)
			->addData("id_karyawan", "-1");
	$list=$service->execute()->getContent();
	$dbtable=new DBTable($db, "smis_fnc_radiology");
	foreach($list as $x){
		$one['id_radiology']=$x['id'];
		$one['id_dokter']=$x['id_karyawan'];
		$one['dokter']=$x['karyawan'];
		$one['waktu']=$x['waktu_asli'];
		$one['tagihan']=$x['asli'];
		$one['bonus']=$x['nilai'];
		$one['persen']=$x['percentage'];
		$one['keterangan']=$x['sebagai']." - ".$x['keterangan']." - ".$x['pasien'];
		$dbtable->insert($one);
	}
	$pack=new ResponsePackage();
	$pack->setContent(count($list));
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}



$head00="<tr>
		<th colspan='30' id='surat_bukti'>REKAP RADIOLOGY DOKTER<font id='untuk_table_rekap_pv_radiology'></font></th>
		</tr>";
$head0="<tr>
		<th>Nomor</th>
		<th  id='kode_table_rekap_pv_radiology'></th>
		<th>Waktu</th>
		<th colspan='3' id='waktu_table_rekap_pv_radiology'></th>
		</tr>";
$head2="<tr>
		<th >Dari</th>
		<th id='dari_table_rekap_pv_radiology'></th>
		<th >Sampai</th>
		<th colspan='3'  id='sampai_table_rekap_pv_radiology'></th>
		</tr>";

$head3="<tr>
		<th >Pajak</th>
		<th id='pajak_table_rekap_pv_radiology' colspan='20'></th>
		</tr>";

$uitable->addHeader("before", $head00)
		->addHeader("before", $head0)
		->addHeader("before", $head2)
		->addHeader("before", $head3)
		->setName("rekap_pv_radiology")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && ( $_POST['command'] == "list" || $_POST['command'] == "excel" ) ) {
    require_once 'finance/class/adapter/RecapitulationProvitShareRadiologyAdapter.php';
	loadLibrary("smis-libs-function-math");
	$dbtable=new DBTable($db, "smis_fnc_rekap_radiology");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$dbtable->addCustomKriteria(NULL, "waktu >= '".$_POST['dari']."'");
	$dbtable->addCustomKriteria(NULL, "waktu < '".$_POST['sampai']."'");
	$dbtable->setShowAll(true);
	$data=$dbtable->view("", "0");
	
	$adapter=new RecapitulationProvitShareRadiologyAdapter($_POST['pajak']);
    $adapter    ->setExcel($_POST['command']=="excel");

    $uidata=$adapter->setUseID(false)
                    ->getContent($data['data']);
    if($_POST['command']=="list" ){
        $list=$uitable->setContent($uidata)
				  ->getBodyContent();
        $json['list']=$list;
        $json['pagination']="";
        $json['number']="0";
        $json['number_p']="0";
        $json['data']=$uidata;
        $json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
        $json['nomor']="RRAD-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
        $ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
        $to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	
        $json['json']=json_encode($uidata);
        $json['status']=1;
        
        $pack=new ResponsePackage();
        $pack->setContent($json);
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($pack->getPackage());
    }else{
        $column      = array();
        $column['A'] = "Dokter";
        $column['B'] = "Biaya Awal";
        $column['C'] = "Bagian Dokter";
        $column['D'] = "Bagian RS";
        $column['E'] = "Dana Pengembangan";
        
        require_once "finance/class/ExcelRekapitulasi.php";
        $excel = new ExcelRekapitulasi("REKAP RENUMERASI RADIOLOGY","RRAD",$_POST['dari'],$_POST['sampai'],$_POST['pajak'],$column);
        $excel->createExcel($uidata);
    }
	
	return;
}

$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$uitable->addModal("nama_accounting", "chooser-rekap_pv_radiology-rekap_pv_radiology_accounting-Accounting", "Accounting", "")
		->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("pajak", "text", "Pajak (%) ", $pajak);
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("rekap_pv_radiology.rekaptotal()");
$print_acc=new Button("","","Accounting");
$print_acc->setClass("btn-primary")
	  	  ->setIsButton(Button::$ICONIC_TEXT)
	  	  ->setIcon("fa fa-print")
	  	  ->setAction("rekap_pv_radiology.print()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action)
          ->addButton($print_acc);

$excel = new Button("","","Excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setIcon("fa fa-file-excel");
$excel ->setClass("btn btn-primary");
$excel ->setAction("rekap_pv_radiology.excel()");          

$form=$uitable
	  ->getModal()
	  ->setTitle("Radiology")
	  ->getForm()
      ->addElement("",$btn_froup)
      ->addElement("",$excel);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("rekap_pv_radiology.batal()");

$load=new LoadingBar("rekap_rad_bar", "");
$modal=new Modal("rekap_rad_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_rekap_pv_radiology'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var rekap_pv_radiology;
	var rekap_pv_radiology_karyawan;
	var rekap_pv_radiology_accounting;
	var rekap_pv_radiology_data;
	var IS_REKAP_PV_RADIOLOGY_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		rekap_pv_radiology=new TableAction("rekap_pv_radiology","finance","rekap_pv_radiology",new Array());

		rekap_pv_radiology_accounting=new TableAction("rekap_pv_radiology_accounting","finance","rekap_pv_radiology",new Array());
		rekap_pv_radiology_accounting.setSuperCommand("rekap_pv_radiology_accounting");
		rekap_pv_radiology_accounting.selected=function(json){
			$("#rekap_pv_radiology_nama_accounting").val(json.nama);
		};

		rekap_pv_radiology.addRegulerData=function(data){
			data['dari']=$("#rekap_pv_radiology_dari").val();
			data['sampai']=$("#rekap_pv_radiology_sampai").val();
			data['pajak']=$("#rekap_pv_radiology_pajak").val();
			$("#dari_table_rekap_pv_radiology").html(getFormattedDate(data['dari']));
			$("#sampai_table_rekap_pv_radiology").html(getFormattedDate(data['sampai']));
			$("#pajak_table_rekap_pv_radiology").html(data['pajak']+"%");			
			return data;
		};

		rekap_pv_radiology.batal=function(){
			IS_REKAP_PV_RADIOLOGY_RUNNING=false;
			$("#rekap_rad_modal").modal("hide");
		};
		
		rekap_pv_radiology.afterview=function(json){
			if(json!=null){
				$("#kode_table_rekap_pv_radiology").html(json.nomor);
				$("#waktu_table_rekap_pv_radiology").html(json.waktu);
				rekap_pv_radiology_data=json;
			}
		};

		rekap_pv_radiology.rekaptotal=function(){
			if(IS_REKAP_PV_RADIOLOGY_RUNNING) return;
			$("#rekap_rad_bar").sload("true","Fetching total data",0);
			$("#rekap_rad_modal").modal("show");
			IS_REKAP_PV_RADIOLOGY_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total>0) {
					rekap_pv_radiology.rekaploop(0,total);
				} else {
					$("#rekap_rad_modal").modal("hide");
					IS_REKAP_PV_RADIOLOGY_RUNNING=false;
				}
			});
		};

		rekap_pv_radiology.rekaploop=function(current,total){
			$("#rekap_rad_bar").sload("true","Calculation... [ "+current+" / "+total+" ] ",(current*100/total));
			if(current>=total || !IS_REKAP_PV_RADIOLOGY_RUNNING) {
				$("#rekap_rad_modal").modal("hide");
				rekap_pv_radiology.view();
				IS_REKAP_PV_RADIOLOGY_RUNNING=false;
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['limit_start']=current;			
			$.post("",d,function(res){
				setTimeout(function(){rekap_pv_radiology.rekaploop(++current,total)},300);
			});
		};
				
	});
</script>