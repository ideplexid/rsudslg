<?php

/**
 * masih belum jadi nunggu buatkan service */

global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once "finance/class/responder/PendapatanKamarMayatResponder.php";
require_once 'finance/class/MapRuangan.php';

$header=array ("Tanggal","Ruang","No.Reg","NRM","Nama","Jenis Layanan","Biaya","Operator","Cara Bayar");
$plafon=new MasterSlaveServiceTemplate($db, "get_kamar_mayat", "finance", "pendapatan_kamar_mayat");
$responder = new PendapatanKamarMayatResponder($db,$plafon->getUItable(),$plafon->getAdapter(),"get_kamar_mayat");
$plafon -> setDBresponder($responder);

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $_POST['max'] = 1000000000000;
    $responder->setTitle("Pendapatan Kamar Mayat");
    $responder->setFileName("Pendapatan Kamar Mayat");
    $responder->addResetMoney("G","Biaya","biaya");

    $responder->setSummary("F",array("G"));
    $responder->addColumn("A","Tanggal");
    $responder->addColumn("B","Ruang");
    $responder->addColumn("C","No.Reg");
    $responder->addColumn("D","NRM");
    $responder->addColumn("E","Nama");
    $responder->addColumn("F","Jenis Layanan");
    $responder->addColumn("G","Biaya");
    $responder->addColumn("H","Operator");
    $responder->addColumn("I","Cara Bayar");
}

$plafon->setDateEnable(true);
if( isset($_POST['command']) && $_POST['command']=="list" ){	
	$plafon->setEntity("kamar_mayat");
	$plafon->getServiceResponder()->addData("sampai",$_POST['sampai']);	
	$plafon->getServiceResponder()->addData("dari",$_POST['dari']);
}

require_once "finance/class/adapter/PendapatanKamarMayatAdapter.php";
$adapt  = new PendapatanKamarMayatAdapter();
$adapt	->add("Tanggal","waktu","date d M Y H:i")
        ->add("No.Reg","noreg_pasien")
        ->add("NRM","nrm_pasien")
        ->add("Nama","nama_pasien")
        ->add("Jenis Layanan","layanan")
        ->add("Biaya","biaya","money Rp.")
        ->add("Operator","petugas")
        ->add("Cara Bayar","carabayar","unslug");
$adapt	->addSummary("Biaya", "biaya","money Rp.");

$adapt->addFixValue("Jenis Layanan","<strong>Total</strong>");
$plafon->setAdapter($adapt);
$plafon ->getUItable()
		->setAction(false)
		->setHeader($header);


if(!isset($_POST['command'])){	
	$plafon ->getUItable()
			->addModal("dari", "date", "Dari", "","n")
			->addModal("sampai", "date", "Sampai", "","n");
   
 $btn = $plafon ->getUItable()
                ->setAddButtonEnable(false)
                ->setPrintButtonEnable(false)
                ->getHeaderButton();

$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_kamar_mayat.excel()");

$plafon ->getForm(true,"")
        ->addElement("", $btn)
        ->addElement("", $excel);

	$plafon->getUItable()->clearContent();
	$plafon->addViewData("dari","dari");
	$plafon->addViewData("sampai","sampai");
}
$plafon->initialize();