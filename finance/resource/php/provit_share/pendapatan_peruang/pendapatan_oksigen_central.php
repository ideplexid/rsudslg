<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once "finance/class/responder/PendapatanOksigenCentralServiceResponder.php";


$list=new ServiceConsumer($db, "get_oksigen_report_grup");
$list->setMode(ServiceConsumer::$CLEAN_BOTH_JOIN_SINGLE_ARRAY);
$list->execute();
$list_name=$list->getContent();
//echo json_encode($list_name);
$ruang   = new OptionBuilder();
$ruang->add("","",1);
$already = array();
foreach($list_name as $x){
    if(!in_array($x,$already))
        $ruang->addSingle($x);
}
$ruangan=$ruang->getContent();


$dkadapter = new SimpleAdapter ();
$dkadapter  ->add ( "Jabatan", "nama_jabatan" )
            ->add ( "Nama", "nama" )
            ->add ( "NIP", "nip" );
$header    = array ('Nama','Jabatan',"NIP" );
$dktable   = new Table ( $header);
$dktable    ->setName ( "perawat_oksigen_central" )
            ->setModel ( Table::$SELECT );
$perawat   = new EmployeeResponder ( $db, $dktable, $dkadapter, "perawat" );
$oksigen_central = new MasterSlaveServiceTemplate($db, "get_oksigen_central_grup", "finance", "pendapatan_oksigen_central");
$responder = new PendapatanOksigenCentralServiceResponder($db,$oksigen_central->getUItable(),$oksigen_central->getAdapter(),"get_oksigen_central_grup");
$oksigen_central -> setDBresponder($responder);
$oksigen_central ->getServiceResponder()->setMode(ServiceConsumer::$CLEAN_BOTH);
$oksigen_central ->setDateTimeEnable(true);
$header = array ("No.","Ruangan","Mulai","Pasien",'NRM',"No. Reg","H. Jam","H. Menit","Jam","Menit","Skala",'H. Total');
if(isset($_POST['dari']) && isset($_POST['sampai'])){    
	$oksigen_central ->getServiceResponder()
                     ->addData("dari",$_POST['dari'])
                     ->addData("sampai",$_POST['sampai']);
}
$btn = $oksigen_central->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
                       ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
                       ->addNoClear("dari")
                       ->addNoClear("sampai")
                       ->getUItable()
                       ->setAddButtonEnable(false)
                       ->getHeaderButton();
$oksigen_central ->getUItable()
                 ->setAction(false)
                 ->addModal("dari", "datetime", "Dari", "")
                 ->addModal("sampai", "datetime", "Sampai", "")
                 ->addModal("ruangan", "select", "Ruangan", $ruangan);

$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_oksigen_central.excel()");

$oksigen_central ->getForm(true,"")
                 ->addElement("", $btn)
                 ->addElement("", $excel);
$oksigen_central ->getUItable()
                 ->setHeader($header)
                 ->setFooterVisible(false);

require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
require_once "finance/class/MapRuangan.php";
$summary=new TindakanPerRuangAdapter();
$summary    ->addSummary("H. Total", "harga","money Rp.");
$summary    ->addFixValue("NRM","<strong>Total</strong>");

$oksigen_central ->setAdapter($summary)
                 ->getAdapter()
                 ->setUseNumber(true, "No.","back.")
                 ->add("Mulai", "waktu","date d M Y H:i")
                 ->add("H. Jam", "harga_jam","money Rp.")
                 ->add("H. Menit", "harga_menit","money Rp.")
                 ->add("Jam", "jam")
                 ->add("Menit", "menit")
                 ->add("Skala", "skala")
                 ->add("H. Total", "harga","money Rp.")
                 ->add("Pasien", "nama_pasien")
                 ->add("NRM", "nrm_pasien","only-digit8")
                 ->add("No. Reg", "noreg_pasien","only-digit8");
$oksigen_central ->addViewData("dari", "dari")
                 ->addViewData("sampai", "sampai")
                 ->addViewData("perawat", "perawat")
                 ->addViewData("ruangan", "ruangan");
		
$oksigen_central ->initialize();