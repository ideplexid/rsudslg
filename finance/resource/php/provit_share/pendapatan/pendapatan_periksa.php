<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

require_once 'finance/class/MapRuangan.php';
$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"all","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "dokter_periksa" );
$dktable->setModel ( Table::$SELECT );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );


$periksa=new MasterSlaveServiceTemplate($db, "pendapatan_periksa", "finance", "pendapatan_periksa");
$periksa->setDateTimeEnable(true);
$responder = new PendapatanPerRuangPerDokterResponder($db,$periksa->getUItable(),$periksa->getAdapter(),"pendapatan_periksa");
$periksa -> setDBresponder($responder);
$periksa->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);

$header=array ("No.","Waktu",'Dokter',"Pasien",'NRM','Harga',"Cara Bayar","Ruangan");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$periksa->getServiceResponder()->addData("dari",$_POST['dari']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$periksa->getServiceResponder()->addData("sampai",$_POST['sampai']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$periksa->setEntity($_POST['ruangan']);
}

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $responder->setTitle("Pendapatan Periksa Dokter");
    $responder->setFileName("Pendapatan Periksa Dokter");
    $responder->addResetMoney("F","Harga","harga");
    $responder->setSummary("E",array("F"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Waktu");
    $responder->addColumn("C","Dokter");
    $responder->addColumn("D","Pasien");
    $responder->addColumn("E","NRM");
    $responder->addColumn("F","Harga");
    $responder->addColumn("G","Cara Bayar");
    $responder->addColumn("H","Ruangan");
}

$btn=$periksa->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$periksa->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
         ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
		 ->addModal("dokter", "chooser-pendapatan_periksa-dokter_periksa-Pilih Dokter", "Dokter", "")
		 ->addModal("id_dokter", "hidden", "", "");
$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_periksa.excel()");

$periksa ->getForm(true,"")
                ->addElement("", $btn)
                ->addElement("", $excel);
         
$periksa->getUItable()
		 ->setHeader($header)
         ->setFooterVisible(false);
         
require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
$summary=new TindakanPerRuangAdapter();
$summary->addSummary("Harga", "harga","money Rp.");
$summary->addFixValue("NRM","<strong>Total</strong>");
$periksa->getSuperCommand()->addResponder("dokter_periksa", $dokter);
$periksa->addSuperCommand("dokter_periksa", array());
$periksa->addSuperCommandArray("dokter_periksa", "dokter", "nama");
$periksa->addSuperCommandArray("dokter_periksa", "id_dokter", "id");
$periksa->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Waktu", "waktu","date d M Y H:i")
		 ->add("Pasien", "nama_pasien")
		 ->add("NRM", "nrm_pasien","only-digit8")
		 ->add("Dokter", "nama_dokter")
         ->add("Cara Bayar", "carabayar","unslug")
         ->add("Ruangan", "smis_entity")
		 ->add("Harga", "harga","money Rp.");
$periksa->addViewData("dari", "dari")
	   ->addViewData("sampai", "sampai")
	   ->addViewData("dokter", "dokter")
	   ->addViewData("id_dokter", "id_dokter")
       ->addViewData("carabayar", "carabayar")
	   ->addViewData("ruangan", "ruangan");
$periksa->initialize();
?>
