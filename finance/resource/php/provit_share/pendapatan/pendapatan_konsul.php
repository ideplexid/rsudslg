<?php

global $db;

require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/MapRuangan.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"all","default"=>"1");


$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");

$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "dokter_konsul" );
$dktable->setModel ( Table::$SELECT );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );


$konsul=new MasterSlaveServiceTemplate($db, "pendapatan_konsul", "finance", "pendapatan_konsul");
$konsul->setDateTimeEnable(true);
$responder = new PendapatanPerRuangPerDokterResponder($db,$konsul->getUItable(),$konsul->getAdapter(),"pendapatan_konsul");
$konsul -> setDBresponder($responder);
$konsul->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $responder->setTitle("Pendapatan Konsul Dokter");
    $responder->setFileName("Pendapatan Konsul Dokter");
    $responder->addResetMoney("F","Harga","harga");
    $responder->setSummary("E",array("F"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Waktu");
    $responder->addColumn("C","Dokter");
    $responder->addColumn("D","Pasien");
    $responder->addColumn("E","NRM");
    $responder->addColumn("F","Harga");
    $responder->addColumn("G","Cara Bayar");
    $responder->addColumn("H","Ruangan");
}

$header=array ("No.","Waktu",'Dokter',"Pasien",'NRM','Harga',"Cara Bayar","Ruangan");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$konsul->getServiceResponder()->addData("dari",$_POST['dari']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$konsul->getServiceResponder()->addData("sampai",$_POST['sampai']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$konsul->setEntity($_POST['ruangan']);
}
$btn=$konsul->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$konsul->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
         ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
		 ->addModal("dokter", "chooser-pendapatan_konsul-dokter_konsul-Pilih Dokter", "Dokter", "")
         ->addModal("id_dokter", "hidden", "", "");
         

$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_konsul.excel()");

$konsul ->getForm(true,"")
        ->addElement("", $btn)
        ->addElement("", $excel);

$konsul->getUItable()
		 ->setHeader($header)
         ->setFooterVisible(false);
         
require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
$summary=new TindakanPerRuangAdapter();
$summary->addSummary("Harga", "harga","money Rp.");
$summary->addFixValue("NRM","<strong>Total</strong>");
$konsul->getSuperCommand()->addResponder("dokter_konsul", $dokter);
$konsul->addSuperCommand("dokter_konsul", array());
$konsul->addSuperCommandArray("dokter_konsul", "dokter", "nama");
$konsul->addSuperCommandArray("dokter_konsul", "id_dokter", "id");
$konsul->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Waktu", "waktu","date d M Y H:i")
		 ->add("Pasien", "nama_pasien")
		 ->add("NRM", "nrm_pasien","only-digit8")
		 ->add("Dokter", "nama_dokter")
         ->add("Cara Bayar", "carabayar","unslug")
         ->add("Ruangan", "smis_entity")
		 ->add("Harga", "harga","money Rp.");
$konsul->addViewData("dari", "dari")
	   ->addViewData("sampai", "sampai")
	   ->addViewData("dokter", "dokter")
       ->addViewData("carabayar", "carabayar")
	   ->addViewData("id_dokter", "id_dokter")
	   ->addViewData("ruangan", "ruangan");
$konsul->initialize();