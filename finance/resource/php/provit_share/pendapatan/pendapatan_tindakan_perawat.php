<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/function/sort_tigd.php';
require_once 'finance/class/MapRuangan.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"all","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "perawat_tindakan_perawat" );
$dktable->setModel ( Table::$SELECT );
$perawat = new EmployeeResponder ( $db, $dktable, $dkadapter, "perawat" );

$urutkan=new OptionBuilder();
$urutkan->add("","","1");
$urutkan->add("Perawat","sort_tigd_perawat_name");
$urutkan->add("Tanggal","sort_tigd_waktu");

$tindakan_perawat=new MasterSlaveServiceTemplate($db, "pendapatan_tindakan_perawat", "finance", "pendapatan_tindakan_perawat");
$responder = new PendapatanPerRuangPerDokterResponder($db,$tindakan_perawat->getUItable(),$tindakan_perawat->getAdapter(),"pendapatan_tindakan_perawat");
$tindakan_perawat -> setDBresponder($responder);
$tindakan_perawat ->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $responder->setTitle("Pendapatan Perawat");
    $responder->setFileName("Pendapatan Perawat");
    $responder->addResetMoney("G","Harga","harga_tindakan");
    $responder->setSummary("E",array("G"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Waktu");
    $responder->addColumn("C","Perawat");
    $responder->addColumn("D","Pasien");
    $responder->addColumn("E","NRM");
    $responder->addColumn("F","Tindakan");
    $responder->addColumn("G","Harga");
    $responder->addColumn("H","Cara Bayar");
    $responder->addColumn("I","Ruangan");
}

$tindakan_perawat->setDateTimeEnable(true);
$header=array ("No.","Waktu",'Perawat',"Pasien",'NRM',"Tindakan",'Harga',"Cara Bayar","Ruangan");
if(isset($_POST['dari']) && isset($_POST['sampai'])){    
	$tindakan_perawat->getServiceResponder()->addData("dari",$_POST['dari']);
	$tindakan_perawat->getServiceResponder()->addData("sampai",$_POST['sampai']);
	$tindakan_perawat->getServiceResponder()->addData("nama_tindakan",$_POST['nama_tindakan']);
	$tindakan_perawat->setEntity($_POST['ruangan']);
    $tindakan_perawat->getServiceResponder()->setSortingMethode($_POST['urutkan']);
}
$btn=$tindakan_perawat->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$tindakan_perawat->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
         ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
         ->addModal("urutkan", "select", "Urutkan", $urutkan->getContent())
         ->addModal("id_perawat", "hidden", "", "")
         ->addModal("perawat", "chooser-pendapatan_tindakan_perawat-perawat_tindakan_perawat-Pilih Perawat", "Perawat", "")
		 ->addModal("nama_tindakan", "chooser-pendapatan_tindakan_perawat-ptp_tindakan_perawat-Tindakan", "Tindakan", "");
         
$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_tindakan_perawat.excel()");

$tindakan_perawat ->getForm(true,"")
        ->addElement("", $btn)
        ->addElement("", $excel);
         
$tindakan_perawat->getUItable()
		 ->setHeader($header)
         ->setFooterVisible(false);
         
require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
$summary=new TindakanPerRuangAdapter();
$summary->addSummary("Harga", "harga_tindakan","money Rp.");
$summary->addFixValue("NRM","<strong>Total</strong>");

$tindakan_perawat->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Waktu", "waktu","date d M Y H:i")
		 ->add("Pasien", "nama_pasien")
		 ->add("Tindakan", "nama_tindakan")
		 ->add("NRM", "nrm_pasien","only-digit8")
		 ->add("Perawat", "nama_perawat")
         ->add("Cara Bayar", "carabayar","unslug")
         ->add("Ruangan", "smis_entity")
		 ->add("Harga", "harga_tindakan","money Rp.");
$tindakan_perawat->addViewData("dari", "dari")
	   ->addViewData("sampai", "sampai")
	   ->addViewData("perawat", "perawat")
       ->addViewData("carabayar", "carabayar")
       ->addViewData("ruangan", "ruangan")
       ->addViewData("perawat", "perawat")
       ->addViewData("urutkan", "urutkan")
	   ->addViewData("id_perawat", "id_perawat")
       ->addViewData("nama_tindakan","nama_tindakan");
		
$dktable = new Table ( array ("Nama","Kelas","Tarif"), "", NULL, true );
$dktable->setName ( "ptp_tindakan_perawat" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Kelas", "kelas", "unslug" );
$dkadapter->add ( "Tarif", "tarif", "money Rp." );
$tarif = new ServiceResponder ( $db, $dktable, $dkadapter, "get_keperawatan" );

$tindakan_perawat->getSuperCommand()->addResponder("perawat_tindakan_perawat", $perawat);
$tindakan_perawat->addSuperCommand("perawat_tindakan_perawat", array());
$tindakan_perawat->addSuperCommandArray("perawat_tindakan_perawat", "perawat", "nama");
$tindakan_perawat->addSuperCommandArray("perawat_tindakan_perawat", "id_perawat", "id");

$tindakan_perawat->getSuperCommand()->addResponder("ptp_tindakan_perawat", $tarif);
$tindakan_perawat->addSuperCommand("ptp_tindakan_perawat", array());
$tindakan_perawat->addSuperCommandArray("ptp_tindakan_perawat", "nama_tindakan", "nama");
$tindakan_perawat->initialize();