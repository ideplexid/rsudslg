<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/MapRuangan.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");

$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "dokter_fisiotherapy" );
$dktable->setModel ( Table::$SELECT );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );


$fisiotherapy=new MasterSlaveServiceTemplate($db, "pendapatan_fisiotherapy", "finance", "pendapatan_fisiotherapy");
$responder = new PendapatanPerRuangPerDokterResponder($db,$fisiotherapy->getUItable(),$fisiotherapy->getAdapter(),"pendapatan_fisiotherapy");
$fisiotherapy -> setDBresponder($responder);

$fisiotherapy->setDateTimeEnable(true);
$header=array ("No.","Waktu","Ruangan","Cara Bayar",'Dokter',"Pasien",'NRM','Harga',"Dokter Rehab");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$fisiotherapy->getServiceResponder()->addData("dari",$_POST['dari']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$fisiotherapy->getServiceResponder()->addData("sampai",$_POST['sampai']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$fisiotherapy->getServiceResponder()->addData("ruangan",$_POST['ruangan']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$fisiotherapy->setEntity("fisiotherapy");
}
if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $responder->setTitle("Pendapatan Fisiotherapy");
    $responder->setFileName("Pendapatan Fisiotherapy");
    $responder->addResetMoney("H","Harga","biaya");
    $responder->addResetMoney("I","Dokter Rehab","biaya_dokter_rhb");
    $responder->setSummary("G",array("H","I"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Waktu");
    $responder->addColumn("C","Ruangan");
    $responder->addColumn("D","Cara Bayar");
    $responder->addColumn("E","Dokter");
    $responder->addColumn("F","Pasien");
    $responder->addColumn("G","NRM");
    $responder->addColumn("H","Harga");
    $responder->addColumn("I","Dokter Rehab");
}
$btn=$fisiotherapy->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$fisiotherapy->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
		 ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
		 ->addModal("dokter", "chooser-pendapatan_fisiotherapy-dokter_fisiotherapy-Pilih Dokter", "Dokter", "")
         ->addModal("id_dokter", "hidden", "", "");
         
$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_fisiotherapy.excel()");

$fisiotherapy ->getForm(true,"")
            ->addElement("", $btn)
            ->addElement("", $excel);
$fisiotherapy->getUItable()
		 ->setHeader($header)
         ->setFooterVisible(false);
         
require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
$summary=new TindakanPerRuangAdapter();
$summary->addSummary("Harga", "biaya","money Rp.");
$summary->addSummary("Dokter Rehab", "biaya_dokter_rhb","money Rp.");
$summary->addFixValue("NRM","<strong>Total</strong>");
$fisiotherapy->getSuperCommand()->addResponder("dokter_fisiotherapy", $dokter);
$fisiotherapy->addSuperCommand("dokter_fisiotherapy", array());
$fisiotherapy->addSuperCommandArray("dokter_fisiotherapy", "dokter", "nama");
$fisiotherapy->addSuperCommandArray("dokter_fisiotherapy", "id_dokter", "id");
$fisiotherapy->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Waktu", "tanggal","date d M Y")
		 ->add("Pasien", "nama_pasien")
		 ->add("Cara Bayar", "carabayar","unslug")
		 //->add("Ruangan", "ruangan","unslug")
		 ->add("NRM", "nrm_pasien","only-digit8")
		 ->add("Dokter", "nama_dokter")
		 ->add("Harga", "biaya","money Rp.")
		 ->add("Dokter Rehab", "biaya_dokter_rhb","money Rp.");
$fisiotherapy->addViewData("dari", "dari")
	   ->addViewData("sampai", "sampai")
	   ->addViewData("dokter", "dokter")
	   ->addViewData("id_dokter", "id_dokter")
	   ->addViewData("ruangan", "ruangan")
		->addViewData("carabayar", "carabayar");
$fisiotherapy->initialize();