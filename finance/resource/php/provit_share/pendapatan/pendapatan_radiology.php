<?php
global $db;
show_error();
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

require_once 'finance/class/MapRuangan.php';
$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>"PENDAFTARAN","value"=>"Pendaftaran");
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");

$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "dokter_radiology" );
$dktable->setModel ( Table::$SELECT );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );


$radiology=new MasterSlaveServiceTemplate($db, "pendapatan_radiology", "finance", "pendapatan_radiology");
$radiology->setDateTimeEnable(true);
$responder = new PendapatanPerRuangPerDokterResponder($db,$radiology->getUItable(),$radiology->getAdapter(),"pendapatan_radiology");
$radiology -> setDBresponder($responder);

$header=array ("No.","Waktu","Ruangan","Cara Bayar",'Dokter',"Pasien",'NRM','Harga');
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$radiology->getServiceResponder()->addData("dari",$_POST['dari']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$radiology->getServiceResponder()->addData("sampai",$_POST['sampai']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$radiology->getServiceResponder()->addData("ruangan",$_POST['ruangan']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$radiology->setEntity("radiology");
}

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $responder->setTitle("Pendapatan Radiology");
    $responder->setFileName("Pendapatan Radiology");
    $responder->addResetMoney("H","Harga","biaya");
    $responder->setSummary("G",array("H"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Waktu");
    $responder->addColumn("C","Ruangan");
    $responder->addColumn("D","Cara Bayar");
    $responder->addColumn("E","Dokter");
    $responder->addColumn("F","Pasien");
    $responder->addColumn("G","NRM");
    $responder->addColumn("H","Harga");
}

$btn=$radiology->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$radiology->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
		 ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
		 ->addModal("dokter", "chooser-pendapatan_radiology-dokter_radiology-Pilih Dokter", "Dokter", "")
		 ->addModal("id_dokter", "hidden", "", "");
$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_radiology.excel()");

$radiology ->getForm(true,"")
            ->addElement("", $btn)
            ->addElement("", $excel);
$radiology->getUItable()
		 ->setHeader($header)
         ->setFooterVisible(false);
         
require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
$summary=new TindakanPerRuangAdapter();
$summary->addSummary("Harga", "biaya","money Rp.");
$summary->addFixValue("NRM","<strong>Total</strong>");

$radiology->getSuperCommand()->addResponder("dokter_radiology", $dokter);
$radiology->addSuperCommand("dokter_radiology", array());
$radiology->addSuperCommandArray("dokter_radiology", "dokter", "nama");
$radiology->addSuperCommandArray("dokter_radiology", "id_dokter", "id");
$radiology->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Waktu", "waktu_daftar","date d M Y H:i")
		 ->add("Pasien", "nama_pasien")
		 ->add("Cara Bayar", "carabayar","unslug")
		 //->add("Ruangan", "ruangan","unslug")
		 ->add("NRM", "nrm_pasien","only-digit8")
		 ->add("Dokter", "nama_dokter")
		 ->add("Harga", "biaya","money Rp.");
$radiology->addViewData("dari", "dari")
	   ->addViewData("sampai", "sampai")
	   ->addViewData("dokter", "dokter")
	   ->addViewData("id_dokter", "id_dokter")
	   ->addViewData("ruangan", "ruangan")
		->addViewData("carabayar", "carabayar");
$radiology->initialize();