<?php

/**
 * masih belum jadi nunggu buatkan service */

global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

$header=array ("No.","Tanggal","Nama Operasi","No Reg","NRM",'Nama',
				"Opt I","B. Opt. I",
				"Ass. Opt I","B. Ass. Opt. I",
				"Opt II","B. Opt. II",
				"Ass. Opt II","B. Ass. Opt. II",
				"Anas","B. Anas.",
				"Ass. Anas","B. Ass. Anas.",
				"B. Team OK",
				"B. Bidan",
				"B. Kamar","Carabayar","Total");
$plafon=new MasterSlaveServiceTemplate($db, "get_ok_list", "finance", "item_ok");
$responder = new PendapatanPerRuangPerDokterResponder($db,$plafon->getUItable(),$plafon->getAdapter(),"get_ok_list");
$plafon -> setDBresponder($responder);

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $_POST['max'] = 1000000000000;
    $responder->setTitle("Pendapatan Operasi");
    $responder->setFileName("Pendapatan Operasi");
    $responder->addResetMoney("H","B. Opt. I","harga_operator_satu");
    $responder->addResetMoney("J","B. Ass. Opt. I","harga_asisten_operator_satu");
    $responder->addResetMoney("L","B. Opt. II","harga_operator_dua");
    $responder->addResetMoney("N","B. Ass. Opt. II","harga_asisten_operator_dua");
    $responder->addResetMoney("P","B. Anas.","harga_anastesi");
    $responder->addResetMoney("R","B. Ass. Anas","harga_asisten_anastesi");
    $responder->addResetMoney("S","B. Team OK","harga_team_ok");
    $responder->addResetMoney("T","B. Bidan","harga_bidan");
    $responder->addResetMoney("U","B. Kamar","harga_sewa_kamar");
    $responder->addResetMoney("W","Total","harga_tindakan");

    $responder->setSummary("G",array("H","J","L","N","P","R","S","T","U","W"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Tanggal");
    $responder->addColumn("C","Nama Operasi");
    $responder->addColumn("D","No Reg");
    $responder->addColumn("E","NRM");
    $responder->addColumn("F","Nama");
    $responder->addColumn("G","Opt I");
    $responder->addColumn("H","B. Opt. I");
    $responder->addColumn("I","Ass. Opt I");
    $responder->addColumn("J","B. Ass. Opt. I");
    $responder->addColumn("K","Opt II");
    $responder->addColumn("L","B. Opt. II");
    $responder->addColumn("M","Ass. Opt II");
    $responder->addColumn("N","B. Ass. Opt. II");
    $responder->addColumn("O","Anas");
    $responder->addColumn("P","B. Anas.");
    $responder->addColumn("Q","Ass. Anas");
    $responder->addColumn("R","B. Ass. Anas");
    $responder->addColumn("S","B. Team OK");
    $responder->addColumn("T","B. Bidan");
    $responder->addColumn("U","B. Kamar");
    $responder->addColumn("V","Carabayar");
    $responder->addColumn("W","Total");
    
}

$plafon->setDateEnable(true);
if( isset($_POST['command']) && $_POST['command']=="list" ){	
	$plafon->setEntity($_POST['jenislayanan']);
	$plafon->getServiceResponder()->addData("sampai",$_POST['sampai']);	
	$plafon->getServiceResponder()->addData("dari",$_POST['dari']);	
    if($_POST['carabayar']!=""){
        $plafon->getServiceResponder()->addData("carabayar",$_POST['carabayar']);    
    }
}

$plafon->addResouce("js","finance/resorce/js/item_ok.js");
$plafon->addResouce("js","base-js/smis-base-loading.js","before",true);

$adapt=new SummaryAdapter();
$adapt	->setUseNumber(true, "No.","back.")
		->add("No Reg", "noreg_pasien","only-digit9")
        ->add("Tanggal", "waktu","date d M Y")
		->add("NRM", "nrm_pasien","only-digit8")
        ->add("Carabayar", "carabayar","unslug")
		->add("Nama", "nama_pasien")
		->add("Nama Operasi", "nama_sewa_kamar")
		->add("Opt I", "nama_operator_satu")
		->add("B. Opt. I", "harga_operator_satu","money Rp.")
		->add("Ass. Opt I", "nama_asisten_operator_satu")
		->add("B. Ass. Opt. I", "harga_asisten_operator_satu","money Rp.")
		->add("Opt II", "nama_operator_dua")
		->add("B. Opt. II", "harga_operator_dua","money Rp.")
		->add("Ass. Opt II", "nama_asisten_operator_dua")
		->add("B. Ass. Opt. II", "harga_asisten_operator_dua","money Rp.")
		->add("Anas", "nama_anastesi")
		->add("B. Anas.", "harga_anastesi","money Rp.")
		->add("Ass. Anas", "nama_asisten_anastesi")
		->add("B. Ass. Anas.", "harga_asisten_anastesi","money Rp.")
		->add("B. Team OK", "harga_team_ok","money Rp.")
		->add("B. Bidan", "harga_bidan","money Rp.")
        ->add("B. Kamar", "harga_sewa_kamar","money Rp.")
        ->add("Total", "harga_tindakan","money Rp.");

$adapt	->addSummary("B. Opt. I", "harga_operator_satu","money Rp.")		
		->addSummary("B. Ass. Opt. I", "harga_asisten_operator_satu","money Rp.")
		->addSummary("B. Opt. II", "harga_operator_dua","money Rp.")
		->addSummary("B. Ass. Opt. II", "harga_asisten_operator_dua","money Rp.")
		->addSummary("B. Anas.", "harga_anastesi","money Rp.")
		->addSummary("B. Ass. Anas.", "harga_asisten_anastesi","money Rp.")
		->addSummary("B. Team OK", "harga_team_ok","money Rp.")
		->addSummary("B. Bidan", "harga_bidan","money Rp.")
        ->addSummary("B. Kamar", "harga_sewa_kamar","money Rp.")
        ->addSummary("Total", "harga_tindakan","money Rp.");
		
$adapt->addFixValue("Opt I","<strong>Total</strong>");
$plafon->setAdapter($adapt);
$plafon ->getUItable()
		->setAction(false)
		->setHeader($header);


if(!isset($_POST['command'])){	
	require_once 'finance/class/MapRuangan.php';
    $ruangan=MapRuangan::getRuangan();
    $ruangan[]=array("name"=>" - SEMUA - ","value"=>"all","default"=>"1");
    
    $service = new ServiceConsumer ( $db, "get_carabayar" );
    $service->execute ();
    $dataasuransi= $service->getContent ();
    $adapter=new SelectAdapter("nama", "slug");
    $jenis_pembayaran=$adapter->getContent($dataasuransi);
    $jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");
    
	$plafon ->getUItable()
			->addModal("dari", "date", "Dari", "","n")
			->addModal("sampai", "date", "Sampai", "","n")
			->addModal("jenislayanan", "select", "Layanan", $ruangan,"y")
            ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran,"y");	
   
 $btn = $plafon ->getUItable()
                ->setAddButtonEnable(false)
                ->setPrintButtonEnable(false)
                ->getHeaderButton();

$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("item_ok.excel()");

$plafon ->getForm(true,"")
        ->addElement("", $btn)
        ->addElement("", $excel);

	$plafon->getUItable()->clearContent();
	$plafon->addViewData("dari","dari");
	$plafon->addViewData("sampai","sampai");
	$plafon->addViewData("jenislayanan","jenislayanan");
    $plafon->addViewData("carabayar","carabayar");
	
}
$plafon->initialize();