<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/MapRuangan.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"all","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "dokter_tindakan_dokter" );
$dktable->setModel ( Table::$SELECT );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );


$tindakan_dokter = new MasterSlaveServiceTemplate($db, "pendapatan_tindakan_dokter", "finance", "pendapatan_tindakan_dokter");
$tindakan_dokter ->setDateTimeEnable(true);
$responder = new PendapatanPerRuangPerDokterResponder($db,$tindakan_dokter->getUItable(),$tindakan_dokter->getAdapter(),"pendapatan_tindakan_dokter");
$tindakan_dokter -> setDBresponder($responder);
$tindakan_dokter ->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $responder->setTitle("Pendapatan Tindakan Dokter");
    $responder->setFileName("Pendapatan Tindakan Dokter");
    $responder->addResetMoney("G","Biaya Dokter","harga");
    $responder->addResetMoney("H","Biaya Asisten","harga_perawat");
    $responder->setSummary("F",array("G","H"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Waktu");
    $responder->addColumn("C","Tindakan");
    $responder->addColumn("D","Dokter");
    $responder->addColumn("E","Pasien");
    $responder->addColumn("F","NRM");
    $responder->addColumn("G","Biaya Dokter");
    $responder->addColumn("H","Biaya Asisten");
    $responder->addColumn("I","Cara Bayar");
    $responder->addColumn("J","Ruangan");
}

$header=array ("No.","Waktu","Tindakan",'Dokter',"Pasien",'NRM','Biaya Dokter',"Biaya Asisten","Cara Bayar","Ruangan");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$tindakan_dokter->getServiceResponder()->addData("dari",$_POST['dari']);
	$tindakan_dokter->getServiceResponder()->addData("sampai",$_POST['sampai']);
	$tindakan_dokter->getServiceResponder()->addData("nama_tindakan",$_POST['nama_tindakan']);
	$tindakan_dokter->setEntity($_POST['ruangan']);
}
$btn=$tindakan_dokter->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$tindakan_dokter->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
         ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
		 ->addModal("dokter", "chooser-pendapatan_tindakan_dokter-dokter_tindakan_dokter-Pilih Dokter", "Dokter", "")
		 ->addModal("id_dokter", "hidden", "", "")
		 ->addModal("nama_tindakan", "text", "Tindakan", "");


$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_tindakan_dokter.excel()");

$tindakan_dokter ->getForm(true,"")
                ->addElement("", $btn)
                ->addElement("", $excel);

$tindakan_dokter->getUItable()
		 ->setHeader($header)
         ->setFooterVisible(false);
         
require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
$summary=new TindakanPerRuangAdapter();
$summary->addSummary("Biaya Dokter", "harga","money Rp.");
$summary->addSummary("Biaya Asisten", "harga_perawat","money Rp.");
$summary->addFixValue("NRM","<strong>Total</strong>");

$tindakan_dokter->getSuperCommand()->addResponder("dokter_tindakan_dokter", $dokter);
$tindakan_dokter->addSuperCommand("dokter_tindakan_dokter", array());
$tindakan_dokter->addSuperCommandArray("dokter_tindakan_dokter", "dokter", "nama");
$tindakan_dokter->addSuperCommandArray("dokter_tindakan_dokter", "id_dokter", "id");
$tindakan_dokter->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back .")
		 ->add("Waktu", "waktu","date d M Y H:i")
		 ->add("Pasien", "nama_pasien")
		 ->add("Tindakan", "nama_tindakan")
		 ->add("NRM", "nrm_pasien","only-digit8")
		 ->add("Dokter", "nama_dokter")
		 ->add("Tindakan", "nama_tindakan")
         ->add("Cara Bayar", "carabayar","unslug")
         ->add("Ruangan", "smis_entity")
		 ->add("Biaya Dokter", "harga","money Rp.")
		 ->add("Biaya Asisten", "harga_perawat","money Rp.");
$tindakan_dokter->addViewData("dari", "dari")
	   ->addViewData("sampai", "sampai")
	   ->addViewData("dokter", "dokter")
	   ->addViewData("id_dokter", "id_dokter")
	   ->addViewData("ruangan", "ruangan")
       ->addViewData("carabayar", "carabayar")
	   ->addViewData("nama_tindakan", "nama_tindakan");
$tindakan_dokter->initialize();