<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/function/sort_tigd.php';
require_once 'finance/class/MapRuangan.php';
require_once "finance/class/responder/PendapatanPerRuangPerDokterResponder.php";

$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"all","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header);
$dktable->setName ( "dokter_tindakan_perawat_igd" );
$dktable->setModel ( Table::$SELECT );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "" );


$tindakan_perawat_igd=new MasterSlaveServiceTemplate($db, "pendapatan_tindakan_perawat_igd", "finance", "pendapatan_tindakan_perawat_igd");
$tindakan_perawat_igd->setDateTimeEnable(true);
$responder = new PendapatanPerRuangPerDokterResponder($db,$tindakan_perawat_igd->getUItable(),$tindakan_perawat_igd->getAdapter(),"pendapatan_tindakan_perawat_igd");
$tindakan_perawat_igd -> setDBresponder($responder);
$tindakan_perawat_igd->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);

if(isset($_POST['command']) && $_POST['command']=="excel" ){
    $responder->setTitle("Pendapatan Perawat IGD");
    $responder->setFileName("Pendapatan Perawat IGD");
    $responder->addResetMoney("H","Harga","harga_tindakan");
    $responder->setSummary("F",array("H"));
    $responder->addColumn("A","No.");
    $responder->addColumn("B","Waktu");
    $responder->addColumn("C","Dokter");
    $responder->addColumn("D","Perawat");
    $responder->addColumn("E","Pasien");
    $responder->addColumn("F","NRM");
    $responder->addColumn("G","Tindakan");
    $responder->addColumn("H","Harga");
    $responder->addColumn("I","Cara Bayar");
    $responder->addColumn("J","Ruangan");
}


$header=array ("No.","Waktu",'Dokter',"Perawat","Pasien",'NRM',"Tindakan",'Harga',"Cara Bayar","Ruangan");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
	$tindakan_perawat_igd->getServiceResponder()->addData("dari",$_POST['dari']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$tindakan_perawat_igd->getServiceResponder()->addData("sampai",$_POST['sampai']);// getDBtable()->addCustomKriteria(NULL, " tanggal>='".$_POST['dari']."'");
	$tindakan_perawat_igd->setEntity($_POST['ruangan']);
    $tindakan_perawat_igd->getServiceResponder()->setSortingMethode($_POST['urutkan']);
}
$urutkan=new OptionBuilder();
$urutkan->add("","","1");
$urutkan->add("Dokter","sort_tigd_dokter_name");
$urutkan->add("Perawat","sort_tigd_perawat_name");
$urutkan->add("Tanggal","sort_tigd_waktu");

$btn=$tindakan_perawat_igd->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
						  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
						  ->addNoClear("dari")
						  ->addNoClear("sampai")
						  ->getUItable()
						  ->setAddButtonEnable(false)
						  ->getHeaderButton();
$tindakan_perawat_igd->getUItable()
					 ->setAction(false)
					 ->addModal("dari", "datetime", "Dari", "")
					 ->addModal("sampai", "datetime", "Sampai", "")
                     ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran)
					 ->addModal("ruangan", "select", "Ruangan", $ruangan)
                     ->addModal("urutkan", "select", "Urutkan", $urutkan->getContent())
					 ->addModal("dokter", "chooser-pendapatan_tindakan_perawat_igd-dokter_tindakan_perawat_igd-Pilih Dokter atau Perawat", "Dokter/Perawat", "")
					 ->addModal("id_dokter", "hidden", "", "")
					 ->addModal("nama_tindakan", "chooser-pendapatan_tindakan_perawat_igd-ptpi_tindakan_perawat-Tindakan", "Tindakan", "");


$excel = new Button("","","Excel");
$excel ->setClass("btn-primary");
$excel ->setIcon(" fa fa-file-excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setAction("pendapatan_tindakan_perawat_igd.excel()");

$tindakan_perawat_igd ->getForm(true,"")
        ->addElement("", $btn)        
        ->addElement("", $excel);

$tindakan_perawat_igd->getUItable()
					 ->setHeader($header)
                     ->setFooterVisible(false);

require_once "finance/class/adapter/TindakanPerRuangAdapter.php";
$summary=new TindakanPerRuangAdapter();
$summary->addSummary("Harga", "harga_tindakan","money Rp.");
$summary->addFixValue("NRM","<strong>Total</strong>");

$dktable = new Table ( array ("Nama","Kelas","Tarif"), "", NULL, true );
$dktable->setName ( "ptpi_tindakan_perawat" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "Kelas", "kelas", "unslug" );
$dkadapter->add ( "Tarif", "tarif", "money Rp." );
$tarif = new ServiceResponder ( $db, $dktable, $dkadapter, "get_keperawatan" );

$tindakan_perawat_igd->getSuperCommand()->addResponder("dokter_tindakan_perawat_igd", $dokter);
$tindakan_perawat_igd->addSuperCommand("dokter_tindakan_perawat_igd", array());
$tindakan_perawat_igd->addSuperCommandArray("dokter_tindakan_perawat_igd", "dokter", "nama");
$tindakan_perawat_igd->addSuperCommandArray("dokter_tindakan_perawat_igd", "id_dokter", "id");

$tindakan_perawat_igd->getSuperCommand()->addResponder("ptpi_tindakan_perawat", $tarif);
$tindakan_perawat_igd->addSuperCommand("ptpi_tindakan_perawat", array());
$tindakan_perawat_igd->addSuperCommandArray("ptpi_tindakan_perawat", "nama_tindakan", "nama");
$tindakan_perawat_igd->addViewData("nama_tindakan","nama_tindakan");

$tindakan_perawat_igd->setAdapter($summary)
		 ->getAdapter()
		 ->setUseNumber(true, "No.","back.")
		 ->add("Waktu", "waktu","date d M Y H:i")
		 ->add("Pasien", "nama_pasien")
		 ->add("NRM", "nrm_pasien","only-digit8")
		 ->add("Dokter", "nama_dokter")
		 ->add("Tindakan", "nama_tindakan")
		 ->add("Perawat", "nama_perawat")
         ->add("Cara Bayar", "carabayar","unslug")
         ->add("Ruangan", "smis_entity")
		 ->add("Harga", "harga_tindakan","money Rp.");
$tindakan_perawat_igd->addViewData("dari", "dari")
	   ->addViewData("sampai", "sampai")
	   ->addViewData("dokter", "dokter")
	   ->addViewData("id_dokter", "id_dokter")
       ->addViewData("carabayar", "carabayar")
       ->addViewData("urutkan", "urutkan")
	   ->addViewData("ruangan", "ruangan");
$tindakan_perawat_igd->initialize();