<?php 
	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	require_once 'finance/class/adapter/PerawatProfitShareAdapter.php';
	loadLibrary("smis-libs-function-math");
	global $db;
	global $user;

	$form = new Form("pvtk_form", "", "");
	$id_karyawan = new Hidden("pvtk_id_karyawan", "pvtk_id_karyawan", "");
	$form->addElement("", $id_karyawan);
	$karyawan_button = new Button("", "", "Pilih");
	$karyawan_button->setClass("btn-info");
	$karyawan_button->setAction("pv_tindakan_keperawatan_karyawan.chooser('pv_tindakan_keperawatan_karyawan', 'pv_tindakan_keperawatan_karyawan_button', 'pv_tindakan_keperawatan_karyawan', pv_tindakan_keperawatan_karyawan)");
	$karyawan_button->setIcon("icon-white icon-list-alt");
	$karyawan_button->setIsButton(Button::$ICONIC);
	$karyawan_button->setAtribute("id='karyawan_pvtk_browse'");
	$karyawan_text = new Text("pvtk_nama_karyawan", "pvtk_nama_karyawan", "");
	$karyawan_text->setClass("smis-one-option-input");
	$karyawan_text->setAtribute("disabled='disabled'");
	$karyawan_input_group = new InputGroup("");
	$karyawan_input_group->addComponent($karyawan_text);
	$karyawan_input_group->addComponent($karyawan_button);
	$form->addElement("Karyawan", $karyawan_input_group);
    
	$jabatan_karyawan = new Text("pvtk_jabatan_karyawan", "pvtk_jabatan_karyawan", "");
	$jabatan_karyawan->setAtribute("disabled='disabled'");
	$form->addElement("Jabatan", $jabatan_karyawan);
    
	$nip_karyawan = new Text("pvtk_nip_karyawan", "pvtk_nip_karyawan", "");
	$nip_karyawan->setAtribute("disabled='disabled'");
	$form->addElement("NIP", $nip_karyawan);
    
	$id_akuntan = new Hidden("pvtk_id_akuntan", "pvtk_id_akuntan", "");
	$form->addElement("", $id_akuntan);
    
	$akuntan_button = new Button("", "", "Pilih");
	$akuntan_button->setClass("btn-info");
	$akuntan_button->setAction("pv_tindakan_keperawatan_akuntan.chooser('pv_tindakan_keperawatan_akuntan', 'pv_tindakan_keperawatan_akuntan_button', 'pv_tindakan_keperawatan_akuntan', pv_tindakan_keperawatan_akuntan)");
	$akuntan_button->setIcon("icon-white icon-list-alt");
	$akuntan_button->setIsButton(Button::$ICONIC);
	$akuntan_button->setAtribute("id='akuntan_pvtk_browse'");
	$akuntan_text = new Text("pvtk_nama_akuntan", "pvtk_nama_akuntan", "");
	$akuntan_text->setClass("smis-one-option-input");
	$akuntan_text->setAtribute("disabled='disabled'");
	$akuntan_input_group = new InputGroup("");
	$akuntan_input_group->addComponent($akuntan_text);
	$akuntan_input_group->addComponent($akuntan_button);
	$form->addElement("Akuntan", $akuntan_input_group);
    
	$tanggal_from_text = new Text("pvtk_dari", "pvtk_dari", "");
	$tanggal_from_text->setClass("mydate");
	$tanggal_from_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Dari", $tanggal_from_text);
    
	$tanggal_to_text = new Text("pvtk_sampai", "pvtk_sampai", "");
	$tanggal_to_text->setClass("mydate");
	$tanggal_to_text->setAtribute("data-date-format='yyyy-mm-dd'");
	$form->addElement("Sampai", $tanggal_to_text);
    
    $option=new OptionBuilder();
    $option->add("","both",1);
    $option->add("Individu","individu",0);
    $option->add("Communal","communal",0);
    $select=new Select("pvtk_type","",$option->getContent());
    $form->addElement("Jenis", $select);
    
	$pjkserv = new ServiceConsumer($db, "get_pajak");
	$pjkserv->execute();
	$pajak = $pjkserv->getContent();
	$pajak_text = new Text("pvtk_pajak", "pvtk_pajak", $pajak);
	$form->addElement("Pajak (%)", $pajak_text);
	$show_button = new Button("", "", "Tampilkan");
	$show_button->setClass("btn-primary");
	$show_button->setIcon("icon-white icon-repeat");
	$show_button->setIsButton(Button::$ICONIC);
	$show_button->setAction("pvtk.view()");
	$print_button = new Button("", "", "Karyawan");
	$print_button->setClass("btn-primary");
	$print_button->setIcon("icon-white icon-print");
	$print_button->setIsButton(Button::$ICONIC_TEXT);
	$print_button->setAction("pvtk.print_karyawan()");
	$print2_button = new Button("", "", "Akuntansi");
	$print2_button->setClass("btn-primary");
	$print2_button->setIcon("icon-white icon-print");
	$print2_button->setIsButton(Button::$ICONIC_TEXT);
    $print2_button->setAction("pvtk.print_akuntan()");
    
    $excel = new Button("","","Excel");
    $excel ->setIsButton(Button::$ICONIC_TEXT);
    $excel ->setIcon("fa fa-file-excel");
    $excel ->setClass("btn btn-primary");
    $excel ->setAction("pvtk.excel()");

	$btn_group = new ButtonGroup("noprint");
	$btn_group->addButton($show_button);
	$btn_group->addButton($print_button);
	$btn_group->addButton($print2_button);
    $form->addElement("", $btn_group);
    $form->addElement("", $excel);
	
	$table = new Table(
		array("Tanggal", "Ruangan", "Pasien", "Tindakan", "Biaya Awal", "Persentase", "Bagian Karyawan", "Bagian RS", "Pajak", "Dana Pengembangan"),
		"",
		null,
		true
	);
	$table->setName("pvtk_table");
	$table->setAction(false);
	$table->setHeaderVisible(false);
	$table->setFooterVisible(false);
	$header_html = "
		<tr>
			<th colspan='10'>SURAT BUKTI PENERIMAAN <font id='untuk_table'></font></th>
		</tr>
		<tr>
			<th colspan='1'>Nomor</th>
			<th colspan='5' id='info_pvtk_nomor'></th>
			<th colspan='1'>Waktu</th>
			<th colspan='3' id='info_pvtk_waktu'></th>
		</tr>
		<tr>
			<th colspan='1'>Nama</th>
			<th colspan='2' id='info_pvtk_nama'></th>
			<th colspan='1'>Jabatan</th>
			<th colspan='2' id='info_pvtk_jabatan'></th>
			<th colspan='1'>NIP</th>
			<th colspan='3' id='info_pvtk_nip'></th>
		</tr>
		<tr>
			<th colspan='1'>Dari</th>
			<th colspan='2' id='info_pvtk_dari'></th>
			<th colspan='1'>Sampai</th>
			<th colspan='2' id='info_pvtk_sampai'></th>
			<th colspan='1'>Pajak</th>
			<th colspan='3' id='info_pvtk_pajak'></th>
		</tr>
		<tr>
			<th colspan='1'>Tanggal</th>
			<th colspan='1'>Ruangan</th>
			<th colspan='1'>Pasien</th>
			<th colspan='1'>Tindakan</th>
			<th colspan='1'>Biaya Awal</th>
			<th colspan='1'>Persentase</th>
			<th colspan='1'>Bagian Karyawan</th>
			<th colspan='1'>Bagian RS</th>
			<th colspan='1'>Pajak</th>
			<th colspan='1'>Dana Pengembangan</th>
		</tr>
	";
	$table->addHeader("after", $header_html);
	
	if(isset($_POST['super_command']) && $_POST['super_command'] == "cair"){
		$dbtab = new DBTable($db, "smis_fnc_bayar");
		$dbres = new DBResponder($dbtab, new Table(array()), new SimpleAdapter());
		$dbres->addColumnFixValue("waktu", date("Y-m-d"));
		$res = $dbres->command("save");
		echo json_encode($res);
		return;
	}

	if (isset($_POST['super_command']) && $_POST['super_command'] != "") {
		$head = array('Nama','Jabatan',"NIP");
		$dkadapter = new SimpleAdapter();
		$dkadapter->add("Jabatan","nama_jabatan");
		$dkadapter->add("Nama","nama");
		$dkadapter->add("NIP","nip");
	
		$dktable = new Table($head);
		$dktable->setName("pv_tindakan_keperawatan_karyawan");
		$dktable->setModel(Table::$SELECT);	
		$karyawan = new EmployeeResponder($db, $dktable, $dkadapter, "perawat");
	
		$actable = new Table($head);
		$actable->setName("pv_tindakan_keperawatan_akuntan");
		$actable->setModel(Table::$SELECT);
		$accounting = new EmployeeResponder($db,$actable,$dkadapter,"aku");
	
		$super = new SuperCommand();
		$super->addResponder("pv_tindakan_keperawatan_karyawan", $karyawan);
		$super->addResponder("pv_tindakan_keperawatan_akuntan", $accounting);
		$init = $super->initialize();
		if ($init != null) {
			echo $init;
			return;
		}
	}
	
	if(isset($_POST['command'])  &&  ( $_POST['command'] == "list" || $_POST['command'] == "excel" ) ) {
		$service = new ServiceConsumer($db, "get_pv_tindakan_keperawatan");
		$service->setMode(ServiceConsumer::$CLEAN_BOTH)
				->addData("dari", $_POST['dari'])
				->addData("sampai", $_POST['sampai'])
                ->addData("type", $_POST['type'])
				->addData("id_karyawan", $_POST['id_karyawan']);
		$content = $service->execute()->getContent();
        $adapter = new PerawatProfitShareAdapter($_POST['pajak']);
        $adapter    ->setExcel($_POST['command']=="excel");
		$uidata = $adapter->add("Tanggal", "waktu")
				  ->add("Pasien", "pasien")
				  //->add("Ruangan", "ruangan","unslug")
				  ->add("Tindakan", "sebagai")
				  ->add("Persentase", "percentage")
				  ->add("Biaya Awal", "asli","money Rp.")
				  ->add("Bagian Karyawan", "nilai","money Rp.")
				  ->setUseID(false)
				  ->getContent($content);
        if($_POST['command']=="list"){
            $body_html = 	"
                <tr>
                    <td>Terbilang</td>
                    <td colspan='9' id='pvtk_numbertell'></td>
                </tr>
                <tr>
                    <td colspan='5'>Penerima</td>
                    <td colspan='5'>Pencetak</td>
                </tr>
                <tr>
                    <td colspan='5'></br></br></br></br> ( <strong id='penerima_pv_tindakan_keperawatan'></strong> ) </td>
                    <td colspan='5'></br></br></br></br> ( <strong >" . $user->getNameOnly() . "</strong> ) </td>
                </tr>
            ";
        
            $list = $table->setContent($uidata)
                        ->addBody("after", $body_html)
                        ->getBodyContent();
            $json['list'] = $list;
            $json['pagination'] = "";
            $json['number'] = "0";
            $json['number_p'] = "0";
            $json['data'] = $uidata;
            $json['waktu'] = ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
            $json['nomor'] = date("dmy-his")."-".substr(md5($user->getNameOnly()."-".$_POST['id_karyawan']), 5,rand(2, 5));
            $ft = ArrayAdapter::format("date d M Y", $_POST['dari']);
            $to = ArrayAdapter::format("date d M Y", $_POST['sampai']);	
        
            $json['keterangan_perawat'] = "Pembayaran Renumerasi Tindakan Keperawatan ".$_POST['nama_karyawan']." Tanggal ".$ft." - ".$to;
            $json['nilai_perawat'] = $adapter->getTerimaKaryawan();
            $json['pembilang_perawat'] = numbertell($adapter->getTerimaKaryawan())." Rupiah";
            $json['keterangan_accounting'] = "Penerimaan Bagian RS (Pajak + Dana Pengembangan ) Atas Tindakan Keperawatan ".$_POST['nama_karyawan']." Tanggal ".$ft." - ".$to;
            $json['nilai_accounting'] = $adapter->getTerimaRS();
            $json['pembilang_accounting'] = numbertell($adapter->getTerimaRS())." Rupiah";
        
            $json['json'] = json_encode($uidata);
            $json['status'] = 1;
        
            $pack = new ResponsePackage();
            $pack->setContent($json);
            $pack->setStatus(ResponsePackage::$STATUS_OK);
            echo json_encode($pack->getPackage());
        }else{         

            $column      = array();
            $column['A'] = "Tanggal";
            $column['B'] = "Ruangan";
            $column['C'] = "Pasien";
            $column['D'] = "Tindakan";
            $column['E'] = "Persentase";
            $column['F'] = "Biaya Awal";
            $column['G'] = "Bagian Karyawan";
            $column['H'] = "Bagian RS";
            $column['I'] = "Pajak";
            $column['J'] = "Dana Pengembangan";
            
            require_once "finance/class/ExcelDetail.php";
            $excel = new ExcelDetail("SURAT BUKTI PENERIMAAN TINDAKAN PERAWAT",$_POST['dari'],$_POST['sampai'],$_POST['pajak'],$column,$adapter->getTerimaKaryawan());
            $excel->createExcel($uidata);
        }
		
		return;
	}

	echo $form->getHtml();
	echo "<div class='clear'></div>";
	echo "<div id='result_pvtk'>".$table->getHtml()."</div>";
	echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
	echo addJS("framework/smis/js/table_action.js");
	echo addCSS("framework/bootstrap/css/datepicker.css");	
?>
<script type="text/javascript">
	function PVTKAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PVTKAction.prototype.constructor = PVTKAction;
	PVTKAction.prototype = new TableAction();
	PVTKAction.prototype.addRegulerData = function(data) {
		data['id_karyawan'] = $("#pvtk_id_karyawan").val();
		data['nama_karyawan'] = $("#pvtk_nama_karyawan").val();
		data['jabatan_karyawan'] = $("#pvtk_jabatan_karyawan").val();
		data['nip_karyawan'] = $("#pvtk_nip_karyawan").val();
		data['dari'] = $("#pvtk_dari").val();
		data['sampai'] = $("#pvtk_sampai").val();
		data['pajak'] = $("#pvtk_pajak").val();
        data['type'] = $("#pvtk_type").val();

		$("#info_pvtk_nama").html(data['nama_karyawan']);
		$("#info_pvtk_jabatan").html(data['jabatan_karyawan']);
		$("#info_pvtk_nip").html(data['nip_karyawan']);
		$("#info_pvtk_dari").html(getFormattedDate(data['dari']));
		$("#info_pvtk_sampai").html(getFormattedDate(data['sampai']));
		$("#info_pvtk_pajak").html(data['pajak'] + "%");			
		return data;
	};
	PVTKAction.prototype.afterview = function(json) {
		if (json != null) {			
			$("#info_pvtk_nomor").html(json.nomor);
			$("#info_pvtk_waktu").html(json.waktu);
			$("#pvtk_table_list").html(json.list);
			$("#pvtk_numbertell").html(json.pembilang_perawat);
			$("#penerima_pv_tindakan_keperawatan").html($("#pvtk_nama_karyawan").val());
			pvtk_data = json;
		}
	};
	PVTKAction.prototype.print_karyawan = function() {
		var r = this.getRegulerData();
		r['command'] = "save";
		r['id'] = "";
		r['super_command'] = "cair";
		r['jenis'] = "pembayaran_renumerasi_keperawatan";
		r['keterangan'] = pvtk_data.keterangan_perawat;
		r['json'] = pvtk_data.json;
		r['noref'] = "EMP-" + pvtk_data.nomor;
		r['nilai'] = pvtk_data.nilai_perawat;			
		r['status'] = "1";

		$("#untuk_table").html(" RENUMERASI TINDAKAN KEPERAWATAN ");
		$("#info_pvtk_nomor").html(r['noref']);
		$("#pvtk_numbertell").html(pvtk_data.pembilang_perawat);
		$("#penerima_pv_tindakan_keperawatan").html($("#pvtk_nama_karyawan").val());		
		$("#bagian_karyawan").css("font-weight","800");
		$("#bagian_accounting").css("font-weight","100");
		r['html']=$("#result_pvtk").html();
		
		showLoading();
		$.post("",r,function(res){
			getContent(res);
			dismissLoading();
			smis_print($("#result_pvtk").html());
		});
	};
	PVTKAction.prototype.print_akuntan = function() {
		var r = this.getRegulerData();
		r['command'] = "save";
		r['id'] = "";
		r['super_command'] = "cair";
		r['jenis'] = "penerimaan_pajak_dana_pengembangan";
		r['keterangan'] = pvtk_data.keterangan_accounting;
		r['json'] = pvtk_data.json;
		r['noref'] = "ACC-" + pvtk_data.nomor;
		r['nilai'] = pvtk_data.nilai_accounting;			
		r['status'] = "1";

		$("#untuk_table").html(" PAJAK DAN DANA PENGEMBANGAN DARI TINDAKAN KEPERAWATAN ");
		$("#info_pvtk_nomor").html(r['noref']);		
		$("#pvtk_numbertell").html(pvtk_data.pembilang_accounting);
		$("#penerima_pv_tindakan_keperawatan").html($("#pvtk_nama_akuntan").val());
		$("#bagian_karyawan").css("font-weight","100");
		$("#bagian_accounting").css("font-weight","800");
		r['html']=$("#result_pvtk").html();
		
		showLoading();
		$.post("",r,function(res){
			getContent(res);
			dismissLoading();
			smis_print($("#result_pvtk").html());
		});
	};

	function PVTKKaryawanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PVTKKaryawanAction.prototype.constructor = PVTKKaryawanAction;
	PVTKKaryawanAction.prototype = new TableAction();
	PVTKKaryawanAction.prototype.selected = function(json) {
		$("#pvtk_id_karyawan").val(json.id);
		$("#pvtk_nip_karyawan").val(json.nip);
		$("#pvtk_nama_karyawan").val(json.nama);
		$("#pvtk_jabatan_karyawan").val(json.nama_jabatan);
		pvtk_data = json;
	};

	function PVTKAkuntanAction(name, page, action, column) {
		this.initialize(name, page, action, column);
	}
	PVTKAkuntanAction.prototype.constructor = PVTKAkuntanAction;
	PVTKAkuntanAction.prototype = new TableAction();
	PVTKAkuntanAction.prototype.selected = function(json) {
		$("#pvtk_id_akuntan").val(json.id);
		$("#pvtk_nama_akuntan").val(json.nama);
	};
	
	var pvtk;
	var pvtk_data;
	var pv_tindakan_keperawatan_karyawan;
	var pv_tindakan_keperawatan_akuntan;
	$(document).ready(function() {
		$('.mydate').datepicker();
		pv_tindakan_keperawatan_karyawan = new PVTKKaryawanAction(
			"pv_tindakan_keperawatan_karyawan",
			"finance",
			"pv_tindakan_keperawatan",
			new Array()
		);
		pv_tindakan_keperawatan_karyawan.setSuperCommand("pv_tindakan_keperawatan_karyawan");
		pv_tindakan_keperawatan_akuntan = new PVTKAkuntanAction(
			"pv_tindakan_keperawatan_akuntan",
			"finance",
			"pv_tindakan_keperawatan",
			new Array()
		);
		pv_tindakan_keperawatan_akuntan.setSuperCommand("pv_tindakan_keperawatan_akuntan");
		pvtk = new PVTKAction(
			"pvtk",
			"finance",
			"pv_tindakan_keperawatan",
			new Array()
		);
	});
</script>
