<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="cair"){
	$dbtab=new DBTable($db, "smis_fnc_bayar");
	$dbres=new DBResponder($dbtab, new Table(array()), new SimpleAdapter());
	$dbres->addColumnFixValue("waktu", date("Y-m-d"));
	$res=$dbres->command("save");
	echo json_encode($res);
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']!="") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	
	$dktable=new Table($head);
	$dktable->setName("pv_konsul_karyawan");
	$dktable->setModel(Table::$SELECT);	
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"dokter");
	
	$actable=new Table($head);
	$actable->setName("pv_konsul_accounting");
	$actable->setModel(Table::$SELECT);
	$accounting=new EmployeeResponder($db,$actable,$dkadapter,"finance");
	
	$super=new SuperCommand();
	$super->addResponder("pv_konsul_karyawan",$karyawan);
	$super->addResponder("pv_konsul_accounting",$accounting);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

$header=array("Tanggal","Ruangan","Pasien","Sebagai","Biaya Awal","Persentase","Bagian Dokter","Bagian RS","Pajak","Dana Pengembangan");
$uitable=new Table($header);
$head00="<tr>
		<th colspan='30' id='surat_bukti'>SURAT BUKTI PENERIMAAN <font id='untuk_table_pv_konsul'></font></th>
		</tr>";
$head0="<tr>
		<th>Nomor</th>
		<th colspan='5' id='kode_table_pv_konsul'></th>
		<th>Waktu</th>
		<th colspan='10' id='waktu_table_pv_konsul'></th>
		</tr>";
$head1="<tr>
		<th>Nama</th>
		<th colspan='2' id='nama_table_pv_konsul'></th>
		<th>Jabatan</th>
		<th colspan='2' id='jabatan_table_pv_konsul'></th>
		<th>NIP</th>
		<th colspan='20' id='nip_table_pv_konsul'></th>
		</tr>";
$head2="<tr>
		<th >Dari</th>
		<th colspan='2' id='dari_table_pv_konsul'></th>
		<th >Sampai</th>
		<th colspan='2' id='sampai_table_pv_konsul'></th>
		<th >Pajak</th>
		<th id='pajak_table_pv_konsul' colspan='20'></th>
		</tr>";


$uitable->addHeader("before", $head00)
		->addHeader("before", $head0)
		->addHeader("before", $head1)
		->addHeader("before", $head2)
		->setName("pv_konsul")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) &&  ( $_POST['command'] == "list" || $_POST['command'] == "excel" ) ) {
	require_once 'finance/class/adapter/ProvitShareAdapter.php';
	loadLibrary("smis-libs-function-math");
	$service=new ServiceConsumer($db, "get_pv_konsul");
	$service->setMode(ServiceConsumer::$CLEAN_BOTH)
			->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->addData("id_karyawan", $_POST['id_karyawan']);
	$content=$service->execute()->getContent();
    $adapter=new ProvitShareAdapter($_POST['pajak']);
    $adapter    ->setExcel($_POST['command']=="excel");
	$uidata=$adapter->add("Tanggal", "waktu")
					->add("Pasien", "pasien")
					//->add("Ruangan", "ruangan","unslug")
					->add("Sebagai", "sebagai")
					->add("Persentase", "percentage")
					->add("Biaya Awal", "asli","money Rp.")
					->add("Bagian Dokter", "nilai","money Rp.")
					->setUseID(false)
					->getContent($content);
    if($_POST['command']=="list"){
        $body0="<tr>
                <td>Terbilang</td>
                <td colspan='20' id='numbertell'></td>
            </tr>";
        $body1="<tr>
            <td colspan='5'>Penerima</td>
            <td colspan='5'>Pencetak</td>
        </tr>";
        $body2="<tr>
                <td colspan='5'></br></br></br></br> ( <strong id='penerima_table_pv_konsul'></strong> ) </td>
                <td colspan='5'></br></br></br></br> ( <strong >".$user->getNameOnly()."</strong> ) </td>
            </tr>
            ";

        $list=$uitable->setContent($uidata)
                ->addBody("after", $body0)
                ->addBody("after", $body1)
                ->addBody("after", $body2)
                ->getBodyContent();
        $json['list']=$list;
        $json['pagination']="";
        $json['number']="0";
        $json['number_p']="0";
        $json['data']=$uidata;
        $json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
        $json['nomor']="KSL-".date("dmy-his")."-".substr(md5($user->getNameOnly()."-".$_POST['id_karyawan']), 5,rand(2, 5));
        $ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
        $to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	

        $json['keterangan_dokter']="Pembayaran Gaji Tindakan ".$_POST['jabatan_karyawan']." ".$_POST['nama_karyawan']." Tanggal ".$ft." - ".$to;
        $json['nilai_dokter']=$adapter->getTerimaDokter();
        $json['pembilang_dokter']=numbertell($adapter->getTerimaDokter())." Rupiah";
        $json['keterangan_accounting']="Penerimaan Bagian RS (Pajak + Dana Pengembangan ) Atas Tindakan ".$_POST['jabatan_karyawan']." ".$_POST['nama_karyawan']." Tanggal ".$ft." - ".$to;
        $json['nilai_accounting']=$adapter->getTerimaRS();
        $json['pembilang_accounting']=numbertell($adapter->getTerimaRS())." Rupiah";

        $json['json']=json_encode($uidata);
        $json['status']=1;

        $pack=new ResponsePackage();
        $pack->setContent($json);
        $pack->setStatus(ResponsePackage::$STATUS_OK);
        echo json_encode($pack->getPackage());
    }else{
        $column      = array();
        $column['A'] = "Tanggal";
        $column['B'] = "Ruangan";
        $column['C'] = "Pasien";
        $column['D'] = "Tindakan";
        $column['E'] = "Persentase";
        $column['F'] = "Biaya Awal";
        $column['G'] = "Bagian Dokter";
        $column['H'] = "Bagian RS";
        $column['I'] = "Pajak";
        $column['J'] = "Dana Pengembangan";
        
        require_once "finance/class/ExcelDetail.php";
        $excel = new ExcelDetail("SURAT BUKTI PENERIMAAN KONSUL DOKTER",$_POST['dari'],$_POST['sampai'],$_POST['pajak'],$column,$adapter->getTerimaDokter());
        $excel->createExcel($uidata);
    }

	return;
}

$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$uitable->addModal("id_karyawan", "hidden", "", "")
		->addModal("nama_karyawan", "chooser-pv_konsul-pv_konsul_karyawan-Karyawan", "Karyawan", "")
		->addModal("jabatan_karyawan", "text", "Jabatan", "")
		->addModal("nip_karyawan", "text", "NIP", "")
		->addModal("nama_accounting", "chooser-pv_konsul-pv_konsul_accounting-Accounting", "Accounting", "")
		->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("pajak", "text", "Pajak (%) ", $pajak);
$action=new Button("","","");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("pv_konsul.view()");
$print_acc=new Button("","","Accounting");
$print_acc->setClass("btn-primary")
	  	  ->setIsButton(Button::$ICONIC_TEXT)
	  	  ->setIcon("fa fa-print")
	  	  ->setAction("pv_konsul.print_acc()");
$print_emp=new Button("","","Karyawan");
$print_emp->setClass("btn-primary")
	  	  ->setIsButton(Button::$ICONIC_TEXT)
	  	  ->setIcon("fa fa-print")
	  	  ->setAction("pv_konsul.print_gaji()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action)
		  ->addButton($print_emp)
		  ->addButton($print_acc);
$form=$uitable
	  ->getModal()
	  ->setTitle("Tindakan")
	  ->getForm()
      ->addElement("",$btn_froup);
      
$excel = new Button("","","Excel");
$excel ->setIsButton(Button::$ICONIC_TEXT);
$excel ->setIcon("fa fa-file-excel");
$excel ->setClass("btn btn-primary");
$excel ->setAction("pv_konsul.excel()");

$form->addElement("", $excel);
echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_pv_konsul'>".$uitable->getHtml()."</div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">
	var pv_konsul;
	var pv_konsul_karyawan;
	var pv_konsul_accounting;
	var pv_konsul_data;
	$(document).ready(function(){
		$('.mydate').datepicker();
		pv_konsul=new TableAction("pv_konsul","finance","pv_konsul",new Array());
		pv_konsul_karyawan=new TableAction("pv_konsul_karyawan","finance","pv_konsul",new Array());
		pv_konsul_karyawan.setSuperCommand("pv_konsul_karyawan");
		pv_konsul_karyawan.selected=function(json){
			$("#pv_konsul_nip_karyawan").val(json.nip);
			$("#pv_konsul_nama_karyawan").val(json.nama);
			$("#pv_konsul_jabatan_karyawan").val(json.nama_jabatan);
			$("#pv_konsul_id_karyawan").val(json.id);
		};

		pv_konsul_accounting=new TableAction("pv_konsul_accounting","finance","pv_konsul",new Array());
		pv_konsul_accounting.setSuperCommand("pv_konsul_accounting");
		pv_konsul_accounting.selected=function(json){
			$("#pv_konsul_nama_accounting").val(json.nama);
		};

		pv_konsul.addRegulerData=function(data){
			data['id_karyawan']=$("#pv_konsul_id_karyawan").val();
			data['nama_karyawan']=$("#pv_konsul_nama_karyawan").val();
			data['jabatan_karyawan']=$("#pv_konsul_jabatan_karyawan").val();
			data['dari']=$("#pv_konsul_dari").val();
			data['sampai']=$("#pv_konsul_sampai").val();
			data['pajak']=$("#pv_konsul_pajak").val();

			$("#nama_table_pv_konsul").html(data['nama_karyawan']);
			$("#jabatan_table_pv_konsul").html(data['jabatan_karyawan']);
			$("#dari_table_pv_konsul").html(getFormattedDate(data['dari']));
			$("#sampai_table_pv_konsul").html(getFormattedDate(data['sampai']));
			$("#nip_table_pv_konsul").html($("#pv_konsul_nip_karyawan").val());
			$("#pajak_table_pv_konsul").html(data['pajak']+"%");			
			return data;
		};

		pv_konsul.afterview=function(json){
			if(json!=null){
				$("#kode_table_pv_konsul").html(json.nomor);
				$("#waktu_table_pv_konsul").html(json.waktu);
				pv_konsul_data=json;
			}
		};


		
		
		pv_konsul.print_gaji=function(){
			var r=this.getRegulerData();
			r['command']="save";
			r['id']="";
			r['super_command']="cair";
			r['jenis']="pembayaran_gaji";
			r['keterangan']=pv_konsul_data.keterangan_dokter;
			r['json']=pv_konsul_data.json;
			r['noref']="EMP-"+pv_konsul_data.nomor;
			r['nilai']=pv_konsul_data.nilai_dokter;			
			r['status']="1";

			$("#untuk_table_pv_konsul").html(" UANG KONSUL");
			$("#bagian_dokter").css("font-weight","800");
			$("#bagian_accounting").css("font-weight","100");
			
			$("#kode_table_pv_konsul").html(r['noref']);
			$("#numbertell").html(pv_konsul_data.pembilang_dokter);
			$("#penerima_table_pv_konsul").html($("#pv_konsul_nama_karyawan").val());			
			r['html']=$("#result_pv_konsul").html();
			
			showLoading();
			$.post("",r,function(res){
				getContent(res);
				dismissLoading();
				smis_print($("#result_pv_konsul").html());
			});
			
		};

		pv_konsul.print_acc=function(){
			var r=this.getRegulerData();
			r['command']="save";
			r['id']="";
			r['super_command']="cair";
			r['jenis']="penerimaan_pajak_dana_pengembangan";
			r['keterangan']=pv_konsul_data.keterangan_accounting;
			r['json']=pv_konsul_data.json;
			r['noref']="ACC-"+pv_konsul_data.nomor;
			r['nilai']=pv_konsul_data.nilai_accounting;
			r['status']="1";

			$("#untuk_table_pv_konsul").html(" PAJAK DAN DANA PENGEMBANGAN DARI KONSUL");
			$("#bagian_dokter").css("font-weight","100");
			$("#bagian_accounting").css("font-weight","800");
			
			$("#kode_table_pv_konsul").html(r['noref']);
			$("#penerima_table_pv_konsul").html($("#pv_konsul_nama_accounting").val());
			$("#numbertell").html(pv_konsul_data.pembilang_accounting);
			r['html']=$("#result_pv_konsul").html();
			
			showLoading();
			$.post("",r,function(res){
				getContent(res);
				dismissLoading();
				smis_print($("#result_pv_konsul").html());
			});
			
		};
		
				
	});
</script>