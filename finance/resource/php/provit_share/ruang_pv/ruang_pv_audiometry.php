<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';



$header=array("Tanggal","Pasien","Sebagai","Nilai");
$uitable=new Table($header);
$head2="<tr>
		<th >Dari</th>
		<th colspan='2' id='dari_table_ruang_pv_audiometry'></th>
		<th >Sampai</th>
		<th colspan='2' id='sampai_table_ruang_pv_audiometry'></th>
		<th >Ruang</th>
		<th id='ruang_table_ruang_pv_audiometry' colspan='20'></th>
		</tr>";


$uitable->addHeader("before", $head2)
		->setName("ruang_pv_audiometry")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']== "list") {	
	require_once 'finance/class/adapter/ProvitShareAdapter.php';
	loadLibrary("smis-libs-function-math");
	$ruang=$_POST['ruang'];
	$service=new ServiceConsumer($db, "get_pv_audiometry",NULL,$ruang);
	$service->setMode(ServiceConsumer::$CLEAN_BOTH)
			->addData("dari", $_POST['dari'])
			->addData("sampai", $_POST['sampai'])
			->addData("id_karyawan", "-1");
	$content=$service->execute()->getContent();
	$adapter=new ProvitShareAdapter($_POST['pajak']);
	$uidata=$adapter->add("Tanggal", "waktu")
					->add("Pasien", "pasien")
					->add("Sebagai", "sebagai")
					->add("Nilai", "asli","money Rp.")
					->setUseID(false)
					->getContent($content);
	

	
	$list=$uitable->setContent($uidata)
				  ->getBodyContent();
	$json['list']=$list;
	$json['pagination']="";
	$json['number']="0";
	$json['number_p']="0";
	$json['data']=$uidata;
	$json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
	$json['nomor']="AUD-".date("dmy-his")."-".substr(md5($user->getNameOnly()."-".$_POST['id_karyawan']), 5,rand(2, 5));
	$ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
	$to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	
	
	$json['json']=json_encode($uidata);
	$json['status']=1;
	
	$pack=new ResponsePackage();
	$pack->setContent($json);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}


$uitable->addModal("id_karyawan", "hidden", "", "")
		->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("ruang", "select", "Ruang", "");
$action=new Button("","","");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("ruang_pv_audiometry.view()");

$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action)
		  ->addButton($print_emp)
		  ->addButton($print_acc);
$form=$uitable
	  ->getModal()
	  ->setTitle("Tindakan")
	  ->getForm()
	  ->addElement("",$btn_froup);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_ruang_pv_audiometry'>".$uitable->getHtml()."</div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">
	var ruang_pv_audiometry;
	var ruang_pv_audiometry_karyawan;
	var ruang_pv_audiometry_accounting;
	var ruang_pv_audiometry_data;
	$(document).ready(function(){
		$('.mydate').datepicker();
		ruang_pv_audiometry=new TableAction("ruang_pv_audiometry","finance","ruang_pv_audiometry",new Array());
		ruang_pv_audiometry_karyawan=new TableAction("ruang_pv_audiometry_karyawan","finance","ruang_pv_audiometry",new Array());
		ruang_pv_audiometry_karyawan.setSuperCommand("ruang_pv_audiometry_karyawan");
		ruang_pv_audiometry_karyawan.selected=function(json){
			$("#ruang_pv_audiometry_nip_karyawan").val(json.nip);
			$("#ruang_pv_audiometry_nama_karyawan").val(json.nama);
			$("#ruang_pv_audiometry_jabatan_karyawan").val(json.nama_jabatan);
			$("#ruang_pv_audiometry_id_karyawan").val(json.id);
		};

		ruang_pv_audiometry_accounting=new TableAction("ruang_pv_audiometry_accounting","finance","ruang_pv_audiometry",new Array());
		ruang_pv_audiometry_accounting.setSuperCommand("ruang_pv_audiometry_accounting");
		ruang_pv_audiometry_accounting.selected=function(json){
			$("#ruang_pv_audiometry_nama_accounting").val(json.nama);
		};

		ruang_pv_audiometry.addRegulerData=function(data){
			data['id_karyawan']=$("#ruang_pv_audiometry_id_karyawan").val();
			data['nama_karyawan']=$("#ruang_pv_audiometry_nama_karyawan").val();
			data['jabatan_karyawan']=$("#ruang_pv_audiometry_jabatan_karyawan").val();
			data['dari']=$("#ruang_pv_audiometry_dari").val();
			data['sampai']=$("#ruang_pv_audiometry_sampai").val();
			data['pajak']=$("#ruang_pv_audiometry_pajak").val();

			$("#nama_table_ruang_pv_audiometry").html(data['nama_karyawan']);
			$("#jabatan_table_ruang_pv_audiometry").html(data['jabatan_karyawan']);
			$("#dari_table_ruang_pv_audiometry").html(getFormattedDate(data['dari']));
			$("#sampai_table_ruang_pv_audiometry").html(getFormattedDate(data['sampai']));
			$("#nip_table_ruang_pv_audiometry").html($("#ruang_pv_audiometry_nip_karyawan").val());
			$("#pajak_table_ruang_pv_audiometry").html(data['pajak']+"%");			
			return data;
		};

		ruang_pv_audiometry.afterview=function(json){
			if(json!=null){
				$("#kode_table_ruang_pv_audiometry").html(json.nomor);
				$("#waktu_table_ruang_pv_audiometry").html(json.waktu);
				ruang_pv_audiometry_data=json;
			}
		};


		
		
		
		
				
	});
</script>