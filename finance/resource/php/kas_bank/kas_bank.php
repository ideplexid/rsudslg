<?php
    
    if(isset($_POST['super_command']) && $_POST['super_command']=="detail_kas_bank"){
        require_once "";
        return;
    }
    
    require_once "smis-base/smis-include-service-consumer.php";    
    require_once "smis-libs-class/MasterTemplate.php";    
    require_once "smis-libs-class/MasterServiceTemplate.php"; 
    require_once "smis-libs-class/MasterSlaveServiceTemplate.php"; 
   
    $dbtable = new DBTable($db,"smis_fnc_akun");
    $kb      = $dbtable->select(substr($_POST['action'],9));
    $title   = $kb->nomor_akun." - ".$kb->nama_akun;
    $detail  = new Button("","","Detail");
    $detail  ->setIcon("fa fa-list-alt")
             ->setClass("btn btn-primary")
             ->setIsButton(Button::$ICONIC);
    $jenis   = new OptionBuilder();
    $jenis   ->add("Masuk","1","1");
    $jenis   ->add("Keluar","0","1");
    
    $view    = new Button("","","Tampil");
    $view    ->setIcon("fa fa-search")
             ->setClass("btn btn-primary")
             ->setAction($_POST['action'].".view()")
             ->setIsButton(Button::$ICONIC);
    $grup=array();    
    if(!isset($_POST['command'])){
        $serv=new ServiceConsumer($db,"get_all_grup",NULL,"accounting");
        $serv->execute();
        $grup=$serv->getContent();
    }
    
    $master = new MasterSlaveServiceTemplate($db,"input_kas_bank","finance",$_POST['action']);
    $master ->getUItable()
            ->addModal("dari","datetime","Dari","","y",null,false)
            ->addModal("sampai","datetime","Sampai","","y",null,false)
            ->addModal("saldo","money","Saldo","","y",null,true)
            ->addModal("akun","hidden","",$kb->nomor_akun,"y",null,true);
    $master ->addNoClear("dari")
            ->addNoClear("sampai")
            ->addNoClear("saldo")
            ->addNoClear("akun")
            ->addRegulerData("dari","dari","id-value")
            ->addRegulerData("sampai","sampai","id-value");
    $master ->getForm()
            ->addElement("",$view)
            ->setTitle($title);
    $master ->getUItable()
            ->clearContent();
    $master ->getServiceResponder()
            ->addData("kode_akun",$kb->nomor_akun);
    $master ->addJSColumn("id",true)
            ->addJSColumn("tanggal",false)
            ->addJSColumn("nomor",false)
            ->addJSColumn("keterangan",false)
            ->addJSColumn("grup",false)
            ->addJSColumn("dari",false)
            ->addJSColumn("sampai",false)
            ->addJSColumn("saldo",false);
    $master ->setDateTimeEnable(true);
    $master ->getUItable()
            ->addContentButton("detail_transaksi",$detail)
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false)
            ->setHeader(array("No.","Tanggal","Nomor","Grup","Keterangan","Debet","Kredit"));
    $master ->getUItable()
            ->addModal("id","hidden","","")
            ->addModal("nomor","text","Nomor","")
            ->addModal("tanggal","datetime","Tanggal",date("Y-m-d H:i:s"))
            ->addModal("keterangan","textarea","Keterangan","")
            ->addModal("grup","select","Grup",$grup)
            ->addModal("jenis","hidden","","memorial");
            //->addModal("io","select","Jenis",$jenis->getContent());
    $master ->getAdapter()
            ->setUseNumber(true,"No.","back.")
            ->add("Tanggal","tanggal","date d M Y H:i")
            ->add("Nomor","nomor")
            ->add("Grup","grup","unslug")
            ->add("Keterangan","keterangan")
            ->add("Debet","debet","money Rp.")
            ->add("Kredit","kredit","money Rp.")
            ->add("Jenis","io","trivial_1_Masuk_Keluar");
    $master ->setTableActionClass("KasBankAction");
    $master ->setModalTitle("Kas & Bank");
    $master ->addResouce("js","finance/resource/js/kas_bank.js","before");
    $master ->setAutoReload(true);
    $master ->initialize();
?>