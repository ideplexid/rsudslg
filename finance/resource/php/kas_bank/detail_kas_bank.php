<?php
global $db;
require_once("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'finance/class/table/DetailKasBankTable.php';

$label   = new Button("","","");
$label   ->addClass("btn-primary")
         ->setIsButton(Button::$ICONIC)
         ->setIcon("fa fa-trello");
$header  = array ("No.","Nomor",'Account',"Keterangan","Debet","Kredit");
$detail  = new MasterSlaveServiceTemplate($db, "input_detail_kas_bank", "finance", $_POST['action']);
$detail  ->setEntity("accounting");
$detail  ->getUItable()
         ->setHeader($header);
$uitable = new DetailKasBankTable($header);
$uitable ->setName($_POST['action']);
$uitable ->setCurrentAccount($_POST['proceed_akun']);
$uitable ->setReloadButtonEnable(false);
$uitable ->setFooterVisible(false);
$detail  ->setUITable($uitable);

if( isset($_POST['command']) && $_POST['command']=="list" ){	
	$detail ->getServiceResponder()
            ->addData("proceed_akun",$_POST['proceed_akun'])
            ->addData("kode_akun",$_POST['kode_akun'])	
            ->addData("nama_akun",$_POST['nama_akun'])	
	        ->addData("id_transaksi",$_POST['id_transaksi'])
            ->addData("ket",$_POST['ket']);
}

$adapt   = new SummaryAdapter();
$adapt	 ->setUseNumber(true, "No.","back.")
		 ->add("Nomor", "nomor_account")
		 ->add("Account", "nama_account")
		 ->add("Keterangan", "keterangan")
		 ->add("Debet","debet","money Rp.")
		 ->add("Kredit","kredit","money Rp.")
         ->addSummary("Debet","debet","money Rp.")
         ->addSummary("Kredit","kredit","money Rp.")
         ->addFixValue("Tanggal","<strong>Total</strong>");
$detail  ->setAdapter($adapt);
       
$super_cmd = "dkb_".$_POST['action'];
            
if(!isset($_POST['command'])){
    $jenis     = new OptionBuilder();
    $jenis     ->add("Masuk","1",$_POST['io']=="1"?"1":"0");
    $jenis     ->add("Keluar","0",$_POST['io']=="0"?"1":"0");
    $dbtable   = new DBTable($db,"smis_fnc_akun");
    $kb        = $dbtable->select(substr($_POST['action'],16));
    
    
	$detail ->getUItable()
			->addModal("id_transaksi","hidden","",$_POST['id_transaksi'],"n",null,true)
            ->addModal("kode_akun","hidden","",$kb->id,"n",null,true)
            ->addModal("nama_akun","hidden","",$kb->nama_akun,"n",null,true)
            ->addModal("proceed_akun","hidden","",$kb->nomor_akun,"n",null,true)
            ->addModal("nomor","text","Nomor",$_POST['nomor'],"y",null,true)
            ->addModal("tanggal","datetime","Tanggal",$_POST['tanggal'],"y",null,true)
            ->addModal("grup","text","Grup",$_POST['grup'],"y",null,true)
            ->addModal("ket","text","Keterangan",$_POST['keterangan'],"y",null,true);
	$detail ->addNoClear("id_transaksi")
            ->addNoClear("kode_akun")
            ->addNoClear("tanggal")
            ->addNoClear("grup");
    $detail ->addJSColumn("id",true)
            ->addJSColumn("nomor_account",false)
            ->addJSColumn("nama_account",false)
            ->addJSColumn("keterangan",false)
            ->addJSColumn("debet",false)
            ->addJSColumn("kredit",false);
    $btn    = new Button("","","Kembali");
	$btn    ->addClass("btn-primary")
            ->setIcon("fa fa-backward")
            ->setAction($_POST['action'].".backward();")
            ->setIsButton(Button::$ICONIC_TEXT);
	$detail	->getForm()
			->addElement("",$btn);
	$detail ->getUItable()
            ->clearContent();
    $detail ->getUItable()
			->addModal("id","hidden","")
            ->addModal("nama_account","chooser-".$_POST['action']."-dkb_".$_POST['action']."-Akun","Nama Akun","","n",null,true)
            ->addModal("nomor_account","text","Kode Akun","","n",null,true)
            ->addModal("keterangan","textarea","Keterangan")
            ->addModal("debet","money","Debet")
            ->addModal("kredit","money","Kredit");    
    $detail ->addRegulerData("id_transaksi","id_transaksi","id-value")
            ->addRegulerData("grup","grup","id-value")
            ->addRegulerData("kode_akun","kode_akun","id-value")
            ->addRegulerData("nama_akun","nama_akun","id-value")
            ->addRegulerData("ket","ket","id-value")
            ->addRegulerData("proceed_akun","proceed_akun","id-value");
    $detail ->setModalTitle($kb->nomor_akun." - ".$kb->nama_akun);
    $detail ->setTableActionClass("DetailKasBankAction");
    $detail ->addResouce("js","finance/resource/js/detail_kas_bank.js","before");
    $detail ->setAutoReload(true);
}

if(isset($_POST['super_command']) && $_POST['super_command']!="" || !isset($_POST['command'])){
    $adapter   = new SimpleAdapter();
    $adapter   ->setUseNumber(true,"No.","back.")
               ->add("Nama","nama")
               ->add("Kode","nomor")
               ->add("Klas","klas")
               ->add("Sub","sub");
    $uitable   = new Table(array("No.","Nama","Kode","Klas","Sub"));
    $uitable   ->setName($super_cmd)
               ->setModel(Table::$SELECT);
    $responder = new ServiceResponder($db,$uitable,$adapter,"get_account","accounting");
    $detail    ->getSuperCommand()
               ->addResponder($super_cmd,$responder);
    $detail    ->addSuperCommand($super_cmd,array());
    $detail    ->addSuperCommandArray($super_cmd,"nama_account","nama")
               ->addSuperCommandArray($super_cmd,"nomor_account","nomor");
}

$detail ->initialize();
?>

