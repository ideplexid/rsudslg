<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
$settings = new SettingsBuilder ( $db, "finance", "finance", "map_pembayaran_umum", "Pembayaran Umum" );
$settings->setShowDescription ( true );
$settings->addTabs ( "accounting", "Mapping Accounting","fa fa-list-alt" );
if($settings->isGroup("accounting")){
    $settings->addItem ( "accounting", new SettingsItem ( $db, "finance-accounting-debet-pembayaran-umum", "Kode Debet Pembayaran Umum", "", "chooser-map_pembayaran_umum-debet_map_umum-Debet", "" ) );
    $settings->addItem ( "accounting", new SettingsItem ( $db, "finance-accounting-kredit-pembayaran-umum", "Kode Kredit Pembayaran Umum", "", "chooser-map_pembayaran_umum-kredit_map_umum-Kredit", "" ) );
    $settings->addSuperCommandAction("debet_map_umum","kode_akun");
    $settings->addSuperCommandAction("kredit_map_umum","kode_akun");
    $settings->addSuperCommandArray("debet_map_umum","finance-accounting-debet-pembayaran-umum","nomor",true);
    $settings->addSuperCommandArray("kredit_map_umum","finance-accounting-kredit-pembayaran-umum","nomor",true);
}
$settings->setPartialLoad(true);
$settings->init ();
?>