<?php 
global $db;
global $user;

require_once "smis-base/smis-include-service-consumer.php";
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$serv=new ServiceConsumer($db,"employee",NULL,"hrd");
    $serv->addData("command","count");
    $serv->execute();
    $result=$serv->getContent();
    $max_data=$result;
    
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    $pack->setContent($max_data);
    echo json_encode($pack->getPackage());
    return;
}

$header = array('No.',"Nama","NIP","Debet",'Kredit');
$uitable= new Table($header);
$uitable->setName("mapping_karyawan");

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
    $serv=new ServiceConsumer($db,"employee",NULL,"hrd");
    $serv->addData("command","list");
    $serv->addData("kriteria","");
    $serv->addData("number",$_POST['current']);
    $serv->addData("max","1");
    $serv->execute();
    $result=$serv->getContent();
    $content=$result['data'];
    $dbtable=new DBTable($db,"smis_fnc_mapping_employee");
    foreach($content as $x){
        $save=array();
        $save['nama_employee']=$x['nama'];
        $save['nip_employee']=$x['nip'];
        $save['id_employee']=$x['id'];
        $idx['id_employee']=$x['id'];
        $dbtable->insertOrUpdate($save,$idx);
    }
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    echo json_encode($pack->getPackage());
    return;
}


if(isset($_POST['command']) && $_POST['command']!=""){
    $adapter=new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Nama","nama_employee");
    $adapter->add("NIP","nip_employee");
    $adapter->add("Debet","debet");
    $adapter->add("Kredit","kredit");
    $dbtable=new DBTable($db,"smis_fnc_mapping_employee");
    $responder=new DBResponder($dbtable,$uitable,$adapter);
    $data=$responder->command($_POST['command']);
    if($data!=null){
        echo json_encode($data);   
    }
    return;
}

$uitable->addModal("id","hidden","","");
$uitable->addModal("nama_employee","text","Nama","",'n',null,true);
$uitable->addModal("debet","chooser-mapping_karyawan-mapping_karyawan_debet-Debet","Debet","");
$uitable->addModal("kredit","chooser-mapping_karyawan-mapping_karyawan_kredit-Kredit","Kredit","");

$clone=new Button("","","Download");
$clone->setIsButton(Button::$ICONIC_TEXT);
$clone->setIcon(" fa fa-download");
$clone->setClass("btn-primary");
$clone->setAction("mapping_karyawan.rekaptotal()");

$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setDelButtonEnable(false);
$uitable->addHeaderButton($clone);

$modal=$uitable->getModal();
$modal->setTitle("Mapping Karyawan");

echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/smis/js/table_action.js");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("finance/resource/js/mapping_karyawan.js",false);
?>