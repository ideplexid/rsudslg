<?php 
global $db;
global $user;
require_once "smis-base/smis-include-service-consumer.php";
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	$serv=new ServiceConsumer($db,"get_daftar_vendor",NULL,"pembelian");
    $serv->addData("command","count");
    $serv->execute();
    $result=$serv->getContent();
    $max_data=$result;
    
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    $pack->setContent($max_data);
    echo json_encode($pack->getPackage());
    return;
}

$header = array('No.',"Nama","ID","d_hutang",'k_hutang','db_hutang','kb_hutang',"d_piutang",'k_piutang','db_piutang','kb_piutang');
$uitable= new Table($header);
$uitable->setName("mapping_vendor");


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
    $serv=new ServiceConsumer($db,"get_daftar_vendor",NULL,"pembelian");
    $serv->addData("command","list");
    $serv->addData("kriteria","");
    $serv->addData("number",$_POST['current']);
    $serv->addData("max","1");
    $serv->execute();
    $result=$serv->getContent();
    $content=$result['data'];
    $dbtable=new DBTable($db,"smis_fnc_mapping_vendor");
    foreach($content as $x){
        $save=array();
        $save['nama_vendor']=$x['nama'];
        $save['id_vendor']=$x['id'];
        $idx['id_vendor']=$x['id'];
        $dbtable->insertOrUpdate($save,$idx);
    }
    $pack=new ResponsePackage();
    $pack->setStatus(ResponsePackage::$STATUS_OK);
    echo json_encode($pack->getPackage());
    return;
}


if(isset($_POST['command']) && $_POST['command']!=""){
    $adapter=new SimpleAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Nama","nama_vendor");
    $adapter->add("ID","id_vendor");    
    $adapter->add("d_hutang","d_hutang");
    $adapter->add("k_hutang","k_hutang");
    $adapter->add("db_hutang","db_hutang");
    $adapter->add("kb_hutang","kb_hutang");    
    $adapter->add("d_piutang","d_piutang");
    $adapter->add("k_piutang","k_piutang");
    $adapter->add("db_piutang","db_piutang");
    $adapter->add("kb_piutang","kb_piutang");
    
    $dbtable=new DBTable($db,"smis_fnc_mapping_vendor");
    $responder=new DBResponder($dbtable,$uitable,$adapter);
    $data=$responder->command($_POST['command']);
    if($data!=null){
        echo json_encode($data);   
    }
    return;
}

$uitable->addModal("id","hidden","","");
$uitable->addModal("nama_vendor","text","Nama","",'n',null,true);
$uitable->addModal("","label","<strong>Pengakuan Hutang</strong>","");
$uitable->addModal("d_hutang","chooser-mapping_vendor-d_hutang-Debet Hutang","Debet","");
$uitable->addModal("k_hutang","chooser-mapping_vendor-k_hutang-Kredit Hutang","Kredit","");
$uitable->addModal("","label","<strong>Pembayaran Hutang</strong>","");
$uitable->addModal("db_hutang","chooser-mapping_vendor-db_hutang-Debut Bayar Hutang","Debet","");
$uitable->addModal("kb_hutang","chooser-mapping_vendor-kb_hutang-Kredit Bayar Hutang","Kredit","");
$uitable->addModal("","label","<strong>Pengakuan Piutang</strong>","");
$uitable->addModal("d_piutang","chooser-mapping_vendor-d_piutang-Debet Piutang","Debet","");
$uitable->addModal("k_piutang","chooser-mapping_vendor-k_piutang-Kredit Piutang","Kredit","");
$uitable->addModal("","label","<strong>Pembayaran Piutang</strong>","");
$uitable->addModal("db_piutang","chooser-mapping_vendor-db_piutang-Debet Bayar Piutang","Debet","");
$uitable->addModal("kb_piutang","chooser-mapping_vendor-kb_piutang-Kredit Bayar Piutang","Kredit","");

$clone=new Button("","","Download");
$clone->setIsButton(Button::$ICONIC_TEXT);
$clone->setIcon(" fa fa-download");
$clone->setClass("btn-primary");
$clone->setAction("mapping_vendor.rekaptotal()");

$header1="<tr> <th rowspan='2'>No.</th> <th rowspan='2'>Nama</th> <th rowspan='2'>ID</th> <th colspan='2'>Hutang</th>  <th colspan='2'>Bayar Hutang</th> <th colspan='2'>Piutang</th> <th colspan='2'>Bayar Piutang</th> <th rowspan='2'>".$clone->getHtml()."</th> </tr>";
$header2="<tr> <th>Debet</th><th>Kredit</th> <th>Debet</th><th>Kredit</th> <th>Debet</th><th>Kredit</th> <th>Debet</th><th>Kredit</th> </tr>";
$uitable->addHeader("before",$header1);
$uitable->addHeader("before",$header2);
$uitable->setHeaderVisible(false);
$uitable->setAddButtonEnable(false);
$uitable->setPrintButtonEnable(false);
$uitable->setReloadButtonEnable(false);
$uitable->setDelButtonEnable(false);

$modal=$uitable->getModal();
$modal->setTitle("Mapping Vendor");
$modal->setComponentSize(Modal::$MEDIUM);

echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/smis/js/table_action.js");
echo addJS ( "base-js/smis-base-loading.js");
echo addJS("finance/resource/js/mapping_vendor.js",false);
?>