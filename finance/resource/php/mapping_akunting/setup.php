<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
$settings = new SettingsBuilder ( $db, "finance", "finance", "setup", "Setting Akunting" );
$settings->setShowDescription ( true );
$settings->addTabs ( "setup_faktur", "Setup Faktur","fa fa-tag" );
if($settings->isGroup("setup_faktur")){
    $settings->addItem ( "setup_faktur", new SettingsItem ( $db, "finance-enabled-notif-hutang-faktur", "Notif Hutang Faktur", "1", "checkbox", "Notif ke Akunting ketika ada perencanaan pemabyaran hutang" ) );
    $settings->addItem ( "setup_faktur", new SettingsItem ( $db, "finance-enabled-notif-bayar-faktur", "Notif Bayar Faktur ", "1", "checkbox", "Notif ke Akunting ketika ada pembayaran faktur yang dicairkan" ) );
}
$settings->setPartialLoad(true);
$settings->init ();
?>