<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_fnc_piutang");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_one_register",NULL,"registration");
	$serv->addData("id", $_POST['noreg_pasien']);
	$serv->execute();
	$data=$serv->getContent();	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}

loadLibrary("smis-libs-function-medical");
$_KEYS=medical_service_finance();
$_SAVE=medical_service_finance_zero();
$_QUERY="";
$_ADAPTER=new SimpleAdapter();
$_HEADER=array();
foreach($_KEYS as $k=>$v){
	$_QUERY=" SUM(".$k.") as ".$k.", ";
	$_HEADER[]=$v;
	$_ADAPTER->add($v, $k,"nonzero-money Rp.");
}

if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$noreg=$_POST['noreg_pasien'];
	$response=new ServiceConsumer($db, "get_resume_pasien");
	$response->addData("noreg_pasien", $noreg);
	$response->setMode(ServiceConsumer::$CLEAN_BOTH);
	$response->execute();
	$list=$response->getContent();
	
	$_SAVE=array();
	$_SAVE['nama_pasien']=$_POST['nama_pasien'];
	$_SAVE['noreg_pasien']=$_POST['noreg_pasien'];
	$_SAVE['nrm_pasien']=$_POST['nrm_pasien'];
	$_SAVE['waktu']=$_POST['waktu'];
	$_SAVE['urji']=$_POST['urji'];
	$_SAVE['carabayar']=$_POST['carabayar'];
	$_SAVE['total']=0;
	$_SAVE['cash']=0;
	$_SAVE['cash_resep']=0;
	$_SAVE['asuransi_lunas']=0;
	$_SAVE['asuransi_belum_lunas']=0;
	$_SAVE['diskon']=0;
	$_SAVE['bank']=0;
	$_SAVE['piutang']=0;
	
	foreach($list as $x){
		$name=$x['layanan'];
		$value=$x['nilai']*1;
		$_SAVE[$name]+=$value;
	}
	
	foreach($_KEYS as $k=>$v){
		$_SAVE['total']+=$_SAVE[$k];
	}
	
	$_SAVE['piutang']=$_SAVE['total']-$_SAVE['cash']-$_SAVE['cash_resep']-$_SAVE['asuransi_lunas']-$_SAVE['asuransi_belum_lunas']-$_SAVE['diskon']-$_SAVE['bank'];
	
	$dbtable=new DBTable($db, "smis_fnc_piutang");
	$dbtable->insert($_SAVE);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($_SAVE);
	echo json_encode($res->getPackage());
	return;
}

array_unshift($_HEADER,"No.");
array_unshift($_HEADER,"Tanggal");
array_unshift($_HEADER,"Nama");
array_unshift($_HEADER,"NRM");
array_unshift($_HEADER,"No. Reg");
array_unshift($_HEADER,"URJI");
array_unshift($_HEADER,"Cara Bayar");
$_HEADER[]="Total";
$_HEADER[]="Cash";
$_HEADER[]="Cash Resep";
$_HEADER[]="Asuransi Lunas";
$_HEADER[]="Asuransi Belum Lunas";
$_HEADER[]="Diskon";
$_HEADER[]="Bank";
$_HEADER[]="Piutang";		

$uitable=new Table($_HEADER);
$uitable->setName("piutang_satu_pasien")
		->setActionEnable(false)
		->setFooterVisible(false);

$_ADAPTER->setUseNumber(true, "No.","back.");
$_ADAPTER->add("Nama", "nama_pasien");
$_ADAPTER->add("NRM", "nrm_pasien","digit8");
$_ADAPTER->add("No. Reg", "noreg_pasien","digit8");
$_ADAPTER->add("URJI", "urji","trivial_0_URJ_URI");
$_ADAPTER->add("Cara Bayar", "carabayar");
$_ADAPTER->add("Tanggal", "waktu","date d M Y");
$_ADAPTER->add("Total", "total","nonzero-money Rp.");
$_ADAPTER->add("Cash", "cash","nonzero-money Rp.");
$_ADAPTER->add("Cash Resep", "cash_resep","nonzero-money Rp.");
$_ADAPTER->add("Asuransi Lunas", "asuransi_lunas","nonzero-money Rp.");
$_ADAPTER->add("Asuransi Belum Lunas", "asuransi_belum_lunas","nonzero-money Rp.");
$_ADAPTER->add("Diskon", "diskon","nonzero-money Rp.");
$_ADAPTER->add("Bank", "bank","nonzero-money Rp.");
$_ADAPTER->add("Piutang", "piutang","nonzero-money Rp.");

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_fnc_piutang");
	$dbtable->setOrder(" waktu ASC ");
	if(isset($_POST['status']) && $_POST['status']!=''){
		if($_POST['status']=="URI"){
			$dbtable->addCustomKriteria("urji", "='1'");
		}else{
			$dbtable->addCustomKriteria("urji", "='0'");
		}
	}
	if(isset($_POST['carabayar']) && $_POST['carabayar']!=''){
		$dbtable->addCustomKriteria("carabayar", "='".$_POST['carabayar']."'");
	}
	
	if($_POST['command']=="multipart_view"){
		$query=$dbtable->getQueryCount("");
		$total=$db->get_var($query);
		$res=new ResponsePackage();
		$res->setContent($total);
		$res->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($res->getPackage());
		return;
	}
	
	if($_POST['command']=="last_count"){
		$dbtable->setShowAll(true);
		$query=$dbtable->getQueryView("", "0");
		
		$q="SELECT 
		'' as id,
		'' as waktu,
		'Total' as nama_pasien,
		'' as noreg_pasien,
		'' as nrm_pasien,
		'' as urji,
		'' as carabayar,
		$_QUERY
		SUM(total) as total,
		SUM(cash) as cash,
		SUM(cash_resep) as cash_resep,
		SUM(asuransi_lunas) as asuransi_lunas,
		SUM(asuransi_belum_lunas) as asuransi_belum_lunas,
		SUM(diskon) as diskon,
		SUM(bank) as bank,
		SUM(piutang) as piutang,
		'' as prop
		FROM (".$query['query'].") as X ";
		
		$one=$db->get_result($q,false);
		$content=$_ADAPTER->getContent($one);
		$content[0]['No.']="";
		$content[0]['Nama']="<strong>TOTAL</strong>";
		$content[0]['Tanggal']="";
		$content[0]['NRM']="";
		$content[0]['No. Reg']="";
		$content[0]['URJI']="";
		$content[0]['Cara Bayar']="";
		$uitable->setContent($content);
		$list=$uitable->getBodyContent();
		
		$res=new ResponsePackage();
		$res->setContent($list);
		$res->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($res->getPackage());
		return;
	}
	
	$dbres=new DBResponder($dbtable, $uitable, $_ADAPTER);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$urji=new OptionBuilder();
$urji->addSingle("URI");
$urji->addSingle("URJ");
$urji->addSingle("");

$carabayar=new OptionBuilder();
$serv=new ServiceConsumer($db, "get_carabayar",NULL,"registration");
$serv->setMode(ServiceConsumer::$SINGLE_MODE);
$serv->execute();
$data=$serv->getContent();

$carabayar->add("","","1");
foreach($data as $x){
	$carabayar->add($x['nama'],$x['slug']);
}

$uitable->clearContent();
$uitable->addModal("noreg_pasien", "text", "Noreg","");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-refresh")
	   ->setAction("piutang_satu_pasien.multipart_view()");

$action2=new Button("","","View");
$action2->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("icon-white fa fa-circle-o-notch")
		->setAction("piutang_satu_pasien.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($action2);

$form=$uitable
	  ->getModal()
	  ->setTitle("finance")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("piutang_satu_pasien.batal()");

$load=new LoadingBar("rekap_piutang_satu_pasien_bar", "");
$modal=new Modal("rekap_piutang_satu_pasien_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_piutang_satu_pasien'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var piutang_satu_pasien;
	var piutang_satu_pasien_data;
	var IS_piutang_satu_pasien_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		piutang_satu_pasien=new TableAction("piutang_satu_pasien","finance","piutang_satu_pasien",new Array());
		piutang_satu_pasien.batal=function(){
			IS_piutang_satu_pasien_RUNNING=false;
			$("#rekap_piutang_satu_pasien_modal").modal("hide");
		};

		piutang_satu_pasien.addRegulerData=function(d){
			d['noreg_pasien']=$("#piutang_satu_pasien_noreg_pasien").val();
			return d;
		};
		
		piutang_satu_pasien.afterview=function(json){
			if(json!=null){
				$("#kode_table_piutang_satu_pasien").html(json.nomor);
				$("#waktu_table_piutang_satu_pasien").html(json.waktu);
				piutang_satu_pasien_data=json;
			}
		};

		piutang_satu_pasien.rekaptotal=function(){
			if(IS_piutang_satu_pasien_RUNNING) return;
			$("#rekap_piutang_satu_pasien_bar").sload("true","Fetching total data",0);
			$("#rekap_piutang_satu_pasien_modal").modal("show");
			IS_piutang_satu_pasien_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=all.length;
					piutang_satu_pasien.rekaploop(0,all,total);
				} else {
					$("#rekap_piutang_satu_pasien_modal").modal("hide");
					IS_piutang_satu_pasien_RUNNING=false;
				}
			});
		};

		piutang_satu_pasien.rekaploop=function(current,all,total){
			if(current>=total || !IS_piutang_satu_pasien_RUNNING) {
				IS_piutang_satu_pasien_RUNNING=false;
				piutang_satu_pasien.multipart_view();
				return;
			}
			var one_entity=all[current];
			$("#rekap_piutang_satu_pasien_bar").sload("true",one_entity['nama_pasien']+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['noreg_pasien']=one_entity['id'];		
			d['nama_pasien']=one_entity['nama_pasien'];		
			d['nrm_pasien']=one_entity['nrm'];		
			d['urji']=one_entity['uri'];
			d['carabayar']=one_entity['carabayar'];
			d['waktu']=one_entity['tanggal_pulang'];
			$.post("",d,function(res){
				var ct=getContent(res);
				setTimeout(function(){piutang_satu_pasien.rekaploop(++current,all,total)},300);
			});
		};

		piutang_satu_pasien.multipart_view=function(){
			$("#rekap_piutang_satu_pasien_bar").sload("true","Viewing data...",0);
			$("#rekap_piutang_satu_pasien_modal").modal("show");
			IS_piutang_satu_pasien_RUNNING=true;
			var d=this.getViewData();
			$("#piutang_satu_pasien_list").html("");
			d['command']="multipart_view";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total==0){
					$("#rekap_piutang_satu_pasien_modal").modal("hide");
					IS_piutang_satu_pasien_RUNNING=false;
				}else{
					var pages=Math.ceil(total/5);
					piutang_satu_pasien.multipart_loop(0,pages);
				}
			});
			
		};

		piutang_satu_pasien.multipart_loop=function(current,total){
			if(current>=total || !IS_piutang_satu_pasien_RUNNING) {
				IS_piutang_satu_pasien_RUNNING=false;
				piutang_satu_pasien.last_count();
				return;
			}
			$("#rekap_piutang_satu_pasien_bar").sload("true","Page Number [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			var d=this.getViewData();
			d['number']=current;
			d['max']=5;		
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#piutang_satu_pasien_list").append(ct.list);
				setTimeout(function(){piutang_satu_pasien.multipart_loop(++current,total)},300);
			});
		};

		piutang_satu_pasien.last_count=function(){
			var d=this.getRegulerData();
			d['command']="last_count";
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#piutang_satu_pasien_list").append(ct);
				$("#rekap_piutang_satu_pasien_modal").modal("hide");
			});
		};
				
	});
</script>