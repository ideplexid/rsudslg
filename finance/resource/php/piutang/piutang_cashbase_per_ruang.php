<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';
if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_fnc_piutang_per_ruang");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_all_patient_pulang",NULL,"registration");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);
	$serv->execute();
	$data=$serv->getContent();	
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$noreg=$_POST['noreg_pasien'];
	$response=new ServiceConsumer($db, "get_resume_pasien");
	$response->addData("noreg_pasien", $noreg);
	$response->setMode(ServiceConsumer::$CLEAN_BOTH);
	$response->execute();
	$list=$response->getContent();
		
	$save=array();
	$save['nama_pasien']=$_POST['nama_pasien'];
	$save['noreg_pasien']=$_POST['noreg_pasien'];
	$save['nrm_pasien']=$_POST['nrm_pasien'];
	$save['waktu']=$_POST['waktu'];
	$save['carabayar']=$_POST['carabayar'];
	$save['carapulang']=$_POST['carapulang'];
	
	$save["karcis_irna"]=0;
	$save["karcis_irja"]=0;
	
	$save["tind_irja"]=0; 	
	$save["ird"]=0; 		
	$save["irna_a"]=0; 		
	$save["irna_b"]=0; 		
	$save["irna_c"]=0; 		
	$save["irna_d"]=0; 		
	$save["irna_e"]=0; 		
	$save["irna_f"]=0; 		
	$save["irna_g"]=0;		
	$save["icu"]=0; 		
	$save["ok"]=0; 			
	
	$save["anastesi"]=0; 	
	$save["gizi"]=0; 		
	$save["farmasi"]=0; 	
	$save["bmhp"]=0; 		
	$save["laborat"]=0;		
	$save["radiology"]=0;	
	$save["bank_darah"]=0;	
	$save["kamar_mayat"]=0;	
	$save["ambulance"]=0;	
	$save['total']=0;
	$save['cash']=0;
	$save['cash_resep']=0;
	$save['asuransi_lunas']=0;
	$save['asuransi_belum_lunas']=0;
	$save['diskon']=0;
	$save['bank']=0;
	
	foreach($list as $x){
		$ruangan=$x['ruangan'];
		$name=$x['layanan'];
		$value=$x['nilai']*1;	
		if($name=="karcis")
			$save["karcis_irja"]+=$value;
		else if($name=="administratsi")
			$save["karcis_irna"]+=$value;
		else if($name=="alat_obat" || $name=="oksigen_central" || $name=="oksigen_manual" )
			$save["bmhp"]+=$value;
		else if($name=="penjualan_resep")
			$save["farmasi"]+=$value;
		else if($name=="laboratory")
			$save["laborat"]+=$value;
		else if($name=="radiology")
			$save["radiology"]+=$value;
		else if($name=="asuhan_gizi")
			$save["gizi"]+=$value;
		else if($name=="anastesi")//not yet ready
			$save["anastesi"]+=$value;
		else if($name=="ambulan")
			$save["ambulance"]+=$value;
		else if(strpos($name, "darah")!==false)
			$save["bank_darah"]+=$value;
		else if(strpos($name, "mayat")!==false)//not yet ready
			$save["kamar_mayat"]+=$value;
		else if($name=="cash")
			$save["cash"]+=$value;
		else if($name=="cash_resep")
			$save["cash_resep"]+=$value;
		else if($name=="asuransi_lunas")
			$save["asuransi_lunas"]+=$value;
		else if($name=="asuransi_belum_lunas")
			$save["asuransi_belum_lunas"]+=$value;
		else if($name=="diskon")
			$save["diskon"]+=$value;
		else if($name=="bank")
			$save["bank"]+=$value;
		else if($ruangan=="icu")
			$save["icu"]+=$value;
		else if($ruangan=="igd")
			$save["ird"]+=$value;
		else if(strpos($ruangan, "operasi")!==false)
			$save["ok"]+=$value;
		else if(strpos($ruangan, "irna_a")!==false)
			$save["irna_a"]+=$value;
		else if(strpos($ruangan, "irna_b")!==false)
			$save["irna_b"]+=$value;
		else if(strpos($ruangan, "irna_c")!==false)
			$save["irna_c"]+=$value;
		else if(strpos($ruangan, "irna_d")!==false)
			$save["irna_d"]+=$value;
		else if(strpos($ruangan, "irna_e")!==false)
			$save["irna_e"]+=$value;
		else if(strpos($ruangan, "irna_f")!==false)
			$save["irna_f"]+=$value;
		else if(strpos($ruangan, "irna_g")!==false)
			$save["irna_g"]+=$value;
		else if(strpos($ruangan, "poli")!==false)
			$save["tind_irja"]+=$value;
		
		if(isset($x['no_kwitansi']) && $x['no_kwitansi']!="" && $name=="cash" ){
			$save['kwitansi']=$x['no_kwitansi'];
		}
	}
	
	$save['total']+=$save['karcis_irna'];
	$save['total']+=$save['karcis_irja'];
	$save['total']+=$save['tind_irja'];
	$save['total']+=$save['ird'];
	$save['total']+=$save['irna_a'];
	$save['total']+=$save['irna_b'];
	$save['total']+=$save['irna_c'];
	$save['total']+=$save['irna_d'];
	$save['total']+=$save['irna_e'];
	$save['total']+=$save['irna_f'];
	$save['total']+=$save['irna_g'];
	$save['total']+=$save['icu'];
	$save['total']+=$save['ok'];
	$save['total']+=$save['anastesi'];
	$save['total']+=$save['gizi'];
	$save['total']+=$save['farmasi'];
	$save['total']+=$save['bmhp'];
	$save['total']+=$save['laborat'];
	$save['total']+=$save['radiology'];
	$save['total']+=$save['bank_darah'];
	$save['total']+=$save['kamar_mayat'];
	$save['total']+=$save['ambulance'];
	
	//$save['total']+=$save['administrasi'];
	//$save['total']+=$save['karcis'];
	//$save['total']+=$save['oksigen_central'];
	//$save['total']+=$save['oksigen_manual'];
	//$save['piutang']=$save['total']-$save['cash']-$save['cash_resep']-$save['asuransi_lunas']-$save['asuransi_belum_lunas']-$save['diskon']-$save['bank'];
	
	if($save['total']-$save['cash'] > 0){
		$save['total']=$save['cash'];
		$save['pasien_sharing']=$save['cash'];
		/*SEMUA AREA DI RESET - KARENA PASIEN BELUM LUNAS*/
		$save["karcis_irna"]=0;	
		$save["karcis_irja"]=0;	
		$save["tind_irja"]=0; 	
		$save["ird"]=0; 		
		$save["irna_a"]=0; 		
		$save["irna_b"]=0; 		
		$save["irna_c"]=0; 		
		$save["irna_d"]=0; 		
		$save["irna_e"]=0; 		
		$save["irna_f"]=0; 		
		$save["irna_g"]=0;		
		$save["icu"]=0; 		
		$save["ok"]=0; 					
		$save["anastesi"]=0;
		$save["gizi"]=0; 		
		$save["farmasi"]=0; 	
		$save["bmhp"]=0; 		
		$save["laborat"]=0;		
		$save["radiology"]=0;	
		$save["bank_darah"]=0;	
		$save["kamar_mayat"]=0;	
		$save["ambulance"]=0;	
		
		/*SEMUA AREA DI RESET - KARENA PASIEN BELUM LUNAS*/
	}
	
	$dbtable=new DBTable($db, "smis_fnc_piutang_per_ruang");
	$dbtable->insert($save);
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($save);
	echo json_encode($res->getPackage());
	return;
}

$header=array(
		"NO.","KWITANSI","TANGGAL",
		"NAMA",
		"JENIS PASIEN",
		"STATUS PASIEN",
		"NRM","NOREG",
		"KARCIS IRNA",
		"KARCIS IRJA",
		"TIND. IRJA", // semua tindakan di poliklinik
		"IRD", //semua tindakan IGD
		"IRNA A", //irna A
		"IRNA B",  
		"IRNA C",  
		"IRNA D",  
		"IRNA E",  
		"IRNA F",  
		"IRNA G",  
		"ICU",  
		"OK",  
		"ANESTESI",  
		"GIZI",  
		"FARMASI",  
		"BMHP", 	// barang habis pakai semua ruangan
		"LABORAT",	
		"RADIOLOGY",
		"BANK DARAH",
		"KAMAR MAYAT",
		"AMBULANCE",
		"PASIEN SHARING",
		"TOTAL");
$uitable=new Table($header);
$uitable->setName("piutang_cashbase_per_ruang")
		->setActionEnable(false)
		->setFooterVisible(false);

$adapter=new SimpleAdapter();
$adapter->setUseNumber(true, "NO.","back.");
$adapter->add("KWITANSI", "kwitansi");
$adapter->add("NAMA", "nama_pasien");
$adapter->add("NRM", "nrm_pasien","digit8");
$adapter->add("JENIS PASIEN", "carabayar","unslug");
$adapter->add("STATUS PASIEN", "carapulang");
$adapter->add("NOREG", "noreg_pasien","digit8");
$adapter->add("TANGGAL", "waktu","date d M Y");
$adapter->add("KARCIS IRNA", "karcis_irna","nonzero-money Rp.");
$adapter->add("KARCIS IRJA", "karcis_irja","nonzero-money Rp.");
$adapter->add("TIND. IRJA", "tind_irja","nonzero-money Rp.");
$adapter->add("IRD", "ird","nonzero-money Rp.");
$adapter->add("IRNA A", "irna_a","nonzero-money Rp.");
$adapter->add("IRNA B", "irna_b","nonzero-money Rp.");
$adapter->add("IRNA C", "irna_c","nonzero-money Rp.");
$adapter->add("IRNA D", "irna_d","nonzero-money Rp.");
$adapter->add("IRNA E", "irna_e","nonzero-money Rp.");
$adapter->add("IRNA F", "irna_f","nonzero-money Rp.");
$adapter->add("IRNA G", "irna_g","nonzero-money Rp.");
$adapter->add("ICU", "icu","nonzero-money Rp.");
$adapter->add("OK", "ok","nonzero-money Rp.");
$adapter->add("ANESTESI", "anastesi","nonzero-money Rp.");
$adapter->add("GIZI", "gizi","nonzero-money Rp.");
$adapter->add("FARMASI", "farmasi","nonzero-money Rp.");
$adapter->add("BMHP", "bmhp","nonzero-money Rp.");
$adapter->add("LABORAT", "laborat","nonzero-money Rp.");
$adapter->add("RADIOLOGY", "radiology","nonzero-money Rp.");
$adapter->add("BANK DARAH", "bank_darah","nonzero-money Rp.");
$adapter->add("KAMAR MAYAT", "kamar_mayat","nonzero-money Rp.");
$adapter->add("AMBULANCE", "ambulan","nonzero-money Rp.");
$adapter->add("PASIEN SHARING", "pasien_sharing","nonzero-money Rp.");
$adapter->add("TOTAL", "total","nonzero-money Rp.");

/*$adapter->add("Cash", "cash","nonzero-money Rp.");
$adapter->add("Cash Resep", "cash_resep","nonzero-money Rp.");
$adapter->add("Asuransi Lunas", "asuransi_lunas","nonzero-money Rp.");
$adapter->add("Asuransi Belum Lunas", "asuransi_belum_lunas","nonzero-money Rp.");
$adapter->add("Diskon", "diskon","nonzero-money Rp.");
$adapter->add("Bank", "bank","nonzero-money Rp.");
$adapter->add("Piutang", "piutang","nonzero-money Rp.");*/

if(isset($_POST['command']) && $_POST['command']!=""){
	$dbtable=new DBTable($db, "smis_fnc_piutang_per_ruang");
	$dbtable->setOrder(" waktu ASC ");
	if(isset($_POST['status']) && $_POST['status']!=''){
		if($_POST['status']=="URI"){
			$dbtable->addCustomKriteria("urji", "='1'");
		}else{
			$dbtable->addCustomKriteria("urji", "='0'");
		}
	}
	if(isset($_POST['carabayar']) && $_POST['carabayar']!=''){
		$dbtable->addCustomKriteria("carabayar", "='".$_POST['carabayar']."'");
	}
	
	if($_POST['command']=="multipart_view"){
		$query=$dbtable->getQueryCount("");
		$total=$db->get_var($query);
		$res=new ResponsePackage();
		$res->setContent($total);
		$res->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($res->getPackage());
		return;
	}
	
	if($_POST['command']=="last_count"){
		$dbtable->setShowAll(true);
		$query=$dbtable->getQueryView("", "0");
		
		$q="SELECT 
		'' as id,
		'' as waktu,
		'Total' as nama_pasien,
		'' as noreg_pasien,
		'' as nrm_pasien,
		'' as carabayar,
		'' as carapulang,
		SUM(karcis_irna) as karcis_irna,
		SUM(karcis_irja) as karcis_irja,
		SUM(tind_irja) as tind_irja,
		SUM(ird) as ird,
		SUM(irna_a) as irna_a,
		SUM(irna_b) as irna_b,
		SUM(irna_c) as irna_c,
		SUM(irna_d) as irna_d,
		SUM(irna_e) as irna_e,
		SUM(irna_f) as irna_f,
		SUM(irna_g) as irna_g,
		SUM(icu) as icu,
		SUM(ok) as ok,
		SUM(anastesi) as anastesi,
		SUM(gizi) as gizi,
		SUM(farmasi) as farmasi,
		SUM(bmhp) as bmhp,
		SUM(laborat) as laborat,
		SUM(radiology) as radiology,
		SUM(bank_darah) as bank_darah,
		SUM(kamar_mayat) as kamar_mayat,
		SUM(ambulance) as ambulance,
		SUM(total) as total,
		SUM(cash) as cash,
		SUM(cash_resep) as cash_resep,
		SUM(asuransi_lunas) as asuransi_lunas,
		SUM(asuransi_belum_lunas) as asuransi_belum_lunas,
		SUM(diskon) as diskon,
		SUM(bank) as bank,
		SUM(piutang) as piutang,
		'' as prop
		FROM (".$query['query'].") as X ";
		
		$one=$db->get_result($q,false);
		$content=$adapter->getContent($one);
		$content[0]['No.']="";
		$content[0]['Nama']="<strong>TOTAL</strong>";
		$content[0]['Tanggal']="";
		$content[0]['NRM']="";
		$content[0]['No. Reg']="";
		$content[0]['URJI']="";
		$content[0]['Cara Bayar']="";
		$uitable->setContent($content);
		$list=$uitable->getBodyContent();
		
		$res=new ResponsePackage();
		$res->setContent($list);
		$res->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($res->getPackage());
		return;
	}
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}


$carabayar=new OptionBuilder();
$serv=new ServiceConsumer($db, "get_carabayar",NULL,"registration");
$serv->setMode(ServiceConsumer::$SINGLE_MODE);
$serv->execute();
$data=$serv->getContent();

$carabayar->add("","","1");
foreach($data as $x){
	$carabayar->add($x['nama'],$x['slug']);
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari","");
$uitable->addModal("sampai", "date", "Sampai","");
$uitable->addModal("carabayar", "select", "Carabayar",$carabayar->getContent());
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-refresh")
	   ->setAction("piutang_cashbase_per_ruang.multipart_view()");

$action2=new Button("","","View");
$action2->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("icon-white fa fa-circle-o-notch")
		->setAction("piutang_cashbase_per_ruang.rekaptotal()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($action2);

$form=$uitable
	  ->getModal()
	  ->setTitle("finance")
	  ->getForm()
	  ->addElement("",$btn_froup);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("piutang_cashbase_per_ruang.batal()");

$load=new LoadingBar("rekap_piutang_cashbase_per_ruang_bar", "");
$modal=new Modal("rekap_piutang_cashbase_per_ruang_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_piutang_cashbase_per_ruang'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var piutang_cashbase_per_ruang;
	var piutang_cashbase_per_ruang_data;
	var IS_piutang_cashbase_per_ruang_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		piutang_cashbase_per_ruang=new TableAction("piutang_cashbase_per_ruang","finance","piutang_cashbase_per_ruang",new Array());
		piutang_cashbase_per_ruang.batal=function(){
			IS_piutang_cashbase_per_ruang_RUNNING=false;
			$("#rekap_piutang_cashbase_per_ruang_modal").modal("hide");
		};

		piutang_cashbase_per_ruang.addRegulerData=function(d){
			d['carabayar']=$("#piutang_cashbase_per_ruang_carabayar").val();
			d['dari']=$("#piutang_cashbase_per_ruang_dari").val();
			d['sampai']=$("#piutang_cashbase_per_ruang_sampai").val();
			return d;
		};
		
		piutang_cashbase_per_ruang.afterview=function(json){
			if(json!=null){
				$("#kode_table_piutang_cashbase_per_ruang").html(json.nomor);
				$("#waktu_table_piutang_cashbase_per_ruang").html(json.waktu);
				piutang_cashbase_per_ruang_data=json;
			}
		};

		piutang_cashbase_per_ruang.rekaptotal=function(){
			if(IS_piutang_cashbase_per_ruang_RUNNING) return;
			$("#rekap_piutang_cashbase_per_ruang_bar").sload("true","Fetching total data",0);
			$("#rekap_piutang_cashbase_per_ruang_modal").modal("show");
			IS_piutang_cashbase_per_ruang_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=all.length;
					piutang_cashbase_per_ruang.rekaploop(0,all,total);
				} else {
					$("#rekap_piutang_cashbase_per_ruang_modal").modal("hide");
					IS_piutang_cashbase_per_ruang_RUNNING=false;
				}
			});
		};

		piutang_cashbase_per_ruang.rekaploop=function(current,all,total){
			if(current>=total || !IS_piutang_cashbase_per_ruang_RUNNING) {
				IS_piutang_cashbase_per_ruang_RUNNING=false;
				piutang_cashbase_per_ruang.multipart_view();
				return;
			}
			var one_entity=all[current];
			$("#rekap_piutang_cashbase_per_ruang_bar").sload("true",one_entity['nama_pasien']+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['noreg_pasien']=one_entity['id'];		
			d['nama_pasien']=one_entity['nama_pasien'];		
			d['nrm_pasien']=one_entity['nrm'];		
			d['urji']=one_entity['uri'];
			d['carabayar']=one_entity['carabayar'];
			d['carapulang']=one_entity['carapulang'];
			d['waktu']=one_entity['tanggal_pulang'];
			$.post("",d,function(res){
				var ct=getContent(res);
				setTimeout(function(){piutang_cashbase_per_ruang.rekaploop(++current,all,total)},300);
			});
		};

		piutang_cashbase_per_ruang.multipart_view=function(){
			$("#rekap_piutang_cashbase_per_ruang_bar").sload("true","Viewing data...",0);
			$("#rekap_piutang_cashbase_per_ruang_modal").modal("show");
			IS_piutang_cashbase_per_ruang_RUNNING=true;
			var d=this.getViewData();
			$("#piutang_cashbase_per_ruang_list").html("");
			d['command']="multipart_view";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total==0){
					$("#rekap_piutang_cashbase_per_ruang_modal").modal("hide");
					IS_piutang_cashbase_per_ruang_RUNNING=false;
				}else{
					var pages=Math.ceil(total/5);
					piutang_cashbase_per_ruang.multipart_loop(0,pages);
				}
			});
			
		};

		piutang_cashbase_per_ruang.multipart_loop=function(current,total){
			if(current>=total || !IS_piutang_cashbase_per_ruang_RUNNING) {
				IS_piutang_cashbase_per_ruang_RUNNING=false;
				piutang_cashbase_per_ruang.last_count();
				return;
			}
			$("#rekap_piutang_cashbase_per_ruang_bar").sload("true","Page Number [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			var d=this.getViewData();
			d['number']=current;
			d['max']=5;		
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#piutang_cashbase_per_ruang_list").append(ct.list);
				setTimeout(function(){piutang_cashbase_per_ruang.multipart_loop(++current,total)},300);
			});
		};

		piutang_cashbase_per_ruang.last_count=function(){
			var d=this.getRegulerData();
			d['command']="last_count";
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#piutang_cashbase_per_ruang_list").append(ct);
				$("#rekap_piutang_cashbase_per_ruang_modal").modal("hide");
			});
		};
				
	});
</script>