<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/adapter/DokterPerJenisAdapter.php';

$list=new ServiceProviderList($db, "push_antrian");
$list->execute();
$ruangan=$list->getContent();
$pendaftaran=array("name"=>"PENDAFTARAN","value"=>"Pendaftaran");
$semua=array("name"=>"","value"=>"","default"=>"1");
$ruangan[]=$pendaftaran;
$ruangan[]=$semua;

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");

$lumpj_dokter=new MasterSlaveServiceTemplate($db, "lumpj_dokter", "finance", "lumpj_dokter");
$lumpj_dokter->setDateTimeEnable(true);
$header=array ("No.","Nama","Jumlah","Harga");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
    $lumpj_dokter->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);
	$lumpj_dokter->getServiceResponder()->addData("dari",$_POST['dari']);
	$lumpj_dokter->getServiceResponder()->addData("sampai",$_POST['sampai']);
	if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
        $lumpj_dokter->setEntity($_POST['ruangan']);        
    }
}
$btn=$lumpj_dokter->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$lumpj_dokter->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
		 ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran);
$lumpj_dokter->getForm(true,"")
		 ->addElement("", $btn);
$lumpj_dokter->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new DokterPerJenisAdapter();
$lumpj_dokter->setAdapter($summary);
$lumpj_dokter->addViewData("dari", "dari")
             ->addViewData("sampai", "sampai")
             ->addViewData("ruangan", "ruangan")
             ->addViewData("carabayar", "carabayar");
$lumpj_dokter->initialize();
?>

