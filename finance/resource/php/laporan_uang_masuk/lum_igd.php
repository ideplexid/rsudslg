<?php
global $db;
show_error();
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/adapter/PerawatAdapter.php';

$list=new ServiceProviderList($db, "push_antrian");
$list->execute();
$ruangan=$list->getContent();
$pendaftaran=array("name"=>"PENDAFTARAN","value"=>"Pendaftaran");
$semua=array("name"=>"","value"=>"","default"=>"1");
$ruangan[]=$pendaftaran;
$ruangan[]=$semua;

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");



$lum_igd=new MasterSlaveServiceTemplate($db, "lum_igd", "finance", "lum_igd");
$lum_igd->setDateTimeEnable(true);
$header=array ("No.","Waktu","Nama","Ruangan","Cara Bayar","Pasien",'NRM','Harga',"Jumlah","Total");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
    $lum_igd->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);
	$lum_igd->getServiceResponder()->addData("dari",$_POST['dari']);
	$lum_igd->getServiceResponder()->addData("sampai",$_POST['sampai']);
	if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
        $lum_igd->setEntity($_POST['ruangan']);        
    }
}
$btn=$lum_igd->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$lum_igd->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
		 ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran);
$lum_igd->getForm(true,"")
		 ->addElement("", $btn);
$lum_igd->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new PerawatAdapter();
$lum_igd->setAdapter($summary);
$lum_igd->addViewData("dari", "dari")
             ->addViewData("sampai", "sampai")
             ->addViewData("ruangan", "ruangan")
             ->addViewData("carabayar", "carabayar");
$lum_igd->initialize();
?>

