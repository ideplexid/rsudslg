<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/adapter/OkPerJenisAdapter.php';

$list=new ServiceProviderList($db, "push_antrian");
$list->execute();
$ruangan=$list->getContent();
$pendaftaran=array("name"=>"PENDAFTARAN","value"=>"Pendaftaran");
$semua=array("name"=>"","value"=>"","default"=>"1");
$ruangan[]=$pendaftaran;
$ruangan[]=$semua;

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");

$lumpj_ok=new MasterSlaveServiceTemplate($db, "lumpj_ok", "finance", "lumpj_ok");
$lumpj_ok->setDateTimeEnable(true);
$header=array ("No.","Nama","Jumlah","Harga");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
    $lumpj_ok->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);
	$lumpj_ok->getServiceResponder()->addData("dari",$_POST['dari']);
	$lumpj_ok->getServiceResponder()->addData("sampai",$_POST['sampai']);
	if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
        $lumpj_ok->setEntity($_POST['ruangan']);        
    }
}
$btn=$lumpj_ok->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$lumpj_ok->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
		 ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran);
$lumpj_ok->getForm(true,"")
		 ->addElement("", $btn);
$lumpj_ok->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new OkPerJenisAdapter();
$lumpj_ok->setAdapter($summary);
$lumpj_ok->addViewData("dari", "dari")
             ->addViewData("sampai", "sampai")
             ->addViewData("ruangan", "ruangan")
             ->addViewData("carabayar", "carabayar");
$lumpj_ok->initialize();
?>

