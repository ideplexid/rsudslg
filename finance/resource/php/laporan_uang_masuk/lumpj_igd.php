<?php
global $db;
show_error();
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/adapter/PerawatPerJenisAdapter.php';

$list=new ServiceProviderList($db, "push_antrian");
$list->execute();
$ruangan=$list->getContent();
$pendaftaran=array("name"=>"PENDAFTARAN","value"=>"Pendaftaran");
$semua=array("name"=>"","value"=>"","default"=>"1");
$ruangan[]=$pendaftaran;
$ruangan[]=$semua;

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");



$lumpj_igd=new MasterSlaveServiceTemplate($db, "lumpj_igd", "finance", "lumpj_igd");
$lumpj_igd->setDateTimeEnable(true);
$header=array ("No.","Nama","Jumlah","Harga");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
    $lumpj_igd->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);
	$lumpj_igd->getServiceResponder()->addData("dari",$_POST['dari']);
	$lumpj_igd->getServiceResponder()->addData("sampai",$_POST['sampai']);
	if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
        $lumpj_igd->setEntity($_POST['ruangan']);        
    }
}
$btn=$lumpj_igd->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$lumpj_igd->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
		 ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran);
$lumpj_igd->getForm(true,"")
		 ->addElement("", $btn);
$lumpj_igd->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new PerawatPerJenisAdapter();
$lumpj_igd->setAdapter($summary);
$lumpj_igd->addViewData("dari", "dari")
             ->addViewData("sampai", "sampai")
             ->addViewData("ruangan", "ruangan")
             ->addViewData("carabayar", "carabayar");
$lumpj_igd->initialize();
?>

