<?php
global $db;
require_once 'smis-libs-hrd/EmployeeResponder.php';
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'smis-libs-class/MasterTemplate.php';
require_once 'smis-libs-class/MasterServiceTemplate.php';
require_once 'smis-libs-class/MasterSlaveServiceTemplate.php';
require_once 'smis-libs-class/ServiceProviderList.php';
require_once 'finance/class/adapter/AlokPerJenisAdapter.php';

$list=new ServiceProviderList($db, "push_antrian");
$list->execute();
$ruangan=$list->getContent();
$pendaftaran=array("name"=>"PENDAFTARAN","value"=>"Pendaftaran");
$semua=array("name"=>"","value"=>"","default"=>"1");
$ruangan[]=$pendaftaran;
$ruangan[]=$semua;

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");

$lumpj_alok=new MasterSlaveServiceTemplate($db, "lumpj_alok", "finance", "lumpj_alok");
$lumpj_alok->setDateTimeEnable(true);
$header=array ("No.","Nama","Jumlah","Harga");
if(isset($_POST['dari']) && isset($_POST['sampai'])){
    $lumpj_alok->getServiceResponder()->setMode(ServiceConsumer::$JOIN_ENTITY);
	$lumpj_alok->getServiceResponder()->addData("dari",$_POST['dari']);
	$lumpj_alok->getServiceResponder()->addData("sampai",$_POST['sampai']);
	if(isset($_POST['ruangan']) && $_POST['ruangan']!=""){
        $lumpj_alok->setEntity($_POST['ruangan']);        
    }
}
$btn=$lumpj_alok->addFlag("dari", "Pilih Tanggal Mulainya", "Silakan Pilih Tanggal Mulainya Dahulu")
			  ->addFlag("sampai", "Pilih Tanggal Sampainya", "Silakan Pilih Tanggal Sampainya Dahulu")
			  ->addNoClear("dari")
			  ->addNoClear("sampai")
			  ->getUItable()
			  ->setAddButtonEnable(false)
			  ->getHeaderButton();
$lumpj_alok->getUItable()
		 ->setAction(false)
		 ->addModal("dari", "datetime", "Dari", "")
		 ->addModal("sampai", "datetime", "Sampai", "")
		 ->addModal("ruangan", "select", "Ruangan", $ruangan)
		 ->addModal("carabayar", "select", "Carabayar", $jenis_pembayaran);
$lumpj_alok->getForm(true,"")
		 ->addElement("", $btn);
$lumpj_alok->getUItable()
		 ->setHeader($header)
		 ->setFooterVisible(false);
$summary=new AlokPerJenisAdapter();
$lumpj_alok->setAdapter($summary);
$lumpj_alok->addViewData("dari", "dari")
             ->addViewData("sampai", "sampai")
             ->addViewData("ruangan", "ruangan")
             ->addViewData("carabayar", "carabayar");
$lumpj_alok->initialize();
?>

