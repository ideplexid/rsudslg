<?php 
    require_once "smis-libs-class/MasterTemplate.php";
    
    $master=new MasterTemplate($db,"smis_fnc_beban","finance","beban");
    $master ->getUItable()
            ->setHeader(array("Nama","Debet","Kredit","Keterangan"))
            ->addModal("id","hidden","","")
            ->addModal("nama","text","Nama","")
            ->addModal("debet","chooser-beban-beban_debet-Debet","Debet","")
            ->addModal("kredit","chooser-beban-beban_kredit-Kredit","Kredit","")
            ->addModal("keterangan","textarea","Keterangan","");
    $master ->getAdapter()
            ->add("Nama","nama")
            ->add("Debet","debet")
            ->add("Kredit","kredit")
            ->add("Keterangan","keterangan");
    $master->setModalTitle("Beban");
    
    $master->addSuperCommandAction("beban_debet","kode_akun");
    $master->addSuperCommandAction("beban_kredit","kode_akun");
    $master->addSuperCommandArray("beban_debet","debet","nomor");
    $master->addSuperCommandArray("beban_kredit","kredit","nomor");
    $master->initialize();
?>