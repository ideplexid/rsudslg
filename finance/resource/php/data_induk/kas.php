<?php 
    require_once "smis-libs-class/MasterTemplate.php";
    $master=new MasterTemplate($db,"smis_fnc_kas","finance","kas");
    $master ->getUItable()
            ->setHeader(array("Nama","Akun","Keterangan"))
            ->addModal("id","hidden","","")
            ->addModal("nama","text","Nama","")
            ->addModal("nomor_akun","text","Nomor Akun","")
            ->addModal("keterangan","textarea","Keterangan","");
    $master ->getAdapter()
            ->add("Nama","nama")
            ->add("Akun","nomor_akun")
            ->add("Keterangan","keterangan");
    $master->setModalTitle("Kas & Bank");
    $master->initialize();
?>