<?php 
    global $db;
    require_once "smis-base/smis-include-service-consumer.php";
    require_once "smis-libs-class/MasterTemplate.php";
    
    $uitable = new Table(array("No.","Nomor Akun","Nama","Sub","Klas"));
    $uitable ->setName("kode_akun");
    $uitable ->setModel(Table::$SELECT);
    $adapter = new SimpleAdapter();
    $adapter ->setUseNumber(true,"No.","back.")
             ->add("Nomor Akun","nomor")
             ->add("Nama","nama")
             ->add("Sub","sub")
             ->add("Klas","klas");
    $service = new ServiceResponder($db,$uitable,$adapter,"get_account","accounting");
    
    $master = new MasterTemplate($db,"smis_fnc_akun","finance","akun_kas_bank");
    $master ->getUItable()
            ->setHeader(array("Nama","Nomor","Keterangan"))
            ->addModal("id","hidden","","")
            ->addModal("nama_akun","chooser-akun_kas_bank-kode_akun-Pilih Akun","Nama","")
            ->addModal("nomor_akun","text","Nomor Akun","")
            ->addModal("keterangan","textarea","Keterangan","");
    $master ->getAdapter()
            ->add("Nama","nama_akun")
            ->add("Nomor","nomor_akun")
            ->add("Keterangan","keterangan");
    $master ->setModalTitle("Akun Kas & Bank");
    $master ->addSuperCommandResponder("kode_akun",$service);
    $master ->addSuperCommand("kode_akun");
    $master ->addSuperCommandArray("kode_akun","nama_akun","nama");
    $master ->addSuperCommandArray("kode_akun","nomor_akun","nomor");
    $master ->initialize();
?>