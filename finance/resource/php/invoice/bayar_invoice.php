<?php

/**
 * digunakan untuk mendetailkan
 * pembayaran termin invoice
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_invoice
 *                - smis_hrd_invoice_bayar
 * @since		: 13 Oktober 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
/* for handle external data/handling invoice detail */
if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == 'bayar_invoice_invoice') {
	$uitable = new Table ( array ('Tanggal','Nomor',"Nama"), "", NULL, true );
	$uitable->setName ( "bayar_invoice_invoice" );
	$uitable->setModel ( Table::$SELECT );
    if (isset ( $_POST ['command'] )) {
		$adapter = new SimpleAdapter ();
		$adapter->add("Tanggal","tanggal","date d M Y");
		$adapter->add("Nama","kepada");		
		$adapter->add("Nomor","nomor");		
		$column = array ('id','tanggal','kepada','nomor' );
		$dbtable = new DBTable ( $db, "smis_fnc_invoice", $column );
		$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml ();
	return;
}

/* handling for payout */
$uitable = new Table ( array("No.","Tanggal","Keterangan","Nilai"), "&nbsp;", NULL, true );
$uitable->setName ( "bayar_invoice" );
	
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    $adapter    = new SummaryAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Tanggal","tanggal_bayar");
    $adapter->add("Keterangan","keterangan");
    $adapter->add("Nilai","nilai","money Rp.");
    
    $adapter->addFixValue("Keterangan","<strong>TOTAL</strong>");
    $adapter->addSummary("Nilai","nilai","money Rp.");
    $dbtable = new DBTable($db,"smis_fnc_invoice_bayar");
    $dbtable->addCustomKriteria("id_invoice","='".$_POST['id_invoice']."'");
    require_once "finance/class/responder/DetailInvoiceResponder.php";
    require_once "finance/class/responder/BayarInvoiceResponder.php";
    $dbres   = new BayarInvoiceResponder($dbtable,$uitable,$adapter);
    $data    = $dbres->command($_POST['command']);
    echo  json_encode($data);
    return;
}

/* This is Modal Form and used for add and edit the current */
$id_invoice=(isset($_POST['id_invoice'])?$_POST['id_invoice']:"");
$uitable->addModal("id_invoice","hidden","",$id_invoice);
$uitable->addModal("tanggal","date","Tanggal","","n",null,true);
$uitable->addModal("jatuh_tempo","date","Tempo","","n",null,true);
$uitable->addModal("nomor","text","Nomor","","n",null,true);
$form=$uitable->getModal()->setTitle("Detail Invoice")->getForm();
$form->setTitle("Pembayaran Invoice");
$uitable->clearContent();

$uitable->addModal("id","hidden","","");
$uitable->addModal("tanggal_bayar","date","Tanggal Bayar","");
$uitable->addModal("keterangan","textarea","Keterangan","");
$uitable->addModal("deskripsi","summernote","Deskripsi","");
$uitable->addModal("nilai","money","Nilai","");

$back=new Button("","","");
$back->setIcon("fa fa-backward");
$back->setClass("btn btn-primary");
$back->setAction("bayar_invoice.back_to_invoice()");
$back->setIsButton(Button::$ICONIC);
$uitable->addFooterButton($back);


echo $form->getHtml();
echo $uitable->getHtml();
echo $uitable->getModal()->setTitle("Bayar Invoice")->getHtml ();
/* table of current content */
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "finance/resource/js/bayar_invoice.js",false );
?>