<?php

/**
 * digunakan untuk mendetailkan
 * isi dari sebuah invoice
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_invoice
 *                - smis_hrd_invoice_detail
 * @since		: 13 Oktober 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
/* for handle external data/handling invoice detail */
if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == 'detail_invoice_invoice') {
	$uitable = new Table ( array ('Tanggal','Nomor',"Nama"), "", NULL, true );
	$uitable->setName ( "detail_invoice_invoice" );
	$uitable->setModel ( Table::$SELECT );
    if (isset ( $_POST ['command'] )) {
		$adapter = new SimpleAdapter ();
		$adapter->add("Tanggal","tanggal","date d M Y");
		$adapter->add("Nama","kepada");		
		$adapter->add("Nomor","nomor");		
		$column = array ('id','tanggal','kepada','nomor' );
		$dbtable = new DBTable ( $db, "smis_fnc_invoice", $column );
        $dbres = new DBResponder( $dbtable, $uitable, $adapter );
		$data = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml ();
	return;
}

/* handling for payout */
$uitable = new Table ( array("No.","Deskripsi","Qty","Satuan","Harga","Diskon","Total"), "&nbsp;", NULL, true );
$uitable->setName ( "detail_invoice" );
	
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    $adapter    = new SummaryAdapter();
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Deskripsi","deskripsi");
    $adapter->add("Qty","kuantitas");
    $adapter->add("Satuan","satuan");
    $adapter->add("Harga","harga","money Rp.");
    $adapter->add("Diskon","diskon","back%");
    $adapter->add("Total","total","money Rp.");
    
    $adapter->addFixValue("Deskripsi","<strong>TOTAL</strong>");
    $adapter->addSummary("Total","total","money Rp.");
    $dbtable = new DBTable($db,"smis_fnc_invoice_detail");
    $dbtable->addCustomKriteria("id_invoice","='".$_POST['id_invoice']."'");
    require_once "finance/class/responder/DetailInvoiceResponder.php";
    $dbres   = new DetailInvoiceResponder($dbtable,$uitable,$adapter);
    if($dbres->isSave()){
        $total=$_POST['kuantitas']*$_POST['harga']*(100-$_POST['diskon'])/100;
        $dbres->addColumnFixValue("total",$total); 
    }
    $data    = $dbres->command($_POST['command']);
    echo  json_encode($data);
    return;
}

/* This is Modal Form and used for add and edit the current */
$id_invoice=(isset($_POST['id_invoice'])?$_POST['id_invoice']:"");
$uitable->addModal("id_invoice","hidden","",$id_invoice);
$uitable->addModal("tanggal","date","Tanggal","","n",null,true);
$uitable->addModal("jatuh_tempo","date","Tempo","","n",null,true);
$uitable->addModal("nomor","text","Nomor","","n",null,true);
$uitable->addModal("diskon_global","money","Diskon","","y",null,true);
$form=$uitable->getModal()->setTitle("Detail Invoice")->getForm();
$form->setTitle("Detail Invoice");
$uitable->clearContent();

$uitable->addModal("id","hidden","","");
$uitable->addModal("deskripsi","textarea","Deskripsi","");
$uitable->addModal("kuantitas","text","Qty","");
$uitable->addModal("satuan","text","Satuan","");
$uitable->addModal("harga","money","Harga","");
$uitable->addModal("diskon","text","Diskon","");

$back=new Button("","","");
$back->setIcon("fa fa-backward");
$back->setClass("btn btn-primary");
$back->setAction("detail_invoice.back_to_invoice()");
$back->setIsButton(Button::$ICONIC);
$uitable->addFooterButton($back);

echo $form->getHtml();
echo $uitable->getHtml();
echo $uitable->getModal()->setTitle("Detail Invoice")->getHtml ();
/* table of current content */
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "finance/resource/js/detail_invoice.js",false );
?>