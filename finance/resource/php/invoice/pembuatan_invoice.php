<?php 
    global $db;
    require_once "smis-libs-class/MasterTemplate.php";  
    require_once "finance/class/responder/InvoiceResponder.php";  
 
    $detail=new Button("","","Detail Invoice");
    $detail->setIcon("fa fa-list-alt");
    $detail->setClass("btn btn-primary");
    $detail->setIsButton(Button::$ICONIC);
    
    $detail2=new Button("",""," Pembayaran");
    $detail2->setIcon("fa fa-handshake-o");
    $detail2->setClass("btn btn-primary");
    $detail2->setIsButton(Button::$ICONIC);
    
    
    
    $master = new MasterTemplate($db,"smis_fnc_invoice","finance","pembuatan_invoice");
    $responder=new InvoiceResponder($master->getDBtable(),$master->getUItable(),$master->getAdapter());
    $master->setDBresponder($responder);
    $master->setDateEnable(true);
    $master ->getUItable()
            ->setExcelButtonEnable(true)
            ->setExcelElementButtonEnable(true)
            ->addContentButton("detail_invoice",$detail)
            ->addContentButton("bayar_invoice",$detail2)
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false)
            ->setHeader(array("No.","Tanggal","Nama","No Ref","Jatuh Tempo","Total","Lunas","Sisa","Kas & Bank"));
    $master ->getUItable()
            ->addModal("id","hidden","","")
            ->addModal("tanggal","date","Tanggal",date("Y-m-d"))
            ->addModal("jatuh_tempo","date","Tempo",date("Y-m-d"))
            ->addModal("kepada","chooser-pembuatan_invoice-pembuatan_invoice_vendor-Vendor","Vendor","")
            ->addModal("alamat","textarea","Alamat","")
            ->addModal("klasifikasi","text","Klasifikasi","")
            ->addModal("nomor","text","No. Ref","")
            ->addModal("noref","text","Nomor","")
            ->addModal("diskon_persen","text","Diskon Persen","")            
            ->addModal("diskon_rupiah","money","Diskon Uang","")            
            ->addModal("pajak_ppn","text","Persen PPN","")
            ->addModal("pajak_pph22","text","Persen PPH22","")            
            ->addModal("pajak_pph23","text","Persen PPH23","")
            ->addModal("term","textarea","Term","")
            ->addModal("catatan","textarea","Catatan","")
            ->addModal("saran","textarea","Saran","")
            ->addModal("nama_dompet","chooser-pembuatan_invoice-pembuatan_invoice_dompet-Kas & Bank","Kas & Bank","")
            ->addModal("id_dompet","hidden","","")
            ->addModal("id_vendor","hidden","","")
            ->addModal("no_akun","hidden","","");
    $master ->getAdapter()
            ->setUseNumber(true,"No.","back.")
            ->add("Kas & Bank","nama_dompet")
            ->add("Tanggal","tanggal","date d M Y")
            ->add("Total","total","money Rp.")
            ->add("Lunas","lunas","money Rp.")
            ->add("Sisa","sisa","money Rp.")
            ->add("Nama","kepada")
            ->add("Jatuh Tempo","jatuh_tempo","date d M Y")
            ->add("No Ref","noref");
    $master ->setModalTitle("Invoice");
    $master ->addResouce("js","finance/resource/js/pembuatan_invoice.js");
    
    if($master->getDBResponder()->isPreload() || $_POST['super_command']!=""  ){
        if($master->getDBResponder()->isPreload() || $_POST['super_command']=="pembuatan_invoice_dompet" ){
            $uitable=new Table(array("Nama","Akun","Keterangan"),"");
            $uitable->setName("pembuatan_invoice_dompet");
            $uitable->setModel(Table::$SELECT);
            $dbtable=new DBTable($db,"smis_fnc_kas");
            $adapter=new SimpleAdapter();
            $adapter->add("Nama","nama");
            $adapter->add("Akun","nomor_akun");
            $adapter->add("Keterangan","keterangan");
            $responder=new DBResponder($dbtable,$uitable,$adapter);
            $master->getSuperCommand()->addResponder("pembuatan_invoice_dompet",$responder);
            $master->addSuperCommand("pembuatan_invoice_dompet",array());
            $master->addSuperCommandArray("pembuatan_invoice_dompet","nama_dompet","nama");
            $master->addSuperCommandArray("pembuatan_invoice_dompet","id_dompet","id");
            $master->addSuperCommandArray("pembuatan_invoice_dompet","no_akun","nomor_akun");
        }
        
        if($master->getDBResponder()->isPreload() || $_POST['super_command']=="pembuatan_invoice_vendor" ){
            require_once "smis-base/smis-include-service-consumer.php";
            $uitable=new Table(array("Nama","Alamat","Kode"),"");
            $uitable->setName("pembuatan_invoice_vendor");
            $uitable->setModel(Table::$SELECT);
            $adapter=new SimpleAdapter();
            $adapter->add("Nama","nama");
            $adapter->add("Alamat","alamat");
            $adapter->add("Kode","kode");
            $responder=new ServiceResponder($db,$uitable,$adapter,"get_daftar_vendor");
            $responder->setEntity("pembelian");
            $master->getSuperCommand()->addResponder("pembuatan_invoice_vendor",$responder);
            $master->addSuperCommand("pembuatan_invoice_vendor",array());
            $master->addSuperCommandArray("pembuatan_invoice_vendor","kepada","nama");
            $master->addSuperCommandArray("pembuatan_invoice_vendor","alamat","alamat");
            $master->addSuperCommandArray("pembuatan_invoice_vendor","id_vendor","id");
        }
        
    }
    
    $master ->initialize();
?>