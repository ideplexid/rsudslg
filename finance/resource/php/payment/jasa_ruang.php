<?php
global $db;
setChangeCookie(false);

if(isset($_POST['super_command']) && $_POST['super_command']=="cair"){
	$database=$_POST['datasave'];
	$dbtab=new DBTable($db, "smis_fnc_bayar");
	$dbtab->insert($database);
	$response=new ResponsePackage();
	$response->setAlertVisible(true);
	$response->setAlertContent("Berhasil", "Data Berhasil Di Simpan", ResponsePackage::$TIPE_INFO);
	$response->setContent("");
	$response->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($response->getPackage());
	return;
}

if(isset($_POST['command']) && $_POST['command']== "proses") {
	require_once 'finance/class/template/ProvitShare.php';
	$from=$_POST['dari'];
	$to=$_POST['sampai'];
	$ruangan=ArrayAdapter::format("slug",substr($_POST['ruangan'], 1)); 
	$jenis_ruangan=$_POST['jenis'];
	$pajak=$_POST['pajak'];
	$jasa_ruang=new ProvitShare($db, $from, $to, $ruangan, $jenis_ruangan, $pajak);
	$response=new ResponsePackage();
	$response->setAlertVisible(false);
	$response->setContent($jasa_ruang->getHtml());
	$response->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($response->getPackage());
	return;
}

require_once 'smis-base/smis-include-service-consumer.php';
$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$pjkserv=new ServiceConsumer($db, "get_urjip");
$pjkserv->setMode(ServiceConsumer::$CLEAN_AUTONOMOUS);
$pjkserv->setCached(true,"get_urjip");
$pjkserv->execute();
$ruang=$pjkserv->getContent();
$option=new OptionBuilder();
$option->add("", "");
foreach($ruang as $name=>$val){
	$tipe=$val[0]=="URJ"?"Unit Rawat Jalan":$val[0]=="URI"?"Unit Rawat Inap":"Unit Penunjang";
	$option->add(ArrayAdapter::format("unslug",$name), $tipe);
}

$ruangan=new Select("ruangan_jruang", "", $option->getContent());
$jenis=new Text("jenis_ruangan","Jenis","");
$dari=new Text("dari_jruang","Dari","",Text::$DATE);
$sampai=new Text("sampai_jruang","Sampai","",Text::$DATE);
$pjk=new Text("pajak_jruang","Pajak",$pajak."%");
$pjkhide=new Hidden("pjk_jruang","pjk",$pajak);

$jenis->setDisabled(true);
$pjk->setDisabled(true);

$action=new Button("","","");
$action->setClass("btn-primary");
$action->setIsButton(Button::$ICONIC);
$action->setIcon("icon-white fa fa-circle-o-notch fa-spin ");
$action->setAction("jasa_ruang.proses()");

$print=new Button("","","");
$print->setClass("btn-primary");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print ");
$print->setAction("jasa_ruang.print()");

$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($print);

$form=new Form("form_ruangan","","Jasa Pelayanan Ruang");
$form->addElement("Ruangan",$ruangan);
$form->addElement("Dari",$dari);
$form->addElement("Sampai",$sampai);
$form->addElement("Jenis",$jenis);
$form->addElement("Pajak",$pjk);
$form->addElement("",$pjkhide);
$form->addElement("",$btn_froup);

echo $form->getHtml();
echo "<div class='clear'></div>
		<div id='result_jruang' ></div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	var jasa_ruang;
	var ruangan;
	var jruang_save_data;
	$(document).ready(function(){
		$('.mydate').datepicker();
		$("#ruangan_jruang").click(function(){
			$("#jenis_ruangan").val(this.value);
		});
		jasa_ruang=new TableAction("jasa_ruang","finance","jasa_ruang",new Array());
		ruangan=new TableAction("ruangan","finance","jasa_ruang",new Array());
		ruangan.setSuperCommand("ruangan");
		jasa_ruang.proses=function(){
			if($("#ruangan_jruang option:selected").text()==""){
				showWarning("Peringatan", "Silakan Pilih Ruangan");
				return ;
			}
			var data=this.getRegulerData();
			data['command']="proses";
			data['jenis']=$("#ruangan_jruang").val();
			data['ruangan']=$("#ruangan_jruang option:selected").text();
			data['dari']=$("#dari_jruang").val();
			data['sampai']=$("#sampai_jruang").val();
			data['pajak']=$("#pjk_jruang").val();
			showLoading();
			$.post('',data,function(res){
				var data=getContent(res);
				$("#result_jruang").html(data.html);
				jruang_save_data=data;
				dismissLoading();
			});
		};

		jasa_ruang.print=function(){
			var r=this.getRegulerData();
			r['command']="save";
			r['super_command']="cair";
			r['datasave']=jruang_save_data;
			showLoading();
			$.post("",r,function(res){
				getContent(res);
				dismissLoading();
				smis_print($("#result_jruang").html());
			});
		};
		
	});
</script>
<style type="text/css">
	@media print{
		#result_jruang th, #result_jruang td{ font-size: 10px; }
	}
</style>