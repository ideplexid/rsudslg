<?php 
global $db;
require_once "smis-base/smis-include-service-consumer.php";
require_once "smis-libs-class/MasterTemplate.php";
require_once 'smis-libs-class/ServiceProviderList.php';
require_once "finance/class/responder/BulkFakturResponder.php";
require_once "finance/class/table/FakturBulkTable.php";

if(isset($_POST['super_command']) && $_POST['super_command']=="dompet_bulk_faktur"){
        require_once "finance/snippet/dompet.php";
        return;
}

$list_faktur    = array();
$detail         = new Button("","","Detail Pembayaran");
$detail         ->setIcon("fa fa-list-alt")
                ->setClass("btn btn-primary")
                ->setIsButton(Button::$ICONIC);
$carabayar      = new OptionBuilder();
$carabayar      ->addSingle("Tunai")
                ->addSingle("Giro")
                ->addSingle("Cek")
                ->addSingle("Transfer Bank");
$uitable        = new FakturBulkTable(array());
$uitable        ->setName("bulk_faktur")
                ->setPrintElementButtonEnable(true);
$master         = new MasterTemplate($db,"smis_fnc_bayar_bulk","finance","bulk_faktur");
$master         ->setUITable($uitable);
$master         ->getDBTable()
                ->setOrder(" id desc ",true)
                ->addCustomKriteria("jenis","='faktur'");
$responder      = new BulkFakturResponder($master->getDBTable(),$master->getUITable(),$master->getAdapter());
$master         ->setDBResponder($responder);
if($master->getDBResponder()->isPreload() &&  $_POST['super_command']==""){
        $serv           = new ServiceProviderList($db, "get_faktur");
        $serv ->execute();
        $list_faktur    = $serv->getContent(); 
}
$master ->setDateEnable(true);
$master ->getUItable()		   
        ->addContentButton("detail_bulk",$detail)
        ->setPrintButtonEnable(false)
        ->setReloadButtonEnable(false)
        ->setHeader(array("No.","Number","Tanggal","Total","Vendor","Asal Faktur","Sumber Dana","Penerima","Pembayaran","Cair"));
$master ->getUItable()
        ->addModal("id","hidden","","")
        ->addModal("id_vendor","hidden","","")
        ->addModal("id_dompet","hidden","","")
        ->addModal("no_akun","hidden","","")
        ->addModal("waktu","date","Tanggal",date("Y-m-d"))
        ->addModal("nama_vendor","chooser-bulk_faktur-vendor-Vendor","Vendor","")
        ->addModal("asal_faktur","select","Faktur",$list_faktur,"n")
        ->addModal("nama_dompet","chooser-bulk_faktur-dompet_bulk_faktur-Kas & Bank","Kas & Bank","","n")
        ->addModal("total","money","Total","0","y",null,true)
        ->addModal("penerima","text","Penerima","","n")
        ->addModal("code", "select", "Cara Bayar", $carabayar->getContent());
                
$master ->getAdapter()
        ->setUseNumber(true,"No.","back.")
        ->add("Total","total","money Rp.")
        ->add("Vendor","nama_vendor")
        ->add("Tanggal","waktu","date d M Y")
        ->add("Asal Faktur","asal_faktur","unslug")
        ->add("Sumber Dana","nama_dompet")
        ->add("Penerima","penerima")
        ->add("Number","pynum")
        ->add("Cair","status","trivial_1_<i class='fa fa-check'></i>_")
        ->add("Pembayaran","code");

$uitable = new Table(array("Kode","Nama","Alamat"));
$uitable ->setName("vendor")
         ->setModel(Table::$SELECT);
$adapter = new SimpleAdapter();
$adapter ->add("Kode","kode")
         ->add("Nama","nama")
         ->add("Alamat","alamat");
$vendor  = new ServiceResponder($db,$uitable,$adapter,"get_daftar_vendor");
$master  ->addSuperCommandResponder("vendor",$vendor);
$master  ->addSuperCommand("vendor", array());
$master  ->addSuperCommandArray("vendor", "nama_vendor", "nama")
         ->addSuperCommandArray("vendor", "id_vendor", "id");    
$master  ->setModalTitle("Pembayaran Faktur Gerombolan");
$master  ->addResouce("js","finance/resource/js/bulk_faktur.js");
$master  ->addResouce("css","finance/resource/css/bulk_faktur.css");
$master  ->initialize();
?>