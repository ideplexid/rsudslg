<?php
/**
 * digunakan untuk bulk_umum_detail khusus KMU
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_bulk_umum_detail
 * @since		: 14 Mei 2015
 * @version		: 1.2.0
 * 
 * */

global $db;
/**handling untuk pengambilan data faktur */
if (isset ( $_POST['super_command'] ) && $_POST['super_command'] == 'bulk_umum') {
	$uitable 	 = new Table ( array ('No.','Tanggal','Vendor'), "", NULL, true );
	$uitable 	 ->setName ( "bulk_umum" )
			 	 ->setModel ( Table::$SELECT );
	if(isset($_POST['command'])){
		$adapter = new SimpleAdapter ();
		$adapter ->setUseNumber(true,"No.","back.")
				 ->add("Tanggal","tanggal","date d M Y")
				 ->add("Vendor","nama_vendor");		
		$dbtable = new DBTable ( $db, "smis_fnc_bayar_bulk" );
		$dbres   = new DBResponder ( $dbtable, $uitable, $adapter );
		$data    = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml();
	return;
}

/** used for handle the price of cost */
if(isset($_POST['super_command']) && $_POST['super_command']=="beban_bulk"){
    global $db;
    $uitable   = new Table(array("Nama","Keterangan"),"");
	$uitable   ->setName($_POST['super_command'])
			   ->setModel(Table::$SELECT);
    $dbtable   = new DBTable($db,"smis_fnc_beban");
    $adapter   = new SimpleAdapter();
	$adapter   ->add("Nama","nama")
			   ->add("Keterangan","keterangan");
    $responder = new DBResponder($dbtable,$uitable,$adapter);
    $super	   = new SuperCommand();
    $super	   ->addResponder($_POST['super_command'],$responder);
    $data	   = $super->initialize();
    if($data  != null){
        echo $data;
    }
    return;
}

$uitable 	   = new Table ( array ("No.",'Keterangan',"Deskripsi",'Nilai' ), "&nbsp;", NULL, true );
$uitable 	   ->setName("bulk_umum_detail")
			   ->setFooterVisible(false);

if (isset($_POST['command'])) {
	require_once "finance/class/responder/BulkUmumDetailResponder.php";
	$adapter   = new SummaryAdapter();
	$adapter   ->setUseNumber(true,"No.","back.")
			   ->add("Keterangan", "keterangan")
			   ->add("Nilai", "nilai", "money Rp.")
			   ->add("Deskripsi","deskripsi")
			   ->addSummary("Nilai","nilai","money Rp.");
	$dbtable   = new DBTable($db, "smis_fnc_bayar");
	$dbtable   ->addCUstomKriteria("id_bulk"," ='".$_POST['id_bulk']."' ")
			   ->addCustomKriteria("jenis", "='pembayaran_umum_bulk'")
			   ->setShowAll(true);
	$responder = new BulkUmumDetailResponder($dbtable, $uitable, $adapter);
	$result    = $responder->command($_POST['command']);
	if($result){
		echo json_encode($result);
	}
	return;
}

		
/**header form */
$uitable	->addModal("nama_vendor","text","Vendor","","y",null,true)
			->addModal("waktu","date","Tanggal","","y",null,true)
			->addModal("penerima","text","Penerima","","y",null,true)
			->addModal("code","text","Pembayaran","","y",null,true)
			->addModal("nama_dompet","text","Sumber Dana","","y",null,true)
			->addModal("no_akun","hidden","","","y",null,true)
			->addModal("id_vendor","hidden","","")
			->addModal("id_dompet","hidden","","")
			->addModal("id_bulk","hidden","",$_POST['id_bulk'],"y",null,true);
$form		= $uitable->getModal()->getForm();
$back		= new Button("","","Back");
$back		->setClass(" btn-primary")
			->setIcon("fa fa-backward")
			->setIsButton(Button::$ICONIC_TEXT)
			->setAction ( "bulk_umum_detail.back()" );
$form		->addElement ( "", $back );

/**content form */
$uitable	->clearContent();
$uitable	->addModal("id", "hidden", "", "")
			->addModal("jenis","hidden","","pembayaran_umum_bulk")
			->addModal("keterangan","chooser-bulk_umum_detail-beban_bulk-Biaya","Pembayaran","")
			->addModal("deskripsi","textarea","Deskripsi","")
			->addModal("noref","text","No Referensi","")
			->addModal("nilai","money","Nilai","")
			->addModal("status","hidden","","1")
			->addModal("html","summernote-print","Detail","")
			->addModal("debet","hidden","","")
			->addModal("kredit","hidden","","");

echo $form->getHtml();
echo "<div class='clear line'></div>";
echo $uitable->getModal()->setTitle("Faktur")->getHtml();
echo $uitable->getHtml();

echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("finance/resource/js/bulk_umum_detail.js",false);
?>