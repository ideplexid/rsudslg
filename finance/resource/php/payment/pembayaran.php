<?php
/**
 * this is handling about element where used for patient management
 * @author goblooge
 *
 */

if(isset($_POST['super_command']) && $_POST['super_command']=="pembayaran_dompet"){
    require_once "finance/snippet/dompet.php";
    return;
}

if(isset($_POST['super_command']) && $_POST['super_command']=="beban"){
    global $db;
    $uitable=new Table(array("Nama","Keterangan"),"");
    $uitable->setName($_POST['super_command']);
    $uitable->setModel(Table::$SELECT);
    $dbtable=new DBTable($db,"smis_fnc_beban");
    $adapter=new SimpleAdapter();
    $adapter->add("Nama","nama");
    $adapter->add("Keterangan","keterangan");
    $responder=new DBResponder($dbtable,$uitable,$adapter);
    $super=new SuperCommand();
    $super->addResponder($_POST['super_command'],$responder);
    $data=$super->initialize();
    if($data!=null){
        echo $data;
    }
    return;
}


$header=array('Tanggal','Jenis',"No Referensi",'Pembayaran',"Deskripsi","Kas & Bank","Penerima","Grup Transaksi");
require_once "finance/class/table/PembayaranUmumTable.php";
$uitable = new PembayaranUmumTable($header,"Pembayaran Umum",NULL,true);
$uitable->setPrintElementButtonEnable(true);
$uitable->setName("pembayaran");
if(isset($_POST['command'])) {
    require_once "finance/class/responder/PembayaranUmumResponder.php";
	$adapter = new SimpleAdapter();
    $adapter ->add("Tanggal","waktu","date d M Y")
             ->add("Jenis","jenis","unslug")
             ->add("No Referensi","noref")
             ->add("Penerima","penerima")
             ->add("Pembayaran","keterangan")
             ->add("Deskripsi","deskripsi")
             ->add("Nilai","nilai","money Rp.")
             ->add("No Referensi","noref")
             ->add("Kas & Bank","nama_dompet")
             ->add("Grup Transaksi","grup","unslug");
	$dbtable = new DBTable($db,"smis_fnc_bayar");
	$dbtable ->addCustomKriteria("jenis", "='pembayaran_umum'");
    $dbtable ->setOrder(" id DESC ",true);
	$dbres   = new PembayaranUmumResponder($dbtable,$uitable,$adapter);
	$data    = $dbres->command($_POST ['command']);
	echo json_encode($data);
	return;
}

require_once "smis-base/smis-include-service-consumer.php";
$serv = new ServiceConsumer($db,"get_all_grup",null,"accounting");
$serv ->execute();
$grup = $serv->getContent();

$uitable->addModal("id","hidden","","");
$uitable->addModal("waktu","date","Tanggal",date("Y-m-d"));
$uitable->addModal("grup","select","Grup Transaksi",$grup);
$uitable->addModal("jenis","hidden","","pembayaran_umum");
$uitable->addModal("keterangan","chooser-pembayaran-beban-Biaya","Pembayaran","");
$uitable->addModal("deskripsi","textarea","Deskripsi","");
$uitable->addModal("penerima","text","Penerima","");
$uitable->addModal("noref","text","No Referensi","");
$uitable->addModal("nilai","money","Nilai","");
$uitable->addModal("status","hidden","","1");
$uitable->addModal("html","summernote-print","Detail","");
$uitable->addModal("nama_dompet","chooser-pembayaran-pembayaran_dompet-Kas & Bank","Kas & Bank","");
$uitable->addModal("id_dompet","hidden","","");
$uitable->addModal("no_akun","hidden","","");
$uitable->addModal("debet","hidden","","");
$uitable->addModal("kredit","hidden","","");
$modal = $uitable->getModal();
$modal->setTitle("pembayaran");
$modal->setComponentSize(Modal::$MEDIUM);
echo $uitable->getHtml();
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addJS("finance/resource/js/pembayaran.js",false);
echo addCSS("framework/bootstrap/css/datepicker.css");
?>