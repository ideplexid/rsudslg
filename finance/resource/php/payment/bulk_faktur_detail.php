<?php
/**
 * digunakan untuk bulk_faktur_detail khusus KMU
 * karena ada beberapa aturan yang agak njelimet
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_employee
 * 				  - smis_hrd_bulk_faktur_detail
 * @since		: 14 Mei 2015
 * @version		: 1.2.0
 * 
 * */

global $db;
/* for handle external data/handling employee detail */
if (isset ( $_POST['super_command'] ) && $_POST['super_command'] == 'bulk_faktur') {
	$uitable 	 = new Table ( array ('No.','Tanggal','Vendor'), "", NULL, true );
	$uitable 	 ->setName ( "bulk_faktur" )
			 	 ->setModel ( Table::$SELECT );
	if(isset($_POST['command'])){
		$adapter = new SimpleAdapter ();
		$adapter ->setUseNumber(true,"No.","back.")
				 ->add("Tanggal","tanggal","date d M Y")
				 ->add("Vendor","nama_vendor");		
		$dbtable = new DBTable ( $db, "smis_fnc_bayar_bulk" );
		$dbres   = new DBResponder ( $dbtable, $uitable, $adapter );
		$data    = $dbres->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml ();
	return;
}

if (isset ( $_POST['super_command'] ) && $_POST['super_command'] == 'nofaktur_bulk') {
	require_once "smis-base/smis-include-service-consumer.php";
	$header  	    = array ("No Faktur","Tgl Faktur","Tagihan","Terbayar" );
	$uitable 	    = new Table ( $header, "", NULL, true );
	$uitable 	    ->setName ( "nofaktur_bulk" )
			 	    ->setModel ( Table::$SELECT );
	if(isset($_POST['command'])){
		$adapter    = new SimpleAdapter ();
		$adapter    ->add ("No Faktur", "no_faktur")
				    ->add ("Tgl Faktur", "tanggal", "date d M Y")
				    ->add ("Terbayar", "total_dibayar","money Rp.")
				    ->add ("Tagihan", "total_tagihan","money Rp.");
		$ruangan	= $_POST['asal_faktur'];
		$responder 	= new ServiceResponder ( $db,$uitable,$adapter,"get_faktur",$ruangan );
		$data    	= $responder->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml ();
	return;
}

$uitable 	   = new Table ( array ("No.",'No Faktur','Nilai' ), "&nbsp;", NULL, true );
$uitable 	   ->setName("bulk_faktur_detail")
			   ->setFooterVisible(false);

if (isset ( $_POST ['command'] )) {
	require_once "finance/class/responder/BulkFakturDetailResponder.php";
	$adapter   = new SummaryAdapter();
	$adapter   ->setUseNumber(true,"No.","back.")
			   ->add("No Faktur", "noref")
			   ->add("Nilai", "nilai", "money Rp.")
			   ->addSummary("Nilai","nilai","money Rp.");
	$dbtable   = new DBTable($db, "smis_fnc_bayar");
	$dbtable   ->addCUstomKriteria("id_bulk"," ='".$_POST['id_bulk']."' ")
			   ->addCustomKriteria("jenis", "='pembayaran_faktur_bulk'")
			   ->setShowAll(true);
	$responder = new BulkFakturDetailResponder($dbtable, $uitable, $adapter);
	$result    = $responder->command($_POST['command']);
	if($result){
		echo json_encode($result);
	}
	return;
}

require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-class/ServiceProviderList.php';
$serv           = new ServiceProviderList($db, "get_faktur");
$serv ->execute();
$list_faktur    = $serv->getContent(); 
		
/**header form */
$uitable	->addModal("nama_vendor","text","Vendor","","y",null,true)
			->addModal("waktu","date","Tanggal","","y",null,true)
			->addModal("penerima","text","Penerima","","y",null,true)
			->addModal("code","text","Pembayaran","","y",null,true)
			->addModal("asal_faktur","select","Asal Faktur",$list_faktur,"n",null,true)
			->addModal("nama_dompet","text","Sumber Dana","","y",null,true)
			->addModal("no_akun","hidden","","","y",null,true)
			->addModal("id_vendor","hidden","","")
			->addModal("id_dompet","hidden","","")
			->addModal("id_bulk","hidden","",$_POST['id_bulk'],"y",null,true);
$form		= $uitable->getModal()->getForm();
$back		= new Button("","","Back");
$back		->setClass(" btn-primary")
			->setIcon("fa fa-backward")
			->setIsButton(Button::$ICONIC_TEXT)
			->setAction ( "bulk_faktur_detail.back()" );
$form		->addElement ( "", $back );

/**content form */
$uitable	->clearContent();
$uitable	->addModal("id", "hidden", "", "")
			->addModal("noref", "chooser-bulk_faktur_detail-nofaktur_bulk-Faktur","Faktur", "","n",null,true)
			->addModal("jt_bayar", "date", "Jatuh Tempo","",null,true)
			->addModal("id_faktur","hidden","","")
			->addModal("nilai", "money","Nilai","")
			->addModal("jenis", "hidden","","pembayaran_faktur_bulk")
			->addModal("keterangan", "textarea", "Keterangan", "");

echo $form->getHtml();
echo "<div class='clear line'></div>";
echo $uitable->getModal()->setTitle("Faktur")->getHtml();
echo $uitable->getHtml();

echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("finance/resource/js/bulk_faktur_detail.js",false);
?>