<?php

/**
 * menu untuk membayar termin di masing-masing
 * gaji pegawai
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_fnc_termin_gaji
 *                - smis_fnc_termin_gaji_detail
 * @since		: 3 Okt 2017
 * @version		: 1.0.0
 * 
 * */

global $db;
if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == 'termin_detail_gaji') {
    require_once "smis-base/smis-include-service-consumer.php";
    $serv=new ServiceConsumer($db,"get_all_detail_salary_by_periode");
    $serv->addData("id_periode",$_POST['id_periode']);
    $serv->setEntity("hrd");
    $serv->execute();    
    $list=$serv->getContent();
    
    $one=array();
    $idx=array();
    $dbtable=new DBTable($db,"smis_fnc_termin_gaji_detail");
    foreach($list as $x){
        
        $terbayar_q="SELECT sum(dibayar) as total FROM smis_fnc_termin_gaji_detail WHERE id_detail_periode='".$x['id']."' AND id_termin<'".$_POST['id_termin']."' AND prop!='del' ";
        $terbayar=$db->get_var($terbayar_q);
        $sisa=$x['total_salary']-$terbayar;
        
        if($sisa>0){
            $one['id_detail_periode']   = $x['id'];
            $one['nip_employee']        = $x['nip_employee'];
            $one['id_employee']         = $x['id_employee'];
            $one['nama_employee']       = $x['nama_employee'];
            $one['golongan_employee']   = $x['golongan_employee'];
            $one['gaji']                = $x['total_salary'];
            $one['terbayar']            = $terbayar;
            $one['sisa']                = $sisa;
            $one['id_periode']          = $_POST['id_periode'];
            $one['id_termin']           = $_POST['id_termin'];
            
            $idx['id_detail_periode']   = $x['id'];
            $idx['id_termin']           = $_POST['id_termin'];
            $idx['id_periode']          = $_POST['id_periode'];
            if($dbtable->is_exist($idx)){
                $dbtable->update($one,$idx);
            }else{
                $one['dibayar']         = $sisa;
                $dbtable->insert($one);
            }
        }
    }
    
    return;
}

if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == 'termin_detail_termin') {
	$uitable=new Table(array("No.","Tanggal","Periode","Tanggal Periode"));
    $uitable->setName ( "termin_detail_termin" );
	$uitable->setModel ( Table::$SELECT );
    if (isset ( $_POST ['command'] )) {
		$adapter=new SimpleAdapter();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add("Periode","periode");
        $adapter->add("Tanggal","tanggal","date d M Y");
        $adapter->add("Tanggal Periode","tanggal_periode","date d M Y");        
        $dbtable=new DBTable($db,"smis_fnc_termin_gaji");
        $service=new DBResponder($dbtable,$uitable,$adapter);
		$data = $service->command ( $_POST ['command'] );
		echo json_encode ( $data );
		return;
	}
	echo $uitable->getHtml ();
	return;
}

/* handling for payout */
require_once "hrd/class/table/AutoSalaryTable.php";
$uitable = new Table ( array("No.","Nama","NIP","Gaji","Terbayar","Sisa","Dibayar"), "&nbsp;", NULL, true );
$uitable->setName ( "termin_detail" );

$id_periode=(isset($_POST['id_termin'])?$_POST['id_termin']:"");
$termin=new DBTable($db,"smis_fnc_termin_gaji");
$periode=$termin->select($id_periode);
if($periode!=null){
    if($periode->kunci=="1"){
        $uitable->setAction(false);
    }
}
	
/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    require_once "finance/class/responder/TerminDetailResponder.php";
    $adapter = new SummaryAdapter();
    $adapter->addFixValue("Nama","<strong>Total</strong>");
    $adapter->addSummary("Gaji","gaji","money Rp.");
    $adapter->addSummary("Terbayar","terbayar","money Rp.");
    $adapter->addSummary("Sisa","sisa","money Rp.");
    $adapter->addSummary("Dibayar","dibayar","money Rp.");
    $adapter->setUseNumber(true,"No.","back.");
    $adapter->add("Nama","nama_employee");
    $adapter->add("NIP","nip_employee");
    $adapter->add("Gaji","gaji","money Rp.");
    $adapter->add("Terbayar","terbayar","money Rp.");
    $adapter->add("Dibayar","dibayar","money Rp.");
    $adapter->add("Sisa","sisa","money Rp.");
    
    
    $dbtable = new DBTable($db,"smis_fnc_termin_gaji_detail");
    $dbtable->addCustomKriteria("id_termin","='".$_POST['id_termin']."'");
    $dbres   = new TerminDetailResponder($dbtable,$uitable,$adapter);
    $data    = $dbres->command($_POST['command']);
    echo  json_encode($data);
    return;
}

/* This is Modal Form and used for add and edit the current */
$uitable->addModal("id_termin","hidden","",$id_periode);
$uitable->addModal("periode","chooser-termin_detail-termin_detail_termin-Termin","Termin","","y",null,true);
$uitable->addModal("tanggal","text","Tanggal","","y",null,true);
$uitable->addModal("id_periode","hidden","","","",null,true);
$uitable->addModal("template","text","Template","");
$uitable->addModal("tanggal_periode","text","Tanggal Periode","","y",null,true);
$form=$uitable->getModal()->setTitle("Termin Pembayaran Gaji")->getForm();
$uitable->clearContent();

$button=new Button("","","Import");
$button->addClass(" btn btn-primary ");
$button->setIcon(" fa fa-download");
$button->setAction("termin_detail.import();");
$button->setIsButton(Button::$ICONIC_TEXT);
$form->addElement("",$button);

$uitable->addModal("id","hidden","","");
$uitable->addModal("id_detail_periode","text","Tanggal","","y",null,true);
$uitable->addModal("nip_employee","text","NIP","","y",null,true);
$uitable->addModal("nama_employee","text","Nama","","y",null,true);
$uitable->addModal("gaji","money","Gaji","","y",null,true);
$uitable->addModal("terbayar","money","Terbayar","","y",null,true);
$uitable->addModal("sisa","money","Sisa","","y",null,true);
$uitable->addModal("dibayar","money","Dibayar","","y",null,false);

$back=new Button("","","");
$back->setIcon("fa fa-backward");
$back->setClass("btn btn-primary");
$back->setAction("termin_detail.back_to_periode()");
$back->setIsButton(Button::$ICONIC);
$uitable->addFooterButton($back);

echo $form->getHtml();
echo $uitable->getHtml();
echo $uitable->getModal()->setTitle("Penggajian")->getHtml ();
/* table of current content */
echo addJS ( "framework/smis/js/table_action.js" );
echo addJS ( "finance/resource/js/termin_detail.js",false );

?>