<?php
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']=="cair"){
	$database=$_POST['datasave'];
	$dbtab=new DBTable($db, "smis_fnc_bayar");
	$dbtab->insert($database);
	$response=new ResponsePackage();
	$response->setAlertVisible(true);
	$response->setAlertContent("Berhasil", "Data Berhasil Di Simpan", ResponsePackage::$TIPE_INFO);
	$response->setContent("");
	$response->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($response->getPackage());
	return;
}

if(isset($_POST['super_command']) && $_POST['super_command']!="") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("karyawan");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"");
	$super=new SuperCommand();
	$super->addResponder("karyawan",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

if(isset($_POST['command']) && $_POST['command']== "proses") {
	require_once 'finance/class/template/Salary.php';
	$salary=new Salary($db,$_POST['id_karyawan'],$_POST['dari'],$_POST['sampai'],$_POST['nama_karyawan'],$_POST['jabatan_karyawan'],$_POST['pajak']);
	$response=new ResponsePackage();
	$response->setAlertVisible(false);
	$response->setContent($salary->getHtml());
	$response->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($response->getPackage());
	return;
}
require_once 'smis-base/smis-include-service-consumer.php';
$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$id=new Hidden("id_karyawan","","");
$btn=new Button("","","");
$btn->setClass("btn-primary");
$btn->setIsButton(Button::$ICONIC);
$btn->setIcon("icon-list-alt icon-white");
$btn->setAction("salary.chooser('salary','nama_karyawan','karyawan',karyawan) ");
$inp=new Text("nama_karyawan","Nama","");
$inp->setDisabled(true);
$inp->setClass("smis-one-option-input");
$nama=new InputGroup("");
$nama->addComponent($inp);
$nama->addComponent($btn);

$nip=new Text("nip_karyawan","NIP","");
$jabatan=new Text("jabatan_karyawan","Jabatan","");
$dari=new Text("dari_salary","Dari","",Text::$DATE);
$sampai=new Text("sampai_salary","Sampai","",Text::$DATE);
$pjk=new Text("pajak","Pajak",$pajak."%");
$pjkhide=new Hidden("pjk_salary","pjk",$pajak);

$id->setDisabled(true);
$nama->setDisabled(true);
$jabatan->setDisabled(true);
$nip->setDisabled(true);
$pjk->setDisabled(true);

$action=new Button("","","");
$action->setClass("btn-primary");
$action->setIsButton(Button::$ICONIC);
$action->setIcon("icon-white fa fa-circle-o-notch fa-spin ");
$action->setAction("salary.proses()");

$print=new Button("","","");
$print->setClass("btn-primary");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setAction("salary.print()");

$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action);
$btn_froup->addButton($print);

$form=new Form("form_karyawan","","Pegawai");
$form->addElement("",$id);
$form->addElement("Nama",$nama);
$form->addElement("NIP",$nip);
$form->addElement("Jabatan",$jabatan);
$form->addElement("Dari",$dari);
$form->addElement("Sampai",$sampai);
$form->addElement("Pajak",$pjk);
$form->addElement("",$pjkhide);
$form->addElement("",$btn_froup);

echo $form->getHtml();
echo "<div class='clear'></div>
		<div id='result_salary' ></div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
	var salary;
	var karyawan;
	var salary_save_data;
	$(document).ready(function(){
		$('.mydate').datepicker();
		salary=new TableAction("salary","finance","salary",new Array());
		karyawan=new TableAction("karyawan","finance","salary",new Array());
		karyawan.setSuperCommand("karyawan");
		karyawan.selected=function(json){
			$("#nip_karyawan").val(json.nip);
			$("#nama_karyawan").val(json.nama);
			$("#jabatan_karyawan").val(json.nama_jabatan);
			$("#id_karyawan").val(json.id);
		};

		salary.proses=function(){
			var data=this.getRegulerData();
			data['command']="proses";
			data['id_karyawan']=$("#id_karyawan").val();
			data['nama_karyawan']=$("#nama_karyawan").val();
			data['jabatan_karyawan']=$("#jabatan_karyawan").val();
			data['dari']=$("#dari_salary").val();
			data['sampai']=$("#sampai_salary").val();
			data['pajak']=$("#pjk_salary").val();
			showLoading();
			$.post('',data,function(res){
				var data=getContent(res);
				$("#result_salary").html(data.html);
				salary_save_data=data;
				dismissLoading();
			});
		};

		salary.print=function(){
			var r=this.getRegulerData();
			r['command']="save";
			r['super_command']="cair";
			r['datasave']=salary_save_data;
			showLoading();
			$.post("",r,function(res){
				getContent(res);
				dismissLoading();
				smis_print($("#result_salary").html());
			});
			
		};
		
	});
</script>
<style type="text/css">
	@media print{
		#result_salary th, #result_salary td{ font-size: 10px; }
	}
</style>