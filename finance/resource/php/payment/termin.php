<?php 
    require_once "smis-base/smis-include-service-consumer.php";    
    require_once "smis-libs-class/MasterTemplate.php";  
    require_once "finance/class/responder/TerminResponder.php";  
    require_once "finance/class/table/TerminTable.php";  
  
    
    $detail=new Button("","","");
    $detail->setIcon("fa fa-list-alt");
    $detail->setClass("btn btn-primary");
    $detail->setIsButton(Button::$ICONIC);
    
    $submit=new Button("","","");
    $submit->setIcon("fa fa-money");
    $submit->setClass("btn btn-success");
    $submit->setIsButton(Button::$ICONIC);
    
    $master = new MasterTemplate($db,"smis_fnc_termin_gaji","finance","termin");
    $dbtable=$master->getDBtable()->setOrder("id DESC ",true);
    $termintable=new TerminTable(array("No.","Periode Gaji","Tanggal Periode","Tanggal Termin","Total","Kas & Bank"));
    $termintable->setName("termin");
    $termintable->addModal("id","hidden","","")
                ->addModal("id_periode","hidden","","")
                ->addModal("periode","chooser-termin-periode_gaji-Periode Gaji","Periode Gaji","","n",null,true)
                ->addModal("template","text","Template","","n",null,true)
                ->addModal("tanggal_periode","date","Tanggal Periode","","n",null,true)
                ->addModal("tanggal","date","Tanggal Termin",date("Y-m-d"))
                ->addModal("nama_dompet","chooser-termin-termin_dompet-Kas & Bank","Kas & Bank","")
                ->addModal("id_dompet","hidden","","")
                ->addModal("no_akun","hidden","","");
    $responder=new TerminResponder($master->getDBtable(),$termintable,$master->getAdapter());
    $master->setUITable($termintable);
    $master->setDBresponder($responder);
    $master->setDateEnable(true);
    $master ->getUItable()
            ->addContentButton("detail_termin",$detail)
            ->addContentButton("upload_termin",$submit)
            ->setPrintButtonEnable(false)
            ->setReloadButtonEnable(false);
    $master ->getAdapter()
            ->setUseNumber(true,"No.","back.")
            ->add("Kas & Bank","nama_dompet")
            ->add("Template","template")
            ->add("Periode Gaji","periode")
            ->add("Tanggal Periode","tanggal_periode","date d M Y")
            ->add("Tanggal Termin","tanggal","date d M Y")
            ->add("Total","total_uang","money Rp.")
            ->add("kunci","kunci");
    $master ->setModalTitle("Periode Pembayaran Gaji");
    $master ->setModalComponentSize(Modal::$MEDIUM);
    $master ->addResouce("js","finance/resource/js/termin.js");
    
    if($master->getDBResponder()->isPreload() || $_POST['super_command']!=""  ){
        $uitable=new Table(array("Nama","Akun","Keterangan"),"");
        $uitable->setName("termin_dompet");
        $uitable->setModel(Table::$SELECT);
        $dbtable=new DBTable($db,"smis_fnc_kas");
        $adapter=new SimpleAdapter();
        $adapter->add("Nama","nama");
        $adapter->add("Akun","nomor_akun");
        $adapter->add("Keterangan","keterangan");
        $responder=new DBResponder($dbtable,$uitable,$adapter);
        $master->getSuperCommand()->addResponder("termin_dompet",$responder);
        $master->addSuperCommand("termin_dompet",array());
        $master->addSuperCommandArray("termin_dompet","nama_dompet","nama");
        $master->addSuperCommandArray("termin_dompet","id_dompet","id");
        $master->addSuperCommandArray("termin_dompet","no_akun","nomor_akun");
        
        $uitable=new Table(array("No.","Tanggal","Template","Periode","Sisa"));
        $uitable->setName("periode_gaji");
        $uitable->setModel(Table::$SELECT);
        $adapter=new SimpleAdapter();
        $adapter->setUseNumber(true,"No.","back.");
        $adapter->add("Periode","periode");
        $adapter->add("Tanggal","tanggal","date d M Y");
        $adapter->add("Template","template");   
        $adapter->add("Sisa","sisa","money Rp.");   
        $service=new ServiceResponder($db,$uitable,$adapter,"get_periode_gaji");    
        $master->getSuperCommand()->addResponder("periode_gaji",$service);
        $master->addSuperCommand("periode_gaji",array());
        $master->addSuperCommandArray("periode_gaji","template","template");
        $master->addSuperCommandArray("periode_gaji","tanggal_periode","tanggal");
        $master->addSuperCommandArray("periode_gaji","periode","periode");
        $master->addSuperCommandArray("periode_gaji","id_periode","id");
    }
    
    
    $master ->initialize();
?>