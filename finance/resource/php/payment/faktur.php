<?php
global $db;
setChangeCookie ( false );
if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] != "") {
	if ($_POST ['super_command'] == "fp") {
		require_once 'finance/class/template/FakturPayment.php';
		$fp = new FakturPayment ( $db, $_POST ['id_faktur'],$_POST['ruangan'], $_POST ['jt_faktur'], $_POST ['nofaktur'] );
		$fp->initialize ();
		return;
	}

	if ($_POST ['super_command'] == "faktur_dk") {
		require_once 'finance/class/template/DiskonPayment.php';
		$fp = new DiskonPayment( $db, $_POST ['id_faktur'], $_POST['ruangan'], $_POST ['jk_total'], $_POST ['nofaktur'] );
		$fp->initialize ();
		return;
	}
    
    if(isset($_POST['super_command']) && $_POST['super_command']=="faktur_dompet"){
    	require_once "finance/snippet/dompet.php";
    	return;
	}
	
	$header  = array ("Vendor","No Faktur","Tgl Faktur","Jatuh Tempo","Keterangan","Tagihan","Terbayar" );
	$table   = new Table ( $header, "", NULL, true );
	$table	 ->setModel ( Table::$SELECT )
			 ->setName ( "fkt" );
	$adapter = new SimpleAdapter ();
	$adapter ->add ("No Faktur", "no_faktur" )
			 ->add ("Tgl Faktur", "tanggal", "date d M Y" )
			 ->add ("Tgl Datang", "tanggal_datang", "date d M Y" )
			 ->add ("Jatuh Tempo", "tanggal_tempo", "date d M Y" )
			 ->add ("Keterangan", "keterangan" )
			 ->add ("Vendor", "nama_vendor" )
             ->add ("Terbayar", "total_dibayar","money Rp." )
             ->add ("Tagihan", "total_tagihan","money Rp." );
	require_once 'finance/class/service_provider/FakturServiceResponder.php';
	
	$ruangan	= $_POST['ruangan'];
	$responder 	= new FakturServiceResponder ( $db, $table, $adapter, $ruangan );
	$super 		= new SuperCommand ();
	$super		->addResponder ( "fkt", $responder );
	$result 	= $super->initialize ();
	if ($result != null)
		echo $result;
	return;
}

require_once "smis-base/smis-include-service-consumer.php";
require_once 'smis-libs-class/ServiceProviderList.php';
$serv = new ServiceProviderList($db, "get_faktur");
$serv ->execute();
$list = $serv->getContent();


$modal = new Modal ( "", "", "faktur" );
$modal ->addModalElement ( "hidden", "", "", "id", "", "", "y" )
	   ->addModalElement ( "hidden", "", "", "hbayar", "", "y", true )
	   ->addModalElement ( "select", $list, "Ruangan", "ruangan", "", false )
	   ->addModalElement ( "chooser-faktur-fkt-Pilih Faktur", "", "No Faktur", "no_faktur", "", "y", false )
	   ->addModalElement ( "text", "", "Total Tagihan", "total_tagihan", "", "y", true )
	   ->addModalElement ( "text", "", "Total Diskon", "total_diskon", "", "y", true )
	   ->addModalElement ( "text", "", "Total Bayar", "total_bayar", "", "y", true )
	   ->addModalElement ( "text", "", "Sisa", "sisa", "", "y", true );
$form  = $modal->getForm ();

echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("finance/resource/js/faktur.js",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");
echo addCSS ("finance/resource/css/faktur.css",false);
echo $form->getHtml ();
echo "<div class='clear line'></div>";
echo "<div id='faktur_content' ></div>";
?>