<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="dokter_tutup_tagihan") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	
	$dktable=new Table($head);
	$dktable->setName("dokter_tutup_tagihan");
	$dktable->setModel(Table::$SELECT);	
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"dokter");
	
	$super=new SuperCommand();
	$super->addResponder("dokter_tutup_tagihan",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

$header  = array ('No.','Waktu','Tindakan',"Harga","Dokter","Visite","Konsul","Tindakan Medis Non Operatif Rawat Inap","Tindakan Medis Non Operatif Rawat Jalan","Periksa Dokter Poli","Periksa Dokter IGD","Total Jaspel Dokter" );
$uitable = new Table ($header, "", NULL, false);
$uitable ->setName ( "laporan_jaspel_tutup_tagihan" );
$uitable ->setFooterVisible(false);

if (isset ( $_POST ['command'] )) {
    require_once "finance/class/adapter/LaporanJaspelAdapter.php";
    require_once "finance/class/responder/LaporanJaspelResponder.php";
    $adapter = new LaporanJaspelAdapter ();
	$adapter ->add ( "Waktu", "waktu" ,"date d M Y")
             ->add ( "Tindakan", "nama_tagihan" )
             ->add ( "Dokter", "nama_dokter" )
             ->add ( "Harga", "harga","money Rp." )
             ->add ( "Visite", "visite_dokter", "nonzero-money Rp." )
             ->add ( "Konsul", "konsul_dokter", "nonzero-money Rp." )
             ->add ( "Tindakan Medis Non Operatif Rawat Inap", "tindakan_dokter_ranap", "nonzero-money Rp." )
             ->add ( "Tindakan Medis Non Operatif Rawat Jalan", "tindakan_dokter_rajal", "nonzero-money Rp." )
             ->add ( "Periksa Dokter Poli", "periksa_rajal", "nonzero-money Rp." )
             ->add ( "Periksa Dokter IGD", "periksa_igd", "nonzero-money Rp." );
             
    
    $dbtable = new DBTable ( $db, "smis_ksr_kolektif" );
    $dbtable ->setShowAll(true);

    $dari = $_POST['dari'];
    $sampai = $_POST['sampai'];
    $ruangan = $_POST['ruangan'];
    $carabayar = $_POST['carabayar'];
    $dokter = $_POST['dokter'];
    $id_dokter = $_POST['id_dokter'];
    $tindakan = $_POST['tindakan'];
    

    

    $query = "
        SELECT DATE(tanggal_pulang) as waktu, 
         if(jenis_tagihan='visite_dokter','Visite',
            if(jenis_tagihan='konsul_dokter','Konsul',
                if(jenis_tagihan='tindakan_dokter' AND urjigd!='URJ','Tindakan Medis Non Operatif Ranap',
                    if(jenis_tagihan='konsultasi_dokter' AND urjigd!='URJ','Pemeriksaan IGD',
                        if(jenis_tagihan='konsultasi_dokter' AND urjigd='URJ','Pemeriksaan Poli','Tindakan Medis Non Operatif Rajal')
                    )
                )
            )
        ) as nama_tagihan,
        if(jenis_tagihan='visite_dokter',1,
            if(jenis_tagihan='konsul_dokter',2,
                if(jenis_tagihan='tindakan_dokter' AND urjigd!='URJ',3,
                    if(jenis_tagihan='konsultasi_dokter' AND urjigd!='URJ',4,
                        if(jenis_tagihan='konsultasi_dokter' AND urjigd='URJ',5,6)
                    )
                )
            )
        ) as urut,
        nama_dokter,
        SUM(total) as harga,
        SUM(if(jenis_tagihan='visite_dokter',jaspel_dokter,0)) as visite_dokter,
        SUM(if(jenis_tagihan='konsul_dokter',jaspel_dokter,0)) as konsul_dokter,
        SUM(if(jenis_tagihan='tindakan_dokter' AND urjigd!='URJ',jaspel_dokter,0)) as tindakan_dokter_ranap,
        SUM(if(jenis_tagihan='tindakan_dokter' AND urjigd='URJ',jaspel_dokter,0)) as tindakan_dokter_rajal,
        SUM(if(jenis_tagihan='konsultasi_dokter' AND urjigd!='URJ',jaspel_dokter,0)) as periksa_igd,
        SUM(if(jenis_tagihan='konsultasi_dokter' AND urjigd='URJ',jaspel_dokter,0)) as periksa_rajal,
        SUM(jaspel_dokter) as total
        FROM smis_ksr_kolektif 
        WHERE   smis_ksr_kolektif.prop!='del'
                AND tutup_tagihan=1
                AND jaspel_dokter>0
                AND DATE(tanggal_pulang) >= '$dari'
                AND DATE(tanggal_pulang) <= '$sampai'
                ".($ruangan==""?"":" AND ruangan = '$ruangan' ")."
                ".($dokter==""?"":" AND id_dokter = '$id_dokter'")."
                ".($carabayar==""?"":" AND carabayar = '$carabayar' ")."
        
    ";

    if($tindakan!=""){
        if($tindakan == "visite_dokter"){
            $query .= " AND jenis_tagihan='visite_dokter' ";
        }else if($tindakan == "konsul_dokter"){
            $query .= " AND jenis_tagihan='konsul_dokter' ";
        }else if($tindakan == "tindakan_dokter_ranap"){
            $query .= " AND jenis_tagihan='tindakan_dokter' AND urjigd!='URJ' ";
        }else if($tindakan == "tindakan_dokter_rajal"){
            $query .= " AND jenis_tagihan='tindakan_dokter' AND urjigd='URJ' ";
        }else if($tindakan == "periksa_rajal"){
            $query .= " AND jenis_tagihan = 'konsultasi_dokter' AND urjigd='URJ' ";
        }else if($tindakan == "periksa_igd"){
            $query .= " AND jenis_tagihan='konsultasi_dokter' AND urjigd!='URJ' ";
        }
    }

    $query .="  GROUP BY DATE(tanggal_pulang), jenis_tagihan, id_dokter ORDER BY DATE(tanggal_pulang), id_dokter, urut ";

    $qc = "SELECT 1 as total ";

    $dbtable ->setPreferredQuery(true,$query,$qc);

    $dbres   = new LaporanJaspelResponder ( $dbtable, $uitable, $adapter );
    $dbres ->setFileName("Laporan Jaspel Tutup Tagihan");
	
    /*
    if($dbres->is("excel")){
        $adapter ->add ( "Tanggal", "waktu","date d M Y" )
                 ->add ( "Pembayaran", "jenis", "unslug" )
                 ->add ( "Keterangan", "keterangan" )
                 ->add ( "Nilai", "nilai")
                 ->add ( "No Referensi", "noref" )
                 ->addFixValue ( "Tanggal", "Total" )
                 ->addFixValue ( "Pembayaran", " " )
                 ->addFixValue ( "Keterangan", " " )
                 ->addFixValue ( "id", " " )
                 ->addSummary ( "Nilai", "nilai" );
    }*/
    
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=null){
        echo json_encode ( $data );        
    }
	return;
}


require_once 'finance/class/MapRuangan.php';

$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$tindakan = new OptionBuilder();
$tindakan ->add("Visite","visite_dokter");
$tindakan ->add("Konsul","konsul_dokter");
$tindakan ->add("Tindakan Dokter Non Operatif Ranap","tindakan_dokter_ranap");
$tindakan ->add("Tindakan Dokter Non Operatif Poli","tindakan_dokter");
$tindakan ->add("Periksa Dokter IGD","periksa_igd");
$tindakan ->add("Periksa Dokter Poli","periksa_rajal");
$tindakan ->add("","",1);


$uitable ->addModal("dari","date","Dari","")
         ->addModal("sampai","date","Sampai","")
         ->addModal("ruangan","select","Ruangan",$ruangan)
         ->addModal("carabayar","select","Carabayar",$jenis_pembayaran)
         ->addModal("dokter", "chooser-laporan_jaspel_tutup_tagihan-dokter_tutup_tagihan-Pilih Dokter", "Dokter", "")
         ->addModal("id_dokter","hidden","","")
         ->addModal("tindakan","select","Tindakan",$tindakan->getContent());

$btn    = new Button ( "", "", "" );
$btn    ->setIsButton ( Button::$ICONIC )
        ->setIcon ( "fa fa-refresh" )
        ->setClass( "btn-primary" )
        ->setAction ( "laporan_jaspel_tutup_tagihan.view()" );

$excel  = new Button ( "", "", "" );
$excel  ->setIsButton ( Button::$ICONIC )
        ->setIcon ( "fa fa-file-excel-o" )
        ->setClass( "btn-primary" )
        ->setAction ( "laporan_jaspel_tutup_tagihan.excel()" );

$form   = $uitable->getModal()->getForm();
$form   ->addElement("",$btn)
        ->addElement("",$excel);

echo $form    ->getHtml();
echo $uitable ->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("finance/resource/js/laporan_jaspel_tutup_tagihan.js",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");
?>