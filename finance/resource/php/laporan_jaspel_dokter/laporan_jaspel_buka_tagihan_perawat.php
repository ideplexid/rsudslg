<?php
global $db;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']=="dokter_buka_tagihan_perawat") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	
	$dktable=new Table($head);
	$dktable->setName("dokter_buka_tagihan_perawat");
	$dktable->setModel(Table::$SELECT);	
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"dokter");
	
	$super=new SuperCommand();
	$super->addResponder("dokter_buka_tagihan_perawat",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

$header  = array ('No.','Tanggal','Tindakan',"Harga","Nama Ruangan","Tindakan Keperawatan","Pemeriksaan Poli","Tindakan Medis Non Operatif Rawat Jalan","Total Jaspel" );
$uitable = new Table ($header, "", NULL, false);
$uitable ->setName ( "laporan_jaspel_buka_tagihan_perawat" );
$uitable ->setFooterVisible(false);

if (isset ( $_POST ['command'] )) {
    require_once "finance/class/adapter/LaporanJaspelPerawatAdapter.php";
    require_once "finance/class/responder/LaporanJaspelPerawatResponder.php";
    $adapter = new LaporanJaspelPerawatAdapter ();
	$adapter ->add ( "Tanggal", "waktu" ,"date d M Y")
             ->add ( "Tindakan", "nama_tagihan" )
             ->add ( "Harga", "harga","money Rp." )
             ->add ( "Nama Ruangan", "nama_ruangan","unslug" )
             ->add ( "Tindakan Keperawatan", "tindakan_perawat", "nonzero-money Rp." )
             ->add ( "Pemeriksaan Poli", "periksa_dokter", "nonzero-money Rp." )
             ->add ( "Tindakan Medis Non Operatif Rawat Jalan", "tindakan_dokter", "nonzero-money Rp." );
    
    $dbtable = new DBTable ( $db, "smis_ksr_kolektif" );
    $dbtable ->setShowAll(true);

    $dari           = $_POST['dari'];
    $sampai         = $_POST['sampai'];
    $ruangan        = $_POST['ruangan'];
    $carabayar      = $_POST['carabayar'];
    $dokter         = $_POST['dokter'];
    $id_dokter      = $_POST['id_dokter'];
    $tindakan       = $_POST['tindakan'];
    
    $query = "
        SELECT DATE(tanggal_masuk) as waktu, 
         if(jenis_tagihan='tindakan_dokter','Tindakan Dokter',
            if(jenis_tagihan='konsultasi_dokter','Periksa Dokter',
                if(jenis_tagihan='tindakan_perawat' ,'Tindakan Perawat','')
            )
        ) as nama_tagihan,
        if(jenis_tagihan='tindakan_dokter',1,
            if(jenis_tagihan='konsultasi_dokter',2,
                if(jenis_tagihan='tindakan_perawat' ,3,4)
            )
        ) as urut,
        nama_tagihan as tindakan,
        SUM(total) as harga,
        ruangan as nama_ruangan,
        SUM(if(jenis_tagihan='tindakan_dokter',jaspel_perawat,0)) as tindakan_dokter,
        SUM(if(jenis_tagihan='konsultasi_dokter' ,jaspel_perawat,0)) as periksa_dokter,
        SUM(if(jenis_tagihan='tindakan_perawat',jaspel_perawat,0)) as tindakan_perawat,
        SUM(jaspel_perawat) as total
        FROM smis_ksr_kolektif 
        WHERE   smis_ksr_kolektif.prop!='del'
                AND tutup_tagihan=0
                AND jaspel_perawat>0
                AND DATE(tanggal_masuk) >= '$dari'
                AND DATE(tanggal_masuk) <= '$sampai'
                AND urjigd='URJ'
                ".($ruangan==""?"":" AND ruangan = '$ruangan' ")."
                ".($dokter==""?"":" AND id_dokter = '$id_dokter'")."
                ".($carabayar==""?"":" AND carabayar = '$carabayar' ")."
        
    ";

    if($tindakan!=""){
        if($tindakan == "tindakan_dokter"){
            $query .= " AND jenis_tagihan='tindakan_dokter'  ";
        }else if($tindakan == "periksa_dokter"){
            $query .= " AND jenis_tagihan='konsultasi_dokter' ";
        }else if($tindakan == "tindakan_perawat"){
            $query .= " AND jenis_tagihan='tindakan_perawat' ";
        }
    }else{
        $query .= " AND (jenis_tagihan='tindakan_perawat' OR jenis_tagihan='konsultasi_dokter' OR jenis_tagihan='tindakan_dokter' ) ";
    }

    $query .="  GROUP BY DATE(tanggal_masuk), jenis_tagihan, id_dokter ORDER BY DATE(tanggal_masuk), id_dokter, urut ";

    $qc = "SELECT 1 as total ";

    $dbtable ->setPreferredQuery(true,$query,$qc);

    $dbres   = new LaporanJaspelPerawatResponder ( $dbtable, $uitable, $adapter );
    $dbres ->setFileName("Laporan Jaspel Perawat Buka Tagihan");
	
    /*
    if($dbres->is("excel")){
        $adapter ->add ( "Tanggal", "waktu","date d M Y" )
                 ->add ( "Pembayaran", "jenis", "unslug" )
                 ->add ( "Keterangan", "keterangan" )
                 ->add ( "Nilai", "nilai")
                 ->add ( "No Referensi", "noref" )
                 ->addFixValue ( "Tanggal", "Total" )
                 ->addFixValue ( "Pembayaran", " " )
                 ->addFixValue ( "Keterangan", " " )
                 ->addFixValue ( "id", " " )
                 ->addSummary ( "Nilai", "nilai" );
    }*/
    
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=null){
        echo json_encode ( $data );        
    }
	return;
}


require_once 'finance/class/MapRuangan.php';

$ruangan=MapRuangan::getRuangan();
$ruangan[]=array("name"=>" - SEMUA - ","value"=>"","default"=>"1");

$service = new ServiceConsumer ( $db, "get_carabayar" );
$service->execute ();
$dataasuransi= $service->getContent ();
$adapter=new SelectAdapter("nama", "slug");
$jenis_pembayaran=$adapter->getContent($dataasuransi);
$jenis_pembayaran[]=array("name"=>"","value"=>"","default"=>"1");


$tindakan = new OptionBuilder();
$tindakan ->add("Tindakan Dokter Non Operatif Poli","tindakan_dokter");
$tindakan ->add("Periksa Dokter IGD","periksa_dokter");
$tindakan ->add("Tindakan Keperawatan","tindakan_perawat");
$tindakan ->add("","",1);


$uitable ->addModal("dari","date","Dari","")
         ->addModal("sampai","date","Sampai","")
         ->addModal("ruangan","select","Ruangan",$ruangan)
         ->addModal("carabayar","select","Carabayar",$jenis_pembayaran)
         ->addModal("dokter", "chooser-laporan_jaspel_buka_tagihan_perawat-dokter_buka_tagihan-Pilih Dokter", "Dokter", "")
         ->addModal("id_dokter","hidden","","")
         ->addModal("tindakan","select","Tindakan",$tindakan->getContent());

$btn    = new Button ( "", "", "" );
$btn    ->setIsButton ( Button::$ICONIC )
        ->setIcon ( "fa fa-refresh" )
        ->setClass( "btn-primary" )
        ->setAction ( "laporan_jaspel_buka_tagihan_perawat.view()" );

$excel  = new Button ( "", "", "" );
$excel  ->setIsButton ( Button::$ICONIC )
        ->setIcon ( "fa fa-file-excel-o" )
        ->setClass( "btn-primary" )
        ->setAction ( "laporan_jaspel_buka_tagihan_perawat.excel()" );

$form   = $uitable->getModal()->getForm();
$form   ->addElement("",$btn)
        ->addElement("",$excel);

echo $form    ->getHtml();
echo $uitable ->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("finance/resource/js/laporan_jaspel_buka_tagihan_perawat.js",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");
?>