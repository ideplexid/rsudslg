<?php
setChangeCookie ( false );
$header  = array ('Tanggal','Pembayaran','Keterangan',"Nilai","No Referensi" );
$uitable = new Table ($header, "", NULL, false);
$uitable ->setName ( "laporan_pengeluaran" );

if (isset ( $_POST ['command'] )) {
	$adapter = new SummaryAdapter ();
	$adapter ->add ( "Tanggal", "waktu" ,"date d M Y")
             ->add ( "Pembayaran", "jenis", "unslug" )
             ->add ( "Keterangan", "keterangan" )
             ->add ( "Nilai", "nilai", "money Rp." )
             ->add ( "No Referensi", "noref" )
             ->addFixValue ( "Tanggal", "<strong>Total</strong>" )
             ->addSummary ( "Nilai", "nilai", "money Rp." );
    
	$dbtable = new DBTable ( $db, "smis_fnc_bayar" );
	$dbtable ->addCustomKriteria ( "status", "='1'" )
             ->addCustomKriteria("jenis", "!='diskon_faktur'")
             ->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbres   = new DBResponder ( $dbtable, $uitable, $adapter );
	if ($dbres->isView ()) {
		if (isset ( $_POST ['dari'] ) && $_POST ['dari'] != "" && $_POST ['dari'] != "0000-00-00")
			$dbtable->addCustomKriteria ( "tgl_cair >= ", "'" . $_POST ['dari'] . "'" );
		if (isset ( $_POST ['sampai'] ) && $_POST ['sampai'] != "" && $_POST ['sampai'] != "0000-00-00")
			$dbtable->addCustomKriteria ( "tgl_cair <= ", "'" . $_POST ['sampai'] . "'" );
        if (isset ( $_POST ['jenis'] ) && $_POST ['jenis'] != "")
			$dbtable->addCustomKriteria ( "jenis = ", "'" . $_POST ['jenis'] . "'" );
	}
    
    if($dbres->is("excel")){
        $adapter ->add ( "Tanggal", "waktu","date d M Y" )
                 ->add ( "Pembayaran", "jenis", "unslug" )
                 ->add ( "Keterangan", "keterangan" )
                 ->add ( "Nilai", "nilai")
                 ->add ( "No Referensi", "noref" )
                 ->addFixValue ( "Tanggal", "Total" )
                 ->addFixValue ( "Pembayaran", " " )
                 ->addFixValue ( "Keterangan", " " )
                 ->addFixValue ( "id", " " )
                 ->addSummary ( "Nilai", "nilai" );
    }
    
	$data = $dbres->command ( $_POST ['command'] );
	if($data!=null){
        echo json_encode ( $data );        
    }
	return;
}

$query = "SELECT distinct jenis FROM smis_fnc_bayar";
$res   = $db->get_result($query);
$jenis = new OptionBuilder();
$jenis ->add("","","1");
foreach($res as $x){
    $jenis ->add(ArrayAdapter::slugFormat("unslug",$x->jenis),$x->jenis,"0");
}

$uitable ->addModal("dari","date","Dari","")
         ->addModal("sampai","date","Sampai","")
         ->addModal("jenis","select","Jenis",$jenis->getContent());

$btn    = new Button ( "", "", "" );
$btn    ->setIsButton ( Button::$ICONIC )
        ->setIcon ( "fa fa-refresh" )
        ->setClass( "btn-primary" )
        ->setAction ( "laporan_pengeluaran.view()" );

$excel  = new Button ( "", "", "" );
$excel  ->setIsButton ( Button::$ICONIC )
        ->setIcon ( "fa fa-file-excel-o" )
        ->setClass( "btn-primary" )
        ->setAction ( "laporan_pengeluaran.excel()" );

$form   = $uitable->getModal()->getForm();
$form   ->addElement("",$btn)
        ->addElement("",$excel);

echo $form    ->getHtml();
echo $uitable ->getHtml();
echo addJS  ("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS  ("framework/smis/js/table_action.js");
echo addJS  ("finance/resource/js/laporan_pengeluaran.js",false);
echo addCSS ("framework/bootstrap/css/datepicker.css");
?>