<?php
setChangeCookie ( false );
$header=array ('Tanggal','Persentase',"Nilai",'Keterangan',"No Referensi" );
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_diskon" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SimpleAdapter ();
	$adapter->add ( "Tanggal", "waktu","date d M Y" );
	$adapter->add ( "Pembayaran", "jenis", "unslug" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Persentase", "persen" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "No Referensi", "noref" );
	$dbtable = new DBTable ( $db, "smis_fnc_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria("jenis", "='diskon_faktur'");
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if ($dbres->isView ()) {
		if (isset ( $_POST ['dari'] ) && $_POST ['dari'] != "" && $_POST ['dari'] != "0000-00-00")
			$dbtable->addCustomKriteria ( "waktu >= ", "'" . $_POST ['dari'] . "'" );
		if (isset ( $_POST ['sampai'] ) && $_POST ['sampai'] != "" && $_POST ['sampai'] != "0000-00-00")
			$dbtable->addCustomKriteria ( "waktu <= ", "'" . $_POST ['sampai'] . "'" );
	}
	$data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$dari = new Text ( "dari_diskon", "", "" );
$dari->setModel ( Text::$DATE );
$sampai = new Text ( "sampai_diskon", "", "" );
$sampai->setModel ( Text::$DATE );
$btn = new Button ( "", "", "" );
$btn->setIsButton ( Button::$ICONIC );
$btn->setIcon ( "fa fa-refresh" );
$btn->setClass( "btn-primary" );
$btn->setAction ( "laporan_diskon.view()" );
$form = new Form ( "", "", "" );
$form->addElement ( "Dari", $dari );
$form->addElement ( "Sampai", $sampai );
$form->addElement ( "", $btn );

echo $form->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/smis/js/table_action.js" );

?>

<script type="text/javascript">
var laporan_diskon;
//var employee;
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','waktu','keterangan','jenis',"nilai","noref");
	laporan_diskon=new TableAction("laporan_diskon","finance","laporan_diskon",column);
	laporan_diskon.getViewData=function(){
		var view_data=this.getRegulerData();
		view_data['command']="list";
		view_data['kriteria']=$("#"+this.prefix+"_kriteria").val();
		view_data['number']=$("#"+this.prefix+"_number").val();
		view_data['max']=$("#"+this.prefix+"_max").val();
		view_data['dari']=$("#dari_diskon").val();
		view_data['sampai']=$("#sampai_diskon").val();
		return view_data;
	};
	laporan_diskon.view();
});
</script>
