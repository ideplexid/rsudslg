<?php
setChangeCookie ( false );
$header=array ('Tanggal','Pembayaran','Keterangan',"Nilai","No Referensi" );
$uitable = new Table ( $header, "", NULL, false );
$uitable->setName ( "laporan_hutang" );
if (isset ( $_POST ['command'] )) {
	$adapter = new SummaryAdapter ();
	$adapter->add ( "Tanggal", "waktu","date d M Y" );
	$adapter->add ( "Pembayaran", "jenis", "unslug" );
	$adapter->add ( "Keterangan", "keterangan" );
	$adapter->add ( "Nilai", "nilai", "money Rp." );
	$adapter->add ( "No Referensi", "noref" );
    $adapter->addFixValue ( "Tanggal", "<strong>Total</strong>" );
    $adapter->addSummary ( "Nilai", "nilai", "money Rp." );
    
	$dbtable = new DBTable ( $db, "smis_fnc_bayar" );
	$dbtable->setFetchMethode ( DBTable::$ARRAY_FETCH );
	$dbtable->addCustomKriteria ( "status", "='0'" );
	$dbtable->addCustomKriteria("jenis", !"='diskon_faktur'");
	$dbres = new DBResponder ( $dbtable, $uitable, $adapter );
	if ($dbres->isView ()) {
		if (isset ( $_POST ['dari'] ) && $_POST ['dari'] != "" && $_POST ['dari'] != "0000-00-00")
			$dbtable->addCustomKriteria ( "waktu >= ", "'" . $_POST ['dari'] . "'" );
		if (isset ( $_POST ['sampai'] ) && $_POST ['sampai'] != "" && $_POST ['sampai'] != "0000-00-00")
			$dbtable->addCustomKriteria ( "waktu <= ", "'" . $_POST ['sampai'] . "'" );
	}
    
    
    if($dbres->is("excel")){
        $adapter->add ( "Tanggal", "waktu","date d M Y" );
        $adapter->add ( "Pembayaran", "jenis", "unslug" );
        $adapter->add ( "Keterangan", "keterangan" );
        $adapter->add ( "Nilai", "nilai");
        $adapter->add ( "No Referensi", "noref" );
        $adapter->addFixValue ( "Tanggal", "Total" );
        $adapter->addFixValue ( "Pembayaran", " " );
        $adapter->addFixValue ( "Keterangan", " " );
        $adapter->addFixValue ( "id", " " );
        $adapter->addSummary ( "Nilai", "nilai" );
    }
    
	$data = $dbres->command ( $_POST ['command'] );
    if($data!=null){
        echo json_encode ( $data );        
    }
	return;
}

$dari = new Text ( "dari_hutang", "", "" );
$dari->setModel ( Text::$DATE );
$sampai = new Text ( "sampai_hutang", "", "" );
$sampai->setModel ( Text::$DATE );

$btn = new Button ( "", "", "" );
$btn->setIsButton ( Button::$ICONIC );
$btn->setIcon ( "fa fa-refresh" );
$btn->setClass( "btn-primary" );
$btn->setAction ( "laporan_hutang.view()" );

$excel = new Button ( "", "", "" );
$excel->setIsButton ( Button::$ICONIC );
$excel->setIcon ( "fa fa-file-excel-o" );
$excel->setClass( "btn-primary" );
$excel->setAction ( "laporan_hutang.excel()" );


$form = new Form ( "", "", "" );
$form->addElement ( "Dari", $dari );
$form->addElement ( "Sampai", $sampai );
$form->addElement ( "", $btn );
$form->addElement ( "", $excel );


echo $form->getHtml();
echo $uitable->getHtml ();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script type="text/javascript">
var laporan_hutang;
//var employee;
$(document).ready(function(){
    $(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','waktu','keterangan','jenis',"nilai","noref");
	laporan_hutang=new TableAction("laporan_hutang","finance","laporan_hutang",column);
	laporan_hutang.getViewData=function(){
		var view_data=this.getRegulerData();
		view_data['command']="list";
		view_data['kriteria']=$("#"+this.prefix+"_kriteria").val();
		view_data['number']=$("#"+this.prefix+"_number").val();
		view_data['max']=$("#"+this.prefix+"_max").val();
		view_data['dari']=$("#dari_hutang").val();
		view_data['sampai']=$("#sampai_hutang").val();
		return view_data;
	};
	laporan_hutang.view();
});
</script>
