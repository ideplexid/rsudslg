var bulk_faktur;
var bulk_faktur_detail;
var nofaktur_bulk;
$(document).ready(function(){
    $(".mydate").datepicker();
    var column          = new Array("id","noref","jt_bayar","id_faktur","nilai","keterangan","nama_vendor","waktu","code","id_vendor","id_bulk","id_dompet","nama_dompet","no_akun","penerima","asal_faktur","jenis");    
    bulk_faktur_detail  = new TableAction("bulk_faktur_detail","finance","bulk_faktur_detail",column);
    bulk_faktur_detail  .addNoClear("nama_vendor")
                        .addNoClear("waktu")
                        .addNoClear("code")
                        .addNoClear("id_vendor")
                        .addNoClear("id_bulk")
                        .addNoClear("id_dompet")
                        .addNoClear("nama_dompet")
                        .addNoClear("no_akun")
                        .addNoClear("penerima")
                        .addNoClear("asal_faktur");
    bulk_faktur_detail.show_add_form = function(){
        this.clear(false);
        this.show_form();
        return this;
    };

    bulk_faktur_detail.back  = function(){
        var data             = this.getRegulerData();
        data['action']       = "bulk_faktur";
        showLoading();
        $.post("",data,function(res){
            $("#bulk_faktur").html(res);
            dismissLoading();
        });
    };

    bulk_faktur_detail.addRegulerData   = function(data){
        data["nama_vendor"]  = this.get("nama_vendor");
        data["waktu"]        = this.get("waktu");
        data["code"]         = this.get("code");
        data["id_vendor"]    = this.get("id_vendor");
        data["id_bulk"]      = this.get("id_bulk");
        data["id_dompet"]    = this.get("id_dompet");
        data["nama_dompet"]  = this.get("nama_dompet");
        data["no_akun"]      = this.get("no_akun");
        data["penerima"]     = this.get("penerima");
        data["asal_faktur"]  = this.get("asal_faktur");
        return data;
    };

    nofaktur_bulk = new TableAction("nofaktur_bulk","finance","bulk_faktur_detail",new Array());
    nofaktur_bulk .setSuperCommand("nofaktur_bulk");
    nofaktur_bulk .addRegulerData=function(data){
        data["asal_faktur"]  = bulk_faktur_detail.get("asal_faktur");
        data["id_vendor"]    = bulk_faktur_detail.get("id_vendor");
        return data;
    };

    nofaktur_bulk.selected=function(json){
        bulk_faktur_detail .set("jt_bayar",json.tanggal_tempo)
                           .set("noref",json.no_faktur)
                           .set("nilai",json.total_tagihan)
                           .set("keterangan",json.keterangan)
                           .set("id_faktur",json.id);
    };

    bulk_faktur = new TableAction("bulk_faktur","finance","bulk_faktur_detail",new Array());
    bulk_faktur.setSuperCommand("bulk_faktur");
    bulk_faktur.selected = function(json){
        if(json.status==1){
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "finance/resource/css/bulk_faktur_detail.css"
             }).appendTo("div#bulk_faktur");
        }
        bulk_faktur_detail  .set("nama_vendor",json.nama_vendor)
                            .set("waktu",json.waktu)
                            .set("code",json.code)
                            .set("id_vendor",json.id_vendor)
                            .set("id_bulk",json.id)
                            .set("id_dompet",json.id_dompet)
                            .set("nama_dompet",json.nama_dompet)
                            .set("no_akun",json.no_akun)
                            .set("penerima",json.penerima)
                            .set("asal_faktur",json.asal_faktur)
                            .view();
    };    
    var id_bulk = bulk_faktur_detail.get("id_bulk");
    bulk_faktur.select(id_bulk);
});