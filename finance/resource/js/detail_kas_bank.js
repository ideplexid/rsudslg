function DetailKasBankAction(name,page,action,column){
	TableAction.call( this, name,page,action,column);
}
DetailKasBankAction.prototype.constructor = DetailKasBankAction;
DetailKasBankAction.prototype = Object.create( TableAction.prototype );
DetailKasBankAction.prototype.backward=function(id){
    showLoading();
	var self=this;
	var edit_data=this.getEditData(id);
	var data = {
        page                : self.page,
        action              : self.action.substr(7),
        prototype_name      : self.prototype_name,
        prototype_slug      : self.prototype_slug,
        prototype_implement : self.prototype_implement
    }
    $.post("",data,function(res){
        $("#"+self.action.substr(7)).html(res);
        dismissLoading();
    });
};