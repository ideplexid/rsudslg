var bulk_umum;
var bulk_umum_detail;
var beban_bulk;
$(document).ready(function(){
    $(".mydate").datepicker();
    var column          = new Array("id","noref","nilai","keterangan","deskripsi","html","nama_vendor","waktu","code","id_bulk","id_dompet","nama_dompet","no_akun","penerima","asal_faktur","jenis","debet","kredit");    
    bulk_umum_detail    = new TableAction("bulk_umum_detail","finance","bulk_umum_detail",column);
    bulk_umum_detail    .addNoClear("nama_vendor")
                        .addNoClear("waktu")
                        .addNoClear("code")
                        .addNoClear("id_vendor")
                        .addNoClear("id_bulk")
                        .addNoClear("id_dompet")
                        .addNoClear("nama_dompet")
                        .addNoClear("no_akun")
                        .addNoClear("penerima");
    bulk_umum_detail.show_add_form = function(){
        this.clear(false);
        this.show_form();
        return this;
    };

    bulk_umum_detail.back  = function(){
        var data             = this.getRegulerData();
        data['action']       = "bulk_umum";
        showLoading();
        $.post("",data,function(res){
            $("#bulk_umum").html(res);
            dismissLoading();
        });
    };

    bulk_umum_detail.addRegulerData   = function(data){
        data["nama_vendor"]  = this.get("nama_vendor");
        data["waktu"]        = this.get("waktu");
        data["code"]         = this.get("code");
        data["id_vendor"]    = this.get("id_vendor");
        data["id_bulk"]      = this.get("id_bulk");
        data["id_dompet"]    = this.get("id_dompet");
        data["nama_dompet"]  = this.get("nama_dompet");
        data["no_akun"]      = this.get("no_akun");
        data["penerima"]     = this.get("penerima");
        return data;
    };

    beban_bulk = new TableAction("beban_bulk","finance","bulk_umum_detail",new Array());
    beban_bulk .setSuperCommand("beban_bulk");
    beban_bulk.selected=function(json){
        bulk_umum_detail .set("keterangan",json.keterangan)
                         .set("debet",json.debet)
                         .set("kredit",json.kredit);
    };

    bulk_umum = new TableAction("bulk_umum","finance","bulk_umum_detail",new Array());
    bulk_umum.setSuperCommand("bulk_umum");
    bulk_umum.selected = function(json){
        if(json.status==1){
            $("<link/>", {
                rel  : "stylesheet",
                type : "text/css",
                href : "finance/resource/css/bulk_faktur_detail.css"
             }).appendTo("div#bulk_umum");
        }
        bulk_umum_detail .set("nama_vendor",json.nama_vendor)
                         .set("waktu",json.waktu)
                         .set("code",json.code)
                         .set("id_vendor",json.id_vendor)
                         .set("id_bulk",json.id)
                         .set("id_dompet",json.id_dompet)
                         .set("nama_dompet",json.nama_dompet)
                         .set("no_akun",json.no_akun)
                         .set("penerima",json.penerima)
                         .set("asal_faktur",json.asal_faktur)
                         .view();
    };    
    var id_bulk = bulk_umum_detail.get("id_bulk");
    bulk_umum.select(id_bulk);
});