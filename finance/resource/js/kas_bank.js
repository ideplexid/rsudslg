function KasBankAction(name,page,action,column){
	TableAction.call( this, name,page,action,column);
}
KasBankAction.prototype.constructor = KasBankAction;
KasBankAction.prototype = Object.create( TableAction.prototype );
KasBankAction.prototype.detail_transaksi=function(id){
    showLoading();
	var self=this;
	var edit_data=this.getEditData(id);
	$.post('',edit_data,function(res){
		var json=getContent(res);
		if(json!=null) {
        	var data = {
                page                : self.page,
                action              : "detail_"+self.action,
                prototype_name      : self.prototype_name,
                prototype_slug      : self.prototype_slug,
                prototype_implement : self.prototype_implement,
                id_transaksi        : json.id,
                nomor               : json.nomor,
                tanggal             : json.tanggal,
                keterangan          : json.keterangan,
                io                  : json.io,
                grup                : json.grup
            }
            showLoading();
            $.post("",data,function(res){
                $("#"+self.action).html(res);
                dismissLoading();
            });
		}
        dismissLoading();
	});
};

KasBankAction.prototype.postAction=function(id){
    if(!this.is_post_action_del)
        this.detail_transaksi(id);
};

KasBankAction.prototype.afterview=function(json){
    showLoading();
	var self=this;
	var data=this.getRegulerData();
    data['action']="get_last_saldo";
    data['akun']=$("#"+this.prefix+"_akun").val();
	$.post('',data,function(res){
		var json=getContent(res);
        var uang=Number(json)
        setMoney("#"+self.prefix+"_saldo",uang);
		dismissLoading();
	});
};