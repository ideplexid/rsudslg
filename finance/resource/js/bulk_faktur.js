$(document).ready(function(){
    bulk_faktur.detail_bulk  = function(id){
        var data             = this.getRegulerData();
        data['action']       = "bulk_faktur_detail";
        data['id_bulk']      = id;
        showLoading();
        $.post("",data,function(res){
            $("#bulk_faktur").html(res);
            dismissLoading();
        });
    };

    bulk_faktur.cair  = function(id){
        var self             = this;        
        bootbox.confirm("Anda Yakin akan mencairkan Pembayaran Gerombolan ini ? Proses ini tidak dapat dikembalikan",function(r){
            if(r){
                var data             = self.getRegulerData();
                data['command']      = "save";
                data['status']       = "1";
                data['id']           = id;
                showLoading();
                $.post("",data,function(res){
                    self.view();
                    dismissLoading();
                });
            }
        });
    };

    bulk_faktur.postAction = function(id){
        bulk_faktur.detail_bulk(id);
    };

    bulk_faktur.setDisabledOnEdit(new Array("nama_vendor","asal_faktur","nama_dompet","chooser_nama_vendor","chooser_nama_dompet"));

    dompet_bulk_faktur = new TableAction("dompet_bulk_faktur","finance","bulk_faktur",new Array());
    dompet_bulk_faktur.setSuperCommand("dompet_bulk_faktur");
    dompet_bulk_faktur.selected=function(json){
        bulk_faktur.set("id_dompet",json.id);
        bulk_faktur.set("nama_dompet",json.nama);
        bulk_faktur.set("no_akun",json.nomor_akun);
    };
});