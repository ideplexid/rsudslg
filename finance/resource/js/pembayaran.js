var pembayaran;
var pembayaran_dompet;
var beban;

$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement':'bottom'});
	var column=new Array('id','waktu','jenis','noref','deskripsi','keterangan',"grup","nilai","html","status","id_dompet","nama_dompet","no_akun","penerima","debet","kredit");
	pembayaran=new TableAction("pembayaran","finance","pembayaran",column);
	pembayaran.view();
    
    pembayaran_dompet=new TableAction("pembayaran_dompet","finance","pembayaran",column);
	pembayaran_dompet.setSuperCommand("pembayaran_dompet");
    pembayaran_dompet.selected=function(json){
        $("#"+pembayaran.prefix+"_id_dompet").val(json.id);
        $("#"+pembayaran.prefix+"_nama_dompet").val(json.nama);
        $("#"+pembayaran.prefix+"_no_akun").val(json.nomor_akun);
    };
    
    beban=new TableAction("beban","finance","pembayaran",column);
	beban.setSuperCommand("beban");
    beban.selected=function(json){
        $("#"+pembayaran.prefix+"_keterangan").val(json.nama+" - "+json.keterangan);
        $("#"+pembayaran.prefix+"_debet").val(json.debet);
        $("#"+pembayaran.prefix+"_kredit").val(json.kredit);
    };
});