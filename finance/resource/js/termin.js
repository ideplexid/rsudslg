$(document).ready(function(){
    termin.detail_termin=function(id){
        var data={
            page:"finance",
            action:"termin_detail",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_termin:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#termin").html(res);
            dismissLoading();
        });
    };
    
    termin.upload_termin=function(id){
        var data={
            page:"finance",
            action:"termin",
            command:"upload_termin",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id:id
        }
        showLoading();
        $.post("",data,function(res){
            getContent(res);
            termin.view();
            dismissLoading();
        });
    };
    
});