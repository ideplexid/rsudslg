var laporan_pengeluaran;
$(document).ready(function(){
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','waktu','keterangan','jenis',"nilai","noref");
	laporan_pengeluaran=new TableAction("laporan_pengeluaran","finance","laporan_pengeluaran",column);
	laporan_pengeluaran.getViewData=function(){
		var view_data=this.getRegulerData();
		view_data['command']="list";
		view_data['kriteria']=$("#"+this.prefix+"_kriteria").val();
		view_data['number']=$("#"+this.prefix+"_number").val();
		view_data['max']=$("#"+this.prefix+"_max").val();
		view_data['dari']=$("#"+this.prefix+"_dari").val();
		view_data['sampai']=$("#"+this.prefix+"_sampai").val();
		view_data['jenis']=$("#"+this.prefix+"_jenis").val();
		return view_data;
	};
	laporan_pengeluaran.view();
	
	
});