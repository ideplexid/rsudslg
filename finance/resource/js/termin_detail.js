/**
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /finance/resource/php/kepegawaian/termin_detail.php
 * @since		: 23 September 2017
 * @version		: 1.0.0
 * 
 * */


/*ACTION SCRIPT*/
var termin_detail_termin
var termin_detail;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','id_detail_periode','nip_employee','nama_employee',"sisa","dibayar","gaji","terbayar");
    termin_detail=new TableAction("termin_detail","finance","termin_detail",column);
    termin_detail.addRegulerData=function(x){
       x['id_periode']=$("#termin_detail_id_periode").val();
       x['id_termin']=$("#termin_detail_id_termin").val();
       return x;
    };
    termin_detail.back_to_periode=function(){
        var data={
            page:"finance",
            action:"termin",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_periode:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#termin").html(res);
            dismissLoading();
        })
    };
    
    termin_detail.import=function(){
       var a=this.getRegulerData();
       a['super_command']="termin_detail_gaji";
       a['id_periode']=$("#"+this.prefix+"_id_periode").val();
       a['id_termin']=$("#"+this.prefix+"_id_termin").val();
       showLoading();
       $.post("",a,function(res){
           termin_detail.view();
           dismissLoading();
       });
    };
    
    termin_detail_termin=new TableAction("termin_detail_termin","finance","termin_detail",new Array());
	termin_detail_termin.setSuperCommand("termin_detail_termin");
	termin_detail_termin.selected=function(json){
        $("#termin_detail_id_termin").val(json.id);
		$("#termin_detail_id_periode").val(json.id_periode);
		$("#termin_detail_periode").val(json.periode);
        $("#termin_detail_template").val(json.template);
		$("#termin_detail_tanggal_periode").val(json.tanggal_periode);
        $("#termin_detail_tanggal").val(json.tanggal);
        termin_detail.view();
    };
    
    if($("#termin_detail_id_termin").val()!=""){
        var id=$("#termin_detail_id_termin").val();
        termin_detail_termin.select(id);
    }
});
