var fp;
var id_faktur;
var jt_faktur;
var nofaktur;
var asal_ruang_faktur;

$(document).ready(function(){

    id_faktur           = $("#h_id_faktur").val();
    jt_faktur           = $("#h_jt_faktur").val();
    nofaktur            = $("#h_no_faktur").val();
    asal_ruang_faktur   = $("#h_asal_ruang_faktur").val();
    
    $(".mydate").datepicker();
    var column  = new Array("id","penerima","code","waktu","jt_bayar","noref","nilai","keterangan","json","asal_faktur","id_dompet","nama_dompet","no_akun","id_vendor","nama_vendor");
    fp          = new TableAction("fp","finance","faktur",column);
    
    fp.getRegulerData       = function(){
        var the_id          = "#fp_asal_faktur";
        var data            = {
            page            : this.page,
            action          : this.action,
            super_command   : this.super_command,
            id_faktur       : id_faktur,
            jt_faktur       : jt_faktur,
            nofaktur        : nofaktur,
            ruangan         : asal_ruang_faktur,
            status          : 0,
            jenis           : "pembayaran_faktur"
        };
        return data;
    };
    fp.setSuperCommand("fp");
    fp.cair         = function(id){
        var self    = this;
        bootbox.confirm("Anda Yakin akan mencairkan Kwitansi ini ? Proses ini tidak dapat dikembalikan",function(r){
            if(r){
                var d            = self.getRegulerData();
                d['command']     = "save";
                d['status']      = "1";
                d['asal_faktur'] = asal_ruang_faktur;
                d['id']          = id;
                showLoading();
                $.post("",d,function(res){
                    var json     = getContent(res);
                    self.view();
                    dismissLoading();
                });
            }
        });
    };

    fp.cekSave = function(){
        result = TableAction.prototype.cekSave.call(this);
        if(result){
            var sisa =  Number(getMoney("#faktur_sisa"));
            var nilai = Number(fp.get("nilai"));
            if(nilai>sisa){
                var id_alt	= "#modal_alert_"+this.prefix+"_add_form";		
                var warn	= '<div class="alert alert-block alert-info "><h4>Pemberitahuan</h4> Jumlah Pembayaran Tidak Boleh Lebih dari Sisa Tagihan </div>';
                $(id_alt).html(warn);
                $(id_alt).focus();
                result = false;
            }
        }
        return result;
    }

    fp.view();
    fp.view             = function(){
        showLoading();
        var self        = this;
        var view_data   = this.getViewData();
        $.post('',view_data,function(res){
            var json    = getContent(res);
            if(json==null) {
                return;
            }
            $("#"+self.prefix+"_add_form").smodal('hide');
            $("#"+self.prefix+"_list").html(json.list);
            $("#"+self.prefix+"_pagination").html(json.pagination);		
            hitungFaktur();
            dismissLoading();
        });
    };
    
    
    faktur_dompet           = new TableAction("faktur_dompet","finance","faktur",column);
    faktur_dompet.setSuperCommand("faktur_dompet");
    faktur_dompet.selected  = function(json){
        $("#"+fp.prefix+"_id_dompet").val(json.id);
        $("#"+fp.prefix+"_nama_dompet").val(json.nama);
        $("#"+fp.prefix+"_no_akun").val(json.nomor_akun);
    };
    
});