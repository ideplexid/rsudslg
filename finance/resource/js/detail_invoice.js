/**
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /finance/resource/php/kepegawaian/detail_invoice.php
 * @since		: 13 Oktober 2017
 * @version		: 1.0.0
 * 
 * */

/*ACTION SCRIPT*/
var detail_invoice_invoice;
var detail_invoice;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','id_invoice','deskripsi','kuantitas',"satuan",'harga','diskon');
    formatMoney("#detail_invoice_diskon_global");
    detail_invoice=new TableAction("detail_invoice","finance","detail_invoice",column);
    detail_invoice.addRegulerData=function(x){
       x['id_invoice']=$("#detail_invoice_id_invoice").val();
       return x;
    };
    detail_invoice.back_to_invoice=function(){
        var data={
            page:"finance",
            action:"pembuatan_invoice",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_invoice:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#pembuatan_invoice").html(res);
            dismissLoading();
        })
    };
    
    detail_invoice.postAction=function(id){
        var idx=$("#detail_invoice_id_invoice").val();
        detail_invoice_invoice.select(idx);
    };
    
    detail_invoice_invoice=new TableAction("detail_invoice_invoice","finance","detail_invoice",column);
	detail_invoice_invoice.setSuperCommand("detail_invoice_invoice");
	detail_invoice_invoice.selected=function(json){
        $("#detail_invoice_id_invoice").val(json.id);
		$("#detail_invoice_tanggal").val(json.tanggal);
		$("#detail_invoice_nomor").val(json.nomor);
        $("#detail_invoice_jatuh_tempo").val(json.jatuh_tempo);
        setMoney("#detail_invoice_diskon_global",Number(json.diskon));
        detail_invoice.view();
    };
    
    if($("#detail_invoice_id_invoice").val()!=""){
        var id=$("#detail_invoice_id_invoice").val();
        detail_invoice_invoice.select(id);
    }
});