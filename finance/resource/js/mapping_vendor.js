var mapping_vendor;
var d_hutang;
var k_hutang;
var db_hutang;
var kb_hutang;
var d_piutang;
var k_piutang;
var db_piutang;
var kb_piutang;


var IS_mapping_vendor_RUNNING;
$(document).ready(function(){
    //hutang
    d_hutang=new TableAction("d_hutang","finance","kode_akun",new Array());
    d_hutang.setSuperCommand("d_hutang");
    d_hutang.selected=function(json){
        $("#mapping_vendor_d_hutang").val(json.nomor);
    };
    d_hutang.setOwnChooserData(true);
    d_hutang.setOwnChooserData(true);
    d_hutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    
    k_hutang=new TableAction("k_hutang","finance","kode_akun",new Array());
    k_hutang.setSuperCommand("k_hutang");
    k_hutang.selected=function(json){
        $("#mapping_vendor_k_hutang").val(json.nomor);
    };
    k_hutang.setOwnChooserData(true);
    k_hutang.setOwnChooserData(true);
    k_hutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    
    db_hutang=new TableAction("db_hutang","finance","kode_akun",new Array());
    db_hutang.setSuperCommand("db_hutang");
    db_hutang.selected=function(json){
        $("#mapping_vendor_db_hutang").val(json.nomor);
    };
    db_hutang.setOwnChooserData(true);
    db_hutang.setOwnChooserData(true);
    db_hutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    kb_hutang=new TableAction("kb_hutang","finance","kode_akun",new Array());
    kb_hutang.setSuperCommand("kb_hutang");
    kb_hutang.selected=function(json){
        $("#mapping_vendor_kb_hutang").val(json.nomor);
    };
    kb_hutang.setOwnChooserData(true);
    kb_hutang.setOwnChooserData(true);
    kb_hutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    
    
    //piutang
    d_piutang=new TableAction("d_piutang","finance","kode_akun",new Array());
    d_piutang.setSuperCommand("d_piutang");
    d_piutang.selected=function(json){
        $("#mapping_vendor_d_piutang").val(json.nomor);
    };
    d_piutang.setOwnChooserData(true);
    d_piutang.setOwnChooserData(true);
    d_piutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    k_piutang=new TableAction("k_piutang","finance","kode_akun",new Array());
    k_piutang.setSuperCommand("k_piutang");
    k_piutang.selected=function(json){
        $("#mapping_vendor_k_piutang").val(json.nomor);
    };
    k_piutang.setOwnChooserData(true);
    k_piutang.setOwnChooserData(true);
    k_piutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    
    db_piutang=new TableAction("db_piutang","finance","kode_akun",new Array());
    db_piutang.setSuperCommand("db_piutang");
    db_piutang.selected=function(json){
        $("#mapping_vendor_db_piutang").val(json.nomor);
    };
    db_piutang.setOwnChooserData(true);
    db_piutang.setOwnChooserData(true);
    db_piutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    kb_piutang=new TableAction("kb_piutang","finance","kode_akun",new Array());
    kb_piutang.setSuperCommand("kb_piutang");
    kb_piutang.selected=function(json){
        $("#mapping_vendor_kb_piutang").val(json.nomor);
    };
    kb_piutang.setOwnChooserData(true);
    kb_piutang.setOwnChooserData(true);
    kb_piutang.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    
    
    var column=new Array(
        "id","nama_vendor","id_vendor",
        "d_hutang","k_hutang","db_hutang","kb_hutang",
        "d_piutang","k_piutang","db_piutang","kb_piutang"
    );
    mapping_vendor=new TableAction("mapping_vendor","finance","mapping_vendor",column);
    mapping_vendor.batal=function(){
        smis_loader.cancel();
    };

    mapping_vendor.rekaptotal=function(){
        if(IS_mapping_vendor_RUNNING) return;
        smis_loader.onCancel=function(){
            IS_mapping_vendor_RUNNING=false;
        };
        
        smis_loader.showLoader();
        smis_loader.updateLoader('true',"Fetching total data",0);
        
        IS_mapping_vendor_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var json=getContent(res);
            if(json>0) {
                mapping_vendor.rekaploop(0,json);
            } else {
                smis_loader.cancel();
            }
        });
    };

    mapping_vendor.rekaploop=function(current,total){
        if(current>=total || !IS_mapping_vendor_RUNNING) {
            smis_loader.cancel();
            IS_mapping_vendor_RUNNING=false;
            mapping_vendor.view();
            return;
        }
        smis_loader.updateLoader('true',"Processing... [ "+current+" / "+total+" ] ",(current*100/total));
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['current']=current;			
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){mapping_vendor.rekaploop(++current,total)},100);
        });
    };
    
    mapping_vendor.view();
    
            
});