var laporan_jaspel_buka_tagihan_perawat;
var dokter_buka_tagihan_perawat;
$(document).ready(function(){
    $(".mydate").datepicker();
    laporan_jaspel_buka_tagihan_perawat = new TableAction("laporan_jaspel_buka_tagihan_perawat","finance","laporan_jaspel_buka_tagihan_perawat",new Array());
    laporan_jaspel_buka_tagihan_perawat.addRegulerData = function(x){
        x['dari'] = laporan_jaspel_buka_tagihan_perawat.get('dari');
        x['sampai'] = laporan_jaspel_buka_tagihan_perawat.get('sampai');
        x['ruangan'] = laporan_jaspel_buka_tagihan_perawat.get('ruangan');
        x['carabayar'] = laporan_jaspel_buka_tagihan_perawat.get('carabayar');
        x['dokter'] = laporan_jaspel_buka_tagihan_perawat.get('dokter');
        x['id_dokter'] = laporan_jaspel_buka_tagihan_perawat.get('id_dokter');
        x['tindakan'] = laporan_jaspel_buka_tagihan_perawat.get('tindakan');
        return x;
    }
    dokter_buka_tagihan_perawat = new TableAction("dokter_buka_tagihan_perawat","finance","laporan_jaspel_buka_tagihan_perawat", new Array());
    dokter_buka_tagihan_perawat.setSuperCommand("dokter_buka_tagihan_perawat");
    dokter_buka_tagihan_perawat.selected = function(json){
        laporan_jaspel_buka_tagihan_perawat.set("id_dokter",json.id);
        laporan_jaspel_buka_tagihan_perawat.set("dokter",json.nama);
    };
    
});