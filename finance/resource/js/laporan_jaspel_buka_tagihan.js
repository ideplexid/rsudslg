var laporan_jaspel_buka_tagihan;
var dokter_buka_tagihan;
$(document).ready(function(){
    $(".mydate").datepicker();
    laporan_jaspel_buka_tagihan = new TableAction("laporan_jaspel_buka_tagihan","finance","laporan_jaspel_buka_tagihan",new Array());
    laporan_jaspel_buka_tagihan.addRegulerData = function(x){
        x['dari'] = laporan_jaspel_buka_tagihan.get('dari');
        x['sampai'] = laporan_jaspel_buka_tagihan.get('sampai');
        x['ruangan'] = laporan_jaspel_buka_tagihan.get('ruangan');
        x['carabayar'] = laporan_jaspel_buka_tagihan.get('carabayar');
        x['dokter'] = laporan_jaspel_buka_tagihan.get('dokter');
        x['id_dokter'] = laporan_jaspel_buka_tagihan.get('id_dokter');
        x['tindakan'] = laporan_jaspel_buka_tagihan.get('tindakan');
        return x;
    }
    dokter_buka_tagihan = new TableAction("dokter_buka_tagihan","finance","laporan_jaspel_buka_tagihan", new Array());
    dokter_buka_tagihan.setSuperCommand("dokter_buka_tagihan");
    dokter_buka_tagihan.selected = function(json){
        laporan_jaspel_buka_tagihan.set("id_dokter",json.id);
        laporan_jaspel_buka_tagihan.set("dokter",json.nama);
    };
    
});