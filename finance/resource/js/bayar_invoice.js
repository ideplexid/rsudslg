/**
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @used		: /finance/resource/php/kepegawaian/bayar_invoice.php
 * @since		: 13 Oktober 2017
 * @version		: 1.0.0
 * 
 * */

/*ACTION SCRIPT*/
var bayar_invoice_invoice;
var bayar_invoice;
$(document).ready(function(){
	$('.mydate').datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array('id','id_invoice','deskripsi','tanggal_bayar',"keterangan",'nilai');
    bayar_invoice=new TableAction("bayar_invoice","finance","bayar_invoice",column);
    bayar_invoice.addRegulerData=function(x){
       x['id_invoice']=$("#bayar_invoice_id_invoice").val();
       return x;
    };
    bayar_invoice.back_to_invoice=function(){
        var data={
            page:"finance",
            action:"pembuatan_invoice",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_invoice:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#pembuatan_invoice").html(res);
            dismissLoading();
        })
    };
    
    bayar_invoice_invoice=new TableAction("bayar_invoice_invoice","finance","bayar_invoice",column);
	bayar_invoice_invoice.setSuperCommand("bayar_invoice_invoice");
	bayar_invoice_invoice.selected=function(json){
        $("#bayar_invoice_id_invoice").val(json.id);
		$("#bayar_invoice_tanggal").val(json.tanggal);
		$("#bayar_invoice_nomor").val(json.nomor);
        $("#bayar_invoice_jatuh_tempo").val(json.jatuh_tempo);
        bayar_invoice.view();
    };
    
    if($("#bayar_invoice_id_invoice").val()!=""){
        var id=$("#bayar_invoice_id_invoice").val();
        bayar_invoice_invoice.select(id);
    }
});