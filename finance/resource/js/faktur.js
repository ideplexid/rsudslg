var faktur;
var fkt;	

function hitungFaktur(){
    var id=$("#faktur_id").val();
    var dt=fkt.getRegulerData();
    dt['id']=id;
    dt['command']="sisa";
    dt['ruangan']=$("#faktur_ruangan").val();
    
    showLoading();
    $.post("",dt,function(res){
        var json=getContent(res);
        var diskon=Number(json.diskon);
        var bayar=Number(json.bayar);
        var total_tagihan=Number($("#faktur_hbayar").val());
        var sisa=total_tagihan-diskon-bayar;
        $("#faktur_total_diskon").val("Rp. "+diskon.formatMoney(2,",","."));
        $("#faktur_total_bayar").val("Rp. "+bayar.formatMoney(2,",","."));
        $("#faktur_sisa").val("Rp. "+sisa.formatMoney(2,",","."));
        dismissLoading();	
    });
}

$(document).ready(function(){
    faktur=new TableAction("faktur","finance","faktur",new Array());
    fkt=new TableAction("fkt","finance","faktur",new Array());
    fkt.setSuperCommand("fkt");
    fkt.getRegulerData=function(){
        var r=$("#faktur_ruangan").val();
        var reg_data={	
                page:this.page,
                action:this.action,
                super_command:this.super_command,
                prototype_name:this.prototype_name,
                ruangan:r,
                prototype_slug:this.prototype_slug,
                prototype_implement:this.prototype_implement
                };
        return reg_data;
    };
    fkt.selected=function(json){
        $("#faktur_content").html(json.chtml);
        $("#faktur_no_faktur").val(json.nofaktur);
        $("#faktur_hbayar").val(json.hbayar);
        var tt=Number(json.hbayar);
        $("#faktur_total_tagihan").val("Rp. "+tt.formatMoney(2,",","."));
        $("#faktur_id").val(json.idf);
        hitungFaktur();
    };

    stypeahead("#faktur_no_faktur",3,fkt,"no_faktur",function(item){
        fkt.select(item.id);				
    });
});