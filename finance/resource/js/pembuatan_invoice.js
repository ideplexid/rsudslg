$(document).ready(function(){
    pembuatan_invoice.detail_invoice=function(id){
        var data={
            page:"finance",
            action:"detail_invoice",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_invoice:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#pembuatan_invoice").html(res);
            dismissLoading();
        })
    };
    
    pembuatan_invoice.bayar_invoice=function(id){
        var data={
            page:"finance",
            action:"bayar_invoice",
            prototype_implement:"",
            prototype_slug:"",
            prototype_name:"",
            id_invoice:id
        }
        showLoading();
        $.post("",data,function(res){
            $("div#pembuatan_invoice").html(res);
            dismissLoading();
        })
    };
    
});