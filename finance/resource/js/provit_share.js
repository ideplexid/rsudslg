function loadFirstPagePV(name){
    $("#"+name+"_header > ul > li.active a ").trigger( "click" );
}

loadFirstPagePV('rekap');


function loadPagePV(file,ref){
    var data={
                page:"finance",
                action:file,
                prototype_implement:"",
                prototype_slug:"",
                prototype_name:""
            };
    showLoading();
    $.post('',data,function(res){
        try {
            var data = JSON.parse(res);
            getContent(res);
            return;
        } catch(e) {
            try{
                $("#"+ref).html(res);
                dismissLoading();
            }catch (e) {
                dismissLoading();
                console.log(e.stack);
                
                var rres=replaceAll("<","&lt;",res);
                rres=replaceAll(">","&gt;",rres);				
                
                var problem="";
                problem+=" <label class='label label-warning'> Javascript  </label> ";
                problem+="<pre>"+e.stack+"</pre>";
                problem+="</br>";
                problem+="</br>";
                problem+=" <label class='label label-info'> Server  </label> ";
                problem+="<pre>"+rres+"</pre>";
                
                showFullWarning("Error Javascript Loading Page",problem,"full_model");
            }finally{
                if($("#smis_window_wrapper").is(":hidden")){
                    $("#smis_window_wrapper").show("fast");
                }				
            }
        }		
    });
}