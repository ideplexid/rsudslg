$(document).ready(function(){
    bulk_umum.detail_bulk  = function(id){
        var data             = this.getRegulerData();
        data['action']       = "bulk_umum_detail";
        data['id_bulk']      = id;
        showLoading();
        $.post("",data,function(res){
            $("#bulk_umum").html(res);
            dismissLoading();
        });
    };

    bulk_umum.cair  = function(id){
        var self             = this;        
        bootbox.confirm("Anda Yakin akan mencairkan Pembayaran Gerombolan ini ? Proses ini tidak dapat dikembalikan",function(r){
            if(r){
                var data             = self.getRegulerData();
                data['command']      = "save";
                data['status']       = "1";
                data['id']           = id;
                showLoading();
                $.post("",data,function(res){
                    self.view();
                    dismissLoading();
                });
            }
        });
    };

    bulk_umum.postAction = function(id){
        bulk_umum.detail_bulk(id);
    };

    bulk_umum.setDisabledOnEdit(new Array("nama_vendor","asal_umum","nama_dompet","chooser_nama_vendor","chooser_nama_dompet"));

    dompet_bulk_umum = new TableAction("dompet_bulk_umum","finance","bulk_umum",new Array());
    dompet_bulk_umum.setSuperCommand("dompet_bulk_umum");
    dompet_bulk_umum.selected=function(json){
        bulk_umum.set("id_dompet",json.id);
        bulk_umum.set("nama_dompet",json.nama);
        bulk_umum.set("no_akun",json.nomor_akun);
    };
});