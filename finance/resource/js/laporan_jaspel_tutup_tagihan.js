var laporan_jaspel_tutup_tagihan;
var dokter_tutup_tagihan;
$(document).ready(function(){
    $(".mydate").datepicker();
    laporan_jaspel_tutup_tagihan = new TableAction("laporan_jaspel_tutup_tagihan","finance","laporan_jaspel_tutup_tagihan",new Array());
    laporan_jaspel_tutup_tagihan.addRegulerData = function(x){
        x['dari'] = laporan_jaspel_tutup_tagihan.get('dari');
        x['sampai'] = laporan_jaspel_tutup_tagihan.get('sampai');
        x['ruangan'] = laporan_jaspel_tutup_tagihan.get('ruangan');
        x['carabayar'] = laporan_jaspel_tutup_tagihan.get('carabayar');
        x['dokter'] = laporan_jaspel_tutup_tagihan.get('dokter');
        x['id_dokter'] = laporan_jaspel_tutup_tagihan.get('id_dokter');
        x['tindakan'] = laporan_jaspel_tutup_tagihan.get('tindakan');
        return x;
    }
    dokter_tutup_tagihan = new TableAction("dokter_tutup_tagihan","finance","laporan_jaspel_tutup_tagihan", new Array());
    dokter_tutup_tagihan.setSuperCommand("dokter_tutup_tagihan");
    dokter_tutup_tagihan.selected = function(json){
        laporan_jaspel_tutup_tagihan.set("id_dokter",json.id);
        laporan_jaspel_tutup_tagihan.set("dokter",json.nama);
    };
    
});