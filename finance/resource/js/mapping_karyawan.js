var mapping_karyawan;
var mapping_karyawan_debet;
var mapping_karyawan_kredit;
var IS_mapping_karyawan_RUNNING;
$(document).ready(function(){
    var column=new Array("id","nama_employee","id_employee","debet","kredit");
    mapping_karyawan=new TableAction("mapping_karyawan","finance","mapping_karyawan",column);
    mapping_karyawan.batal=function(){
        smis_loader.cancel();
    };

    mapping_karyawan.rekaptotal=function(){
        if(IS_mapping_karyawan_RUNNING) return;
        smis_loader.onCancel=function(){
            IS_mapping_karyawan_RUNNING=false;
        };
        
        smis_loader.showLoader();
        smis_loader.updateLoader('true',"Fetching total data",0);
        
        IS_mapping_karyawan_RUNNING=true;
        var d=this.getRegulerData();
        d['super_command']="total";
        $.post("",d,function(res){
            var json=getContent(res);
            if(json>0) {
                mapping_karyawan.rekaploop(0,json);
            } else {
                smis_loader.cancel();
            }
        });
    };

    mapping_karyawan.rekaploop=function(current,total){
        if(current>=total || !IS_mapping_karyawan_RUNNING) {
            smis_loader.cancel();
            IS_mapping_karyawan_RUNNING=false;
            mapping_karyawan.view();
            return;
        }
        smis_loader.updateLoader('true',"Processing... [ "+current+" / "+total+" ] ",(current*100/total));
        var d=this.getRegulerData();
        d['super_command']="limit";
        d['current']=current;			
        $.post("",d,function(res){
            var ct=getContent(res);
            setTimeout(function(){mapping_karyawan.rekaploop(++current,total)},100);
        });
    };
    
    mapping_karyawan.view();
    
    mapping_karyawan_debet=new TableAction("mapping_karyawan_debet","finance","kode_akun",new Array());
    mapping_karyawan_debet.setSuperCommand("mapping_karyawan_debet");
    mapping_karyawan_debet.selected=function(json){
        $("#mapping_karyawan_debet").val(json.nomor);
    };
    mapping_karyawan_debet.setOwnChooserData(true);
    mapping_karyawan_debet.setOwnChooserData(true);
    mapping_karyawan_debet.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
    mapping_karyawan_kredit=new TableAction("mapping_karyawan_kredit","finance","kode_akun",new Array());
    mapping_karyawan_kredit.setSuperCommand("mapping_karyawan_kredit");
    mapping_karyawan_kredit.selected=function(json){
        $("#mapping_karyawan_kredit").val(json.nomor);
    };
    mapping_karyawan_kredit.setOwnChooserData(true);
    mapping_karyawan_kredit.setOwnChooserData(true);
    mapping_karyawan_kredit.addChooserData=function(data){
        data['action']='kode_akun';
        return;
    }
            
});