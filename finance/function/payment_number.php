<?php

function payment_number($akun){
    $akun = strtolower($akun);
    if(strpos($akun,"bank")!==false){
        return payment_number_bank();
    }else{
        return payment_number_kas();
    }
}

function payment_number_kas(){
    loadLibrary("smis-libs-function-math");
    global $db;
    $month   = date("m");
    $year    = substr(date("Y"),2);
    $set     = "finance-pynum-kas-".$month."-".$year;
    $number  = getSettings($db,$set,0);
    $number += 1;
    setSettings($db,$set,$number);
    return $number."/KK/".romanNumerals($month)."/".$year;
}

function payment_number_bank(){
    loadLibrary("smis-libs-function-math");
    global $db;
    $month   = date("m");
    $year    = substr(date("Y"),2);
    $set     = "finance-pynum-bank-".$month."-".$year;
    $number  = getSettings($db,$set,0);
    $number += 1;
    setSettings($db,$set,$number);
    return $number."/KB/".romanNumerals($month)."/".$year;
}

?>