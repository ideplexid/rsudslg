<?php 
    require_once "smis-base/smis-include-service-consumer.php";
    $serv    =  new ServiceConsumer($db,"last_saldo",null,"accounting");
    $serv    ->addData("akun",$_POST['akun']);
    $serv    ->execute();
    $content = $serv->getContent();
    $package = new ResponsePackage();
    $package ->setStatus(ResponsePackage::$STATUS_OK);
    $package ->setContent($content);
    echo json_encode($package->getPackage());
?>