<?php 

function get_karyawan($action,$jenis){
	$head = array('Nama','Jabatan',"NIP");
	$dkadapter = new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	
	$dktable = new Table($head);
	$dktable->setName($action."_karyawan");
	$dktable->setModel(Table::$SELECT);	
	$karyawan = new EmployeeResponder($db, $dktable, $dkadapter, $jenis);
	
	$super = new SuperCommand();
	$super->addResponder($action."_karyawan", $karyawan);
	$init = $super->initialize();
	return $init;
}

?>
