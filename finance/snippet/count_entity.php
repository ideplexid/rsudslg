<?php 

function count_entity(Database $db,string $service,string $truncate=NULL){
	if($truncate!=NULL){
		$dbtable=new DBTable($db,$truncate);
		$dbtable->truncate();
	}
	require_once 'smis-libs-class/ServiceProviderList.php';
	$serv=new ServiceProviderList($db,$service);
	$serv->execute();	
	$entity=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($entity);
	return json_encode($res->getPackage());
}


?>