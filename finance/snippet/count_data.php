<?php 


function count_data(Database $db,string $service,Array $data,string $entity,string $truncate=NULL){
	if($truncate!=NULL){
		$dbtable=new DBTable($db,$truncate);
		$dbtable->truncate();
	}
	require_once 'smis-base/smis-include-service-consumer.php';
	$serv=new ServiceConsumer($db,$service,$data,$entity);
	$serv->execute();
	$entity=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($entity);
	return json_encode($res->getPackage());
}
?>