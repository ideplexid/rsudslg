<?php 

class RecapitulationResepAdapter extends SimpleAdapter{
	
	private $total_harga=0;
	private $total_rs=0;
	private $total_dokter=0;
	
	public function getContent($data){
		$content=array();
		foreach($data as $d){
			$adapt=$this->adapt($d);
			$content[]=$adapt;
		}
		
		$a=array();
		$a['Nama']="<strong>Total</strong>";
		$a['Total']="<strong>".self::format("money Rp.", $this->total_harga)."</strong>";
		$a['Bagian Dokter']="<strong>".self::format("money Rp.", $this->total_dokter)."</strong>";
		$a['Bagian RS']="<strong>".self::format("money Rp.", $this->total_rs)."</strong>";
		$content[]=$a;
		return $content;
	}
	
	public function adapt($d){
		$a=parent::adapt($d);
		$a['Bagian RS']=$d->harga-$d->bagian_dokter;
		$this->total_harga+=$d->harga;
		$this->total_rs+=$a['Bagian RS'];
		$this->total_dokter+=$d->bagian_dokter;
		$a['Bagian RS']=self::format("money Rp.", $a['Bagian RS']);
		return $a;
	}
	
}

?>