<?php 

require_once "finance/class/MapRuangan.php";
class ProvitShareAdapter extends SimpleAdapter{
	
	private $total_diterima;
	private $total_biaya_awal;
	private $total_potongan;
	private $total_pajak;
	private $total_pengembangan;	
    private $pajak;
    private $excel;

	public function __construct($pajak){
		parent::__construct();
		$this->pajak=$pajak;
    }
    

    public function setExcel($ex){
        $this->excel=$ex;
        return $this;
    }
	
	public function adapt($d){
        $a=parent::adapt($d);
        $a['Ruangan'] = MapRuangan::getRealName($d['ruangan']);
		$pajak=$d['asli']*$this->pajak/100;
		$dana_pengembangan=$d['asli']-$d['nilai']-$pajak;
		$potongan=$d['asli']-$d['nilai'];
		$this->total_diterima+=$d['nilai'];
		$this->total_biaya_awal+=$d['asli'];
		$this->total_pajak+=$pajak;
		$this->total_pengembangan+=$dana_pengembangan;
        $this->total_potongan+=$potongan;
        if($this->excel){
            $a['Biaya Awal']=$d['asli'];
            $a['Bagian Dokter']=$d['nilai'];
            $a['Waktu']=$this->month_to_number($a['Tanggal']);
            $a['Pajak']=$pajak;
            $a['Dana Pengembangan']=$dana_pengembangan;
            $a['Bagian RS']=$potongan;
        }else{
            $a['Waktu']=$this->month_to_number($a['Tanggal']);
            $a['Pajak']=self::format("money Rp.", $pajak);
            $a['Dana Pengembangan']=self::format("money Rp.", $dana_pengembangan);		
            $a['Bagian RS']=self::format("money Rp.", $potongan); 
        }
		
		return $a;
	}
	
	private function month_to_number($date){
        loadLibrary("smis-libs-function-time");
		$tgl=explode(" ", $date);
		$tanggal=$tgl[0];
		$bulan=reverse_month_format($tgl[1]);
		$tahun=$tgl[2];
		return $tahun."-".$bulan."-".$tanggal;
	}
	
	public function getContent($data){
		$thedata=parent::getContent($data);
		$thedata=array_sort($thedata, "Waktu");
        $array=array();
        if($this->excel){
            $array['Persentase']="Total";
            $array['Biaya Awal']= $this->total_biaya_awal;
            $array['Bagian Dokter']= $this->total_diterima;
            $array['Bagian RS']=$this->total_potongan;
            $array['Pajak']=$this->total_pajak;
            $array['Dana Pengembangan']=$this->total_pengembangan;
        }else{
            $array['Persentase']="Total";
            $array['Biaya Awal']=self::format("money Rp.", $this->total_biaya_awal);
            $array['Bagian Dokter']="<font id='bagian_dokter'>".self::format("money Rp.", $this->total_diterima)."</font>";
            $array['Bagian RS']="<font id='bagian_accounting'>".self::format("money Rp.", $this->total_potongan)."</font>";
            $array['Pajak']=self::format("money Rp.", $this->total_pajak);
            $array['Dana Pengembangan']=self::format("money Rp.", $this->total_pengembangan);
        }
		
		$thedata[]=$array;
		return $thedata;
	}
	
	public function getTerimaDokter(){
		return $this->total_diterima;
	}
	
	public function getTerimaRS(){
		return $this->total_potongan;
	}
	
}