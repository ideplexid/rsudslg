<?php 

class LaporanJaspelAdapter extends SimpleAdapter{
    private $first = null;
    private $current_uniq = "";
    private $current_total = 0;
    private $full_total = 0;
    
    private $content = array();
    private $asnumber = false;
    private $anumber = 0;

    public function setAsNumber($enabled){
        $this->asnumber = $enabled;
        return $this;
    }

    public function adapt($d){
        $result = parent::adapt($d);
        $next_uniq = $d->nama_dokter."-".$d->waktu;
        //$result['Total Jaspel Dokter'] = $next_uniq;

        if($this->current_uniq!=$next_uniq){
            $this->anumber++;
            $result['No.'] = $this->anumber.".";
            //$result['Total Jaspel Dokter'] = $next_uniq;
            $this->current_uniq = $next_uniq;
            if($this->first!=null){
               //$result['Total Jaspel Dokter'] = json_encode($this->first);
               if($this->asnumber){
                    $this->first['Total Jaspel Dokter'] = $this->current_total;
               }else{
                    $this->first['Total Jaspel Dokter'] = self::moneyFormat("nonzero-money Rp.",$this->current_total);
               }
                $this->current_total = 0;
            }
            $this->first = &$result;            
        }else{
            $result['Waktu'] = "";
            $result['Dokter'] = "";
        }
        $this->current_total += $d->total;
        $this->content[] = &$result;
        $this->full_total += $d->total;
        return null;
    }

    public function getContent($data){
		foreach($data as $d){
            $this->adapt($d);
        }
        
        if($this->first!=null){
            if($this->asnumber){
                $this->first['Total Jaspel Dokter'] = $this->current_total;
           }else{
                $this->first['Total Jaspel Dokter'] = self::moneyFormat("nonzero-money Rp.",$this->current_total);
           }
        }
		if($this->is_use_remove_zero){
			$this->content=$this->removeZero($this->content);
        }
        if($this->asnumber){
            $this->content[] = array(
                'Tindakan' => "Total Jaspel Tindakan",
                'Total Jaspel Dokter' => $this->full_total
            );
        }else{
            $this->content[] = array(
                'Tindakan' => "Total Jaspel Tindakan",
                'Total Jaspel Dokter' => self::moneyFormat("nonzero-money Rp.",$this->full_total)
            );
        }        
        return $this->content;
    }


}