<?php 

class PerawatAdapter extends ArrayAdapter{
    private $total_biaya;
    
    public function adapt($d){
        $this->number++;
        $x=array();
        $x['id']=$d['id'];
        $x['No.']=$this->number;
        $x['Waktu']=ArrayAdapter::format("date d M Y",$d['waktu']);
        $x['Pasien']=$d['nama_pasien'];
        $x['Nama']=$d['nama_tindakan'];
        $x['Ruangan']=ArrayAdapter::format("unslug",$d['smis_entity']);
        $x['Cara Bayar']=ArrayAdapter::format("unslug",$d['carabayar']);
        $x['NRM']=ArrayAdapter::format("only-digit8",$d['nrm_pasien']);
        $x['Harga']=ArrayAdapter::format("money Rp.",$d['harga_tindakan']);
        $x['Jumlah']=$d['jumlah'];
        $x['Total']=ArrayAdapter::format("money Rp.",$d['harga_tindakan']*$d['jumlah']);
        $this->total_biaya+=$d['harga_tindakan']*$d['jumlah'];
        return $x;
    }
    
    public function getContent($data){
        $result=parent::getContent($data);
        $last=array();
        $last['Waktu']="<strong>Total</strong>";
        $last['Total']=ArrayAdapter::format("money Rp.",$this->total_biaya);
        $result[]=$last;
        return $result;
    }
    
}

?>