<?php 


class RecapitulationProvitShareAdapter extends SimpleAdapter{

	private $total_diterima;
	private $total_biaya_awal;
	private $total_potongan;
	private $total_pajak;
	private $total_pengembangan;
    private $pajak;
    private $excel;
	public function __construct($pajak){
		parent::__construct();
		$this->pajak=$pajak;
    }
    
    public function setExcel($excel){
        $this->excel = $excel;
        return $this;
    }
	
	public function adapt($d){
		$a=array();
		$pajak=$d['asli']*$this->pajak/100;
		$dana_pengembangan=$d['asli']-$d['nilai']-$pajak;
		$potongan=$d['asli']-$d['nilai'];
		$this->total_diterima+=$d['nilai'];
		$this->total_biaya_awal+=$d['asli'];
		$this->total_pajak+=$pajak;
		$this->total_pengembangan+=$dana_pengembangan;
		$this->total_potongan+=$potongan;
		$a['Pajak']=$pajak;
		$a['Bagian RS']=$potongan;
		$a['Dana Pengembangan']=$dana_pengembangan;
		$a['Biaya Awal']=$d['asli'];
		$a['Bagian Dokter']=$d['nilai'];
		$a['Dokter']=$d['karyawan'];
		return $a;
	}
	
	public function group($rows){
		$result=array();
		foreach($rows as $row){
			if($row['Dokter']=="") 
				$row['Dokter']=" ... N/A ... ";
			$key=$row['Dokter'];
			if(!isset($result[$key])){
				$result[$key]=$row;
			}else{
				$rcontent=$result[$key];
				$rcontent['Biaya Awal']+=$row['Biaya Awal'];
				$rcontent['Dana Pengembangan']+=$row['Dana Pengembangan'];
				$rcontent['Bagian RS']+=$row['Bagian RS'];
				$rcontent['Bagian Dokter']+=$row['Bagian Dokter'];
				$rcontent['Pajak']+=$row['Pajak'];
				$result[$key]=$rcontent;
			}
		}
		
		ksort($result);
		$final_result=array();
		foreach($result as $oneres){
            if(!$this->excel){
                $oneres['Biaya Awal']=self::format("money Rp.", $oneres['Biaya Awal']);
                $oneres['Dana Pengembangan']=self::format("money Rp.", $oneres['Dana Pengembangan']);
                $oneres['Bagian RS']=self::format("money Rp.", $oneres['Bagian RS']);
                $oneres['Bagian Dokter']=self::format("money Rp.", $oneres['Bagian Dokter']);
                $oneres['Pajak']=self::format("money Rp.", $oneres['Pajak']);    
            }
			$final_result[]=$oneres;
		}
		return $final_result;		
	}
	
	public function getContent($data){
		$result_data=parent::getContent($data);
        $thedata=$this->group($result_data);
        $array=array();
        if($this->excel){
            $array['Dokter']="Total";
            $array['Biaya Awal']=$this->total_biaya_awal;
            $array['Bagian Dokter']=$this->total_diterima;
            $array['Bagian RS']=$this->total_potongan;
            $array['Pajak']=$this->total_pajak;
            $array['Dana Pengembangan']=$this->total_pengembangan;    
        }else{
            $array['Dokter']="<strong>Total</strong>";
            $array['Biaya Awal']="<strong>".self::format("money Rp.", $this->total_biaya_awal)."</strong>";
            $array['Bagian Dokter']="<strong><font id='bagian_dokter'>".self::format("money Rp.", $this->total_diterima)."</font></strong>";
            $array['Bagian RS']="<strong><font id='bagian_accounting'>".self::format("money Rp.", $this->total_potongan)."</font></strong>";
            $array['Pajak']="<strong>".self::format("money Rp.", $this->total_pajak)."</strong>";
            $array['Dana Pengembangan']="<strong>".self::format("money Rp.", $this->total_pengembangan)."</strong>";    
        }
		$thedata[]=$array;
		return $thedata;
	}
	
	public function getTerimaDokter(){
		return $this->total_diterima;
	}
	
	public function getTerimaRS(){
		return $this->total_potongan;
	}
	
}