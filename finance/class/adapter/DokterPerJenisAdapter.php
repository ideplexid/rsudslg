<?php 

class DokterPerJenisAdapter extends ArrayAdapter{
    private $total_biaya;
    
    public function adapt($d){
        $this->number++;
        $x=array();
        $x['No.']=$this->number.".";
        $x['Nama']=$d['nama_tindakan'];
        $x['Harga']=ArrayAdapter::format("money Rp.",$d['harga']);
        $x['Jumlah']=$d['jumlah'];
        $this->total_biaya+=$d['harga'];
        return $x;
    }
    
    public function group_by($data){
        $content=array();
        $one=array();
        foreach($data as $x){
            if(!isset($content[$x['nama_tindakan']])){
                $content[$x['nama_tindakan']]=array();
                $content[$x['nama_tindakan']]['nama_tindakan']=$x['nama_tindakan'];
                $content[$x['nama_tindakan']]['jumlah']=0;
                $content[$x['nama_tindakan']]['harga']=0;
            }
            $content[$x['nama_tindakan']]['jumlah']+=$x['jumlah'];
            $content[$x['nama_tindakan']]['harga']+=$x['harga'];
        }
        return $content;
    }
    
    public function getContent($data){
        $after=$this->group_by($data);
        $result=parent::getContent($after);
        $last=array();
        $last['Nama']="<strong>Total</strong>";
        $last['Harga']=ArrayAdapter::format("money Rp.",$this->total_biaya);
        $result[]=$last;
        return $result;
    }
    
}

?>