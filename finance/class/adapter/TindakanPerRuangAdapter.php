<?php 
class TindakanPerRuangAdapter extends SummaryAdapter{
    public function adapt($d){
        $a = parent::adapt($d);
        if(isset($d['ruangan'])){
            $a['Ruangan'] = MapRuangan::getRealName(self::format("slug",$d['ruangan']));
        }else{
            $a['Ruangan'] = MapRuangan::getRealName(self::format("slug",$d['smis_entity']));
        }
        return $a;
    }
}