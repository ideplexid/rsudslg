<?php 

class OkAdapter extends ArrayAdapter{
    private $total_biaya;
    
    public function adapt($d){
        $this->number++;
        $harga=$d['harga_operator_satu'];
        $harga+=$d['harga_operator_dua'];
        $harga+=$d['harga_asisten_operator_satu'];
        $harga+=$d['harga_asisten_operator_dua'];
        $harga+=$d['harga_anastesi'];
        $harga+=$d['harga_asisten_anastesi'];
        $harga+=$d['harga_asisten_anastesi_dua'];
        $harga+=$d['harga_team_ok'];
        $harga+=$d['harga_bidan'];
        $harga+=$d['harga_bidan_dua'];
        $harga+=$d['harga_perawat'];
        $harga+=$d['harga_sewa_kamar'];
        $harga+=$d['harga_oomloop_satu'];
        $harga+=$d['harga_oomloop_dua'];
        $harga+=$d['harga_instrument'];
        $harga+=$d['harga_instrument_dua'];
        
        $x=array();
        $x['id']=$d['id'];
        $x['No.']=$this->number;
        $x['Waktu']=ArrayAdapter::format("date d M Y",$d['waktu']);
        $x['Pasien']=$d['nama_pasien'];
        $x['Nama']=$d['nama_tindakan'];
        $x['Ruangan']=ArrayAdapter::format("unslug",$d['smis_entity']);
        $x['Cara Bayar']=ArrayAdapter::format("unslug",$d['carabayar']);
        $x['NRM']=ArrayAdapter::format("only-digit8",$d['nrm_pasien']);        
        $x['Harga']=ArrayAdapter::format("money Rp.",$harga);
        $this->total_biaya+=$harga;
        return $x;
    }
    
    public function getContent($data){
        $result=parent::getContent($data);
        $last=array();
        $last['Waktu']="<strong>Total</strong>";
        $last['Harga']=ArrayAdapter::format("money Rp.",$this->total_biaya);
        $result[]=$last;
        return $result;
    }
    
}

?>