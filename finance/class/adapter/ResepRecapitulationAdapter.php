<?php 

class ResepRecapitulationAdapter extends SimpleAdapter{
	
	private $total_tagihan;
	private $total_bagian_karyawan;//bagian_rs
	private $total_bagian_rs;
	
	
	public function __construct(){
		parent::__construct();
		$this->content=array();
	}
	
	public function adapt($d){
		$a=parent::adapt($d);
		$this->total_tagihan+=$d['total'];
		$this->total_bagian_karyawan+=$d['bagian_dokter'];
		$this->total_bagian_rs+=$d['bagian_rs'];
		return $a;
	}
	
	public function getContent($data){
		$ldata=$this->joinData($data);
		$thedata=parent::getContent($ldata);
		$array=array();
		$array['Nama']="<strong>Total</strong>";
		$array['Total']=self::format("money Rp.", $this->total_tagihan);
		$array['Bagian Dokter']="<font id='bagian_dokter'>".self::format("money Rp.", $this->total_bagian_karyawan)."</font>";
		$array['Bagian RS']="<font id='bagian_accounting'>".self::format("money Rp.", $this->total_bagian_rs)."</font>";
		$thedata[]=$array;
		return $thedata;
	}
	
	public function getTerimaDokter(){
		return $this->total_bagian_karyawan;
	}
	
	public function getTerimaRS(){
		return $this->total_bagian_rs;
	}
	
	public function joinData($data){
		$result=array();
		foreach($data as $d){
			$id_dokter=$d['id_karyawan'];
			if(isset($result[$id_dokter])){
				$result[$id_dokter]['total']+=$d['total'];
				$result[$id_dokter]['bagian_dokter']+=$d['bagian_dokter'];
				$result[$id_dokter]['bagian_rs']+=$d['bagian_rs'];
			}else{
				$result[$id_dokter]=$d;
			}
		}
		return $result;
	}
}

?>