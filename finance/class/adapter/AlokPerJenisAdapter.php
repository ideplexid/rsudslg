<?php 

class AlokPerJenisAdapter extends ArrayAdapter{
    private $total_biaya;
    
    public function adapt($d){
        $this->number++;
        $x=array();
        $x['No.']=$this->number.".";
        $x['Nama']=$d['nama'];
        $x['Harga']=ArrayAdapter::format("money Rp.",$d['harga']);
        $x['Jumlah']=$d['jumlah'];
        $this->total_biaya+=$d['harga'];
        return $x;
    }
    
    public function group_by($data){
        $content=array();
        $one=array();
        foreach($data as $x){
            if(!isset($content[$x['nama']])){
                $content[$x['nama']]=array();
                $content[$x['nama']]['nama']=$x['nama'];
                $content[$x['nama']]['jumlah']=0;
                $content[$x['nama']]['harga']=0;
            }
            $content[$x['nama']]['jumlah']+=$x['jumlah'];
            $content[$x['nama']]['harga']+=$x['harga'];
        }
        return $content;
    }
    
    public function getContent($data){
        $after=$this->group_by($data);
        $result=parent::getContent($after);
        $last=array();
        $last['Nama']="<strong>Total</strong>";
        $last['Harga']=ArrayAdapter::format("money Rp.",$this->total_biaya);
        $result[]=$last;
        return $result;
    }
    
}

?>