<?php 

class PendapatanKamarMayatAdapter extends SummaryAdapter{
    public function adapt($d){
        $a = parent::adapt($d);
        $a['Ruang'] = MapRuangan::getRealName(self::format("slug",$d['ruangan']));
        return $a;
    }
}