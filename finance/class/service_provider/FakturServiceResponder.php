<?php
require_once 'smis-base/smis-include-service-consumer.php';

/**
 * 
 * this class is called in finance/faktur.php
 * use to get Faktur from service then 
 * took it show Payment and Discount
 * this file was dependent to : 
 * - finance/class/template/DiskonPayment.php
 * - finance/class/template/FakturPayment.php
 * both of that file use to find the discount 
 * and payment history
 * 
 * @author goblooge
 * @since 29 Oct 2014
 * @version 1.0.0
 * @license LGPL
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @link http://www.goblooge.com
 */
class FakturServiceResponder extends ServiceResponder {
	private $ruang;
	public function __construct($db,$table,$adapter,$ruangan) {
		parent::__construct($db,$table,$adapter,"get_faktur",$ruangan);
		$this->ruang=$ruangan;
	}
	
	public function command($command){
		if($command=="sisa"){
			/*for counting total of discount and payment */
			$pack=new ResponsePackage();
			$content=$this->sisa($_POST['id'],$_POST['ruangan']);
			$pack->setContent($content);
			$pack->setStatus(ResponsePackage::$STATUS_OK);
			$pack->setAlertVisible(false);
			return $pack->getPackage();
		}else{
			return parent::command($command);	
		}
	}
	
	/**
	 * count the value that left from this factur
	 * after subsract with payment and discount
	 * @param string $id
	 * @return object of payment and discount
	 */
	public function sisa($id,$ruang){
		$qsum="SELECT 
				SUM(if(jenis='diskon_faktur',nilai,0)) as  diskon,
				SUM(if(jenis='pembayaran_faktur' AND status=1,nilai,0)) as  bayar
				FROM smis_fnc_bayar 
				WHERE id_faktur='".$id."' AND asal_faktur='".$ruang."' AND prop!='del'";
		$row=$this->getDB()->get_row($qsum,true);
		return $row;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ServiceResponder::edit()
	 */
	public function edit() {
		$d = parent::edit ();
		$a = $this->getHtml($d);
		return $a;
	}
	
	/**
	 * this function will parse the data that got from service
	 * then parse it into HTML,and repackage it for viewing to 
	 * user. this funtion was dependent to : 
 	 * - finance/class/template/DiskonPayment.php
	 * - finance/class/template/FakturPayment.php
	 * @param data got from service $d
	 * @return multitype:unknown string
	 */
	public function getHtml($d) {
		$tabulator = new Tabulator("","",Tabulator::$POTRAIT);
		$tpfaktur = new TablePrint("dfaktur");
		$tpfaktur->setMaxWidth(false);
		$tpfaktur->setDefaultBootrapClass(true);
		$tpfaktur->addColumn("<strong>INFORMASI FAKTUR</strong>",2,1)->commit("title","success");
		$po = $d ['po'];
		$tgl_jt=isset($po ['tanggal_jt']) && $po ['tanggal_jt']!="" && $po ['tanggal_jt']!="0000-00-00"?ArrayAdapter::format("date d M Y",$po ['tanggal_jt']):"-";
		$tgl_po=isset($po ['tanggal']) && $po ['tanggal']!="" && $po ['tanggal']!="0000-00-00"?ArrayAdapter::format("date d M Y",$po ['tanggal']):"-";
		$tpfaktur->addColumn("<strong>DETAIL PURCHASE ORDER (PO)</strong>",2,1)->commit("body","info");
		$tpfaktur->addColumn("Vendor",1,1)->addColumn($po ['nama_vendor'],1,1,"body")->addColumn("Tangal",1,1)->addColumn($tgl_po,1,1,"body")->addColumn("Tanggal Jatuh Tempo",1,1)->addColumn($tgl_jt,1,1,"body");
		$tpfaktur->addColumn("<strong>Keterangan</strong>",2,1,"body")->addColumn($po ['keterangan'],2,1,"body");
		$tpfaktur->addColumn("<strong>DETAIL FAKTUR</strong>",2,1)->commit("body","info");
		$tpfaktur->addColumn("No Faktur",1,1)->addColumn($d ['no_faktur'],1,1,"body")->addColumn("Tgl Faktur",1,1)->addColumn(ArrayAdapter::format("date d M Y",$d ['tanggal']),1,1,"body");
		$tpfaktur->addColumn("Tanggal Datang",1,1)->addColumn(ArrayAdapter::format("date d M Y",$d ['tanggal_datang']),1,1,"body")->addColumn("Jatuh Tempo",1,1)->addColumn(ArrayAdapter::format("date d M Y",$d ['tanggal_tempo']),1,1,"body");
		$tpfaktur->addColumn("<strong>Keterangan</strong>",2,1,"body")->addColumn($d ['keterangan'],3,1,"body");
		$tabulator->add("informasi","Informasi Faktur",$tpfaktur->getHtml (),Tabulator::$TYPE_HTML," fa fa-credit-card");
		
		$dtpfaktur = new TablePrint("dlistfaktur");
		$dtpfaktur->setMaxWidth(false);
		$dtpfaktur->setDefaultBootrapClass(true);
		$dtpfaktur	->addColumn("Barang",1,1)
					->addColumn("Jenis",1,1)
					->addColumn("Jumlah",1,1)
					->addColumn("Subtotal",1,1)
					->addColumn("Diskon",1,1)
					->addColumn("Nilai Diskon",1,1)
					->addColumn("Total",1,1)
					->commit("title","warning");
		$dtl = $d ['detail'];
		foreach($dtl as $c) {
			$dtpfaktur	->addColumn($c ['nama_barang'],1,1)
						->addColumn($c ['nama_jenis_barang'],1,1)
						->addColumn($c ['jumlah'] . " " . $c ['satuan'],1,1)
						->addColumn(ArrayAdapter::format("money Rp.",$c ['harga_sebelum_diskon']),1,1)
						->addColumn($c ['val_diskon'] ,1,1)
						->addColumn(ArrayAdapter::format("money Rp.",$c ['n_diskon']) ,1,1)
						->addColumn(ArrayAdapter::format("money Rp.",$c ['harga_setelah_diskon']),1,1)
						->commit("body");
		}
		$dtpfaktur	->addColumn("Sub Total Biaya",6,1)
					->addColumn(ArrayAdapter::format("money Rp.",$d ['harga_sebelum_diskon_global']),1,1)
					->commit("footer");
		$dtpfaktur	->addColumn("Diskon Global",6,1)
					->addColumn( $d ['diskon_global'] ,1,1)
					->commit("footer");
		
        /*kode untuk menambahkan biaya lain-lain seperti materai dan lain-lain*/
        if(isset($d['biaya_lain'])){
            $list_x=array($d['biaya_lain']);
            $total=0;
            $list=array();
            foreach($list_x as $keterangan=>$value){
                foreach($value as $name=>$v){
                    $total = $total + $v;
                    $list[ucwords($name)]=$v;
                }
            }
            $dtpfaktur	->addColumn("<strong>Sub Total</strong>",6,1)
                        ->addColumn(ArrayAdapter::format("money Rp.",$d ['total_bayar']-$total),1,1)
                        ->commit("footer");
		    foreach($list as $keterangan=>$value){
                $dtpfaktur	->addColumn("<strong>".$keterangan."</strong>",6,1)
                            ->addColumn(ArrayAdapter::format("money Rp.",$value),1,1)
                            ->commit("footer");
            }
        }
        
		$dtpfaktur	->addColumn("<strong>Total Dibayar</strong>",6,1)
					->addColumn(ArrayAdapter::format("money Rp.",$d ['total_bayar']),1,1)
					->commit("footer","info");
		
		$tabulator->add("detail","Detail Faktur",$dtpfaktur->getHtml (),Tabulator::$TYPE_HTML," fa fa-list-alt");
		
        /*
            require_once 'finance/class/template/DiskonPayment.php';
            $dk=new DiskonPayment($this->getDB (),$d ['id'],$this->ruang,$d ['total_bayar'],$d ['no_faktur'],$d);
            ob_start ();
            $dk->initialize ();
            $res = ob_get_clean ();
            $tabulator->add("diskon","Diskon",$res,Tabulator::$TYPE_HTML);
		*/
		
        require_once 'finance/class/template/FakturPayment.php';
		$fp = new FakturPayment($this->getDB (),$d ['id'],$this->ruang,$d ['tanggal_tempo'],$d ['no_faktur'],$d);
		ob_start ();
		$fp->initialize ();
		$res = ob_get_clean ();
		$tabulator->add("termin_faktur","Termin Pembayaran",$res,Tabulator::$TYPE_HTML," fa fa-tag");
		
		$con=array();
		$con['chtml']=$tabulator->getHtml ();
		$con['idf']=$d ['id'];
		$con['hbayar']=$d['total_bayar'];
		$con['nofaktur']=$d['no_faktur'];
		
		
		return $con;
	}
}

?>