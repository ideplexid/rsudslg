<?php 
require_once 'smis-framework/smis/service/ServiceConsumer.php';

/**
 * this class is used for getting the Salary of Emplpoyee
 * then sort it based on it's type, it's place and date
 * 
 * @author goblooge
 * @since 27 Oct 2014
 * @version 1.0.0
 * @license LGPL v 3
 * @copyright Nurul Huda <goblooge@gmail.com>
 */

class SalaryServiceConsumer extends ServiceConsumer{
	private $db;
	public function __construct($db,$from,$to,$id_karyawan){
		$this->db=$db;
		$data ["id_karyawan"] = $id_karyawan;
		$data ["from"] = $from;
		$data ["to"] = $to;		
		parent::__construct($db, "get_salary",$data);
		$this->setMode(ServiceConsumer::$CLEAN_BOTH);
	}
	
	public function getContent(){
		$result=array();
		$content=parent::getContent();
		foreach($content as $data){
			$tipe=$data["sebagai"];
			if(!isset($result[$tipe])) 
				$result[$tipe]=array();
			$result[$tipe][]=$data;
		}
		return $result;
	}	
}

?>