<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
require_once 'finance/class/table/FakturUITable.php';
/**
 * this class use for managing payment of faktur
 * including partial payment and print of faktur
 * this one use finance/faktur/FakturUITable.php
 * as viewing basic of Factur Printing.
 * 
 * @uses finance/class/service_provider/FakturServiceResponder.php
 * @author goblooge
 * @since 29 Oct 2014
 * @version 1.0.0
 * @license LGPL
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @link http://www.goblooge.com
 *
 */
class FakturPayment extends ModulTemplate {
	private $uitable;
	private $adapter;
	private $jt_faktur;
	private $idfaktur;
	private $nofaktur;
	private $db;
	private $json;
	private $html;
	private $ruangan;
	public function __construct($db, $id_faktur,$ruangan, $jt_faktur, $nofaktur, $json = array(), $html = ""){
		$this->idfaktur = $id_faktur;
		$this->jt_faktur = $jt_faktur == "0000-00-00" ? date('Y-m-d', strtotime("+1 months", time())): $jt_faktur;
		$this->nofaktur = $nofaktur;
		$this->ruangan=$ruangan;
		$this->db = $db;
		$this->json = $json;
		$this->html = $html;
		$header = array("Tgl Dibuat","Jatuh Tempo","Tgl Cair","No Faktur","Nilai","Keterangan","Cara Bayar","Penerima","Cair","Kas & Bank");
		$this->uitable = new FakturUITable($header, "", NULL, true);
		$this->uitable->setName("fp");
		$this->uitable->setPrintElementButtonEnable(true);
		$this->uitable->setPrintButtonEnable(false);
	}
	public function command($command){
        require_once "finance/class/responder/FakturPaymentResponder.php";
		$adapter = new SimpleAdapter();
		$adapter->add("Tgl Dibuat", "waktu", "date d M Y");
		$adapter->add("Jatuh Tempo", "jt_bayar", "date d M Y");
		$adapter->add("Tgl Cair", "tgl_cair", "date d M Y");
		$adapter->add("No Faktur", "noref");
		$adapter->add("Nilai", "nilai", "money Rp.");
		$adapter->add("Keterangan", "keterangan");
		$adapter->add("Cara Bayar", "code");
		$adapter->add("Penerima", "penerima");
        $adapter->add("Kas & Bank", "nama_dompet");
		$adapter->add("Cair", "status", "trivial_0__<font class='label label-info'><i class='icon-white icon-ok'></i></font>");
		$dbtable = new DBTable($this->db, "smis_fnc_bayar");
		$dbtable->addCustomKriteria("id_faktur", "='" . $this->idfaktur . "'");
		$dbtable->addCustomKriteria("asal_faktur", "='".$this->ruangan."'");
		$dbtable->addCustomKriteria("jenis", "='pembayaran_faktur'");
		$responder = new FakturPaymentResponder($dbtable, $this->uitable, $adapter);
        if($responder->isSave()&& $_POST['status']=="1"){
            $responder->addColumnFixValue("tgl_cair",date("Y-m-d"));
        }
		$d = $responder->command($command);
		echo json_encode($d);
	}
	public function initialize(){
		if(isset($_POST ['super_command'])&& $_POST ['super_command'] == "fp" && isset($_POST ['command'])){
			$this->command($_POST ['command']);
		} else {
			$this->phpPreLoad();
			$this->jsLoader();
			$this->cssLoader();
			$this->jsPreLoad();
			$this->cssPreLoad();
			$this->htmlPreLoad();
		}
	}
	public function phpPreLoad(){
		$carabayar = new OptionBuilder();
		$carabayar	->addSingle("Tunai")
					->addSingle("Giro")
					->addSingle("Cek")
                    ->addSingle("Transfer Bank");
		
		$this->uitable->addModal("id", "hidden", "", "");
		$this->uitable->addModal("waktu", "date", "Tanggal Dibuat", date("Y-m-d"));
		$this->uitable->addModal("jt_bayar", "date", "Jatuh Tempo", $this->jt_faktur);
		$this->uitable->addModal("noref", "hidden", "", $this->nofaktur);
		$this->uitable->addModal("id_faktur", "hidden", "", $this->idfaktur);
		$this->uitable->addModal("nilai", "money", "Nilai", "","n");
		$this->uitable->addModal("asal_faktur", "hidden", "", $this->ruangan);
		$this->uitable->addModal("json", "hidden", "", json_encode($this->json));
		$this->uitable->addModal("code", "select", "Cara Bayar", $carabayar->getContent());
		$this->uitable->addModal("penerima", "text", "Penerima", "");
		$this->uitable->addModal("keterangan", "textarea", "Keterangan", "");
		$this->uitable->addModal("nama_dompet","chooser-fp-faktur_dompet-Kas & Bank","Kas & Bank","");
        $this->uitable->addModal("id_dompet","hidden","","");
        $this->uitable->addModal("id_vendor","hidden","",$this->json['id_vendor']);
        $this->uitable->addModal("nama_vendor","hidden","",$this->json['nama_vendor']);
        $this->uitable->addModal("no_akun","hidden","","");
    
        $idfaktur   = new Hidden("h_id_faktur","",$this->idfaktur);
        $jtfaktur   = new Hidden("h_jt_faktur","",$this->jt_faktur);
        $nofaktur   = new Hidden("h_no_faktur","",$this->nofaktur);
        $asalfktur  = new Hidden("h_asal_ruang_faktur","",$this->ruangan);
        
        echo $idfaktur->getHtml();
        echo $jtfaktur->getHtml();
        echo $nofaktur->getHtml();
        echo $asalfktur->getHtml();
        
		echo $this->uitable->getHtml();
		echo $this->uitable->getModal()
							->setTitle("Kwitansi Faktur")
							->setComponentSize(Modal::$MEDIUM)
							->getHtml();
                            
        echo addCSS("framework/bootstrap/css/datepicker.css");
		echo addJS("framework/smis/js/table_action.js");
		echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
		echo addJS("finance/resource/js/faktur_payment.js",false);
		echo addCSS("finance/resource/css/pembayaran_faktur.css",false);   
	}
}
?>