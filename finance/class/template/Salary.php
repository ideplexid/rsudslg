<?php
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'finance/class/template/SalaryServiceConsumer.php';

/**
 * 
 * this Class Used for getting Salary From 
 * all user so it will be good 
 * find the Salary of one single
 * person from all service.
 * the service that consume by this Class is
 * finance/class/template/SalaryServiceConsumer.php
 * 
 * @version 1.0.2
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @license LGPL v 3
 * @author goblooge
 * @since 24 Oct 2007
 * @link http://www.goblooge.com
 */
class Salary {
	private $db;
	private $id;
	private $from;
	private $to;
	private $nama;
	private $jabatan;
	private $pajak;
	
	public function __construct($db,$id,$from,$to,$nama,$jabatan,$pajak) {
		$this->db=$db;
		$this->id=$id;
		$this->from=$from;
		$this->to=$to;
		$this->nama=$nama;
		$this->jabatan=$jabatan;
		$this->pajak=$pajak;
	}
	
	/**
	 * system will create unique system number based on
	 * 1. current employee that will get the salary
	 * 2. time
	 * 3. current user login
	 * 4. random number generator
	 * 5. date from
	 * 6. date to
	 * @return string generated random Number
	 */
	private function createUniqueNumber(){
		global $user;
		loadLibrary("smis-libs-function-string");
		$name=$user->getNameOnly();
		$id_karyawan=ArrayAdapter::format("only-digit6", $this->id);
		$from=str_replace("-", "/", substr($this->from, 5));
		$to=str_replace("-", "/", substr($this->to, 5));
		$gen=strtoupper(generateRandomString(rand(1, 5)));
		$gen2=strtoupper(substr(md5($name.date("dmyhisu")),5,5));
		$num="SLR-".$id_karyawan."-".$from."-".$to."-".$gen2."/".$gen;
		return $num;
	}
	
	/**
	 * calling tthe service of SalaryServiceConsumer
	 * @return Object of Salary that got from Service
	 */
	public function getSalary() {
		$service=new SalaryServiceConsumer($this->db,$this->from,$this->to,$this->id);
		$service->execute ();
		$content=$service->getContent ();
		return $content;
	}
	
	/**
	 * afte gor data from the whole service in system
	 * this function will parse the data into HTML for wieing to user
	 * this function user getSalary() to get the Data from service
	 * and also create Random Number as 
	 * System ID from from function createUniqueNumber()
	 * @return multitype:string number Ambigous <number, multitype:, unknown>
	 */
	public function getHtml() {
		$datalist=$this->getSalary ();
		$random=$this->createUniqueNumber();
		$tp=new TablePrint("salary_dokter");
		$tp	->addTableClass("table table-bordered table-hover table-condensed ")
			->setMaxWidth(false);
		
		$tp ->addColumn("Kode Transaksi",2,1)
			->addColumn($random, 7, 1)
			->commit("title","error");
		
		$tp	->addColumn("Nama",2,1)
			->addColumn($this->nama,3,1)
			->addColumn("Jabatan",2,1)
			->addColumn($this->jabatan,2,1)
			->commit("title");
		
		$tp	->addColumn("Dari",2,1)
			->addColumn(ArrayAdapter::format("date d M Y",$this->from),3,1)
			->addColumn("Sampai",2,1)
			->addColumn(ArrayAdapter::format("date d M Y",$this->to),3,1)
			->commit("title");
		
		$tp	->addColumn("Waktu",1,1)
			->addColumn("Pasien",1,1)
			->addColumn("Ruang",1,1)
			->addColumn("Persen",1,1)
			->addColumn("Bruto",1,1)
			->addColumn("Netto",1,1)
			->addColumn("Dana Pengembangan",1,1)
			->addColumn("Pajak (".$this->pajak."%)",1,1)
			->addColumn("Keterangan",1,1)
			->commit("title","success");
		
		$total_bruto=0;
		$total_neto=0;
		$total_danap= 0;
		$total_pajak=0;
		foreach($datalist as $layanan=>$clayanan) {
			$tp->addColumn("<strong>".ArrayAdapter::format("unslug",$layanan)."</strong>",10,1)->commit("body","warning");
			foreach($clayanan as $data){
				$dana_pengembangan=0;
				$pajak=0;
				if($data['percentage']<100){
					$dana_pengembangan=((100-($data['percentage']+$this->pajak))*$data['asli']/100);
					$pajak=($this->pajak)*$data['asli']/100;
				}
				$tp	->addColumn($data['waktu'],1,1)
					->addColumn($data['pasien'],1,1)
					->addColumn(ArrayAdapter::format("unslug",$data['ruangan']),1,1)
					->addColumn($data['percentage'],1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$data['asli']),1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$data['nilai']),1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$dana_pengembangan),1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$pajak),1,1)
					->addColumn($data['keterangan'],1,1)
					->commit("body");
				$total_bruto+=$data['asli'];
				$total_neto+=$data['nilai'];
				$total_danap+=$dana_pengembangan;
				$total_pajak+=$pajak;
			}
		}
		
		$tp	->addColumn("Total",4,1)
			->addColumn(ArrayAdapter::format("money Rp.",$total_bruto),1,1)
			->addColumn("<i><u>".ArrayAdapter::format("money Rp.",$total_neto)."</u></i>",1,1)
			->addColumn(ArrayAdapter::format("money Rp.",$total_danap),1,1)
			->addColumn(ArrayAdapter::format("money Rp.",$total_pajak),1,1)
			->addColumn("",1,1)
			->commit("footer");	

		loadLibrary("smis-libs-function-math");
		$tp	->addColumn("Terbilang", 4, 1)
			->addColumn("<i>".numbertell($total_neto)." Rupiah </i>",7,1)
			->commit("footer");
		
		global $user;
		$tp	->addColumn("Tuban, ".date("d M Y H:i"),9,1)
			->commit("footer");
		$tp	->addColumn("Karyawan",5,1)
			->addColumn("Petugas", 4, 1)
			->commit("footer");
		$tp	->addColumn("</br></br></br></br><u>".$this->nama."</u>",5,1)
			->addColumn("</br></br></br></br><u>".$user->getNameOnly()."</u>", 4, 1)
			->commit("footer");
		
		$ft=ArrayAdapter::format("date d M Y", $this->from);
		$to=ArrayAdapter::format("date d M Y", $this->to);
		
		$dresponse=array();
		$dresponse['waktu']=date("Y-m-d");
		$dresponse['jenis']="pembayaran_gaji";
		$dresponse['keterangan']="Pembayaran ".$this->jabatan." ".$this->nama." Tanggal ".$ft." - ".$to;
		$dresponse['html']=$tp->getHtml();
		$dresponse['json']=json_encode($datalist);
		$dresponse['noref']=$random;
		$dresponse['nilai']=$total_neto;
		$dresponse['status']=1;
		return $dresponse;
	}
}

?>