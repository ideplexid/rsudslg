<?php
require_once 'smis-base/smis-include-service-consumer.php';
require_once 'finance/class/template/ProvitShareServiceConsumer.php';

/**
 *
 * this class i used for getting Provit Sharing 
 * of spesific Room
 * 
 * @author goblooge
 * @since 29 Oct 2014
 * @version 1.0.0
 * @license LGPL
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @link http://www.goblooge.com
 */
class ProvitShare {
	private $db;
	private $id;
	private $from;
	private $to;
	private $ruangan;
	private $jenis_ruangan;
	private $pajak;
	
	public function __construct($db,$from,$to,$ruangan,$jenis_ruangan,$pajak) {
		$this->db=$db;
		$this->from=$from;
		$this->to=$to;
		$this->ruangan=$ruangan;
		$this->pajak=$pajak;
		$this->jenis_ruangan=$jenis_ruangan;
	}
	
	/**
	 * get the all content of salary from service
	 * @return content of salary that will send by service
	 */
	public function getSalary() {
		$service=new ProvitShareServiceConsumer($this->db,$this->from,$this->to,$this->ruangan);
		$service->execute ();
		$content=$service->getContent ();
		return $content;
	}
	
	/**
	 * create uique id from room name, current user, 
	 * date from , and date to 
	 * and Random Number from system
	 * @return string
	 */
	private function createUniqueNumber(){
		global $user;
		loadLibrary("smis-libs-function-string");
		$name=$user->getNameOnly();
		$ruang=ArrayAdapter::format("unslug", $this->ruangan);
		$from=str_replace("-", "/", substr($this->from, 5));
		$to=str_replace("-", "/", substr($this->to, 5));
		$gen=strtoupper(generateRandomString(rand(1, 5)));
		$gen2=strtoupper(substr(md5($name.date("dmyhisu")),5,5));
		$num="PVR-".$ruang."-".$from."-".$to."-".$gen2."/".$gen;
		return $num;
	}
	
	/**
	 * convert the salary content to become HTML
	 * that ready for view to user
	 * @return multiple part
	 */
	public function getHtml() {
		$datalist=$this->getSalary ();
		$random=$this->createUniqueNumber();
		$tp=new TablePrint("salary_ruangan");
		$tp	->setDefaultBootrapClass(true)
			->setMaxWidth(false);		
		$tp ->addColumn("Kode Transaksi",2,1)
			->addColumn($random, 7, 1)
			->commit("title","error");
		
		$tp	->addColumn("Ruangan",2,1)
			->addColumn(ArrayAdapter::format("unslug", $this->ruangan),3,1)
			->addColumn("Jenis Ruangan",2,1)
			->addColumn($this->jenis_ruangan,2,1)
			->commit("title");
				
		$tp	->addColumn("Dari",2,1)
			->addColumn(ArrayAdapter::format("date d M Y",$this->from),3,1)
			->addColumn("Sampai",2,1)
			->addColumn(ArrayAdapter::format("date d M Y",$this->to),3,1)
			->commit("title");
		
		$tp	->addColumn("Waktu",1,1)
			->addColumn("Pasien",1,1)
			->addColumn("Petugas",1,1)
			->addColumn("Persen",1,1)
			->addColumn("Bruto",1,1)
			->addColumn("Netto",1,1)
			->addColumn("Dana Pengembangan",1,1)
			->addColumn("Pajak (".$this->pajak."%)",1,1)
			->addColumn("Keterangan",1,1)
			->commit("title","success");
		
		$total_bruto=0;
		$total_neto=0;
		$total_danap= 0;
		$total_pajak=0;
		foreach($datalist as $layanan=>$clayanan) {
			$tp->addColumn("<strong>".ArrayAdapter::format("unslug",$layanan)."</strong>",10,1)->commit("body","warning");
			foreach($clayanan as $data){
				$dana_pengembangan=0;
				$pajak=0;
				if($data['percentage']<100){
					$dana_pengembangan=((100-($data['percentage']+$this->pajak))*$data['asli']/100);
					$pajak=($this->pajak)*$data['asli']/100;
				}
				$tp	->addColumn($data['waktu'],1,1)
					->addColumn($data['pasien'],1,1)
					->addColumn(ArrayAdapter::format("unslug",$data['karyawan']),1,1)
					->addColumn($data['percentage'],1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$data['asli']),1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$data['nilai']),1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$dana_pengembangan),1,1)
					->addColumn(ArrayAdapter::format("money Rp.",$pajak),1,1)
					->addColumn($data['keterangan'],1,1)
					->commit("body");
				$total_bruto+=$data['asli'];
				$total_neto+=$data['nilai'];
				$total_danap+=$dana_pengembangan;
				$total_pajak+=$pajak;
			}
		}
		
		$tp	->addColumn("Total",4,1)
			->addColumn(ArrayAdapter::format("money Rp.",$total_bruto),1,1)
			->addColumn(ArrayAdapter::format("money Rp.",$total_neto),1,1)
			->addColumn(ArrayAdapter::format("money Rp.",$total_danap),1,1)
			->addColumn(ArrayAdapter::format("money Rp.",$total_pajak),1,1)
			->addColumn("",1,1)
			->commit("footer");	
		
		loadLibrary("smis-libs-function-math");
		$tp	->addColumn("Terbilang", 5, 1)
			->addColumn(numbertell($total_neto)." Rupiah ",7,1)
			->commit("footer");
		
		global $user;
		$ruang=ArrayAdapter::format("unslug-ucword", $this->ruangan);
		$tp	->addColumn("Tuban, ".date("d M Y H:i"),9,1)
			->commit("footer");
		$tp	->addColumn("Kepala Ruangan ".$ruang,5,1)
			->addColumn("Petugas", 4, 1)
			->commit("footer");
		$tp	->addColumn("</br></br></br></br>( ____________________ )",5,1)
			->addColumn("</br></br></br></br><u>".$user->getNameOnly()."</u>", 4, 1)
			->commit("footer");
		
		$ft=ArrayAdapter::format("date d M Y", $this->from);
		$to=ArrayAdapter::format("date d M Y", $this->to);
		$dresponse=array();
		$dresponse['waktu']=date("Y-m-d");
		$dresponse['jenis']="pembayaran_provit_share";
		$dresponse['keterangan']="Pembayaran Provit Share pada ".$this->ruangan." Tanggal ".$ft." - ".$to;
		$dresponse['html']=$tp->getHtml();
		$dresponse['json']=json_encode($datalist);
		$dresponse['noref']=$random;
		$dresponse['nilai']=$total_neto;
		$dresponse['status']=1;
		
		return $dresponse;
	}
}

?>