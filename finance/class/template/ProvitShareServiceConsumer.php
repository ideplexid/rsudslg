<?php 
require_once 'smis-framework/smis/service/ServiceConsumer.php';

/**
 * this class is used for getting the Provit Share of Room
 * then sort it based on it's type, it's place and date
 * 
 * @author goblooge
 * @since 27 Oct 2014
 * @version 1.0.0
 * @license LGPL v 3
 * @copyright Nurul Huda <goblooge@gmail.com>
 */

class ProvitShareServiceConsumer extends ServiceConsumer{
	private $db;
	public function __construct($db,$from,$to,$ruangan){
		$this->db=$db;
		$data ["ruangan"] = $ruangan;
		$data ["from"] = $from;
		$data ["to"] = $to;		
		parent::__construct($db, "get_provit_share",$data,$ruangan);
		$this->setMode(ServiceConsumer::$CLEAN_BOTH);
	}
	
	public function getContent(){
		$result=array();
		$content=parent::getContent();
		foreach($content as $data){
			$tipe=$data["sebagai"];
			if(!isset($result[$tipe])) 
				$result[$tipe]=array();
			$result[$tipe][]=$data;
		}
		return $result;
	}	
}

?>