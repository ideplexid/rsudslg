<?php
require_once 'smis-framework/smis/template/ModulTemplate.php';
/**
 * this class use for managing discount of faktur
 * both Percentage and Value of discount
 * 
 * @uses finance/class/service_provider/FakturServiceResponder.php
 * @author goblooge
 * @since 29 Oct 2014
 * @version 1.0.0
 * @license LGPL
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @link http://www.goblooge.com
 *
 */
class DiskonPayment extends ModulTemplate {
	private $uitable;
	private $adapter;
	private $total;
	private $idfaktur;
	private $nofaktur;
	private $db;
	private $json;
	private $html;
	private $ruangan;
	public function __construct($db, $id_faktur,$ruangan, $total, $nofaktur, $json = "{}", $html = "") {
		$this->idfaktur = $id_faktur;
		$this->total= $total;
		$this->nofaktur = $nofaktur;
		$this->db = $db;
		$this->json = json_encode ( $json );
		$this->html = $html;
		$this->ruangan=$ruangan;
		$header = array ("Tanggal","Nilai","Persentase","Keterangan");
		$this->uitable = new Table( $header, "", NULL, true );
		$this->uitable->setName ( "faktur_dk" );
		$this->uitable->setPrintElementButtonEnable ( true );
		$this->uitable->setPrintButtonEnable ( false );
	}
	public function command($command) {
		$adapter = new SimpleAdapter ();
		$adapter->add ( "Tanggal", "waktu", "date d M Y" );
		$adapter->add ( "Nilai", "nilai", "money Rp." );
		$adapter->add ( "Persentase", "persen","back%" );
		$adapter->add ( "Keterangan", "keterangan" );
		$dbtable = new DBTable ( $this->db, "smis_fnc_bayar" );
		$dbtable->addCustomKriteria ( "id_faktur", "='" . $this->idfaktur . "'" );
		$dbtable->addCustomKriteria("asal_faktur", "='".$this->ruangan."'");
		$dbtable->addCustomKriteria ( "jenis", "='diskon_faktur'" );
		$responder = new DBResponder ( $dbtable, $this->uitable, $adapter );
		$d = $responder->command ( $command );
		echo json_encode ( $d );
	}
	public function initialize() {
		if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == "faktur_dk" && isset ( $_POST ['command'] )) {
			$this->command ( $_POST ['command'] );
		} else {
			$this->phpPreLoad ();
			$this->jsLoader ();
			$this->cssLoader ();
			$this->jsPreLoad ();
			$this->cssPreLoad ();
			$this->htmlPreLoad ();
		}
	}
	public function phpPreLoad() {
		$this->uitable->addModal ( "id", "hidden", "", "" );
		$this->uitable->addModal ( "waktu", "date", "Tanggal Dibuat", date ( "Y-m-d" ) );
		$this->uitable->addModal ( "noref", "hidden", "", $this->nofaktur );
		$this->uitable->addModal ( "id_faktur", "hidden", "", $this->idfaktur );
		$this->uitable->addModal ( "max_diskon", "hidden", "", $this->total);
		$this->uitable->addModal ( "v_diskon", "text", "Total", ArrayAdapter::format("only-money Rp.", $this->total),"n","",true);
		$this->uitable->addModal ( "nilai", "money", "Nilai", "" );
		$this->uitable->addModal ( "persen", "text", "Persentase", "" );
		$this->uitable->addModal ( "asal_faktur", "hidden", "", $this->ruangan );
		$this->uitable->addModal ( "status", "hidden", "", "1" );
		$this->uitable->addModal ( "json", "hidden", "", $this->json );
		$this->uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
		echo addCSS ( "framework/bootstrap/css/datepicker.css" );
		echo addJS ( "framework/smis/js/table_action.js" );
		echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
		echo $this->uitable->getHtml ();
		echo $this->uitable->getModal ()->setTitle ( "Kwitansi Faktur" )->setComponentSize ( Modal::$MEDIUM )->getHtml ();
	}
	public function jsPreLoad() {
		?>
				<script type="text/javascript">
				var faktur_dk;
				var id_faktur_dk="<?php echo $this->idfaktur; ?>";
				var nofaktur_dk="<?php echo $this->nofaktur; ?>";
				var total_faktur="<?php echo $this->total; ?>";
				var asal_ruang_faktur_dk="<?php echo $this->ruangan; ?>";
				$(document).ready(function(){
					$(".mydate").datepicker();
					
					/**
					*  when Percentage Change then Update the Value
					*/
					$("#faktur_dk_persen").keyup(function(e){
						var val=this.value;
						var total=Number($("#faktur_dk_max_diskon").val());
						var res=total*val/100;
						setMoney("#faktur_dk_nilai",res+"");
					});

					/**
					*  when Value Change then Update the Percentage
					*/
					$("#faktur_dk_nilai").keyup(function(e){
						var val=getMoney("#faktur_dk_nilai");
						var total=Number($("#faktur_dk_max_diskon").val());
						var res=val*100/total;
						$("#faktur_dk_persen").val(res);
					});
					
					var column=new Array("id","waktu","jt_bayar","noref","nilai","keterangan","json","persen","asal_faktur");
					faktur_dk=new TableAction("faktur_dk","finance","faktur",column);					
					faktur_dk.getRegulerData=function(){
						var data={
							page:this.page,
							action:this.action,
							super_command:this.super_command,
							ruangan:asal_ruang_faktur_dk,
							id_faktur:id_faktur_dk,
							jk_total:total_faktur,
							nofaktur:nofaktur_dk,
							status:1,
							jenis:"diskon_faktur"
						};
						return data;
					};
					faktur_dk.setSuperCommand("faktur_dk");
					faktur_dk.view();
					faktur_dk.view=function(){
						showLoading();
						var self=this;
						var view_data=this.getViewData();
						$.post('',view_data,function(res){
							var json=getContent(res);
							if(json==null) {
								return;
							}
							$("#"+self.prefix+"_add_form").smodal('hide');
							$("#"+self.prefix+"_list").html(json.list);
							$("#"+self.prefix+"_pagination").html(json.pagination);		
							hitungFaktur();
							dismissLoading();
						});
					};
					
					
				});
			</script>
			<?php
	}
	public function cssPreLoad() {
		?>
		<style type="text/css">
			#faktur_dk_add_form_form>div>label {width: 230px;}
		</style>
		<?php
	}
}
?>