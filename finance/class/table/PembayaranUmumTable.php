<?php 

class PembayaranUmumTable extends Table{
    
    public function getPrintedElement($pure,$formatted){
        global $db;
        global $user;
        loadLibrary("smis-libs-function-math");
        $table   = new TablePrint("pbu");
        $table->setDefaultBootrapClass(false);
        $table->setMaxWidth(false);
        
        $logo    = getLogo();
        $img     = "<img src='".$logo."' />";
        $name    = getSettings($db,"smis_autonomous_name","");
		$address = getSettings($db,"smis_autonomous_address","");
        
        $table->addColumn($img,2,4,null,null," pbu_logo ");
        $table->addColumn($name,4,1,null,null," pbu_name ");
        $table->addColumn("Bukti Kas Keluar",3,2,null,null," pbu_title ");
        $table->commit("header");
        
        $table->addColumn($address,4,3,null,null,"pbu_address");
        $table->commit("header");
        
        $table->addColumn("No.",1,1);
        $table->addColumn(":",1,1);
        $table->addColumn($formatted["No Referensi"],1,1,null,null,"pbu_noref");
        $table->commit("header");
        
        $table->addColumn("Tgl",1,1);
        $table->addColumn(":",1,1);
        $table->addColumn($formatted['Tanggal'],1,1,null,null,"pbu_tanggal");
        $table->commit("header");
        
        $table->addSpace(9,1);
        $table->commit("body");
        
        $table->addColumn("Dibayarkan Kepada ",2,1);
        $table->addColumn(":",1,1);
        $table->addColumn($pure->penerima,6,1,null,null,"pbu_penerima");        
        $table->commit("body");
        
        $table->addColumn("Jumlah",2,1);
        $table->addColumn(":",1,1);
        $table->addColumn("Rp. ".ArrayAdapter::format("only-money",$pure->nilai),6,1,null,null,"pbu_jumlah");        
        $table->commit("body");
        
        
        $table->addColumn("Terbilang",2,1);
        $table->addColumn(":",1,1);
        $table->addColumn(numbertell($pure->nilai)." Rupiah",6,1,null,null,"pbu_terbilang");        
        $table->commit("body");
        
        
        $table->addColumn("Keterangan",2,1);
        $table->addColumn(":",1,1);
        $table->addColumn($pure->keterangan,6,1,null,null,"pbu_ket");        
        $table->commit("body");
        
        $table->addSpace(9,1);
        $table->commit("body");
        
        $table->addColumn("Dibukukan Oleh",3,1,null,null,"pbu_footer");
        $table->addSpace(1,1);
        $table->addColumn("Disetujui Oleh",2,1,null,null,"pbu_footer");
        $table->addSpace(1,1);
        $table->addColumn("Dibayar Oleh",2,1,null,null,"pbu_footer");
        $table->commit("body");
        
        $table->addSpace(9,1);
        $table->commit("body");
        
        $table->addColumn("_______________ ",3,1,null,null,"pbu_footer");
        $table->addSpace(1,1);
        $table->addColumn("_______________ ",2,1,null,null,"pbu_footer");
        $table->addSpace(1,1);
        $table->addColumn("<u>".$user->getNameOnly()."</u>",2,1,null,null,"pbu_footer");
        $table->commit("body");
        
        $css="<style type='text/css'>".getSettings($db,"finance-css-print-pembayaran-umum","")."</style>";
        
        return $table->getHtml().$css;
	}
    
}

?>