<?php 

class TerminTable extends Table{
    
    public function getContentButton($id){
        if($this->current_data['kunci']==1){
            $this->setDelButtonEnable(false);
            $this->setEditButtonEnable(false);
        }else{
            $this->setDelButtonEnable(true);
            $this->setEditButtonEnable(true);
        }
        
        return parent::getContentButton($id);
    }
    
}

?>