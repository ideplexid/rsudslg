<?php
/**
 * 
 * this Class is used for 
 * viewing payment of faktur 
 * and for printing purpose 
 * of each faktur
 * 
 * @uses finance/class/template/FakturPayment.php
 * @author goblooge
 * @since 29 Oct 2014
 * @version 1.0.0
 * @license LGPL
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @link http://www.goblooge.com
 *
 */
class FakturUITable extends Table {
	public function getContentButton($id) {
		if ($this->current_data ['Cair'] == "") {
			$this->setEditButtonEnable ( true );
			$this->setDelButtonEnable ( true );
			$btngrup = parent::getContentButton ( $id );
			$cair 	 = new Button  ( "", "", "Cair" );
			$cair	 ->setIsButton ( Button::$ICONIC )
					 ->setIcon   ( "icon-black fa fa-money" )
					 ->setClass  (" btn-info")
					 ->setAction ( $this->action . ".cair('" . $id . "')" );
			$btngrup ->addButton ( $cair );
			return $btngrup;
		} else {
			$this->setEditButtonEnable ( false );
			$this->setDelButtonEnable ( false );
			return parent::getContentButton ( $id );
		}
	}

	public function getPrintedElement($p, $f) {
		require_once 'smis-libs-class/smis-company-profile.php';
		global $user;
		global $db;
		$company 	= CompanyProfile::getInstance ();
		$header 	= $company->getHeader ();
		$json 		= json_decode ( $p->json );
		
		loadLibrary ( "smis-libs-function-math" );
		$tableprint = new TablePrint ( "faktur_tagihan" );
		$tableprint	->setDefaultBootrapClass(false)
					->setMaxWidth(true)
					->addColumn("<font class='f20'>".getSettings($db, "finance-company-name", "")."</font>", 1, 1,NULL,NULL,"center  bold")
					->addSpace(1, 1)
					->addColumn("<font class='f20'>No. ".ArrayAdapter::format("only-digit8", $p->id)."<font>", 1, 1,NULL,NULL,"center  bold")
					->commit("body");
		$tableprint	->addColumn("<font class='f15'>".(getSettings($db, "finance-company-address", "")."</font>"), 1, 1,NULL,NULL,"center")
					->addSpace(2, 1)
					->commit("body");		
		$tableprint	->addColumn("<font class='f15'>".(getSettings($db, "finance-company-town", ""))."</font>", 1, 1,NULL,NULL,"center")
					->addSpace(2, 1)
					->commit("body");
		$tableprint	->addColumn("<font class='f20'>PENGELUARAN KAS<font>", 3, 1,NULL,NULL,"center bold")
					->commit("body");		
		$tableprint	->addColumn("<font class='f13'>Telah Terima Dari </font>", 1, 1)
					->addColumn("<font class='f13'> : ".strtoupper($company->getTitle())."</font>", 2, 1)
					->commit("body");		
		$tableprint	->addColumn("<font class='f13'>Uang Sebanyak </font>", 1, 1)
					->addColumn("<font class='f13'> : ".ArrayAdapter::format("only-money Rp.", $p->nilai)."</font>", 2, 1)
					->commit("body");		
		$tableprint	->addSpace(1, 1)
					->addColumn("<font class='f13'> : ".numbertell ( $p->nilai )." Rupiah </font>", 2, 1)
					->commit("body");

		$utk_byr	= "Hutang Pada ".$json->nama_vendor." Untuk Faktur No. ".$json->no_faktur;
		$tableprint	->addColumn("<font class='f13'>Untuk Pembayaran</font>", 1, 1)
					->addColumn("<font class='f13'> : ".$utk_byr."</font>", 2, 1)
					->commit("body");

		$br			= getSettings($db, "finance-gap-print", "1");
		$br			= $br*1;
		$b			= "";
		for($i=0;$i<$br;$i++){
			$b	   .= "</br>";
		}
		$tableprint	->addColumn($b, 3, 1)
					->commit("body");
		
		$nama_dir	= getSettings($db, "finance-nama-direktur", "");
		$nip_dir	= getSettings($db, "finance-nip-direktur", "");
		$dir		= "<u>".$nama_dir."</u></br> NIK. ".$nip_dir;
		
		$kasir_keu	= "Kasir Keuangan.</br></br></br></br></br></br> ".$user->getNameOnly();
		$ka_direk	= "Menyetujui : </br></br></br></br></br> ".$dir;
		$ka_market  = getSettings($db, "finance-company-town", "").", ".ArrayAdapter::format ( "date d M Y", $p->waktu )."</br> Penerima </br></br></br></br></br>".$p->penerima;
		
		$tableprint	->addSpace(3, 1)
					->commit("body");
		
		$tableprint	->addColumn($kasir_keu, 1, 1,NULL,NULL,"center top_solid bottom_solid")
					->addColumn($ka_direk, 1, 1,NULL,NULL,"center top_solid left_solid bottom_solid")
					->addColumn($ka_market, 1, 1,NULL,NULL,"center top_solid left_solid bottom_solid")
					->commit("body","valign_top");
		
		return $tableprint->getHtml();
	}
}

?>