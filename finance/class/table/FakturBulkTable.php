<?php
/**
 * 
 * this Class is used for 
 * viewing payment of faktur bulk
 * and for printing purpose 
 * of each faktur
 * 
 * @author goblooge
 * @since 29 Oct 2014
 * @version 1.0.0
 * @license LGPL
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @link http://www.goblooge.com
 *
 */

require_once "finance/class/table/FakturUITable.php";
class FakturBulkTable extends FakturUITable {
	public function getPrintedElement($p, $f) {
		global $db;
		$dbtable	= new DBTable($db,"smis_fnc_bayar");
		$dbtable	->addCustomKriteria("id_bulk","='".$p->id."'")
					->setShowAll(true);
		$data		= $dbtable->view("","0");
		$list		= $data['data'];

		loadLibrary ( "smis-libs-function-math" );
		require_once 'smis-libs-class/smis-company-profile.php';
		global $user;
		global $db;
		$company 	= CompanyProfile::getInstance ();
		$header 	= $company->getHeader ();
		$json 		= json_decode ( $p->json );
		
		$keluar 	= strpos(strtolower($p->nama_dompet),"bank")!==false?"Bank":"Kas"; 
		
		$tableprint = new TablePrint ( "faktur_tagihan_bulk" );
		$tableprint	->setDefaultBootrapClass(false)
					->setMaxWidth(false);
		$tableprint ->addColumn($company->getLogoHtml(),1,3)
					->addColumn("Bukti ".$keluar." Keluar",3,2,null,null,"title")
                    ->addColumn("No. Bukti",1,1,null,null,"pynum")
                    ->addColumn(" : ".$p->pynum,1,1,"body",null,"pynum");
		$tableprint ->addColumn("Tanggal Pembuatan",1,1,null,null,"waktu")
                    ->addColumn(" : ".ArrayAdapter::format("date d M Y",$p->waktu),1,1,"body",null,"waktu");		
		$tableprint ->addColumn("Kepada",1,1,null,null,"vendor")
					->addColumn(" : ".$p->nama_vendor,4,1,"body",null,"vendor");
        $tableprint ->addSpace(6,1,"body");
		$tableprint ->addColumn("Keterangan",5,1,null,null,"subtitle btom")
					->addColumn("Nilai",1,1,"body",null,"subtitle  btom");
		
		$no = 0;
		foreach($list as $l){
			$tableprint ->addColumn((++$no).".",1,1)
						->addColumn(" Pembayaran Faktur ".$l->noref,4,1)
						->addColumn(ArrayAdapter::format("money Rp.",$l->nilai),1,1,"body");
		}
		$tableprint ->addColumn("Jumlah",5,1,null,null,"subtitle  left btop")
                    ->addColumn(ArrayAdapter::format("money Rp.",$p->total),1,1,"body",null,"subtitle right btop");
                    
        $tableprint ->addSpace(6,1,"body");
		$tableprint ->addColumn(" Terbilang : ",6,1,"body",null,"left underlined");
        $tableprint ->addColumn(numbertell($p->total)." Rupiah",6,1,"body",null,"left italic");
        
        $tableprint ->addSpace(6,1,"body");
		$tableprint ->addColumn("Penganggung Jawab",2,1,null,null,"center")
					->addColumn("Dibayar Oleh",1,1,null,null,"center")
					->addColumn("Disetujui",1,1,null,null,"center")
					->addColumn("Dibukukan Oleh",1,1,null,null,"center")
					->addColumn( $company->getTown().",__________",1,1,"body",null,"center");
		$tableprint ->addSpace(1,1)
					->addSpace(1,1)
					->addSpace(1,1)
					->addSpace(1,1)
					->addSpace(1,1)
                    ->addSpace(1,1,"body");
        $tableprint ->addSpace(6,1,"body");
		$tableprint ->addColumn("___________________",2,1,null,null,"center")
					->addColumn($user->getNameOnly(),1,1,null,null,"center")
					->addColumn("___________________",1,1,null,null,"center")
					->addColumn("___________________",1,1,null,null,"center")
					->addColumn("___________________",1,1,"body",null,"center");
					
		return $tableprint->getHtml();
	}
}

?>