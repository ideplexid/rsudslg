<?php 

class DetailKasBankTable extends Table{
    private $currrent_account;
    public function setCurrentAccount($account){
        $this->currrent_account=$account;
        return $this;
    }
    public function getContentButton($id){
        if($this->current_data['Nomor'] == $this->currrent_account){
            $this->setDelButtonEnable(false);
            $this->setEditButtonEnable(false);
        }else{
            $this->setDelButtonEnable(true);
            $this->setEditButtonEnable(true);
        }
        return parent::getContentButton($id);
    }
}

?>