<?php 

class PendapatanOksigenManualServiceResponder extends ServiceResponder{
    public function excel(){
		global $user;
		loadLibrary("smis-libs-function-export");
        $this->addData("command","list");
        $this->getService()->setData($this->getData());        
        $this->getService()->setMode(ServiceConsumer::$CLEAN_BOTH);
        $this->getService()->execute();        
        $d=$this->getService()->getContent();
        
        $data = array();
        foreach($d as $one){
            foreach($one as $x){
                $data[] = $x;
            }
        }
        
        $this->getAdapter()->setNumber(0);
        $this->getAdapter()->add("H. Liter", "harga_liter")
                            ->add("Biaya Lain", "biaya_lain")
                            ->add("H. Total", "harga");
		   
        $uidata = $this->getAdapter()->getContent($data);
        
        require_once "smis-libs-out/php-excel/PHPExcel.php";
		$file = new PHPExcel ();
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
        $sheet->setTitle ("Pendapatan Oksigen Manual");
        
        $sheet->mergeCells("A1:J1")->setCellValue ( "A" . 1, "Pendapatan Oksigen Central - ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

		$sheet->setCellValue ( "A" . 2, "No.");
		$sheet->setCellValue ( "B" . 2, "Ruangan" );
		$sheet->setCellValue ( "C" . 2, "Mulai" );
		$sheet->setCellValue ( "D" . 2, "Pasien" );
		$sheet->setCellValue ( "E" . 2, "NRM" );
		$sheet->setCellValue ( "F" . 2, "No. Reg" );
		$sheet->setCellValue ( "G" . 2, "Liter" );
		$sheet->setCellValue ( "H" . 2, "H. Liter" );
		$sheet->setCellValue ( "I" . 2, "Biaya Lain" );
        $sheet->setCellValue ( "J" . 2, "H. Total" );  
        
        $index=2;
		$no=0;
		$sheet->getStyle ( "A1:J2")->getFont ()->setBold ( true );
		foreach($uidata as $x){
			$no++;
			$index++;
			$sheet->setCellValue ( "A" . $index, $x["No."]." ");
            $sheet->setCellValue ( "B" . $index, $x["Ruangan"] );
            $sheet->setCellValue ( "C" . $index, $x["Mulai"] );
            $sheet->setCellValue ( "D" . $index, $x["Pasien"] );
            $sheet->setCellValue ( "E" . $index, $x["NRM"] );
            $sheet->setCellValue ( "F" . $index, $x["No. Reg"] );
            $sheet->setCellValue ( "G" . $index, $x["Liter"] );
            $sheet->setCellValue ( "H" . $index, $x["H. Liter"] );
            $sheet->setCellValue ( "I" . $index, $x["Biaya Lain"] );
            $sheet->setCellValue ( "J" . $index, $x["H. Total"] );  
        }
        $sheet->setCellValue ( "E" . $index, "Total" );
        $sheet->setCellValue ( "J" . $index, "=sum(J3:J".($index-1).")" );
        $sheet->getStyle ( 'A2:J' . $index )->applyFromArray ( $fillcabang );
        $sheet->getStyle ( "A".$index.":J".$index)->getFont ()->setBold ( true );
        $sheet->getStyle('H3:J'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="Pendapatan Oksigen Manual.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );

		return;
	}
}