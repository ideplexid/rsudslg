<?php 
require_once "finance/class/responder/FakturPaymentResponder.php";
class BulkFakturDetailResponder extends FakturPaymentResponder{    
    public function save(){
        global $db;
        $result = DBResponder::save();
        $this->notifyTotalPembayaran($_POST['id_faktur'],$_POST['asal_faktur']);
        $this->notifyTotalPengeluaran($_POST['id_bulk']);
        return $result;
    }
    
    public function delete(){
        $x           = $this->dbtable->select($_POST['id']);
        $id_faktur   = $x->id_faktur;
        $id_bulk     = $x->id_bulk;
        $asal_faktur = $x->asal_faktur;
        $result      = parent::delete();
        $this->notifyTotalPembayaran($id_faktur,$asal_faktur);
        $this->notifyTotalPengeluaran($id_bulk);
        return $result;
    }
    
    public function notifyTotalPengeluaran($id_bulk){
        $db              = $this->dbtable->get_db();
        $qtotal          = "SELECT ifnull(sum(nilai),0) as total FROM smis_fnc_bayar WHERE prop!='del' AND id_bulk='".$id_bulk."' status=1";
        $data['total']   = $db->get_var($qtotal);        
        $update['id']    = $id_bulk;        
        $dbtable         = new DBTable($db,"smis_fnc_bayar_bulk");
        $dbtable         ->update($data,$update);
    }
}
?>