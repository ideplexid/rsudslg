<?php 

/**
 * this class update the
 * smis_hrd_invoice total, tax and other
 * based on input in smis_hrd_invoice_detail
 * and smis_hrd_invoice_bayar
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @license 	: LGPLv2
 * @database 	: - smis_hrd_invoice
 *                - smis_hrd_invoice_detail
 *                - smis_hrd_invoice_bayar
 * @since		: 13 Oktober 2017
 * @version		: 1.0.0
 * 
 * */
class DetailInvoiceResponder extends DBResponder{
    
    protected $kepada_invoice;
    protected $nomor_invoice;
    
    
    public function save(){
        $result = parent::save();
        $dbtable=$this->updateTotalInvoince($_POST['id_invoice']);
        $this->notifyAccounting($_POST['id_invoice'],$dbtable,"");
        return $result;
    }
    
    public function delete(){
        $result= parent::delete();
        $dbtable=$this->updateTotalInvoince($_POST['id_invoice']);
        $this->notifyAccounting($_POST['id_invoice'],$dbtable,"del");
        return $result;
    }
    
    public function notifyAccounting($id_invoice,DBTable $dbtable,$operation){
        $data['jenis_akun']     = "transaction";
        $data['jenis_data']     = "piutang";
        $data['id_data']        = $id_invoice;
        $data['entity']         = "finance";
        $data['service']        = "get_detail_accounting_piutang_invoice";
        $data['data']           = $id_invoice;
        $data['code']           = "FPIV-".$id_invoice;
        $data['operation']      = $operation;
        
        $invoice                = $dbtable->select($id_invoice);
        $data['tanggal']        = $invoice->tanggal;
        $data['uraian']         = "Invoice ".$invoice->kepada." Dengan Nomor ".$invoice->nomor;
        $data['nilai']          = $invoice->total;
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $this;
    }
    
    public function updateTotalInvoince($id){
        /*mengambil id invoicenya*/
        $dbtable= new DBTable($this->getDBTable()->get_db(),"smis_fnc_invoice");
        $invoice=$dbtable->select(array("id"=>$id));
        
        $this->nomor_invoice    = $invoice->nomor;
        $this->kepada_invoice   = $invoice->kepada;
        
        /*mengambil detail pembayaran*/
        $query  = "SELECT sum(total) as total
                    FROM smis_fnc_invoice_detail WHERE id_invoice='".$id."' AND prop!='del' ";
        $nilai  = $this->getDBTable()->get_db()->get_var($query);
        
        /*mengambil data pelunasan*/
        $query  = "SELECT sum(nilai) as total
                    FROM smis_fnc_invoice_bayar WHERE id_invoice='".$id."' AND prop!='del' ";
        $lunas  = $this->getDBTable()->get_db()->get_var($query);
        
        $update=array();
        $update['sub_total']        = $nilai;
        $update['diskon']           = $nilai*$invoice->diskon_persen/100+$invoice->diskon_rupiah;
        $update['dpp']              = $nilai-$update['diskon'];       
        $update['nilai_ppn']        = $update['dpp']*$invoice->pajak_ppn/100;;
        $update['sub_total_pajak']  = $update['dpp']+$update['nilai_ppn'];
        $update['nilai_pph23']      = $update['dpp']*$invoice->pajak_pph23/100;
        $update['nilai_pph22']      = $update['dpp']*$invoice->pajak_pph22/100;
        $update['total']            = $update['sub_total_pajak']-$update['nilai_pph23']-$update['nilai_pph22'];
        $update['lunas']            = $lunas;
        $update['sisa']             = $update['total']-$lunas;
        
        $idx=array();
        $idx['id']=$id;
        
        $dbtable->update($update,$idx);
        return $dbtable;
    }
    
}

?>