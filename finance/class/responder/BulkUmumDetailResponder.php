<?php 
require_once "finance/class/responder/FakturPaymentResponder.php";
class BulkUmumDetailResponder extends FakturPaymentResponder{    
    public function save(){
        global $db;
        $result = DBResponder::save();
        $this->notifyTotalPengeluaran($_POST['id_bulk']);
        return $result;
    }
    
    public function delete(){
        $x           = $this->dbtable->select($_POST['id']);
        $id_faktur   = $x->id_faktur;
        $id_bulk     = $x->id_bulk;
        $asal_faktur = $x->asal_faktur;
        $result      = parent::delete();
        $this->notifyTotalPengeluaran($id_bulk);
        return $result;
    }
    
    public function notifyTotalPengeluaran($id_bulk){
        $db              = $this->dbtable->get_db();
        $qtotal          = "SELECT ifnull(sum(nilai),0) as total FROM smis_fnc_bayar WHERE prop!='del' AND id_bulk='".$id_bulk."'";
        $data['total']   = $db->get_var($qtotal);        
        $update['id']    = $id_bulk;        
        $dbtable         = new DBTable($db,"smis_fnc_bayar_bulk");
        $dbtable         ->update($data,$update);
        $this->synchronizeToAccounting($db,$id_bulk);
    }

    public function synchronizeToAccounting($db,$id,$is_del=""){
        require_once "smis-base/smis-include-service-consumer.php";
        $this->dbtable->setName("smis_fnc_bayar_bulk");
        $x = $this->dbtable->selectEventDel($id);
        $data               = array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "bayar_umum";
        $data['id_data']    = $id;
        $data['entity']     = "finance";
        $data['service']    = "get_detail_accounting_bayar_umum_bulk";
        $data['data']       = $id;
        $data['code']       = "finance-bayar-umum-bulk-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu;
        $data['uraian']     = "Pembayaran Umum untuk ".$x->nama_vendor;
        $data['nilai']      = $x->nilai;
        
        $serv = new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }    
}
?>