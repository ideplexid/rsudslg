<?php 

class PendapatanOksigenCentralServiceResponder extends ServiceResponder{
    public function excel(){
		global $user;
		loadLibrary("smis-libs-function-export");
        $this->addData("command","list");
        $this->getService()->setData($this->getData());
        $this->getService()->execute();
        $this->getService()->setMode(ServiceConsumer::$CLEAN_BOTH);
        $d = $this->getService()->getContent();

        $data = array();
        foreach($d as $one){
            foreach($one as $x){
                $data[] = $x;
            }
        }

        $this->getAdapter()->setNumber(0);
        $this->getAdapter()->add("H. Jam", "harga_jam")
                            ->add("H. Menit", "harga_menit")
                            ->add("H. Total", "harga");

        $uidata = $this->getAdapter()->getContent($data);

        require_once "smis-libs-out/php-excel/PHPExcel.php";
		$file = new PHPExcel ();
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
        $sheet->setTitle ("Pendapatan Oksigen Central");
        
        $sheet->mergeCells("A1:L1")->setCellValue ( "A" . 1, "Pendapatan Oksigen Central - ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

		$sheet->setCellValue ( "A" . 2, "No.");
		$sheet->setCellValue ( "B" . 2, "Ruangan" );
		$sheet->setCellValue ( "C" . 2, "Mulai" );
		$sheet->setCellValue ( "D" . 2, "Pasien" );
		$sheet->setCellValue ( "E" . 2, "NRM" );
		$sheet->setCellValue ( "F" . 2, "No. Reg" );
		$sheet->setCellValue ( "G" . 2, "H. Jam" );
		$sheet->setCellValue ( "H" . 2, "H. Menit" );
		$sheet->setCellValue ( "I" . 2, "Jam" );
        $sheet->setCellValue ( "J" . 2, "Menit" );  
        $sheet->setCellValue ( "K" . 2, "Skala" );  
        $sheet->setCellValue ( "L" . 2, "H. Total" );  
        
        $index=2;
		$no=0;
        $sheet->getStyle ( "A1:L2")->getFont ()->setBold ( true );
        
		foreach($uidata as $x){
			$no++;
			$index++;
			$sheet->setCellValue ( "A" . $index, $x["No."]." ");
            $sheet->setCellValue ( "B" . $index, $x["Ruangan"] );
            $sheet->setCellValue ( "C" . $index, $x["Mulai"] );
            $sheet->setCellValue ( "D" . $index, $x["Pasien"] );
            $sheet->setCellValue ( "E" . $index, $x["NRM"] );
            $sheet->setCellValue ( "F" . $index, $x["No. Reg"] );
            $sheet->setCellValue ( "G" . $index, $x["H. Jam"] );
            $sheet->setCellValue ( "H" . $index, $x["H. Menit"] );
            $sheet->setCellValue ( "I" . $index, $x["Jam"] );
            $sheet->setCellValue ( "J" . $index, $x["Menit"] );  
            $sheet->setCellValue ( "K" . $index, $x["Skala"] );  
            $sheet->setCellValue ( "L" . $index, $x["H. Total"] );  
        }
        //$index++;
        $sheet->setCellValue ( "E" . $index, "Total" );
        $sheet->setCellValue ( "L" . $index, "=sum(L3:L".($index-1).")" );
        $sheet->getStyle ( 'A2:L' . $index )->applyFromArray ( $fillcabang );
        $sheet->getStyle ( "A".$index.":L".$index)->getFont ()->setBold ( true );
        $sheet->getStyle('G3:H'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
		$sheet->getStyle('L3:L'.$index)->getNumberFormat()->setFormatCode('#,##0.00');
		
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="Data Pendapatan Oksigen Central.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );

		return;
	}
}