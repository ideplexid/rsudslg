<?php

class BulkFakturResponder extends DBResponder{
    public function save(){
        global $db;
        if(isset($_POST['status']) && $_POST['status']=="1"){
            $this->addColumnFixValue("tgl_cair",date("Y-m-d"));
        }else  if($_POST['id']==""){
            require_once "finance/function/payment_number.php";
            $this->addColumnFixValue("pynum",payment_number($_POST['nama_dompet']));
            $this->addColumnFixValue("jenis","faktur");
        }
        $result = parent::save();        
        $x      = $this->dbtable->select($result['id']);
        if(isset($_POST['status']) && $_POST['status']=="1" && getSettings($db,"finance-enabled-notif-bayar-faktur","1")=="1"){
            $this->notifyAccounting($result['id'],$this->column_fix_value["tgl_cair"],"bayar_faktur_bulk","FCFB",$x->total,"");
            $this->cairAll($result['id']);
        }else if(getSettings($db,"finance-enabled-notif-hutang-faktur","1")=="1"){
            $this->notifyAccounting($result['id'],$_POST['waktu'],"hutang_faktur_bulk","FHFB",$x->total,"");
        }
        return $result;
    }
    
    public function delete(){
        $x               = $this->dbtable->select($_POST['id']);
        $id_faktur       = $x->id_faktur;
        $asal_faktur     = $x->asal_faktur;
        $result          = parent::delete();
        $this->notifyAccounting($_POST['id'],date("Y-m-d"),"hutang_faktur_bulk","FHFB",$x->nilai,"del");
        $this->notifyTotalPembayaran($id_faktur,$asal_faktur);
        $this->deleteAll();
        return $result;
    }

    public function deleteAll($id_bulk){
        $query = "UPDATE smis_fnc_bayar SET smis_fnc_bayar.prop = 'del'
                    WHERE smis_fnc_bayar.id_bulk='".$id_bulk."' ";
        $db    = $this->dbtable->get_db();
        $db    ->query($query);
    }

    public function cairAll($id_bulk){
        $query = "UPDATE smis_fnc_bayar LEFT JOIN smis_fnc_bayar_bulk ON
                        smis_fnc_bayar.id_bulk   =  smis_fnc_bayar_bulk.id
                    SET smis_fnc_bayar.tgl_cair  = smis_fnc_bayar_bulk.tgl_cair,
                        smis_fnc_bayar.code      = smis_fnc_bayar_bulk.code,
                        smis_fnc_bayar.status    = smis_fnc_bayar_bulk.status,
                        smis_fnc_bayar.pynum     = smis_fnc_bayar_bulk.pynum
                    WHERE smis_fnc_bayar.id_bulk = '".$id_bulk."' ";
        $db    = $this->dbtable->get_db();
        $db    ->query($query);
    }

    public function notifyTotalPembayaran($id_bulk,$asal_faktur){
        $db             = $this->dbtable->get_db();
        $dbtable        = new DBTable($db,"smis_ksr_bayar");
        $dbtable        ->setShowAll(true)
                        ->addCustomKriteria("id_bulk","='".$id_bulk."'");
        $data           = $dbtable->view("","0");
        $list           = $data['data'];
        foreach($list as $l){
            $this->notifyTotalPembayaranOne($l->id, $l->id_faktur,$asal_faktur);
        }
    }

    public function notifyTotalPembayaranOne($id_bayar, $id_faktur,$asal_faktur){
        /**notif kepada semua id_faktur */
        $db                 = $this->dbtable->get_db();
        $qtotal             = "SELECT ifnull(sum(nilai),0) as total FROM smis_fnc_bayar WHERE prop!='del' AND id_faktur='".$id_faktur."' AND id!='".$id_bayar."'";
        $data['bayar']      = $db->get_var($qtotal);
        $data['id']         = $id_faktur;
        require_once "smis-base/smis-include-service-consumer.php";
        $serv               = new ServiceConsumer($db,"set_nilai_terbayar_faktur",$data,$asal_faktur);
        $serv->execute();
    }
    
    public function notifyAccounting($id,$tanggal,$jenis,$code,$nilai,$operation){
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "pembayaran_hutang";
        $data['id_data']    = $id;
        $data['entity']     = "finance";
        $data['service']    = "get_detail_accounting_".$jenis;
        $data['data']       = $id;
        $data['code']       = $code."-".$id;
        $data['operation']  = $operation;
        $data['tanggal']    = $tanggal;
        $data['uraian']     = ucwords(strtolower(ArrayAdapter::slugFormat("unslug",$jenis)))." pada No. Bulk ".$id;
        $data['nilai']      = $nilai;
        require_once "smis-base/smis-include-service-consumer.php";
        $serv               = new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $this;
    }    
}

?>