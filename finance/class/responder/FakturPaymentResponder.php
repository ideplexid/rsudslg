<?php 
class FakturPaymentResponder extends DBResponder{    
    public function save(){
        global $db;
        $result = parent::save(); 
        if(isset($_POST['status']) && $_POST['status']=="1" && getSettings($db,"finance-enabled-notif-bayar-faktur","1")=="1"){
            $x  = $this->dbtable->select($result['id']);            
            $this->notifyAccounting($result['id'],$this->column_fix_value["tgl_cair"],"bayar_faktur","FCF",$x->nilai,"");
        }else if(getSettings($db,"finance-enabled-notif-hutang-faktur","1")=="1"){
            $this->notifyAccounting($result['id'],$_POST['waktu'],"hutang_faktur","FHF",$_POST['nilai'],"");
        }

        $this->notifyTotalPembayaran($_POST['id_faktur'],$_POST['asal_faktur']);
        return $result;
    }
    
    public function delete(){
        $x               = $this->dbtable->select($_POST['id']);
        $id_faktur       = $x->id_faktur;
        $asal_faktur     = $x->asal_faktur;
        $result          = parent::delete();
        $this->notifyAccounting($_POST['id'],date("Y-m-d"),"hutang_faktur","FHF",$x->nilai,"del");
        $this->notifyTotalPembayaran($id_faktur,$asal_faktur);
        return $result;
    }

    public function notifyTotalPembayaran($id_faktur,$asal_faktur){
        $db              = $this->dbtable->get_db();
        $qtotal          = "SELECT ifnull(sum(nilai),0) as total FROM smis_fnc_bayar WHERE prop!='del' AND id_faktur='".$id_faktur."' AND status=1 ";
        $data['bayar']   = $db->get_var($qtotal);
        
        $data['id']      = $id_faktur;
        require_once "smis-base/smis-include-service-consumer.php";
        $serv            = new ServiceConsumer($db,"set_nilai_terbayar_faktur",$data,$asal_faktur);
        $serv->execute();
    }
    
    public function notifyAccounting($id,$tanggal,$jenis,$code,$nilai,$operation){
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "pembayaran_hutang";
        $data['id_data']    = $id;
        $data['entity']     = "finance";
        $data['service']    = "get_detail_accounting_".$jenis;
        $data['data']       = $id;
        $data['code']       = $code."-".$id;
        $data['operation']  = $operation;
        $data['tanggal']    = $tanggal;
        $data['uraian']     = ucwords(strtolower(ArrayAdapter::slugFormat("unslug",$jenis)))." pada No. Faktur ".$_POST['nofaktur'];
        $data['nilai']      = $nilai;
        require_once "smis-base/smis-include-service-consumer.php";
        $serv               = new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $this;
    }    
}
?>