<?php 

class LaporanJaspelResponder extends DBResponder{
    
    private $fname;
    public function setFileName($fname){
        $this->fname=$fname;
    } 

    public function excel(){
        require_once "smis-libs-out/php-excel/PHPExcel.php";
        $d = $this->getDBTable()->view("","0");
        $data = $d['data'];

        $adapter = $this->adapter;
        $adapter ->add ( "Waktu", "waktu" ,"date d M Y")
                ->add ( "Visite", "visite_dokter" )
                ->add ( "Konsul", "konsul_dokter" )
                ->add ( "Tindakan Medis Non Operatif Rawat Inap", "tindakan_dokter_ranap" )
                ->add ( "Tindakan Medis Non Operatif Rawat Jalan", "tindakan_dokter_rajal" )
                ->add ( "Periksa Dokter Poli", "periksa_rajal" )
                ->add ( "Periksa Dokter IGD", "periksa_igd" )
                ->add ( "Harga", "harga" );
        $adapter ->setAsNumber(true);
        $uidata = $adapter->getContent($data);

		$file = new PHPExcel ();
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
        $sheet->setTitle ($this->fname);
        $last_column = PHPExcel_Cell::stringFromColumnIndex(count($this->excel_column) - 1);

        $sheet  ->mergeCells("A1:L1")
                ->setCellValue ( "A" . 1, $this->fname." - ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

        $sheet->setCellValue("A2","No.");
        $sheet->setCellValue("B2","Tindakan");
        $sheet->setCellValue("C2","Harga");
        $sheet->setCellValue("D2","Nama Dokter");
        $sheet->setCellValue("E2","Tanggal/Bulan");
        $sheet->setCellValue("F2","Visite");
        $sheet->setCellValue("G2","Konsul");
        $sheet->setCellValue("H2","Tindakan Medis Non Operatif RANAP");
        $sheet->setCellValue("I2","Pemeriksaan IGD");
        $sheet->setCellValue("J2","Pemeriksaan Poli");
        $sheet->setCellValue("K2","Tindakan Medis Non Operatif Poli");
        $sheet->setCellValue("L2","Total Jaspel Dokter");
        $sheet->getStyle ( "A1:L2")->getFont ()->setBold ( true );

        $column_num = 2;
        $cur_carabayar = "";
        $total = 0;
        foreach($uidata as $x){
                $column_num++;
                $sheet->setCellValue("A".$column_num,$x["No."]." ");
                $sheet->setCellValue("B".$column_num,$x["Tindakan"]);
                $sheet->setCellValue("C".$column_num,$x["Harga"]=="0"?"":$x["Harga"]);
                $sheet->setCellValue("D".$column_num,$x["Dokter"]);
                $sheet->setCellValue("E".$column_num,$x["Waktu"]);
                $sheet->setCellValue("F".$column_num,$x["Visite"]=="0"?"":$x["Visite"]);
                $sheet->setCellValue("G".$column_num,$x["Konsul"]=="0"?"":$x["Konsul"]);
                $sheet->setCellValue("H".$column_num,$x["Tindakan Medis Non Operatif Rawat Inap"]=="0"?"":$x["Tindakan Medis Non Operatif Rawat Inap"]);
                $sheet->setCellValue("I".$column_num,$x["Periksa Dokter IGD"]=="0"?"":$x["Periksa Dokter IGD"]);
                $sheet->setCellValue("J".$column_num,$x["Periksa Dokter Poli"]=="0"?"":$x["Periksa Dokter Poli"]);
                $sheet->setCellValue("K".$column_num,$x["Tindakan Medis Non Operatif Rawat Jalan"]=="0"?"":$x["Tindakan Medis Non Operatif Rawat Jalan"]);
                $sheet->setCellValue("L".$column_num,$x["Total Jaspel Dokter"]=="0"?"":$x["Total Jaspel Dokter"]);
                
                $sheet->getStyle( 'C'.$column_num)->getNumberFormat()->setFormatCode('#,##0.00');
                $sheet->getStyle( 'F'.$column_num.":L".$column_num)->getNumberFormat()->setFormatCode('#,##0.00');
        }   
        $sheet->getStyle ( 'A1:L'.$column_num )->applyFromArray ($fillcabang);     
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);

		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="'.$this->fname.'.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );

		return;
    }

}