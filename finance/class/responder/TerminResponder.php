<?php 

class TerminResponder extends DBResponder{
    
    public function command($command){
        if($command=="upload_termin"){
            $this->uploadTermin();
            $p=new ResponsePackage();
            $p->setStatus(ResponsePackage::$STATUS_OK);
            $p->setAlertContent("Success","Termin Terkirim");
            $p->setAlertVisible(true);
            return $p->getPackage();
        }else{
            return parent::command($command);
        }
    }
    
    /**
     * @brief notif and recall all of the value
     *        hoo many the periode of salary bill payed
     * @return  null
     */
    public function uploadTermin(){
        $db=$this->getDBTable()->get_db();
        
        /*re count the total of it's current caller*/
        $id_termin=$_POST['id'];
        $query="SELECT sum(dibayar) as total FROM smis_fnc_termin_gaji_detail 
                WHERE id_termin='".$id_termin."' AND prop!='del' ";
        $total=$db->get_var($query);
        $id=array("id"=>$id_termin);
        $update=array("total_uang"=>$total);        
        $this->getDBTable()->update($update,$id);
        
        /*resume of all history of payment in it's current periode*/
        $x=$this->select(array("id"=>$_POST['id']));
        $id_periode=$x->id_periode;
        $query="SELECT sum(total_uang) as total FROM smis_fnc_termin_gaji 
                WHERE id_periode='".$id_periode."' AND prop!='del' ";        
        $total=$db->get_var($query);
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($db,"payment_salary_notif");
        $serv->addData("id_periode",$id_periode);
        $serv->addData("lunas",$total);
        $serv->setEntity("hrd");
        $serv->execute();
        
        //kunci termin
        $idx['id']=$_POST['id'];
        $upd['kunci']=1;
        $this->dbtable->update($upd,$idx);
        
        //kunci detail termin
        $this->dbtable->setName("smis_fnc_termin_gaji");
        $idxd['id_periode']=$_POST['id'];
        $this->dbtable->update($upd,$idx);
        
    }
    
}

?>