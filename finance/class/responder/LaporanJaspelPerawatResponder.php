<?php 

class LaporanJaspelPerawatResponder extends DBResponder{
    
    private $fname;
    public function setFileName($fname){
        $this->fname=$fname;
    } 
   
    public function excel(){
        require_once "smis-libs-out/php-excel/PHPExcel.php";
        $d = $this->getDBTable()->view("","0");
        $data = $d['data'];

        $adapter = $this->adapter;
        $adapter ->add ( "Tanggal", "waktu" ,"date d M Y")
                 ->add ( "Tindakan", "nama_tagihan" )
                 ->add ( "Harga", "harga" )
                 ->add ( "Nama Ruangan", "nama_ruangan","unslug" )
                 ->add ( "Tindakan Keperawatan", "tindakan_perawat" )
                 ->add ( "Pemeriksaan Poli", "periksa_dokter" )
                 ->add ( "Tindakan Medis Non Operatif Rawat Jalan", "tindakan_dokter" );
        $adapter ->setAsNumber(true);
        $uidata = $adapter->getContent($data);

		$file = new PHPExcel ();
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
        $last_column = PHPExcel_Cell::stringFromColumnIndex(count($this->excel_column) - 1);

        $sheet  ->mergeCells("A1:I1")
                ->setCellValue ( "A" . 1, $this->fname." - ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

        $sheet->setCellValue("A2","No.");
        $sheet->setCellValue("B2","Tanggal");
        $sheet->setCellValue("C2","Tindakan");
        $sheet->setCellValue("D2","Harga");
        $sheet->setCellValue("E2","Nama Ruangan");
        $sheet->setCellValue("F2","Tindakan Keperawatan");
        $sheet->setCellValue("G2","Pemeriksaan Poli");
        $sheet->setCellValue("H2","Tindakan Medis Non Operatif Rawat Jalan");
        $sheet->setCellValue("I2","Total Jaspel");
        $sheet->getStyle ( "A1:I2")->getFont ()->setBold ( true );

        $column_num = 2;
        $cur_carabayar = "";
        $total = 0;
        foreach($uidata as $x){
                $column_num++;
                $sheet->setCellValue("A".$column_num,$x["No."]." ");
                $sheet->setCellValue("B".$column_num,$x["Tanggal"]);
                $sheet->setCellValue("C".$column_num,$x["Tindakan"]);
                $sheet->setCellValue("D".$column_num,$x["Harga"]=="0"?"":$x["Harga"]);
                $sheet->setCellValue("E".$column_num,$x["Nama Ruangan"]);
                $sheet->setCellValue("F".$column_num,$x["Tindakan Keperawatan"]=="0"?"":$x["Tindakan Keperawatan"]);
                $sheet->setCellValue("G".$column_num,$x["Pemeriksaan Poli"]=="0"?"":$x["Pemeriksaan Poli"]);
                $sheet->setCellValue("H".$column_num,$x["Tindakan Medis Non Operatif Rawat Jalan"]=="0"?"":$x["Tindakan Medis Non Operatif Rawat Jalan"]);
                $sheet->setCellValue("I".$column_num,$x["Total Jaspel"]=="0"?"":$x["Total Jaspel"]);
                
                $sheet->getStyle( 'D'.$column_num)->getNumberFormat()->setFormatCode('#,##0.00');
                $sheet->getStyle( 'F'.$column_num.":I".$column_num)->getNumberFormat()->setFormatCode('#,##0.00');
        }   
        $sheet->getStyle ( 'A1:I'.$column_num )->applyFromArray ($fillcabang);     
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="'.$this->fname.'.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );

		return;
    }

}