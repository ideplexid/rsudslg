<?php

class PendapatanPerRuangPerDokterResponder extends ServiceResponder{
    
    private $reset_key = array();
    private $excel_column = array();
    private $sum_col = array();
    private $col_total = "A";
    
    private $title = "";
    private $fname = "";

    public function setSummary($tname,$col){
        $this->col_total = $tname;
        $this->sum_col = $col;
        return $this;
    }

    public function setTitle($t){
        $this->title = $t;
        return $t;
    }

    public function setFileName($nm){
        $this->fname = $nm;
        return $this;
    }

    public function addResetMoney($column,$name,$dbname){
        $this->reset_key[$column] = array("name"=>$name,"key"=>$dbname);
        return $this;
    }

    public function addColumn($col,$name){
        $this->excel_column[$col]=$name;
        return $this;
    }


    public function excel(){
		global $user;
		loadLibrary("smis-libs-function-export");
        $this->addData("command","list");
        $this->getService()->setData($this->getData());
        $this->getService()->execute();

        $this->getService()->setMode(ServiceConsumer::$MULTIPLE_MODE);
        $d = $this->getService()->getContent();
       
        $result = array();
        foreach($d as $autonomous=>$entity){
            foreach($entity as $ename=>$e_content){
                $list = $e_content['data'];
                foreach($list as $x){
                    $x['smis_entity'] = $ename;
                    $result[] = $x;
                }
            }
        }
        //echo json_encode($result);
        //return;
        $this->getAdapter()->setNumber(0);
        $adapter = $this->getAdapter();
        foreach($this->reset_key as $x){
            $adapter ->add($x['name'],$x['key']);
        }
      
        $uidata = $this->getAdapter()->getContent($result);

        require_once "smis-libs-out/php-excel/PHPExcel.php";
		$file = new PHPExcel ();
		
		$fillcabang=array();
		$fillcabang['borders']=array();
		$fillcabang['borders']['allborders']=array();
		$fillcabang['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
		
		$file->setActiveSheetIndex ( 0 );
		$sheet = $file->getActiveSheet ( 0 );
        $sheet->setTitle ($this->title);
        $last_column = PHPExcel_Cell::stringFromColumnIndex(count($this->excel_column) - 1);

        $sheet->mergeCells("A1:".$last_column."1")->setCellValue ( "A" . 1, $this->title." - ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']));

        foreach($this->excel_column as $col=>$name){
            $sheet->setCellValue ( $col."2", $name);        
        }
        
        $index=2;
		$no=0;
        $sheet->getStyle ( "A1:".$last_column."2")->getFont ()->setBold ( true );
        
		foreach($uidata as $x){
			$no++;
            $index++;
            
            foreach($this->excel_column as $col=>$name){
                $sheet->setCellValue ( $col.$index, $x[$name]);
            }
        }
        //$index++;
        $sheet->setCellValue ( $this->col_total . $index, "Total" );
        foreach($this->sum_col as $colx){
            $sheet->setCellValue ( $colx . $index, "=sum(".$colx."3:".$colx.($index-1).")" );
        }
        $sheet->getStyle ( 'A2:'.$last_column.$index )->applyFromArray ( $fillcabang );
        $sheet->getStyle ( "A".$index.":L".$index)->getFont ()->setBold ( true );
        foreach($this->reset_key as $col=>$x){
            $sheet->getStyle($col.'3:'.$col.$index)->getNumberFormat()->setFormatCode('#,##0.00');
        }

		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename="'.$this->fname.'.xls"' );
		header ( 'Cache-Control: max-age=0' );
		$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
		$writer->save ( 'php://output' );

		return;
	}
}