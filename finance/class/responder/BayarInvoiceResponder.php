<?php 

class BayarInvoiceResponder extends DetailInvoiceResponder{
    public function save(){
        $result = parent::save();
        $dbtable=$this->updateTotalInvoince($_POST['id_invoice']);
        $this->notifyBayarAccounting($result['id'],$this->dbtable,"");
        return $result;
    }
    
    public function delete(){
        $result= parent::delete();
        $dbtable=$this->updateTotalInvoince($_POST['id_invoice']);
        $this->notifyBayarAccounting($_POST['id'],$this->dbtable,"del");
        return $result;
    }
    
    
    public function notifyBayarAccounting($id,DBTable $dbtable,$operation){
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "penerimaan_piutang";
        $data['id_data']    = $id;
        $data['entity']     = "finance";
        $data['service']    = "get_detail_accounting_bayar_invoice";
        $data['data']       = $id;
        $data['code']       = "FBIV-".$id;
        $data['operation']  = $operation;
        
        $bayar_invoice      = $dbtable->select($id);
        $data['tanggal']    = $bayar_invoice->tanggal_bayar;
        $data['uraian']     = "Pendapatan Invoice ".$this->kepada_invoice." Dengan Nomor ".$this->nomor_invoice;
        $data['nilai']      = $bayar_invoice->nilai;
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $this;
    }
}

?>