<?php 

class PembayaranUmumResponder extends DBResponder{
    
    public function postToArray(){
        $result=parent::postToArray();
        if($this->isSave()){
            $result["jt_bayar"] = $_POST['waktu'];
            $result["tgl_cair"] = $_POST['waktu'];
        }
        return $result;
    }
    
    public function save(){
        $result=parent::save();
        $this->synchronizeToAccounting($this->getDBTable()->get_db(),$result['id'],"");
        return $result;
    }
    
    public function delete(){
        $this->synchronizeToAccounting($this->getDBTable()->get_db(),$_POST['id'],"del");
        return parent::delete();
    }
    
    
    public function synchronizeToAccounting($db,$id,$is_del=""){
        require_once "smis-base/smis-include-service-consumer.php";
        
        $x=$this->dbtable->selectEventDel($id);
        $data=array();
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "bayar_umum";
        $data['id_data']    = $id;
        $data['entity']     = "finance";
        $data['service']    = "get_detail_accounting_bayar_umum";
        $data['data']       = $id;
        $data['code']       = "finance-bayar-umum-".$id;
        $data['operation']  = $is_del;
        $data['tanggal']    = $x->waktu;
        $data['uraian']     = "Pembayaran Umum untuk ".$x->keterangan;
        $data['nilai']      = $x->nilai;
        
        $serv=new ServiceConsumer($db,"push_notify_accounting",$data,"accounting");
        $serv->execute();
    }
    
}

?>