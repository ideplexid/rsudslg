<?php 

class InvoiceResponder extends DBResponder{
    
    public function postToArray(){
        $update=parent::postToArray();
        if($_POST['id']!="" || $_POST['id']!=0){
            $id=$_POST['id'];
            $query  = "SELECT sum(total) as total
                        FROM smis_fnc_invoice_detail WHERE id_invoice='".$id."' AND prop!='del' ";
            $nilai  = $this->getDBTable()->get_db()->get_var($query);
            
            /*mengambil data pelunasan*/
            $query  = "SELECT sum(nilai) as total
                        FROM smis_fnc_invoice_bayar WHERE id_invoice='".$id."' AND prop!='del' ";
            
            $lunas  = $this->getDBTable()->get_db()->get_var($query);
            $update['sub_total']        = $nilai;
            $update['diskon']           = $nilai*$update['diskon_persen']/100+$update['diskon_rupiah'];
            $update['dpp']              = $nilai-$update['diskon'];            
            $update['nilai_ppn']        = $update['dpp']*$array['pajak_ppn']/100;;
            $update['sub_total_pajak']  = $update['dpp']+$update['nilai_ppn'];
            $update['nilai_pph23']      = $update['dpp']*$update['pajak_pph23']/100;
            $update['nilai_pph22']      = $update['dpp']*$update['pajak_pph22']/100;
            $update['total']            = $update['sub_total_pajak']-$update['nilai_pph23']-$update['nilai_pph22'];
            $update['lunas']            = $lunas;
            $update['sisa']             = $update['total']-$lunas;
            
        }
        return $update;
    }
    
    public function save(){
        $result = parent::save();
        $this->notifyAccounting($_POST['id'],$this->dbtable,"");
        return $result;
    }
    
    public function delete(){
        $result= parent::delete();
        $this->notifyAccounting($_POST['id'],$this->dbtable,"del");
        return $result;
    }
    
    public function notifyAccounting($id_invoice,DBTable $dbtable,$operation){
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "piutang";
        $data['id_data']    = $id_invoice;
        $data['entity']     = "finance";
        $data['service']    = "get_detail_accounting_piutang_invoice";
        $data['data']       = $id_invoice;
        $data['code']       = "FPIV-".$id_invoice;
        $data['operation']  = $operation;
        
        $invoice            = $dbtable->select($id_invoice);
        $data['tanggal']    = $invoice->tanggal;
        $data['uraian']     = "Invoice ".$invoice->kepada." Dengan Nomor ".$invoice->nomor;
        $data['nilai']      = $invoice->total;
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $this;
    }
    
    public function excel_element(){
        $id      = $_POST['id'];
        $invoice = $this->select(array("id"=>$id));
        
        $dbtable=new DBTable($this->getDBTable()->get_db(),"smis_fnc_invoice_detail");
        $dbtable->addCustomKriteria(" id_invoice "," ='".$id."' ");
        $data = $dbtable->view("",0);
        $list=$data['data'];
        require_once ("smis-libs-out/php-excel/PHPExcel.php");
        
        $fillcabang = array ();
        $fillcabang['fill']=array();
        $fillcabang['fill']['type']=PHPExcel_Style_Fill::FILL_SOLID;
        $fillcabang['fill']['color']=array();
        $fillcabang['fill']['color']['rgb']='AFAFAF';
        
        $bottom['borders']=array();
        $bottom['borders']['bottom']=array();
        $bottom['borders']['bottom']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        
        $border['borders']=array();
        $border['borders']['allborders']=array();
        $border['borders']['allborders']['style']=PHPExcel_Style_Border::BORDER_THIN ;
        
        $center = array();
        $center ['alignment']=array();
        $center ['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        
        $topalign = array();
        $topalign ['alignment']=array();
        $topalign ['alignment']['vertical']=PHPExcel_Style_Alignment::VERTICAL_TOP;
        
        
        $file = new PHPExcel ();
        $sheet = $file->getActiveSheet ();
        
        /*header*/
        $sheet->mergeCells("A7:I7")->setCellValue("A7","INVOICE");
        $sheet->getStyle("A7")->getFont()->setBold(true);
        $sheet->getStyle("A7")->applyFromArray($center);
        $sheet->setCellValue("A9","Kepada : ");
        $sheet->getStyle("A9")->getFont()->setBold(true);
        $sheet->setCellValue("G9","No. Invoice : ");
        $sheet->mergeCells("H9:I9")->setCellValue("H9",$invoice->nomor);
        $sheet->mergeCells("A10:C10")->setCellValue("A10",$invoice->kepada);
        $sheet->mergeCells("A11:C12")->setCellValue("A11",$invoice->alamat);
        $sheet->setCellValue("G10","Tgl. Invoice : ");
        $sheet->mergeCells("H10:I10")->setCellValue("H10",ArrayAdapter::format("date d M Y",$invoice->tanggal));
        $sheet->setCellValue("G11","No. Ref : ");
        $sheet->mergeCells("H11:I11")->setCellValue("H11",$invoice->noref);
        $sheet->setCellValue("G12","Term : ");
        $sheet->mergeCells("H12:I12")->setCellValue("H12",$invoice->term);
        $sheet->setCellValue("G13","Tgl Jatuh Tempo : ");
        $sheet->mergeCells("H13:I13")->setCellValue("H13",ArrayAdapter::format("date d M Y",$invoice->jatuh_tempo));
        $sheet->setCellValue("G14","Klasifikasi : ");
        $sheet->mergeCells("H14:I14")->setCellValue("H14",$invoice->klasifikasi);
        
        /*content*/
        $sheet->setCellValue("A16","No.");
        $sheet->mergeCells("B16:C16")->setCellValue("B16","Deskripsi");
        $sheet->setCellValue("D16","Qty");
        $sheet->setCellValue("E16","Satuan");
        $sheet->setCellValue("F16","Harga IDR");
        $sheet->setCellValue("G16","Diskon");
        $sheet->setCellValue("H16","Pajak PPN");
        $sheet->setCellValue("I16","Jumlah");
        $sheet->getStyle("A16:I16")->getFont()->setBold(true);
        $sheet->getStyle("A16:I16")->applyFromArray($fillcabang);
        $sheet->getStyle("A16:I16")->applyFromArray($bottom);
        
        $position=17;
        $nomor=1;
        foreach($list as $x){
            $sheet->setCellValue("A".$position,$nomor.".");
            $sheet->mergeCells("B".$position.":C".$position)->setCellValue("B".$position,$x->deskripsi);
            $sheet->setCellValue("D".$position,$x->kuantitas);
            $sheet->setCellValue("E".$position,$x->satuan);
            $sheet->setCellValue("F".$position,$x->harga);
            $sheet->setCellValue("G".$position,$x->diskon."%");
            $sheet->setCellValue("H".$position,$x->pajak_ppn."%");
            $sheet->setCellValue("I".$position,"=F".$position."*D".$position."*(100%-G".$position.")");
            $nomor++;
            $position++;
        }
        
        loadLibrary("smis-libs-function-math");
        $terbilang=numbertell($invoice->sub_total_pajak);
        
        $start=$position;
        /*footer kiri*/
        $sheet->mergeCells("A".($position+1).":E".($position+3));
        $sheet->setCellValue("A".($position+1),"Terbilang : \n".$terbilang." Rupiah");
        $sheet->getStyle("A".($position+1).":E".($position+3))->applyFromArray($topalign);
        $sheet->getStyle("A".($position+1))->getAlignment()->setWrapText(true);
        
        $position+=3;
        $sheet->mergeCells("A".($position+1).":E".($position+4));
        $sheet->setCellValue("A".($position+1),"Catatan : \n ".$invoice->catatan);
        $sheet->getStyle("A".($position+1).":E".($position+4))->applyFromArray($border);
        $sheet->getStyle("A".($position+1).":E".($position+4))->applyFromArray($topalign);
        $sheet->getStyle("A".($position+1))->getAlignment()->setWrapText(true);
        
        $position+=6;
        $sheet->mergeCells("A".($position).":G".($position));
        $sheet->setCellValue("A".$position,"Saran Pembayaran");
        $position++;
        $sheet->mergeCells("A".$position.":G".($position+5));
        $sheet->setCellValue("A".$position,"Silakan melakukan pembayaran dengan transfer ke rekening berikut : \n".$invoice->saran);
        $sheet->getStyle("A".$position.":G".($position+5))->applyFromArray($topalign);        
        $sheet->getStyle("A".($position))->getAlignment()->setWrapText(true);
        
        $position+=6;
        $sheet->mergeCells("A".($position).":I".($position));
        $sheet->setCellValue("A".$position,"Terima Kasih Atas Kerjasamanya");
        $sheet->getStyle("A".$position)->applyFromArray($center);
        $sheet->getStyle("A".$position)->getFont()->setBold(true);
        
        
        /*footer kanan*/
        $position=$start;
        $sheet->setCellValue("I".($position),$invoice->sub_total);
        $sheet->setCellValue("I".(++$position),$invoice->diskon );
        $sheet->setCellValue("I".(++$position),"=I".($position-2)."-I".($position-1));
        $sheet->setCellValue("I".(++$position),$invoice->nilai_ppn);
        $sheet->setCellValue("I".(++$position),"=I".($position-1)."+I".($position-2));
        $sheet->setCellValue("I".(++$position),$invoice->nilai_pph22);
        $sheet->setCellValue("I".(++$position),$invoice->nilai_pph23);
        $sheet->setCellValue("I".(++$position),$invoice->lunas);
        $sheet->setCellValue("I".(++$position),"=I".($position-4)."-I".($position-2)."-I".($position-3)."-I".($position-1));
        
        $position=$start;
        $sheet->setCellValue("H".($position),"Sub Total");
        $sheet->setCellValue("H".(++$position),"Diskon");
        $sheet->setCellValue("H".(++$position),"DPP");
        $sheet->setCellValue("H".(++$position),"Pajak");
        $sheet->setCellValue("H".(++$position),"Total");
        $sheet->setCellValue("H".(++$position),"PPh 22 (".$invoice->pajak_pph22."%)");
        $sheet->setCellValue("H".(++$position),"PPh 23 (".$invoice->pajak_pph23."%)");
        $sheet->setCellValue("H".(++$position),"Terbayar");
        $sheet->setCellValue("H".(++$position),"Sisa");
        
        $sheet->getColumnDimension ( "A" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "B" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "C" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "D" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "E" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "F" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "G" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "H" )->setAutoSize ( true );
		$sheet->getColumnDimension ( "I" )->setAutoSize ( true );
        
        //$objPHPExcel->getActiveSheet()->getStyle("A1")->getNumberFor‌​mat()->setFormatCode‌​("#,##0.00");
        $sheet->getStyle("F17:F".$start)->getNumberFormat()->setFormatCode("#,##0.00");
        $sheet->getStyle("I17:I".$position)->getNumberFormat()->setFormatCode("#,##0.00");
        
        
        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Invoice.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );
        
    }
    
}

?>