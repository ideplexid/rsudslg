<?php 

class TerminDetailResponder extends DBResponder{
    private $total_uang;
    private $employee;
    private $nilai;
    
    public function save(){
        global $db;
        $result         = parent::save();  
        $this->employee = $_POST['nama_employee'];
        $this->nilai    = $_POST['dibayar'];        
        $dbtable        = $this->updateTotalTermin($_POST['id_termin']);
        $this->notifyAccounting($result['id'],$_POST['id_termin'],$dbtable,"");
        return $result;
    }
    
    public function delete(){
        $x=$this->dbtable->select($_POST['id']);
        $this->employee = $x->nama_employee;
        $this->nilai    = $x->dibayar;        
        $result         = $parent::delete();
        $dbtable        = $this->updateTotalTermin($_POST['id_termin']);
        $this->notifyAccounting($_POST['id'],$_POST['id_termin'],$dbtable,"del");
        return $result;
    }
    
    public function updateTotalTermin($id_termin){  
        $db                 = $this->getDBTable()->get_db();
        $query              = "SELECT sum(dibayar) as total FROM smis_fnc_termin_gaji_detail 
                               WHERE id_termin='".$id_termin."' AND prop!='del' ";
        $total              = $db->get_var($query);
        $id                 = array("id"=>$id_termin);
        $update             = array("total_uang"=>$total);
        $dbtable            = new DBTable($db,"smis_fnc_termin_gaji");
        $dbtable->update($update,$id);
        $this->total_uang   = $total;
        return $dbtable;
    }
    
    
    public function notifyAccounting($id,$id_termin,DBTable $dbtable,$operation){
        $data['jenis_akun'] = "transaction";
        $data['jenis_data'] = "bayar_gaji";
        $data['id_data']    = $id;
        $data['entity']     = "finance";
        $data['service']    = "get_detail_accounting_bayar_gaji";
        $data['data']       = $id;
        $data['code']       = "BSLR-".$id;
        $data['operation']  = $operation;
        
        $search=array("id"=>$id_termin);
        $result=$dbtable->select($search);
        $data['tanggal']    = $result->tanggal;
        $data['uraian']     = "Bayar Gaji Periode ".$result->periode." Atas Nama ".$this->employee;
        $data['nilai']      = $this->nilai;
        
        require_once "smis-base/smis-include-service-consumer.php";
        $serv=new ServiceConsumer($this->dbtable->get_db(),"push_notify_accounting",$data,"accounting");
        $serv->execute();
        return $this;
    }
    
}

?>