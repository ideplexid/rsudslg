<?php 

class ExcelRekapitulasi {
    private $nama;
    private $dari;
    private $sampai;
    private $pajak;
    private $nomor;
    private $column;
    public function  __construct($nama,$nomor,$dari,$sampai,$pajak,$column){
        $this->nama=$nama;
        $this->dari=$dari;
        $this->sampai=$sampai;
        $this->pajak=$pajak;
        $this->nomor = $nomor;
        $this->column=$column;
    }

    public function createExcel($uidata){
        require_once "smis-libs-out/php-excel/PHPExcel.php";
       
        global $user;
        $nomor = $this->nomor."-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
        $waktu  = ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
        $ft     = ArrayAdapter::format("date d M Y", $this->dari);
        $to     = ArrayAdapter::format("date d M Y", $this->sampai);	

        $fillcabang = array();
        $fillcabang['borders'] = array();
        $fillcabang['borders']['allborders'] = array();
        $fillcabang['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

        $last_column = PHPExcel_Cell::stringFromColumnIndex(count($this->column) - 1);
        $file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
        $sheet  ->setTitle ("Rekapitulasi");

        $sheet->mergeCells("A1:".$last_column."1")->setCellValue ( "A1", $this->nama );
        $sheet->mergeCells("D2:".$last_column."2");
        $sheet->mergeCells("D3:".$last_column."3");
        $sheet->mergeCells("B4:".$last_column."4");
        $sheet->setCellValue("A2","Nomor");
        $sheet->setCellValue("B2",$nomor);
        $sheet->setCellValue("C2","Waktu");
        $sheet->setCellValue("D2",$waktu);

        $sheet->setCellValue("A3","Dari");
        $sheet->setCellValue("B3",$ft);
        $sheet->setCellValue("C3","Sampai");
        $sheet->setCellValue("D3",$to);
        
        $sheet->setCellValue("A4","Pajak");
        $sheet->setCellValue("B4",$this->pajak."% ");

        foreach($this->column as $c=>$nm){
            $sheet->setCellValue($c."5",$nm);
            $sheet->getColumnDimension($c)->setAutoSize(true);
        }
        $row = 5;
        foreach($uidata as $x){
            $row++;
            foreach($this->column as $c=>$nm){
                $sheet->setCellValue($c.$row,$x[$nm]);
            }
        }
        $sheet->getStyle('B6:'.$last_column.$row)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle ( "A1:".$last_column."5")->getFont ()->setBold ( true );
        $sheet->getStyle ( "A".$row.":".$last_column.$row)->getFont ()->setBold ( true );
        $sheet->getStyle ( 'A1:'.$last_column.$row )->applyFromArray ( $fillcabang );
       

        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$this->nama.'.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );        
    }

}