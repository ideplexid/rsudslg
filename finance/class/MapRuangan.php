<?php 

class MapRuangan{

    private static $LIST_RUANGAN = null;
    public static function getRuangan(){
        self::getRealName("");
        $ruangan = array();
        foreach(self::$LIST_RUANGAN as $slug=>$name){
            $ruangan[] = array("name"=>$name,"value"=>$slug);
        }
        return $ruangan;
    }
    public static function getRealName($slug){
        if(self::$LIST_RUANGAN==null){
            global $db;
            require_once 'smis-base/smis-include-service-consumer.php';
            $urjip=new ServiceConsumer($db, "get_urjip",array());
            $urjip->setMode(ServiceConsumer::$MULTIPLE_MODE);
            $urjip->setCached(true,"get_urjip");
            $urjip->execute();
            $content=$urjip->getContent();
            $ruangan=array();
            foreach ($content as $autonomous=>$ruang){
                foreach($ruang as $nama_ruang=>$jip){
                        $ruangan[$nama_ruang]=isset($jip['name'])?$jip['name']:ArrayAdapter::format("unslug",$nama_ruang);
                }
            }
            self::$LIST_RUANGAN= $ruangan;
        }
        if(isset(self::$LIST_RUANGAN[$slug])){
            return self::$LIST_RUANGAN[$slug];
        }else{
            return ArrayAdapter::format("unslug",$slug);
        }
    }

}