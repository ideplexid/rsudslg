<?php 

class ExcelDetail {
    private $nama;
    private $dari;
    private $sampai;
    private $pajak;
    private $column;
    private $nilai_bilang;
    public function  __construct($nama,$dari,$sampai,$pajak,$column,$nilai_bilang){
        $this->nama=$nama;
        $this->dari=$dari;
        $this->sampai=$sampai;
        $this->pajak=$pajak;
        $this->column=$column;
        $this->nilai_bilang = $nilai_bilang;
    }

    public function createExcel($uidata){
        require_once "smis-libs-out/php-excel/PHPExcel.php";
       
        global $user;
        $nomor  = date("dmy-his")."-".substr(md5($user->getNameOnly()."-".$_POST['id_karyawan']), 5,rand(2, 5));
        $waktu  = ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
       

        $fillcabang = array();
        $fillcabang['borders'] = array();
        $fillcabang['borders']['allborders'] = array();
        $fillcabang['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

        $last_column = PHPExcel_Cell::stringFromColumnIndex(count($this->column) - 1);
        $file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
        $sheet  ->setTitle ("Rekapitulasi");

        $sheet->mergeCells("A1:".$last_column."1")->setCellValue ( "A1", $this->nama );
        
        $sheet->setCellValue("A2","Nomor");
        $sheet->mergeCells("B2:F2")->setCellValue("B2",$nomor);
        $sheet->setCellValue("G2","Waktu");
        $sheet->mergeCells("H2:".$last_column."2")->setCellValue("H2",$waktu);

        $sheet->setCellValue("A3","Nama");
        $sheet->mergeCells("B3:C3")->setCellValue("B3",$_POST['nama_karyawan']);
        $sheet->setCellValue("D3","Jabatan");
        $sheet->mergeCells("E3:F3")->setCellValue("E3",$_POST['jabatan_karyawan']);
        $sheet->setCellValue("G3","NIP");
        $sheet->mergeCells("H3:".$last_column."3")->setCellValue("H3",$_POST['nip_karyawan']);

        $sheet->setCellValue("A4","Dari");
        $sheet->mergeCells("B4:C4")->setCellValue("B4",ArrayAdapter::format("date d M Y",$this->dari));
        $sheet->setCellValue("D4","Sampai");
        $sheet->mergeCells("E4:F4")->setCellValue("E4",ArrayAdapter::format("date d M Y",$this->sampai));
        $sheet->setCellValue("G4","Pajak");
        $sheet->mergeCells("H4:".$last_column."4")->setCellValue("H4",$_POST['pajak']."%");

        foreach($this->column as $c=>$nm){
            $sheet->setCellValue($c."5",$nm);
            $sheet->getColumnDimension($c)->setAutoSize(true);
        }
        $row = 5;
        foreach($uidata as $x){
            $row++;
            foreach($this->column as $c=>$nm){
                $sheet->setCellValue($c.$row,$x[$nm]);
            }
        }
        $sheet->getStyle('F6:'.$last_column.$row)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle ( "A1:".$last_column."5")->getFont ()->setBold ( true );
        $sheet->getStyle ( "A".$row.":".$last_column.$row)->getFont ()->setBold ( true );
        $sheet->getStyle ( 'A1:'.$last_column.$row )->applyFromArray ( $fillcabang );

        $row += 2;
        $terbilang = numbertell($this->nilai_bilang)." Rupiah";
        $sheet->setCellValue("A".$row,"Terbilang");
        $sheet->mergeCells("B".$row.":".$last_column.$row)->setCellValue("B".$row,$terbilang);
        $row++;
        $sheet->mergeCells("A".$row.":E".$row)->setCellValue("A".$row,"Penerima");
        $sheet->mergeCells("F".$row.":".$last_column.$row)->setCellValue("F".$row,"Pencetak");

        $row +=4;
        global $user;
        $sheet->mergeCells("A".$row.":E".$row)->setCellValue("A".$row,"(".$_POST['nama_karyawan'].")");
        $sheet->mergeCells("F".$row.":".$last_column.$row)->setCellValue("F".$row,"(".$user->getNameOnly().")");
        $sheet->getStyle ( "A".$row.":".$last_column.$row)->getFont ()->setBold ( true );

        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="'.$this->nama.'.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );        
    }

}