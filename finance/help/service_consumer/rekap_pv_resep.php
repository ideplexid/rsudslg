<?php 

/**
 * 
 * this manual provided service manual for salary.
 * data that needed is look like these
 * 
 * [
 *   {
 *       "id_karyawan": "1 (ID dari Dokter)",
 *       "karyawan": "Azriel Azis Sp.d",
 *       "total": "15000000",
 *       "bagian_rs": "14000000",
 *       "bagian_karyawan": 1000000,
 *   },
 *   {
 *       "id_karyawan": "2 (ID dari Dokter)",
 *       "karyawan": "Nasukhan",
 *       "total": "20000000",
 *       "bagian_rs": "18000000",
 *       "bagian_karyawan": 1800000,
 *   }
 *  ]
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 * 
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$provided=array();
$provided['from']="01-11-2014 (start date need to search)";
$provided['to']="01-12-2014 (end date need to search)";

$needed=array();
$needed[0]=array();
$needed[0]['id_karyawan']="1 (string the karyawan id) ";
$needed[0]['karyawan']="Azriel Azis (name of room) ";
$needed[0]['total']="15000000 (int, sum of what his sell) ";
$needed[0]['bagian_rs']="14000000 (int, sum what rs get) ";
$needed[0]['bagian_karyawan']="1000000 (int, sum what employee get) ";

$needed[1]=array();
$needed[1]['id_karyawan']="1 (string the karyawan id) ";
$needed[1]['karyawan']="Azriel Azis (name of room) ";
$needed[1]['total']="15000000 (int, sum of what his sell) ";
$needed[1]['bagian_rs']="14000000 (int, sum what rs get) ";
$needed[1]['bagian_karyawan']="1000000 (int, sum what employee get) ";


$descrption="
this service searching to the entire system to collect recapitulation
of prescription recipe from doctor, this contains the summary of provit 
share that produce by doctor in from <= X < to";


$author="
* @version 1.0
* @since 7 Apr 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("rekap_pv_resep",ManualService::$RESPOND_DEFAULT);
$man->setAuthor($author);
$man->setDescription($descrption);
$man->setDataProvided($provided);
$man->setDataNeeded($needed);

echo $man->getHtml();



?>