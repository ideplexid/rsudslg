<?php 

/**
 * 
 * this manual provided service manual for get_provit_share.
 * data that needed is look like these
 * 
 *	[
 * 	    	{
 *		        "waktu": "20 Nopember 2014",
 *		        "ruangan": "anggrek",
 *		        "sebagai": "Tindakan Perawat ",
 *		        "percentage": "51%",
 *		        "nilai": 5865,
 *		        "asli": "11500",
 *		        "pasien": "000011 : Kunyem",
 *		        "keterangan": "Pasang NGT KELAS III",
 *		        "karyawan": "Sri Wahyuni"
 *	    	}
 *	] 
 * 
 * @version 1.0
 * @since 17 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 * 
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$provided=array();
$provided['ruangan']="anggrek (the id of the employee provided by hrd)";
$provided['from']="01-11-2014 (start date need to search)";
$provided['to']="01-12-2014 (end date need to search)";

$needed=array();
$needed[0]=array();
$needed[0]['waktu']="26 Oktober 2014 (string, use ArrayAdapter to format) ";
$needed[0]['ruangan']="Apotek (name of room) ";
$needed[0]['sebagai']="Embalase Resep (string, participation) ";
$needed[0]['percentage']="20% (string, of percentage) ";
$needed[0]['asli']="100000 (int, of the value real price) ";
$needed[0]['nilai']="20000 (real price x percentage) ";
$needed[0]['pasien']="Nurul Huda S.Kom : 1110 (the pasien nrm and register number) ";
$needed[0]['keterangan']="Resep No. 111, Bodrex 1000 x 3 (string descrption, all you want) ";
$needed[0]['karyawan']="Eka Safitri Amd.Keb (the current karyawan name) ";

$needed[1]=array();
$needed[1]['waktu']="27 Oktober 2014 (string, use ArrayAdapter to format) ";
$needed[1]['ruangan']="Apotek (name of room) ";
$needed[1]['sebagai']="Embalase Resep (string, participation) ";
$needed[1]['percentage']="20% (string, of percentage) ";
$needed[1]['asli']="3000 (int, of the value real price) ";
$needed[1]['nilai']="600 (real price x percentage) ";
$needed[1]['pasien']="Nurul Huda S.Kom : 1110 (the pasien nrm and register number) ";
$needed[1]['keterangan']="Resep No. 111, Bodrex 1000 x 3 + Panadol 2 x 20000 + Paramex 1 x 3000 ";
$needed[1]['karyawan']="Eka Safitri Amd.Keb (the current karyawan name) ";


$descrption="
this service searching to the entire system to collect salary based on 
provit share beetween room group and the company. 
this service has only one goal,finding the the detail about 
the provit share of the room, beetween two range time (from and to).
let say that the time range is X, 
the implementator must find detail of salary in from <= X < to.
this service would be spesific in one entity (anggrek only, laboratory only, etc)";

$author="
* @version 1.0
* @since 17 Nov 2014
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_provit_share",ManualService::$RESPOND_DEFAULT);
$man->setAuthor($author);
$man->setDescription($descrption);
$man->setDataProvided($provided);
$man->setDataNeeded($needed);

echo $man->getHtml();



?>