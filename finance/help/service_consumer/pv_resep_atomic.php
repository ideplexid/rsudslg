<?php 

/**
 * 
 * this manual provided service manual for get_provit_share.
 * data that needed is look like these
 * 
 *	[
 * 	    	{
 *		        "total": "5",
 *		        "return": "3",
 *		        "sell": "2",
 *				"lreturn":["1","2","3"],
 *				"lsell":["1","2"]
 *	    	}
 *	] 
 * 
 * @version 1.0
 * @since 17 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 * 
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$provided=array();
$provided['id']="1 (the recipe id)";
$provided['retsell']="ret (if ret then it's id return, if sell then it's id recipe)";

$needed=array();
$needed["id_dokter"]="1  (ID of Doctor) ";
$needed["nama_dokter"]="Jono (Doctor Name) ";
$needed["no_resep"]="R0090 (Recipe Number) ";
$needed["harga"]="1500000 (should be minus in Return)";
$needed["bagian_dokter"]="10000 (should be minus in Return)";
$needed["waktu"]="2014-12-12 23:59:59";

$descrption="
	this service provide a single data of recipe detail
";

$author="
* @version 1.0
* @since 28 May 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("pv_resep_atomic",ManualService::$RESPOND_DEFAULT);
$man->setAuthor($author);
$man->setDescription($descrption);
$man->setDataProvided($provided);
$man->setDataNeeded($needed);

echo $man->getHtml();



?>