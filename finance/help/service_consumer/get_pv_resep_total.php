<?php 

/**
 * 
 * this manual provided service manual for get_provit_share.
 * data that needed is look like these
 * 
 *	[
 * 	    	{
 *		        "total": "5",
 *		        "return": "3",
 *		        "sell": "2",
 *				"lreturn":["1","2","3"],
 *				"lsell":["1","2"]
 *	    	}
 *	] 
 * 
 * @version 1.0
 * @since 17 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 * 
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$provided=array();
$provided['from']="01-11-2014 (start date need to search)";
$provided['to']="01-12-2014 (end date need to search)";
$provided['id_dokter']="1 (end date need to search)";

$needed=array();
$needed["total"]="5  (string, use ArrayAdapter to format) ";
$needed["return"]="3 (Recipe total that Return) ";
$needed["sell"]="2 (Recipe total that Sold) ";
$needed["lreturn"]=array();
$needed["lreturn"][]=1;
$needed["lreturn"][]=2;
$needed["lreturn"][]=3;
$needed["lsell"]=array();
$needed["lsell"][]=1;
$needed["lsell"][]=2;

$descrption="
	this service should provide the total data of recipe that sold and return
	data that provide 
";

$author="
* @version 1.0
* @since 28 May 2015
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_pv_resep_total",ManualService::$RESPOND_DEFAULT);
$man->setAuthor($author);
$man->setDescription($descrption);
$man->setDataProvided($provided);
$man->setDataNeeded($needed);

echo $man->getHtml();



?>