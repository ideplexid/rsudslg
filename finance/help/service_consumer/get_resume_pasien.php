<?php 

/**
 * 
 * this provided manual for get_tagihan service
 * 
 * @version 1.0
 * @since 16 Nov 2014
 * @author goblooge
 * @license LGPL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * 
 */

require_once 'smis-libs-class/ManualService.php';

$response=array();																								// the data that needed to
$response[0]=array();
$response[0]["layanan"]="penjualan_resep";																				// tindakan_dokter can be change as you want to, it should be a variable
$response[0]["ruangan"]="apotek_rawat_inap";
$response[0]["nilai"]="10000000";

$response=array();																								// the data that needed to
$response[1]=array();
$response[1]["layanan"]="return_resep";																				// tindakan_dokter can be change as you want to, it should be a variable
$response[1]["ruangan"]="apotek_rawat_inap";
$response[1]["nilai"]="100000";


$provided=array();
$provided['noreg_pasien']="1";

$descrption="
This Service Consumer have several goal :
1. Provide Summary of Bill for patient
";

$author="
* @version 1.0
* @since 1 Jan 2016
* @author goblooge
* @license LGPL v3
* @copyright Nurul Huda &#60; goblooge@gmail.com &#62;
";

$man=new ManualService("get_resume_pasien");
$man->setAuthor($author);
$man->setDataNeeded($response);
$man->setDataProvided($provided);
$man->setDescription($descrption);

echo $man->getHtml();



?>