<?php
    global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	$inventory_policy = new InventoryPolicy("finance", $user,"modul/");
	
	$policy = new Policy("finance", $user);
	$policy	->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT)
			->addPolicy("settings","settings",Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings")
			->addPolicy("payment","payment",Policy::$DEFAULT_COOKIE_CHANGE,"modul/payment")
			->addPolicy("salary", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/salary")
			->addPolicy("jasa_ruang", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/jasa_ruang")
			->addPolicy("faktur", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/faktur")
			->addPolicy("pembayaran", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/pembayaran")
			->addPolicy("termin", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/termin")
			->addPolicy("termin_detail", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/termin_detail")
			->addPolicy("bulk_faktur", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/bulk_faktur")
			->addPolicy("bulk_faktur_detail", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/bulk_faktur_detail")
			->addPolicy("bulk_umum", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/bulk_umum")
			->addPolicy("bulk_umum_detail", "payment", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/payment/bulk_umum_detail")
						
            ->addPolicy("mapping_akunting","mapping_akunting",Policy::$DEFAULT_COOKIE_CHANGE,"modul/mapping_akunting")
			->addPolicy("setup", "mapping_akunting", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/mapping_akunting/setup")
			->addPolicy("map_pembayaran_umum", "mapping_akunting", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/mapping_akunting/map_pembayaran_umum")
			->addPolicy("mapping_karyawan", "mapping_akunting", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/mapping_akunting/mapping_karyawan")
			->addPolicy("mapping_vendor", "mapping_akunting", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/mapping_akunting/mapping_vendor")
			->addPolicy("kode_akun", "mapping_akunting", Policy::$DEFAULT_COOKIE_KEEP,"snippet/kode_akun")
	
            ->addPolicy("invoice","invoice",Policy::$DEFAULT_COOKIE_CHANGE,"modul/invoice")
			->addPolicy("pembuatan_invoice", "invoice", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/invoice/pembuatan_invoice")
			->addPolicy("detail_invoice", "invoice", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/invoice/detail_invoice")
			->addPolicy("bayar_invoice", "invoice", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/invoice/bayar_invoice")
			
			->addPolicy("laporan_jaspel_dokter","laporan_jaspel_dokter",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_jaspel_dokter")
			->addPolicy("laporan_jaspel_tutup_tagihan","laporan_jaspel_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_jaspel_dokter/laporan_jaspel_tutup_tagihan")
			->addPolicy("laporan_jaspel_buka_tagihan","laporan_jaspel_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_jaspel_dokter/laporan_jaspel_buka_tagihan")
			->addPolicy("laporan_jaspel_tutup_tagihan_perawat","laporan_jaspel_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_jaspel_dokter/laporan_jaspel_tutup_tagihan_perawat")
			->addPolicy("laporan_jaspel_buka_tagihan_perawat","laporan_jaspel_dokter",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_jaspel_dokter/laporan_jaspel_buka_tagihan_perawat")
			
            ->addPolicy("data_induk","data_induk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/data_induk")
			->addPolicy("kas", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/kas")
			->addPolicy("beban", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/beban")
			->addPolicy("akun_kas_bank", "data_induk", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/data_induk/akun_kas_bank")
			
            ->addPolicy("laporan","laporan",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan")
			->addPolicy("laporan_hutang", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_hutang")
			->addPolicy("laporan_pengeluaran", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_pengeluaran")
			->addPolicy("laporan_diskon", "laporan", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan/laporan_diskon")
			
            ->addPolicy("laporan_uang_masuk","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan_uang_masuk")
			->addPolicy("lum_alok","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lum_alok")
			->addPolicy("lum_perawat","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lum_perawat")
			->addPolicy("lum_igd","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lum_igd")
			->addPolicy("lum_dokter","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lum_dokter")
            ->addPolicy("lum_ok","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lum_ok")
            ->addPolicy("lumpj_alok","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lumpj_alok")
			->addPolicy("lumpj_perawat","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lumpj_perawat")
			->addPolicy("lumpj_igd","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lumpj_igd")
			->addPolicy("lumpj_dokter","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lumpj_dokter")
			->addPolicy("lumpj_ok","laporan_uang_masuk",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/laporan_uang_masuk/lumpj_ok")
			
            
            ->addPolicy("piutang","piutang",Policy::$DEFAULT_COOKIE_CHANGE,"modul/piutang")
			->addPolicy("piutang_sekarang", "piutang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/piutang/piutang_sekarang")
			->addPolicy("piutang_cashbase", "piutang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/piutang/piutang_cashbase")
			->addPolicy("piutang_cashbase_per_ruang", "piutang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/piutang/piutang_cashbase_per_ruang")
			->addPolicy("piutang_acruelbase", "piutang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/piutang/piutang_acruelbase")
			->addPolicy("piutang_satu_pasien", "piutang", Policy::$DEFAULT_COOKIE_KEEP,"resource/php/piutang/piutang_satu_pasien")
			
            ->addPolicy("provit_share","provit_share",Policy::$DEFAULT_COOKIE_CHANGE,"modul/provit_share")
			
            ->addPolicy("pv_tindakan_keperawatan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_tindakan_keperawatan")
			->addPolicy("pv_tindakan_igd","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_tindakan_igd")
			->addPolicy("pv_sub_karyawan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_sub_karyawan")
			->addPolicy("pv_sub_cair","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_sub_cair")
			->addPolicy("pv_tindakan_keperawatan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_tindakan_keperawatan")
			->addPolicy("pv_tindakan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_tindakan")
			->addPolicy("pv_konsul","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_konsul")
			->addPolicy("pv_periksa","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_periksa")
			->addPolicy("pv_resep","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_resep")
			->addPolicy("pv_visite","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_visite")
			->addPolicy("pv_ekg","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_ekg")
			->addPolicy("pv_endoscopy","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_endoscopy")
			->addPolicy("pv_bronchoscopy","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_bronchoscopy")
			->addPolicy("pv_audiometry","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_audiometry")
			->addPolicy("pv_spirometry","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_spirometry")
			->addPolicy("pv_faalparu","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_faalparu")
			->addPolicy("pv_laboratory","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_laboratory")
			->addPolicy("pv_radiology","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pv/pv_radiology")
			
            ->addPolicy("rekap_pv_tindakan_keperawatan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_tindakan_keperawatan")
			->addPolicy("rekap_pv_tindakan_igd","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_tindakan_igd")
			->addPolicy("rekap_pv_tindakan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_tindakan")
			->addPolicy("rekap_pv_resep","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_resep")
			->addPolicy("rekap_pv_periksa","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_periksa")
			->addPolicy("rekap_pv_visite","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_visite")
			->addPolicy("rekap_pv_konsul","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_konsul")
			->addPolicy("rekap_pv_ekg","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_ekg")
			->addPolicy("rekap_pv_endoscopy","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_endoscopy")
			->addPolicy("rekap_pv_bronchoscopy","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_bronchoscopy")
			->addPolicy("rekap_pv_audiometry","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_audiometry")
			->addPolicy("rekap_pv_spirometry","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_spirometry")
			->addPolicy("rekap_pv_faalparu","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_faalparu")
			->addPolicy("rekap_pv_laboratory","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_laboratory")
			->addPolicy("rekap_pv_radiology","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/rekap_pv/rekap_pv_radiology")
			
            ->addPolicy("pendapatan_laboratory","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_laboratory")
			->addPolicy("pendapatan_radiology","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_radiology")
			->addPolicy("pendapatan_fisiotherapy","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_fisiotherapy")
			->addPolicy("pendapatan_periksa","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_periksa")
			->addPolicy("pendapatan_konsul","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_konsul")
			->addPolicy("pendapatan_visite","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_visite")
			->addPolicy("pendapatan_tindakan_dokter","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_tindakan_dokter")
			->addPolicy("pendapatan_tindakan_perawat","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_tindakan_perawat")
			->addPolicy("pendapatan_tindakan_perawat_igd","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/pendapatan_tindakan_perawat_igd")
			->addPolicy("item_ok","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan/item_ok")
			
			->addPolicy("jenis_tindakan_keperawatan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_tindakan_keperawatan")
			->addPolicy("jenis_tindakan","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_tindakan")
			->addPolicy("jenis_resep","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_resep")
			->addPolicy("jenis_periksa","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_periksa")
			->addPolicy("jenis_visite","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_visite")
			->addPolicy("jenis_konsul","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_konsul")
			->addPolicy("jenis_ekg","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_ekg")
			->addPolicy("jenis_endoscopy","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_endoscopy")
			->addPolicy("jenis_bronchoscopy","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_bronchoscopy")
			->addPolicy("jenis_audiometry","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_audiometry")
			->addPolicy("jenis_spirometry","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_spirometry")
			->addPolicy("jenis_faalparu","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_faalparu")
			->addPolicy("jenis_laboratory","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_laboratory")
			->addPolicy("jenis_radiology","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/jenis/jenis_radiology")
			
            ->addPolicy("pendapatan_oksigen_central","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan_peruang/pendapatan_oksigen_central")
			->addPolicy("pendapatan_oksigen_manual","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan_peruang/pendapatan_oksigen_manual")
            ->addPolicy("pendapatan_kamar_mayat","provit_share",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/provit_share/pendapatan_peruang/pendapatan_kamar_mayat")
            ->combinePolicy($inventory_policy);
	
    $dbtable = new DBTable($db,"smis_fnc_kas");
    $dbtable ->setShowAll(true);
    $data    = $dbtable->view("",0);
    $list    = $data['data'];
    foreach($list as $x){
        $policy->addPolicy("pembayaran_dompet_".$x->id,"pembayaran_dompet_".$x->id,Policy::$DEFAULT_COOKIE_CHANGE,"modul/pembayaran_dompet");
    }
    
    $dbtable = new DBTable($db,"smis_fnc_akun");
    $dbtable ->setShowAll(true);
    $data    = $dbtable ->view("",0);
    $list    = $data["data"];
    $policy  ->addPolicy("kas_bank","kas_bank",Policy::$DEFAULT_COOKIE_CHANGE,"modul/kas_bank");		
    $policy  ->addPolicy("get_last_saldo","kas_bank",Policy::$DEFAULT_COOKIE_KEEP,"snippet/get_last_saldo");		
    foreach($list as $x){
        $policy ->addPolicy("kas_bank_".$x->id,"kas_bank",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/kas_bank/kas_bank");
        $policy ->addPolicy("detail_kas_bank_".$x->id,"kas_bank",Policy::$DEFAULT_COOKIE_KEEP,"resource/php/kas_bank/detail_kas_bank");
    }
    
    $policy->initialize();
?>
