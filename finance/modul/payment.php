<?php
$tabs = new Tabulator ( "payment", "" );
if(getSettings($db,"finance-payment-bulk-faktur","1")=="1")
    $tabs ->add ( "bulk_faktur", "Pembayaran Faktur Gerombolan", "", Tabulator::$TYPE_HTML ," fa fa-ticket");

if(getSettings($db,"finance-payment-bulk-umum","1")=="1")
    $tabs ->add ( "bulk_umum", "Pembayaran Umum Gerombolan", "", Tabulator::$TYPE_HTML ," fa fa-money");

if(getSettings($db,"finance-payment-faktur","1")=="1")
    $tabs ->add ( "faktur", "Pembayaran Faktur", "", Tabulator::$TYPE_HTML ," fa fa-ticket");

if(getSettings($db,"finance-payment-umum","1")=="1")
    $tabs ->add ( "pembayaran", "Pembayaran Umum", "", Tabulator::$TYPE_HTML ," fa fa-money");

if(getSettings($db,"finance-payment-termin","1")=="1")
    $tabs ->add ( "termin", "Pembayaran Gaji", "", Tabulator::$TYPE_HTML ," fa fa-money");
/*
if(getSettings($db,"finance-payment-salary","1")=="1")
    $tabs ->add ( "salary", "Bonus Dokter", "", Tabulator::$TYPE_HTML," fa fa-user-md" );

if(getSettings($db,"finance-payment-jasa","1")=="1")
    $tabs ->add ( "jasa_ruang", "Jasa Pelayanan Ruangan", "", Tabulator::$TYPE_HTML,"fa fa-eercast");
*/
$tabs->setPartialLoad(true,"finance","payment","payment",true);
echo  $tabs->getHtml ();
?>