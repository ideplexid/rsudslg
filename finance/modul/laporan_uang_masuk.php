<?php
	$tab = new Tabulator("laporan_uang_masuk","Laporan Uang Masuk",Tabulator::$LANDSCAPE_RIGHT);
	$tab->add ("lum_perawat","Detail Tindakan Perawat","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$tab->add ("lum_igd","Detail Tindakan Perawat IGD","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$tab->add ("lum_dokter","Detail Tindakan Dokter","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$tab->add ("lum_ok","Detail Operasi","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$tab->add ("lum_alok","Detail Alat & Obat","",Tabulator::$TYPE_HTML," fa fa-stethoscope" );
	
    $tab->add ("lumpj_perawat","Grup Tindakan Perawat","",Tabulator::$TYPE_HTML," fa fa-list" );
	$tab->add ("lumpj_igd","Grup Tindakan IGD","",Tabulator::$TYPE_HTML," fa fa-list" );
	$tab->add ("lumpj_dokter","Grup Tindakan Dokter","",Tabulator::$TYPE_HTML," fa fa-list" );
	$tab->add ("lumpj_ok","Grup Operasi","",Tabulator::$TYPE_HTML," fa fa-list" );
	$tab->add ("lumpj_alok","Grup Alat & Obat","",Tabulator::$TYPE_HTML," fa fa-list" );
	
    $tab->setPartialLoad(true,"finance","laporan_uang_masuk","laporan_uang_masuk",true);
    echo $tab->getHtml();
?>