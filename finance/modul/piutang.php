<?php 
    $tabs=new Tabulator("piutang", "");
    $tabs->add("piutang_cashbase_per_ruang", "Lap. Per Ruang", "",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("piutang_sekarang", "Lap. Sekarang", "",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("piutang_cashbase", "Lap. Pulang", "",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("piutang_acruelbase", "Lap. Masuk", "",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("piutang_satu_pasien", "Lap. Pasien", "",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->setPartialLoad(true,"finance","piutang","piutang",false);
    echo $tabs->getHtml();
?>