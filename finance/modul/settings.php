<?php
require_once 'smis-framework/smis/api/SettingsBuilder.php';
global $db;
setChangeCookie ( false );
changeCookie ();
$settings = new SettingsBuilder ( $db, "finance", "finance", "settings", "Settings Keuangan" );
$settings->setShowDescription ( true );
$settings->addTabs ( "profile", "Profile"," fa fa-id-card" );
if($settings->isGroup("profile")){
    $settings->addItem ( "profile", new SettingsItem ( $db, "finance-nama-direktur", "Nama Direktur", "", "text", "Nama Direktur " ) );
    $settings->addItem ( "profile", new SettingsItem ( $db, "finance-nip-direktur", "NIP Direktur", "", "text", "NIP Direktur " ) );
    $settings->addItem ( "profile", new SettingsItem ( $db, "finance-company-name", "Nama Perusahaan", "", "text", "Nama Perusahaan" ) );
    $settings->addItem ( "profile", new SettingsItem ( $db, "finance-company-address", "Alamat Perusahaan", "", "text", "Alamat Perusahaan" ) );
    $settings->addItem ( "profile", new SettingsItem ( $db, "finance-company-town", "Kota Perusahaan", "", "text", "Kota Perusahaan" ) );
    $settings->addItem ( "profile", new SettingsItem ( $db, "finance-gap-print", "Gap Cetakan", "", "text", "Gap Cetakan" ) );    
}

$settings->addTabs ( "payment", "Pembayaran","fa fa-money" );
if($settings->isGroup("payment")){
    $settings->addItem ( "payment", new SettingsItem ( $db, "finance-payment-bulk-faktur", "Pembayaran Gerombolan Faktur", "1", "checkbox", "" ) );
    $settings->addItem ( "payment", new SettingsItem ( $db, "finance-payment-bulk-umum", "Pembayaran Gerombolan Umum", "1", "checkbox", "" ) );
    $settings->addItem ( "payment", new SettingsItem ( $db, "finance-payment-faktur", "Pembayaran Faktur", "1", "checkbox", "" ) );
    $settings->addItem ( "payment", new SettingsItem ( $db, "finance-payment-umum", "Pembayaran Umum", "1", "checkbox", "" ) );
    $settings->addItem ( "payment", new SettingsItem ( $db, "finance-payment-termin", "Pembayaran Periode Gaji", "1", "checkbox", "" ) );
    //$settings->addItem ( "payment", new SettingsItem ( $db, "finance-payment-salary", "Pembayaran Bonus Dokter", "1", "checkbox", "" ) );
    //$settings->addItem ( "payment", new SettingsItem ( $db, "finance-payment-jasa", "Pembayaran Jasa Dokter", "1", "checkbox", "" ) );    
}

$settings->addTabs ( "print", "Print","fa fa-print" );
if($settings->isGroup("print")){
    $settings->addItem ( "print", new SettingsItem ( $db, "finance-css-print-pembayaran-umum", "Cetak Pembayaran Umum", "", "textarea", "CSS Pembayaran Umum, gunakan class .pbu_logo, .pbu_name, .pbu_title, .pbu_address, .pbu_noref, .pbu_tanggal, .pbu_penerima, .pbu_jumlah, .pbu_terbilang, .pbu_ket, .pbu_footer" ) );

}

$settings->setPartialLoad(true);
$settings->init ();
?>