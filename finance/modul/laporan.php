<?php

	$tab = new Tabulator("laporan","");
	$tab->add ("laporan_hutang","Laporan Hutang","",Tabulator::$TYPE_HTML," fa fa-ticket" );
	$tab->add ("laporan_pengeluaran","Laporan Pengeluaran","",Tabulator::$TYPE_HTML," fa fa-money" );
	$tab->add ("laporan_diskon","Laporan Diskon","",Tabulator::$TYPE_HTML ," fa fa-scissors");
	$tab->setPartialLoad(true,"finance","laporan","laporan",true);
    echo $tab->getHtml();

?>