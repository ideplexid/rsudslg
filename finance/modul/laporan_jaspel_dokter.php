<?php
	$tab = new Tabulator("laporan_jaspel_dokter","Laporan Jaspel Dokter",Tabulator::$LANDSCAPE);	
	
	$dokter = new Tabulator("dokter","Dokter",Tabulator::$POTRAIT);
	$dokter->add ("laporan_jaspel_tutup_tagihan","Laporan Jasa Pelayanan Tutup Tagihan","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$dokter->add ("laporan_jaspel_buka_tagihan","Laporan Jasa Pelayanan Buka Tagihan","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$dokter->setPartialLoad(true,"finance","dokter","dokter",true);
	
	$klinik = new Tabulator("poliklinik","Poliklinik",Tabulator::$POTRAIT);
	$klinik->add ("laporan_jaspel_tutup_tagihan_perawat","Laporan Jasa Pelayanan Tutup Tagihan","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$klinik->add ("laporan_jaspel_buka_tagihan_perawat","Laporan Jasa Pelayanan Buka Tagihan","",Tabulator::$TYPE_HTML," fa fa-user-md" );
	$klinik->setPartialLoad(true,"finance","poliklinik","poliklinik",true);
	
	$tab ->add("dokter","Dokter",$dokter->getHtml(),Tabulator::$TYPE_HTML," fa fa-user-md");
	$tab ->add("klinik","Poliklinik",$klinik->getHtml(),Tabulator::$TYPE_HTML," fa fa-user-md");

    echo $tab->getHtml();
?>