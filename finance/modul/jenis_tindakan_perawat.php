<?php 

	require_once 'smis-base/smis-include-service-consumer.php';
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	require_once 'finance/class/adapter/PerawatProfitShareAdapter.php';
	global $db;
	global $user;
	
	/* super command untuk memanggil karyawan */
	if (isset($_POST['super_command']) && $_POST['super_command'] != "") {
		require_once "finance/snippet/karyawan.php";
		$res=get_karyawan("jenis_tindakan_perawat","perawat");
		if($res!=null){
			echo $res;
			return;
		}
	}

	$serv=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
	$serv->execute();
	$con=$serv->getContent();	
	$select=new Select("pvtk_carabayar","pvtk_carabayar",$con);
	$form->addElement("Carabayar", $select);
	
		
	$table = new Table(array("Tanggal", "Ruangan", "Pasien", "Tindakan", "Biaya Awal", "Persentase", "Bagian Karyawan", "Bagian RS", "Pajak", "Dana Pengembangan"),"",null,true);
	$table->setName("pvtk_table");
	$table->setAction(false);
	$table->setHeaderVisible(false);
	$table->setFooterVisible(false);
	
	
?>


<?php 


if(isset($_POST['super_command']) && $_POST['super_command']=="total") {
	require_once 'smis-libs-class/ServiceProviderList.php';
	$dbtable=new DBTable($db, "smis_mr_kematian");
	$dbtable->truncate();
	$serv=new ServiceConsumer($db, "get_patient_mati",NULL,"registration");
	$serv->addData("command", "count");
	$serv->addData("dari", $_POST['dari']);
	$serv->addData("sampai", $_POST['sampai']);	
	$serv->execute();
	$data=$serv->getContent();
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($data);
	echo json_encode($res->getPackage());
	return;
}


if(isset($_POST['super_command']) && $_POST['super_command']=="limit") {
	$data=array();	
	$response=new ServiceConsumer($db, "get_patient_mati",NULL,"registration");
	$response->addData("command", "list");
	$response->addData("dari", $_POST['dari']);
	$response->addData("sampai", $_POST['sampai']);
	$response->addData("halaman", $_POST['halaman']);	
	$response->setMode(ServiceConsumer::$SINGLE_MODE);
	$response->execute();
	$list=$response->getContent();
	$dbtable=new DBTable($db,"smis_mr_kematian");
    loadLibrary("smis-libs-function-time");
	loadLibrary("smis-libs-function-medical");
	foreach($list as $one){		
		$up=array();
		$up['jk']=$one['kelamin'];
		$up['nama_pasien']=$one['nama_pasien'];
		$up['nrm_pasien']=$one['nrm'];
		$up['umur_pasien']=medical_umur($one['tanggal'],$one['tgl_lahir']);	
		$up['tanggal']=$one['tanggal_pulang'];
		$up['carapulang']=$one['carapulang'];	
		$up['carabayar']=$one['carabayar'];		
		$up['domisili']=$one['alamat_pasien'];	
		$up['dokter']=$one['nama_dokter'];
		$up['ruangan']=$one['kamar_inap']==""?$one['jenislayanan']:$one['kamar_inap'];		
		$up['durasi']=day_diff_only($one['tanggal_pulang'],$one['tanggal']);		
		$query="SELECT * FROM smis_mr_diagnosa WHERE noreg_pasien='".$one['id']."' AND diagnosa!='' AND prop!='del' ORDER BY  id DESC;";
		$diag=$db->get_row($query);
		if($diag!=NULL){
			$up['diagnosa']=$diag->sebab_sakit==""?$diag->diagnosa:$diag->sebab_sakit;
			if($up['dokter']=="") $up['dokter']=$diag->nama_dokter;
		}
		$dbtable->insert($up);
	}
	$res=new ResponsePackage();
	$res->setStatus(ResponsePackage::$STATUS_OK);
	$res->setContent($list);
	echo json_encode($res->getPackage());
	return;
}

$service=new ServiceConsumer($db,"get_jenis_patient",NULL,"registration");
$service->execute();
$carabayar=$service->getContent();
$total=count($carabayar);

$head0="<tr>
			<th rowspan='2'>No.</th>
			<th rowspan='2'>Tanggal</th>
			<th rowspan='2'>No RM</th> 
			<th rowspan='2'>Umur</th> 
			<th colspan='2'>Seks</th>
			<th colspan='3'>Kematian</th>
			<th rowspan='2'>Diagnosa</th>
			<th rowspan='2'>Dokter</th>  
			<th rowspan='2'>Lama Dirawat</th>
			<th rowspan='2'>Domisili</th>  
			<th rowspan='2'>Ruangan</th> 
			<th colspan='$total'>Carabayar</th>
		</tr>";
$_result="";
$_summary="";
$_query="";
foreach($carabayar as $x){
	$_result.="<th>".$x['name']."</th>";
	$_query.="if(carabayar='".$x['name']."',1,0 ) as ".$x['value'].", ";
	$_summary.="sum(".$x['value'].") as ".$x['value'].", ";
}
$head1="<tr>
			<th>L</th> 
			<th>P</th>
			
			<th>< 1 Jam Post Operasi</th> 
			<th>< 48 Jam</th> 
			<th>>= 48 Jam</th>
			
			".$_result."
		</tr>";
			
				
$uitable=new Table(array());
$uitable->addHeader("before",$head0);
$uitable->addHeader("before",$head1);
$uitable->addHeaderElement("No.");
$uitable->addHeaderElement("Tanggal");
$uitable->addHeaderElement("NRM");
$uitable->addHeaderElement("Umur");
$uitable->addHeaderElement("L");
$uitable->addHeaderElement("P");
$uitable->addHeaderElement("< 1 Jam Post Operasi");
$uitable->addHeaderElement("< 48 Jam");
$uitable->addHeaderElement(">= 48 Jam");
$uitable->addHeaderElement("Diagnosa");
$uitable->addHeaderElement("Dokter");
$uitable->addHeaderElement("Lama Dirawat");
$uitable->addHeaderElement("Domisili");
$uitable->addHeaderElement("Ruangan");
foreach($carabayar as $x){
	$uitable->addHeaderElement($x['name']);
}

$uitable->setHeaderVisible(false);
$uitable->setName("lap_index_kematian")
		->setActionEnable(false)
		->setFooterVisible(false);
/*
if(isset($_POST['command']) && $_POST['command']=="excel"){
	loadLibrary("smis-libs-function-export");
	$dbtable=new DBTable($db, "smis_mr_kematian");
	$dbtable->setOrder(" tanggal ASC");
	$dbtable->setFetchMethode(DBTable::$ARRAY_FETCH);
	$dbtable->setShowAll(true);
	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien","only-digit8");
	$adapter->add("Ruangan", "ruangan","unslug");
	$adapter->add("Masuk", "tanggal_mrs","date d/m/Y");
	$adapter->add("Keluar", "tanggal_kmrs","date d/m/Y");
	$adapter->add("Durasi", "lama_dirawat","back Hari");
	$adapter->add("0 - 28 HR", "28hr","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("28 HR - 1 TH", "1th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("1-4 TH", "4th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("5-14 TH", "14th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("15-24 TH", "24th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("25-44 TH", "44th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("45-59 TH", "59th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("60-64 TH", "64th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("65 TH+", "65th","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Kelas","kelas", "unslug");
	$adapter->add("Meninggal", "meninggal","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("Sembuh", "sembuh","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("L", "laki","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("P", "pr","trivial_0__<i class='fa fa-check'></>");
	
	$dt=$dbtable->view("", "0");
	$ctx=$adapter->getContent($dt['data']);
	createExcel("pasien_pulang_inap", $ctx);
	return;
		
}*/



if(isset($_POST['command']) && $_POST['command']!=""){	

	$query_view="
			SELECT 	tanggal,		
			nama_pasien,
			nrm_pasien,
			umur_pasien,
			dokter,
			durasi,
			domisili,
			".$_query."
			if(carapulang='Dipulangkan Mati < 1 Jam Post Operasi',1,0) as m1,
			if(carapulang='Dipulangkan Mati <=48 Jam',1,0) as mk48, 
			if(carapulang='Dipulangkan Mati >48 Jam',1,0) as ml48,
			if(jk=0,1,0) as laki,
			if(jk=1,1,0) as pr,
			diagnosa,
			ruangan as ruangan
			FROM smis_mr_kematian			
	";
	$query_count="SELECT COUNT(*) FROM smis_mr_kematian";
	
	$dbtable=new DBTable($db, "smis_mr_kematian");
	$dbtable->setPreferredQuery(true,$query_view,$query_count);
	$dbtable->setUseWhereforView(true);
	$dbtable->setOrder(" tanggal ASC");	
	$adapter=new SimpleAdapter();
	$adapter->setUseNumber(true, "No.","back.");
	$adapter->add("NRM", "nrm_pasien","only-digit8");
	$adapter->add("Ruangan", "ruangan","unslug");
	$adapter->add("Tanggal", "tanggal","date d M Y H:i");
	$adapter->add("Lama Dirawat", "durasi","back Hari");	
	$adapter->add("L", "laki","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("P", "pr","trivial_0__<i class='fa fa-check'></>");	
	$adapter->add("Diagnosa", "diagnosa");
	$adapter->add("Domisili", "domisili");
	$adapter->add("Umur", "umur_pasien");
	$adapter->add("Durasi", "durasi","back Hari");
	$adapter->add("Dokter", "dokter");	
	$adapter->add("< 1 Jam Post Operasi", "m1","trivial_0__<i class='fa fa-check'></>");
	$adapter->add("< 48 Jam", "mk48","trivial_0__<i class='fa fa-check'></>");
	$adapter->add(">= 48 Jam", "ml48","trivial_0__<i class='fa fa-check'></>");
	foreach($carabayar as $x){
		$adapter->add($x['name'], $x['value'],"trivial_0__<i class='fa fa-check'></>");
	}
	
	if($_POST['command']=="last_count"){		
		$adapter=new SimpleAdapter();
		$adapter->setUseNumber(true, "No.","back.");
		$adapter->add("Tanggal", "tanggal","date d M Y");
		$adapter->add("NRM", "nrm_pasien","only-digit8");
		$adapter->add("Ruangan", "ruangan","unslug");
		$adapter->add("Lama Dirawat", "durasi");
		$adapter->add("Diagnosa", "diagnosa");
		$adapter->add("Domisili", "domisili");
		$adapter->add("Dokter", "dokter");	
		$adapter->add("L", "laki");
		$adapter->add("P", "pr");
		$adapter->add("< 1 Jam Post Operasi", "m1");
		$adapter->add("< 48 Jam", "mk48");
		$adapter->add(">= 48 Jam", "ml48");	
		$adapter->add("Domisili", "domisili");
		foreach($carabayar as $x){
			$adapter->add($x['name'], $x['value']);
		}		
		$dbtable->setShowAll(true);
		$query=$dbtable->getQueryView("", "0");
		$q="SELECT ".$_summary." 
			'' as nama_pasien, 
			'' as tanggal,
			'' as nrm_pasien,
			'' as durasi,
			domisili,
			'' as ruangan,
			'' as diagnosa,
			sum(m1) as m1,
			sum(mk48) as mk48, 
			sum(ml48) as ml48,
			sum(laki) as laki,
			sum(pr) as pr,
			'' as prop		
		FROM (".$query['query'].") as X ";
		
		$one=$db->get_result($q,false);
		$adapter->setRemoveZeroEnable(true);
		$content=$adapter->getContent($one);
		$content[0]['No.']="<strong>TOTAL</strong>";
		$content[0]['Tanggal']="";
		$content[0]['NRM']="";
		$content[0]['Durasi']="";
		$uitable->setContent($content);
		$list=$uitable->getBodyContent();
		
		$res=new ResponsePackage();
		$res->setContent($list);
		$res->setStatus(ResponsePackage::$STATUS_OK);
		echo json_encode($res->getPackage());
		return;
	}
	
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	$hasil=$dbres->command($_POST['command']);
	echo json_encode($hasil);
	return;
}

$uitable->clearContent();
$uitable->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "");
$action=new Button("","","View");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("lap_index_kematian.rekaptotal()");
$view=new Button("","","View");
$view->setClass("btn-primary")
		->setIsButton(Button::$ICONIC)
		->setIcon("fa fa-refresh")
		->setAction("lap_index_kematian.multiview()");
$excel=new Button("","","Excel");
$excel->setClass("btn-primary")
	->setIsButton(Button::$ICONIC)
	->setIcon("fa fa-file-excel-o")
	->setAction("lap_index_kematian.excel()");

$btng=new ButtonGroup("");
$btng->addButton($action);
$btng->addButton($view);
//$btng->addButton($excel);

$form=$uitable
	  ->getModal()
	  ->setTitle("Medical Record")
	  ->getForm()
	  ->addElement("",$btng);

$close=new Button("", "", "Batal");
$close	->addClass("btn-primary")
		->setIsButton(Button::$ICONIC_TEXT)
		->setIcon("fa fa-close")
		->setAction("lap_index_kematian.batal()");

$load=new LoadingBar("rekap_lap_index_kematian_bar", "");
$modal=new Modal("rekap_lap_index_kematian_modal", "", "Processing...");
$modal	->addHTML($load->getHtml(),"after")
		->addFooter($close);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_lap_index_kematian'>".$uitable->getHtml()."</div>";
echo $modal->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
echo addJS ( "base-js/smis-base-loading.js");
?>

<script type="text/javascript">
	var lap_index_kematian;
	var tindakan_lap_index_kematian;
	var IS_lap_index_kematian_RUNNING;
	$(document).ready(function(){
		$('.mydate').datepicker();
		lap_index_kematian=new TableAction("lap_index_kematian","medical_record","lap_index_kematian",new Array());
		lap_index_kematian.addRegulerData=function(data){
			data['dari']=$("#lap_index_kematian_dari").val();
			data['sampai']=$("#lap_index_kematian_sampai").val();
			$("#dari_table_lap_index_kematian").html(getFormattedDate(data['dari']));
			$("#sampai_table_lap_index_kematian").html(getFormattedDate(data['sampai']));							
			return data;
		};

		lap_index_kematian.batal=function(){
			IS_lap_index_kematian_RUNNING=false;
			$("#rekap_lap_index_kematian_modal").modal("hide");
		};
		
		lap_index_kematian.afterview=function(json){
			if(json!=null){
				$("#kode_table_lap_index_kematian").html(json.nomor);
				$("#waktu_table_lap_index_kematian").html(json.waktu);
				lap_index_kematian_data=json;
			}
		};

		lap_index_kematian.rekaptotal=function(){
			if(IS_lap_index_kematian_RUNNING) return;
			$("#rekap_lap_index_kematian_bar").sload("true","Fetching total data",0);
			$("#rekap_lap_index_kematian_modal").modal("show");
			IS_lap_index_kematian_RUNNING=true;
			var d=this.getRegulerData();
			d['super_command']="total";
			$.post("",d,function(res){
				var all=getContent(res);
				if(all!=null) {
					var total=Number(all);
					lap_index_kematian.rekaploop(0,total);
				} else {
					$("#rekap_lap_index_kematian_modal").modal("hide");
					IS_lap_index_kematian_RUNNING=false;
				}
			});
		};

		lap_index_kematian.excel=function(){
			var d=this.getRegulerData();
			d['command']="excel";
			download(d);
		};

		lap_index_kematian.rekaploop=function(current,total){
			if(current>=total || !IS_lap_index_kematian_RUNNING) {
				//$("#rekap_lap_index_kematian_modal").modal("hide");
				IS_lap_index_kematian_RUNNING=false;
				lap_index_kematian.multiview();
				return;
			}
			var d=this.getRegulerData();
			d['super_command']="limit";
			d['halaman']=current;
			$.post("",d,function(res){
				var ct=getContent(res);
				var u="";//ct[0]['nama_pasien']+"  "+ct[0]['nrm_pasien'];
				$("#rekap_lap_index_kematian_bar").sload("true"," Processing - "+u+"... [ "+(current+1)+" / "+total+" ] ",(current*100/total));
				setTimeout(function(){lap_index_kematian.rekaploop(++current,total)},300);
			});
		};
		
		lap_index_kematian.multiview=function(){
			$("#rekap_lap_index_kematian_bar").sload("true","Viewing data...",0);
			$("#rekap_lap_index_kematian_modal").modal("show");
			IS_lap_index_kematian_RUNNING=true;
			var d=this.getViewData();
			$("#lap_index_kematian_list").html("");
			d['command']="multipart_view";
			$.post("",d,function(res){
				var total=Number(getContent(res));
				if(total==0){
					$("#rekap_lap_index_kematian_modal").modal("hide");
					IS_lap_index_kematian_RUNNING=false;
				}else{
					var pages=Math.ceil(total/5);
					lap_index_kematian.multipart_loop(0,pages);
				}
			});			
		};
		
		lap_index_kematian.multipart_loop=function(current,total){
			if(current>=total || !IS_lap_index_kematian_RUNNING) {
				IS_lap_index_kematian_RUNNING=false;
				lap_index_kematian.last_count();
				return;
			}
			$("#rekap_lap_index_kematian_bar").sload("true","Page Number [ "+(current+1)+" / "+total+" ] ",(current*100/total));
			var d=this.getViewData();
			d['number']=current;
			d['max']=5;		
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#lap_index_kematian_list").append(ct.list);
				setTimeout(function(){lap_index_kematian.multipart_loop(++current,total)},300);
			});
		};
		
		lap_index_kematian.last_count=function(){
			var d=this.getRegulerData();
			d['command']="last_count";
			$.post("",d,function(res){
				var ct=getContent(res);
				$("#lap_index_kematian_list").append(ct);
				$("#rekap_lap_index_kematian_modal").modal("hide");
			});
		};
	});
</script>