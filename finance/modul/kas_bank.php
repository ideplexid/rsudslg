<?php 
global $db;
$dbtable = new DBTable($db,"smis_fnc_akun");
$dbtable ->setShowAll(true);
$data    = $dbtable ->view("",0);
$list    = $data["data"];

$tabs = new Tabulator("kas_bank","kas_bank",Tabulator::$LANDSCAPE_RIGHT);
foreach($list as $l){
    $tabs ->add("kas_bank_".$l->id,$l->nomor_akun." - ".$l->nama_akun,"",Tabulator::$TYPE_HTML,"fa fa-university");
}
$tabs->setPartialLoad(true,"finance","kas_bank","kas_bank",true);
echo $tabs->getHtml();
?>