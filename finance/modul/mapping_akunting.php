<?php
	$tab = new Tabulator("mapping_akunting","Mapping Akunting",Tabulator::$POTRAIT);
	$tab->add ("setup","Setup Akunting","",Tabulator::$TYPE_HTML," fa fa-list-alt" );
	$tab->add ("map_pembayaran_umum","Pembayaran Umum","",Tabulator::$TYPE_HTML," fa fa-ticket" );
	$tab->add ("mapping_karyawan","Mapping Karyawan","",Tabulator::$TYPE_HTML," fa fa-users" );
	$tab->add ("mapping_vendor","Mapping Vendor","",Tabulator::$TYPE_HTML," fa fa-address-book" );
	$tab->setPartialLoad(true,"finance","mapping_akunting","mapping_akunting",true);
    echo $tab->getHtml();
?>