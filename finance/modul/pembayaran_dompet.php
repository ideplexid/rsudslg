<?php
/**
 * this is handling about element where used for patient management
 * @author goblooge
 *
 */

$id_dompet=str_replace("pembayaran_dompet_","",$_POST['action']);
$dbtable=new DBTable($db,"smis_fnc_kas");
$kas=$dbtable->select(array("id"=>$id_dompet));

$header=array('Tanggal','Jenis',"No Referensi",'Keterangan',"Kas & Bank");
$uitable = new Table($header,"Pengeluaran ".$kas->nama,NULL,true);
$uitable->setName("pembayaran_dompet_".$id_dompet);
if(isset($_POST['command'])) {
	$adapter = new SimpleAdapter();
	$adapter->add("Tanggal","waktu","date d M Y");
	$adapter->add("Jenis","jenis","unslug");
	$adapter->add("No Referensi","noref");
	$adapter->add("Keterangan","keterangan");
	$adapter->add("Nilai","nilai","money Rp.");
	$adapter->add("No Referensi","noref");
    $adapter->add("Kas & Bank","nama_dompet");
	$dbtable = new DBTable($db,"smis_fnc_bayar");
	$dbtable->addCustomKriteria("jenis", "='pembayaran_dompet'");
    $dbtable->addCustomKriteria("id_dompet", "='".$id_dompet."'");
	$dbres = new DBResponder($dbtable,$uitable,$adapter);
    if($dbres->isSave()){
        $dbres->addColumnFixValue("jt_bayar",$_POST['waktu']);
        $dbres->addColumnFixValue("tgl_cair",$_POST['waktu']);
    }
	$data = $dbres->command($_POST ['command']);
	echo json_encode($data);
	return;
}



$uitable->addModal("id","hidden","","");
$uitable->addModal("waktu","date","Tanggal",date("Y-m-d"));
$uitable->addModal("jenis","hidden","","pembayaran_dompet");
$uitable->addModal("keterangan","textarea","Keterangan","");
$uitable->addModal("noref","text","No Referensi","");
$uitable->addModal("nilai","money","Nilai","");
$uitable->addModal("status","hidden","","1");
$uitable->addModal("html","summernote-print","Detail","");
$uitable->addModal("nama_dompet","hidden","",$kas->nama);
$uitable->addModal("id_dompet","hidden","",$id_dompet);
$uitable->addModal("no_akun","hidden","",$kas->nomor_akun);
$modal = $uitable->getModal();
$modal->setTitle("pembayaran");
$hidden=new Hidden("pembayaran_dompet_id","",$id_dompet);

echo $uitable->getHtml();
echo $modal->getHtml();
echo $hidden->getHtml();
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");
?>
<script type="text/javascript">
var pb_dompet;
$(document).ready(function(){
    var id_dompet=$("#pembayaran_dompet_id").val();
	$(".mydate").datepicker();
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement':'bottom'});
	var column=new Array('id','waktu','jenis','noref','keterangan',"nilai","html","status","id_dompet","nama_dompet","no_akun");
	pb_dompet=new TableAction("pembayaran_dompet_"+id_dompet,"finance","pembayaran_dompet_"+id_dompet,column);
    window["pembayaran_dompet_" + id_dompet]=pb_dompet;
    pb_dompet.view();
});
</script>