<?php 
global $db;
global $user;
require_once 'smis-base/smis-include-service-consumer.php';

if(isset($_POST['super_command']) && $_POST['super_command']!="") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");

	$actable=new Table($head);
	$actable->setName("rekap_pv_resep_accounting");
	$actable->setModel(Table::$SELECT);
	$accounting=new EmployeeResponder($db,$actable,$dkadapter,"finance");

	$super=new SuperCommand();
	$super->addResponder("rekap_pv_resep_accounting",$accounting);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

$header=array("Nama","Total","Bagian Dokter","Bagian RS");
$uitable=new Table($header);
$head00="<tr>
		<th colspan='30' id='surat_bukti'>REKAP RESEP DOKTER<font id='untuk_table_rekap_pv_resep'></font></th>
		</tr>";
$head0="<tr>
		<th>Nomor</th>
		<th  id='kode_table_rekap_pv_resep'></th>
		<th>Waktu</th>
		<th colspan='3' id='waktu_table_rekap_pv_resep'></th>
		</tr>";
$head2="<tr>
		<th >Dari</th>
		<th id='dari_table_rekap_pv_resep'></th>
		<th >Sampai</th>
		<th colspan='3'  id='sampai_table_rekap_pv_resep'></th>
		</tr>";

$head3="<tr>
		<th >Pajak</th>
		<th id='pajak_table_rekap_pv_resep' colspan='20'></th>
		</tr>";

$uitable->addHeader("before", $head00)
		->addHeader("before", $head0)
		->addHeader("before", $head2)
		->addHeader("before", $head3)
		->setName("rekap_pv_resep")
		->setActionEnable(false)
		->setFooterVisible(false);

if(isset($_POST['command']) && $_POST['command']== "list") {	
	require_once 'finance/class/adapter/ResepRecapitulationAdapter.php';
	loadLibrary("smis-libs-function-math");
	$service=new ServiceConsumer($db, "rekap_pv_resep");
	$service->setMode(ServiceConsumer::$CLEAN_BOTH)
			->addData("from", $_POST['dari'])
			->addData("to", $_POST['sampai']);
	$content=$service->execute()->getContent();
	$adapter=new ResepRecapitulationAdapter($_POST['pajak']);
	$uidata=$adapter->add("Nama","nama_karyawan")
					->add("Total","total","money Rp.")
					->add("Bagian Dokter","bagian_dokter","money Rp.")
					->add("Bagian RS","bagian_rs","money Rp.")
					->setUseID(false)
					->getContent($content);
	$list=$uitable->setContent($uidata)
				  ->getBodyContent();
	
	
	
	$json['list']=$list;
	$json['pagination']="";
	$json['number']="0";
	$json['number_p']="0";
	$json['data']=$uidata;
	$json['waktu']=ArrayAdapter::format("date d M Y H:i:s", date("Y-m-d H:i:s"));
	$json['nomor']="RRSP-".date("dmy-his")."-".substr(md5($user->getNameOnly()), 5,rand(2, 5));
	$ft=ArrayAdapter::format("date d M Y", $_POST['dari']);
	$to=ArrayAdapter::format("date d M Y", $_POST['sampai']);	
	$json['json']=json_encode($uidata);
	$json['status']=1;
	
	$pack=new ResponsePackage();
	$pack->setContent($json);
	$pack->setStatus(ResponsePackage::$STATUS_OK);
	echo json_encode($pack->getPackage());
	return;
}

$pjkserv=new ServiceConsumer($db, "get_pajak");
$pjkserv->execute();
$pajak=$pjkserv->getContent();

$uitable->addModal("nama_accounting", "chooser-rekap_pv_resep-rekap_pv_resep_accounting-Accounting", "Accounting", "")
		->addModal("dari", "date", "Dari", "")
		->addModal("sampai", "date", "Sampai", "")
		->addModal("pajak", "text", "Pajak (%) ", $pajak);
$action=new Button("","","");
$action->setClass("btn-primary")
	   ->setIsButton(Button::$ICONIC)
	   ->setIcon("icon-white fa fa-circle-o-notch")
	   ->setAction("rekap_pv_resep.view()");
$print_acc=new Button("","","Accounting");
$print_acc->setClass("btn-primary")
	  	  ->setIsButton(Button::$ICONIC_TEXT)
	  	  ->setIcon("fa fa-print")
	  	  ->setAction("rekap_pv_resep.print()");
$btn_froup=new ButtonGroup("");
$btn_froup->addButton($action)
		  ->addButton($print_acc);
$form=$uitable
	  ->getModal()
	  ->setTitle("Tindakan")
	  ->getForm()
	  ->addElement("",$btn_froup);

echo $form->getHtml();
echo "<div class='clear'></div>";
echo "<div id='result_rekap_pv_resep'>".$uitable->getHtml()."</div>";
echo addJS("framework/bootstrap/js/bootstrap-datepicker.js");
echo addJS("framework/smis/js/table_action.js");
echo addCSS("framework/bootstrap/css/datepicker.css");

?>

<script type="text/javascript">
	var rekap_pv_resep;
	var rekap_pv_resep_karyawan;
	var rekap_pv_resep_accounting;
	var rekap_pv_resep_data;
	$(document).ready(function(){
		$('.mydate').datepicker();
		rekap_pv_resep=new TableAction("rekap_pv_resep","finance","rekap_pv_resep",new Array());

		rekap_pv_resep_accounting=new TableAction("rekap_pv_resep_accounting","finance","rekap_pv_resep",new Array());
		rekap_pv_resep_accounting.setSuperCommand("rekap_pv_resep_accounting");
		rekap_pv_resep_accounting.selected=function(json){
			$("#rekap_pv_resep_nama_accounting").val(json.nama);
		};

		rekap_pv_resep.addRegulerData=function(data){
			data['dari']=$("#rekap_pv_resep_dari").val();
			data['sampai']=$("#rekap_pv_resep_sampai").val();
			data['pajak']=$("#rekap_pv_resep_pajak").val();
			$("#dari_table_rekap_pv_resep").html(getFormattedDate(data['dari']));
			$("#sampai_table_rekap_pv_resep").html(getFormattedDate(data['sampai']));
			$("#pajak_table_rekap_pv_resep").html(data['pajak']+"%");			
			return data;
		};

		rekap_pv_resep.afterview=function(json){
			if(json!=null){
				$("#kode_table_rekap_pv_resep").html(json.nomor);
				$("#waktu_table_rekap_pv_resep").html(json.waktu);
				rekap_pv_resep_data=json;
			}
		};
		
	});
</script>