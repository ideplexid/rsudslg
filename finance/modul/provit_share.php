<?php 

$detail = new Tabulator("detail", "detail",Tabulator::$LANDSCAPE_RIGHT);
$detail	->add("pv_tindakan_keperawatan", "Tindakan Keperawatan", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('pv_tindakan_keperawatan', 'pv_tindakan_keperawatan')")
        ->add("pv_tindakan_igd", "Tindakan IGD", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('pv_tindakan_igd', 'pv_tindakan_igd')")
        ->add("pv_tindakan", "Tindakan Dokter", "",Tabulator::$TYPE_HTML,"fa fa-user-md","loadPagePV('pv_tindakan','pv_tindakan')")
        ->add("pv_konsul", "Konsul", "",Tabulator::$TYPE_HTML,"fa fa-recycle","loadPagePV('pv_konsul','pv_konsul')")
        ->add("pv_periksa", "Periksa", "",Tabulator::$TYPE_HTML,"fa fa-medkit","loadPagePV('pv_periksa','pv_periksa')")
        ->add("pv_visite", "Visite", "",Tabulator::$TYPE_HTML,"fa fa-tint","loadPagePV('pv_visite','pv_visite')")
        ->add("pv_resep", "Resep", "",Tabulator::$TYPE_HTML,"fa fa-file","loadPagePV('pv_resep','pv_resep')")
        //->add("pv_ekg", "EKG", "",Tabulator::$TYPE_HTML,"fa fa-line-chart","loadPagePV('pv_ekg','pv_ekg')")
        //->add("pv_endoscopy", "Endoscopy", "",Tabulator::$TYPE_HTML,"fa fa-tree","loadPagePV('pv_endoscopy','pv_endoscopy')")
        //->add("pv_bronchoscopy", "Bronchoscopy", "",Tabulator::$TYPE_HTML,"fa fa-fire-extinguisher","loadPagePV('pv_bronchoscopy','pv_bronchoscopy')")
        //->add("pv_spirometry", "Spirometry", "",Tabulator::$TYPE_HTML,"fa fa-heart","loadPagePV('pv_spirometry','pv_spirometry')")
        //->add("pv_audiometry", "Audiometry", "",Tabulator::$TYPE_HTML,"fa fa-music","loadPagePV('pv_audiometry','pv_audiometry')")
        ->add("pv_laboratory", "Laboratory", "",Tabulator::$TYPE_HTML,"fa fa-eyedropper","loadPagePV('pv_laboratory','pv_laboratory')")
        ->add("pv_radiology", "Radiology", "",Tabulator::$TYPE_HTML,"fa fa-warning","loadPagePV('pv_radiology','pv_radiology')");

$rekap  = new Tabulator("rekap", "rekap",Tabulator::$LANDSCAPE_RIGHT);
$rekap  ->add("rekap_pv_tindakan_keperawatan", "Tindakan Keperawatan", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('rekap_pv_tindakan_keperawatan', 'rekap_pv_tindakan_keperawatan')")	
		->add("rekap_pv_tindakan_igd", "Tindakan IGD", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('rekap_pv_tindakan_igd', 'rekap_pv_tindakan_igd')")	
		->add("rekap_pv_tindakan", "Tindakan Dokter", "",Tabulator::$TYPE_HTML,"fa fa-user-md","loadPagePV('rekap_pv_tindakan','rekap_pv_tindakan')")
		->add("rekap_pv_konsul", "Konsul", "",Tabulator::$TYPE_HTML,"fa fa-recycle","loadPagePV('rekap_pv_konsul','rekap_pv_konsul')")
		->add("rekap_pv_periksa", "Periksa", "",Tabulator::$TYPE_HTML,"fa fa-medkit","loadPagePV('rekap_pv_periksa','rekap_pv_periksa')")
		->add("rekap_pv_visite", "Visite", "",Tabulator::$TYPE_HTML,"fa fa-tint","loadPagePV('rekap_pv_visite','rekap_pv_visite')")
		->add("rekap_pv_resep", "Resep", "",Tabulator::$TYPE_HTML,"fa fa-file","loadPagePV('rekap_pv_resep','rekap_pv_resep')")
		//->add("rekap_pv_ekg", "EKG", "",Tabulator::$TYPE_HTML,"fa fa-line-chart","loadPagePV('rekap_pv_ekg','rekap_pv_ekg')")
		//->add("rekap_pv_endoscopy", "Endoscopy", "",Tabulator::$TYPE_HTML,"fa fa-tree","loadPagePV('rekap_pv_endoscopy','rekap_pv_endoscopy')")
		//->add("rekap_pv_bronchoscopy", "Bronchoscopy", "",Tabulator::$TYPE_HTML,"fa fa-fire-extinguisher","loadPagePV('rekap_pv_bronchoscopy','rekap_pv_bronchoscopy')")
		//->add("rekap_pv_spirometry", "Spirometry", "",Tabulator::$TYPE_HTML,"fa fa-heart","loadPagePV('rekap_pv_spirometry','rekap_pv_spirometry')")
		//->add("rekap_pv_audiometry", "Audiometry", "",Tabulator::$TYPE_HTML,"fa fa-music","loadPagePV('rekap_pv_audiometry','rekap_pv_audiometry')")
		->add("rekap_pv_laboratory", "Laboratory", "",Tabulator::$TYPE_HTML,"fa fa-eyedropper","loadPagePV('rekap_pv_laboratory','rekap_pv_laboratory')")
		->add("rekap_pv_radiology", "Radiology", "",Tabulator::$TYPE_HTML,"fa fa-warning","loadPagePV('rekap_pv_radiology','rekap_pv_radiology')");

$pendapatan = new Tabulator("pendapatan", "pendapatan",Tabulator::$LANDSCAPE_RIGHT);
$pendapatan	->add("pendapatan_tindakan_dokter", "Tindakan Dokter", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('pendapatan_tindakan_dokter', 'pendapatan_tindakan_dokter')")
			->add("pendapatan_periksa", "Periksa", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('pendapatan_periksa', 'pendapatan_periksa')")
			->add("pendapatan_konsul", "Konsul", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('pendapatan_konsul', 'pendapatan_konsul')")
			->add("pendapatan_visite", "Visite", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('pendapatan_visite', 'pendapatan_visite')")
			->add("pendapatan_tindakan_perawat", "Tindakan Perawat", "",Tabulator::$TYPE_HTML,"fa fa-user-md","loadPagePV('pendapatan_tindakan_perawat','pendapatan_tindakan_perawat')")
			->add("pendapatan_tindakan_perawat_igd", "Tindakan Perawat IGD", "",Tabulator::$TYPE_HTML,"fa fa-recycle","loadPagePV('pendapatan_tindakan_perawat_igd','pendapatan_tindakan_perawat_igd')")
			->add("pendapatan_laboratory", "Laboratory", "",Tabulator::$TYPE_HTML,"fa fa-eyedropper","loadPagePV('pendapatan_laboratory','pendapatan_laboratory')")
			->add("pendapatan_radiology", "Radiology", "",Tabulator::$TYPE_HTML,"fa fa-warning","loadPagePV('pendapatan_radiology','pendapatan_radiology')")
			->add("pendapatan_fisiotherapy", "Fisiotherapy", "",Tabulator::$TYPE_HTML,"fa fa-user","loadPagePV('pendapatan_fisiotherapy','pendapatan_fisiotherapy')")
            ->add("item_ok", "Kamar Operasi", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('item_ok', 'item_ok')");
            
            
$jenis  = new Tabulator("jenis", "jenis",Tabulator::$LANDSCAPE_RIGHT);
$jenis  ->add("jenis_tindakan_keperawatan", "Tindakan Keperawatan", "", Tabulator::$TYPE_HTML, "fa fa-user-md", "loadPagePV('jenis_tindakan_keperawatan', 'jenis_tindakan_keperawatan')")	
		->add("jenis_tindakan", "Tindakan Dokter", "",Tabulator::$TYPE_HTML,"fa fa-user-md","loadPagePV('jenis_tindakan','jenis_tindakan')")
		->add("jenis_konsul", "Konsul", "",Tabulator::$TYPE_HTML,"fa fa-recycle","loadPagePV('jenis_konsul','jenis_konsul')")
		->add("jenis_periksa", "Periksa", "",Tabulator::$TYPE_HTML,"fa fa-medkit","loadPagePV('jenis_periksa','jenis_periksa')")
		->add("jenis_visite", "Visite", "",Tabulator::$TYPE_HTML,"fa fa-tint","loadPagePV('jenis_visite','jenis_visite')")
		->add("jenis_resep", "Resep", "",Tabulator::$TYPE_HTML,"fa fa-file","loadPagePV('jenis_resep','jenis_resep')")
		//->add("jenis_ekg", "EKG", "",Tabulator::$TYPE_HTML,"fa fa-line-chart","loadPagePV('jenis_ekg','jenis_ekg')")
		//->add("jenis_endoscopy", "Endoscopy", "",Tabulator::$TYPE_HTML,"fa fa-tree","loadPagePV('jenis_endoscopy','jenis_endoscopy')")
		//->add("jenis_bronchoscopy", "Bronchoscopy", "",Tabulator::$TYPE_HTML,"fa fa-fire-extinguisher","loadPagePV('jenis_bronchoscopy','jenis_bronchoscopy')")
		//->add("jenis_spirometry", "Spirometry", "",Tabulator::$TYPE_HTML,"fa fa-heart","loadPagePV('jenis_spirometry','jenis_spirometry')")
		//->add("jenis_audiometry", "Audiometry", "",Tabulator::$TYPE_HTML,"fa fa-music","loadPagePV('jenis_audiometry','jenis_audiometry')")
		->add("jenis_laboratory", "Laboratory", "",Tabulator::$TYPE_HTML,"fa fa-eyedropper","loadPagePV('jenis_laboratory','jenis_laboratory')")
		->add("jenis_radiology", "Radiology", "",Tabulator::$TYPE_HTML,"fa fa-warning","loadPagePV('jenis_radiology','jenis_radiology')");

$per_ruang = new Tabulator("pendapatan_peruang", "pendapatan_peruang",Tabulator::$LANDSCAPE_RIGHT);
$per_ruang	->add("pendapatan_oksigen_central", "Oksigen Central", "",Tabulator::$TYPE_HTML,"fa fa-fire-extinguisher","loadPagePV('pendapatan_oksigen_central','pendapatan_oksigen_central')")
            ->add("pendapatan_oksigen_manual", "Oksigen Manual", "",Tabulator::$TYPE_HTML,"fa fa-fire-extinguisher","loadPagePV('pendapatan_oksigen_manual','pendapatan_oksigen_manual')")
            ->add("pendapatan_kamar_mayat", "Kamar Mayat", "", Tabulator::$TYPE_HTML, "fa fa-users", "loadPagePV('pendapatan_kamar_mayat', 'pendapatan_kamar_mayat')");



$tabs = new Tabulator("total", "total",Tabulator::$POTRAIT);
echo $tabs	//->add("jenis", "Jenis Pasien (Unit Cost)", $pendapatan,Tabulator::$TYPE_COMPONENT,"fa fa-money","loadFirstPagePV('jenis')")
			->add("rekapitulasi", "Rekapitulasi Layanan Rumah Sakit", $rekap,Tabulator::$TYPE_COMPONENT,"fa fa-file","loadFirstPagePV('rekap')")
			->add("detail", "Detail Layanan Rumah Sakit", $detail,Tabulator::$TYPE_COMPONENT,"fa fa-file-text","loadFirstPagePV('detail')")
			->add("pendapatan", "Pendapatan Per Ruang Per Dokter", $pendapatan,Tabulator::$TYPE_COMPONENT,"fa fa-user-md","loadFirstPagePV('pendapatan')")
			->add("pendapatan_peruang", "Pendapatan Per Ruang", $per_ruang,Tabulator::$TYPE_COMPONENT,"fa fa-university","loadFirstPagePV('pendapatan_peruang')")
			->getHtml();
echo addJS("finance/resource/js/provit_share.js",false);
?>
