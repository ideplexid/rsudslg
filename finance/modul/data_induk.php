<?php 
    $tabs=new Tabulator("data_induk","data_induk",Tabulator::$LANDSCAPE_RIGHT);
    $tabs->add("kas","Kas & Bank","",Tabulator::$TYPE_HTML,"fa fa-briefcase");
    $tabs->add("beban","Beban","",Tabulator::$TYPE_HTML,"fa fa-money");
    $tabs->add("akun_kas_bank","Akun Kas Bank","",Tabulator::$TYPE_HTML,"fa fa-bank");

    $tabs->setPartialLoad(true, "finance", "data_induk", "data_induk",true);
    echo $tabs->getHtml();
?>