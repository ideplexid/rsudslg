<?php
	global $NAVIGATOR;
    global $db;
	$mr = new Menu ("fa fa-bank");
	$mr->addProperty ( 'title', 'Finance' );
	$mr->addProperty ( 'name', 'Keuangan' );
	$mr->addSubMenu ( "Data Induk", "finance", "data_induk", "Data Induk" ,"fa fa-database");
	$mr->addSubMenu ( "Pembayaran", "finance", "payment", "Pembayaran" ,"fa fa-money");
	$mr->addSubMenu ( "Mutasi Kas & Bank", "finance", "kas_bank", "Kas dan Bank" ,"fa fa-university");
	$mr->addSubMenu ( "Invoice", "finance", "invoice", "Invoice" ,"fa fa-money");
	$mr->addSubMenu ( "Laporan Uang Keluar", "finance", "laporan", "Laporan" ,"fa fa-book");
	$mr->addSubMenu ( "Laporan Uang Masuk", "finance", "laporan_uang_masuk", "Laporan" ,"fa fa-money");
	$mr->addSubMenu ( "Provit Share", "finance", "provit_share", "Pembagian Hasil" ,"fa fa-money");
	$mr->addSubMenu ( "Laporan Jasa Pelayanan", "finance", "laporan_jaspel_dokter", "Laporan Jasa Pelayanan" ,"fa fa-money");
	$mr->addSubMenu ( "Settings", "finance", "settings", "Settings" ,"fa fa-cog");
	$mr->addSubMenu ( "Mapping Akunting", "finance", "mapping_akunting", "Menu Map Data Akunting" ,"fa fa-map");
	$mr->addSubMenu ( "Laporan Piutang", "finance", "piutang", "Piutang" ,"fa fa-money");
	
    $dbtable=new DBTable($db,"smis_fnc_kas");
    $dbtable->setShowAll(true);
    $data=$dbtable->view("",0);
    $list=$data['data'];
    foreach($list as $x){
        $mr->addSubMenu ( "Pengeluaran ". $x->nama, "finance", "pembayaran_dompet_".$x->id, $x->nama ,"fa fa-money");
    }
    
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, "", "Keuangan", "finance");
	$mr = $inventory_navigator->extendMenu($mr);
	$NAVIGATOR->addMenu ( $mr, 'finance' );
?>
