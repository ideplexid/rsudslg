<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_fnc_termin_gaji_detail");
    $termin_detail = $dbtable->selectEventDel($id);
    
    $termin_db = new DBTable($db,"smis_fnc_termin_gaji");
    $termin=$termin_db->selectEventDel($termin_detail->id_termin);
    
    $akun_db = new DBTable($db,"smis_fnc_mapping_employee");
    $akun=$akun_db->selectEventDel(array("id_employee"=>$termin_detail->id_employee) );
    
    $kredit_akun=$akun->kredit;
    if($termin->id_dompet!="0"){
        $db_kas=new DBTable($db,"smis_fnc_kas");
        $hasil=$db_kas->select($termin->id_dompet);
        if($hasil!=null && $hasil->nomor_akun!=""){
            $kredit_akun=$hasil->nomor_akun;    
        }
    }
    
    //content untuk kredit / kas atau paten
    $kredit=array();
    $kredit['akun']     = $kredit_akun;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $termin_detail->dibayar;
    $kredit['ket']      = "Pembayaran Gaji ".$termin_detail->nama_employee." Pada Periode Gaji : ".$termin->periode;
    $kredit['code']     = "kredit-bayar-gaji-".$termin_detail->id;

    //content untuk debet - patent
    $debet=array();
    $debet['akun']    = $akun->debet;
    $debet['debet']   = $termin_detail->dibayar;
    $debet['kredit']  = 0;
    $debet['ket']     = "Hutang Gaji ".$termin_detail->nama_employee." Pada Periode Gaji : ".$termin->periode;
    $debet['code']    = "debet-bayar-gaji-".$termin_detail->id;

    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $termin->tanggal;
    $header['keterangan']   = "Bayar Gaji Karyawan ".$termin_detail->nama_employee." Pada Periode Gaji ".$termin->periode;
    $header['code']         = "bayar-gaji-karyawan-".$id;
    $header['nomor']        = "BGJK-".$id;
    $header['debet']        = $termin_detail->dibayar;
    $header['kredit']       = $termin_detail->dibayar;
    $header['io']           = "0";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>