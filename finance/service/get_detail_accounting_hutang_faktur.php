<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_fnc_bayar");
    $bayar_faktur = $dbtable->selectEventDel($id);
    
    $akun_db = new DBTable($db,"smis_fnc_mapping_vendor");
    $akun=$akun_db->selectEventDel(array("id_vendor"=>$bayar_faktur->id_vendor) );
    
    /*$kredit_akun=$akun->d_hutang;
    if($termin->id_dompet!="0"){
        $db_kas=new DBTable($db,"smis_fnc_kas");
        $hasil=$db_kas->select($termin->id_dompet);
        if($hasil!=null && $hasil->nomor_akun!=""){
            $kredit_akun=$hasil->nomor_akun;    
        }
    }*/
    
    //content untuk kredit / kas atau paten
    $kredit=array();
    $kredit['akun']     = $akun->k_hutang;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $bayar_faktur->nilai;
    $kredit['ket']      = "Pengakuan Hutang Faktur ".$bayar_faktur->nama_vendor." Pada No. Faktur : ".$bayar_faktur->noref;
    $kredit['code']     = "kredit-hutang-faktur-".$bayar_faktur->id;

    //content untuk debet - patent
    $debet=array();
    $debet['akun']    = $akun->d_hutang;
    $debet['debet']   = $bayar_faktur->nilai;
    $debet['kredit']  = 0;
    $debet['ket']     = "Pengakuan Hutang Faktur ".$bayar_faktur->nama_vendor." Pada No. Faktur : ".$bayar_faktur->noref;
    $debet['code']    = "debet-hutang-faktur-".$bayar_faktur->id;

    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $bayar_faktur->waktu;
    $header['keterangan']   = "Pengakuan Hutang Faktur ".$bayar_faktur->nama_vendor." Pada No. Faktur : ".$bayar_faktur->noref;
    $header['code']         = "hutang-faktur-".$id;
    $header['nomor']        = "FPHF-".$id;
    $header['debet']        = $bayar_faktur->nilai;
    $header['kredit']       = $bayar_faktur->nilai;
    $header['io']           = "0";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>