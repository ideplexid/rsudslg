<?php 
    global $db;
    $id               = $_POST['data'];
    $dbtable          = new DBTable($db,"smis_fnc_bayar");
    $x                = $dbtable->selectEventDel($id);
    
    $debet            = array();
    $debet['akun']    = $x->debet!=""?$x->debet:getSettings($db,"finance-accounting-debet-pembayaran-umum","");
    $debet['debet']   = $x->nilai;
    $debet['kredit']  = 0;
    $debet['ket']     = "Pengeluaran Umum [".$x->noref."] - ".$x->keterangan." : ".$x->deskripsi;
    $debet['code']    = "debet-bayar-umum-".$x->id;

    //default sesuai pilihan akun
    $no_kredit        = $x->no_akun;
    if($no_kredit==""){
        //jika tidak dipilih sesuai akun kas, akan diambil sesuai kredit jenis tagihan
        $no_kredit=$x->kredit;
        if($no_kredit==""){
            //jika kosong juga makan akan ikut default
            $no_kredit  = getSettings($db,"finance-accounting-kredit-pembayaran-umum","");
        }
    }
    
    $kredit             = array();
    $kredit['akun']     = $no_kredit;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $x->nilai;
    $kredit['ket']      = "Pemabayaran Umum [".$x->noref."] - ".$x->keterangan." : ".$x->deskripsi;
    $kredit['code']     = "kredit-bayar-umum-".$x->id;

    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $x->waktu;
    $header['keterangan']   = "Pembayaran Umum ".$x->keterangan." [".$x->noref."]";
    $header['code']         = "bayar-umum-".$x->id;
    $header['nomor']        = "FBU-".$x->id;
    $header['debet']        = $x->nilai;
    $header['kredit']       = $x->nilai;
    $header['io']           = "0";
    $header['grup']         = $x->grup;
    
    $transaction            = array();
    $transaction['header']  = $header;
    $transaction['content'] = $list;
    
    $final                  = array();
    $final[]                = $transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>