<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_fnc_invoice");
    $invoice = $dbtable->selectEventDel($id);
    
    $akun_db = new DBTable($db,"smis_fnc_mapping_vendor");
    $akun=$akun_db->selectEventDel(array("id_vendor"=>$invoice->id_vendor) );
    
   
    /*$kredit_akun=$akun->d_hutang;
    if($termin->id_dompet!="0"){
        $db_kas=new DBTable($db,"smis_fnc_kas");
        $hasil=$db_kas->select($termin->id_dompet);
        if($hasil!=null && $hasil->nomor_akun!=""){
            $kredit_akun=$hasil->nomor_akun;    
        }
    }*/
    
    //content untuk kredit / kas atau paten
    $kredit=array();
    $kredit['akun']     = $akun->k_piutang;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $invoice->total;
    $kredit['ket']      = "Pengakuan Piutang Invoice ".$invoice->kepada." Pada No. Invoice : ".$invoice->noref;
    $kredit['code']     = "kredit-piutang-faktur-".$invoice->id;

    //content untuk debet - patent
    $debet=array();
    $debet['akun']    = $akun->d_piutang;
    $debet['debet']   = $invoice->total;
    $debet['kredit']  = 0;
    $debet['ket']     = "Pengakuan Hutang Invoice ".$invoice->kepada." Pada No. Invoice : ".$invoice->noref;
    $debet['code']    = "debet-piutang-invoice-".$invoice->id;

    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $invoice->tanggal;
    $header['keterangan']   = "Pengakuan Piutang Invoice ".$invoice->kepada." Pada No. Invoice : ".$invoice->noref;
    $header['code']         = "piutang-invoice-".$invoice->id;
    $header['nomor']        = "FPIV-".$invoice->id;
    $header['debet']        = $invoice->total;
    $header['kredit']       = $invoice->total;
    $header['io']           = "0";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>