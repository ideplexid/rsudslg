<?php 
    show_error();
    global $db;
    $id             = $_POST['data'];
    $dbtable        = new DBTable($db,"smis_fnc_bayar_bulk");
    $bayar_bulk     = $dbtable->selectEventDel($id);    
    $akun_db        = new DBTable($db,"smis_fnc_mapping_vendor");
    $akun           = $akun_db->selectEventDel(array("id_vendor"=>$bayar_bulk->id_vendor) );
    
    $kredit_akun         = $akun->kb_hutang;
    if($bayar_bulk->id_dompet!="0"){
        $db_kas          = new DBTable($db,"smis_fnc_kas");
        $hasil           = $db_kas->select($bayar_bulk->id_dompet);
        if($hasil!=null && $hasil->nomor_akun!=""){
            $kredit_akun = $hasil->nomor_akun;    
        }
    }
    $list               = array();
    
    //content untuk kredit / kas atau paten
    $kredit             = array();
    $kredit['akun']     = $kredit_akun;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $bayar_bulk->total;
    $kredit['ket']      = "Pembayaran Hutang Faktur Bulk ".$bayar_bulk->nama_vendor." Pada No. Bulk : ".$bayar_bulk->id;
    $kredit['code']     = "kredit-bayar-faktur-bulk-".$bayar_bulk->id;
    $list[]             = $kredit;
    
    /**ini bisa lebih dari satu */
    $bayar_dbtable    = new DBTable($db,"smis_fnc_bayar");
    $bayar_dbtable    ->addCustomKriteria("id_bulk","='".$id."'");
    $bayar_dbtable    ->setShowAll(true);
    $result           = $bayar_dbtable->view("","0");
    $data             = $result['data'];
    foreach($data as $bayar_faktur){
        $debet            = array();
        $debet['akun']    = $akun->d_hutang;
        $debet['debet']   = $bayar_faktur->nilai;
        $debet['kredit']  = 0;
        $debet['ket']     = "Pembayaran Hutang Faktur ".$bayar_faktur->nama_vendor." Pada No. Faktur : ".$bayar_faktur->noref;
        $debet['code']    = "debet-bayar-faktur-bulk-".$bayar_faktur->id;
        $list[]           = $debet;
    }

    //content untuk header
    $header                 = array();
    $header['tanggal']      = $bayar_bulk->tgl_cair;
    $header['keterangan']   = "Pembayaran Hutang Faktur Bulk ".$bayar_bulk->nama_vendor." Pada No. Bukti : ".$bayar_bulk->pynum;
    $header['code']         = "bayar-faktur-bulk-".$id;
    $header['nomor']        = "FPBF-".$id;
    $header['debet']        = $bayar_bulk->total;
    $header['kredit']       = $bayar_bulk->total;
    $header['io']           = "0";
    
    $transaction            = array();
    $transaction['header']  = $header;
    $transaction['content'] = $list;
    
    $final                  = array();
    $final[]                = $transaction;
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']     = 1;
    $id['id']               = $_POST['data'];
    $dbtable->update($update,$id);

?>