<?php 
    show_error();
    global $db;
    $id             = $_POST['data'];
    $dbtable        = new DBTable($db,"smis_fnc_bayar_bulk");
    $bayar_bulk     = $dbtable->selectEventDel($id);    
    $list           = array();

    //default sesuai pilihan akun
    $no_kredit        = $bayar_bulk->no_akun;
    if($no_kredit==""){
        //jika tidak dipilih sesuai akun kas, akan diambil sesuai kredit jenis tagihan
        $no_kredit=$x->kredit;
        if($no_kredit==""){
            //jika kosong juga makan akan ikut default
            $no_kredit  = getSettings($db,"finance-accounting-kredit-pembayaran-umum","");
        }
    }
    
    $kredit             = array();
    $kredit['akun']     = $no_kredit;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $bayar_bulk->total;
    $kredit['ket']      = "Pembayaran Umum Bulk ".$bayar_bulk->nama_vendor." Pada No. Bulk : ".$bayar_bulk->id;
    $kredit['code']     = "kredit-bayar-umum-bulk-".$bayar_bulk->id;
    $list[]             = $kredit;

    /**ini bisa lebih dari satu */
    $bayar_dbtable    = new DBTable($db,"smis_fnc_bayar");
    $bayar_dbtable    ->addCustomKriteria("id_bulk","='".$id."'");
    $bayar_dbtable    ->setShowAll(true);
    $result           = $bayar_dbtable->view("","0");
    $data             = $result['data'];
    foreach($data as $bayar_umum){
        $debet            = array();
        $debet['akun']    = $bayar_umum->debet!=""?$bayar_umum->debet:getSettings($db,"finance-accounting-debet-pembayaran-umum","");
        $debet['debet']   = $bayar_umum->nilai;
        $debet['kredit']  = 0;
        $debet['ket']     = "Pembayaran Umum ".$bayar_umum->nama_vendor." Pada No. Ref : ".$bayar_umum->noref;
        $debet['code']    = "debet-bayar-umum-bulk-".$bayar_umum->id;
        $list[]           = $debet;
    }

    //content untuk header
    $header                 = array();
    $header['tanggal']      = $bayar_bulk->tgl_cair;
    $header['keterangan']   = "Pembayaran Umum Bulk ".$bayar_bulk->nama_vendor." Pada No. Bukti : ".$bayar_bulk->pynum;
    $header['code']         = "bayar-umum-bulk-".$id;
    $header['nomor']        = "FPBU-".$id;
    $header['debet']        = $bayar_bulk->total;
    $header['kredit']       = $bayar_bulk->total;
    $header['io']           = "0";
    
    $transaction            = array();
    $transaction['header']  = $header;
    $transaction['content'] = $list;
    
    $final                  = array();
    $final[]                = $transaction;
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']     = 1;
    $id['id']               = $_POST['data'];
    $dbtable->update($update,$id);

?>