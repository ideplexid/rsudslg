<?php 
    global $db;
    $id      = $_POST['data'];
    
    $dbtable = new DBTable($db,"smis_fnc_invoice_bayar");
    $bayar = $dbtable->selectEventDel($id);
    
    $dbtable = new DBTable($db,"smis_fnc_invoice");
    $invoice = $dbtable->selectEventDel($bayar->id_invoice);
    
    
    $akun_db = new DBTable($db,"smis_fnc_mapping_vendor");
    $akun=$akun_db->selectEventDel(array("id_vendor"=>$invoice->id_vendor) );
    
   
    $debet_akun=$akun->db_piutang;
    if($invoice->id_dompet!="0"){
        $db_kas=new DBTable($db,"smis_fnc_kas");
        $hasil=$db_kas->select($invoice->id_dompet);
        if($hasil!=null && $hasil->nomor_akun!=""){
            $debet_akun=$hasil->nomor_akun;    
        }
    }
    
    
    //content untuk kredit
    $kredit=array();
    $kredit['akun']     = $akun->kb_piutang;
    $kredit['debet']    = 0;
    $kredit['kredit']   = $bayar->nilai;
    $kredit['ket']      = "Pemabayaran Invoice ".$invoice->kepada." Pada No. Invoice : ".$invoice->noref;
    $kredit['code']     = "kredit-bayar-invoice-".$bayar->id;

    //content untuk debet - patent
    $debet=array();
    $debet['akun']    = $debet_akun;
    $debet['debet']   = $bayar->nilai;
    $debet['kredit']  = 0;
    $debet['ket']     = "Pemabayaran Invoice ".$invoice->kepada." Pada No. Invoice : ".$invoice->noref;
    $debet['code']    = "debet-bayar-invoice-".$bayar->id;

    
    $list   = array();
    $list[] = $debet;
    $list[] = $kredit;

    //content untuk header
    $header=array();
    $header['tanggal']      = $bayar->tanggal_bayar;
    $header['keterangan']   = "Pengakuan Piutang Invoice ".$invoice->kepada." Pada No. Invoice : ".$invoice->noref;
    $header['code']         = "bayar-invoice-".$bayar->id;
    $header['nomor']        = "FBIV-".$bayar->id;
    $header['debet']        = $bayar->nilai;
    $header['kredit']       = $bayar->nilai;
    $header['io']           = "0";
    
    $transaction=array();
    $transaction['header']=$header;
    $transaction['content']=$list;
    
    $final=array();
    $final[]=$transaction;
        
    echo json_encode($final);
    
    /*notif that already change or crawler by akunting*/
    $update['akunting']=1;
    $id['id']=$_POST['data'];
    $dbtable->update($update,$id);
?>