<?php
	global $wpdb;
	
	require_once 'smis-libs-inventory/install.php';
	$install = new InventoryInstallator($wpdb, "", "");
	$install->setUsing(false, true, true);
	$install->extendInstall("fnc");
	$install->install();
		
	require_once("finance/resource/install/table/smis_fnc_akun.php");
	require_once("finance/resource/install/table/smis_fnc_kas.php");
	require_once("finance/resource/install/table/smis_fnc_beban.php");
	require_once("finance/resource/install/table/smis_fnc_bayar_bulk.php");
	require_once("finance/resource/install/table/smis_fnc_bayar.php");
	require_once("finance/resource/install/table/smis_fnc_piutang.php");
	require_once("finance/resource/install/table/smis_fnc_piutang_per_ruang.php");
	require_once("finance/resource/install/table/smis_fnc_radiology.php");
	require_once("finance/resource/install/table/smis_fnc_resep.php");
	require_once("finance/resource/install/table/smism_fnc_tindakan_perawat.php");	
	require_once("finance/resource/install/table/smis_fnc_termin_gaji.php");
	require_once("finance/resource/install/table/smis_fnc_termin_gaji_detail.php");
    require_once("finance/resource/install/table/smis_fnc_invoice.php");
	require_once("finance/resource/install/table/smis_fnc_invoice_detail.php");
    require_once("finance/resource/install/table/smis_fnc_invoice_bayar.php");
    require_once("finance/resource/install/table/smis_fnc_mapping_employee.php");
    require_once("finance/resource/install/table/smis_fnc_mapping_vendor.php");
    require_once("finance/resource/install/view/smis_fnc_rekap_radiology.php");
	
?>
