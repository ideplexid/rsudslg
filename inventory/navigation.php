<?php
global $db;
global $NAVIGATOR;

require_once 'smis-libs-inventory/navigation.php';
$prototype = get_all_prototype ( $db, "inventory" );
foreach ( $prototype as $p ) {
	// Administrator Top Menu
	$mr = new Menu ("fa fa-cloud");
	$mr->addProperty ( 'title', $p->nama);
	$mr->addProperty ( 'name', $p->nama );
	
	require_once 'smis-libs-inventory/navigation.php';
	$inventory_navigator = new InventoryNavigator($NAVIGATOR, $p->keterangan, $p->nama, $p->slug);
	$mr = $inventory_navigator->extendMenu($mr,$p->nama,$p->slug,"inventory");
	$NAVIGATOR->addMenu ( $mr, $p->slug  );
		
}

?>