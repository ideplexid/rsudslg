<?php
global $PLUGINS;

$init ['name'] = 'inventory';
$init ['path'] = SMIS_DIR . "inventory/";
$init ['description'] = "Template Dari Sebuah Inventory untuk Menghandle Masalah Stok Barang Merupakan sebuah Prototype";
$init ['require'] = "administrator, inventory";
$init ['service'] = "";
$init ['version'] = "1.0.0";
$init ['number'] = "1";
$init ['type'] = "prototype";

$myplugins = new Plugin ( $init );
$PLUGINS [$init ['name']] = $myplugins;
?>