<?php 
require_once ("smis-framework/smis/template/NavigatorTemplate.php");
class InventNavigator extends NavigatorTemplate {
	public function __construct($navigation, $tooltip, $name, $page) {
		parent::__construct ( $navigation, $tooltip, $name, $page,"fa fa-truck" );
	}
	public function topMenu() {
		$this->addPrototype ( $this->name, $this->page, "rawat", "Antrian of " . $this->name, "Antrian", "antrian" ,"fa fa-sort-alpha-asc");
		
	}
}
?>