<?php
	require_once 'smis-libs-class/Policy.php';
	global $user;
	$slug = $_POST ['prototype_slug'];
	$implement = $_POST ['prototype_implement'];
	
	require_once 'smis-libs-inventory/policy.php';
	$ipolicy=new InventoryPolicy($slug, $user);
	$ipolicy->setPrototype(true, $implement);
	
	
	$policy=new Policy($slug, $user);
	$policy->setPrototype(true,$implement);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	
	$policy->combinePolicy($ipolicy);
	$policy->initialize();
?>