<?php
	global $db;
	if (isset($_POST['id_obat']) && isset($_POST['ruangan']) ) { 
		$dbtable = new DBTable($db, "smis_ivt_stok_obat_" . $_GET['entity']);
		// get actual item stock :
		$row = $dbtable->get_row("
			SELECT id_obat, SUM(sisa) AS 'sisa', satuan
			FROM smis_ivt_stok_obat_" . $_GET['entity'] . " a
			LEFT JOIN smis_ivt_obat_masuk_". $_GET['entity']." b
			ON a.id_obat_masuk=b.id
			WHERE 
			a.prop NOT LIKE 'del' 
			AND b.status='sudah'
			AND id_obat = '" . $_POST['id_obat'] . "' 
			AND satuan = satuan_konversi AND konversi = '1'
			GROUP BY id_obat, satuan, satuan_konversi
			LIMIT 0, 1
		");
		$data = array(
			"jumlah" 	=> $row != null ? $row->sisa : 0,
			"satuan"	=> $row != null ? $row->satuan : "-"
		);		
		echo json_encode($data);
	}
	
?>