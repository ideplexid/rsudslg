<?php
	global $db;
	if (isset($_POST['id_obat']) && isset($_POST['jumlah']) && isset($_POST['satuan']) && 
		isset($_POST['keterangan']) && isset($_POST['user'])) {
		$boi_entity = $_GET['entity'];
		
		$id_obat = $_POST['id_obat'];
		$jumlah = $_POST['jumlah'];
		$satuan = $_POST['satuan'];
		$keterangan = $_POST['keterangan'];
		$operator = $_POST['user'];
		$data = array();

		$stok_dbtable = new DBTable($db, "smis_ivt_stok_obat_" . $boi_entity);
		// get actual item stock :
		$row = $stok_dbtable->get_row("
			SELECT SUM(sisa) AS 'sisa', GROUP_CONCAT(id) AS 'daftar_id_stok'
			FROM smis_ivt_stok_obat_" . $boi_entity . "
			WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '1' AND satuan_konversi = '" . $satuan . "' AND sisa > 0
		");
		if ($row == null) {
			// tidak tersedia id_obat yang diminta
			$data['success'] = 0;
			$data['reason'] = "Obat tidak ditemukan.";
			$data['stok'] = 0;
			$data['id_transaksi'] = 0;
			echo json_encode($data);
			return;
		}
		$sisa = $row->sisa;
		$daftar_id_stok = $row->daftar_id_stok;
		if ($sisa < $jumlah) {
			// stok tidak mencukupi
			$data['success'] = 0;
			$data['reason'] = "Stok tidak mencukupi. Stok tersedia: " . $sisa . ", permintaan penggunaan: " . $jumlah . ".";
			$data['stok'] = $stok;
			$data['id_transaksi'] = 0;
			echo json_encode($data);
			return;
		}
		$id_stok_arr = explode(",", $daftar_id_stok);
		$id_transaksi = "";
		$penggunaan_obat_dbtable = new DBTable($db, "smis_ivt_penggunaan_obat_" . $boi_entity);
		$riwayat_stok_obat_dbtable = new DBTable($db, "smis_ivt_riwayat_stok_obat_" . $boi_entity);
		$sisa_permintaan = $jumlah;
		foreach ($id_stok_arr as $id_stok) {
			if ($sisa_permintaan == 0)
				break;
			// menghitung stok sisa, stok digunakan, dan sisa permintaan:
			$row = $stok_dbtable->select($id_stok);
			$stok_sisa = $row->sisa;
			$stok_digunakan = 0;
			if ($stok_sisa > $sisa_permintaan) {
				$stok_digunakan = $sisa_permintaan;
				$stok_sisa = $stok_sisa - $sisa_permintaan;
				$sisa_permintaan = 0;
			} else {
				$stok_digunakan = $stok_sisa;
				$sisa_permintaan = $sisa_permintaan - $stok_sisa;
				$stok_sisa = 0;
			}
			// simpan transaksi penggunaan:
			$data_penggunaan = array(
				"id_stok_obat"	=> $id_stok,
				"tanggal"		=> date("Y-m-d"),
				"jumlah"		=> $stok_digunakan,
				"keterangan"	=> $keterangan
			);
			$penggunaan_obat_dbtable->insert($data_penggunaan);
			$id_penggunaan_obat = $penggunaan_obat_dbtable->get_inserted_id();
			// update stok:
			$data_stok = array(
				"sisa" 	=> $stok_sisa
			);
			$data_id_stok = array(
				"id"	=> $id_stok
			);
			$stok_dbtable->update($data_stok, $data_id_stok);
			// simpan riwayat stok:
			$data_riwayat_stok_obat = array(
				"tanggal"		=> date("Y-m-d"),
				"id_stok_obat"	=> $id_stok,
				"jumlah_masuk"	=> 0,
				"jumlah_keluar"	=> $stok_digunakan,
				"sisa"			=> $stok_sisa,
				"keterangan"	=> $keterangan,
				"nama_user"		=> $operator
			);
			$riwayat_stok_obat_dbtable->insert($data_riwayat_stok_obat);
			// concate id_transaksi untuk kembalian nilai
			$id_transaksi .= $id_penggunaan_obat . ",";
		}
		$id_transaksi = rtrim($id_transaksi, ",");
		// get actual item stock :
		$row = $stok_dbtable->get_row("
			SELECT SUM(sisa) AS 'sisa'
			FROM smis_ivt_stok_obat_" . $boi_entity . "
			WHERE prop NOT LIKE 'del' AND id_obat = '" . $id_obat . "' AND satuan = '" . $satuan . "' AND konversi = '1' AND satuan_konversi = '" . $satuan . "'
		");
		$data = array(
			"success"		=> 1,
			"reason"		=> "",
			"stok"			=> $row->sisa,
			"id_transaksi"	=> $id_transaksi
		);
		echo json_encode($data);
	}
?>