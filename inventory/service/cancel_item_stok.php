<?php
	global $db;
	if (isset($_POST['id_obat']) && isset($_POST['jumlah']) && isset($_POST['satuan']) &&
		isset($_POST['keterangan']) && isset($_POST['user']) && isset($_POST['id_transaksi']) ) {
		$jumlah = $_POST['jumlah'];
		$keterangan = $_POST['keterangan'];
		$operator = $_POST['user'];
		$id_transaksi = $_POST['id_transaksi'];
		$boi_entity = $_GET['entity'];

		$id_transaksi_arr = explode(",", $id_transaksi);
		$penggunaan_obat_dbtable = new DBTable($db, "smis_ivt_penggunaan_obat_" . $boi_entity);
		$stok_obat_dbtable = new DBTable($db, "smis_ivt_stok_obat_" . $boi_entity);
		$riwayat_stok_obat_dbtable = new DBTable($db, "smis_ivt_riwayat_stok_obat_" . $boi_entity);
		foreach ($id_transaksi_arr as $id_transaksi_d) {
			// mendapatkan id_stok_obat dan stok digunakan pada transaksi penggunaan obat:
			$row = $penggunaan_obat_dbtable->select($id_transaksi_d);			
			$id_stok_obat = $row->id_stok_obat;
			$stok_digunakan = $row->jumlah;
			// membatalkan transaksi penggunaan:
			$penggunaan_obat_data = array(
				"prop"	=> "del"
			);
			$penggunaan_obat_id_data = array(
				"id" => $id_transaksi_d
			);
			$penggunaan_obat_dbtable->update($penggunaan_obat_data, $penggunaan_obat_id_data);
			// get actual item stock :
			$row = $stok_obat_dbtable->select($id_stok_obat);
			$stok_sisa = $row->sisa + $stok_digunakan;
			// update stok obat:
			$stok_obat_data = array(
				"sisa"	=> $stok_sisa
			);
			$stok_obat_id = array(
				"id"	=> $id_stok_obat
			);
			$stok_obat_dbtable->update($stok_obat_data, $stok_obat_id);
			// simpan riwayat stok:
			$riwayat_stok_obat_data = array(
				"tanggal"		=> date("Y-m-d"),
				"id_stok_obat"	=> $id_stok_obat,
				"jumlah_masuk"	=> $stok_digunakan,
				"jumlah_keluar"	=> 0,
				"sisa"			=> $stok_sisa,
				"keterangan"	=> $keterangan,
				"nama_user"		=> $operator
			);
			$riwayat_stok_obat_dbtable->insert($riwayat_stok_obat_data);
		}
		$data = array(
			"success"	=> 1,
			"reason"	=> ""
		);
		echo json_encode($data);
	}
?>