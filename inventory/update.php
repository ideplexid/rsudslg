<?php
global $db;
global $wpdb;
    
$proto=getAllActivePrototypeOf($db, "inventory");
foreach($proto as $slug=>$name){
    require_once 'smis-libs-inventory/update.php';
    $inventory=new InventoryInstallator($wpdb, $slug, "");
    $inventory->extendInstall("ivt");
    $inventory->install();
}

?>