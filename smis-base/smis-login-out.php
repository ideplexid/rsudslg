<?php 
	global $user;
	$response=new ResponsePackage();
	$response->setAlertVisible(true);
	$response->setStatus(ResponsePackage::$STATUS_LOGOUT);
	$response->setAlertContent("You Are Now Logout","You Are Log Out",ResponsePackage::$TIPE_INFO);
	$response->setContent("");
	setChangeCookie(false);
	if($user!=null)
		$user->logout();
	logout();
	echo json_encode($response->getPackage());
?>