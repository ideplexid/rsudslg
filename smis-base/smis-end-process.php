<?php 
/**
 * this is the last processing place 
 * of the system, like running a 
 * psudo crontab job, close the databse, save the log
 * and etc. 
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	
global $querylog;
global $notification;
global $db;

if($querylog!=null) {
	if(!is_serverbus_request ()){
		require_once 'smis-libs-class/CronJob.php';
		$cron = new CronJob($db);
		$cron ->start();
	}
	
	if($notification!=null) {
		$notification->commit();
	}
	$querylog ->save();
}
DBConnector::close();
?>