<?php 
global $db;
if(getSettings($db,"override_php_ini","0")=="1"){
    $max_execution_time=getSettings($db,"max_execution_time",30);
    $memory_limit=getSettings($db,"memory_limit",'16M');
    ini_set("max_execution_time",$max_execution_time);
    ini_set("memory_limit",$memory_limit);
}

?>