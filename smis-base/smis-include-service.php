<?php
 
/** 
 * this one used for acativating the smis system
 * instead of using reguler reuirement like 
 * smis-base/smis-include-system
 * this one used only in service purpose
 * 
 * 
 * @author goblooge
 * @since 12 Januari 2014
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @version 3.3
 * @used	- smis-base/smis-service-loader.php
 * 			- smis-base/smis-service.php
 * 			- smis-serverbus/autonomous.php
 * 			- smis-serverbus/service.php
 *          - index.php
 * */

	require_once("smis-framework/smis/database/Database.php");
	require_once("smis-framework/smis/database/DBTable.php");
	require_once("smis-framework/smis/log/User.php");
	require_once('smis-framework/smis/connection/Communication.php');
	
	include_once 'smis-framework/smis/connection/PackageComunication.php';
	include_once 'smis-framework/smis/connection/Communication.php';
	include_once 'smis-framework/smis/connection/SendRequest.php';
	include_once 'smis-framework/smis/package/ResponsePackage.php';
	
?>