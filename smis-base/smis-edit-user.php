<?php 
/**
 * this used for edit password 
 * and edit user settings all at once
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	
if(isset($_POST['edit'])){	
	if($_POST['edit']=="password" && isset($_POST['old']) && isset($_POST['pass'])){
		$iduser		= getSession('userid');
		$old		= md5($_POST['old']);
		$new		= md5($_POST['pass']);
		$user		= new User();
		$user->loadUser($iduser);
		$update		= $user->updatePassword($old,$new);
		$response=new ResponsePackage();
		if($update){
			$response ->setStatus("ok")
					  ->setAlertVisible(true)
					  ->setAlertContent("Success","Password Updated","alert-warning");
		}else{
			$response ->setWarning(true,"Failed","Old Password Wrong ");
		}
		echo json_encode($response->getPackage());
	}else if($_POST['edit']=="settings"){
		global $user;
		$response	= new ResponsePackage();
		$user 		->saveTimeOut($_POST['timeout'])
					->saveNavigatorMode($_POST['st_mmode'])
					->saveSliderMode($_POST['st_smode'])
					->saveColorSlide($_POST['color_slider'])
					->saveColorInvert($_POST['color_invert'])
					->saveColorGrayscale($_POST['color_grayscale'])
					->saveCSS($_POST['css']);
		$response	->setStatus("ok")
					->setAlertVisible(true)
					->setAlertContent("Success","Timeout Update","alert-info");
		echo json_encode($response->getPackage());
	}
}

	

?>