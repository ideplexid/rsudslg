<?php 

/**
 * this is realtime mode search in here, for check the user login mode
 * check is user still have the session and etc. 
 * it will show many thing for user including message ping.
 * 
 * 
 * @author goblooge
 * @license LGL v3
 * @copyright Nurul Huda <goblooge@gmail.com>
 * @since 17 Nov 2014
 * @version 1.0
 * 
 */

global $db;
global $user;
$message=array();

$realtime_set=getSettings($db, "smis-enable-realtime", "0");
$timeout_realtime=getSettings($db, "smis-realtime", "30000");
$message['realtime']=$realtime_set=="0"?"0":$timeout_realtime;
$message['notification']=$user->getNotification();

$response=new ResponsePackage();
$response->setStatus(ResponsePackage::$STATUS_OK);
$response->setContent($message);

echo json_encode($response->getPackage());




?>