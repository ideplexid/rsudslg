<?php
/** 
 * this one is only usefull or used in refresh (first load) and plugins that need to access
 * PLUGINS and Navigator only Such
 * Administrator->Plugins
 * Administrator->Log
 * Administrator->Authorization
 * check here if the action is three of them
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */
global $PLUGINS;
global $NAVIGATOR;
global $CACHED_SETTINGS;
loadLibrary("smis-libs-function-system");
init_database($db);
init_plugins($db);
init_navigation();

?>