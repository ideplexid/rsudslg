<?php

/**
 * used for include all file 
 * that need to build a smis program
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	 

/*base ui*/
require_once("smis-framework/smis/ui/base/Html.php");
require_once("smis-framework/smis/ui/base/Button.php");
require_once("smis-framework/smis/ui/base/Label.php");
require_once("smis-framework/smis/ui/base/Img.php");
require_once("smis-framework/smis/ui/base/File.php");
require_once("smis-framework/smis/ui/base/CheckBox.php");
require_once("smis-framework/smis/ui/base/Hidden.php");
require_once("smis-framework/smis/ui/base/Select.php");
require_once("smis-framework/smis/ui/base/Text.php");
require_once("smis-framework/smis/ui/base/TextArea.php");
require_once("smis-framework/smis/ui/base/Password.php");
require_once("smis-framework/smis/ui/base/Table.php");

/*api*/
require_once("smis-framework/smis/api/Plugin.php");

/*composite ui*/
require_once("smis-framework/smis/ui/composite/Navigator.php");
require_once("smis-framework/smis/ui/composite/Menu.php");
require_once("smis-framework/smis/ui/composite/ButtonGroup.php");
require_once("smis-framework/smis/ui/composite/InputGroup.php");
require_once("smis-framework/smis/ui/composite/Form.php");
require_once("smis-framework/smis/ui/composite/Modal.php");
require_once("smis-framework/smis/ui/composite/Report.php");
require_once("smis-framework/smis/ui/composite/Pagination.php");
require_once("smis-framework/smis/ui/composite/Tabulator.php");
require_once("smis-framework/smis/ui/composite/TablePrint.php");
require_once("smis-framework/smis/ui/composite/Tree.php");
require_once("smis-framework/smis/ui/composite/RowSpan.php");
require_once("smis-framework/smis/ui/composite/LoadingBar.php");
require_once("smis-framework/smis/ui/composite/Alert.php");
require_once("smis-framework/smis/ui/composite/Panel.php");
require_once("smis-framework/smis/ui/factory/ComponentFactory.php");
?>