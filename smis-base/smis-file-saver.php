<?php
/**
 * this is used for saving file that uploaded by a user.
 * via system upload.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	

$response	= new ResponsePackage();
$response	->setStatus(ResponsePackage::$STATUS_OK);
global $user;
if($user->isAdministrator() && isset($_POST['filepath']) && !is_dir($_POST['filepath'])){
	$em		= "";
	$handle = fopen($_POST['filepath'], "w");
	$hasil	= false;
	try{
		$hasil	= fwrite($handle, $_POST['filecontent']);
	}catch(Exception $e){
		$em		= $e->getMessage();
	}
	fclose($handle);		
	if($hasil!==false && $em==""){
		$response	->setAlertVisible(true)
					->setAlertContent("Save Success", "File ".$_POST['filepath']." Save Successfully !! ");
		echo json_encode($response->getPackage());
		return;
	}
}	
$content = "Error Saving File. 
			<ul>
				<li>You are Not Administrator</li>
				<li>File is a Folder</li>
				<li>File Not Writable</li>
				<li>".getcwd()."/".$_POST['filepath']." Not Writable</li>
				".($em!=""?"<li>".$em."</li>":"")."
			</ul>";
$response	->setAlertVisible(false)
			->setWarning("y", "Error Saving File ".(isset($_POST['filepath'])?$_POST['filepath']:""), $content);
echo json_encode($response->getPackage());
?>