<?php 

/**
 * this file used for initializin the
 * smis basic varible but only for service purpose
 * the different is, in here not need the ui
 * 
 * @used 		: - smis-base/smis-service-loader.php
 *				  - smis-base/smis-service.php
 *				  - smis-serverbus/service.php
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @version		: 1.0.1
 * @since		: 14 Mar 2014
 * @copyright	: LGPLv3
 * */

global $user;
global $wpdb;
global $db;
global $querylog;

$dbconnector=new DBConnector(SMIS_SERVER,SMIS_DATABASE,SMIS_USERNAME,SMIS_PASSWORD);
$result=$dbconnector->connect();
$wpdb=new DBController();
$user=new User();
$user->setRawUser("SERVER BUS", "NOT SET");
$querylog=new QueryLog($wpdb, $user);
$db=new Database($wpdb,$user,$querylog);
$notification=new Notification($db);
?>