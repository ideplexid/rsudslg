<?php 
/**
 * this file used when a modul need 
 * a service consumer instead calling all modul
 * better calling a spesific class * 
 * should be require when using service
 * 
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @version 	: 12.0.1
 * @license 	: LGPLv3
 * @since		: 14 februari 2016
 * */
 
	require_once("smis-framework/smis/template/ModulTemplate.php");
	require_once 'smis-framework/smis/service/ServiceConsumer.php';
	require_once 'smis-framework/smis/service/ServiceResponder.php';
	require_once 'smis-framework/smis/connection/PackageComunication.php';
	require_once 'smis-framework/smis/connection/SendRequest.php';
	require_once 'smis-framework/smis/connection/Communication.php';
?>