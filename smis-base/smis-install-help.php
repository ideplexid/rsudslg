<div class='alert alert-info'>
	<button type="button" class="close" data-dismiss="alert"><i class='icon-black icon-remove'></i></button>
	<h4> Installation Instruction </h4>
	<p style="text-align: justify; "><span style="text-decoration: underline; font-weight: bold; ">Before install you need to make sure several thing</span></p>
<ul>
<li style="text-align: justify;"><span > This software was based on PHP Language and compatible with Apache Server 3.4 and up.</span></li>
<li style="text-align: justify; "><span > This Software use MySQL as the Database System and compatible with MySQL 4.x and up.&nbsp;</span></li>
</ul>
<p style="text-align: justify;"><span style="text-decoration: underline; font-weight: bold; ">step by step of installation</span></p>
<ul>
<li style="text-align: justify;"><span >fill the <span style="font-style: italic;">server filed</span>&nbsp;with the ip address of MySQL Server Machine, if you use same machine with the Apache Server, fill it as "<span style="font-weight: bold; font-style: italic;">localhost</span>" without a quote.</span></li>
<li style="text-align: justify;"><span >fill the <span style="font-style: italic;">database field</span> with the database name that you want to use as  Database, you must make sure that the database is already create.</span></li>
<li style="text-align: justify;"><span >fill the <span style="font-style: italic;">username field </span>with the username authorization of MySQL Database (usually <span style="font-style: italic;"><span style="font-weight: bold;">root</span>&nbsp;</span>but it's <span style="font-weight: bold;">strongly</span>&nbsp;recomended not to use root as the username, create another database username).</span></li>
<li style="text-align: justify;"><span >the username must have authorization to create table, create view, insert, update and delete.</span></li>
<li style="text-align: justify;"><span >fill the <span style="font-style: italic;">password field</span> with the password of user authorization of MySQL Database (root user usually don't have password, again, it's <span style="font-weight: bold;">strongly</span> recomended not using blank password for security reason).&nbsp;</span></li>
<li style="text-align: justify;"><span >choose the connector based mode, if you used PHP version below 5.5.0 then choose MYSQL, for PHP 5.5.0 and above choose MYSQLI or PDO</li>
<li style="text-align: justify;"><span >after all of that you can press the install button and let's  do the rest.</span></li>
<li style="text-align: justify;"><span >wait until a message come out that inform you about the installation. &nbsp;</span></li>
</ul>
<p style="text-align: justify;"><span style="text-decoration: underline; font-weight: bold; ">Installation Process</span></p>
<ol>
<li style="text-align: justify;"><span ><span style="line-height: 1.2;">when the installation begin, Installer will check is the database exist, if not then installation will fail, and "</span><span style="line-height: 1.2; font-weight: bold;">Error Connecting Database" </span><span style="line-height: 1.2;">message will be shown.</span><br></span></li>
<li style="text-align: justify;"><span >afer that Installer  will check is the user exist, is user have authorization, and are username and password correct. if not the installation will fail , and "<span style="font-weight: bold;">Error&nbsp;Connecting Database"&nbsp;</span>message&nbsp;will be shown.</span></li>
<li style="text-align: justify;"><span >if  success connect to database, then Installer will begin to installing based system on it's database, if user that you fill don't have any authorization of create table, create view, insert , update or delete. then installation will fail and "<span style="font-weight: bold;">Error Installing Database"</span> Message will be shown.</span></li>
<li style="text-align: justify;"><span >if  success to install it's based system this installation of actually succeeded, and  will create an configuration file name "smis-config.php" in directory "<span style="font-style: italic; text-decoration:underline; font-weight: bold;"><?php echo getcwd(); ?>/smis-base/smis-config.php</span>". if you the configuration successfully create, "<span style="font-weight: bold;">Success Message</span>" will be shown including the admin username and password. just reload (press F5) and login page will be shown. use&nbsp;admin &nbsp;username and&nbsp;password to login to system (by default username of administrator is "<span style="font-style: italic;">admin</span>" and password is "<span style="font-style: italic;">admin</span>" , both without quote). after login is strongly recomended <span style="font-weight: bold;">to change</span> the administrator password.&nbsp;</span></li>
<li style="text-align: justify;"><span > sometimes installation successfull but configuration file failed to create. it's cause by write access of the directory (the directory or the file is not <span style="font-style: italic;">writeable</span>). "<span style="font-weight: bold;">Error Creating Configuration File</span>" message will be shown, and you should create that file on your own. the content of the file will be shown within the textarea of the message, just simply copy and paste it (remember do no add anything to this configuration file, even a single space, dot or tab will destroy everything).</span></li>
<li style="text-align: justify;"><span >after you create the configuration file, just do like step 4.</span></li>
<li style="text-align: justify;"><span >Congratulation... This Software was Successfully Installed in your Machine.</span></li>
</ol>
	

</div>

