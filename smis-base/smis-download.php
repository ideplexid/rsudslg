<?php 
/** 
 * this used for downloading the file
 * from server, even file that have same name
 * will be write as different namefile in
 * the server, so when there another file that have
 * same name will not be overwriten. this download function 
 * used for rename the filename to it's original name
 * 
 * @since       : 14 Aug 2015
 * @version     : 1.0.2
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @license     : LGPLv2
 * 
 */

$actual_file_name   = "smis-upload/".$_POST['smis_download'];
require_once ("smis-libs-function/smis-libs-function-string.php");
$download_file_name = file_name_upload($_POST['smis_download']);
$mime = 'application/zip';

header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Cache-Control: private', false);
header('Content-Type: ' . $mime);
header('Content-Disposition: attachment; filename="'. $download_file_name .'"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($actual_file_name));
header('Connection: close');
readfile($actual_file_name);
exit();

?>