<?php 
	global $user;
	
	$name=getSettings($db, "smis_autonomous_title", "Safethree MIS");
	$modal=new Modal("smis-maintenance","",$name);
	$modal->setAlwaysShow(true);
	$logo=getLogo();
	
	$message=getSettings($db, "smis_maintenance_mode_msg", "Tidak Ada Pesan Dari Administrator");
	
	$msg="Saudara <strong>".(is_object($user)?"'".$user->getNameOnly()."'":"")."</strong> yang terhormat, 
			Untuk Sementara Sistem tidak dapat digunakan dikarenakan ada <strong>Maintenance Server</strong>.
			Pesan dari Administrator : 
			<div class='line'></div>
			$message
			";
	
	$rp=new RowSpan();
	$rp->addSpan("<div id='smis_logo' ><img src='".$logo."'></div>",2);
	$rp->addSpan("<div class='alert alert-info' >".$msg."</div>",10);
	
	$modal->addHTML($rp->getHtml(),"before");
	
	
	$button=new Button("", "", "Logout");
	$button->setIcon("fa fa-sign-out icon-white");
	$button->addClass("btn btn-primary");
	$button->setAction("logout()");
	$button->setIsButton(Button::$ICONIC_TEXT);	
	$modal->addFooter($button);
	$modal->setModalSize(Modal::$HALF_MODEL);
	
	echo "<div id='gap'> &nbsp; </div>";
	echo $modal->getHtml();

?>

<style type="text/css">
#license-area{
	width:460px !important;
}

#smis-login{
	top:3px !important;
	left:3px !important;
}


.modal-header{color:#b94a48;}
#smis-login > .modal-body > .modal-body-title { float:left; max-width:200px; }
#smis-login > .modal-body > .modal-body-form { float:left; max-width:300px;}
.smis_form > div > label{ text-transform: uppercase; font-weight:800; font-style: italic;} 
#gap{ margin-bottom:20px; }
#smis_logo{
	text-align: center;
}
</style>