<?php 

/**
 * this file used for initializin the
 * smis basic varible.
 * @used 		: - index.php
 *				  - smis-base/smis-upload.php
 *				  - smis-framework/smis/database/Database.php
 * @author 		: Nurul Huda
 * @copyright 	: goblooge@gmail.com
 * @version		: 1.0.1
 * @since		: 14 Mar 2014
 * @copyright	: LGPLv3
 * */

global $PLUGINS;
global $NAVIGATOR;
global $CACHED_SETTINGS;
global $user;
global $wpdb;
global $querylog;
global $db;
global $notification;
global $DEBUGGABLE;

$CACHED_SETTINGS=array();
$wpdb=new DBController();
$iduser=getSession('userid');
$user=new User();
$user->loadUser($iduser);
$querylog=new QueryLog($wpdb,$user);
$db=new Database($wpdb,$user,$querylog);
$notification=new Notification($db);
$querylog->setEnabled(getSettings($db,"smis_log_system","false"));
$NAVIGATOR=new Navigator();
$DEBUGGABLE=getSettings($db, "smis-debuggable-mode", "0");
?>