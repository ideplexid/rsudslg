<?php 

	function handleError($errno, $errstr, $errfile, $errline, array $errcontext){
		if (0 === error_reporting()) {
			return false;
		}
		throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
	}
	set_error_handler('handleError');

	$server=$_POST['server'];
	$username=$_POST['username'];
	$password=$_POST['password'];
	$database=$_POST['database'];
	$php_mode=$_POST['php_mode'];
	$config_name=$_POST['config_name'];
	define("SMIS_PHP_MODE",$php_mode);
    
	//connecting to database
	$dbconnector=new DBConnector($server,$database,$username,$password);
	$result=$dbconnector->connect();	
	
	if($result==null){
		$data=$data=array("msg"=>"
			<div class='alert alert-danger'>
				<button type='button' class='close' data-dismiss='alert'>x</button>
				<h4>Error Connecting Database</h4>
				<p>cannot connect to this database, perhaps it caused by : 
					<ul>
						<li>Database Version Incompatible</li>
						<li>It's Not MySQL Database</li>
						<li>Database Server Address is Wrong</li>
						<li>Database Server Down</li>
						<li>Database Not Exist</li>
						<li>Username or Password are wrong</li>
						<li>Your Computer Firewall or Antivirus are Blocking Access </li>
						<li>Connecting Mode was wrong, try another one</li>
					</ul>
					
					Error Message : ".$dbconnector->getError()."
				</p>
			</div>
			","status"=>"fail","");
		echo json_encode($data);
		return;
	}
	
	
	//installing system database;
	global $wpdb;
	$wpdb=new DBController();
	if(!defined('SMIS_DATABASE')) define('SMIS_DATABASE',$database);
	require_once 'smis-administrator/install.php';
	if(!$wpdb->is_installed()){
		$data=$data=array("msg"=>"
			<div class='alert alert-danger'>
				<button type='button' class='close' data-dismiss='alert'>x</button>
				<h4>Error Installing Database</h4>
				<p>cannot install in this database, perhaps it caused by : 
					<ul>
						<li>It's Not MySQL Database</li>
						<li>Database Version Incompatible</li>
						<li>Connecting Mode was wrong, try another one</li>
						<li>Database user have no authorization to create table, create view, insert, update or delete</li>
					</ul>
				</p>
			</div>
			","status"=>"fail","");
		
		echo json_encode($data);
		return;
	}
	
	//creating configuration file
	
	$file="smis-base/smis-config.php";
	$content="<?php 
			
		define('SMIS_SERVER','".$server."');
		define('SMIS_USERNAME','".$username."');
		define('SMIS_PASSWORD','".$password."');
		define('SMIS_DATABASE','".$database."'); 
		define('SMIS_PHP_MODE','".$php_mode."'); 
		define('SMIS_SESSION_PREFIX','".$database."'); 
		
				?>";
	
	
	$status=false;
	try{
		if(is_multisite()){			
			$service_content='<?php 
			
			define("MULTISITE","'.$config_name.'");
			$DIR=getcwd();
			SMIS_DIR=$DIR."/../../smis-serverbus/";
			chdir($SMIS_DIR);
			require_once ("service.php");
			
			?>';
								
			$index_content='<?php 
			define("MULTISITE","'.$config_name.'");
			require_once "index.php";
			?>';			
			$multisite_smis_config="<?php require_once \"smis-multisite/smis-config.php\";?>";
			$smis_config=file_put_contents($file, $multisite_smis_config);
			$cur_smis_config=file_put_contents(getcwd()."/smis-multisite/config/smis-config-".$config_name.".php", $content);
			$cur_server_bus=file_put_contents(getcwd()."/smis-multisite/service/smis-service-".$config_name.".php", $service_content);
			$cur_index=file_put_contents(getcwd()."/smis-index-".$config_name.".php", $index_content);
			$status=$cur_smis_config && $cur_server_bus && $cur_index;
		}else{
			$status=file_put_contents($file, $content);
		}
	}catch(Exception $e){
		$status=false;
	}
	
	if($status===false){
		$msg="<div class='alert alert-warning'>
				<button type='button' class='close' data-dismiss='alert'>x</button>
				<h4>Error Creating Configuration File</h4>";
		
		if(is_multisite()){
			$msg.="<p>Please Create File <strong>".getcwd()."/smis-base/smis-config.php</strong> and fill with these data...</p>
				<textarea style='width:100%;height:200px !important'>".$multisite_smis_config."</textarea>
				<p>Then Please Create Database Config File : <strong>".getcwd()."/smis-multisite/config/smis-config-".$config_name.".php</strong> and fill with theese data....</p>
				<textarea style='width:100%;height:200px !important'>".$content."</textarea>
				<p>Then Please Create Service File : </p>".getcwd()."/smis-multisite/serverbus/smis-service-".$config_name.".php</strong> and fill with theese data....</p>
				<textarea style='width:100%;height:200px !important'>".$service_content."</textarea>
				<p>Then Please Create Index File : </p>".getcwd()."/smis-index-".$config_name.".php</strong> and fill with theese data....</p>
				<textarea style='width:100%;height:200px !important'>".$index_content."</textarea>
				";
				
		}else{
			$msg.="<p>Please Create File <strong>".getcwd()."/smis-base/smis-config.php</strong> and fill with these data...</p>
					<textarea style='width:100%;height:200px !important'>".$content."</textarea>";
		}
		
		$msg.="<p>after creating the configuration file, reload this page by Pressing F5 and login using : 
					<ul>
						<li>username : <strong>admin</strong> </li>
						<li>password : <strong>admin</strong> </li>
					</ul>
					After you Successfully Login, it's Highly Recomended that you <strong>change</strong> the Admin Passowrd
				</p>
			</div>";
		
		$data=array("msg"=>$msg,"status"=>"fail","");
		echo json_encode($data);
		return;
	}
	
	
	//instalation successfull
	$data=array("msg"=>"
			<div class='alert alert-success'>
				<button type='button' class='close' data-dismiss='alert'>x</button>
				<h4>Instalation was successfull </h4>
				<p>Reload this page by Pressing F5 and Login using this account :
					<ul>
						<li>username : <strong>admin</strong> </li>
						<li>password : <strong>admin</strong> </li>
					</ul>
					After you Successfully Login, it's Highly Recomended that you <strong>change</strong> the Admin Passowrd
				</p>
			</div>
			","status"=>"success","");
	echo json_encode($data);
	return;
	
	
?>