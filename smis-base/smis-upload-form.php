<?php 
$modal=$_POST['modal'];
$element=$_POST['element'];
$model=$_POST['model'];//single or multiple
$filetype=$_POST['filetype'];//file type
$curval=$_POST['curval']; // current value of element
$multisite=is_multisite()?MULTISITE:"";

$icon="fa ";
switch ($filetype){
	case "image" : $icon.="fa-file-image-o"; break;
	case "video" : $icon.="fa-file-video-o"; break;
	case "audio" : $icon.="fa-file-audio-o"; break;
	case "archive" : $icon.="fa-file-archive-o"; break;
	case "code" : $icon.="fa-file-code-o"; break;
	case "document" : $icon.="fa-file-text-o"; break;
	case "all" : $icon.="fa-file-o"; break;
}

$btn_class="btn btn-".($model=="multiple"?"primary":"info");
$name_title=" Upload ".ucwords($model)." File".($model=="single"?"":"s")." of ".ucwords($filetype)." File";

$tpl="";
$cv=json_decode($curval,true);
if($cv!=null){
	foreach($cv as $file){
		$filename=file_name_upload($file);
		$id=str_replace(".", "_smis_", $filename);
		$nfile=$file;
		$size=upload_filesize("smis-upload/".$file);
	
		$tpl .= '<li id="'.$id.'" nfile="'.$nfile.'" ><i width="48" height="48px" class="fa fa-upload fa-3x"></i><input type="text" value="0" data-width="48" data-height="48"'.
				' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p>'.$filename.' <i>'.$size.'</i></p><span></span></li>';
	
	}
}

loadLibrary("smis-libs-function-javascript");
echo addJS("framework/bootstrap/js/jquery.ui.widget.js");
echo addJS("framework/bootstrap/js/jquery.iframe-transport.js");
echo addJS("framework/bootstrap/js/jquery.knob.js");
echo addJS("framework/bootstrap/js/jquery.fileupload.js");
echo addJS("base-js/smis-md5.js");
echo addCSS("framework/bootstrap/css/jquery.fileupload.css");

?>


<form id="upload" method="post" action="smis-base/smis-upload.php" enctype="multipart/form-data">
	<div id="drop">
		<a class='<?php echo $btn_class; ?>' data-toggle='popover' data-content='<?php echo $name_title; ?>'>
			<i class='<?php echo $icon;?>'></i> <?php echo $name_title; ?>
		</a>
		<input type="file" name="upl" />
		<input type='hidden' id='element_upload' value="<?php echo $element;?>">
		<input type='hidden' id='multisite' value="<?php echo $multisite;?>">
		<input type='text' id='element_upload_value' value="">
		<input type='text' id='element_upload_model' value="<?php echo $model; ?>">
		<input type='text' id='element_upload_filetype' value="<?php echo $filetype; ?>">
	</div>	
	<ul><?php echo $tpl; ?></ul>
</form>

<script type="text/javascript">
<?php echo arrayToJSVar("var array", $cv,false);?>
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
});
function removeFile(prt){

	bootbox.confirm("Anda yakin Ingin Menghapus ? Proses ini tidak dapat dikembalikan ", function(result) {
		   if(result){

			    var nfile=$(prt).attr("nfile");
			    var index=array.indexOf(nfile);
			    if (index > -1) {
			       array.splice(index, 1);
			       var p=JSON.stringify(array);
			        $("#element_upload_value").val(p);
			    }
			    remove_file(nfile);
			    $(prt).fadeOut(function(){
			        prt.remove();
			    });
			   
			    
		   }
		}); 
	
	
}

	$(document).ready(function(){
		var ul = $('#upload ul');		
		$('#upload ul li span').click(function(){
			var prt=$(this).parent();
			removeFile(prt);
        });
		
		
		
		<?php arrayToJSVar("array", $cv,false) ?>
	    $('#drop a').click(function(){
	        $(this).parent().find('input').click();
	    });

	   
    
	    // Initialize the jQuery File Upload plugin
	    $('#upload').fileupload({
	        dropZone: $('#drop'),
	        add: function (e, data) {
		        var id=data.files[0].name;
		        id=MD5(id);
	        	 var tpl = $('<li id="'+id+'" nfile="" class="working"><input type="text" value="0" data-width="48" data-height="48"'+
	             ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');
	             
	            tpl.find('p').text(data.files[0].name).append('<i>' + formatFileSize(data.files[0].size) + '</i>');
	            data.context = tpl.appendTo(ul);
	            tpl.find('input').knob();
	            
	            tpl.find('span').click(function(){
	                if(tpl.hasClass('working')){
	                    jqXHR.abort();
	                }else if(!tpl.hasClass('error')){
						removeFile(tpl);
		            }
	            });

	            // Automatically upload the file once it is added to the queue
	            var jqXHR = data.submit();
	        },
	        done: function (e, data) {
	        	var result=data._response.result;
	        	var json=$.parseJSON(result);
	        	
	        	if(json.status!='error'){
		        	if($("#element_upload_model").val()=="single"){
		        		$("#element_upload_value").val(json.file);
			        }else{
			        	array.push(json.file);
				        var p=JSON.stringify(array);
			        	$("#element_upload_value").val(p);
				    }	
		        	var id=data.files[0].name;
			        id=MD5(id);
		        	$("#"+id).attr("nfile",json.file);	        	      
		        	$("#modal_alert_smis-upload-modal").html("");
			    }else{
			    	data.context.addClass('reject');
			    	var data='<div class="alert alert-block alert-danger "><button type="button" class="close" data-dismiss="alert"> X </button><h4>Gagal</h4>'+json.msg+'</div>';
			    	$("#modal_alert_smis-upload-modal").html(data);
				}
			    
	        },

	        progress: function(e, data){
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            data.context.find('input').val(progress).change();
	            if(progress == 100){
	                data.context.removeClass('working');
	            }
	        },

	        fail:function(e, data){
	            data.context.addClass('error');
	        }

	    }).bind('fileuploadsubmit', function (e, data) {
	        data.formData = {
	                'filetype': $('#element_upload_filetype').val(),
					'multisite': $('#multisite').val()
	            };
	        });


	    // Prevent the default action when a file is dropped on the window
	    $(document).on('drop dragover', function (e) {
	        e.preventDefault();
	    });

	    // Helper function that formats the file sizes
	    function formatFileSize(bytes) {
	        if (typeof bytes !== 'number') {
	            return '';
	        }

	        if (bytes >= 1000000000) {
	            return (bytes / 1000000000).toFixed(2) + ' GB';
	        }

	        if (bytes >= 1000000) {
	            return (bytes / 1000000).toFixed(2) + ' MB';
	        }

	        return (bytes / 1000).toFixed(2) + ' KB';
	    }
		
	});
</script>


<style type="text/css">
	#upload #drop{text-align:center;font-family:'PT Sans Narrow', sans-serif;height:100px;border:dashed 2px #eee;padding:10px;}
	#drop a{padding:12px 26px;cursor:pointer;display:inline-block;margin-top:30px;line-height:1;}
	#upload #drop input{display:none;}#upload ul{list-style:none;margin-top:20px;margin-left:0px;margin-right:0px;}
	#upload ul li{background-color: #F6F6F6;border: 1px solid #efefef;height: 52px;padding: 15px;position: relative;margin-top:2px}
	#upload ul li input{display: none;}
	#upload ul li p{overflow: hidden;white-space: nowrap;color: #000;font-size: 12px;font-weight: bold;position: absolute;top: 20px;left: 100px;}
	#upload ul li i{font-weight: normal;font-style:normal;color:#7f7f7f;display:block;}#upload ul li canvas{top: 15px;left: 32px;position: absolute;}
	#upload ul li span{width: 15px;height: 12px;background: url('smis-framework/bootstrap/img/icons.png') no-repeat;background-position: 0 -12px;position: absolute;top: 34px;right: 33px;cursor:pointer;}
	#upload ul li.working span{height: 16px;background-position: 0 0px;}
	#upload ul li.error p{color:yellow;}
	#upload ul li.reject p{color:red;}
</style>