<?php 

	if(isset($_COOKIE["smis-index"])):
		setcookie("smis-index", '', time()-7000000);
	endif;

	/*this is used for preparing instalation for smis */
	if(isset($_POST['username']) && isset($_POST['password'])){
		$username=$_POST['username'];
		$password=$_POST['password'];
		$uname = $wpdb->escapeString($username);
		$pass = md5($wpdb->escapeString($password));
	
		global $wpdb;
		$query="SELECT * FROM smis_adm_user WHERE username='$uname' AND password='$pass' AND prop!='del'";
		$user_row=$wpdb->get_row($query);
	
		if($user_row==null){
			//logn fail
			$response=new ResponsePackage();
			$response->setAlertVisible(true);
			$response->setAlertContent("Login Failed","please contact the administrator",ResponsePackage::$TIPE_INFO);
			$response->setContent("fail");
			echo json_encode($response->getPackage());
		}else{
			//login success
			$user=new User();
			$user->loadUser($user_row->id);
			$user->login();
			
			$response=new ResponsePackage();
			$response->setAlertVisible(true);
			$response->setAlertContent("Login Success","success",ResponsePackage::$TIPE_INFO);
			$response->setContent("success");
			$response->setStatus("ok");
			echo json_encode($response->getPackage());
		}
		return;
	
	}	
	require_once 'smis-libs-function/smis-libs-function-base.php';
	$name=getSettings($db, "smis_autonomous_title", "Safethree MIS");
	
	$modal=new Modal("smis-login","",$name);
	$modal->setAlwaysShow(true);
	$logo=getLogo();
	$modal->addHTML("<div id='smis_logo' ><img src='".$logo."'></div>","before");
	$tuser=new Text("username","username","");
	$tuser->addAtribute("autofocus");
	$modal->addElement("Username",$tuser);
	$modal->addElement("Password",new Password("password","password",""));
	
	$button=new Button("", "", "Login");
	$button->setIcon("fa fa-sign-in");
	$button->addClass("btn btn-primary");
	$button->setAction("login()");
	$button->setIsButton(Button::$ICONIC_TEXT);
	$modal->addFooter($button);
	
	if(getSettings($db,"smis-multisite-enabled","0")=="1"){
		$button=new Button("", "", "Site Map");
		$button->setIcon("fa fa-sitemap");
		$button->addClass("btn btn-inverse");
		$button->setAction("to_sitemap()");
		$button->setIsButton(Button::$ICONIC_TEXT);
		$modal->addFooter($button);
	}
	
	$res="<div id='body'>";
	if(getSettings($db, "smis-enable-registration", "0")=="1"){
		$button=new Button("", "", "Register");
		$button->setIcon("fa fa-qq");
		$button->addClass("btn btn-info");
		$button->setAction("registration.show_add_form()");
		$button->setIsButton(Button::$ICONIC_TEXT);
		$modal->addFooter($button);
		
		$menu=new OptionBuilder();
		$menu->add("Accounting",'{"accounting":{"account":"1","transaksi_harian":"1","transaksi_memorial":"1","laporan":"1"}}');
		$menu->add("Anggrek",'{"anggrek_ii_a_plus":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","boi":"1","pengaturan_stok_minmaks":"1"},"anggrek_ii_b":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"anggrek_vip2":{"antrian":"1","gizi":"1","layanan":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"anggrek_vip1":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Anyelir",'{"anyelir":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Ambulan",'{"ambulatory":{"ambulan":"1"}}');
		$menu->add("Apotek",'{"apotek":{"penjualan_resep":"1","penjualan_apotek_lain":"1","retur_penjualan_resep":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Bougenvile",'{"bougenvile_ii_a":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"bougenvile_ii_a_plus":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"bougenvile_ii_b":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","rkbu":"1","setting_layanan":"1","pengaturan_stok_minmaks":"1","boi":"1"},"bougenvile_iii":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Cempaka",'{"cempaka_ii_b":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","pengaturan_stok_minmaks":"1","rkbu":"1","boi":"1"},"cempaka_iii":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Fisiotherapy",'{"fisiotherapy":{"pemeriksaan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Gizi",'{"gizi":{"asuhan":"1","pesanan":"1","belanja":"1","menu_makanan":"1","diet":"1","settings":"1"}}');
		$menu->add("Gudang Umum",'{"gudang_umum":{"barang_masuk":"1","penyimpanan_barang":"1","inventarisasi_barang":"1","daftar_barang":"1","riwayat_stok_barang":"1","pengecekan_stok_ed":"1","penyesuaian_stok_barang":"1","permintaan_barang_unit":"1","barang_keluar":"1","retur_barang_unit":"1","retur_keluar":"1"}}');
		$menu->add("Gudang Farmasi",'{"gudang_farmasi":{"obat_reguler_masuk":"1","obat_sito_masuk":"1","obat_konsinyasi_masuk":"1","penyimpanan_obat":"1","daftar_obat":"1","riwayat_stok_obat":"1","pengecekan_stok_ed":"1","penyesuaian_stok_obat":"1","pengecekan_stok_minimum_unit":"1","permintaan_obat_unit":"1","obat_keluar":"1","retur_obat_unit":"1","retur_keluar":"1"}}');
		$menu->add("ICU",'{"icu":{"antrian":"1","layanan":"1","gizi":"1","setting_layanan":"1","daftar":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Isolasi",'{"isolasi":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","rkbu":"1","setting_layanan":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Kasir",'{"kasir":{"pembayaran_patient":"1","pembayaran_resep":"1","kwitansi":"1","mbank":"1"}}');
		$menu->add("Keuangan",'{"finance":{"payment":"1","laporan":"1"}}');
		$menu->add("Kenanga",'{"kenanga":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"mawar":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"melati":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Laboratory",'{"laboratory":{"pemeriksaan":"1","arsip":"1","laporan":"1","settings":"1"}}');
		$menu->add("Lily",'{"lily_ii_a":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"lily_ii_a_plus":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"lily_iii":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Mawar",'{"mawar":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Melati",'{"kenanga":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"mawar":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"},"melati":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Perencanaan",'{"perencanaan":{"jenis_barang":"1","barang":"1","barang_sito":"1","barang_konsinyasi":"1","rkbu":"1","rencana_pengadaan":"1","persetujuan_rencana_pengadaan":"1"}}');
		$menu->add("Personalia",'{"hrd":{"job":"1","employee":"1"}}');
		$menu->add("Pendaftaran",'{"registration":{"registration_patient":"1","register_inap":"1","masterperusahaan":"1","asuransi":"1","laporan_pasien":"1","laporan_pasien_daftar":"1","laporan_bonus":"1"}}');	
		$menu->add("Radiology",'{"radiology":{"pemeriksaan":"1","arsip":"1","unread":"1","laporan":"1","bacaan_normal":"1","laporan_detail":"1","settings":"1"}}');
		$menu->add("Ruang Bayi",'{"bayi":{"antrian":"1","layanan":"1","gizi":"1","daftar":"1","setting_layanan":"1","rkbu":"1","pengaturan_stok_minmaks":"1","boi":"1"}}');
		$menu->add("Rekam Medis",'{"medical_record":{"icd":"1","diagnosa":"1","indikator_klinis":"1","morbiditas":"1","stp":"1","lap_operasi":"1","lap_igd":"1","lap_kb":"1"}}');
		
		
		$uitable=new Table(array());
		$uitable->setName("registration");
		$uitable->addModal("username", "text", "Username", "");
		$uitable->addModal("realname", "text", "Realname", "");
		$uitable->addModal("email", "text", "Email", "");
		$uitable->addModal("menu", "select", "Menu", $menu->getContent());
		$register=$uitable->getModal();
		$res.=$register->getHtml();
	}
	
	$res.= $modal->getHtml();
	$res.= addJS("framework/smis/js/table_action.js");
	$res.="</div>";
	echo $res;
?>

<script type="text/javascript">
	var registration;
	$(document).ready(function(){
		registration=new TableAction("registration","smis-base","smis-registration",new Array("username","realname","menu","email"));
	});

	delete_cookie("smis-index");
	$("#username").keypress(function(e) {
		    if(e.which == 13) {
		    	$('#password').focus();
		    }
	});

	$("#password").keypress(function(e) {
		    if(e.which == 13) {
		    	login();
		    }
	});
	
	function to_sitemap(){
		window.location.href = './smis-multisite.php';
	}
	
	function login(){
		if($("#username").val()==""){
			smis_alert("Please Fill Username","", "alert-info");
			$("#username").focus();
			return;
		}

		if($("#password").val()==""){
			smis_alert("Please Fill Password","", "alert-info");
			$("#username").focus();
			return;
		}
		
		showLoading();	
		$.post('',
			{
				page:"smis-base",
				action:"smis-login-page",
				command:"login",
				username:$("#username").val(),
				password:$("#password").val()
			},function(res){		
				dismissLoading();
				json=getContent(res);
				if(json=='success'){
					location.reload();
				}else{
					$("#username").focus();
				}
		});
	}
</script>
<style type="text/css">
	#license-area{width:460px !important;}
	#smis-login{top:3px !important;	left:3px !important; z-index:190;}
	#smis-login > .modal-body > .modal-body-title { float:left; max-width:200px; }
	#smis-login > .modal-body > .modal-body-form { float:left; max-width:300px;}
	.smis_form > div > label{ text-transform: uppercase; font-weight:800; font-style: italic;} 
	#smis_logo img{ max-width:200px; max-height:auto; margin:0 auto;}
	.smis{ max-width: 563px;max-height: 316px;position: absolute;top:0;bottom: 0;left: 0;right: 0;margin: auto;}
	#smis-modal{	top:0;	left:0;}
	#smis_logo{	text-align: center;}
</style>