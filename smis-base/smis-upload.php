<?php
chdir(getcwd()."/../");

if(isset($_POST['multisite']) && $_POST['multisite']!=""){
	define("MULTISITE",$_POST['multisite']);
}

require_once "smis-base/smis-config.php";
require_once 'smis-base/smis-include-ui.php';
require_once 'smis-base/smis-include-system.php';
require_once 'smis-libs-function/smis-libs-function-essential.php';
session_name(getSessionPrefix());	
session_start();
$dbconnector = new DBConnector ( SMIS_SERVER, SMIS_DATABASE, SMIS_USERNAME, SMIS_PASSWORD );
$result = $dbconnector->connect ();
require_once 'smis-base/smis-function.php';
require_once 'smis-base/smis-init-variable.php';


$allowed = array();
$filetype=$_POST['filetype'];
$authorize=is_authorize(is_realtime());
if(!$authorize){
	echo '{"status":"error","msg":"Your Are Not Authorize"}';
	return;
}

if($filetype=='image'){
	$allowed=array('png', 'jpg', 'gif','bmp','tiff','jpeg');
}else if($filetype=='video'){
	$allowed=array('avi','3gp','mkv','mp4','fla','wmv');
}else if($filetype=='audio'){
	$allowed=array('mp3','ogg','wav','mid','amr','wma','m4a','m4p');
}else if($filetype=='archive'){
	$allowed=array('zip','rar','7z','tar','gz','bzip');
}else if($filetype=='document'){
	$allowed=array('rtf','doc','docx','ppt','pptx','xls','xlsx','txt','pdf');
}else if($filetype=='code'){
	$allowed=array('java','php','html','txt','css','js','c','cpp','py');
}else{
	$allowed = array('png', 'jpg', 'gif','bmp',	'tiff','jpeg',			//image
			'avi','3gp','mkv','mp4','fla','wmv',						//video
			'mp3','ogg','wav','mid','amr','wma','m4a','m4p',			//audio
			'zip','rar','7z','tar','gz','bzip',							//archive
			'rtf','doc','docx','ppt','pptx','xls','xlsx','txt','pdf',	//document
			'java','php','html','txt','css','js','c','cpp','py'			//source code
	);
}


$random_name=md5(date("mdyhisu").$_POST['multisite']);

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
	if(!in_array(strtolower($extension), $allowed)){
		echo '{"status":"error","msg":"tipe file seharusnya '.$filetype.' ('.implode(" , ",$allowed).')"}';
		return;
	}
	if(move_uploaded_file($_FILES['upl']['tmp_name'], 'smis-upload/'.$random_name."_".$_FILES['upl']['name'])){
		$data['status']="success";
		$data['filetype']=$_POST['filetype'];
		$data['file']=$random_name."_".$_FILES['upl']['name'];
		$data['filename']=$_FILES['upl']['name'];
		echo json_encode($data);
	}
	global $db;
	global $user;
	$dupload['namefile']=$_FILES['upl']['name'];
	$dupload['linkfile']=$random_name."_".$dupload['namefile'];
	$dupload['size']=$_FILES['upl']['size'];
	$dupload['ip']=$user->get_client_ip();
	$dupload['user']=$user->getName();
	$dupload['filetype']=$filetype;
	$dbtable=new DBTable($db, "smis_adm_upload");
	$suk=$dbtable->insert($dupload);
	return;
}
echo '{"status":"error","msg":"Unknow"}';

?>