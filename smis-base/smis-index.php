<?php 
/**
 * for the first time a logined user
 * will be automatically redirect here
 * this is the first landing page
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	 

echo "<html>"; 
	echo "<head>"; 
		echo "<title>".getTitle()."</title>";
		echo '<link rel="icon" type="image/png" href='.getLogo().' />';
		echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
          $theme = "smis-custom-default.css";
            if(function_exists("getSettings") )
			    $theme = getSettings($db, "smis-color-theme", $theme);
			if(function_exists("addCSS")){
                global $user;
                if($user!=null && $user->getCSS()!=""){
                    echo addCSS("smis-theme/".$user->getCSS(),false);
                }else{
                    echo addCSS("smis-theme/".$theme,false);
                }
            }else {
				echo "<link rel='stylesheet' type='text/css' href='smis-theme/".$theme."'>";
			}
		echo addJS("framework/jquery/jquery-2.1.1.min.js");
		echo addJS("base-js/smis-base.js");
		echo addJS("framework/jquery/jquery.maskMoney.min.js");
		echo addJS("framework/jquery/jquery.touchSwipe.min.js");
		echo addJS("framework/bootstrap/js/bootstrap-typeahead.js");
		echo addJS("framework/bootstrap/js/bootstrap.min.js");
		echo addJS("framework/bootstrap/js/bootbox.min.js");
		echo addJS("framework/bootstrap/js/bootstrap-slider.js");
		echo addJS("framework/bootstrap/js/bootstrap-colorpicker.js");
		echo addJS("base-js/smis-base-ui.js");
		echo addJS("base-js/smis-base-money.js");
		echo addJS("base-js/smis-base-table.js");
		echo addJS("base-js/smis-base-format.js");
		echo addJS("base-js/smis-base-file.js");
		echo addJS("base-js/smis-base-print.js");
		echo addJS("base-js/smis-base-prototype.js");
		echo addJS("base-js/smis-base-summernote.js");
		echo addJS("base-js/smis-base-smodal.js");
		echo addJS("base-js/smis-base-tree.js");
		echo addJS("base-js/smis-base-shortcut.js");
		echo addJS("base-js/smis-base-clipboard.js");
		echo addJS("base-js/smis-index.js");
		echo addJS("base-js/smis-base-shorcut.js");
		echo addJS("base-js/smis-base-drawing.js");
		echo addJS("framework/bootstrap/js/summernote.min.js");
		echo addJS("framework/bootstrap/js/jquery.visible.min.js");
	echo "</head>";
		
	global $user;
	$css	= "";
	$img	= "";
	if($user!=null){
		$css	 = " hue-rotate(".$user->getColorSlide()."deg) ";
		$css	.= " invert(".$user->getColorInvert().") ";
		$css	.= " grayscale(".($user->getColorGrayscale()/100).") ";
		$img	 = " hue-rotate(".(360-$user->getColorSlide())."deg) ";
		$img	.= " invert(".($user->getColorInvert()).") ";
		$img	 = "<style type='text/css'> img {-webkit-filter:".$img.";} </style>";
		
		echo $img;
		if($user->getSliderModel()=="1"){
			echo addCSS("base-css/smis-base-slider-setup.css");
		}
	}

	echo '<body style="'.($css==""?"":"-webkit-filter:".$css).' >';
		echo '<div id="body">';
			require_once 'smis-base/smis-loading.php';
			echo "<div id='non_printing_area' class='smis'>";
				if(!$installed){
					/** var $installed defined in index.php if not install then prepare instalation */
					require_once 'smis-base/smis-install-prepare.php';
				}else if(!$authorize){
					/** var $authorize defined in index.php if not authorized then going to login-page*/
					require_once 'smis-base/smis-login-page.php';
				}else if($authorize && getSettings($db, "smis_maintenance_mode", "0")=="1" && !$user->isAdministrator() ){
					/** if this is maintenance state and not admin that login then state to maintenance server */
					require_once 'smis-base/smis-maintenance.php';
				}else{
					/**normal condition */
					if(getSettings($db, "smis-enable-realtime", "0")=="1"){
						$timeout_realtime	= getSettings($db, "smis-realtime", "30000");
						$code				= new Hidden("realtime_init_time", "", $timeout_realtime);
						echo $code->getHtml();
						echo addJS("base-js/smis-base-realtime.js");
					}
					
					$slack	= new Hidden("slack_enable_mode", "", getSettings($db, "slack_enable", "0"));
					echo $slack->getHtml();
					
					$max_timeout	 = getSettings($db, "smis-max-timout-ajax-request", -1)*1;
					if($max_timeout<5000 && $max_timeout!=-1){
						$max_timeout = -1;
					}
					$max_time		= new Hidden("smis_post_max_timeout","smis_post_max_timeout",$max_timeout);
					echo $max_time	->getHtml();
					
					echo '<div id="navigator" class="noprint">';
						require_once 'smis-base/smis-index-navigator.php';
					echo '</div>';
					echo '<div id="smis_window" >';
						echo '<div class="i_container">';		
							if($user!=null && $user->getSettingsNavigatorMode()=="sidebar"){ 
								echo '<div class="smis_sidebar">';
									require_once 'smis-base/smis-sidebar.php';
								echo '</div>';
							}
							echo '<div class="smis_content">';
								require_once 'smis-base/smis-window-wrapper.php';
							echo '</div>'; 
						echo '</div>';
					echo '</div>';
				}
			echo "</div>";
			
			if($installed && $authorize){
				if(getSettings($db, "smis_autonomous_sound", "0")=="1"){
					echo "<audio id='smis_sound_source' src='".getSound()."' preload='auto'></audio>";
				}
			}
			echo "<div id	='smis_index_modal'>";
				require_once 'smis-base/smis-index-modal.php';
			echo "</div>";
			
			echo "<div class='noprint smis-alert-modal' id='alert'></div>";
			echo "<div id='printing_area' class='row-fluid'></div>";
		echo "</div>";
	echo "</body>";
echo "</html>";
?>