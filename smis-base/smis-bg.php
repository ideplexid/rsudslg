<?php 
/**
 * a background image that could be set in the system settings 
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	
if(function_exists("getSettings")){
	global $db;
	$wall	= getSettings($db, "background-wallpaper", "");
	if($wall!="") {
		?>
				<style type="text/css">
				body{
					background: url('smis-upload/<?php echo $wall; ?>') no-repeat center bottom fixed; 
				}
				</style>
		<?php
	}	
	if(getSettings($db, "live-wallpaper", "0")=="1"){
		$list_bg	= getAllFileInDir("smis-bg");
		foreach($list_bg as $slug){
			$name	= str_replace(".php", "", $slug);
			$name	= str_replace("smis-bg-", "", $name);
			$bname	= ArrayAdapter::format("clean-slug", $name);
			$id="smis-bg-".$bname;
			if(getSettings($db, $id, "0")=="1") require_once 'smis-bg/'.$slug;
		}
	}
}else{
	require_once 'smis-bg/smis-bg-hill.php';
	require_once 'smis-bg/smis-bg-cloud.php';
	require_once 'smis-bg/smis-bg-wave.php';
	require_once 'smis-bg/smis-bg-clock.php';
} 
 ?>