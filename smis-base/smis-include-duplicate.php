<?php 
/**
 * when used a duplicate system
 * simply require this file and all
 * needed class would provided
 * 
 * @author      : Nurul Huda
 * @since       : 26 April 2017
 * @version     : 1.0.0
 * @copyright   : goblooge@gmail.com
 * @lincense    : LGPLv3
 * */

require_once "smis-base/smis-include-service-consumer.php";
require_once "smis-framework/smis/interface/LockerInterface.php";
require_once "smis-framework/smis/database/DuplicateResponder.php";
require_once "smis-framework/smis/service/ServiceProvider.php";
require_once "smis-framework/smis/service/SynchronousServiceProvider.php";
require_once "smis-framework/smis/ui/complex/TableSynchronous.php";
require_once "smis-framework/smis/database/SynchronousViewAdapter.php";
require_once "smis-libs-class/Locker.php";
require_once "smis-libs-class/DuplicateLocker.php";
?>