<?php 
	/*this is used for preparing instalation for smis */
	require_once 'smis-base/smis-logo.php';
	echo "<div id='body'>";
	$div="
			<div class='row-fluid install-header'>
				<div class='install-logo span4' >
					<img id='smis_logo_image' src='".getLogo()."'/>
				</div>
				
			<div class='install-desc span8'>
						<p>Safethree&#8482;  is trademark by &copy; <a href='#' onclick='goblooge()'>goblooge</a>, All Right Reserved. 
								By Installing This Software you Agreed with the <a href='#' onclick='license()'>Term and Condition</a>.</p>
						<p>This Wizard will guide you to install. Just Fill the Blank area below then click install to begin installation process. if you have a problem with installation, consider to read the <a href='#' onclick='help()'>Help</a>.</p>
						
				</div>
			</div>
			";
	
	$modal=new Modal("smis-modal","","Safethree OS Installation");
	$modal->setAlwaysShow(true);	
	$modal->addHtml($div,"before");
	$modal->addHtml("<div id='install-show'></div>","before");
	
	$option_builder=new OptionBuilder();
	$option_builder->add("MYSQL","mysql","1");
	$option_builder->add("MYSQLI","mysqli");
	$option_builder->add("PDO","mysqli");
	
	$select=new Select("php_mode", "Connector", $option_builder->getContent());
	
	$modal->addElement("Server",new Text("server","server","localhost"));
	$modal->addElement("Database",new Text("database","database","smis"));
	$modal->addElement("Username",new Text("username","username","root"));
	$modal->addElement("Password",new Text("password","password",""));
	if(is_multisite()){
		$modal->addElement("Config Name",new Text("configname","configname",""));	
	}else{
		$modal->addElement("",new Hidden("configname","configname","smis-base/smis-config.php"));
	}
	$modal->addElement("Connector",$select);
	
	
	
	$button=new ButtonGroup("");
	$btn=new Button("", "", "Install");
	$btn->setClass("btn-primary");
	$btn->setIsButton(Button::$ICONIC_TEXT);
	$btn->setAction("install()");
	$btn->setIcon("icon-white ".Button::$icon_download_alt);
	$button->addButton($btn);
	
	$modal->addFooter($button);
	
	echo $modal->getHtml();
	
	echo "<div id='install-license' class='hide'>";
	require_once 'smis-base/smis-install-license.php';
	echo "</div>";
	
	echo "<div id='install-goblooge' class='hide'>";
	require_once 'smis-base/smis-install-goblooge.php';
	echo "</div>";
	
	echo "<div id='install-help' class='hide'>";
	require_once 'smis-base/smis-install-help.php';
	echo "</div>";
	
	echo "<div id='install-progress' class='hide'>
			<div class='alert alert-inverse'>
				<h4>Installing in Progress</h4>
				<p>Please Wait While Installation is in Progress... <i class='spin icon-black icon-refresh'></i> </p>
			</div>
		</div>";
	
	echo "<div id='install-pre-error' class='hide'>
			<div class='alert alert-warning'>
				<button type='button' class='close' data-dismiss='alert'>x</button>
				<h4>Input Field Not Correct</h4>
				<p>Please Fill Server, Database , Username and Password Correctly </p>
			</div>
		</div>";
	echo "</div>";
?>
<style type="text/css">
body{
	max-height:100% !important;
	overflow:hidden !important;
}
</style>
<script type="text/javascript">
	function license(){
		var li=$("#install-license").html();
		$("#install-show").html(li);
	}

	function help(){
		var li=$("#install-help").html();
		$("#install-show").html(li);
	}

	function goblooge(){
		var li=$("#install-goblooge").html();
		$("#install-show").html(li);
	}
	
	
	
	function install(){
		if($("#server").val()=="" || $("#database").val()=="" || $("#username").val()==""){
			var li=$("#install-pre-error").html();
			$("#install-show").html(li);
			return;
		}

		var li=$("#install-progress").html();
		$("#install-show").html(li);
		
		$.post('',
			{
				page:"smis-base",
				action:"smis-install-action",
				command:"install",
				server:$("#server").val(),
				database:$("#database").val(),
				username:$("#username").val(),
				password:$("#password").val(),
				php_mode:$("#php_mode").val(),
				config_name:$("#configname").val()
			},function(res){		
				var json=$.parseJSON(res);
				$("#install-show").html(json.msg);				
		});
	}
</script>
<style type="text/css">
	.smis{  max-width: 562px; max-height: 505px; position: absolute; top:10; bottom: 0; left: 0; right: 0; margin: auto;}
	#smis-modal{ top:0; left:0;}
	#license-modal{z-index:6666;}
	#license-area{ width:460px !important; }
	
</style>