<?php 
/**
 * this is how the navigator create
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	 

if($installed && $authorize){	//installed and authorized was define at index.php 
	global $NAVIGATOR;
	$user		= new User();
	$user		->loadUser(getSession('userid'));
	$mode		= $user->getSettingsNavigatorMode();
	$notif		= $user->getNotification();
	$NAVIGATOR 	->setUser($user->getCapability(), $user->getMenu(),$user);
	$NAVIGATOR 	->setNavigatorMode($mode);
	$NAVIGATOR 	->setNotification($notif);
	echo $NAVIGATOR->getHtml();
}
?>