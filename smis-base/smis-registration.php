<?php 
$response = new ResponsePackage ();
$response->setAlertVisible ( true );
$response->setStatus ( ResponsePackage::$STATUS_UNAHTORIZED);
$response->setAlertContent ( "Your Are not Authorize", "you are not authorize to access this page", "alert-danger" );
$response->setContent ( "" );

if($_POST['command'] && $_POST['command']=="save"){
	if(getSettings($db, "smis-enable-registration", "0")=="1"){
		$dbtable=new DBTable($db, "smis_adm_user");
		$id=array("username"=>$username);
		if(!$dbtable->is_exist($id) && $_POST['username']!=""){
			$array=array();
			$array['username']=$_POST['username'];
			$array['realname']=$_POST['realname'];
			$array['email']=$_POST['email'];
			$array['menu']=$_POST['menu'];
			$array['password']=md5("default");
			$array['authority']="user";
			$dbtable->insert($array);
			$response->setStatus ( ResponsePackage::$STATUS_OK);
			$response->setAlertContent ( "Success", "User Saved", "alert-success" );
		}else{
			$response->setStatus ( ResponsePackage::$STATUS_OK);
			$response->setAlertContent ( "Fail", "Username Have Been Used", "alert-danger" );
		}
	
	}	
}else{
	$response->setStatus ( ResponsePackage::$STATUS_OK);
	$response->setAlertVisible(false);
	$response->setContent ( "" );
}

echo json_encode($response->getPackage());
?>