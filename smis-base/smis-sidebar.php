<?php 
	global $user;
	global $NAVIGATOR;
	echo "<div id='sidebar_wrapper'>";
		echo "<div id='spacer'>";
			$foto=$user->getFoto();
			if($foto==""){
				$foto=getLogo();
				if( strlen($foto) > 1000 ){
					$foto="<img id='user_photo' class='blackie' src='".$foto."' >";
				}else{
					$foto="<img id='user_photo' src='$foto' >";
				}
			}else{
				$foto="<img id='user_photo' src='smis-upload/".$foto."' >";
			}

			?>
				<div id='user_identity'>
					<div><?php echo $foto;?></div>
					<div>
						<div><?php echo $user->getNameOnly(); ?></div>
						<div class='noSwipe'>
                            <?php 						
                                $search=new Text("search_menu_box","","");
                                $search->addAtribute("placeholder","Type here...");
                                echo $search->getHtml();
                            ?>
                            </div>
					</div>
				</div>
			<?php 
		echo "</div>";
		echo "<div id='sidebar'>";

		if($NAVIGATOR->getNavigatorMode()==Navigator::$MENU_MODE_SIDEBAR){
			$menus=$NAVIGATOR->getMenuSideBaseHtml();
			echo $menus;
		}
		echo "</div>";
	echo "</div>";
?>
