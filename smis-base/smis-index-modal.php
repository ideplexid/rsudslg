<?php 
/**
 * this is the default modal that used
 * in smis like chooser, summernote and etc
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.2
 * @license     : LGPLv2
 * */	 

 /**warning modal */
$warning = new Modal("warning","Warning","Warning");
echo $warning->getHtml();

/**confim modal */
$confirm = new Modal("confirm","Confirm","Confirm");
$action  = new Button('confirm_button', 'OK', 'Ok');
$action  ->setClass("btn-success")
         ->setAtribute("data-target='#confirm'")
         ->setAction("$($(this).data('target')).smodal('hide')");
$confirm ->addFooter($action);
echo $confirm->getHtml();

/**help modal */
$help   = new Modal("smis-help-modal","Help","Help");
$help   ->setModalSize(Modal::$HALF_MODEL);
echo $help->getHtml();

/**summernote modal */
$summernote = new Modal("smis-summernote-modal","Summernote","Summernote");
$summernote ->setModalSize(Modal::$FULL_MODEL)
            ->addHTML("<div id='summernote'></div>");
$action     = new Button('summernote_button_save', 'Ok', 'Ok');
$action     ->setAction("smis_summernote_save()")
            ->setClass("btn-success");
$summernote ->addFooter($action);
echo $summernote->getHtml();

/**upload modal */
$upload = new Modal("smis-upload-modal","Upload","Upload");
$action = new Button('upload_button', 'Save', 'Save');
$action ->setClass("btn-primary")
        ->setIsButton(Button::$ICONIC_TEXT)
        ->setIcon("fa fa-save")
        ->setAction("uploadDone()");
$upload ->addFooter($action);
echo $upload->getHtml();

/**chooser modal */
$chooser = new Modal("smis-chooser-modal","Pilih","Pilih");
$action  = new Button('close', 'Close', 'Close');
$action  ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("icon-white ".Button::$icon_remove_circle)
         ->setClass("btn-primary")
         ->setAtribute("data-target='#smis-chooser-modal'")
         ->setAction("$($(this).data('target')).smodal('hide')");
$chooser ->addFooter($action);
echo $chooser->getHtml();

/**image modal */
$image = new Modal("smis-image-modal","Image","Image");
$action  = new Button('close', 'Close', 'Close');
$action  ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("icon-white ".Button::$icon_remove_circle)
         ->setClass("btn-primary")
         ->setAtribute("data-target='#smis-image-modal'")
         ->setAction("$($(this).data('target')).smodal('hide')");
$image ->addFooter($action);
echo $image->getHtml();

/**print preview modal */
$print        = new Modal("smis-print-preview", "Print Preview", "Print Preview");
$print        ->setModalSize(Modal::$FULL_MODEL);
$print_action = new Button('print', 'Print', 'Print');
$print_action ->setIsButton(Button::$ICONIC_TEXT)
              ->setIcon("icon-white ".Button::$icon_print)
              ->setClass("btn-primary")
              ->setAtribute("data-target='#smis-print-preview'")
              ->setAction("smis_print($(\"#smis-print-preview .modal-body\").html())");
$action       = new Button('close', 'Close', 'Close');
$action       ->setIsButton(Button::$ICONIC_TEXT)
              ->setIcon("icon-white ".Button::$icon_remove_circle)
              ->setClass("btn-primary")
              ->setAtribute("data-target='#smis-print-preview'")
              ->setAction("$($(this).data('target')).smodal('hide')");
$print        ->addFooter($action)
              ->addFooter($print_action);
echo $print->getHtml();

/**loader modal */
$loader         = new LoadingBar("smis-loader-bar", "");
$loader_modal   = new Modal("smis-loader-modal","load-modal","Loading...");
$loader_modal   ->setModalSize(Modal::$HALF_MODEL)
                ->addHTML($loader->getHtml(),"after"); 
$button         = new Button("","","Cancel");
$button         ->setIsButton(Button::$ICONIC_TEXT)
                ->setClass("btn-primary")
                ->setIcon(" fa fa-times")
                ->setAction("smis_loader.cancel()");
$loader_modal   ->addFooter($button);
echo $loader_modal->getHtml();

/**drawig modal */
$action = new Button('', 'save', 'Save');
$action ->setAction("_sdraw.save()")
        ->setClass("btn-success")
        ->setIcon("fa fa-save")
        ->setIsButton(Button::$ICONIC_TEXT);
$red     = new Button("sdraw-red","sdraw-red","Red");
$red     ->setIsButton(Button::$ICONIC_TEXT)
         ->setAction("_sdraw.color('#AA0000');")
         ->setIcon("fa fa-pencil");
$green   = new Button("sdraw-green","sdraw-green","Green");
$green   ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("fa fa-pencil")
         ->setAction("_sdraw.color('#00AA00');");
$blue    = new Button("sdraw-blue","sdraw-blue","Blue");
$blue    ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("fa fa-pencil")
         ->setAction("_sdraw.color('#0000AA');");
$cyan    = new Button("sdraw-cyan","sdraw-cyan","Cyan");
$cyan    ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("fa fa-pencil")
         ->setAction("_sdraw.color('#00AAAA');");
$magenta = new Button("sdraw-magenta","sdraw-magenta","Magenta");
$magenta ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("fa fa-pencil")
         ->setAction("_sdraw.color('#AA00AA');");
$yellow  = new Button("sdraw-yellow","sdraw-yellow","Yellow");
$yellow  ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("fa fa-pencil")
         ->setAction("_sdraw.color('#AAAA00');");
$black   = new Button("sdraw-black","sdraw-black","Black");
$black   ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("fa fa-pencil")
         ->setAction("_sdraw.color('#000000');");
$white   = new Button("sdraw-white","sdraw-white","White");
$white   ->setIsButton(Button::$ICONIC_TEXT)
         ->setIcon("fa fa-pencil")
         ->setAction("_sdraw.color('#FFFFFF');");
$btgcolr = new ButtonGroup("","","");
$btgcolr ->setMax(1,"Color")
         ->addButton($red)
         ->addButton($green)
         ->addButton($blue)
         ->addButton($cyan)
         ->addButton($magenta)
         ->addButton($yellow)
         ->addButton($black)
         ->addButton($white);

$pencil = new OptionBuilder();
$pencil ->add("Pencil Micro Size",1,1)
        ->add("Pencil Mini Size",2,0)
        ->add("Pencil Small Size",3,0)
        ->add("Pencil Standard Size",4,0)
        ->add("Pencil Medium Size",5,0)
        ->add("Pencil Big Size",6,0)
        ->add("Pencil Jumbo Size",8,0)
        ->add("Pencil Megaton Size",10,0);

$color  = new Text("sdraw-color","","");
$size   = new Select("sdraw-size","",$pencil->getContent());
$undo   = new Button("","sdraw-undo","Undo");
$undo   ->setIsButton(Button::$ICONIC_TEXT);
$undo   ->setIcon("fa fa-undo")
        ->setClass("btn-danger")
        ->setAction("_sdraw.undo()");
$redo   = new Button("","sdraw-redo","Redo");
$redo   ->setIsButton(Button::$ICONIC_TEXT)
        ->setIcon("fa fa-pencil")
        ->setClass("btn-success")
        ->setAction("_sdraw.redo()");
$clear  = new Button("","sdraw-clear","Clear");
$clear  ->setIsButton(Button::$ICONIC_TEXT)
        ->setIcon("fa fa-eraser")
        ->setAction("_sdraw.clear()");
$paddle = new Button("","sdraw-padlock","Lock");
$paddle ->setIsButton(Button::$ICONIC_TEXT)
        ->setIcon("fa fa-lock")
        ->setClass("btn-warning")
        ->setAction("_sdraw.lock()");
$inputgrup = new InputGroup("","","");
$inputgrup ->addComponent($color)
           ->addComponent($btgcolr)
           ->addComponent($size)
           ->addComponent($undo)
           ->addComponent($redo)
           ->addComponent($clear)
           ->addComponent($paddle);

$sdraw  = new Modal("smis-drawing-modal","Drawing","Drawing");
$sdraw  ->setModalSize(Modal::$FULL_MODEL)
        ->addHTML("<div>".$inputgrup->getHtml()."</div>","before")
        ->addHTML("<div id='smis-drawing'></div>")
        ->addFooter($action);
echo $sdraw ->getHtml();

/**map modal */
$map          = new Modal("smis-map-chooser", "Map View Modal", "Pilih Lokasi...");
$map_search   = new Text("smis-map-search","","");
$longitude    = new Hidden("smis-map-longitude","","");
$latitude     = new Hidden("smis-map-latitude","","");
$alamat       = new Text("smis-map-address","","");
$alamat       ->setDisabled(true);
$point_action = new Button('choose', 'choose', 'Choose');
$point_action ->setIsButton(Button::$ICONIC_TEXT)
              ->setIcon("fa fa-point ")
              ->setClass("btn-primary")
              ->setAtribute("data-target='#smis-map-chooser'");
$map          ->addElement("",$map_search,"");
$map          ->addElement("",$latitude,"");
$map          ->addElement("",$longitude,"");
$map          ->addElement("Address",$alamat,"");
$map          ->addHTML("<div id='map-modal-viewer' style='width:530px;height:250px;'></div>","before");
$map          ->addFooter($point_action);

echo $map->getHtml();

$url = get_laravel();
$laravel = new Hidden("url_laravel", "", $url);
echo $laravel->getHtml();
?>