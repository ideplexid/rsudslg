<?php 
/**
 * this system used to correct and find 
 * a file information like a file path 
 * size and etc.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	

$result			 				= array();
$result['exist'] 				= 0;
if(isset($_POST['filepath']) && file_exists($_POST['filepath'])){
	$result['exist']	 		= 1;
	$result['size']		 		= filesize($_POST['filepath']);
	$isfile				 		= is_file($_POST['filepath']);
	$result['lines']	 		= "0";
	$result['read']		 		= false;
	$result['write']	 		= false;
	$result['exec']		 		= false;
	if($isfile){
		$file 			 		= new SplFileObject($_POST['filepath']);
		$file->seek($file->getSize());
		$result['lines'] 		= $file->key();
		$result['read']	 		= $file->isReadable();
		$result['write'] 		= $file->isWritable();
		$result['exec']	 		= $file->isExecutable();
	}	
	$finfo 						= finfo_open(FILEINFO_MIME);
	$istext						= substr(finfo_file($finfo, $_POST['filepath']), 0, 4) == 'text';
	$result['msg']				= "";
	$result['filecontent']		= "";
	if($result['size']<=1000000 && $result['read'] && $istext && $isfile){
		$result['filecontent']	= file_get_contents($_POST['filepath']);
	}else{
		$result['msg']			= "File Failed to Load, it can be Caused by : 
									<ul>
										<li>It is Folder</li>
										<li>File is Not Text File</li>
										<li>File Not Readable</li>
										<li>File Size more than 1 MB</li>
									</ul>";
	}
}
$response 	= new ResponsePackage();
$response	->setContent($result)
			->setAlertVisible(false)
			->setStatus(ResponsePackage::$STATUS_OK);
echo json_encode($response->getPackage());
?>