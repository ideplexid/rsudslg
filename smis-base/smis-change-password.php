<?php 
/**
 * this function used for change the password 
 * of the user that login, password changing 
 * regulary is very important
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	

	if(isset($_POST['old']) && isset($_POST['pass'])){
		$iduser	= getSession('userid');
		$old	= md5($_POST['old']);
		$new	= md5($_POST['pass']);
		$user	= new User();
		$user	->loadUser($iduser);
		$update	= $user->updatePassword($old,$new);
		$response=new ResponsePackage();
		if($update){
			$response	->setStatus("ok")
						->setAlertVisible(true)
						->setAlertContent("Success","Password Updated","alert-warning");	
		}else{
			$response	->setWarning(true,"Failed","Old Password Wrong ");
		}
		echo json_encode($response->getPackage());
	}
?>