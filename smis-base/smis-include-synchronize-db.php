<?php 
/**
 * special case for synhronize data
 * should include this base. because there some data 
 * that eed to be synchronize.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	 

require_once "smis-framework/smis/database/SynchronousSenderAdapter.php";
require_once "smis-framework/smis/database/SynchronousViewAdapter.php";
require_once "smis-framework/smis/ui/complex/TableSynchronous.php";
require_once "smis-framework/smis/database/DBSynchronousResponder.php";
    
?>