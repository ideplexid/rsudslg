<?php 

/**
 * this fiel initiate all smis-library
 * that need to be inlcuded so the 
 * smis - system would work.
 * 
 * @author 		: Nurul Huda
 * @since 		: 15 april 2014
 * @version 	: 2.0.1
 * @copyright	: goblooge@gmail.com
 * @license		: LGPLv3
 * @used		: index.php
 * 				  smis-base/smis-service-loader.php
 * 				  smis-base/smis-upload.php
 * 				  smis-serverbus/service.php
 * 
 * 
 * */

/*for initiate the smis system*/
define('SMIS_DIR',"");

/*include the interface*/
require_once "smis-framework/smis/abstract/Responder.php";
require_once "smis-framework/smis/abstract/MySQLTable.php";

/*database connector*/
require_once("smis-framework/smis/database/DBController.php");
require_once("smis-framework/smis/database/DBConnector.php");
require_once("smis-framework/smis/database/Database.php");
require_once("smis-framework/smis/database/DBTable.php");
require_once("smis-framework/smis/database/DBResponder.php");
require_once("smis-framework/smis/database/DBReport.php");
require_once("smis-framework/smis/database/ArrayAdapter.php");
require_once("smis-framework/smis/database/SelectAdapter.php");
require_once("smis-framework/smis/database/SimpleAdapter.php");
require_once("smis-framework/smis/database/SummaryAdapter.php");
require_once("smis-framework/smis/api/OptionBuilder.php");
require_once("smis-framework/smis/api/SuperCommand.php");
require_once('smis-framework/smis/log/User.php');
require_once('smis-framework/smis/log/QueryLog.php');
require_once('smis-framework/smis/package/ResponsePackage.php');
require_once "smis-libs-class/Notification.php";

?>