<?php 
/**
 * this function allow user
 * to access smis directly
 * without even login using terminal or  
 * command prompt based
 * 
 * @author : Nurul Huda
 * @since  : 18 Apr 2017
 * @version : 1.0.1
 * @copyright : goblooge@gmail.com
 * @license : LGPLv3
 * */


require_once ('smis-base/smis-service-variable.php');
require_once ('smis-base/smis-include-service.php');
require_once ('smis-base/smis-function.php'); 

global $db;
global $user;

$username   = $argv[1];
$password   = $argv[2];

/* load the user data */
$result=$user->loginByDirect($username,$password);
if($result!==false){
    date_default_timezone_set ( getSettings ( $db, "smis_autonomous_timezone", "Asia/Jakarta" ) );
    $dbtable=new DBTable($db,"smis_adm_scheduled");
    $dbtable->setShowAll(true);
    $data=$dbtable->view("","0");
    $list=$data['data'];
    $dtime=date("Y-m-d H:i:s");
    foreach($list as $x){
        if($x->next_run<=$dtime || $x->next_run=="0000-00-00 00:00:00"){
            $warp=null;
            $date = new DateTime($dtime);
            $date->add(new DateInterval('PT'.$x->delay.'S'));
            $nex_run=$date->format('Y-m-d H:i:s');
            $update['next_run']=$nex_run;
            $update['last_run']=$dtime;            
            $id['id']=$x->id;
            $dbtable->update($update,$id,$warp);
            require_once ($x->path);
        }
    }
}

?>