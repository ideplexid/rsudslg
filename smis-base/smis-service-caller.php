<?php 
/**
 * this file used for smis-service to call service on entity
 * Autonomuse to Entity
 */

/*multisite handler*/
if(isset($_GET['multisite']) && $_GET['multisite']!=""){
	define("MULTISITE",$_GET['multisite']);
}
 
require_once getcwd()."/smis-service-loader.php";

global $wpdb;
global $db;
global $user;
global $querylog;
date_default_timezone_set(getSettings($db, "smis_autonomous_timezone", "Asia/Jakarta"));
$user->setRawUser($_POST['user'],$_POST['ip'] );
$user->setCode($_POST['smis_code']);
$user->setService($_POST['smis_service']);
$querylog->setEnabled(getSettings($db,"smis_log_system","false"));
$balasan="";
try{
	$url=$_POST['smis_path_url'];
	ob_start();
	require_once $url;
	$balasan=ob_get_clean();
	echo $balasan;
}catch(Exception $e){
	$balasan=$e->getMessage();
}

try{
	$url=$_POST['smis_path_url'];
	$smis_service_caller_data_balasan=array();
	$smis_service_caller_data_balasan['post']=json_encode($_POST);
	$smis_service_caller_data_balasan['get']=json_encode($_GET);
	$smis_service_caller_data_balasan['code']=$_POST['smis_code'];
	$smis_service_caller_data_balasan['path']=$url;
	$smis_service_caller_data_balasan['response']=$balasan;	
		
	$autonom=getSettings($db,"smis_autonomous_id","smis");
	$data=$smis_service_caller_data_balasan;
	$code=$_POST['smis_code'];
	$entity=$_POST['smis_entity'];
	$service=$_POST['smis_service'];
	$querylog->saveServiceEntityLog($service,$code,$data,$autonom,$entity);	
	require_once 'smis-base/smis-end-process.php';
}catch(Exception $e){
	require_once 'smis-base/smis-end-process.php';
}


?>