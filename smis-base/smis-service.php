<?php 
/**
 * this is used for respond when serverbus calling from here.
 * Server Bus to Entity
 * server bus will call this file for one autonomous.
 */
 
if(isset($_POST['msg'])){
	require_once 'smis-base/smis-service-variable.php';
	require_once ('smis-base/smis-include-service.php');
	require_once ('smis-base/smis-function.php');
    require_once "smis-base/smis-php-ini.php";
	
	global $wpdb;
	global $db;
	global $user;
	global $querylog;
	$_ERROR_REQUEST	= array();
	$send_key		= getSettings($db, "smis_serverbus_key", md5(date('mdyhisu')));
	$receive_key	= getSettings($db, "smis_autonomous_key", md5(date('MDYhisu')));
	$name			= getSettings($db, "smis_autonomous_name", "Non Name");
	
	$comunication	= new Communication();
	$comunication->setKey($send_key, $receive_key);
	
	$msg			= $_POST['msg'];
	$content		= $comunication->receive_command($msg);
	$request		= json_decode($content,true);
	$entity			= $request['entity'];
	$autonomous		= $request['autonomous'];
	$service		= $request['service'];
	$data			= $request['data'];
	$tuser			= $request['user'];
	$ip				= $request['ip'];
	$acaller		= $request['acaller'];
	$code			= $request['code'];
	
	$user->setRawUser($tuser, $ip);
	if($service=="ping"){
		echo $comunication->send_command("Ping Response Success From : ".$name);
		return;
	}
	
	$DATA_REQUEST	= $data;
	$DATA_RESPONSE	= array();
	$PATH=array();
	
	
	function is_not_prototype($name,$link){
		if($name!=$link) return false;
		return true;
	}
	
	/** list all entity that provide a service */
	$F_TEST=array();
	$list=getListActivePlugins();
	
	if($service=="get_entity"){
        /** only list the entity that needed **/
		$service_model		= $DATA_REQUEST;
		foreach($list as $the_entity=>$link_check){			
            if( (is_string($entity) && ($the_entity==$entity || $entity=="all")) || ( is_array($entity) && in_array($the_entity,$entity) ) ){
				$filename	= $link_check."/service/".$service_model.".php";
				if(file_exists($filename)){
					$DATA_RESPONSE[]	= $the_entity;
					$PATH[$the_entity]	= $filename;
				}
			}
            
		}
	}else{	
        /** passing to the entity that match the kriteria **/
		foreach($list as $the_entity=>$link_check){// get service		
			if((is_string($entity) && ($the_entity==$entity || $entity=="all")) || ( is_array($entity) && in_array($the_entity,$entity) ) ){
				$path_check		= $link_check."/service/".$service.".php";
				if(file_exists($path_check)){
					$DATA_REQUEST['smis_path_url']	= $path_check;
					$DATA_REQUEST['smis_code']		= $code;
					$DATA_REQUEST['smis_entity']	= $the_entity;
					$DATA_REQUEST['smis_service']	= $service;
					$URL="";			
					if(defined("MULTISITE")){
						/* this block certainly used for define multisite system
						 * system will push a get varible contains multisite name 
						 * if this have multisite defined here then foward it
						 * to the smis-service-caller.php*/		
						if(is_not_prototype($the_entity,$link_check)){
							$PATH[$the_entity]		 = "<strong>".$path_check."</strong>";
							$URL					 = curMultisitePageURL()."smis-base/smis-service-caller.php?multisite=".MULTISITE;
						}else{
							$PATH[$the_entity]		 = "<strong>".$path_check."</strong> <font class='label label-info' ><i class='icon-white icon-ok'></i></font>";
							$URL					 = curMultisitePageURL()."smis-base/smis-service-caller.php?multisite=".MULTISITE."&&entity=".$the_entity;
						}
					}else{
						if(is_not_prototype($the_entity,$link_check)){
							$PATH[$the_entity]		 = "<strong>".$path_check."</strong>";
							$URL					 = curPageURL()."smis-base/smis-service-caller.php";
						}else{
							$PATH[$the_entity]		 = "<strong>".$path_check."</strong> <font class='label label-info' ><i class='icon-white icon-ok'></i></font>";
							$URL					 = curPageURL()."smis-base/smis-service-caller.php?entity=".$the_entity;
						}
					}
					$_ERROR_REQUEST[$the_entity]	 ="";
					$response						 = getContentUrl($DATA_REQUEST,$ip,$user->getName(),$acaller, $URL);
					$DATA_RESPONSE[$the_entity]		 = json_decode($response,true);
					if(json_last_error()!==JSON_ERROR_NONE){
						$_ERROR_REQUEST[$the_entity] = $response;
					}				
				}else{
					$PATH[$the_entity]				 = "no-service";
				}	
			}else{
				$PATH[$the_entity]					 = "false-entity";
			}
		}	
	}	
	
	echo $comunication->send_command(json_encode($DATA_RESPONSE));
	$sblog['post']			= $content;
	$sblog['code']			= $code;
	$sblog['erponse']		= json_encode($_ERROR_REQUEST);
	$sblog['request']		= json_encode($DATA_REQUEST);
	$sblog['path']			= json_encode($PATH);
	$sblog['response']		= json_encode($DATA_RESPONSE);
	$autonomous_id			= getSettings($db,"smis_autonomous_id","smis");
	$querylog->saveServiceAutonomous($service,$code,$sblog,$autonomous_id);
}
?>