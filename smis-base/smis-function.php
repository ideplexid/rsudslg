<?php  
/**
 * this is the base function
 * to load a data or file or function 
 * in the system.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	
function loadLibrary($name){
	require_once("smis-libs-function/".$name.".php");
}

function loadClass($name){
	require_once("smis-libs-class/".$name.".php");
}

function loadFunction($modul,$name){
    require_once ($modul."/function/".$name.".php");
}

loadLibrary("smis-libs-function-base");
loadLibrary("smis-libs-function-ui");
loadLibrary("smis-libs-function-string");
?>