<?php 	
/**
 *  this is the default index after user login
 *  to the system.
 * 
 * @author      : Nurul Huda
 * @copyright   : goblooge@gmail.com
 * @since       : 17 Aug 2014
 * @version     : 1.0.1
 * @license     : LGPLv2
 * */	 

global $db;
global $user;
$old		= new Password("old","old","");
$new		= new Password("new","new","");
$retype		= new Password("retype","retype","");
$save		= new Button("","","Save");
$save		->setIsButton(Button::$ICONIC_TEXT)
			->setIcon("fa fa-save")
			->setClass("btn-primary")
			->setAction("changePassword()");

$time			 = $user->getTimeOut();
$st_mmode		 = $user->getSettingsNavigatorMode();
$st_slider		 = $user->getSliderModel();
$color_slide	 = $user->getColorSlide();
$color_invert	 = $user->getColorInvert();
$color_grayscale = $user->getColorGrayscale();
		
$opsi_durasi	 = new OptionBuilder();
$opsi_durasi	 ->add("5 Menit (Terlalu Cepat)"	, "5"	,$time=="5"		?"1":"0")
					->add("10 Menit (Standar)"		, "10"	,$time=="10"	?"1":"0")
					->add("20 Menit (Sedang)"		, "20"	,$time=="20"	?"1":"0")
					->add("40 Menit (Lama)"			, "40"	,$time=="40"	?"1":"0")
					->add("60 Menit (Terlalu Lama)"	, "60"	,$time=="60"	?"1":"0")
					->add("Tidak Terbatas (Bahaya)"	, "1440",$time=="1440"	?"1":"0");
$timeout		 = new Select("login_timeout", "", $opsi_durasi->getContent());

$colorslide		 = new Text("color_slider", "Color", $color_slide);
$colorslide		 ->setSlider();
$colorinvert	 = new CheckBox("color_invert", "Color", $color_invert);
$colorgray		 = new Text("color_grayscale", "Color", $color_grayscale);
$colorgray		 ->setSlider(0,100);

$opsi_mmode		 = new OptionBuilder();
$opsi_mmode		 ->add("Multiple Menu"	, "multiple",$st_mmode=="multiple"?"1":"0")
					->add("Single Menu"		, "single"	,$st_mmode=="single"  ?"1":"0")
					->add("Single Menu > 5"	, "single_5",$st_mmode=="single_5"?"1":"0")
					->add("Single Menu > 6"	, "single_6",$st_mmode=="single_6"?"1":"0")
					->add("Single Menu > 7"	, "single_7",$st_mmode=="single_7"?"1":"0")
					->add("Single Menu > 8"	, "single_8",$st_mmode=="single_8"?"1":"0")
					->add("Sidebar Menu"	, "sidebar",$st_mmode=="sidebar"?"1":"0");
$mmode			 = new Select("st_mmode", "", $opsi_mmode->getContent());


$slider			 = new OptionBuilder();
$slider			 ->add("Dinamic Model","0",$st_slider=="0"?"1":"0")
					->add("Force to Static","1",$st_slider=="1"?"1":"0");
$smode			 = new Select("st_smode", "", $slider->getContent());

$current_css	 = $user->getCSS();
$list_bg 		 = getAllFileInDir ( "smis-theme/" );
$option_css		 = new OptionBuilder();
$option_css		 ->add("","",($current_css==""?1:0));

foreach ( $list_bg as $slug ) {
	$option_css->add($slug,$slug,($current_css==$slug?1:0));
}
$css=new Select("css", "", $option_css->getContent());

$save_settings 	= new Button("", "", "Save");
$save_settings	->setIsButton(Button::$ICONIC_TEXT)
				->setIcon("fa fa-save")
				->setClass("btn btn-primary")
				->setAction("save_settings()");

$tabulator		= new Tabulator("", "");
$front			= new TablePrint("data_diri");
$front			->addTableClass("table table-hover table-striped table-condensed");
$front			->setMaxWidth(false);
$front			->addColumn("Nama",1,1)
				->addColumn($user->getName(),1,1)
				->commit("body");						
$front			->addColumn("Otoritas",1,1)
				->addColumn($user->getCapability(),1,1)
				->commit("body");						
$front			->addColumn("IP",1,1)
				->addColumn($user->getIp(),1,1)
				->commit("body");						
$front			->addColumn("Waktu",1,1)
				->addColumn(date('d F Y, H:i:s')." (".getSettings($db, "smis_autonomous_timezone", "Asia/Jakarta").")",1,1)
				->commit("body");						
$warn			= "Jika Anda Bukan <strong>".$user->getName() ."</strong>, Silakan <a  class='btn btn-primary fa fa-power-off' href='#'  onclick='logout()'> Log Out...</a>";
$front			->addColumn($warn,2,1)
				->commit("body");

$pengumuman		= getSettings($db,"smis_annoucement","");

$password 	= new TablePrint("data_diri");
$password 	->addTableClass("table table-condensed")
			->setMaxWidth(false);						
$password 	->addColumn("Perubahan Terakhir",1,1)
			->addColumn($user->getLastChange(),1,1)
			->commit("body");
$password 	->addColumn("Password Lama",1,1)
			->addColumn($old->getHtml(),1,1)
			->commit("body");						
$password 	->addColumn("Password Baru",1,1)
			->addColumn($new->getHtml(),1,1)
			->commit("body");						
$password 	->addColumn("Ulangi Password Baru",1,1)
			->addColumn($retype->getHtml(),1,1)
			->commit("body");						
$password 	->addColumn("",1,1)
			->addColumn($save->getHtml(),1,1)
			->commit("body");

$settings 	= new TablePrint("settings");
$settings 	->addTableClass("table table-condensed")
			->setMaxWidth(false);
$settings 	->addColumn("Durasi Login",1,1)
			->addColumn($timeout->getHtml(),1,1)
			->commit("body");
$settings	->addColumn("CSS Mode",1,1)
			->addColumn($css->getHtml(),1,1)
			->commit("body");
$settings	->addColumn("Navigator Mode",1,1)
			->addColumn($mmode->getHtml(),1,1)
			->commit("body");
$settings	->addColumn("Slider Mode",1,1)
			->addColumn($smode->getHtml(),1,1)
			->commit("body");
$settings	->addColumn("Color Slider",1,1)
			->addColumn($colorslide->getHtml(),1,1)
			->commit("body");
$settings	->addColumn("Color Grayscale",1,1)
			->addColumn($colorgray->getHtml(),1,1)
			->commit("body");
$settings	->addColumn("Color Invert",1,1)
			->addColumn($colorinvert->getHtml(),1,1)
			->commit("body");
$settings	->addColumn("",1,1)
			->addColumn($save_settings->getHtml(),1,1)
			->commit("body");

$notif		= $user->getNotification();
if($notif>0) {
	if($notif<=10) 		$notif = "<font class='label label-info'>".$notif."</font>";
	else if($notif<=20) $notif = "<font class='label label-inverse'>".$notif."</font>";
	else if($notif<=30) $notif = "<font class='label label-success'>".$notif."</font>";
	else if($notif<=40) $notif = "<font class='label label-info'>".$notif."</font>";
	else if($notif<=50) $notif = "<font class='label label-warning'>".$notif."</font>";
	else if($notif>50) 	$notif = "<font class='label label-important'>".$notif."</font>";
}else{
	$notif	= "";
}

$rang		= $user->rangeChange()>30?"<font class='label label-important'>".$user->rangeChange()."</font>":"";
if($pengumuman!="" && $pengumuman!="<p><br></p>"){
	$tabulator ->add("pengumuman", "Pengumuman", $pengumuman, Tabulator::$TYPE_HTML,"fa fa-volume-up");
}
$tabulator	->add("front", "General", $front, Tabulator::$TYPE_COMPONENT,"fa fa-user")
			->add("notif", "Notification ".$notif, "smis-tools/notification.php", Tabulator::$TYPE_INCLUDE,"fa fa-lightbulb-o")
			->add("password", "Password ".$rang, $password, Tabulator::$TYPE_COMPONENT,"fa fa-lock")
			->add("settings", "Settings", $settings, Tabulator::$TYPE_COMPONENT,"fa fa-gear");

if($user->getCapability()=="administrator"){            
	global $PLUGINS;
	/* sorting the plugins */
	$plug 	= array ();
	$smisp 	= array();
	foreach ( $PLUGINS as $pn ) {
		$pn_name	= $pn->getName();
		if(startsWith($pn_name, "smis-")){
			$smisp[$pn_name] 	= $pn;
		}else {
			$plug [$pn_name] 	= $pn;
		}
	}
	krsort($smisp);
	ksort ( $plug );
	foreach($smisp as $pn){
		array_unshift($plug , $pn);
	}
	
	$plugin_update	= array();
	foreach ( $plug as $key => $p ) {
		$status 	= "Activated";
		$status 	= $p->is_actived() && $p->is_update();
		if($status){
			$plugin_update[$key] = $p;
		}
	}
	
	if(count($plugin_update)>0){
		$table	  =new TablePrint("");
		$table	->setDefaultBootrapClass(true)
				->setMaxWidth(false);
		
		$table	->addColumn("THIS PLUGINS NEED TO BE UPDATED",4,1)
				->commit("title");
		$table	->addColumn("Plugins",1,1)
				->addColumn("Current Version",1,1)
				->addColumn("Update Version",1,1)
				->commit("title");
		foreach($plugin_update as $key=>$p){
			$table	->addColumn(strtoupper($p->getName()),1,1)
					->addColumn($p->getCurrentVersion(),1,1)
					->addColumn($p->getVersion(),1,1)
					->commit("body");
		}
		$tabulator  ->add("plugins", "Plugins", $table, Tabulator::$TYPE_COMPONENT,"fa fa-list-alt");
		
	}
}

echo $tabulator->getHtml();
echo addJS("smis-base-js/smis-index-home.js",false);
echo addJS("smis-base-js/smis-base-swipe.js",false);

$url = get_laravel();
$laravel = new Hidden("url_laravel", "", $url);
echo $laravel->getHtml();
?>