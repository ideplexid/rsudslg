<?php 
/**
 * this function allow user to 
 * access smis directly without even login
 * it's same like blog post
 * 
 * @author : Nurul Huda
 * @since  : 18 Apr 2017
 * @version : 1.0.0
 * @copyright : goblooge@gmail.com
 * @license : LGPLv3
 * */
 

require_once ('smis-base/smis-service-variable.php');
require_once ('smis-base/smis-include-service.php');
require_once ('smis-base/smis-function.php'); 

global $db;
global $user;
$filter               = array();
$filter['service']    = $_GET['smis_direct'];
$filter['password']   = $_GET['smis_direct_password'];
$filter['autonomous'] = $_GET['smis_direct_autonomous'];

$dbtable    = new DBTable($db,"smis_adm_directory");
$x          = $dbtable->select($filter);
if($x==null){
    echo "Direct Service Not Found";
}else{
    ob_start ();
    if(getSettings($db, "smis-debuggable-mode","0")=="1"){
        show_error();
    }
    $user->setRawUser("", $filter['autonomous']);
    require_once ($x->provider);
    $result = ob_get_clean ();
    if ($CHANGE_COOKIE && ! isset ( $_POST ['smis_help'] )) {
        changeCookie ();
    }
    echo $result;
}
?>