<?php
	global $db;
	global $user;
	require_once 'smis-libs-class/Policy.php';
	require_once 'smis-libs-inventory/policy.php';
	
	$policy=new Policy("cssd", $user);
	$policy->setDefaultPolicy(Policy::$DEFAULT_POLICY_RESTRICT);
	$policy->addPolicy("cssd", "cssd", Policy::$DEFAULT_COOKIE_CHANGE,"modul/cssd");
	$policy->addPolicy("laporan", "laporan", Policy::$DEFAULT_COOKIE_CHANGE,"modul/laporan");
	$policy->addPolicy("settings", "settings", Policy::$DEFAULT_COOKIE_CHANGE,"modul/settings");

	
    $inventory_policy = new InventoryPolicy("cssd", $user,"modul/");
	$policy->combinePolicy($inventory_policy);
	$policy->initialize();
?>
