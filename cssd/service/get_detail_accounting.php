<?php

global $db;
$id         = $_POST['data'];
$dbtable    = new DBTable($db,"smis_amb_ambulan");
$x          = $dbtable->selectEventDel($id);
$total      = $x->gaji_sopir + $x->tarif + $x->biaya_perjalanan + $x->biaya_perawat + $x->biaya_dokter + $x->biaya_alat + $x->biaya_lain;

$list       = array();

$debet=array();
$debet['akun']      = getSettings($db, "ambulan-accounting-debet-penggunaan-mobil", "");
$debet['debet']     = $total;
$debet['kredit']    = 0;
$debet['ket']       = "Piutang Ambulan - Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$debet['code']      = "debet-ambulan-".$x->id;
$list[]             = $debet;

$kredit=array();
$kredit['akun']     = getSettings($db, "ambulan-accounting-kredit-penggunaan-mobil", "");
$kredit['debet']    = 0;
$kredit['kredit']   = $total;
$kredit['ket']      = "Pendapatan Ambulan - Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$kredit['code']     = "kredit-ambulan-".$x->id;
$list[]             = $kredit;

//content untuk header
$header=array();
$header['tanggal']      = $x->tanggal;
$header['keterangan']   = "Transaksi Ambulan Pasien ".$x->nama_pasien." dengan No.Reg ".$x->id;
$header['code']         = "Ambulan-".$x->id;
$header['nomor']        = "AMB-".$x->id;
$header['debet']        = $total;
$header['kredit']       = $total;
$header['io']           = "1";

$transaction_Ambulan               = array();
$transaction_Ambulan['header']     = $header;
$transaction_Ambulan['content']    = $list;

/*transaksi keseluruhan*/
$transaction    = array();
$transaction[]  = $transaction_Ambulan;
echo json_encode($transaction);

/*confirm that already synch*/
$update['akunting'] = 1;
$id['id']           = $x->id;
$dbtable->setName("smis_amb_ambulan");
$dbtable->update($update,$id);

?>