var supir_km;
var pasien_km;
var mobil_ambulan_km;
var ambulan_km;
$(document).ready(function(){

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array(
			"id","mobil","id_mobil",
			"jarak","tanggal","lokasi","biaya_perjalanan",
            "id_sopir","nama_sopir","tarif",
            "biaya_alat","biaya_lain","keterangan", "selesai"
			);
	ambulan_km=new TableAction("ambulan_km","ambulatory","ambulan_km",column);
	ambulan_km.getRegulerData=function(){
		var reg_data={	
				page:this.page,
				action:this.action,
				super_command:this.super_command,
				prototype_name:this.prototype_name,
				prototype_slug:this.prototype_slug,
				prototype_implement:this.prototype_implement
            };
		reg_data['noreg_pasien']    = $("#noreg_pasien_km").val();
		reg_data['nama_pasien']     = $("#nama_pasien_km").val();
		reg_data['nrm_pasien']      = $("#nrm_pasien_km").val();
		reg_data['jenis_pasien']    = $("#jenis_pasien_km").val();
		reg_data['umur']            = $("#umur_pasien_km").val();
		reg_data['tgl_lahir']       = $("#tgl_lahir_pasien_km").val();
		reg_data['alamat']          = $("#alamat_pasien_km").val();
		reg_data['jk']              = $("#jk_pasien_km").val();
		return reg_data;
	};
	ambulan_km.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
    };
    
        
    ambulan_km.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#"+this.prefix+"_noreg_pasien").val();
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    ambulan_km.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };
	
	pasien_km=new TableAction("pasien_km","ambulatory","ambulan_km",new Array());
	pasien_km.setSuperCommand("pasien_km");
	pasien_km.setShowParentModalInChooser(false);
	pasien_km.selected=function(json){
		var nama=json.nama_pasien;
		var nrm=json.nrm;
		var noreg=json.id;		
		$("#nama_pasien_km").val(nama);
		$("#nrm_pasien_km").val(nrm);
		$("#noreg_pasien_km").val(noreg);
		$("#jenis_pasien_km").val(json.carabayar);
		$("#umur_pasien_km").val(json.umur);
		$("#tgl_lahir_pasien_km").val(json.tgl_lahir);
		$("#alamat_pasien_km").val(json.alamat_pasien);
        if(json.kelamin == "0") {
            $("#jk_pasien_km").val("L");
        } else if(json.kelamin == "1") {
            $("#jk_pasien_km").val("P");
        }
		
		ambulan_km.view();
	};

	supir_km=new TableAction("supir_km","ambulatory","ambulan_km",new Array());
	supir_km.setSuperCommand("supir_km");
	supir_km.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#ambulan_km_nama_sopir").val(nama);
		$("#ambulan_km_id_sopir").val(nip);
	};

	mobil_ambulan_km=new TableAction("mobil_ambulan_km","ambulatory","ambulan_km",new Array());
	mobil_ambulan_km.setSuperCommand("mobil_ambulan_km");
	mobil_ambulan_km.selected=function(json){
		$("#ambulan_km_mobil").val(json.mobil);
		$("#ambulan_km_id_mobil").val(json.id_mobil);
        setMoney("#ambulan_km_tarif",json.tarif);
        var ket="Dalam Kota";
        if(json.luar_dalam==1){
            ket="Luar Kota";
        }
        $("#ambulan_km_keterangan").val(ket);
        $("#ambulan_km_jarak").trigger("change");
	};
    
    $("#ambulan_km_jarak").on("change",function(){
        var jarak=$(this).val();
        var uang=getMoney("#ambulan_km_tarif");
        var total=uang*jarak;
        setMoney("#ambulan_km_biaya_perjalanan",total);
    });

	$(".mydate").datepicker();
	
});