var dokter;
var perawat;
var supir;
var pasien;
var lokasi;
var mobil_ambulan;
var ambulan;
$(document).ready(function(){

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array(
        "id","id_perawat","id_dokter","id_sopir","mobil","id_mobil",
        "nama_sopir","nama_perawat","nama_dokter",
        "lokasi","tanggal","gaji_sopir","biaya_perjalanan","total_biaya","biaya_perawat","jasa_perawat",
        "jenis_mobil","id_jenis_mobil","km_pertama","km_berikutnya","kelas","jarak",
        "biaya_dokter","biaya_alat","biaya_lain","biaya_perawat","kondisi", "selesai","didampingi_perawat",
        "desinfektan", "biaya_desinfektan",
        "tujuan"
    );
	ambulan=new TableAction("ambulan","ambulatory","ambulan",column);
	ambulan.getRegulerData=function(){
		var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
        };
		reg_data['noreg_pasien']    = $("#noreg_pasien").val();
		reg_data['nama_pasien']     = $("#nama_pasien").val();
		reg_data['nrm_pasien']      = $("#nrm_pasien").val();
        reg_data['jenis_pasien']    = $("#jenis_pasien").val();
		reg_data['umur']            = $("#umur_pasien").val();
		reg_data['tgl_lahir']       = $("#tgl_lahir_pasien").val();
		reg_data['alamat']          = $("#alamat_pasien").val();
        reg_data['jk']              = $("#jk_pasien").val();
        reg_data['uri']             = $("#uri").val();
        reg_data['ruangan']         = $("#ruangan").val();
		return reg_data;
	};
    
	ambulan.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
    };

    ambulan.hitung_biaya = function(){
        let jarak = Number(ambulan.get("jarak"));
        let km_pertama = Number(ambulan.get("km_pertama"));
        let km_berikutnya = Number(ambulan.get("km_berikutnya"));
        jarak = jarak-5;
        total = 0;
        if(jarak>0){
            total = km_pertama+km_berikutnya*jarak;
        }else{
            total = km_pertama;
        }

        ambulan.set("biaya_perjalanan",total);
        $("#ambulan_biaya_perjalanan").trigger("change");
    };

    $("#ambulan_biaya_perjalanan, #ambulan_didampingi_perawat, #ambulan_desinfektan").on("change",function(){
        ambulan.hitung_perawat();
    });

    ambulan.hitung_perawat = function(){
        let total = ambulan.get("biaya_perjalanan");
        let persen = ambulan.get("jasa_perawat");
        let biaya_perawat = 0;
        let biaya_desinfektan = 0; 
        if($("#ambulan_didampingi_perawat").is(":checked")){
            biaya_perawat = total*persen/100;
        }
        if ($("#ambulan_desinfektan").is(":checked")) {
            biaya_desinfektan = 100000;
        }
        let total_biaya = total+biaya_perawat+biaya_desinfektan;
        ambulan.set("biaya_perawat",biaya_perawat);
        ambulan.set("biaya_desinfektan",biaya_desinfektan);
        ambulan.set("total_biaya",total_biaya);

    }
    
    $("#ambulan_jarak").on("keyup",function(){
        ambulan.hitung_biaya();
    });
    
    /*ambulan.selesai = function(id) {
        var data=this.getRegulerData();
		data['super_command']   = "selesai";
		data['id']              = id;
		data['selesai']         = 1;
		showLoading();
		$.post('',data,function(res){
			var json=getContent(res);
			ambulan.view();
            dismissLoading();
		});
    };*/

    ambulan.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    ambulan.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };
	
	pasien=new TableAction("pasien","ambulatory","ambulan",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		var nama=json.nama_pasien;
		var nrm=json.nrm;
		var noreg=json.id;		
		$("#nama_pasien").val(nama);
		$("#nrm_pasien").val(nrm);
		$("#noreg_pasien").val(noreg);
        $("#jenis_pasien").val(json.carabayar);
		$("#umur_pasien").val(json.umur);
		$("#tgl_lahir_pasien").val(json.tgl_lahir);
        $("#alamat_pasien").val(json.alamat_pasien);
        $("#uri").val(json.uri);
        $("#ruangan").val(json.last_ruangan);
        if(json.kelamin == "0") {
            $("#jk_pasien").val("L");
        } else if(json.kelamin == "1") {
            $("#jk_pasien").val("P");
        }
		ambulan.view();
	};

	lokasi=new TableAction("lokasi","ambulatory","ambulan",new Array());
	lokasi.setSuperCommand("lokasi");
	lokasi.selected=function(json){
		$("#ambulan_jenis_mobil").val(json.jenis_mobil);
        $("#ambulan_id_jenis_mobil").val(json.id_jenis_mobil);
        $("#ambulan_kelas").val(json.kelas);
        $("#ambulan_km_pertama").val(json.km_pertama);
        $("#ambulan_km_berikutnya").val(json.km_berikutnya);
        $("#ambulan_kondisi").val(json.doa);
        ambulan.hitung_biaya();
	};

	dokter=new TableAction("dokter","ambulatory","ambulan",new Array());
	dokter.setSuperCommand("dokter");
	dokter.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#ambulan_nama_dokter").val(nama);
		$("#ambulan_id_dokter").val(nip);
	};

	perawat=new TableAction("perawat","ambulatory","ambulan",new Array());
	perawat.setSuperCommand("perawat");
	perawat.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#ambulan_nama_perawat").val(nama);
		$("#ambulan_id_perawat").val(nip);
	};

	supir=new TableAction("supir","ambulatory","ambulan",new Array());
	supir.setSuperCommand("supir");
	supir.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#ambulan_nama_sopir").val(nama);
		$("#ambulan_id_sopir").val(nip);
	};

	mobil_ambulan=new TableAction("mobil_ambulan","ambulatory","ambulan",new Array());
	mobil_ambulan.setSuperCommand("mobil_ambulan");
	mobil_ambulan.selected=function(json){
		$("#ambulan_mobil").val(json.mobil);
		$("#ambulan_id_mobil").val(json.id);
	};

	$(".mydate").datepicker();
	
});