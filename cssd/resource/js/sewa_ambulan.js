var perawat;
var supir;
var sewa_ambulan;
$(document).ready(function(){

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array("id","id_perawat","id_sopir","tanggal","nama","jenis_mobil","id_jenis_mobil","harga_jam","harga_hari","durasi_jam","durasi_hari","nama_perawat","nama_sopir","selesai","tujuan");
	sewa_ambulan=new TableAction("sewa_ambulan","ambulatory","sewa_ambulan",column);
    sewa_ambulan.view();
    
	perawat=new TableAction("perawat","ambulatory","sewa_ambulan",new Array());
	perawat.setSuperCommand("perawat");
	perawat.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#sewa_ambulan_nama_perawat").val(nama);
		$("#sewa_ambulan_id_perawat").val(nip);
	};

	supir=new TableAction("supir","ambulatory","sewa_ambulan",new Array());
	supir.setSuperCommand("supir");
	supir.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#sewa_ambulan_nama_sopir").val(nama);
		$("#sewa_ambulan_id_sopir").val(nip);
    };
    
    lokasi=new TableAction("lokasi","ambulatory","sewa_ambulan",new Array());
	lokasi.setSuperCommand("lokasi");
	lokasi.selected=function(json){
		$("#sewa_ambulan_jenis_mobil").val(json.jenis_mobil);
        $("#sewa_ambulan_id_jenis_mobil").val(json.id_jenis_mobil);
        $("#sewa_ambulan_harga_hari").val(json.durasi_hari);
        $("#sewa_ambulan_harga_jam").val(json.durasi_jam);
    };
    
    $("#sewa_ambulan_durasi_hari").on("change",function(){
        if($(this).val()=="0" || $(this).val()==""){
            $("#sewa_ambulan_durasi_jam").prop("disabled",false);
        }else{
            $("#sewa_ambulan_durasi_jam").prop("disabled",true);
            $("#sewa_ambulan_durasi_jam").val("");
        }
    });

    $("#sewa_ambulan_durasi_jam").on("change",function(){
        if($(this).val()=="0" || $(this).val()==""){
            $("#sewa_ambulan_durasi_hari").prop("disabled",false);
        }else{
            $("#sewa_ambulan_durasi_hari").prop("disabled",true);
            $("#sewa_ambulan_durasi_hari").val("");
        }
    });

	$(".mydate").datepicker();
	
});