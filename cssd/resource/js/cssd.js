var cssd;
$(document).ready(function(){
    $("#noreg_pasien").prop("disabled",true);
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array(
        "id",
        "tanggal",
        "nama_petugas",
        "id_pegawai",
        "nama_layanan",
        "id_layanan",
        "quantity",
        "satuan",
        "total",
        "keterangan",
    );
	cssd=new TableAction("cssd","cssd","cssd",column);
	cssd.getRegulerData=function(){
		var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
        };
		reg_data['noreg_pasien']    = $("#noreg_pasien").val();
		reg_data['nama_pasien']     = $("#nama_pasien").val();
		reg_data['nrm_pasien']      = $("#nrm_pasien").val();
        reg_data['jenis_pasien']    = $("#jenis_pasien").val();
		reg_data['umur']            = $("#umur_pasien").val();
		reg_data['tgl_lahir']       = $("#tgl_lahir_pasien").val();
		reg_data['alamat']          = $("#alamat_pasien").val();
        reg_data['jk']              = $("#jk_pasien").val();
        reg_data['uri']             = $("#uri").val();
        reg_data['ruangan']         = $("#ruangan").val();
		return reg_data;
	};
    
	cssd.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
    };

    cssd.hitung_biaya = function(){
        let satuan = Number(cssd.get("satuan"));
        let quantity = Number(cssd.get("quantity"));        
        cssd.set("total",satuan*quantity);
    };

    cssd.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    $("#cssd_quantity").on("change",function(){
        cssd.hitung_biaya();
    });


    cssd.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };
	
	pasien=new TableAction("pasien","cssd","cssd",new Array());
	pasien.setSuperCommand("pasien");
	pasien.setShowParentModalInChooser(false);
	pasien.selected=function(json){
		var nama=json.nama_pasien;
		var nrm=json.nrm;
		var noreg=json.id;		
		$("#nama_pasien").val(nama);
		$("#nrm_pasien").val(nrm);
		$("#noreg_pasien").val(noreg);
        $("#jenis_pasien").val(json.carabayar);
		$("#umur_pasien").val(json.umur);
		$("#tgl_lahir_pasien").val(json.tgl_lahir);
        $("#alamat_pasien").val(json.alamat_pasien);
        $("#uri").val(json.uri);
        $("#ruangan").val(json.last_ruangan);
        if(json.kelamin == "0") {
            $("#jk_pasien").val("L");
        } else if(json.kelamin == "1") {
            $("#jk_pasien").val("P");
        }
		cssd.view();
	};

	layanan=new TableAction("layanan","cssd","cssd",new Array());
	layanan.setSuperCommand("layanan");
	layanan.selected=function(json){
        cssd.set("satuan",json.tarif);
        cssd.set("nama_layanan",json.jenis_tindakan);
        cssd.set("id_layanan",json.id);
        cssd.hitung_biaya();
	};

    petugas=new TableAction("petugas","cssd","cssd",new Array());
	petugas.setSuperCommand("petugas");
	petugas.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#cssd_nama_petugas").val(nama);
		$("#cssd_id_pegawai").val(nip);
	};

	$(".mydate").datepicker();
	
});