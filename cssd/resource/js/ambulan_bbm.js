var dokter_bbm;
var perawat_bbm;
var supir_bbm;
var pasien_bbm;
var lokasi_bbm;
var mobil_ambulan_bbm;
var ambulan_bbm;
$(document).ready(function(){

	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	var column=new Array(
        "id","id_perawat","id_dokter","id_sopir","mobil","id_mobil",
        "nama_sopir","nama_perawat","nama_dokter",
        "lokasi","tanggal","gaji_sopir","biaya_perjalanan","total_biaya","biaya_perawat","jasa_perawat",
        "jenis_mobil","id_jenis_mobil","km_pertama","km_berikutnya","kelas","jarak",
        "biaya_dokter","biaya_alat","biaya_lain","biaya_perawat","kondisi", "selesai","didampingi_perawat",
        "desinfektan", "biaya_desinfektan",
		"jenis_bbm",
		"bbm_liter",
		"kebutuhan_bbm",
        "tujuan"
    );
	ambulan_bbm=new TableAction("ambulan_bbm","ambulatory","ambulan_bbm",column);
	ambulan_bbm.getRegulerData=function(){
		var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
        };
		reg_data['noreg_pasien']    = $("#noreg_pasien_bbm").val();
		reg_data['nama_pasien']     = $("#nama_pasien_bbm").val();
		reg_data['nrm_pasien']      = $("#nrm_pasien_bbm").val();
        reg_data['jenis_pasien']    = $("#jenis_pasien_bbm").val();
		reg_data['umur']            = $("#umur_pasien_bbm").val();
		reg_data['tgl_lahir']       = $("#tgl_lahir_pasien_bbm").val();
		reg_data['alamat']          = $("#alamat_pasien_bbm").val();
        reg_data['jk']              = $("#jk_pasien_bbm").val();
        reg_data['uri']             = $("#uri_bbm").val();
        reg_data['ruangan']         = $("#ruangan_bbm").val();
		return reg_data;
	};
    
	ambulan_bbm.show_add_form=function(){
		if($("#noreg_pasien").val()==""){
			showWarning("Silakan Pilih Pasien","Silakan Pilih Pasien Terlebih Dahulu");
			return;
		}
		this.clear();
		this.show_form();
    };

    ambulan_bbm.hitung_biaya = function(){
        var jarak = Number(ambulan_bbm.get("jarak"));
        var km_pertama = Number(ambulan_bbm.get("km_pertama"));
        var km_berikutnya = Number(ambulan_bbm.get("km_berikutnya"));
        var bbm_liter = ambulan_bbm.get("bbm_liter");
        var jarak_berikutnya = jarak-5;
        if(jarak_berikutnya<0){
            jarak_berikutnya = 0;
        }
        var total = (km_pertama) + (km_berikutnya*jarak_berikutnya);
        var total_biaya = total*bbm_liter;
        ambulan_bbm.set("kebutuhan_bbm",total.toFixed(2));
        ambulan_bbm.set("biaya_perjalanan",total_biaya.toFixed(2));
        $("#ambulan_bbm_biaya_perjalanan").trigger("change");
    };

    $("#ambulan_bbm_biaya_perjalanan, #ambulan_bbm_didampingi_perawat, #ambulan_bbm_desinfektan").on("change",function(){
        ambulan_bbm.hitung_perawat();
    });

    ambulan_bbm.hitung_perawat = function(){
        let total = ambulan_bbm.get("biaya_perjalanan");
        let persen = ambulan_bbm.get("jasa_perawat");
        let biaya_perawat = 0;
        let biaya_desinfektan = 0; 
        if($("#ambulan_bbm_didampingi_perawat").is(":checked")){
            biaya_perawat = total*persen/100;
        }
        if ($("#ambulan_bbm_desinfektan").is(":checked")) {
            biaya_desinfektan = 100000;
        }
        let total_biaya = total+biaya_perawat+biaya_desinfektan;
        ambulan_bbm.set("biaya_perawat",biaya_perawat);
        ambulan_bbm.set("biaya_desinfektan",biaya_desinfektan);
        ambulan_bbm.set("total_biaya",total_biaya);

    }
    
    $("#ambulan_bbm_jarak").on("keyup",function(){
        ambulan_bbm.hitung_biaya();
    });
    
    /*ambulan_bbm.selesai = function(id) {
        var data=this.getRegulerData();
		data['super_command']   = "selesai";
		data['id']              = id;
		data['selesai']         = 1;
		showLoading();
		$.post('',data,function(res){
			var json=getContent(res);
			ambulan_bbm.view();
            dismissLoading();
		});
    };*/

    ambulan_bbm.cekTutupTagihan = function(){
        var reg_data={	
            page:this.page,
            action:this.action,
            super_command:this.super_command,
            prototype_name:this.prototype_name,
            prototype_slug:this.prototype_slug,
            prototype_implement:this.prototype_implement
            };			
        var noreg                 = $("#noreg_pasien").val();
        if(noreg==""){
            smis_alert("Error","Silakan Pilih Pasien Terlebih Dahulu","alert-danger");
            return;
        }
        reg_data['command']        = 'cek_tutup_tagihan';
        reg_data['noreg_pasien']  = noreg;
        
        var res = $.ajax({
            type: "POST",
            url: "",
            data:reg_data,
            async: false
        }).responseText;

        var json = getContent(res);
        if(json=="1"){
            return false;
        }else{
            return true;
        }
    };

    ambulan_bbm.save = function(){
        showLoading();
        var cek = this.cekTutupTagihan();
        if(cek){
            TableAction.prototype.save.call(this);        
        }
        dismissLoading();
    };
	
	pasien_bbm=new TableAction("pasien_bbm","ambulatory","ambulan_bbm",new Array());
	pasien_bbm.setSuperCommand("pasien_bbm");
	pasien_bbm.setShowParentModalInChooser(false);
	pasien_bbm.selected=function(json){
		var nama=json.nama_pasien;
		var nrm=json.nrm;
		var noreg=json.id;		
		$("#nama_pasien_bbm").val(nama);
		$("#nrm_pasien_bbm").val(nrm);
		$("#noreg_pasien_bbm").val(noreg);
        $("#jenis_pasien_bbm").val(json.carabayar);
		$("#umur_pasien_bbm").val(json.umur);
		$("#tgl_lahir_pasien_bbm").val(json.tgl_lahir);
        $("#alamat_pasien_bbm").val(json.alamat_pasien);
        $("#uri_bbm").val(json.uri);
        $("#ruangan_bbm").val(json.last_ruangan);
        if(json.kelamin == "0") {
            $("#jk_pasien").val("L");
        } else if(json.kelamin == "1") {
            $("#jk_pasien").val("P");
        }
		ambulan_bbm.view();
	};

	lokasi_bbm=new TableAction("lokasi_bbm","ambulatory","ambulan_bbm",new Array());
	lokasi_bbm.setSuperCommand("lokasi_bbm");
	lokasi_bbm.selected=function(json){
		$("#ambulan_bbm_jenis_mobil").val(json.jenis_mobil);
        $("#ambulan_bbm_id_jenis_mobil").val(json.id_jenis_mobil);
        $("#ambulan_bbm_kelas").val(json.kelas);
        $("#ambulan_bbm_km_pertama").val(json.km_pertama);
        $("#ambulan_bbm_km_berikutnya").val(json.km_berikutnya);
        $("#ambulan_bbm_kondisi").val(json.doa);
        $("#ambulan_bbm_jenis_bbm").val(json.jenis_bbm);
        ambulan_bbm.set("bbm_liter",json.harga_liter);
		ambulan_bbm.hitung_biaya();
	};

	dokter_bbm=new TableAction("dokter_bbm","ambulatory","ambulan_bbm",new Array());
	dokter_bbm.setSuperCommand("dokter_bbm");
	dokter_bbm.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#ambulan_bbm_nama_dokter").val(nama);
		$("#ambulan_bbm_id_dokter").val(nip);
	};

	perawat_bbm=new TableAction("perawat_bbm","ambulatory","ambulan_bbm",new Array());
	perawat_bbm.setSuperCommand("perawat_bbm");
	perawat_bbm.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#ambulan_bbm_nama_perawat").val(nama);
		$("#ambulan_bbm_id_perawat").val(nip);
	};

	supir_bbm=new TableAction("supir_bbm","ambulatory","ambulan_bbm",new Array());
	supir_bbm.setSuperCommand("supir_bbm");
	supir_bbm.selected=function(json){
		var nama=json.nama;
		var nip=json.id;		
		$("#ambulan_bbm_nama_sopir").val(nama);
		$("#ambulan_bbm_id_sopir").val(nip);
	};

	mobil_ambulan_bbm=new TableAction("mobil_ambulan_bbm","ambulatory","ambulan_bbm",new Array());
	mobil_ambulan_bbm.setSuperCommand("mobil_ambulan_bbm");
	mobil_ambulan_bbm.selected=function(json){
		$("#ambulan_bbm_mobil").val(json.mobil);
		$("#ambulan_bbm_id_mobil").val(json.id);
	};

	$(".mydate").datepicker();
	
});