<?php 
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']=="supir"){ 
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("supir");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"");
	$super=new SuperCommand();
	$super->addResponder("supir",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
    }
}




$head=array("Tanggal","Penyewa","Jenis Mobil","Durasi Jam","Durasi Hari","Tujuan","Paramedis","Sopir","Biaya");
$uitable=new Table($head);
$uitable->setAction(false);
$uitable->setName("laporan_sewa_ambulan");
if(isset($_POST['command'])){
    require_once 'ambulatory/class/responder/LaporanSewaResponder.php';

	$adapter=new SimpleAdapter();
    $adapter->add("Tanggal", "tanggal","date d M Y")
            ->add("Penyewa", "nama")
            ->add("Jenis Mobil", "jenis_mobil")
            ->add("Durasi Jam", "durasi_jam","back Jam")
            ->add("Durasi Hari", "durasi_hari","back Hari")
            ->add("Jenis Mobil", "jenis_mobil")
            ->add("Tujuan", "tujuan")
            ->add("Paramedis", "nama_perawat")
            ->add("Sopir", "nama_sopir")
            ->add("Biaya", "biaya_perjalanan","money Rp.");
	$dbtable=new DBTable($db, "smis_amb_sewa_ambulan");
	$dbres=new LaporanSewaResponder($dbtable, $uitable, $adapter);
	if($dbres->isView() || $dbres->isExcel()){
		$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
        if(isset($_POST['id_supir']) && $_POST['id_supir'] != "" && $_POST['nama_supir']!="") {
            $dbtable->addCustomKriteria("id_sopir", "='".$_POST['id_supir']."'");
        }
	}
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$load=new Button("", "", "");
$load->setClass("btn-primary");
$load->setIsButton(Button::$ICONIC);
$load->setIcon("fa fa-refresh");
$load->setAction("laporan_sewa_ambulan.view()");

$excel=new Button("", "", "");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setAction("laporan_sewa_ambulan.excel()");

$btgrup=new ButtonGroup("");
$btgrup->addButton($load)
	   ->addButton($excel);


$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$uitable->addModal("nama_supir", "chooser-laporan_sewa_ambulan-supir", "Supir", "","y",NULL,false);
$uitable->addModal("id_supir", "hidden", "", "");
$form=$uitable->getModal()->getForm();
$form->addElement("", $btgrup);

echo $form->getHtml();
echo $uitable->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script type="text/javascript">
var laporan_sewa_ambulan;
var dokter;
$(document).ready(function(){
	$(".mydate").datepicker();
	laporan_sewa_ambulan=new TableAction("laporan_sewa_ambulan","ambulatory","laporan_sewa_ambulan");
	laporan_sewa_ambulan.addRegulerData=function(reg_data){
		reg_data['dari']=$("#laporan_sewa_ambulan_dari").val();
        reg_data['sampai']=$("#laporan_sewa_ambulan_sampai").val();
        reg_data['nama_supir']=$("#laporan_sewa_ambulan_nama_supir").val();
        reg_data['id_supir']=$("#laporan_sewa_ambulan_id_supir").val();
		return reg_data;
    };
    
    supir=new TableAction("supir","ambulatory","laporan_sewa_ambulan");
	supir.setSuperCommand("supir");
	supir.selected=function(json){
		$("#laporan_sewa_ambulan_nama_supir").val(json.nama);
		$("#laporan_sewa_ambulan_id_supir").val(json.id);
    };
    
});

</script>