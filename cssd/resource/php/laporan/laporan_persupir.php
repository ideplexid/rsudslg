<?php 
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']!="") {
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("supir");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"supir");
	$super=new SuperCommand();
	$super->addResponder("supir",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
	}
}

$head=array("Tanggal","Pasien","Lokasi","Kondisi","Supir","Biaya","Gaji Supir");
$uitable=new Table($head);
$uitable->setAction(false);
$uitable->setName("laporan_persupir");
$uitable->setFooterVisible(false);
if(isset($_POST['command'])){
	require_once 'ambulatory/class/adapter/LapSupirAdapter.php';
	$adapter=new LapSupirAdapter();
	$adapter->add("Pasien", "nama_pasien")
		   ->add("Lokasi", "lokasi")
		   ->add("Tanggal", "waktu","date d M Y")
		   ->add("Supir", "nama_sopir")
		   ->add("Kondisi", "kondisi")
		   ->add("Biaya", "biaya_perjalanan","money Rp.")
		   ->add("Gaji Supir", "gaji_sopir","money Rp.");
	$dbtable=new DBTable($db, "smis_amb_ambulan");
	$dbtable->setShowAll(true);
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	if($dbres->isView()){
		$dbtable->addCustomKriteria(NULL, "waktu>='".$_POST['dari']."'");
		$dbtable->addCustomKriteria(NULL, "waktu<'".$_POST['sampai']."'");
        if(isset($_POST['id_supir']) && $_POST['id_supir'] != "") {
            $dbtable->addCustomKriteria("id_sopir", "='".$_POST['id_supir']."'");
        }
	}
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$load=new Button("", "", "");
$load->setClass("btn-primary");
$load->setIsButton(Button::$ICONIC);
$load->setIcon("fa fa-refresh");
$load->setAction("laporan_persupir.view()");

$print=new Button("", "", "");
$print->setClass("btn-primary");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-print");
$print->setAction("laporan_persupir.print()");

$btgrup=new ButtonGroup("");
$btgrup->addButton($load)
	   ->addButton($print);

$uitable->addModal("nama", "chooser-laporan_persupir-supir", "Supir", "","y",NULL,true);
$uitable->addModal("id_supir", "hidden", "", "");
$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$form=$uitable->getModal()->getForm();
$form->addElement("", $btgrup);

echo $form->getHtml();
echo $uitable->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script type="text/javascript">
var laporan_persupir;
var supir;
$(document).ready(function(){
	$(".mydate").datepicker();
	laporan_persupir=new TableAction("laporan_persupir","ambulatory","laporan_persupir");
	laporan_persupir.addRegulerData=function(reg_data){
		reg_data['dari']=$("#laporan_persupir_dari").val();
		reg_data['sampai']=$("#laporan_persupir_sampai").val();
		reg_data['id_supir']=$("#laporan_persupir_id_supir").val();
		return reg_data;
	};

	supir=new TableAction("supir","ambulatory","laporan_persupir");
	supir.setSuperCommand("supir");
	supir.selected=function(json){
		$("#laporan_persupir_nama").val(json.nama);
		$("#laporan_persupir_id_supir").val(json.id);
	};
});

</script>