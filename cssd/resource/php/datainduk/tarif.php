<?php 
require_once 'smis-libs-class/MasterTemplate.php';
global $db;
$tarif=new MasterTemplate($db, "smis_amb_tarif", "ambulatory", "tarif");
$uitable=$tarif->getUItable();
$header=array("No.","Mobil","Status","L/D","Tarif / KM");
$uitable->setHeader($header);
$uitable->addModal("id", "hidden", "", "")
		->addModal("mobil", "chooser-tarif-tarif_mobil-Mobil", "Mobil", "")
		->addModal("id_mobil", "hidden", "", "")
        ->addModal("luar_dalam_kota", "checkbox", "Luar Kota", "")
		->addModal("tarif","money", "Tarif / KM", "");
$adapter=$tarif->getAdapter();
$adapter->setUseNumber(true, "No.","back.");
$adapter->add("Mobil", "mobil")
		->add("L/D", "luar_dalam_kota","trivial_0_Luar_Dalam")
		->add("Tarif / KM","tarif","money Rp.")
		->add("Status","status_mobil","trivial_0_Tidak Dipakai_Dipakai");
$tarif->setModalTitle("Tarif");

//supercommand
$table=new Table(array("Mobil","Plat","Keterangan"));
$table->setName("tarif_mobil");
$table->setModel(Table::$SELECT);
$dbtable=new DBTable($db,"smis_amb_mobil");
$adapter=new SimpleAdapter();
$adapter->add("Mobil","mobil");
$adapter->add("Plat","nomor");
$adapter->add("Keterangan","keterangan");
$dbres=new DBResponder($dbtable,$table,$adapter);
$tarif->getSuperCommand()->addResponder("tarif_mobil",$dbres);
$tarif->addSuperCommand("tarif_mobil", array());
$tarif->addSuperCommandArray("tarif_mobil", "mobil", "mobil");
$tarif->addSuperCommandArray("tarif_mobil", "id_mobil", "id");

$tarif->initialize();
?>