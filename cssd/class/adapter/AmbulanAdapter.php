<?php 

class AmbulanAdapter extends ArrayAdapter{
	public function adapt($d) {
		$array = array ();
		$array ['id'] = $d->id;
        $array ['Tanggal'] = self::format ( "date d M Y", $d->tanggal );
        $array ['Kelas'] = self::format ( "unslug", $d->kelas );
		$array ['Sopir'] = $d->nama_sopir;
		$biaya = $d->biaya_perjalanan + $d->biaya_perawat + $d->biaya_dokter + $d->biaya_alat + $d->biaya_lain;
        $array ['Biaya Jarak'] = self::format ( "money Rp.", $d->biaya_perjalanan );
        $array ['Jasa Perawat'] = self::format ( "money Rp.", $d->biaya_perawat );
        $array ['Biaya Desinfektan'] = self::format ( "money Rp.", $d->biaya_desinfektan );
        $array ['Total Biaya'] = self::format ( "money Rp.", $biaya );
        $array ['Jenis Mobil'] = $d->jenis_mobil;
        $array ['KM'] = $d->jarak." KM";
        $array ['Jenis BBM'] = $d->jenis_bbm;
        $array ['Jarak'] = $d->jarak." KM";
		
        $array ['Tujuan'] = $d->tujuan;
        $array ['Lokasi'] = $d->tujuan;
		$array ['Kondisi'] = $d->kondisi;
        $array ['Keterangan'] = $d->keterangan;
        $array ['Dokter'] = $d->nama_dokter;
        $array ['Perawat'] = $d->nama_perawat;
        $array ['Selesai'] = $d->selesai == 0 ? "":"<i class='fa fa-check'></i>";
		return $array;
	}
}


?>