<?php 
/**
 * this class used to adapt 
 * the cost of patient that need to be determine
 * that one data in ambulan would become several data in cashier
 * @author : Nurul Huda
 * @license : Apache 2.0
 * @since : 09-Mar-2017
 * @copyright : goblooge@gmail.com
 * */

require_once "smis-framework/smis/database/SynchronousSenderAdapter.php";
class CCSDTagihanAdapter extends SynchronousSenderAdapter{
    private $content;
    public function __construct(){
        parent::__construct();
        $this->content=array();
    }
    
    public function adapt($d){
		global $db;
        $urjigd = $d->uri==1?"URI":($d->ruangan=="igd"?"IGD":"URJ");
        $onedata=array();
		$onedata ['waktu'] = ArrayAdapter::format ( "date d M Y", $d->tanggal );
		$onedata ['nama'] = $d->nama_layanan;
		$onedata ['id'] = $d->id;
		$onedata ['start'] = $d->tanggal;
		$onedata ['end'] = $d->tanggal;
		$onedata ['biaya'] = $d->total ;
		$onedata ['satuan'] = $d->satuan ;
		$onedata ['jumlah'] = $d->quantity;
        $onedata ['keterangan'] = $d->keterangan;
		$onedata ['prop'] = $this->getProp();
		$onedata ['debet'] = getSettings($db, "ambulan-accounting-debet-penggunaan-mobil", "");
		$onedata ['kredit'] = getSettings($db, "ambulan-accounting-kredit-penggunaan-mobil", "");
        $onedata ['urjigd'] = $urjigd;
        $onedata ['tanggal_tagihan'] = $d->tanggal;

        $this->content [] = $onedata;		
        return $this->content;
    }
    
    public function getContent($data){
        parent::getContent($data);
        return $this->content;
    }
    
}

?>