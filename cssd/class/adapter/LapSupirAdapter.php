<?php 

class LapSupirAdapter extends SimpleAdapter{
	
	private $total_biaya;
	private $total_gaji;
	
	public function getContent($data){
		$dt=parent::getContent($data);
		$biaya=self::format("money Rp.", $this->total_biaya);
		$gaji=self::format("money Rp.", $this->total_gaji);
		$var=array("Pasien"=>"<strong>Total</strong>","Lokasi"=>"","Kondisi"=>"","Supir"=>"","Biaya"=>$biaya,"Gaji Supir"=>$gaji);
		$dt[]=$var;
		return $dt;
	}
	
	public function adapt($d){
		$a=parent::adapt($d);
		$this->total_biaya+=$d->biaya_perjalanan;
		$this->total_gaji+=$d->gaji_sopir;
		return $a;
	}
	
}

?>