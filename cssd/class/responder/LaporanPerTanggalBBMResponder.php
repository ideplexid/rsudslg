<?php 

class LaporanPerTanggalBBMResponder extends DBResponder{
    public function excel(){
        
        $dbtable = $this->getDBTable();
        $dbtable ->setShowAll(true);
        $data = $dbtable ->view("","0");
        $list = $data['data'];
        require_once "smis-libs-out/php-excel/PHPExcel.php";
       
        
        $fill = array();
        $fill['borders'] = array();
        $fill['borders']['allborders'] = array();
        $fill['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN ;

        $file   = new PHPExcel ();
        $file   ->setActiveSheetIndex ( 0 );
		$sheet  = $file->getActiveSheet ( 0 );
        $sheet  ->setTitle ("Rekapitulasi");

        $sheet->mergeCells("A1:L1")->setCellValue ( "A1", "LAPORAN PEMAKAIAN AMBULAN BBM/Liter RSUD SLG" );
        $sheet->mergeCells("A2:L2")->setCellValue ( "A2", "TANGGAL : ".ArrayAdapter::format("date d M Y",$_POST['dari'])." - ".ArrayAdapter::format("date d M Y",$_POST['sampai']) );
        
        
        $sheet->setCellValue("A4","Tanggal");
        $sheet->setCellValue("B4","NRM");
        $sheet->setCellValue("C4","No. Reg");
        $sheet->setCellValue("D4","Pasien");
        $sheet->setCellValue("E4","Jenis Mobil");
        $sheet->setCellValue("F4","Jenis BBM");
        $sheet->setCellValue("G4","Jarak");
        $sheet->setCellValue("H4","Kelas");
        $sheet->setCellValue("I4","Supir");
        $sheet->setCellValue("J4","Total Biaya");
        $sheet->setCellValue("K4", "Lokasi");
        $sheet->setCellValue("L4","Keterangan");
        
   
        $row = 4;
        foreach($list as $x){
            $row++;
            $sheet->setCellValue("A".$row,ArrayAdapter::format("date d M Y",$x->tanggal));
            $sheet->setCellValue("B".$row,$x->nrm_pasien);
            $sheet->setCellValue("C".$row,$x->noreg_pasien);
            $sheet->setCellValue("D".$row,$x->nama_pasien);
            $sheet->setCellValue("E".$row,$x->jenis_mobil);
            $sheet->setCellValue("F".$row,$x->jenis_bbm);
            $sheet->setCellValue("G".$row,$x->km." KM");
            $sheet->setCellValue("H".$row, ArrayAdapter::format("unslug",$x->kelas));
            $sheet->setCellValue("I".$row,$x->nama_sopir);
            $sheet->setCellValue("J".$row,$x->biaya_perjalanan);
            $sheet->setCellValue("K".$row,$x->tujuan);
            $sheet->setCellValue("L".$row,$x->keterangan);

            //$sheet->setCellValue("M".$row,$x->tujuan);
            //$sheet->setCellValue("N".$row,$x->nama_dokter);
            //$sheet->setCellValue("O".$row,$x->nama_perawat);
            
        }
        $sheet->getStyle('J5:J'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle ( "A1:L5")->getFont ()->setBold ( true );
        $sheet->getStyle ( 'A4:L'.$row )->applyFromArray ( $fill );
       
        foreach(range('A','L') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        header ( 'Content-Type: application/vnd.ms-excel' );
        header ( 'Content-Disposition: attachment;filename="Laporan Pemakaian Ambulan RSUD SLG.xls"' );
        header ( 'Cache-Control: max-age=0' );
        $writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
        $writer->save ( 'php://output' );        
    }
}