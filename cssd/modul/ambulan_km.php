<?php
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
global $db;
$header=array ('Nama','Jabatan',"NIP" );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );

$dktable = new Table ( $header );
$dktable->setName ( "supir_km" );
$dktable->setModel ( Table::$SELECT );
$supir = new EmployeeResponder ( $db, $dktable, $dkadapter, getSettings($db,"smis-ambulan-slug-supir","") );

$dktable = new Table ( array("Mobil","Status","L/D","Tarif/KM") );
$dktable->setName ( "mobil_ambulan_km" );
$dktable->setModel ( Table::$SELECT );
$adapter=new SimpleAdapter();
$adapter->add("Mobil", "mobil");
$adapter->add("Status", "status_mobil", "trivial_0_Tidak Dipakai_Dipakai");
$adapter->add("L/D", "luar_dalam","trivial_0_Dalam_Luar");
$adapter->add("Tarif/KM", "tarif","money Rp.");
$dbtable=new DBTable($db, "smis_amb_tarif");
$mobil = new DBResponder ( $dbtable,$dktable, $adapter );

/* PASIEN */
$header=array ('Nama','NRM',"No Reg" );
$ptable = new Table ($header);
$ptable->setName ( "pasien_km" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "No Reg", "id" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );

$super = new SuperCommand ();
$super->addResponder ( "supir_km", $supir );
$super->addResponder ( "pasien_km", $presponder );
$super->addResponder ( "mobil_ambulan_km", $mobil);
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}
$header=array ('Tanggal','Supir','Biaya','Lokasi',"Kondisi", "Selesai");
$_synchronous=getSettings($db,"smis-ambulan-autosync-cashier","0")=="1";
$uitable=null;
$header=array ('Tanggal','Biaya','Keterangan', "Selesai");
if($_synchronous){
    $uitable = new TableSynchronous ( $header, "&nbsp;", NULL, true );
}else{
    $uitable = new Table ( $header, "&nbsp;", NULL, true );
}
$uitable->setName ( "ambulan_km" );

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    
    //$dbtable = new DBTable ( $db, "smis_amb_ambulan" );
    require_once "ambulatory/class/dbtable/AmbulatoryDBTable.php";
    $dbtable = new AmbulatoryDBTable ( $db, "smis_amb_ambulan" );
    $dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");

    $dbtable->addCustomKriteria ( "noreg_pasien", " ='" . $_POST ['noreg_pasien'] . "'" );
    $adapter=null;
    $dbres=null;
    if($_synchronous){
        require_once "ambulatory/class/responder/AmbulanSynchronizeResponder.php";
        require_once "ambulatory/class/adapter/AmbulanSynchronizeAdapter.php";
        require_once "ambulatory/class/adapter/AmbulanTagihanAdapter.php";
        $sync_adapter=new AmbulanTagihanAdapter();
        $dbtable->activateTableSynchronous($_synchronous);
        $adapter = new AmbulanSynchronizeAdapter ();
        $dbres = new AmbulanSynchronizeResponder ( $dbtable, $uitable, $adapter,$sync_adapter );
    }else{
        require_once 'ambulatory/class/adapter/AmbulanAdapter.php';
        require_once "ambulatory/class/responder/AmbulanResponder.php";
        $adapter = new AmbulanAdapter ();
        $dbres = new AmbulanResponder ( $dbtable, $uitable, $adapter );
    }
    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$pemakaian=getSettings($db, "smis-rs-ambulan-mobil", "0")=="1";
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "tanggal", "date", "Tanggal", "" );
$uitable->addModal ( "id_mobil", "hidden", "", "" );
$uitable->addModal ( "mobil", "chooser-ambulan_km-mobil_ambulan_km-Mobil", "Mobil", "" );
$uitable->addModal ( "tarif", "money", "Tarif/KM", "" );
$uitable->addModal ( "jarak", "text", "Jarak", "" );
$uitable->addModal ( "biaya_perjalanan", "money", "Biaya", "" );
$uitable->addModal ( "lokasi", "text", "Lokasi", "" );
$uitable->addModal ( "id_sopir", "hidden", "", "" );
$uitable->addModal ( "nama_sopir", "chooser-ambulan_km-supir_km-Supir", "Supir", "" );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );
$uitable->addModal ( "selesai", "checkbox", "Selesai", "" );

$modal = $uitable->getModal ();
$modal->setTitle ( "Ambulance" );

$nrm            = new Text ( "nrm_pasien_km", "nrm_pasien_km", "" );
$noreg          = new Text ( "noreg_pasien_km", "noreg_pasien_km", "" );
$nama           = new Text ( "nama_pasien_km", "nama_pasien_km", "" );
$jenis_pasien   = new Text ( "jenis_pasien_km", "jenis_pasien_km", "" );
$umur           = new Text ( "umur_pasien_km", "umur_pasien_km", "" );
$tgl_lahir      = new Text ( "tgl_lahir_pasien_km", "tgl_lahir_pasien_km", "", "date" );
$alamat         = new Text ( "alamat_pasien_km", "alamat_pasien_km", "" );
$jk             = new Text ( "jk_pasien_km", "jk_pasien_km", "" );
$action         = new Button ( "", "", "Select" );
$action->setIsButton(Button::$ICONIC_TEXT);
$action->setClass(" btn-primary ");
$action->setIcon(" fa fa-check");
$action->setAction ( "ambulan_km.chooser('proyek_plpp','nama_pasien_km','pasien_km',pasien_km)" );
$nrm->setDisabled ( true );
$noreg->setDisabled ( true );
$nama->setDisabled ( true );
$jenis_pasien->setDisabled ( true );
$umur->setDisabled ( true );
$tgl_lahir->setDisabled ( true );
$alamat->setDisabled ( true );
$jk->setDisabled ( true );

// form for proyek
$form = new Form ( "form_pasien", "", "Ambulan" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "No Registrasi", $noreg );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Jenis Pasien", $jenis_pasien );
$form->addElement ( "Umur", $umur );
$form->addElement ( "Tgl Lahir", $tgl_lahir );
$form->addElement ( "Alamat", $alamat );
$form->addElement ( "L/P", $jk );
$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "ambulatory/resource/js/ambulan_km.js",false );

?>
