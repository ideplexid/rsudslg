<?php 
global $db;

if(isset($_POST['super_command']) && $_POST['super_command']=="petugas"){ 
	require_once 'smis-libs-hrd/EmployeeResponder.php';
	$head=array('Nama','Jabatan',"NIP");
	$dktable=new Table($head,"",NULL,true);
	$dktable->setName("petugas");
	$dktable->setModel(Table::$SELECT);
	$dkadapter=new SimpleAdapter();
	$dkadapter->add("Jabatan","nama_jabatan");
	$dkadapter->add("Nama","nama");
	$dkadapter->add("NIP","nip");
	$karyawan=new EmployeeResponder($db,$dktable,$dkadapter,"");
	$super=new SuperCommand();
	$super->addResponder("petugas",$karyawan);
	$init=$super->initialize();
	if($init != null) {
		echo $init;
		return;
    }
}




$head=array('Tanggal',"NRM","Noreg","Pasien",'Layanan',"Satuan","Quantity","Total","Keterangan","Petugas");
$uitable=new Table($head);
$uitable->setAction(false);
$uitable->setName("laporan");
if(isset($_POST['command'])){

    $adapter = new SimpleAdapter ();
	$adapter->add("NRM","nrm_pasien");
    $adapter->add("Noreg","noreg_pasien");
    $adapter->add("Pasien","nama_pasien");    
    $adapter->add("Tanggal","tanggal","date d M Y");
    $adapter->add("Layanan","nama_layanan");
    $adapter->add("Tanggal","tanggal","date d M Y");
    $adapter->add("Layanan","nama_layanan");
    $adapter->add("Satuan","satuan","money Rp.");
    $adapter->add("Quantity","quantity");
    $adapter->add("Petugas","nama_petugas");    
    $adapter->add("Keterangan","keterangan");
    $adapter->add("Total","total","money Rp.");
	$dbtable=new DBTable($db, "smis_pesanan_cssd");
	$dbres=new DBResponder($dbtable, $uitable, $adapter);
	if($dbres->isView() || $dbres->isExcel()){
		$dbtable->addCustomKriteria(NULL, "tanggal>='".$_POST['dari']."'");
        $dbtable->addCustomKriteria(NULL, "tanggal<'".$_POST['sampai']."'");
        if(isset($_POST['id_petugas']) && $_POST['id_petugas'] != "" && $_POST['nama_petugas']!="") {
            $dbtable->addCustomKriteria("id_pegawai", "='".$_POST['id_petugas']."'");
        }
	}
	$data=$dbres->command($_POST['command']);
	echo json_encode($data);
	return;
}

$load=new Button("", "", "");
$load->setClass("btn-primary");
$load->setIsButton(Button::$ICONIC);
$load->setIcon("fa fa-refresh");
$load->setAction("laporan.view()");

$excel=new Button("", "", "");
$excel->setClass("btn-primary");
$excel->setIsButton(Button::$ICONIC);
$excel->setIcon("fa fa-file-excel-o");
$excel->setAction("laporan.excel()");

$btgrup=new ButtonGroup("");
$btgrup->addButton($load);
	//    /->addButton($excel);


$uitable->addModal("dari", "date", "Dari", "");
$uitable->addModal("sampai", "date", "Sampai", "");
$uitable->addModal("nama_petugas", "chooser-laporan-petugas", "Petugas", "","y",NULL,false);
$uitable->addModal("id_petugas", "hidden", "", "");
$form=$uitable->getModal()->getForm();
$form->setTitle("Laporan CSSD");

$form->addElement("", $btgrup);

echo $form->getHtml();
echo $uitable->getHtml();
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );

?>

<script type="text/javascript">
var laporan;
var dokter;
$(document).ready(function(){
	$(".mydate").datepicker();
	laporan=new TableAction("laporan","cssd","laporan");
	laporan.addRegulerData=function(reg_data){
		reg_data['dari']=$("#laporan_dari").val();
        reg_data['sampai']=$("#laporan_sampai").val();
        reg_data['nama_petugas']=$("#laporan_nama_petugas").val();
        reg_data['id_petugas']=$("#laporan_id_petugas").val();
		return reg_data;
    };
    
    petugas=new TableAction("petugas","cssd","laporan");
	petugas.setSuperCommand("petugas");
	petugas.selected=function(json){
		$("#laporan_nama_petugas").val(json.nama);
		$("#laporan_id_petugas").val(json.id);
    };
    
});

</script>