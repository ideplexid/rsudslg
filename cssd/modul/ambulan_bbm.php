<?php
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
global $db;
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header );
$dktable->setName ( "dokter_bbm" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$dokter = new EmployeeResponder ( $db, $dktable, $dkadapter, "dokter" );

$dktable = new Table ( $header );
$dktable->setName ( "perawat_bbm" );
$dktable->setModel ( Table::$SELECT );
$perawat = new EmployeeResponder ( $db, $dktable, $dkadapter, "perawat" );

$dktable = new Table ( $header );
$dktable->setName ( "supir_bbm" );
$dktable->setModel ( Table::$SELECT );
$supir = new EmployeeResponder ( $db, $dktable, $dkadapter, getSettings($db,"smis-ambulan-slug-supir","") );

$dktable = new Table ( array("Mobil","Plat","Status","Keterangan") );
$dktable->setName ( "mobil_ambulan" );
$dktable->setModel ( Table::$SELECT );
$adapter=new SimpleAdapter();
$adapter->add("Mobil", "mobil");
$adapter->add("Plat", "nomor");
$adapter->add("Status", "status_mobil", "trivial_0_Tidak Dipakai_Dipakai");
$adapter->add("Keterangan", "keterangan");
$dbtable=new DBTable($db, "smis_amb_mobil");
$mobil = new DBResponder ( $dbtable,$dktable, $adapter );

/* PASIEN */
$header=array ('Nama','NRM',"No Reg" );
$ptable = new Table ($header);
$ptable->setName ( "pasien_bbm" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "No Reg", "id" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );

/* LOKASI */
$header=array ("Jenis Mobil","5 KM Pertama",'KM Berikutnya',"BBM","Kelas" );
$lokasi = new Table ( $header );
$lokasi->setName ( "lokasi_bbm" );
$lokasi->setModel ( Table::$SELECT );
$adapter = new SimpleAdapter ();
$adapter->add ( "Jenis Mobil", "jenis_mobil" );
$adapter->add ( "5 KM Pertama", "km_pertama","back Ltr" );
$adapter->add ( "KM Berikutnya", "km_berikutnya", "back Ltr" ); 
$adapter->add ( "Kelas", "kelas", "unslug" );
$adapter->add ( "BBM", "jenis_bbm", "unslug" );
$lresponder = new ServiceResponder ( $db, $lokasi, $adapter, "get_ambulan_bbm" );

$super = new SuperCommand ();
$super->addResponder ( "dokter_bbm", $dokter );
$super->addResponder ( "perawat_bbm", $perawat );
$super->addResponder ( "supir_bbm", $supir );
$super->addResponder ( "pasien_bbm", $presponder );
$super->addResponder ( "lokasi_bbm", $lresponder );
$super->addResponder ( "mobil_ambulan_bbm", $mobil);
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}

/*if (isset ( $_POST ['super_command'] ) && $_POST ['super_command'] == "selesai") {
    $id = $_POST['id'];
    $dbtable = new DBTable($db, "smis_amb_ambulan");
    
    $dbtable->addCustomKriteria("", "id = '".$id."' ");
}*/

$header=array ('Tanggal','Jenis Mobil',"Jenis BBM","Jarak","Total Biaya","Lokasi","Sopir","Keterangan","Selesai");
//$_synchronous=getSettings($db,"smis-ambulan-autosync-cashier","0")=="1";
//
//$uitable=null;
//$header=array ('Tanggal','Jenis Mobil',"KM","Sopir","Kelas","Biaya Desinfektan","Total Biaya","Dokter","Perawat","Selesai");
if($_synchronous){
    $uitable = new TableSynchronous ( $header, "&nbsp;", NULL, true );
}else{
    $uitable = new Table ( $header, "&nbsp;", NULL, true );
}
$uitable->setName ( "ambulan_bbm" );
/*
$print = new Button("","","");
$print->setClass("btn-inverse");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-check");
$uitable->addContentButton("activate_print",$print);
*/ 

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    //$dbtable = new DBTable ( $db, "smis_amb_ambulan" );
    require_once "ambulatory/class/dbtable/AmbulatoryDBTable.php";
    $dbtable = new AmbulatoryDBTable ( $db, "smis_amb_ambulan" );
    $dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");
    $dbtable->addCustomKriteria ( "noreg_pasien", " ='" . $_POST ['noreg_pasien'] . "'" );
    $adapter=null;
    $dbres=null;
    if($_synchronous){
       
        require_once "ambulatory/class/responder/AmbulanSynchronizeResponder.php";
        require_once "ambulatory/class/adapter/AmbulanSynchronizeAdapter.php";
        require_once "ambulatory/class/adapter/AmbulanTagihanAdapter.php";
        $sync_adapter=new AmbulanTagihanAdapter();
        $dbtable->activateTableSynchronous($_synchronous);
        $adapter = new AmbulanSynchronizeAdapter ();
        $dbres = new AmbulanSynchronizeResponder ( $dbtable, $uitable, $adapter,$sync_adapter );
    }else{
        require_once "ambulatory/class/responder/AmbulanResponder.php";
        require_once 'ambulatory/class/adapter/AmbulanAdapter.php';
        $adapter = new AmbulanAdapter ();
        $dbres = new AmbulanResponder ( $dbtable, $uitable, $adapter );
    }
    /*
    if($dbres->isSave()){
        $pertama = $_POST['km_pertama'];
        $berikutnya = $_POST['km_berikutnya'];
        $jarak = $_POST['jarak']-5;

        if($jarak>0){
            $dbres->addColumnFixValue("biaya_perjalanan",$pertama+$berikutnya*($jarak));
        }else{
            $dbres->addColumnFixValue("biaya_perjalanan",$pertama);
        }
        
    }*/

    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}


global $db;
$service = new ServiceConsumer($db, "get_kelas");
$service->setCached(true,"get_kelas");
$service->execute ();
$kelas_serv = $service->getContent ();
$opsi_kelas = new OptionBuilder();
foreach($kelas_serv as $k){
    $nama = $k ['nama'];
    $slug = $k ['slug'];
    $opsi_kelas->add ($nama,$slug);
}

$pemakaian=getSettings($db, "smis-rs-ambulan-mobil", "0")=="1";
$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "id_dokter", "hidden", "", "" );
$uitable->addModal ( "id_perawat", "hidden", "", "" );
$uitable->addModal ( "id_sopir", "hidden", "", "" );
$uitable->addModal ( "tanggal", "date", "Tanggal", "" );
if($pemakaian){
	$uitable->addModal ( "mobil", "chooser-ambulan_bbm-mobil_ambulan_bbm-Mobil", "Mobil", "" );
	$uitable->addModal ( "id_mobil", "hidden", "", "" );
}else{
	$uitable->addModal ( "mobil", "hidden", "", "" );
	$uitable->addModal ( "id_mobil", "hidden", "", "" );
}
$uitable->addModal ( "jenis_mobil", "chooser-ambulan_bbm-lokasi_bbm", "Jenis Mobil", "" );
$uitable->addModal ( "id_jenis_mobil", "hidden", "", "" );
$uitable->addModal ( "km_pertama", "hidden", "", "","n",null,true );
$uitable->addModal ( "km_berikutnya", "hidden", "", "","n",null,true );
$uitable->addModal ( "jenis_bbm", "text", "Jenis BBM", "","n",null,true );
$uitable->addModal ( "jarak", "text", "Jarak", "","n",NULL,false );
$uitable->addModal ( "bbm_liter", "money", "BBM/Liter", "","n",null,true );
$uitable->addModal ( "kebutuhan_bbm", "text", "Keb. BBM (Liter)", "","n",null,true );
$uitable->addModal ( "biaya_perjalanan", "money", "Total Biaya", "","n",null,true );

$uitable->addModal ( "tujuan", "text", "Tujuan", "" );
$uitable->addModal ( "nama_sopir", "chooser-ambulan_bbm-supir_bbm-Supir", "Supir", "" );
//$uitable->addModal ( "kelas", "select", "Kelas", $opsi_kelas->getContent(),"n",null,true );

//$uitable->addModal ( "biaya_perjalanan", "money", "Tot. Jarak", "money","n",null,true );
//$uitable->addModal ( "didampingi_perawat", "checkbox", "Di Dampingi Perawat", "0","y",null,false );
//$uitable->addModal ( "jasa_perawat", "hidden", "", getSettings($db,"smis-ambulan-persen-jasa-perawat",10),"n",null,true );
//$uitable->addModal ( "biaya_perawat", "money", "Biaya Perawat", "","y",null,true );
//$uitable->addModal ( "desinfektan", "checkbox", "Desinfektan", "0","y",null,false );
//$uitable->addModal ( "biaya_desinfektan", "money", "Biaya Desinfektan", "money","y",null,true );

//$uitable->addModal ( "gaji_sopir", "money", "Jasa Supir", "" );
//$uitable->addModal ( "nama_dokter", "chooser-ambulan_bbm-dokter_bbm", "Dokter", "" );
//$uitable->addModal ( "nama_perawat", "chooser-ambulan_bbm-perawat_bbm", "Perawat", "" );
$uitable->addModal ( "selesai", "checkbox", "Selesai", "" );



$modal = $uitable->getModal ();
$modal ->setComponentSize(Modal::$MEDIUM);
$modal->setTitle ( "Ambulance BBM" );

$nrm            = new Text ( "nrm_pasien_bbm", "nrm_pasien_bbm", "" );
$noreg          = new Text ( "noreg_pasien_bbm", "noreg_pasien_bbm", "" );
$nama           = new Text ( "nama_pasien_bbm", "nama_pasien_bbm", "" );
$jenis_pasien   = new Text ( "jenis_pasien_bbm", "jenis_pasien_bbm", "" );
$umur           = new Text ( "umur_pasien_bbm", "umur_pasien_bbm", "" );
$tgl_lahir      = new Text ( "tgl_lahir_pasien_bbm", "tgl_lahir_pasien_bbm", "", "date" );
$alamat         = new Text ( "alamat_pasien_bbm", "alamat_pasien_bbm", "" );
$jk             = new Text ( "jk_pasien_bbm", "jk_pasien_bbm", "" );

$uri            = new Hidden ( "uri_bbm", "uri_bbm", "" );
$ruangan        = new Hidden ( "ruangan_bbm", "ruangan_bbm", "" );


$action         = new Button ( "", "", "Select" );



$action->setIsButton(Button::$ICONIC_TEXT);
$action->setClass(" btn-primary ");
$action->setIcon(" fa fa-check");
$action->setAction ( "ambulan_bbm.chooser('proyek_plpp','nama_pasien','pasien_bbm',pasien_bbm)" );
$nrm->setDisabled ( true );
$noreg->setDisabled ( true );
$nama->setDisabled ( true );
$jenis_pasien->setDisabled ( true );
$umur->setDisabled ( true );
$tgl_lahir->setDisabled ( true );
$alamat->setDisabled ( true );
$jk->setDisabled ( true );

// form for proyek
$form = new Form ( "form_pasien", "", "Ambulan BBM" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "No Registrasi", $noreg );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Jenis Pasien", $jenis_pasien );
$form->addElement ( "Umur", $umur );
$form->addElement ( "Tgl Lahir", $tgl_lahir );
$form->addElement ( "Alamat", $alamat );
$form->addElement ( "L/P", $jk );
$form->addElement ( "", $uri );
$form->addElement ( "", $ruangan );
$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "ambulatory/resource/js/ambulan_bbm.js",false );

?>
