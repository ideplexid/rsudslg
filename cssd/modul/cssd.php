<?php
require_once "smis-base/smis-include-synchronize-db.php";
require_once ("smis-base/smis-include-service-consumer.php");
require_once 'smis-libs-hrd/EmployeeResponder.php';
global $db;
$header=array ('Nama','Jabatan',"NIP" );
$dktable = new Table ( $header );
$dktable->setName ( "petugas" );
$dktable->setModel ( Table::$SELECT );
$dkadapter = new SimpleAdapter ();
$dkadapter->add ( "Jabatan", "nama_jabatan" );
$dkadapter->add ( "Nama", "nama" );
$dkadapter->add ( "NIP", "nip" );
$petugas = new EmployeeResponder ( $db, $dktable, $dkadapter, "" );


/* PASIEN */
$header=array ('Nama','NRM',"No Reg" );
$ptable = new Table ($header);
$ptable->setName ( "pasien" );
$ptable->setModel ( Table::$SELECT );
$padapter = new SimpleAdapter ();
$padapter->add ( "Nama", "nama_pasien" );
$padapter->add ( "NRM", "nrm", "digit8" );
$padapter->add ( "No Reg", "id" );
$presponder = new ServiceResponder ( $db, $ptable, $padapter, "get_registered" );

/* LOKASI */
$header=array ("No.","Jenis Tindakan",'Satuan',"Tarif" );
$taif = new Table ( $header );
$taif->setName ( "layanan" );
$taif->setModel ( Table::$SELECT );
$adapter = new SimpleAdapter ();
$adapter->setUseNumber(true,"No.","back.");
$adapter->add ( "Tarif", "tarif","money" );
$adapter->add ( "Satuan", "satuan" );
$adapter->add ( "Jenis Tindakan", "jenis_tindakan" ); 
$lresponder = new ServiceResponder ( $db, $taif, $adapter, "get_ccsd" );

$super = new SuperCommand ();
$super->addResponder ( "petugas", $petugas );
$super->addResponder ( "pasien", $presponder );
$super->addResponder ( "layanan", $lresponder);
$init = $super->initialize ();
if ($init != null) {
	echo $init;
	return;
}



$header=array ('Tanggal',"NRM","Noreg","Pasien",'Layanan',"Satuan","Quantity","Total","Keterangan","Petugas");
$uitable=new Table($header);
$uitable->setName ( "cssd" );

/*
$print = new Button("","","");
$print->setClass("btn-inverse");
$print->setIsButton(Button::$ICONIC);
$print->setIcon("fa fa-check");
$uitable->addContentButton("activate_print",$print);
*/ 

/* this is respond when system have to response */
if (isset ( $_POST ['command'] )) {
    //$dbtable = new DBTable ( $db, "smis_amb_ccsd" );
    require_once "cssd/class/dbtable/ccsdDBTable.php";
    require_once "cssd/class/responder/CCSDResponder.php";
    
    $dbtable = new ccsdDBTable ( $db, "smis_pesanan_cssd" );
    $dbtable ->setAutoSynch(getSettings($db,"cashier-real-time-tagihan","0")!="0");
    $dbtable->addCustomKriteria ( "noreg_pasien", " ='" . $_POST ['noreg_pasien'] . "'" );
    $adapter=null;
    $dbres=null;
    $adapter = new SimpleAdapter ();
    $adapter->add("NRM","nrm_pasien");
    $adapter->add("Noreg","noreg_pasien");
    $adapter->add("Pasien","nama_pasien");    
    $adapter->add("Tanggal","tanggal","date d M Y");
    $adapter->add("Layanan","nama_layanan");
    $adapter->add("Satuan","satuan","money Rp.");
    $adapter->add("Quantity","quantity");
    $adapter->add("Petugas","nama_petugas");    
    $adapter->add("Keterangan","keterangan");
    $adapter->add("Total","total","money Rp.");
    $dbres = new CCSDResponder ( $dbtable, $uitable, $adapter );

    $data = $dbres->command ( $_POST ['command'] );
	echo json_encode ( $data );
	return;
}

$uitable->addModal ( "id", "hidden", "", "" );
$uitable->addModal ( "tanggal", "date", "Tanggal", date("Y-m-d") );
$uitable->addModal ( "nama_petugas", "chooser-cssd-petugas-Petugas", "Petugas", "" );
$uitable->addModal ( "id_pegawai", "hidden", "", "");
$uitable->addModal ( "nama_layanan", "chooser-cssd-layanan-Layanan", "Layanan", "" );
$uitable->addModal ( "id_layanan", "hidden", "", "");

//$uitable->addModal ( "gaji_sopir", "money", "Jasa Supir", "" );
$uitable->addModal ( "satuan", "money", "Harga Satuan", "","m",null,true );
$uitable->addModal ( "quantity", "text", "Quantity", 1 );
$uitable->addModal ( "total", "money", "Total", "","m",null,true );
$uitable->addModal ( "keterangan", "textarea", "Keterangan", "" );

$modal = $uitable->getModal ();
$modal ->setComponentSize(Modal::$MEDIUM);
$modal->setTitle ( "CSSD" );

$noreg = ComponentFactory::createComponent("noreg_pasien","","noreg_pasien","Noreg Pasien","chooser-cssd-pasien-Pasien","","");

$nrm            = new Text ( "nrm_pasien", "nrm_pasien", "" );
//$noreg          = new Text ( "noreg_pasien", "noreg_pasien", "" );
$nama           = new Text ( "nama_pasien", "nama_pasien", "" );
$jenis_pasien   = new Text ( "jenis_pasien", "jenis_pasien", "" );
$umur           = new Text ( "umur_pasien", "umur_pasien", "" );
$tgl_lahir      = new Text ( "tgl_lahir_pasien", "tgl_lahir_pasien", "", "date" );
$alamat         = new Text ( "alamat_pasien", "alamat_pasien", "" );
$jk             = new Text ( "jk_pasien", "jk_pasien", "" );

$uri            = new Hidden ( "uri", "uri", "" );
$ruangan        = new Hidden ( "ruangan", "ruangan", "" );


$action         = new Button ( "", "", "Select" );



$action->setIsButton(Button::$ICONIC_TEXT);
$action->setClass(" btn-primary ");
$action->setIcon(" fa fa-check");
$action->setAction ( "cssd.chooser('proyek_plpp','nama_pasien','pasien',pasien)" );
$nrm->setDisabled ( true );
$noreg->setDisabled ( true );
$nama->setDisabled ( true );
$jenis_pasien->setDisabled ( true );
$umur->setDisabled ( true );
$tgl_lahir->setDisabled ( true );
$alamat->setDisabled ( true );
$jk->setDisabled ( true );

// form for proyek
$form = new Form ( "form_pasien", "", "cssd" );
$form->addElement ( "NRM", $nrm );
$form->addElement ( "No Registrasi", $noreg );
$form->addElement ( "Nama", $nama );
$form->addElement ( "Jenis Pasien", $jenis_pasien );
$form->addElement ( "Umur", $umur );
$form->addElement ( "Tgl Lahir", $tgl_lahir );
$form->addElement ( "Alamat", $alamat );
$form->addElement ( "L/P", $jk );
$form->addElement ( "", $uri );
$form->addElement ( "", $ruangan );
//$form->addElement ( "", $action );
echo $form->getHtml ();

/* table of current content */
echo "<div id='table_content'>";
echo $uitable->getHtml ();
echo $modal->getHtml ();
echo "</div>";
echo addJS ( "framework/bootstrap/js/bootstrap-datepicker.js" );
echo addJS ( "framework/smis/js/table_action.js" );
echo addCSS ( "framework/bootstrap/css/datepicker.css" );
echo addJS ( "cssd/resource/js/cssd.js",false );

?>
